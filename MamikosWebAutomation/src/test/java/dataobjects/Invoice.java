package dataobjects;

public class Invoice {
    private static ThreadLocal<String> invoiceNumber = new ThreadLocal<>();
    private static ThreadLocal<String> totalAmount = new ThreadLocal<>();

    /**
     * save invoice number
     * @param inv
     */
    public static synchronized void setInvoiceNumber(String inv) {
        invoiceNumber.set(inv);
    }

    /**
     * get invoice number
     * @return
     */
    public static synchronized String getInvoiceNumber() {
        return invoiceNumber.get();
    }

    /**
     * save Total Amount
     * @param total
     */
    public static synchronized void setTotalAmount(String total) {
        totalAmount.set(total);
    }

    /**
     * get Total Amount
     * @return
     */
    public static synchronized String getTotalAmount() {
        return totalAmount.get();
    }
}
