package runners.consultant;

import io.cucumber.testng.CucumberOptions;
import runners.mamikos.BaseTestRunner;


@CucumberOptions(
        plugin = {"json:target/results/consultant/cucumber-report.json",  "html:target/results/consultant"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@crm-staging"}
)

public class ConsultantToolsTestRunner extends BaseTestRunner
{

}
