package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/listingGP/cucumber-report.json",  "html:target/results/listingGP"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@listingGP"}

)
public class ListingGoldplusTestRunner extends BaseTestRunner
{

}
