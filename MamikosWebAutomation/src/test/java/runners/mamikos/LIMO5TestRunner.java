package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/listingGP5/cucumber-report.json",  "html:target/results/listingGP5"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@LIMO5"

)
public class LIMO5TestRunner extends BaseTestRunner
{

}