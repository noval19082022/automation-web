package runners.mamikos;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/dom3/cucumber-report.json",  "html:target/results/dom3"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@DOM3"
)
public class DOM3TestRunner extends BaseTestRunner{
}
