package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/listingGP4/cucumber-report.json",  "html:target/results/listingGP2"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@LIMO4"

)
public class LIMO4TestRunner extends BaseTestRunner
{

}