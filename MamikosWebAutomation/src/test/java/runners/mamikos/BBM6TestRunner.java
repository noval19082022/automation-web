package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking_billing_management_pipeline_6/cucumber-report.json",  "html:target/results/booking_billing_management_pipeline_6"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@BBM6"
)
public class BBM6TestRunner extends BaseTestRunner{
}
