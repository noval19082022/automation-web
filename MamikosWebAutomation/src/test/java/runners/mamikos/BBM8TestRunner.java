package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking_billing_management_pipeline_8/cucumber-report.json",  "html:target/results/booking_billing_management_pipeline_8"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@BBM8"}
)
public class BBM8TestRunner extends BaseTestRunner {
}
