package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/tenant_engagement/cucumber-report.json",  "html:target/results/tenant_engagement"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@BBM"}

)
public class BBMTestRunner extends BaseTestRunner{
}
