package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/listingGP3/cucumber-report.json",  "html:target/results/listingGP3"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@LIMO3"

)
public class LIMO3TestRunner extends BaseTestRunner
{

}