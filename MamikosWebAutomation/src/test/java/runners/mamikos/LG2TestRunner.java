package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/listingGP2/cucumber-report.json",  "html:target/results/listingGP2"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@LG2 or @listing-monetization"}

)
public class LG2TestRunner extends BaseTestRunner
{

}