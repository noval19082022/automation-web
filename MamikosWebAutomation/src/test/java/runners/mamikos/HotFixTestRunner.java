package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
					plugin = {"json:target/results/hotfix/cucumber-report.json",  "html:target/results/hotfix"},
					features = "src/test/resources/features",
					glue = "steps",
					tags = {"@hotfix"}

		)
public class HotFixTestRunner extends BaseTestRunner
{
	
}


