package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/limo1/cucumber-report.json",  "html:target/results/limo1"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@LIMO1"

)
public class LIMO1TestRunner extends BaseTestRunner
{

}
