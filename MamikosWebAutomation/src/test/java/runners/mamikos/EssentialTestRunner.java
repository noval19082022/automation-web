package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/essential/cucumber-report.json",  "html:target/results/essential"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@essentiaTest"}

)
public class EssentialTestRunner extends BaseTestRunner
{

}