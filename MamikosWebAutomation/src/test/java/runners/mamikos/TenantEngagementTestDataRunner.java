package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@teng-cookies"}
)
public class TenantEngagementTestDataRunner extends BaseTestRunner{
}
