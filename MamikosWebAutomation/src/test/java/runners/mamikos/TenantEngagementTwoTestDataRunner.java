package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@teng-cookies1"}
)
public class TenantEngagementTwoTestDataRunner extends BaseTestRunner{
}
