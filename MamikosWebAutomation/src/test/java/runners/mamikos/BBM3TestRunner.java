package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking_billing_management_pipeline_3/cucumber-report.json",  "html:target/results/booking_billing_management_pipeline_3"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@BBM3"}

)
public class BBM3TestRunner extends BaseTestRunner{
}
