package runners.mamikos;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/dom5/cucumber-report.json",  "html:target/results/dom5"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@DOM5"
)
public class DOM5TestRunner extends BaseTestRunner{
}
