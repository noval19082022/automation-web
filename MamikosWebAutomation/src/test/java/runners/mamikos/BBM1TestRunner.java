package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking_billing_management_pipeline_1/cucumber-report.json",  "html:target/results/booking_billing_management_pipeline_1"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@BBM1"

)
public class BBM1TestRunner extends BaseTestRunner{
}
