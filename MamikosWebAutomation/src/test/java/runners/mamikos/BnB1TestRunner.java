package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/manage_booking1/cucumber-report.json",  "html:target/results/manage_booking1"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@occupancyAndBilling1"}

)
public class BnB1TestRunner extends BaseTestRunner
{

}