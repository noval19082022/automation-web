package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/dom/cucumber-report.json",  "html:target/results/dom"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@DOM"}

)
public class DomTestRunner extends BaseTestRunner
{

}