package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking_billing_management_pipeline_2/cucumber-report.json",  "html:target/results/booking_billing_management_pipeline_2"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@BBM2"}

)
public class BBM2TestRunner extends BaseTestRunner{
}
