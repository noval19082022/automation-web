package runners.mamikos;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/dom2/cucumber-report.json",  "html:target/results/dom2"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@DOM2"
)
public class DOM2TestRunner extends BaseTestRunner{
}
