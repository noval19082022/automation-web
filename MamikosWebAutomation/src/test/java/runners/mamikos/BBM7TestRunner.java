package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking_billing_management_pipeline_7/cucumber-report.json",  "html:target/results/booking_billing_management_pipeline_7"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@BBM7"}
)
public class BBM7TestRunner extends BaseTestRunner {
}
