package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/limo2/cucumber-report.json",  "html:target/results/limo2"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@LIMO2"

)
public class LIMO2TestRunner extends BaseTestRunner
{

}