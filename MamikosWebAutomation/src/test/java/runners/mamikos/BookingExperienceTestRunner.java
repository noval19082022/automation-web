package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking-experience/cucumber-report.json",  "html:target/results/booking-experience"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@bookingExperience or @booking-experience"}

)
public class BookingExperienceTestRunner extends BaseTestRunner
{

}
