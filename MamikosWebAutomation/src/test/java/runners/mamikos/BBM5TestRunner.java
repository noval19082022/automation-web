package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking_billing_management_pipeline_5/cucumber-report.json",  "html:target/results/booking_billing_management_pipeline_5"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@BBM5"}

)
public class BBM5TestRunner extends BaseTestRunner{
}
