package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/propertyManagement/cucumber-report.json",  "html:target/results/propertyManagement"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@pman"}
)

public class PropertyManagementTestRunner extends BaseTestRunner
{

}
