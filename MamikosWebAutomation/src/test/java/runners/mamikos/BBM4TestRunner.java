package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/booking_billing_management_pipeline_4/cucumber-report.json",  "html:target/results/booking_billing_management_pipeline_4"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@BBM4"

)
public class BBM4TestRunner extends BaseTestRunner{
}
