package runners.mamikos;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import utilities.ThreadManager;


public class BaseTestRunner extends AbstractTestNGCucumberTests 
{
	@BeforeClass (alwaysRun=true)
	@Parameters({"browserName", "runnerName"})
	public void beforeClass(String browserName, @Optional("default") String runnerName)
	{
		ThreadManager.setBrowser(browserName);
		ThreadManager.setRunnerName(runnerName);
	}
}


