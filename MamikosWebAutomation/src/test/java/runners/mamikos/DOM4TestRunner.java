package runners.mamikos;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/dom4/cucumber-report.json",  "html:target/results/dom4"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = "@DOM4"
)
public class DOM4TestRunner extends BaseTestRunner{
}
