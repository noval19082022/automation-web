package runners.mamikos;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/manage_booking/cucumber-report.json",  "html:target/results/manage_booking"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@bnb"}

)
public class BnBTestRunner extends BaseTestRunner
{

}