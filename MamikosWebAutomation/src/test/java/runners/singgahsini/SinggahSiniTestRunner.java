package runners.singgahsini;

import io.cucumber.testng.CucumberOptions;
import runners.mamikos.BaseTestRunner;

@CucumberOptions(
        plugin = {"json:target/results/singgahsini/cucumber-report.json",  "html:target/results/singgahsini"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@singgahsini"}
)



public class SinggahSiniTestRunner extends BaseTestRunner {
}
