package steps.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import pageobjects.backoffice.*;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchContractSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SearchContractPO searchContract = new SearchContractPO(driver);
    private SearchInvoicePO searchInvoice = new SearchInvoicePO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    private LoginPO login = new LoginPO(driver);
    private LeftMenuPO left = new LeftMenuPO(driver);
    private ApplyAddsOnContractPO addon = new ApplyAddsOnContractPO(driver);

    //Test Data
    private String propertyFile1 = "src/test/resources/testdata/mamikos/OB.properties";
    private String kostName = JavaHelpers.getPropertyValue(propertyFile1, "kostNameBookingFemale_" + Constants.ENV);

    //test Data Consultant
    private String consultantProperty = "src/test/resources/testdata/consultant/consultant.properties";
    private String tenantProperty = "src/test/resources/testdata/consultant/tenant.properties";
    private String tenantHP = JavaHelpers.getPropertyValue(tenantProperty, "tenantHp_yudha_" + Constants.ENV);
    private String paymentDate = JavaHelpers.getPropertyValue(consultantProperty, "paymentDate_" + Constants.ENV);

    //Test Data Payment
    private String payment = "src/test/resources/testdata/mamikos/payment.properties";
    private String propertyName_ = JavaHelpers.getPropertyValue(payment, "propertyName_" + Constants.ENV);
    private String propertyName_BNI = JavaHelpers.getPropertyValue(payment, "propertyName_BNI_" + Constants.ENV);

    //Test Data Apply Voucher
    private String voucher = "src/test/resources/testdata/mamikos/voucherku.properties";
    private String kostNameForApplyVoucherBBK = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherBBK_" + Constants.ENV);
    private String kostNameForApplyVoucherMamirooms = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherMamirooms_" + Constants.ENV);
    private String kostNameForApplyVoucherPremium = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherPremium_" + Constants.ENV);
    private String kostNameForApplyVoucherMamichecker = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherVerifiedByMamichecker_" + Constants.ENV);
    private String kostNameForApplyVoucherGaransi = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherGaransi_" + Constants.ENV);
    private String kostNameForApplyVoucherGoldPlus = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherGoldPlus_" + Constants.ENV);
    private String kostNameForApplyVoucherNewGoldPlus1 = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherNewGoldPlus1_" + Constants.ENV);
    private String kostNameForApplyVoucherNewGoldPlus2 = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherNewGoldPlus2_" + Constants.ENV);
    private String kostNameForApplyVoucherNewGoldPlus3 = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherNewGoldPlus3_" + Constants.ENV);

    //Test Data MamiPoin
    private String mamipoin = "src/test/resources/testdata/mamikos/mamipoin.properties";
    private String kostNameForApplyMamiPoin = JavaHelpers.getPropertyValue(mamipoin, "kostNameForApplyMamiPoin_" + Constants.ENV);

    //Test Data
    private String obProperty = "src/test/resources/testdata/mamikos/OB.properties";
    private String additionalPricePhoneNumber = JavaHelpers.getPropertyValue(obProperty, "additionalPricePhone_" + Constants.ENV);
    private String bookingOwnerPhone = JavaHelpers.getPropertyValue(obProperty, "uprasOwner_" + Constants.ENV);
    private String additionalPriceTenantPhone = JavaHelpers.getPropertyValue(obProperty, "additionalPricePhone_" + Constants.ENV);

    //Tenant Engagement Test Data
    private String tengTenantTD = "src/test/resources/testdata/mamikos/tenant-engagement.properties";
    private String tengTenantphoneNumber = JavaHelpers.getPropertyValue(tengTenantTD, "sakti_" + Constants.ENV);
    private String tengTenantApplyVoucher = JavaHelpers.getPropertyValue(tengTenantTD, "tengTenantApplyVoucher_" + Constants.ENV);
    private String adiTengApplyVoucher = JavaHelpers.getPropertyValue(tengTenantTD, "adiTengApplyVoucherPhone_" + Constants.ENV);
    private String voucherBaseOnUser = JavaHelpers.getPropertyValue(tengTenantTD, "tengVoucherBaseOnUserPhone_" + Constants.ENV);
    private String adiTengApplyVoucherDua = JavaHelpers.getPropertyValue(tengTenantTD, "adiTengApplyVoucherDuaPhone_" + Constants.ENV);
    private String tenantWhitelistMamipoin = JavaHelpers.getPropertyValue(tengTenantTD, "tenantWhitelistMamipoinPhone_" + Constants.ENV);
    private String tenantBlacklistMamipoin = JavaHelpers.getPropertyValue(tengTenantTD, "tenantBlacklistMamipoinPhone_" + Constants.ENV);
    private String adiTengAddOnsPhone = JavaHelpers.getPropertyValue(tengTenantTD, "adiTengAddOnsPhone_" + Constants.ENV);
    private String bbmApplyVocNumber = JavaHelpers.getPropertyValue(tengTenantTD, "bbmApplyVocPhone_" + Constants.ENV);

    //Booking and Billing Test Data
    private String obTenantTD = "src/test/resources/testdata/mamikos/OB.properties";
    private String obTenantStatus = JavaHelpers.getPropertyValue(obTenantTD, "tenantStatus_" + Constants.ENV);
    private String bbmAcceptChat = JavaHelpers.getPropertyValue(obProperty, "tenantAcceptChatPhone_" + Constants.ENV);

    @Then("user Navigate {string} page")
    public void user_navigate_page(String pageHeader) {
        Assert.assertEquals(searchContract.getSearchContractPageHeader(), pageHeader, "Page Header does not match");
    }

    @Then("user search for Kost with name")
    public void user_search_for_Kost_with_name() throws InterruptedException {
        searchContract.selectFilterSearchBy("Kost Name");
        searchContract.enterTextToSearchTextbox(kostName);
        searchContract.clickOnSearchContractButton();
    }

    @Then("user search by Kost name {string}")
    public void user_search_by_Kost_name(String kos) throws InterruptedException {
        searchContract.selectFilterSearchBy("Kost Name");
        searchContract.enterTextToSearchTextbox(kos);
        searchContract.clickOnSearchContractButton();
    }

    @And("user search by renter phone {string}")
    public void user_search_by_renter_phone(String renterPhoneNumber) throws InterruptedException {
        searchContract.selectFilterSearchBy("Renter Phone Number");
        searchContract.enterTextToSearchTextbox(renterPhoneNumber);
        searchContract.clickOnSearchContractButton();
    }

    @Then("user click on Cancel the Contract Button from action column")
    public void user_click_on_Cancel_the_Contract_Button_from_action_column() throws InterruptedException {
        if (searchContract.getBookingStatus().equalsIgnoreCase("booked") || searchContract.getBookingStatus().equalsIgnoreCase("active")) {
            searchContract.clickOnBookingCancelButton();
        }
    }

    @When("user access search contract menu")
    public void user_access_search_contract_menu() throws InterruptedException {
        left.clickOnSearchContractMenu();
    }

    @When("user search for {string} and cancel contract")
    public void user_search_for_and_cancel_contract(String kosName) throws InterruptedException {
        searchContract.selectFilterSearchBy("Kost Name");
        if (kosName.equals("payment")) {
            searchContract.enterTextToSearchTextbox(propertyName_);
        } else if (kosName.equals("paymentBNI")) {
            searchContract.enterTextToSearchTextbox(propertyName_BNI);
        } else if (kosName.equals("applyVoucherBBK")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyVoucherBBK);
        } else if (kosName.equals("applyVoucherMamirooms")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyVoucherMamirooms);
        } else if (kosName.equals("applyVoucherPremium")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyVoucherPremium);
        } else if (kosName.equals("applyVoucherMamichecker")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyVoucherMamichecker);
        } else if (kosName.equals("applyVoucherGaransi")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyVoucherGaransi);
        } else if (kosName.equals("applyVoucherGoldPlus")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyVoucherGoldPlus);
        } else if (kosName.equals("applyVoucherNewGoldPlus1")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyVoucherNewGoldPlus1);
        } else if (kosName.equals("applyVoucherNewGoldPlus2")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyVoucherNewGoldPlus2);
        } else if (kosName.equals("applyVoucherNewGoldPlus3")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyVoucherNewGoldPlus3);
        } else if (kosName.equals("potentialTenant")) {
            searchContract.enterTextToSearchTextbox(tenantHP);
        } else if (kosName.equals("applyMamiPoin")) {
            searchContract.enterTextToSearchTextbox(kostNameForApplyMamiPoin);
        } else {
            searchContract.enterTextToSearchTextbox(kosName);
        }
        searchContract.clickOnSearchContractButton();
        searchContract.clickOnCancelContractButton();
        searchContract.checkTenantActiveContract();
    }

    @When("user terminate contract using url")
    public void user_terminate_contract_using_url() throws InterruptedException {
        String mainUrl = Constants.BACKOFFICE_URL + "/contract/terminate/";
        String contractID = searchContract.getIDContract();
        selenium.navigateToPage(mainUrl + contractID);
    }

    @When("user access property management menu")
    public void user_access_property_management_menu() throws InterruptedException {
        selenium.hardWait(5);
        selenium.pageScrollInView(By.xpath("//a[@href='https://jambu.kerupux.com/admin/property']"));
        selenium.clickOn(By.xpath("//a[@href='https://jambu.kerupux.com/admin/property']"));
    }

    @When("user search property name {string}")
    public void user_search_property_name(String propertyName) throws InterruptedException {
        selenium.hardWait(2);
        selenium.enterText(By.name("property_name"), propertyName, true);
        selenium.clickOn(By.id("buttonSearch"));
    }

    @When("system display property name {string}")
    public void system_display_property_name(String propertyName) throws InterruptedException {
        selenium.hardWait(2);

    }

    @Then("user search contract by tenant phone number")
    public void user_search_contract_by_tenant_phone_number() throws InterruptedException {
        searchContract.searchContractbyTenantPhoneNumber(tenantHP);
    }

    @When("user terminate active contract")
    public void user_terminate_active_contract() throws InterruptedException {
        searchContract.clickOnTerminateContractButton();
    }

    @And("user cancel active contract")
    public void user_cancel_active_contract() {
        searchContract.clickOnCancelContractButton();
    }

    @And("check tenant have active contract")
    public void check_tenant_have_active_contract() {
        searchContract.checkTenantActiveContract();
    }

    @When("user search contract by tenant phone number {string}")
    public void user_search_contract_by_tenant_phone_number_x(String phoneNumber) throws InterruptedException {
        String tenantPhoneNumber = null;
        if (phoneNumber.equalsIgnoreCase("ob additional price phone number")) {
            tenantPhoneNumber = additionalPricePhoneNumber;
        } else if (phoneNumber.equalsIgnoreCase("ob additional price kost with dp automation")) {
            tenantPhoneNumber = additionalPriceTenantPhone;
        } else if (phoneNumber.equalsIgnoreCase("teng tenant phone number")) {
            tenantPhoneNumber = tengTenantphoneNumber;
        } else if (phoneNumber.equalsIgnoreCase("teng tenant apply voucher")) {
            tenantPhoneNumber = tengTenantApplyVoucher;
        } else if (phoneNumber.equalsIgnoreCase("adi TENG apply voucher")) {
            tenantPhoneNumber = adiTengApplyVoucher;
        } else if (phoneNumber.equalsIgnoreCase("voucher base on user")) {
            tenantPhoneNumber = voucherBaseOnUser;
        } else if (phoneNumber.equalsIgnoreCase("ob tenant status")) {
            tenantPhoneNumber = obTenantStatus;
        } else if (phoneNumber.equalsIgnoreCase("adi TENG apply voucher dua")) {
            tenantPhoneNumber = adiTengApplyVoucherDua;
        } else if (phoneNumber.equalsIgnoreCase("adi TENG Tenant Add Ons")) {
            tenantPhoneNumber = adiTengAddOnsPhone;
        } else if (phoneNumber.equalsIgnoreCase("tenant whitelist mamipoin")) {
            tenantPhoneNumber = tenantWhitelistMamipoin;
        } else if (phoneNumber.equalsIgnoreCase("tenant blacklist mamipoin")) {
            tenantPhoneNumber = tenantBlacklistMamipoin;
        } else if (phoneNumber.equalsIgnoreCase("bbm accept from chat phone number")) {
            tenantPhoneNumber = bbmAcceptChat;
        } else if (phoneNumber.equalsIgnoreCase("BBM apply voc1 phone number")) {
            tenantPhoneNumber = bbmApplyVocNumber;
        }

        else {
            tenantPhoneNumber = phoneNumber;
        }
        searchContract.searchContractbyTenantPhoneNumber(tenantPhoneNumber);
    }

    @Then("user set invoice of contract from unpaid to paid")
    public void user_set_invoice_of_contract_from_unpaid_to_paid() throws InterruptedException {
        searchContract.clickOnInvoiceUrl();
        searchInvoice.clickOnChangeStatusButton();
        searchInvoice.selectPaymentStatus();
        searchInvoice.setPaymentDate(paymentDate);
        searchInvoice.clickOnSubmitButton();
    }

    @Then("admin backoffice add adds on to tenant contract")
    public void admin_backoffice_add_adds_on_to_tenant_contract() throws InterruptedException {
        searchContract.clickOnAddOn();
        addon.setAddOnByIndex(5);
    }

    @Then("user verify {string} column is displayed")
    public void user_verify_contract_id_column_is_displayed(String contractId) {
        Assert.assertTrue(searchContract.isContractIDColumnAppeared(contractId), "Contract ID column is not appeared");
        Assert.assertEquals(searchContract.getContractIDColumnText(contractId), contractId, "Contract ID column text is not equal to " + contractId);
    }

    @And("user verify Contract Id value is {string}")
    public void user_verify_Contract_Id_value_is(String contractId) {
        Assert.assertTrue(searchContract.isContractIDValueAppeared(contractId), "Contract ID value is not appeared");
        Assert.assertEquals(searchContract.getContractIDValueText(contractId), contractId, "Contract id value is not equal to " + contractId);
    }

    @And("user verify Owner Id value is {string}")
    public void user_verify_Owner_Id_value_is(String ownerId) {
        Assert.assertTrue(searchContract.isOwnerIDValueTextAppeared(ownerId), "Owner ID value is not appeared");
        Assert.assertEquals(searchContract.getOwnerIDValueText(ownerId), ownerId, "Owner ID value is not equal to " + ownerId);
    }

    @When("admin clicks on add ons button on contract {string} index")
    public void admin_clicks_on_add_ons_button_on_contract_index(String index) throws InterruptedException {
        searchContract.clicksOnAddOnsIndex(index);
    }

    @When("admin add add ons {string} index to tenant contract")
    public void admin_add_add_ons_index_to_tenant_contract(String index) throws InterruptedException {
        addon.setAddOnDropDownByIndex(index);
    }

    @When("admin add add ons {string} to tenant contract")
    public void admin_add_add_ons_to_tenant_contract(String text) throws InterruptedException {
        addon.setAddOnDropDownByText(text);
    }

    @When("admin clicks on invoice number {string} on first index contract")
    public void admin_clicks_on_invoice_number_on_first_index_contract(String index) throws InterruptedException {
        searchContract.clicksOnInvoiceNumberOnFirstIndex(index);
    }

    @When("admin deletes add ons from contract flow")
    public void admin_deletes_add_ons_from_contract_flow() throws InterruptedException {
        searchContract.deleteAddOns();
    }

    @Then("admin can sees callout message is {string}")
    public void admin_can_sees_callout_message_is(String callOutMessage) {
        Assert.assertEquals(searchContract.getCalloutText(), callOutMessage);
    }

    @And("user input biaya kerusakan {string}")
    public void user_input_biaya_kerusakan(String biayaKerusakan) throws InterruptedException {
        searchContract.inputBiayaKerusakan(biayaKerusakan);
    }

    @And("user fills kost level {string}")
    public void user_fills_kost_level(String kostLevel) throws InterruptedException {
        searchContract.fillsKostLevel(kostLevel);
    }

    @And("user input nama pemilik rekening {string}")
    public void user_fills_name_pemilik_rekening(String inputNameRekening) throws InterruptedException {
        searchContract.fillsNamePemilikRekening(inputNameRekening);
    }

    @And("user input nomor rekening {string}")
    public void user_fills_nomor_rekening(String inputNomorRekening) throws InterruptedException {
        searchContract.fillsNomorRekening(inputNomorRekening);
    }

    @And("user click drop down bank name and choose one bank")
    public void click_choose_bank_name() throws InterruptedException {
        searchContract.chooseBankName();
    }

    @And("user click search button")
    public void user_click_search_button() throws InterruptedException {
        searchContract.clickSearchButton();
    }

    @And("user click button extend contract")
    public void user_click_extend_kontrak_button() throws InterruptedException {
        searchContract.clickExtendContractButton();
    }

    @And("user fills duration {string} month")
    public void user_fills_duration_month(String month) throws InterruptedException {
        searchContract.fillsDurationMonth(month);
    }

    @And("user click extend button")
    public void user_click_extend_button() throws InterruptedException {
        searchContract.clickExtendButton();
    }

    @And("user click ganti owner button")
    public void user_click_ganti_owner_button() throws InterruptedException {
        searchContract.clickGantiOwnerButton();
    }

    @And("user click see log button")
    public void user_click_see_log_button() throws InterruptedException {
        searchContract.clickSeeLogButton();
    }

    @And("user click akhiri contract button")
    public void user_click_akhiri_contract_button() throws InterruptedException {
        searchContract.clickAkhiriContractButton();
    }

    @And("user input new owner phone number {string}")
    public void user_input_number_owner(String inputNumberOwner) throws InterruptedException {
        searchContract.inputNumberOwnerText(inputNumberOwner);
    }

    @And("user click cari alternatif pemilik")
    public void user_click_cari_alternatif_pemilik() throws InterruptedException {
        searchContract.clickCariAlternatifPemilik();
    }

    @And("user click proses ganti pemilik")
    public void user_click_proses_ganti_pemilik() throws InterruptedException {
        searchContract.clickProsesGantiPemilik();
    }

    @And("user click cancel proses ganti pemilik")
    public void user_click_cancel_proses_ganti_pemilik() throws InterruptedException {
        searchContract.clickCancelProsesGantiPemilik();
    }

    @And("user click edit deposit button")
    public void user_click_edit_deposit_button() throws InterruptedException {
        searchContract.clickEditDepositButton();
    }

    @And("user input transfer date")
    public void user_input_transfer_date() throws InterruptedException {
        searchContract.userInputActiveTransferDate();
        if (searchContract.getDateTransfer()) {
            searchContract.userInputDayTransferDate();
        }
    }

    @And("user click on simpan Draft button")
    public void user_click_on_simpan_draft_button() throws InterruptedException {
        searchContract.userClickOnSimpanDraftButton();
    }

    @And("user Input damage details {string}")
    public void user_view_damage_details(String inputText) throws InterruptedException {
        for (int i = 1; i < 9; i++) {
            searchContract.inputTextDamageDetails(inputText + i);
        }
    }

    @Then("user see maximal length {string}")
    public void user_see_maximal_length(String maxlenght) throws InterruptedException {
        Assert.assertEquals(searchContract.userSeeMaximalLength(), maxlenght, "200");
    }

    @Then("user see dropdown will be close")
    public void user_see_dropdown_will_be_close() throws InterruptedException {
        Assert.assertEquals(searchContract.getNameBank(), "ANZ Indonesia");
    }

    @Then("data other owner will be gone")
    public void data_other_owner_will_be_gone() throws InterruptedException {
        Assert.assertEquals(searchContract.getInformasiPemilik(), "Informasi Pemilik Sekarang");
    }

    @Then("user redirected to menu search contract")
    public void user_redirected_to_menu_search_contract() throws InterruptedException {
        Assert.assertEquals(searchContract.getSearchContractPageHeader(), "Search Contract");
    }

    @Then("user will get blank data detail")
    public void user_will_get_blank_data() throws InterruptedException {
        Assert.assertEquals(searchContract.getSearchContractPageHeader(), "Search Contract");
        Assert.assertTrue(searchContract.checkBlankData(), "Data Display");
    }

    @Then("user will see sisa deposit")
    public void user_will_see_sisa_deposit() throws InterruptedException {
        Assert.assertEquals(searchContract.getSisaDeposit(), "Sisa Deposit");
    }

    @Then("user will see detail data contract")
    public void user_will_see_detail_data_contract() throws InterruptedException {
        Assert.assertEquals(searchContract.getDataContract(), "Data Contract");
    }

    @Then("user will see detail pop up edit deposit Apik {string}")
    public void user_will_see_detail_popup_apik(String editDepositConfirmToFinance) throws InterruptedException {
        Assert.assertEquals(searchContract.getEditDepositApik(), editDepositConfirmToFinance);
    }

    @Then("user see new popup termination appeared")
    public void user_will_see_popup_termination() throws InterruptedException {
        Assert.assertEquals(searchContract.getAkhiriKontrakSewa(), "Akhiri Kontrak Sewa");
    }

    @Then("user see akhiri kontrak button disabled")
    public void user_see_akhiri_kontrak_button_disable() throws InterruptedException {
        Assert.assertEquals(searchContract.getAkhiriKontrakButtonDisable(), "Akhiri Kontrak Sewa");
    }

    @Then("user redirect to search contract menu detail")
    public void user_redirect_to_search_contract_menu_detail() throws InterruptedException {
        Assert.assertEquals(searchContract.redirectToSearchContractMenuDetail(), "Search Contract");
    }

    @Then("user will see Konfirmasi Sisa Deposit button hidden")
    public void user_will_see_konfirmasi_sisa_deposit_button_hidden() throws InterruptedException {
        Assert.assertEquals(searchContract.getKonfirmasiSisaDepositButton(), "Pastikan data rekening dan kerusakan sudah sesuai");
    }

    @Then("admin can sees callout message contains {string}")
    public void admin_can_sees_callout_message_contains(String calloutText) {
        List<String> calloutTextSplit = Arrays.asList(calloutText.split(" "));
        for (int i = 0; i < calloutTextSplit.size(); i++) {
            Assert.assertTrue(searchContract.getCalloutText().contains(calloutTextSplit.get(i)));
        }
    }

    @Then("user choose contract date period")
    public void user_choose_contract_date_period() throws InterruptedException {
        searchContract.clickContractDatePeriod();
    }

    @And("user select date period {string}")
    public void user_select_today(String date) throws InterruptedException {
        searchContract.chooseToday(date);
    }

    @And("user search by {string} and input field {string}")
    public void user_searchby(String sortBy, String input) throws InterruptedException {
        searchContract.selectFilterSearchBy(sortBy);
        searchContract.enterTextToSearchTextbox(input);
    }

    @And("user input field search {string}")
    public void user_input_field_search(String field) throws InterruptedException {
        searchContract.enterTextToSearchTextbox(field);
        searchContract.clickOnSearchContractButton();
    }

    @Then("user will get data detail")
    public void user_will_get_data_detail() throws InterruptedException {
        Assert.assertEquals(searchContract.getSearchContractPageHeader(), "Search Contract");
        searchContract.checkDataDetail();
    }

    @And("user verify that detail kos is {string}")
    public void user_detail_kos(String detail) throws InterruptedException {
        Assert.assertEquals(searchContract.getTextDetail(), detail);
    }

    @And("user see popup succsessful batalkan and status contract change to cancelled")
    public void user_verify_batalkan_contract() throws InterruptedException {
        Assert.assertEquals(searchContract.getCalloutText(), "Kontrak berhasil dibatalkan.");
        Assert.assertEquals(searchContract.getTextStatusContract(), "Cancelled");
    }

    @Then("user will see message {string}")
    public void user_see_message(String messageDraft) throws InterruptedException {
        Assert.assertEquals(searchContract.getMessageDraft(), messageDraft);
    }
}
