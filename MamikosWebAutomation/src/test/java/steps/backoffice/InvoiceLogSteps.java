package steps.backoffice;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.InvoiceLogPO;
import utilities.ThreadManager;

import java.util.List;

public class InvoiceLogSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private InvoiceLogPO log = new InvoiceLogPO(driver);

    @Then("admin verify invoice log has {string} as {string}")
    public void admin_verify_invoice_log_has_as(String biliingReminder, String reminderType){
        Assert.assertEquals(log.getBiliingReminderTypeTableValue(biliingReminder), biliingReminder, "Billing reminder is not equal to " + biliingReminder);
        Assert.assertEquals(log.getReminderTypeTableText(reminderType), reminderType, "Billing reminder type is not equal to " + reminderType);
    }

    @Then("admin verify PN reminder status information")
    public void admin_verify_PN_reminder_status_information(DataTable dataTable) {
        List<List<String>> list = dataTable.asLists(String.class);
        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals(log.tableHeadData.get(i).getText(), list.get(0).get(i), "Table head text is not equal to " + list.get(0).get(i));
            for (int j = 0; j < list.size(); j++) {
                Assert.assertEquals(log.pushNotifTableData.get(j).getText(), list.get(1).get(j), "Push notif table data is not equal to " + list.get(1).get(j));
            }
        }
    }

    @Then("admin verify WhatsApp reminder status information")
    public void admin_verify_WhatsApp_reminder_status_information(DataTable dataTable) {
        List<List<String>> list = dataTable.asLists(String.class);
        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals(log.tableHeadData.get(i).getText(), list.get(0).get(i), "Table head text is not equal to " + list.get(0).get(i));
            for (int j = 0; j < list.size(); j++) {
                Assert.assertEquals(log.whatsAppTableData.get(j).getText(), list.get(1).get(j), "WhatsApp table data is not equal to " + list.get(1).get(j));
            }
        }
    }
}
