package steps.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.backoffice.LoginPO;
import utilities.Constants;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class AdminLoginSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private LoginPO login = new LoginPO(driver);

    @Given("user login  as a Admin via credentials")
    public void user_login_as_a_Admin_via_credentials_and() throws InterruptedException {
        login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_LOGIN_EMAIL, Constants.BACKOFFICE_LOGIN_PASSWORD);
    }

    @Given("user login as a Admin backoffice")
    public void user_login_as_a_Admin_backoffice() throws InterruptedException {
        login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_LOGIN_EMAIL_BACKOFFICE, Constants.BACKOFFICE_LOGIN_PASSWORD_BACKOFFICE);
    }

    @And("user login  as a Admin bangkrupux via credentials")
    public void user_login_as_a_Admin_bangkrupux_via_credentials_and() throws InterruptedException {
        login.enterCredentialsAndClickOnLoginButtonBangkrupux(Constants.BACKOFFICE_LOGIN_EMAIL, Constants.BACKOFFICE_LOGIN_PASSWORD);
    }

    @When("user login as a consultant via credentials")
    public void userLoginAsAConsultantViaCredentials() throws InterruptedException {
        login.enterCredentialsAndClickOnLoginButton(Constants.CONSULTANT_EMAIL, Constants.CONSULTANT_PASSWORD);
    }

    @When("user login Backoffice as a {string} via credentials")
    public void user_login_Backoffice_as_a_via_credentials(String account) throws InterruptedException {
        if (account.equalsIgnoreCase("PMAN01")){
            login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_PMAN01_EMAIL, Constants.BACKOFFICE_PMAN01_PASSWORD);
        } else if (account.equalsIgnoreCase("PMAN02")){
            login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_PMAN02_EMAIL, Constants.BACKOFFICE_PMAN02_PASSWORD);
        } else if (account.equalsIgnoreCase("PMAN03")){
            login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_PMAN03_EMAIL, Constants.BACKOFFICE_PMAN03_PASSWORD);
        } else if (account.equalsIgnoreCase("BBM01")){
            login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_BBM01_EMAIL, Constants.BACKOFFICE_BBM01_PASSWORD);
        } else if (account.equalsIgnoreCase("BBM02")){
            login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_BBM02_EMAIL, Constants.BACKOFFICE_BBM02_PASSWORD);
        } else if (account.equalsIgnoreCase("BBM03")){
            login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_BBM03_EMAIL, Constants.BACKOFFICE_BBM03_PASSWORD);
        } else {
            System.out.println("The user is not valid, please login with the valid account");
        }
    }
}
