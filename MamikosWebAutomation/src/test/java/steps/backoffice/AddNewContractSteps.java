package steps.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.AddNewContractPO;
import pageobjects.backoffice.SearchContractPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class AddNewContractSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AddNewContractPO contract = new AddNewContractPO(driver);
    private SearchContractPO searchContract = new SearchContractPO(driver);

    //test data
    private String tenantProperty="src/test/resources/testdata/consultant/tenant.properties";
    private String  tenantHP = JavaHelpers.getPropertyValue(tenantProperty,"tenantHp_yudha_" + Constants.ENV);

    @And("user click add contract button")
    public void user_click_add_contract_button() throws InterruptedException {
        contract.clickAddContractButton();
    }

    @And("user choose property")
    public void user_choose_property() throws InterruptedException {
        contract.setTextOnNoHpOwner();
        contract.clickOnCekOwnerButton();
        contract.selectOwnerKos();
        contract.clickOnLanjutkanToTenantPhoneNumberButton();
    }

    @When("user choose property by phone number {string}")
    public void user_choose_property_by_phone_number(String phoneNumber) throws InterruptedException {
        contract.setTextOnNoHpOwner(phoneNumber);
        contract.clickOnCekOwnerButton();
        contract.selectOwnerKos();
        contract.clickOnLanjutkanToTenantPhoneNumberButton();
    }

    @And("user input tenant phone number")
    public void user_input_tenant_phone_number() throws InterruptedException {
        contract.setTextOnNoHpTenant(tenantHP);
        contract.clickOnCekTenantButton();
        contract.clickOnLanjutkanToDataDiriButton();
    }

    @When("user input tenant phone number {string}")
    public void user_input_tenant_phone_number(String phoneNumber) throws InterruptedException {
        contract.setTextOnNoHpTenant(phoneNumber);
        contract.clickOnCekTenantButton();
        contract.clickOnLanjutkanToDataDiriButton();
    }


    @And("user fill detail sewa")
    public void user_fill_detail_sewa() throws InterruptedException, ParseException {
        contract.chooseRoomNumber();
        contract.fillDateBill();
        contract.chooseRentCount();
        contract.chooseRentDuration();
        contract.uploadIdentityPhoto();
        contract.uploadRenterPhotoKtp();
        contract.selectStatus();
        contract.selectPekerjaan();
        contract.clickOnLanjutkanToTambahanBiayaButton();
    }

    @And("user skip tambahan biaya")
    public void user_skip_tambahan_biaya() throws InterruptedException {
        contract.clickOnLanjutkanToKonfirmasiButton();
    }

    @And("user confirm contract")
    public void user_confirm_contract() throws InterruptedException {
        contract.clickOnSelesaiButton();
    }

    @And("validate there is new active contract")
    public void validate_there_is_new_active_contract() {
        Assert.assertEquals(contract.getTenantActiveLabel(),"Active","no active contract");
        Assert.assertTrue(searchContract.terminateContractButtonIsExist());
    }

    @When("admin master terminate contract by today date")
    public void admin_master_terminate_contract_by_today_date() throws InterruptedException {
        searchContract.terminateContractByTodayDate();
    }
}
