package steps.backoffice.cpDisbursement;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.cpDisbursement.DaftarTransferPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class DaftarTransferSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private DaftarTransferPO listCP = new DaftarTransferPO(driver);

    //Test Data
    private String CPDisbursment = "src/test/resources/testdata/mamipay/cpDisbursement.properties";
    private String emptyDaftarMessage1 = JavaHelpers.getPropertyValue(CPDisbursment,"emptyDaftarMessage1_" + Constants.ENV);
    private String emptyDaftarMessage2 = JavaHelpers.getPropertyValue(CPDisbursment,"emptyDaftarMessage2_" + Constants.ENV);
    private String emptyProcessMessage1 = JavaHelpers.getPropertyValue(CPDisbursment,"emptyProcessMessage1_" + Constants.ENV);
    private String emptyProcessMessage2 = JavaHelpers.getPropertyValue(CPDisbursment,"emptyProcessMessage2_" + Constants.ENV);
    private String emptyFailedMessage1 = JavaHelpers.getPropertyValue(CPDisbursment,"emptyFailedMessage1_" + Constants.ENV);
    private String emptyFailedMessage2 = JavaHelpers.getPropertyValue(CPDisbursment,"emptyFailedMessage2_" + Constants.ENV);
    private String propertyName = JavaHelpers.getPropertyValue(CPDisbursment,"propertyname_" + Constants.ENV);
    private String noRekening = JavaHelpers.getPropertyValue(CPDisbursment,"noRekening_" + Constants.ENV);
    private String pemilikRekening = JavaHelpers.getPropertyValue(CPDisbursment,"pemilikRekening_" + Constants.ENV);

    @When("user choose search cp disbursement by {string}")
    public void user_choose_search_cp_disbursement_by(String searchBy) throws InterruptedException {
        listCP.chooseSearchDaftarTransferBy(searchBy);
    }

    @When("user write search cp disbursement keyword {string}")
    public void user_write_search_cp_disbursement_keyword(String keyword) throws InterruptedException {
        listCP.searchDaftarTransfer(keyword);
    }

    @Then("should be show {string} empty page")
    public void should_be_show_empty_page(String tab) throws InterruptedException {
        Assert.assertTrue(listCP.isEmptyPageAppear());
        switch (tab){
            case "Daftar Transfer":
                Assert.assertEquals(listCP.getEmptyPageMessage1(),emptyDaftarMessage1);
                Assert.assertEquals(listCP.getEmptyPageMessage2(),emptyDaftarMessage2);
                break;
            case "Transfer Diproses" :
                Assert.assertEquals(listCP.getEmptyPageMessage1(),emptyProcessMessage1);
                Assert.assertEquals(listCP.getEmptyPageMessage2(),emptyProcessMessage2);
                break;
            case "Transfer Gagal" :
                Assert.assertEquals(listCP.getEmptyPageMessage1(),emptyFailedMessage1);
                Assert.assertEquals(listCP.getEmptyPageMessage2(),emptyFailedMessage2);
                break;
            default:
                System.out.println("Tab doesn't exist");
        }
    }

    @Then("should be show all disbursement with nama property {string}")
    public void should_be_show_all_disbursement_with_nama_property(String name) throws InterruptedException {
        Assert.assertFalse(listCP.isEmptyPageAppear());
        int total = listCP.getTotalListCPDisbursement();
        if (name.equalsIgnoreCase("PMAN")){
            for (int x=0;x<total;x++){
                Assert.assertEquals(listCP.getPropertyName(x), propertyName);
            }
        } else {
            for (int x=0;x<total;x++){
                Assert.assertEquals(listCP.getPropertyName(x),name);
            }
        }

    }

    @Then("should be show all disbursement with nama pemilik rekening {string}")
    public void should_be_show_all_disbursement_with_nama_pemilik_rekening(String accountName) throws InterruptedException {
        int total = listCP.getTotalListCPDisbursement();
        if (accountName.equalsIgnoreCase("PMAN")){
            for (int x=0;x<=(total-1);x++){
                Assert.assertEquals(listCP.getAccountName(x),pemilikRekening);
            }
        } else {
            for (int x=0;x<=(total-1);x++){
                Assert.assertEquals(listCP.getAccountName(x),accountName);
            }
        }

    }

    @Then("should be show all disbursement with nomor rekening {string}")
    public void should_be_show_all_disbursement_with_nomor_rekening(String accountNumber) throws InterruptedException {
        int total = listCP.getTotalListCPDisbursement();
        if (accountNumber.equalsIgnoreCase("PMAN")){
            for (int x=0;x<=(total-1);x++){
                Assert.assertEquals(listCP.getAccountNumber(x),noRekening);
            }
        } else {
            for (int x=0;x<=(total-1);x++){
                Assert.assertEquals(listCP.getAccountNumber(x),accountNumber);
            }
        }
    }

    @When("user open {string} tab")
    public void user_open_tab(String tab) throws InterruptedException {
        listCP.clickTabCPDisbursement(tab);
    }
}
