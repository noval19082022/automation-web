package steps.backoffice.cpDisbursement;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.cpDisbursement.TambahDataTransferPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TambahDataTransferSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private TambahDataTransferPO addCP = new TambahDataTransferPO(driver);

    //Test Data
    private String CPDisbursment = "src/test/resources/testdata/mamipay/cpDisbursement.properties";
    private String propertyName = JavaHelpers.getPropertyValue(CPDisbursment,"propertyname_" + Constants.ENV);
    private String productType = JavaHelpers.getPropertyValue(CPDisbursment,"productType_" + Constants.ENV);
    private String bankName = JavaHelpers.getPropertyValue(CPDisbursment,"bankName_" + Constants.ENV);
    private String noRekening = JavaHelpers.getPropertyValue(CPDisbursment,"noRekening_" + Constants.ENV);
    private String pemilikRekening = JavaHelpers.getPropertyValue(CPDisbursment,"pemilikRekening_" + Constants.ENV);
    private String noTelpPemilik = JavaHelpers.getPropertyValue(CPDisbursment,"noTelpPemilik_" + Constants.ENV);
    private String totalPendapatan = JavaHelpers.getPropertyValue(CPDisbursment,"totalPendapatan_" + Constants.ENV);
    private String tipeTransaksi = JavaHelpers.getPropertyValue(CPDisbursment,"tipeTransaksi_" + Constants.ENV);
    private String char50plus = JavaHelpers.getPropertyValue(CPDisbursment,"50plusChar_" + Constants.ENV);
    private String char50 = JavaHelpers.getPropertyValue(CPDisbursment,"50char_" + Constants.ENV);

    @When("user tambah data transfer")
    public void user_tambah_data_transfer() throws InterruptedException {
        addCP.clickTambahDataTransfer();
    }

    @Then("should display nama property error message {string}")
    public void should_display_nama_property_error_message(String errorMessage) {
        Assert.assertEquals(addCP.getPropertyNameErrorMessage(),errorMessage);
    }

    @When("user set nama property CP {string}")
    public void user_set_nama_property_CP(String keyword) throws InterruptedException {
        addCP.setNamaProperty(keyword);
    }

    @Then("should showing property suggestion {string}")
    public void should_showing_property_suggestion(String propertyName) throws InterruptedException {
        Assert.assertEquals(addCP.getPropertySuggestion(),propertyName);
    }

    @When("user choose suggestion")
    public void user_choose_suggestion() throws InterruptedException {
        addCP.clickFirstPropertySuggestion();
    }

    @Then("should auto fill Kost {string} data")
    public void should_auto_fill_Kost_data(String property) throws InterruptedException {
        if (property.equalsIgnoreCase("Singgahsini Harapan Bunda Halmehara Utara")){
            Assert.assertEquals(addCP.getProductType(),productType);
            Assert.assertEquals(addCP.getBankName(),bankName);
            Assert.assertEquals(addCP.getNoRekening(),noRekening);
            Assert.assertEquals(addCP.getPemilikRekening(),pemilikRekening);
            Assert.assertEquals(addCP.getNoTelponPemilik(),noTelpPemilik);
        }
    }

    @When("user fill total pendapatan {string}")
    public void user_fill_total_pendapatan(String amount) {
        if (amount.equalsIgnoreCase("PMAN")){
            addCP.setTotalPendapatan(totalPendapatan);
        } else {
            addCP.setTotalPendapatan(amount);
        }
    }

    @When("user select tipe transaksi {string}")
    public void user_select_tipe_transaksi(String revModel) {
        if (revModel.equalsIgnoreCase("PMAN")){
            addCP.setTipeTransaksi(tipeTransaksi);
        }else if (revModel.equalsIgnoreCase("50+ char")){
            addCP.setTipeTransaksi("Lainnya");
            addCP.setLainnyaTipeTransaksi(char50plus);
        }else{
            addCP.setTipeTransaksi(revModel);
        }
    }

    @When("user select tanggal transfer ke pemilik {string}")
    public void user_select_tanggal_transfer_ke_pemilik(String date) throws InterruptedException {
        if (date.equalsIgnoreCase("today")){
            //get today date
            SimpleDateFormat today = new SimpleDateFormat("d");
            Date dates = new Date();
            addCP.setTanggalTransfer(today.format(dates));
        } else {
            addCP.setTanggalTransfer(date);
        }
    }

    @When("user close tambah data transfer pop up")
    public void user_close_tambah_data_transfer_pop_up() throws InterruptedException {
        addCP.closeTambahDataTransfer();
    }

    @Then("data should not delete")
    public void data_should_not_delete() throws InterruptedException {
        //get today
        SimpleDateFormat today = new SimpleDateFormat("yyyy-MM-dd");
        Date dates = new Date();

        Assert.assertEquals(addCP.getPropertyName(),propertyName);
        Assert.assertEquals(addCP.getProductType(),productType);
        Assert.assertEquals(addCP.getBankName(),bankName);
        Assert.assertEquals(addCP.getNoRekening(),noRekening);
        Assert.assertEquals(addCP.getPemilikRekening(),pemilikRekening);
        Assert.assertEquals(addCP.getNoTelponPemilik(),noTelpPemilik);
        Assert.assertEquals(addCP.getTotalPendapatan(),totalPendapatan);
        Assert.assertEquals(addCP.getTipeTransaksi(),tipeTransaksi);
        Assert.assertEquals(addCP.getTanggalTransfer(),today.format(dates));
    }

    @Then("user can only input tipe transaksi lainnya with 50 characters")
    public void user_can_only_input_tipe_transaksi_lainnya_with_50_characters() throws InterruptedException {
        Assert.assertEquals(addCP.getTipeTransaksiLainnya(),char50);
    }

    @And("user click Tambahkan button")
    public void user_click_tambahkan_button() throws InterruptedException {
        addCP.clickTambahkan();
    }

    @Then("the data successfully added on Transfer Pendapatan table")
    public void the_data_successfully_added_on_transfer_pendapatan_table(){
        Assert.assertEquals(addCP.getTglTransferCol(), addCP.getTglTransferCol(), "The data does not match");
        Assert.assertEquals(addCP.getNamaPropCol(), addCP.getNamaPropCol(), "The data does not match");
        Assert.assertEquals(addCP.getTipeTransaksiCol(), addCP.getTipeTransaksiCol(), "The data does not match");
        Assert.assertEquals(addCP.getTotalPndptnCol(), addCP.getTotalPndptnCol(), "The data does not match");
        Assert.assertEquals(addCP.getDetailRekCol(), addCP.getDetailRekCol(), "The data does not match");
    }
}
