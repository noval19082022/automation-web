package steps.backoffice;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.DisbursementPO;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.List;

public class DisbursementSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private DisbursementPO disburse = new DisbursementPO(driver);

    @When("admin clicks on Allow Disbursment Tab")
    public void admin_clicks_on_Allow_Disbursment_Tab() throws InterruptedException {
        disburse.clickOnAllowDisburseTab();
    }

    @When("admin filter by {string} with value {string}")
    public void admin_filter_by_with_value(String searchBy, String byValue) throws InterruptedException {
        disburse.searchByDisburse(searchBy, byValue);
    }

    @When("admin clicks on transfer button")
    public void admin_clicks_on_transfer_button() throws InterruptedException {
        disburse.clickOnTransferIndexOne();
    }

    @Then("admin can sees list cost below:")
    public void admin_can_sees_list_cost_below(List<String> priceList) {
        for (String s : priceList) {
            String harga = "(//*[.='" + s + "']/following-sibling::*)[1]";
            Assert.assertNotNull(selenium.waitInCaseElementVisible(driver.findElement(By.xpath(harga)), 20));
        }
    }

    @Then("admin can sees total disburse is not include add on + admin fee")
    public void admin_can_sees_total_disburse_is_not_include_add_on_admin_fee() {
        int kostPrice = disburse.getKostPrice();
        int getDisbursementPrice = disburse.getTotalDisbursementPrice();

        Assert.assertEquals(kostPrice, getDisbursementPrice);

    }

    @When("admin clicks on transfer now button")
    public void admin_clicks_on_transfer_now_button() throws InterruptedException {
        disburse.transferNowButton();
    }

    @When("admin clicks on processed disbursement tab")
    public void admin_clicks_on_processed_disbursement_tab() throws InterruptedException {
        disburse.clickOnProcessedTab();
    }

    @When("admin clicks on detail button on processed section")
    public void admin_clicks_on_detail_button_on_processed_section() throws InterruptedException {
        disburse.clicksOnDetailButtonProcessedTab();
    }

    @Then("admin can sees total disburse is equal to kost price")
    public void admin_can_sees_total_disburse_is_equal_to_kost_price() throws InterruptedException {
        int kostPrice = JavaHelpers.extractNumber(disburse.getProcessedKostPrice());
        System.out.println(kostPrice);
        int totalDisburse = JavaHelpers.extractNumber(disburse.getProcessedTotalDisburse());
        Assert.assertEquals(kostPrice , totalDisburse);
        disburse.clickOnActivePopUpCloseButton();
    }

    @When("admin clicks on paid disbursement tab")
    public void admin_clicks_on_paid_disbursement_tab() throws InterruptedException {
        disburse.clicksOnPaidTab();
    }

    @When("admin clicks on mark already transfer first index")
    public void admin_clicks_on_mark_already_transfer_first_index() throws InterruptedException {
        disburse.clicksOnMarkAlreadyTransferButton();
    }

    @When("admin choose today date and mark as transferred")
    public void admin_choose_today_date_and_mark_as_transferred() throws InterruptedException {
        disburse.markTransferredTodayDate();
    }

    @When("admin clicks on action button first index transferred tab")
    public void admin_clicks_on_action_button_first_index_transferred_tab() throws InterruptedException {
        disburse.clickOnActionButtonFirstIndex();
    }

    @Then("admin can not sees price with name {string}")
    public void admin_can_not_sees_price_with_name(String priceName) {
        Assert.assertFalse(disburse.isTransferredPriceVisible(priceName));
    }

}
