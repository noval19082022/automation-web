package steps.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.EmailTemplatePO;
import pageobjects.backoffice.SMSTemplatePO;
import utilities.ThreadManager;

public class EmailTemplateSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private EmailTemplatePO email = new EmailTemplatePO(driver);
    private SMSTemplatePO sms = new SMSTemplatePO(driver);

    @Then("user delete Email Template with content {string}")
    public void user_delete_Email_Template_with_content(String content) throws InterruptedException {
        email.clickOnDeleteEmailTemplateButton();
        Assert.assertFalse(sms.isTableContentTemplateAppeared(content), "Template table content " + content + " is still appeared");
    }

    @Given("user click add Email Template button")
    public void user_click_add_Email_Template_button() throws InterruptedException {
        email.clickOnAddEmailTemplateButton();
    }

    @When("user select Email day period with {string}")
    public void user_select_Email_day_period_with(String day) throws InterruptedException {
        email.clickOnEmailTemplateDayPeriodSelection(day);
    }

    @And("user fill Email Template subject with {string}")
    public void user_fill_Email_Template_subject_with(String subject) {
        email.fillEmailTemplateSubject(subject);
    }

    @Then("user fill Email Template content with {string}")
    public void user_fill_Email_Template_content_with(String content) {
        email.fillEmailTemplateContent(content);
    }

    @And("user click create Email Template button")
    public void user_click_create_Email_Template_button() throws InterruptedException {
        email.clickOnCreateEmailTemplateButton();
    }

    @And("user verify Email Template subject with {string}")
    public void user_verify_Email_Template_subject_with(String subject) {
        Assert.assertEquals(email.getTableSubjectTemplate(subject), subject, "Template table content is not equal to " + subject);
    }

    @And("user verify Email Template content with {string}")
    public void user_verify_Email_Template_content_with(String content) {
        Assert.assertEquals(sms.getTableContentTemplate(content), content, "Template table content is not equal to " + content);
    }

    @Given("user click edit Email Template button")
    public void user_click_edit_Email_Template_button() throws InterruptedException {
        email.clickOnEditEmailTemplateButton();
    }

    @And("user click save on edit Email Template page")
    public void user_click_save_on_edit_Email_Template_page() throws InterruptedException {
        email.clickOnSaveEmailTemplateButton();
    }

    @And("user set the initial state to display Email template Day -1")
    public void user_set_the_initial_state_to_display_email_template() throws InterruptedException {
        email.setEmailTemplate();
    }
}
