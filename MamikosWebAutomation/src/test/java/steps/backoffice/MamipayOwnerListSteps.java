package steps.backoffice;

import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.backoffice.MamipayOwnerListPO;
import utilities.ThreadManager;

public class MamipayOwnerListSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private MamipayOwnerListPO ownerList = new MamipayOwnerListPO(driver);

    @When("user search owner phone {string} in mamipay owner list")
    public void user_search_owner_phone_in_mamipay_owner_list(String phone) throws InterruptedException {
        ownerList.enterTextOwnerPhoneNumber(phone);
        ownerList.clickSearchOwnerButton();
    }

    @When("user delete the mamipay data from first list")
    public void user_delete_the_mamipay_data_from_first_list() throws InterruptedException {
        if (ownerList.isFirstDeleteOwnerButtonAppear()) {
            ownerList.clickFirstDeleteOwnerButton();
            driver.switchTo().alert().accept();
        }
    }
}
