package steps.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.SMSTemplatePO;
import pageobjects.backoffice.WhatsAppTemplatePO;
import utilities.ThreadManager;

public class WhatsAppTemplateSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private WhatsAppTemplatePO wa = new WhatsAppTemplatePO(driver);
    private SMSTemplatePO sms = new SMSTemplatePO(driver);


    @Then("user delete WhatsApp Template with content {string}")
    public void user_delete_WhatsApp_Template_with_content(String content) throws InterruptedException {
        wa.clickOnDeleteWhatsAppTemplateButton();
        Assert.assertFalse(sms.isTableContentTemplateAppeared(content), "Template table content " + content + " is still appeared");
    }

    @Given("user click add WhatsApp Template button")
    public void user_click_add_WhatsApp_Template_button() throws InterruptedException {
        wa.clickOnAddWhatsAppTemplateButton();
    }

    @When("user select WhatsApp Template selection with {string}")
    public void user_select_WhatsApp_Template_selection_with(String item) throws InterruptedException {
        wa.clickOnWhatsAppTemplateSelection(item);
    }

    @And("user click create WhatsApp Template button")
    public void user_click_create_WhatsApp_Template_button() throws InterruptedException {
        wa.clickOnCreateWhatsAppTemplateButton();
    }

    @Then("user verify WhatsApp Template content with {string}")
    public void user_verify_WhatsApp_Template_content_with(String content) {
        Assert.assertEquals(sms.getTableContentTemplate(content), content, "Template table content is not equal to" + content);
    }

    @Given("user click edit WhatsApp Template button")
    public void user_click_edit_WhatsApp_Template_button() throws InterruptedException {
        wa.clickOnEditWhatsAppTemplateButton();
    }

    @And("user click save on edit WhatsApp Template page")
    public void user_click_save_on_edit_WhatsApp_Template_page() throws InterruptedException {
        wa.clickOnSaveWhatsAppTemplateButton();
    }

    @And("user select WA day period with {string}")
    public void user_select_WA_day_period_with(String period) throws InterruptedException {
        wa.clickOnWATemplateDayPeriodSelection(period);
    }

    @And("user set the initial state to display Whatsapp template Day -5")
    public void user_set_the_initial_state_to_display_whatsapp_template() throws InterruptedException {
        wa.setWhatsappTemplate();
    }

}
