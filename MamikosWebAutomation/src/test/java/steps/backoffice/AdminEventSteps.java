package steps.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.AdminEventPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class AdminEventSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AdminEventPO event = new AdminEventPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user access menu event")
    public void user_access_menu_event() throws InterruptedException {
        event.accessMenuEvent();
    }

    @And("user set banner 1 to order 1")
    public void user_set_banner_1_to_order_1() throws InterruptedException {
        event.setBanner1ToOrder1();
    }

    @And("user set banner 2 to order 1")
    public void user_set_banner_2_to_order_1() throws InterruptedException {
        event.setBanner2ToOrder1();
    }

    @And("user set banner 2 to order 2")
    public void user_set_banner_2_to_order_2() throws InterruptedException {
        event.setBanner2ToOrder2();
    }

    @And("user set banner 3 to order 1")
    public void user_set_banner_3_to_order_1() throws InterruptedException {
        event.setBanner3ToOrder1();
    }

    @And("user set banner 3 to order 3")
    public void user_set_banner_3_to_order_3() throws InterruptedException {
        event.setBanner3ToOrder3();
    }

    @And("user go to event banner section")
    public void user_go_to_event_banner_section() {
        event.goToEventBanner();
    }

    @And("user click on banner 1")
    public void user_click_on_banner() throws InterruptedException {
        event.clickOnBanner();
    }

   @Then("banner order should be {string}")
   public void banner(String message) throws InterruptedException {
        Assert.assertEquals(event.banner(), message, "Error message is not equal to " + message);
    }

    @Then("user see update banner {string}")
    public void user_see_update_banner(String message) throws InterruptedException {
        Assert.assertEquals(event.userSeeUpdateBanner(), message, "Error message is not equal to " + message);
    }

}
