package steps.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.SMSTemplatePO;
import utilities.ThreadManager;

public class SMSTemplateSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SMSTemplatePO sms = new SMSTemplatePO(driver);

    @Given("user click add SMS Template button")
    public void user_click_add_template_button() throws InterruptedException {
        sms.clickOnAddSMSTemplateButton();
    }

    @When("user fill SMS Template content with {string}")
    public void user_fill_SMS_Template_Content(String content) {
        sms.fillSMSTemplateContent(content);
    }

    @And("user click create SMS Template button")
    public void user_click_create_template_button() throws InterruptedException {
        sms.clickOnCreateSMSTemplateButton();
    }

    @Then("user delete SMS Template with content {string}")
    public void user_delete_SMS_Template_with_content(String content) throws InterruptedException {
        sms.clickOnDeleteSMSTemplateButton();
        Assert.assertFalse(sms.isTableContentTemplateAppeared(content), "Template table content " + content + " is still appeared");
    }

    @Then("user verify SMS Template content with {string}")
    public void user_verify_SMS_Template_content_with(String content) {
        Assert.assertEquals(sms.getTableContentTemplate(content), content, "Template table content is not equal to " + content);

    }

    @Given("user click edit SMS Template button")
    public void user_click_edit_template_button() throws InterruptedException {
        sms.clickOnEditSMSTemplateButton();
    }

    @And("user click save on edit SMS Template page")
    public void user_click_save_on_edit_template_page() throws InterruptedException {
        sms.clickOnSaveSMSTemplateButton();
    }

    @And("user select SMS day period with {string}")
    public void user_select_SMS_day_period_with(String period) throws InterruptedException {
        sms.clickOnSMSTemplatePeriodSelection(period);
    }

    @And("user set the initial state to display SMS template Day -5")
    public void user_set_the_initial_state_to_display_sms_template() throws InterruptedException {
        sms.setSmsTemplate();
    }
}
