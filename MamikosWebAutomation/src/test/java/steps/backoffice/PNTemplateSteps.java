package steps.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.PNTemplatePO;
import pageobjects.backoffice.SMSTemplatePO;
import utilities.ThreadManager;

public class PNTemplateSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private PNTemplatePO pn = new PNTemplatePO(driver);
    private SMSTemplatePO sms = new SMSTemplatePO(driver);

    @Then("user delete PN Template with content {string}")
    public void user_delete_PN_Template_with_content(String content) throws InterruptedException {
        pn.clickOnDeletePNTemplateButton();
        Assert.assertFalse(sms.isTableContentTemplateAppeared(content), "Template table content " + content + " is still appeared");
    }

    @Given("user click add PN Template button")
    public void user_click_add_PN_Template_button() throws InterruptedException {
        pn.clickOnAddPNTemplateButton();
    }

    @When("user select PN day period with {string}")
    public void user_select_PN_day_period_with(String day) throws InterruptedException {
        pn.clickOnPNTemplateDayPeriodSelection(day);
    }

    @And("user fill PN Template title with {string}")
    public void user_fill_PN_Template_title_with(String title) {
        pn.fillPNTemplateTitle(title);
    }

    @And("user fill PN Template content with {string}")
    public void user_fill_PN_Template_content_with(String content) {
        pn.fillPNTemplateContent(content);
    }

    @And("user click create PN Template button")
    public void user_click_create_PN_Template_button() throws InterruptedException {
        pn.clickOnCreatePNTemplateButton();
    }

    @Then("user verify PN Template title with {string}")
    public void user_verify_PN_Template_title_with(String title) {
        pn.getTableTitleTemplate(title);
    }

    @And("user verify PN Template content with {string}")
    public void user_verify_PN_Template_content_with(String content) {
        pn.getTableContentTemplate(content);
    }

    @Given("user click edit PN Template button")
    public void user_click_edit_PN_Template_button() throws InterruptedException {
        pn.clickOnEditPNTemplateButton();
    }

    @And("user click save on edit PN Template page")
    public void user_click_save_on_edit_PN_Template_page() throws InterruptedException {
        pn.clickOnSavePNTemplateButton();
    }

    @Then("user verify add template callout error with {string}")
    public void user_verify_add_template_callout_error_with(String err) {
        Assert.assertEquals(pn.getCalloutErrorText(), err, "Callout error is not equal to " + err);
    }

    @And("user set the initial state to display PN template Day -1")
    public void user_set_the_initial_state_to_display_pn_template() throws InterruptedException {
        pn.setPnTemplate();
    }
}
