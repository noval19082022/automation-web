package steps.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.LeftMenuPO;
import utilities.ThreadManager;

public class AdminLeftMenuSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private LeftMenuPO left = new LeftMenuPO(driver);

    @Given("user click on Search Contract Menu form left bar")
    public void user_click_on_Search_Contract_Menu_form_left_bar() throws InterruptedException {
        left.clickOnSearchContractMenu();
    }

    @Then("user will see button {string}")
    public void user_will_see_button(String editDeposit) throws InterruptedException {
        Assert.assertEquals(left.userSeeEditDepositButton(), editDeposit, "Edit Deposit");
    }

    @Given("user click on invoice admin fee discount Menu form left bar")
    public void user_click_on_invoice_admin_fee_discount_Menu_form_left_bar() throws InterruptedException {
        left.clickOnInvoiceAdminFeeDiscountMenu();
    }

    @When("user click Mamipay Owner List")
    public void user_click_Mamipay_Owner_List() throws InterruptedException {
        left.clickMamipayOwnerListMenu();
    }

    @When("admin clicks on Search Invoice Menu form left bar")
    public void admin_clicks_on_Search_Invoice_Menu_form_left_bar() throws InterruptedException {
        left.clickOnSearchInvoiceMenu();
    }

    @Given("user access menu {string} sub menu of voucher discount")
    public void user_access_menu_sub_menu_of_voucher_discount(String subMenu) throws InterruptedException {
        left.clickOnSubMenuOfVoucherDiscount(subMenu);
    }

    @When("user click on Billing Reminder Template from left menu")
    public void user_click_on_Billing_Reminder_Template_from_left_menu() throws InterruptedException {
        left.clickOnBillingReminderTemplateMenu();
    }

    @And("user click {string} submenu of Billing Reminder Template")
    public void user_click_submenu_of_Billing_Reminder_Template(String menu) throws InterruptedException {
        left.clickOnSubMenuOfBillingReminderTemplateMenu(menu);
    }

    @When("admin go to Paid Invoice list Disbursment Page")
    public void admin_go_to_Paid_Invoice_list_Disbursment_Page() throws InterruptedException {
       left.goToDisbursementPage();
    }

    @When("admin go to Paid Invoice list refund Page")
    public void admin_go_to_Paid_Invoice_list_refund_Page() throws InterruptedException {
        left.goToPaidInvoiceList();
    }

    @Then("admin go to refund Page {string}")
    public void admin_go_to_refund_Page(String refund) throws InterruptedException {
        Assert.assertEquals(left.goToRefundPage(), refund, "Daftar Invoice Refund");
    }

    @When("admin clicks on Transfer Deposit Tenant Menu form left bar")
    public void admin_clicks_on_Transfer_Deposit_Tenant_Menu_form_left_bar() throws InterruptedException {
        left.clickOnTransferDepositTenantMenu();
    }

    @When("user access menu CP Disbursement")
    public void user_access_menu_CP_Disbursement() throws InterruptedException {
        left.goToCPDisbursementPage();
    }

    @When("user access mamipay menu {string}")
    public void user_access_mamipay_menu(String menu) throws InterruptedException {
        left.clickMamipayMenu(menu);
    }
}
