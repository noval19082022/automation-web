package steps.singgahSini;

import io.cucumber.java.en.And;
import org.openqa.selenium.WebDriver;
import pageobjects.singgahSini.SinggahSiniCommonPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class SinggahSiniCommonSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SinggahSiniCommonPO commonSSPO = new SinggahSiniCommonPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @And("user close pop up notification mamikos")
    public void user_close_pop_up_notification_mamikos()throws InterruptedException{
        commonSSPO.closeMamikosNotifPopUp();
    }

}
