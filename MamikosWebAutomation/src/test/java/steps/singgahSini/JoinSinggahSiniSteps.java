package steps.singgahSini;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.singgahSini.JoinSinggahSiniPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;
import java.util.List;


public class JoinSinggahSiniSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JoinSinggahSiniPO joinSSPO = new JoinSinggahSiniPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @And("user click button gabung sekarang")
    public void user_click_button_gabung_sekarang()throws InterruptedException {
        joinSSPO.clickOnJoinNowButton();
    }

    @And("user input name with value {string}")
    public void user_input_name_with_value(String name){
        joinSSPO.insertName(name);
    }

    @And("user input phone number with value {string}")
    public void user_input_phone_number_with_value(String phone){
        joinSSPO.insertPhoneNumber(phone);
    }

    @And("user input city with value {string}")
    public void user_input_city_with_value(String city)throws InterruptedException{
        joinSSPO.insertCity(city);
    }

    @Then("user input city with wrong value {string}")
    public void user_input_city_with_wrong_value(String city) throws InterruptedException {
        joinSSPO.enterWrongCity(city);
    }

    @Then("user select subdistrict {string}")
    public void user_select_subdistrict(String subdistrict) throws InterruptedException {
        joinSSPO.selectSubdistrict(subdistrict);
    }

    @Then("user select village {string}")
    public void user_select_village(String village) throws InterruptedException {
        joinSSPO.selectVillage(village);
    }

    @And("user input address with value {string}")
    public void user_input_address_with_value(String address){
        joinSSPO.insertAddressNotes(address);
    }

    @And("user input kost name with value {string}")
    public void user_input_kost_name_with_value(String kostName){
        joinSSPO.insertKostName(kostName);
    }

    @And("user click button register")
    public void user_click_button_register()throws InterruptedException{
        joinSSPO.clickOnRegisterButton();
    }

    @Then("user validate page title equals to {string}")
    public void user_validate_page_title_equals_to(String title) throws InterruptedException {
        Assert.assertEquals(joinSSPO.getPageTitle(), title, "Halaman tidak sesuai dengan " + title);
    }

    @Then("system display description {string}")
    public void system_display_description(String description) throws InterruptedException {
        Assert.assertEquals(joinSSPO.getDescription(), description, "Description not match");
    }

    @Then("user validate home page title equals to {string}")
    public void user_validate_home_page_title_equals_to(String page){
        Assert.assertEquals(joinSSPO.getHomePageTitle(), page, "Halaman tidak sesuai dengan " + page);
    }

    @Then("user validate error message on empty register owner form")
    public void user_validate_error_message_on_empty_register_owner_form(List<String> message){
        Assert.assertEquals(joinSSPO.getFullNameErrorMessage(), message.get(0) , "Error Message tidak sesuai dengan " +message.get(0));
        Assert.assertEquals(joinSSPO.getPhoneErrorMessage(), message.get(1), "Error Message tidak sesuai dengan " +message.get(1));
        Assert.assertEquals(joinSSPO.getKostNameErrorMessage(), message.get(2), "Error Message tidak sesuai dengan " +message.get(2));
        Assert.assertEquals(joinSSPO.getCityErrorMessage(), message.get(3), "Error Message tidak sesuai dengan " +message.get(3));
        Assert.assertEquals(joinSSPO.getAddressErrorMessage(), message.get(4), "Error Message tidak sesuai dengan " +message.get(4));
    }

    @Then("user validate modal title equals to {string}")
    public void user_validate_modal_title_equals_to(String title){
        Assert.assertEquals(joinSSPO.getModalTitle(), title, "Halaman tidak sesuai dengan " + title);
    }

    @Then("user validate error message on full name form is equal to {string}")
    public void user_validate_error_message_on_full_name_form(String errorMessage){
        Assert.assertEquals(joinSSPO.getFullNameErrorMessage(), errorMessage, "Error Message tidak sesuai dengan " +errorMessage);
    }

    @Then("user validate error message on phone number form is equal to {string}")
    public void user_validate_error_message_on_phone_number_form (String errorMessage){
        Assert.assertEquals(joinSSPO.getPhoneErrorMessage(), errorMessage, "Error Message tidak sesuai dengan " +errorMessage);
    }

    @Then("user validate error message on kost name form is equal to {string}")
    public void user_validate_error_message_on_kost_name_form (String errorMessage){
        Assert.assertEquals(joinSSPO.getKostNameErrorMessage(), errorMessage, "Error Message tidak sesuai dengan " +errorMessage);
    }

    @Then("user validate error message on address form is equal to {string}")
    public void user_validate_error_message_on_address_form (String errorMessage){
        Assert.assertEquals(joinSSPO.getAddressErrorMessage(), errorMessage, "Error Message tidak sesuai dengan " +errorMessage);
    }

    @And("user click on back button")
    public void click_on_back_button() throws InterruptedException{
            joinSSPO.clickOnBackButton();
    }

    @And("user click on exit button")
    public void click_on_exit_button() throws InterruptedException{
        joinSSPO.clickOnExitButton();
    }

    @And("user click on next button")
    public void click_on_next_button() throws InterruptedException{
        joinSSPO.clickOnNextButton();
    }

    @And("user click on oke button")
    public void click_on_oke_button() throws InterruptedException{
        joinSSPO.clckOnOkeButton();
    }

    @Then("user validate error message city search form is equal to {string}")
    public void user_validate_error_message_on_city_search_form (String errorMessage){
        Assert.assertEquals(joinSSPO.getCitySearchErrorMessage(), errorMessage, "Error Message tidak sesuai dengan " +errorMessage);
    }

}
