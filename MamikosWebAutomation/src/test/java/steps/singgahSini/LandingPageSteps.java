package steps.singgahSini;

import dataobjects.Common;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

import org.testng.Assert;
import pageobjects.singgahSini.LandingPagePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class LandingPageSteps {
    private WebDriver driver = ThreadManager.getDriver();
    SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private LandingPagePO landing = new LandingPagePO(driver);
    private Common data;

    //Test Data Landing Page Singgahsini
    private String landingPage = "src/test/resources/testdata/singgahsini/landingPage.properties";

    // Manfaat
    private String advantagesTitle = JavaHelpers.getPropertyValue(landingPage, "advantagesTitle_" + Constants.ENV);
    private String advantageImg1 = JavaHelpers.getPropertyValue(landingPage,"advantageImg1_"+ Constants.ENV);
    private String advantageImg2 = JavaHelpers.getPropertyValue(landingPage,"advantageImg2_"+ Constants.ENV);
    private String advantageImg3 = JavaHelpers.getPropertyValue(landingPage,"advantageImg3_"+ Constants.ENV);

    private String advantageSection1a = JavaHelpers.getPropertyValue(landingPage,"advantageSection1a_"+ Constants.ENV);
    private String advantageSection1b = JavaHelpers.getPropertyValue(landingPage,"advantageSection1b_"+ Constants.ENV);
    private String advantageSection2a = JavaHelpers.getPropertyValue(landingPage,"advantageSection2a_"+ Constants.ENV);
    private String advantageSection2b = JavaHelpers.getPropertyValue(landingPage,"advantageSection2b_"+ Constants.ENV);
    private String advantageSection3a = JavaHelpers.getPropertyValue(landingPage,"advantageSection3a_"+ Constants.ENV);
    private String advantageSection3b = JavaHelpers.getPropertyValue(landingPage,"advantageSection3b_"+ Constants.ENV);

    private String advantageDetails1 = JavaHelpers.getPropertyValue(landingPage,"advantageDetails1_"+ Constants.ENV);
    private String advantageDetails2 = JavaHelpers.getPropertyValue(landingPage,"advantageDetails2_"+ Constants.ENV);
    private String advantageDetails3 = JavaHelpers.getPropertyValue(landingPage,"advantageDetails3_"+ Constants.ENV);

    // Join
    private String joinImg = JavaHelpers.getPropertyValue(landingPage,"joinImg_"+ Constants.ENV);
    private String joinTitle = JavaHelpers.getPropertyValue(landingPage,"joinTitle_"+ Constants.ENV);
    private String joinDescription = JavaHelpers.getPropertyValue(landingPage,"joinDescription_"+ Constants.ENV);

    private String firstStep = JavaHelpers.getPropertyValue(landingPage, "firstStep_" + Constants.ENV);
    private String secondStep = JavaHelpers.getPropertyValue(landingPage, "secondStep_" + Constants.ENV);
    private String thirdStep = JavaHelpers.getPropertyValue(landingPage, "thirdStep_" + Constants.ENV);
    private String fourthStep = JavaHelpers.getPropertyValue(landingPage, "fourthStep_" + Constants.ENV);
    private String fifthStep = JavaHelpers.getPropertyValue(landingPage, "fifthStep_" + Constants.ENV);

    private String firstQuestion = JavaHelpers.getPropertyValue(landingPage, "firstQuestion_" + Constants.ENV);
    private String firstAnswer = JavaHelpers.getPropertyValue(landingPage, "firstAnswer_" + Constants.ENV);

    private String secondQuestion = JavaHelpers.getPropertyValue(landingPage, "secondQuestion_" + Constants.ENV);
    private String secondAnswerP1 = JavaHelpers.getPropertyValue(landingPage, "secondAnswerP1_" + Constants.ENV);
    private String secondAnswerP2 = JavaHelpers.getPropertyValue(landingPage, "secondAnswerP2_" + Constants.ENV);
    private String secondAnswerP3 = JavaHelpers.getPropertyValue(landingPage, "secondAnswerP3_" + Constants.ENV);

    private String thirdQuestion = JavaHelpers.getPropertyValue(landingPage, "thirdQuestion_" + Constants.ENV);
    private String thirdAnswer = JavaHelpers.getPropertyValue(landingPage, "thirdAnswer_" + Constants.ENV);

    private String fourthQuestion = JavaHelpers.getPropertyValue(landingPage, "fourthQuestion_" + Constants.ENV);
    private String fourthAnswerP1 = JavaHelpers.getPropertyValue(landingPage, "fourthAnswerP1_" + Constants.ENV);
    private String fourthAnswerP2 = JavaHelpers.getPropertyValue(landingPage, "fourthAnswerP2_" + Constants.ENV);
    private String fourthAnswerP3 = JavaHelpers.getPropertyValue(landingPage, "fourthAnswerP3_" + Constants.ENV);
    private String fourthAnswerP4 = JavaHelpers.getPropertyValue(landingPage, "fourthAnswerP4_" + Constants.ENV);

    private String fifthQuestion = JavaHelpers.getPropertyValue(landingPage, "fifthQuestion_" + Constants.ENV);
    private String fifthAnswer = JavaHelpers.getPropertyValue(landingPage, "fifthAnswer_" + Constants.ENV);

    public LandingPageSteps(Common data) {
        this.data = data;
    }

    @When("user click on hyperlink {string}")
    public void user_click_on_hyperlink(String hyperlink) throws InterruptedException {
        data.pageYOffset = selenium.getPageYOffset();
        landing.clickOnHyperlink(hyperlink);
    }

    @When("user scroll to {string} section")
    public void user_scroll_to_section(String section) {
        switch (section){
            case "Owner Should Join Singgahsini":
                landing.scrollToOwnerShouldJoinSinggahsiniSection();
                break;
            case "Testimonial":
                landing.scrollToOwnerTestimonialSection();
                break;
            case "FAQ":
                landing.scrollToFaqSection();
                break;
            case "Cara Bergabung":
                landing.scrollToHowToJoinSection();
                break;
        }
    }

    @Then("system display {string} section")
    public void system_display_section(String section) {
        switch(section){
            case "Tentang Kami":
                Assert.assertTrue(landing.AboutUsSectionIsPresent(), "Section " + section + "is present");
                break;
            case "Owner Should Join Singgahsini":
                Assert.assertTrue(landing.introductionSectionIsPresent(), "Section " + section + "is present");
                break;
            case "Cara Bergabung":
                Assert.assertTrue(landing.howToJoinSectionIsPresent(), "Section " + section + "is present");
                break;
            case "Galeri":
                Assert.assertTrue(landing.gallerySectionIsPresent(), "Section " + section + "is present");
                break;
        }
    }

    @Then("system display content {string} section")
    public void system_display_content_section(String section) throws InterruptedException {
        switch(section){
            case "Manfaat":
                //Assert Manfaat Title
                Assert.assertEquals(landing.getAdvantagesTitle(), advantagesTitle, "Title not match");

                //Assert Manfaat Images
                String[] advantageImgs = {advantageImg1, advantageImg2, advantageImg3};
                for (int i = 0; i < advantageImgs.length; i++) {
                    Assert.assertEquals(landing.getImgSrc(i), advantageImgs[i],"Image src does not match");
                }

                //Assert Manfaat Section Headings
                String[] advantageSections = {advantageSection1a, advantageSection1b, advantageSection2a, advantageSection2b, advantageSection3a, advantageSection3b};
                for (int i = 0; i< advantageSections.length; i++){
                    Assert.assertEquals(landing.getAdvantages(i), advantageSections[i], "Advantages Section Headings ke-"+(i+1)+" not match");
                }

                //Assert Manfaat Details
                String[] advantageDetails = {advantageDetails1, advantageDetails2, advantageDetails3};
                for (int i = 0; i< advantageDetails.length; i++){
                    Assert.assertEquals(landing.getSubAdvantages(i), advantageDetails[i], "Advantages Section Details ke-"+(i+1)+" not match");
                }

                break;
            case "Owner Should Join the Singgahsini":
                Assert.assertTrue(landing.introductionSectionContentIsPresent(), "Section " + section + "is not present");
                break;
            case "Cara Bergabung":
                // Assert Image
                Assert.assertEquals(landing.getJoinImgSrc(), joinImg, "Join IMG src does not match");

                // Assert Title and Text
                Assert.assertEquals(landing.howToJoinSectionContentIsPresent("Title"), joinTitle, "Title " + section + " is not the same");
                Assert.assertEquals(landing.howToJoinSectionContentIsPresent("Description"), joinDescription, "Description " + section + " is not the same");
                break;
            case "Testimonial":
                Integer totalTestimonial = 3;
                for (int i = 1; i <= totalTestimonial; i++) {
                    Assert.assertTrue(landing.testimonialContentIsPresent(i), "Section " + section + "is not present");
                }
                break;
            case "Galeri":
                Assert.assertTrue(landing.galleryContentIsPresent(), "Section " + section + "is not present");
                break;
            case "FAQ":
                Assert.assertTrue(landing.informationCenterSectionIsPresent(), "Section " + section + "is not present");
                Assert.assertEquals(landing.getFAQTitle(), "Tanya Jawab Seputar Singgahsini dan Apik",  "FAQ title not equals");
                String[] questions = {firstQuestion, secondQuestion ,thirdQuestion, fourthQuestion, fifthQuestion};

                String[] secondAnswer = {secondAnswerP1, secondAnswerP2, secondAnswerP3};
                String[] fourthAnswer = {fourthAnswerP1, fourthAnswerP2, fourthAnswerP3, fourthAnswerP4};

                //Asserting Questions
                for (int i = 0; i < questions.length; i++){
                    Assert.assertEquals(landing.getQuestions(i), questions[i], "Question not equals");
                }

                //Asserting Answer1
                landing.expandFAQ(0);
                Assert.assertEquals(landing.getAnswer(0), firstAnswer, "Answer 1 not equals");

                //Asserting Answer2
                landing.expandFAQ(1);
                for (int i = 0; i < secondAnswer.length; i++){
                    Assert.assertEquals(landing.getSecondAnswer(i+1), secondAnswer[i], "Answer 2 not equals");
                }

                //Asserting Answer3
                landing.expandFAQ(2);
                Assert.assertEquals(landing.getAnswer(2), thirdAnswer, "Answer 3 not equals");

                //Asserting Answer4
                landing.expandFAQ(3);
                for (int i = 0; i < fourthAnswer.length; i++){
                    Assert.assertEquals(landing.getFourthAnswer(i), fourthAnswer[i], "Answer 4 not equals");
                }

                //Asserting Answer5
                landing.expandFAQ(4);
                Assert.assertEquals(landing.getAnswer(4), fifthAnswer, "Answer 5 not equals");
        }
    }

    @Then("system display join now button")
    public void system_display_join_now_button() {
        landing.joinNiwButtonIsPresent();
    }

    @When("user click hyperlink {string} on footer")
    public void user_click_hyperlink_on_footer(String footerMenu) throws InterruptedException {
        landing.scrollToFooter(footerMenu);
        data.pageYOffset = selenium.getPageYOffset();
        landing.clickHyperlinkOnFooter(footerMenu);
    }

    @Then("system auto scroll up to section {string}")
    public void system_auto_scroll_up_to_section(String section) {
        switch(section){
            case "Manfaat":
                Assert.assertTrue(landing.benefitsSectionIsPresent(), "Section " + section + "is present");
                break;
            case "Tentang Kami":
                Assert.assertTrue(landing.AboutUsSectionIsPresent(), "Section " + section + "is present");
                break;
            case "Tanya Jawab":
                Assert.assertTrue(landing.informationCenterSectionIsPresent(), "Section " + section + "is present");
                break;
            case "Cara Bergabung":
                Assert.assertTrue(landing.howToJoinSectionIsPresent(), "Section " + section + "is present");
                break;
            case "Galeri":
                Assert.assertTrue(landing.gallerySectionIsPresent(), "Section " + section + "is present");
                break;
        }
        Assert.assertTrue(data.pageYOffset > selenium.getPageYOffset(), "Auto scroll section not working");
    }

    @When("user click social media {string}")
    public void user_click_social_media(String socialMedia) throws InterruptedException {
        landing.clickHyperlinkOnFooter(socialMedia);
    }

    @Then("system open social media {string} on new tab")
    public void system_open_social_media_on_new_tab(String socialMedia) throws InterruptedException {
        Assert.assertTrue(landing.getSocialMediaUrlOnNewTab().contains(socialMedia.toLowerCase()));
    }

    @When("user click on view more button")
    public void user_click_on_view_more_button() throws InterruptedException {
        landing.clickViewMorButtonOnGallerySection();
    }
}
