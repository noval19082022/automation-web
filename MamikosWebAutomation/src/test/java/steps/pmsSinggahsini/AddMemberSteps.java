package steps.pmsSinggahsini;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.AddMemberPO;
import pageobjects.pmsSinggahsini.CommonPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class AddMemberSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AddMemberPO addMember = new AddMemberPO(driver);
    private CommonPO common = new CommonPO(driver);

    //Test Data PMS
    private String PMSUser = "src/test/resources/testdata/pms-singgahsini/datauser.properties";
    private String memberyudha = JavaHelpers.getPropertyValue(PMSUser,"memberYudha_" + Constants.ENV);
    private String nameyudha = JavaHelpers.getPropertyValue(PMSUser,"memberYudhaName_" + Constants.ENV);
    private String memberinvalid = JavaHelpers.getPropertyValue(PMSUser,"memberInvalid_" + Constants.ENV);

    @When("user add member {string}")
    public void user_add_member(String member) throws InterruptedException {
        switch (member){
            case "yudha":
                addMember.setMemberEmail(memberyudha);
                break;
            case "invalid member":
                addMember.setMemberEmail(memberinvalid);
                break;
        }
        addMember.clickTambahButton();
    }

    @Then("error message {string} should be appear below tambah member field")
    public void error_message_should_be_appear_below_tambah_member_field(String message) {
        Assert.assertEquals(addMember.getMemberNotFoundMessage(),message);
    }

    @Then("member {string} added")
    public void member_added(String member) throws InterruptedException {
        common.waitMultipleJavascriptLoading(7);
        switch (member){
            case "yudha":
                Assert.assertEquals(addMember.getMemberName(),nameyudha);
                Assert.assertEquals(addMember.getMemberEmail(),memberyudha);
                break;
            default:
                System.out.println("member not found");
        }
    }

    @When("user cancel hapus member")
    public void user_cancel_hapus_member() throws InterruptedException {
        addMember.deleteMember();
        addMember.cancelHapusMember();
    }

    @When("user hapus member {string}")
    public void user_hapus_member(String member) throws InterruptedException {
        common.waitMultipleJavascriptLoading(2);
        switch (member){
            case "yudha":
                if (addMember.getMemberName().equalsIgnoreCase(nameyudha)){
                    addMember.deleteMember();
                    addMember.confirmHapusMember();
                }
                break;
        }
    }

    @Then("member {string} removed from the list")
    public void member_removed_from_the_list(String member) throws InterruptedException {
        switch (member){
            case "yudha":
                Assert.assertFalse(addMember.memberNotDisplayed(nameyudha));
                break;
            default:
                System.out.println("member found");
        }
    }
}
