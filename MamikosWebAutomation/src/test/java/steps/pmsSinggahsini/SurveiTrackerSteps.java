package steps.pmsSinggahsini;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.SurveiTrackerPO;
import utilities.ThreadManager;

public class SurveiTrackerSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SurveiTrackerPO surveiTracker = new SurveiTrackerPO(driver);

    @And("user click Survey Tracker menu")
    public void user_click_survey_tracker_menu() throws InterruptedException {
        surveiTracker.clickSurveyTrackerMenu();
    }

    @And("user choose {string} filter in survei tracker")
    public void user_choose_filter_in_survei_tracker(String mainPageFilter) throws InterruptedException {
        surveiTracker.clickMainPageFilter();
        surveiTracker.selectMainPageFilter(mainPageFilter);
    }

    @Then("user verify nama pencari kos from filter in survei tracker is {string}")
    public void user_verify_nama_pencari_kos_from_filter_in_survei_tracker_is(String tenantName) {
        Assert.assertEquals(surveiTracker.getTenantNameOnMainPageFilter(tenantName), tenantName, "Tenant Name does not match" + tenantName);
    }

    @Then("user verify nama properti from filter in survei tracker is {string}")
    public void user_verify_nama_properti_from_filter_in_survei_tracker_is(String propertyName) {
        Assert.assertEquals(surveiTracker.getPropertyNameOnMainPageFilter(propertyName), propertyName, "Property Name does not match" + propertyName);
    }

    @And("user click filter in homepage")
    public void user_click_filter_in_homepage() throws InterruptedException {
        surveiTracker.clickFilterHomePage();
    }

    @And("user search {string} in status field")
    public void user_search_in_status_field(String filterStatus) throws InterruptedException {
        surveiTracker.clickFilterStatus();
        surveiTracker.selectFilterStatus(filterStatus);
    }

    @And("user click terapkan survei")
    public void user_click_terapkan_survei() throws InterruptedException {
        surveiTracker.clickTerapkanButton();
    }

    @And("user click search button on main page filter survei")
    public void user_click_search_button_on_main_page_filter() throws InterruptedException {
        surveiTracker.clickSearchButtonMainPageFilter();
    }

    @Then("user verify search result on main page bse contains Tidak Ada Konfirmasi")
    public void user_verify_search_result_on_main_page_bse_contains_Tidak_Ada_Konfirmasi() {
        Assert.assertEquals(surveiTracker.geTidakAdaKonfirm(), "Tidak Ada Konfirmasi");

    }
}