package steps.pmsSinggahsini;

import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import pageobjects.pmsSinggahsini.RoomAllotmentPO;
import utilities.ThreadManager;

import java.text.ParseException;

public class RoomAllotmentSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private RoomAllotmentPO roomAllotment = new pageobjects.pmsSinggahsini.RoomAllotmentPO(driver);

    @When("user set room {string} as out of order {int} days")
    public void user_set_room_as_out_of_order(String room, int duration) throws InterruptedException, ParseException {
        roomAllotment.setRoomOutOfOrder(room);
        roomAllotment.setOutOfOrderReason("Automation Test PMAN");
        roomAllotment.setOutOfOrderDuration(duration);
        roomAllotment.submitOutOfOrder();
    }

    @Then("room {string} set as out of order")
    public void room_set_as_out_of_order(String room) {
        roomAllotment.isRoomHaveOutOfOrderStatus(room);
    }
}
