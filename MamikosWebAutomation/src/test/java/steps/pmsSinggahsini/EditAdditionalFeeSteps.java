package steps.pmsSinggahsini;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.CommonPO;
import pageobjects.pmsSinggahsini.EditAdditionalFeePO;
import utilities.ThreadManager;

public class EditAdditionalFeeSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private EditAdditionalFeePO fee = new EditAdditionalFeePO(driver);
    private CommonPO common = new CommonPO(driver);

    @When("user click Tambah Biaya button")
    public void user_click_Tambah_Biaya_button() throws InterruptedException {
        fee.clickTambahBiaya();
    }

    @When("user add/edit biaya tambahan {string} {string}")
    public void user_add_biaya_tambahan(String feeName, String feeAmount) throws InterruptedException {
        fee.setNamaBiayaTambahan(feeName);
        fee.setHarga(feeAmount);
        fee.clickTambahPopUp();
    }

    @Then("biaya tambahan {string} with amount {string} should be appear")
    public void biaya_tambahan_with_amount_should_be_appear(String feeName, String feeAmount) throws InterruptedException {
        common.waitMultipleJavascriptLoading(15);
        Assert.assertEquals(fee.getLastFeeNameText(),feeName);
        Assert.assertEquals(fee.getLastFeeAmountText(),feeAmount);
    }

    @When("user click action button and ubah in biaya tambahan {string}")
    public void user_click_action_button_and_ubah_in_biaya_tambahan(String feeName) throws InterruptedException {
        fee.clickActionButton(feeName);
        fee.clickUbahBiayaTambahan(feeName);
    }

    @When("user click action button and hapus in biaya tambahan {string}")
    public void user_click_action_button_and_hapus_in_biaya_tambahan(String feeName) throws InterruptedException {
        fee.clickActionButton(feeName);
        fee.clickHapusBiayaTambahan(feeName);
    }

    @When("user confirm hapus biaya tambahan")
    public void user_confirm_hapus_biaya_tambahan() throws InterruptedException {
        fee.confirmHapusBiayaTambahan();
    }

    @Then("biaya tambahan {string} should not exist in list")
    public void biaya_tambahan_should_not_exist_in_list(String feeName) throws InterruptedException {
        common.waitMultipleJavascriptLoading(10);
        Assert.assertFalse(fee.isFeeNameExist(feeName));
    }
}
