package steps.pmsSinggahsini;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.CommonPO;
import pageobjects.pmsSinggahsini.HomepagePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.List;

public class HomepageSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HomepagePO home = new HomepagePO(driver);
    private CommonPO common = new CommonPO(driver);

    //Test Data PMS
    private String PMS = "src/test/resources/testdata/pms-singgahsini/datauser.properties";
    private String property = JavaHelpers.getPropertyValue(PMS,"propertyTest_" + Constants.ENV);
    private String PMANID = JavaHelpers.getPropertyValue(PMS,"propertyIDPMAN_" + Constants.ENV);
    private String PMANName = JavaHelpers.getPropertyValue(PMS,"propertyNamePMAN_" + Constants.ENV);
    private String urlDetailProperty = JavaHelpers.getPropertyValue(PMS,"urlDetailProperty_" + Constants.ENV);
    private String urlKetersediaanKamar = JavaHelpers.getPropertyValue(PMS,"urlKetersediaanKamar_" + Constants.ENV);

    //Filter
    private String tanggalLiveMulai = JavaHelpers.getPropertyValue(PMS, "tanggalLiveMulai_" + Constants.ENV);
    private String tanggalLiveAkhir = JavaHelpers.getPropertyValue(PMS, "tanggalLiveAkhir_" + Constants.ENV);
    private String tanggalExpiredMulai = JavaHelpers.getPropertyValue(PMS,"tanggalExpiredMulai_" + Constants.ENV);
    private String tanggalExpiredAkhir = JavaHelpers.getPropertyValue(PMS, "tanggalExpiredAkhir_" + Constants.ENV);

    private String detailPropKota = JavaHelpers.getPropertyValue(PMS, "detailPropKota_" + Constants.ENV);
    private String detailPropProduk = JavaHelpers.getPropertyValue(PMS, "detailPropProduk1_" + Constants.ENV);
    private String detailPropBSE = JavaHelpers.getPropertyValue(PMS, "detailPropBSE_" + Constants.ENV);
    private String detailPropBD = JavaHelpers.getPropertyValue(PMS, "detailPropBD_" + Constants.ENV);
    private String detailPropAS = JavaHelpers.getPropertyValue(PMS, "detailPropAS_" + Constants.ENV);
    private String detailPropHosp = JavaHelpers.getPropertyValue(PMS, "detailPropHosp_" + Constants.ENV);

    @When("user search singgahsini property")
    public void user_search_singgahsini_property() throws InterruptedException {
        home.searchProperty(property);
    }

    @Then("button are available")
    public void button_are_available(List<String> button) throws InterruptedException {
        for (int i=0;i< button.size();i++){
            System.out.println(button.get(i));
            Assert.assertTrue(home.checkButtonExist(button.get(i)));
        }
    }

    @Then("button are not available")
    public void button_are_not_available(List<String> button) throws InterruptedException {
        for (int i=0;i<button.size();i++){
            System.out.println(button.get(i));
            Assert.assertFalse(home.checkButtonExist(button.get(i)));
        }
    }

    @When("user search property using ID {string}")
    public void user_search_property_using_ID(String id) throws InterruptedException {
        if (id.equalsIgnoreCase("PMAN")){
            home.searchPropertyByID(PMANID);
        } else {
            home.searchPropertyByID(id);
        }
    }

    @When("user click search property")
    public void user_click_search_property() throws InterruptedException {
        home.clickSearchProperty();
    }

    @When("user search property using name {string}")
    public void user_search_property_using_name(String name) {
        if (name.equalsIgnoreCase("PMAN")){
            home.searchPropertyByName(PMANName);
        } else {
            home.searchPropertyByName(name);
        }
    }

    @Then("property with ID {string} should be found")
    public void property_with_ID_should_be_found(String id) throws InterruptedException {
        common.waitMultipleJavascriptLoading(25);
        if (id.equalsIgnoreCase("PMAN")){
            Assert.assertEquals(home.getPropertyIDFirstList(),PMANID,"ID not equals");
        } else {
            Assert.assertEquals(home.getPropertyIDFirstList(),id,"ID not equals");
        }
    }

    @Then("property with name {string} should be found")
    public void property_with_name_should_be_found(String name) throws InterruptedException {
        common.waitMultipleJavascriptLoading(10);
        if (name.equalsIgnoreCase("PMAN")){
            Assert.assertEquals(home.getPropertyNameFirstList(),PMANName,"Name not equals");
        } else {
            Assert.assertEquals(home.getPropertyNameFirstList(),name,"Name not equals");
        }
    }

    @When("user click x button in search property")
    public void user_click_x_button_in_search_property() throws InterruptedException {
        home.clickClearSearch();
    }

    @Then("search property field should be empty")
    public void search_property_field_should_be_empty() {
        Assert.assertEquals(home.getSearchTextValue(),"");
    }

    @Then("property empty page should be displayed")
    public void property_empty_page_should_be_displayed() throws InterruptedException {
        Assert.assertTrue(home.emptyPageProperty());
    }

    @When("user click homepage action button {string}")
    public void user_click_homepage_action_button(String button) throws InterruptedException {
        home.clickHomeActionButton(button);
    }

    @Then("user redirect to detail property page")
    public void user_redirect_to_detail_property_page() throws InterruptedException {
        common.waitMultipleJavascriptLoading(5);
        Assert.assertEquals(home.getURL(),urlDetailProperty);
    }

    @Then("user redirect to room allotment page")
    public void user_redirect_to_room_allotment_page() throws InterruptedException {
        common.waitMultipleJavascriptLoading(5);
        Assert.assertEquals(home.getURL(),urlKetersediaanKamar);
    }

    @When("user clicks button filter")
    public void user_clicks_button_filter() throws InterruptedException {
        home.clickMainButton(0);
    }

    @And("user inputs values into filter using data {string}")
    public void user_inputs_values_into_filter_using_data(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(7);

        if (data.equalsIgnoreCase("PMAN")) {
            home.inputDate(0, tanggalLiveMulai);
            home.inputDate(1, tanggalLiveAkhir);
            home.inputDate(2, tanggalExpiredMulai);
            home.inputDate(3, tanggalExpiredAkhir);
        };

        home.inputDataDropdown(detailPropProduk, "propProduk", 1, Constants.ENV);
        home.inputDataDropdown(detailPropBSE, "propBSE", 2, Constants.ENV);
        home.inputDataDropdown(detailPropBD, "propBD", 3, Constants.ENV);
        home.inputDataDropdown(detailPropAS, "propAS", 4, Constants.ENV);
        home.inputDataDropdown(detailPropHosp, "propHosp", 5, Constants.ENV);
        home.inputDataDropdown(detailPropKota, "propKota", 6, Constants.ENV);
    }

    @And("user clicks Terapkan")
    public void user_clicks_terapkan() throws InterruptedException {
        home.applyFilter();
    }

}
