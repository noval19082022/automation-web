package steps.pmsSinggahsini;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.pmsSinggahsini.LoginPMSPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class LoginPMSSteps {
    JavaHelpers java;
    SeleniumHelpers selenium;
    private WebDriver driver = ThreadManager.getDriver();
    private LoginPMSPO login = new LoginPMSPO(driver);

    //Test Data Consultant
    private String PMSUser = "src/test/resources/testdata/pms-singgahsini/datauser.properties";
    private String PMANEmail = JavaHelpers.getPropertyValue(PMSUser,"PMSUserPMANEmail_" + Constants.ENV);
    private String PMANPassword = JavaHelpers.getPropertyValue(PMSUser,"PMSUserPMANPassword_" + Constants.ENV);
    private String yudhaEmail = JavaHelpers.getPropertyValue(PMSUser,"PMSUserYudhaEmail_" + Constants.ENV);
    private String yudhaPassword = JavaHelpers.getPropertyValue(PMSUser,"PMSUserYudhaPassword_" + Constants.ENV);
    private String adiEmail = JavaHelpers.getPropertyValue(PMSUser,"PMSUserAdiEmail_" + Constants.ENV);
    private String adiPassword = JavaHelpers.getPropertyValue(PMSUser,"PMSUserAdiPassword_" + Constants.ENV);
    private String yosuaEmail = JavaHelpers.getPropertyValue(PMSUser,"PMSUserYosuaEmail_" + Constants.ENV);
    private String yosuaPassword = JavaHelpers.getPropertyValue(PMSUser,"PMSUserYosuaPassword_" + Constants.ENV);

    @Given("user login as {string}")
    public void user_login_as(String type)
            throws InterruptedException {
        String email="";
        String password="";

        switch (type) {
            case "pman admin":
                email = PMANEmail;
                password = PMANPassword;
                break;
            case "yudha":
                email = yudhaEmail;
                password = yudhaPassword;
                break;
            case "adi":
                email = adiEmail;
                password = adiPassword;
                break;
            case "yosua":
                email = yosuaEmail;
                password = yosuaPassword;
                break;
            default:
                throw new IllegalArgumentException("Please input a valid credentials");
        }
        login.setEmail(email);
        login.setPassword(password);
        login.clickLoginButton();
    }

    @When("user logout")
    public void user_logout() {
        login.userLogOut();
    }
}
