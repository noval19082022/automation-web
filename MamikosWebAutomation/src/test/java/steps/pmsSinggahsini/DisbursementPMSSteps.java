package steps.pmsSinggahsini;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.CommonPO;
import pageobjects.pmsSinggahsini.DisbursementPMSPO;
import pageobjects.pmsSinggahsini.HomepagePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class DisbursementPMSSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HomepagePO home = new HomepagePO(driver);
    private DisbursementPMSPO disbursement = new DisbursementPMSPO(driver);
    private CommonPO common = new CommonPO(driver);

    // Test Data PMS
    private String PMS = "src/test/resources/testdata/pms-singgahsini/datauser.properties";
    private String property = JavaHelpers.getPropertyValue(PMS,"propertyTest_" + Constants.ENV);
    private String PMANID = JavaHelpers.getPropertyValue(PMS,"propertyIDPMAN_" + Constants.ENV);
    private String PMANName = JavaHelpers.getPropertyValue(PMS,"propertyNamePMAN_" + Constants.ENV);

    // Tambahan Pendapatan
    private String tambahanPendapatanNama = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanNama_" + Constants.ENV);
    private String tambahanPendapatanKuantitas = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanKuantitas_" + Constants.ENV);
    private String tambahanPendapatanHarga = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanHargaInput_" + Constants.ENV);
    private String tambahanPendapatanHargaDisplay = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanHargaDisplay_" + Constants.ENV);
    private String tambahanPendapatanTotalSatuanDisplay = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanTotalSatuan_" + Constants.ENV);

    private String tambahanPendapatanNamaEdit = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanNamaEdit_" + Constants.ENV);
    private String tambahanPendapatanKuantitasEdit = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanKuantitasEdit_" + Constants.ENV);
    private String tambahanPendapatanHargaEdit = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanHargaInputEdit_" + Constants.ENV);
    private String tambahanPendapatanHargaDisplayEdit = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanHargaDisplayEdit_" + Constants.ENV);
    private String tambahanPendapatanTotalSatuanDisplayEdit = JavaHelpers.getPropertyValue(PMS, "tambahanPendapatanTotalSatuanEdit_" + Constants.ENV);

    // Disbursement Note
    private String disbursementNote = "src/test/resources/testdata/pms-singgahsini/disbursement.properties";
    private String noteLessThan = JavaHelpers.getPropertyValue(disbursementNote, "disNoteLessThan1500_" + Constants.ENV);
    private String noteMoreThan = JavaHelpers.getPropertyValue(disbursementNote, "disNoteMoreThan1500_" + Constants.ENV);
    private String errorMsgNote = JavaHelpers.getPropertyValue(disbursementNote, "errorMessageNote_" + Constants.ENV);
    private String empStateErrorMsg = JavaHelpers.getPropertyValue(disbursementNote, "emptyStateErrorMessage_" + Constants.ENV);
    private String disbursementProp = JavaHelpers.getPropertyValue(disbursementNote, "disbursementProp_" + Constants.ENV);
    private String ketTambEmpState = JavaHelpers.getPropertyValue(disbursementNote, "keteranganTambahanEmptyState_" + Constants.ENV);

    @When("user clicks disbursement menu on PMS navbar")
    public void user_clicks_disbursement_menu_on_PMS_navbar() throws InterruptedException {
        home.clickNavbarMenu(1);
    }

    @And("user search disbursement property using name {string}")
    public void user_search_disbursement_property_using_name(String name) throws InterruptedException {
        if (name.equalsIgnoreCase("PMAN")){
            disbursement.searchPropertyByName(PMANName);
        } else if (name.equalsIgnoreCase("Disbursement Prop")){
            disbursement.searchPropertyByName(disbursementProp);
        } else {
            disbursement.searchPropertyByName(name);
        }
    }

    @Then("search result should match with profile {string}")
    public void search_result_should_match_with_profile(String name) throws InterruptedException {
        common.waitMultipleJavascriptLoading(3);
        if (name.equalsIgnoreCase("PMAN")) {
            Assert.assertEquals(disbursement.getPropertyNameFirstList(), PMANName, "Name not equals");
        } else if (name.equalsIgnoreCase("Disbursement Prop")){
            Assert.assertEquals(disbursement.getPropertyNameFirstList(), disbursementProp, "Name not equals");
        } else {
            Assert.assertEquals(disbursement.getPropertyNameFirstList(), name, "Name not equals");
        }
    }

    @When("user click disbursement action button {string}")
    public void user_click_disbursement_action_button(String action) throws InterruptedException {
        common.waitMultipleJavascriptLoading(2);
        if (action.equalsIgnoreCase("Lihat Detail")) {
            disbursement.clickKebabBtn(0);
            disbursement.clickAction(1);
        } else if (action.equalsIgnoreCase("Approve")) {
            disbursement.clickKebabBtn(0);
            disbursement.clickAction(0);
        } else if (action.equalsIgnoreCase("Unapprove")) {
            disbursement.clickKebabBtn(0);
            disbursement.clickAction(0);
        }
    }

    @When("user click tambahan pendapatan action button {string}")
    public void user_click_tambahan_pendapatan_action_button(String button) throws InterruptedException {
        common.waitMultipleJavascriptLoading(2);
        if (button.equalsIgnoreCase("Ubah")) {
            disbursement.clickKebabBtnTmbhPndptn();
            disbursement.clickActionTmbhPndptn(0);
        } else if (button.equalsIgnoreCase("Hapus")) {
            disbursement.clickKebabBtnTmbhPndptn();
            disbursement.clickActionTmbhPndptn(1);
        }
    }

    @And("user redirect to Detail Transfer Pendapatan for {string}")
    public void user_redirect_to_detail_transfer_pendapatan_for(String name) throws InterruptedException {
        common.waitMultipleJavascriptLoading(6);
        if (name.equalsIgnoreCase("PMAN")) {
            Assert.assertEquals(disbursement.getPropertyHeader(), PMANName, "Name not equals");
        } else if (name.equalsIgnoreCase("Disbursement Prop")){
            Assert.assertEquals(disbursement.getPropertyHeader(), disbursementProp, "Name not equals");
        } else {
            Assert.assertEquals(disbursement.getPropertyHeader(), name, "Name not equals");
        }
    }

    @And("user clicks button Tambahkan at section {string}")
    public void user_clicks_button_tambahkan_at_section(String section) throws InterruptedException {
        if (section.equalsIgnoreCase("Rincian Penjualan Kamar")) {
            disbursement.clickTambahkan(0);
        } else if (section.equalsIgnoreCase("Biaya Pengurangan Operasional")) {
            disbursement.clickTambahkan(1);
        } else if (section.equalsIgnoreCase("Tambahan Pendapatan")) {
            disbursement.clickTambahkan(2);
        }
    }

    @And("user inputs Tambahan Pendapatan using detail {string}")
    public void user_inputs_tambahan_pendapatan_using_detail(String name) throws InterruptedException {
        if (name.equalsIgnoreCase("PMAN")) {
            disbursement.createTambahanPendapatan(tambahanPendapatanNama, tambahanPendapatanKuantitas, tambahanPendapatanHarga);
        } else if (name.equalsIgnoreCase("PMAN Edit")) {
            disbursement.createTambahanPendapatan(tambahanPendapatanNamaEdit, tambahanPendapatanKuantitasEdit, tambahanPendapatanHargaEdit);
        }
    }

    @Then("Tambahan Pendapatan section will contain data as {string}")
    public void tambahan_pendapatan_section_will_contain_data_as(String name) throws InterruptedException {
        common.waitMultipleJavascriptLoading(15);
        if (name.equalsIgnoreCase("PMAN")) {
            String tambahanPendapatanKuantitasDisplay = tambahanPendapatanKuantitas + "x";
            Assert.assertEquals(disbursement.getNamaPendapatan(), tambahanPendapatanNama, "Nama Pendapatan not equal");
            Assert.assertEquals(disbursement.getHargaSatuan(), tambahanPendapatanHargaDisplay, "Harga Satuan not equal");
            Assert.assertEquals(disbursement.getKuantitas(), tambahanPendapatanKuantitasDisplay, "Kuantitas not equal");
            Assert.assertEquals(disbursement.getTotalPendapatan(), tambahanPendapatanTotalSatuanDisplay, "Total Pendapatan not equal");
        } else if (name.equalsIgnoreCase("PMAN Edit")) {
            String tambahanPendapatanKuantitasDisplayEdit = tambahanPendapatanKuantitasEdit + "x";
            Assert.assertEquals(disbursement.getNamaPendapatan(), tambahanPendapatanNamaEdit, "Nama Pendapatan not equal");
            Assert.assertEquals(disbursement.getHargaSatuan(), tambahanPendapatanHargaDisplayEdit, "Harga Satuan not equal");
            Assert.assertEquals(disbursement.getKuantitas(), tambahanPendapatanKuantitasDisplayEdit, "Kuantitas not equal");
            Assert.assertEquals(disbursement.getTotalPendapatan(), tambahanPendapatanTotalSatuanDisplayEdit, "Total Pendapatan not equal");
        }
    }

    @And("user delete Tambahan Pendapatan")
    public void user_delete_tambahan_pendapatan() throws InterruptedException {
        disbursement.deleteEntry();
    }

    @Then("Tambahan Pendapatan {string} will disappear from list")
    public void tambahan_pendapatan_will_disappear_from_list(String name) throws InterruptedException {
        common.waitMultipleJavascriptLoading(6);
        if (name.equalsIgnoreCase("PMAN")) {
            Assert.assertEquals((disbursement.getStatusTambahanPendapatan(tambahanPendapatanNama)), false, "Tambahan Pendapatan not deleted yet");
        } else if (name.equalsIgnoreCase("PMAN Edit")) {
            Assert.assertEquals((disbursement.getStatusTambahanPendapatan(tambahanPendapatanNamaEdit)), false, "Tambahan Pendapatan not deleted yet");
        }
    }

    //----empty state disbursement----//
    @And("user click for next month disbursement")
    public void user_click_for_next_month_disbursement() throws InterruptedException, ParseException {
        disbursement.clickCalendar();
    }

    @Then("search result should showing empty state")
    public void search_result_should_showing_empty_state(){
        Assert.assertEquals(disbursement.getEmptyStateTitle(), disbursement.getEmptyStateTitle(), "the empty state title does not match");
        Assert.assertEquals(disbursement.getEmptyStateBody(), disbursement.getEmptyStateBody(), "the empty state title does not match");
    }

    //-----disbursement note char limit-----//
    @And("user click Ubah button at Keterangan tambahan untuk owner")
    public void user_click_ubah_button_at_keterangan_tambahan_untuk_owner() throws InterruptedException {
        disbursement.clickUbahAtKetTambahan();
    }

    @And("user click Detail Keterangan field")
    public void user_click_detail_keterangan_field() throws InterruptedException {
        disbursement.clickTxtAreaDetKet();
    }

    @And("user input characters {string}")
    public void user_input_characters(String note) throws InterruptedException {
        if (note.equalsIgnoreCase("note <= 1500")){
            disbursement.enterTextDetKet(noteLessThan);
        } else if (note.equalsIgnoreCase("note > 1500")){
            int i;
            for (i=1; i<=3; i++){
                disbursement.enterTextDetKetMoreThan(noteMoreThan);
            }
        }
    }

    @And("user click Simpan button in Keterangan Tambahan popup")
    public void user_click_simpan_button_in_keterangan_tambahan_popup() throws InterruptedException {
        disbursement.clickSimpanKetTmbhn();
    }

    @Then("the data will be saved and toast appears")
    public void the_data_will_be_saved_and_toast_appears(){
        Assert.assertEquals(disbursement.getToastDisbursementNote(), disbursement.getToastDisbursementNote(), "the toast does not match");
    }

    @And("user will see Keterangan Tambahan value {string}")
    public void user_will_see_keterangan_tambahan_value(String note){
        if (note.equalsIgnoreCase("note <= 1500")){
            Assert.assertEquals(disbursement.getDetKetAfterSimpan(), noteLessThan, "The disbursement note is not " + noteLessThan);
        }
    }

    @Then("user verify Status data pendapatan is {string}")
    public void user_verify_Status_data_pendapatan_is(String status) {
        Assert.assertEquals(disbursement.getStatusDataPendapatan(),status,"Status Data Pendapatan not equals to"+status+" ");
    }

    @When("user confirm approve disbursement")
    public void user_confirm_approve_disbursement() throws InterruptedException {
        disbursement.confirmApproveDisbursement();
    }

    @Then("refresh button is not available")
    public void refresh_button_is_not_available() throws InterruptedException {
        Assert.assertFalse(disbursement.isRefreshButtonExist(),"Button Refresh is Present");
    }

    @Then("refresh button is available")
    public void refresh_button_is_available() throws InterruptedException {
        Assert.assertTrue(disbursement.isRefreshButtonExist(),"Button Refresh is Not Present");
    }

    @When("user click approve/unapprove Disbursement in detail disbursement")
    public void user_click_approve_Disbursement_in_detail_disbursement() throws InterruptedException {
        disbursement.clickApproveUnapproveDisbursement();
    }

    @Then("user will see the error message cannot be inputted more than 1500")
    public void user_will_see_the_error_message_cannot_be_inputted_more_than_1500(){
        Assert.assertEquals(disbursement.getErrorMessageNote(), errorMsgNote, "The error message note is not correct");
    }

    @When("user delete all note")
    public void user_delete_all_note(){
        disbursement.deleteAllNote();
    }

    @Then("the empty state error message will be displayed")
    public void the_empty_state_error_message_will_be_displayed(){
        Assert.assertEquals(disbursement.getEmpStateErrorMsg(), empStateErrorMsg, "The error message note is not correct");
    }

    @And("user will see Keterangan tambahan empty state")
    public void user_will_see_keterangan_tambahan_empty_state(){
        Assert.assertEquals(disbursement.getKetTambEmpState(), ketTambEmpState, "The error message note is not correct");
    }

    @Then("the Simpan button is disable")
    public void the_simpan_button_is_disable(){
        Assert.assertEquals(disbursement.getSimpanBtnDisable(), disbursement.getSimpanBtnDisable(), "The simpan button is enable");

    }

    //-------Informasi Pendapatan Transfer Properti--------//
    @When("user open new tab on Riwayat Transfer Pendapatan")
    public void user_open_new_tab_on_riwayat_transfer_pendapatan(){
        disbursement.openNewTabRwytTransPndptn();
    }

    @When("user switch to {int} window")
    public void user_switch_to_window(int window) {
        disbursement.switchToWindow(window);
    }

    @When("user click Refresh Halaman ini")
    public void user_click_Refresh_Halaman_ini() throws InterruptedException {
        disbursement.clickRefreshHlmnBtn();
    }

    @Then("user will see Model Kerja Sama Booking {string} in Informasi Transfer Pendapatan Properti")
    public void user_will_see_Model_Kerja_Sama_Booking_in_Informasi_Transfer_Pendapatan_Properti(String mks) {
        Assert.assertEquals(disbursement.getMKSBooking(),mks, "The result is not match");
    }

    @And("user will see Model Kerja Sama DBET {string} in Informasi Transfer Pendapatan Properti")
    public void user_will_see_Model_Kerja_Sama_DBET_in_Informasi_Transfer_Pendapatan_Properti(String mks) {
        Assert.assertEquals(disbursement.getMKSDbet(),mks, "The result is not match");
    }

    @And("user will see Add On JP {string} in Informasi Transfer Pendapatan Properti")
    public void user_will_see_Add_On_JP_in_Informasi_Transfer_Pendapatan_Properti(String addOn){
        Assert.assertEquals(disbursement.getAddOnJP(), addOn, "The result is not match");
    }

    @And("user will see Add On ADP {string} in Informasi Transfer Pendapatan Properti")
    public void user_will_see_Add_On_ADP_in_Informasi_Transfer_Pendapatan_Properti(String addOn){
        Assert.assertEquals(disbursement.getAddOnADP(), addOn, "The result is not match");
    }
    //-------Informasi Pendapatan Transfer Properti END--------//
}