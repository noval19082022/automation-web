package steps.pmsSinggahsini;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.TenantCommunicationPO;
import utilities.ThreadManager;
import java.util.List;
import java.util.Map;

public class TenantCommunicationSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private TenantCommunicationPO tenantCommunication = new TenantCommunicationPO(driver);

    @And("user click Tenant Communication menu")
    public void user_click_tenant_communication_menu() throws InterruptedException {
        tenantCommunication.clickTenantCommunicationMenu();
    }

    @And("user choose {string} on main page filter")
    public void user_choose_on_main_page_filter(String mainPageFilter) throws InterruptedException {
        tenantCommunication.clickMainPageFilter();
        tenantCommunication.selectMainPageFilter(mainPageFilter);
    }

    @And("user input {string} in the search field on main page")
    public void user_input_in_the_search_field_on_main_page(String keyword) {
        tenantCommunication.inputSearchFieldMainPage(keyword);
    }

    @And("user click search button on main page filter")
    public void user_click_search_button_on_main_page_filter() throws InterruptedException {
        tenantCommunication.clickSearchButtonMainPageFilter();
    }

    @Then("user verify search result on main page contains:")
    public void user_verify_search_result_on_main_page_contains(DataTable dataTable) {
        List<List<String>> list = dataTable.asLists(String.class);
        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals(tenantCommunication.mainPageTableData.get(i).getText(), list.get(0).get(i), "Table head text is not equal to " + list.get(0).get(i));
        }
    }

    @And("user click action Button")
    public void user_click_action_Button() throws InterruptedException {
        tenantCommunication.clickActionButton();
    }

    @And("user choose tandai belum follow up on action button")
    public void user_choose_tandai_belum_follow_up_on_action_button() throws InterruptedException {
        tenantCommunication.clickTandaiBelumFollowUp();
    }

    @Then("user verify search result on main page bse contains tandai belum follow up")
    public void user_verify_search_result_on_main_page_bse_contains_tandai_belum_follow_up() {
        Assert.assertEquals(tenantCommunication.getTandaiBelumFollowUpButton(), "Tandai belum follow-up");
    }

    @And("user choose tandai sudah follow up on action button")
    public void user_choose_tandai_sudah_follow_up_on_action_button() throws InterruptedException {
        tenantCommunication.clickTandaiSudahFollowUp();
    }

    @Then("user verify search result on main page bse contains tandai sudah follow up")
    public void user_verify_search_result_on_main_page_bse_contains_tandai_sudah_follow_up() {
        Assert.assertEquals(tenantCommunication.getTandaiSudahFollowUpButton(), "Tandai sudah follow-up");
    }

    @And("user click filter penyewa")
    public void user_click_filter_penyewa() throws InterruptedException {
        tenantCommunication.clickFilterPenyewa();
    }

    @And("user click terapkan")
    public void user_click_terapkan() throws InterruptedException {
        tenantCommunication.clickTerapkanButton();
    }

    @Then("user verify search result on main page bse contains butuh cepat")
    public void user_verify_search_result_on_main_page_bse_contains_butuh_cepat() {
        Assert.assertTrue(tenantCommunication.getFilterResult().contains("Butuh Cepat"));
    }


    @Then("user verify search result on main page bse contains butuh konfirmasi")
    public void user_verify_search_result_on_main_page_bse_contains_butuh_konfirmasi() {
        Assert.assertEquals(tenantCommunication.getFilterResult(), "Butuh Konfirmasi");
    }


    @Then("user verify search result on main page bse contains diajukan")
    public void user_verify_search_result_on_main_page_bse_contains_diajukan() {
        Assert.assertEquals(tenantCommunication.getFilterResult(), "Diajukan");
    }

    @Then("user verify search result on main page bse contains sudah check in")
    public void user_verify_search_result_on_main_page_bse_contains_sudah_check_in() {
        Assert.assertEquals(tenantCommunication.getFilterResult(), "Sudah Check-in");
    }

    @And("user click Follow Up Status")
    public void user_click_Follow_Up_Status() throws InterruptedException {
        tenantCommunication.clickFollowUpStatus();
    }

    @And("user click Sudah Follow Up")
    public void user_click_Sudah_Follow_Up() throws InterruptedException {
        tenantCommunication.clickSudahDiFollowUp();
    }

    @And("user click Belum Follow Up")
    public void user_click_Belum_Follow_Up() throws InterruptedException {
        tenantCommunication.clickBelumDiFollowUp();
    }

    @Then("detail displayed and contains")
    public void detail_displayed_and_contains(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        for (Map<String, String> content : table) {
            if(content.get("value").equals("available")){
                Assert.assertTrue(tenantCommunication.isDetailRoomImageAvailable());
            } else {
                Assert.assertTrue(tenantCommunication.getDetailText().contains(content.get("value")));
            }
        }
    }

    @And("user click reset button in PMS Admin")
    public void user_click_reset_button_in_PMS_Admin() throws InterruptedException {
        tenantCommunication.clickReset();
    }

    @Then("user verify search result on main page not contains:")
    public void user_verify_search_result_on_main_page_not_contains(DataTable dataTable) {
        List<List<String>> list = dataTable.asLists(String.class);
        for (int i = 0; i < list.size(); i++) {
            Assert.assertNotEquals(tenantCommunication.mainPageTableData.get(i).getText(), list.get(0).get(i), "Table head text is not equal to " + list.get(0).get(i));
        }
}
    @And("user has reset the filter")
    public void user_has_reset_the_filter() throws InterruptedException {
        tenantCommunication.getIconResetIsDisabled();
    }

    @And("user click BSE Maya")
    public void user_click_BSE_Maya() throws InterruptedException {
        tenantCommunication.clickBSE();
    }

    @Then("user verify search result on main page bse contains BSE Maya")
    public void user_verify_search_result_on_main_page_bse_contains_BSE_Maya() {
        Assert.assertTrue(tenantCommunication.getFilterResultOwner().contains("Account Manager Maya"));
    }

    @Then("user see data {string} on Tenant communication page")
    public void user_see_data_on_tenant_communiation_page(String data){
        Assert.assertTrue(tenantCommunication.getEmptyDataTenantCommunication().contains(data));
    }
    @And("user click Tambah Catatan")
    public void user_click_Tambah_Catatan() throws InterruptedException {
        tenantCommunication.clickTambahCatatan();
    }

    @And("user click Note Prioritaskan")
    public void user_click_Note_Prioritaskan() throws InterruptedException {
        tenantCommunication.clickPrioritaskan();
    }

    @And("user fill {string} in note field")
    public void user_fill_note_field(String keyword) {
        tenantCommunication.enterTextNote(keyword);
    }

    @And("user click Simpan in note field")
    public void user_click_Simpan_in_note_field() throws InterruptedException {
        tenantCommunication.clickSimpanNote();
    }
    @Then("user verify search result on main page bse contains Prioritaskan")
    public void user_verify_search_result_on_main_page_bse_contains_Prioritaskan() {
        Assert.assertEquals(tenantCommunication.getFilterResultNote(), "prioritaskan");
    }
    @And("user clear note field")
    public void user_clear_note_field() throws InterruptedException {
        tenantCommunication.clearNoteField();
    }

    @Then("user verify search result on main page bse contains Tambah Catatan")
    public void user_verify_search_result_on_main_page_bse_contains_Tambah_Catatan() {
        Assert.assertEquals(tenantCommunication.getTambahCatatanText(), "+ Tambah Catatan");
    }
    @And("user choose {string} on filter tahapan and {string} on filter status")
    public void user_choose_on_filter_tahapan_and_on_filter_status(String filterTahapan,String filterStatus) throws InterruptedException {
        tenantCommunication.clickFilterPenyewa();
        tenantCommunication.selectFilterTahapan(filterTahapan);
        tenantCommunication.clickFilterStatus();
        tenantCommunication.selectFilterStatus(filterStatus);
    }

    @Then("user verify search result on main page bse contains chat")
    public void user_verify_search_result_on_main_page_bse_contains_chat() {
        Assert.assertTrue(tenantCommunication.getFilterResult2().contains("Chat"));
    }
    @Then("user verify search result on main page bse contains booking")
    public void user_verify_search_result_on_main_page_bse_contains_Booking() {
        Assert.assertTrue(tenantCommunication.getFilterResult2().contains("Booking"));
    }
    @Then("user verify search result on main page bse contains survei")
    public void user_verify_search_result_on_main_page_bse_contains_survei() {
        Assert.assertEquals(tenantCommunication.getFilterResult2(), "Survei");
    }

    @Then("user verify search result on main page bse contains checkin")
    public void user_verify_search_result_on_main_page_bse_contains_check_in() {
        Assert.assertEquals(tenantCommunication.getFilterResult2(), "Checkin");
    }

    @Then("user can see {string} on page")
    public void user_see_empty_data_on_page(String text){
        Assert.assertEquals(tenantCommunication.getEmptyPageText(),text);
    }

    @Then("user verify search result on main page bse contains {string}")
    public void user_verify_search_result_on_main_page_bse_contains(String status) {
        Assert.assertEquals(tenantCommunication.getFilterResult(), status,"Status not match");
    }

    @And("user click track status chat WA")
    public void user_click_track_status_chat_wa() throws InterruptedException {
        tenantCommunication.clickTrackStatusWAButton();
    }

    @And("user input {string} on {string} track status chat whatsApp")
    public void user_input_on_track_status_chat_whats_app(String data, String field) {
        tenantCommunication.enterTextTrackStatusWA(data,field);
    }

    @And("user click gunakan nomor ini")
    public void user_click_gunakan_nomor_ini() throws InterruptedException{
        tenantCommunication.clickGunakanNomorIniButton();
    }

    @And("user choose {string} nama properti")
    public void userChooseNamaProperti(String arg0) {
    }

    @And("user click tambah button")
    public void user_click_tambah_button() throws InterruptedException {
        tenantCommunication.clickTambahButton();
    }

    @Then("user see pagination menu on Main Page is displayed")
    public void user_see_pagination_menu_on_main_page() {
        Assert.assertTrue(tenantCommunication.verifyPaginationMenuOnMainPage());
    }

    @And("user click pagination number {string}")
    public void user_click_pagination_number(String paginationNumber) throws InterruptedException {
        tenantCommunication.clickPaginationNumber(paginationNumber);
    }

    @Then("user see display data row from 20 riwayat")
    public void user_see_display_data_row_from_20_riwayat() {
        Assert.assertTrue(tenantCommunication.verifyDisplayDataRow());
    }

    @And("user clicks on the tenant name on the first row")
    public void user_clicks_on_the_tenant_name_on_the_first_row() throws InterruptedException {
        tenantCommunication.clickTenantNameOnTheFirstRow();
    }

    @Then("user will be in the second pagination")
    public void user_will_be_in_the_second_pagination() {
        Assert.assertTrue(tenantCommunication.verifySecondPagination());
    }

    @Then("user verify nama property on profile page filter is {string}")
    public void user_verify_nama_property_on_profile_page_filter_is(String propertyName) {
        Assert.assertEquals(tenantCommunication.getPropertyNameOnProfilePageFilter(propertyName), propertyName, "Property Name does not match" + propertyName);
    }

    @Then("user verify nama property on main page filter is {string}")
    public void user_verify_nama_property_on_main_page_filter_is(String propertyName) {
        Assert.assertEquals(tenantCommunication.getPropertyNameOnMainPageFilter(propertyName), propertyName, "Property Name does not match" + propertyName);
    }

    @Then("user verify nama property on main page filter is not {string}")
    public void user_verify_nama_property_on_main_page_filter_is_not(String propertyName) {
        Assert.assertFalse(tenantCommunication.getPropertyNameOnMainPageFilterIsNot(propertyName), "Property Name does match");
    }
    @Then("user see at Tenant Main Page Column contains")
    public void user_see_at_Tenant_Main_Page_Column_contains(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        int i=0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(tenantCommunication.getColumnName(i),content.get("Head Table"),"Table Segment should contain " + content.get("Head Table"));
            i++;
        }
   }
    @Then("user verify search result on profile page bse contains {string}")
    public void user_verify_search_result_on_profile_page_bse_contains_Profile_Penyewa(String namaPenyewa) {
        Assert.assertEquals(tenantCommunication.getTextProfilPenyewa(), "Profil Penyewa");
        Assert.assertEquals(tenantCommunication.getRenterName(namaPenyewa), namaPenyewa, "Nama Penyewa does not match" + namaPenyewa);
        Assert.assertEquals(tenantCommunication.getTextRiwayatPencarianKos(), "Riwayat Pencarian Kos");
    }

    @Then("user verify nama penyewa on main page filter is {string}")
    public void user_verify_nama_penyewa_on_main_page_filter_is(String tenantName) {
        Assert.assertEquals(tenantCommunication.getTenantNameOnMainPageFilter(tenantName), tenantName, "Tenant Name does not match" + tenantName);
    }













    @And("user click add tracker status WA")
    public void user_click_add_tracker_status_WA() throws InterruptedException {
        tenantCommunication.clickTambahTrackerWA();
    }
    @And("user filled {string} in note field tracker WA status")
    public void user_filled_note_field_tracker_wa_status(String keyword) {
        tenantCommunication.enterTextNoteStatusWA(keyword);
    }

    @And("user click Tambah in tracker status WA")
    public void user_click_Tambah_in_tracker_status_WA() throws InterruptedException {
        tenantCommunication.clickTambahStatusWA();
    }
    @Then("user verify add tracker status wa")
    public void user_verify_add_tracker_status_wa() {
        Assert.assertEquals(tenantCommunication.getTextPrioritaskan(), "prioritaskan");
    }

    @And("user set the initial state to Tandai belum follow-up")
    public void user_set_the_initial_state_to_tandai_belum_follow_up() throws InterruptedException {
        tenantCommunication.setTandaiBelumFollowUp();
    }

    @Then("user see pagination menu on Detail Tenant is displayed")
    public void user_see_pagination_menu_on_detail_tenant() {
        Assert.assertTrue(tenantCommunication.verifyPaginationMenuOnDetailTenant());
    }

}
