package steps.pmsSinggahsini;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ISelect;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.CommonPO;
import pageobjects.pmsSinggahsini.DetailPropertyPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class DetailPropertySteps {
    private WebDriver driver = ThreadManager.getDriver();
    private DetailPropertyPO detail = new DetailPropertyPO(driver);
    private CommonPO common = new CommonPO(driver);

    //Test Data PMS
    private String PMS = "src/test/resources/testdata/pms-singgahsini/datauser.properties";
    //profil pemilik
    private String ownerName = JavaHelpers.getPropertyValue(PMS,"namaPemilik_" + Constants.ENV);
    private String ownerNameEdit = JavaHelpers.getPropertyValue(PMS,"namaPemilikEdit_" + Constants.ENV);
    private String ownerPhoneNumber = JavaHelpers.getPropertyValue(PMS,"nomorHpPemilik_" + Constants.ENV);
    private String ownerPhoneNumberEdit = JavaHelpers.getPropertyValue(PMS,"nomorHpPemilikEdit_" + Constants.ENV);
    private String ownerAddress = JavaHelpers.getPropertyValue(PMS,"alamatPemilik_" + Constants.ENV);
    private String ownerAddressEdit = JavaHelpers.getPropertyValue(PMS,"alamatPemilikEdit_" + Constants.ENV);
    private String ownerProvince = JavaHelpers.getPropertyValue(PMS,"provinsiPemilik_" + Constants.ENV);
    private String ownerCity = JavaHelpers.getPropertyValue(PMS,"kotaKabupatenPemilik_" + Constants.ENV);
    private String ownerRegion = JavaHelpers.getPropertyValue(PMS,"kecamatanPemilik_" + Constants.ENV);
    private String ownerRegionEdit = JavaHelpers.getPropertyValue(PMS,"kecamatanPemilikEdit_" + Constants.ENV);
    private String ownerVillage = JavaHelpers.getPropertyValue(PMS,"kelurahanPemilik_" + Constants.ENV);
    private String ownerVillageEdit = JavaHelpers.getPropertyValue(PMS,"kelurahanPemilikEdit_" + Constants.ENV);
    //informasi transfer pendapatan
    private String bankAccNumber = JavaHelpers.getPropertyValue(PMS,"nomorRekening_" + Constants.ENV);
    private String bankName = JavaHelpers.getPropertyValue(PMS,"namaBank_" + Constants.ENV);
    private String bankBranchName = JavaHelpers.getPropertyValue(PMS,"cabangBank_" + Constants.ENV);
    private String accountOwnerName = JavaHelpers.getPropertyValue(PMS,"namaPemilikRekening_" + Constants.ENV);
    private String disbursementDate = JavaHelpers.getPropertyValue(PMS,"tanggalTransferkePemilik_" + Constants.ENV);
    private String bankAccNumberEdit = JavaHelpers.getPropertyValue(PMS,"nomorRekeningEdit_" + Constants.ENV);
    private String bankNameEdit = JavaHelpers.getPropertyValue(PMS,"namaBankEdit_" + Constants.ENV);
    private String bankBranchNameEdit = JavaHelpers.getPropertyValue(PMS,"cabangBankEdit_" + Constants.ENV);
    private String accountOwnerNameEdit = JavaHelpers.getPropertyValue(PMS,"namaPemilikRekeningEdit_" + Constants.ENV);
    private String disbursementDateEdit = JavaHelpers.getPropertyValue(PMS,"tanggalTransferkePemilikEdit_" + Constants.ENV);
    //Detail Kerja Sama
    private String product = JavaHelpers.getPropertyValue(PMS,"jenisProduk_" + Constants.ENV);
    private String productEdit = JavaHelpers.getPropertyValue(PMS, "jenisProdukEdit_" + Constants.ENV);
    private String revenueModel = JavaHelpers.getPropertyValue(PMS,"modelKerjaSama_" + Constants.ENV);
    private String revenueModelEdit = JavaHelpers.getPropertyValue(PMS, "modelKerjaSamaEdit_" + Constants.ENV);
    private String basicCommission = JavaHelpers.getPropertyValue(PMS,"basicCommission_" + Constants.ENV);
    private String basicCommissionEdit = JavaHelpers.getPropertyValue(PMS, "basicCommissionEdit_" + Constants.ENV);
    private String totalRoom = JavaHelpers.getPropertyValue(PMS,"totalKamar_" + Constants.ENV);
    private String jpType = JavaHelpers.getPropertyValue(PMS,"tipeJP_" + Constants.ENV);
    private String jpTypeEdit = JavaHelpers.getPropertyValue(PMS, "tipeJPEdit_" + Constants.ENV);
    private String jpPrecentage = JavaHelpers.getPropertyValue(PMS,"presentaseJP_" + Constants.ENV);
    private String jpPrecentageEdit = JavaHelpers.getPropertyValue(PMS, "presentaseJPEdit_" + Constants.ENV);
    private String jpAmount = JavaHelpers.getPropertyValue(PMS,"jumlahJP_" + Constants.ENV);
    private String jpAmountEdit = JavaHelpers.getPropertyValue(PMS, "jumlahJPEdit_" + Constants.ENV);
    private String adpType = JavaHelpers.getPropertyValue(PMS,"tipeADP_" + Constants.ENV);
    private String adpTypeEdit = JavaHelpers.getPropertyValue(PMS, "tipeADPEdit_" + Constants.ENV);
    private String adpPrecentage = JavaHelpers.getPropertyValue(PMS,"presentaseADP_" + Constants.ENV);
    private String adpPrecentageEdit = JavaHelpers.getPropertyValue(PMS, "presentaseADPEdit_" + Constants.ENV);
    private String adpAmount = JavaHelpers.getPropertyValue(PMS,"jumlahADP_" + Constants.ENV);
    private String adpAmountEdit = JavaHelpers.getPropertyValue(PMS, "jumlahADPEdit_" + Constants.ENV);
    private String ownerShare = JavaHelpers.getPropertyValue(PMS,"presentasePemilik_" + Constants.ENV);
    private String ownerShareEdit = JavaHelpers.getPropertyValue(PMS,"presentasePemilikEdit_" + Constants.ENV);
    private String mamikosShare = JavaHelpers.getPropertyValue(PMS,"presentaseMamikos_" + Constants.ENV);
    private String mamikosShareEdit = JavaHelpers.getPropertyValue(PMS,"presentaseMamikosEdit_" + Constants.ENV);

    private String agreementHybrid = JavaHelpers.getPropertyValue(PMS, "kerjaSamaHybrid_" + Constants.ENV);
    private String agreementHybridEdit = JavaHelpers.getPropertyValue(PMS, "kerjaSamaHybridEdit_" + Constants.ENV);
    private String dbetPercentageOwner = JavaHelpers.getPropertyValue(PMS,"DBETPresentasePemilik_" + Constants.ENV);
    private String dbetPercentageOwnerEdit = JavaHelpers.getPropertyValue(PMS, "DBETPresentasePemilikEdit_" + Constants.ENV);
    private String dbetPercentageMamikos = JavaHelpers.getPropertyValue(PMS, "DBETPresentaseMamikos_" + Constants.ENV);
    private String dbetPercentageMamikosEdit = JavaHelpers.getPropertyValue(PMS, "DBETPresentaseMamikosEdit_" + Constants.ENV);

    private String contractDuration = JavaHelpers.getPropertyValue(PMS,"jangkaWaktuKerjaSama_" + Constants.ENV);
    private String startDate = JavaHelpers.getPropertyValue(PMS,"awalKerjaSama_" + Constants.ENV);
    private String endDate = JavaHelpers.getPropertyValue(PMS,"akhirKerjaSama_" + Constants.ENV);
    private String memberFee = JavaHelpers.getPropertyValue(PMS,"biayaKeanggotaan_" + Constants.ENV);
    private String memberFeeEdit = JavaHelpers.getPropertyValue(PMS, "biayaKeanggotaanEdit_" + Constants.ENV);
    //Biaya Tambahan
    private String additionalFee1Name = JavaHelpers.getPropertyValue(PMS,"biayaTambahan1Name_" + Constants.ENV);
    private String additionalFee1Amount = JavaHelpers.getPropertyValue(PMS,"biayaTambahan1Amount_" + Constants.ENV);
    private String additionalFee2Name = JavaHelpers.getPropertyValue(PMS,"biayaTambahan2Name_" + Constants.ENV);
    private String additionalFee2Amount = JavaHelpers.getPropertyValue(PMS,"biayaTambahan2Amount_" + Constants.ENV);
    private String additionalFee3Name = JavaHelpers.getPropertyValue(PMS,"biayaTambahan3Name_" + Constants.ENV);
    private String additionalFee3Amount = JavaHelpers.getPropertyValue(PMS,"biayaTambahan3Amount_" + Constants.ENV);
    //Rincian Tipe Kamar dan Harga
    private String  tipeKamar1 = JavaHelpers.getPropertyValue(PMS,"tipeKamar1_" + Constants.ENV);
    private String  tipeKamar2 = JavaHelpers.getPropertyValue(PMS,"tipeKamar2_" + Constants.ENV);
    private String  tipeKamar3 = JavaHelpers.getPropertyValue(PMS,"tipeKamar3_" + Constants.ENV);
    private String  gender1 = JavaHelpers.getPropertyValue(PMS,"genderListing1_" + Constants.ENV);
    private String  gender2 = JavaHelpers.getPropertyValue(PMS,"genderListing2_" + Constants.ENV);
    private String  gender3 = JavaHelpers.getPropertyValue(PMS,"genderListing3_" + Constants.ENV);
    private String  totalRoom1 = JavaHelpers.getPropertyValue(PMS,"totalRoom1_" + Constants.ENV);
    private String  totalRoom2 = JavaHelpers.getPropertyValue(PMS,"totalRoom2_" + Constants.ENV);
    private String  totalRoom3 = JavaHelpers.getPropertyValue(PMS,"totalRoom3_" + Constants.ENV);
    private String  monthlyPrice1 = JavaHelpers.getPropertyValue(PMS,"monthlyPrice1_" + Constants.ENV);
    private String  monthlyPrice2 = JavaHelpers.getPropertyValue(PMS,"monthlyPrice2_" + Constants.ENV);
    private String  monthlyPrice3 = JavaHelpers.getPropertyValue(PMS,"monthlyPrice3_" + Constants.ENV);
    private String  threeMonthlyPrice1 = JavaHelpers.getPropertyValue(PMS,"3monthlyPrice1_" + Constants.ENV);
    private String  threeMonthlyPrice2 = JavaHelpers.getPropertyValue(PMS,"3monthlyPrice2_" + Constants.ENV);
    private String  threeMonthlyPrice3 = JavaHelpers.getPropertyValue(PMS,"3monthlyPrice3_" + Constants.ENV);
    private String  sixMonthlyPrice1 = JavaHelpers.getPropertyValue(PMS,"6monthlyPrice1_" + Constants.ENV);
    private String  sixMonthlyPrice2 = JavaHelpers.getPropertyValue(PMS,"6monthlyPrice2_" + Constants.ENV);
    private String  sixMonthlyPrice3 = JavaHelpers.getPropertyValue(PMS,"6monthlyPrice3_" + Constants.ENV);
    private String  monthlyStaticPrice1 = JavaHelpers.getPropertyValue(PMS,"monthlyStaticPrice1_" + Constants.ENV);
    private String  monthlyStaticPrice2 = JavaHelpers.getPropertyValue(PMS,"monthlyStaticPrice2_" + Constants.ENV);
    private String  monthlyStaticPrice3 = JavaHelpers.getPropertyValue(PMS,"monthlyStaticPrice3_" + Constants.ENV);
    private String  threeMonthlyStaticPrice1 = JavaHelpers.getPropertyValue(PMS,"3monthlyStaticPrice1_" + Constants.ENV);
    private String  threeMonthlyStaticPrice2 = JavaHelpers.getPropertyValue(PMS,"3monthlyStaticPrice2_" + Constants.ENV);
    private String  threeMonthlyStaticPrice3 = JavaHelpers.getPropertyValue(PMS,"3monthlyStaticPrice3_" + Constants.ENV);
    private String  sixMonthlyStaticPrice1 = JavaHelpers.getPropertyValue(PMS,"6monthlyStaticPrice1_" + Constants.ENV);
    private String  sixMonthlyStaticPrice2 = JavaHelpers.getPropertyValue(PMS,"6monthlyStaticPrice2_" + Constants.ENV);
    private String  sixMonthlyStaticPrice3 = JavaHelpers.getPropertyValue(PMS,"6monthlyStaticPrice3_" + Constants.ENV);
    private String  monthlyPublishPrice1 = JavaHelpers.getPropertyValue(PMS,"monthlyPublishPrice1_" + Constants.ENV);
    private String  monthlyPublishPrice2 = JavaHelpers.getPropertyValue(PMS,"monthlyPublishPrice2_" + Constants.ENV);
    private String  monthlyPublishPrice3 = JavaHelpers.getPropertyValue(PMS,"monthlyPublishPrice3_" + Constants.ENV);
    private String  threeMonthlyPublishPrice1 = JavaHelpers.getPropertyValue(PMS,"3monthlyPublishPrice1_" + Constants.ENV);
    private String  threeMonthlyPublishPrice2 = JavaHelpers.getPropertyValue(PMS,"3monthlyPublishPrice2_" + Constants.ENV);
    private String  threeMonthlyPublishPrice3 = JavaHelpers.getPropertyValue(PMS,"3monthlyPublishPrice3_" + Constants.ENV);
    private String  sixmonthlyPublishPrice1 = JavaHelpers.getPropertyValue(PMS,"6monthlyPublishPrice1_" + Constants.ENV);
    private String  sixmonthlyPublishPrice2 = JavaHelpers.getPropertyValue(PMS,"6monthlyPublishPrice2_" + Constants.ENV);
    private String  sixmonthlyPublishPrice3 = JavaHelpers.getPropertyValue(PMS,"6monthlyPublishPrice3_" + Constants.ENV);

    // OVERVIEW Tab
    // Profile Properti
    private String propertyID = JavaHelpers.getPropertyValue(PMS,"propertyIDPMAN_" + Constants.ENV);
    private String propertyName = JavaHelpers.getPropertyValue(PMS, "propertyNamePMAN_" + Constants.ENV);
    private String propertyTypes = JavaHelpers.getPropertyValue(PMS, "propertyType_" + Constants.ENV);
    private String propertyTenantJob1 = JavaHelpers.getPropertyValue(PMS, "propertySyaratPekerjaanPenyewa1_" + Constants.ENV);
    private String propertyTenantJob2 = JavaHelpers.getPropertyValue(PMS, "propertySyaratPekerjaanPenyewa2_" + Constants.ENV);
    private String propertyTenantJob3 = JavaHelpers.getPropertyValue(PMS, "propertySyaratPekerjaanPenyewa3_" + Constants.ENV);
    private String propertyTenantJob4 = JavaHelpers.getPropertyValue(PMS, "propertySyaratPekerjaanPenyewa4_" + Constants.ENV);
    private String propertyTenantReligion1 = JavaHelpers.getPropertyValue(PMS, "propertySyaratAgamaPenyewa1_" + Constants.ENV);
    private String propertyTenantReligion2 = JavaHelpers.getPropertyValue(PMS, "propertySyaratAgamaPenyewa2_" + Constants.ENV);
    private String propertyTenantReligion3 = JavaHelpers.getPropertyValue(PMS, "propertySyaratAgamaPenyewa3_" + Constants.ENV);
    private String propertyTenantReligion4 = JavaHelpers.getPropertyValue(PMS, "propertySyaratAgamaPenyewa4_" + Constants.ENV);
    private String propertyTenantReligion5 = JavaHelpers.getPropertyValue(PMS, "propertySyaratAgamaPenyewa5_" + Constants.ENV);
    private String propertyTenantReligion6 = JavaHelpers.getPropertyValue(PMS, "propertySyaratAgamaPenyewa6_" + Constants.ENV);
    private String propertyTenantReligion7 = JavaHelpers.getPropertyValue(PMS, "propertySyaratAgamaPenyewa7_" + Constants.ENV);
    private String propertyAddress = JavaHelpers.getPropertyValue(PMS, "propertyAddress_" + Constants.ENV);
    private String propertyAddressEdit = JavaHelpers.getPropertyValue(PMS, "propertyAddressEdit_" + Constants.ENV);
    private String propertyMapResult = JavaHelpers.getPropertyValue(PMS, "propertyPetaLokasiResult_" + Constants.ENV);
    private String propertyMapResultEdit = JavaHelpers.getPropertyValue(PMS, "propertyPetaLokasiResultEdit_" + Constants.ENV);
    private String propertyMapField = JavaHelpers.getPropertyValue(PMS, "propertyPetaLokasiField_" + Constants.ENV);
    private String propertyMapFieldEdit = JavaHelpers.getPropertyValue(PMS, "propertyPetaLokasiFieldEdit_" + Constants.ENV);
    // Penanggung Jawab
    private String BSEName = JavaHelpers.getPropertyValue(PMS, "namaBSE_" + Constants.ENV);
    private String BSENameEdit = JavaHelpers.getPropertyValue(PMS, "namaBSEEdit_" + Constants.ENV);
    private String BDName = JavaHelpers.getPropertyValue(PMS, "namaBD_" + Constants.ENV);
    private String BDNameEdit = JavaHelpers.getPropertyValue(PMS, "namaBDEdit_" + Constants.ENV);
    private String ASName = JavaHelpers.getPropertyValue(PMS, "namaAS_" + Constants.ENV);
    private String ASNameEdit = JavaHelpers.getPropertyValue(PMS, "namaASEdit_" + Constants.ENV);
    private String hospitalityName = JavaHelpers.getPropertyValue(PMS, "namaHospitality_" + Constants.ENV);
    private String hospitalityNameEdit = JavaHelpers.getPropertyValue(PMS, "namaHospitalityEdit_" + Constants.ENV);

    @When("user click property detail tab {string}")
    public void user_click_property_detail_tab(String tab) throws InterruptedException {
        detail.clickTab(tab);
    }

    @Then("profil pemilik section match with profil pemilik {string}")
    public void profil_pemilik_section_match_with_profil_pemilik(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(20);
        if (data.equalsIgnoreCase("PMAN")){
            Assert.assertEquals(detail.getOwnerName(), ownerName, "owner Name not equal");
            Assert.assertEquals(detail.getOwnerPhoneNumber(), ownerPhoneNumber, "owner Phone Number not equal");
            Assert.assertEquals(detail.getOwnerAddress(), ownerAddress, "owner Address not equal");
            Assert.assertEquals(detail.getOwnerProvince(), ownerProvince, "owner Province not equal");
            Assert.assertEquals(detail.getOwnerCity(), ownerCity, "owner Name City not equal");
            Assert.assertEquals(detail.getOwnerRegion(), ownerRegion, "owner Region not equal");
            Assert.assertEquals(detail.getOwnerVillage(), ownerVillage, "owner Village not equal");
        }else if (data.equalsIgnoreCase("PMAN Edit")){
            Assert.assertEquals(detail.getOwnerName(), ownerNameEdit, "owner Name not equal");
            Assert.assertEquals(detail.getOwnerPhoneNumber(), ownerPhoneNumberEdit, "owner Phone Number not equal");
            Assert.assertEquals(detail.getOwnerAddress(), ownerAddressEdit, "owner Address not equal");
            Assert.assertEquals(detail.getOwnerProvince(), ownerProvince, "owner Province not equal");
            Assert.assertEquals(detail.getOwnerCity(), ownerCity, "owner Name City not equal");
            Assert.assertEquals(detail.getOwnerRegion(), ownerRegionEdit, "owner Region not equal");
            Assert.assertEquals(detail.getOwnerVillage(), ownerVillageEdit, "owner Village not equal");
        } else {
            System.out.println("Test Data not set yet");
        }
    }

    @When("user click ubah profil pemilik")
    public void user_click_ubah_profil_pemilik() throws InterruptedException {
        detail.clickUbahProfilPemilik();
    }

    @When("user change profil pemilik using profil pemilik {string}")
    public void user_change_profil_pemilik_using_profil_pemilik(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(14);
        if (data.equalsIgnoreCase("PMAN Edit")){
            detail.setOwnerName(ownerNameEdit);
            detail.setOwnerPhoneNumber(ownerPhoneNumberEdit);
            detail.setOwnerAddress(ownerAddressEdit);
//            detail.setOwnerProvince("Jawa Tengah");
//            detail.setOwnerCity("Kota Semarang");
            common.waitMultipleJavascriptLoading(3);
            detail.setOwnerRegion(ownerRegionEdit);
            common.waitMultipleJavascriptLoading(2);
            detail.setOwnerVillage(ownerVillageEdit);
            detail.saveEditOwnerProfile();
        } else if (data.equalsIgnoreCase("PMAN")) {
            detail.setOwnerName(ownerName);
            detail.setOwnerPhoneNumber(ownerPhoneNumber);
            detail.setOwnerAddress(ownerAddress);
//            detail.setOwnerProvince("Jawa Tengah");
//            detail.setOwnerCity("Kota Semarang");
            common.waitMultipleJavascriptLoading(3);
            detail.setOwnerRegion(ownerRegion);
            common.waitMultipleJavascriptLoading(2);
            detail.setOwnerVillage(ownerVillage);
            detail.saveEditOwnerProfile();
        } else {
            System.out.println("Test Data not set yet");
        }
    }

    @Then("informasi transfer pendapatan section match with informasi transfer pendapatan {string}")
    public void informasi_transfer_pendapatan_section_match_with_informasi_transfer_pendapatan(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(20);
        if (data.equalsIgnoreCase("PMAN")){
            Assert.assertEquals(detail.getBankAccountNumber(), bankAccNumber, "No Rekening not equal");
            Assert.assertEquals(detail.getBankName(), bankName, "Nama Bank not equal");
            Assert.assertEquals(detail.getBankBranchName(), bankBranchName, "Nama Cabang Bank not equal");
            Assert.assertEquals(detail.getAccountOwnerName(), accountOwnerName, "Nama Pemilik Rekening not equal");
            Assert.assertEquals(detail.getDisbursementDate(), disbursementDate, "Tanggal Disbursement not equal");
        } else if (data.equalsIgnoreCase("PMAN edit")){
            Assert.assertEquals(detail.getBankAccountNumber(), bankAccNumberEdit, "No Rekening not equal");
            Assert.assertEquals(detail.getBankName(), bankNameEdit, "Nama Bank not equal");
            Assert.assertEquals(detail.getBankBranchName(), bankBranchNameEdit, "Nama Cabang Bank not equal");
            Assert.assertEquals(detail.getAccountOwnerName(), accountOwnerNameEdit, "Nama Pemilik Rekening not equal");
            Assert.assertEquals(detail.getDisbursementDate(), disbursementDateEdit, "Tanggal Disbursement not equal");
        }else {
            System.out.println("Test Data not set yet");
        }
    }

    @When("user scroll to detail kerja sama section")
    public void user_scroll_to_detail_kerja_sama_section() throws InterruptedException {
        common.waitMultipleJavascriptLoading(6);
        detail.scrollToDetailKerjaSama();
    }

    @When("user scroll to biaya tambahan section")
    public void user_scroll_to_biaya_tambahan_section() throws InterruptedException {
        common.waitMultipleJavascriptLoading(20);
        detail.scrollToBiayaTambahan();
    }

    @When("user scroll to rincian harga kamar section")
    public void user_scroll_to_rincian_harga_kamar_section() throws InterruptedException {
        common.waitMultipleJavascriptLoading(5);
        detail.scrollToRincianHargaKamar();
    }

    @Then("detail kerja sama section match with detail kerja sama {string}")
    public void detail_kerja_sama_section_match_with_detail_kerja_sama(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(15);
        if (data.equalsIgnoreCase("PMAN")){
            Assert.assertEquals(detail.getProduct(), product, "Jenis Produk not equal");
            Assert.assertEquals(detail.getRevenueModel(), revenueModel, "Model Kerja Sama not equal");
            Assert.assertEquals(detail.getBasicCommission(), basicCommission, "Basic Commisison not equal");
            Assert.assertEquals(detail.getTotalRoom(), totalRoom, "Jumlah Kamar not equal");
            Assert.assertEquals(detail.getJPType(), jpType, "Tipe Add On JP not equal");
            Assert.assertEquals(detail.getJPPrecentage(), jpPrecentage, "Presentase Add On JP not equal");
            Assert.assertEquals(detail.getJPAmount(), jpAmount, "Jumlah Add On JP not equal");
            Assert.assertEquals(detail.getADPType(), adpType, "Tipe Add On ADP not equal");
            Assert.assertEquals(detail.getADPPrecentage(), adpPrecentage, "Persentase Add On ADP not equal");
            Assert.assertEquals(detail.getADPAmount(), adpAmount, "Jumlah Add On ADP not equal");
            Assert.assertEquals(detail.getOwnerShare(), ownerShare, "Pendapatan Pemilik not equal");
            Assert.assertEquals(detail.getMamikosShare(), mamikosShare, "Pendapatan Mamikos not equal");
            Assert.assertEquals(detail.getContractDuration(0), contractDuration, "Jangka Waktu Kerja Sama not equal");
            Assert.assertEquals(detail.getStartDate(0), startDate, "Awal Kerja Sama not equal");
            Assert.assertEquals(detail.getEndDate(0), endDate, "Akhir Kerja Sama not equal");
            Assert.assertEquals(detail.getMemberFee(0), memberFee, "Biaya Keanggotaan not equal");
        } else if (data.equalsIgnoreCase("PMAN Edit")) {
            Assert.assertEquals(detail.getProduct(), productEdit, "Jenis Produk not equal");
            Assert.assertEquals(detail.getRevenueModel(), revenueModelEdit, "Model Kerja Sama not equal");
            Assert.assertEquals(detail.getBasicCommission(), basicCommissionEdit, "Basic Commisison not equal");
            Assert.assertEquals(detail.getTotalRoom(), totalRoom, "Jumlah Kamar not equal");
            Assert.assertEquals(detail.getJPType(), jpTypeEdit, "Tipe Add On JP not equal");
            Assert.assertEquals(detail.getJPPrecentage(), jpPrecentageEdit, "Presentase Add On JP not equal");
            Assert.assertEquals(detail.getJPAmount(), jpAmountEdit, "Jumlah Add On JP not equal");
            Assert.assertEquals(detail.getADPType(), adpTypeEdit, "Tipe Add On ADP not equal");
            Assert.assertEquals(detail.getADPPrecentage(), adpPrecentageEdit, "Persentase Add On ADP not equal");
            Assert.assertEquals(detail.getADPAmount(), adpAmountEdit, "Jumlah Add On ADP not equal");
            Assert.assertEquals(detail.getOwnerShare(), ownerShareEdit, "Pendapatan Pemilik not equal");
            Assert.assertEquals(detail.getMamikosShare(), mamikosShareEdit, "Pendapatan Mamikos not equal");
            Assert.assertEquals(detail.getContractDuration(0), contractDuration, "Jangka Waktu Kerja Sama not equal");
            Assert.assertEquals(detail.getStartDate(0), startDate, "Awal Kerja Sama not equal");
            Assert.assertEquals(detail.getEndDate(0), endDate, "Akhir Kerja Sama not equal");
            Assert.assertEquals(detail.getMemberFee(0), memberFeeEdit, "Biaya Keanggotaan not equal");
        } else if (data.equalsIgnoreCase("PMAN Hybrid on")) {
            Assert.assertEquals(detail.getProduct(), product, "Jenis Produk not equal");
            Assert.assertEquals(detail.getRevenueModel(), revenueModel, "Model Kerja Sama not equal");
            Assert.assertEquals(detail.getBasicCommission(), basicCommission, "Basic Commisison not equal");
            Assert.assertEquals(detail.getTotalRoom(), totalRoom, "Jumlah Kamar not equal");
            Assert.assertEquals(detail.getJPType(), jpType, "Tipe Add On JP not equal");
            Assert.assertEquals(detail.getJPPrecentage(), jpPrecentage, "Presentase Add On JP not equal");
            Assert.assertEquals(detail.getJPAmount(), jpAmount, "Jumlah Add On JP not equal");
            Assert.assertEquals(detail.getADPType(), adpType, "Tipe Add On ADP not equal");
            Assert.assertEquals(detail.getADPPrecentage(), adpPrecentage, "Persentase Add On ADP not equal");
            Assert.assertEquals(detail.getADPAmount(), adpAmount, "Jumlah Add On ADP not equal");
            Assert.assertEquals(detail.getOwnerShare(), ownerShare, "Pendapatan Pemilik not equal");
            Assert.assertEquals(detail.getMamikosShare(), mamikosShare, "Pendapatan Mamikos not equal");
            Assert.assertEquals(detail.getDBETOwnerShare(), dbetPercentageOwnerEdit, "DBET Pendapatan Pemilik not equal");
            Assert.assertEquals(detail.getDBETMamikosShare(), dbetPercentageMamikosEdit, "DBET Pendapatan Mamikos not equal");
            Assert.assertEquals(detail.getContractDuration(2), contractDuration, "Jangka Waktu Kerja Sama not equal");
            Assert.assertEquals(detail.getStartDate(2), startDate, "Awal Kerja Sama not equal");
            Assert.assertEquals(detail.getEndDate(2), endDate, "Akhir Kerja Sama not equal");
            Assert.assertEquals(detail.getMemberFee(2), memberFee, "Biaya Keanggotaan not equal");
        } else {
            System.out.println("Test Data not set yet");
        }
    }

    @When("user click ubah on detail kerja sama section")
    public void user_click_ubah_on_detail_kerja_sama_section() throws InterruptedException {
        detail.clickUbahDetailKerjaSama(0);
    }

    @And("user change detail kerja sama using detail {string}")
    public void user_change_detail_kerja_sama_using_detail(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(7);
        if (data.equalsIgnoreCase("PMAN")) {
            detail.setProduct(product);
            detail.setRevenueModel(revenueModel);
            detail.setBasicCommission(basicCommission);
            detail.setJpType(jpType);
            if (jpType.equalsIgnoreCase("Full A")
                    || jpType.equalsIgnoreCase("Full B")
                    || jpType.equalsIgnoreCase("Partial")) {
                detail.setJpPercentage(jpPrecentage);
                detail.setJpAmount(jpAmount);
            }
            detail.setAdpType(adpType);
            if (adpType.equalsIgnoreCase("3 Bulan")
                    || adpType.equalsIgnoreCase("6 Bulan")) {
                detail.setAdpPercentage(adpPrecentage);
                detail.setAdpAmount(adpAmount);
            }
            detail.setMemberFee(memberFee);
            detail.saveDetailKerjaSama();
        } else if (data.equalsIgnoreCase("PMAN Edit")) {
            detail.setProduct(productEdit);
            detail.setRevenueModel(revenueModelEdit);
            detail.setBasicCommission(basicCommissionEdit);
            detail.setJpType(jpTypeEdit);
            if (jpTypeEdit.equalsIgnoreCase("Full A")
                    || jpTypeEdit.equalsIgnoreCase("Full B")
                    || jpTypeEdit.equalsIgnoreCase("Partial")) {
                detail.setJpPercentage(jpPrecentageEdit);
                detail.setJpAmount(jpAmountEdit);
            }
            detail.setAdpType(adpTypeEdit);
            if (adpTypeEdit.equalsIgnoreCase("3 Bulan")
                    || adpTypeEdit.equalsIgnoreCase("6 Bulan")) {
                detail.setAdpPercentage(adpPrecentageEdit);
                detail.setAdpAmount(adpAmountEdit);
            }
            detail.setMemberFee(memberFeeEdit);
            detail.saveDetailKerjaSama();
        } else if (data.equalsIgnoreCase("PMAN 5505")){
            detail.setRevenueModel(revenueModelEdit);

        }
        else {
            System.out.println("Test Data not set yet");
        }
    }

    @And("user change details on Model kerja sama hybrid using detail {string}")
    public void user_change_details_on_model_kerja_sama_hybrid_using_detail(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(7);
        if (data.equalsIgnoreCase("PMAN Hybrid on")) {
            detail.toggleHybrid(true);
            detail.fillPendapatanMamikos(dbetPercentageMamikosEdit);
            detail.saveDetailKerjaSama();
        } else if (data.equalsIgnoreCase("PMAN Hybrid off")) {
            detail.toggleHybrid(false);
            detail.saveDetailKerjaSama();
        }
    }

    @Then("biaya tambahan section match with biaya tambahan {string}")
    public void biaya_tambahan_section_match_with_biaya_tambahan(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(15);
        if (data.equalsIgnoreCase("PMAN")){
            String[] BiayaTambahanName = {additionalFee1Name,additionalFee2Name, additionalFee3Name};
            String[] BiayaTambahanAmount = {additionalFee1Amount,additionalFee2Amount, additionalFee3Amount};

            for (int i=0;i<BiayaTambahanName.length;i++){
                Assert.assertEquals(detail.getBiayaTambahanName(i), BiayaTambahanName[i], "Biaya Tambahan Name ke-"+i+" not equal");
                Assert.assertEquals(detail.getBiayaTambahanAmount(i), BiayaTambahanAmount[i], "Biaya Tambahan Amount ke-"+i+" not equal");
            }
        } else {
            System.out.println("Test Data not set yet");
        }
    }

    @Then("rincian harga kamar section match with rincian harga kamar {string}")
    public void rincian_harga_kamar_section_match_with_rincian_harga_kamar(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(5);
        if (data.equalsIgnoreCase("PMAN")){
            String[] TipeA = {tipeKamar1,gender1,totalRoom1,monthlyPrice1,threeMonthlyPrice1,sixMonthlyPrice1,monthlyStaticPrice1,threeMonthlyStaticPrice1,sixMonthlyStaticPrice1,monthlyPublishPrice1,threeMonthlyPublishPrice1,sixmonthlyPublishPrice1};
            String[] TipeB = {tipeKamar2,gender2,totalRoom2,monthlyPrice2,threeMonthlyPrice2,sixMonthlyPrice2,monthlyStaticPrice2,threeMonthlyStaticPrice2,sixMonthlyStaticPrice2,monthlyPublishPrice2,threeMonthlyPublishPrice2,sixmonthlyPublishPrice2};
            String[] TipeC = {tipeKamar3,gender3,totalRoom3,monthlyPrice3,threeMonthlyPrice3,sixMonthlyPrice3,monthlyStaticPrice3,threeMonthlyStaticPrice3,sixMonthlyStaticPrice3,monthlyPublishPrice3,threeMonthlyPublishPrice3,sixmonthlyPublishPrice3};

            for (int i=0;i<TipeA.length;i++){
                Assert.assertEquals(detail.getTipeKamardanHargaTipeA(i), TipeA[i], "Tipe Kamar A data ke-"+(i+1)+" not equal");
                Assert.assertEquals(detail.getTipeKamardanHargaTipeB(i), TipeB[i], "Tipe Kamar B data ke-"+(i+1)+" not equal");
                Assert.assertEquals(detail.getTipeKamardanHargaTipeC(i), TipeC[i], "Tipe Kamar C data ke-"+(i+1)+" not equal");
            }
        } else {
            System.out.println("Test Data not set yet");
        }
    }

    @When("user click ubah informasi transfer pendapatan")
    public void user_click_ubah_informasi_transfer_pendapatan() throws InterruptedException {
        detail.clickUbahInformasiTransferPendapatan();
    }

    @When("user change informasi transfer pendapatan {string}")
    public void user_change_informasi_transfer_pendapatan(String data) throws InterruptedException {
        if (data.equalsIgnoreCase("PMAN")){
            detail.setAccountNumber(bankAccNumber);
            detail.setBankName(bankName);
            detail.setBranchName(bankBranchName);
            detail.setOwnerAccountName(accountOwnerName);
            detail.setDisburseDate(disbursementDate);

            detail.saveEditInformasiTransferPendapatan();
        } else if (data.equalsIgnoreCase("PMAN edit")) {
            detail.setAccountNumber(bankAccNumberEdit);
            detail.setBankName(bankNameEdit);
            detail.setBranchName(bankBranchNameEdit);
            detail.setOwnerAccountName(accountOwnerNameEdit);
            detail.setDisburseDate(disbursementDateEdit);

            detail.saveEditInformasiTransferPendapatan();
        } else {
            System.out.println("Data not set yet");
        }
    }

    // OVERVIEW Tab
    // Profile Properti - START
    @And("profile property section match with profile property {string}")
    public void profile_property_section_match_with_profile_property(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(8);
        if (data.equalsIgnoreCase("PMAN")) {
            String jobsExpected = propertyTenantJob1 + ", " + propertyTenantJob3 + ", " + propertyTenantJob4;
            String jobsReceived = detail.getPropertyTenantJob();
            String jobsActual = JavaHelpers.sortStringElements(jobsReceived);
            String religionsExpected = propertyTenantReligion2 + ", " + propertyTenantReligion3 + ", " + propertyTenantReligion4 + ", " + propertyTenantReligion6;
            String religionsReceived = detail.getPropertyTenantReligion();
            String religionsActual = JavaHelpers.sortStringElements(religionsReceived);

            Assert.assertEquals(detail.getPropertyID(), propertyID, "Property ID not equal");
            Assert.assertEquals(detail.getPropertyName(), propertyName, "Property Name not equal");
            Assert.assertEquals(detail.getPropertyProduct(), product, "Property Product Type not equal");
            Assert.assertEquals(detail.getPropertyType(), propertyTypes, "Property Types not equal");
            Assert.assertEquals(jobsActual, jobsExpected, "Property Tenant Job not equal");
            Assert.assertEquals(religionsActual, religionsExpected, "Property Tenant Religion not equal");
            Assert.assertEquals(detail.getPropertyAddress(), propertyAddress, "Property Address not equal");
            Assert.assertEquals(detail.getPropertyMap(), propertyMapResult, "Property Map not equal");
        } else if (data.equalsIgnoreCase("PMAN Edit")) {
            String jobsExpectedEdit = propertyTenantJob2 + ", " + propertyTenantJob3;
            String jobsReceivedEdit = detail.getPropertyTenantJob();
            String jobsActualEdit = JavaHelpers.sortStringElements(jobsReceivedEdit);
            String religionsExpectedEdit = propertyTenantReligion1 + ", " + propertyTenantReligion2;
            String religionsReceivedEdit = detail.getPropertyTenantReligion();
            String religionsActualEdit = JavaHelpers.sortStringElements(religionsReceivedEdit);

            Assert.assertEquals(detail.getPropertyID(), propertyID, "Property ID not equal");
            Assert.assertEquals(detail.getPropertyName(), propertyName, "Property Name not equal");
            Assert.assertEquals(detail.getPropertyProduct(), product, "Property Product Type not equal");
            Assert.assertEquals(detail.getPropertyType(), propertyTypes, "Property Types not equal");
            Assert.assertEquals(jobsActualEdit, jobsExpectedEdit, "Property Tenant Job not equal");
            Assert.assertEquals(religionsActualEdit, religionsExpectedEdit, "Property Tenant Religion not equal");
            Assert.assertEquals(detail.getPropertyAddress(), propertyAddressEdit, "Property Address not equal");
            Assert.assertEquals(detail.getPropertyMap(), propertyMapResultEdit, "Property Map not equal");
        } else {
            System.out.println("Test Data not set yet");
        }
    }

    @When("user click button ubah on profile property section")
    public void user_click_button_ubah_on_profile_property_section() throws InterruptedException {
        detail.clickUbahProfileProperti();
    }

    @And("user change profile property using profile property {string}")
    public void user_change_profile_property_using_profile_property(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(8);
        detail.selectDropdown(0);
        detail.setPekerjaanPenyewa(propertyTenantJob1);
        detail.setPekerjaanPenyewa(propertyTenantJob4);
        detail.setPekerjaanPenyewa(propertyTenantJob2);
        detail.selectDropdown(1);
        detail.setAgamaPenyewa(propertyTenantReligion1);
        detail.setAgamaPenyewa(propertyTenantReligion3);
        detail.setAgamaPenyewa(propertyTenantReligion4);
        detail.setAgamaPenyewa(propertyTenantReligion6);
        if (data.equalsIgnoreCase("PMAN")) {
            detail.setPropertyTextbox(propertyAddress, 0);
            detail.setPropertyTextbox(propertyMapField, 1);
            detail.saveEditProfileProperti();
        } else if (data.equalsIgnoreCase("PMAN Edit")) {
            detail.setPropertyTextbox(propertyAddressEdit, 0);
            detail.setPropertyTextbox(propertyMapFieldEdit, 1);
            detail.saveEditProfileProperti();
        } else {
            System.out.println("Test Data not set yet");
        }
    }
    // Profile Properti - END

    // Penanggung Jawab - START
    @Then("penanggung jawab section match with profil penanggung jawab {string}")
    public void penanggung_jawab_section_match_with_profil_penanggung_jawab(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(4);
        if (data.equalsIgnoreCase("PMAN")) {
            Assert.assertEquals(detail.getBSEName(), BSEName, "BSE Name not equal" );
            Assert.assertEquals(detail.getBDName(), BDName, "BD Name not equal");
            Assert.assertEquals(detail.getASName(), ASName, "AS Name not equal");
            Assert.assertEquals(detail.getHospitalityName(), hospitalityName, "Hospitality Name not equal");
        } else if (data.equalsIgnoreCase("PMAN Edit")) {
            Assert.assertEquals(detail.getBSEName(), BSENameEdit, "BSE Name not equal");
            Assert.assertEquals(detail.getBDName(), BDNameEdit, "BD Name not equal");
            Assert.assertEquals(detail.getASName(), ASNameEdit, "AS Name not equal");
            Assert.assertEquals(detail.getHospitalityName(), hospitalityNameEdit, "Hospitality Name not equal");
        } else {
            System.out.println("Test Data not set yet");
        }
    }

    @When("user click button edit on penanggung jawab section")
    public void user_click_button_edit_on_penanggung_jawab_section() throws InterruptedException {
        detail.clickEditPenanggungJawab();
    }

    @And("user change profil penanggung jawab using profil penanggung jawab {string}")
    public void user_change_profil_penanggung_jawab_using_profil_penanggung_jawab(String data) throws InterruptedException {
        common.waitMultipleJavascriptLoading(4);
        if (data.equalsIgnoreCase("PMAN Edit")) {
            detail.setBSEName(BSENameEdit);
            detail.setBDName(BDNameEdit);
            detail.setASName(ASNameEdit);
            detail.setHospitalityName(hospitalityNameEdit);
            detail.saveEditPenanggungJawab();
        } else if (data.equalsIgnoreCase("PMAN")) {
            detail.setBSEName(BSEName);
            detail.setBDName(BDName);
            detail.setASName(ASName);
            detail.setHospitalityName(hospitalityName);
            detail.saveEditPenanggungJawab();
        } else {
            System.out.println("Test Data not set yet");
        }
    }
    // Penanggung Jawab - END
    // OVERVIEW Tab

    @When("user click ubah biaya tambahan")
    public void user_click_ubah_biaya_tambahan() throws InterruptedException {
        detail.clickUbahBiayaTambahan();
    }

    //-------Informasi Pendapatan Transfer Properti--------//
    @When("user change Model Kerja Sama {string}")
    public void user_change_Model_Kerja_Sama(String mks) throws InterruptedException {
        detail.revenueModelChanges(mks);
    }

    @When("user change Tipe Add On JP {string}")
    public void user_change_Tipe_Add_On_JP(String jpType) throws InterruptedException {
        detail.jpChanges(jpType);
    }

    @And("user change Persentase Add On JP {string}")
    public void user_change_Persentase_Add_On_JP(String jpPercent) {
        if (!(jpPercent.equalsIgnoreCase("-"))){
            detail.inputJPPercentage(jpPercent);
        }
    }

    @And("user change Jumlah Add On JP {string}")
    public void user_change_Jumlah_Add_On_JP(String jpAmount){
        if (!(jpAmount.equalsIgnoreCase("-"))){
            detail.inputJPAmount(jpAmount);
        }
    }

    @And("user change Tipe Add On ADP {string}")
    public void user_change_Tipe_Add_On_ADP(String adpType) throws InterruptedException {
        detail.adpChanges(adpType);
    }

    @And("user change Persentase Add On ADP {string}")
    public void user_change_Percentase_ADP_On_ADP(String adpPercent){
        if (!(adpPercent.equalsIgnoreCase("-"))){
            detail.inputADPPercentage(adpPercent);
        }
    }

    @And("user change Jumlah Add On ADP {string}")
    public void user_change_Jumlah_Add_On_ADP(String adpAmount){
        if (!(adpAmount.equalsIgnoreCase("-"))){
            detail.inputADPAmount(adpAmount);
        }
    }

    @And("user change the revenue model, jp type, adp type into default setting")
    public void user_change_the_revenue_model_jp_type_adp_type_into_default_setting() throws InterruptedException {
        detail.revertBackToDefault();
    }
    //-------Informasi Pendapatan Transfer Properti END--------//
}
