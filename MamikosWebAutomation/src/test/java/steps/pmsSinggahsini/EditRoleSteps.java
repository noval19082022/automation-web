package steps.pmsSinggahsini;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.EditRolePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.List;

public class EditRoleSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private EditRolePO editRole = new EditRolePO(driver);

    //Test Data PMS
    private String PMSUser = "src/test/resources/testdata/pms-singgahsini/datauser.properties";
    private String existingRoleName = JavaHelpers.getPropertyValue(PMSUser,"existingRoleName_" + Constants.ENV);

    @Then("nama role should be {string}")
    public void nama_role_should_be(String name) {
        Assert.assertEquals(editRole.getRoleName(),name);
    }

    @Then("permissions should be checked:")
    public void permissions_should_be_checked(List<String> permission) {
        for (int i=0;i<permission.size();i++){
            Assert.assertTrue(editRole.getPermissionCheck(permission.get(i)));
        }
    }

    @When("user change name to existing role name")
    public void user_change_name_to_existing_role_name() throws InterruptedException {
        String RoleName = existingRoleName;
        editRole.setRoleName(RoleName);
    }

    @When("user change name to {string}")
    public void user_change_name_to(String name) throws InterruptedException {
        editRole.setRoleName(name);
    }
}
