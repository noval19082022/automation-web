package steps.pmsSinggahsini;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.CommonPO;
import pageobjects.pmsSinggahsini.RoleListPO;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class RoleListSteps {
    JavaHelpers java;
    SeleniumHelpers selenium;
    private WebDriver driver = ThreadManager.getDriver();
    private RoleListPO role = new RoleListPO(driver);
    private CommonPO common = new CommonPO(driver);

    @When("user open role management menu")
    public void user_open_role_management_menu() throws InterruptedException {
        common.waitMultipleJavascriptLoading(10);
        role.clickRoleMenu();
    }

    @And("user click on Tambah Role")
    public void user_click_on_Tambah_Role() throws InterruptedException {
        role.clickTambahRole();
    }

    @Then("toast message {string} should be appear")
    public void toast_message_should_be_appear(String message) throws InterruptedException {
        Assert.assertEquals(role.getToastMessage(),message);
    }

    @When("user search role {string}")
    public void user_search_role(String keyword) throws InterruptedException {
        role.setKeyword(keyword);
        role.clickCari();
    }

    @Then("Role {string} should be exist on the list")
    public void role_should_be_exist_on_the_list(String rolename) throws InterruptedException {
        common.waitMultipleJavascriptLoading(10);
        Assert.assertEquals(role.getRoleList(),rolename);
    }

    @And("user click action button")
    public void user_click_action_button() throws InterruptedException {
        role.clickActionButton();
    }

    @And("user choose role action {string}")
    public void user_choose_role_action(String action) throws InterruptedException {
        role.chooseAction(action);
    }

    @And("user confirm hapus role")
    public void user_confirm_hapus_role() throws InterruptedException {
        role.confirmHapusRole();
    }

    @Then("role tidak ditemukan page should be appear")
    public void role_tidak_ditemukan_page_should_be_appear() throws InterruptedException {
        Assert.assertTrue(role.ruleNotFound());
    }

    @And("user cancel hapus role")
    public void user_cancel_hapus_role() throws InterruptedException {
        role.cancelHapusRole();
    }
}
