package steps.pmsSinggahsini.OtherTransaction;

import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.OtherTransaction.AddOwnerExpenditurePO;
import utilities.ThreadManager;

import java.util.List;
import java.util.Map;

public class AddOnwerExpenditureSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AddOwnerExpenditurePO addOEx = new AddOwnerExpenditurePO(driver);

    @Given("user open other transaction menu")
    public void user_open_other_transaction_menu() throws InterruptedException {
        addOEx.clickOtherTransactionMenu();
    }

    @When("user add new data owner expenditure")
    public void user_add_new_data_owner_expenditure() throws InterruptedException {
        addOEx.addNewDataOwnerExpenditure();
    }

    @When("user choose tipe pengajuan cashout {string}")
    public void user_choose_tipe_pengajuan_cashout(String tipe) throws InterruptedException {
        addOEx.selectTipePengajuanCashout(tipe);
    }

    @When("user select property {string}")
    public void user_select_property(String name) throws InterruptedException {
        addOEx.selectProperty(name);
    }

    @When("user input multiple pengeluaran :")
    public void user_input_multiple_pengeluaran(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
        List<Map<String, String>> data = dataTable.asMaps(String.class,String.class);
        for (Map<String,String> pengeluaran : data){
            String no = pengeluaran.get("no");
            String category = pengeluaran.get("Kategori Pengeluaran");
            String name = pengeluaran.get("Nama Pengeluaran");
            String quantity = pengeluaran.get("Kuantitas");
            String amount = pengeluaran.get("Nominal Pengeluaran");
            String status = pengeluaran.get("Status Persediaan");
            String product = pengeluaran.get("Jenis Produk");

            addOEx.setKategoriPengeluaran(category,no);
            addOEx.setNamaPengeluaran(name,no);
            addOEx.setKuantitas(quantity,no);
            addOEx.setNominalPengeluaran(amount,no);
            addOEx.setStatusPersediaan(status,no);
            addOEx.setJenisProduk(product,no);

            //Tambah Pengeluaran if any
            if (Integer.parseInt(no) < data.size()){
                addOEx.addMorePengeluaran();
            }
        }
    }

    @When("user upload {string} lampiran")
    public void user_upload_lampiran(String type) throws InterruptedException {
        if (type.equalsIgnoreCase("valid")){
            addOEx.uploadValidImage();
        } else if (type.equalsIgnoreCase("invalid")) {
            addOEx.uploadInvalidImage();
        }
    }

    @When("user input no invoice biaya {string}")
    public void user_input_no_invoice_biaya(String inv) {
        addOEx.inputInvoiceNumber(inv);
    }

    @When("user input tujuan transfer {string}")
    public void user_input_tujuan_transfer(String vendor) throws InterruptedException {
        addOEx.selectTujuanTransfer(vendor);
    }

    @When("user click tambah data")
    public void user_click_tambah_data() throws InterruptedException {
        addOEx.clickAddData();
    }

    @When("user {string} tambah data owner expenditure")
    public void user_tambah_data_owner_expenditure(String action) throws InterruptedException {
        if (action.equalsIgnoreCase("confirm")){
            addOEx.confirmAddOwnerExpenditure();
        }else if (action.equalsIgnoreCase("cancel")){
            addOEx.cancelAddOwnerExpenditure();
        }
    }

    @Then("confirmation pop up owner expenditure appear")
    public void confirmation_pop_up_owner_expenditure_appear() throws InterruptedException {
        Assert.assertTrue(addOEx.isAddOwnerExpenditurePopUpAppear());
    }

    @Then("should have confirmation title {string}")
    public void should_have_confirmation_title(String title) {
        Assert.assertEquals(addOEx.getConfirmationPopUpTitle(),title);
    }

    @Then("should have confirmation description {string}")
    public void should_have_confirmation_description(String desc) {
        Assert.assertEquals(addOEx.getDescriptionConfirmationPopUp(),desc);
    }

    @Then("confirmation pop up should closed")
    public void confirmation_pop_up_should_closed() throws InterruptedException {
        Assert.assertFalse(addOEx.isAddOwnerExpenditurePopUpAppear());
    }

    @Then("tambah data button should be enable")
    public void tambah_data_button_should_be_enable() throws InterruptedException {
        Assert.assertFalse(addOEx.isTambahDataButtonEnable());
    }
}
