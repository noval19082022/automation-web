package steps.pmsSinggahsini.OtherTransaction;

import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.OtherTransaction.ListOwnerExpenditurePO;
import utilities.ThreadManager;

import java.util.List;
import java.util.Map;

public class ListOwnerExpenditureSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ListOwnerExpenditurePO listOEx = new ListOwnerExpenditurePO(driver);

    @Then("new owner expenditure record should be on the first list contains:")
    public void new_owner_expenditure_record_should_be_on_the_first_list_contains(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
        List<Map<String, String>> data = dataTable.asMaps(String.class,String.class);
        for (Map<String,String> record : data) {
            String tipe = record.get("Tipe Pengajuan Cash Out");
            String prop = record.get("Nama Properti");
            String total = record.get("Total Pengeluaran");

            Assert.assertEquals(listOEx.getTipePengajuanCashOut(),tipe);
            Assert.assertEquals(listOEx.getNamaProperty(),prop);
            Assert.assertEquals(listOEx.getTotalPengeluaran(),total);
        }
    }

    @Then("should contains nama pengeluaran:")
    public void should_contains_nama_pengeluaran(io.cucumber.datatable.DataTable dataTable) throws InterruptedException {
        List<String> data = dataTable.asList();
        for (int i=1; i< data.size(); i++){
            Assert.assertEquals(listOEx.getNamaPengeluran(i),data.get(i-1));
        }
    }
}
