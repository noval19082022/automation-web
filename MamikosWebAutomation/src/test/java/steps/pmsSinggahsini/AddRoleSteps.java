package steps.pmsSinggahsini;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.pmsSinggahsini.AddRolePO;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.List;

public class AddRoleSteps {
    JavaHelpers java;
    SeleniumHelpers selenium;
    private WebDriver driver = ThreadManager.getDriver();
    private AddRolePO addRole = new AddRolePO(driver);

    @When("user add new pms role name {string}")
    public void user_add_new_pms_role_name(String rolename) throws InterruptedException {
        addRole.setRoleName(rolename);
    }

    @When("user choose permissions")
    public void user_choose_permissions(List<String> permission) throws InterruptedException {
        addRole.setPermissions(permission);
    }

    @And("user click back to role list")
    public void user_click_back_to_role_list() throws InterruptedException {
        addRole.clickBackButton();
    }

    @And("user reset role permissions")
    public void user_reset_role_permissions() throws InterruptedException {
        addRole.clickResetPermissionButton();
    }

    @Then("all permissions role are unchecked")
    public void all_permissions_role_are_unchecked() throws InterruptedException {
        Assert.assertTrue(addRole.noCheckedPermission());
    }

    @And("user click simpan role")
    public void user_click_simpan_role() throws InterruptedException {
        addRole.clickSimpanButton();
    }

    @Then("error message {string} should be appear")
    public void error_message_should_be_appear(String message) throws InterruptedException {
        Assert.assertEquals(addRole.getErrorMessage(),message);
    }
}
