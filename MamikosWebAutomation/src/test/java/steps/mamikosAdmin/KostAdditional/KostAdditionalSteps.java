package steps.mamikosAdmin.KostAdditional;

import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.KostAdditional.KostAdditionalPO;
import utilities.ThreadManager;

public class KostAdditionalSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private KostAdditionalPO kostAdditional = new pageobjects.mamikosAdmin.KostAdditional.KostAdditionalPO(driver);

    @When("user search {string} in kost additional")
    public void user_search_in_kost_additional(String keyword) throws InterruptedException {
        kostAdditional.searchKosAdditional(keyword);
    }

    @When("user click on atur ketersediaan button")
    public void user_click_on_atur_ketersediaan_button() throws InterruptedException {
        kostAdditional.clickButtonAturKetersediaan();
    }

    @Then("status room {string} should be equal to {string}")
    public void status_room_should_be_equal_to(String room, String status) throws InterruptedException {
        Assert.assertEquals(kostAdditional.getRoomUnitStatus(room),status,"room status "+room+" is not equal to "+status+".");
    }

    @When("user click the edit button in room with status is occupied with room name {string}")
    public void user_click_the_edit_button_in_room_with_status_is_occupied_with_room_name_x(String name) throws InterruptedException {
        kostAdditional.clickOnEditButton(name);
    }

    @And("user update room status to empty and click the update button")
    public void user_update_room_status_to_empty_and_click_the_update_button() throws InterruptedException {
        kostAdditional.updateRoomStatusToEmpty();
    }

    @Then("user can see alert {string} on atur kamar page")
    public void user_can_see_alert_x_on_atur_kamar_page(String text) throws InterruptedException {
        if (text.equalsIgnoreCase("Kamar dengan kontrak tidak bisa diubah.")) {
            Assert.assertEquals(kostAdditional.getAlertText(text), "Kamar dengan kontrak tidak bisa diubah.");
        } else if (text.equalsIgnoreCase("Success! Kamar berhasil di-update")) {
            Assert.assertEquals(kostAdditional.getSuccessAlertText(text), "Success!");
        }
    }

    @And("user update lantai name to {string}")
    public void user_update_floor_name_to_x(String keyword) throws InterruptedException {
        kostAdditional.updateFloorName(keyword);
    }
}
