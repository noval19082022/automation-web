package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.ListKostLevelPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class ListKostLevelSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private ListKostLevelPO list = new ListKostLevelPO(driver);

    //Test Data Kost Level
    private String kostLevel = "src/test/resources/testdata/mamikosadmin/kostLevel.properties";

    //original data
    private String levelName = JavaHelpers.getPropertyValue(kostLevel, "levelName_" + Constants.ENV);
    private String levelKey = JavaHelpers.getPropertyValue(kostLevel, "levelKey_" + Constants.ENV);
    private String levelOrder = JavaHelpers.getPropertyValue(kostLevel, "levelOrder_" + Constants.ENV);
    private String levelIsPartner = JavaHelpers.getPropertyValue(kostLevel, "levelIsPartner_" + Constants.ENV);
    private String LevelSetValues = JavaHelpers.getPropertyValue(kostLevel, "LevelSetValues_" + Constants.ENV);
    private String levelBenefit = JavaHelpers.getPropertyValue(kostLevel, "levelBenefit_" + Constants.ENV);
    private String levelCriteria = JavaHelpers.getPropertyValue(kostLevel, "levelCriteria_" + Constants.ENV);
    private String levelStatus = JavaHelpers.getPropertyValue(kostLevel, "levelStatus_" + Constants.ENV);
    private String levelRoom = JavaHelpers.getPropertyValue(kostLevel, "levelRoom_" + Constants.ENV);
    private String levelFee = JavaHelpers.getPropertyValue(kostLevel, "levelFee_" + Constants.ENV);
    private String levelType = JavaHelpers.getPropertyValue(kostLevel, "levelType_" + Constants.ENV);
    private String levelBooking = JavaHelpers.getPropertyValue(kostLevel, "levelBooking_" + Constants.ENV);
    private String levelConsultant = JavaHelpers.getPropertyValue(kostLevel, "levelConsultant_" + Constants.ENV);
    private String levelOwner = JavaHelpers.getPropertyValue(kostLevel, "levelOwner_" + Constants.ENV);
    private String levelInvoiceType = JavaHelpers.getPropertyValue(kostLevel, "levelInvoiceType_" + Constants.ENV);
    private String levelValue = JavaHelpers.getPropertyValue(kostLevel, "levelValue_" + Constants.ENV);
    private String levelNotes = JavaHelpers.getPropertyValue(kostLevel, "levelNotes_" + Constants.ENV);

    //update data
    private String levelName_update = JavaHelpers.getPropertyValue(kostLevel, "levelName_update_" + Constants.ENV);
    private String levelKey_update = JavaHelpers.getPropertyValue(kostLevel, "levelKey_update_" + Constants.ENV);
    private String levelOrder_update = JavaHelpers.getPropertyValue(kostLevel, "levelOrder_update_" + Constants.ENV);
    private String levelIsPartner_update = JavaHelpers.getPropertyValue(kostLevel, "levelIsPartner_update_" + Constants.ENV);
    private String LevelSetValues_update = JavaHelpers.getPropertyValue(kostLevel, "LevelSetValues_update_" + Constants.ENV);
    private String levelBenefit_update = JavaHelpers.getPropertyValue(kostLevel, "levelBenefit_update_" + Constants.ENV);
    private String levelCriteria_update = JavaHelpers.getPropertyValue(kostLevel, "levelCriteria_update_" + Constants.ENV);
    private String levelStatus_update = JavaHelpers.getPropertyValue(kostLevel, "levelStatus_update_" + Constants.ENV);
    private String levelRoom_update = JavaHelpers.getPropertyValue(kostLevel, "levelRoom_update_" + Constants.ENV);
    private String levelFee_update = JavaHelpers.getPropertyValue(kostLevel, "levelFee_update_" + Constants.ENV);
    private String levelType_update = JavaHelpers.getPropertyValue(kostLevel, "levelType_update_" + Constants.ENV);
    private String levelBooking_update = JavaHelpers.getPropertyValue(kostLevel, "levelBooking_update_" + Constants.ENV);
    private String levelConsultant_update = JavaHelpers.getPropertyValue(kostLevel, "levelConsultant_update_" + Constants.ENV);
    private String levelOwner_update = JavaHelpers.getPropertyValue(kostLevel, "levelOwner_update_" + Constants.ENV);
    private String levelInvoiceType_update = JavaHelpers.getPropertyValue(kostLevel, "levelInvoiceType_update_" + Constants.ENV);
    private String levelValue_update = JavaHelpers.getPropertyValue(kostLevel, "levelValue_update_" + Constants.ENV);
    private String levelNotes_update = JavaHelpers.getPropertyValue(kostLevel, "levelNotes_update_" + Constants.ENV);


    @Then("system display new kost level")
    public void system_display_new_kost_level() throws InterruptedException {
        list.searchKostLevel(levelName);
        String[] dataLevel = {levelName,
                levelKey,
                levelBenefit,
                levelCriteria,
                levelStatus,
                levelRoom,
                levelFee,
                levelType,
                levelBooking,
                levelConsultant,
                levelOwner,
                levelInvoiceType,
                levelValue,
                levelNotes};

        for (int i = 1; i < 15; i++){
            Assert.assertEquals(list.getValueColumnKostLevel(i).toLowerCase(), dataLevel[i-1].toLowerCase(), "Value not match");
        }
    }

    @Then("system display updated kost level")
    public void system_display_updated_kost_level() throws InterruptedException {
        list.searchKostLevel(levelName_update);
        String[] dataLevel = {levelName_update,
                levelKey_update,
                levelBenefit_update,
                levelCriteria_update,
                levelStatus_update,
                levelRoom_update,
                levelFee_update,
                levelType_update,
                levelBooking_update,
                levelConsultant_update,
                levelOwner_update,
                levelInvoiceType_update,
                levelValue_update,
                levelNotes_update};

        for (int i = 1; i < 15; i++){
            Assert.assertEquals(list.getValueColumnKostLevel(i).toLowerCase(), dataLevel[i-1].toLowerCase(), "Value not match");
        }
    }

    @Then("system display new kost level is not found")
    public void system_display_new_kost_level_is_not_found() throws InterruptedException {
        list.searchKostLevel(levelName);
        Assert.assertFalse(list.listKostLevelIsAppeared());
    }

    @When("user delete kost level")
    public void user_delete_kost_level() throws InterruptedException {
        list.searchKostLevel(levelName);
        if (list.listKostLevelIsAppeared()){
            list.clickOnDeleteKostLevelIcon();
            selenium.waitTillAlertPresent();
            selenium.acceptAlert();
            selenium.hardWait(5);
        }
    }

    @When("user click on page number {string} list kost level")
    public void user_click_on_page_number_list_kost_level(String pageNumber) throws InterruptedException {
        list.clickOnPageNumber(pageNumber);
    }

    @Then("system display list kost level page number {string} is active")
    public void system_display_list_kost_level_page_number_is_active(String pageNumber) {
        Assert.assertTrue(list.pageNumberButtonIsActive(pageNumber).contains("active"), "Button is not active");
    }
}
