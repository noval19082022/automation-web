package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.CommonPO;
import utilities.ThreadManager;

public class CommonSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private CommonPO common = new CommonPO(driver);

    @Then("System display alert message {string}")
    public void system_display_alert_message(String message) {
        Assert.assertTrue(common.getMessageAlert().contains(message), "Message alert not contains " + message);
    }

    @Then("System display danger alert message {string}")
    public void system_display_danger_alert_message(String message) {
        Assert.assertTrue(common.getMessageDangerAlert().contains(message), "Danger alert message not contains " + message);
    }

    @Then("System display popup message {string}")
    public void system_display_popup_message(String message) throws InterruptedException {
        Assert.assertEquals(common.getPopupMessage(), message, "Popup message not match");
    }
}
