package steps.mamikosAdmin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.InvoiceDetailFeePO;
import pageobjects.backoffice.SearchInvoicePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.List;

public class SearchInvoiceSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JavaHelpers java = new JavaHelpers();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private SearchInvoicePO invoice = new SearchInvoicePO(driver);
    private InvoiceDetailFeePO invoiceDetail = new InvoiceDetailFeePO(driver);

    String owner = "src/test/resources/testdata/mamikos/tenant-engagement-owner.properties";
    String settlementKostPrice = JavaHelpers.getPropertyValue(owner, "settlementInvoiceKostPrice_" + Constants.ENV);

    @When("admin search invoice with {string} value {string}")
    public void admin_search_invoice_with_value(String searchBy, String value) throws InterruptedException {
        invoice.setSearchBy(searchBy);
        invoice.setSearchByValue(value);
    }

    @When("admin clicks on invoice details first index")
    public void admin_clicks_on_invoice_details_first_index() throws InterruptedException {
        invoice.clickOnInvoiceDetailFeeFirstIndex();
    }

    @When("admin clicks on invoice details second index")
    public void admin_clicks_on_invoice_details_second_index() throws InterruptedException {
        invoice.clickOnInvoiceDetailFeeSecondIndex();
    }

    @When("admin clicks on dp invoice details first index")
    public void admin_clicks_on_dp_invoice_details_first_index() throws InterruptedException {
        invoice.clickOnInvoiceDPFirstType();
    }

    @When("admin clicks on invoice settlement details")
    public void admin_clicks_on_invoice_settlement_details() throws InterruptedException {
        invoice.clickOnInvoiceSettlementFirstType();
    }

    @When("admin adds new fee with type {string} name {string} cost {string}")
    public void admin_adds_new_fee_with_type_name_cost(String typeFee, String feeName, String feeCost) throws InterruptedException {
        invoiceDetail.clickOnBtnAddFee();
        invoiceDetail.setDropFeeType(typeFee);
        invoiceDetail.setFeeName(feeName);
        invoiceDetail.setInpFeeAmount(feeCost);
        invoiceDetail.clickOnBtnAddAdditionalFee();
        while (!invoiceDetail.isSuccessBoxVisible()) {
            selenium.hardWait(1);
        }
    }

    @When("^admin deletes additional other price with name below :$")
    public void admin_deletes_additional_other_price_with_name(List<String> otherPriceName) throws InterruptedException {
        for (String s : otherPriceName) {
            invoiceDetail.deleteAdditionalOtherPrice(s);
        }
    }

    @Then("admin can sees invoice details page")
    public void admin_can_sees_invoice_details_page() {
        Assert.assertEquals(invoiceDetail.getPageTitle(), "Invoice Detail Fee");
    }

    @Then("admin can sees {string} on invoice details page")
    public void admin_can_sees_invoice_number(String invoiceEl) {
        Assert.assertTrue(invoiceDetail.isInvoiceDetailsElementVisible(invoiceEl));
    }

    @Then("admin can sees additional price with name {string}")
    public void admin_can_sees_additional_price_with_name(String otherPriceName) {
        Assert.assertEquals(invoiceDetail.getOtherPriceName(), otherPriceName);
    }

    @Then("admin can sees total cost is basic amount + deposit fee + additional fee + admin fee")
    public void admin_can_sees_total_cost_is_basic_amount_deposit_fee_additional_fee_admin_fee() {
        Integer totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        Integer deposit = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Invoice Deposit Fee").split(",", 2)[0]);
        Integer basicAmount = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Basic Amount").split(",", 2)[0]);
        Integer otherPrice = invoiceDetail.getOtherPriceNumber("Biaya Tetap");
        Integer adminFee = invoiceDetail.getOtherPriceNumber("Admin");
        Integer totalToAssert = deposit + basicAmount + otherPrice + adminFee;

        Assert.assertEquals(totalCost, totalToAssert, "Is not match");
    }

    @Then("admin can sees total cost is basic amount + deposit fee + admin fee")
    public void admin_can_sees_total_cost_is_basic_amount_deposit_fee_admin_fee() {
        Integer totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        Integer deposit = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Invoice Deposit Fee").split(",", 2)[0]);
        Integer basicAmount = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Basic Amount").split(",", 2)[0]);
        Integer adminFee = invoiceDetail.getOtherPriceNumber("Admin");
        Integer totalToAssert = deposit + basicAmount + adminFee;

        Assert.assertEquals(totalCost, totalToAssert, "Is not match");
    }

    @Then("admin can sees total cost is basic amount + biaya tetap + admin fee")
    public void admin_can_sees_total_cost_is_basic_amount_biaya_tetap_admin_fee() {
        Integer totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        Integer otherPrice = invoiceDetail.getOtherPriceNumber("Biaya Tetap");
        Integer adminFee = invoiceDetail.getOtherPriceNumber("Admin");
        Integer basicAmount = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Basic Amount").split(",", 2)[0]);
        Integer totalToAssert = otherPrice + adminFee + basicAmount;

        Assert.assertEquals(totalCost, totalToAssert);
    }

    @Then("admin can sees total cost is basic amount + biaya tetap + biaya lainnya")
    public void admin_can_sees_total_cost_is_basic_amount_biaya_tetap_biaya_lainnya() {
        Integer totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        Integer otherPrice = invoiceDetail.getOtherPriceNumber("Biaya Tetap");
        Integer otherPriceAdditional = invoiceDetail.getOtherPriceNumber("Biaya Lainnya");
        Integer basicAmount = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Basic Amount").split(",", 2)[0]);
        Integer totalToAssert = otherPrice + basicAmount + otherPriceAdditional;

        Assert.assertEquals(totalCost, totalToAssert);
    }

    @Then("admin can sees total cost is basic amount + biaya lainnya + admin fee")
    public void admin_can_sees_total_cost_is_basic_amount_biaya_lainnya_admin_fee() {
        Integer totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        Integer otherPrice = invoiceDetail.getOtherPriceNumber("Biaya Lainnya");
        Integer adminFee = invoiceDetail.getOtherPriceNumber("Admin");
        Integer basicAmount = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Basic Amount").split(",", 2)[0]);
        Integer totalToAssert = otherPrice + adminFee + basicAmount;

        Assert.assertEquals(totalCost, totalToAssert);
    }

    @Then("admin can sees total cost is basic amount + biaya tetap + biaya lainnya + admin fee")
    public void admin_can_sees_total_cost_is_basic_amount_biaya_tetap_biaya_lainnya_admin_fee() {
        Integer totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        Integer otherPriceFixed = invoiceDetail.getOtherPriceNumber("Biaya Tetap");
        Integer otherPriceAdditional = invoiceDetail.getOtherPriceNumber("Biaya Lainnya");
        Integer adminFee = invoiceDetail.getOtherPriceNumber("Admin");
        Integer basicAmount = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Basic Amount").split(",", 2)[0]);
        Integer totalToAssert = otherPriceAdditional + otherPriceFixed + adminFee + basicAmount;

        Assert.assertEquals(totalCost, totalToAssert);
    }

    @Then("admin can sees total cost is basic amount + biaya tetap + deposit fee + admin fee")
    public void admin_can_sees_total_cost_is_basic_amount_biaya_tetap_deposit_fee_admin_fee() {
        int totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        int depositFee = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Invoice Deposit Fee").split(",", 2)[0]);
        int adminFee = invoiceDetail.getOtherPriceNumber("Admin");
        int basicAmount = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Basic Amount").split(",", 2)[0]);
        int totalToAssert = depositFee + adminFee + basicAmount;

        Assert.assertEquals(totalCost, totalToAssert);
    }

    @Then("admin edits basic amount and verify basic amount not changed")
    public void admin_edits_basic_amount_and_verify_the_changed() throws InterruptedException {
        int basicAmountBeforeEdit = JavaHelpers.extractNumber(invoiceDetail.getBasicAmountText().split(",", 2)[0]);
        invoiceDetail.editBasicAmount(basicAmountBeforeEdit + 5);
        invoice.clickOnInvoiceDetailFeeFirstIndex();
        int basicAmountAfterEdit = JavaHelpers.extractNumber(invoiceDetail.getBasicAmountText().split(",", 2)[0]);

        Assert.assertEquals(basicAmountBeforeEdit, basicAmountAfterEdit);
    }

    @Then("admin changes DP basic amount and verify total amount change on settlement invoice for tenant {string}")
    public void admin_changes_DP_basic_amount_and_verify_total_amount_change_on_settlement_invoice(String tenantPhone) throws InterruptedException {
        int basicAmountBeforeEdit = JavaHelpers.extractNumber(invoiceDetail.getBasicAmountText().split(",", 2)[0]);
        invoiceDetail.editBasicAmount(basicAmountBeforeEdit + 10000);
        invoice.setSearchBy("Renter Phone Number");
        invoice.setSearchByValue(tenantPhone);
        int totalAmountSettlement = JavaHelpers.extractNumber(invoice.getTotalAmount(1));
        int totalAmountDP = JavaHelpers.extractNumber(invoice.getTotalAmount(2)) - 1000;

        Assert.assertEquals(totalAmountSettlement, Integer.parseInt(settlementKostPrice) - totalAmountDP);
    }

    @Then("admin edits basic amount and verify basic amount is changed for tenant {string}")
    public void admin_edits_basic_amount_and_verify_basic_amount_is_changed(String tenantPhone) throws InterruptedException {
        int basicAmountBeforeEdit = JavaHelpers.extractNumber(invoiceDetail.getBasicAmountText().split(",", 2)[0]);
        invoiceDetail.editBasicAmount(basicAmountBeforeEdit + 10000);
        invoice.setSearchBy("Renter Phone Number");
        invoice.setSearchByValue(tenantPhone);
        invoice.clickOnInvoiceDetailFeeFirstIndex();
        int basicAmountAfterEdit = JavaHelpers.extractNumber(invoiceDetail.getBasicAmountText().split(",", 2)[0]);

        Assert.assertTrue(basicAmountBeforeEdit < basicAmountAfterEdit);
    }

    @Then("admin can sees total cost is basic amount + admin fee")
    public void admin_can_sees_total_cost_is_basic_amount_admin_fee() throws InterruptedException {
        int totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        int basicAmount = JavaHelpers.extractNumber(invoiceDetail.getBasicAmountText().split(",", 2)[0]);
        int adminFee = invoiceDetail.getOtherPriceNumber("Admin");
        Assert.assertEquals(totalCost, basicAmount + adminFee);
    }

    @Then("admin can sees total cost is basic amount + deposit fee + biaya tetap + admin fee")
    public void admin_can_sees_total_cost_is_basic_amount_deposit_fee_biaya_tetap_admin_fee() {
        int totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        int basicAmount = JavaHelpers.extractNumber(invoiceDetail.getBasicAmountText().split(",", 2)[0]);
        int adminFee = invoiceDetail.getOtherPriceNumber("Admin");
        int depositFee = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Invoice Deposit Fee").split(",", 2)[0]);
        int otherPriceFixed = invoiceDetail.getOtherPriceNumber("Biaya Tetap");
        Assert.assertEquals(totalCost, basicAmount + adminFee + depositFee + otherPriceFixed);
    }

    @Then("admin can sees total cost is basic amount + deposit fee")
    public void admin_can_sees_total_cost_is_basic_amount_deposit_fee() {
        int totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        int deposit = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Invoice Deposit Fee").split(",", 2)[0]);
        int basicAmount = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Basic Amount").split(",", 2)[0]);
        int additionalPrice = JavaHelpers.extractNumber(invoiceDetail.getAdditionalPriceNumber("Cuci Kaki"));
        int totalToAssert = deposit + basicAmount + additionalPrice;
        Assert.assertEquals(totalCost, totalToAssert, "Is not match");
    }

    @When("admin adds new fee with type {string} index {string}")
    public void admin_adds_new_fee_with_type_index(String typeFee, String index) throws InterruptedException {
        invoiceDetail.clickOnBtnAddFee();
        invoiceDetail.setDropFeeType(typeFee);
        invoiceDetail.setDropAddOn(index);
        invoiceDetail.clickOnBtnAddAdditionalFee();
        while (!invoiceDetail.isSuccessBoxVisible()) {
            selenium.hardWait(1);
        }
    }

    @When("admin master adds new fee with type {string} text value {string}")
    public void admin_master_adds_new_fee_with_type_text_value(String type, String dropDownTextValue) throws InterruptedException {
        invoiceDetail.clickOnBtnAddFee();
        invoiceDetail.setDropFeeType(type);
        invoiceDetail.setDropAddOnByText(dropDownTextValue);
        invoiceDetail.clickOnBtnAddAdditionalFee();
        while (!invoiceDetail.isSuccessBoxVisible()) {
            selenium.hardWait(1);
        }
    }

    @Then("admin can sees add ons successfully added")
    public void admin_can_sees_add_ons_succesfully_added() {
        Assert.assertTrue(invoiceDetail.getCompleteAddAdditionalPriceText().contains("berhasil dibuat"));
        Assert.assertNotNull(invoiceDetail.getAddsOnFeeTypeElementLenght());
    }

    @And("user select {string} on auto extend selection")
    public void user_select_on_auto_extend_selection(String item) {
        invoice.selectOnAutoExtendSelection(item);
        Assert.assertEquals(invoice.getAutoExtendSelectionText(item), item, "Auto extend selection is not equal to " + item);
    }

    @And("user click search invoice button on search invoice admin page")
    public void user_click_search_invoice_button_on_search_invoice_admin_page() throws InterruptedException {
        invoice.clickOnSearchInvoiceButton();
    }

    @Then("user verify search invoice results have auto extend {string}, {string}")
    public void user_verify_search_invoice_results_have_auto_extend(String value, String status) {
        for (int i = 0; i < invoice.getResultsElement(value,status).size(); i++) {
            Assert.assertTrue(invoice.isSearchInvoiceResultsHaveAutoExtendCorrectValue(value, status, i), "Auto extend value is not appeared as " + value);
            Assert.assertEquals(invoice.getAutoExtendValueOfSearchInvoiceResults(value, status, i), value, "Auto extend value is not equal to " + value);
        }
    }

    @When("admin adds custom add ons with name {string} price amount {string}")
    public void admin_adds_custom_add_ons_with_name_price_amount(String addOnsName, String price) throws InterruptedException {
        invoiceDetail.addCustomAddOns(addOnsName, price);
    }

    @Then("admins can sees error message")
    public void admins_can_sees_error_message() {
        Assert.assertEquals(invoiceDetail.getActionResultMessage(), "The cost value must be an integer.", "Message is not fail message");
    }

    @When("admin edit additional fee {string} price amount to {string}")
    public void admin_edit_additional_fee_price_amount_to(String additionalPriceType, String priceAmount) throws InterruptedException {
        invoiceDetail.clickOnEditButton(additionalPriceType);
        invoiceDetail.setInpFeeAmount(priceAmount);
        invoiceDetail.clickOnBtnAddFee();
        invoiceDetail.waitCalloutActionResult();
    }

    @Then("admin master can sees additional success message is {string}")
    public void admin_master_can_sees_additional_success_message_is(String actionMessage) {
        Assert.assertEquals(invoiceDetail.getActionResultMessage(), actionMessage);
    }

    @Then("admin master edits additional fee {string} name to {string}")
    public void admin_master_edits_additional_fee_name_to(String additionalPriceType, String targetName) throws InterruptedException {
        invoiceDetail.clickOnEditButton(additionalPriceType);
        invoiceDetail.editFeeName(targetName);
        invoiceDetail.clickOnBtnAddFee();
        invoiceDetail.waitCalloutActionResult();
    }

    @And("admin clicks on see log button with link value {string}")
    public void admin_clicks_on_see_log_button_with_link(String link) throws InterruptedException {
        invoice.clickOnSeeLogButton(link);
    }

    @Then("admin can not sees price with name {string} in invoice details")
    public void admin_can_not_sees_price_with_name_in_invoice_details(String priceName) throws InterruptedException {
        Assert.assertFalse(invoiceDetail.isPriceWithNameVisible(priceName), priceName + " is still visible");
    }

    @Then("admin can sees add ons price with name {string} price {string}")
    public void admin_can_sees_add_ons_price_with_name_price(String priceName, String price) {
        Assert.assertTrue(invoiceDetail.isPriceWithNameVisibleContractFlow(priceName));
        Assert.assertEquals(String.valueOf(JavaHelpers.extractNumber(invoiceDetail.getAdditionalPriceNumberContractFlow(priceName))), price);
    }

    @Then("admin master can sees add ons price with name {string} price {string} in invoice detail")
    public void admin_can_sees_add_ons_price_with_name_price_in_invoice_detail(String priceName, String price) {
        Assert.assertTrue(invoiceDetail.isPriceWithNameVisibleContractFlow(priceName));
        Assert.assertEquals(String.valueOf(JavaHelpers.extractNumber(invoiceDetail.getAdditionalPriceNumber(priceName))), price);
    }

    @Then("admin can sees total cost is basic amount + deposit fee + biaya tetap")
    public void admin_can_sees_total_cost_is_basic_amount_deposit_fee_biaya_tetap() {
        int totalCost = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Total Amount").split(",", 2)[0]);
        int basicAmount = JavaHelpers.extractNumber(invoiceDetail.getBasicAmountText().split(",", 2)[0]);
        int depositFee = JavaHelpers.extractNumber(invoiceDetail.getInvoiceElementValue("Invoice Deposit Fee").split(",", 2)[0]);
        int otherPriceFixed = invoiceDetail.getOtherPriceNumber("Biaya Tetap");
        Assert.assertEquals(totalCost, basicAmount + depositFee + otherPriceFixed);
    }
}