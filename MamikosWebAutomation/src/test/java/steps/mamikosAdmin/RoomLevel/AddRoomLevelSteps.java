package steps.mamikosAdmin.RoomLevel;

import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikosAdmin.RoomLevel.AddRoomLevelPO;
import pageobjects.mamikosAdmin.RoomLevel.ListRoomLevelPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class AddRoomLevelSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ListRoomLevelPO list = new ListRoomLevelPO(driver);
    private AddRoomLevelPO add = new AddRoomLevelPO(driver);

    //Test Data Kost Level
    private String roomLevel = "src/test/resources/testdata/mamikosadmin/roomLevel.properties";

    //Kost Name
    private String roomLevelName = JavaHelpers.getPropertyValue(roomLevel, "roomLevelName_" + Constants.ENV);

    @When("user update room level with empty level name")
    public void user_update_room_level_with_empty_level_name() throws InterruptedException {
        list.searchRoomLevel(roomLevelName);
        list.clickOnFirstListIconUpdate();
        add.enterLevelName("");
        add.clickOnSaveButton();
    }
}
