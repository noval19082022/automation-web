package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.PropertyPackagePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class PropertyPackageSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private PropertyPackagePO propertyPackage = new PropertyPackagePO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String ownerPhone = JavaHelpers.getPropertyValue(consultantProperty,"ownerPhonePackage_" + Constants.ENV);
    private String ownerName = JavaHelpers.getPropertyValue(consultantProperty,"ownerNamePackage_" + Constants.ENV);
    private String propertyName = JavaHelpers.getPropertyValue(consultantProperty,"propertyNamePackage_" + Constants.ENV);
    private String propertyNameAdded = JavaHelpers.getPropertyValue(consultantProperty,"propertyNamePackageAdded_" + Constants.ENV);
    private String successDelete = JavaHelpers.getPropertyValue(consultantProperty,"successDeleteContract_" + Constants.ENV);
    private String kostLevelGp1 = JavaHelpers.getPropertyValue(consultantProperty,"levelGp1_" + Constants.ENV);
    private String kostLevelGp2 = JavaHelpers.getPropertyValue(consultantProperty,"levelGp2_" + Constants.ENV);
    private String kostLevelGp3 = JavaHelpers.getPropertyValue(consultantProperty,"levelGp3_" + Constants.ENV);
    private String kostLevelReguler = JavaHelpers.getPropertyValue(consultantProperty,"levelReguler_" + Constants.ENV);


    @When("user search contract by owner phone number")
    public void user_search_contract_by_owner_phone_number() throws InterruptedException {
        propertyPackage.clickOnFilter();
        propertyPackage.searchOwner(ownerPhone);
        propertyPackage.clickOnSearchPropertyPackage();
    }

    @When("user search owner property name {string} on property package")
    public void user_search_owner_phone_on_property_package(String propertyName) throws InterruptedException {
        propertyPackage.clickOnFilter();
        propertyPackage.searchOwner(propertyName);
        propertyPackage.clickOnSearchPropertyPackage();
    }

    @When("user click terminate property contract")
    public void user_click_terminate_property_contract() throws InterruptedException {
        String statusPackage;

        selenium.hardWait(3);
        int numberOfList = propertyPackage.getNumberOfList();
        for (int i = 1; i <= numberOfList; i++) {
            statusPackage = propertyPackage.getStatusPropertyPackage(i);
            if(statusPackage.equals("inactive")) {
                propertyPackage.clickOnActionButton(i);
                propertyPackage.clickOnTerminateContract();
            }
        }
    }

    @When("user click add new package button")
    public void user_click_add_new_package_button() throws InterruptedException {
        propertyPackage.clickOnAddNewPackage();
    }

    @When("user input registered owner phone number to validate")
    public void user_input_registered_owner_phone_number_to_validate() throws InterruptedException {
        propertyPackage.setOwnerPhoneField(ownerPhone);
        propertyPackage.clickOnSearch();
    }

    @When("user add data property contract with property level Goldplus one")
    public void user_input_data_property_contract_with_property_level_goldplus_one() throws InterruptedException {
        propertyPackage.selectPropertyName(propertyName);
        propertyPackage.selectPropertyLevelGp1("Mamikos Goldplus 1 PROMO");
        propertyPackage.selectJoinDate();
        propertyPackage.clickOnNextButton();
        propertyPackage.clickOnSaveButton();
    }

    @When("user add data property contract with property level Goldplus two")
    public void user_input_data_property_contract_with_property_level_goldplus_two() throws InterruptedException {
        propertyPackage.selectPropertyName(propertyName);
        propertyPackage.selectPropertyLevelGp2("Mamikos Goldplus 2 PROMO");
        propertyPackage.selectJoinDate();
        propertyPackage.clickOnNextButton();
        propertyPackage.clickOnSaveButton();
    }

    @When("user add data property contract with property level Goldplus three")
    public void user_add_data_property_contract_with_property_level_Goldplus_three() throws InterruptedException {
        propertyPackage.selectPropertyName(propertyName);
        propertyPackage.selectPropertyLevelGp3("Mamikos Goldplus 3 PROMO");
        propertyPackage.selectJoinDate();
        propertyPackage.clickOnNextButton();
        propertyPackage.clickOnSaveButton();
    }

    @When("user assign kost and room level to property level")
    public void user_assign_kost_and_room_level_to_property_level() throws InterruptedException {
        propertyPackage.clickOnAssignButton();
    }

    @Then("user verify that kost Level is {string}")
    public void user_verify_that_kost_Level_is(String kosLevel) throws InterruptedException {
        if (kosLevel.contains("GoldPlus 1")) {
            Assert.assertEquals(propertyPackage.getKosLevelLabel(), kostLevelGp1, "Kost Level Not " + kostLevelGp1);
        }else if (kosLevel.contains("GoldPlus 2")){
            Assert.assertEquals(propertyPackage.getKosLevelLabel(), kostLevelGp2, "Kost Level Not " + kostLevelGp2);
        }else if (kosLevel.contains("GoldPlus 3")){
            Assert.assertEquals(propertyPackage.getKosLevelLabel(), kostLevelGp3, "Kost Level Not " + kostLevelGp3);
        }else{
            Assert.assertEquals(propertyPackage.getKosLevelLabel(), kostLevelReguler, "Kost Level Not " + kostLevelReguler);
        }
    }

    @When("user input {string} on owner phone number to validate")
    public void user_input_on_owner_phone_number_to_validate(String phoneNumber) throws InterruptedException {
        propertyPackage.setOwnerPhoneField(phoneNumber);
        propertyPackage.clickOnSearch();
    }

    @Then("user verify there is warning message {string}")
    public void user_verify_there_is_warning_message(String message) {
        Assert.assertEquals(propertyPackage.getWarningMessage(), message, "Warning Message Not "+ message);
    }

    @When("user upgrade property contract")
    public void user_upgrade_property_contract() throws InterruptedException {
        String statusPackage;

        selenium.hardWait(7);
        int numberOfList = propertyPackage.getNumberOfList();
        for (int i = 1; i <= numberOfList; i++) {
            statusPackage = propertyPackage.getStatusPropertyPackage(i);
            if(statusPackage.equals("inactive")) {
                propertyPackage.clickOnActionButton(i);
            }
        }
        propertyPackage.clickOnUpdateContract();
        propertyPackage.clickOnUpgradeContract();
    }

    @When("user click see detail property contract")
    public void user_click_see_detail_property_contract() throws InterruptedException {
        propertyPackage.clickOnDetailPackage();
    }

    @When("user unassign goldplus level")
    public void user_unassign_goldplus_level() throws InterruptedException {
        propertyPackage.clickOnUnassignButton();
    }

    @When("user search contract by owner name {string}")
    public void user_search_contract_by_owner_name(String search) throws InterruptedException {
        if(search.equals("valid")){
            propertyPackage.searchOwnerByName(ownerName);
        } else {
            propertyPackage.searchOwnerByName(search);
        }
        propertyPackage.clickOnSearchPropertyPackage();
    }

    @When("user search contract by owner phone")
    public void user_search_contract_by_owner_phone() throws InterruptedException {
        propertyPackage.searchOwnerByPhone(ownerPhone);
        propertyPackage.clickOnSearchPropertyPackage();
    }

    @When("user search contract by property name")
    public void user_search_contract_by_property_name() throws InterruptedException {
        propertyPackage.searchByProperty(propertyName);
        propertyPackage.clickOnSearchPropertyPackage();
    }

    @Then("system displays list of property package with correct owner name")
    public void system_displays_list_of_property_package_with_owner_name() throws InterruptedException {
        propertyPackage.getOwnerNamePropertyPackage();
        propertyPackage.getOwnerPhonePropertyPackage();
        int numberOfList = propertyPackage.getNumberOfList();;
        for (int i = 1; i <= numberOfList; i++) {
            Assert.assertTrue(propertyPackage.getOwnerNamePropertyPackage().contains(ownerName), "Owner Not  " + ownerName);
        }
    }

    @Then("system displays list of property package with correct owner phone")
    public void system_displays_list_of_property_package_with_correct_owner_phone() throws InterruptedException {
        propertyPackage.getOwnerPhonePropertyPackage();
        int numberOfList = propertyPackage.getNumberOfList();;
        for (int i = 1; i <= numberOfList; i++) {
            Assert.assertEquals(propertyPackage.getOwnerPhonePropertyPackage(), ownerPhone, "Owner Not "+ ownerPhone);
        }
    }

    @Then("system displays list of property package with correct property name")
    public void system_displays_list_of_property_package_with_coorect_property_name() throws InterruptedException {
        propertyPackage.getPropertyName();
        int numberOfList = propertyPackage.getNumberOfList();;
        for (int i = 1; i <= numberOfList; i++) {
            Assert.assertTrue(propertyPackage.getPropertyName().contains(propertyName), "Property not contains " + propertyName);
        }
    }

    @When("user filter property package by status {string}")
    public void user_filter_property_package_by_status(String status) throws InterruptedException {
        propertyPackage.clickOnFilterStatus(status);
    }

    @Then("system displays list of property package with contract status {string}")
    public void system_displays_list_of_property_package_with_contract_status(String status) throws InterruptedException {
        int numberOfList = propertyPackage.getNumberOfList();;
        for (int i = 1; i <= numberOfList; i++) {
            Assert.assertEquals(propertyPackage.getStatusPropertyPackage(i), status, "Contract status Not "+ status);
        }
    }

    @When("user filter property package by kost level {string}")
    public void user_filter_property_package_by_kost_level(String kostLevel) throws InterruptedException {
        propertyPackage.clickOnFilterKostLevel(kostLevel);
    }

    @Then("system displays list of property package with kost level {string}")
    public void system_displays_list_of_property_package_with_kost_level(String kostLevel) throws InterruptedException {
        propertyPackage.getLevelPropertyPackage();
        int numberOfList = propertyPackage.getNumberOfList();;
        for (int i = 1; i <= numberOfList; i++) {
            Assert.assertEquals(propertyPackage.getLevelPropertyPackage(), kostLevel, "Kost Level Not "+ kostLevel);
        }
    }


    @When("user add new property")
    public void user_add_new_property() throws InterruptedException {
        String statusPackage;

        selenium.hardWait(7);
        int numberOfList = propertyPackage.getNumberOfList();
        for (int i = 1; i <= numberOfList; i++) {
            statusPackage = propertyPackage.getStatusPropertyPackage(i);
            if(statusPackage.equals("inactive")) {
                propertyPackage.clickOnActionButton(i);
            }
        }
        propertyPackage.clickOnAddNewProperty();
        propertyPackage.selectProperty(propertyNameAdded);
        propertyPackage.clickOnSaveNewPropertyButton();
        propertyPackage.clickOnSaveButton();
    }

    @When("user select property that have been added")
    public void user_select_property_that_have_been_added() throws InterruptedException {
        propertyPackage.clickOnPropertyListOnDetail(propertyNameAdded);
    }

    @When("user remove one property")
    public void user_remove_one_property() throws InterruptedException {
        String statusPackage;

        selenium.hardWait(7);
        int numberOfList = propertyPackage.getNumberOfList();
        for (int i = 1; i <= numberOfList; i++) {
            statusPackage = propertyPackage.getStatusPropertyPackage(i);
            if(statusPackage.equals("inactive")) {
                propertyPackage.clickOnActionButton(i);
            }
        }
        propertyPackage.clickOnRemoveProperty();
        propertyPackage.clickOnPropertyToRemove(propertyNameAdded);
        propertyPackage.clickOnRemovePropertyButton();
    }

    @Then("user verify property does not appear on the list")
    public void user_verify_property_does_not_appear_on_the_list() throws InterruptedException {
            Assert.assertFalse(propertyPackage.getPropertyName().contains(propertyNameAdded), "Property contains" + propertyNameAdded);

    }

    @When("user click delete contract")
    public void user_click_delete_contract() throws InterruptedException {
        String statusPackage;

        selenium.hardWait(7);
        int numberOfList = propertyPackage.getNumberOfList();
        for (int i = 1; i <= numberOfList; i++) {
            statusPackage = propertyPackage.getStatusPropertyPackage(i);
            if(statusPackage.equals("inactive")) {
                propertyPackage.clickOnActionButton(i);
            }
        }
        propertyPackage.clickOnDeleteContract();
    }

    @Then("user verify success delete contract appear")
    public void user_verify_success_delete_contract_appear() throws InterruptedException {
        Assert.assertEquals(propertyPackage.getSuccessMessage(), successDelete, "Message Not "+ successDelete);
    }

    @Then("no list property package appear")
    public void no_list_propeety_package_appear() throws InterruptedException {
        Assert.assertEquals(propertyPackage.getNumberOfList(),1);
    }
}
