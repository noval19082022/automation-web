package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.SalesMotionAdminPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class SalesMotionAdminSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SalesMotionAdminPO salesMotion = new SalesMotionAdminPO(driver);
    private JavaHelpers java = new JavaHelpers();

    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String taskName = JavaHelpers.getPropertyValue(consultantProperty,"taskName_" + Constants.ENV);
    private String objective = JavaHelpers.getPropertyValue(consultantProperty,"objective_" + Constants.ENV);
    private String tnc = JavaHelpers.getPropertyValue(consultantProperty,"tnc_" + Constants.ENV);
    private String requirement = JavaHelpers.getPropertyValue(consultantProperty,"requirement_" + Constants.ENV);
    private String benefit = JavaHelpers.getPropertyValue(consultantProperty,"benefit_" + Constants.ENV);
    private String participant = JavaHelpers.getPropertyValue(consultantProperty,"participant_" + Constants.ENV);
    private String url = JavaHelpers.getPropertyValue(consultantProperty,"url_" + Constants.ENV);
    private String editSalesMotion = JavaHelpers.getPropertyValue(consultantProperty,"editSalesMotionName_" + Constants.ENV);
    private String newAssignee = JavaHelpers.getPropertyValue(consultantProperty,"newAssignee_" + Constants.ENV);
    private String updatedBy = JavaHelpers.getPropertyValue(consultantProperty,"updatedBy_" + Constants.ENV);

    @When("user click Create New Sales Motion")
    public void user_click_Create_New_Sales_Motion() throws InterruptedException {
        salesMotion.clickOnCreateSalesMotion();
    }

    @When("user submit task sales motion complete")
    public void user_submit_data_sales_motion_complete() throws InterruptedException, ParseException {
        salesMotion.setTaskName(taskName);
        salesMotion.clickOnTypeTask();
        salesMotion.clickOnDivision();
        String today = java.updateTime("yyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        salesMotion.selectStartDateToday(today);
        String tomorrowDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        salesMotion.selectDueDateTomorrow(tomorrowDate);
        salesMotion.setObjective(objective);
        salesMotion.setTermCondition(tnc);
        salesMotion.setRequirement(requirement);
        salesMotion.setBenefit(benefit);
        salesMotion.setParticipant(participant);
        salesMotion.setUrlLink(url);
        salesMotion.clickOnSubmitSalesMotion();
    }

    @When("user search sales motion")
    public void user_search_sales_motion() throws InterruptedException {
        salesMotion.searchSalesMotion(editSalesMotion);
    }

    @When("user set sales motion to not active and default assignee")
    public void user_set_sales_motion_to_not_active_throws() throws InterruptedException {
        String statusOnList = salesMotion.getStatusOnListSalesMotion();
        if(statusOnList.equals("Active")) {
            salesMotion.clickOnDeactiveButton();
            salesMotion.clickOnSeeDetailButton();
            String assignee = salesMotion.getAssignee();
            if(assignee.equals("Yudha")) {
                salesMotion.setAssigneeOtherConsultant();
            }
        }
        else{
            salesMotion.clickOnSeeDetailButton();
            String assignee = salesMotion.getAssignee();
            if(assignee.equals("Yudha")) {
                salesMotion.setAssigneeOtherConsultant();
            }
        }
    }

    @When("user submit consultant to handle task sales motion")
    public void user_submit_consultant_to_handle_task_sales_motion() throws InterruptedException {
        salesMotion.clickOnConsultant();
        salesMotion.clickOnSubmitAssignee();
    }

    @Then("user can see {string}")
    public void user_can_see(String text) {
        Assert.assertEquals(salesMotion.getDetailPage(), text, text + " Not Present");
    }

    @When("user click edit sales motion")
    public void user_click_edit_sales_motion() throws InterruptedException {
        salesMotion.clickOnEditSalesMotion();
    }

    @When("user activate sales motion from edit page")
    public void user_activate_sales_motion_from_edit_page() throws InterruptedException {
        salesMotion.clickUpdateButton();
    }

    @Then("user verify sales motion status is {string}")
    public void user_verify_sales_motion_status_is(String status) throws InterruptedException {
        Assert.assertEquals(salesMotion.getStatusOnDetailSalesMotion(), status, "Sales Motion Not "+ status);
    }

    @When("user activate sales motion from list page")
    public void user_activate_sales_motion_from_list_page() throws InterruptedException, ParseException {
        salesMotion.clickOnActivateButton();
        String tomorrowDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        salesMotion.selectDueDateTomorrow(tomorrowDate);
        salesMotion.clickOnSubmitDueDate();
    }

    @Then("user verify sales motion status on list is {string}")
    public void user_verify_sales_motion_status_on_list_is(String status) {
        Assert.assertEquals(salesMotion.getStatusOnListSalesMotion(), status, "Sales Motion Not "+ status);
    }

    @When("user click detail sales motion")
    public void user_click_detail_sales_motion() throws InterruptedException {
        salesMotion.clickOnSeeDetailButton();
    }

    @When("user edit assignee to other consultant")
    public void user_edit_assignee_to_other_consultant() throws InterruptedException {
        salesMotion.setAssigneeOtherConsultant();
    }

    @Then("user verify new assignee consultant appear on detail sales motion")
    public void user_verify_new_assignee_consultant_appear_on_detail_sales_motion() throws InterruptedException {
        Assert.assertEquals(salesMotion.getAssignee(), newAssignee, "Sales Motion Assignee Not "+ newAssignee);
    }

    @Then("user verify sales motion updated by correct consultant")
    public void user_verify_sales_motion_updated_by_correct_consultant() throws InterruptedException {
        Assert.assertEquals(salesMotion.getUpdatedBy(), updatedBy, "Sales Motion Not updated by "+ updatedBy);
    }

    @When("user set sales motion to not active from detail")
    public void user_set_sales_motion_to_not_active_from_detail() throws InterruptedException {
        String statusOnList = salesMotion.getStatusOnListSalesMotion();
        if(statusOnList.equals("Active"))
        {
            salesMotion.clickOnSeeDetailButton();
            salesMotion.clickOnEditSalesMotionDetail();
            salesMotion.clickUpdateButton();
        }
    }

}
