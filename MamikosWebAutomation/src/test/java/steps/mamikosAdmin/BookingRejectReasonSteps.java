package steps.mamikosAdmin;

import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikosAdmin.BookingRejectReasonPO;
import utilities.ThreadManager;

public class BookingRejectReasonSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private BookingRejectReasonPO bookingReason = new BookingRejectReasonPO(driver);

    @When("user click on first radio button in reject reason")
    public void user_click_on_first_radio_button_in_reject_reason() throws InterruptedException {
        bookingReason.clickOnFirstRadioButton();
    }

    @When("user click reject BBK button in booking reject reason")
    public void user_click_reject_BBK_button_in_booking_reject_reason() throws InterruptedException {
        bookingReason.clickOnRejectButton();
    }
}
