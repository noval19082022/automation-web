package steps.mamikosAdmin.SanjuniPero;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.SanjuniPero.SanjuniPeroPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class SanjuniPeroSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SanjuniPeroPO sanjunipero = new SanjuniPeroPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);


    @When("user access menu sanjunipero")
    public void user_access_menu_sanjunipero() {
        sanjunipero.accessMenuSanjunipero();
    }
    @And("user choose parent")
    public void user_choose_parent () throws InterruptedException {
        sanjunipero.chooseParent();
    }
    @And("user click add new parent")
    public void user_click_add_new_parent () throws InterruptedException {
        sanjunipero.addNewParent();
    }
    @And("user click biaya sewa")
    public void user_click_biaya_sewa () throws InterruptedException {
        sanjunipero.biayaSewa();
    }
    @And("user choose 3 biaya sewa {string} {string} {string}")
    public void user_choose_3_biaya_sewa (String Daily, String Weekly, String Monthly) throws InterruptedException {
        switch (Daily) {
            case "Daily":
                sanjunipero.chooseBiayaSewa(Daily);
                break;
            case "Weekly":
                sanjunipero.chooseBiayaSewa(Weekly);
                break;
            case "Monthly":
                sanjunipero.chooseBiayaSewa(Monthly);
                break;
        }
    }
    @And("user fills {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}")
    public void user_fills_mandetory (String slug, String typeKost, String biayaSewa, String tittleTag, String tittleHeader, String subtittleHeader, String facilityTag, String faq, String faqAnswer) throws InterruptedException {
        sanjunipero.fillsMandetory(slug, typeKost, biayaSewa, tittleTag, tittleHeader, subtittleHeader, facilityTag, faq, faqAnswer);
    }
    @And("user click checkbox Active Sanjunipero")
    public void click_checkbox_active_sanjunipero () throws InterruptedException {
        sanjunipero.checkboxActiveSanjunipero();
    }
    @And("user click save button Sanjunipero")
    public void click_save_button_sanjunipero () throws InterruptedException {
        sanjunipero.saveButtonSanjunipero();
    }
    @And("user check slug name already exist")
    public void user_check_slug_name_already_exist () throws InterruptedException {
        sanjunipero.checkSlugNameAlreadyExist();
    }
    @And("user fills mandatory already exist {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}")
    public void user_fills_mandatory_already_exist (String slug, String typeKost, String biayaSewa, String tittleTag, String tittleHeader, String subtittleHeader, String facilityTag, String faq, String faqAnswer) throws InterruptedException {
        sanjunipero.fillsMandatoryAlreadyExist(slug, typeKost, biayaSewa, tittleTag, tittleHeader, subtittleHeader, facilityTag, faq, faqAnswer);
    }
    @And("user click preview on action")
    public void user_click_action_preview () throws InterruptedException {
        sanjunipero.clickActionPreview();
    }
    @And("user click semua tipe kost")
    public void user_click_semua_tipe_kost () throws InterruptedException {
        sanjunipero.clickSemuaTipeKost();
    }
    @And("user click deactive on action")
    public void user_click_deactive_action () throws InterruptedException {
        sanjunipero.clickDeactiveAction();
    }
    @And("user click enable on action")
    public void user_click_enable_action () throws InterruptedException {
        sanjunipero.clickEnableAction();
    }
    @Then("user view three gender")
    public void user_view_three_gender () {
        Assert.assertTrue(sanjunipero.tipeKost(), "Captcha box is not present");
    }

    @Then("user view room list")
    public void user_view_room_list () {
        Assert.assertTrue(sanjunipero.roomList(), "Room list is not present");
    }

    @Then("user view cost rent has been selected")
    public void user_view_cost_rent () {
        Assert.assertTrue(sanjunipero.costRent(), "Room list is not present");
    }

    @Then("user view filter data kost on landing page")
    public void user_view_filter_data_kost () {
        Assert.assertTrue(sanjunipero.viewFilterDataKost(), "Captcha box is not present");
    }
    @Then("user view deactive type page")
    public void user_view_deactive_type_page () {
        Assert.assertTrue(sanjunipero.deactiveParentSanjunipero(), "Captcha box is not present");
    }
    @Then("user view enable type page")
    public void user_view_enable_type_page () throws InterruptedException {
        Assert.assertTrue(sanjunipero.enableParentSanjunipero(), "Captcha box is not present");
    }
}
