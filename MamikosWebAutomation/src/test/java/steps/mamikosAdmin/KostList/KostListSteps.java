package steps.mamikosAdmin.KostList;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.KostListPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class KostListSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private KostListPO list = new KostListPO(driver);

    //Test Data Kost Level
    private String kostList = "src/test/resources/testdata/mamikosadmin/KostList.properties";
    private String kostLevel = "src/test/resources/testdata/mamikosadmin/kostLevel.properties";

    //Kost List
    private String kostName = JavaHelpers.getPropertyValue(kostList, "kostName_" + Constants.ENV);
    private String kostPMAN = JavaHelpers.getPropertyValue(kostList, "kostPMAN_" + Constants.ENV);
    private String regularLevel = JavaHelpers.getPropertyValue(kostLevel, "kosLevelRegular_" + Constants.ENV);

    @When("user search by kost name on kost list page")
    public void user_search_by_kost_name_on_kost_list_page() throws InterruptedException {
        list.searchKostLevel(kostName);
    }

    @Then("system display content page kost list")
    public void system_display_content_page_kost_list() {
        Assert.assertTrue(list.uploadCSVButtonIsAppeared(), "Element not appeared");
        Assert.assertTrue(list.kostNameEditTextIsAppeared(), "Element not appeared");
        Assert.assertTrue(list.ownerNameEditTextIsAppeared(), "Element not appeared");
        Assert.assertTrue(list.ownerPhoneNumberEditTextIsAppeared(), "Element not appeared");
        Assert.assertTrue(list.allLevelDropdownIsAppeared(), "Element not appeared");
        Assert.assertTrue(list.searchButtonIsAppeared(), "Element not appeared");
        String[] column = {"ID" , "Kost Name", "Owner Name", "Owner Phone Number", "Level", "Flag", "Last Update", "Actions"};

        for (int i=0; i<8; i++){
            Assert.assertEquals(list.getColumnName(i+1), column[i], "Column name not match");
        }
    }

    @Then("system display kost list contains name")
    public void system_display_kost_list_contains() {
        int count = list.getNumberOfList();

        for (int i=0; i< count; i++){
            Assert.assertTrue((list.getKostName(i+1)).toLowerCase().contains(kostName.toLowerCase()));
        }
    }

    @When("user search kost pman")
    public void user_search_kost_pman() throws InterruptedException {
        list.searchKostLevel(kostPMAN);
    }

    @When("user click edit kost level")
    public void user_click_edit_kost_level() throws InterruptedException {
        list.clickEditKosLevel();
    }

    @When("user click kost list actions {string}")
    public void user_click_kost_list_actions(String button) throws InterruptedException {
        if (button.equalsIgnoreCase("Edit Kost Level")){
            list.clickEditKosLevel();
        } else if (button.equalsIgnoreCase("Room List")){
            list.clickRoomList();
        } else if (button.equalsIgnoreCase("History")){
            list.clickHistory();
        } else {
            System.out.println("button "+button+" doesn't exist");
        }
    }

    @When("change level to {string}")
    public void change_level_to(String level) throws InterruptedException {
        if (level.equalsIgnoreCase("Reguler")){
            list.selectKostLevel(regularLevel);
        } else {
            list.selectKostLevel(level);
        }
    }

    @When("user save edit kost level")
    public void user_save_edit_kost_level() throws InterruptedException {
        list.clickSave();
    }

    @When("user cancel pop up assign room level")
    public void user_cancel_pop_up_assign_room_level() throws InterruptedException {
        list.cancelPopUpAssignRoomLevel();
    }

    @Then("level should be {string}")
    public void level_should_be(String level) {
        if (level.equalsIgnoreCase("Reguler") && Constants.ENV.equalsIgnoreCase("prod")){
            Assert.assertEquals(list.getLevelName(),"Regular","level name not equals");
        } else {
            Assert.assertEquals(list.getLevelName(),level,"level name not equals");
        }
    }
}
