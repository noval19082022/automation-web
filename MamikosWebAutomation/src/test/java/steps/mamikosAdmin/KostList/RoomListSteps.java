package steps.mamikosAdmin.KostList;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.RoomListPO;
import utilities.ThreadManager;

public class RoomListSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private RoomListPO room = new RoomListPO(driver);

    @When("user search room by room name {string}")
    public void user_search_room_by_room_name(String roomName) throws InterruptedException {
        room.searchRoom(roomName);
        room.clickSearchRoomButton();
    }

    @When("user click room list action {string}")
    public void user_click_room_list_action(String button) throws InterruptedException {
        if (button.equalsIgnoreCase("Edit Room Level")){
            room.clickEditRoomLevel();
        } else if (button.equalsIgnoreCase("History")){
            room.clickHistory();
        } else {
            System.out.println("button "+button+" doesn't exist");
        }
    }

    @When("user click Assign All button")
    public void user_click_Assign_All_button() throws InterruptedException {
        room.clickAssignAll();
    }

    @Then("all level should be change to {string}")
    public void all_level_should_be_change_to(String level) {
        Assert.assertTrue(room.allRoomLevelNameEqual(level));
    }

    @When("user save edit level popup")
    public void user_save_edit_level_popup() throws InterruptedException {
        room.clickSubmitAssignPopUp();
    }
}
