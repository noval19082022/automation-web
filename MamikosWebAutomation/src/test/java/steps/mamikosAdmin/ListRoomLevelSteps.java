package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.RoomLevel.ListRoomLevelPO;
import utilities.ThreadManager;

public class ListRoomLevelSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ListRoomLevelPO list = new ListRoomLevelPO(driver);

    @Then("system display content page list room level")
    public void system_display_content_page_list_room_level() {
        Assert.assertTrue(list.addRoomLevelButtonIsAppeared(), "Element not appeared");
        Assert.assertTrue(list.searchLevelEditTextIsAppeared(), "Element not appeared");
        Assert.assertTrue(list.searchLevelButtonIsAppeared(), "Element not appeared");
        String[] column = {"ID" , "Level Name", "Key", "Status", "Charging Name", "Charging Fee", "Charging Type", "Charge for Booking Contract", "Charge for Consultant Contract", "Charge for Owner Contract", "Invoice Type", "Notes", "Actions"};

        for (int i=0; i<13; i++){
            Assert.assertEquals(list.getColumnName(i+1), column[i], "Column name not match");
        }
    }

    @When("user click on page number {string} list room level")
    public void user_click_on_page_number_list_room_level(String pageNumber) throws InterruptedException {
        list.clickOnPageNumber(pageNumber);
    }

    @Then("system display list room level page number {string} is active")
    public void system_display_list_room_level_page_number_is_active(String pageNumber) {
        Assert.assertTrue(list.pageNumberButtonIsActive(pageNumber).contains("active"), "Button is not active");
    }
}
