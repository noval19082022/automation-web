package steps.mamikosAdmin;

import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikosAdmin.BookingOwnerReqPO;
import utilities.ThreadManager;

public class BookingOwnerReqSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private BookingOwnerReqPO bookingRequest = new BookingOwnerReqPO(driver);

    @When("user click reject BBK button in booking owner request")
    public void user_click_reject_BBK_button_in_booking_owner_request() throws InterruptedException {
        bookingRequest.clickOnFirstRejectButton();
    }

}
