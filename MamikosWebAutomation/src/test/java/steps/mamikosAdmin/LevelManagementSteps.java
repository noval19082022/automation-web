package steps.mamikosAdmin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

import pageobjects.mamikosAdmin.KostLevelPO;
import pageobjects.mamikosAdmin.RoomLevelPO;

public class LevelManagementSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private JavaHelpers java = new JavaHelpers();
    private KostLevelPO kostLevel = new KostLevelPO(driver);
    private RoomLevelPO roomLevel = new RoomLevelPO(driver);

    //Test Data Level Management
    private String levelManagement="src/test/resources/testdata/mamikos/levelManagement.properties";
    private String kostLevelNameTesting = JavaHelpers.getPropertyValue(levelManagement, "kostLevelNameTesting_" + Constants.ENV);
    private String kostLevelNotesTesting = JavaHelpers.getPropertyValue(levelManagement, "kostLevelNotesTesting_" + Constants.ENV);
    private String kostLevelBenefitsTesting = JavaHelpers.getPropertyValue(levelManagement, "kostLevelBenefitsTesting_" + Constants.ENV);
    private String kostLevelNameEdit = JavaHelpers.getPropertyValue(levelManagement, "kostLevelNameEdit_" + Constants.ENV);
    private String kostLevelNotesEdit = JavaHelpers.getPropertyValue(levelManagement, "kostLevelNotesEdit_" + Constants.ENV);
    private String kostLevelBenefitsEdit = JavaHelpers.getPropertyValue(levelManagement, "kostLevelBenefitsEdit_" + Constants.ENV);

    private String roomLevelNameTesting = JavaHelpers.getPropertyValue(levelManagement, "roomLevelNameTesting_" + Constants.ENV);
    private String roomLevelNotesTesting = JavaHelpers.getPropertyValue(levelManagement, "roomLevelNotesTesting_" + Constants.ENV);
    private String roomLevelChargingNameTesting = JavaHelpers.getPropertyValue(levelManagement, "roomLevelChargingNameTesting_" + Constants.ENV);
    private String roomLevelChargingFeeTesting = JavaHelpers.getPropertyValue(levelManagement, "roomLevelChargingFeeTesting_" + Constants.ENV);
    private String roomLevelChargingTypeTesting = JavaHelpers.getPropertyValue(levelManagement, "roomLevelChargingTypeTesting_" + Constants.ENV);
    private String roomLevelNameEdit = JavaHelpers.getPropertyValue(levelManagement, "roomLevelNameEdit_" + Constants.ENV);
    private String roomLevelNotesEdit = JavaHelpers.getPropertyValue(levelManagement, "roomLevelNotesEdit_" + Constants.ENV);
    private String roomLevelChargingNameEdit = JavaHelpers.getPropertyValue(levelManagement, "roomLevelChargingNameEdit_" + Constants.ENV);
    private String roomLevelChargingFeeEdit = JavaHelpers.getPropertyValue(levelManagement, "roomLevelChargingFeeEdit_" + Constants.ENV);
    private String roomLevelChargingTypeEdit = JavaHelpers.getPropertyValue(levelManagement, "roomLevelChargingTypeEdit_" + Constants.ENV);

    @And("user clicks on Add Kost Level button")
    public void user_clicks_on_add_kost_level_button() throws InterruptedException {
        kostLevel.clickOnKostLevelButton();
    }

    @And("user fills out form create kost level for {string} and click save")
    public void user_fills_out_form_create_kost_level_for_and_click_save(String type) throws InterruptedException, ParseException {
        String newKostLevelName="";
        String newKostLevelNotes="";
        String newKostLevelBenefits="";
        switch (type) {
            case "testing":
                newKostLevelName = kostLevelNameTesting;
                newKostLevelNotes = kostLevelNotesTesting;
                newKostLevelBenefits = kostLevelBenefitsTesting;
                break;
            case "edit":
                newKostLevelName = kostLevelNameEdit;
                newKostLevelNotes = kostLevelNotesEdit;
                newKostLevelBenefits = kostLevelBenefitsEdit;
                break;
        }
        kostLevel.setNewKostLevelName(newKostLevelName);
        kostLevel.setNewKostLevelNotes(newKostLevelNotes);
        kostLevel.setNewKostLevelBenefits(newKostLevelBenefits);
        kostLevel.clickOnSaveButton();
    }

    @And("user clicks on save confirmation button")
    public void user_clicks_on_save_confirmation_button() throws InterruptedException {
        kostLevel.clickOnSaveConfirmationButton();
    }

    @And("user clicks the ok button on the successful popup")
    public void user_clicks_the_ok_button_on_the_successful_popup() throws InterruptedException {
        kostLevel.clickOnOkButtonOnTheSuccessfulPopup();
    }

    @When("user filter kost level by keyword {string}")
    public void user_filter_kost_level_by_keyword(String keyword) {
        kostLevel.setKeywordSearchField(keyword);
    }

    @And("user clicks on Search Kost Level button")
    public void user_clicks_on_search_kost_level_button() throws InterruptedException {
        kostLevel.clickOnSearchKostLevelButton();
    }

    @And("user clicks {string} on Kost Level")
    public void user_click_on_kost_level(String action) {
        if(action.equals("edit")){
            kostLevel.clickOnEditKostLevel();
        }
        else if(action.equals("delete")){
            kostLevel.clickOnDeleteKostLevel();
        }
    }

    @Then("user verify alert success {string} and {string}")
    public void user_verify_alert_success_and(String titleMessage, String contentMessage) throws InterruptedException {
        Assert.assertEquals(kostLevel.getTitleMessageAlert(), titleMessage,"Title message is false");
        Assert.assertTrue(kostLevel.getContentMessageAlert().contains(contentMessage), "Content message is false");
    }

    @And("user clicks on Add Room Level button")
    public void user_clicks_on_add_room_level_button() throws InterruptedException {
        roomLevel.clickOnRoomLevelButton();
    }

    @And("user fills out form create room level for {string} and click save")
    public void user_fills_out_form_create_room_level_for_and_click_save(String type) throws InterruptedException, ParseException {
        String newRoomLevelName="";
        String newRoomLevelNotes="";
        String newRoomLevelChargingName="";
        String newRoomLevelChargingFee="";
        String newRoomLevelChargingType="";
        switch (type) {
            case "testing":
                newRoomLevelName = roomLevelNameTesting;
                newRoomLevelNotes = roomLevelNotesTesting;
                newRoomLevelChargingName = roomLevelChargingNameTesting;
                newRoomLevelChargingFee = roomLevelChargingFeeTesting;
                newRoomLevelChargingType = roomLevelChargingTypeTesting;
                break;
            case "edit":
                newRoomLevelName = roomLevelNameEdit;
                newRoomLevelNotes = roomLevelNotesEdit;
                newRoomLevelChargingName = roomLevelChargingNameEdit;
                newRoomLevelChargingFee = roomLevelChargingFeeEdit;
                newRoomLevelChargingType = roomLevelChargingTypeEdit;
                break;
        }
        roomLevel.setNewRoomLevelName(newRoomLevelName);
        roomLevel.setNewRoomLevelNotes(newRoomLevelNotes);
        roomLevel.setNewRoomLevelChargingName(newRoomLevelChargingName);
        roomLevel.setNewRoomLevelChargingFee(newRoomLevelChargingFee);
        roomLevel.setNewRoomLevelChargingType(newRoomLevelChargingType);
        roomLevel.clickOnSaveButton();
    }

    @When("user filter room level by keyword {string}")
    public void user_filter_room_level_by_keyword(String keyword) {
        roomLevel.setKeywordSearchField(keyword);
    }

    @And("user clicks {string} on Room Level")
    public void user_click_on_room_level(String action) {
        if(action.equals("edit")){
            roomLevel.clickOnEditRoomLevel();
        }
        else if(action.equals("delete")){
            roomLevel.clickOnDeleteRoomLevel();
        }
    }

    @And("user clicks on Search Room Level button")
    public void user_clicks_on_search_room_level_button() throws InterruptedException {
        roomLevel.clickOnSearchRoomLevelButton();
    }

    @Then("user can see {string} kost level on Kost Level menu")
    public void user_can_see_kost_level_on_kost_level_menu(String type) {
        String kostLevelName="";
        if(type.equals("created")){
            kostLevelName = kostLevelNameTesting;
        }
        else if(type.equals("edited")){
            kostLevelName = kostLevelNameEdit;
        }
        Assert.assertEquals(kostLevel.getKostLevelName(), kostLevelName);
    }

    @Then("user can see {string} room level on Room Level menu")
    public void user_can_see_room_level_on_room_level_menu(String type) {
        String roomLevelName="";
        if(type.equals("created")){
            roomLevelName = roomLevelNameTesting;
        }
        else if(type.equals("edited")){
            roomLevelName = roomLevelNameEdit;
        }
        Assert.assertEquals(roomLevel.getRoomLevelName(), roomLevelName);
    }

    @Then("user verify no level displayed")
    public void user_verify_no_level_displayed() {
        Assert.assertTrue(roomLevel.checkResultFilterIsNotPresent());
    }

}