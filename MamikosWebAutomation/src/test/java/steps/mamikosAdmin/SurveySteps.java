package steps.mamikosAdmin;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.KostReview.KostReviewPO;
import pageobjects.mamikosAdmin.SegmentPO;
import pageobjects.mamikosAdmin.Survey.SurveyPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public class SurveySteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SegmentPO segment = new SegmentPO(driver);
    private SurveyPO surveyPO = new SurveyPO(driver);
    private KostReviewPO kostReviewPO = new KostReviewPO(driver);
    private JavaHelpers java = new JavaHelpers();


    @And("user click on edit survey button on {string}")
    public void user_click_on_edit_survey_button_on(String tenant) throws InterruptedException {
        surveyPO.clickOnEditButton(tenant);
    }

    @And("user change survey status to {string}")
    public void user_change_survey_status_to(String survey) throws InterruptedException {
        surveyPO.changeSurveyStatus(survey);
    }

    @Then("user verify change survey success alert with {string}")
    public void user_verify_change_survey_success_alert_with(String text) throws InterruptedException {
        Assert.assertTrue(kostReviewPO.isAlertAppear(), "Success alert is not appeared");
        Assert.assertEquals(kostReviewPO.getAlertText().substring(2,67), text, "Success alert text is not equal to " + text);
    }

    @And("user change survey date to {string}")
    public void user_change_survey_date_to(String date) throws ParseException, InterruptedException {
        String exactDate = "";
        if(date.equalsIgnoreCase("Tomorrow")){
            exactDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        }
        surveyPO.changeSurveyDate(exactDate);
    }

    @And("user change survey time to {string}")
    public void user_change_survey_time_to(String time) throws InterruptedException {
        surveyPO.changeSurveyTime(time);
    }

    @Then("user see at Tenant Survey table contains:")
    public void user_see_at_tenant_survey_table(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        int i=0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(segment.getAllSegmentHeadTable(i),content.get("Head Table"),"Table Segment should contain " + content.get("Head Table"));
            i++;
        }
    }

    @And("user verify filter Tenant Name in the Tenant Survey Page is displayed")
    public void user_verify_filter_tenant_name_in_the_tenant_survey_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(surveyPO.isFilterTenantNameDisplayed());
    }

    @And("user verify filter Tenant Phone Number in the Tenant Survey Page is displayed")
    public void user_verify_filter_tenant_phone_number_in_the_tenant_survey_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(surveyPO.isFilterTenantPhoneNumberDisplayed());
    }

    @And("user verify filter Property Name in the Tenant Survey Page is displayed")
    public void user_verify_filter_property_name_in_the_tenant_survey_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(surveyPO.isFilterPropertyNameDisplayed());
    }

    @And("user verify filter Survey Status in the Tenant Survey Page is displayed")
    public void user_verify_filter_survey_status_in_the_tenant_survey_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(surveyPO.isFilterSurveyStatusDisplayed());
    }

    @And("user verify filter Survey Date in the Tenant Survey Page is displayed")
    public void user_verify_filter_survey_date_in_the_tenant_survey_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(surveyPO.isFilterSurveyDateDisplayed());
    }

    @And("user verify Search button in the Tenant Survey Page is displayed")
    public void user_verify_search_button_in_the_tenant_survey_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(surveyPO.isSearchButtonDisplayed());
    }

    @And("user verify Reset Filter button in the Tenant Survey Page is displayed")
    public void user_verify_reset_filter_button_in_the_tenant_survey_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(surveyPO.isResetFilterButtonDisplayed());
    }

    @And("user verify Create Survey button in the Tenant Survey Page is displayed")
    public void user_verify_create_survey_button_in_the_tenant_survey_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(surveyPO.isCreateSurveyButtonDisplayed());
    }

    @And("user verify Pagination button in the Tenant Survey Page is displayed")
    public void user_verify_pagination_button_in_the_tenant_survey_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(surveyPO.isPaginationButtonDisplayed());
    }

    @And("user fill Tenant Name filter with {string}")
    public void user_fill_Tenant_Name_filter_with(String tenantName) throws InterruptedException {
        surveyPO.fillOnTenantNameFilter(tenantName);
        Assert.assertTrue(surveyPO.isSearchButtonDisplayed(),"Search button is not displayed");
        surveyPO.clickOnSearchFilterButton();
    }

    @Then("user verify Tenant Name filter result with {string}")
    public void user_verify_Tenant_Name_filter_result_with(String tenantName) {
        for (int i = 0; i < surveyPO.tenantName.size(); i++) {
            Assert.assertEquals(surveyPO.getTenantNameTableResult(i), tenantName, "Tenant name is not equal to " + tenantName);
        }
    }

    @And("user choose {string} on survey status filter")
    public void user_choose_on_survey_status_filter(String surveyStatus) throws InterruptedException {
        surveyPO.selectSurveyStatusFilter(surveyStatus);
        Assert.assertTrue(surveyPO.isSearchButtonDisplayed(),"Search button is not displayed");
        surveyPO.clickOnSearchFilterButton();
    }

    @Then("user verify Status filter result with {string}")
    public void user_verify_Status_filter_result_with(String status) {
        for (int i = 0; i < surveyPO.status.size(); i++) {
            Assert.assertEquals(surveyPO.getSurveyStatusTableResult(i), status, "Survey status is not equal to " + status);
        }
    }

    @And("user choose {string} on survey date filter")
    public void user_choose_on_survey_date_filter(String date) throws InterruptedException {
        surveyPO.clickOnSurveyDateFilter(date.substring(0,2));
        Assert.assertTrue(surveyPO.isSearchButtonDisplayed(),"Search button is not displayed");
        surveyPO.clickOnSearchFilterButton();
    }

    @Then("user verify Survey Time filter result with {string}")
    public void user_verify_Survey_Time_filter_result_with(String surveyTime) {
        for (int i = 0; i < surveyPO.surveyTime.size(); i++) {
            Assert.assertEquals(surveyPO.getSurveyTimeTableResult(i).substring(0,15), surveyTime, "Survey time is not equal to " + surveyTime);
        }
    }

    @And("user fill Tenant Phone Number filter with {string}")
    public void user_fill_Tenant_Phone_Number_filter_with(String phone) throws InterruptedException {
        surveyPO.fillOnTenantPhoneNumberFilter(phone);
        Assert.assertTrue(surveyPO.isSearchButtonDisplayed(),"Search button is not displayed");
        surveyPO.clickOnSearchFilterButton();
    }

    @Then("user verify Tenant Phone Number filter result with {string}")
    public void user_verify_Tenant_Phone_Number_filter_result_with(String phone) {
        for (int i = 0; i < surveyPO.tenantPhoneNumber.size(); i++) {
            Assert.assertEquals(surveyPO.getTenantPhoneNumberTableResult(i), phone, "Tenant phone number is not equal to " + phone);
        }
    }

    @And("user fill Property Name filter with {string}")
    public void user_fill_Property_Name_filter_with(String property) throws InterruptedException {
        surveyPO.fillOnPropertyNameFilter(property);
        Assert.assertTrue(surveyPO.isSearchButtonDisplayed(),"Search button is not displayed");
        surveyPO.clickOnSearchFilterButton();
    }

    @Then("user verify Property Name filter result with {string}")
    public void user_verify_Property_Name_filter_result_with(String property) {
        for (int i = 0; i < surveyPO.propertyName.size(); i++) {
            Assert.assertEquals(surveyPO.getPropertyNameTableResult(i), property, "Property name is not equal to " + property);
        }
    }
}
