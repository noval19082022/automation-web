package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.HostileOwnerPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class HostileOwnerSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HostileOwnerPO hostileOwner = new pageobjects.mamikosAdmin.HostileOwnerPO(driver);

    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String ownerPhone = JavaHelpers.getPropertyValue(consultantProperty,"ownerPhoneHostile_" + Constants.ENV);
    private String ownerId = JavaHelpers.getPropertyValue(consultantProperty,"ownerIdHostile_" + Constants.ENV);
    private String hostileScore = JavaHelpers.getPropertyValue(consultantProperty,"hostileScore_" + Constants.ENV);

    @When("user click Add button")
    public void user_click_Add_button() throws InterruptedException {
        hostileOwner.clickOnAddButton();
    }

    @When("user input hostile owner data")
    public void user_input_hostile_owner_data() throws InterruptedException {
        hostileOwner.inputFieldID(ownerId);
        hostileOwner.clickSaveButton();
        hostileOwner.inputFieldScore(hostileScore);
        hostileOwner.clickSaveButton();
    }

    @When("user select hostile reason {string}")
    public void user_select_hostile_reason(String reason) throws InterruptedException {
        hostileOwner.selectHostileReason(reason);
    }

    @When("user click set button")
    public void user_click_set_button() throws InterruptedException {
        hostileOwner.clickSaveButton();
        hostileOwner.clickOkButton();
    }

    @When("user search by owner phone on hostile owner page")
    public void user_search_by_owner_phone_on_hostile_owner_page() throws InterruptedException {
        hostileOwner.inputOwnerPhone(ownerPhone);
        hostileOwner.clickOnSearchButton();
    }

    @Then("user verify owner id on listing hostile owner correct and hostile reason is {string}")
    public void user_verify_owner_id_on_listing_hostile_owner_correct_and_hostile_reason_is(String reason) throws InterruptedException {
        Assert.assertEquals(hostileOwner.getOwnerId(), ownerId, "Owner Id Not "+ ownerId);
        if(reason.equals("Indikasi fraud")){
            Assert.assertEquals(hostileOwner.getHostileReason(), "Indikasi fraud", "Hostile reason not "+ "Indikasi fraud");
        } else if (reason.equals("Indikasi kompetitor")){
            Assert.assertEquals(hostileOwner.getHostileReason(), "Indikasi kompetitor", "Hostile reason not "+ "Indikasi kompetitor");
        } else if(reason.equals("Tidak bayar minimum charging GP")){
            Assert.assertEquals(hostileOwner.getHostileReason(), "Tidak bayar minimum charging GP", "Hostile reason not "+ "Tidak bayar minimum charging GP");
        } else if(reason.equals("Terbukti Fraud")){
            Assert.assertEquals(hostileOwner.getHostileReason(), "Terbukti Fraud", "Hostile reason not "+ "Terbukti Fraud");
        }
    }

    @When("user reset score hostile owner")
    public void user_reset_score_hostile_owner() throws InterruptedException {
        if(hostileOwner.checkOwnerId() == true) {
            hostileOwner.clickOnActionButton();
            hostileOwner.clickOnResetAction();
            hostileOwner.clickSaveButton();
        }
    }
}
