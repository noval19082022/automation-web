package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.CreateKostLevelPO;
import pageobjects.mamikosAdmin.ListKostLevelPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class CreateUpdateKostLevelSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private CreateKostLevelPO create = new CreateKostLevelPO(driver);
    private ListKostLevelPO list = new ListKostLevelPO(driver);

    //Test Data Kost Level
    private String kostLevel = "src/test/resources/testdata/mamikosadmin/kostLevel.properties";

    //original data
    private String levelName = JavaHelpers.getPropertyValue(kostLevel, "levelName_" + Constants.ENV);
    private String levelKey = JavaHelpers.getPropertyValue(kostLevel, "levelKey_" + Constants.ENV);
    private String levelOrder = JavaHelpers.getPropertyValue(kostLevel, "levelOrder_" + Constants.ENV);
    private String levelIsPartner = JavaHelpers.getPropertyValue(kostLevel, "levelIsPartner_" + Constants.ENV);
    private String LevelSetValues = JavaHelpers.getPropertyValue(kostLevel, "LevelSetValues_" + Constants.ENV);
    private String levelBenefit = JavaHelpers.getPropertyValue(kostLevel, "levelBenefit_" + Constants.ENV);
    private String levelCriteria = JavaHelpers.getPropertyValue(kostLevel, "levelCriteria_" + Constants.ENV);
    private String levelStatus = JavaHelpers.getPropertyValue(kostLevel, "levelStatus_" + Constants.ENV);
    private String levelRoom = JavaHelpers.getPropertyValue(kostLevel, "levelRoom_" + Constants.ENV);
    private String levelFee = JavaHelpers.getPropertyValue(kostLevel, "levelFee_" + Constants.ENV);
    private String levelType = JavaHelpers.getPropertyValue(kostLevel, "levelType_" + Constants.ENV);
    private String levelBooking = JavaHelpers.getPropertyValue(kostLevel, "levelBooking_" + Constants.ENV);
    private String levelConsultant = JavaHelpers.getPropertyValue(kostLevel, "levelConsultant_" + Constants.ENV);
    private String levelOwner = JavaHelpers.getPropertyValue(kostLevel, "levelOwner_" + Constants.ENV);
    private String levelInvoiceType = JavaHelpers.getPropertyValue(kostLevel, "levelInvoiceType_" + Constants.ENV);
    private String levelValue = JavaHelpers.getPropertyValue(kostLevel, "levelValue_" + Constants.ENV);
    private String levelNotes = JavaHelpers.getPropertyValue(kostLevel, "levelNotes_" + Constants.ENV);

    //update data
    private String levelName_update = JavaHelpers.getPropertyValue(kostLevel, "levelName_update_" + Constants.ENV);
    private String levelKey_update = JavaHelpers.getPropertyValue(kostLevel, "levelKey_update_" + Constants.ENV);
    private String levelOrder_update = JavaHelpers.getPropertyValue(kostLevel, "levelOrder_update_" + Constants.ENV);
    private String levelIsPartner_update = JavaHelpers.getPropertyValue(kostLevel, "levelIsPartner_update_" + Constants.ENV);
    private String LevelSetValues_update = JavaHelpers.getPropertyValue(kostLevel, "LevelSetValues_update_" + Constants.ENV);
    private String levelBenefit_update = JavaHelpers.getPropertyValue(kostLevel, "levelBenefit_update_" + Constants.ENV);
    private String levelCriteria_update = JavaHelpers.getPropertyValue(kostLevel, "levelCriteria_update_" + Constants.ENV);
    private String levelStatus_update = JavaHelpers.getPropertyValue(kostLevel, "levelStatus_update_" + Constants.ENV);
    private String levelRoom_update = JavaHelpers.getPropertyValue(kostLevel, "levelRoom_update_" + Constants.ENV);
    private String levelFee_update = JavaHelpers.getPropertyValue(kostLevel, "levelFee_update_" + Constants.ENV);
    private String levelType_update = JavaHelpers.getPropertyValue(kostLevel, "levelType_update_" + Constants.ENV);
    private String levelBooking_update = JavaHelpers.getPropertyValue(kostLevel, "levelBooking_update_" + Constants.ENV);
    private String levelConsultant_update = JavaHelpers.getPropertyValue(kostLevel, "levelConsultant_update_" + Constants.ENV);
    private String levelOwner_update = JavaHelpers.getPropertyValue(kostLevel, "levelOwner_update_" + Constants.ENV);
    private String levelInvoiceType_update = JavaHelpers.getPropertyValue(kostLevel, "levelInvoiceType_update_" + Constants.ENV);
    private String levelValue_update = JavaHelpers.getPropertyValue(kostLevel, "levelValue_update_" + Constants.ENV);
    private String levelNotes_update = JavaHelpers.getPropertyValue(kostLevel, "levelNotes_update_" + Constants.ENV);

    @When("user create kost level")
    public void user_create_kost_level() throws InterruptedException {
        list.clickOnAddKostLevelButton();
        create.enterLevelName(levelName);
        create.enterKey(levelKey);
        create.SelectOrder(levelOrder);
        create.selectIsPartnerLevel(levelIsPartner);
        create.selectSetLevelValues(LevelSetValues);
        create.selectStatus(levelStatus);
        create.enterNotes(levelNotes);
        create.selectRoomLevel(levelRoom);
        create.clickOnSaveButton();
        create.clickOnYesSaveKostLevelButton();
        create.clickOnOkLevelSuccessfullyAddedButton();
    }

    @When("user update kost level with empty level name")
    public void user_update_kost_level_with_empty_level_name() throws InterruptedException {
        list.searchKostLevel(levelName);
        list.clickOnUpdateKostLevelIcon();
        create.enterLevelName("");
        create.clickOnSaveButton();
        create.clickOnYesSaveKostLevelButton();
    }

    @When("user update kost level")
    public void user_update_kost_level() throws InterruptedException {
        list.searchKostLevel(levelName);
        list.clickOnUpdateKostLevelIcon();
        create.enterLevelName(levelName_update);
        create.enterKey(levelKey_update);
        create.SelectOrder(levelOrder_update);
        create.selectIsPartnerLevel(levelIsPartner_update);
        create.selectSetLevelValues(LevelSetValues_update);
        create.selectStatus(levelStatus_update);
        create.enterNotes(levelNotes_update);
        create.selectChargingType(levelType_update);
        create.selectRoomLevel(levelRoom_update);
        create.clickOnSaveButton();
        create.clickOnYesSaveKostLevelButton();
        create.clickOnOkLevelSuccessfullyAddedButton();
    }
}
