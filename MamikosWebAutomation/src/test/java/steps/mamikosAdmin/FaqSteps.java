package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.CreateFAQPO;
import pageobjects.mamikosAdmin.FaqPO;
import utilities.ThreadManager;

public class FaqSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private FaqPO faq = new FaqPO(driver);
    private CreateFAQPO add = new CreateFAQPO(driver);

    @Then("system display management level content")
    public void system_display_management_level_content() {
        Assert.assertTrue(faq.verifyContentListFAQ(), "Incomplete contents");
    }

    @When("user add level faq")
    public void user_add_level_faq() throws InterruptedException {
        faq.clickOnAddLevelFaqButton();
        add.enterQuestion("Question Automate");
        add.enterAnswer("Answer Automate");
        add.clickOnSaveButton();
    }

    @When("user delete level faq")
    public void user_delete_level_faq() throws InterruptedException {
        faq.searchLevelFaq("Question Automate");
        faq.deleteFirstListLevelFaq();
    }

    @Then("system display new level faq")
    public void system_display_new_level_faq() throws InterruptedException {
        faq.searchLevelFaq("Question Automate");
        String[] valueFaq = {"Question Automate", "Answer Automate"};

        for (int i=0 ; i<valueFaq.length; i++){
            Assert.assertEquals(faq.getValueColumnFaq(i+1), valueFaq[i], "Value not match");
        }
    }

    @Then("system display new level faq is not found")
    public void system_display_new_level_faq_is_not_found() throws InterruptedException {
        faq.searchLevelFaq("Question Automate");
        Assert.assertFalse(faq.listLevelFaqIsAppeared());
    }
}
