package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikosAdmin.KostPO;
import utilities.ThreadManager;

public class KostSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private KostPO kost = new KostPO(driver);

    @When("user search kos name {string} in admin kos page")
    public void user_search_kos_name_in_admin_kos_page(String kosName) throws InterruptedException {
        kost.searchKosName(kosName);
    }

    @Then("user thanos the kos in admin kos page")
    public void user_thanos_the_kos_in_admin_kos_page() throws InterruptedException {
        kost.clickOnFirstThanosButton();
    }
}
