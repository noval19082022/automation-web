package steps.mamikosAdmin.KostReview;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.KostReview.KostReviewPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.List;

public class KostReviewSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private KostReviewPO kostReview = new KostReviewPO(driver);
    private JavaHelpers java = new JavaHelpers();

    @And("user click on Create Review button")
    public void user_click_on_Create_Review_button() throws InterruptedException {
        kostReview.clickOnCreateReviewButton();
    }

    @And("user fill first Create Review without contract data")
    public void user_fill_first_Create_Review_data(List<String> data) throws InterruptedException, ParseException {
        String tempStartDate = "";
        String tempEndDate = "";
        if (data.get(6).equalsIgnoreCase("Today")) {
            tempStartDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        }else if(data.get(6) != null){
            tempStartDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", Integer.parseInt(data.get(6)), 0, 0, 0);
        }
        if(data.get(7).equalsIgnoreCase("Tomorrow")) {
            tempEndDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        }else if(data.get(7) != null){
            tempEndDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", Integer.parseInt(data.get(7)), 0, 0, 0);
        }
        kostReview.fillFirstCreateReviewWithoutContractData(data.get(0), data.get(1), data.get(2), data.get(3),data.get(4),data.get(5), tempStartDate, tempEndDate);
    }

    @And("user fill second Create Review data")
    public void user_fill_second_Create_Review_data(List<String> data) throws InterruptedException {
        kostReview.fillSecondCreateReviewData(data.get(0), data.get(1), data.get(2), data.get(3), data.get(4), data.get(5), data.get(6));
    }

    @Then("user receive success alert for created kost review with text {string}")
    public void user_recieve_success_alert_for_created_kost_review(String text) throws InterruptedException {
        Assert.assertTrue(kostReview.isAlertAppear(), "Success created kost review alert is not appeared");
        Assert.assertEquals(kostReview.getAlertText().substring(2,37), text, "Success created alert text is not equal to " + text);
    }

    @And("user click Delete Review button on {string}")
    public void user_click_Delete_Review_button(String content) throws InterruptedException {
        kostReview.clickOnDeleteReviewButton(content);
    }

    @Then("user receive success alert for deleted kost review with text {string}")
    public void user_receive_success_alert_for_deleted_kost_review_with_text(String text) throws InterruptedException {
        Assert.assertTrue(kostReview.isAlertAppear(), "Success created kost review alert is not appeared");
        Assert.assertEquals(kostReview.getAlertText().substring(2,34), text, "Success created alert text is not equal to " + text);
    }

    @And("user click cancel on Create Review section")
    public void user_click_on_cancel_Create_Review_section() throws InterruptedException {
        kostReview.clickOnCancelButton();
    }

    @And("user click save on Create Review Section")
    public void user_click_save_on_Create_Review_Section() throws InterruptedException {
        kostReview.clickOnSaveButton();
    }

    @And("user fill first Create Review with contract data")
    public void user_fill_first_Create_Review_with_contract_data(List<String> data) throws InterruptedException {
        kostReview.fillFirstCreateReviewWithContractData(data.get(0), data.get(1), data.get(2));
    }

    @And("user click on Edit Review button on {string}")
    public void user_click_on_Edit_Review_button(String content) throws InterruptedException {
        kostReview.clickOnEditButton(content);
    }

    @And("user change Kost Review status to {string}")
    public void user_change_Kost_Review_status_to(String status) {
        kostReview.changeKostReviewStatus(status);
    }

    @And("user click save on Edit Review Section")
    public void user_click_save_on_Edit_Review_Section() throws InterruptedException {
        kostReview.clickOnSaveButton();
    }

    @Then("user receive success alert for updated kost review with text {string}")
    public void user_receive_success_alert_for_updated_kost_review_with_text(String text) throws InterruptedException {
        Assert.assertTrue(kostReview.isAlertAppear(), "Success created kost review alert is not appeared");
        Assert.assertEquals(kostReview.getAlertText().substring(2,25), text, "Success created alert text is not equal to " + text);
    }

    @And("user verify there are {string} input")
    public void user_verify_there_are_input(String status) {
        switch (status) {
            case " locked ":
                Assert.assertEquals(kostReview.getTenantNameInputAttribute().substring(42, 50), status, "Tenant name input is not " + status);
                Assert.assertEquals(kostReview.getTenantPhoneNumberAttribute().substring(42, 50), status, "Tenant phone input is not " + status);
            break;
            case "disabled":
                Assert.assertEquals(kostReview.getKostNameAttribute().substring(42, 50), status, "Kost name input is not " + status);
            break;
            case "tepicker":
                Assert.assertEquals(kostReview.getStartDateInputAttribute().substring(67, 75), status, "Start date input is not " + status);
                Assert.assertEquals(kostReview.getEndDateInputAttribute().substring(67, 75), status, "End date input is not " + status);
            break;
        }
    }

    @And("user click Live button on {string}")
    public void user_click_Live_button(String content) throws InterruptedException {
        kostReview.clickOnLiveButton(content);
    }

    @Then("user receive success alert for kost review updated to live with text {string}")
    public void user_receive_success_alert_for_kost_review_updated_to_live_with_text(String text) throws InterruptedException {
        Assert.assertTrue(kostReview.isAlertAppear(), "Success created kost review alert is not appeared");
        Assert.assertEquals(kostReview.getAlertText().substring(2,33), text, "Success created alert text is not equal to " + text);
    }

    @And("user click Reject button on {string}")
    public void user_click_Reject_button(String content) throws InterruptedException {
        kostReview.clickOnRejectButton(content);
    }

    @Then("user receive success alert for kost review updated to reject with text {string}")
    public void user_receive_success_alert_for_kost_review_updated_to_reject_with_text(String text) throws InterruptedException {
        Assert.assertTrue(kostReview.isAlertAppear(), "Success created kost review alert is not appeared");
        Assert.assertEquals(kostReview.getAlertText().substring(2,34), text, "Success created alert text is not equal to " + text);
    }
}
