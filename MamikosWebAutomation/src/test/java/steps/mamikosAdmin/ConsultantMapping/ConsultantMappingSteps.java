package steps.mamikosAdmin.ConsultantMapping;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.ConsultantMapping.ConsultantMappingPO;
import utilities.ThreadManager;

import java.util.List;

public class ConsultantMappingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ConsultantMappingPO consultantMappingPO = new ConsultantMappingPO(driver);

    @And("user click on {string} button")
    public void user_click_on_desired_button(String text) throws InterruptedException {
        Assert.assertEquals(consultantMappingPO.getAddMappingButtonText(), text, "Button text is not equal to " + text);
        consultantMappingPO.clickOnAddMappingButton();
    }

    @And("user fill add mapping data with:")
    public void user_fill_add_mapping_data_with(List<String> data) throws InterruptedException {
        consultantMappingPO.fillAddMappingData(data.get(0), data.get(1), data.get(2), data.get(3));
        consultantMappingPO.clickOnSaveChangesButton();
    }

    @Then("user verify success notification with {string}")
    public void user_verify_success_notification_with(String message) throws InterruptedException {
        Assert.assertTrue(consultantMappingPO.isAlertAppear(), "Success alert is not appeared");
        Assert.assertEquals(consultantMappingPO.getAlertText().substring(2, 31), message, "Success notification alert message is not equal to " + message);
    }

    @And("user search mapping by consultant name with {string}")
    public void user_search_mapping_by_consultant_name_with(String name) throws InterruptedException {
        consultantMappingPO.fillSearchMappingByName(name);
        consultantMappingPO.clickOnSearchButton();
    }

    @And("user click hapus profil on mapping result")
    public void user_click_string_on_mapping_result() throws InterruptedException {
        consultantMappingPO.clickOnUpdateMappingButton();
        consultantMappingPO.clickOnDeleteMappingButton();
    }

    @And("user click edit profil on mapping result")
    public void user_click_edit_profil_on_mapping_result() throws InterruptedException {
        consultantMappingPO.clickOnUpdateMappingButton();
        consultantMappingPO.clickOnEditMappingButton();
    }

    @And("user add kecamatan with {string}")
    public void user_add_kecamatan_with(String subdistrict) throws InterruptedException {
        consultantMappingPO.addKecamatanOnEditMapping(subdistrict);
    }

    @Then("user verify update success notification with {string}")
    public void user_verify_update_success_notification_with(String message) throws InterruptedException {
        Assert.assertTrue(consultantMappingPO.isAlertAppear(), "Success alert is not appeared");
        Assert.assertEquals(consultantMappingPO.getAlertText().substring(2, 49), message, "Success notification alert message is not equal to " + message);
    }
}
