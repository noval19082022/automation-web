package steps.mamikosAdmin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.DataBookingPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class DataBookingMenuSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private DataBookingPO databooking = new pageobjects.mamikosAdmin.DataBookingPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String tenantProperty="src/test/resources/testdata/consultant/tenant.properties";
    private String tenantHPCrm = JavaHelpers.getPropertyValue(tenantProperty,"tenatHp_crm_" + Constants.ENV);
    private String firstTag = JavaHelpers.getPropertyValue(consultantProperty,"firstTag_" + Constants.ENV);
    private String secondTag = JavaHelpers.getPropertyValue(consultantProperty,"secondTag_" + Constants.ENV);
    private String thirdTag = JavaHelpers.getPropertyValue(consultantProperty,"thirdTag_" + Constants.ENV);
    private String fourthTag = JavaHelpers.getPropertyValue(consultantProperty,"fourthTag_" + Constants.ENV);
    private String fifthTag = JavaHelpers.getPropertyValue(consultantProperty,"fifthTag_" + Constants.ENV);
    private String sixthTag = JavaHelpers.getPropertyValue(consultantProperty,"sixthTag_" + Constants.ENV);

    //Test Data OB
    private String obProperty = "src/test/resources/testdata/mamikos/OB.properties";
    private String obProperty2 = "src/test/resources/testdata/occupancy-and-billing/tenant.properties";
    private String tenantAdditionalPrice = JavaHelpers.getPropertyValue(obProperty, "additionalPricePhone_" + Constants.ENV);
    private String tenantRejected = JavaHelpers.getPropertyValue(obProperty, "tenantKostSayaRejected_" + Constants.ENV);
    private String tenantRejectAdmin = JavaHelpers.getPropertyValue(obProperty2, "obOwnerRejectFullPhone_" + Constants.ENV);

    //Test Data Teng
    private String tengTenant = "src/test/resources/testdata/mamikos/tenant-engagement.properties";
    private String tengAddsOnTenant = JavaHelpers.getPropertyValue(tengTenant, "tengInvoiceDetail_" + Constants.ENV);

    //Test Data PF
    private String ugeSquad = "src/test/resources/testdata/mamikos/ugeSquad.properties";
    private String tenantRefund = JavaHelpers.getPropertyValue(ugeSquad, "tenantRefund_" + Constants.ENV);
    private String tenantConfirmBooking = JavaHelpers.getPropertyValue(ugeSquad, "tenantConfirmBooking_" + Constants.ENV);


    @And("user show filter data booking")
    public void userShowFilterDataBooking() throws InterruptedException {
        databooking.showOrHideFilter();
    }

    @And("user click action button in data booking")
    public void userClickActionButton() throws InterruptedException {
        databooking.clickActionButton();
    }

    @And("user search data booking with status {string}")
    public void user_Search_Booking_Status(String booked) throws InterruptedException {
        databooking.userSearchBookingStatus(booked);
    }

    @When("user search data booking using tenant phone {string}")
    public void user_search_data_booking_using_tenant_phone(String user) {
        switch (user.toLowerCase()) {
            case "crm":
                databooking.setTenantPhoneField(tenantHPCrm);
                break;
            case "additional price ob":
                databooking.setTenantPhoneField(tenantAdditionalPrice);
                break;
            case "teng adds on":
                databooking.setTenantPhoneField(tengAddsOnTenant);
                break;
            case "ob rejected":
                databooking.setTenantPhoneField(tenantRejected);
                break;
            case "tenant refund":
                databooking.setTenantPhoneField(tenantRefund);
                break;
            case "tenant reject on admin":
                databooking.setTenantPhoneField(tenantRejectAdmin);
                break;
            case "tenant confirm booking":
                databooking.setTenantPhoneField(tenantConfirmBooking);
                break;
            default:
                throw new IllegalArgumentException("Input with valid test data crm, additional price ob, teng adds on");
        }
    }

    @When("user edit consultant notes")
    public void user_edit_consultant_notes() throws InterruptedException {
        databooking.clickActionButton();
        databooking.clickEditConsultantNote();
    }

    @When("user set remarks to {string}")
    public void user_set_remarks_to(String remarks) {
        databooking.chooseRemarks(remarks);
    }

    @When("user set notes to {string}")
    public void user_set_notes_to(String notes) {
        databooking.setRemarksNotes(notes);
    }

    @When("user save notes")
    public void user_save_notes() throws InterruptedException {
        databooking.clickSaveRemarksNotes();
    }

    @Then("remarks set to {string}")
    public void remarksSetTo(String remarks) {
        Assert.assertEquals(databooking.getRemarks(),remarks,"remarks not as expected");
    }

    @And("note set to {string}")
    public void noteSetTo(String note) {
        Assert.assertEquals(databooking.getNotes(),note,"Note not as expected");
    }

    @And("user go to detail booking")
    public void userGoToDetailBooking() throws InterruptedException {
        databooking.clickActionButton();
        databooking.clickDetailButton();
    }

    @Then("history notes row {string} is {string} {string}")
    public void historyNotesRowIs(String row, String remarks, String notes) {
        databooking.navigateToNotesColumn();
        Assert.assertEquals(databooking.getNotesInDetailBooking(row),notes,"notes is not as expected");
        Assert.assertEquals(databooking.getRemarksInDetailBooking(row),remarks,"remarks is not as expected");
    }

    @Then("data booking contains all kost tags")
    public void dataBookingContainsAllKostTags() throws InterruptedException {
        databooking.navigateToTagRemarksColumn();
            Assert.assertEquals(databooking.getFirstTag(),firstTag);
            Assert.assertEquals(databooking.getSecondTag(),secondTag);
            Assert.assertEquals(databooking.getThirdTag(),thirdTag);
            Assert.assertEquals(databooking.getFourthTag(),fourthTag);
            Assert.assertEquals(databooking.getFifthTag(),fifthTag);
            Assert.assertEquals(databooking.getSixthTag(),sixthTag);
    }

    @And("user set filter kost type by {string}")
    public void userSetFilterKostTypeBy(String option) throws InterruptedException {
        databooking.setKosTypeFilter(option);
    }

    @And("user apply filter")
    public void userApplyFilter() throws InterruptedException {
        databooking.applyFilter();
    }

    @Then("updated by tag and remarks set to {string}")
    public void updatedByTagAndRemarksSetTo(String name) {
        Assert.assertEquals(databooking.getUpdatedBy(), name, "remarks not as expected");
    }

    @When("user go to confirm booking via action button")
    public void user_go_to_confir_booking_via_action_button() throws InterruptedException {
        databooking.clickActionButton();
        databooking.clikcActionConfirmButton();
    }
    @And("user click lanjutkan button in data booking")
    public void user_click_next_button() throws InterruptedException {
        databooking.clickNextButton();
    }
    @When("user batalkan down payment")
    public void user_batalkan_down_payment() throws InterruptedException {
        databooking.cancelDownPayment();
    }

    @When("user navigates to {string} confirmation page data booking admin")
    public void user_navigates_to_x_page_data_booking_admin(String pageTitle) throws InterruptedException {
        databooking.navigateToConfirmPage(pageTitle);
    }

    @Then("user can sees down payment is {string} and price is {string}")
    public void user_can_sees_down_payment_is_x_and_price_is_x(String dpPercentage, String dpPrice) {
        Assert.assertEquals(databooking.getDownPaymentActivePercentage(), dpPercentage, "Active dp is not " + dpPercentage);
        Assert.assertEquals(databooking.getDownPaymentActivePrice(), dpPrice, "Active dp price is not " + dpPrice);
    }

    @When("user confirm booking from admin")
    public void user_confirm_booking_from_admin() throws InterruptedException {
        databooking.clickOnConfirmationButton();
    }

    @When("user set refund reason for data booking")
    public void user_set_refund_reason_for_data_booking() throws InterruptedException {
        databooking.clickActionButton();
        databooking.clickOnSetTransferPermissionButton();
        databooking.setTransferPermissionStatus();
        databooking.setRefundReason();
        databooking.clickOnSendButton();
    }

    @Then("user verify transfer permission of data booking is {string}")
    public void user_verify_transfer_permission_of_data_booking_is(String status) {
        Assert.assertEquals(databooking.getTransferPermissionStatus(), status, "status transfer permission not match");
    }

    @Then("user can sees other price with name {string} and price {string} on tambahan biaya")
    public void user_can_sees_other_price_with_name_and_price_on_tambahan_biaya(String priceName, String priceNumber) {
        Assert.assertEquals(databooking.getOtherPriceNameOnTambahanBiaya(), priceName, "Other price name is not " + priceName);
        Assert.assertEquals(databooking.getOtherPriceNumberOnTambahanBiaya(), priceNumber, "Other price name is not " + priceName);
    }

    @Then("user can sees other price with name {string} and price {string} on konfirmasi")
    public void user_can_sees_other_price_with_name_and_price_on_konfirmasi(String priceName, String priceNumber) {
        Assert.assertEquals(databooking.getOtherPriceNameOnConfirmation(), priceName, "Other price name on confirmation is not " + priceName);
        Assert.assertEquals(databooking.getOtherPriceNumberOnConfirmation(), priceNumber, "Other price number on confirmation is not " + priceNumber);
    }

    @When("user process to reject booking")
    public void user_reject_booking_with_full_kost_reason_from_action_button() throws InterruptedException {
        databooking.clickActionButton();
        databooking.clickOnRejectedListButton();
    }

    @When("admin proceed to cancel booking")
    public void admin_proceed_to_cancel_booking() throws InterruptedException {
        databooking.clickActionButton();
        databooking.clickOnCancelListButton();
    }

    @When("user choose {string} and click on send button")
    public void user_can_sees_is_selected_and_click_on_send_button(String rejectReasonText) throws InterruptedException {
        databooking.chooseRejectReason(rejectReasonText);
        databooking.clickOnSendButtonRejectBooking();
    }

    @When("admin reject booking with {string} and reason is {string}")
    public void admin_reject_booking_with_and_reason_is(String rejectReasonText, String otherReason) throws InterruptedException {
        databooking.chooseRejectReason(rejectReasonText);
        selenium.hardWait(2);
        databooking.setOtherReasonText(otherReason);
        databooking.clickOnSendButtonRejectBooking();
    }

    @When("admin cancel booking with {string} and reason is {string}")
    public void admin_cancel_booking_with_and_reason_is(String cancelReason, String cancelReasonText) throws InterruptedException {
        databooking.chooseCancelReason(cancelReason);
        selenium.hardWait(2);
        databooking.setCancelOtherReasonText(cancelReasonText);
        databooking.clickOnSendButtonRejectBooking();
    }

    @When("user allow disbursement on tenant {string}")
    public void user_allow_disbursement_on_tenant(String tenantPhone) throws InterruptedException {
        databooking.allowDisbursement(tenantPhone);
    }

    @When("admin go to data booking detail for phone {string} first index")
    public void admin_go_to_data_booking_detail_in_index(String phoneNumber) throws InterruptedException {
        databooking.goToDataBookingDetailIndex(phoneNumber);
    }

    @When("admin goes to invoice detail from contract log {string} index")
    public void admin_goes_to_invoice_detail_from_contract_log_index(String index) throws InterruptedException {
        databooking.goToInvoiceDetail(index);
    }

    @And("user clicks the Booking Now button")
    public void userClicksTheBookingNowButton() throws InterruptedException {
        databooking.clickBookingNowButton();
    }

    @And("user selects room by Nama Kost {string}")
    public void selectRoomByNamaKost(String kostName) throws InterruptedException {
        databooking.selectRoomByNamaKost(kostName);
    }

    @And("user fills phone number field on form booking by {string}")
    public void userFillsPhoneNumberFieldOnFormBooking(String phoneNumber) throws InterruptedException {
        databooking.fillPhoneNumberField(phoneNumber);
    }

    @And("user clicks the Search button on form booking")
    public void userClicksTheSearchButtonOnFormBooking() throws InterruptedException {
        databooking.clickSearchButton();
    }

    @And("user clicks the Next button on form booking step 2")
    public void userClicksTheNextButtonOnFormBookingStep2() throws InterruptedException {
        databooking.clickNextButtonStep2();
    }

    @And("user will be taken to step 3")
    public void userWillBeTakenToStep3() throws InterruptedException {
        Assert.assertTrue(databooking.isStep3Displayed());
    }

    @And("user choose search tenant by name on form booking")
    public void user_choose_search_tenant_by_name_on_form_booking() throws InterruptedException {
        databooking.userChooseSearchTenantByName();
    }

    @And("user verify pop up error on form booking {string}")
    public void userVerifyPopUpErrorOnFormBooking(String popUpMessage) throws InterruptedException {
        databooking.verifyPopUpError(popUpMessage);
    }

    @And("user choose booking type by {string} on form booking")
    public void user_choose_booking_type_on_form_booking(String bookingType) throws InterruptedException {
        databooking.userChooseBookingType(bookingType);
    }

    @And("user fills date on form booking by {string}")
    public void user_fills_date_on_form_booking(String date) throws InterruptedException {
        databooking.fillDateField(date);
    }

    @And("user clicks the Next button on form booking step 3")
    public void user_clicks_the_next_button_on_form_booking_step_3() throws InterruptedException {
        databooking.clickNextButtonStep3();
    }

    @And("user clicks the Submit button on form booking")
    public void user_clicks_the_submit_button_on_form_booking() throws InterruptedException {
        databooking.clickSubmitButton();
    }

    @And("user fills contract id on form booking by {string}")
    public void user_fills_contract_id_on_form_booking(String contractId) throws InterruptedException {
        databooking.fillContractIdField(contractId);
    }

    @Then("user verify success message on data booking page is appear")
    public void user_verify_success_message_on_data_booking_page_is_appear() throws InterruptedException {
        Assert.assertTrue(databooking.isSuccessMessageAppear());
    }

    @When("Contract Valid message on form booking is displayed")
    public void user_verify_booking_title_is_displayed() throws InterruptedException {
        Assert.assertTrue(databooking.isContractValidMessageDisplayed());
    }
}