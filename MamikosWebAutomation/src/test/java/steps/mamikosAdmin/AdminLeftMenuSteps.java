package steps.mamikosAdmin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikosAdmin.LeftMenuPO;
import utilities.ThreadManager;

public class AdminLeftMenuSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private LeftMenuPO leftMenu = new pageobjects.mamikosAdmin.LeftMenuPO(driver);

    @When("user clicks on Point Management-Segment menu")
    public void user_clicks_on_point_management_segment_menu() throws InterruptedException {
        leftMenu.clickOnSegmentMenu();
    }

    @When("user clicks on Point Management-Owner Setting menu")
    public void user_clicks_on_point_management_owner_setting_menu() throws InterruptedException {
        leftMenu.clickOnOwnerSettingMenu();
    }

    @When("user clicks on Point Management-Tenant Setting menu")
    public void user_clicks_on_point_management_tenant_setting_menu() throws InterruptedException {
        leftMenu.clickOnTenantSettingMenu();
    }

    @When("user clicks on Point Management-User Point menu")
    public void user_clicks_on_Point_Management_User_Point_menu() throws InterruptedException {
        leftMenu.clickOnUserPointMenu();
    }

    @When("user clicks on Point Management-Activity menu")
    public void user_clicks_on_Point_Management_Activity_menu() throws InterruptedException {
        leftMenu.clickOnActivityMenu();
    }

    @When("user clicks on Point Management-Owner Room Group menu")
    public void user_clicks_on_Point_Management_Owner_Room_Group_menu() throws InterruptedException {
        leftMenu.clickOnOwnerRoomGroupMenu();
    }

    @When("user clicks on Kost Level menu")
    public void user_clicks_on_Kost_level_menu() throws InterruptedException {
        leftMenu.clickOnKostLevelMenu();
    }

    @When("user clicks on Room Level menu")
    public void user_clicks_on_Room_level_menu() throws InterruptedException {
        leftMenu.clickOnRoomLevelMenu();
    }

    @When("user access to Sales Motion menu")
    public void user_access_to_Sales_Motion_menu() throws InterruptedException {
        leftMenu.clickOnSalesMotionMenu();
    }

    @When("user access to Property Package menu")
    public void user_access_to_Property_Package_menu() throws InterruptedException {
        leftMenu.clickOnPropertyPackageMenu();
    }

    @When("user access to hostile owner menu")
    public void user_access_to_Hostile_Owner_menu() throws InterruptedException {
        leftMenu.clickOnHostileOwnerMenu();
    }

    @When("user access to kost owner menu")
    public void user_access_to_kost_owner_menu() throws InterruptedException {
            leftMenu.clickOnKostOwnerMenu();
        if (leftMenu.isClickOnKostOwnerMenu()){
            leftMenu.clickMuatUlangButton();
            leftMenu.clickOnKostOwnerMenu();
        }
    }

    @When("user access to data booking menu")
    public void user_access_to_data_booking_menu() throws InterruptedException {
        leftMenu.clickOnDataBookingMenu();
    }

    @Given("user access menu {string} sub menu of management level")
    public void user_access_menu_sub_menu_of_management_level(String subMenu) throws InterruptedException {
        leftMenu.clickOnSubMenuOfManagementLevel(subMenu);
    }

    @When("user access to kost menu")
    public void user_access_to_kost_menu() throws InterruptedException {
        leftMenu.clickOnKostMenu();
    }

    @When("user access to Reward Type menu")
    public void user_access_to_reward_type_menu() throws InterruptedException {
        leftMenu.clickOnRewardTypeMenu();
    }

    @When("user access to Reward List menu")
    public void user_access_to_reward_list_menu() throws InterruptedException {
        leftMenu.clickOnRewardListMenu();
    }

    @When("user access to user point menu")
    public void user_access_to_user_point_menu() throws InterruptedException{
        leftMenu.userPointMenuButton();
    }

    @When("user click on Kost Review menu")
    public void user_clicks_on_Kost_Review_menu() throws InterruptedException {
        leftMenu.clickOnKostReviewMenu();
    }

    @When("user click on Tenant Survey menu")
    public void user_click_on_Tenant_Survey_menu() throws InterruptedException {
        leftMenu.clickOnTenantSurveyMenu();
    }

    @When("user click on Owner Point Blacklist menu")
    public void user_click_on_Owner_Point_Blacklist_menu() throws InterruptedException {
        leftMenu.clickOnOwnerPointBlacklistMenu();
    }

    @When("user clicks on Point Management-Expiry menu")
    public void user_clicks_on_Point_Management_Expiry_menu() throws InterruptedException {
        leftMenu.clickOnExpiryMenu();
    }

    @When("user clicks on Chat-Chat Room menu")
    public void user_clicks_on_chat_chat_room_menu() throws InterruptedException{
        leftMenu.clickOnChatRoomMenu();
    }

    @When("user click on Consultant Mapping menu")
    public void user_click_on_Consultant_Mapping_Menu() throws InterruptedException {
        leftMenu.clickOnConsultantMappingMenu();
    }

    @When("user access bangkerupux menu {string}")
    public void user_access_bangkerupux_menu(String menu) throws InterruptedException {
        switch (menu){
            case "Kost Additional":
                leftMenu.clickKostAdditionalMenu();
                break;
            case "Data Booking":
                leftMenu.clickOnDataBookingMenu();
                break;
            default:
                System.out.println("Menu doesn't exist");
        }
    }
}
