package steps.mamikosAdmin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.KostOwnerPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class KostOwnerSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private KostOwnerPO kostOwner = new KostOwnerPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);


    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String ownerPhone = JavaHelpers.getPropertyValue(consultantProperty,"ownerPhoneHostile_" + Constants.ENV);

    @When("user search by owner phone on kost owner page")
    public void user_search_owner_phone_on_kost_owner_page() throws InterruptedException {
        kostOwner.setOwnerPhone(ownerPhone);
        kostOwner.clickOnSearchButton();
    }

    @Then("user verify that owner have hostile label")
    public void user_verify_that_owner_have_hostile_label() {
        Assert.assertEquals(kostOwner.getHostileLabel(), "Hostile", "Label Not "+ "Hostile");
    }

    @When("user search kos/apartment name {string} in admin kos owner page")
    public void user_search_kos_name_in_admin_kos_owner_page(String kosName) throws InterruptedException {
        kostOwner.searchKosName(kosName);
    }
    @And("user filter edited - ready to verify")
    public void user_filter_edited_ready_to_verify() throws InterruptedException {
        kostOwner.editedReadyToVerify();
    }

    @When("user delete the kos in admin kos owner")
    public void user_delete_the_kos_in_admin_kos_owner() throws InterruptedException {
        kostOwner.clickOnFirstDeleteButton();
        driver.switchTo().alert().accept();
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
    }

    @When("user verify the kos in admin kos owner")
    public void user_verify_the_kos_in_admin_kos_owner() throws InterruptedException {
        kostOwner.clickOnFirstVerifyButton();
    }

    @When("user click BBK Data button")
    public void user_click_BBK_Data_button() throws InterruptedException {
        kostOwner.clickOnFirstBBKDataButton();
    }

    @When("user click reject button in admin kos owner page")
    public void user_click_reject_button_in_admin_kos_owner_page() throws InterruptedException {
        kostOwner.clickOnFirstRejectButton();
    }

    @When("user click checkbox {string} in reject reason page below {string}")
    public void user_click_checkbox_in_reject_reason_page_below(String selection, String section) throws InterruptedException {
        kostOwner.clickOnRejectReasonCheckbox(section, selection);
    }

    @When("user click reject button in kos owner reject reason")
    public void user_click_reject_button_in_kos_owner_reject_reason() throws InterruptedException {
        kostOwner.clickOnRejectButton();
    }

    @When("user click send in send reject pop up")
    public void user_click_send_in_send_reject_pop_up() throws InterruptedException {
        kostOwner.clickOnSendButton();
    }

    @Then("user see status is {string} in admin kos owner page")
    public void user_see_status_is_in_admin_kos_owner_page(String status) {
        Assert.assertEquals(kostOwner.getKosStatusLabel(), status, "Kos status is wrong");
    }
}
