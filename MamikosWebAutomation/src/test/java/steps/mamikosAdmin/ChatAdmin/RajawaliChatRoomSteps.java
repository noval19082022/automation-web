package steps.mamikosAdmin.ChatAdmin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.ChatAdmin.ChatAdminPO;
import pageobjects.mamikosAdmin.LeftMenuPO;
import utilities.ThreadManager;

public class RajawaliChatRoomSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private LeftMenuPO leftMenu = new pageobjects.mamikosAdmin.LeftMenuPO(driver);
    private ChatAdminPO chatAdmin = new ChatAdminPO(driver);

    @And("user go to Rajawali Chat Room")
    public void user_go_to_rajawali_chat_room() throws InterruptedException {
        leftMenu.clickOnChatRoomMenu();
        chatAdmin.clickOnConsultantButton();
    }

    @When("user click on the Group Chat")
    public void user_click_on_the_group_chat() throws InterruptedException {
        chatAdmin.clickOnTenantChat();

    }

    @Then("user able to see Kos Name")
    public void user_able_to_see_kost_name() throws InterruptedException {
        chatAdmin.verifyKosTitle();
        chatAdmin.setKosTitle(chatAdmin.getChatKosTitle());
    }

    @When("user click on Kos Name")
    public void user_click_on_kost_name() throws InterruptedException {
        chatAdmin.clickOnKosTitle();
    }

    @Then("user will directed to Kos Detail in new tab")
    public void user_will_directed_to_kost_detail_in_newTab() {
        Assert.assertEquals(chatAdmin.getKosTitle(), chatAdmin.getKosDetailTitle(), "Kost not match.");
    }
}