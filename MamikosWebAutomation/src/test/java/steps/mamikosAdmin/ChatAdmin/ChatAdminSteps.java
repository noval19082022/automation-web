package steps.mamikosAdmin.ChatAdmin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import pageobjects.mamikosAdmin.ChatAdmin.ChatAdminPO;
import pageobjects.mamikosAdmin.KostReview.KostReviewPO;
import pageobjects.mamikosAdmin.LeftMenuPO;
import utilities.ThreadManager;

public class ChatAdminSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ChatAdminPO chatAdmin = new ChatAdminPO(driver);


    @When("user click button consultant on field chat room")
    public void user_click_button_consultant_on_field_chat_room() throws InterruptedException {
        chatAdmin.clickOnConsultantButton();
    }

    @And("user click one of chat from chat list")
    public void user_click_one_of_chat_from_chat_list() throws InterruptedException{
        chatAdmin.clickOnChatList();
    }

    @And("user click last image on chat")
    public void user_click_last_image_on_chat() throws InterruptedException{
        int totalImage = chatAdmin.getTotalImage();
        chatAdmin.clickOnImageChat(totalImage-1);
    }

    @Then("user see image on pop up")
    public void user_see_image_on_pop_up() {
        Assert.assertTrue(chatAdmin.imageOnPopupPresent(),"image not present");
    }

    @When("user click close on popup image")
    public void user_click_close_on_popup_image() throws InterruptedException{
        chatAdmin.clickOnClosePopUpButton();
    }

    @Then("pop up image will be close")
    public void pop_up_image_will_be_close() {
        Assert.assertFalse(chatAdmin.imageOnPopupPresent(),"image present");
    }
}
