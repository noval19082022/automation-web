package steps.mamikosAdmin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.TransferDepositTenantPO;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class TransferDepositTenantSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JavaHelpers java = new JavaHelpers();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private TransferDepositTenantPO transferDepositTenant = new TransferDepositTenantPO(driver);

    @When ("user click Tab {string}")
    public void user_click_tab(String tab) throws InterruptedException{
        transferDepositTenant.chooseTabOnTransferDeposit(tab);
    }

    @And("user click button detail on field action")
    public void user_click_button_detail_on_field_action() throws InterruptedException{
        transferDepositTenant.clickOnDetailButton();
    }

    @Then("user will be see pop up with title {string}")
    public void user_will_be_see_popUp(String popUp) throws InterruptedException{
        Assert.assertTrue(transferDepositTenant.popUpDetailTransferAppear(), "PopUp Detail Transfer isn't appear");
        Assert.assertEquals(transferDepositTenant.getTitlePopUpDetailTransfer(), popUp, "Title doesn't match");
    }

    @And("user click button tandai sudah transfer on field action")
    public void user_click_button_tandai_sudah_transfer_on_field_action() throws InterruptedException{
        transferDepositTenant.clickOnTandaiSudahTransferButton();
    }

    @Then("user will be see pop up with title {string} on tab Failed")
    public void user_will_be_see_popUp_on_tab_failed(String popUp) throws InterruptedException{
        Assert.assertTrue(transferDepositTenant.popUpTandaiSudahTransferAppear(), "PopUp Tandai Sudah Transfer isn't appear");
        Assert.assertEquals(transferDepositTenant.getTitlePopUpTandaiSudahTransfer(), popUp, "Title doesn't match");
    }

    @And("user click button transfer ulang on field action")
    public void user_click_button_transfer_ulang_on_field_action() throws InterruptedException{
        transferDepositTenant.clickOnTransferUlangButton();
    }

    @Then("user will be see search column and detail tab Failed")
    public void user_will_be_see_search_column_and_detail_tab_failed() throws InterruptedException{
        Assert.assertTrue(transferDepositTenant.searchColumnIsAppeared(),"search column isn't appear");
        String[] dataField = {"Detail Tenant",
                "Detail Kos",
                "Transfer Due Date",
                "Transfer Failed",
                "Tenant Bank Account",
                "Sisa Deposit",
                "Action"};
        for (int i = 0; i <7; i++){
            Assert.assertEquals(transferDepositTenant.getNameColumnTabFailed(i).toLowerCase(), dataField[i].toLowerCase(), "Value not match");
        }
    }

    @Then("user will be see search column and detail tab Process")
    public void user_will_be_see_search_column_and_detail_tab_process() throws InterruptedException{
        Assert.assertTrue(transferDepositTenant.searchColumnIsAppeared(),"search column isn't appear");
        String[] dataField = {"Detail Tenant",
                "Detail Kos",
                "Transfer Success At",
                "Tenant Bank Account",
                "Sisa Deposit",
                "Status",
                "Action"};
        for (int i = 0; i <7; i++){
            Assert.assertEquals(transferDepositTenant.getNameColumnTabFailed(i).toLowerCase(), dataField[i].toLowerCase(), "Value not match");
        }
    }

    @And("user see tenant name, kost name, bank, nomor rekening, nama rekening, transfer due date, transfer success, sisa deposit and button kembali")
    public void user_see_all_label_on_popup() throws InterruptedException{
        for (int i = 0; i <8; i++){
            Assert.assertTrue(transferDepositTenant.labelOnPopUpIsAppeared(i), "label isn't appear" + transferDepositTenant.labelOnPopUpIsAppeared(i));
        }
        Assert.assertTrue(transferDepositTenant.buttonBackIsAppeared(), "button back isn't appear");
    }

    @And("user click button receipt on field action")
    public void user_click_button_receipt_on_field_action() throws InterruptedException{
        transferDepositTenant.clickOnReceiptButton();
    }

    @Then("system display message success download {string}")
    public void system_display_message_success_download(String message) {
        Assert.assertEquals(transferDepositTenant.getSuccessDownloadMessage(),message,"message isn't match");
    }

    @And("user click button transfer on field action")
    public void user_click_button_transfer_on_field_action() throws InterruptedException{
        transferDepositTenant.clickOnTransferButton();
    }

    @When("user click button back on popup")
    public void user_click_button_back_on_popup() throws InterruptedException{
        transferDepositTenant.clickOnBackButton();
    }

    @Then("user will be see search column and detail tab Confirmed")
    public void user_will_be_see_search_column_and_detail_tab_confirmed() throws InterruptedException{
        Assert.assertTrue(transferDepositTenant.searchColumnIsAppeared(),"search column isn't appear");
        String[] dataField = {"Detail Tenant",
                "Detail Kos",
                "Transfer Due Date",
                "Tenant Bank Account",
                "Sisa Deposit",
                "Action"};
        for (int i = 0; i <6; i++){
            Assert.assertEquals(transferDepositTenant.getNameColumnTabFailed(i).toLowerCase(), dataField[i].toLowerCase(), "Value not match");
        }
    }

    @When("user click button transfer ulang on popup")
    public void user_click_button_transfer_ulang_on_popup() throws InterruptedException{
        transferDepositTenant.clickOnTransferUlangButtonPopUp();
    }

    @And("user will be see button download is not available")
    public void user_will_be_see_button_download_is_not_available() {
        Assert.assertFalse(transferDepositTenant.buttonDownloadIsAppeared(),"Button is Appeared");
    }

    @And("user input search value {string}")
    public void user_input_search_value(String searchText) throws InterruptedException{
        transferDepositTenant.inputPhoneNumber(searchText);
    }

    @Then("system display transfer sisa deposit list {string}")
    public void system_display_field_transfer_sisa_deposit_list(String phoneNumber) throws InterruptedException{
        transferDepositTenant.verifyListShortByPhoneNumber(phoneNumber);
    }

    @Then("system display transfer sisa deposit list sort by invalid search value {string}")
    public void system_display_transfer_sisa_deposit_list_sort_by_invalid_search_value(String text) throws InterruptedException{
        Assert.assertEquals(transferDepositTenant.getInvalidPhoneNumberText(),text,"Text isn't match");
    }

    @And("user click button search on page transfer sisa deposit")
    public void user_lick_button_search_invoice() throws InterruptedException{
        transferDepositTenant.clickOnSearchButton();
    }
}
