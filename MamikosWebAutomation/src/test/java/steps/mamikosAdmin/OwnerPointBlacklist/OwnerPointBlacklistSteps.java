package steps.mamikosAdmin.OwnerPointBlacklist;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.OwnerPointBlacklist.OwnerPointBlacklistPO;
import utilities.ThreadManager;

import java.util.List;
import java.util.Map;

public class OwnerPointBlacklistSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private OwnerPointBlacklistPO blacklistPO = new OwnerPointBlacklistPO(driver);

    @And("user click switch status on {string} user type")
    public void user_click_switch_status_on_user_type(String status) throws InterruptedException {
        blacklistPO.clickOnSwitchStatusButton(status);
    }

    @Then("user verify success alert on Manage Owner Point Blacklist with {string}")
    public void user_verify_success_alert_for_changing_status_with(String message) throws InterruptedException {
        Assert.assertTrue(blacklistPO.isSuccessAlertAppear(), "Success alert is not appeared");
        Assert.assertEquals(blacklistPO.getSuccessAlertText().substring(2,48), message, "Success alert message is not equal to " + message);
    }

    @And("user revert back {string} to {string}")
    public void user_revert_back(String user, String status) throws InterruptedException {
        blacklistPO.clickOnRevertSwitchStatusButton(user);
        Assert.assertEquals(blacklistPO.getBlacklistStatus(status), status, "Blacklist status is not equal to " + status);
    }

    @And("user click on Add User Type button")
    public void user_click_on_Add_User_Type_button() throws InterruptedException {
        blacklistPO.clickOnAddUserTypeButton();
    }

    @And("user select user type data with {string}")
    public void user_select_user_type_data_with(String value) throws InterruptedException {
        blacklistPO.selectUserTypeValue(value);
    }

    @And("user select status as {string}")
    public void user_select_status_as(String status) throws InterruptedException {
        blacklistPO.selectStatusRadioButton(status);
    }

    @And("user click save button on Add User Type")
    public void user_click_save_button_on_Add_User_Type() throws InterruptedException {
        blacklistPO.clickOnSaveButton();
    }

    @When("user click delete button on {string}")
    public void user_click_delete_button_on(String userType) throws InterruptedException {
        blacklistPO.clickOnDeleteButton(userType);
    }

    @Then("user verify Manage Owner Point Blacklist page items")
    public void user_verify_Manage_Owner_Point_Blacklist_page_items(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        int i=0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(blacklistPO.getColumnName(i),content.get("Head Table"),"Table Segment should contain " + content.get("Head Table"));
            i++;
        }
    }
}
