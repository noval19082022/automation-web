package steps.mamikosAdmin;

import io.cucumber.java.en.And;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikosAdmin.LoginPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class AdminLoginSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private LoginPO login = new LoginPO(driver);

    //Admin Loyalty Data
    private String pointAndReward="src/test/resources/testdata/mamikos/pointAndReward.properties";
    private String adminLoyaltyEmail = JavaHelpers.getPropertyValue(pointAndReward,"adminLoyaltyEmail_" + Constants.ENV);
    private String adminLoyaltyPassword = JavaHelpers.getPropertyValue(pointAndReward,"adminLoyaltyPassword_" + Constants.ENV);

    //Admin Consultant Data
    private String consultantAccount="src/test/resources/testdata/consultant/datauser.properties";
    private String consultantEmail = JavaHelpers.getPropertyValue(consultantAccount,"consultantEmail_" + Constants.ENV);
    private String consultantPassword = JavaHelpers.getPropertyValue(consultantAccount,"consultantPassword_" + Constants.ENV);

    //Admin Dom
    private String adminDomAccount="src/test/resources/testdata/mamikos/DOM.properties";
    private String adminDomEmail = JavaHelpers.getPropertyValue(adminDomAccount,"adminDomEmail_" + Constants.ENV);
    private String adminDomPassword = JavaHelpers.getPropertyValue(adminDomAccount,"adminDomPassword_" + Constants.ENV);

    //Admin Occupancy And Billing Data
    private String obsPropFile = "src/test/resources/testdata/mamikos/OB.properties";
    private String obsEmail = JavaHelpers.getPropertyValue(obsPropFile, "riniAdminEmail_" + Constants.ENV);
    private String obsPass = JavaHelpers.getPropertyValue(obsPropFile, "riniAdminPass_" + Constants.ENV);

    @And("user logs in to Mamikos Admin via credentials as {string}")
    public void user_logs_in_to_mamikos_admin_via_credentials_as(String type) throws InterruptedException {
        String email = "";
        String password = "";

        if (type.equalsIgnoreCase("admin loyalty")) {
            email = adminLoyaltyEmail;
            password = adminLoyaltyPassword;
        }else if(type.equalsIgnoreCase("admin consultant")){
            email = consultantEmail;
            password = consultantPassword;
        }else if(type.equalsIgnoreCase("admin DOM")){
            email = adminDomEmail;
            password = adminDomPassword;
        } else if(type.equalsIgnoreCase("admin ob")) {
            email= obsEmail;
            password= obsPass;
        } else if (type.equalsIgnoreCase("admin automation")) {
            email = Constants.CONSULTANT_EMAIL;
            password = Constants.CONSULTANT_PASSWORD;
        }
        login.enterCredentialsAndClickOnLoginButton(email, password);
    }

}
