package steps.mamikosAdmin;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.ListKostLevelPO;
import utilities.ThreadManager;

public class KostLevelSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ListKostLevelPO list = new ListKostLevelPO(driver);

    @When("user click on page number {string} kost list")
    public void user_click_on_page_number_kost_list(String pageNumber) throws InterruptedException {
        list.clickOnPageNumber(pageNumber);
    }

    @Then("system display kost list page number {string} is active")
    public void system_display_kost_list_page_number_is_active(String pageNumber) {
        Assert.assertTrue(list.pageNumberButtonIsActive(pageNumber).contains("active"), "Button is not active");
    }
}
