package steps.mamikosAdmin;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikosAdmin.ActivityPO;
import pageobjects.mamikosAdmin.OwnerRoomGroupPO;
import pageobjects.mamikosAdmin.SegmentPO;
import pageobjects.mamikosAdmin.UserPointPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class PointManagementSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SegmentPO segment = new SegmentPO(driver);
    private JavaHelpers java = new JavaHelpers();
    private UserPointPO userPoint = new UserPointPO(driver);
    private ActivityPO activity = new ActivityPO(driver);
    private OwnerRoomGroupPO roomGroup = new OwnerRoomGroupPO(driver);

    //Test Data Point And Reward
    private String pointAndReward="src/test/resources/testdata/mamikos/pointAndReward.properties";
    private String segmentType = JavaHelpers.getPropertyValue(pointAndReward,"segmentType_" + Constants.ENV);
    private String segmentTypeWithSpecialCharacter = JavaHelpers.getPropertyValue(pointAndReward,"segmentTypeWithSpecialCharacter_"+Constants.ENV);
    private String segmentTitle = JavaHelpers.getPropertyValue(pointAndReward,"segmentTitle_" + Constants.ENV);
    private String kostLevelForAddSegment = JavaHelpers.getPropertyValue(pointAndReward,"kostLevelForAddSegment_"+Constants.ENV);
    private String roomLevelForAddSegment = JavaHelpers.getPropertyValue(pointAndReward,"roomLevelForAddSegment_"+Constants.ENV);
    private String popUpConfirmationTitleBlacklist = JavaHelpers.getPropertyValue(pointAndReward,"popUpConfirmationTitleBlacklist_"+Constants.ENV);
    private String popUpConfirmationTitleWhitelist = JavaHelpers.getPropertyValue(pointAndReward, "popUpConfirmationTitleWhitelist_"+ Constants.ENV);
    private String popUpConfirmationBodyBlacklist = JavaHelpers.getPropertyValue(pointAndReward,"popUpConfirmationBodyBlacklist_"+Constants.ENV);
    private String popUpConfirmationBodyWhitelist = JavaHelpers.getPropertyValue(pointAndReward,"popUpConfirmationBodyWhitelist_"+Constants.ENV);
    private String pointAmount = JavaHelpers.getPropertyValue(pointAndReward,"pointAmount_"+Constants.ENV);
    private String noteAdjustPointTopup =JavaHelpers.getPropertyValue(pointAndReward, "noteAdjustPointTopup_"+Constants.ENV);
    private String noteAdjustPointTopdown = JavaHelpers.getPropertyValue(pointAndReward, "noteAdjustPointTopdown_" + Constants.ENV);
    private String activityKeyOwner = JavaHelpers.getPropertyValue(pointAndReward, "activityKeyOwner_" + Constants.ENV);
    private String activityNameOwner = JavaHelpers.getPropertyValue(pointAndReward, "activityNameOwner_" + Constants.ENV);
    private String activityKeyTenant = JavaHelpers.getPropertyValue(pointAndReward, "activityKeyTenant_" + Constants.ENV);
    private String activityNameTenant = JavaHelpers.getPropertyValue(pointAndReward, "activityNameTenant_" + Constants.ENV);
    private String activityTargetOwner = JavaHelpers.getPropertyValue(pointAndReward, "activityTargetOwner_" + Constants.ENV);
    private String activityTargetTenant = JavaHelpers.getPropertyValue(pointAndReward, "activityTargetTenant_" + Constants.ENV);
    private String activityTitleOwner = JavaHelpers.getPropertyValue(pointAndReward, "activityTitleOwner_" + Constants.ENV);
    private String activityTitleTenant = JavaHelpers.getPropertyValue(pointAndReward, "activityTitleTenant_" + Constants.ENV);
    private String activityDescription = JavaHelpers.getPropertyValue(pointAndReward, "activityDescription_" + Constants.ENV);
    private String activityImage = JavaHelpers.getPropertyValue(pointAndReward, "activityImage_" + Constants.ENV);
    private String activitySequence = JavaHelpers.getPropertyValue(pointAndReward, "activitySequence_" + Constants.ENV);
    private String activityStatus = JavaHelpers.getPropertyValue(pointAndReward, "activityStatus_" + Constants.ENV);
    private String roomGroupFloor = JavaHelpers.getPropertyValue(pointAndReward, "roomGroupFloor_" + Constants.ENV);
    private String roomGroupFloorHigher = JavaHelpers.getPropertyValue(pointAndReward, "roomGroupFloorHigher_" + Constants.ENV);
    private String roomGroupCeil = JavaHelpers.getPropertyValue(pointAndReward, "roomGroupCeil_" + Constants.ENV);
    private String roomGroupFloorUpdated = JavaHelpers.getPropertyValue(pointAndReward, "roomGroupFloorUpdated_" + Constants.ENV);
    private String roomGroupCeilUpdated = JavaHelpers.getPropertyValue(pointAndReward, "roomGroupCeilUpdated_" + Constants.ENV);
    private String tenantNameKeyword = JavaHelpers.getPropertyValue(pointAndReward, "tenantNameKeyword_" + Constants.ENV);
    private String phoneNumberKeyword = JavaHelpers.getPropertyValue(pointAndReward, "phoneNumberKeyword_" + Constants.ENV);

    @And("user clicks on Add Segment button")
    public void user_clicks_on_add_segment_button() throws InterruptedException {
        segment.clickOnAddSegmentButton();
    }

    @Then("user see add activity button is displayed")
    public void user_see_add_activity_button() {
        Assert.assertTrue(activity.verifyActivityButton());
    }

    @Then("user see keyword filter field is displayed")
    public void user_see_keyword_filter_field() {
        Assert.assertTrue(activity.verifyKeywordFilterField());
    }

    @And("user see filter status button is displayed")
    public void user_see_filter_status() {
        Assert.assertTrue(activity.verifyFilterStatus());
    }

    @And("user see search button is displayed")
    public void user_see_search_button() {
        Assert.assertTrue(userPoint.verifySearchButton());
    }

    @And("user see edit button is displayed")
    public void user_see_edit_button() {
        Assert.assertTrue(activity.verifyEditButton());
    }

    @And("user see delete button is displayed")
    public void user_see_delete_button() {
        Assert.assertTrue(activity.verifyDeleteButton());
    }

    @And("user see pagination is displayed")
    public void user_see_pagination() {
        Assert.assertTrue(activity.verifyPagination());
    }

    @And("user see id column is displayed")
    public void user_see_id_column() {
        Assert.assertTrue(activity.verifyIdColumn());
    }

    @And("user see key column is displayed")
    public void user_see_key_column() {
        Assert.assertTrue(activity.verifyKeyColumn());
    }

    @And("user see name column is displayed")
    public void user_see_name_column() {
        Assert.assertTrue(activity.verifyNameColumn());
    }

    @And("user see title column is displayed")
    public void user_see_title_column() {
        Assert.assertTrue(activity.verifyTitleColumn());
    }

    @And("user see target column is displayed")
    public void user_see_target_column() {
        Assert.assertTrue(activity.verifyTargetColumn());
    }

    @And("user see status column is displayed")
    public void user_see_status_column() {
        Assert.assertTrue(activity.verifyStatusColumn());
    }

    @And("user see order column is displayed")
    public void user_see_order_column() {
        Assert.assertTrue(activity.verifyOrderColumn());
    }

    @And("user see actions column is displayed")
    public void user_see_actions_column() {
        Assert.assertTrue(activity.verifyActionsColumn());
    }

    @And("user fills out form create segment for {string} and click save")
    public void user_fills_out_form_create_segment_for_and_click_save(String type) throws InterruptedException, ParseException {
        String newSegmentType = "";
        String newSegmentTitle = "";
        String kostLevel = "";
        String roomLevel = "";
        String today = java.updateTime("yy MMM dd ss", java.getTimeStamp("yy MMM dd ss"), "yyMMMddss", 0, 0, 0, 0);
        switch (type) {
            case "add":
                newSegmentType = segmentType + today + "_added";
                newSegmentTitle = segmentTitle + today + "_added";
                kostLevel = kostLevelForAddSegment;
                roomLevel = roomLevelForAddSegment;
                break;
            case "edit":
                newSegmentType = segmentType + today + "_updated";
                newSegmentTitle = segmentTitle + today + "_updated";
                kostLevel = kostLevelForAddSegment;
                roomLevel = roomLevelForAddSegment;
                break;
            case "type empty":
                newSegmentTitle = segmentTitle + today;
                kostLevel = kostLevelForAddSegment;
                roomLevel = roomLevelForAddSegment;
                break;
            case "type special character":
                newSegmentType = segmentTypeWithSpecialCharacter;
                newSegmentTitle = segmentTitle + today;
                kostLevel = kostLevelForAddSegment;
                roomLevel = roomLevelForAddSegment;
                break;
            case "title empty":
                newSegmentType = segmentType + today + "_added";
                kostLevel = kostLevelForAddSegment;
                roomLevel = roomLevelForAddSegment;
                break;
            default:
                newSegmentType = segmentType + today + "_added";
                newSegmentTitle = segmentTitle + today + "_added";
                break;
        }
        segment.setNewSegmentType(newSegmentType);
        segment.setNewSegmentTitle(newSegmentTitle);

        if(type.contains("New GoldPlus")){
            segment.selectKostLevelOnCreateSegmentPage(type.replace("add tenant - ",""));
            segment.selectRoomLevelOnCreateSegmentPage(type.replace("add tenant - ",""));
            segment.selectTargetCreateSegmentPage("Tenant");
        } else {
            segment.selectKostLevelOnCreateSegmentPage(kostLevel);
            segment.selectRoomLevelOnCreateSegmentPage(roomLevel);
        }
        segment.clickOnSaveButton();
    }

    @Then("user verify allert success {string} and {string}")
    public void user_verify_allert_success_and(String titleMessage, String contentMessage) throws InterruptedException {
        Assert.assertEquals(segment.getTitleMessageAllert(), titleMessage,"Title message is false");
        Assert.assertTrue(segment.getContentMessageAllert().contains(contentMessage), "Content message is false");
    }

    @And("user verify new segment displayed")
    public void user_verify_new_segment_displayed() {
        Assert.assertTrue(segment.checkNewSegmentPresentInSegmentPage(segmentType));
    }

    @Then("user see new segment added displayed on Manage Point-Setting menu")
    public void user_see_new_segment_added_displayed_on_manage_point_setting_menu() {
        Assert.assertTrue(segment.checkNewSegmentPresentInSettingPage(segmentTitle));
    }

    @And("user click {string} on segment")
    public void user_click_on_segment(String action) throws InterruptedException {
        int segmentNumber = segment.getAllSegmentNumber();
        for(int i=0;i<segmentNumber;i++){
            if(segment.getSegmentName(i).contains(segmentType)) {
                if(action.equals("edit")){
                    segment.clickOnEditSegment(i+1);
                    break;
                }
                else if(action.equals("delete")){
                    segment.clickOnDeleteSegment(i+1);
                    break;
                }
            }
        }
    }

    @And("user verify new segment not displayed")
    public void user_verify_new_segment_not_displayed() {
        Assert.assertFalse(segment.checkNewSegmentPresentInSegmentPage(segmentType));
    }

    @Then("user see new segment added not displayed on Manage Point-Setting menu")
    public void user_see_new_segment_added_not_displayed_on_manage_point_setting_menu() {
        Assert.assertFalse(segment.checkNewSegmentPresentInSettingPage(segmentType));
    }

    @Then("user see segment validation {string}")
    public void user_see_segment_validation(String validationMessage) {
        Assert.assertEquals(segment.getSegmentValidationMessage(),validationMessage,"Validation message is not match");
    }

    @When("user filter user point by keyword {string}")
    public void user_filter_user_point_by_keyword(String keyword) {
        String newKeyword = "";
        if(keyword.equals("tenant name")){
            newKeyword = tenantNameKeyword;
        }
        else if(keyword.equals("phone number")){
            newKeyword = phoneNumberKeyword;
        }
        userPoint.setKeywordSearchField(newKeyword);
    }

    @Then("system display list user point contains {string}")
    public void system_display_list_user_point_contains(String keyword) {
        String keywordResult = keyword.toLowerCase();
        int resultNumber = userPoint.getFilterResultNumber();
        for (int i = 1 ; i <= resultNumber ; i++){
            Assert.assertTrue(userPoint.getFilterResultList(i,1).toLowerCase().contains(keywordResult) || userPoint.getFilterResultList(i,2).contains(keywordResult));
        }
    }

    @And("user clicks on Search button")
    public void user_clicks_on_search_button() throws InterruptedException {
        userPoint.clickOnSearchButton();
    }

    @And("user select filter User {string}")
    public void user_select_filter_user(String userType) throws InterruptedException {
        userPoint.clickOnUserDropdown();
        userPoint.selectUserFilter(userType);
    }

    @Then("system display list user point as {string}")
    public void system_display_list_user_point_as(String userPointType) {
        for(int i = 0 ; i < userPoint.getFilterResultNumber() ; i++){
            if(userPointType.equals("Owner") || userPointType.equals("Tenant")){
                Assert.assertEquals(userPoint.getTextUserColumn(i),userPointType, "User is not match");
            }
            else if (userPointType.equals("Whitelist") || userPointType.equals("Blacklist")){
                Assert.assertTrue(userPoint.getTextStatusColumn(i).contains(userPointType), "Status is not match");
            }
        }
    }

    @And("user select filter Status {string}")
    public void user_select_filter_status(String status) throws InterruptedException {
        userPoint.clickOnStatusDropdown();
        userPoint.selectStatusFilter(status);
    }

    @And("user clicks Total Point header")
    public void user_clicks_total_point_header() throws InterruptedException {
        userPoint.clickOnTotalPointHeader();
    }

    @Then("user verify total point sorted {string}")
    public void user_verify_total_point_sorted(String condition) {
        List<Long> obtainedList = new ArrayList<>();
        for(int i = 0 ; i < userPoint.getFilterResultNumber() ; i++){
            obtainedList.add(Long.valueOf(userPoint.getTextTotalPoint(i)));
        }

        List<Long> sortedList = new ArrayList<>(obtainedList);
        Collections.sort(sortedList);

        if(condition.equals("descending")){
            Collections.reverse(sortedList);
        }
        Assert.assertEquals(obtainedList, sortedList);
    }

    @And("user {string} user point and click {string} on pop up confirmation")
    public void user_user_point_and_click_on_pop_up_confirmation(String action, String confirmation) throws InterruptedException {
        String initialStatus = "";
        String popUpConfirmationTitle = "";
        String popUpConfirmationBody = "";
        if(action.equals("Blacklist")){
            initialStatus = "Whitelist";
            popUpConfirmationTitle = popUpConfirmationTitleBlacklist;
            popUpConfirmationBody = popUpConfirmationBodyBlacklist;
        }
        else {
            initialStatus = "Blacklist";
            popUpConfirmationTitle = popUpConfirmationTitleWhitelist;
            popUpConfirmationBody = popUpConfirmationBodyWhitelist;
        }
        userPoint.clickOnUserPointStatus(initialStatus);
        Assert.assertTrue(userPoint.getTitlePopUpConfirmationChangeStatus().contains(popUpConfirmationTitle));
        Assert.assertTrue(userPoint.getBodyPopUpConfirmationChangeStatus().contains(popUpConfirmationBody));
        userPoint.clickOnPopUpCOnfirmationButton(confirmation);
    }

    @When("user clicks Adjust Point icon")
    public void user_clicks_Adjust_Point_icon() throws InterruptedException {
        userPoint.clickOnAdjustPointIcon();
    }

    @When("user choose adjustment type {string}")
    public void user_choose_adjustment_type(String adjustmentType) throws InterruptedException {
        userPoint.choosePointAdjustmentType(adjustmentType);
    }

    @When("user fills out point amount")
    public void user_fills_out_point_amount() {
        userPoint.setPointAmount(pointAmount);
    }

    @When("user fills out note for {string}")
    public void user_fills_out_note_for(String noteType) {
        String type = "";
        if (noteType.equals("topup")){
            type = noteAdjustPointTopup;
        }
        else if (noteType.equals("topdown")){
            type = noteAdjustPointTopdown;
        }
        userPoint.setPointAdjusmentNote(type);
    }

    @When("user clicks on Submit button on Adjust Point form")
    public void user_clicks_on_Submit_button_on_Adjust_Point_form() throws InterruptedException {
        userPoint.clickOnSubmitAdjustPointButton();
    }

    @When("user clicks on Add Activity button")
    public void user_clicks_on_Add_Activity_button() throws InterruptedException {
        activity.clickOnAddActivityButton();
    }

    @When("user fills out form create activity for {string} and click save")
    public void user_fills_out_form_create_activity_for_and_click_save(String type) throws InterruptedException, ParseException {
        String newActivityKey="";
        String newActivityName="";
        String newActivityTarget="";
        String newActivityTitle="";
        String newActivityDescription="";
        String newActivitySequence="";
        String newActivityStatus="";
        String newUploadImageActivity="";
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "yyyyMMMdd", 0, 0, 0, 0);
        switch (type) {
            case "add owner target":
                newActivityKey = activityKeyOwner + today + "_added";
                newActivityName = activityNameOwner;
                newActivityTarget = activityTargetOwner;
                newActivityTitle = activityTitleOwner;
                newActivityDescription = activityDescription;
                newActivitySequence = activitySequence;
                newActivityStatus = activityStatus;
                newUploadImageActivity = activityImage;
                break;
            case "add tenant target":
                newActivityKey = activityKeyTenant + today + "_added";
                newActivityName = activityNameTenant;
                newActivityTarget = activityTargetTenant;
                newActivityTitle = activityTitleTenant;
                newActivityDescription = activityDescription;
                newActivitySequence = activitySequence;
                newActivityStatus = activityStatus;
                newUploadImageActivity = activityImage;
                break;
            case "empty field":
                newActivityTarget = activityTargetTenant;
                newActivityTitle = activityTitleTenant;
                newActivityDescription = activityDescription;
                newActivitySequence = activitySequence;
                newActivityStatus = activityStatus;
//                newUploadImageActivity = activityImage;
                break;
            case "edit":
                newActivityKey = activityKeyOwner + today + "_updated";
                newActivityName = activityNameOwner + " Updated";
                newActivityTarget = activityTargetOwner;
                newActivityTitle = activityTitleOwner;
                newActivityDescription = activityDescription;
                newActivitySequence = activitySequence;
                newActivityStatus = activityStatus;
                break;
        }
        activity.setActivityKeyField(newActivityKey);
        activity.setActivityNameField(newActivityName);
        activity.selectActivityTarget(newActivityTarget);
        activity.setActivityTitleField(newActivityTitle);
        activity.setActivityDescription(newActivityDescription);
        activity.setActivitySequenceOrder(newActivitySequence);
        activity.selectActivityStatus(newActivityStatus);
        activity.selectActivityImage(newUploadImageActivity);
        activity.clickOnSaveButton();
    }

    @Then("user see new activity added displayed on Manage Point-Owner Setting menu")
    public void user_see_new_activity_added_displayed_on_Manage_Point_Owner_Setting_menu() {
        Assert.assertTrue(activity.checkNewActivityPresentInSettingPage(activityNameOwner));
    }

    @Then("user see new activity added displayed on Manage Point-Tenant Setting menu")
    public void user_see_new_activity_added_displayed_on_Manage_Point_Tenant_Setting_menu() {
        Assert.assertTrue(activity.checkNewActivityPresentInSettingPage(activityNameTenant));
    }

    @When("user filter activity by keyword {string}")
    public void user_filter_activity_by_keyword(String keyword) {
        activity.setKeywordSearchField(keyword);
    }

    @Then("user verify new activity displayed")
    public void user_verify_new_activity_displayed(DataTable activity) {
        int columnNumber = 6;
        List<List<String>> listActivity = activity.asLists(String.class);
        for (int i=0; i<listActivity.size(); i++) {
            for (int j=1; j<=columnNumber; j++) {
                Assert.assertTrue(userPoint.getFilterResultList(i+1,j+1).contains(listActivity.get(i).get(j-1)));
            }
        }
    }

    @Then("user see validation")
    public void user_see_validation(List<String> validationList) {
        int validationNumber = activity.getActivityValidationNumber();
        for (int i=0; i<validationNumber ; i++){
            Assert.assertEquals(activity.getActivityValidationMessage(i),validationList.get(i));
        }
    }

    @And("user click {string} on activity")
    public void user_click_on_activity(String action) {
        if(action.equals("edit")){
            activity.clickOnEditActivity();
        }
        else if(action.equals("delete")){
            activity.clickOnDeleteActivity();
        }
    }

    @Then("user verify no activity displayed")
    public void user_verify_no_activity_displayed() {
        Assert.assertTrue(activity.checkResultFilterIsNotPresent());
    }

    @Then("user see new activity added not displayed on Manage Point-Owner Setting menu")
    public void user_see_new_activity_added_not_displayed_on_Manage_Point_Owner_Setting_menu() {
        Assert.assertFalse(activity.checkNewActivityPresentInSettingPage(activityNameOwner+" Updated"));
    }

    @Then("user see new activity added not displayed on Manage Point-Tenant Setting menu")
    public void user_see_new_activity_added_not_displayed_on_Manage_Point_Tenant_Setting_menu() {
        Assert.assertFalse(activity.checkNewActivityPresentInSettingPage(activityNameTenant));
    }

    @When("user clicks on Add Owner Room Group button")
    public void user_clicks_on_Add_Owner_Room_Group_button() throws InterruptedException {
        roomGroup.clickOnAddOwnerRoomGroupButton();
    }


    @When("user fills out form create owner room group for {string} and click save")
    public void user_fills_out_form_create_owner_room_group_for_and_click_save(String type) throws InterruptedException {
        String newRoomGroupFloor="";
        String newRoomGroupCeil="";

        switch (type) {
            case "add":
                newRoomGroupFloor = roomGroupFloor;
                newRoomGroupCeil = roomGroupCeil;
                break;
            case "floor field higher":
                newRoomGroupFloor = roomGroupFloorHigher;
                newRoomGroupCeil = roomGroupCeil;
                break;
            case "edit":
                newRoomGroupFloor = roomGroupFloorUpdated;
                newRoomGroupCeil = roomGroupCeilUpdated;
                break;
            case "empty":
                break;
        }
        roomGroup.setRoomGroupFloor(newRoomGroupFloor);
        roomGroup.setRoomGroupCeil(newRoomGroupCeil);
        activity.clickOnSaveButton();
    }


    @And("user verify new room group {string} displayed")
    public void user_verify_new_room_group_displayed(String type) {
        String group="";
        if(type.equals("added")){
            group = roomGroupFloor+"-"+roomGroupCeil;
        }
        else if(type.equals("updated")){
            group = roomGroupFloorUpdated+"-"+roomGroupCeilUpdated;
        }
        Assert.assertTrue(roomGroup.checkRoomGroupIsPresent(group));
    }

    @And("user click {string} on room group")
    public void user_click_on_room_group(String action) throws InterruptedException {
        int roomGroupNumber = roomGroup.getRoomGroupNumber();
        for(int i=0;i<roomGroupNumber;i++){
            if(action.equals("edit")) {
                if (roomGroup.getGroupText(i).contains(roomGroupFloor)) {
                    roomGroup.clickOnEditRoomGroup(i + 1);
                    break;
                }
            }
            else if(action.equals("delete")){
                if (roomGroup.getGroupText(i).contains(roomGroupFloorUpdated)){
                    roomGroup.clickOnDeleteRoomGroup(i+1);
                    break;
                }
            }
        }
    }

    @And("user verify new room group {string} not displayed")
    public void user_verify_new_room_group_not_displayed(String group) {
        Assert.assertFalse(roomGroup.checkRoomGroupIsPresent(group));
    }

    @Then("user see Bulk Adjust Point button")
    public void user_see_Bulk_Adjust_Point_button() throws InterruptedException {
        userPoint.checkBulkAdjustPointButton();
    }

    @And("user see Bulk Update Blacklist button")
    public void user_see_Bulk_Update_Blacklist_button() throws InterruptedException {
        userPoint.checkBulkUpdateBlacklistButton();
    }

    @And("user see Keyword Filter")
    public void user_see_Keyword_Filter() throws InterruptedException {
        userPoint.checkKeywordFilter();
    }

    @And("user see User Filter")
    public void user_see_User_Filter() throws InterruptedException {
        userPoint.checkUserFilter();
    }

    @And("user see Status Filter")
    public void user_see_Status_Filter() throws InterruptedException {
        userPoint.checkStatusFilter();
    }

    @And("user see Search button")
    public void user_see_Search_button() throws InterruptedException {
        userPoint.checkSearchButton();
    }

    @And("user see Name")
    public void user_see_Name() throws InterruptedException {
        userPoint.checkName();
    }

    @And("user see Email")
    public void user_see_Email() throws InterruptedException {
        userPoint.checkEmail();
    }

    @And("user see Phone Number")
    public void user_see_Phone_Number() throws InterruptedException {
        userPoint.checkPhoneNumber();
    }

    @And("user see User")
    public void user_see_User() throws InterruptedException {
        userPoint.checkUser();
    }

    @And("user see Total Point")
    public void user_see_Total_Point() throws InterruptedException {
        userPoint.checkTotalPoint();
    }

    @And("user see Status")
    public void user_see_Status() throws InterruptedException {
        userPoint.checkStatus();
    }

    @And("user see Adjust Point icon")
    public void user_see_Adjust_Point_icon() throws InterruptedException {
        userPoint.checkAdjustPointIcon();
    }

    @And("user see History icon")
    public void user_see_History_icon() throws InterruptedException {
        userPoint.checkHistoryIcon();
    }

    @And("user see Pagination")
    public void user_see_Pagination() throws InterruptedException {
        userPoint.checkPagination();
    }

    @Then("user see at point management segment contains:")
    public void user_see_at_point_management_segment(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        int i=0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(segment.getAllSegmentHeadTable(i),content.get("Head Table"),"Table Segment should contain " + content.get("Head Table"));
            i++;
        }
    }

    @Then("user verify the pagination of Owner Room Group")
    public void user_verify_the_pagination_of_Owner_Room_Group() {
        Assert.assertTrue(roomGroup.isPaginationAppear(), "Pagination in Owner Room Group is not appeared");
    }

    @Then("user verify Manage Point Owner Room Group page items")
    public void user_verify_Manage_Point_Owner_Room_Group_page_items(List<String> data) {
        for (int i = 0; i < data.size(); i++) {
            if(i == 0){
                Assert.assertEquals(roomGroup.getAddOwnerRoomGroupText(), data.get(i), "Add owner room group button text is not equal to " + data.get(i));
            }
            else{
                Assert.assertEquals(roomGroup.getTableTitleText(i), data.get(i), "Table title text is not equal to " + data.get(i));
                Assert.assertTrue(roomGroup.isEditButtonAppeared(), "Edit button in Owner Room Group is not appeared");
                Assert.assertTrue(roomGroup.isDeleteButtonAppeared(), "Delete button in Owner Room Group is not appeared");
                Assert.assertTrue(roomGroup.isPaginationAppear(), "Pagination in Owner Room Group is not appeared");
            }
        }
    }

    @When("user click next page button on manage user point")
    public void user_click_next_page_button_on_manage_user_point() throws InterruptedException {
        userPoint.clickNextPage();
    }

    @Then("next manage user point page will be opened")
    public void next_manage_user_point_page_will_be_opened() {
        String pageIndex = userPoint.getPageIndex();
        if(pageIndex != null){
            Assert.assertEquals(pageIndex, "2", "Page index is not correct");
        }
    }

    @When("user click previous page button on manage user point")
    public void user_click_previous_page_button_on_manage_user_point() throws InterruptedException {
        userPoint.clickPrevPage();
    }

    @Then("previous manage user point page will be opened")
    public void previous_manage_user_point_page_will_be_opened() {
        String pageIndex = userPoint.getPageIndex();
        if(pageIndex != null){
            Assert.assertEquals(pageIndex, "1", "Page index is not correct");
        }
    }

    @Then("manage user point page {string} will be opened")
    public void manage_user_point_page_will_be_opened(String page) {
        String pageIndex = userPoint.getPageIndex();
        if(pageIndex != null) {
            Assert.assertEquals(pageIndex, page, "Page index is not correct");
        }
    }

    @When("user click history icon on manage user point page")
    public void user_click_history_icon_on_manage_user_point_page() throws InterruptedException {
        userPoint.clickHistoryIcon();
    }

    @When("user click back to user point")
    public void user_click_back_to_user_point()throws InterruptedException {
        userPoint.clickOnBacktoUserPointBurron();
    }

    @Then("user see at manage user point history contains:")
    public void user_see_at_manage_user_point_history_contain(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        for (Map<String, String> content : table) {
            Assert.assertTrue(userPoint.getContentManagePointHistoryPage().contains(content.get("Content")),"Review page should contain" + content.get("Content"));
        }
    }

    @When("user choose to filter all activity with value {string}")
    public void user_choose_to_filter_all_activity_with_value(String value) throws InterruptedException {
        userPoint.chooseDropDownAllActivity(value);
        userPoint.clickOnSearchButton();
    }

    @Then("history with selected filter value {string} is displayed")
    public void voucher_with_selected_filter_value_is_displayed(String value){
        Assert.assertTrue(userPoint.getContentManagePointHistoryPage().contains(value));
    }

    @And("user fill Owner Point Expiry in with {string}")
    public void user_fill_Owner_Point_Expiry_in_with(String value){
        userPoint.fillOwnerPointExpiry(value);
    }

    @And("user fill Tenant Point Expiry in with {string}")
    public void user_fill_Tenant_Point_Expiry_in_with(String value){
        userPoint.fillTenantPointExpiry(value);
    }

    @And("user click on Point Expiry Save button")
    public void user_click_on_Point_Expiry_Save_button() throws InterruptedException{
        userPoint.clickOnPointExpirySaveButton();
    }

    @And("user check checkbox total room")
    public void user_check_checkbox_total_room() throws InterruptedException{
        userPoint.clickOnCheckboxTotalRoom();
    }

    @And("user uncheck checkbox total room")
    public void user_uncheck_checkbox_total_room() throws InterruptedException{
        userPoint.clickOnCheckboxTotalRoom();
    }

    @And("user can fill the field")
    public void user_can_fill_the_field() throws InterruptedException{
        String pointLimit = "3000";
        userPoint.enterPointLimit(pointLimit);
    }

    @And("user click button save Terms and Conditions")
    public void user_click_button_save_Terms_and_Conditions() throws InterruptedException{
        userPoint.clickOnSaveTermsAndCondition();
    }

    @And("system display success message")
    public void system_display_success_message() throws InterruptedException{
        Assert.assertTrue(userPoint.successSaveTnCIsDisplayed(), "succes save Terms and Conditions is not present");
    }

    @Then("user see Manage Tenant Point Setting Page")
    public void user_see_manage_tenant_point_setting_page() throws InterruptedException{
        Assert.assertTrue(userPoint.isPageTenantPointSettingVisible(), "tenant point setting page is not present");
    }

    @And("user input point limit install app tenant point setting {string}")
    public void user_input_point_limit_install_app_tenant_point_setting(String pointLimit) throws InterruptedException{
        userPoint.enterPointLimitInstallApp(pointLimit);
    }

    @And("user click cancel button install app and ok on alert")
    public void user_click_cancel_button_install_app() throws InterruptedException{
        userPoint.clickOnPInstallAppCancelButton();
    }

    @And("user click save button install app and ok on alert")
    public void user_click_save_button_install_app() throws InterruptedException{
        userPoint.clickOnPInstallAppSaveButton();
    }

    @And("user click button save Terms and Conditions Tenant Setting")
    public void user_click_button_save_Terms_and_Conditions_tenant_setting() throws InterruptedException{
        userPoint.clickOnSaveTermsAndConditionTenantSetting();
    }

    @And("user click Bulk Update Blacklist")
    public void user_click_bulk_update_blacklist() throws InterruptedException{
        userPoint.clickOnBulkUpdateBlacklistButton();
    }

    @And("user click Bulk Adjust Point")
    public void user_click_bulk_adjust_point() throws InterruptedException{
        userPoint.clickOnBulkAdjustPointButton();
    }

    @Then("user see {string} pop-up appear")
    public void bulk_update_blacklist_popup_appear(String popUp) throws InterruptedException{
        Assert.assertTrue(userPoint.checkBulkUpdateBlacklistPopUp(popUp), "bulk update blacklis popup is not present");
    }

    @Then("success Update Blacklist using csv")
    public void success_update_blacklist_using_csv() throws InterruptedException{
        Assert.assertTrue(userPoint.successSaveTnCIsDisplayed(), "succes upload csv is not present");
    }

    @And("admin master upload csv file {string}")
    public void admin_master_upload_csv_file(String file) throws InterruptedException{
        userPoint.uploadCSVFile(file);
    }

    @And("user click button submit csv bulk blacklist")
    public void user_click_button_submit_csv_bulk_blacklist() {
        userPoint.clickOnSubmitBulkUpdateButton();
    }

    @And("user click button submit csv bulk adjust point")
    public void user_click_button_submit_csv_bulk_adjust_point() {
        userPoint.clickOnSubmitBulkAdjustPointButton();
    }

    @And("user set the default status to Whitelist")
    public void user_set_default_status_to_whitelist() throws InterruptedException {
        userPoint.setDefaultStatusToWhitelist();
    }


}