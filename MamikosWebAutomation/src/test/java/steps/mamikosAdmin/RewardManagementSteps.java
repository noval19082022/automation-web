package steps.mamikosAdmin;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikosAdmin.PropertyPackagePO;
import pageobjects.mamikosAdmin.RewardManagementPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;
import org.testng.Assert;


public class RewardManagementSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private RewardManagementPO rewardManagement = new RewardManagementPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user click button add reward type")
    public void user_click_button_add_reward_type() throws InterruptedException {
        rewardManagement.clickOnAddRewardType();
    }

    @And("user input reward type key {string}")
    public void user_Input_Reward_Type_Key(String rewardKey) throws InterruptedException{
        rewardManagement.setRewardTypeKey(rewardKey);
    }

    @And("user input reward type name {string}")
    public void user_Input_Reward_Type_Name(String rewardName) throws InterruptedException{
        rewardManagement.setRewardTypeName(rewardName);
    }

    @And("user click save button reward type")
    public void user_Click_Save_Button_Reward_Type() throws InterruptedException{
        rewardManagement.clickOnSave();
    }

    @Then("system display success add reward type")
    public void system_Display_Success_Add_Reward_Type() throws InterruptedException{
        Assert.assertTrue(rewardManagement.successAddRewardTypeIsDisplayed(), "succes add reward label is not present");
    }

    @When("user input reward type key {string} on search box")
    public void user_Input_Reward_Type_Key_On_Search_Box(String rewardKey) throws InterruptedException{
        rewardManagement.setOnSearchBoxRewardTypeKey(rewardKey);
    }

    @And("user click button search reward type")
    public void user_Click_Button_Search_Reward_Type() throws InterruptedException{
        rewardManagement.clickOnSearchRewardType();
    }

    @And("user click button edit reward type")
    public void user_Click_Button_Edit_Reward_Type() throws InterruptedException{
        rewardManagement.clickOnEditRewardType();
    }

    @And("system display error message {string}")
    public void system_Display_Error_Message(String text) throws InterruptedException{
        Assert.assertEquals(rewardManagement.getErrorMesssage(),text,"error message don't match");
    }

    @And("user click button delete on manage reward type")
    public void user_click_button_delete_on_manage_reward_type() throws InterruptedException{
        rewardManagement.clickOnDeleteRewardType();
    }

    @And("system display Reward List Management page")
    public void system_Display_Reward_List_Management_Page() throws InterruptedException{
        for (int i = 0; i < 10; i++){
            Assert.assertTrue(rewardManagement.fieldOnTableIsDisplayed(i));
        }
        Assert.assertTrue(rewardManagement.rewardListHeaderIsDisplayed());
        Assert.assertTrue(rewardManagement.filterButtonIsDisplayed());
        Assert.assertTrue(rewardManagement.addRewardButtonIsDisplayed());
    }

    @And("user filter reward name {string}")
    public void user_Filter_Reward_Name(String rewardName) throws InterruptedException{
        rewardManagement.setRewardTypeNameOnFilter(rewardName);
    }

    @And("user click button filter reward")
    public void user_Click_Button_Filter_Reward() throws InterruptedException{
        rewardManagement.clickOnFilterRewardList();
    }

    @And("user click button update reward")
    public void user_Click_Button_Update_Reward() throws InterruptedException{
        rewardManagement.clickOnUpdateRewardList();
    }

    @And("user click checkbox Active")
    public void user_Click_Checkbox() throws InterruptedException{
        rewardManagement.setStatusRewardList();
    }

    @And("user click button update reward on page detail reward")
    public void user_Click_Button_Update_Reward_On_Page_Detail_Reward() throws InterruptedException{
        rewardManagement.clickOnUpdateReward();
    }

    @And("user filter owner user point name {string}")
    public void user_filter_owner_user_point_name(String ownerUserPointName) throws InterruptedException{
        rewardManagement.setOwnerUserPointNameOnFilter(ownerUserPointName);
    }

    @And("user click on whitelist to change blacklist")
    public void user_click_on_whitelist_to_change_blacklist() throws InterruptedException{
        rewardManagement.changeWhitelistToBlacklist();
    }

    @And("user see alert success")
    public void user_see_alert_success() throws InterruptedException{
        Assert.assertTrue(rewardManagement.isAlertSuccessPresent(),"not showing alert success");
    }

    @And("user click on blacklist to change whitelist")
    public void user_click_on_blacklist_to_change_whitelist()throws InterruptedException{
        rewardManagement.changeBlacklistToWhitelist();
    }

}
