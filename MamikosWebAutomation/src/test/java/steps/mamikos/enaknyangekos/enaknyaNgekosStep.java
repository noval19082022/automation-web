package steps.mamikos.enaknyangekos;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.enaknyangekos.enaknyaNgekosPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class enaknyaNgekosStep {
    private WebDriver driver = ThreadManager.getDriver();
    private enaknyaNgekosPO headerEnaknyaNgekos = new enaknyaNgekosPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);



    @When("user is on the LP EnaknyaNgekos")
    public void user_on_landingpage_enaknyangekos()  {
        Assert.assertEquals(headerEnaknyaNgekos.onLandingpageEnaknyangekos(), "#EnaknyaNgekos ala Kos Andalan", "#EnaknyaNgekos ala Kos Andalan");
    }

    @And("scroll the page to Kenapa #EnaknyaNgekos section or more")
    public void user_scroll_page_enaknyangekos() {
        headerEnaknyaNgekos.scrollPageEnaknyangekos();
    }

    @And("click Booking Kos button on the header")
    public void user_click_booking_kos_button() throws InterruptedException {
        headerEnaknyaNgekos.clickBookingKosButton();
    }

    @And("click Promo button on the header")
    public void user_click_promo_button() throws InterruptedException {
        headerEnaknyaNgekos.clickPromoButton();
    }

    @And("click on voucher banner")
    public void user_click_voucher_button() throws InterruptedException {
        headerEnaknyaNgekos.clickVoucherButton();
    }

    @And("click fitur unggulan button on the header")
    public void user_click_fitur_unggulan_button() throws InterruptedException {
        headerEnaknyaNgekos.clickFiturUnggulanButton();
    }

    @And("click product dan layanan button on the header")
    public void user_click_product_dan_layanan_button() throws InterruptedException {
        headerEnaknyaNgekos.clickProductDanLayananButton();
    }

    @And("click Twitter icon on the footer")
    public void user_click_twitter_icon () throws InterruptedException {
        headerEnaknyaNgekos.clickTwitterIcon();
    }

    @And("click Instagram icon on the footer")
    public void user_click_instagram_icon () throws InterruptedException {
        headerEnaknyaNgekos.clickInstagramIcon();
    }

    @And("click Facebook icon on the footer")
    public void user_click_facebook_icon () throws InterruptedException {
        headerEnaknyaNgekos.clickFacebookIcon();
    }

    @And("user click on video thumbnail")
    public void user_click_video_thumbnail () throws InterruptedException {
        headerEnaknyaNgekos.clickVideoThumbnail();
    }

    @Then("user see pop up video player is shown and can play video")
    public void user_view_video() throws InterruptedException {
        headerEnaknyaNgekos.viewVideo();
    }

    @Then("Mulai Cari Kos button is shown on the Navigation bar")
    public void user_view_mulai_cari_kos_button() throws InterruptedException {
        headerEnaknyaNgekos.viewMulaiCariKosButton();
    }

    @Then("user directed to menu booking {string}")
    public void user_direct_to_menu_booking(String message) {
        Assert.assertEquals(headerEnaknyaNgekos.directToMenuBooking(), message, "Error message is not equal to " + message);
    }

    @Then("page auto-scroll to Voucher Promotion section {string}")
    public void user_autoscroll_to_voucher(String message) {
        Assert.assertEquals(headerEnaknyaNgekos.autoscrollToVoucher(), message, "Error message is not equal to " + message);
    }

    @Then("user directed to detail promo page {string}")
    public void user_direct_to_menu_promo(String message) {
        Assert.assertEquals(headerEnaknyaNgekos.directToMenuPromo(), message, "Error message is not equal to " + message);
    }

    @Then("user directed to website twitter")
    public void user_direct_to_website_twitter() throws InterruptedException {
        headerEnaknyaNgekos.directToWebsite();
    }

    @Then("user directed to website instagram")
    public void user_direct_to_website_instagram() throws InterruptedException {
        headerEnaknyaNgekos.directToWebsite();
    }

    @Then("user directed to website facebook")
    public void user_direct_to_website_facebook() throws InterruptedException {
        headerEnaknyaNgekos.directToWebsite();
    }

    @Then("page auto-scroll to Fitur Unggulan Promotion section {string}")
    public void user_autoscroll_to_fitur_unggulan(String message) throws InterruptedException {
        Assert.assertEquals(headerEnaknyaNgekos.autoscrollToFiturUnggulan(), message, "Error message is not equal to " + message);
    }

    @Then("page auto-scroll to product dan layanan Promotion section {string}")
    public void user_autoscroll_to_product_dan_layanan(String message) throws InterruptedException {
        Assert.assertEquals(headerEnaknyaNgekos.autoscrollToProductDanLayanan(), message, "Error message is not equal to " + message);
    }

    @And("click {string} on the footer")
    public void user_click_button_on_footer (String button) throws InterruptedException {
        headerEnaknyaNgekos.clickOnFooter(button);
    }

    @Then("user can see popup send email")
    public void user_view_popup_send_email() throws InterruptedException {
        Assert.assertEquals(headerEnaknyaNgekos.viewSendEmail(), "Form Bantuan");
    }

    @And("click on cari kos singgahsini & apik")
    public void user_cari_kos_ss_apik() throws InterruptedException {
        headerEnaknyaNgekos.clickCariAPIK();
    }
}
