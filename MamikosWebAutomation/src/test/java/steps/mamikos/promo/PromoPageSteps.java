package steps.mamikos.promo;

import dataobjects.PromoData;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.promo.PromoDetailPO;
import pageobjects.mamikos.promo.PromoListPO;
import utilities.ThreadManager;

import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class PromoPageSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private PromoListPO promo = new PromoListPO(driver);
    private PromoDetailPO promoDetail = new PromoDetailPO(driver);
    private PromoData promoData;

    public PromoPageSteps(PromoData promoData) {
        this.promoData = promoData;
    }

    @When("user click SALIN on any promo")
    public void user_click_SALIN_on_any_promo() throws InterruptedException {
        promo.clickOnFirstCopyPromo();
    }

    @Then("tool tips will be appear")
    public void tool_tips_will_be_appear() {
        Assert.assertTrue(promo.isTooltipPresent(), "Tooltip not appear");
    }

    @And("promo code can be copied {string}")
    public void promo_code_can_be_copied(String promoCode) throws IOException, UnsupportedFlavorException, InterruptedException {
      //  String promoCode = promo.getFirstPromoCode();
        if (promo.isGetClipboardText()) {
            Assert.assertEquals(promo.getClipboardText2(), promoCode, "ENAKNYANGEKOS18");
        }else{
            Assert.assertEquals(promo.getClipboardText(), promoCode, "Failed to copy promo");
        }
    }

    @When("user click next page button")
    public void user_click_next_page_button() throws InterruptedException {
        promo.clickNextPage();
    }

    @Then("next promo page will be opened")
    public void next_promo_page_will_be_opened() {
        String pageIndex = promo.getPageIndex();
        if(pageIndex != null){
            Assert.assertEquals(pageIndex, "2", "Page index is not correct");
        }
    }

    @When("user click previous page button")
    public void user_click_previous_page_button() throws InterruptedException {
        promo.clickPrevPage();
    }

    @Then("previous promo page will be opened")
    public void previous_promo_page_will_be_opened() {
        String pageIndex = promo.getPageIndex();
        if(pageIndex != null){
            Assert.assertEquals(pageIndex, "1", "Page index is not correct");
        }
    }

    @When("user click page index {string}")
    public void user_click_page_index(String index) throws InterruptedException {
        promo.clickPageIndex(index);
    }

    @Then("promo page {string} will be opened")
    public void page_will_be_opened(String page) {
        String pageIndex = promo.getPageIndex();
        if(pageIndex != null) {
            Assert.assertEquals(pageIndex, page, "Page index is not correct");
        }
    }

    @And("user see the promo title in first promo")
    public void user_see_the_promo_title_in_first_promo() {
        promoData.promoTitle = promo.getFirstPromoTitle();
    }

    @When("user click see detail on first promo")
    public void user_click_see_detail_on_first_promo() throws InterruptedException {
        promo.clickFirstSeeDetail();
    }

    @Then("detail promo page opened with correct title")
    public void detail_promo_page_opened_with_correct_title() {
        Assert.assertEquals(promoDetail.getPromoTitle(), promoData.promoTitle, "Promo title is not correct");
    }

    @And("user see button booking now")
    public void user_see_button_booking_now() {
        Assert.assertTrue(promoDetail.bookingNowButtonDisplayed(), "Use Now button is not appear");
    }

    @And("user click button atur promo")
    public void user_click_button_atur_promo() throws InterruptedException{
        promo.clickOnSetPromoButton();
    }

    @Then("user see page title is {string} on Promo Page")
    public void user_see_page_title_on_promo_page(String title) {
        Assert.assertEquals(promo.getPagePromoTitle(), title, "title is not match");
    }

    @And("user see status promo is {string}")
    public void userSeeStatusPromoIs(String statusPromo) {
        Assert.assertEquals(promo.getStatusPromo(), statusPromo, "status promo is not match");
    }
}
