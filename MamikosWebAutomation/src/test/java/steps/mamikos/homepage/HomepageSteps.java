package steps.mamikos.homepage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.homepage.HomepagePO;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import pageobjects.mamikos.tenant.search.SearchPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomepageSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private HomepagePO homepage = new HomepagePO(driver);
    private SearchPO search = new SearchPO(driver);
    private SearchListingPO searchListing = new SearchListingPO(driver);
    private HeaderPO header = new HeaderPO(driver);
    private KosDetailPO kosdetail = new KosDetailPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    private String locationName="src/test/resources/testdata/mamikos/homepage.properties";
    private String recommenLoc = JavaHelpers.getPropertyValue(locationName,"recommenLoc_" + Constants.ENV);
    private String promoLoc = JavaHelpers.getPropertyValue(locationName,"promoLoc_" + Constants.ENV);

    @When("user see the banner promo")
    public void user_see_the_banner_promo(){
        Assert.assertTrue(homepage.isFirstBannerPresent(), "First banner is not present!");

    }

    @Then("user clicks on previous button and it will display previous banner")
    public void user_clicks_on_previous_button_and_it_will_display_previous_banner() throws InterruptedException {
        String firstBanner = homepage.getActiveBannerIndex();
        homepage.clickOnPreviousButton();
        Assert.assertEquals(firstBanner, homepage.getPreviousBannerIndex(), "Previous promo banner is not equals!");
    }

    @When("user see the last banner promo")
    public void user_see_the_last_banner_promo() {
        Assert.assertTrue(homepage.isThirdBannerPresent(), "Third banner is not present!");
    }

    @Then("user clicks on next button and it will display next banner")
    public void user_clicks_on_next_button_and_it_will_display_next_banner() throws InterruptedException {
        String nextBanner = homepage.getNextBannerIndex();
        homepage.clickOnNextButton();
        Assert.assertEquals(nextBanner, homepage.getLastBannerIndex(), "Next promo banner is not equals!");
    }

    @When("user clicks on Lihat Semua button")
    public void user_clicks_on_lihat_semua_button() throws InterruptedException {
        homepage.clickOnSeeAllButton();
    }

    @Then("page redirect to {string}")
    public void page_redirect_to(String address) throws InterruptedException {
        Assert.assertEquals(homepage.getURL(), address, "Promo URL doesn't match!");

    }

    @When("user reach homepage, and scroll to testimonial section")
    public void user_reach_homepage_and_scroll_to_testimonial_section() {
        homepage.scrollDownToTestimonial();
    }

    @And("user see the testimonial elements")
    public void user_see_the_testimonial_elements() throws InterruptedException {
        Assert.assertTrue(homepage.isTestimonialTitlePresent(), "Title not present!");
        for(int i = 1; i <= homepage.getTestimonialSize(); i++) {
            Assert.assertTrue(homepage.isOwnerImgPresent(i), "Owner profile pic not present!");
            Assert.assertTrue(homepage.isOwnerNamePresent(i), "Owner Name not present!");
            Assert.assertTrue(homepage.isOwnerKosNamePresent(i), "Kos Name not present!");
            Assert.assertTrue(homepage.isTestimonialDescPresent(i), "Testimonial desc not present!");
        }
    }

    @Then("user clicks on next button")
    public void user_clicks_on_next_button() throws InterruptedException {
        String lastTestimonial = homepage.getLastTestimonialElement();
        homepage.clickNextButton();
        Assert.assertEquals(homepage.getNextTestimonialElement(), lastTestimonial, "Next and last testimonial are not equals!");
    }

    @And("user clicks on previous button")
    public void user_clicks_on_previous_button() throws InterruptedException {
        String firstTestimonial = homepage.getFirstTestimonialElement();
        homepage.clickPrevButton();
        Assert.assertEquals(homepage.getPrevTestimonialElement(), firstTestimonial, "Prev and first testimonial are not equals!");
    }

    @Then("user see title wording {string}")
    public void user_see_title_wording(String text) {
        Assert.assertEquals(homepage.getSearchTitleText(), text, "Search Title wording is not equals");
    }

    @Then("user see description wording {string}")
    public void user_see_description_wording(String text) {
        Assert.assertEquals(homepage.getSearchDescText(), text, "Search description is not equals");
    }

    @Then("user see label wording {string}")
    public void user_see_label_wording(String text) {
        Assert.assertEquals(homepage.getSearchLabel(), text, "Search Label is not equals");
    }

    @And("user see Cari button")
    public void user_see_cari_button() {
        Assert.assertTrue(homepage.isSearchButtonDisplayed(), "Search button not present");
    }

    @When("user reach homepage, and scroll to kost register section")
    public void userReachHomepageAndScrollToKostRegisterSection() {
        homepage.scrollDownToKosRegister();
    }

    @Then("user click on Daftar Kos Gratis and redirect to {string}")
    public void user_click_on_daftar_kos_gratis_and_redirect_to(String address) throws InterruptedException {
        Assert.assertEquals(homepage.getRegisterKosURL(), Constants.MAMIKOS_URL + address, "Kos register address not equals");
    }

    @When("user reach homepage, and scroll to recommendation section")
    public void user_reach_homepage_and_scroll_to_recommendation_section() {
        homepage.scrollDownToRecommendation();
    }

    @Then("user see element filter city")
    public void user_see_element_filter_city() {
        Assert.assertTrue(homepage.isCityFilterDisplayed(), "City Filter in recommendation section is not displayed!");
    }

    @And("user see kos thumbnail")
    public void user_see_kos_thumbnail() {
        Assert.assertTrue(homepage.isRoomCardRecommendationDisplayed(), "Room thumbnail in recommendation section is not displayed!");
    }

    @And("user see see all button")
    public void user_see_see_all_button() {
        Assert.assertTrue(homepage.isSeeAllRecomendationDisplayed(), "see all button in recommendation section is not displayed!");
    }

    @And("user click on Location button from home page")
    public void user_click_on_Location_button_from_home_page() throws InterruptedException {
        homepage.clickOnLocationKosRecommendation();
    }

    @Then("user see location list same with {string}")
    public void user_see_location_list_same(String locationName) {
        List<String> location = new ArrayList<>();
        if(locationName.equalsIgnoreCase("recommenLoc"))
        {
            locationName = recommenLoc;
            location = homepage.listPopularCity();
        }
        else if(locationName.equalsIgnoreCase("promoLoc"))
        {
            locationName = promoLoc;
            location = homepage.listPromotionCity();
        }

        List<String> loc = new ArrayList<>(Arrays.asList(locationName.split(",")));
        Assert.assertEquals(location.size(), loc.size(), "list length is not equals");

        for (String a : location) {
            for (int i = 0; i < loc.size(); i++) {
                if (a.equals(loc.get(i))){
                    break;
                }  else if (i == loc.size()-1) {
                    Assert.assertEquals(a, loc.get(i), "list location " + a + " is not exist!");
                }
            }
        }
    }

    @And("user clicks on next and previous button recommendation")
    public void user_clicks_on_next_and_previous_button_recommendation() throws InterruptedException {
        String lastRoomCard = homepage.getLastRoomRecommendation();
        homepage.clickNextRecommendationButton();
        homepage.clickPrevRecommendationButton();
        Assert.assertEquals(homepage.getLastRoomRecommendation(), lastRoomCard, "Next and last recommendation are not equals!");
    }

    @Then("user clicks on Lihat Semua button and redirect to Landing kost, according to the city that selected")
    public void user_clicks_on_lihat_semua_button_and_redirect_to_landing_kost() throws InterruptedException {
        String city = homepage.getCityRecommendation().toLowerCase();
        homepage.clickSeeAllRecommenButton();
        searchListing.clickFTUEKosListingPopUp();
        String title = search.getTitleKostLanding().toLowerCase();
        Assert.assertTrue(title.contains(city), "Option Location not equals with the title!");
        Assert.assertTrue(title.contains(city), "Choosen Location not equals with the title listing!");
        List<String> addressList = search.listKostAddress();
        for (String a : addressList) {
            Assert.assertTrue(a.contains(title), "Search result " + a + " not in correct location");
        }
    }

    @Then("page redirect to page {string}")
    public void page_redirect_to_page(String url) throws InterruptedException {
        Assert.assertTrue(homepage.getURL().contains(url), "URL doesn't match!");
    }

    @When("user reach homepage, and scroll to promotion section")
    public void user_reach_homepage_and_scroll_to_promotion_section() {
        homepage.scrollDownToPromotion();
    }

    @Then("user see element filter city promotion section")
    public void user_see_element_filter_city_promotion_section() {
        Assert.assertTrue(homepage.isCityFilterPromotionDisplayed(), "City Filter in promotion section is not displayed!");
    }

    @And("user see kos thumbnail promotion section")
    public void user_see_kos_thumbnail_promotion_section() {
        Assert.assertTrue(homepage.isRoomCardPromotionDisplayed(), "Room thumbnail in promotion section is not displayed!");
    }

    @And("user see see all button promotion section")
    public void user_see_see_all_button_promotion_section() {
        Assert.assertTrue(homepage.isSeeAllPromotionDisplayed(), "see all button in recommendation section is not displayed!");
    }

    @And("user click on Location button from promotion section")
    public void user_click_on_Location_button_from_promotion_section() throws InterruptedException {
        homepage.clickOnLocationKosPromotion();
    }

    @And("user clicks on next and previous button promotion")
    public void user_clicks_on_next_and_previous_button_promotion() throws InterruptedException {
        String lastRoomCard = homepage.getLastRoomTitlePromo();
        homepage.clickNextPromoButton();
        homepage.clickPrevPromoButton();
        Assert.assertEquals(homepage.getLastRoomTitlePromo(), lastRoomCard, "Last room card and room card after move are not equals!");
    }

    @And("user clicks on Lihat Semua button on promotion section")
    public void user_clicks_on_lihat_semua_button_on_promotion_section() throws InterruptedException {
        homepage.clickSeeAllPromoButton();
    }

    @When("user reach homepage, and scroll to popular area section")
    public void user_reach_homepage_and_scroll_to_popular_area_section() {
        homepage.scrollDownToPopularArea();
    }

    @Then("in popular area section, there's this city kos :")
    public void in_popular_area_section_theres_this_city_kos(List<String> popularCity) {
        List<String> webPopularCity = homepage.listPopularAreaKos();
        Assert.assertEquals(webPopularCity, popularCity, "Popular city not match");
    }

    @Then("user clicks on city and redirect to Landing kost, according to the city that selected")
    public void user_clicks_on_city_and_redirect_to_landing_kost_according_to_the_city_that_selected() throws InterruptedException {
        String city = homepage.getThirdPopularCityText().toLowerCase();
        homepage.clickThirdPopularAreaCityText();
        searchListing.clickFTUEKosListingPopUp();
        Assert.assertTrue(city.contains(searchListing.getLandingKostFilter()), "The landing page not match");
    }

    @And("user clicks on Lihat Semua button on popular area section")
    public void user_clicks_on_lihat_semua_button_on_popular_area_section() throws InterruptedException {
        homepage.clickSeeAllPopularArea();
    }

    @When("user reach homepage, and scroll to kost around campus section")
    public void user_reach_homepage_and_scroll_to_kost_around_campus_section() throws InterruptedException {
        homepage.scrollDownToKostAroundCampus();
    }

    @Then("in kost near campus section, there's this campus :")
    public void in_kost_near_campus_section_theres_this_campus(List<String> nearCampus) {
        List<String> campus = homepage.listPopularAreaCampusKos();
        Assert.assertEquals(campus, nearCampus, "Popular campus not match");
    }

    @Then("in kost near campus section, there's this city campus :")
    public void in_kost_near_campus_section_theres_this_city_campus(List<String> cityCampus) {
        List<String> city = homepage.listPopularAreaCityCampusKos();
        Assert.assertEquals(city, cityCampus, "City popular campus not match");
    }

    @And("user clicks on Lihat Semua button on popular campus area section")
    public void user_clicks_on_lihat_semua_button_on_popular_campus_area_section() throws InterruptedException {
        homepage.clickSeeAllCampusArea();
    }

    @When("user reach homepage, and scroll to bottom of mamikos")
    public void user_reach_homepage_and_scroll_to_bottom_of_mamikos() {
        homepage.scrollDownToBottomMamikos();
    }

    @Then("check the footer menu display on homepage")
    public void check_the_footer_menu_display_on_homepage() {

        Assert.assertTrue(homepage.isLogoMamikosDisplayed(), "see logo mamikos is not displayed!");
        Assert.assertTrue(homepage.isGooglePlayStoreDisplayed(), "see google play store is not displayed!");
        Assert.assertTrue(homepage.isAppStoreDisplayed(), "see App Store is not displayed!");
        Assert.assertTrue(homepage.isTentangKamiDisplayed(), "see tentang kami is not displayed!");
        Assert.assertTrue(homepage.isJobMamikosDisplayed(), "see job mamikos is not displayed!");
        Assert.assertTrue(homepage.isPromosiIklanAndaGratisDisplayed(), "see promosi iklan anda gratis is not displayed!");
        Assert.assertTrue(homepage.isPusatBantuanDisplayed(), "see pusat bantuan is not displayed!");
        Assert.assertTrue(homepage.isKebijakanPrivasiDisplayed(), "see kebijakan privasi is not displayed!");
        Assert.assertTrue(homepage.isSyaratDanKetentuanUmumDisplayed(), "see syarat dan ketentuan umum is not displayed!");
    }

    @And("user click link google play store on footer section and redirect to page {string}")
    public void user_click_link_google_play_store_on_footer_section(String address) throws InterruptedException{
        homepage.clickButtonGooglePlayStore();
        Assert.assertTrue(homepage.getGooglePlayStoreLinkURL().contains(address), "Google play store URL doesn't equals");
        header.switchToFirstTab();
    }

    @Then("user click link app store on footer section and redirect to page {string}")
    public void user_click_link_app_store_on_footer_section(String address) throws InterruptedException {
        homepage.clickButtonGAppStore();
        Assert.assertTrue(homepage.getAppStoreLinkURL().contains(address), "App store URL doesn't equals");
    }

    @And("user click link tentang kami on footer section")
    public void user_click_link_tentang_kami_on_footer_section() throws InterruptedException {
        homepage.clickLinkTentangKami();
    }

    @And("user click link job mamikos on footer section")
    public void user_click_link_job_mamikos_on_footer_ssection() throws InterruptedException {
        homepage.clickJobMamikosLink();
    }

    @And("user click link promosikan iklan anda gratis on footer section")
    public void user_click_link_promosikan_iklan_anda_gratis_on_footer_section() throws InterruptedException {
        homepage.clickPromosikanIklanGratisLink();
    }

    @And("user click link kebijakan privasi section")
    public void user_click_link_kebijakan_privasi_section() throws InterruptedException {
        homepage.clickKebijakanPrivasiLink();
    }

    @And("user click link pusat bantuan section then page redirect to page {string}")
    public void user_click_link_pusat_bantuan_section_then_page_redirect_to_page(String address) throws InterruptedException {
        Assert.assertEquals(homepage.getHelpCenterLinkURL(), address, "Address help center link not equals");
    }

    @And("user click link syarat dan ketentuan then page redirect to page {string}")
    public void user_click_link_syarat_dan_ketentuan_then_page_redirect_to_page(String address) throws InterruptedException {
        Assert.assertEquals(homepage.getSyaratKetentuanLinkURL(), address, "Address syarat dan ketentuan not equals");
    }

    @Then("user click promo kos thumbnail and redirect to detail promo kos")
    public void user_click_promo_kos_thumbnail_and_redirect_to_detail_promo_kos() throws InterruptedException {
        String kostName = homepage.getThumbnailTitle();
        homepage.clickPromoThumbnail();
        Assert.assertTrue(kosdetail.getTextKostTitle().contains(kostName), "Kos Name is not equals!");
    }

    @Then("user click facebook")
    public void user_click_facebook() throws InterruptedException {
//        selenium.closeTabWindowBrowser();
        homepage.clickFacebookIcon();
    }

    @Then("user click twitter")
    public void user_click_twitter() throws InterruptedException {
//        selenium.closeTabWindowBrowser();
        homepage.clickTwitterIcon();
    }

    @Then("user click instagram")
    public void user_click_instagram() throws InterruptedException {
        homepage.clickInstagramIcon();
    }

    @And("user sees e-mail is {string}")
    public void user_sees_eMail_is_appear(String email) throws InterruptedException {
        selenium.closeTabWindowBrowser();
        Assert.assertEquals(homepage.getEmailText(), email, "Email icon is not present!");
    }

    @And("user sees whatsapp is {string}")
    public void user_sees_whatsapp_is_appear(String number1){
        Assert.assertEquals(homepage.getWhatsappNumber1Text(), number1, "Whatsapp number 1 is not equals!");
    }

    @When("user switch to main window")
    public void user_switch_to_main_window() throws InterruptedException {
        selenium.closeTabWindowBrowser();
        selenium.switchToWindow(1);
    }

    @Then("page will redirect to {string}")
    public void page_will_redirect_to(String url) throws InterruptedException {
        selenium.switchToWindow(2);
        Assert.assertEquals(homepage.getURL(), Constants.MAMIKOS_URL + url, "Address Promo Kost is not equals");
        selenium.closeTabWindowBrowser();
    }

    @And("user sees copyright is {string}")
    public void user_sees_copyright_is(String copyright) {
        Assert.assertEquals(homepage.getCopyrightText(), copyright, "Copyright is not present!");
    }

    @And("user maximize the screen size")
    public void user_maximize_the_screen_size() {
        selenium.setSizeWindow(1920, 1080);
    }

    @When("user switch to homepage")
    public void user_switch_to_homepage() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.closeTabWindowBrowser();
    }
}
