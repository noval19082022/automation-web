package steps.mamikos.owner.kostmanagement;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.kostmanagement.BillsDetailsPO;
import pageobjects.mamikos.owner.kostmanagement.ManageBillsPO;
import pageobjects.mamikos.owner.kostmanagement.TenantDetailsPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.List;

public class ManageBillsSteps {

	private WebDriver driver = ThreadManager.getDriver();
	SeleniumHelpers selenium = new SeleniumHelpers(driver);
	private ManageBillsPO bills = new ManageBillsPO(driver);
	private BillsDetailsPO billsDetail = new BillsDetailsPO(driver);
	private JavaHelpers java = new JavaHelpers();
	private TenantDetailsPO tenant = new TenantDetailsPO(driver);

	//test data Teng
	private String tengOwner = "src/test/resources/testdata/mamikos/tenant-engagement-owner.properties";
	private String manageBillsKostName = JavaHelpers.getPropertyValue(tengOwner, "kost_name_manage_bills_" + Constants.ENV);
	private String addOnsKostName = JavaHelpers.getPropertyValue(tengOwner, "addsOnKost_" + Constants.ENV);

	@Then("owner can see kost filter is selected with kost name {string}")
	public void owner_can_see_kost_filter_is_selected_with_kost_name(String kostName) {
		if (kostName.equalsIgnoreCase("Kost Name Manage Bills")) {
			kostName = manageBillsKostName;
		}
		else if(kostName.equalsIgnoreCase("add ons kost")) {
			kostName = addOnsKostName;
		}
		Assert.assertEquals(bills.getSelectedKostText(), kostName);
	}

	@Then("owner can see result on Belum Bayar box")
	public void owner_can_see_result_on_Belum_Bayar_box() {
		boolean isResult = bills.getResultOnNotPaidYet().size() > 0;
		boolean isNoRenter = bills.getAllPaidTitle().equalsIgnoreCase("Tidak Ada yang Belum Bayar");
		Assert.assertTrue(isResult || isNoRenter);
	}

	@When("owner set Kelola Tagihan filter month to {string} month and year to {string}")
	public void owner_set_Kelola_Tagihan_filter_month_to_month_and_year_to(String monthNumber, String year) throws InterruptedException, ParseException {
		if(monthNumber.equalsIgnoreCase("current")) {
			monthNumber = java.updateTimeLocal("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "M", "en", 0, 0, 0, 0, 0);
		}
		else if (monthNumber.equalsIgnoreCase("next")){
			monthNumber = java.updateTimeLocal("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "M", "en", 1, 0, 0, 0, 0);
		}

		if (year.equalsIgnoreCase("current")) {
			year = java.updateTimeLocal("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "yyyy", "en", 0, 0, 0, 0, 0);
		}
		bills.selectManageBillsMonthFilter(monthNumber);
		bills.selectYearsFilter(year);
	}

	@When("owner set Kelola Tagihan filter month to {string} month")
	public void owner_set_Kelola_Tagihan_filter_month_to_month(String monthNumber) throws InterruptedException, ParseException {
		if(monthNumber.equalsIgnoreCase("current")) {
			monthNumber = java.updateTimeLocal("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "M", "en", 0, 0, 0, 0, 0);
		}
		else if (monthNumber.equalsIgnoreCase("next")){
			monthNumber = java.updateTimeLocal("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "M", "en", 0, 1, 0, 0, 0);
		}
		bills.selectManageBillsMonthFilter(monthNumber);
	}

	@When("owner set Kelola Tagihan filter rent count by {string}")
	public void owner_set_Kelola_Tagihan_filter_rent_count_by(String rentCount) throws InterruptedException {
		bills.setRentCount(rentCount);
	}

	@When("owner goes to bills details")
	public void owner_goes_to_bills_details() throws InterruptedException {
		bills.clickOnRenterResult("1");
	}

	@When("owner ticks on pay outside mamipay checkbox")
	public void owner_ticks_on_pay_outside_mamipay_checkbox() throws InterruptedException {
		billsDetail.clickOnPayOutsideMamipayCheckBox();
	}

	@Then("owner can sees pay outside mamipay pop-up")
	public void owner_can_sees_pay_outside_mamipay_pop_up() {
		Assert.assertTrue(billsDetail.isPopUpActive());
		Assert.assertEquals(billsDetail.getPayOutsideMamipayTitle(), "Yakin ingin tandai “Bayar di Luar Mamipay”?");
		Assert.assertEquals(billsDetail.getPayOutsideMamipayCaption(), "Proses ini tidak bisa dibatalkan. Pastikan uang sewa sudah Anda terima sebelum menandai tagihan ini.");
		Assert.assertTrue(billsDetail.isCancelAndAcceptButton());
	}

	@When("owner clicks on yes mark button on pay outside mamipay pop-up")
	public void owner_clicks_on_yes_mark_button_on_pay_outside_mamipay_pop_up() throws InterruptedException {
		billsDetail.clicksOnYesMark();
	}

	@Then("owner can sees payment status is {string}")
	public void owner_can_sees_payment_status_is(String paymentStatus) throws InterruptedException {
		billsDetail.waitTillPopUpPayOutsideMamipayNotVisible();
		Assert.assertEquals(billsDetail.getPaymentStatus(), paymentStatus);
	}

	@When("owner clicks on edit additional price")
	public void owner_clicks_on_edit_additional_price() throws InterruptedException {
		billsDetail.clicksOnEditAdditionalPrice();
	}

	@Then("owner can sees edit additional price pop-up")
	public void owner_can_sees_edit_additional_price_pop_up() throws InterruptedException {
		Assert.assertTrue(billsDetail.getPopUpHeaderTitle().equalsIgnoreCase("Biaya Tambahan"));
		billsDetail.dismissPopUp();
	}

	@When("owner clicks on in mamikos tab")
	public void owner_clicks_on_in_mamikos_tab() throws InterruptedException {
		bills.clicksOnInMamikosTab();
	}

	@When("owner clicks on bank transfer edit button")
	public void owner_clicks_on_bank_transfer_edit_button() throws InterruptedException {
		billsDetail.clicksOnBankTransferEdit();
	}

	@Then("owner can sees edit bank transfer pop-up")
	public void owner_can_sees_edit_bank_transfer_pop_up() {
		Assert.assertEquals(billsDetail.getPayOutsideMamipayTitle(), "Ubah Akun Bank Melalui Aplikasi");
		Assert.assertEquals(billsDetail.getPayOutsideMamipayCaption(), "Perubahaan akun bank Anda hanya bisa dilakukan melalui Aplikasi Mamikos.");
		Assert.assertTrue(billsDetail.getActivPopUpButtonText().equalsIgnoreCase("Saya Mengerti"));
	}

	@When("owner sets sorting to {string}")
	public void owner_sets_sorting_to(String sortingType) throws InterruptedException {
		bills.sortingResultBy(sortingType);
	}

	@Then("owner can sees {string} is sorted accordingly")
	public void owner_can_sees_is_sorted_accordingly(String sortView) throws Throwable {
		int kostRow = bills.getResultOnNotPaidYet().size();
		int currentRoom;
		int nextRoom;
		Character currentRenterFirstLetter;
		Character nextRenterFirstLetter;
		int currentResultDate;
		int nextResultDate;

		switch (sortView) {
			case "Room":
				for(int i = 0; i < kostRow; i++) {
					currentRoom = bills.getRoomNumberKostResult(i + 1);
					nextRoom = bills.getRoomNumberKostResult(currentRoom + 1);
					Assert.assertTrue(currentRoom <= nextRoom);
				}
				break;
			case "Due Date":
				for (int i = 0; i < kostRow; i++) {
					currentResultDate = Integer.parseInt(bills.getPaymentDueDate(i + 1).split(" ")[0]);
					nextResultDate = Integer.parseInt(bills.getPaymentDueDate(i + 2).split(" ")[0]);
					Assert.assertTrue(currentResultDate <= nextResultDate);
					if (bills.isRenterResultVisible(i+2)) {
						break;
					}
				}
				break;
			case "Renter Name":
				for(int i = 0; i < kostRow; i++) {
					currentRenterFirstLetter = bills.getRenterName(i + 1).toCharArray()[0];
					nextRenterFirstLetter = bills.getRenterName(i + 2).toCharArray()[0];
					Assert.assertTrue(currentRenterFirstLetter <= nextRenterFirstLetter);
					if (bills.isRenterResultVisible(i+2)) {
						break;
					}
				}
				break;
			default:
				throw new Throwable("Input with Room, Due Date, or Renter Name");
		}
	}

	@When("owner clicks on tenant box on bills details")
	public void owner_clicks_on_tenant_box_on_bills_details() throws InterruptedException {
		billsDetail.clickOnTenantInfo();
	}

	@Then("owner redirected to tenant biodata's page")
	public void owner_redirected_to_tenant_biodata_s_page() {
		selenium.switchToWindow(2);
		Assert.assertTrue(selenium.getURL().contains("ownerpage/billing-management/profile-tenant/"));
		Assert.assertTrue(selenium.getURL().contains("/payment-tenant"));
		Assert.assertTrue(tenant.isTenantProfileVisible());
	}

	@When("owner clicks on edit kost price button")
	public void owner_clicks_on_edit_kost_price_button() throws InterruptedException {
		billsDetail.clickOnEditKostPrice();
	}

	@Then("owner can see pop-up edit kost price form")
	public void owner_can_see_pop_up_edit_kost_price_form() {
		Assert.assertTrue(billsDetail.getPopUpHeaderTitle().contains("Harga Sewa"));
		Assert.assertTrue(billsDetail.getActivPopUpButtonText().contains("Simpan Harga Sewa"));
	}

	@When("owner sets kost filter to {string}")
	public void owner_sets_kost_filter_to(String kostName) throws InterruptedException {
		bills.setKostBills(kostName);
	}

	@When("owner sets kost filter to {string} for recurring invoice")
	public void owner_sets_kost_filter_to_for_recurring(String kostName) throws InterruptedException {
		bills.setKostBillsRecurring(kostName);
	}

	@When("owner sets renter name to {string}")
	public void owner_sets_renter_name_to(String renterName) throws InterruptedException {
		bills.setRenterName(renterName);
	}

	@Then("^owner can sees total amount is basic amount plus other price$")
	public void owner_can_sees_total_amount_is_basic_amount_other_price(List<Integer> priceList) {
		int totalCost = JavaHelpers.extractNumber(billsDetail.getTotalCostText());
		int perPeriodCost = JavaHelpers.extractNumber(billsDetail.getPerPeriodText());
		int additionalPriceCost = 0;
		for (int number : priceList) {
			additionalPriceCost += number;
		}
		Assert.assertEquals( perPeriodCost, totalCost - additionalPriceCost);
	}

	@Then("owner can not sees add ons with name {string} in the price list")
	public void owner_can_not_sees_add_ons_with_name_in_the_price_list(String priceName) {
		Assert.assertFalse(billsDetail.isPricePresent(priceName), priceName + " is present in price list");
		selenium.back();
	}

	@When("owner clicks on transferred tab")
	public void owner_clicks_on_transferred_tab() throws InterruptedException {
		bills.clicksOnTransferredTab();
	}

	@And("user clicks Sudah bayar tab")
	public void user_clicks_sudah_bayar_tab() throws InterruptedException {
		bills.clicksOnSudahBayarTab();
	}

	@And("user clicks on billing invoice")
	public void user_clicks_on_billing_invoice() throws InterruptedException {
		bills.openTheInvoice();
	}

	@And("user see Kapan uang masuk ke rekening saya? and clicks on disbursement link")
	public void user_see_kapan_uang_masuk_ke_rekening_saya_and_clicks_on_disbursement_link() throws InterruptedException {
		bills.clicksDisbursementLink();
	}

	@Then("user redirect to disbursement link and clicks back")
	public void user_redirect_to_disbursement_link_and_clicks_back(){
		bills.getDisbursementLinkPage();
		bills.clickBackOnBrowser();
	}

	@And("owner choose kost for {string} on Tagihan page")
	public void owner_choose_kost_for_x_on_tagihan_page(String kostName) throws InterruptedException{
		bills.clickKostNameSelected(kostName);
	}

	@And("owner choose month with {string}")
	public void owner_choose_month_with_x(String monthName) throws InterruptedException{
		bills.clickMonthSelected(monthName);
	}

}
