package steps.mamikos.owner.dashboard;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.account.LoginPO;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.owner.DashboardOwnerPO;
import pageobjects.mamikos.owner.common.AddPopUpPO;
import pageobjects.mamikos.owner.common.LeftMenuPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;
import java.util.List;
import java.util.Map;

public class OwnerDashboardSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private DashboardOwnerPO doPO = new DashboardOwnerPO(driver);
    private LoginPO loginpo = new LoginPO(driver);
    private AddPopUpPO popUp = new AddPopUpPO(driver);
    private HeaderPO header = new HeaderPO(driver);
    private LeftMenuPO leftMenu = new LeftMenuPO(driver);
    private int reviewListCard = 0;

    //UGE Properties
    private String uge="src/test/resources/testdata/mamikos/ugeSquad.properties";

    //UG - Owner GP
    private String phoneOwnerGp1 = JavaHelpers.getPropertyValue(uge,"owner_phone_gp1_" + Constants.ENV);
    private String passwordOwnerGp1 = JavaHelpers.getPropertyValue(uge,"owner_password_general_" + Constants.ENV);
    private String phoneOwnerGp2 = JavaHelpers.getPropertyValue(uge,"owner_phone_gp2_" + Constants.ENV);
    private String passwordOwnerGp2 = JavaHelpers.getPropertyValue(uge,"owner_password_general_" + Constants.ENV);
    private String phoneOwnerGp3 = JavaHelpers.getPropertyValue(uge,"owner_phone_gp3_" + Constants.ENV);
    private String passwordOwnerGp3 = JavaHelpers.getPropertyValue(uge,"owner_password_general_" + Constants.ENV);
    private String phoneOwnerNonGp = JavaHelpers.getPropertyValue(uge,"owner_phone_nongp_" + Constants.ENV);
    private String passwordOwnerNonGp = JavaHelpers.getPropertyValue(uge,"owner_password_general_" + Constants.ENV);
    private String phoneNewOwnerNoListing = JavaHelpers.getPropertyValue(uge,"owner_phone_nolisting_" + Constants.ENV);
    private String passwordNewOwnerNoListing = JavaHelpers.getPropertyValue(uge,"owner_password_nolisting_" + Constants.ENV);
    private String phoneOwnerGPonReview = JavaHelpers.getPropertyValue(uge,"owner_phone_onreview_" + Constants.ENV);
    private String passwordOwnerGPonReview= JavaHelpers.getPropertyValue(uge,"owner_password_onreview_" + Constants.ENV);
    private String phoneOwnerGPMenungguPembayaran = JavaHelpers.getPropertyValue(uge,"owner_phone_menunggupembayaran_" + Constants.ENV);
    private String passwordOwnerGPMenungguPembayaran = JavaHelpers.getPropertyValue(uge,"owner_password_general_" + Constants.ENV);
    @When("user fills out owner gp login as {string} and click on Enter button")
    public void user_fills_out_login_form_with_credentails_and_and_click_on_Enter_button(String type)
            throws InterruptedException {
        String phone="";
        String password="";

        switch (type) {
            case "owner gp 1":
                phone = phoneOwnerGp1;
                password = passwordOwnerGp1;
                break;
            case "owner gp 2":
                phone = phoneOwnerGp2;
                password = passwordOwnerGp2;
                break;
            case "owner gp 3":
                phone = phoneOwnerGp3;
                password = passwordOwnerGp3;
                break;
            case "owner non gp":
                phone = phoneOwnerNonGp;
                password = passwordOwnerNonGp;
                break;
            case "new owner have no listing":
                phone = phoneNewOwnerNoListing;
                password = passwordNewOwnerNoListing;
                break;
            case "owner gp on review":
                phone = phoneOwnerGPonReview;
                password = passwordOwnerGPonReview;
                break;
            case "owner gp menunggu pembayaran":
                phone = phoneOwnerGPMenungguPembayaran;
                password = passwordOwnerGPMenungguPembayaran;
                break;
        }

        loginpo.loginAsOwnerToApplication(phone,password);
        // Click the new 'Saya Mengerti' pop up if appear
        popUp.clickOnIUnderstandOnSetAdditionalCostsPopUp();
    }

    @And("validate that owner have {string}")
    public void validate_gp_label (String gplabel) {
        Assert.assertEquals(doPO.getGpLabel(), gplabel, "GP Level is not equal to " + gplabel);
        if(doPO.getGpLabel() == gplabel){
            doPO.redDotAppear(); //Red Dot Appear on GP Label Sedang Direview
        }
        else if(doPO.getGpLabel() == gplabel){
            doPO.redDotAppear(); //Red Dot Appear on GP Label Menunggu Pembayaran
        }
    }

    @Then("User click daftar sekarang")
    public void click_daftar_sekarang() throws InterruptedException{
        doPO.clickGPLabel();
    }

    @And("user click GP Label in Home")
    public void click_gp_label_in_home() throws InterruptedException{
        doPO.clickGPLabel();
    }

    @And("user click GP Card in Owner Dashboard")
    public void user_click_GP_Card_in_Owner_Dashboard() throws InterruptedException{
        doPO.clickGPCard();
    }

    @And("user click on rating card details")
    public void user_click_on_rating_card_details() throws InterruptedException {
        doPO.clickOnRatingCardDetails();
    }

    @And("validate expired GP Expired Date Label is {string}")
    public void validate_expired_GP_Expired_Date_Label(String gpExpiredDate) {
        Assert.assertEquals(doPO.getGpExpiredDate(), gpExpiredDate, "Text Expired Date Label is not match");
    }

    @Then("user validate review section with {string} and {string}")
    public void user_validate(String noReview, String nonPrem) {
        Assert.assertEquals(doPO.getNoReviewListText(noReview), noReview, "Message is not equal to " + noReview);
        Assert.assertEquals(doPO.getNonPremiumText(nonPrem), nonPrem, "Message is not equal to " + nonPrem);
    }

    @Then("user verify there are only {int} review lists")
    public void user_verify_there_are_only_review_lists(Integer number) {
        Assert.assertEquals(doPO.getReviewListsCard(), number, "Kos review list should have " + number + " items");
        reviewListCard = doPO.getReviewListsCard();
    }

    @When("user click one of review lists")
    public void user_click_one_of_review_lists() throws InterruptedException {
        doPO.clickOnKosReviewListing();
    }

    @Then("user should see the review detail page")
    public void user_should_see_the_review_detail_page() {
        Assert.assertTrue(doPO.isDetailedReviewListsAppear(), "Detailed kos review lists is not appear");
    }

    @And("user click {string} on Owner Kost Review")
    public void user_click_on_Owner_Kost_Review(String text) throws InterruptedException {
        Assert.assertEquals(doPO.getSeeAllKostReviewText(), text, "See all kost review text is not equal to " + text);
        doPO.clickOnSeeAllKostReview();
    }

    @Then("user verify there are more than {int} review lists")
    public void user_verify_there_are_more_than_int_review_lists(int number) {
        Assert.assertTrue( doPO.getRatingCardWrapperSize() > number, "Kost review lists are not more than " + number);
    }

    @Then("user verify there are {int} or {int} review lists")
    public void user_verify_there_are_int_or_int_review_lists(int number1, int number2) {
        if (reviewListCard == number1){
            Assert.assertEquals(reviewListCard, number1, "Kos review lists are not equal to " + number1);
        }else if(reviewListCard == number2){
            Assert.assertEquals(reviewListCard, number2, "Kos review lists are not equal to " + number2);
        }
    }

    @And("user verify there is no kos review section")
    public void user_verify_there_is_no_kos_review_section() {
        Assert.assertFalse(doPO.isSeeAllKostReviewTextAppear(), "See all kost review text is appeared");
    }

    @Then("user verify there is {string} section")
    public void user_verify_there_is_string_section(String title) {
        Assert.assertTrue(doPO.isWaktunyaMengelolaKosTextAppear(), "Waktunya mengelola kost text is not appeared");
        Assert.assertEquals(doPO.getWaktuMengelolaTitle(), title, "Section title is not equal to " + title);
    }

    @When("owner go to Kelola Tagihan page")
    public void owner_go_to_Kelola_Tagihan_page() throws InterruptedException {
        leftMenu.clicOnKostManagementMenu();
        leftMenu.clickOnManageBillLeftMenu();
    }

    @Then("daftar goldplus label is {string}")
    public void daftar_goldplus_label_is(String label) {
        if(label.equals("appear")){
            Assert.assertTrue(doPO.isDaftarGoldplusAppear(), "Daftar goldplus is dissapear");
        } else if (label.equals("dissapear")){
            Assert.assertFalse(doPO.isDaftarGoldplusAppear(), "Daftar goldplus is appear");
        }
    }

    @And("user see widget waktunya mengelola properti is as expected")
    public void user_see_widget_waktunya_mengelola_properti_is_as_expected(DataTable dataTable){
        List<Map<String, String>> table = dataTable.asMaps();
        int i = 0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(doPO.widgetWaktunyaMengelolaProperti("title", i), content.get("title"), "title not equal to "+content.get("title"));
            Assert.assertEquals(doPO.widgetWaktunyaMengelolaProperti("subtitle", i), content.get("subtitle"),"subtitle not equal to"+content.get("subtitle"));
            i++;
        }
    }

    @Then("user verify text {string} on section info untuk anda is appear")
    public void user_verify_text_on_section_info_untuk_anda_is_appear(String textInfoAnda) throws InterruptedException {
       doPO.clickOnText(textInfoAnda);
    }

    @Then("user can see pengajuan sewa detail on dashboard")
    public void user_can_see_pengajuan_sewa_detail_on_dashboard() throws InterruptedException {
        Assert.assertTrue(doPO.isPengajuanSewaSectionPresent(), "not appears pengajuan sewa");
    }

    @When("user click on reject booking")
    public void user_click_on_reject_booking() {
        doPO.clickOnRejectButtonOnDashboard();
    }

}
