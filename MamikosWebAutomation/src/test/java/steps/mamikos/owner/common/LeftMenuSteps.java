package steps.mamikos.owner.common;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikos.owner.common.LeftMenuPO;
import utilities.ThreadManager;

public class LeftMenuSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private LeftMenuPO leftMenuPO = new LeftMenuPO(driver);

    @And("user click on my property menu")
    public void user_click_on_my_property_menu() throws InterruptedException {
        leftMenuPO.clickOnPropertiSayaMenu();
    }

    @When("user click on kos menu")
    public void user_click_on_kos_menu() throws InterruptedException {
        leftMenuPO.clickOnKosButton();
    }
}
