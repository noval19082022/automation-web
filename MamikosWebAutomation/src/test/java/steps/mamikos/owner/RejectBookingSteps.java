package steps.mamikos.owner;

import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.owner.booking.RejectBookingPO;
import pageobjects.mamikos.tenant.profile.BookingHistoryPO;
import utilities.Constants;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class RejectBookingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private RejectBookingPO reject = new RejectBookingPO(driver);

    @When("owner reject booking with full room reason")
    public void owner_reject_booking_with_full_room_reason() throws InterruptedException {
        reject.rejectBookingFullRoom();
    }

    @When("owner reject booking with reject test")
    public void owner_reject_booking_with_reject_test() throws InterruptedException {
        reject.rejectBookingWithRejectTest();
    }

    @When("owner choose other reason with costum reason {string}")
    public void owner_choose_other_reason_with_costum_reason(String reasonText) throws InterruptedException {
        reject.rejectBookingOtherReason(reasonText);
    }

    @Then("owner clicks on T&C and redirect to Term And Condition Page")
    public void owner_clicks_on_T_C_and_redirect_to_Term_And_Condition_Page() throws InterruptedException {
        reject.clicksOnTermAndConditionLink();
        selenium.switchToWindow(2);
        selenium.hardWait(2);
        Assert.assertTrue(selenium.getURL().contains("syarat-dan-ketentuan-bisa-booking-pemilik"));
        selenium.switchToWindow(1);
        selenium.hardWait(2);
    }

    @Then("owner can sees Term And Condition check box is not tick")
    public void owner_can_sees_Term_And_Condition_check_box_is_not_tick() {
        Assert.assertTrue(reject.getTAndCAttributeValue("class").contains("reject-modal__tnc-check bg-c-checkbox"));
        selenium.pageScrollUsingCoordinate(0, 2000);
        Assert.assertTrue(reject.getYesRejectButtonAttributeValue("disabled").contains("true"));
    }

    @When("owner inputs other reason with {string} and tick on Term And Condition check box")
    public void owner_inputs_other_reason_with_and_tick_on_Term_And_Condition_check_box(String rejectReason) throws InterruptedException {
        reject.inputOtherReasonWithoutReject(rejectReason);
    }

    @Then("owner can sees Term And Condition is ticked and yes reject button is in active state")
    public void owner_can_sees_Term_And_Condition_is_ticked_and_yes_reject_button_is_in_active_state() {
        boolean yesRejectActivity;
        Assert.assertTrue(reject.getTAndCAttributeValue("class").contains("mami-checkbox__active"));
        Assert.assertFalse(reject.isYesCancelButtonActive("disabled"));
    }

    @When("owner tick on Term And Condition Check Box")
    public void owner_tick_on_Term_And_Condition_Check_Box() {
        reject.clickOnTAndCCheckBox();
    }

    @Then("owner can see is yes reject button is not clickable")
    public void owner_can_see_is_yes_reject_button_is_not_clickable() {
        Assert.assertTrue(reject.getYesRejectButtonAttributeValue("disabled").contains("true"));
    }

    @And("owner reject booking with bss reason")
    public void owner_reject_booking_with_bss_reason() throws InterruptedException {
        reject.rejectBookingBssReason();
    }

    @When("owner choose paid booking other reason with costum reason {string}")
    public void owner_choose_paid_booking_other_reason_with_costum_reason(String reasonsText) throws InterruptedException {
        reject.rejectBookingPaidOtherReason(reasonsText);
    }

    @And("user click on close icon on popup reject")
    public void user_click_on_close_icon_on_popup_reject() throws InterruptedException {
        reject.clickCloseButtonRejectPopup();
    }

    @Then("user can see confirmation reject popup")
    public void user_can_see_confirmation_reject_popup() throws InterruptedException {
        Assert.assertTrue(reject.isPopupConfirmationPresent(),"not appears popup");
    }

    @And("user click on tolak button on cta")
    public void user_click_on_tolak_button_on_cta() throws InterruptedException{
        reject.clickRejectOnCta();
    }
}

