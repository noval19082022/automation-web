package steps.mamikos.owner.mamipay;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.common.LeftMenuPO;
import pageobjects.mamikos.owner.mamipay.ActivateMamipayPO;
import pageobjects.mamikos.owner.mamipay.ManageBookingBillsPO;
import utilities.ThreadManager;

public class MamipaySteps {
    private WebDriver driver = ThreadManager.getDriver();
    private LeftMenuPO leftMenu = new LeftMenuPO(driver);
    private ManageBookingBillsPO manage = new ManageBookingBillsPO(driver);
    private ActivateMamipayPO mamipay = new ActivateMamipayPO(driver);

    @And("user clicks on Manage Booking And Bills Menu")
    public void user_clicks_on_manage_booking_and_bills_menu() throws InterruptedException {
        leftMenu.clickOnBookingAndBillingMenu();
    }


    @Then("user verify mamipay pop up {string} and {string}")
    public void user_verify_mamipay_pop_up(String title, String content) {
        Assert.assertTrue(manage.isTitleSliderOneExist(title), "Title on slider one doesn't exist");
        Assert.assertTrue(manage.isContentSliderOneExist(content), "Content on slider one doesn't exist");
        Assert.assertEquals(manage.getTitleSliderOne(title), title, "Title on slider one not equal to " + title);
        Assert.assertEquals(manage.getContentSliderOne(content), content, "Content on slider one not equal to " + content);
    }
    @Then("user verify button tambah data property")
    public void user_verify_button_tambah_data_property() {
        Assert.assertTrue(manage.getAddPropertyButton(), "Button add property is not showing");
    }

    @Then("user verify on button Add Property {string}")
    public void user_verify_on_button_add_property(String text) {
        Assert.assertTrue(manage.isAddPropetyButtonExist(), "Add property button doesn't exist");
        Assert.assertEquals(manage.getAddPropertyButton(), text, "Add property button text not equal to " + text);
    }

    @Then("user verify on button {string}")
    public void user_verify_on_button(String activeBooking) throws InterruptedException {
        Assert.assertTrue(manage.isButtonActivateBookingExist(), "Activate booking button doesn't exist");
        Assert.assertEquals(manage.getButtonActivateBooking(), activeBooking, "Active booking button text not equal to " + activeBooking);
    }

    @And("user clicks on OK on error Popup")
    public void user_clicks_on_ok_on_error_popup() throws InterruptedException {
        manage.clickOnPopupErrorOkButton();
    }

    @Then("user verify on button Add Occupant {string}")
    public void user_verify_on_button_add_occupant(String text) {
        Assert.assertTrue(manage.isAddOccupantDataButtonExist(), "Add occupant button doesn't exist");
        Assert.assertEquals(manage.getAddOccupantDataButton(), text, "Add occupant button text not equal to " + text);
    }

    @And("user clicks on Free Premium Pop up")
    public void user_clicks_on_free_premium_pop_up() throws InterruptedException {
        mamipay.clickOnPremiumPopup();
    }

    @And("user clicks Next on Activate Mamipay screen")
    public void user_clicks_Next_on_activate_mamipay_screen() throws InterruptedException {
        mamipay.clickOnNextButton();
    }


    @And("user clicks Skip on Activate Mamipay Screen")
    public void user_clicks_Skip_on_activate_mamipay_screen() throws InterruptedException {
        mamipay.clickOnSkipButton();
    }

    @Then("user verify Activate Mamipay Form pages")
    public void user_verify_activate_mamipay_form_pages() {
        mamipay.isActivateMamipayFormPresent();
    }

    @And("user fill out activate mamipay form with Bank Account Number {string} and Bank Owner Name {string}")
    public void user_fill_out_activate_mamipay_form_with_bank_account_number_and_bank_owner_name(String bankAccountNumber, String bankOwnerName) throws InterruptedException {
        mamipay.fillActivateMamipayForm(bankAccountNumber, bankOwnerName);
    }

    @Then("user see the form input error {string}")
    public void user_see_the_form_input_error(String messages) {
        mamipay.getFormErrorMessages(messages);
    }

    @And("user fill out activate mamipay form with Bank Account Number {string}")
    public void user_fill_out_activate_mamipay_form_with_bank_account_number(String bankAccountNumber) throws InterruptedException {
        mamipay.fillBankAccountNumberForm(bankAccountNumber);
    }

    @And("user fill out activate mamipay form with Bank Account Name {string}")
    public void user_fill_out_activate_mamipay_form_with_bank_account_name(String bankAccountName) throws InterruptedException {
        mamipay.fillBankAccountNameForm(bankAccountName);
    }

    @Then("user input field name with {string} at form activate mamipay")
    public void user_input_field_name_with_at_form_activate_mamipay(String fullName) {
        mamipay.fillInputNameForm(fullName);
    }

    @Then("user clicks on button Next BBK")
    public void user_clicks_on_button_Next_BBK() throws InterruptedException{
        mamipay.clickOnbuttonBBK();
    }

    @Then("user clicks on kost locatin Bank Account at form activate mamipay")
    public void user_clicks_on_kost_locatin_Bank_Account_at_form_activate_mamipay() throws InterruptedException {
        mamipay.clickOnBankAccount();
    }

    @Then("user see the form input error {string} at form activate mamipay")
    public void user_see_the_form_input_error_at_form_activate_mamipay(String invalidFullName) throws InterruptedException {
        Assert.assertEquals(mamipay.getErrorMessageInputName(), invalidFullName, "Error meesage not show");
    }

    @Then("user clear text field name at form activate mamipay")
    public void user_clear_text_field_name_at_form_activate_mamipay() throws InterruptedException {
        mamipay.clearTextfullName();
    }

    @Then("user see toas message input error {string}")
    public void user_see_toas_message_input_error(String toastMessage) throws InterruptedException {
        Assert.assertEquals(mamipay.geToastMessageInputName(), toastMessage, "Anda Belum Melengkapi Data");
    }

    @Then("user click on button next at form mamipay")
    public void user_click_on_button_next_at_form_mamipay() throws InterruptedException {
        mamipay.clickOnNextButtonMamipay();
    }

    @Then("user select bank account with {string}")
    public void user_select_bank_account_with(String bankName) throws InterruptedException {
        mamipay.fillInputBankName(bankName);
    }

    @Then("user fill out active mamipay form with  Bank Owner Name {string}")
    public void user_fill_out_active_mamipay_form_with_Bank_Owner_Name(String bankAccountName) throws InterruptedException {
       mamipay.fillBankAccountNameForm(bankAccountName);
    }

    @Then("user see button for submitted data activate mamipay")
    public void user_see_button_for_submitted_data_activate_mamipay() {
        Assert.assertTrue(mamipay.isActivateMamipayButtonSubmitPresent(), "Submit button doesn't exist");
    }

    @Then("user see button for submitted data activate mamipay is disabled")
    public void user_see_button_for_submitted_data_activate_mamipay_is_disabled() {
        Assert.assertFalse(mamipay.isActivateMamipayButtonSubmitPresent(), "Submit button not disabled");
    }

    @Then("user see the form input error {string} at form bank account activate mamipay")
    public void user_see_the_form_input_error_at_form_bank_account_activate_mamipay(String invalidBankAccount) throws InterruptedException {
        Assert.assertEquals(mamipay.getErrorMessageInputBankAccount(), invalidBankAccount, "Error message not show");
    }

    @Then("user see form input error {string} at form Bank Account Name activae mamipay")
    public void user_see_form_input_error_at_form_Bank_Account_Name_activae_mamipay(String invalidBankAccountName) throws InterruptedException {
        Assert.assertEquals(mamipay.getErrorMessageInputBankAccountName(), invalidBankAccountName, "Error message not show");
    }

    @Then("user see mamipay form information {string}")
    public void user_see_mamipay_form_information(String info) {
        Assert.assertEquals(mamipay.getInfoMamipayForm().trim(), info, "Info message in mamipay form is wrong");
    }

    @Then("user see text {string} beside checkbox in term and condition mamipay form")
    public void user_see_text_beside_checkbox_in_term_and_condition_mamipay_form(String agreement) {
        Assert.assertEquals(mamipay.getAgreementText().trim(), agreement, "Agreement message in mamipay form is wrong");
    }

    @When("user clicks on Terms And Conditions checkbox in Mamipay form")
    public void user_clicks_on_Terms_And_Conditions_checkbox_in_Mamipay_form() throws InterruptedException {
        mamipay.clickTermsAndConsCheckbox();
    }

    @And("user clicks on Terms And Conditions link in Mamipay form")
    public void user_clicks_on_Terms_And_Conditions_link_in_Mamipay_form() throws  InterruptedException {
        mamipay.clickOnTermAndConditionsMamipay();
    }

    @When("user click submit data button to activate mamipay")
    public void user_click_submit_data_button_to_activate_mamipay() throws InterruptedException {
        mamipay.clickSubmitButtonMamipay();
    }

    @When("user see activate mamipay form with Bank Account Number {string}")
    public void user_see_activate_mamipay_form_with_Bank_Account_Number(String accountNo) throws InterruptedException {
        Assert.assertEquals(mamipay.getInputTextBankAcc().trim(), accountNo, "Bank account number in mamipay form is wrong");
    }

    @When("user see active mamipay form with Bank Owner Name {string}")
    public void user_see_active_mamipay_form_with_Bank_Owner_Name(String bankOwnerName) {
        Assert.assertEquals(mamipay.getInputTextBankOwnerName().trim(), bankOwnerName, "Bank owner name in mamipay form is wrong");
    }

    @When("user see active mamipay form with Bank Name {string}")
    public void user_see_active_mamipay_form_with_Bank_Name(String bankName) {
        Assert.assertEquals(mamipay.getInputTextBankName().trim(), bankName, "Bank name in mamipay form is wrong");
    }

    @Then("user see activate mamipay form with Full Name {string}")
    public void user_see_activate_mamipay_form_with_Full_Name(String fullName) throws InterruptedException {
        Assert.assertEquals(mamipay.getInputTextFullName().trim(), fullName, "Full Name in mamipay form is wrong");
    }

    @Then("user click back in request activation sent page")
    public void user_click_back_in_request_activation_sent_page() throws InterruptedException {
        mamipay.clickBackButtonActivationRequest();
    }
}