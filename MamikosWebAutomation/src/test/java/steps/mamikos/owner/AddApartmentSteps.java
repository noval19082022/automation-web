package steps.mamikos.owner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.When;
import org.testng.Assert;
import pageobjects.mamikos.owner.DashboardOwnerPO;
import pageobjects.mamikos.owner.apartment.AddPO;
import pageobjects.mamikos.owner.common.AddPopUpPO;
import pageobjects.mamikos.owner.common.LeftMenuPO;
import pageobjects.mamikos.owner.common.TopSectionPO;
import pageobjects.mamikos.owner.kos.KostListPO;
import utilities.ThreadManager;

public class AddApartmentSteps {

	private WebDriver driver = ThreadManager.getDriver();
	private LeftMenuPO ownerLeftMenu = new LeftMenuPO(driver);
	private TopSectionPO ownerTopSection = new TopSectionPO(driver);
	private AddPopUpPO addPopup = new AddPopUpPO(driver);
	private AddPO appartmentAdd = new AddPO(driver);
	private KostListPO kosList = new KostListPO(driver);
	private DashboardOwnerPO dashboardOwnerPO = new DashboardOwnerPO(driver);



	@When("user clicks on Baru pop up text")
	public void user_clicks_on_Baru_pop_up_text() throws InterruptedException 
	{
		ownerLeftMenu.clickOnBaruText();
	}

	@When("user clicks on Owner Apartment Menu")
	public void user_clicks_on_Owner_Apartment_Menu() throws InterruptedException 
	{
		ownerLeftMenu.clickOnOwnerApartmentMenu();
		addPopup.clickInstantPopUp();
	}

	@When("user clicks on Add Data button")
	public void user_clicks_on_Add_Data_button() throws InterruptedException 
	{
		kosList.dismissActivePopUp();
		addPopup.clickBackAttentionPopUp();
		kosList.clickLaterInPopUp();
		ownerTopSection.clickOnAddDataButton();
	}

	@When("user clicks on Add button")
	public void user_clicks_on_Add_button() throws InterruptedException 
	{
		ownerTopSection.clickOnAddButton();
	}

	@When("user selects Appartment option and click on Add Data button")
	public void user_selects_Appartment_option_and_click_on_Add_Data_button() throws InterruptedException 
	{
		addPopup.selectAppartmentOptionAndClickOnAddButton();
	}

	@When("user fills appartment form with data appartment name {string}, unit name {string}")
	public void user_fills_appartment_form_with_data_appartment_name_unit_name(String appartment, String unit)
	{
		appartmentAdd.fillAddApartmentForm(appartment, unit);
	}

	@When("user choose Apartment on page pilih jenis properti")
	public void user_choose_apartment_on_page_pilih_jenis_properti() throws InterruptedException{
		dashboardOwnerPO.chooseApartmentPropertyType();
	}

	@And("user clicks button Buat Apartment")
	public void user_clicks_button_buat_apartment() throws InterruptedException{
		dashboardOwnerPO.clickCreateKosInPopUp();
	}

	@When("user click Edit Data Apartment")
	public void user_click_Edit_Data_Apartment() throws InterruptedException {
		appartmentAdd.clickEditApartData();
	}

	@When("user clicks on Submit button in edit apartment")
	public void user_clicks_on_Submit_button_in_edit_apartment() throws InterruptedException {
		appartmentAdd.clickSubmit();
	}

	@When("user click done in success page pop up of edit apartment")
	public void user_click_done_in_success_page_pop_up_of_edit_apartment() throws InterruptedException {
		appartmentAdd.clickDone();
	}

	@Then("user see apartment with name {string} and status {string}")
	public void user_see_apartment_with_name_and_status(String name, String status) {
		Assert.assertEquals(appartmentAdd.getFirstApartName().trim(), name, "Apartment name is wrong");
		Assert.assertEquals(appartmentAdd.getFirstApartStatus().trim(), status, "Apartment status is wrong");
	}

	@When("user fill apartment's unit number with {string}")
	public void user_fill_apartment_s_unit_number_with(String number) {
		appartmentAdd.fillUnitNumber(number);
	}

	@When("user select unit type {string}")
	public void user_select_unit_type(String unitType) {
		appartmentAdd.selectUnitType(unitType);
	}

	@When("user fill apartment's floor with {string}")
	public void user_fill_apartment_s_floor_with(String floor) {
		appartmentAdd.fillFloor(floor);
	}

	@When("user fill apartment's size with {string} m2")
	public void user_fill_apartment_s_size_with_m2(String size) {
		appartmentAdd.fillSize(size);
	}

	@When("user fill apartment's description with {string}")
	public void user_fill_apartment_s_description_with(String desc) {
		appartmentAdd.fillDesc(desc);
	}

	@Then("user see list apartemen")
	public void user_see_list_apartemen() throws InterruptedException {
		appartmentAdd.scrollApartemenList();
	}
}
