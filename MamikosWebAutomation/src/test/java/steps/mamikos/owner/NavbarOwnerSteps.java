package steps.mamikos.owner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import utilities.Constants;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class NavbarOwnerSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HeaderPO header = new HeaderPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);


    @Then("check the header menu display on homepage owner")
    public void check_the_header_menu_display_on_homepage_owner() {

            Assert.assertTrue(header.isHelpCenterOwnerDisplayed(), "Element Pusat Bantuan not present!");
            Assert.assertTrue(header.isNotificationOwnerButtonDisplayed(), "Element Notifikasi Button not present!");
        }

    @And("user click download app button and redirection to {string}")
    public void user_click_download_app_button_and_redirection_to(String url) throws InterruptedException{
        Assert.assertEquals(header.getDownloadAppURl(),url,"URL not Equals");
    }

    @And("user click promo ads button and redirection to {string}")
    public void user_click_promo_ads_button_and_redirection_to(String url)throws InterruptedException{
        Assert.assertEquals(header.getPromoAdsURl(),Constants.MAMIKOS_URL + url,"URL not Equals");
    }

    @And("user click back button in page")
    public void click_back_button_in_page() {
        selenium.back();
    }


    @And("user click help center button and redirection to {string}")
    public void user_click_help_center_button_and_redirection_to(String url) throws InterruptedException{
        Assert.assertTrue(header.getHelpCenterURl().contains(url),"URL not Equals");
    }

    @And("user click header Search Ads")
    public void user_click_header_search_ads() throws InterruptedException {
        header.clickOnSearchAds();
    }

    @And("user choose kos dropdownlist and redirection to {string}")
    public void user_choose_kos_dropdown_list_and_redirection_to(String url) throws InterruptedException {
        Assert.assertEquals(header.getDropdownKos(),Constants.MAMIKOS_URL + url,"URL not Equals");
    }

    @And("user choose Apartemen dropdownlist and redirection to {string}")
    public void user_choose_apartemen_dropdownlist_and_redirection_to(String url) throws InterruptedException {
        Assert.assertEquals(header.getDropdownApartementAds(),Constants.MAMIKOS_URL + url,"URL not Equals");
    }

//    Commented because the job vacancy page is temporarily hidden
//    @Then("user choose Job Ads dropdownlist and redirection to {string}")
//    public void user_choose_job_ads_dropdownlist_and_redirection_to(String url) throws InterruptedException{
//        Assert.assertEquals(header.getDropdownJobAds(),Constants.MAMIKOS_URL + url,"URL not Equals");
//    }

    @Then("user see dropdown with button owner page and exit")
    public void user_see_dropdown_with_button_owner_page_and_exit() {
        Assert.assertTrue(header.isOwnerPageDisplayed(), "Owner page menu is missing");
        Assert.assertTrue(header.isExitButtonDisplayed(), "Exit menu is missing");
    }

    @When("user click owner page button")
    public void user_click_owner_page_button() throws InterruptedException {
        header.clickOnOwnerPage();
    }

    @When("user click booking kos button and redirection to {string}")
    public void user_click_booking_kos_button_and_redirection_to(String url) throws InterruptedException {
        Assert.assertEquals(header.getBookingKosURL(),Constants.MAMIKOS_URL + url,"URL not Equals");
    }
}

