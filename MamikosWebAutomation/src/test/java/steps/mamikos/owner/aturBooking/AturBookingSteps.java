package steps.mamikos.owner.aturBooking;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.aturBooking.AturBookingPO;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import utilities.ThreadManager;
import pageobjects.pmsSinggahsini.CommonPO;

public class AturBookingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AturBookingPO atur= new AturBookingPO(driver);
    private KosDetailPO kosDetailPO = new KosDetailPO(driver);
    private CommonPO common = new CommonPO(driver);


    @Then("user can see confirmation Atur Booking popup")
    public void user_can_see_confirmation_atur_booking_popup() {
        Assert.assertEquals(atur.getTitleText(),"Buat peraturan masuk kos, agar dapat penyewa yang cocok");
   }

    @And("user click on atur booking button")
    public void user_click_on_atur_booking_button() throws InterruptedException {
        atur.clickManageBookingBtn();
    }

    @Then("user can see atur booking page")
    public void user_can_see_atur_booking_page(){
        Assert.assertEquals(atur.getManageBookingTitile(),"Di sini, Anda bisa mengatur booking seperti apa yang bisa diajukan calon penyewa.");
    }

    @And("user click Ubah Peraturan Masuk Kos section at dashboard")
    public void user_click_ubah_peraturan_masuk_kos_section_at_dashboard() throws InterruptedException {
        Assert.assertEquals(atur.getTitleUbahPeraturanKos(), "Ubah Peraturan Masuk Kos\n" +
                "Aturan untuk calon penyewa", "Title is not match");
        atur.clickUbahPeraturanKosDashboard();
    }

    @Then("user redirect to Peraturan Masuk Kos page")
    public void user_redirect_to_peraturan_masuk_kos_page(){
        Assert.assertEquals(atur.getTitlePeraturanMasukKosPage(), "Peraturan saat masuk kos", "Title is not match");
    }

    @When("user click Pengajuan Booking")
    public void user_click_pengajuan_booking() throws InterruptedException {
        atur.clickPengajuanBooking();
    }

    @And("user click Ubah Aturan at pengajuan booking page")
    public void user_click_ubah_aturan_at_pengajuan_booking_page() throws InterruptedException {
        common.waitMultipleJavascriptLoading(7);
        Assert.assertEquals(atur.getAturBookingText(),"Ubah aturan", "not appear button");
        atur.clickUbahAturan();
    }

    @And("user select kost GP {string}")
    public void user_select_kost_gp(String kostGp) throws InterruptedException {
        atur.selectKostDropdown(kostGp);
    }

    @And("user click Simpan at Peraturan Masuk Kos page")
    public void user_click_simpan_at_peraturan_masuk_kos_page() throws InterruptedException {
        atur.clickSimpanPeratrnMskKos();
    }

    @Then("user will see toast Peraturan Masuk Kos")
    public void user_will_see_toast_peraturan_masuk_kos(){
        Assert.assertEquals(atur.getToastSimpanKosGp(), "Untuk mengubah aturan, mohon hubungi tim Mamikos yang mengelola kos Anda.", "wording toast is not matcg");
    }

    @When("user choose kost name with {string}")
    public void user_choose_kost_name_with_x(String kostNotGp) throws InterruptedException {
        atur.clickKosNameSelected(kostNotGp);
    }

    @And("user activated {string}")
    public void user_activated_special_kost_kriteria_for_x(String type) throws InterruptedException {
        atur.clickKriteriaPenyewa();
        if (type.equalsIgnoreCase("special kos kriteria")){
            atur.actiavtedOnKosKhusus();
        }
        else if (type.equalsIgnoreCase("bisa pasutri")){
            atur.activatedSetPasutri();
        }
        else if (type.equalsIgnoreCase("boleh bawa anak")){
            atur.activatedSetBawaAnak();
        }
        else if (type.equalsIgnoreCase("KTP")){
            atur.activatedKtp();
        }
        else if (type.equalsIgnoreCase("Buku nikah")){
            atur.clickKriteriaPenyewa();
            atur.activatedSetBukuNikah();
        }
        else if (type.equalsIgnoreCase("Kartu Keluarga")){
            atur.clickKriteriaPenyewa();
            atur.activatedSetKK();
        }
        else if (type.equalsIgnoreCase("KK")){
            atur.activatedSetKK();
        }
        else if (type.equalsIgnoreCase("Wajib sertakan buku nikah")){
            atur.activatedSetBukuNikah();
        }
    }

    @And("user click on Simpan button on atur page")
    public void user_click_on_simpan_button_on_atur_page() throws InterruptedException {
        atur.clickSimpanPeratrnMskKos();
    }

    @Then("user can see toast {string}")
    public void user_can_see_toast_x(String toast) throws InterruptedException {
        Assert.assertEquals(atur.getToastSimpanNotGp(),toast, toast + "not present ");
    }

    @Then("user can see Peraturan kos with {string}")
    public void user_can_see_peraturan_kos(String kostRule)throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();

        if(kostRule.equalsIgnoreCase("Khusus Karyawan")){
            Assert.assertEquals(atur.kostEmployeesAsPresent(), kostRule, kostRule + "not present");
        }
        else if (kostRule.equalsIgnoreCase("Boleh pasutri")){
            Assert.assertEquals(atur.getPasutriText(), kostRule, kostRule + "not present");
        }
        else if (kostRule.equalsIgnoreCase("Boleh bawa anak")){
            Assert.assertEquals(atur.getBawaAnakText(), kostRule, kostRule + "not present");
        }
        else if (kostRule.equalsIgnoreCase("Khusus Mahasiswa")){
            Assert.assertEquals(atur.getMahasiswaText(), kostRule, kostRule + "not present");
        }
    }

    @And("button {string} will disable")
    public void button_x_wll_disable(String type) throws InterruptedException{
        if (type.equalsIgnoreCase("kamar hanya bagi penyewa")) {
            Assert.assertTrue(atur.disableRoomForTenantOnly(),"Button is enable ");
        }
        else if (type.equalsIgnoreCase("Maks 2 orang/kamar")){
            Assert.assertTrue(atur.disableTwoTenant(),"button is enable");
        }
    }

    @When("user select and click kos with {string}")
    public void user_select_and_click_kost_with_x(String kostNotGp1) throws InterruptedException {
        atur.clickKosName(kostNotGp1);
    }

    @And("user select on Mahasiswa")
    public void user_select_on_mahasiswa() throws InterruptedException {
        atur.selectKosMahasiswa();
    }

    @Then("{string} is activated")
    public void x_is_activated(String name) throws InterruptedException {
        if(name.equalsIgnoreCase("Boleh bawa anak")) {
            atur.isBawaAnakToogleActive();
        }
        else if(name.equalsIgnoreCase("Boleh untuk pasutri")){
            atur.isPasutriToogleActive();
        }
    }

    @And("user clicks on Ubah waktu button")
    public void user_clicks_on_ubah_waktu_button()throws InterruptedException{
        atur.clickWaktuMulaiKos();
        atur.clickOnUpdateTime();
    }

    @And("user nonactive booking today toogle")
    public void user_nonactive_booking_today_toogle() throws InterruptedException{
        atur.clickOnH0AfterBooking();
    }

    @And("user choose minim checkin date with {string}")
    public void user_choose_minim_checkin_date_with_x(String checkinDate) throws InterruptedException{
        atur.selectMinusCheckinDate(checkinDate);
    }

    @And("user choose minim checkin time with {string}")
    public void user_choose_minim_checkin_time_with_x(String checkinTime) throws InterruptedException{
        atur.selectMinusCheckinTime(checkinTime);
    }

    @And("user clicks on Simpan button on update checkin time popup")
    public void user_clicks_on_simpan_button_on_update_checkin_time_popup() throws InterruptedException{
        atur.clickOnSaveButton();
    }

    @Then("user can see Ketentuan Kos with {string} and {string}")
    public void user_can_see_ketentuan_kos_with_x_and_y(String text1, String text2)throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        Assert.assertEquals(atur.nearestCheckinRuleAsPresent(), text1, "not present kost rule");
        Assert.assertEquals(atur.furthestCheckinRuleAsPresent(), text2, "not present kost rule");
    }

  }
