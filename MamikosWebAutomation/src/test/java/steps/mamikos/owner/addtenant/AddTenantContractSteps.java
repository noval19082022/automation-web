package steps.mamikos.owner.addtenant;

import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang.NullArgumentException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.addtenant.AddTenantContractPO;
import pageobjects.mamikos.tenant.profile.EditProfilePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class AddTenantContractSteps {

    private WebDriver driver = ThreadManager.getDriver();
    SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private AddTenantContractPO addTenantPO = new AddTenantContractPO(driver);
    private JavaHelpers java = new JavaHelpers();
    private EditProfilePO editProfilePO = new EditProfilePO(driver);

    private String kostName = null;

    //Test Data Apply Voucher
    private String voucherku="src/test/resources/testdata/mamikos/voucherku.properties";
    private String kostNameForApplyVoucherBBK = JavaHelpers.getPropertyValue(voucherku,"kostNameForApplyVoucherBBK_" + Constants.ENV);
    private String phoneNumberForApplyVoucherBBK = JavaHelpers.getPropertyValue(voucherku,"phoneNumberForApplyVoucherBBK_"+ Constants.ENV);
    private String roomNumberForApplyVoucherBBK = JavaHelpers.getPropertyValue(voucherku,"roomNumberForApplyVoucherBBK_" + Constants.ENV);
    private String tenantNameForApplyVoucherBBK = JavaHelpers.getPropertyValue(voucherku, "tenantNameForApplyVoucherBBK_" + Constants.ENV);
    private String genderForApplyVoucherBBK = JavaHelpers.getPropertyValue(voucherku, "genderForApplyVoucherBBK_" + Constants.ENV);
    private String rentCountForApplyVoucher = JavaHelpers.getPropertyValue(voucherku,"rentCount");
    private String roomPriceForApplyVoucherBBK = JavaHelpers.getPropertyValue(voucherku, "roomPriceForApplyVoucherBBK_"+ Constants.ENV);
    private String rentDurationForApplyVoucher = JavaHelpers.getPropertyValue(voucherku, "rentDuration");
    private String applyVoucherNewGoldPlus3 = JavaHelpers.getPropertyValue(voucherku,"kostNameForApplyVoucherNewGoldPlus3_" + Constants.ENV);

    //OB Test Data And File
    private String obOwnerFile = "src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
    private String obTenantFile = "src/test/resources/testdata/occupancy-and-billing/tenant.properties";
    private String gpKostMarkRoomName = JavaHelpers.getPropertyValue(obOwnerFile, "gpKostUprasDua_" + Constants.ENV);
    private String fullKostName = JavaHelpers.getPropertyValue(obOwnerFile, "kostNameFull_" + Constants.ENV);
    private String femaleKostName = JavaHelpers.getPropertyValue(obOwnerFile, "kostNameFemale_" + Constants.ENV);
    private String malePhone = JavaHelpers.getPropertyValue(obTenantFile, "sakti_" + Constants.ENV);


    @And("user select kost {string} for tenant")
    public void user_select_kost_for_tenant(String kostName) throws InterruptedException {
        if(kostName.equals("applyVoucherBBK")){
            addTenantPO.selectKost(kostNameForApplyVoucherBBK);
        }
        else if(kostName.equals("applyVoucherNewGoldPlus3")){
            addTenantPO.selectKost(applyVoucherNewGoldPlus3);
        }
        else if(kostName.equals("full kost")) {
            addTenantPO.selectKost(fullKostName);
        }
        else if(kostName.equals("female kost")) {
            addTenantPO.selectKost(femaleKostName);
        }
    }

    @And("user input tenant information for {string}")
    public void user_input_tenant_information_for(String tenantType) throws InterruptedException {
        String phoneNumber = "";
        String roomNumber = "";
        String tenantName = "";
        String gender = "";

        if (tenantType.equals("apply voucher")){
            phoneNumber = phoneNumberForApplyVoucherBBK;
            roomNumber = roomNumberForApplyVoucherBBK;
            tenantName = tenantNameForApplyVoucherBBK;
            gender = genderForApplyVoucherBBK;
        }
        addTenantPO.setTenantPhoneNumber(phoneNumber);
        addTenantPO.selectRoomNumber(roomNumber);
        addTenantPO.clickAddTenantButton();
        addTenantPO.setTenantName(tenantName);
        addTenantPO.chooseTenantGender(gender);
        addTenantPO.clickONextButton();
    }

    @And("user input payment information for {string}")
    public void user_input_payment_information_for(String tenantType) throws ParseException, InterruptedException {
        String rentCount = "";
        String rentPrice = "";
        String rentDuration = "";
        String dueDate = "";

        if (tenantType.equals("apply voucher")){
            String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
            rentCount = rentCountForApplyVoucher;
            rentPrice = roomPriceForApplyVoucherBBK;
            rentDuration = rentDurationForApplyVoucher;
            dueDate = today;
        }
        addTenantPO.selectRentCount(rentCount);
        addTenantPO.setRentPrice(rentPrice);
        addTenantPO.selectRentDuration(rentDuration);
        addTenantPO.selectDueDate(dueDate);
        addTenantPO.clickONextButton();
    }

    @And("user click next button on other cost information")
    public void user_click_next_button_on_other_cost_information() throws InterruptedException {
        addTenantPO.clickONextButton();
    }

    @And("user click save button on detail payment")
    public void user_click_save_button_on_detail_payment() throws InterruptedException {
        addTenantPO.clickOnSaveButton();
    }

    @And("user verify pop up success save tenant data")
    public void user_verify_pop_up_success_save_tenant_data() {
        Assert.assertTrue(addTenantPO.verifyPopUpTitleAddTenant() &&
                addTenantPO.verifyPopUpDescriptionAddTenant() &&
                addTenantPO.verifySeeTenantDetailButton() &&
                addTenantPO.verifyAddOtherTenantButton());
    }

    @And("user click continue until start adding contract")
    public void user_click_continue_until_start_adding_contract() throws InterruptedException {
        for(int i = 0; i<3;i++){
            addTenantPO.clickOnContinueButton();
        }
        addTenantPO.clickOnStartButton();
        addTenantPO.clickOnAddContractButton();
    }

    @Then("owner redirected to Input Renter's Information form with kost name {string}")
    public void owner_redirected_to_Input_Renter_s_Information_form(String kostName) throws InterruptedException {
        this.kostName = kostName.toLowerCase();
        switch (kostName) {
            case "mark kost gp":
                this.kostName = gpKostMarkRoomName;
                break;
            default:
                this.kostName = kostName;
        }
        Assert.assertEquals(addTenantPO.getFormTitle(), "Masukkan Informasi Penyewa");
        Assert.assertEquals(addTenantPO.getSelectedKostName().toLowerCase(), this.kostName.toLowerCase());
        Assert.assertTrue(addTenantPO.getFullRoomName().contains("Kamar"));
    }

    @Then("owner can sees full pop up restriction")
    public void owner_can_sees_full_pop_up_restriction() {
        Assert.assertTrue(addTenantPO.isFullRoomPopUp());
        Assert.assertEquals(addTenantPO.getFullRoomRestrictionTitle(), "Seluruh Kamar Kos Sudah Terisi");
        Assert.assertEquals(addTenantPO.getFullRoomRestrictionDescription(), "Saat ini sistem kami mencatat seluruh kamar Kos Anda sudah penuh. Anda bisa mengubahnya di sini.");
    }

    @When("owner clicks on change room's data on full room pop up restriction")
    public void owner_clicks_on_change_room_s_data_on_full_room_pop_up_restriction(){
        addTenantPO.clicsOnChangeRoomDataButton();
    }

    @Then("owner redirected to update room page")
    public void owner_redirected_to_update_room_page() throws InterruptedException {
        Assert.assertTrue(selenium.getURL().contains("/edit"));
        selenium.hardWait(10);
        selenium.back();
    }

    @Then("owner can not sees full room pop up restriction")
    public void owner_can_not_sees_full_room_pop_up_restriction() {
        Assert.assertFalse(addTenantPO.isFullRoomPopUp());
    }

    @When("owner input phone number with {string} and choose first available room and clicks on add renter button")
    public void owner_input_phone_number_with_and_choose_first_available_room_and_clicks_on_add_renter_button(String phoneNumber) throws InterruptedException {
        if(phoneNumber.equalsIgnoreCase("male phone number")) {
            phoneNumber = malePhone;
        }else {
            throw new IllegalArgumentException("Input with available choice");
        }
        addTenantPO.setTenantPhoneNumber(phoneNumber);
        addTenantPO.selectFirstAvailableRoom();
        selenium.hardWait(3);
        addTenantPO.clickAddTenantButton();
    }

    @Then("owner can sees different gender restriction pop-up")
    public void owner_can_sees_different_gender_restriction_pop_up() throws InterruptedException{
        Assert.assertEquals(addTenantPO.getDifferentGenderPopUpTitleText(), "Jenis kelamin tidak sama!");
        Assert.assertEquals(addTenantPO.getDifferentGenderPopUpDescriptionText(), "Jenis kelamin penghuni kost harus sama dengan detail kost.");
    }
}
