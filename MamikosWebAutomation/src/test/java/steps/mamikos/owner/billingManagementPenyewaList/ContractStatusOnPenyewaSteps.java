package steps.mamikos.owner.billingManagementPenyewaList;

import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang.NullArgumentException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.addtenant.AddTenantContractPO;
import pageobjects.mamikos.owner.billingManagementPenyewaList.ContractStatusOnPenyewaPO;
import pageobjects.mamikos.tenant.profile.EditProfilePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class ContractStatusOnPenyewaSteps {

    private WebDriver driver = ThreadManager.getDriver();
    SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private ContractStatusOnPenyewaPO penyewaPO = new ContractStatusOnPenyewaPO(driver);

    private JavaHelpers java = new JavaHelpers();

    //Test data OB for penyewa kontrak sewa section
    private String propertyFile1="src/test/resources/testdata/mamikos/OB.properties";
    private String propertyFile2="src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
    private String totalBiayaSewa=JavaHelpers.getPropertyValue(propertyFile1, "totalBiayaSewa_" + Constants.ENV);
    private String hargaSewa=JavaHelpers.getPropertyValue(propertyFile1, "hargaSewa_" + Constants.ENV);
    private String listrikName=JavaHelpers.getPropertyValue(propertyFile1, "listrikName_" + Constants.ENV);
    private String listrikPrice=JavaHelpers.getPropertyValue(propertyFile1, "listrik_" + Constants.ENV);
    private String sampahName=JavaHelpers.getPropertyValue(propertyFile1, "sampahName_" + Constants.ENV);
    private String sampahPrice=JavaHelpers.getPropertyValue(propertyFile1, "sampah_" + Constants.ENV);
    private String totalBiayaSewaFullPay = JavaHelpers.getPropertyValue(propertyFile2, "totalBiayaSewaFullPay_" + Constants.ENV);
    private String hargaSewaFullPay = JavaHelpers.getPropertyValue(propertyFile2, "hargaSewaFullPay_" + Constants.ENV);
    private String biayaLain1FullPay = JavaHelpers.getPropertyValue(propertyFile2, "biayaLain1FullPay_" + Constants.ENV);
    private String biayaLainPrice1FullPay = JavaHelpers.getPropertyValue(propertyFile2, "biayaLain1PriceFullPay_" + Constants.ENV);
    private String totalBiayaSewaDpst = JavaHelpers.getPropertyValue(propertyFile2, "totalBiayaSewaDpst_" + Constants.ENV);
    private String hargaSewaDpst = JavaHelpers.getPropertyValue(propertyFile2, "hargaSewaDpst_" + Constants.ENV);
    private String biayaLain1Dpst = JavaHelpers.getPropertyValue(propertyFile2, "biayaLain1Dpst_" + Constants.ENV);
    private String biayaLainPrice1Dpst = JavaHelpers.getPropertyValue(propertyFile2, "biayaLain1PriceDpst_" + Constants.ENV);

    @Then("user can see Penyewa list")
    public void user_can_see_penyewa_list(){
        Assert.assertEquals(penyewaPO.getPenyewaTitle(), "Penyewa", "you are not in Penyewa page");
    }

    @And("user click on Kost name search")
    public void user_click_on_kost_name_search() throws InterruptedException {
        penyewaPO.clickSearchKost();
    }

    @And("user search kost {string}")
    public void user_search_kost(String searchKost) throws InterruptedException{
        penyewaPO.searchKostOnPenyewa(searchKost);
    }

    @And("user click on dropdown Filter box and select {string} filter")
    public void user_click_on_dropdown_filter_box_and_select_filter(String filter) throws InterruptedException {
        penyewaPO.clickFilterOnPenyewa(filter);
    }

    @Then("user will see contract with status {string}")
    public void user_will_see_contract_with_status(String contractStatus) throws InterruptedException{
        Assert.assertEquals(penyewaPO.getContractStatus(contractStatus), contractStatus, "contract status is not match");
    }

    @And("user click download biodata penyewa button")
    public void user_click_download_biodata_penyewa_button() throws InterruptedException {
        penyewaPO.clickDownloadBiodataPenyewa();
    }

    @Then("user will see pop up for upcoming feature")
    public void user_will_see_pop_up_for_upcoming_feature(){
        Assert.assertEquals(penyewaPO.getPopUpDownloadBiodataPenyewa(), "Fitur ini sedang kami kembangkan", "wording pop up is not match");
    }

    @And("user tick on checkbox pop up")
    public void user_tick_on_checkbox_pop_up() throws InterruptedException {
        penyewaPO.tickOnCheckBoxPopUpDownloadBiodata();
    }

    @Then("user will see information about upcoming feature")
    public void user_will_see_information_about_upcoming_feature() throws InterruptedException {
        Assert.assertEquals(penyewaPO.getInformationAfterTickCheckbox(), "Kami akan memberitahu Anda saat fitur ini sudah tersedia.", "wording pop up is not match");
        penyewaPO.clickKembaliOnPopUpAfterTickCheckbox();
    }

    @And("user click Selengkapnya button on {string} contract")
    public void user_click_selengkapnya_button_on_contract(String contract) throws InterruptedException {
        int numberOfList = penyewaPO.getNumberListOfContract();
        for (int i=1; i<=numberOfList; i++){
            if (penyewaPO.getContractName(i).equals(contract)){
                selenium.hardWait(2);
                penyewaPO.clickSelengkapnyaNew(i);
            }
        }
    }

    @And("user click tab Kontrak sewa")
    public void user_click_tab_kontrak_sewa() throws InterruptedException {
        penyewaPO.clickKontrakSewa();
    }

    @And("user click Ubah kontrak penyewa button")
    public void user_click_ubah_kontrak_penyewa_button() throws InterruptedException {
        penyewaPO.clickUbahKontrakPenyewa();
    }

    @Then("user check prices at penyewa owner are same to contract at kos saya")
    public void user_check_prices_at_penyewa_owner_are_same_to_contract_at_kos_saya(){
        Assert.assertEquals(penyewaPO.getTotalBiayaPrice(), "Rp" + totalBiayaSewa + " / bulan", "The price is not match");
        Assert.assertEquals(penyewaPO.getHargaSewaPrice(), hargaSewa, "The price is not match");
        Assert.assertEquals(penyewaPO.getBiayaLain1(), listrikName, "The wording is not match");
        Assert.assertEquals(penyewaPO.getBiayaLainPrice1(), listrikPrice, "The price is not match");
        Assert.assertEquals(penyewaPO.getSampahName(), sampahName, "The wording is not match");
        Assert.assertEquals(penyewaPO.getSampahPrice(), sampahPrice, "The price is not match");
    }

    @Then("user will see wording of warning tenant who don't have kos saya at Semua filter")
    public void user_will_see_wording_of_warning_tenant_who_dont_have_kos_saya_at_semua_filter(){
        Assert.assertEquals(penyewaPO.getWarningAtSemuaFltr(), penyewaPO.getWarningAtSemuaFltr(), "The wording is not match");
    }

    @When("user owner click Siapa saja penyewa yang belum hyperlink")
    public void user_owner_click_siapa_saja_penyewa_yang_belum_bayar_hyperlink() throws InterruptedException {
        penyewaPO.clickHyperlinkSemuaFltr();
    }

    @Then("user will redirect to Sedang menyewa filter and user will see wording of warning tenant who don't have kos saya")
    public void user_will_redirect_to_sedang_menyewa_filter_and_user_will_see_wording_of_warning_tenant_who_dont_have_kos_saya(){
        Assert.assertEquals(penyewaPO.getWarningAtSdgMenyewaFltr(), penyewaPO.getWarningAtSdgMenyewaFltr(), "The wording is not match");
        Assert.assertEquals(penyewaPO.getWarningLine2AtSdgMenyewaFltr(), penyewaPO.getWarningLine2AtSdgMenyewaFltr(), "The wording is not match");
    }

    //-----------waiting terminated confirmation------------//
    @And("user will see alert terminatation")
    public void user_will_see_alert_termination(){
        Assert.assertEquals(penyewaPO.getAlertTermination(), penyewaPO.getAlertTermination(), "The Alert is not match");
    }

    @When("user click Selengkapnya button")
    public void user_click_selengkapnya_button() throws InterruptedException {
        penyewaPO.clickSelengkapnya();
    }

    @Then("user will see Tolak and Konfirmasi button on detail penyewa page")
    public void user_will_see_tolak_and_konfirmasi_button_on_detail_penyewa_page(){
        Assert.assertEquals(penyewaPO.getTolakButton(), penyewaPO.getTolakButton(), "The button is not exist");
        Assert.assertEquals(penyewaPO.getKonfirmasiButton(), penyewaPO.getKonfirmasiButton(), "The button is not exist");
    }

    //------------change owner's phone number------------//
    @And("user click Kirim ulang kode hyperlink")
    public void user_click_kirim_ulang_kode_hyperlink() throws InterruptedException {
        penyewaPO.clickKirimUlangKode();
    }

    @Then("user will redirect to Kirim kode unik ke penyewa page")
    public void user_will_redirect_to_kirim_kode_unik_ke_penyewa_page(){
        Assert.assertEquals(penyewaPO.getKrmKodeUnikPage(), penyewaPO.getKrmKodeUnikPage(), "You are not Kirim kode unik page");
    }

    @And("user will see old number of owner {string}")
    public void user_will_see_old_number_of_owner(String oldNumber){
        Assert.assertEquals(penyewaPO.getPhoneNumberPenyewa(), oldNumber, "The number phone is not match");
    }

    @When("user click Ubah nomor HP hyperlink")
    public void user_click_ubah_nomor_hp_hyperlink() throws InterruptedException {
        penyewaPO.clickUbahNmrHp();
    }

    @And("user change owner's phone number into {string} and click Gunakan")
    public void user_change_owners_phone_number_into_and_click_gunakan(String ubhPhoneNumber) throws InterruptedException {
        penyewaPO.clickPhoneNmbField(ubhPhoneNumber);
    }

    @Then("user will see new number phone of owner {string}")
    public void user_will_see_new_number_phone_of_owner(String newNumber){
        Assert.assertEquals(penyewaPO.getPhoneNumberPenyewa(), newNumber, "The number phone is not match");
    }

    @When("user change number again to old number {string}")
    public void user_change_number_again_to_old_number(String ubhPhoneNumber) throws InterruptedException {
        penyewaPO.clickUbahNmrHp();
        penyewaPO.changeToFirstCondition(ubhPhoneNumber);
    }

    @Then("user will see the old number of owner {string}")
    public void user_will_see_the_old_number_of_owner(String newNumber){
        Assert.assertEquals(penyewaPO.getPhoneNumberPenyewa(), newNumber, "The number phone is not match");
    }

    //--------------------------------------check full payment-------------------------------------//
    @Then("user check prices at penyewa owner are same to contract at kos saya for full payment")
    public void user_check_prices_at_penyewa_owner_are_same_to_contract_at_kos_saya_for_full_payment(){
        Assert.assertEquals(penyewaPO.getTotalBiayaPrice(), "Rp" + totalBiayaSewaFullPay + " / bulan", "The price is not match");
        Assert.assertEquals(penyewaPO.getHargaSewaPrice(), hargaSewaFullPay, "The price is not match");
        Assert.assertEquals(penyewaPO.getBiayaLain1(), biayaLain1FullPay, "The wording is not match");
        Assert.assertEquals(penyewaPO.getBiayaLainPrice1(), biayaLainPrice1FullPay, "The price is not match");
    }

    //--------------------------------------check DPST payment-------------------------------------//
    @Then("user check prices at penyewa owner are same to contract at kos saya for dpst payment")
    public void user_check_prices_at_penyewa_owner_are_same_to_contract_at_kos_saya_for_dpst_payment() {
        Assert.assertEquals(penyewaPO.getTotalBiayaPrice(), "Rp" + totalBiayaSewaDpst + " / bulan", "The price is not match");
        Assert.assertEquals(penyewaPO.getHargaSewaPrice(), hargaSewaDpst, "The price is not match");
        Assert.assertEquals(penyewaPO.getBiayaLain1(), biayaLain1Dpst, "The wording is not match");
        Assert.assertEquals(penyewaPO.getBiayaLainPrice1(), biayaLainPrice1Dpst, "The price is not match");
    }
}
