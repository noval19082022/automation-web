package steps.mamikos.owner.settings;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.owner.common.AddPopUpPO;
import pageobjects.mamikos.owner.common.LeftMenuPO;
import pageobjects.mamikos.owner.settings.SettingsPO;
import utilities.ThreadManager;

import java.util.Random;

public class SettingsSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SettingsPO settingsPO = new SettingsPO(driver);
    private HeaderPO header = new HeaderPO(driver);
    private AddPopUpPO popUp = new AddPopUpPO(driver);

    @And("user clicks on Owner Settings button")
    public void user_clicks_on_Owner_Settings_button() throws InterruptedException {
        header.clickSettings();
        popUp.clickInstantPopUp();
    }

    @When("user clicks on Change Name button")
    public void user_clicks_on_Change_Name_button() throws InterruptedException {
        settingsPO.clickOnChangeNameButton();
    }

    @And("user fills phone number owner registered new flow {string}")
    public void user_fills_phone_number(String phone) throws InterruptedException {
        settingsPO.inputPhoneNumberOwnerRegisteredNewFlow(phone);
    }

    @And("user fills out name input with {string}")
    public void user_fills_out_name_input_with(String name) throws InterruptedException {
        settingsPO.inputFormNameAndClickSubmitButton(name);
    }

    @Then("user verify username text as {string}")
    public void user_verify_username_text_as(String name) {
        Assert.assertEquals(settingsPO.getUsernameText(name), name, "Username is not equal to " + name);
    }

    @Then("user verify Error popup message {string}")
    public void user_verify_Error_popup_message(String content) {
        Assert.assertTrue(settingsPO.isErrorPopUpPresent(), "Error pop up is not present");
        Assert.assertEquals(settingsPO.getErrorPopupText(), content, "Error pop up message is not equal to "+content);
    }

    @And("user clicks on Submit button")
    public void user_clicks_on_Submit_button() throws InterruptedException {
        settingsPO.clickonSubmitChangeName();
    }

    @And("user clicks on Simpan button")
    public void user_clicks_on_Simpan_button() throws InterruptedException {
        settingsPO.clickonSimpanChangeNoHp();
    }

    @When("user fills out name input with {string} + random text")
    public void user_fills_out_name_input_with_random_text(String name) throws InterruptedException {
        settingsPO.inputFormNameAndClickSubmitButton(name + " " + RandomStringUtils.randomAlphabetic(5));
    }

    @When("user click recommendation via email")
    public void user_click_recommendation_via_email() throws InterruptedException {
        settingsPO.clickRecommendationViaEmail();
    }

    @When("user click chat notification")
    public void user_click_chat_notification() throws InterruptedException {
        settingsPO.clickChatNotification();
    }

    @When("user click SMS update kos")
    public void user_click_SMS_update_kos() throws InterruptedException {
        settingsPO.clickSMSUpdateKos();
    }

    @Then("user see pop up error message {string} in settings page")
    public void user_see_pop_up_error_message_in_settings_page(String errorText) {
        Assert.assertEquals(settingsPO.getErrorPopupText(), errorText, "Error message not appear/wrong");
    }

    @Then("user can only input max 20 characters : {string}")
    public void user_can_only_input_max_20_characters(String inputText) {
        Assert.assertEquals(settingsPO.getInputNameText(), inputText, "Can insert more than 20 characters");
    }

    @Then("user see input phone number placeholder {string}")
    public void user_see_button_simpan_disable(String placeholder) {
        Assert.assertEquals(settingsPO.getPhoneNumberPlaceHolder(), placeholder, "Phone number placeholder is not equal to " + placeholder);
    }

    @Then("user see message error validation {string}")
    public void user_see_message_error_validation (String message) {
        Assert.assertEquals(settingsPO.userSeeMessageErrorValidation(), message, "error messages is not equal to " + message);
    }

    @And("user clicks on Change Nomor Handphone button")
    public void user_clicks_on_Change_Nomor_Handphone_button() throws InterruptedException {
        settingsPO.clickChangePhoneNumberButton();
    }

    @And("user see otp pop up")
    public void user_see_otp_pop_up() {
        Assert.assertTrue(settingsPO.isOTPPopUpAppear(), "OTP pop up is not appear");
    }

    @Then("user click on resend otp button")
    public void user_click_on_resend_otp_button() throws InterruptedException {
        settingsPO.clickOnResendOTPButton();
        Assert.assertTrue(settingsPO.isOTPPopUpAppear(), "OTP pop up is not appeared");
    }

    @Then("user click OK in pop up in settings page")
    public void user_click_OK_in_pop_up_in_settings_page() throws InterruptedException {
        settingsPO.clickonOkButton();
    }

    @Then("user see toast message {string} in settings page")
    public void user_see_toast_message_in_settings_page(String string) throws InterruptedException {
        Assert.assertEquals(settingsPO.getToast(), string, "Warning daily price is not correct");
    }

    @Then("user verify profile picture is null")
    public void user_verify_profile_picture_is_null() {
        Assert.assertTrue(settingsPO.isProfilePictureNull(), "Profile picture is not null");
    }

    @Then("user verify profile picture is show")
    public void user_verify_profile_picture_is_show() {
        Assert.assertTrue(settingsPO.isProfilePictureNotNull(), "Profile picture is null");
    }

    @Then("user verify mamipoin is available")
    public void user_verify_mamipoin_is_available() {
        Assert.assertTrue(settingsPO.isMamipoinAvailable(), "Mamipoin is not available");
    }


}
