package steps.mamikos.owner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.owner.common.AddPopUpPO;
import pageobjects.mamikos.owner.kos.AddKosPO;
import pageobjects.mamikos.owner.kos.KostListPO;
import pageobjects.mamikos.owner.kos.UpdateRoomPO;
import pageobjects.mamikos.mamiads.UpgradePremiumPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.List;

public class UpdateRoomSteps {

	private WebDriver driver = ThreadManager.getDriver();
	private KostListPO kostList = new KostListPO(driver);
	private AddKosPO addkos = new AddKosPO(driver);
	private UpgradePremiumPO upgradePremium = new UpgradePremiumPO(driver);
	private AddPopUpPO popUp = new AddPopUpPO(driver);
	private UpdateRoomPO updateRoom = new UpdateRoomPO(driver);
	private SeleniumHelpers selenium = new SeleniumHelpers(driver);


	private String kostName;
	private int roomNumber;
	private String obOwnerKostData = "src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
	private String kostNameReject = JavaHelpers.getPropertyValue(obOwnerKostData, "wendyWildRitKostName_" + Constants.ENV);
	private String kostMarkOccupied = JavaHelpers.getPropertyValue(obOwnerKostData, "gpKostUprasDua_" + Constants.ENV);
	private String kostMarkRoomNotGp = JavaHelpers.getPropertyValue(obOwnerKostData, "kostMarkRoomNotGp_" + Constants.ENV);
	private String kostRejectFullReason = JavaHelpers.getPropertyValue(obOwnerKostData, "ownerRejectFullReason_" + Constants.ENV);


	@When("user navigates to update kamar page for kost {string} list number {string}")
	public void user_navigates_to_update_kamar_page_for_kost(String kostName, String listNumber) throws InterruptedException {
		popUp.clickOnIUnderstandOnSetAdditionalCostsPopUp();
		addkos.clickOnMenuPropertySaya();
		upgradePremium.clickOnKosMenu();
		kostList.navigateToUpdateRoomPage(kostName, listNumber);
	}

	@When("user clicks on edit kost button for kost list number {string}")
	public void user_clicks_on_edit_kost_button_for_kost_list_number_x(String listNumber) throws InterruptedException {
		updateRoom.waitTillLoadingDisappear();
		updateRoom.clickOnEditButtonRoomList(listNumber);
	}

	@When("user clicks on update kamar button")
	public void user_clicks_on_update_kamar_button() throws InterruptedException {
		updateRoom.clickUpdateBtn();
	}

	@Then("user can sees toast on update room/price as {string} {string} {string}")
	public void user_can_sees_toast_x(String Kosong, String Terisi, String room) throws InterruptedException {
		if (updateRoom.getText()) {
			Assert.assertEquals(updateRoom.getNullText(), Kosong, "Kosong");
		} else if (updateRoom.getText()){
			Assert.assertEquals(updateRoom.getFilledText(), Terisi, "Terisi");
		} else {
			Assert.assertEquals(updateRoom.getTextTotalRoom(), room, "Total room is wrong");
		}
	}

	@When("owner goes to my property kos menu")
	public void owner_goes_to_my_property_kos_menu() throws InterruptedException {
		popUp.clickOnIUnderstandOnSetAdditionalCostsPopUp();
		addkos.clickOnMenuPropertySaya();
//		upgradePremium.clickOnKosMenu();
	}

	@When("owner search {string} and click see complete button")
	public void owner_goes_to_my_property_kos_menu(String kostName) throws InterruptedException {

		switch (kostName.toLowerCase()) {
			case "owner test reject":
				this.kostName = kostNameReject;
				break;
			case "mark room occupied":
				this.kostName = kostMarkOccupied;
				break;
			case "mark room not gp":
				this.kostName = kostMarkRoomNotGp;
				break;
			case "owner reject full reason":
				this.kostName = kostRejectFullReason;
				break;
			default:
				this.kostName = kostName;
		}
		upgradePremium.clickOnKosMenu();
		kostList.dismissActivePopUp();
		kostList.openSelectedKostListDetail(this.kostName, "1");
	}

	@Then("owner verify room is zero")
	public void owner_verify_room_is_zero() throws InterruptedException{
		Assert.assertEquals(kostList.getAvailableRoomNumberText(), 0, "Kost is not zero");
	}

	@When("owner click on update room")
	public void owner_click_on_update_room() throws InterruptedException {
		kostList.clickUpdateRoomKostList("1");
	}

	@Then("owner sees all kost is in {string} status")
	public void owner_see_all_kost_is_in_filled_status(String status) throws InterruptedException {
		int i = 1;
		while (updateRoom.isRoomIsAvailable(String.valueOf(i))) {
			Assert.assertTrue(updateRoom.getRoomStatusText(String.valueOf(i)).equalsIgnoreCase(status));
			if (i == 5) {
				break;
			}
			i++;
		}
	}

	@When("owner change all filled status to available")
	public void owner_change_all_filled_status_to_available() throws InterruptedException {
		int i = 1;
		while (updateRoom.isRoomIsAvailable(String.valueOf(i))) {
			if(updateRoom.getRoomStatusText(String.valueOf(i)).equalsIgnoreCase("terisi")) {
				updateRoom.clickOnEditButtonRoomList(String.valueOf(i));
				selenium.hardWait(2);
				updateRoom.clickOnCheckBoxRenter();
				selenium.hardWait(2);
				updateRoom.clickOnUpdateRoomButton();
				selenium.hardWait(2);
				if(i == 5) {
					break;
				}
				updateRoom.waitTillUpdateRoomButtonDisappear();
			}
			i++;
		}
	}

	@When("user enter text {string} on search bar in room allotment and hit enter")
	public void user_enter_text_on_search_bar_in_room_allotment_and_hit_enter(String text) {
		updateRoom.searchNameOrRoomNo(text);
	}

	@Then("user see room list have room name that contains text {string}")
	public void user_see_room_list_have_room_name_that_contains_text(String text) {
		List<String> roomNames = updateRoom.getAllRoomNameInTable();
		for (String name : roomNames) {
			Assert.assertTrue(name.contains(text), "Room name not contain " + text);
		}
	}

	@Then("user see room list is empty in room allotment page")
	public void user_see_room_list_is_empty_in_room_allotment_page() {
		Assert.assertTrue(updateRoom.isTableEmpty(), "Table is not empty");
	}

	@When("user click edit button in first row of the table")
	public void user_click_edit_button_in_first_row_of_the_table() throws InterruptedException {
		updateRoom.clickFirstEditButton();
	}

	@When("user fill room floor in room allotment page with {string}")
	public void user_fill_room_floor_in_room_allotment_page_with(String text) {
		updateRoom.insertTextFloor(text);
	}

	@Then("user see error message {string} under floor field in update room page")
	public void user_see_error_message_under_floor_field_in_update_room_page(String error) {
		Assert.assertEquals(updateRoom.getErrorFloor().trim(), error, "Error message floor is wrong");
	}

	@Then("user see floor field is disabled")
	public void user_see_floor_field_is_disabled() {
		Assert.assertTrue(updateRoom.isFloorFieldDisabled(), "Floor field is not disabled");
	}

	@Then("user see button update room is disabled")
	public void user_see_button_update_room_is_disabled() {
		Assert.assertTrue(updateRoom.isUpdateRoomDisabled(), "Update room is not disabled");
	}

	@When("user close update room pop up in room allotment")
	public void user_close_update_room_pop_up_in_room_allotment() throws InterruptedException {
		updateRoom.closeUpdateRoomPopUp();
	}

	@When("owner click on edit button on room {string}")
	public void owner_click_on_edit_button_on_room_x(String roomStatus) throws InterruptedException {
		roomNumber = 1;
		updateRoom.clickOnUpdateRoomButtonOnFirstFoundEmptyRoom(roomNumber, roomStatus);
	}

	@When("owner tick on already occupied tickbox and click on update room")
	public void owner_tick_on_already_occupied_tickbox_and_click_on_update_room() throws InterruptedException {
		selenium.hardWait(5);
		updateRoom.clickOnCheckBoxRenter();
		updateRoom.clickOnUpdateRoomButton();
	}

	@Then("owner can sees Pop-Up owner not add renter's data")
	public void owner_can_sees_Pop_Up_owner_not_add_renter_s_data() {
		Assert.assertEquals(updateRoom.getPopupAddRenterTitle(), "Anda belum tambah data penyewa");
		Assert.assertEquals(updateRoom.getPopupAddRenterDescription(), "Sebelum menandai kamar menjadi \"sudah berpenghuni\", mohon tambahkan data penyewa terlebih dahulu.");
		Assert.assertEquals(updateRoom.getAddRenterText(), "Tambah Penyewa");
	}

	@When("owner click on Add Renter button")
	public void owner_click_on_Add_Renter_button() throws InterruptedException {
		updateRoom.clickOnAddRenterButton();
	}

	@Then("owner can sees room is on {string} status")
	public void owner_can_sees_room_is_on_occupied_status(String status) {
		Assert.assertEquals(updateRoom.getRoomStatusText("1"), status);
	}

	@When("user fill room name in room allotment page with {string}")
	public void user_fill_room_name_in_room_allotment_page_with(String roomName) {
		updateRoom.insertTextRoomName(roomName);
	}

	@Then("user see error message {string} under room name field in update room page")
	public void user_see_error_message_under_room_name_field_in_update_room_page(String error) {
		Assert.assertEquals(updateRoom.getErrorRoomName().trim(), error, "Error message room name is wrong");
	}

	@When("user click kos {string} in update room list")
	public void user_click_kos_in_update_room_list(String kostName) throws InterruptedException {
		updateRoom.clickKosName(kostName);
	}

	@Then("user see room name field is disabled in room allotment")
	public void user_see_room_name_field_is_disabled_in_room_allotment() {
		Assert.assertTrue(updateRoom.isRoomNameFieldDisabled(), "Room name field is not disabled");
	}

	@Then("user see label {string} in room name")
	public void user_see_label_in_room_name_or_number(String roomNo) {
		Assert.assertEquals(updateRoom.getGoldPlusLabel(roomNo), "Goldplus");
	}

	@Then("user see total room is {string} {string} in update room page")
	public void user_see_total_room_is_in_update_room_page(String total, String room) throws InterruptedException {
		if (updateRoom.isTotalRoom()) {
		//	Assert.assertEquals(updateRoom.getTotalRoom().trim(), total, "Total room is wrong");
		}else{
			Assert.assertEquals(updateRoom.getTextTotalRoom(), room, "Total room is wrong");
		}
	}

	@When("user tick already inhabited checkbox")
	public void user_tick_already_inhabited_checkbox() throws InterruptedException {
		updateRoom.clickAlreadyInhabitedCheckbox();
	}

	@When("user click update room button in update room pop up")
	public void user_click_update_room_button_in_update_room_pop_up() throws InterruptedException {
		updateRoom.clickUpdateRoomButton();
	}

	@When("user filter the room with {string} in update room page")
	public void user_filter_the_room_with_in_update_room_page(String filter) throws InterruptedException {
		updateRoom.filterRoomTable(filter);
	}

	@When("user click add room in room list")
	public void user_click_add_room_in_room_list() throws InterruptedException {
		updateRoom.clickAddRoomButton();
	}

	@When("user delete room name or number {string} in room allotment")
	public void user_delete_room_name_or_number_in_room_allotment(String roomNo) throws InterruptedException {
		updateRoom.deleteRoom(roomNo);
	}

	@Then("user see selected filter is {string} in room allotment page")
	public void user_see_selected_filter_is_in_room_allotment_page(String filter) {
		Assert.assertEquals(updateRoom.getRoomFilter().trim(), filter, "Room filter is wrong");
	}

	@Then("user see first floor name or number is in update room page")
	public void user_see_first_floor_name_or_number_is_in_update_room_page() throws InterruptedException {
	//	Assert.assertEquals(updateRoom.getFirstFloorNumber().trim(), "Room floor name is wrong");
		String comment = driver.findElement(By.cssSelector("tbody > tr:nth-of-type(1) > td:nth-of-type(3)")).getText();
		boolean condition = false;
		if(comment.equals("1") || comment.equals("abcd11111")) {
			condition = true;
		}
		Assert.assertTrue(condition);
	}

	@Then("user see first room name or number is in update room page")
	public void user_see_first_room_name_or_number_is_in_update_room_page() throws InterruptedException {
//		if (updateRoom.isFirstRoomName()) {
//			Assert.assertEquals(updateRoom.getFirstRoomName(), roomName1, "Room name is wrong");
//		}else{
//			Assert.assertEquals(updateRoom.getFirstRoomName(), roomName2, "Room name is wrong");
//		}
		String comment = driver.findElement(By.cssSelector("tbody > tr:nth-of-type(1) > td:nth-of-type(2)")).getText();
		boolean condition = false;
		if(comment.equals("001A") || comment.equals("1001A001A001A001A001A")) {
			condition = true;
		}
		Assert.assertTrue(condition);
	}

	@And("user tick on tickbox and click on Tambah kamar")
	public void user_tick_on_tickbox_and_click_on_tambah_kamar() throws InterruptedException {
		updateRoom.tickOnCheckBoxRenter();
		updateRoom.clickTambahKmr();
		}

	@Then("toast will be appears and the room is untick again")
	public void toast_will_be_appears_and_the_room_is_untick_again() throws InterruptedException {
		Assert.assertEquals(updateRoom.getUntickRoom(), updateRoom.getUntickRoom(), "Checkbox is untick");
	}

	@And("user click update room button in update room")
	public void user_click_update_room_button_in_update_room() throws InterruptedException {
		updateRoom.clickUpdateRoomBtn();
	}
}
