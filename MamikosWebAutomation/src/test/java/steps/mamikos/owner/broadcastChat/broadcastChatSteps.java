package steps.mamikos.owner.broadcastChat;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.broadcastChat.BroadcastChatPO;
import pageobjects.mamikos.owner.common.ChatPopUpPO;
import pageobjects.mamikos.owner.DashboardOwnerPO;
import utilities.ThreadManager;


public class broadcastChatSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ChatPopUpPO chatPopUp = new ChatPopUpPO(driver);
    private DashboardOwnerPO dashboard = new DashboardOwnerPO(driver);
    private BroadcastChatPO broadcast = new BroadcastChatPO(driver);


    @When("the user click broadcast chat entry point")
    public void the_user_click_broadcast_chat_entry_point() throws InterruptedException {
        chatPopUp.clickOnBCChatPage();
    }

    @Then("user verify pop up message {string} is appear")
    public void user_verify_pop_up_message_is_appear(String message) throws InterruptedException {
        Assert.assertEquals(dashboard.getWarningBroadcast(), message, "Warning Message Not "+ message);
    }

    @Then("user see button Lihat Detail Paket on Broadcast Chat Page is present")
    public void user_verify_button_lihat_detail_paket_is_displayed() throws InterruptedException {
        broadcast.isLihatDetailPaketDisplayed();
    }

    @Then("user see button Beli Paket on Broadcast Chat Page is present")
    public void user_verify_button_beli_paket_is_displayed() throws InterruptedException {
        broadcast.isBeliPaketDisplayed();
    }

    @Then("user see button Ajukan Ganti Paket on Broadcast Chat Page is present")
    public void user_verify_button_ajukan_ganti_paket_is_displayed() throws InterruptedException {
        broadcast.isAjukanGantiPaketBtnDisplay();
    }

    @When("user click on Button Tambah Broadcast Chat")
    public void user_click_on_Button_Tambah_Broadcast_Chat() throws InterruptedException {
        broadcast.clickontambahbroadcastchat();
    }

    @When("user click on Button Bantuan Tips")
    public void user_click_on_Button_Bantuan_Tips() throws InterruptedException {
        broadcast.clickonbantuantips();
    }

    @When("user enter text {string} on BC list kos")
    public void user_enter_text_on_BC_list_kos(String text) {
        broadcast.searchKostBC(text);
    }

    @Then("user verify {string} is disable")
    public void user_verify_string_is_disable(String menu) throws InterruptedException {
        Assert.assertTrue(broadcast.isKostCardDisabled(menu));
    }

    @Then("user see invalid search kost {string}")
    public void user_see_invalid_search_kost(String error) {
        Assert.assertEquals(broadcast.getInvalidSearchKostMessage().trim(), error, "Error message room name is wrong");
    }

    @When("user click on Button Lihat Rincian Broadcast Chat")
    public void user_click_on_Button_Lihat_Rincian() throws InterruptedException {
        broadcast.clickonLihatRincianButton();
    }

    @Then("user see Label Pesan BroadcastChat Terkirim is Present")
    public void user_see_Label_Pesan_Terkirim_is_Present() throws InterruptedException {
        broadcast.isLabelPesanTerkirimDisplayed();
    }

    @When("user click button pilih kost")
    public void user_click_button_pilih_kost() throws InterruptedException {
        broadcast.clickonPilihKosButton();
    }

    @Then("user will see alert the kost dont have a recipient")
    public void user_will_see_alert_the_kost_dont_have_a_recipient(){
        Assert.assertEquals(broadcast.getlabelKosBelumMemilikiCalonPenerimaLabel(), "Kos belum memiliki calon penerima");
    }

    @When("user click Masukan Pesan button")
    public void user_click_Masukan_Pesan_button() throws InterruptedException {
        broadcast.clickOnMasukanPesanButton();
    }

    @When("user click Pilih Kost Button")
    public void user_click_Pilih_Kost_Button() throws InterruptedException {
        broadcast.clickOnPreviewPesanBtn();
    }
    @When("user selects first message option")
    public void user_selects_first_message_option() throws InterruptedException {
        broadcast.selectFirstOptionBC();
    }

    @When("user click Pilih Pesan button")
    public void user_click_Pilih_Pesan_button() throws InterruptedException {
        broadcast.clickOnpilihPesanBtn();
    }

    @When("user input {string} on Broadcast Message")
    public void user_input_on_Broadcast_Message(String text) throws InterruptedException {
        broadcast.inputBCmessage(text);
    }

    @When("user click Preview Pesan Button")
    public void user_click_Preview_Pesan_Button() throws InterruptedException {
        broadcast.clickOnPreviewPesanBtn();
    }

    @Then("user see {string} on Preview Broadcast Message")
    public void user_see_on_Preview_Broadcast_Message(String error) {
        Assert.assertEquals(broadcast.getPreviewMessageBC().trim(), error, "Error message is wrong");
    }

    @Then("user see {string} under BC textfield")
    public void user_see_string_under_BC_textfield(String error) {
        Assert.assertEquals(broadcast.getErrorInputBC().trim(), error, "Error message is wrong");
    }

    @When("user click Ubah Button on BC Page")
    public void user_click_Ubah_Button_on_BC_Page() throws InterruptedException {
        broadcast.clickOnUbahTemplateMsg();
    }

    @When("user selects second message BC option")
    public void user_selects_second_message_BC_option() throws InterruptedException {
        broadcast.clickOnSecondOptionMessage();
    }

    @Then("user verify BC Textfield is not visible")
    public void user_verify_BC_Textfield_is_not_visible() throws InterruptedException {
        broadcast.isTextFieldBCMessageDisplayed();
    }

    @When("user click Tidak Jadi button on BC pop up")
    public void user_click_Tidak_Jadi_button_on_BC_pop_up() throws InterruptedException {
        broadcast.clickOnTidakJadiBtn();
    }

    @When("user click Keluar button on BC pop up")
    public void user_click_Keluar_button_on_BC_pop_up() throws InterruptedException {
        broadcast.clickOnKeluarButton();
    }

    @When("user click back arrow button on BC page")
    public void user_click_back_arrow_button_on_BC_page() throws InterruptedException {
        broadcast.clickOnBackButtonBC();
    }
}