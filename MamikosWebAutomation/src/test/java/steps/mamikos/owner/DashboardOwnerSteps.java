package steps.mamikos.owner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.owner.DashboardOwnerPO;
import pageobjects.mamikos.owner.common.AddPopUpPO;
import pageobjects.mamikos.owner.common.LeftMenuPO;
import pageobjects.mamikos.owner.kos.KostListPO;
import utilities.Constants;
import utilities.ThreadManager;

import static org.testng.Assert.assertTrue;

public class DashboardOwnerSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private DashboardOwnerPO dashboard = new DashboardOwnerPO(driver);
    private LeftMenuPO menu = new LeftMenuPO(driver);
    private AddPopUpPO popUp = new AddPopUpPO(driver);
    private HeaderPO header = new HeaderPO(driver);
    private KostListPO kosList = new KostListPO(driver);

    @When("user clicks Update Harga at kos")
    public void user_clicks_Update_Harga_at_kos() throws InterruptedException {
        kosList.clickUpdatePrice();
    }

    @When("user click Chat CS button")
    public void user_click_Chat_CS_button() throws InterruptedException {
        dashboard.clickOnChatCS();
    }

    @And("user click Kos button")
    public void user_click_Kos_button() throws InterruptedException {
        menu.clickOnKosButton();
        popUp.clickInstantPopUp();
    }

    @When("user clicks on Add Kos button")
    public void user_clicks_on_Add_Kos_button() throws InterruptedException {
        dashboard.clickAddKostButton();
    }

    @When("user click Kos in choose property pop up")
    public void user_click_Kos_in_choose_property_pop_up() throws InterruptedException {
        dashboard.clickKosInPopUp();
    }

    @When("user click create kos button in pop up")
    public void user_click_create_kos_button_in_pop_up() throws InterruptedException {
        dashboard.clickCreateKosInPopUp();
    }

    @When("user click on Saldo MamiAds button")
    public void user_click_on_saldo_mamiads_button() throws InterruptedException {
        dashboard.clickSaldoMamiAdsButton();
    }

    @When("user click Add Tenant Contract button in dashboard page")
    public void user_click_add_tenant_contract_button_in_dashboard_page() throws InterruptedException {
        dashboard.clickAddTenantContractButton();
    }


    @When("user click mamipoin in owner's menu")
    public void user_click_mamipoin_in_owner_s_menu() throws InterruptedException {
        dashboard.clickMamipoinButton();
    }

    @When("user clicks on Bills Menu")
    public void user_clicks_on_Bills_Menu() throws InterruptedException {
        dashboard.clickBillsMenu();
    }

    @Then("user see counter, link see all notification and latest {int} notification")
    public void user_see_counter_link_see_all_notification_and_latest_notification(int notif) {
        Assert.assertTrue(dashboard.isNotifCounterAppear(), "Notification counter is not appear/null");
        Assert.assertTrue(dashboard.isSeeAllNotifAppear(), "See All Notification button is not appear");
        Assert.assertEquals(dashboard.countNotificationList(), notif, "Notification number is wrong");
    }

    @Then("user see owner's name & phone number, text link {string} & {string}")
    public void user_see_owner_s_name_phone_number_text_link(String settings, String logout) {
        Assert.assertTrue(header.getOwnerNameInPopUp() != null &&
                !header.getOwnerNameInPopUp().equals(""), "Owner name not appear/wrong");
        Assert.assertEquals(header.getOwnerPhoneInPopUp(), Constants.OWNER_PHONE, "Owner phone number not appear/wrong");
        Assert.assertEquals(header.getSettingsLabel(), settings, "Settings label is wrong");
        Assert.assertEquals(header.getLogoutLabel(), logout, "Logout label is wrong");
    }

    @When("user click notification owner button")
    public void user_click_notification_owner_button() throws InterruptedException {
        header.clickNotificationOwner();
    }

    @When("user navigates to property kost by manage all property pop-up")
    public void user_navigates_to_property_kost_by_manage_all_property_popup() throws InterruptedException {
        dashboard.clickOnManageAllKostButton();
    }

    @Then("user click left menu Kos Management")
    public void user_click_left_menu_Kos_Management() throws InterruptedException {
        dashboard.clickKosManagement();
    }

    @And("user click lengkapi Data Diri")
    public void user_click_lengkapi_Data_Diri() throws InterruptedException{
        dashboard.lengkapiDataDiri();
    }
    @Then("user verify menu Lengkapi Data Diri Anda")
    public void user_verify_menu_Lengkapi_Data_Diri_Anda() {
        Assert.assertFalse(dashboard.lengkapiDataDiriAnda(), "Page is not showing");
    }

    @And("user click update price in Home")
    public void user_click_update_price_in_Home() throws InterruptedException {
        dashboard.clickUpdatePrice();
    }

    @When("user clicks widget set available room")
    public void user_clicks_widget_set_available_room() throws InterruptedException {
        dashboard.clickWidgetRoomAllotment();
    }

    @When("user scroll down homepage")
    public void user_scroll_down_homepage() throws InterruptedException {
       dashboard.scrollBottomPage();
    }

    @Then("user see list active kos")
    public void user_see_list_active_kos() throws InterruptedException {
        dashboard.scrollPropertyList();
    }

    @Then("user see button back")
    public void user_see_button_back() throws InterruptedException {
        assertTrue(dashboard.isButtonBackRoomShow(), "Button back present");
    }

    @Then("user see screen update room")
    public void user_see_screen_update_room() throws InterruptedException {
       assertTrue(dashboard.isRoomAllotmentPageShow(),"room page present");
    }

    @Then("user click textbox search")
    public void user_click_textbox_search() throws InterruptedException {
        dashboard.clickOnSearchRoomAllotment();
    }

    @Then("user see filter search room")
    public void user_see_filter_search_room() throws InterruptedException {
        dashboard.clickOnFilterRoomAllotment();
    }

    @Then("user see name or number room")
    public void user_see_name_or_number_room() throws InterruptedException {
       Assert.assertTrue(dashboard.isNameOrRoomAppear(), "Name/Room Number is not appear");
    }

    @Then("user see floor")
    public void user_see_floor() throws InterruptedException {
        dashboard.clickOnFloor();
    }

    @Then("user see button add room")
    public void user_see_button_add_room() throws InterruptedException {
        dashboard.clickOnAddedRoom();
    }

    @Then("user see button edit")
    public void user_see_button_edit() throws InterruptedException {
        dashboard.clickOnEditRoom();
    }

    @Then("user see button delete")
    public void user_see_button_delete() throws InterruptedException {
        dashboard.clickOnDeleteRoom();
    }

    @Then("user can see waktu mengelola section")
    public void user_can_see_waktu_mengelola_section() {
        dashboard.getWaktuMengelolaTitle();
    }

    @And("user click on Home menu")
    public void user_clicks_on_home_menu() throws InterruptedException {
        menu.clickOnHomeOwnerLeftMenu();
    }

    @And("user clicks widget set price")
    public void user_click_widget_set_price() throws InterruptedException {
        dashboard.clickOnSetPrice();
    }

    @Then("user can see set price page")
    public void user_can_see_set_price_page() throws InterruptedException {
        assertTrue(dashboard.isRoomAllotmentPageShow(),"room page present");
    }

    @And("user can see booking register widget")
    public void user_can_see_booking_register_widget() throws InterruptedException  {
        assertTrue(dashboard.isDirectBookingPresent(),"not appears booking registered widget");
    }

    @And("user cliks widget booking register")
    public void user_clicks__widget_booking_register() throws InterruptedException {
        dashboard.clickOnDirectBookingWidget();
    }

    @And("user can see manage direct booking popup")
    public void user_can_see_manage_direct_booking_popup() throws InterruptedException {
        assertTrue(dashboard.isTotalNotBookingPopupPresent(), "not appear manage direct booking popup");
        dashboard.clickOnCloseOnPopupTotalNotBooking();
    }

    @And("user click on widget Penyewa")
    public void user_clicks_on_widget_penyewa() throws InterruptedException {
        dashboard.clickOnTenantWidget();
    }

    @Then("user can see booking benefit page")
    public void user_can_see_booking_benefit_page() throws InterruptedException {
        assertTrue(dashboard.isBookingBenefitShow(),"not in booking benefit page");
    }

    @And("user click on widget help center")
    public void user_click_on_widget_hel_center() throws InterruptedException{
        dashboard.clickOnHelpCenter();
    }

    @Then("user can see help center page")
    public void user_can_see_help_center_page() throws InterruptedException{
        assertTrue(dashboard.isHelpCenterShow(), "not in help center page");
    }

    @Then("user can see blank page manage bills")
    public void user_can_see_blank_page_manage_bills() throws InterruptedException {
        assertTrue(dashboard.isManageBillBlankPageShow(),"not appears blank page");
    }

    @Then("user can see manage booking blank page")
    public void user_can_see_manage_booking_blank_page() throws InterruptedException {
        assertTrue(dashboard.isManageBookingBlankPagePresent(),"not appear manage booking blank page");
    }

    @And("user click on see more notification")
    public void user_click_on_see_more_notification() throws InterruptedException {
        header.clickOnSeeMoreNotification();
    }

    @And("user clicks on add new property ads")
    public void user_clicks_on_add_new_property_ads() throws InterruptedException {
        dashboard.clickOnAddNewPropertyAds();
    }

    @And("user clicks on add data on pop up")
    public void user_clicks_on_add_data_on_pop_up() throws InterruptedException {
        dashboard.clickOnAddDataButton();
    }

    @And("user click update room availability in Home")
    public void user_click_update_room_availability_in_Home() throws InterruptedException {
        dashboard.clickUpdateRoom();
    }

    @Then("user see username in top right shows as {string}")
    public void user_see_username_in_top_right_shows_as(String name) {
        Assert.assertEquals(header.getOwnerUsername(), name, "Username label is wrong");
    }

    @Then("user see user's name {string} in owner dashboard")
    public void user_see_user_s_name_in_owner_dashboard(String userName) {
        Assert.assertEquals(dashboard.getUserGreeting().trim(), userName, "Username greeting is wrong");
    }

    @When("user click username in owner dashboard")
    public void user_click_username_in_owner_dashboard() throws InterruptedException {
        dashboard.clickUserGreeting();
    }

    @When("user can see {string} menu in owner dashboard")
    public void user_can_see_x_menu_in_owner_dashboard(String name) throws InterruptedException {
        if(name.equalsIgnoreCase("Atur Ketersedian Kamar Mengelola data kamar kos")){
            Assert.assertEquals(dashboard.getUpdateRoom(), name, "Not appears Atur Ketersediaan Kamar");
        }
        else if(name.equalsIgnoreCase("Atur Harga Update harga sewa di iklan kos")){
            Assert.assertEquals(dashboard.getUpdatePrice(), name, "Not appears Atur Harga");
        }
        else if(name.equalsIgnoreCase("Daftar ke Booking Langsung")){
            Assert.assertEquals(dashboard.getDirectBooking(), name, "Not appears booking langsung menu");
        }
        else if(name.equalsIgnoreCase("Daftar kontrak penyewa kos")){
            Assert.assertEquals(dashboard. getPenyewa(), name, "Not appears Penyewa menu");
        }
        else if(name.equalsIgnoreCase("Tambah Penyewa")){
            Assert.assertEquals(dashboard.getTambahPenyewa(), name, "Not appears Tambah Penyewa menu");
        }
        else if(name.equalsIgnoreCase("Pusat Bantuan")){
            Assert.assertEquals(dashboard.getPusatBantuan(), name, "Not appears Pusat Bantuan menu");
        }
    }

    @And("user click on Penyewa widget at dashboard")
    public void user_click_on_penyewa_widget_at_dashboard() throws InterruptedException {
        dashboard.clickPenyewaSection();
    }

    @Then("user can see {string} page")
    public void user_can_see_tenant_list(String name) {
        Assert.assertEquals(dashboard.getPenyewaTitlePage(), name, "Not in Penyewa page ");
    }

    @Then("user can see {string} menu on dashboard")
    public void user_can_see_x_menu_on_dashboard(String menu) {
        Assert.assertEquals(dashboard.getPeraturanMenu(), menu, menu + "Not appear");
    }

    @When("user click on Ubah peraturan kos menu")
    public void user_click_on_ubah_peraturan_kos_menu() throws InterruptedException {
        dashboard.clickPeraturanMasukKos();
    }

    @Then("user can see pendapatan section")
    public void user_can_see_pendapatan_section() {
        Assert.assertEquals(dashboard.getPendapatanTitle(),"Pendapatan Total");
    }

    @And("user click on lihat laporan keuangan")
    public void user_click_on_lihat_laporan_keuangan() throws InterruptedException {
        dashboard.clickPendapatanButton();
    }

    @Then("user can see {string} and {string}")
    public void user_can_see_x_and_y(String title, String subTitle) throws InterruptedException{
        Assert.assertEquals(dashboard.getFinancialreportTitle(), title , "title as not appears");
        Assert.assertEquals(dashboard.getFinancialreportnsubTitle(), subTitle, "subtitle is not appears");
    }

    @And("user click on Laporan keuangan on manajemen kos menu")
    public void user_click_on_laporan_keuangan_menu_on_manajemen_kos_menu() throws InterruptedException {
        menu.clicOnKostManagementMenu();
        menu.clickOnfinancialReportMenu();
    }

    @And("user click disbursement information in Owner Dashboard")
    public void user_clicks_disbursement_information_in_owner_dashboard() throws InterruptedException{
        dashboard.clickDisbursementButton();
    }
}
