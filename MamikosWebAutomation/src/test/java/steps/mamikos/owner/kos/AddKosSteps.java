package steps.mamikos.owner.kos;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.mamiads.UpgradePremiumPO;
import pageobjects.mamikos.owner.common.AddPopUpPO;
import pageobjects.mamikos.owner.kos.AddKosPO;
import pageobjects.mamikos.owner.kos.KostListPO;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import pageobjects.mamikosAdmin.KostOwnerPO;
import pageobjects.mamikosAdmin.LeftMenuPO;
import pageobjects.mamikosAdmin.LoginPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.List;

public class AddKosSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AddKosPO addkos = new AddKosPO(driver);
    private AddPopUpPO popUp = new AddPopUpPO(driver);
    private JavaHelpers java = new JavaHelpers();
    private KostListPO kosList = new KostListPO(driver);
    private String randomTextKosName = "";
    private String randomTextKosType = "";
    private String imageSrc;
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private LoginPO login = new LoginPO(driver);
    private LeftMenuPO leftMenu = new pageobjects.mamikosAdmin.LeftMenuPO(driver);
    private KostOwnerPO kostOwner = new KostOwnerPO(driver);
    private HeaderPO header = new HeaderPO(driver);
    private KosDetailPO kosDetailPO = new KosDetailPO(driver);
    private pageobjects.mamikos.account.LoginPO loginpo = new pageobjects.mamikos.account.LoginPO(driver);
    private UpgradePremiumPO upgradePremium = new UpgradePremiumPO(driver);

    @Given("user selects Kost option and click on Add Data button")
    public void user_selects_Kost_option_and_click_on_Add_Data_button() throws InterruptedException {
        addkos.clickOnKosRadioButton();
        addkos.clickOnAddDataButton();
    }

    @Given("user input kost location {string} and clicks on first autocomplete result {string} {string}")
    public void user_input_kost_location_and_clicks_on_first_autocomplete_result(String text, String kabupaten, String kecamatan) throws InterruptedException {
        addkos.insertKosLocation(text);
        addkos.clickOnFirstResult(kabupaten, kecamatan);
    }

    @Given("user input kost location {string}")
    public void user_input_kost_location(String text) throws InterruptedException {
        addkos.insertKosLocation(text);
        addkos.checkResult();
    }

    @When("user input address note {string}")
    public void user_input_address_note(String note) {
        addkos.enterAddressNotes(note);
    }

    @Given("user input address note {string} and random text")
    public void user_input_address_note_and_random_text(String note) {
        String random = java.generateAlphanumeric(6);
        addkos.enterAddressNotes(note + random);
    }

    @When("user fills kost name field with {string}")
    public void user_fills_kost_name_field_with(String text) throws InterruptedException {
        addkos.insertKosName(text);
    }

    @Then("user see validation message for existing kost name {string} in kost name field")
    public void user_see_validation_message_for_existing_kost_name_in_kost_name_field(String invalidKostName2) {
        Assert.assertEquals(addkos.getErrorMessageExistingKostName(), invalidKostName2, "error message wrong");
    }

    @Then("button Next is disabled")
    public void button_Next_is_disabled() {
        Assert.assertTrue(addkos.checkDisabledNextButtonLocationData(), "button next is enabled");
    }

    @When("user clicks on kost type icon {string}")
    public void user_clicks_on_kost_type_icon(String text) throws InterruptedException {
        addkos.clickOnIconGender(text);
    }

    @When("user clicks on kost size {string}")
    public void user_clicks_on_kost_size(String size) throws InterruptedException {
        addkos.clickOnIconRoomSize(size);
    }

    @And("user click owner dashboard from homepage")
    public void user_click_owner_dashboard_from_homepage() throws InterruptedException {
        addkos.ownerDashboardMenu();
    }

    @Given("user click left menu Property Saya")
    public void user_click_left_menu_Property_Saya() throws InterruptedException {
        addkos.clickOnMenuPropertySaya();
    }
    @Then("user verify on button Management kos")
    public void user_verify_on_button_Management_ko() {
        Assert.assertTrue(addkos.managementKosMenu(), "Management Kos is not showing");
    }
    @Given("user click menu Kost")
    public void user_click_menu_Kost() throws InterruptedException {
        addkos.clickOnMenuKost();
    }

    @Given("user click button Edit Data Kos")
    public void user_click_button_Edit_Data_Kos() throws InterruptedException {
        addkos.buttonEditKost();
        if (addkos.disableButtonEditKost()){
            selenium.navigateToPage(Constants.ADMIN_URL);
            login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_LOGIN_EMAIL, Constants.BACKOFFICE_LOGIN_PASSWORD);
            leftMenu.clickOnKostOwnerMenu();
            kostOwner.searchKosName("Kose Putri Automation");
            kostOwner.clickOnFirstVerifyButton();
            selenium.navigateToPage(Constants.MAMIKOS_URL);
            header.logoutAsAOwner();
            header.clickOnEnterButton();
            loginpo.popUpOwnerLogin();
            loginpo.loginAsOwnerToApplication(Constants.OWNER_PHONE,Constants.OWNER_PASSWORD);
            kosDetailPO.clickOnCloseButton();
            addkos.clickOnMenuPropertySaya();
            addkos.clickOnMenuKost();
            kosList.clickOnSearchKosTextBox();
            kosList.inputKosName("Kose Putri Automation");
            kosList.clickKosName();
            upgradePremium.clickOnLihatSelengkapnyaButton();
            addkos.buttonEditKost();
        }
    }

    @Then("user see validation message {string}")
    public void user_see_validation_message(String editAddress) {
        Assert.assertTrue(addkos.EditCompleteKostAddress().contains(editAddress), "error message wrong");
    }

    @When("user click back on pop up attention")
    public void user_click_back_on_pop_up_attention() throws InterruptedException {
        popUp.clickBackAttentionPopUp();
    }

    @When("user click continue input data on pop up")
    public void user_click_continue_input_data_on_pop_up() throws InterruptedException {
        popUp.clickContinueInputDataPopUp();
    }

    @And("user edit other popup data")
    public void edit_other_data_on_pop_up() throws InterruptedException {
        popUp.editOtherDataPopUp();
    }

    @When("user click button Move Page in pop up")
    public void user_click_button_Move_Page_in_pop_up() throws InterruptedException {
        popUp.clickMovePageInPopUp();
        popUp.waitLoadingFinish();
    }

    @Then("user see pop up confirmation request attention")
    public void user_see_pop_up_confirmation_request_attention() {
        popUp.isAttentionPopUpAppear();
    }

    @When("user click Edit in Facilities")
    public void user_click_Edit_in_Facilities() throws InterruptedException {
        addkos.clickEditFacilities();
    }

    @When("user uncheck/check facilities under {string}")
    public void user_check_facilities_under(String section, List<String> facilities) throws InterruptedException {
        for (String facility : facilities) {
            addkos.clickFacilitiesCheckbox(section, facility);
        }
    }

    @Then("user see edit finished button is disabled")
    public void user_see_edit_finished_button_is_disabled() {
        Assert.assertEquals(addkos.isEditFinishedButtonDisabled(), "Edit Selesai");
    }

    @And("user see {string} has warning title {string} and description {string}")
    public void user_see_has_warning_title_and_description(String facility, String title, String desc) {
        Assert.assertEquals(addkos.getWarningTitleFacility(facility), title, "Warning title in " + facility + " is wrong");
        Assert.assertEquals(addkos.getWarningDescFacility(facility), desc, "Warning description in " + facility + " is wrong");
    }

    @When("user clicks checkbox room type")
    public void user_clicks_checkbox_room_type() throws InterruptedException {
        addkos.clickOnChecklistRoomType();
    }

    @Then("user see next button disable")
    public void user_see_next_button_disable() {
        Assert.assertTrue(addkos.checkDisabledNextButtonCreateKost(), "button next is enabled");
    }

    @When("user input kos name with {string}")
    public void user_input_kos_name_with(String KosNameDataKost) {
        addkos.insertKosNameDataKost(KosNameDataKost);
    }

    @When("user input room type with {string}")
    public void user_input_room_type_with(String roomType) {
        addkos.insertRoomTypeDataKost(roomType);
    }

    @When("user input room type with {string} in pop up")
    public void user_input_room_type_with_in_pop_up(String roomType) {
        addkos.insertRoomTypePopUp(roomType);
    }

    @When("user clicks checkbox administrator kos")
    public void user_clicks_checkbox_administrator_kos() throws InterruptedException {
        addkos.clickOnChecklistAdminKost();
        if (addkos.unhecklistAdminKost()) {
            addkos.clickOnChecklistAdminKost();
        }
    }

    @When("user input administrator name with {string}")
    public void user_input_administrator_name_with(String adminKos) {
        addkos.insertNameAdminKos(adminKos);
    }

    @And("user input administrator phone with {string}")
    public void user_input_administrator_phone_with(String adminPhone) {
        addkos.insertAdminPhone(adminPhone);
    }

    @Given("user click add new kos button")
    public void user_click_add_new_kos_button() throws InterruptedException {
        addkos.clickAddNewKos();
    }

    @Then("user fill kos description with {string}")
    public void user_fill_kos_description_with(String desc) throws InterruptedException {
        addkos.insertKosDescription(desc);
    }

    @Then("user select kos year built {string}")
    public void user_select_kos_year_built(String year) throws InterruptedException {
        addkos.selectKosYearBuilt(year);
    }

    @Then("user clicks on next button in bottom of add kos page")
    public void user_clicks_on_next_button_in_bottom_of_add_kos_page() throws InterruptedException {
        popUp.waitLoadingFinish();
        addkos.clickNext();
    }

    @And("user clicks on lanjutkan button in bottom of add kos page")
    public void user_clicks_on_lanjutkan_button_in_bottom_of_add_kos_page() throws InterruptedException{
        popUp.waitLoadingFinish();
        addkos.clickLanjutkan();
    }

    @And("user click edit selesai button")
    public void user_click_edit_selesai_button() throws InterruptedException {
        for (int i = 0; i < 2; i++) {
            addkos.clickEditSelesai();
            popUp.waitLoadingFinish();
        }
    }
    @And("user clicks next when photo submission pop up appear")
    public void user_clicks_next_when_photo_submission_pop_up_appear() throws InterruptedException {
        int i=0;
        while (popUp.isPhotoSubmissionNextButtonAppear() && i < 10) {
            popUp.clickNextPhotoSubmitPopUp();
            i++;
        }
    }

    @Then("user insert photo image to {string}")
    public void user_insert_photo_image_to(String section) throws InterruptedException {
        addkos.uploadPhoto(section,"valid");
    }

    @Then("user insert invalid photo image to {string}")
    public void user_insert_invalid_photo_kos_to(String section) throws InterruptedException {
        addkos.uploadPhoto(section,"invalid");
    }

    @Then("user click checkbox rent price other than monthly")
    public void user_click_checkbox_rent_price_other_than_monthly() {
        addkos.clickOtherThanMonthlyPrice();
    }

    @When("user input daily price with {string} in add kos page")
    public void user_input_daily_price_with_in_add_kos_page(String price) throws InterruptedException {
        addkos.insertDailyPrice(price);
    }

    @When("user input monthly price with {string} in add kos page")
    public void user_input_monthly_price_with_in_add_kos_page(String price) throws InterruptedException {
        addkos.insertMonthlyPrice(price);
    }

    @When("user input weekly price with {string} in add kos page")
    public void user_input_weekly_price_with_in_add_kos_page(String price) throws InterruptedException {
        addkos.insertWeeklyPrice(price);
    }

    @When("user input three monthly price with {string} in add kos page")
    public void user_input_three_monthly_price_with_in_add_kos_page(String price) throws InterruptedException {
        addkos.insert3MonthlyPrice(price);
    }

    @When("user input six monthly price with {string} in add kos page")
    public void user_input_six_monthly_price_with_in_add_kos_page(String price) throws InterruptedException {
        addkos.insert6MonthlyPrice(price);
    }

    @When("user input yearly price with {string} in add kos page")
    public void user_input_yearly_price_with_in_add_kos_page(String price) throws InterruptedException {
        addkos.insertYearlyPrice(price);
    }

    @When("user clicks checkbox additional price")
    public void user_clicks_checkbox_additional_price() {
        addkos.clickAdditionalCostCheckbox();
    }

    @When("user input deposit with {string}")
    public void user_input_deposit_with(String deposit) throws InterruptedException {
        addkos.clickDepositCheckbox();
        addkos.insertDeposit(deposit);
    }

    @When("user input fine with {string}")
    public void user_input_fine_with(String fine) throws InterruptedException {
        addkos.clickFineCheckbox();
        addkos.insertFine(fine);
    }

    @Then("user see warning message in {string} is {string}")
    public void user_see_warning_message_in_is(String section, String errorMessage) {
        Assert.assertEquals(addkos.getErrorMessagePrice(section), errorMessage, "Error message in " + section + " is wrong");
    }

    @When("user fills kost name field with {string} and random text")
    public void user_fills_kost_name_field_with_and_random_text(String kosName) throws InterruptedException {
        this.randomTextKosName = java.generateAlphanumeric(5);
        addkos.insertKosName(kosName + randomTextKosName);
    }

    @Then("user delete first kos on the list")
    public void user_delete_first_kos_on_the_list() throws InterruptedException {
        addkos.deleteFirstKos();
    }

    @Given("user click add another type from kos {string}")
    public void user_click_add_another_type_from_kos(String name) throws InterruptedException {
        addkos.clickKosName(name);
    }

    @Given("user click {string} in add new room type pop up and click next")
    public void user_click_in_add_new_room_type_pop_up_and_click_next(String from) throws InterruptedException {
        addkos.clickCopyKosType(from);
        addkos.clickNextInPopUp();
    }

    @Given("user input room type with random text")
    public void user_input_room_type_with_random_text() {
        this.randomTextKosType = java.generateAlphanumeric(5);
        addkos.insertRoomTypeDataKost(randomTextKosType);
    }

    @Given("user input room type with {string} and random text")
    public void user_input_room_type_with_and_random_text(String roomType) {
        this.randomTextKosType = java.generateAlphanumeric(4);
        addkos.insertRoomTypeDataKost(roomType + " " + randomTextKosType);
    }

    @When("user input room type with random text in add kos data pop up")
    public void user_input_room_type_with_random_text_in_add_kos_data_pop_up() {
        this.randomTextKosType = java.generateAlphanumeric(5);
        addkos.enterAdditionalCostName(randomTextKosType);
    }

    @And("user input {string} to total available room")
    public void user_input_to_total_available_room(String total) {
        addkos.enterTotalAvailableRoom(total);
    }

    @Given("user clicks checkbox minimum rent duration")
    public void user_clicks_checkbox_minimum_rent_duration() {
        addkos.clickMinDurationRentCheckbox();
    }

    @Given("user select minimum rent duration {string}")
    public void user_select_minimum_rent_duration(String duratiom) throws InterruptedException {
        addkos.selectMinDurationRent(duratiom);
    }

    @Given("user insert cost name {string}")
    public void user_insert_cost_name(String costName) {
        addkos.enterAdditionalCostName(costName);
    }

    @Given("user insert total cost {string}")
    public void user_insert_total_cost(String total) throws InterruptedException {
        addkos.enterTotalAdditionalCost(total);
    }

    @Given("user clicks checkbox down payment")
    public void user_clicks_checkbox_down_payment() {
        addkos.clickDownPaymentCheckbox();
    }

    @Given("user select down payment percentage {string}")
    public void user_select_down_payment_percentage(String percent) throws InterruptedException {
        addkos.selectDownPayment(percent);
    }

    @Given("user select payment expired date after {string} {string}")
    public void user_select_payment_expired_date_after(String number, String time) throws InterruptedException {
        addkos.selectTimeLimit(number, time);
    }

    @And("user input amount denda {string}")
    public void user_input_amount_denda(String denda) throws InterruptedException {
        addkos.inputAmountDenda(denda);
    }

    @Given("user click done in success page")
    public void user_click_done_in_success_page() throws InterruptedException {
        addkos.clickDoneSuccessButton();
    }

    @And("user click skip in ask mamikos page")
    public void user_click_skip_in_ask_mamikos_page() throws InterruptedException {
        addkos.clickSkipAskMamikosButton();
    }

    @Then("user see kos with name {string} and random text, status {string} and type {string}")
    public void user_see_kos_with_name_and_random_text_status_and_type(String name, String status, String type) throws InterruptedException {
        String expected = name + this.randomTextKosName + this.randomTextKosType;
        Assert.assertTrue(kosList.getFirstKosName().trim().replaceAll("\\s", "")
                .contains(expected.replaceAll("\\s", "")), "Kos name is wrong");
        Assert.assertTrue(kosList.getFirstKosStatus().trim().contains(status), "Kos status is wrong");
        Assert.assertEquals(kosList.getFirstKosType().trim(), type, "Kos type is wrong");
    }

    @Then("user search kos with name {string} and random text, status {string} and type {string}")
    public void user_search_kos_with_name_and_random_text_status_and_type(String name, String status, String type) throws InterruptedException {
        String expected = name + this.randomTextKosName + this.randomTextKosType;
        Assert.assertFalse(kosList.getFirstKosName().trim().replaceAll("\\s", "")
                .contains(expected.replaceAll("\\s", "")), "Kos name is wrong");
        Assert.assertTrue(kosList.getFirstKosStatus().trim().contains(status), "Kos status is wrong");
        Assert.assertEquals(kosList.getFirstKosType().trim(), type, "Kos type is wrong");
    }

    @Then("user doesn't see kos with name {string} and random text, status {string} and type {string} in first list")
    public void user_doesn_t_see_kos_with_name_and_random_text_status_and_type_in_first_list(String name, String status, String type) throws InterruptedException {
        String expected = name + this.randomTextKosName + this.randomTextKosType;
        Assert.assertTrue(kosList.getFirstKosName().trim().replaceAll("\\s", "")
                .contains(expected.replaceAll("\\s", "")), "Kos name is still appear");
        Assert.assertTrue(kosList.getFirstKosStatus().trim().contains(status), "Kos status is still same");
        Assert.assertNotEquals(kosList.getFirstKosType().trim(), type, "Kos type is same");
    }

    @When("user set kos rules :")
    public void user_set_kos_rules(List<String> rules) throws InterruptedException {
        addkos.clickSetRuleButton();
        for (String rule : rules) {
            addkos.clickKosRulesCheckbox(rule);
        }
    }

    @When("user upload kos rules photo")
    public void user_upload_kos_rules_photo() {
        addkos.uploadKosRulesPhoto();
    }

    @And("user click upload kos rules photo")
    public void user_click_upload_kos_rules_photo() throws InterruptedException {
        addkos.clickUploadKosRulesPhoto();
    }

    @When("user upload invalid kos rules photo")
    public void user_upload_invalid_kos_rules_photo() {
        addkos.uploadInvalidKosRulesPhoto();
    }

    @Then("user see warning message invalid kos photo")
    public void user_see_warning_message_invalid_kos_photo() {
        for (int i = 0; i < addkos.getUploadInvalidKosPhotoText().size(); i++) {
            Assert.assertEquals(addkos.getUploadInvalidKosPhotoText().get(i),"Upload Gagal Format foto tidak didukung");
        }
    }

    @When("user input other notes {string}")
    public void user_input_other_notes(String notes) {
        addkos.enterOtherNotes(notes);
    }

    @When("user clicks on edit done button in bottom of add kos page")
    public void user_clicks_on_edit_done_button_in_bottom_of_add_kos_page() throws InterruptedException {
        addkos.clickEditDoneButton();
    }

    @Then("user see text {string} below complete kos data")
    public void user_see_text_below_complete_kos_data(String text) {
        Assert.assertEquals(kosList.getFirstKosRejectMsg().trim(), text, "Kos rejected message is wrong");
    }

    @Then("user see error message {string} under room type field")
    public void user_see_error_message_under_room_type_field(String message) {
        Assert.assertEquals(addkos.getErrorMessageRoomType().trim(), message, "Room type error message is wrong");
    }

    @Given("user click Edit in Data Kos")
    public void user_click_Edit_in_Data_Kos() throws InterruptedException {
        addkos.clickEditKosData();
    }

    @Then("user see success add data kos pop up with text {string}")
    public void user_see_success_add_data_kos_pop_up_with_text(String message) throws InterruptedException {
        Assert.assertEquals(addkos.getTitlePopUpSuccessEditKos().trim(), message, "Pop up title success message in edit kos is wrong");
    }

    @When("user click Edit in Kos Address")
    public void user_click_Edit_in_Kos_Address() throws InterruptedException {
        addkos.clickEditKosAddress();
    }

    @When("user click Edit in Kos Photos")
    public void user_click_Edit_in_Kos_Photos() throws InterruptedException {
        addkos.clickEditKosPhotos();
    }

    @When("user click Edit in Room Photos")
    public void user_click_Edit_in_Room_Photos() throws InterruptedException {
        addkos.clickEditRoomPhotos();
    }

    @When("user click Edit in Room Availability")
    public void user_click_Edit_in_Room_Availability() throws InterruptedException {
        addkos.clickEditRoomAvailability();
    }

    @When("user click done in success page pop up of edit kos")
    public void user_click_done_in_success_page_pop_up_of_edit_kos() throws InterruptedException {
        popUp.clickDoneEditKosPopUp();
    }

    @Then("user see kos address is in {string}")
    public void user_see_kos_address_is_in(String address) throws InterruptedException {
        Assert.assertTrue(kosList.getFirstKosNameLocation().trim().replaceAll("\\s", "")
                .contains(address.replaceAll("\\s", "")), "Kos address in title is wrong");
    }

    @Then("user see location kos address is in {string}")
    public void user_see_location_kos_address_is_in(String address) throws InterruptedException {
        Assert.assertFalse(kosList.getFirstKosName().trim().replaceAll("\\s", "")
                .contains(address.replaceAll("\\s", "")), "Kos address in title is wrong");
    }

    @Given("user see kos name field is disabled")
    public void user_see_kos_name_field_is_disabled() {
      //  Assert.assertTrue(addkos.isKosNameFieldEnable(), "Kos name field is still enable");
        if (addkos.isKosNameFieldEnable()) {
            Assert.assertTrue(addkos.isKosNameFieldEnable(), "Kos name field is still enable");
        }else {
            Assert.assertFalse(addkos.isKosNameFieldEnable(), "Kos name field is still enable");
        }
    }

    @Then("user see kos room type checkbox is disabled")
    public void user_see_kos_room_type_checkbox_is_disabled() {
        Assert.assertTrue(addkos.isRoomTypeCheckboxEnable(), "Kos room type checkbox is still enable");
    }

    @When("user click manage room availability button")
    public void user_click_manage_room_availability_button() throws InterruptedException {
        // Button element same with next button in pop up
        addkos.clickNextInPopUp();
    }

    @Then("user fill room name or number in room allotment page with {string}")
    public void user_fill_room_name_or_number_in_room_allotment_page_with(String name) throws InterruptedException {
        addkos.enterRoomName(name);
    }

    @Then("user fill room floor with {string}")
    public void user_fill_room_floor_with(String floor) {
        addkos.enterFloorNumber(floor);
    }

    @Then("user see error message {string} under floor field")
    public void user_see_error_message_under_floor_field(String errorMsg) {
        Assert.assertEquals(addkos.getErrorFloorField().trim(), errorMsg, "Floor error message is wrong or not appear");
    }

    @Then("user see error message {string} under kos name field")
    public void user_see_error_message_under_kos_name_field(String errorMsg) {
        Assert.assertEquals(addkos.getErrorKosNameField().trim(), errorMsg, "Kos name error message is wrong or not appear");
    }

    @Then("user see error message {string} under data kos room type field")
    public void user_see_error_message_under_data_kos_room_type_field(String errorMsg) {
        Assert.assertEquals(addkos.getErrorKosTypeField().trim(), errorMsg, "Kos room type error message is wrong or not appear");
    }

    @Then("user see error message {string} under kos description field")
    public void user_see_error_message_under_kos_description_field(String errorMsg) {
        Assert.assertEquals(addkos.getErrorKosDescField().trim(), errorMsg, "Kos description error message is wrong or not appear");
    }

    @Then("user see error message {string} under kos manager name field")
    public void user_see_error_message_under_kos_manager_name_field(String errorMsg) {
        Assert.assertEquals(addkos.getErrorKosManagerNameField().trim(), errorMsg, "Kos manager name error message is wrong or not appear");
    }

    @Then("user see error message {string} under kos manager phone field")
    public void user_see_error_message_under_kos_manager_phone_field(String errorMsg) {
        Assert.assertEquals(addkos.getErrorKosManagerPhoneField().trim(), errorMsg, "Kos manager phone error message is wrong or not appear");
    }

    @Then("user see button see photo, button change photo, and button delete photo")
    public void user_see_button_see_photo_button_change_photo_and_button_delete_photo() {
        Assert.assertTrue(addkos.isSeeKosRulePhotoAppear(), "See kos rule photo button is not appear");
        Assert.assertTrue(addkos.isChangeKosRulePhotoAppear(), "Change kos rule photo button is not appear");
        Assert.assertTrue(addkos.isDeleteKosRulePhotoAppear(), "Delete kos rule photo button is not appear");
    }

    @Then("user upload different kos rule photo and see kos rule photo is changed")
    public void user_upload_different_kos_rule_photo_and_see_kos_rule_photo_is_changed() throws InterruptedException {
        addkos.uploadOtherKosRulesPhoto();
        Assert.assertNotEquals(addkos.getKosRulePhotoImageSrc(), imageSrc, "Kos rule photo is not changed");
    }

    @Then("user clicks delete photo and see photo deleted in kos rule")
    public void user_clicks_delete_photo_and_see_photo_deleted_in_kos_rule() throws InterruptedException {
       // this.imageSrc = addkos.getKosRulePhotoImageSrc();
        addkos.clickDeleteKosRulePhoto();
        Assert.assertTrue(addkos.isKosRuleAddPhotoAppear(), "Add photo button in kos rule is not appear");
    }

    @Then("user clicks delete photo in kos rule")
    public void user_clicks_delete_photo_in_kos_rule() throws InterruptedException {
        addkos.clickDeleteKosRulePhoto();
        Assert.assertFalse(addkos.isKosRuleAddPhotoAppear(), "Add photo button in kos rule is not appear");
    }

    @Then("user delete last kos rule photo")
    public void user_delete_last_kos_rule_photo() {
        addkos.clickDeleteKosRulePhoto();
    }

    @When("user click checkbox already inhabited")
    public void user_click_checkbox_already_inhabited() throws InterruptedException {
        addkos.clickAlreadyInhabitedCheckbox();
    }

    @When("user see first room name or number is {string}")
    public void user_see_first_room_name_or_number_is(String room) {
        Assert.assertTrue(addkos.getFirstRoomName(room), "Kos room name / number is wrong");
    }

    @When("user see first floor name or number is {string}")
    public void user_see_first_floor_name_or_number_is(String floor) {
        Assert.assertTrue(addkos.getFirstFloorNumber(floor),  "Room floor name / number is wrong");
    }

    @When("user see first room already inhabited checkbox is {string}")
    public void user_see_first_room_already_inhabited_checkbox_is(String status) {
        if (status.equals("checked")) {
            Assert.assertTrue(addkos.isAlreadyInhabitedChecked(), "Checkbox room inhabited is not checked");
        }
        else {
            Assert.assertFalse(addkos.isAlreadyInhabitedChecked(), "Checkbox room inhabited is checked");
        }
    }

    @When("user click done in room availability")
    public void user_click_done_in_room_availability() throws InterruptedException {
        addkos.clickDoneRoomAvail();
    }

    @Then("user see add new kos button")
    public void user_see_add_new_kos_button() {
        Assert.assertTrue(addkos.isAddNewKosBtnAppear(), "Add new kos button is not appear");
    }

    @Then("user see error message {string} under address note field")
    public void user_see_error_message_under_address_note_field(String error) {
        Assert.assertEquals(addkos.getErrorMessageRoomType(), error, "Error message in address notes field is wrong or not appear");
    }

    @Then("user see error message {string} under kabupaten field")
    public void user_see_error_message_under_kabupaten_field(String error) {
        Assert.assertEquals(addkos.getErrorCityField(), error, "Error message in kabupaten/city field is wrong or not appear");
    }

    @Then("user see error message {string} under kecamatan field")
    public void user_see_error_message_under_kecamatan_field(String error) {
        Assert.assertEquals(addkos.getErrorSubdistrictField(), error, "Error message in kecamatan field is wrong or not appear");
    }

    @Then("user see error message {string}, {string} under {string}")
    public void user_see_error_message_under(String title, String message, String section) {
        Assert.assertEquals(addkos.getErrorFacilityTitle(section), title, "Error message title in facility " + section + " is wrong or not appear");
        Assert.assertEquals(addkos.getErrorFacilityDesc(section), message, "Error message description in facility " + section + " is wrong or not appear");
    }

    @Then("user see warning under room size {string}")
    public void user_see_warning_under_room_size(String message) {
        Assert.assertEquals(addkos.getErrorRoomSize(), message, "Error message room size is wrong or not appear");
    }

    @When("user input other room size with {string} and {string}")
    public void user_input_other_room_size_with_and(String length, String width) {
        addkos.enterRoomSizeLength(length);
        addkos.enterRoomSizeWidth(width);
    }

    @Given("user click button Edit Private Data")
    public void user_click_button_Edit_Private_Data() throws InterruptedException {
        addkos.clickOnEditPersonalDataButton();
    }

    @When("user click icon close on page pilih jenis properti")
    public void user_click_icon_close_on_page_pilih_jenis_properti() throws InterruptedException{
        addkos.clickOnIconClose();
    }

    @Then("user see page title is {string} on choose property type page")
    public void user_see_page_title_is_on_choose_property_type_page(String title) {
        Assert.assertEquals(addkos.getPageChoosePropertyTypeTitle(), title, "title is not match");
    }

    @When("user click complete kos data in first kos list")
    public void user_click_complete_kos_data_in_first_kos_list() throws InterruptedException {
        kosList.clickCompleteKosData();
    }

    @Then("user see kos description is disabled")
    public void user_see_kos_description_is_disabled() {
        Assert.assertFalse(addkos.isKosDescEnable(), "Kos description is still enable");
    }

    @Then("user see kos year built is disabled")
    public void user_see_kos_year_built_is_disabled() {
        Assert.assertFalse(addkos.isKosYearBuildEnable(), "Kos year built is still enable");
    }

    @Then("user see error message {string} under total available room")
    public void user_see_error_message_under_total_available_room(String error) {
        Assert.assertEquals(addkos.getErrorMessageTotalAvailableRoom().trim(), error, "Error message total available room is wrong/not appear");
    }

    @Then("user see error message {string} under total room")
    public void user_see_error_message_under_total_room(String error) {
        Assert.assertEquals(addkos.getErrorMessageTotalRoom().trim(), error, "Error message total room is wrong/not appear");
    }

    @When("user delete all kost photos")
    public void user_delete_all_kost_photos() throws InterruptedException {
        addkos.clickHapusFoto();
    }

    @Then("user see warning message user need to complete the photo")
    public void user_see_warning_message_user_need_to_complete_the_photo() {
        for (int i = 0; i < addkos.getNeedtoCompletePhotoText().size(); i++) {
            Assert.assertEquals(addkos.getNeedtoCompletePhotoText().get(i),"Anda harus melengkapi foto ini");
        }
    }
}
