package steps.mamikos.owner.kos;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.kos.AddKosPO;
import pageobjects.mamikos.owner.kos.KostListPO;
import pageobjects.mamikos.owner.kos.UpdateKosPO;
import pageobjects.mamikos.owner.kos.UpdateRoomPO;
import utilities.ThreadManager;

public class UpdatePriceSteps {

    private final WebDriver driver = ThreadManager.getDriver();
    private final UpdateKosPO updateKos = new UpdateKosPO(driver);
    private final AddKosPO addKos = new AddKosPO(driver);
    private final KostListPO kosList = new KostListPO(driver);
    private final UpdateRoomPO updateRoom = new UpdateRoomPO(driver);
    private String dailyPrice = null;
    private String weeklyPrice = null;
    private String monthlyPrice = null;
    private String threeMonthlyPrice = null;
    private String sixMonthlyPrice = null;
    private String yearlyPrice = null;


    @When("user click see other prices")
    public void user_click_see_other_prices() throws InterruptedException {
        updateKos.clickSeeOtherPrices();
    }

    @When("user input daily price with {string}")
    public void user_input_daily_price_with(String dailyPrice) throws InterruptedException {
        updateKos.inputDailyPriceKos(dailyPrice);
    }

    @When("user input weekly price with {string}")
    public void user_input_weekly_price_with(String weeklyPrice) throws InterruptedException {
        updateKos.inputWeeklyPrice(weeklyPrice);
    }

    @When("user input monthly price with {string}")
    public void user_input_monthly_price_with(String monthlyPrice) throws InterruptedException {
        updateKos.inputMonthlyPrice(monthlyPrice);
    }

    @When("user input three monthly price with {string}")
    public void user_input_three_monthly_price_with(String threeMonthlyPrice) throws InterruptedException {
        updateKos.inputThreeMonthlyPrice(threeMonthlyPrice);
    }

    @When("user input six monthly price with {string}")
    public void user_input_six_monthly_price_with(String sixMonthlyPrice) throws InterruptedException {
        updateKos.inputSixMonthlyPrice(sixMonthlyPrice);
    }

    @When("user input yearly price with {string}")
    public void user_input_yearly_price_with(String yearlyPrice) throws InterruptedException {
        updateKos.inputYearlyPrice(yearlyPrice);
    }

    @When("user clicks update and user can sees toast on update price as {string}")
    public void user_clicks_update_and_verify_toast(String toastText) throws InterruptedException {
        updateKos.clickButtonUpdate();
    }

    @And("user see daily price is {string}")
    public void user_see_daily_price_is(String dailyPrice) {
        Assert.assertEquals(updateKos.getDailyPrice(), dailyPrice, "Daily price is not correct");
    }

    @And("user see weekly price is {string}")
    public void user_see_weekly_price_is(String weeklyPrice) {
        Assert.assertEquals(updateKos.getWeeklyPrice(), weeklyPrice, "Weekly price is not correct");
    }

    @And("user see monthly price is {string}")
    public void user_see_monthly_price_is(String monthlyPrice) {
        Assert.assertEquals(updateKos.getMonthlyPrice(), monthlyPrice, "Monthly price is not correct");
    }

    @And("user see three monthly price is {string}")
    public void user_see_three_monthly_price_is(String threeMonthPrice) {
        Assert.assertEquals(updateKos.getThreeMonthlyPrice(), threeMonthPrice, "Three monthly price is not correct");
    }

    @Then("user see six monthly price is {string}")
    public void user_see_six_monthly_price_is(String sixMonthlyPrice) {
        Assert.assertEquals(updateKos.getSixMonthlyPrice(), sixMonthlyPrice, "Six monthly price is not correct");
    }

    @And("user see yearly price is {string}")
    public void user_see_yearly_price_is(String yearlyPrice) {
        Assert.assertEquals(updateKos.getYearlyPrice(), yearlyPrice, "Yearly price is not correct");
    }

    @When("user clicks button set the price")
    public void user_clicks_button_set_the_price() throws InterruptedException {
        updateKos.clickSetPriceButton();
    }

    @Then("user see warning daily price with {string}")
    public void user_see_warning_daily_price_with(String warningDailyPrice) throws InterruptedException {
        Assert.assertEquals(updateKos.getWarningDailyPrice(), warningDailyPrice, "Warning daily price is not correct");
    }

    @Then("user not see warning in daily price")
    public void user_not_see_warning_in_daily_price() {
        Assert.assertFalse(updateKos.isWarningDailyPriceAppear(), "Warning daily price is appear");
    }

    @And("user see warning weekly price with {string}")
    public void user_see_warning_weekly_price_with(String warningWeeklyPrice) throws InterruptedException {
        Assert.assertEquals(updateKos.getWarningWeeklyPrice(), warningWeeklyPrice, "Warning weekly price is not correct");
    }

    @Then("user not see warning in weekly price")
    public void user_not_see_warning_in_weekly_price() {
        Assert.assertFalse(updateKos.isWarningWeeklyPriceAppear(), "Warning weekly price is appear");
    }

    @And("user see warning monthly price with {string}")
    public void user_see_warning_monthly_price_with(String warningMonthlyPrice) throws InterruptedException {
        Assert.assertEquals(updateKos.getWarningMonthlyPrice(), warningMonthlyPrice, "Warning monthly price is not correct");
    }

    @Then("user not see warning in monthly price")
    public void user_not_see_warning_in_monthly_price() {
        Assert.assertFalse(updateKos.isWarningMonthlyPriceAppear(), "Warning monthly price is appear");
    }

    @And("user see warning three monthly price with {string}")
    public void user_see_warning_three_monthly_price_with(String warningThreeMonthlyPrice) throws InterruptedException {
        Assert.assertEquals(updateKos.getWarningThreeMonthlyPrice(), warningThreeMonthlyPrice, "Warning three monthly price is not correct");
    }

    @Then("user not see warning in three monthly price")
    public void user_not_see_warning_in_three_monthly_price() {
        Assert.assertFalse(updateKos.isWarningThreeMonthlyPriceAppear(), "Warning three monthly price is appear");
    }

    @And("user see warning six monthly price with {string}")
    public void user_see_warning_six_monthly_price_with(String warningSixMonthlyPrice) throws InterruptedException {
        Assert.assertEquals(updateKos.getWarningSixMonthlyPrice(), warningSixMonthlyPrice, "Warning six monthly price is not correct");
    }

    @Then("user not see warning in six monthly price")
    public void user_not_see_warning_in_six_monthly_price() {
        Assert.assertFalse(updateKos.isWarningSixMonthlyPriceAppear(), "Warning six monthly price is appear");
    }

    @And("user see warning yearly price with {string}")
    public void user_see_warning_yearly_price_with(String warningYearlyPrice) throws InterruptedException {
        Assert.assertEquals(updateKos.getWarningYearlyPrice(), warningYearlyPrice, "Warning yearly price is not correct");
    }

    @Then("user not see warning in yearly price")
    public void user_not_see_warning_in_yearly_price() {
        Assert.assertFalse(updateKos.isWarningYearlyPriceAppear(), "Warning yearly price is appear");
    }

    @And("user see button update price disable")
    public void user_see_button_update_price_disable() throws InterruptedException {
        Assert.assertTrue(updateKos.isButtonUpdatePriceDisable(), "Button update price enable");
    }

    @Then("user see text {string}")
    public void user_see_text(String textUpdateHarga) throws InterruptedException {
        Assert.assertEquals(updateKos.getTextUpdateHarga(), textUpdateHarga, "Title not appears");
    }

    @Then("user see textbox monthly price")
    public void user_see_textbox_monthly_price() {
        Assert.assertTrue(updateKos.isTextboxMonthlyPriceChecked(), "Textbox monthly price not appears");
    }

    @Then("user see textlink other prices")
    public void user_see_textlink_other_prices() {
        Assert.assertTrue(updateKos.isTextlinkOtherPricesChecked(), "Textlink not appears");
    }

    @Then("user see textbox daily price, weekly price, {int} monthly price, {int} monthly price, yearly price")
    public void user_see_textbox_daily_price_weekly_price_monthly_price_monthly_price_yearly_price(Integer int1, Integer int2) throws InterruptedException {
        Assert.assertTrue(updateKos.isTextboxDailyPriceChecked(), "Textbox daily price not appears");
        Assert.assertTrue(updateKos.isTextboxWeeklyPriceChecked(), "Textbox weekly price not appears");
        Assert.assertTrue(updateKos.isTextboxThreeMonthlyPriceChecked(), "Textbox threemonthly price not appears");
        Assert.assertTrue(updateKos.isTextboxSixMonthlyPriceChecked(), "Textbox six monthly price not appears");
        Assert.assertTrue(updateKos.isTextboxYearlyPriceChecked(), "Textbox yearly price not appears");
    }

    @Then("user see field other costs mothly, fine fee, deposit fee, DP fee")
    public void user_see_field_other_costs_mothly_fine_fee_deposit_fee_DP_fee() throws InterruptedException {
        Assert.assertTrue(updateKos.isTextOtherPriceChecked(), "Text biaya lainnya per bulan not appears");
        Assert.assertTrue(updateKos.isTextFineFeeChecked(), "Text biaya denda per bulan not appears");
        Assert.assertTrue(updateKos.isTextDepositFeeChecked(), "Text biaya deposit not appears");
        Assert.assertTrue(updateKos.isTextDPFeeChecked(), "Text biaya DP not appears");
    }

    @Then("user see button update price enable")
    public void user_see_button_update_price_enable() throws InterruptedException {
        Assert.assertTrue(updateKos.isButtonUpdatePriceDisable(), "Button update price not appear");
    }

    @And("user click kos {string} in update price list")
    public void user_click_kos_in_update_price_list(String kosName) throws InterruptedException {
        updateKos.clickKosName(kosName);
    }
    @And("user search {string} in update room")
    public void user_search_in_update_room(String search) throws InterruptedException {
        updateKos.clickSearchInUpdateRoom(search);
    }

    @When("user click Add Kos in update price page")
    public void user_click_Add_Kos_in_update_price_page() throws InterruptedException {
        // Add kos button have same element with button done in room availability
        addKos.clickDoneRoomAvail();
    }

    @Then("user see infobar in update price with text {string}")
    public void user_see_infobar_in_update_price_with_text(String text) {
        Assert.assertEquals(updateKos.getPromoNgebutInfo(), text, "Text in promo ngebut infobar is not correct");
    }

    @When("user click monthly price checkbox in edit price")
    public void user_click_monthly_price_checkbox_in_edit_price() throws InterruptedException {
        updateKos.clickMonthlyPriceCheckbox();
    }

    @When("user click annual price checkbox in edit price")
    public void user_click_annual_price_checkbox_in_edit_price() throws InterruptedException {
        updateKos.clickYearlyPriceCheckbox();
    }

    @Then("user see monthly price field is disabled")
    public void user_see_monthly_price_field_is_disabled() {
        Assert.assertFalse(updateKos.isMonthlyPriceFieldEnable(), "Monthly price field is not disable");
    }

    @Then("user see yearly price field is disabled")
    public void user_see_yearly_price_field_is_disabled() {
        Assert.assertFalse(updateKos.isYearlyPriceFieldEnable(), "Yearly price field is not disable");
    }

    @When("user close infobar promo ngebut in update price")
    public void user_close_infobar_promo_ngebut_in_update_price() throws InterruptedException {
        updateKos.clickCloseInfobar();
    }

    @When("user memorize daily, weekly, monthly, three monthly, six monthly, and yearly price")
    public void user_memorize_daily_weekly_monthly_three_monthly_six_monthly_and_yearly_price() {
        this.dailyPrice = updateKos.getDailyPrice();
        this.weeklyPrice = updateKos.getWeeklyPrice();
        this.monthlyPrice = updateKos.getMonthlyPrice();
        this.threeMonthlyPrice = updateKos.getThreeMonthlyPrice();
        this.sixMonthlyPrice = updateKos.getSixMonthlyPrice();
        this.yearlyPrice = updateKos.getYearlyPrice();
    }

    @Then("user see daily, weekly, monthly, three monthly, six monthly, and yearly price is same with previous price")
    public void user_see_daily_weekly_monthly_three_monthly_six_monthly_and_yearly_price_is_same_with_previous_price() {
        Assert.assertEquals(updateKos.getDailyPrice(), this.dailyPrice, "Daily price is not correct");
        Assert.assertEquals(updateKos.getWeeklyPrice(), this.weeklyPrice, "Weekly price is not correct");
        Assert.assertEquals(updateKos.getMonthlyPrice(), this.monthlyPrice, "Monthly price is not correct");
        Assert.assertEquals(updateKos.getThreeMonthlyPrice(), this.threeMonthlyPrice, "Three monthly price is not correct");
        Assert.assertEquals(updateKos.getSixMonthlyPrice(), this.sixMonthlyPrice, "Six monthly price is not correct");
        Assert.assertEquals(updateKos.getYearlyPrice(), this.yearlyPrice, "Yearly price is not correct");
    }

    @When("user click update kos button")
    public void user_click_update_kos_button() throws InterruptedException {
        kosList.clickUpdateKosButton();
    }
}
