package steps.mamikos.owner.kos;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.kos.KostListPO;
import utilities.ThreadManager;

import java.util.List;

public class KosListsSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private KostListPO kosList = new KostListPO(driver);

    @Then("user clicks button find your kos/apartment here")
    public void user_clicks_button_find_your_kos_here() throws InterruptedException {
        kosList.clickOnSearchKosTextBox();
    }

    @Then("input name kos/apartment {string}")
    public void input_name_kos(String nameKos) throws InterruptedException {
        kosList.inputKosName(nameKos);
    }

    @Then("user clicks kos name from result")
    public void user_clicks_kos_name_from_result() throws InterruptedException {
        kosList.clickKosName();
    }

    @Then("user see kos with name {string}, status {string} and type {string}")
    public void user_see_kos_with_name_status_and_type(String name, String status, String type) throws InterruptedException {
        Assert.assertTrue(kosList.getFirstKosName().trim().replaceAll("\\s", "")
                .contains(name.replaceAll("\\s", "")), "Kos name is wrong");
        //  Assert.assertTrue(kosList.getFirstKosStatus().trim().contains(status), "Kos status is wrong");
        if (kosList.getFirstKosStatus2()) {
            Assert.assertTrue(kosList.getFirstKosStatus().trim().contains(status), "Kos name field is still enable");
        } else {
            Assert.assertFalse(kosList.getFirstKosStatus().trim().contains(status), "Kos name field is still enable");
        }
        if (kosList.getFirstKosType2()) {
            Assert.assertTrue(kosList.getFirstKosType2(), "Kos type is wrong");
        } else {
            Assert.assertEquals(kosList.getFirstKosType().trim(), type, "Kos type is wrong");
        }
    }

    @Then("user see kos photo src contains {string}")
    public void user_see_kos_photo_src_contains(String imageSrc) {
        Assert.assertTrue(kosList.getFirstKosImageSrc().contains(imageSrc), "Kos image src is wrong");
    }

    @When("user click see kos button")
    public void user_click_see_kos_button() throws InterruptedException {
        kosList.clickFirstSeeKos();
    }

    @Then("user see statistic is {string} in kos list")
    public void user_see_statistic_is_in_kos_list(String statistic) {
        Assert.assertEquals(kosList.getSelectedStatistic(), statistic, "Selected statistic is wrong");
    }

    @Then("user see all statistic option are :")
    public void user_see_all_statistic_option_are(List<String> statisticsOpt) {
        List<String> actualOptions = kosList.getAllStatisticOptions();
        for (int i=0; i<statisticsOpt.size(); i++) {
            Assert.assertEquals(actualOptions.get(i), statisticsOpt.get(i), "Statistic option is wrong, it should be " + statisticsOpt.get(i));
        }
    }

    @When("user click Chat in kos list")
    public void user_click_Chat_in_kos_list() throws InterruptedException {
        kosList.clickChat();
    }

    @When("user click review in kost list")
    public void user_click_review_in_kost_list() throws InterruptedException {
        kosList.clickReview();
    }

    @And("user click delete kos")
    public void user_click_delete_kos() throws InterruptedException {
        kosList.clickDeleteKos();
    }

    @And ("user click cancel to delete kos")
    public void user_click_cancel_to_delete_kos() throws InterruptedException{
        kosList.clickCancelDeleteKost();
    }
}
