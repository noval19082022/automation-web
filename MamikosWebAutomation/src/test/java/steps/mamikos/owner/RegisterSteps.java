package steps.mamikos.owner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.sl.In;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.register.RegisterPO;
import utilities.ThreadManager;

import java.util.List;

public class RegisterSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private RegisterPO registerPO = new RegisterPO(driver);

    @And("user fills out registration form with {string}, {string}, {string}, {string}")
    public void user_fills_out_registration_form_with(String name, String phone, String email, String password) throws InterruptedException {
        registerPO.fillOutRegisterForm(name, phone, email, password);
    }

    @Then("user verify error messages")
    public void user_verify_error_message(List<String> errorMessage) {
        for (int i = 0; i < errorMessage.size(); i++) {
            Assert.assertEquals(registerPO.getErrorMessages(errorMessage.get(i)), errorMessage.get(i), "Error message is not equal to " + errorMessage.get(i));
        }
    }
    @Then("button daftar will be disable")
    public void button_daftar_will_be_disable()throws InterruptedException{
        Assert.assertTrue(registerPO.disableButtonRegister(), "Button is enable");
    }

    @Then("user verify captcha box")
    public void user_verify_captcha_box() {
        Assert.assertTrue(registerPO.isCaptchaPresent(), "Captcha box is not present");
    }

    @And("user click Register on registration form")
    public void user_click_register_on_registration_form() throws InterruptedException {
        registerPO.clickOnRegisterButtonForm();
    }

    @Then("user verify captcha error messages")
    public void user_verify_captcha_error_messages() {
        Assert.assertTrue(registerPO.isCaptchaErrorPresent(), "Captcha error is not present");
    }

    @And("user click on show password button")
    public void user_click_on_show_password_button() throws InterruptedException {
        registerPO.clickPasswordEyeButton();
    }

    @Then("user verify captcha box and password input field")
    public void user_verify_captcha_box_and_password_input_field() {
        Assert.assertTrue(registerPO.isCaptchaPresent(), "Captcha box is not present");
        Assert.assertTrue(registerPO.isPasswordAttributePresent(), "Password input doesn't show password");
    }

    @And("user fills out registration form without click register {string}, {string}, {string}, {string}")
    public void user_fills_out_registration_form_without_click_register(String name, String phone, String email, String password) throws InterruptedException {
        registerPO.fillOutRegistrationFormWithoutClickRegister(name, phone, email, password);
    }

    @Then("user verify error messages on pop up {string}")
    public void user_verify_error_messages_on_pop_up(String error) {
        Assert.assertEquals(registerPO.getPopUpErrorMessage(), error, "Pop up error messages is not equal to " + error);
    }

    @Then("user verify name more than {int} characters")
    public void user_verify_name_more_than_characters(int character) {
        int counter = 0;
        for (int i = 0; i < registerPO.getNameInputText().length(); i++) {
            counter++;
        }
        Assert.assertTrue(counter > character, "Name is less than or equal to " + character);
    }

    @Then("user verify password more than {int} characters")
    public void user_verify_password_more_than_characters(int character) {
        int counter = 0;
        for (int i = 0; i < registerPO.getPasswordInputText().length(); i++) {
            counter++;
        }
        Assert.assertTrue(counter > character, "Password is less than or equal to " + character);
    }

    @Then("user verify password is equal or more than {int} characters")
    public void user_verify_password_is_equal_or_more_than_characters(int character) {
        int counter = 0;
        for (int i = 0; i < registerPO.getPasswordInputText().length(); i++) {
            counter++;
        }
        Assert.assertTrue(counter >= character, "Password kurang dari " + character + "karakter");
    }

    @Then("user validate email input")
    public void user_validate_email_input() {
        Assert.assertTrue(registerPO.getEmailInputText().matches("^\\S+@\\S+$"), "Email format invalid");
    }

    @Then("user verify name is equal or more than {int} characters")
    public void user_verify_name_is_equal_or_more_than(int character) {
        int counter = 0;
        for (int i = 0; i < registerPO.getNameInputText().length(); i++) {
            counter++;
        }
        Assert.assertTrue(counter >= character, "Name is less than " + character);
    }

    @Then("user see email title is displayed")
    public void field_will_have_title(){
        Assert.assertTrue(registerPO.isEmailTitleAvailable(),"Email (Opsional) not displayed");
    }
}
