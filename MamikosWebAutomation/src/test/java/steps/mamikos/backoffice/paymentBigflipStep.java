package steps.mamikos.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.paymentBigflipPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class paymentBigflipStep {
    private WebDriver driver = ThreadManager.getDriver();
    private paymentBigflipPO flip = new paymentBigflipPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @And("user switch to flip for business Test Mode")
    public void user_switch_to_flip_test_mode () throws InterruptedException {
        flip.clickOnTestModeButton();
        Assert.assertEquals(flip.getTestModeText(), "Anda sedang berada dalam Test Mode.");
    }

    @And("user go to riwayat transaksi domestic page")
    public void user_click_riwayat_transaksi_domestik () throws InterruptedException {
        flip.clickOnDomesticButton();
    }

    @And("user click force sucsess transaction")
    public void user_click_force_success () throws InterruptedException {
        flip.clickOnForceSuccessButton();
    }

    @And("user see that refund transaction is success")
    public void user_verify_success_refund () throws InterruptedException {
        Assert.assertEquals(flip.getSuccessText(), "Berhasil");
    }
}
