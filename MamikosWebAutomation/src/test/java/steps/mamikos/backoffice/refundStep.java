package steps.mamikos.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.backoffice.SearchContractPO;
import pageobjects.backoffice.refundPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class refundStep {
    private WebDriver driver = ThreadManager.getDriver();
    private refundPO leftRefund = new refundPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private SearchContractPO searchContract = new SearchContractPO(driver);
    private WebElement roomNumberDropdownList;


    @And("user click on refund button")
    public void user_click_on_add_discount_setting () throws InterruptedException {
        leftRefund.clickOnRefundButton();
    }

    @And("user click on back button popup refund")
    public void user_click_on_back_button_popup_refund () throws InterruptedException {
        leftRefund.clickOnBackButtonPopupRefund();
    }

    @And("user click on uncheck admin fee")
    public void user_click_on_uncheck_admin_fee () throws InterruptedException {
        leftRefund.clickOnUncheckAdminFee();
    }

    @And("user edit paid amount credit card {string}")
    public void user_edit_paid_amount_credit_card (String amount) throws InterruptedException {
        leftRefund.clickOnEditAmount(amount);
    }

    @And("user search by {string} and input field {string} refund menu")
    public void user_searchby (String sortBy, String input)throws InterruptedException {
        leftRefund.selectFilterSearchBy(sortBy);
        leftRefund.enterTextToSearchTextbox(input);
    }

    @And("user choose one of reason list")
    public void user_choose_one_reason () throws InterruptedException {
        leftRefund.clickOnChooseReason();
    }

    @And("user click refund and transfer button")
    public void user_click_refund_and_transfer_button () throws InterruptedException {
        leftRefund.clickOnRefundAndTransferButton();
    }

    @And("user click tab {string} on refund menu")
    public void user_click_on_tab_button (String tabButton) throws InterruptedException {
        leftRefund.clickOnTabButton(tabButton);
    }

    @Then("user successed visit Failed tab see detail columns")
    public void user_verify_tab_failed () throws InterruptedException {
        leftRefund.verifyFailedTab();
    }

    @And("user click button export")
    public void user_click_export_button () throws InterruptedException {
        leftRefund.clickOnExportButton();
    }

    @And("user choose one list date on the popup")
    public void user_choose_date_today () throws InterruptedException {
        leftRefund.chooseDateTodayButton();
    }

    @And("user click button download")
    public void user_click_download_button () throws InterruptedException {
        leftRefund.clickOndownloadButton();
    }

    @And("user click input account name {string}")
    public void user_click_input_account_name (String name) throws InterruptedException {
        leftRefund.clickOnInputAccountName(name);
    }

    @And("user click input account number {string}")
    public void user_click_input_account_number (String number) throws InterruptedException {
        leftRefund.clickOnInputAccountNumber(number);
    }

    @Then("user will get message success download and file exported send email")
    public void user_verify_sucsess_export () throws InterruptedException {
        this.roomNumberDropdownList = roomNumberDropdownList;
        if (selenium.isElementNotDisplayed(roomNumberDropdownList)) {
            Assert.assertTrue(searchContract.calloutText(), "Download Success. File sent to email.");
        }else {
            Assert.assertTrue(searchContract.calloutText(), "Download limit exceeded.");
        }
    }

    @Then("user will get error message")
    public void user_verify_failed_export () {
        this.roomNumberDropdownList = roomNumberDropdownList;
        if (selenium.isElementNotDisplayed(roomNumberDropdownList)) {
            Assert.assertTrue(searchContract.calloutText(), "Download Success. File sent to email.");
        }else {
            Assert.assertTrue(searchContract.calloutText(), "Download limit exceeded.");
        }
    }

    @Then("popup close and there will error massage {string}")
    public void popup_close_and_there_will_error_massage (String errorMassage){
        Assert.assertEquals(leftRefund.getResultFoundText(errorMassage), errorMassage, "Error message is not equal to " + errorMassage);
    }

    @And("user input nomor rekening {string} and pemilik rekening {string}")
    public void user_fills_field_rekening (String inputNoRekening, String inputNameRekening) throws InterruptedException {
        leftRefund.fillsFieldPemilikRekening(inputNoRekening, inputNameRekening);
    }

    @And("user verify that refund transaction is done")
    public void user_verify_refund_done () throws InterruptedException {
        Assert.assertEquals(leftRefund.getAccNumber(), "testing automation refund\n" + "123456\n" + "mandiri");
    }

    @And("user successed visit {string} and user can see detail columns")
    public void user_verify_transferred_tab (String detailsColumn) throws InterruptedException {
        if(detailsColumn.contains("transferred tab")) {
            leftRefund.verifyTransferredTab();
        }
        else if(detailsColumn.contains("transferred tab using flip transaction")) {
            leftRefund.verifyTransferredTab();
            leftRefund.verifyTransactionFlip();
        }
        else if(detailsColumn.contains("transferred tab using credit card transaction")) {
            leftRefund.verifyTransferredTab();
            leftRefund.verifyTransactionCreditCard();
        }
    }

    @And("user click button receipt")
    public void user_click_receipt_button () throws InterruptedException {
        leftRefund.clickReceiptButton();
    }

    @And("user click on dropdown bank name")
    public void user_click_on_dropdown_bank_name () throws InterruptedException {
        leftRefund.clickOnDropdownBankName ();
    }

    @And("user click section bank {string}")
    public void user_choose_bank_account (String bank) throws InterruptedException {
        leftRefund.chooseBankName (bank);
    }

    @And("user input bank name {string}")
    public void user_input_bank_bank_name (String bankName) throws InterruptedException {
        leftRefund.inputBankName (bankName);
    }

    @Then("popup list bank closed and bank selected")
    public void popup_list_bank_closed () {
        Assert.assertTrue(leftRefund.popupListBankClosed(), "popup list bank closed");
    }

    @Then("user will get message not result found")
    public void user_will_get_message_not_result_found () throws InterruptedException {
        Assert.assertTrue(leftRefund.messageNotResultFound(), "popup list bank closed");
    }

}
