package steps.mamikos.backoffice;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikosAdmin.SanjuniPero.SanjuniPeroPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;
import org.testng.Assert;
import pageobjects.backoffice.discountAdminFeePO;


public class discountAdminFeeStep {

        private WebDriver driver = ThreadManager.getDriver();
        private discountAdminFeePO discountAdminFee = new discountAdminFeePO(driver);
        private SeleniumHelpers selenium = new SeleniumHelpers(driver);


        @And("user click on add discount setting button")
        public void user_click_on_add_discount_setting () throws InterruptedException {
            discountAdminFee.userClickOnAddDiscountSetting();
        }

        @And("user click on delete button")
        public void user_click_on_delete_button () throws InterruptedException {
            discountAdminFee.userClickOnDeleteButton();
        }

        @And("user input discount name {string}")
        public void user_input_discount_name(String discountName) throws InterruptedException {
            discountAdminFee.userInputDiscountName(discountName);
        }

        @And("user select kost level")
        public void user_select_kost_level() throws InterruptedException {
            discountAdminFee.userSelectKostLevel();
        }

        @And("user input discount amount {string}")
        public void user_input_discount_amount(String discountAmount) throws InterruptedException {
            discountAdminFee.userInputDiscountAmount(discountAmount);
        }

        @And("user click on checkbox is booking and is non booking transaction")
        public void user_click_on_checkbox() throws InterruptedException {
            discountAdminFee.userClickOnChecbox();
        }

        @And("user click on save button")
        public void user_click_on_save_button() throws InterruptedException {
            discountAdminFee.userClickOnSaveButton();
        }

        @And("user click on edit button")
        public void user_click_on_edit_button() throws InterruptedException {
            discountAdminFee.userClickOnEditButton();
        }

         @And("user click detail fee button")
         public void user_click_on_detail_fee_button() throws InterruptedException {
            discountAdminFee.userClickOnDetailFee();
        }

         @Then("user see discount {string} in detail tagihan")
         public void user_see_discount_admin_fee_in_tagihan(String adminFee) {
            Assert.assertEquals(discountAdminFee.userSeeDicountAdminFeeInTagihan(), adminFee, "Admin Fee");
        }

         @Then("user see discount {string} in menu detail fee")
         public void user_see_discount_admin_fee_in_detail_fee(String adminFee) {
            Assert.assertEquals(discountAdminFee.userSeeDicountAdminFeeInDetailFee(), adminFee, "Admin Fee");
        }

        @Then("user redirect to {string} page")
        public void user_navigate_page(String pageHeader) {
            Assert.assertEquals(discountAdminFee.getInvoiceDiscountPageHeader(), pageHeader, "Invoice Admin Fee Discount Settings");
        }

        @Then("user see message {string}")
        public void user_see_message_success(String success) {
            Assert.assertEquals(discountAdminFee.getMessageSuccess(), success, "Page title is not equal to " + success);
    }

}

