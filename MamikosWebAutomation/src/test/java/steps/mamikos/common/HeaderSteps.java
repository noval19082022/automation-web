package steps.mamikos.common;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.List;

public class HeaderSteps {

	private WebDriver driver = ThreadManager.getDriver();
	private HeaderPO header = new HeaderPO(driver);
	private SeleniumHelpers selenium = new SeleniumHelpers(driver);


	@When("user clicks on Enter button")
	public void user_clicks_on_Enter_button() throws InterruptedException 
	{
		header.clickOnEnterButton();
	}

    @Then("user check the header items display")
    public void user_check_the_header_items_display() throws InterruptedException {
        Assert.assertTrue(header.isBookingKosDisplayed(), "Booking Kos button not present!");
        Assert.assertTrue(header.isDownloadAppDisplayed(), "Download App button not present!");
        Assert.assertTrue(header.isSearchAdsDisplayed(), "Cari Iklan button not present!");
        Assert.assertTrue(header.isFavoriteDisplayed(), "Favorite button ot present!");
        Assert.assertTrue(header.isChatDisplayed(), "Chat button ot present!");
        Assert.assertTrue(header.isNotificationButtonDisplayed(), "Notifikasi button not present!");
        Assert.assertTrue(header.isTenantProfilePictureDisplayed(), "Profile button not present!");
    }

    @Then("cari iklan dropdown display option")
    public void cari_iklan_dropdown_display_option(){
        Assert.assertTrue(header.isDropdownKosDiplayed(), "Kos button is not present");
        Assert.assertTrue(header.isDropdownApartemenDiplayed(), "Apartment button is not present");
//      Commented because the job vacancy page is temporarily hidden
//      Assert.assertTrue(header.isDropdownJobDiplayed(), "Lowongan Kerja button is not present");
    }

    @Then("lainnya dropdown display option")
    public void lainnya_dropdown_display_option(){
        Assert.assertTrue(header.isHelpCenterDropdownDisplayed(), "Help center dropdown is not present");
        Assert.assertTrue(header.isTermConditionDropdownDisplayed(), "Term Condition dropdown is not present");
    }

    @And("user tenant profile picture is shown")
    public void userTenantProfilePictureIsShown() {
        Assert.assertTrue(header.isTenantProfilePictureDisplayed(), "Tenant Profile Picture is not Displayed");
    }

    @And("chat room displayed")
    public void chat_room_displayed() {
        Assert.assertTrue(header.chatRoomIsDisplayed(), "Chat Room is not present");
    }

    @And("display notification list")
    public void display_notification_list() throws InterruptedException {
        Assert.assertTrue(header.isNotificationListDisplayed(), "Notification list is not present ");
        header.clickOnCloseNotification();
    }

    @And("user click cari iklan on header")
    public void user_click_cari_iklan_on_header() throws InterruptedException {
        header.clickOnSearchAds();
    }

    @And("user clicks on kos dropdown item list then redirect to {string}")
    public void user_clicks_on_kos_dropdown_item_list_then_redirect_to(String address) throws InterruptedException {
        Assert.assertEquals(header.getDropdownKos(), Constants.MAMIKOS_URL + address, "Address Cari Kos is not equals");
        selenium.back();
    }

    @And("user click on apartment dropdown item list then redirect to {string}")
    public void user_click_on_apartment_dropdown_item_list_then_redirect_to(String address) throws InterruptedException {
        Assert.assertEquals(header.getDropdownApartementAds(), Constants.MAMIKOS_URL + address, "Address Cari Apartemen is not equals");
        selenium.back();
    }

//    @And("user clicks on Job Vacancy Dropdown Item List then redirect to {string}")
//    public void user_clicks_on_job_vacancy_dropdown_item_list_then_redirect_ro(String address) throws InterruptedException {
//        Assert.assertEquals(header.getDropdownJobAds(), Constants.MAMIKOS_URL + address, "Address Cari Loker is not equals");
//        selenium.back();
//    }

    @And("user click download app on header then redirect to {string}")
    public void user_click_download_app_on_header_then_redirect_to(String address) throws InterruptedException {
        Assert.assertEquals(header.getDownloadAppURl(), address, "Address Download App not equals");
        selenium.back();
    }

    @And("user click pusat bantuan on header and redirect to {string}")
    public void user_click_pusat_bantuan_on_header_and_redirect_to(String address) throws InterruptedException {
        Assert.assertTrue(header.getHelpCenterURl().contains(address), "Link Pusat Bantuan is not equals");
        selenium.back();
    }
    
    @Then("tenant profile dropdown display option")
    public void tenant_profile_dropdown_display_option(List<String> option) throws InterruptedException {
        for (int i = 0; i < option.size(); i++) {
            Assert.assertEquals(header.getProfileOptionForTenant(i+1), option.get(i), "Option List " + option.get(i) + " is not present");
        }
        header.clickOnTenantProfile();
    }

    @And("I/user click Favourite tab")
    public void i_click_favourite_tab() throws InterruptedException {
        header.clickOnFavouriteButton();
    }

	@And("user choose {string} on dropdown list profile")
	public void user_choose_on_dropdown_list_profile(String option) throws InterruptedException {
        header.clickOnTenantProfile();
        header.clickOnAdsDropdown(option);
	}

	@When("user click profile on header")
	public void user_click_profile_on_header() throws InterruptedException {
		header.clickOnTenantProfile();
	}

    @And("user click lainnya on header")
    public void user_click_lainnya_on_header() throws InterruptedException {
        header.clickOnOtherDropdown();
    }

	@And("user click notifikasi on header")
	public void user_click_notifikasi_on_header() throws InterruptedException {
		header.clickOnNotification();
	}

	@When("user/tenant click on booking button on profile dropdown")
	public void x_click_on_booking_button_on_profile_dropdown() throws InterruptedException {
		header.clickBookingProfileDropdown();
	}

    @When("user click owner username on header")
    public void user_click_owner_username_on_header() throws InterruptedException {
        header.clickOwnerUserName();
    }

    @When("navbar before login appears")
    public void navbar_before_login_appears() {
        Assert.assertTrue(header.isBookingKosDisplayed(), "Booking Kos button not present!");
        Assert.assertTrue(header.isDownloadAppDisplayed(), "Download App button not present!");
        Assert.assertTrue(header.isSearchAdsDisplayed(), "Cari Iklan button not present!");
        Assert.assertTrue(header.isHelpCenterDisplayed(), "Pusat Bantuan button not present!");
        Assert.assertTrue(header.isTermConditionDisplayed(), "Syarat Ketentuan button not present!");
        Assert.assertTrue(header.isPromosiAdsDisplayed(), "Promosi Iklan button not present!");
        Assert.assertTrue(header.isEnterButtonDisplayed(), "Enter button not present!");
    }

    @Then("navbar kost page before login appears")
    public void navbar_kost_page_before_login_appears() {
        Assert.assertTrue(header.isDownloadAppDisplayed(), "Download App button not present!");
        Assert.assertTrue(header.isSearchAdsDisplayed(), "Cari Iklan button not present!");
        Assert.assertTrue(header.isHelpCenterDisplayed(), "Pusat Bantuan button not present!");
        Assert.assertTrue(header.isTermConditionDisplayed(), "Syarat Ketentuan button not present!");
        Assert.assertTrue(header.isPromosiAdsDisplayed(), "Promosi Iklan button not present!");
        Assert.assertTrue(header.isEnterButtonDisplayed(), "Enter button not present!");
    }

    @Then("navbar after login appears")
    public void navbar_after_login_appears() throws InterruptedException {
        Assert.assertTrue(header.isSearchAdsDisplayed(), "Cari Iklan button not present!");
        Assert.assertTrue(header.isFavoriteDisplayed(), "Favorite button not present!");
        Assert.assertTrue(header.isChatDisplayed(), "Chat button not present!");
        Assert.assertTrue(header.isNotificationButtonDisplayed(), "Notification button not present!");
        Assert.assertTrue(header.isOtherButtonDisplayed(), "Other button not present!");
        Assert.assertTrue(header.isTenantProfilePictureDisplayed(), "Profile pic not present!");
    }

    @Then("user verify notification text and click on it")
    public void user_verify_notification_text_and_click_on_it() throws InterruptedException {
        Assert.assertEquals(header.getNotificationText(), "Yah, permintaan booking tidak disetujui");
        header.clickOnRecentNotification();
    }

    @When("user clicks menu {string}")
    public void userClicksMenu(String menu) throws InterruptedException {
        header.clickHeaderMenu(menu);
    }

    @Then("user sees the url similar with {string}")
    public void userSeesTheUrlSimilarWith(String url) throws InterruptedException {
	    Assert.assertTrue(header.getHeaderURl().contains(url), "URL is not equals!");
    }

    @When("user clicks menu search ads dropdown")
    public void userClicksMenuSearchAdsDropdown() throws InterruptedException {
        header.clickOnSearchAds();
    }

    @When("user clicks menu navbar {string}")
    public void userClicksMenuNavbar(String ads) throws InterruptedException {
        header.clickOnAdsDropdown(ads);
    }

    @Then("owner can sees new notification")
    public void owner_can_sees_new_notification() {
        Assert.assertTrue(header.isNewNotifPresent());
        Assert.assertTrue(header.getNotificationNumber() > 0);
    }

    @Then("owner can sees first list notification title text contain {string}")
    public void owner_can_sees_first_list_notification_title_text_is(String notifText) throws InterruptedException {
	    header.clickNotificationOwner();
        Assert.assertTrue(header.getNotificationFirstListText().contains(notifText));
    }

    @When("owner click on first list notification")
    public void owner_click_on_first_list_notification() throws InterruptedException {
        header.clickOnFirstListNotification();
    }

    @Then("user click on {string}")
    public void user_click_on(String address) throws InterruptedException {
        Assert.assertEquals(header.getDropdownApartementAds(), Constants.MAMIKOS_URL +"/" + address, "Address Cari Apartemen is not equals");
    }

}