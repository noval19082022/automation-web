package steps.mamikos.common;

import io.cucumber.java.bs.I;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.account.LoginPO;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.owner.common.AddPopUpPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import static utilities.Constants.*;

public class LoginSteps {

	private WebDriver driver = ThreadManager.getDriver();
	private LoginPO loginpo = new LoginPO(driver);
	private AddPopUpPO popUp = new AddPopUpPO(driver);
	private HeaderPO header = new HeaderPO(driver);

	//Test Data Properties
	private String mamipayPropertyFile1="src/test/resources/testdata/mamikos/mamipay.properties";
	private String searchPropertyFile1="src/test/resources/testdata/mamikos/search.properties";
	private String premiumPropertyFile1="src/test/resources/testdata/mamikos/premium.properties";
	private String obPropertyFile1="src/test/resources/testdata/mamikos/OB.properties";

	//Voucherku Properties
	private String voucherkuPropertyFile1="src/test/resources/testdata/mamikos/voucherku.properties";

	//OB properties
	private String bookingDifferentGender="src/test/resources/testdata/mamikos/booking.properties";
	private String obOwnerFile = "src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
	private String obTenantFile = "src/test/resources/testdata/occupancy-and-billing/tenant.properties";

	//Payment Properties
	private String payment="src/test/resources/testdata/mamikos/payment.properties";

	//Martech Properties
	private String martech="src/test/resources/testdata/mamikos/martech.properties";

	//UGE Properties
	private String uge="src/test/resources/testdata/mamikos/ugeSquad.properties";

	//DOM Properties
	private String DOM="src/test/resources/testdata/mamikos/DOM.properties";

	//Mamipoin Tenant Properties
	private String mamipoinTenantProperties="src/test/resources/testdata/mamikos/mamipoin.properties";

	//Mamipoin Owner Properties
	private String mamipoinOwnerProperties="src/test/resources/testdata/mamikos/mamipoinOwner.properties";

	//Owner Mamipay user
	private String mamipayOwnerLoginPhoneNotRegisteredMamipay = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPhoneNotRegisteredMamipay_" + Constants.ENV);
	private String mamipayOwnerLoginPasswordNotRegisteredMamipay = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPasswordNotRegisteredMamipay_" + Constants.ENV);
	private String mamipayOwnerLoginPhoneNotRegisteredMamipay2 = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPhoneNotRegisteredMamipay2_" + Constants.ENV);
	private String mamipayOwnerLoginPasswordNotRegisteredMamipay2 = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPasswordNotRegisteredMamipay2_" + Constants.ENV);
	private String mamipayOwnerLoginPhoneNotRegisteredMamipay3 = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPhoneNotRegisteredMamipay3_" + Constants.ENV);
	private String mamipayOwnerLoginPasswordNotRegisteredMamipay3 = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPasswordNotRegisteredMamipay3_" + Constants.ENV);
	private String mamipayOwnerLoginPhoneDontHaveKost = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPhoneDontHaveKost_" + Constants.ENV);
	private String mamipayOwnerLoginPasswordDontHaveKost = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPasswordDontHaveKost_" + Constants.ENV);
	private String mamipayOwnerLoginPhoneBBKNotActive = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPhoneBBKNotActive_" + Constants.ENV);
	private String mamipayOwnerLoginPasswordBBKNotActive = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPasswordBBKNotActive_" + Constants.ENV);
	private String mamipayOwnerLoginPhoneNotActivatedBooking = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPhoneNotActivatedBooking_" + Constants.ENV);
	private String mamipayOwnerLoginPasswordNotActivatedBooking = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPasswordNotActivatedBooking_" + Constants.ENV);
	private String mamipayOwnerLoginPhoneAllActive = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPhoneAllActive_" + Constants.ENV);
	private String mamipayOwnerLoginPasswordAllActive = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPasswordAllActive_" + Constants.ENV);
	private String mamipayOwnerLoginPhoneOnlyApart = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPhoneOnlyApart_" + Constants.ENV);
	private String mamipayOwnerLoginPasswordOnlyApart = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPasswordOnlyApart_" + Constants.ENV);
	private String mamipayOwnerLoginPhoneNotRegisteredMamipayAndBBK = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPhoneBBKandMamipayOff_" + Constants.ENV);
	private String mamipayOwnerLoginPasswordNotRegisteredMamipayAndBBK = JavaHelpers.getPropertyValue(mamipayPropertyFile1,"ownerLoginPasswordBBKandMamipayOff_" + Constants.ENV);

	//Tenant Search user
	private String tenantFacebookEmail = JavaHelpers.getPropertyValue(searchPropertyFile1,"tenantFacebookEmail_" + Constants.ENV);
	private String tenantFacebookPassword = JavaHelpers.getPropertyValue(searchPropertyFile1,"tenantFacebookPassword_" + Constants.ENV);

    //Tenant Phone Number
    private String tenantPhoneNumber = JavaHelpers.getPropertyValue(searchPropertyFile1,"tenantPhoneNumber_" + Constants.ENV);
    private String tenantPhonePassword = JavaHelpers.getPropertyValue(searchPropertyFile1,"tenantPhonePassword_" + Constants.ENV);
    private String tenantPhoneNumberBX = JavaHelpers.getPropertyValue(searchPropertyFile1,"tenantPhoneNumberBX_" + Constants.ENV);
    private String tenantPhonePasswordBX = JavaHelpers.getPropertyValue(searchPropertyFile1,"tenantPhonePasswordBX_" + Constants.ENV);
	private String tenantCRMPhoneNumber = JavaHelpers.getPropertyValue(searchPropertyFile1,"tenantCRMPhoneNumber_" + Constants.ENV);
	private String tenantCRMPhonePassword = JavaHelpers.getPropertyValue(searchPropertyFile1,"tenantCRMPhonePassword_" + Constants.ENV);
	private String tenantPhoneNumberUG = JavaHelpers.getPropertyValue(uge,"tenant_phone_number_" + Constants.ENV);
	private String tenantPhoneNumberUG2 = JavaHelpers.getPropertyValue(uge,"tenant_phone_number_2_" + Constants.ENV);
	private String tenantPasswordUG = JavaHelpers.getPropertyValue(uge,"tenant_password_general_" + Constants.ENV);
	private String tenantPasswordUG2 = JavaHelpers.getPropertyValue(uge,"tenant_password_2_general_" + Constants.ENV);
	private String tenantPhoneNumberTA = JavaHelpers.getPropertyValue(uge,"tenant_phone_number_" + Constants.ENV);
	private String tenantPasswordTA = JavaHelpers.getPropertyValue(uge,"tenant_password_2_general_" + Constants.ENV);
	private String tenantPhoneNumberDOM = JavaHelpers.getPropertyValue(DOM,"tenant_phone_number_" + Constants.ENV);
	private String tenantPasswordDOM = JavaHelpers.getPropertyValue(DOM,"tenant_password_general_" + Constants.ENV);
	private String tenantVerifPhoneNumberTA = JavaHelpers.getPropertyValue(uge,"tenant_verif_phone_number_" + Constants.ENV);
	private String tenantVerifPasswordTA = JavaHelpers.getPropertyValue(uge,"tenant_verif_password_" + Constants.ENV);
	private String tenantCancelContractPF = JavaHelpers.getPropertyValue(uge, "tenant_phone_cancelcontract_" + Constants.ENV);
	private String tenantCancelContractPassPF = JavaHelpers.getPropertyValue(uge, "tenant_password_general_" + Constants.ENV);


	//Tenant with voucher
	private String tenantFacebookVoucherEmail = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"tenantFacebookVoucherEmail_" + Constants.ENV);
	private String tenantFacebookVoucherPassword = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"tenantFacebookVoucherPassword_" + Constants.ENV);

	//Tenant without voucher
	private String tenantFacebookNonVoucherEmail = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"tenantFacebookNonVoucherEmail_" + Constants.ENV);
	private String tenantFacebookNonVoucherPassword = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"tenantFacebookNonVoucherPassword_" + Constants.ENV);

	//Tenant for apply voucher
	private String tenantFacebookApplyVoucherEmail = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"tenantFacebookApplyVoucherEmail_" + Constants.ENV);
	private String tenantFacebookApplyVoucherPassword = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"tenantFacebookApplyVoucherPassword_" + Constants.ENV);

	//Owner for Apply Voucher
	private String ownerNumberApplyVoucher = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"ownerNumberApplyVoucher_" + Constants.ENV);
	private String ownerPasswordApplyVoucher = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"ownerPasswordApplyVoucher_" + Constants.ENV);

	//Owner for Apply Voucher Mamirooms
	private String ownerNumberApplyVoucherMamirooms = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"ownerNumberApplyVoucherMamirooms_" + Constants.ENV);
	private String ownerPasswordApplyVoucherMamirooms = JavaHelpers.getPropertyValue(voucherkuPropertyFile1,"ownerPasswordApplyVoucherMamirooms_" + Constants.ENV);


	//Tenant booking for gender male
	private String bookingMaleFacebookEmail = JavaHelpers.getPropertyValue(bookingDifferentGender,"tenant_facebook_email_gender_male_" + Constants.ENV);
	private String bookingMaleFacebookPassword = JavaHelpers.getPropertyValue(bookingDifferentGender,"tenant_facebook_password_gender_male_" + Constants.ENV);

	//Tenant booking for gender female
	private String bookingFemaleFacebookEmail = JavaHelpers.getPropertyValue(bookingDifferentGender,"tenant_facebook_email_gender_female_" + Constants.ENV);
	private String bookingFemaleFacebookPassword = JavaHelpers.getPropertyValue(bookingDifferentGender,"tenant_facebook_password_gender_female_" + Constants.ENV);

	//Payment - user tenant
	private String emailTenant_ = JavaHelpers.getPropertyValue(payment,"emailTenant_" + Constants.ENV);
	private String passwordTenant_ = JavaHelpers.getPropertyValue(payment,"passwordTenant_" + Constants.ENV);
	private String loginGmail = JavaHelpers.getPropertyValue(payment,"tenant_login_gmail_" + ENV);
	private String loginGmailPassword = JavaHelpers.getPropertyValue(payment,"tenant_login_gmail_password_" + ENV);

	//Payment - owner
	private String _ownerNumber_payment = JavaHelpers.getPropertyValue(payment,"ownerNumber_payment_" + Constants.ENV);
	private String _ownerPassword_payment = JavaHelpers.getPropertyValue(payment,"ownerPassword_payment_" + Constants.ENV);
	private String _ownerNumber_payment_BNI = JavaHelpers.getPropertyValue(payment,"ownerNumber_payment_BNI_" + Constants.ENV);
	private String _ownerPassword_payment_BNI = JavaHelpers.getPropertyValue(payment,"ownerPassword_payment_BNI_" + Constants.ENV);

	//Martech - Owner
	private String _noHP_ownerNotGP = JavaHelpers.getPropertyValue(martech,"ownerNumber_notGP_" + Constants.ENV);
	private String _password_ownerNotGP = JavaHelpers.getPropertyValue(martech,"ownerPassword_notGP_" + Constants.ENV);

	//UG - Owner Invalid Password Login
	public String invalidPasswordPhoneNumberInput = "0812345670001";
	public String invalidPasswordPasswordInput = "asdasd123";

	//UG - Owner
	public String phoneNumberOwnerEmpty = JavaHelpers.getPropertyValue(uge, "owner_phone_number_empty_" + Constants.ENV);
	public String passwordOwnerEmpty = JavaHelpers.getPropertyValue(uge, "owner_password_general_" + Constants.ENV);

	//UG - Bigflip
	public String IDBigflip = JavaHelpers.getPropertyValue(uge, "ID_bigflip_" + Constants.ENV);
	public String emailBigflip = JavaHelpers.getPropertyValue(uge, "email_bigflip_" + Constants.ENV);
	public String passwordBigflip = JavaHelpers.getPropertyValue(uge, "pass_bigflip_" + Constants.ENV);

	//UG - Owner GP
	private String phoneOwnerGp1 = JavaHelpers.getPropertyValue(uge,"owner_phone_gp1_" + Constants.ENV);
	private String passwordOwnerGp1 = JavaHelpers.getPropertyValue(uge,"owner_password_general_" + Constants.ENV);
	private String phoneOwnerGp2 = JavaHelpers.getPropertyValue(uge,"owner_phone_gp2_" + Constants.ENV);
	private String passwordOwnerGp2 = JavaHelpers.getPropertyValue(uge,"owner_password_general_" + Constants.ENV);
	private String phoneOwnerGp3 = JavaHelpers.getPropertyValue(uge,"owner_phone_gp3_" + Constants.ENV);
	private String passwordOwnerGp3 = JavaHelpers.getPropertyValue(uge,"owner_password_general_" + Constants.ENV);
	private String ownerKosReviewPhone = JavaHelpers.getPropertyValue(uge, "owner_phone_kost_review_" + Constants.ENV);
	private String ownerKosReviewPassword = JavaHelpers.getPropertyValue(uge, "owner_password_general_" + Constants.ENV);
	private String noListingOwnerPhone = JavaHelpers.getPropertyValue(uge, "owner_phone_nolisting_" + Constants.ENV);
	private String noListingOwnerPassword = JavaHelpers.getPropertyValue(uge, "owner_password_general_" + Constants.ENV);
	private String ownerTerminatedGPPhone = JavaHelpers.getPropertyValue(uge, "owner_terminated_gp_" + Constants.ENV );
	private String ownerTerminatedGPPass = JavaHelpers.getPropertyValue(uge, "owner_password_general_" + Constants.ENV );
	private String ownerLastDayGPPhone = JavaHelpers.getPropertyValue(uge, "owner_lastdasy_gp_" + Constants.ENV );
	private String ownerLastDayGPPass = JavaHelpers.getPropertyValue(uge, "owner_password_general_" + Constants.ENV );
	private String ownerDoesntHavePoin = JavaHelpers.getPropertyValue(uge, "owner_doesnt_have_mamipoin_" + Constants.ENV );
	private String ownerDoesntHavePoinPass = JavaHelpers.getPropertyValue(uge, "owner_password_general_" + Constants.ENV );
	private String ownerLimoNotGP = JavaHelpers.getPropertyValue(uge, "owner_phone_nongp_staging_1_" + Constants.ENV );
	private String ownerLimoNotGPPass = JavaHelpers.getPropertyValue(uge, "owner_password_general_" + Constants.ENV );
	private String ownerDeleteGPPhone = JavaHelpers.getPropertyValue(uge, "owner_phone_delete_gp_" + Constants.ENV );
	private String ownerDeleteGPPass = JavaHelpers.getPropertyValue(uge, "owner_password_delete_gp_" + Constants.ENV );
	private String ownerRecurringGPPhone = JavaHelpers.getPropertyValue(uge, "owner_phone_gp_recurring_7_" + Constants.ENV );
	private String ownerRecurringGPPass = JavaHelpers.getPropertyValue(uge, "owner_password_gp_recurring_7_" + Constants.ENV );
	private String ownerLimoNotGPverify = JavaHelpers.getPropertyValue(uge, "owner_phone_NonGP_verify_" + Constants.ENV );
	private String ownerLimoNotGPPassverify = JavaHelpers.getPropertyValue(uge, "owner_password_NonGP_verify_" + Constants.ENV );


	//Owner Premium New Alokasi Saldo
	private String premiumnewalokasi = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_premiumnewalokasi_" + Constants.ENV);
	private String premiumPassword = JavaHelpers.getPropertyValue(premiumPropertyFile1,"premium_password");
	private String nonPremiumOwnerUser = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_nonpremium_" + Constants.ENV);
	private String premiumOwnerUser = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_premium_" + Constants.ENV);
	private String nonHaveKostOwnerUser = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_nonhavekost_" + Constants.ENV);
	private String premiumsaldoOwnerUser = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_premiumsaldo_" + Constants.ENV);
	private String premiumpaymentOwnerUser = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_confirmationpayment_" + Constants.ENV);
	private String premiumNosaldo = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_nosaldo_" + Constants.ENV);
	private String nonKostDitolakOwnerUser = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_nonkostditolak_" + Constants.ENV);
	private String noPropertiAvailOwnerUser = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_nopropertiavail_" + Constants.ENV);
	private String premiumGP = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_premiumGP_"+Constants.ENV);
	private String kostverification = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_kostverification_"+Constants.ENV);
	private String mamiadsNewOwner = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_phone_newower_"+Constants.ENV);
	private String premiumkos0apart1 = JavaHelpers.getPropertyValue(premiumPropertyFile1,"owner_phone_premiumkos0apart1_"+Constants.ENV);
	private String mamiadsListingFullOccupiedOwner = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_all_listing_full_occupied_"+Constants.ENV);
	private String nonMamiadsAllListingIsAllocated = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_nonmamiads_all_listing_is_allocated_"+Constants.ENV);
	private String mamiadsAllListingIsAllocated = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_mamiads_all_listing_is_allocated_"+Constants.ENV);
	private String neverPurchaseMamiadsSaldoBelow5000 = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_never_purchase_mamiads_saldo_below_5000_"+Constants.ENV);
	private String everPurchaseMamiadsSaldoBelow5000 = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_ever_purchase_mamiads_saldo_below_5000_"+Constants.ENV);
	private String mamiadsTenantNeverSeeDetailProperty = JavaHelpers.getPropertyValue(premiumPropertyFile1, "tenant_never_see_detail_property_"+Constants.ENV);
	private String mamiadsTenantNoRekomendasi = JavaHelpers.getPropertyValue(premiumPropertyFile1, "tenant_no_rekomendasi_"+Constants.ENV);
	private String neverPurchaseMamiadsHaveNotActiveAds = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_never_purchase_mamiads_have_not_active_ads_"+Constants.ENV);
	private String tenantMamiAds = JavaHelpers.getPropertyValue(premiumPropertyFile1, "tenant_mamiads_"+Constants.ENV);
	private String mamiadsNewPhoneOwner = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_new_phone_"+Constants.ENV);
	private String haveNotProperty = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_havenot_property_"+Constants.ENV);
	private String saldomamiadsinsufficient = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_saldo_mamiads_insufficient_"+Constants.ENV);
	private String ownerDoesntHaveActiveKostPhone = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_phone_doesnthavekostactive_"+Constants.ENV);
	private String ownerDoesntHaveActiveKostPassword = JavaHelpers.getPropertyValue(premiumPropertyFile1, "owner_password_doesnthavekostactive_"+Constants.ENV);



	//OB - Test Data
	private String bookingBaruDilihatEmail = JavaHelpers.getPropertyValue(bookingDifferentGender,"tenant_facebook_email_baru_dilihat_" + Constants.ENV);
	private String bookingBaruDilihatPassword = JavaHelpers.getPropertyValue(bookingDifferentGender,"tenant_facebook_password_baru_dilihat_" + Constants.ENV);
	private String bookingOwnerPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "uprasOwner_" + Constants.ENV);
	private String bookingOwnerPhonePass = JavaHelpers.getPropertyValue(obPropertyFile1, "uprasOwnerPass_" + Constants.ENV);
	private String baruDilihatEmptyState = JavaHelpers.getPropertyValue(obPropertyFile1, "emptyStateBaruDilihat_" + Constants.ENV);
	private String baruDilihatEmptyStatePass = JavaHelpers.getPropertyValue(obPropertyFile1, "emptyStateBaruDilihatPass_" + Constants.ENV);
	private String tenantCancel = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantCancel_" + Constants.ENV);
	private String tenantCancelPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantCancelPass_" + Constants.ENV);
	private String additionalPriceOwnerPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "ownerWendyAutomation_" + Constants.ENV);
	private String additionalPriceOwnerPass = JavaHelpers.getPropertyValue(obPropertyFile1, "ownerWendyAutomationPass_" + Constants.ENV);
	private String additionalPriceTenantPass = JavaHelpers.getPropertyValue(obPropertyFile1, "additionalPricePass_" + Constants.ENV);
	private String additionalPriceTenantPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "additionalPricePhone_" + Constants.ENV);
	private String tenantBookingFullFlowEmail = JavaHelpers.getPropertyValue(obPropertyFile1, "tenant_booking_full_flow_email_" + Constants.ENV);
	private String tenantBookingFullFlowPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenant_booking_full_flow_password_" + Constants.ENV);
	private String rejectOwnerPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "ownerAutomation_" + Constants.ENV);
	private String rejectOwnerPass = JavaHelpers.getPropertyValue(obPropertyFile1, "ownerAutomationPass_" + Constants.ENV);
	private String dashboardNotBbkOwnerPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "riniOwnerNotBbk_" + Constants.ENV);
	private String dashboardNotBbkOwnerPass = JavaHelpers.getPropertyValue(obPropertyFile1, "riniOwnerNotBbkPass_" + Constants.ENV);
	private String baruDilihatPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "baruDilihatPhone_" + Constants.ENV);
	private String baruDilihatPass = JavaHelpers.getPropertyValue(obPropertyFile1, "baruDilihatPass_" + Constants.ENV);
	private String markRoomGP = JavaHelpers.getPropertyValue(obOwnerFile, "rinisPhoneOwner_" + Constants.ENV);
	private String markRoomGPPass = JavaHelpers.getPropertyValue(obOwnerFile, "rinisPhoneOwnerPass_" + Constants.ENV);
	private String differentGenderMalePhone = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantMalePhone_" + Constants.ENV);
	private String differentGenderMalePhonePass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantMalePhonePass_" + Constants.ENV);
	private String differentGenderFemalePhone = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantFemalePhone_" + Constants.ENV);
	private String differentGenderFemalePhonePass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantFemalePhonePass_" + Constants.ENV);
	private String obQAMainPhone = JavaHelpers.getPropertyValue(obOwnerFile, "uprasMain_" + Constants.ENV);
	private String obQAMainPhonePass = JavaHelpers.getPropertyValue(obOwnerFile, "uprasMainPass_" + Constants.ENV);
	private String gpOwnerPhone = JavaHelpers.getPropertyValue(obOwnerFile, "ownerGP_" + Constants.ENV);
	private String gpOwnerPass = JavaHelpers.getPropertyValue(obOwnerFile, "ownerGPPass_" + Constants.ENV);

	private String gpOwnerPhone2 = JavaHelpers.getPropertyValue(obOwnerFile, "ownerGP2_" + Constants.ENV);
	private String gpOwnerPass2 = JavaHelpers.getPropertyValue(obOwnerFile, "ownerGPPass2_" + Constants.ENV);

	private String obOwnerHaveNotBbkPhone = JavaHelpers.getPropertyValue(obOwnerFile, "ownerNotBBK_" + Constants.ENV);
	private String obOwnerHaveNotBbkPass = JavaHelpers.getPropertyValue(obOwnerFile, "ownerNotBBKPass_" + Constants.ENV);
	private String obTenantBssPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "riniPhone_" + Constants.ENV);
	private String obTenantBssPass = JavaHelpers.getPropertyValue(obPropertyFile1, "riniPass_" + Constants.ENV);
	private String obTenantEmptyState = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantEmptyState_" + Constants.ENV);
	private String obTenantEmptyStatePass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantEmptyStatePass_" + Constants.ENV);
	private String obTenantJobValidation = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantJobValidation_" + Constants.ENV);
	private String obTenantJobValidationPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantJobValidationPass_" + Constants.ENV);
	private String obTenantKaryawanValidation = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKaryawanValidation_" + Constants.ENV);
	private String obTenantKaryawanValidationPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKaryawanValidationPass_" + Constants.ENV);
	private String obTenantMhsValidation = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantMhsValidation_" + Constants.ENV);
	private String obTenantMhsValidationPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantMhsValidationPass_" + Constants.ENV);
	private String obTenantLainnyaValidation = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantLainnyaValidation_" + Constants.ENV);
	private String obTenantLainnyaValidationPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantLainnyaValidationPass_" + Constants.ENV);
	private String obTenantContractDbet = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantContractDbet_" + Constants.ENV);
	private String obTenantContractDbetPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantContractDbetPass_" + Constants.ENV);
	private String obTenantKostSayaHomepage = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostSayaHomepage_" + Constants.ENV);
	private String obTenantKostSayaHomepagePass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostSayaHomepagePass_" + Constants.ENV);
	private String obTenantKosSayaWaitingConfirm = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostWaitingConfirm_" + Constants.ENV);
	private String obTenantKosSayaWaitingConfirmPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostWaitingConfirmPass_" + Constants.ENV);
	private String obTenantKosSayaDraftBookingProbut = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostDraftProbut_" + Constants.ENV);
	private String obTenantKosSayaDraftBookingProbutPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostDraftProbutPass_" + Constants.ENV);
	private String obTenantKosSayaWaitingConfirmProbut = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostWaitingConfirmProbut_" + Constants.ENV);
	private String obTenantKosSayaWaitingConfirmProbutPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostWaitingConfirmProbutPass_" + Constants.ENV);
	private String obTenantKostSayaRejected = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostSayaRejected_" + Constants.ENV);
	private String obTenantKostSayaRejectedPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantKostSayaRejectedPass_" + Constants.ENV);
	private String obTenantStatus = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantStatus_" + Constants.ENV);
	private String obTenantStatusPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantStatusPass_" + Constants.ENV);
	private String obOwnerBds = JavaHelpers.getPropertyValue(obOwnerFile, "ownerBds_" + Constants.ENV);
	private String obOwnerBdsPass = JavaHelpers.getPropertyValue(obOwnerFile, "ownerBdsPass_" + Constants.ENV);
	private String obOwnerRejectBooking = JavaHelpers.getPropertyValue(obPropertyFile1, "ownerRejectBooking_" + Constants.ENV);
	private String obOwnerRejectBookingPass = JavaHelpers.getPropertyValue(obPropertyFile1, "ownerRejectBookingPass_" + Constants.ENV);
	private String obTenantUniqueCode = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantUniqueCode_" + Constants.ENV);
	private String obTenantUniqueCodePass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantUniqueCodePass_" + Constants.ENV);
	private String obTenantNotification = JavaHelpers.getPropertyValue(obTenantFile, "tenantNotif_" + Constants.ENV);
	private String obTenantNotificationPass = JavaHelpers.getPropertyValue(obTenantFile, "tenantNotifPass_" + Constants.ENV);
	private String obTenantFullPayKosSaya = JavaHelpers.getPropertyValue(obTenantFile, "tenantFullPayKosSaya_" + Constants.ENV);
	private String obTenantFullPayKosSayaPass = JavaHelpers.getPropertyValue(obTenantFile, "tenantFullPayKosSayaPass_" + Constants.ENV);
	private String obTenantKostSayaConfirm = JavaHelpers.getPropertyValue(obTenantFile, "tenantKostSayaConfirmBooking_" + Constants.ENV);
	private String obTenantKostSayaConfirmPass = JavaHelpers.getPropertyValue(obTenantFile, "tenantKostSayaConfirmBookingPass_" + Constants.ENV);
	private String obTenantDpStKosSaya = JavaHelpers.getPropertyValue(obTenantFile, "tenantDpStKosSaya_" + Constants.ENV);
	private String obTenantDpStKosSayaPass = JavaHelpers.getPropertyValue(obTenantFile, "tenantDpStKosSayaPass_" + Constants.ENV);
	private String obChatBthResponPngajuanSewaLbl = JavaHelpers.getPropertyValue(obTenantFile, "tenantChatBthPngajuanSewaLbl_" + Constants.ENV);
	private String obChatBthResponPngajuanSewaLblPass = JavaHelpers.getPropertyValue(obTenantFile, "tenantChatBthPngajuanSewaLblPass_" + Constants.ENV);
	private String obRejectWaitingPhone = JavaHelpers.getPropertyValue(obTenantFile, "obRejectWaitingPhone_" + Constants.ENV);
	private String obRejectWaitingPhonePass = JavaHelpers.getPropertyValue(obTenantFile, "obRejectWaitingPhonePass_" + Constants.ENV);
	private String obOwnerRejectFullPhone = JavaHelpers.getPropertyValue(obTenantFile, "obOwnerRejectFullPhone_" + Constants.ENV);
	private String obOwnerRejectFullPass = JavaHelpers.getPropertyValue(obTenantFile, "obOwnerRejectFullPass_" + Constants.ENV);
	private String bbmSinggahSiniPhone = JavaHelpers.getPropertyValue(obTenantFile, "bbmSinggahSiniPhone_" + Constants.ENV);
	private String bbmSinggahSiniPass = JavaHelpers.getPropertyValue(obTenantFile, "bbmSinggahSiniPass_" + Constants.ENV);

	private String bbmAcceptChatPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantAcceptChatPhone_" + Constants.ENV);
	private String bbmAcceptChatPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantAcceptChatPass_" + Constants.ENV);
	private String bbmJobsLainnyaPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantJobsValidationPhone_" + Constants.ENV);
	private String bbmJobsLainnyaPass = JavaHelpers.getPropertyValue(obPropertyFile1, "tenantJobsValidationPass_" + Constants.ENV);


	//Mamipoin - Tenant
	private String tenantBlacklistMamipoinEmail = JavaHelpers.getPropertyValue(mamipoinTenantProperties,"emailTenantBlacklistMamipoin_" + Constants.ENV);
	private String tenantBlacklistMamipoinPassword = JavaHelpers.getPropertyValue(mamipoinTenantProperties,"passwordTenantBlacklistMamipoin_" + Constants.ENV);
	private String tenantEmptyMamipoinEmail = JavaHelpers.getPropertyValue(mamipoinTenantProperties,"emailTenantEmptyMamipoin_" + Constants.ENV);
	private String tenantEmptyMamipoinPassword = JavaHelpers.getPropertyValue(mamipoinTenantProperties,"passwordTenantEmptyMamipoin_" + Constants.ENV);
	private String tenantWhitelistMamipoinEmail = JavaHelpers.getPropertyValue(mamipoinTenantProperties,"emailTenantWhitelistMamipoin_" + Constants.ENV);
	private String tenantWhitelistMamipoinPassword = JavaHelpers.getPropertyValue(mamipoinTenantProperties,"passwordTenantWhitelistMamipoin_" + Constants.ENV);
	private String noTenantNoHaveMamipoin = JavaHelpers.getPropertyValue(mamipoinTenantProperties,"noTenantNoHaveMamipoin_" + Constants.ENV);
	private String passwordTenantNoHaveMamipoin = JavaHelpers.getPropertyValue(mamipoinTenantProperties,"passwordTenantNoHaveMamipoin_" + Constants.ENV);
	private String noTenantHaveMamipoin = JavaHelpers.getPropertyValue(mamipoinTenantProperties,"noTenantHaveMamipoin_" + Constants.ENV);

	//MamiPoin - Owner
	private String mamipoinNewOwnerPhone = JavaHelpers.getPropertyValue(mamipoinOwnerProperties,"phoneNewOwnerMamipoin_" + Constants.ENV);
	private String mamipoinNewOwnerPassword = JavaHelpers.getPropertyValue(mamipoinOwnerProperties,"passwordNewOwnerMamipoin_" + Constants.ENV);
	private String mamipoinOwnerPhone = JavaHelpers.getPropertyValue(mamipoinOwnerProperties,"phoneOwnerMamipoin_" + Constants.ENV);
	private String mamipoinOwnerPassword = JavaHelpers.getPropertyValue(mamipoinOwnerProperties,"passwordOwnerMamipoin_" + Constants.ENV);
	private String ownerApplyMamipoinPhone = JavaHelpers.getPropertyValue(mamipoinOwnerProperties,"phoneOwnerApplyMamipoin_" + Constants.ENV);
	private String ownerApplyMamipoinPassword = JavaHelpers.getPropertyValue(mamipoinOwnerProperties,"passwordOwnerApplyMamipoin_" + Constants.ENV);
	private String mamipoinOwnerFixedPoinPhone = JavaHelpers.getPropertyValue(mamipoinOwnerProperties,"phoneOwnerFixedMamipoin_" + Constants.ENV);
	private String mamipoinOwnerFixedPoinPassword = JavaHelpers.getPropertyValue(mamipoinOwnerProperties,"passwordOwnerFixedMamipoin_" + Constants.ENV);
	private String ownerLimoBlacklist = JavaHelpers.getPropertyValue(mamipoinOwnerProperties, "phoneOwnerBlacklistMamipoin_" + Constants.ENV );
	private String ownerLimoBlacklistPass = JavaHelpers.getPropertyValue(mamipoinOwnerProperties, "passwordOwnerBlacklistMamipoin_" + Constants.ENV );
	private String ownerLimoMamiPoin = JavaHelpers.getPropertyValue(mamipoinOwnerProperties, "phoneOwnerMamipoin_satu_" + Constants.ENV );
	private String ownerLimoMamiPoinPass = JavaHelpers.getPropertyValue(mamipoinOwnerProperties, "passwordOwnerMamipoin_satu_" + Constants.ENV );
	private String ownerLimoMamiPoinDua = JavaHelpers.getPropertyValue(mamipoinOwnerProperties, "phoneOwnerMamipoin_dua_staging_" + Constants.ENV );
	private String ownerLimoMamiPoinDuaPass = JavaHelpers.getPropertyValue(mamipoinOwnerProperties, "passwordOwnerMamipoin_dua_staging_" + Constants.ENV);
	private String ownerLimoMamiPoinTiga = JavaHelpers.getPropertyValue(mamipoinOwnerProperties, "phoneOwnerMamipoin_tiga_staging_" + Constants.ENV );
	private String ownerLimoMamiPoinTigaPass = JavaHelpers.getPropertyValue(mamipoinOwnerProperties, "passwordOwnerMamipoin_tiga_staging_" + Constants.ENV);



	//TENG
	private String tengOwner = "src/test/resources/testdata/mamikos/tenant-engagement-owner.properties";
	private String tengTenant = "src/test/resources/testdata/mamikos/tenant-engagement.properties";
	//TENG - Owner
	private String tengSettlementInvoiceKost = JavaHelpers.getPropertyValue(tengOwner, "settlementInvoiceKostName_" + Constants.ENV);
	private String tengTenantPass = JavaHelpers.getPropertyValue(tengTenant, "generalPass_" + Constants.ENV);
	private String tengManageBillsOwnerPhone = JavaHelpers.getPropertyValue(tengOwner, "manage_bills_phone_" + Constants.ENV);
	private String tengManageBillsOwnerPass = JavaHelpers.getPropertyValue(tengOwner, "manage_bills_password_" + Constants.ENV);
	private String tenantTengBookingPhone = JavaHelpers.getPropertyValue(tengTenant, "tengInvoice_" + Constants.ENV);
	private String tenantTengBookingPhonePass = JavaHelpers.getPropertyValue(tengTenant, "tengInvoicePass_" + Constants.ENV);
	private String tenantTengInvoiceSettle = JavaHelpers.getPropertyValue(tengTenant, "tengInvoiceFixed_" + Constants.ENV);
	private String tengEditBankOwnerPhone = JavaHelpers.getPropertyValue(tengOwner, "adit_ownerphone_" + Constants.ENV);
	private String tengEditBankOwnerPass = JavaHelpers.getPropertyValue(tengOwner, "adit_ownerphonepass_" + Constants.ENV);
	private String tenantTengVoucherKuPhone = JavaHelpers.getPropertyValue(tengTenant, "tenantTengVoucherKuPhone_" + Constants.ENV);
	private String tenantTengVoucherKuPass = JavaHelpers.getPropertyValue(tengTenant, "tenantTengVoucherKuPass_" + Constants.ENV);
	private String tengAdminInvoicePhone = JavaHelpers.getPropertyValue(tengTenant, "tengInvoiceAdmin_" + Constants.ENV);
	private String tengAdminInvoiceOwner = JavaHelpers.getPropertyValue(tengOwner, "ownerWendyAutomation_" + Constants.ENV);
	private String tengOwnerWendyPass = JavaHelpers.getPropertyValue(tengOwner, "ownerWendyAutomationPass_" + Constants.ENV);
	private String tengRecurringInvoice = JavaHelpers.getPropertyValue(tengTenant, "tengRecurringInvoice_" + Constants.ENV);
	private String tengSettlementInvoice = JavaHelpers.getPropertyValue(tengTenant, "tengSettlementInvoice_" + Constants.ENV);
	private String tengInvoiceDetail = JavaHelpers.getPropertyValue(tengTenant, "tengInvoiceDetail_" + Constants.ENV);
	private String tengControlledProperty = JavaHelpers.getPropertyValue(tengTenant, "tengControlledProperty_" + Constants.ENV);
	private String adiTengOwnerAddOnsPhone = JavaHelpers.getPropertyValue(tengOwner, "adiTengOwnerAddOnsPhone_" + Constants.ENV);
	private String adiTengOwnerAddOnsPass = JavaHelpers.getPropertyValue(tengOwner, "adiTengOwnerAddOnsPass_" + Constants.ENV);
	private String adiTengOwnerVoucherPhone = JavaHelpers.getPropertyValue(tengOwner, "adiTengOwnerVoucherPhone_" + Constants.ENV);
	private String adiTengOwnerVoucherPass = JavaHelpers.getPropertyValue(tengOwner, "adiTengOwnerVoucherPass_" + Constants.ENV);

	//TENG-Tenant
	private String adiTengKostReviewSubmittedPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengKostReviewSubmittedPhone_" + Constants.ENV);
	private String adiTengKostReviewSubmittedPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengKostReviewSubmittedPass_" + Constants.ENV);
	private String adiTengKostReviewNotSubmittedPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengKostReviewNotSubmittedPhone_" + Constants.ENV);
	private String adiTengKostReviewNotSubmittedPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengKostReviewNotSubmittedPass_" + Constants.ENV);
	private String adiTengApplyVoucherPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengApplyVoucherPhone_" + Constants.ENV);
	private String adiTengApplyVoucherPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengApplyVoucherPass_" + Constants.ENV);
	private String tengVoucherBaseOnUserPhone = JavaHelpers.getPropertyValue(tengTenant, "tengVoucherBaseOnUserPhone_" + Constants.ENV);
	private String tengVoucherBaseOnUserPass = JavaHelpers.getPropertyValue(tengTenant, "tengVoucherBaseOnUserPass_" + Constants.ENV);
	private String adiTengApplyVoucherDuaPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengApplyVoucherDuaPhone_" + Constants.ENV);
	private String adiTengApplyVoucherDuaPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengApplyVoucherDuaPass_" + Constants.ENV);
	private String adiTengAddOnsPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengAddOnsPhone_" + Constants.ENV);
	private String adiTengAddOnsPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengAddOnsPass_" + Constants.ENV);
	private String adiTengWhitelistMamipoinPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengWhitelistMamipoinPhone_" + Constants.ENV);
	private String adiTengWhitelistMamipoinPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengWhitelistMamipoinPass_" + Constants.ENV);
	private String adiTengBlacklistMamipoinPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengBlacklistMamipoinPhone_" + Constants.ENV);
	private String adiTengBlacklistMamipoinPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengBlacklistMamipoinPass_" + Constants.ENV);
	private String adiTengEmptyMamipoinPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengEmptyMamipoinPhone_" + Constants.ENV);
	private String adiTengEmptyMamipoinPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengEmptyMamipoinPass_" + Constants.ENV);
	private String adiTengVoucherSuggestionPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherSuggestionPhone_" + Constants.ENV);
	private String adiTengVoucherSuggestionPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherSuggestionPass_" + Constants.ENV);
	private String adiTengVoucherGoldplusPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherGoldplusPhone_" + Constants.ENV);
	private String adiTengVoucherGoldplusPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherGoldplusPass_" + Constants.ENV);
	private String adiTengVoucherMamiroomPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherMamiroomPhone_" + Constants.ENV);
	private String adiTengVoucherMamiroomPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherMamiroomPass_" + Constants.ENV);
	private String adiTengVoucherConsultantPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherConsultantPhone_" + Constants.ENV);
	private String adiTengVoucherConsultantPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherConsultantPass_" + Constants.ENV);
	private String adiTengVoucherOwnerPhone = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherOwnerPhone_" + Constants.ENV);
	private String adiTengVoucherOwnerPass = JavaHelpers.getPropertyValue(tengTenant, "adiTengVoucherOwnerPass_" + Constants.ENV);
	private String bbmApplyVocPhoneNumber = JavaHelpers.getPropertyValue(tengTenant, "bbmApplyVocPhone_" + Constants.ENV);
	private String bbmApplyVocPhonePassword = JavaHelpers.getPropertyValue(tengTenant, "bbmApplyVocPass_" + Constants.ENV);

	//PMAN-Ownersini
	private String ownersini ="src/test/resources/testdata/ownersini/ownersiniAccount.properties";
	private String ownersiniPhoneNumber = JavaHelpers.getPropertyValue(ownersini, "ownersini_phone_number_" + Constants.ENV);
	private String ownersiniPassword = JavaHelpers.getPropertyValue(ownersini, "ownersini_password_" + Constants.ENV);

	@When("user fills out owner login as {string} and click on Enter button")
	public void user_fills_out_login_form_with_credentails_and_and_click_on_Enter_button(String type)
			throws InterruptedException {
		String phone="";
		String password="";

		switch (type) {
			case "master":
				phone = Constants.OWNER_PHONE;
				password = Constants.OWNER_PASSWORD;
				break;
			case "mars owner":
				phone = OWNER_PHONE_MARS;
				password = Constants.OWNER_PASSWORD_MARS;
				break;
			case "mars owner 2":
				phone = OWNER_PHONE_MARS_2;
				password = Constants.OWNER_PASSWORD_MARS_2;
				break;
			case "mamipay-notregistered":
				phone = mamipayOwnerLoginPhoneNotRegisteredMamipay;
				password = mamipayOwnerLoginPasswordNotRegisteredMamipay;
				break;
			case "mamipay-notregistered2":
				phone = mamipayOwnerLoginPhoneNotRegisteredMamipay2;
				password = mamipayOwnerLoginPasswordNotRegisteredMamipay2;
				break;
			case "mamipay-notregistered3":
				phone = mamipayOwnerLoginPhoneNotRegisteredMamipay3;
				password = mamipayOwnerLoginPasswordNotRegisteredMamipay3;
				break;
			case "mamipay-and-BBK-notRegistered":
				phone = mamipayOwnerLoginPhoneNotRegisteredMamipayAndBBK;
				password = mamipayOwnerLoginPasswordNotRegisteredMamipayAndBBK;
				break;
			case "mamipay-donthavekost":
				phone = mamipayOwnerLoginPhoneDontHaveKost;
				password = mamipayOwnerLoginPasswordDontHaveKost;
				break;
			case "mamipay-notactivatedbooking":
				phone = mamipayOwnerLoginPhoneNotActivatedBooking;
				password = mamipayOwnerLoginPasswordNotActivatedBooking;
				break;
			case "mamipay-BBKnotActive":
				phone = mamipayOwnerLoginPhoneBBKNotActive;
				password = mamipayOwnerLoginPasswordBBKNotActive;
				break;
			case "mamipay-allactive":
				phone = mamipayOwnerLoginPhoneAllActive;
				password = mamipayOwnerLoginPasswordAllActive;
				break;
			case "nonpremium":
				phone = nonPremiumOwnerUser;
				password = premiumPassword;
				break;
			case "premium":
				phone = premiumOwnerUser;
				password = premiumPassword;
				break;
			case "nonpremiumhavekost":
				phone = nonHaveKostOwnerUser;
				password = premiumPassword;
				break;
			case "premiumsaldo":
				phone = premiumsaldoOwnerUser;
				password = premiumPassword;
				break;
			case "premiumpayment":
				phone = premiumpaymentOwnerUser;
				password = premiumPassword;
				break;
			case "premiumnosaldo":
				phone = premiumNosaldo;
				password = premiumPassword;
				break;
			case "premiumGP":
				phone = premiumGP;
				password = premiumPassword;
				break;
			case "nonkostditolak":
				phone = nonKostDitolakOwnerUser;
				password = premiumPassword;
				break;
			case "nopropertiavail":
				phone = noPropertiAvailOwnerUser;
				password = premiumPassword;
				break;
			case "kostverification":
				phone = kostverification;
				password = premiumPassword;
				break;
			case "premiumkos0apart1":
				phone = premiumkos0apart1;
				password = premiumPassword;
				break;
			case "payment":
				phone = _ownerNumber_payment;
				password = _ownerPassword_payment;
				break;
			case "paymentBNI":
				phone = _ownerNumber_payment_BNI;
				password = _ownerPassword_payment_BNI;
				break;
			case "apply voucher":
				phone = ownerNumberApplyVoucher;
				password = ownerPasswordApplyVoucher;
				break;
			case "apply voucher mamirooms":
				phone = ownerNumberApplyVoucherMamirooms;
				password = ownerPasswordApplyVoucherMamirooms;
				break;
			case "notGP":
				phone = _noHP_ownerNotGP;
				password = _password_ownerNotGP;
				break;
			case "terminatedGP":
				phone = ownerTerminatedGPPhone;
				password = ownerTerminatedGPPass;
				break;
			case "lastDayGP":
				phone = ownerLastDayGPPhone;
				password = ownerLastDayGPPass;
				break;
			case "deleteGP":
				phone = ownerDeleteGPPhone;
				password = ownerDeleteGPPass;
				break;
			case "recurringGP":
				phone = ownerRecurringGPPhone;
				password = ownerRecurringGPPass;
				break;
			case "limo doesnt have poin":
				phone = ownerDoesntHavePoin;
				password = ownerDoesntHavePoinPass;
				break;
			case "limo not gp":
				phone = ownerLimoNotGP;
				password = ownerLimoNotGPPass;
				break;
			case "limo not gp verify":
				phone = ownerLimoNotGPverify;
				password = ownerLimoNotGPPassverify;
				break;
			case "limo blacklist owner poin":
				phone = ownerLimoBlacklist;
				password = ownerLimoBlacklistPass;
				break;
			case "limo owner mamipoin":
				phone = ownerLimoMamiPoin;
				password = ownerLimoMamiPoinPass;
				break;
			case "limo owner mamipoin dua":
				phone = ownerLimoMamiPoinDua;
				password = ownerLimoMamiPoinDuaPass;
				break;
			case "limo owner mamipoin tiga":
				phone = ownerLimoMamiPoinTiga;
				password = ownerLimoMamiPoinTigaPass;
				break;
			case "premiumnewalokasi":
				phone =premiumnewalokasi ;
				password =premiumPassword;
				break;
			case "ob booking female":
				phone = bookingOwnerPhone;
				password = bookingOwnerPhonePass;
				break;
			case "ob additional price":
				phone = additionalPriceOwnerPhone;
				password = additionalPriceOwnerPass;
				break;
			case "ob reject booking":
				phone = rejectOwnerPhone;
				password = rejectOwnerPass;
				break;
			case "owner gp 1":
				phone = phoneOwnerGp1;
				password = passwordOwnerGp1;
				break;
			case "owner gp 2":
				phone = phoneOwnerGp2;
				password = passwordOwnerGp2;
				break;
			case "owner gp 3":
				phone = phoneOwnerGp3;
				password = passwordOwnerGp3;
				break;
			case "MamiPoin New Owner":
				phone = mamipoinNewOwnerPhone;
				password = mamipoinNewOwnerPassword;
				break;
			case "MamiPoin Owner":
				phone = mamipoinOwnerPhone;
				password = mamipoinOwnerPassword;
				break;
			case "MamiPoin Owner Fixed Poin":
				phone = mamipoinOwnerFixedPoinPhone;
				password = mamipoinOwnerFixedPoinPassword;
				break;
			case "Owner Kos Review":
				phone = ownerKosReviewPhone;
				password = ownerKosReviewPassword;
				break;
			case "applyMamiPoin":
				phone = ownerApplyMamipoinPhone;
				password = ownerApplyMamipoinPassword;
				break;
			case "Owner Not Bbk":
				phone = dashboardNotBbkOwnerPhone;
				password = dashboardNotBbkOwnerPass;
				break;
			case "Owner No Listing":
				phone = noListingOwnerPhone;
				password = noListingOwnerPassword;
				break;
			case "owner mark room ob":
				phone = markRoomGP;
				password = markRoomGPPass;
				break;
			case "ob qa main":
				phone = obQAMainPhone;
				password = obQAMainPhonePass;
				break;
			case "teng manage bills":
				phone = tengManageBillsOwnerPhone;
				password = tengManageBillsOwnerPass;
				break;
			case "owner empty":
				phone = phoneNumberOwnerEmpty;
				password = passwordOwnerEmpty;
				break;
			case "owner apartment only":
				phone = mamipayOwnerLoginPhoneOnlyApart;
				password = mamipayOwnerLoginPasswordOnlyApart;
				break;
			case "teng edit bank transfer data":
				phone = tengEditBankOwnerPhone;
				password = tengEditBankOwnerPass;
				break;
			case "owner gold plus":
				phone = gpOwnerPhone;
				password = gpOwnerPass;
				break;
			case "GP Owner LG":
				phone = gpOwnerPhone2;
				password = gpOwnerPass2;
				break;
			case "teng admin invoice":
				phone = tengAdminInvoiceOwner;
				password= tengOwnerWendyPass;
				break;
			case "ob have not bbk":
				phone = obOwnerHaveNotBbkPhone;
				password= obOwnerHaveNotBbkPass;
				break;
			case "newOwnerMamiads":
				phone = mamiadsNewOwner;
				password= premiumPassword;
				break;
			case "mamiadsListingFullOccupied":
				phone = mamiadsListingFullOccupiedOwner;
				password = premiumPassword;
				break;
			case "nonMamiadsAllListingIsAllocated":
				phone = nonMamiadsAllListingIsAllocated;
				password = premiumPassword;
				break;
			case "mamiadsAllListingIsAllocated":
				phone = mamiadsAllListingIsAllocated;
				password = premiumPassword;
				break;
			case "neverPurchaseMamiadsSaldoBelow5000":
				phone = neverPurchaseMamiadsSaldoBelow5000;
				password = premiumPassword;
				break;
			case "everPurchaseMamiadsSaldoBelow5000":
				phone = everPurchaseMamiadsSaldoBelow5000;
				password = premiumPassword;
				break;
			case "neverPurchaseMamiadsHaveNotActiveAds":
				phone = neverPurchaseMamiadsHaveNotActiveAds;
				password = premiumPassword;
				break;
			case "owner bds":
				phone = obOwnerBds;
				password = obOwnerBdsPass;
				break;
			case "newPhoneOwner":
				phone = mamiadsNewPhoneOwner;
				password = premiumPassword;
				break;
			case "haveNotProperty":
			    phone = haveNotProperty;
			    password = premiumPassword;
				break;
			case "tengControlledProperty":
				phone = tengControlledProperty;
				password = tengTenantPass;
				break;
			case "saldomamiadsinsufficient":
				phone = saldomamiadsinsufficient;
				password = premiumPassword;
				break;
			case "adi TENG Owner Add Ons":
				phone = adiTengOwnerAddOnsPhone;
				password = adiTengOwnerAddOnsPass;
				break;
			case "adi TENG Owner Voucher":
				phone = adiTengOwnerVoucherPhone;
				password = adiTengOwnerVoucherPass;
				break;
			case "owner Doesnt HaveActive Kost":
				phone = ownerDoesntHaveActiveKostPhone;
				password = ownerDoesntHaveActiveKostPassword;
				break;
			case "ownersini":
				phone = ownersiniPhoneNumber;
				password = ownersiniPassword;
				break;
			default:
				throw new IllegalArgumentException("Please input a valid credentials");
		}

		loginpo.loginAsOwnerToApplication(phone,password);
	}

	@Then("I should see login pop-up")
	public void i_should_see_login_popup() {
		loginpo.checkLoginPopUp();
	}

	@And("user logs in as Tenant via Facebook credentails as {string}")
	public void userLogsInAsTenantViaFacebookCredentailsAnd(String type)
			throws InterruptedException {

		String email = "";
		String password = "";

		if (type.equalsIgnoreCase("master")) {
			email = Constants.TENANT_FACEBOOK_EMAIL;
			password = Constants.TENANT_FACEBOOK_PASSWORD;
		} else if (type.equalsIgnoreCase("search")) {
			email = tenantFacebookEmail;
			password = tenantFacebookPassword;
		} else if (type.equalsIgnoreCase("voucher")) {
			email = tenantFacebookVoucherEmail;
			password = tenantFacebookVoucherPassword;
		} else if (type.equalsIgnoreCase("nonvoucher")) {
			email = tenantFacebookNonVoucherEmail;
			password = tenantFacebookNonVoucherPassword;
		} else if (type.equalsIgnoreCase("apply voucher")) {
			email = tenantFacebookApplyVoucherEmail;
			password = tenantFacebookApplyVoucherPassword;
		}
		else if (type.equalsIgnoreCase("gender male")) {
			email = bookingMaleFacebookEmail;
			password = bookingMaleFacebookPassword;
		}
		else if (type.equalsIgnoreCase("gender female")) {
			email = bookingFemaleFacebookEmail;
			password = bookingFemaleFacebookPassword;
		}else if (type.equalsIgnoreCase("tenantPayment")) {
			email = emailTenant_;
			password = passwordTenant_;
		}
		else if (type.equalsIgnoreCase("tenantBaruDilihat")) {
			email = bookingBaruDilihatEmail;
			password = bookingBaruDilihatPassword;
		}
		else if (type.equalsIgnoreCase("emptyStateBaruDilihatSection")) {
			email = baruDilihatEmptyState;
			password = baruDilihatEmptyStatePass;
		}
		else if (type.equalsIgnoreCase("tenant blacklist mamipoin")) {
			email = tenantBlacklistMamipoinEmail;
			password = tenantBlacklistMamipoinPassword;
		}
		else if (type.equalsIgnoreCase("tenant empty mamipoin")) {
			email = tenantEmptyMamipoinEmail;
			password = tenantEmptyMamipoinPassword;
		}
		else if(type.equalsIgnoreCase("ob booking flow")) {
			email = tenantBookingFullFlowEmail;
			password = tenantBookingFullFlowPass;
		}
		else if (type.equalsIgnoreCase("tenant whitelist mamipoin")) {
			email = tenantWhitelistMamipoinEmail;
			password = tenantWhitelistMamipoinPassword;
		}
		loginpo.loginAsTenantByFacebook(email,password);
		popUp.clickOnConfirmContinueLogin();
	}

	@And("user logs in as Tenant via Gmail credentails as {string}")
	public void userLogsInAsTenantViaGmailCredentailsAnd(String type)
			throws InterruptedException {

		String email = "";
		String password = "";

		if (type.equalsIgnoreCase("platform")) {
			email = loginGmail;
			password = loginGmailPassword;
		}
		loginpo.loginAsTenantByGmail(email, password);
		popUp.clickOnConfirmContinueLogin();
	}

	@And("user logs out as a Tenant user")
	public void userLogsOutAsATenantUser() throws InterruptedException {
		header.logoutAsATenant();
	}

	@And("user logs out as a Owner user")
	public void userLogsOutAsAOwner() throws InterruptedException {
		header.logoutAsAOwner();
	}

	@When("user click first notification with message {string}")
	public void user_click_first_notification_with_message(String notificationTitle) throws InterruptedException {
		header.clickOnNotification();
		header.clickFirstNotification(notificationTitle);
	}

	@And("user clicks on Register button")
	public void user_clicks_on_Register_button() throws InterruptedException {
		loginpo.clickOnRegisterOwnerButton();
	}

	@And("user fills out owner login with invalid {string} and click on Enter button")
	public void user_fills_out_owner_login_with_invalid_and_click_on_Enter_button(String type) throws InterruptedException {
		String phone="";
		String password="";

		switch (type) {
			case "password":
				phone = invalidPasswordPhoneNumberInput;
				password = invalidPasswordPasswordInput;
				break;
		}
		loginpo.loginOwnerInvalidPassword(phone,password);

	}

	@And("user fills out tenant login with invalid {string} and click on Enter button")
	public void user_fills_out_tenant_login_with_invalid_and_click_on_Enter_button(String type) throws InterruptedException {
		String phone="";
		String password="";

		switch (type) {
			case "PF Wrong Number":
				phone = tenantPhoneNumberTA;
				password = tenantPasswordTA;
				break;
		}
		loginpo.loginAsTenantInvalid(phone,password);

	}
    @When("user login in as Tenant via phone number as {string}")
    public void user_login_in_as_tenant_via_phone_number_as(String type) throws InterruptedException {
        String number = "";
        String password = "";

        if (type.equalsIgnoreCase("DC Automation")) {
            number = tenantPhoneNumberBX;
            password = tenantPhonePasswordBX;
        } else if (type.equalsIgnoreCase("PMAN Tenant")){
        	number = tenantCRMPhoneNumber;
        	password = tenantCRMPhonePassword;
		} else if (type.equalsIgnoreCase("BBM apply voucher1")){
			number = bbmApplyVocPhoneNumber;
			password = bbmApplyVocPhonePassword;
        } else if (type.equalsIgnoreCase("mars tenant")){
		    number = TENANT_PHONE_MARS;
		    password = TENANT_PASSWORD_MARS;
		} else if (type.equalsIgnoreCase("UG Tenant")) {
        	number=tenantPhoneNumberUG;
        	password=tenantPasswordUG;
		}
		else if (type.equalsIgnoreCase("UG Tenant2")) {
			number=tenantPhoneNumberUG;
			password=tenantPasswordUG2;
		}
		else if (type.equalsIgnoreCase("UG Tenant3")) {
			number=tenantPhoneNumberUG2;
			password=tenantPasswordUG;
		}
		else if (type.equalsIgnoreCase("TA Automation")) {
			number=tenantPhoneNumberTA;
			password=tenantPasswordTA;
		}
		else if (type.equalsIgnoreCase("DOM Automation")) {
			number=tenantPhoneNumberDOM;
			password=tenantPasswordDOM;
		}
		else if(type.equalsIgnoreCase("ob additional price")) {
			number= additionalPriceTenantPhone;
			password= additionalPriceTenantPass;
		}
		else if (type.equalsIgnoreCase("OB Cancel Tenant")) {
			number=tenantCancel;
			password=tenantCancelPass;
		}
		else if (type.equalsIgnoreCase("TA Verification")) {
			number = tenantVerifPhoneNumberTA;
			password = tenantVerifPasswordTA;
		}
		else if (type.equalsIgnoreCase("OB Baru Dilihat")) {
			number = baruDilihatPhone;
			password = baruDilihatPass;
		}
		else if (type.equalsIgnoreCase("OB Different Gender Male")){
			number = differentGenderMalePhone;
			password = differentGenderMalePhonePass;
		}
		else if (type.equalsIgnoreCase("OB Different Gender Female")){
			number = differentGenderFemalePhone;
			password = differentGenderFemalePhonePass;
		}
		else if(type.equalsIgnoreCase("TA Wrong Number")){
			number = tenantPhoneNumberTA;
			password = tenantPasswordTA;
		}
		else if(type.equalsIgnoreCase("teng booking")) {
			number = tenantTengBookingPhone;
			password = tenantTengBookingPhonePass;
		}
		else if(type.equalsIgnoreCase("teng invoice settle")) {
			number = tenantTengInvoiceSettle;
			password = tengTenantPass;
		}
		else if(type.equalsIgnoreCase("TENG voucher")) {
			number = tenantTengVoucherKuPhone;
			password = tenantTengVoucherKuPass;
		}
		else if(type.equalsIgnoreCase("teng admin invoice")) {
			number = tengAdminInvoicePhone;
			password = tengTenantPass;
		}
		else if(type.equalsIgnoreCase("teng recurring invoice")) {
			number = tengRecurringInvoice;
			password = tengTenantPass;
		}
		else if(type.equalsIgnoreCase("teng settlement invoice")) {
			number = tengSettlementInvoice;
			password = tengTenantPass;
		}
		else if(type.equalsIgnoreCase("ob tenant bss")) {
			number = obTenantBssPhone;
			password = obTenantBssPass;
		}
		else if(type.equalsIgnoreCase("teng invoice detail")) {
			number = tengInvoiceDetail;
			password = tengTenantPass;
		}
		else if (type.equalsIgnoreCase("OB tenant empty state")){
			number = obTenantEmptyState;
			password = obTenantEmptyStatePass;
		}
		else if (type.equalsIgnoreCase("OB tenant job validation")){
			number = obTenantJobValidation;
			password = obTenantJobValidationPass;
		}
		else if (type.equalsIgnoreCase("OB tenant karyawan validation")){
			number = obTenantKaryawanValidation;
			password = obTenantKaryawanValidationPass;
		}
		else if (type.equalsIgnoreCase("OB tenant mahasiswa validation")){
			number = obTenantMhsValidation;
			password = obTenantMhsValidationPass;
		}
		else if (type.equalsIgnoreCase("OB tenant lainnnya validation")){
			number = obTenantLainnyaValidation;
			password = obTenantLainnyaValidationPass;
		}else if (type.equalsIgnoreCase("MA tenant never see detail property")){
			number = mamiadsTenantNeverSeeDetailProperty;
			password = premiumPassword;
		}else if (type.equalsIgnoreCase("MA tenant no rekomendasi")){
			number = mamiadsTenantNoRekomendasi;
			password = premiumPassword;
		}
		else if (type.equalsIgnoreCase("OB contract sect dbet")){
			number = obTenantContractDbet;
			password = obTenantContractDbetPass;
		}
		else if (type.equalsIgnoreCase("ob kost saya homepage")){
			number = obTenantKostSayaHomepage;
			password = obTenantKostSayaHomepagePass;
		}
		else if (type.equalsIgnoreCase("ob kos saya homepage waiting confirm")){
			number = obTenantKosSayaWaitingConfirm;
			password = obTenantKosSayaWaitingConfirmPass;
		}
		else if (type.equalsIgnoreCase("ob kos saya homepage draft booking probut")){
			number = obTenantKosSayaDraftBookingProbut;
			password = obTenantKosSayaDraftBookingProbutPass;
		}
		else if (type.equalsIgnoreCase("ob kos saya homepage waiting confirm probut")){
			number = obTenantKosSayaWaitingConfirmProbut;
			password = obTenantKosSayaWaitingConfirmProbutPass;
		}
		else if(type.equalsIgnoreCase("adi TENG kost review submitted")) {
			number = adiTengKostReviewSubmittedPhone;
			password = adiTengKostReviewSubmittedPass;
		}
		else if (type.equalsIgnoreCase("tenant mamiads")) {
            number = tenantMamiAds;
            password = premiumPassword;
        }
		else if (type.equalsIgnoreCase("ob kost saya rejected")){
			number = obTenantKostSayaRejected;
			password = obTenantKostSayaRejectedPass;
		}
		else if (type.equalsIgnoreCase("ob tenant status")){
			number = obTenantStatus;
			password = obTenantStatusPass;
		}
		else if (type.equalsIgnoreCase("ob owner reject booking")){
			number = obOwnerRejectBooking;
			password = obOwnerRejectBookingPass;
		}
		else if (type.equalsIgnoreCase("adi TENG kost review not submitted")){
			number = adiTengKostReviewNotSubmittedPhone;
			password = adiTengKostReviewNotSubmittedPass;
		}
		else if (type.equalsIgnoreCase("adi TENG apply voucher")){
			number = adiTengApplyVoucherPhone;
			password = adiTengApplyVoucherPass;
		}
		else if (type.equalsIgnoreCase("ob tenant input unique code")){
			number = obTenantUniqueCode;
			password = obTenantUniqueCodePass;
		}
		else if (type.equalsIgnoreCase("voucher base on user")) {
			number = tengVoucherBaseOnUserPhone;
			password = tengVoucherBaseOnUserPass;
		}
		else if (type.equalsIgnoreCase("ob tenant notification")){
			number = obTenantNotification;
			password = obTenantNotificationPass;
		}
		else if (type.equalsIgnoreCase("PF batalkan kontrak")){
			number = tenantCancelContractPF;
			password = tenantCancelContractPassPF;
		}
		else if (type.equalsIgnoreCase("ob tenant full pay kos saya")){
			number = obTenantFullPayKosSaya;
			password = obTenantFullPayKosSayaPass;
		}
		else if (type.equalsIgnoreCase("ob kost saya confirm")){
			number = obTenantKostSayaConfirm;
			password = obTenantKostSayaConfirmPass;
		}
		else if (type.equalsIgnoreCase("teng no have mamipoin")) {
			number = noTenantNoHaveMamipoin;
			password = passwordTenantNoHaveMamipoin;
		}
		else if (type.equalsIgnoreCase("ob tenant dpst kos saya")){
			number = obTenantDpStKosSaya;
			password = obTenantDpStKosSayaPass;
		}
		else if (type.equalsIgnoreCase("teng have mamipoin")) {
			number = noTenantHaveMamipoin;
			password = passwordTenantNoHaveMamipoin;
		}
		else if (type.equalsIgnoreCase("ob tenant chat bth respon pngajuan sewa label")){
			number = obChatBthResponPngajuanSewaLbl;
			password = obChatBthResponPngajuanSewaLblPass;
		}
		else if (type.equalsIgnoreCase("adi TENG apply voucher dua")){
			number = adiTengApplyVoucherDuaPhone;
			password = adiTengApplyVoucherDuaPass;
		}
		else if (type.equalsIgnoreCase("adi TENG Tenant Add Ons")){
			number = adiTengAddOnsPhone;
			password = adiTengAddOnsPass;
		}
		else if (type.equalsIgnoreCase("ob reject and waiting")){
			number = obRejectWaitingPhone;
			password = obRejectWaitingPhonePass;
		}
		else if (type.equalsIgnoreCase("adi TENG whitelist mamipoin")){
			number = adiTengWhitelistMamipoinPhone;
			password = adiTengWhitelistMamipoinPass;
		}
		else if (type.equalsIgnoreCase("adi TENG blacklist mamipoin")){
			number = adiTengBlacklistMamipoinPhone;
			password = adiTengBlacklistMamipoinPass;
		}
		else if (type.equalsIgnoreCase("adi TENG empty mamipoin")){
			number = adiTengEmptyMamipoinPhone;
			password = adiTengEmptyMamipoinPass;
		}
		else if (type.equalsIgnoreCase("adi TENG voucher suggestion")){
			number = adiTengVoucherSuggestionPhone;
			password = adiTengVoucherSuggestionPass;
		}
		else if (type.equalsIgnoreCase("adi TENG voucher goldplus")){
			number = adiTengVoucherGoldplusPhone;
			password = adiTengVoucherGoldplusPass;
		}
		else if (type.equalsIgnoreCase("adi TENG voucher mamiroom")) {
			number = adiTengVoucherMamiroomPhone;
			password = adiTengVoucherMamiroomPass;
		}
		else if (type.equalsIgnoreCase("adi TENG voucher consultant")) {
			number = adiTengVoucherConsultantPhone;
			password = adiTengVoucherConsultantPass;
		}
		else if (type.equalsIgnoreCase("adi TENG voucher owner")) {
			number = adiTengVoucherOwnerPhone;
			password = adiTengVoucherOwnerPass;
		}
		else if (type.equalsIgnoreCase("ob owner reject full")) {
			number = obOwnerRejectFullPhone;
			password = obOwnerRejectFullPass;
		}
		else if (type.equalsIgnoreCase("bbm kost refund singgahsini")) {
			number = bbmSinggahSiniPhone;
			password = bbmSinggahSiniPass;
		}
		else if (type.equalsIgnoreCase("bbm accept from chat")) {
			number = bbmAcceptChatPhone;
			password = bbmAcceptChatPass;
		}
		else if (type.equalsIgnoreCase("bbm jobs lainnya")) {
			number = bbmJobsLainnyaPhone;
			password = bbmJobsLainnyaPass;
		}
		loginpo.loginAsTenantToApplication(number,password);
    }

	@And("user click login with owner and click forgot password button")
	public void user_click_login_with_owner_and_click_forgot_password_button() throws InterruptedException {
		loginpo.clickOnForgotPasswordButton();
	}

	@And("user click login with tenant and click forgot password button")
	public void user_click_login_with_tenant_and_click_forgot_password_button() throws InterruptedException {
		loginpo.clickOnTenantForgotPasswordButton();
	}

	@And("user clicks on Register as Tenant button")
	public void user_clicks_on_Register_as_Tenant_button() throws InterruptedException {
		loginpo.clickOnRegisterTenantButton();
	}

	@And("user login in as invalid Tenant via phone number as {string}")
	public void user_login_in_as_invalid_Tenant_via_phone_number_as(String type) throws InterruptedException {
		String number = "";
		String password = "";

		if(type.equalsIgnoreCase("TA No Fill Password")){
			number = tenantPhoneNumberTA;
			password = "qwerty123";
		}
		loginpo.loginTenantInvalidPassword(number, password);
	}

	@Then("user verify login error messages {string}")
	public void user_verify_login_error_messages(String error) {
		Assert.assertEquals(loginpo.getLoginErrorMessagesText(error), error, "Login error messages is not equal to " + error);
	}

    @Then("user verify pop up {string} {string}")
    public void user_verify_pop_up(String title, String subtitle) {
		Assert.assertEquals(loginpo.getLoginTitlePopUpText(title), title, "Pop up login title is not equal to " + title);
		Assert.assertEquals(loginpo.getLoginSubtitleText(subtitle), subtitle, "Pop up login subtitle is not equal to " + subtitle);
    }

    @When("user click close on pop up login")
    public void user_click_close_on_pop_up_login() throws InterruptedException {
		loginpo.clickCloseOnPopUpLogin();
    }

	@Then("user verify pop up {string} {string} are not appeared")
	public void user_verify_pop_up_are_not_appeared(String title, String subtitle) {
		Assert.assertFalse(loginpo.isPopupTitleTextAppeared(title), "Pop up title text is appear");
		Assert.assertFalse(loginpo.isPopupSubtitleTextAppeared(subtitle), "Pop up subtitle text is appear");
	}

	@And("user click button pemilik kost")
	public void user_click_button_pemilik_kost()throws InterruptedException{
		loginpo.popUpOwnerLogin();
	}

	@And("user verify login form owner")
	public void user_verify_login_form_owner() throws InterruptedException{
		loginpo.verifyOwnerLoginForm();
	}
	@And("user click back button in login owner")
	public void user_click_back_button_in_login_owner() throws InterruptedException{
		loginpo.clickBackOnPopUpLogin();
	}

	@And("user click button close login form")
	public void user_click_button_close_login_form() throws InterruptedException{
		loginpo.clickCloseOnPopUpLogin();
	}

	@Then("user verify login form close")
	public void user_verify_login_form_close() {
		Assert.assertFalse(loginpo.isPopUpNotShowing(), "Popup still showing showing");
	}

	@Then("user click on owner popup")
	public void user_click_on_owner_popup() throws InterruptedException{
		popUp.clickBackAttentionPopUp();
		popUp.clickNantiSajaOnGpPopUp();
		// Click the new 'Saya Mengerti' pop up if appear
		popUp.clickOnIUnderstandOnSetAdditionalCostsPopUp();
		popUp.clickOnUpdateAllKosButton();
	}

	@When("user login as Admin Bigflip via credentials")
	public void userLoginAsAdminBigflipViaCredentials() throws InterruptedException {
		String ID = IDBigflip;
		String email = emailBigflip;
		String password = passwordBigflip;
		loginpo.enterCredentialsAndClickOnLoginButtonBigflip(ID,email,password);
	}
}