package steps.mamikos.mamipoinTenant;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.voucherku.TenantInvoicePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class ApplyMamipoinSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private TenantInvoicePO invoice = new TenantInvoicePO(driver);

    //Test Data Apply Mamipoin
    private String mamipoin = "src/test/resources/testdata/mamikos/mamipoinOwner.properties";
    private String remainingPaymentBeforeUseMamipoin = JavaHelpers.getPropertyValue(mamipoin, "remainingPaymentBeforeUseMamipoin_" + Constants.ENV);
    private String remainingPaymentAfterUseMamipoin = JavaHelpers.getPropertyValue(mamipoin, "remainingPaymentAfterUseMamipoin_" + Constants.ENV);

    @When("system display remaining payment {string} use mamipoin for payment {string}")
    public void system_display_remaining_payment_use_mamipoin_for_payment(String condition, String paymentPeriod) throws InterruptedException {
        String remainingPaymentBefore = "";
        String remainingPaymentAfter = "";

        switch (paymentPeriod) {
            case "monthly":
                remainingPaymentBefore = remainingPaymentBeforeUseMamipoin;
                remainingPaymentAfter = remainingPaymentAfterUseMamipoin;
                break;
        }
        if(condition.equals("before")){
            Assert.assertEquals(invoice.getRemainingPayment(), remainingPaymentBefore, "Remaining payment before doesn't match");
        }
        else {
            Assert.assertEquals(invoice.getRemainingPayment(), remainingPaymentAfter, "Remaining payment after doesn't match");
        }
    }

    @When("user clicks on mamipoin toggle button to ON")
    public void user_clicks_on_mamipoin_toggle_button_to_on() throws InterruptedException {
        invoice.clickMamipoinToggleButtonToOn();
    }

    @When("user clicks on mamipoin toggle button to OFF")
    public void user_clicks_on_mamipoin_toggle_button_to_off() throws InterruptedException {
        invoice.clickMamipoinToggleButtonToOff();
    }

    @Then("tenant point estimate not displayed on invoice")
    public void tenant_point_estimate_not_displayed_on_invoice() throws InterruptedException {
        Assert.assertFalse(invoice.isPointEstimateTenantVisible());
    }
}
