package steps.mamikos.voucherku;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.voucherku.VoucherkuPO;
import utilities.ThreadManager;

import java.util.List;

public class VoucherkuSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private VoucherkuPO voucherku = new VoucherkuPO(driver);

    @And("user click Profile dropdown")
    public void
    user_click_profile_dropdown() throws InterruptedException
    {
        voucherku.clickProfilDropdown();
    }

    @And ("user clicks Voucherku menu")
    public void
    user_click_voucherku_menu() throws InterruptedException
    {
        voucherku.clickVoucherkuMenu();
    }

    @And ("user clicks Voucher Card from the list")
    public void
    user_click_voucher_card() throws InterruptedException
    {
        voucherku.clickVoucherCard();
    }

    @Then ("user see Tersedia Empty State Landing Page")
    public void
    user_see_tersedia_empty_state_landing_page() throws InterruptedException
    {
        voucherku.isTersediaEmptyStateVisible();
    }

    @And ("user click Terpakai tab")
    public void
    user_click_terpakai_tab() throws InterruptedException
    {
        voucherku.clickTerpakaiTab();
    }

    @Then ("user see Terpakai Empty State Landing Page")
    public void
    user_see_terpakai_empty_state_landing_page() throws InterruptedException
    {
        voucherku.isTerpakaiEmptyStateVisible();
    }

    @And ("user click Kedaluwarsa tab")
    public void
    user_click_kedaluwarsa_tab() throws InterruptedException
    {
        voucherku.clickKedaluwarsaTab();
    }

    @Then ("user see Kedaluwarsa Empty State Landing Page")
    public void
    user_see_kedaluwarsa_empty_state_landing_page() throws InterruptedException
    {
       voucherku.isKedaluwarsaEmptyStateVisible();
    }

    @And("user see voucher detail Image banner")
    public void
    user_see_voucher_detail_image_banner() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherDetailImageBannerVisible(), "Image Banner is not present");

    }

    @And("user see voucher detail Campaign Title")
    public void
    user_see_voucher_detail_campaign_title() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherDetailCampaignTitleVisible(), "Campaign Title is not present");

    }

    @And("user see voucher detail Expired Date")
    public void
    user_see_voucher_detail_expired_date() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherDetailExpiredDateVisible(), "Expired Date is not present");
    }

    @And("user see voucher detail Syarat dan Ketentuan label")
    public void
    user_see_voucher_detail_syarat_dan_ketentuan_label() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherDetailSyaratDanKetentuanLabelVisible(), "Syarat dan Ketentuan Label is not present");
    }

    @And("user see voucher detail Syarat dan Ketentuan description")
    public void
    user_see_voucher_detail_syarat_dan_ketentuan_description() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherDetailSyaratDanKetentuanDescriptionVisible(), "Syarat dan Ketentuan Description is not present");
    }

    @And("user see voucher detail Kode Voucher label")
    public void
    user_see_voucher_detail_kode_voucher_label() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherDetailKodeVoucherLabelVisible(), "Kode Voucher Label is not present");
    }

    @And("user see voucher detail Voucher code")
    public void
    user_see_voucher_detail_voucher_code() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherDetailVoucherCodeVisible(), "Kode Voucher Code is not present");
    }

    @And("user see voucher detail Ticket icon")
    public void
    user_see_voucher_detail_ticket_icon() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherDetailTicketIconVisible(), "Ticket Icon is not present");
    }

    @And("user see voucher detail Salin button")
    public void
    user_see_voucher_detail_salin_button() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherDetailSalinButtonVisible(), "Salin button is not present");
    }

    @And("user clicks on Promo Lainnya button")
    public void user_clicks_on_promo_lainnya_button() throws InterruptedException {
        voucherku.clickPromoLainnyaButton();
    }

    @Then("user verify on {string}")
    public void user_verify_on(String url) {
        Assert.assertEquals(voucherku.getUrlcurrentwindows(), url, "Wrong URL");
    }

    @And("user see red voucher counter")
    public void user_see_red_voucher_counter() throws InterruptedException
    {
        voucherku.isRedVoucherCounterVisible();
    }

    @And("user click Salin button from voucher list")
    public void user_click_salin_button_from_voucher_list() throws InterruptedException
    {
        voucherku.clickVoucherListSalinButton();
    }

    @And("user see kode berhasil disalin toast in voucher list")
    public void user_see_kode_berhasil_disalin_toast_in_voucher_list() throws InterruptedException
    {
        voucherku.isVoucherListCopyToastVisible();
    }

    @And("user click Salin button from voucher detail")
    public void user_click_salin_button_from_voucher_detail() throws InterruptedException
    {
       voucherku.clickVoucherDetailSalinButton();
    }

    @And("user see kode berhasil disalin toast in voucher detail")
    public void user_see_kode_berhasil_disalin_toast_in_voucher_detail() throws InterruptedException
    {
        voucherku.isVoucherDetailCopyToastVisible();
    }

    @And("user see disabled Salin button in voucher list")
    public void user_see_disabled_salin_button_in_voucher_list() throws InterruptedException
    {
        voucherku.isVoucherListSalinButtonDisabledVisible();
    }

    @And("user see disabled Salin button in voucher detail")
    public void user_see_disabled_salin_button_in_voucher_detail() throws InterruptedException
    {
        voucherku.isVoucherDetailSalinButtonDisabledVisible();
    }

    @And ("user clicks Voucher Card in Terpakai tab")
    public void
    user_clicks_voucher_card_in_terpakai_tab() throws InterruptedException
    {
        voucherku.clickTerpakaiVoucherCard();
    }

    @And ("user clicks Voucher Card in Kedaluwarsa tab")
    public void
    user_clicks_voucher_card_in_kedaluwarsa_tab() throws InterruptedException
    {
        voucherku.clickKedaluwarsaVoucherCard();
    }

    @And("user see voucher list header")
    public void
    user_see_voucher_list_header() throws InterruptedException
    {
        voucherku.isVoucherListHeaderVisible();
    }

    @And("user see Tersedia tab")
    public void user_see_tersedia_tab() throws InterruptedException
    {
        voucherku.isVoucherTersediaTabVisible();
    }

    @And("user see Terpakai tab")
    public void user_see_terpakai_tab() throws InterruptedException
    {
        voucherku.isVoucherTerpakaiTabVisible();
    }

    @And("user see Kedaluwarsa tab")
    public void user_see_kedaluwarsa_tab() throws InterruptedException
    {
        voucherku.isVoucherKedaluwarsaTabVisible();
    }

    @And("user see Promo Lainnya")
    public void user_see_promo_lainnya() throws InterruptedException
    {
        voucherku.isVoucherPromoLainnyaVisible();
    }

    @And("user see Lihat button")
    public void user_see_lihat_button() throws InterruptedException
    {
        voucherku.isVoucherPromoLihatButtonVisible();
    }

    @And("user see voucher card")
    public void user_see_voucher_card() throws InterruptedException
    {
        voucherku.isVoucherCardVisible();
    }

    @And("user see voucher list image")
    public void user_see_voucher_list_image() throws InterruptedException
    {
        voucherku.isVoucherListImageVisible();
    }

    @And("user see voucher list Expired Date")
    public void user_see_voucher_list_expired_date() throws InterruptedException
    {
        voucherku.isVoucherListExpiredDateVisible();
    }

    @And("user see voucher list Kode Voucher label")
    public void user_see_voucher_list_kode_voucher_label() throws InterruptedException
    {
        voucherku.isVoucherListKodeVoucherLabelVisible();
    }

    @And("user see voucher list Voucher code")
    public void user_see_voucher_list_voucher_code() throws InterruptedException
    {
        voucherku.isVoucherListVoucherCodeVisible();
    }

    @And("user see voucher list Salin button")
    public void user_see_voucher_list_salin_button() throws InterruptedException
    {
        voucherku.isVoucherListSalinButtonVisible();
    }

    @And("user see voucher list Terpakai label")
    public void user_see_voucher_list_terpakai_label() throws InterruptedException
    {
        voucherku.isVoucherListTerpakaiLabelVisible();
    }

    @And("user see voucher list Kedaluwarsa label")
    public void user_see_voucher_list_kedaluwarsa_label() throws InterruptedException
    {
        voucherku.isVoucherListKedaluwarsaLabelVisible();
    }

    @And("user see voucher list disabled Kode Voucher label")
    public void user_see_voucher_list_disabled_kode_voucher_label() throws InterruptedException
    {
        voucherku.isVoucherListDisabledKodeVoucherLabelVisible();
    }

    @And("user see voucher list disabled Voucher code")
    public void user_see_voucher_list_disabled_voucher_code() throws InterruptedException
    {
        voucherku.isVoucherListDisabledVoucherCodeVisible();
    }

    @And("user see voucher list disabled Salin button")
    public void user_see_voucher_list_disabled_salin_button() throws InterruptedException
    {
        voucherku.isVoucherListSalinButtonDisabledVisible();
    }

    @Then("user see voucher detail Terpakai Label")
    public void user_see_voucher_detail_Terpakai_Label() throws InterruptedException {
        voucherku.isVoucherDetailTerpakaiLabelVisible();
    }

    @Then("user see voucher detail Kedaluwarsa Label")
    public void user_see_voucher_detail_Kedaluwarsa_Label() throws InterruptedException {
        voucherku.isVoucherDetailKedaluwarsaLabelVisible();
    }

    @Then("user see voucher detail disabled Kode Voucher label")
    public void user_see_voucher_detail_disabled_Kode_Voucher_label() throws InterruptedException {
        voucherku.isVoucherDetailDisabledKodeVoucherLabelVisible();
    }

    @Then("user see voucher detail disabled Voucher code")
    public void user_see_voucher_detail_disabled_Voucher_code() throws InterruptedException {
        voucherku.isVoucherDetailDisabledVoucherCodeVisible();
    }

    @And("user scroll up {string} voucher list")
    public void user_scroll_up_voucher_list(String tabName) {
        if(tabName.equalsIgnoreCase("Tersedia")){
            voucherku.scrollUpAvailableVoucherLists();
        }
        else if(tabName.equalsIgnoreCase("Terpakai")){
            voucherku.scrollUpUsedVoucherLists();
        }
        else if(tabName.equalsIgnoreCase("Kedaluwarsa")){
            voucherku.scrollUpExpiredVoucherLists();
        }
    }

    @And("user scroll down {string} voucher list")
    public void user_scroll_down_voucher_list(String tabName) {
        if(tabName.equalsIgnoreCase("Tersedia")){
            voucherku.scrollDownAvailableVoucherLists();
        }
        else if(tabName.equalsIgnoreCase("Terpakai")){
            voucherku.scrollDownUsedVoucherLists();
        }
        else if(tabName.equalsIgnoreCase("Kedaluwarsa")){
            voucherku.scrollDownExpiredVoucherLists();
        }
    }

    @And("user scroll down voucher detail")
    public void user_scroll_down_voucher_detail() throws InterruptedException {
        voucherku.scrollDownVoucherDetail();
    }

    @And("user scroll up voucher detail")
    public void user_scroll_up_voucher_detail() throws InterruptedException {
        voucherku.scrollUpVoucherDetail();
    }

    @Then("user see expired date voucher {string}")
    public void user_see_expired_date_voucher(String ExpiredDate) {
        Assert.assertEquals(voucherku.getVoucherExpiredDate(), ExpiredDate,"Expired date is wrong");
    }

    @Then("user verify voucher is displayed")
    public void user_verify_voucher_is_displayed(List<String> voucher) throws InterruptedException {
        for (String s : voucher) {
            voucherku.scrollToFindTargetVoucher(s);
            Assert.assertTrue(voucherku.isVoucherTargetedPresent(s), voucher + "is not displayed!");
        }
    }

    @Then("user verify voucher is not displayed")
    public void user_verify_voucher_is_not_displayed(List<String> voucher) {
        for (String s : voucher) {
            Assert.assertFalse(voucherku.isVoucherTargetedPresent(s), voucher + "is displayed!");
        }
    }

    @And ("user clicks on Partner tab")
    public void
    user_clicks_on_Partner_tab() throws InterruptedException
    {
        voucherku.clickPartnerTabButton();
    }

    @And ("user see S&K Berlaku text")
    public void user_see_Syarat_dan_Ketentuan_Berlaku_text() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isSnKBerlakuTextVisible(), "S&K Berlaku text is not present");
    }

    @And ("user click on Voucher Partner Card from the list")
    public void user_click_on_Voucher_Partner_Card_from_the_list() throws InterruptedException
    {
        voucherku.clickOnVoucherPartnerCard();
    }

    @And("user see voucher partner detail Image banner")
    public void
    user_see_voucher_partner_detail_image_banner() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherPartnerDetailImageBannerVisible(), "Image Banner is not present");

    }

    @And("user see voucher partner detail Campaign Title")
    public void
    user_see_voucher_partner_detail_campaign_title() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherPartnerDetailCampaignTitleVisible(), "Campaign Title is not present");

    }

    @And("user see voucher partner detail Expired Date")
    public void
    user_see_voucher_partner_detail_expired_date() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherPartnerDetailExpiredDateVisible(), "Expired Date is not present");
    }

    @And("user see voucher partner detail Syarat dan Ketentuan label")
    public void
    user_see_voucher_partner_detail_syarat_dan_ketentuan_label() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherPartnerDetailSyaratDanKetentuanLabelVisible(), "Syarat dan Ketentuan Label is not present");
    }

    @And("user see voucher partner detail Syarat dan Ketentuan description")
    public void
    user_see_voucher_partner_detail_syarat_dan_ketentuan_description() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherPartnerDetailSyaratDanKetentuanDescriptionVisible(), "Syarat dan Ketentuan Description is not present");
    }

    @And("user see voucher partner detail Kode Voucher label")
    public void
    user_see_voucher_partner_detail_kode_voucher_label() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherPartnerDetailKodeVoucherLabelVisible(), "Kode Voucher Label is not present");
    }

    @And("user see voucher partner detail Voucher code")
    public void
    user_see_voucher_partner_detail_voucher_code() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherPartnerDetailVoucherCodeVisible(), "Kode Voucher Code is not present");
    }

    @And("user see voucher partner detail Ticket icon")
    public void
    user_see_voucher_partner_detail_ticket_icon() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherPartnerDetailTicketIconVisible(), "Ticket Icon is not present");
    }

    @And("user see voucher partner detail Salin button")
    public void
    user_see_voucher_partner_detail_salin_button() throws InterruptedException
    {
        Assert.assertTrue(voucherku.isVoucherPartnerDetailSalinButtonVisible(), "Salin button is not present");
    }
    @And("user click Salin button from voucher partner detail")
    public void user_click_salin_button_from_voucher_partner_detail() throws InterruptedException
    {
        voucherku.clickVoucherPartnerDetailSalinButton();
    }

    @And("user see kode berhasil disalin toast in voucher partner detail")
    public void user_see_kode_berhasil_disalin_toast_in_voucher_partner_detail() throws InterruptedException
    {
        voucherku.isVoucherPartnerDetailCopyToastVisible();
    }

    @When("user clicks voucher {string} on voucher list")
    public void user_clicks_voucher_on_vouher_list(String voucherName) throws InterruptedException {
        voucherku.clickVoucherName(voucherName);
    }
}