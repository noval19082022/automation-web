package steps.mamikos.voucherku;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.tenant.payment.PaymentMethodPO;
import pageobjects.mamikos.voucherku.TenantInvoicePO;
import pageobjects.mamipay.CommonPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class ApplyVoucherSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private PaymentMethodPO payment = new PaymentMethodPO(driver);
    private TenantInvoicePO invoice = new TenantInvoicePO(driver);
    private CommonPO common = new CommonPO(driver);

    //Test Data Apply Voucher
    private String voucher = "src/test/resources/testdata/mamikos/voucherku.properties";
    private String remainingPaymentBeforeUseVoucherMonthly = JavaHelpers.getPropertyValue(voucher, "remainingPaymentBeforeUseVoucherMonthly_" + Constants.ENV);
    private String remainingPaymentBeforeUseVoucherMonthlyWithDP = JavaHelpers.getPropertyValue(voucher, "remainingPaymentBeforeUseVoucherMonthlyWithDP_" + Constants.ENV);
    private String remainingPaymentAfterUseVoucherMonthly = JavaHelpers.getPropertyValue(voucher, "remainingPaymentAfterUseVoucherMonthly_" + Constants.ENV);
    private String remainingPaymentAfterUseVoucherMonthlyWithDP = JavaHelpers.getPropertyValue(voucher, "remainingPaymentAfterUseVoucherMonthlyWithDP_" + Constants.ENV);
    private String remainingPaymentAfterUseVoucherMonthlyMamichecker = JavaHelpers.getPropertyValue(voucher, "remainingPaymentAfterUseVoucherMonthlyMamichecker_" + Constants.ENV);
    private String remainingPaymentBeforeUseVoucher3Monthly = JavaHelpers.getPropertyValue(voucher, "remainingPaymentBeforeUseVoucher3Monthly_" + Constants.ENV);
    private String remainingPaymentAfterUseVoucher3Monthly = JavaHelpers.getPropertyValue(voucher, "remainingPaymentAfterUseVoucher3Monthly_" + Constants.ENV);
    private String remainingPaymentBeforeUseVoucherMonthlySettlement = JavaHelpers.getPropertyValue(voucher, "remainingPaymentBeforeUseVoucherMonthlySettlement_" + Constants.ENV);
    private String remainingPaymentAfterUseVoucherMonthlySettlement = JavaHelpers.getPropertyValue(voucher, "remainingPaymentAfterUseVoucherMonthlySettlement_" + Constants.ENV);
    private String remainingPaymentBeforeUseVoucherMonthlyGP = JavaHelpers.getPropertyValue(voucher, "remainingPaymentBeforeUseVoucherMonthlyGP_" + Constants.ENV);
    private String remainingPaymentAfterUseVoucherMonthlyGP = JavaHelpers.getPropertyValue(voucher, "remainingPaymentAfterUseVoucherMonthlyGP_" + Constants.ENV);

    @When("system display remaining payment {string} use voucher for payment {string}")
    public void system_display_remaining_payment_use_voucher_for_payment(String condition, String paymentPeriod) throws InterruptedException {
        String remainingPaymentBefore = "";
        String remainingPaymentAfter = "";

        switch (paymentPeriod) {
            case "monthly":
                remainingPaymentBefore = remainingPaymentBeforeUseVoucherMonthly;
                remainingPaymentAfter = remainingPaymentAfterUseVoucherMonthly;
                break;
            case "monthlyWithDP":
                remainingPaymentBefore = remainingPaymentBeforeUseVoucherMonthlyWithDP;
                remainingPaymentAfter = remainingPaymentAfterUseVoucherMonthlyWithDP;
                break;
            case "3 monthly":
                remainingPaymentBefore = remainingPaymentBeforeUseVoucher3Monthly;
                remainingPaymentAfter = remainingPaymentAfterUseVoucher3Monthly;
                break;
            case "monthly-mamichecker":
                remainingPaymentBefore = remainingPaymentBeforeUseVoucherMonthly;
                remainingPaymentAfter = remainingPaymentAfterUseVoucherMonthlyMamichecker;
                break;
            case "monthlySettlement":
                remainingPaymentBefore = remainingPaymentBeforeUseVoucherMonthlySettlement;
                remainingPaymentAfter = remainingPaymentAfterUseVoucherMonthlySettlement;
                break;
            case "monthlyGP":
                remainingPaymentBefore = remainingPaymentBeforeUseVoucherMonthlyGP;
                remainingPaymentAfter = remainingPaymentAfterUseVoucherMonthlyGP;
                break;
        }
        if(condition.equals("before")){
            Assert.assertEquals(invoice.getRemainingPayment(), remainingPaymentBefore, "Remaining payment before doesn't match");
        }
        else {
            Assert.assertEquals(invoice.getRemainingPayment(), remainingPaymentAfter, "Remaining payment after doesn't match");
        }
    }

    @When("user select payment method {string}")
    public void user_select_payment_method(String paymentMethod) throws InterruptedException {
        payment.selectPaymentMethod(paymentMethod);
    }

    @When("user access voucher form")
    public void user_access_voucher_form() throws InterruptedException {
        invoice.accessVoucherForm();
    }

    @When("input {string} to field voucher code")
    public void input_to_field_voucher_code(String voucherCode) {
        invoice.enterVoucherCode(voucherCode);
    }

    @When("user click use button")
    public void user_click_use_button() throws InterruptedException {
        invoice.clickOnUseButton();
    }

    @When("user remove voucher")
    public void user_remove_voucher() throws InterruptedException {
        invoice.clickOnRemoveVoucherButton();
    }

    @Then("system display voucher alert message {string}")
    public void system_display_voucher_alert_message(String message) throws InterruptedException {
        Assert.assertTrue(invoice.getVoucherAllertMessage().contains(message), "Message doesn't match");
        common.ClickOnClosePopupButton();
    }

    @And("user click Bayar Sekarang button")
    public void user_click_Bayar_Sekarang_button() throws InterruptedException {
        invoice.clickBayarSekarangButton();
    }

    @And("user click Bayar Pelunasan Sekarang button")
    public void user_click_Bayar_Pelunasan_Sekarang_button() throws InterruptedException {
        invoice.clickBayarPelunasanSekarangButton();
    }

    @And("voucher applied successfully")
    public void voucher_applied_successfully() {
        Assert.assertTrue(invoice.verifyVoucherAppliedSuccessfully());
    }

    @Then("voucher removed successfully")
    public void voucherRemovedSuccessfully() {
        Assert.assertTrue(invoice.verifyVoucherRemovedSuccesfully());
    }

    @Then("system display icon voucher invalid")
    public void system_display_icon_voucher_invalid() {
        Assert.assertTrue(invoice.voucherInvalidIconAppeared(), "Voucher valid");
    }

    @When("user click delete voucher")
    public void user_click_delete_voucher() throws InterruptedException {
        invoice.clickOnDeleteVoucherButton();
    }

    @When("tenant clicks Masukkan to go to voucher selection")
    public void tenant_clicks_Masukkan_to_go_to_voucher_selection() throws InterruptedException {
        invoice.clicksOnMasukkanButton();
    }

    @When("tenant clicks Lihat Detail {string} index voucher list")
    public void tenant_clicks_Lihat_Detail_index_voucher_list(String index) throws InterruptedException {
        invoice.clicksOnLihatDetailIndex(index);
    }

    @Then("tenant can sees voucher detail section")
    public void tenant_can_sees_voucher_detail_section() {
        Assert.assertTrue(invoice.isVoucherDetailsVisible());
    }

    @When("tenant clicks Pakai {string} index voucher list")
    public void tenant_clicks_Pakai_index_voucher_list(String index) throws InterruptedException {
        invoice.clicksOnPakaiButtonIndex(index);
    }

    @Then("tenant can sees voucher status is green tick")
    public void tenant_can_sees_voucher_status_is_green_tick() {
        Assert.assertTrue(invoice.isVoucherUsageTickVisible());
    }

    @Then("tenant can see voucher suggestion empty state")
    public void tenant_can_see_voucher_suggestion_empty_state() {
        Assert.assertTrue(invoice.isVoucherSuggestionEmptyStateVisible());
    }

    @Then("tenant can sees voucher eligible contains {string}")
    public void tenant_can_sees_voucher_eligible_contains(String text) {
        Assert.assertTrue(invoice.getVoucherEligibleText().contains(text));
    }

    @Then("tenant can sees voucher eligible is greater or equal than {string}")
    public void tenant_can_sees_voucher_eligible_is_greater_or_equal_than(String voucherAmount) {
        Assert.assertTrue(JavaHelpers.extractNumber(invoice.getVoucherEligibleText()) >= Integer.parseInt(voucherAmount));
    }

    @When("user access voucher form for repayment")
    public void user_access_voucher_form_for_repayment() throws InterruptedException {
        invoice.accessVoucherFormForRepayment();
    }

    @When("tenant clicks Pakai button on voucher detail")
    public void tenant_clicks_Pakai_button_on_voucher_detail() throws InterruptedException {
        invoice.clickOnPakaiButtonOnVoucherDetail();
    }
}
