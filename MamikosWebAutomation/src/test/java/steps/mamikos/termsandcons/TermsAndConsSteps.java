package steps.mamikos.termsandcons;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.FooterPO;
import pageobjects.mamikos.termsandcons.TermsAndConditionsPO;
import utilities.ThreadManager;

import java.util.List;

public class TermsAndConsSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private TermsAndConditionsPO terms = new TermsAndConditionsPO(driver);
    private FooterPO footer = new FooterPO(driver);

    @And("user clicks on Terms And Conditions")
    public void userClicksOnTermsAndConditions() throws  InterruptedException {
        footer.clickOnTermAndConditions();
    }

    @Then("user verify each of Term And Conditions")
    public void userVerifyEachOfTermAndConditions(DataTable termsCons) throws InterruptedException {
        List<List<String>> termList = termsCons.asLists(String.class);
        for(int i=1 ; i < termList.size() ; i++){
            terms.clickOnTermsAndConditionsSidebar(termList.get(0).get(i));
            for (int j = i+1 ; j < termList.size() ; j++) {
                Assert.assertTrue(terms.verifyTermsAndConditionsChilds(termList.get(j).get(i)), "Terms and conditions sub menu not appear in dropdown");
            }
        }
    }

}
