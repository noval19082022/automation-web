package steps.mamikos.booking;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.owner.DashboardOwnerPO;
import pageobjects.mamikos.owner.booking.BookingConfirmContractPO;
import pageobjects.mamikos.owner.booking.BookingConfirmRoomPreferencePO;
import pageobjects.mamikos.owner.booking.ManageBookingPO;
import pageobjects.mamikos.owner.booking.OwnerBookingDetailsPO;
import pageobjects.mamikos.owner.common.LeftMenuPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import static org.testng.Assert.*;


public class OwnerBookingDetailsSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private LeftMenuPO leftMenu = new LeftMenuPO(driver);
    private ManageBookingPO bookingManage = new ManageBookingPO(driver);
    private OwnerBookingDetailsPO ownerBookingDetails = new OwnerBookingDetailsPO(driver);
    private BookingConfirmRoomPreferencePO pref = new BookingConfirmRoomPreferencePO(driver);
    private BookingConfirmContractPO contract = new BookingConfirmContractPO(driver);
    private ManageBookingPO manage = new ManageBookingPO(driver);
    private DashboardOwnerPO dashboardOwner = new DashboardOwnerPO(driver);
    private HeaderPO header = new HeaderPO(driver);
    private JavaHelpers java = new JavaHelpers();

    //Test Data OB
    private String propertyFile1="src/test/resources/testdata/mamikos/OB.properties";
    private String kostName = JavaHelpers.getPropertyValue(propertyFile1,"kostNameBookingFemale_" + Constants.ENV);
    private String kostPrice = JavaHelpers.getPropertyValue(propertyFile1,"kostNameBookingFemalePriceMonthly_" + Constants.ENV);
    private String duration = JavaHelpers.getPropertyValue(propertyFile1,"duration_" + Constants.ENV);
    private String phone = JavaHelpers.getPropertyValue(propertyFile1,"phone_" + Constants.ENV);
    private String status = JavaHelpers.getPropertyValue(propertyFile1,"status_" + Constants.ENV);
    private String totalDPWeekly = JavaHelpers.getPropertyValue(propertyFile1,"totalDP_weekly_" + Constants.ENV);
    private String totalPelunasan = JavaHelpers.getPropertyValue(propertyFile1,"totalPelunasan_weekly_" + Constants.ENV);

    //Test Data Payment
    private String payment="src/test/resources/testdata/mamikos/payment.properties";
    private String nameTenant_ = JavaHelpers.getPropertyValue(payment,"nameTenant_" + Constants.ENV);
    private String depositFee_ = JavaHelpers.getPropertyValue(payment,"depositFee_" + Constants.ENV);
    private String penaltyFee_ = JavaHelpers.getPropertyValue(payment,"penaltyFee_" + Constants.ENV);
    private String additionalCosts_ = JavaHelpers.getPropertyValue(payment,"additionalCosts_" + Constants.ENV);
    private String additionalCostsLabel_ = JavaHelpers.getPropertyValue(payment,"additionalCostsLabel_" + Constants.ENV);
    private String downPayment_ = JavaHelpers.getPropertyValue(payment,"downPayment_" + Constants.ENV);

    //Test Data Voucherku
    private String voucher="src/test/resources/testdata/mamikos/voucherku.properties";
    private String downPaymentApplyVoucher_ = JavaHelpers.getPropertyValue(voucher,"downPaymentApplyVoucher_" + Constants.ENV);

    @And("user navigates to Booking Request page")
    public void userNavigatesToBookingRequestPage() throws InterruptedException {
        int maxLoop = 0;
        do {
            leftMenu.clickOnBookingRequestButton();
            if (maxLoop == 1) {
                break;
            }
            maxLoop++;
        } while (!bookingManage.isInOwnerBookingProposal());
        bookingManage.loadingHandler();
    }

    @When("user navigates to booking request page")
    public void user_navigates_to_booking_request_page() throws InterruptedException {
        leftMenu.clickOnBookingAndBillingMenu();
        bookingManage.clickOnBookingRequestOption();
    }

    @When("user navigates to booking request and filter booking need confirmation")
    public void user_navigates_to_booking_request_and_filter_booking_need_confirmation() throws InterruptedException {
        int maxLoop = 0;
        leftMenu.clicOnKostManagementMenu();
        do {
            if (maxLoop == 1){
                break;
            }
            leftMenu.clickOnBookingRequestButton();
            maxLoop++;
        }
        while (!bookingManage.isInOwnerBookingProposal());
        bookingManage.loadingHandler();
       // bookingManage.dismissInstantBookingPopUp();
        bookingManage.clickOnNeedConfirmationFilter();
        if (bookingManage.clickOnNoRequestBooking()){
            bookingManage.clickOnAllFilter();
            bookingManage.clickOnNeedConfirmationFilter();
        }
    }

    @When("user navigates to manage bill page")
    public void user_navigates_to_manage_bill_page() throws InterruptedException {
        leftMenu.clickOnBookingAndBillingMenu();
        selenium.hardWait(5);
    }

    @Then("booking is displayed with Booking ID starting with {string} , Room name , Room price  , Duration  , {string}  , Phone, Status  on Booking details page")
    public void bookingIsDisplayedWithBookingIDStartingWithRoomNameRoomPriceDurationNamePhoneStatusOnBookingDetailsPage(String bookingIDAlias, String name) {
        String tenantName = null;
        if(name.equalsIgnoreCase("master")){
            tenantName = Constants.TENANT_FACEBOOK_NAME;
        }
        else if (name.equalsIgnoreCase("male gender name")) {
            tenantName = JavaHelpers.getPropertyValue(propertyFile1, "tenant_facebook_name_gender_male_" + Constants.ENV);
        }
        else if (name.equalsIgnoreCase("female gender name")) {
            tenantName = JavaHelpers.getPropertyValue(propertyFile1, "tenant_booking_full_flow_name_" + Constants.ENV);
        }
        assertEquals(ownerBookingDetails.getRoomTitle(), kostName,"Room name doesn't match");
        assertEquals(ownerBookingDetails.getRoomPrice(), "Rp" + kostPrice + "/Bulan","Room price doesn't match");
        assertEquals(ownerBookingDetails.getRentDuration(), duration,"Rent Duration doesn't match");
        assertEquals(ownerBookingDetails.getTenantName(), tenantName,"Tenant name doesn't match");
        assertEquals(ownerBookingDetails.getTenantNumber(), phone,"Tenant phone doesn't match");
        assertEquals(ownerBookingDetails.getBookingStatus(), status,"Status doesn't match");
    }

    @When("user clicks accept button on {string} booking")
    public void user_clicks_accept_button_on_booking(String bookingSquad) throws InterruptedException {
        int numberOfList = manage.getNumberListOfBooking();
        assertNotEquals(0, numberOfList, "Number list of booking need approval is " + numberOfList);

        boolean found = false;
        for (int i = 1; i <= numberOfList; i++) {
            if (bookingSquad.equals("payment") && manage.getTenantName(i).equals(nameTenant_)){
                manage.clickOnAcceptButton(i);
                found = true;
                break;
            }
        }
        assertTrue(found, "Data not found");
    }

    @When("user clicks on Accept button")
    public void userClicksOnAcceptButton() throws InterruptedException {
        ownerBookingDetails.clickOnAcceptButton();
        ownerBookingDetails.waitTillLoadingAnimationDisappear();
    }

    @When("select first room available and clicks on next button")
    public void select_first_room_available_and_clicks_on_next_button() throws InterruptedException {
        pref.selectFirstRoomAvailable();
//        if (pref.selectFirstRoomNoAvailable()); {
//            pref.selectSecondRoomAvailable();
//        }
        pref.clickOnContinueButton();
    }

    @And("user click save button")
    public void user_click_save_button() throws InterruptedException {
        contract.clickOnSaveButton();
    }

    @Then("user will redirect to pengajuan booking page")
    public void user_will_redirect_to_pengajuan_booking_page() throws InterruptedException{
        contract.clickOnOKButton();
        Assert.assertEquals(contract.getPengajuanBookingPage(), contract.getPengajuanBookingPage(), "You are not in Pengajuan Booking page");
    }

    @And("user enters deposite panalty info as deposite fee {string} , panalty fee {string} , panalty fee duration {string} , panalty fee duration type {string} and click on Save")
    public void userEntersDepositePanaltyInfoAsDepositeFeePanaltyFeePanaltyFeeDurationPanaltyFeeDurationTypeAndClickOnSave(String depositeFeeText, String panaltyFeeText, String penaltyFeeDurationText, String penaltyFeeDurationTypeText) throws InterruptedException {
        contract.enterDepositePanaltyInfo(depositeFeeText, panaltyFeeText, penaltyFeeDurationText, penaltyFeeDurationTypeText);
        contract.clickOnSaveButton();
//        contract.clickOnOkButtonRenterAddedPopUp();
        bookingManage.loadingHandler();
    }

    @When("user enters deposit fee, penalty fee, additional costs, down payment, and click on save button")
    public void user_enters_deposit_fee_penalty_fee_additional_costs_down_payment_and_click_on_save_button() throws InterruptedException {
        contract.enterDepositFee(depositFee_);
        contract.enterPenaltyFee(penaltyFee_);
        contract.enterAdditionalCost(additionalCostsLabel_, additionalCosts_);
        contract.clickOnSaveButton();
    }

    @When("user enters down payment and click on save button")
    public void user_enters_down_payment_and_click_on_save_button() throws InterruptedException {
        contract.enterDownPayment(downPaymentApplyVoucher_);
        selenium.hardWait(2);
        contract.clickOnConfirmDownPayment();
        contract.clickOnSaveButton();
    }

    @When("user enters deposit fee, additional costs, and click on save button")
    public void user_enters_deposit_fee_additional_costs_and_click_on_save_button() throws InterruptedException {
        contract.enterDepositFee(depositFee_);
        contract.enterAdditionalCost(additionalCostsLabel_, additionalCosts_);
        contract.clickOnSaveButton();
    }

    @And("owner user navigates to owner page")
    public void ownerUserNavigatesToOwnerPage() throws InterruptedException {
        if(selenium.getWindowHandles().size() > 1) {
            selenium.switchToWindow(1);
        }else {
            leftMenu.clickOnHomeOwnerLeftMenu();
        }
    }

    @Then("user select room number {string} and clicks on next button")
    public void user_select_room_number_and_clicks_on_next_button(String roomNumber) throws InterruptedException {
        pref.selectRoomNumber(roomNumber);
        pref.clickOnContinueButton();
    }

    @And("user select kost {string} on booking request page")
    public void user_select_kost_on_booking_request_page(String kostName) throws InterruptedException {
        bookingManage.selectKostFromFilter(kostName);
    }

    @When("user navigate to status booking langsung page")
    public void user_navigates_to_status_booking_langsung_page() throws InterruptedException {
        leftMenu.clickOnStatusBookingLangsungMenu();
        selenium.hardWait(2);
    }

    @When("user navigate to active kos")
    public void user_navigates_to_active_kos() throws InterruptedException {
        bookingManage.clickOnActiveTab();
        selenium.hardWait(3);
    }

    @When("user click tab {string} and see contract")
    public void user_click_tab_and_see_contract(String tab) throws InterruptedException {
        if(tab.equals("Terbayar")){
            bookingManage.clickOnTerbayarTab();
        }
    }

    @And("user clicks Tolak button on booking list")
    public void user_clicks_tolak_button_on_booking_list() throws InterruptedException {
        ownerBookingDetails.clickOnTolakButton();
    }

    @And("user clicks Ya, Tolak button on tolak pop up confirmation")
    public void user_clicks_ya_tolak_button_on_tolak_pop_up_confirmation() throws InterruptedException {
        ownerBookingDetails.clickOnYaTolakButton();
    }

    @And("user choose Lainnya reason and enter text {string}")
    public void user_choose_lainnya_reason_and_enter_text(String lainnya) throws InterruptedException {
        ownerBookingDetails.clickOnLainnya(lainnya);
    }

    @And("user clicks T&C and clicks Tolak button")
    public void user_clicks_tc_and_click_tolak_button() throws InterruptedException {
        ownerBookingDetails.clickTermAndConditionTolak();
        ownerBookingDetails.clickTolakBookingOnTolakPage();
    }

    @And("user clicks Anda Tolak filter and clicks Lihat Detail button")
    public void user_clicks_anda_tolak_filter_and_clicks_lihat_detail_button() throws InterruptedException {
        ownerBookingDetails.clickAndaTolakFilter();
        ownerBookingDetails.clickLihatDetailOnAndaTolak();
    }

    @Then("user check rejection reason {string}")
    public void user_check_rejection_reason(String rejectLainnya){
        Assert.assertEquals(ownerBookingDetails.getRejectionReason(rejectLainnya), rejectLainnya, "Reason doesn't match");
    }

    @Then("owner can sees deposit is {string}")
    public void owner_can_sees_deposit_is(String depositPrice) {
        Assert.assertEquals(contract.getDepositPrice(), depositPrice, "Deposit price is not equal to " + depositPrice);
    }

    @Then("owner can sees lateness fine is {string} with price is {string}")
    public void owner_can_sees_lateness_fine_is_with_price_is(String lateFineDuration, String lateFine) {
        Assert.assertEquals(contract.getLatenessFinePrice(), lateFine, "Lateness fine is not equal to " + lateFine);
        Assert.assertEquals(contract.getLatenessFineDuration(), lateFineDuration, "Lateness fine duration is not equal to " + lateFineDuration);
    }

    @Then("owner can sees other price {string} {string}")
    public void owner_can_sees_additional_price(String otherPriceName, String otherPrice) {
        Assert.assertEquals(contract.getOtherPriceName(), otherPriceName);
        Assert.assertEquals(contract.getOtherPricePrice(), otherPrice);
    }

    @Then("owner redirected to booking manage page")
    public void owner_redirected_to_booking_manage_page() {
        Assert.assertTrue(manage.isInOwnerBookingProposal());
    }

    @Then("user can see booking detail and appears {string}, {string}, {string} and user validates the value")
    public void user_Can_See_Booking_Detail_And_Appears(String title, String mulaiSewaa, String durasiSewa) throws InterruptedException{
        Assert.assertEquals(ownerBookingDetails.getTitleWaktuPemesanan(), title, "Title doesn't match");
        Assert.assertEquals(ownerBookingDetails.getTitleMulaiSewa(), mulaiSewaa, "Title Mulai Sewa doesn't match");
        Assert.assertEquals(ownerBookingDetails.getTitleDurasiSewa(), durasiSewa, "Title durasi Sewa doesn't match");
        Assert.assertEquals(String.valueOf(java.extractNumber(ownerBookingDetails.getTotalDetail(2))), totalDPWeekly, "Total amount is " + ownerBookingDetails.getTotalDetail(0));
        Assert.assertEquals(String.valueOf(java.extractNumber(ownerBookingDetails.getTotalDetail(3))), totalPelunasan, "Total amount is " + ownerBookingDetails.getTotalDetail(1));
    }

    @Then("user not see {string} on detail tagihan page")
    public void user_not_see_down_payment(String text) throws InterruptedException {
        Assert.assertFalse(ownerBookingDetails.isDownPaymentNotDisplayed(text));
    }

    @And("user clicks on Accept button from chat room")
    public void user_clicks_on_accept_button_from_chat_room() throws InterruptedException{
        ownerBookingDetails.clickOnAcceptButtonFromChatRoom();
        ownerBookingDetails.waitTillLoadingAnimationDisappear();
    }

    @Then("system display title {string} after accept booking from chat room")
    public void system_display_title_after_accept_booking_from_chat_room(String title) throws InterruptedException {
        Assert.assertEquals(ownerBookingDetails.getTitleBookingStatus(),title,"Title doesn't match");
    }

    @And("admin clicks on detail button on chat room")
    public void admin_clicks_on_detail_button_on_chat_room() throws InterruptedException{
        ownerBookingDetails.clickOnDEtailButtonFromChatRoom();
    }

    @Then("owner see status booking is {string}")
    public void owner_see_status_booking_is(String status) {
        Assert.assertEquals(ownerBookingDetails.getBookingStatus(),status,"Status doesn't match");
    }

    @And("Owner can see name of Tenant is {string}")
    public void owner_can_see_name_of_tenant_is(String tenantName) {
        Assert.assertEquals(ownerBookingDetails.getTenantName(),tenantName,"Tenant name doesn't match");
    }

    @And("Owner can see Kost name, harga kos, sisa kamar")
    public void owner_can_see_kost_name_harga_kos_sisa_kamar() {
        Assert.assertTrue(ownerBookingDetails.isKostNameDisplayed());
        Assert.assertTrue(ownerBookingDetails.isPriceKostDisplayed());
        Assert.assertTrue(ownerBookingDetails.isSisaKamarDisplayed());
    }

    @And("Owner can see button ubah kamar")
    public void owner_can_see_button_ubah_kamar() {
        Assert.assertTrue(ownerBookingDetails.isUbahKamarButtonDisplayed());
    }

    @And("owner see uang muka on detail payment")
    public void owner_see_uang_muka_on_detail_payment() {
        Assert.assertTrue(ownerBookingDetails.isDownPaymentLabelDisplayed());
    }
}