package steps.mamikos.booking;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang.ArrayUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.booking.ManageBookingPO;
import pageobjects.mamikos.tenant.booking.*;
import pageobjects.mamikos.tenant.profile.BookingHistoryPO;
import pageobjects.mamikos.tenant.profile.DraftBookingPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.List;

import static org.testng.Assert.*;

public class TenantKostBookingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private KosDetailPO kosDetailPO = new KosDetailPO(driver);
    private BookingFormFillPO bookingForm = new BookingFormFillPO(driver);
    private DraftBookingPO draftBooking = new DraftBookingPO(driver);
    private SuccessBookingPO successBooking = new SuccessBookingPO(driver);
    private JavaHelpers java = new JavaHelpers();
    private BookingConfirmationPO bookingConfirmation = new BookingConfirmationPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private ManageBookingPO manageBooking = new ManageBookingPO(driver);
    private ContactKostPO contactKost = new ContactKostPO(driver);
    private BookingHistoryPO history = new BookingHistoryPO(driver);

    //Test DatabookingConfirmation
    private String propertyFile1="src/test/resources/testdata/mamikos/OB.properties";
    private String propertyFile2="src/test/resources/testdata/mamikos/booking.properties";
    private String obOwnerFile ="src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
    private String kostName = JavaHelpers.getPropertyValue(propertyFile1,"kostNameBookingFemale_" + Constants.ENV);
    private String kostName2 = JavaHelpers.getPropertyValue(propertyFile1,"kostNameOB_" + Constants.ENV);
    private String kostPrice = JavaHelpers.getPropertyValue(propertyFile1,"kostNameBookingFemalePriceMonthly_" + Constants.ENV);
    private String kostPrice2 = JavaHelpers.getPropertyValue(propertyFile1,"kostPriceOB_" + Constants.ENV);
    private String duration = JavaHelpers.getPropertyValue(propertyFile1,"duration_" + Constants.ENV);
    private String gender = JavaHelpers.getPropertyValue(propertyFile1,"gender_" + Constants.ENV);
    private String phone = JavaHelpers.getPropertyValue(propertyFile1,"phone_" + Constants.ENV);
    private String needConfirmationKostName = JavaHelpers.getPropertyValue(obOwnerFile, "wendyWildRitKostName_" + Constants.ENV);
    private String kostPriceWeekly = JavaHelpers.getPropertyValue(propertyFile1,"kostPrice_weekly_" + Constants.ENV);
    private String totalDPWeekly = JavaHelpers.getPropertyValue(propertyFile1,"totalDP_weekly_" + Constants.ENV);
    private String adminFee = JavaHelpers.getPropertyValue(propertyFile1,"adminFee_" + Constants.ENV);
    private String totalBayarPertama = JavaHelpers.getPropertyValue(propertyFile1,"totalBayarPertama_" + Constants.ENV);
    private String totalBayarPertamaPelunasan = JavaHelpers.getPropertyValue(propertyFile1,"totalBayarPertamaPelunasan_" + Constants.ENV);
    private String totalPelunasan = JavaHelpers.getPropertyValue(propertyFile1,"totalPelunasan_weekly_" + Constants.ENV);
    private String totalBayarWeekly = JavaHelpers.getPropertyValue(propertyFile1,"totalPembayaran_weekly_" + Constants.ENV);
    private String totalAmount = JavaHelpers.getPropertyValue(propertyFile1,"totalAmount_" + Constants.ENV);
    private String kostPrice3Bulan = JavaHelpers.getPropertyValue(propertyFile1,"kostPrice_3Bulan_" + Constants.ENV);
    private String totalBayarPertama3Bulan  = JavaHelpers.getPropertyValue(propertyFile1,"totalBayarPertama_3Bulan_" + Constants.ENV);
    private String totalPelunasan3Bulan = JavaHelpers.getPropertyValue(propertyFile1,"totalPelunasan_3Bulan_" + Constants.ENV);
    private String totalDP3Bulan = JavaHelpers.getPropertyValue(propertyFile1,"totalDP_3Bulan_" + Constants.ENV);
    private String totalDP3BulanOnPopUp = JavaHelpers.getPropertyValue(propertyFile1,"totalDP_3BulanOnPopup_" + Constants.ENV);
    private String totalPelunasan3BulanOnPopUp = JavaHelpers.getPropertyValue(propertyFile1,"totalPelunasanOnPopUp_3Bulan_" + Constants.ENV);
    private String kostPricePerTahun = JavaHelpers.getPropertyValue(propertyFile1,"kostPrice_PerTahunan_" + Constants.ENV);
    private String totalBayarPerTahun = JavaHelpers.getPropertyValue(propertyFile1,"totalPembayaran_PerTahun_" + Constants.ENV);
    private String kostPricePerBulan = JavaHelpers.getPropertyValue(propertyFile1,"kostPrice_PerBulan_" + Constants.ENV);
    private String totalBayarPertamaPerBulan  = JavaHelpers.getPropertyValue(propertyFile1,"totalBayarPertama_PerBulan_" + Constants.ENV);
    private String totalPelunasanPerBulan = JavaHelpers.getPropertyValue(propertyFile1,"totalPelunasan_PerBulan_" + Constants.ENV);
    private String totalDPPerBulan = JavaHelpers.getPropertyValue(propertyFile1,"totalDP_PerBulan_" + Constants.ENV);
    private String totalDPPerBulanOnPopUp = JavaHelpers.getPropertyValue(propertyFile1,"totalDP_PerBulanOnPopup_" + Constants.ENV);
    private String totalPelunasanPerBulanOnPopUp = JavaHelpers.getPropertyValue(propertyFile1,"totalPelunasanOnPopUp_PerBulan_" + Constants.ENV);

    //Test Data
    private String consultant="src/test/resources/testdata/consultant/consultant.properties";
    private String propertyHyperlink_ = JavaHelpers.getPropertyValue(consultant,"propertyHyperlink_" + Constants.ENV);

    @And("user clicks on Booking button on Kost details page")
    public void user_clicks_on_booking_button_on_kost_details_page() throws InterruptedException, ParseException {
        String tomorrow = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.selectDateForStartBoarding(tomorrow);
        kosDetailPO.selectRentType("Per bulan");
        kosDetailPO.clickOnBookingButton();
    }

    @When("user clicks Booking button kost details page and booking for today date")
    public void user_clicks_on_booking_button_on_kost_details_booking_today() throws InterruptedException, ParseException {
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.selectDateForStartBoarding("today");
        kosDetailPO.selectRentType("Per bulan");
        kosDetailPO.clickOnBookingButton();
    }

    @When("user choose booking date for tomorrow and proceed to booking form")
    public void user_choose_booking_date_for_tommorrow_and_proced_to_booking_form() throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.chooseTomorrowDateOnKostDetail();
        kosDetailPO.choosePerMonthRadio();
        kosDetailPO.clickOnBookingButton();
    }

    @When("user choose booking date for tomorrow")
    public void user_choose_booking_date_for_tommorrow() throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.chooseTomorrowDateOnKostDetail();
        kosDetailPO.choosePerMonthRadio();
    }

    @When("user select payment period {string}")
    public void user_select_payment_period(String paymentPeriod) throws InterruptedException {
        bookingForm.selectPaymentPeriod(paymentPeriod);
    }

    @And("user choose payment with full payment")
    public void user_choose_payment_with_full_payment () throws  InterruptedException {
        bookingForm.chooseFullPayment ();
    }

    @When("user input boarding start date is tomorrow")
    public void user_input_boarding_start_date_is_tomorrow() throws ParseException, InterruptedException {
        String tomorrowDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        bookingForm.selectDate(tomorrowDate);
    }

    @When("user input boarding start date is today")
    public void user_input_boarding_start_date_is_today() throws ParseException, InterruptedException {
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        bookingForm.selectDateForToday(today);
    }

    @When("user input rent duration equals to {int}")
    public void user_input_rent_duration_equals_to(int duration) throws InterruptedException {
        for (int i = 1; i < duration; i++) {
            bookingForm.increaseRateDuration();
        }
    }

    @When("user/I/tenant fills out Rent Duration equals to 4 Bulan, and clicks on Next button")
    public void xFillsOutRentDurationEqualsTo4BulanAndClicksOnNextButton() throws InterruptedException, ParseException {
        for (int i = 1; i < 4; i++) {
            bookingForm.increaseRateDuration();
        }
        bookingForm.clickOnNextButton();
    }

    @Then("booking confirmation screen displayed with  Kost name ,Room price , Duration  ,Tenant Name  , Gender  , Phone")
    public void bookingConfirmationScreenDisplayedWithKostNameRoomPriceDurationTenantNameGenderPhoneJobTitle() throws InterruptedException {
        assertEquals(bookingConfirmation.getRoomName(), kostName, "Room Name doesn't match");
        assertEquals(bookingConfirmation.getRoomPrice(), "Rp" + kostPrice, "Room Price doesn't match");
        assertEquals(bookingForm.getRentDuration(), duration, "Rent duration doesn't match");
//        Assert.assertEquals(bookingConformation.getTenantName(), Constants.TENANT_FACEBOOK_NAME,"Tenant name doesn't match"); //DOM doesn't give this detail so, need to talk with dev team
        assertTrue(bookingConfirmation.isTenantGenderSelected(gender), "Tenant gender doesn't match");
        //Assert.assertEquals(bookingConformation.getTenantMobile(), phone,"Tenant mobile doesn't match"); //DOM doesn't give this detail so, need to talk with dev team
    }

    @Then("booking confirmation screen displayed with Kost name, Room price for cancel")
    public void bookingConfirmationScreenDisplayedWithKostNameRoomPricePhoneJobTitle(){
        assertEquals(bookingConfirmation.getRoomName(), kostName2, "Room Name doesn't match");
        assertEquals(bookingConfirmation.getRoomPrice(), "Rp" + kostPrice2, "Room Price doesn't match");
    }

    @And("user selects T&C checkbox and clicks on Book button")
    public void userSelectsTCCheckboxAndClicksOnBookButton() throws InterruptedException {
        selenium.hardWait(2);
        int i;
        for (i=0; i<=10; i++){
            selenium.hardWait(2);
            if (bookingConfirmation.isSubmitButtonVisible()){   //check if the submit button (ajukan sewa in form booking) is visible
                bookingConfirmation.clickOnSubmitButton();
                if (bookingConfirmation.isTnCcheckBoxVisible()){    //check if the TnC button is visible
                    bookingConfirmation.clickOnTermsAndConditionCheckBox();
                    bookingConfirmation.clickOnSubmitToOwner();
                    break;
                } else {
                    selenium.refreshPage();
                    selenium.hardWait(10);
                    kosDetailPO.selectBookingDateToday();
                    kosDetailPO.selectRentType("Per bulan");
                    kosDetailPO.clickOnBookingButton();
                }
                break;
            } else {
                selenium.refreshPage();
                selenium.hardWait(10);
                kosDetailPO.selectBookingDateToday();
                kosDetailPO.selectRentType("Per bulan");
                kosDetailPO.clickOnBookingButton();
            }
        }
      //  bookingConfirmation.waitUntilButtonChatSuccessBookingAppear();
    }

    @Then("system display successfully booking")
    public void system_display_successfully_booking() throws InterruptedException {
        bookingConfirmation.bookingSuccessfullyIsPresent();

    }

    @Then("user navigates to main page after booking")
    public void userNavigatesToMainPageAfterBooking() throws InterruptedException {
        bookingConfirmation.isSubmissionStatusButtonPresent();
        bookingConfirmation.clickOnMamikosLogo();
    }

    @When("user click on chat pemilik kos")
    public void user_click_on_chat_pemilik_kos() throws InterruptedException {
        bookingConfirmation.clickOnChatPemilikKosBtn();
    }

    @Then("tenant can see pop-up booking cancel confirmation should appear")
    public void then_pop_up_booking_cancel_should_appear() throws InterruptedException {
        assertTrue(bookingForm.isBookingConfirmationPopUpPresent(), "Booking cancel confirmation is not present");
        assertEquals(bookingForm.getTitleBookingCancelConfirmationText(), "Yakin batalkan Booking?", "Booking cancel confirmation is not " + bookingForm.getTitleBookingCancelConfirmationText());
    }

    @When("tenant click on Save Draft button")
    public void tenant_should_reached_kos_detail_page() throws InterruptedException{
        bookingForm.clickOnSaveDraftButton();
    }

    @And("tenant click on arrow back button on booking confirmation")
    public void tenant_click_on_arrow_back_button_on_booking_confirmation() throws InterruptedException {
        bookingForm.clickOnArrowBackButton();
    }

    @When("tenant click on Still submit button")
    public void tenant_click_on_still_submit_button() throws InterruptedException {
        bookingForm.clickOnStillSubmitButton();
    }

    @Then("tenant can see booking form page")
    public void tenant_can_see_booking_confirmation_page() throws InterruptedException{
        selenium.hardWait(1);
        assertEquals(bookingForm.getDecidePriceAndRentDurationText(), "Tentukan harga dan masa sewa yang Anda butuhkan", "Text is not " + bookingForm.getDecidePriceAndRentDurationText());
        assertEquals(bookingForm.getChooseBaseOnRentDurationText(), "Pilih sesuai kebutuhan dan lamanya Anda akan menetap", "Text is not " + bookingForm.getChooseBaseOnRentDurationText());
        assertTrue(bookingForm.isDurationFormPresent(), "Duration form is not present");
        assertTrue(bookingForm.isBookingSummaryPresent(), "Booking summary is not present");
    }

    @Then("tenant can see Per Month is selected")
    public void tenant_can_see_per_month_is_selected() {
        assertEquals(bookingForm.getSelectedPeriodText(), "Per Bulan", "Selected rent duration is not " + bookingForm.getSelectedPeriodText());
    }

    @Then("tenant should not see daily rent duration")
    public void tenant_should_not_see_daily_rent_duration() {
        int numberOfRentDuration = bookingForm.getListRentDurationNumber();
        for (int i = 1; i <= numberOfRentDuration; i++) {
            assertFalse(bookingForm.isDailyPresent(i), "Daily rent duration is present");
        }
    }

    @Then("^tenant can see rent price list$")
    public void tenant_can_see_rent_price_list(List<String> list) {
        int numberOfRentDuration = bookingForm.getListRentDurationNumber();
        for (int i = 0; i < numberOfRentDuration; i++) {
            assertEquals(bookingForm.getListPriceRentDurationIndexText(i + 1), list.get(i), "Price list rent duration is not " + list.get(i));
        }
    }

    @Then("^tenant can click on rent price count available and can see additional information$")
    public void tenant_can_click_on_rent_price_count_available_and_can_see_additional_information(List<String> list) throws InterruptedException {
        for (int i = 0; i < list.size(); i++) {
            selenium.hardWait(1);
            bookingForm.clickOnRentPriceListIndex(i + 1);
            assertEquals(bookingForm.getSelectedRentDurationAdditionalInformationText(), list.get(i), "Additional info is not " + list.get(i));
        }
    }

    @Then("^tenant can see duration updated according to selected rent count$")
    public void tenant_can_see_duration_update_according_to_selected_rent_count(List<String> list) throws InterruptedException {
        for (int i = 0; i < list.size(); i++) {
            bookingForm.clickOnRentPriceListIndex(i + 1);
            assertEquals(bookingForm.getRentDuration(), list.get(i), "Duration is not " + list.get(i));
            assertEquals(bookingForm.getPreviewRentDurationText(), list.get(i), "Preview duration is not " + list.get(i));
        }
    }

    @Then("tenant can see price is equal in booking overview")
    public void tenant_can_see_price_is_equal_in_booking_overview() throws InterruptedException{
        int paidDurationTime = bookingForm.getListRentDurationNumber();
        for(int i = 0; i < paidDurationTime; i++) {
            int selectedPrice = bookingForm.getSelectedPerPeriodPrice();
            int bookingSummaryPrice = bookingForm.getBookingSummaryPerPeriodPrice();
            bookingForm.clickOnRentPriceListIndex(i + 1);
            assertEquals(selectedPrice, bookingSummaryPrice, "Price is not equal between selected price " + selectedPrice + " and booking summary price " + bookingSummaryPrice);
        }
    }

    @Then("^tenant can see time to pay the bill is contain words to the selected duration$")
    public void tenant_can_see_time_to_pay_the_bill_is_contain_words_to_the_selected_duration(List<String> list) throws InterruptedException {
        int paidDurationTime = bookingForm.getListRentDurationNumber();
        for(int i = 0; i < paidDurationTime; i++) {
            bookingForm.clickOnRentPriceListIndex(i + 1);
            assertTrue(bookingForm.isDurationContainPreviewBookingPresent(list.get(i)));
        }
    }

    @Then("system display hyperlink property detail page for booking")
    public void system_display_hyperlink_property_detail_page_for_booking() throws InterruptedException {
        Assert.assertTrue(bookingForm.getPropertyHyperlink().contains(propertyHyperlink_), "Hyperlink is different");
    }

    @When("user cancel booking")
    public void user_cancel_booking() throws InterruptedException {
        kosDetailPO.cancelBooking();
    }

    @And("user click back button in Booking form page")
    public void userTapBackButtonInBookingFormPage() throws InterruptedException {
        bookingForm.isLoadingAnimationAppear();
        do {
            selenium.hardWait(2);
        }
        while (bookingForm.isLoadingAnimationAppear());
        selenium.back();
    }

    @When("tenant/I/user set tomorrow date click on continue button")
    public void x_click_on_continue_button() throws InterruptedException, ParseException {
        if(bookingForm.isLoadingAnimationAppear()) {
            do {
                selenium.hardWait(1);
            }while (bookingForm.isLoadingAnimationAppear());
        }
        String tomorrowDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        bookingForm.selectDate(tomorrowDate);
        bookingForm.clickOnContinueButton();
    }

    @Then("tenant/I/user can see {string} gender is selected")
    public void x_can_see_male_gender_is_selected(String gender) {
        Assert.assertEquals(gender, bookingForm.getSelectedGenderText(), "Selected gender is not Laki-laki");
    }

    @Then("tenant/I/user can see information that state Kost is only for {string}")
    public void  x_can_see_information_that_state_kost_is_only_for_male(String gender) {
        String alert = bookingForm.getAlertInformationRestrictedGenderText();
        String[] alertArr = alert.split(" ");
        int alertActualSize = alertArr.length - 6;
        for(int i = 0; i <= alertActualSize; i++) {
            alertArr = (String[]) ArrayUtils.remove(alertArr, 0);
        }
        Assert.assertEquals(String.join(" ",alertArr), gender, "");
    }

    @When("tenant/I/user select {string} date as booking date")
    public void xSelectXDateAsBookingDate(String date) throws Exception {
        String bookingDate = (date.equalsIgnoreCase("tomorrow"))
                ? java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0)
                : date;
        if (kosDetailPO.isDatePickerClickable()) {
            kosDetailPO.selectBookingDate(bookingDate);
        }else if (bookingForm.isInBookingForm()) {
            manageBooking.loadingHandler();
            bookingForm.selectDate(bookingDate);
        }
        else {
            throw new Exception("No date picker available");
        }
    }

    @When("tenant/I/user click on Lihat button to go to draft booking page")
    public void xClickOnLihatButtonToGoToDraftBookingPage() throws InterruptedException{
        kosDetailPO.clickOnSeeButton();
    }

    @And("user click exit button")
    public void clickOnExitButtonBooking() throws InterruptedException {
        do {
            Thread.sleep(1000);
        }
        while (bookingConfirmation.isHandleLoadingForPopUpDraft());
        bookingConfirmation.clickOnEXitBookingButton();
    }

    @And("user check confirmation text title")
    public void checkConfirmationTextTitle(){
        assertEquals(bookingConfirmation.getTitleConfirmationDraftText(), "Yakin batalkan Booking?", "Text is not " + bookingConfirmation.getTitleConfirmationDraftText());
    }

    @And("user check confirmation text body")
    public void checkConfirmationTextBody(){
        assertEquals(bookingConfirmation.getBodyConfirmationDraftText(), "Jika kamu batalkan, kamu tetap bisa melihat Kos ini di Draft. Untuk melanjutkan Booking, klik “Lanjut Booking”.", "Text is not " + bookingConfirmation.getBodyConfirmationDraftText());
    }

    @And("user click Continue Booking")
    public void clickContinueBooking() throws InterruptedException {
        bookingConfirmation.continueBookingFromPopUpDraft();
    }

    @And("user click Save Draft")
    public void clickSaveDraft() throws InterruptedException {
        bookingConfirmation.saveDraftBookingFromPopUpDraft();
    }

    @Then("user redirect to kost detail")
    public void redirectToKostDetail(){
        assertTrue(draftBooking.isSeeKostDetail(), "Not in kost detail");
    }

    @And("user check Lihat button clickable")
    public void checkLihatButtonClickable(){
        assertTrue(draftBooking.isSeeButtonClickable(), "Button not clickable");
    }

    @And("user check toast draft")
    public void checkToastDraft() throws InterruptedException {
        assertEquals(draftBooking.getSeeButtonText(), "Tersimpan sebagai Draft\nLIHAT", "Text is not " + draftBooking.getSeeButtonText());
    }

    @And("user click Continue Booking from draft booking page")
    public void clickContinueBookingDraftBooking() throws InterruptedException {
        draftBooking.continueBookingDraftPage();
    }

    @Then("user redirect to Form Booking Harga dan Masa Sewa")
    public void redirectToFormBookingHargaMasaSewa(){
        assertEquals(draftBooking.getFormBookingMasaSewa(), "Tentukan harga dan masa sewa yang Anda butuhkan", "Text is not " + draftBooking.getFormBookingMasaSewa());
    }

    @And("user click See Detail Kost from draft booking page")
    public void clickSeeDetailKostDraftBooking() throws InterruptedException {
        draftBooking.seeDetailKostButtonDraftBooking();
    }

    @And("user redirect to kost detail new tab")
    public void redirectToKostDetailNewTab() throws InterruptedException {
        assertTrue(draftBooking.seeDetailKostOpenNewTab(), "not in kost detail");
    }

    @And("user click delete button on draft booking")
    public void clickDeleteButtonDraftBooking() throws InterruptedException {
        draftBooking.deleteButtonDraftBooking();
    }

    @And("check pop up title on delete draft booking")
    public void modalTitleDeleteDraftBooking(){
        assertEquals(draftBooking.getTitleModalDeletDraft(), "Yakin menghapus Kos ini dari Draft Booking?", "Text is not " + draftBooking.getTitleModalDeletDraft());
    }

    @And("check pop up caption on delete draft booking")
    public void modalCaptionDeleteDraftBooking(){
        assertEquals(draftBooking.getCaptionModalDeleteDraft(), "Kamu akan mengulang kembali dari awal proses Booking di Kos ini.", "Text is not " + draftBooking.getCaptionModalDeleteDraft());
    }

    @And("click close on pop up delete draft booking")
    public void clickCloseModalDeleteDraft() throws InterruptedException {
        draftBooking.closeModalDeleteDraft();
    }

    @And("user click delete button on tab one draft booking")
    public void clickDeleteButtonDraftBookingOnTabOne() throws InterruptedException {
        draftBooking.deleteButtonDraft();
    }

    @And("user click Batalkan on pop up delete draft booking")
    public void clickBatalkanModalDeleteDraft() throws InterruptedException {
        draftBooking.batalkanModalDeleteDraft();
    }

    @And("user click Hapus Draft on pop up delete draft booking")
    public void clickHapusDraftModalDeleteDraft() throws InterruptedException {
        draftBooking.hapusDraftModalDeleteDraft();
    }

    @Then("tenant cannot see {string} as kost name and kost location")
    public void tenantCannotSeeTextAsKostNameAndKostLocation(String text) throws InterruptedException {
        assertTrue(draftBooking.isKostNameAndLocationAbsence(), "Kost is present!");
    }

    @And("user choose boarding date is {string} and clicks on Booking button on Kost details page")
    public void user_choose_boarding_date_is_and_clicks_on_booking_button_on_kost_details_page(String boardingDate) throws InterruptedException, ParseException {
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.selectDateForStartBoarding(boardingDate);
        kosDetailPO.selectRentType("Per bulan");
        kosDetailPO.clickOnBookingButton();
    }

    @When("user create booking for today")
    public void userCreateBookingForToday() throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.selectBookingDateToday();
        kosDetailPO.selectRentType("Per bulan");
        kosDetailPO.clickOnBookingButton();
    }

    @And("user create booking for tomorrow")
    public void userCreateBookingForTomorrow() throws InterruptedException, ParseException {
        kosDetailPO.dismissFTUEScreen();
        String tomorrow = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        kosDetailPO.selectBookingDateTomorrow(tomorrow);
        kosDetailPO.selectRentType("Per bulan");
        kosDetailPO.clickOnBookingButton();
    }

    @Then("booking confirmation screen displayed with  Kost name, Room price, Duration, Tenant Name, Gender")
    public void bookingConfirmationScreenDisplayedWithKostNameRoomPriceDurationTenantNameGender() throws InterruptedException {
        assertEquals(bookingConfirmation.getRoomNameNew(), kostName, "Room name doesn't match");
        assertEquals(bookingConfirmation.getRoomPriceNew(), "Harga Sewa Per Bulan\nRp" + kostPrice, "Room price doesn't match");
        assertEquals(bookingForm.getRentDuration(), duration, "Rent duration doesn't match");
        Assert.assertEquals(bookingConfirmation.getTenantNameNew(), Constants.TENANT_FACEBOOK_NAME, "Tenant name doesn't match");
        assertTrue(bookingConfirmation.isTenantGenderSelected(gender), "Gender doesn't match");
    }

    @And("user click Chat Pemilik Kos")
    public void userClickChatPemilikKos() throws InterruptedException{
        successBooking.clickOnChatPemilikKos();
    }

    @And("user click close chat box")
    public void userClickCloseChatBox() throws InterruptedException{
        successBooking.clickCloseOnChatBox();
    }

    @And("user cancel booking with reason {string}")
    public void user_cancel_booking_with_reason(String reason) throws InterruptedException{
        kosDetailPO.cancelBookingWithReason(reason);
    }

    @Then("tenant redirect to history booking page")
    public void tenant_redirect_to_history_booking_page() {
        assertTrue(selenium.getURL().contains("user/booking"), "Not in user booking page");
    }

    @Then("tenant can see notification is increase and with text {string}")
    public void tenant_can_see_notification_is_increase_and_with_text(String string) {

    }

    @And("user check status booking")
    public void user_check_status_booking() throws InterruptedException{
        Assert.assertEquals(kosDetailPO.getBookingStatusCancel(), "Dibatalkan", "Status doesn't match");
    }

    @And("user check reason cancellation {string}")
    public void user_check_reason_cancellation(String reason) throws InterruptedException{
        Assert.assertEquals(kosDetailPO.getReasonCancellation(reason), reason, "Reason doesn't match");
    }

    @And("user cancel booking with Lainnya and enter text {string} in lainnya box")
    public void user_cancel_booking_with_lainnya_and_enter_text(String lainnya) throws InterruptedException{
        kosDetailPO.cancelBookingWithLainnya(lainnya);
    }

    @Then("reason cancellation lainnya will appears {string}")
    public void reason_cancellation_lainnya_will_appears(String reasonLainnya) throws InterruptedException{
        Assert.assertEquals(kosDetailPO.getReasonCancellationLainnya(reasonLainnya), reasonLainnya, "Reason doesn't match");
    }

    @Then("user can sees down payment text with price is {string}")
    public void user_can_sees_down_payment_text_with_price_is_x(String downPaymentPrice) throws InterruptedException{
        bookingForm.clickOnChangeMethodPayButton();
        Assert.assertEquals(bookingConfirmation.getDownPaymentText(), "Uang muka (DP)", "Active text is not: Uang muka (DP)");
        String afterTrim = kosDetailPO.getTotalDPOnPengajuanSection(0).trim();
        Assert.assertEquals(afterTrim, downPaymentPrice, "DP active price is not: " + downPaymentPrice);
    }

    @When("user clicks on ask kost owner button")
    public void user_clicks_on_ask_kost_owner_button() throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.clickOnAskOwnerButton();
    }

    @Then("user can sees Hubungi Kost pop-up")
    public void user_can_sees_hubungi_kost_popup() throws InterruptedException{
        Assert.assertEquals(contactKost.getContactKostPopUpTitle(), "Hubungi Kost");
        Assert.assertEquals(contactKost.getContactKostPopUpCaption(), "Anda akan terhubung dengan pemilik langsung melalui chatroom mamikos");
    }

    @Then("user can sees {string} is selected")
    public void user_can_sees_x_is_selected(String selectedText) {
        Assert.assertEquals(contactKost.getDefaultSelectedRadioQuestioner(), selectedText);
    }

    @When("user click on Ajukan Sewa button")
    public void user_click_on_ajukan_sewa_button() throws InterruptedException {
        contactKost.clickOnRentSubmitButton();
    }

    @Then("user can sees booking form page")
    public void user_can_sees_booking_form_page() {
        Assert.assertEquals(bookingConfirmation.getBookingFormPageTitle(), "Pengajuan Sewa", "Title is not Pengajuan Sewa");
    }

    @When("tenant dismiss FTUE screen on kost details")
    public void tenant_dismiss_FTUE_screen_on_kost_details() throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
    }

    @Then("tenant can see booking date in booking from is {string} date")
    public void tenant_can_see_booking_date_in_booking_from_is_date(String date) throws ParseException {
        String tomorrow = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d-MMM-yyyy", 1, 0, 0, 0).replace("-", " ");
        if(date.equalsIgnoreCase("tomorrow")) {
            date = tomorrow;
        }
        Assert.assertEquals(bookingForm.getSelectedDate(), date, "is not equal to" + date);
    }

    @When("tenant clicks on rent submit button on kost detail")
    public void tenant_clicks_on_rent_submit_button_on_kost_detail() throws InterruptedException {
        kosDetailPO.clickOnBookingButton();
    }

    @Then("tenant can see disclaimer on checkin date kost detail")
    public void tenant_can_see_disclaimer_on_checkin_date_kost_detail() {
        Assert.assertEquals(kosDetailPO.getMaxMonthAndCheckinDateDisclaimerText(0), "Waktu mulai ngekos terdekat:");
        Assert.assertEquals(kosDetailPO.getMaxMonthAndCheckinDateDisclaimerText(1), "Waktu mulai ngekos terjauh:");
    }

    @Then("tenant can sees kost details with kost name {string}")
    public void tenant_can_sees_kost_details_with_kost_name(String kostName) {
        String kostNameAssert = null;
        if (kostName.equalsIgnoreCase("need confirmation")) {
            kostNameAssert = needConfirmationKostName;
        }
        else {
            kostNameAssert = kostName;
        }
        Assert.assertTrue(kosDetailPO.getKostName().contains(kostNameAssert));
        selenium.switchToWindow(1);
    }

    @Then("user can see {string} and {string} on kost detail")
    public void user_can_see_and_on_kost_detail(String text1, String text2) throws InterruptedException{
        kosDetailPO.dismissFTUEScreen();
        Assert.assertEquals(bookingConfirmation.getText1KostDownPayment(), text1, text1 + " Not Present");
        Assert.assertEquals(bookingConfirmation.getText2KostDownPayment(), text2, text2 + " Not Present");
    }

    @Then("user can see Uang muka, Pelunasan, Total Pembayaran pertama, Wajib bayar DP on kost detail")
    public void user_can_see_uang_muka_dp_pelunasan_total_pembayaran_pertama_wajib_bayar_dp_on_kost_detail() throws InterruptedException{
        assertTrue(kosDetailPO.uangMukaLabelPresent(), "uang muka label not present");
        assertTrue(kosDetailPO.pelunasanLabelPresent(), "pelunasan label not present");
        assertTrue(kosDetailPO.totalBayarPertamaLabelPresent(), "total pembayaran pertama label not present");
        assertTrue(kosDetailPO.wajibBayarWarningPresent(), "wajib bayar warning not present");
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(0))), totalBayarPertama, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(0));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(1))), totalBayarPertamaPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(1));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(2))), totalAmount, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(2));
    }

    @And("user select date {string} and rent type {string}")
    public void user_select_date_and_rent_type(String time, String rentType) throws InterruptedException, ParseException {
        String dateTime = "";
        if (time.equalsIgnoreCase("tomorrow")){
            dateTime = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        }
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.selectDateForStartBoarding(dateTime);
        selenium.hardWait(5);
        kosDetailPO.selectRentType(rentType);
    }

    @And("user clicks on Uang Muka")
    public void user_clicks_on_Uang_Muka() throws InterruptedException{
        kosDetailPO.clickOnUangMukaLabel();
    }

    @And("user clicks on Pelunasan")
    public void user_clicks_on_pelunasan() throws InterruptedException{
        kosDetailPO.clickOnPelunasanLabel();
    }

    @And("user clicks on Pembayaran penuh")
    public void user_clicks_on_Pembayaran_penuh() throws InterruptedException{
        kosDetailPO.clickOnPembayaranPenuhLabel();
    }

    @And("user can see Uang muka, biaya layanan mamikos, Total Pembayaran pertama on popup uang muka detail and user validates the value")
    public void user_can_see_Uang_Muka_Biaya_Layanan_Mamikos_Total_Pembayaran_Pertama_On_Popup_Uang_Muka_Detail_And_User_Validates_The_Value() throws InterruptedException{
        assertTrue(kosDetailPO.uangMukaLabelOnPopupPresent(), "uang muka label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(0), "pelunasan label not present");
        assertTrue(kosDetailPO.totalBayarPertamaLabelOnPopupPresent(), "total pembayaran pertama label not present");
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(3))), totalDPWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(3));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), totalBayarPertama, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
    }

    @And("user can see pelunasan, biaya layanan mamikos, Total Pembayaran pertama on popup pelunasan detail and user validates the value")
    public void user_can_see_pelunasan_Biaya_Layanan_Mamikos_Total_Pembayaran_Pertama_On_Popup_pelunasan_Detail_And_User_Validates_The_Value() throws InterruptedException{
        assertTrue(kosDetailPO.pelunasanLabelOnPopupPresent(), "pelunasan label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(0), "pelunasan label not present");
        assertTrue(kosDetailPO.totalBayarPertamaPelunasanLabelOnPopupPresent(), "total pembayaran pertama label not present");
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(3))), totalPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(3));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), totalBayarPertamaPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
    }

    @And("user click button close popup")
    public void user_click_button_close_popup() throws InterruptedException{
        kosDetailPO.clickOnCloseButton();
    }

    @And("user clicks on total pembayaran pertama")
    public void user_clicks_on_total_pembayaran_pertama() throws InterruptedException{
        kosDetailPO.clickOnTotalBayarPertamaLabel();
    }

    @And("user can see Uang muka, biaya layanan mamikos, Total Pembayaran pertama on popup total pembayaran detail and user validates the value")
    public void user_can_see_Uang_Muka_Biaya_Layanan_Mamikos_Total_Pembayaran_Pertama_On_Popup_total_Detail_And_User_Validates_The_Value() throws InterruptedException{
        assertTrue(kosDetailPO.uangMukaLabelOnPopupPresent(), "uang muka label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(0), "pelunasan label not present");
        assertTrue(kosDetailPO.totalBayarPertamaLabelOnPopupPresent(), "total pembayaran pertama label not present");
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), totalDPWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(7))), totalBayarPertama, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(7));
    }

    @And("user can see pelunasan, biaya layanan mamikos, Total Pembayaran pertama on popup total pembayaran detail and user validates the value")
    public void user_can_see_pelunasan_Biaya_Layanan_Mamikos_Total_Pembayaran_Pertama_On_Popup_total_Detail_And_User_Validates_The_Value() throws InterruptedException{
        assertTrue(kosDetailPO.pelunasanLabelOnPopupPresent(), "pelunasan label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(1), "pelunasan label not present");
        assertTrue(kosDetailPO.totalBayarPertamaPelunasanLabelOnPopupPresent(), "total pembayaran pertama label not present");
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(8))), totalPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(8));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(9))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(9));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(10))), totalBayarPertamaPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(10));
    }

    @And("user can see Pembayaran selanjutnya, Harga sewa per Bulan \\(perbulan price), Biaya Layanan Mamikos, and Total pembayaran selanjutnya and user validates the value")
    public void user_can_see_detail_total_bayar_pertama() throws InterruptedException{
        assertTrue(kosDetailPO.hargasewaLabelOnPopupPresent(), "pelunasan label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(2), "pelunasan label not present");
        assertTrue(kosDetailPO.totalBayarSelanjutnyaLabelOnPopupPresent(), "total pembayaran pertama label not present");
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(11))), kostPriceWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(11));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(12))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(12));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(13))), totalBayarWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(13));
    }

    @And("user can see DP information with {string}, {string}, total DP on pengajuan sewa section")
    public void user_Can_See_DP_Information_With_Total_DP_On_Pengajuan_Sewa_Section(String text1, String text2) throws InterruptedException{
//        bookingForm.clickOnChangeMethodPayButton();
        Assert.assertEquals(kosDetailPO.uangMukaLabelOnPopupPresent(), text1, "uang muka label not present");
        Assert.assertEquals(kosDetailPO.getInfoDiscountDPLabelOnPengajuanSection(), text2, "total discount label not present");
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(12))), totalBayarPertama, "Total dp is " + kosDetailPO.getTotalUangMukaOnPopup(12));
    }


    @Then("user can see Pembayaran Pertama \\(DP), Uang muka \\(DP), Biaya Layanan admin, Total Pembayaran Pertama \\(DP)")
    public void user_Can_See_Pembayaran_Pertama_DP_Uang_Muka_DP_DP_Price_Biaya_Layanan_Admin_Admin_Price_Total_Pembayaran_Pertama_DP_DP_Price_Admin_Biaya_DP_Dari_Harga_Sewa() throws InterruptedException{
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(10))), totalDPWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(10));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(11))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(11));
    }

    @Then("user can see Pelunasan with harga sewa - DP, Biaya Layanan Mamikos and Total Pembayaran pertama\\(Pelunasan)")
    public void userCanSeePelunasanWithHargaSewaDPBiayaLayananMamikosAndTotalPembayaranPertamaPelunasanPelunasanBiayaTambahanDepositBiayaLayananMamikos() throws InterruptedException{
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(3), "pelunasan label not present");
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(13))), totalPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(13));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(14))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(14));
        Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(15))), totalBayarPertamaPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(15));
    }

    @Then("user can see Jika kamu membayar dengan DP, Uang Muka \\(DP), Pelunasan, Jika kamu pakai pembayaran penuh, Total Pembayaran pertama, Bisa bayar pakai DP {string} and user validates the value")
    public void user_Can_See_Jika_Kamu_Membayar_Dengan_DP(String type) throws InterruptedException{
        assertTrue(kosDetailPO.jikaPakaiDPLabelOnKostDetailPresent(), "jika bayar pakai dp label not present");
        assertTrue(kosDetailPO.uangMukaLabelPresent(), "uang muka label not present");
        assertTrue(kosDetailPO.pelunasanLabelPresent(), "pelunasan label not present");
        assertTrue(kosDetailPO.jikaBayarPenuhLabelOnKostDetailPresent(), "jika bayar penuh label not present");
        assertTrue(kosDetailPO.totalBayarPertamaLabelPresent(), "total pembayaran pertama label not present");
        assertTrue(kosDetailPO.bisaPakaiDPWarningPresent(), "bisa bayar pakai dp warning not present");
        if (type.equals("tiga bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(0))), totalDP3Bulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(0));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(1))), totalPelunasan3Bulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(1));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(3))), totalBayarPertama3Bulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(3));
        } else if(type.equals("per bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(0))), totalDPPerBulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(0));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(1))), totalPelunasanPerBulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(1));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(3))), totalBayarPertamaPerBulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(3));
        } else if(type.equals("per minggu")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(0))), totalBayarPertama, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(0));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(1))), totalBayarPertamaPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(1));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(3))), totalBayarWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(3));
        }
    }

    @And("user can see Uang muka, biaya layanan mamikos, Total Pembayaran pertama on popup uang muka detail and user validates the value {string}")
    public void user_can_see_Uang_Muka_Biaya_Layanan_Mamikos_Total_Pembayaran_Pertama_On_Popup_Uang_Muka_Detail_And_User_Validates_The_Value_tiga_bulan(String type) throws InterruptedException{
        assertTrue(kosDetailPO.uangMukaLabelOnPopupPresent(), "uang muka label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(0), "biaya layanan label not present");
        assertTrue(kosDetailPO.totalBayarPertamaLabelOnPopupPresent(), "total pembayaran pertama label not present");
        if (type.equals("tiga bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), totalDP3BulanOnPopUp, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), totalDP3Bulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        } else if (type.equals("per bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), totalDPPerBulanOnPopUp, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), totalDPPerBulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        } else if(type.equals("per minggu")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), totalDPWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), totalBayarPertama, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        }
    }

    @And("user can see pelunasan, biaya layanan mamikos, Total Pembayaran pertama on popup pelunasan detail and user validates the value {string}")
    public void user_can_see_pelunasan_Biaya_Layanan_Mamikos_Total_Pembayaran_Pertama_On_Popup_pelunasan_Detail_And_User_Validates_The_Valuethree_months(String type) throws InterruptedException{
        assertTrue(kosDetailPO.pelunasanLabelOnPopupPresent(), "pelunasan label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(0), "biaya layanan label not present");
        assertTrue(kosDetailPO.totalBayarPertamaPelunasanLabelOnPopupPresent(), "total pembayaran pertama label not present");
        if (type.equals("tiga bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), totalPelunasan3BulanOnPopUp, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), totalPelunasan3Bulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        } else if (type.equals("per bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), totalPelunasanPerBulanOnPopUp, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), totalPelunasanPerBulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        } else if(type.equals("per minggu")) {
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), totalPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), totalBayarPertamaPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        }

    }

    @And("user can see pembayaran penuh, biaya layanan mamikos, Total Pembayaran pertama on popup pembayaran penuh detail and user validates the value {string}")
    public void user_can_see_pembayaran_penuh_Biaya_Layanan_Mamikos_Total_Pembayaran_Pertama_On_Popup_pelunasan_Detail_And_User_Validates_The_Valuethree_months(String type) throws InterruptedException{
        assertTrue(kosDetailPO.pembayaranPenuhLabelOnPopupPresent(), "pelunasan label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(0), "biaya layanan label not present");
        assertTrue(kosDetailPO.totalBayarPenuhLabelOnPopupPresent(), "total pembayaran penuh label not present");
        if (type.equals("tiga bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), kostPrice3Bulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), totalBayarPertama3Bulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        } else if (type.equals("per bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), kostPricePerBulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), totalBayarPertamaPerBulan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        } else if(type.equals("per minggu")) {
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(4))), kostPriceWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(4));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(5))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(5));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), totalBayarWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
        }
    }

    @And("user can see DP information with {string}, {string}, total DP on pengajuan sewa section {string}")
    public void user_Can_See_DP_Information_With_Total_DP_On_Pengajuan_Sewa_Section_three_months(String text1, String text2, String type) throws InterruptedException{
        bookingForm.clickOnChangeMethodPayButton();
        Assert.assertEquals(kosDetailPO.getUangMukaLabelOnPengajuanSection(0), text1, "uang muka label not present");
        Assert.assertEquals(kosDetailPO.getInfoDPLabelOnPengajuanSection(1), text2, "uang muka label not present");
        if (type.equals("tiga bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalDPOnPengajuanSection(0))), totalDP3Bulan, "Total dp is " + kosDetailPO.getTotalDPOnPengajuanSection(0));
        } else if (type.equals("per bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalDPOnPengajuanSection(0))), totalDPPerBulan, "Total dp is " + kosDetailPO.getTotalDPOnPengajuanSection(0));
        } else if(type.equals("per minggu")) {
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalDPOnPengajuanSection(0))), totalBayarPertama, "Total dp is " + kosDetailPO.getTotalDPOnPengajuanSection(0));
        }
    }

    @Then("user can see Pembayaran Pertama \\(DP), Uang muka \\(DP), Biaya Layanan admin, Total Pembayaran Pertama \\(DP) {string}")
    public void user_Can_See_Pembayaran_Pertama_DP_Uang_Muka_DP_DP_Price_Biaya_Layanan_Admin_Admin_Price_Total_Pembayaran_Pertama_DP_DP_Price_Admin_Biaya_DP_Dari_Harga_Sewa_three_months(String type) throws InterruptedException{
        assertTrue(kosDetailPO.uangMukaLabelOnPopupPresent(), "uang muka label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(0), "biaya layanan label not present");
        assertTrue(kosDetailPO.totalBayarPertamaLabelOnPopupPresent(), "total pembayaran pertama label not present");
        if (type.equals("tiga bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(0))), totalDP3BulanOnPopUp, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(0));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBiayaLayananOnPopup(1))), adminFee, "Total amount is " + kosDetailPO.getTotalBiayaLayananOnPopup(1));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBayar(0))), totalDP3Bulan, "Total amount is " + kosDetailPO.getTotalBayar(0));
        } else if (type.equals("per bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(0))), totalDPPerBulanOnPopUp, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(0));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBiayaLayananOnPopup(1))), adminFee, "Total amount is " + kosDetailPO.getTotalBiayaLayananOnPopup(1));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBayar(0))), totalDPPerBulan, "Total amount is " + kosDetailPO.getTotalBayar(0));
        } else if(type.equals("per minggu")) {
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(0))), totalDPWeekly, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(0));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBiayaLayananOnPopup(1))), adminFee, "Total amount is " + kosDetailPO.getTotalBiayaLayananOnPopup(1));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBayar(0))), totalBayarPertama, "Total amount is " + kosDetailPO.getTotalBayar(0));
        }
    }

    @Then("user can see Pelunasan with harga sewa - DP, Biaya Layanan Mamikos and Total Pembayaran pertama\\(Pelunasan) {string}")
    public void userCanSeePelunasanWithHargaSewaDPBiayaLayananMamikosAndTotalPembayaranPertamaPelunasanPelunasanBiayaTambahanDepositBiayaLayananMamikos_three_months(String type) throws InterruptedException{
        assertTrue(kosDetailPO.pelunasanLabelOnPopupPresent(), "pelunasan label not present");
        assertTrue(kosDetailPO.biayaLayananLabelOnPopupPresent(1), "biaya layanan label not present");
        assertTrue(kosDetailPO.totalBayarPertamaPelunasanLabelOnPengajuanSectionPresent(), "total pembayaran pertama label not present");
        if (type.equals("tiga bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(2))), totalPelunasan3BulanOnPopUp, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(2));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBiayaLayananOnPopup(3))), adminFee, "Total amount is " + kosDetailPO.getTotalBiayaLayananOnPopup(3));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBayar(1))), totalPelunasan3Bulan, "Total amount is " + kosDetailPO.getTotalBayar(1));
        } else if (type.equals("per bulan")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(2))), totalPelunasanPerBulanOnPopUp, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(2));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBiayaLayananOnPopup(3))), adminFee, "Total amount is " + kosDetailPO.getTotalBiayaLayananOnPopup(3));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBayar(1))), totalPelunasanPerBulan, "Total amount is " + kosDetailPO.getTotalBayar(1));
        } else if(type.equals("per minggu")) {
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(2))), totalPelunasan, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(2));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBiayaLayananOnPopup(3))), adminFee, "Total amount is " + kosDetailPO.getTotalBiayaLayananOnPopup(3));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalBayar(1))), totalBayarPertamaPelunasan, "Total amount is " + kosDetailPO.getTotalBayar(1));
        }
    }

    @Then("user can see detail payment for rent type {string} not set DP")
    public void user_can_see_detail_payment_for_rent_type(String rentType) throws InterruptedException{
        if (rentType.equals("Per tahun")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(0))), kostPricePerTahun, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(0));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(1))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(1));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(2))), totalBayarPerTahun, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(2));
        }
    }

    @Then("user can see detail payment on pop up total pembayaran {string} non set DP")
    public void user_can_see_detail_payment_on_pop_up_total_pembayaran(String rentType) throws InterruptedException{
        if (rentType.equals("Per tahun")){
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(6))), kostPricePerTahun, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(6));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(7))), adminFee, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(7));
            Assert.assertEquals(String.valueOf(java.extractNumber(kosDetailPO.getTotalUangMukaOnPopup(8))), totalBayarPerTahun, "Total amount is " + kosDetailPO.getTotalUangMukaOnPopup(8));
        }
    }

    @And("user select {string} on type permbayaran pertama option")
    public void user_select_on_type_permbayaran_pertama_option(String typePembayaran) throws InterruptedException{
        bookingForm.selectTypePembayaranPertama(typePembayaran);
    }

    @Then("user verify the message {string} on pop up")
    public void user_verify_the_message_on_pop_up(String bookingCancelMessagePopUp) {
        Assert.assertEquals(kosDetailPO.getMessageCancelBooking(), bookingCancelMessagePopUp, "Message success cancel booking doesn't match!");
    }

    @And("user verify the button success cancel booking is {string}")
    public void user_verify_the_button_success_cancel_booking_is(String textButtonCancelBookingPopUp) {
        Assert.assertEquals(kosDetailPO.getBtnCancelBooking(), textButtonCancelBookingPopUp, "Button success cancel booking doesn't match!");
    }

    @When("user click Ok button on success cancel booking pop up")
    public void user_click_ok_button_on_success_cancel_booking_pop_up() throws InterruptedException{
        int notifCurrent = history.getNotificationNumber();
        kosDetailPO.clickOnPopUpCancelBooking();
        int notifAfter = history.getNotificationNumber();
        Assert.assertEquals(notifCurrent + 1, notifAfter);
    }

    @And("user click on checkinbox and can see {string}")
    public void user_click_on_checkinbox_and_can_see_x(String text) throws InterruptedException {
        kosDetailPO.clickOnChecinDate();
        Assert.assertEquals(kosDetailPO.getKetentuanCheckinDateText(), text, "not appear ketentuan kos");
    }

    @And("user click on Draft menu")
    public void user_click_on_draft_menu() throws InterruptedException {
        draftBooking.clickDraftMenuButton();
    }

    @And("user choose to payment using {string}")
    public void user_choose_to_payment_using(String paymentMethod) throws InterruptedException{
        bookingForm.selectPaymentMethod(paymentMethod);
    }

    @And("user select checkin date for today")
    public void userSelectCheckinDateForToday() throws InterruptedException {
        bookingForm.selectCheckinTime();
    }

    @And("user click delete draft if any")
    public void user_click_delete_draft_if_any() throws InterruptedException {
        draftBooking.deleteAllDraftsIfAny();
    }

    @Then("user verify the {string}")
    public void user_verify_the(String bookingSuccessText) {
        Assert.assertEquals(bookingForm.getBookingSuccessMessage(), bookingSuccessText, "Pengajuan booking status text is not match!");
    }

    @When("user click on Lihat Status Pengajuan")
    public void user_click_on_lihat_status_pengajuan() throws InterruptedException {
        bookingForm.clickOnLihatStatusPengajuan();
    }

    @When("user click lihat selengkapnya the riwayat booking")
    public void user_click_lihat_selengkapnya_the_riwayat_booking() throws InterruptedException {
        kosDetailPO.clickOnLihatSelengkapnyaRiwayatBooking();
    }

    @When("Booking title is displayed")
    public void user_verify_booking_title_is_displayed() throws InterruptedException {
        Assert.assertTrue(bookingForm.isBookingTitleDisplayed());
    }

    @And("user click on Ajukan Sewa")
    public void user_click_on_ajukan_sewa() throws InterruptedException {
        selenium.hardWait(2);
        int i;
        for (i = 0; i <= 10; i++) {
            selenium.hardWait(2);
            if (bookingConfirmation.isSubmitButtonVisible()) {   //check if the submit button (ajukan sewa in form booking) is visible
                bookingConfirmation.clickOnSubmitButton();
            }
        }
    }

    @Then("user can see Informasi popup")
    public void user_can_see_informasi_popup() throws InterruptedException {
        Assert.assertTrue(bookingForm.isInformationBookingDisplayed());
    }

    @And("user click on tnc {string}")
    public void user_click_on_syarat_dan_ketentuan_ngekos(String text) throws InterruptedException {
        if(text.equalsIgnoreCase("Syarat dan Ketentuan Umum")) {
            bookingForm.clickOnTnCBookingReguler();
        }
        else if(text.equalsIgnoreCase("Syarat dan Ketentuan Tinggal di Singgahsini, Apik, & Kos Pilihan")){
            bookingForm.clickOnTnCBookingSinggahSini();
        }
    }

    @Then("user can see tnc content with {string}")
    public void user_can_see_tnc_content_text_with_x(String text) throws InterruptedException {
        Assert.assertEquals(bookingForm.tncContentText(), text, "not same message");
        bookingForm.clickOnOkPahamButton();
    }

    @Then("user can see harga coret on detail price")
    public void user_can_see_harga_coret_on_detail_price() {
        Assert.assertEquals(kosDetailPO.getDiscountLabelOnKostDetail(), kosDetailPO.getDiscountLabelOnKostDetail(), "not appears discount");
    }

    @Then("user cannot see harga coret on detail price")
    public void user_cannot_see_harga_coret_on_kost_detail() throws InterruptedException {
        assertTrue(kosDetailPO.isDiscountLabelAbsence(), "discount label is present");
    }

    @When("user can see tnc with {string}")
    public void user_can_see_tnc_with_x(String text) throws InterruptedException {
        if(text.equalsIgnoreCase("Syarat dan Ketentuan Umum")) {
            Assert.assertEquals(bookingForm.tncBookingTextReguler(), text, "not same text tnc booking");
        }
        else if(text.equalsIgnoreCase("Syarat dan Ketentuan Tinggal di Singgahsini, Apik, & Kos Pilihan")) {
            Assert.assertEquals(bookingForm.tncBookingTextSinggahSini(),text, "not same text tnc booking");
        }
    }

    @Then("user can see validation on jobs")
    public void user_can_see_validation_on_jobs() throws InterruptedException {
        bookingConfirmation.getAlertJobsText();
    }

    @Then("user can see validation on jobs with {string}")
    public void user_can_see_validation_on_jobs_after_click(String text) throws InterruptedException {
        Assert.assertEquals(bookingConfirmation.getAlertJobsTextAfterClick(), text, "text not same in the display");
    }
}