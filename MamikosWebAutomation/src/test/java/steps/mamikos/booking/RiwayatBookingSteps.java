package steps.mamikos.booking;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang.ArrayUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.booking.ManageBookingPO;
import pageobjects.mamikos.tenant.booking.*;
import pageobjects.mamikos.tenant.profile.BookingHistoryPO;
import pageobjects.mamikos.tenant.profile.DraftBookingPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.List;

import static org.testng.Assert.*;

public class RiwayatBookingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JavaHelpers java = new JavaHelpers();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private RiwayatBookingPO riwayatBooking = new RiwayatBookingPO(driver);

    //---------------Check Butuh Pembayaran status---------------//
    @And("user click Riwayat dan Draft Booking menu")
    public void user_click_riwayat_dan_draft_booking_menu() throws InterruptedException {
        riwayatBooking.clickRiwayatBooking();
    }

    @And("user click Filter and choose {string}")
    public void user_click_filter_and_choose(String statusBooking) throws InterruptedException {
        riwayatBooking.clickFilterBooking();
        riwayatBooking.chooseBookingStatus(statusBooking);
    }

    @Then("user will see booking with {string} status")
    public void user_will_see_booking_with_status(String statusBooking){
        Assert.assertEquals(riwayatBooking.getBookingStatus(statusBooking), riwayatBooking.getBookingStatus(statusBooking), "Title of status is not match");
    }

    @And("user click on Selengkapnya")
    public void user_click_on_selengkapnya() throws InterruptedException{
        riwayatBooking.clickSelengkapnyaButton();
    }

    @And("user click on link refund")
    public void user_click_on_ink_refund() throws InterruptedException {
        riwayatBooking.clickOnRefundLink();
    }

}
