package steps.mamikos.booking;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.et.Ja;
import io.cucumber.java.sl.In;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.tenant.booking.BaruDilihatBookingPO;
import pageobjects.mamikos.tenant.booking.BookingListPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import static org.testng.Assert.*;

public class BaruDilihatSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HeaderPO header = new HeaderPO(driver);
    private BookingListPO booking = new BookingListPO(driver);
    private BaruDilihatBookingPO baruDilihat = new BaruDilihatBookingPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);


    //Test data OB
    private String propertyFile1="src/test/resources/testdata/mamikos/OB.properties";
    private String obOwnerOnly="src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
    private String kostNameBaruDilihat = JavaHelpers.getPropertyValue(propertyFile1,"kostNameDetailBaruDilihat_" + Constants.ENV);
    private String kostPriceBaruDilihat = JavaHelpers.getPropertyValue(propertyFile1,"kostBaruDilihatPrice_" + Constants.ENV);
    private String tanggalMasukBarudilihatBaruDilihat = JavaHelpers.getPropertyValue(propertyFile1,"kostBaruDilihatTanggalMasuk_" + Constants.ENV);
    private String durationBaruDilihatBaruDilihat = JavaHelpers.getPropertyValue(propertyFile1,"kostBaruDilihatDurasi_" + Constants.ENV);
    private String durationDraftBooking = JavaHelpers.getPropertyValue(propertyFile1,"kostDraftBookingDurasi_" + Constants.ENV);
    private String tenantphonenumber = JavaHelpers.getPropertyValue(propertyFile1,"tenantPhoneNumber_" + Constants.ENV);
    private String fullRoom = JavaHelpers.getPropertyValue(propertyFile1,"kostPenuh_" + Constants.ENV);
    private String newlySeenKostName = JavaHelpers.getPropertyValue(obOwnerOnly, "wendyWildRiftKostFullName_" + Constants.ENV);


    @And("user clicks on Baru Dilihat section")
        public void userClickOnBaruDilihatSection() throws InterruptedException {
            selenium.hardWait(5);
            baruDilihat.baruDilihatSection();
            selenium.hardWait(3);
        }

    @Then("user can see Empty Baru Dilihat information and can clicks on Cari Kost button")
    public void userCanSeeEmptyBaruDilihatInformation() throws InterruptedException {
        Assert.assertEquals(baruDilihat.getTextBaruDilihat(), "Belum ada Kos yang baru kamu lihat. Yuk, mulai cari & booking Kos yang kamu inginkan");
        selenium.hardWait(3);
        baruDilihat.clickCariKostButton();
        selenium.hardWait(3);
        Assert.assertTrue(selenium.getURL().contains("/cari"));
        selenium.hardWait(3);
        baruDilihat.checkAndClickFtuePromoNgebut();
        baruDilihat.checkAndClickFtueBookingLangsung();
    }

    @Then("user can see Kost name, rent count type for Bulanan, Tanggal masuk, durasi sewa on baru dilihat page")
    public void userCanSeeKostNameRentCountTypeforBulananTanggalMasukDurasiSewaOnBaruDilihatPage()  {
        Assert.assertEquals(baruDilihat.getKostName(),kostNameBaruDilihat,"kost name doesn't match");
        Assert.assertEquals(baruDilihat.getPriceBaruDilihat(),kostPriceBaruDilihat,"kost price doesn't match");
        Assert.assertEquals(baruDilihat.getTanggalMasuk(),tanggalMasukBarudilihatBaruDilihat, "tanggal masuk doesn't match");
        Assert.assertEquals(baruDilihat.getDurasi(),durationBaruDilihatBaruDilihat,"durasi doesn't match");
    }

    @And("I clicks on Search menu on Navbar")
    public void iClicksOnSearchMenuOnNavbar() {
        baruDilihat.clickOnSearchBar();
    }


    @And("user clicks on Lihat detail button")
    public void userCliksOnLihatDetailButton () throws InterruptedException {
        selenium.hardWait(5);
        baruDilihat.clickLihatDetailButton();
        selenium.switchToWindow(2);
    }

    @Then("I should reached kos baru dilihat page")
    public void IShouldReachedKosBaruDilihatPage() throws InterruptedException{
        baruDilihat.getUrlcurrentwindows();
        selenium.hardWait(2);
        assertTrue(baruDilihat.isInKosDetailBaruDilihat(), "You are not in kos detail page");
        selenium.hardWait(3);
    }

    @And("I clicks on Booking Langsung button")
    public void iClickOnBookingLangsungButton() throws InterruptedException{

        selenium.hardWait(2);
        baruDilihat.clickOnBookingLangsung();
    }

    @Then("I can see Booking form page")
    public void iCanSeeBookingFormPage() throws InterruptedException{
        assertTrue(baruDilihat.isInBookingFormPage(),"You are not in booking form page");
    }

    @And("I clicks on Kembali button")
    public void iClicksOnKembaliButton() throws InterruptedException{
        selenium.hardWait(3);
        baruDilihat.clickBackButtonOnBookingForm();
        baruDilihat.isFTUECancelPresent();
    }

    @And("I clicks on Simpan Draft button")
    public void iCLicksOnSimpanDraftButton() throws InterruptedException{
        selenium.hardWait(3);
        baruDilihat.clickOnSimpanDraft();
    }

    @And("user clicks on Draft section")
    public void userClicksOnDraftSection() throws InterruptedException{
        selenium.hardWait(3);
        baruDilihat.clickDraftBookingSection();
    }

    @Then("user can see Kost name, rent count type for Bulanan, Tanggal masuk, durasi sewa on draft page")
    public void userCanSeeKostNameRentCountTypeforBulananTanggalMasukDurasiSewa() throws InterruptedException{
        selenium.hardWait(3);
        Assert.assertEquals(baruDilihat.getKostName(),kostNameBaruDilihat,"kost name doesn't match");
        Assert.assertEquals(baruDilihat.getPriceBaruDilihat(),kostPriceBaruDilihat,"kost price doesn't match");
        Assert.assertEquals(baruDilihat.getTanggalMasuk(),tanggalMasukBarudilihatBaruDilihat, "tanggal masuk doesn't match");
        Assert.assertEquals(baruDilihat.getDurasi(),durationDraftBooking,"durasi doesn't match");

    }

    @And("user fills out Rent Duration equals to 4 Bulan")
    public void userFillsOutRentDurationEqualsto4Bulan() throws InterruptedException{
            baruDilihat.increaseRateDurationBaruDilihat();
    }

    @And("user clicks on Next Button on Booking form")
    public void userClicksOnNextButtonOnBookingForm() throws InterruptedException {
        selenium.hardWait(2);
        baruDilihat.selanjutnyaButtonOnForm();
    }

    @When("user input phone number {string}")
    public void userInputPhoneNumber(String phoneNumber) throws InterruptedException {
        baruDilihat.inputTenantPhoneNumber(phoneNumber);
        }

    @Then("booking confirmation screen displayed with  Kost name , Tanggal masuk, Durasi, Detail Pembayara")
    public void bookingConfirmationScreenDisplayedWithKostNameTanggalMasukDurasiDetailPembayaran() throws InterruptedException{
        assertEquals(baruDilihat.getKostName(),kostNameBaruDilihat,"kost name doesn't match");

    }

    @And("I click on trash icon")
    public void iClickOnTrashIcon() throws InterruptedException{
        baruDilihat.isFTUEDeletePresent();
        selenium.hardWait(3);
        baruDilihat.clickOnTrashIcon();
    }

    @And("I clicks on Batalkan button")
    public void iClicksOnBatalkanButton() throws InterruptedException{
        baruDilihat.clickOnCancelDeleteButton();
    }

    @Then("user can see confirmation delete popup")
    public void userCanDoesntSeeConfirmationDeletePopup() throws InterruptedException{
        assertFalse(baruDilihat.isFTUEDeletePresent(), "popup is present");
    }

    @And("I clicks on Hapus button")
    public void iClicksOnHapusButton() throws InterruptedException{
        baruDilihat.clickOnConfirmDeleteBooking();
    }

    @Then("user doesn't see kost name on Baru Dilihat")
    public void userDoesntSeeKostNameOnBaruDilihat() throws InterruptedException{
        assertFalse(baruDilihat.isKostNameVisible(), "kost name still appear on baru dilihat");
        selenium.hardWait(4);
    }

    @And("user can see available room")
    public void userCanSeeAvailableRoom() throws InterruptedException{
        assertEquals(baruDilihat.getFullRoom(), "Kamar Penuh", "not full room");
        selenium.hardWait(3);
    }

    @Then("I should reached booking form page")
    public void iShouldReachedBookingFormPage() throws InterruptedException{
        assertTrue(baruDilihat.isInKostDetail(), "You are not in booking form page");
    }

    @Then ("I can see disable button for Kost Penuh")
    public void iCanSeeDisableButtoForKostPenuh() throws InterruptedException{
        selenium.hardWait(3);
        assertEquals(baruDilihat.getBookingStatus(),"Kamar Penuh", "user can see Booking Langsung");
    }

    @Then("tenant can sees kost name in list number {int} is {string}")
    public void tenant_can_sees_kost_name_in_list_number_is(Integer index, String kostName) {
        String kostNameAssert;
        if(kostName.equalsIgnoreCase("newly seen")) {
            kostNameAssert = newlySeenKostName;
        }
        else {
            kostNameAssert = kostName;
        }
        assertEquals(baruDilihat.getKostNameIndex(1), kostNameAssert, "Kost name is not equals to " + kostName);
    }

    @When("tenant clicks on direct booking button in list number {int}")
    public void tenant_clicks_on_direct_booking_button_in_list_number(Integer index) throws InterruptedException {
        baruDilihat.clicksOnDirectBookingIndex(index);
    }

    @When("tenant delete all newly seen from the list")
    public void tenant_delete_all_newly_seen_from_the_list() throws InterruptedException {
        baruDilihat.deleteAllNewlySeen();
    }
  }
