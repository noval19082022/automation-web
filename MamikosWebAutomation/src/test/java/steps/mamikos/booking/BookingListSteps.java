package steps.mamikos.booking;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikos.tenant.booking.BookingListPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class BookingListSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private BookingListPO booking = new BookingListPO(driver);

    //Test Data Payment
    private String weeklyPayment="src/test/resources/testdata/mamikos/payment.properties";
    private String _bookingStatus_needPayment = JavaHelpers.getPropertyValue(weeklyPayment,"bookingStatus_needPayment_" + Constants.ENV);
    private String _bookingStatus_paid = JavaHelpers.getPropertyValue(weeklyPayment,"bookingStatus_paid_" + Constants.ENV);
    private String _propertyName = JavaHelpers.getPropertyValue(weeklyPayment,"propertyName_" + Constants.ENV);
    private String _propertyNameBNI = JavaHelpers.getPropertyValue(weeklyPayment,"propertyName_BNI_" + Constants.ENV);

    //Test Data Loyalty
    private String voucherkuData="src/test/resources/testdata/mamikos/voucherku.properties";
    private String kostNameForApplyVoucherBBK = JavaHelpers.getPropertyValue(voucherkuData,"kostNameForApplyVoucherBBK_" + Constants.ENV);

    @When("user click pay know button with rental period is {string}")
    public void user_click_pay_know_button_with_rental_period_is(String rentalPeriod) throws InterruptedException {
        int numberOfList = booking.getNumberListOfBooking();

        for (int i = 1; i <= numberOfList; i++) {
            if (booking.getBookingStatus(i).equals(_bookingStatus_needPayment) && booking.getPropertyName(i).equals(_propertyName) && booking.getRentalPeriod(i).equals(rentalPeriod)) {
                booking.clickOnPayNowButton(i);
                break;
            }
        }
    }

    @When("user click check in button with rental period is {string}")
    public void user_click_check_in_button_with_rental_period_is(String rentalPeriod) throws InterruptedException {
        int numberOfList = booking.getNumberListOfBooking();

        for (int i = 1; i <= numberOfList; i++) {
            if(booking.getBookingStatus(i).equals(_bookingStatus_paid) && booking.getPropertyName(i).equals(_propertyName) && booking.getRentalPeriod(i).equals(rentalPeriod)) {
                booking.clickOnCheckIn(i);
                break;
            }
            else if(booking.getBookingStatus(i).equals(_bookingStatus_paid) && booking.getPropertyName(i).equals(_propertyNameBNI) && booking.getRentalPeriod(i).equals(rentalPeriod)) {
                booking.clickOnCheckIn(i);
                break;
            }
            else if(booking.getBookingStatus(i).equals(_bookingStatus_paid) && booking.getPropertyName(i).equals(kostNameForApplyVoucherBBK) && booking.getRentalPeriod(i).equals(rentalPeriod)) {
                booking.clickOnCheckIn(i);
                break;
            }
        }
    }

    @When("user click check in button with rental period is {string} for repayment")
    public void user_click_check_in_button_with_rental_period_is_for_repayment(String rentalPeriod) throws InterruptedException {
        int numberOfList = booking.getNumberListOfBooking();

        for (int i = 1; i <= numberOfList; i++) {
            if(booking.getBookingStatus(i).equals(_bookingStatus_paid) && booking.getPropertyName(i).equals(_propertyName) && booking.getRentalPeriod(i).equals(rentalPeriod)) {
                booking.clickOnCheckInAfterRepayment(i);
                break;
            } else if (booking.getBookingStatus(i).equals(_bookingStatus_paid) && booking.getPropertyName(i).equals(_propertyNameBNI) && booking.getRentalPeriod(i).equals(rentalPeriod)) {
                booking.clickOnCheckInAfterRepayment(i);
                break;
            }
        }
    }

    @And("user click button bayar sekarang for repayment")
    public void user_click_button_bayar_sekarang_for_repayment() throws InterruptedException{
        booking.clickOnCheckInAfterRepayment(1);
    }

    @And("user click Check in at Riwayat and Draft Booking page")
    public void user_click_check_in_at_Riwayat_and_Draft_Booking_page() throws InterruptedException {
        booking.clickOnCheckIn(1);
    }
}
