package steps.mamikos.booking;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.booking.ManageBookingPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class OwnerBookingListingSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private ManageBookingPO bookingManage = new ManageBookingPO(driver);
    private JavaHelpers java = new JavaHelpers();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    //Test Data
    private String propertyFile1="src/test/resources/testdata/mamikos/OB.properties";
    private String obOwnerData="src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
    private String kostName = JavaHelpers.getPropertyValue(propertyFile1,"kostNameBookingFemale_" + Constants.ENV);
    private String duration = JavaHelpers.getPropertyValue(propertyFile1,"duration_" + Constants.ENV);
    private String status = JavaHelpers.getPropertyValue(propertyFile1,"status_" + Constants.ENV);
    private String tenantName;
    private String kostNameRejectBooking = JavaHelpers.getPropertyValue(obOwnerData, "wendyWildRitKostName_" + Constants.ENV);
    private String kostDOTF = JavaHelpers.getPropertyValue(obOwnerData, "wildRiftDOTF_" + Constants.ENV);
    private String kostPromoNgebut = JavaHelpers.getPropertyValue(propertyFile1, "kostPromoNgebut_" + Constants.ENV);
    private String kostTenantNotification = JavaHelpers.getPropertyValue(propertyFile1, "kostMhsValidation_" + Constants.ENV);
    private String kostDpRejected = JavaHelpers.getPropertyValue(obOwnerData, "ancientWeb_" + Constants.ENV);
    private String kostBookingAdditional = JavaHelpers.getPropertyValue(obOwnerData, "kostBookingAddtional_" + Constants.ENV);
    private String obKostSayaHomepageReject = JavaHelpers.getPropertyValue(propertyFile1, "kosHomepageReject_" + Constants.ENV);
    private String kostRejectFullReason = JavaHelpers.getPropertyValue(obOwnerData, "kostRejectFull_" + Constants.ENV);
    private String obKostSayaHomepageRejectNWaiting = JavaHelpers.getPropertyValue(propertyFile1, "kostReject&Waiting_" + Constants.ENV);
    private String ownerRejectFullReason = JavaHelpers.getPropertyValue(obOwnerData, "ownerRejectFullReason_" + Constants.ENV);

    //Teng test data
    private String tengOwner = "src/test/resources/testdata/mamikos/tenant-engagement-owner.properties";
    private String kostFirstInvoiceName = JavaHelpers.getPropertyValue(tengOwner, "adminFirstInvoiceKostName_" + Constants.ENV);
    private String kostSettlementInvoiceName = JavaHelpers.getPropertyValue(tengOwner, "settlementInvoiceKostName_" + Constants.ENV);
    private String kostInvoiceDetail = JavaHelpers.getPropertyValue(tengOwner, "invoiceDetailKostName_" + Constants.ENV);
    private String kostInvoiceDetailDP = JavaHelpers.getPropertyValue(tengOwner, "invoiceDetailDPKostName_" + Constants.ENV);
    private String invoiceDetailKostDPDepositAdditionalPrice = JavaHelpers.getPropertyValue(tengOwner, "invoiceDetailDPDepositAddFee_" + Constants.ENV);
    private String addsOnKost = JavaHelpers.getPropertyValue(tengOwner, "addsOnKost_" + Constants.ENV);
    private String tengKostApik = JavaHelpers.getPropertyValue(tengOwner, "tengKostApik_" + Constants.ENV);

    @Then("booking is displayed with {string} ,Status , Room name  ,Checkin Date as tomorrow date, Duration on Booking listing page")
    public void bookingIsDisplayedWithNameStatusRoomNameCheckinDateAsTomorrowDateDurationOnBookingListingPage(String name) throws ParseException {
        if(name.equalsIgnoreCase("master name")) {
            tenantName = Constants.TENANT_FACEBOOK_NAME;
        }
        else if (name.equalsIgnoreCase("male gender name")) {
            tenantName = JavaHelpers.getPropertyValue(propertyFile1, "tenant_facebook_name_gender_male_" + Constants.ENV);
        }
        else if (name.equalsIgnoreCase("female gender name")) {
            tenantName = JavaHelpers.getPropertyValue(propertyFile1, "tenant_booking_full_flow_name_" + Constants.ENV);
        }
        Assert.assertEquals(bookingManage.getTenantName(1), tenantName,"Tenant name doesn't match");
        Assert.assertEquals(bookingManage.getBookingStatus(1), status,"Status doesn't match");
        Assert.assertEquals(bookingManage.getRoomName(1), kostName,"Room name doesn't match");
        String tomorrowDate = java.updateTimeLocal("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "d MMMM yyyy", "en", 0, 1, 0, 0, 0);
        //Comment for a while due to element is hard to maintain
        //Assert.assertEquals(bookingManage.getCheckInDate(1), tomorrowDate,"Check In date doesn't match");
        Assert.assertEquals(bookingManage.getRentDuration(1), duration,"Duration doesn't match");
    }

    @When("user clicks on Booking Details button")
    public void userClicksOnBookingDetailsButton() throws InterruptedException {
        bookingManage.clickOnBookingDetailsButton();
    }

    @When("user select kost with name {string}")
    public void user_select_kost_with_name(String kostName) throws InterruptedException {
        switch (kostName.toLowerCase()) {
            case "owner test reject":
                this.kostName = kostNameRejectBooking;
                break;
            case "dotf kost":
                this.kostName = kostDOTF;
                break;
            case "admin first invoice":
                this.kostName = kostFirstInvoiceName;
                break;
            case "settlement invoice kost":
                this.kostName = kostSettlementInvoiceName;
                break;
            case "teng invoice kost detail":
                this.kostName = kostInvoiceDetail;
                break;
            case "teng invoice kost detail dp":
                this.kostName = kostInvoiceDetailDP;
                break;
            case "teng invoice detail kost dp deposit additional price":
                this.kostName = invoiceDetailKostDPDepositAdditionalPrice;
                break;
            case "teng adds on kost":
                this.kostName = addsOnKost;
                break;
            case "ob kost promo ngebut":
                this.kostName = kostPromoNgebut;
                break;
            case "ob kost tenant notification":
                this.kostName = kostTenantNotification;
                break;
            case "ob kost with dp rejected":
                this.kostName = kostDpRejected;
                break;
            case "ob kost booking additional":
                this.kostName = kostBookingAdditional;
                break;
            case "teng kost apik":
                this.kostName = tengKostApik;
                break;
            case "owner reject full reason":
                this.kostName = ownerRejectFullReason;;
                break;
            case "ob kost saya homepage reject":
                this.kostName = obKostSayaHomepageReject;
                break;
            case "ob kost saya homepage reject n waiting":
                this.kostName = obKostSayaHomepageRejectNWaiting;
                break;
            default:
                this.kostName = kostName;
        }
        bookingManage.selectKostFromFilter(this.kostName);
        bookingManage.loadingHandler();
    }

    @When("owner process to reject booking request")
    public void owner_process_to_reject_booking_request() throws InterruptedException {
        bookingManage.clickOnRejectBooking();
    }

    @When("user activated rejected booking request filter")
    public void user_activated_rejected_booking_request_filter() throws InterruptedException {
        bookingManage.activatedYourRejectedFilter();
    }

    @Then("user can sees rejected tenant with name {string}")
    public void user_can_sees_rejected_tenant_with_name(String tenantName) {
        Assert.assertEquals(bookingManage.getFirstListBookingStatus(), "Anda Tolak");
        Assert.assertEquals(bookingManage.getFirstListTenantName(), tenantName);
    }

    @Then("owner can see reason on your reject is {string}")
    public void owner_can_see_reason_on_your_reject_is(String rejectReason) throws InterruptedException {
        bookingManage.clickOnBookingDetailsButton();
        selenium.hardWait(2);
        Assert.assertEquals(bookingManage.getRejectReason(), rejectReason, "Reject reason is not " + rejectReason);
    }
}
