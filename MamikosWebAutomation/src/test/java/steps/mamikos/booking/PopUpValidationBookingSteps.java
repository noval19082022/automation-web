package steps.mamikos.booking;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang.ArrayUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.owner.booking.ManageBookingPO;
import pageobjects.mamikos.tenant.booking.*;
import pageobjects.mamikos.tenant.profile.BookingHistoryPO;
import pageobjects.mamikos.tenant.profile.DraftBookingPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.List;

import static org.testng.Assert.*;

public class PopUpValidationBookingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JavaHelpers java = new JavaHelpers();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private PopUpValidationBookingPO popUpValidation = new PopUpValidationBookingPO(driver);

    @Then("user can see popup confirmation with title content")
    public void user_can_see_popup_confirmation_with_title_content(){
        Assert.assertEquals(popUpValidation.getTitleValidation(), "Belum ada data pekerjaan", "text is not same");
    }

    @And("user can see subtitle content")
    public void user_can_see_subtitle_content() throws InterruptedException {
        Assert.assertEquals(popUpValidation.getSubtitleValidation(), "Kos ini butuh data pekerjaanmu. Mohon masukkan informasi tersebut di halaman profil kamu.", "text is not same");
        popUpValidation.clickClosePopUpValidation();
    }

    @Then("user can see popup confirmation gender with title content")
    public void user_can_see_popup_confirmation_gender_with_title_content(){
        Assert.assertEquals(popUpValidation.getTitleValidation(), "Belum ada data jenis kelamin", "text is not same");
    }

    @And("user can see popup confirmation gender subtitle content")
    public void user_can_see_popup_confirmation_gender_subtitle_content() throws InterruptedException {
        Assert.assertEquals(popUpValidation.getSubtitleValidation(), "Kos ini butuh data jenis kelamin. Mohon masukkan informasi tersebut di halaman profil kamu.", "text is not same");
        popUpValidation.clickClosePopUpValidation();
    }

    @Then("user can see popup confirmation karyawan with title content")
    public void user_can_see_popup_confirmation_karyawan_with_title_content(){
        Assert.assertEquals(popUpValidation.getTitleValidation(), "Kos ini khusus untuk karyawan", "text is not same");
    }

    @And("user can see popup confirmation karyawan subtitle content")
    public void user_can_see_popup_confirmation_karyawan_subtitle_content() throws InterruptedException {
        Assert.assertEquals(popUpValidation.getSubtitleValidation(), "Mohon maaf, hanya penyewa dengan pekerjaan karyawan yang dapat menyewa di sini. Pastikan profilmu sesuai dengan kos ini.", "text is not same");
        popUpValidation.clickClosePopUpValidation();
    }

    @Then("user can see popup confirmation mahasiswa with title content")
    public void user_can_see_popup_confirmation_mahasiswa_with_title_content(){
        Assert.assertEquals(popUpValidation.getTitleValidation(), "Kos ini khusus untuk mahasiswa", "text is not same");
    }

    @And("user can see popup confirmation mahasiswa subtitle content")
    public void user_can_see_popup_confirmation_mahasiswa_subtitle_content() throws InterruptedException {
        Assert.assertEquals(popUpValidation.getSubtitleValidation(), "Mohon maaf, hanya penyewa dengan pekerjaan mahasiswa yang dapat menyewa di sini. Pastikan profilmu sesuai dengan kos ini.", "text is not same");
        popUpValidation.clickClosePopUpValidation();
    }

    @Then("user reach booking form")
    public void user_reach_booking_form(){
        Assert.assertEquals(popUpValidation.getTitleBookingForm(), "Pengajuan Sewa", "you are not in Booking Form");
    }
}
