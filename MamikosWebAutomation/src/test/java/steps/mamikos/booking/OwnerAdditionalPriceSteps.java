package steps.mamikos.booking;

import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.account.LoginPO;
import pageobjects.mamikos.owner.DashboardOwnerPO;
import pageobjects.mamikos.tenant.booking.OwnerAdditionalPricePO;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.Constants;
import utilities.ThreadManager;

import java.util.List;

import static org.testng.Assert.*;

public class OwnerAdditionalPriceSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private DashboardOwnerPO dashboardOwner = new DashboardOwnerPO(driver);
    private OwnerAdditionalPricePO ownerAdditionalPrice = new OwnerAdditionalPricePO(driver);
    private LoginPO loginpo = new LoginPO(driver);

    //test data property
    private String obPropertyFile1 = "src/test/resources/testdata/mamikos/OB.properties";
    private String obOwnerOnlyFile = "src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";

    //test data owner ob login
    private String bookingOwnerPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "uprasOwner_" + Constants.ENV);
    private String bookingOwnerPhonePass = JavaHelpers.getPropertyValue(obPropertyFile1, "uprasOwnerPass_" + Constants.ENV);
    private String kostNameAdditionalPrice = JavaHelpers.getPropertyValue(obPropertyFile1, "ancientWeb_" + Constants.ENV);
    private String additionalPriceOwnerPhone = JavaHelpers.getPropertyValue(obPropertyFile1, "ownerWendyAutomation_" + Constants.ENV);
    private String additionalPriceOwnerPass = JavaHelpers.getPropertyValue(obPropertyFile1, "ownerWendyAutomationPass_" + Constants.ENV);
    private String otherPrice = JavaHelpers.getPropertyValue(obOwnerOnlyFile, "ancient_" + Constants.ENV);

    @When("user fills out owner login as {string} and click on Enter button for check additional price")
    public void user_fills_out_login_form_with_credentails_and_and_click_on_Enter_button(String type) throws InterruptedException {
        String phone = "";
        String password = "";

        switch (type) {
            case "ob booking female":
                phone = bookingOwnerPhone;
                password = bookingOwnerPhonePass;
                break;
            case "ob additional price":
                phone = additionalPriceOwnerPhone;
                password = additionalPriceOwnerPass;
                break;
        }

        loginpo.loginAsOwnerToApplication(phone, password);
    }

    @And("user see pop up additional price on dashboard")
    public void user_see_pop_up_additional_price_on_dashboard() throws InterruptedException {
        assertEquals(ownerAdditionalPrice.getCaptionAdditionalPrice(), "Atur Biaya Tambahan di Kos Anda", "Title is not same to " + ownerAdditionalPrice.getCaptionAdditionalPrice());
        assertEquals(ownerAdditionalPrice.getCaptionAdditionalPriceSubtitle(), "Anda sekarang bisa menentukan biaya DP, deposit, denda, dan biaya tambahan lainnya untuk kos Anda.", "Subtitle is not same to " + ownerAdditionalPrice.getCaptionAdditionalPriceSubtitle());
    }

    @And("user clicks Atur Sekarang")
    public void user_clicks_atur_sekarang() throws InterruptedException {
        ownerAdditionalPrice.clickAturSekarang();
    }

    @When("user clicks on kost name {string}")
    public void user_clicks_on_kost_name(String kost) throws InterruptedException {
        String kostName;
        if (kost.equalsIgnoreCase("additional price web")) {
            kostName = kostNameAdditionalPrice;
        }
        else if (kost.equalsIgnoreCase("other price ob")) {
            kostName = otherPrice;
        }
        else {
            kostName = kost;
        }
        ownerAdditionalPrice.clickKostName(kostName);
    }

    @And("user clicks tooltip mamipoin")
    public void userClicksTooltipMamipon() throws InterruptedException {
        dashboardOwner.dismissTooltip();
    }

    @And("user clicks deposit toggle")
    public void userClicksDepositToggle() throws InterruptedException {
        ownerAdditionalPrice.clickDepositToggle();
    }

    @And("user see information about deposit")
    public void userSeeInformationAboutDeposit() {
        assertEquals(ownerAdditionalPrice.getInformationDeposit(), "Biaya deposit adalah jumlah uang yang dititipkan penyewa kepada Anda, sebagai jaminan selama ngekos.", "This is not " + ownerAdditionalPrice.getInformationDeposit());
    }

    @And("user input deposit amount {string}")
    public void userInputDepositAmount(String depositPrice) throws InterruptedException {
        ownerAdditionalPrice.inputAmountDeposit(depositPrice);
    }

//    Need soultion for this step
//    @And("user sees toast as {string}")
//    public void userSeeToastAsX(String toastString) {
//        assertEquals(ownerAdditionalPrice.getToastInputOrEditSuccess(toastString), toastString, "This is not " + ownerAdditionalPrice.getToastInputOrEditSuccess(toastString));
//    }

    @And("user see deposit list {string}")
    public void userSeeDepositList(String depositPrice) throws InterruptedException {
        assertEquals(ownerAdditionalPrice.getDepositsPriceTest(), depositPrice, "Deposit is not active or price is not equal");
    }

    @And("user clicks Ubah {string} button")
    public void userClicksUbahDepositButton(String additionalName) throws InterruptedException {
        if(additionalName.equalsIgnoreCase("deposit")) {
            ownerAdditionalPrice.clickUbahDepositButton();
        }
        else if(additionalName.equalsIgnoreCase("denda")){
            ownerAdditionalPrice.clickUbahDendaButton();
        }

    }

    @And("user input new deposit amount {string}")
    public void userInputNewDepositAmount(String depositPrice) throws InterruptedException {
        ownerAdditionalPrice.inputNewDeposit(depositPrice);
    }

    @And("user delete {string}")
    public void user_delete_x(String additionalName) throws InterruptedException {
        if(additionalName.equalsIgnoreCase("deposit")) {
            ownerAdditionalPrice.deleteDeposit();
        }
        else if(additionalName.equalsIgnoreCase("denda")){
            ownerAdditionalPrice.clickDeleteDendaButton();
        }
    }

    @And("user see header pop up delete confirmation with {string}")
    public void userSeeHeaderPopUpDeleteConfirmation(String additionalName) {
        if(additionalName.equalsIgnoreCase("deposit")) {
            assertEquals(ownerAdditionalPrice.getHeaderDeleteConfirmationDeposit(), "Yakin hapus biaya Deposit?", "Wording is not same to " + ownerAdditionalPrice.getHeaderDeleteConfirmationDeposit());
        }
        else if(additionalName.equalsIgnoreCase("denda")){
            assertEquals(ownerAdditionalPrice.getHeaderDeleteConfirmationDeposit(), "Yakin hapus biaya Denda?", "Wording is not same to " + ownerAdditionalPrice.getHeaderDeleteConfirmationDeposit());

        }
    }

    @And("user clicks Kembali on delete confirmation")
    public void userClicksKembaliOnDeleteConfirmation() throws InterruptedException {
        ownerAdditionalPrice.clickKembaliOnDeleteConfirmation();
    }

    @And("user clicks Hapus on delete confirmation")
    public void userClicksHapusOnDeleteConfirmation() throws InterruptedException {
        ownerAdditionalPrice.clickHapusOnDeleteConfirmation();
    }

    @And("user see toast delete deposit is success")
    public void userSeeToastDeleteDepositIsSuccess() {
        ownerAdditionalPrice.getToastDeletedDeposit();
    }

    @Then("user cannot see {string} on the list")
    public void userCannotSeeDepositOnTheList(String additionalName) {
        if(additionalName.equalsIgnoreCase("deposit")) {
            assertFalse(ownerAdditionalPrice.isDepositListAppears(), "List Deposit is appears");
        }
        else if(additionalName.equalsIgnoreCase("denda")){
            assertFalse(ownerAdditionalPrice.isDendaListAppears(), "List Denda is appears");
        }
    }

    @Then("user can sees default price is {string}")
    public void user_can_sees_default_price_is_x(String currentPercentage) throws InterruptedException{
        String afterTrim = ownerAdditionalPrice.getDownPaymentCurrentPercentage().trim();
        Assert.assertEquals(afterTrim, currentPercentage, "Current selected percentage is not " + currentPercentage);
    }

    @Then("^user can sees down payment price list is :$")
    public void user_can_sees_default_price_and_information_about_downpayment(List<String> priceList) {
        for (int i = 0; i < 4; i++) {
            Assert.assertEquals(ownerAdditionalPrice.getDPPriceList(i + 1), priceList.get(i), "Price list is not including " + priceList.get(i));
        }
    }

    @Then("^user can sees down payment price number is :$")
    public void user_can_sees_down_payment_price_number_is_x(List<String> listNumber) {
        for (int i = 0; i < 4; i++) {
            Assert.assertEquals(ownerAdditionalPrice.getDPPriceListNumber(i+1), listNumber.get(i), "Price list number is " + listNumber);
        }
    }

    @When("user clicks on down payment toggle")
    public void user_clicks_on_down_payment_toggle() throws InterruptedException {
        ownerAdditionalPrice.clickDownPaymentToggle();
    }

    @When("user clicks save button on additional price")
    public void user_clicks_save_button_on_additional_price() throws InterruptedException {
        ownerAdditionalPrice.clickOnSaveButton();
    }
    
    @Then("user can sees active DP is {string} with price range is {string}")
    public void user_can_sees_down_payment_toggle_is_in_active_state(String percentage, String priceRange) {
        assertEquals(ownerAdditionalPrice.getDPPricePercentage(), percentage, "Percentage is not " + percentage);
        assertEquals(ownerAdditionalPrice.getDPPriceRange(), priceRange, "Price range is not " + priceRange);
    }

    @Then("user delete active down payment")
    public void user_delete_active_down_payment() throws InterruptedException {
        ownerAdditionalPrice.clickOnDeleteDownPayment();
        ownerAdditionalPrice.clickHapusOnDeleteConfirmation();
    }

    @And("user clicks denda toggle")
    public void user_clicks_denda_toggle() throws InterruptedException {
        ownerAdditionalPrice.clickOnDendaToggle();
    }

    @And("user see information about denda")
    public void user_see_information_about_denda() {
        ownerAdditionalPrice.getDendaInformation();
    }

    @And("user input denda ammount {string}")
    public void user_input_denda_ammount_x(String dendaPrice) throws InterruptedException{
        ownerAdditionalPrice.inputDendaPrice(dendaPrice);
    }

    @And("user choose Denda time")
    public void user_choose_denda_time_x() throws InterruptedException {
        ownerAdditionalPrice.clickOnSelectFineTime();
    }

    @And("user input dibebankan with {string}")
     public void user_input_dibebankan_with_x(String dendaTime) throws InterruptedException {
        ownerAdditionalPrice.inputDendaTime(dendaTime);
    }

    @And("user see denda list {string}")
    public void user_see_denda_list_x(String dendaPrice) throws InterruptedException {
        assertEquals(ownerAdditionalPrice.getDendaPriceText(), dendaPrice, "Denda is not active or price is not equal");
    }

    @When("user clicks on other price per month toggle")
    public void user_clicks_on_other_price_per_month_toggle(){
        ownerAdditionalPrice.clickOnOtherPriceToggle();
    }

    @When("user input price name with {string}, price with {string}")
    public void user_input_price_name_with_price_with(String name, String price) throws InterruptedException {
        ownerAdditionalPrice.inputOtherPrice(name, price);
    }

    @Then("user can sees other price with name {string} and price {string} show in the list")
    public void user_can_sees_new_other_price_additional_price_show_in_the_list(String name, String price) {
        Assert.assertEquals(ownerAdditionalPrice.getActiveOtherPricesName(), name, "Other price name is not equal with " + name);
        Assert.assertEquals(ownerAdditionalPrice.getActiveOtherPriceNumber(), price, "Other price number is not equal with " + price);
    }

    @When("user delete active other additional price")
    public void user_delete_active_other_additional_price() throws InterruptedException {
        ownerAdditionalPrice.deleteActiveAdditionalPrice();
    }

    @When("user clicks on update price button")
    public void user_clicks_on_update_price_button() throws InterruptedException {
        ownerAdditionalPrice.clickOnUpdatePriceButton();
    }

    @Then("tenant can not sees active other price")
    public void tenant_can_not_sees_active_other_price() {
        Assert.assertFalse(ownerAdditionalPrice.isOtherPriceNamePresent());
        Assert.assertFalse(ownerAdditionalPrice.isOtherPriceNumberPresent());
    }

    @Then("owner can see {string} and {string} as max and min for {string} penalty")
    public void owner_can_see_and_as_max_and_min_for_penalty(String min, String max, String duration) throws InterruptedException {
        ownerAdditionalPrice.choosePenaltyDuration(duration);
        Assert.assertEquals(ownerAdditionalPrice.getPenaltyDurationAmountInputAttributeValue("min"), min);
        Assert.assertEquals(ownerAdditionalPrice.getPenaltyDurationAmountInputAttributeValue("max"), max);
    }

    @Then("user can see DP popup")
    public void user_can_see_dp_pop_up() throws InterruptedException{
        ownerAdditionalPrice.dpPopUpPresent();
    }

    @When("user can see {string} on dp pop up")
    public void user_can_see_on_dp_pop_up(String text) throws InterruptedException{
        Assert.assertEquals(ownerAdditionalPrice.getInformationDP(), text, text + " Not Present");
    }

    @Then("user can see default setting is {string} and see {string}")
    public void user_can_see_default_setting_is_and_see(String tidakWajib, String text) {
        ownerAdditionalPrice.tidakWajibChoicePresent();
        Assert.assertEquals(ownerAdditionalPrice.getInformationTidakWajibDP(), text, text + " Not Present");

    }

    @And("user choose DP setting with {string}")
    public void user_choose_dp_setting_with (String choice) throws InterruptedException{
        ownerAdditionalPrice.clickOnOptionYaorTidak(choice);
    }

    @And("user see {string} on atur page")
    public void user_see_on_atur_page(String text) {
        Assert.assertEquals(ownerAdditionalPrice.getDPInformation(), text, text + " Not Present");

    }
}