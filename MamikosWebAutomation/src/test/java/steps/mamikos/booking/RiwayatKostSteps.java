package steps.mamikos.booking;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.et.Ja;
import io.cucumber.java.sl.In;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.tenant.booking.BaruDilihatBookingPO;
import pageobjects.mamikos.tenant.booking.BookingListPO;
import pageobjects.mamikos.tenant.booking.RiwayatKostPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import static org.testng.Assert.*;

public class RiwayatKostSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private HeaderPO header = new HeaderPO(driver);
    private RiwayatKostPO riwayatKost = new RiwayatKostPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @And("user click Riwayat Kos menu")
    public void user_click_riwayat_kos_menu() throws InterruptedException {
        riwayatKost.clickRiwayatKosMenu();
    }

    @Then("user will see Riwayat Kos page")
    public void user_will_see_riwayat_kos_page(){
        Assert.assertEquals(riwayatKost.getTitleRiwayatKos(), "Riwayat Kos", "You are not in riwayat page");
    }

    @When("user click review kost")
    public void user_click_review_kost() throws InterruptedException {
        riwayatKost.clickReviewKos();
    }

    @And("user click review kost on riwayat kost detail")
    public void user_click_review_kost_on_riwayat_kost_detail() throws InterruptedException {
        riwayatKost.clickReviewKostOnRiwayatKostDetail();
    }

    @Then("user will see review page and user click close on review page")
    public void user_will_see_review_page_and_user_click_close_on_review_page() throws InterruptedException {
        Assert.assertEquals(riwayatKost.getContainOfReviewKost(), "Tulis review:", "You are not in review kost page");
        riwayatKost.clickCloseRiwayatKost();
    }

    @When("user click Lihat Detail")
    public void user_click_lihat_detail() throws InterruptedException {
        riwayatKost.clickLihatDetail();
    }

    @And("user click Lihat Fasilitas and close pop up fasilitas")
    public void user_click_lihat_fasilitas_and_close_pop_up_fasilitas() throws InterruptedException {
        riwayatKost.clickLihatFasilitas();
        riwayatKost.clickCloseRiwayatKost();
    }

    @And("user click Lihat Riwayat Transaksi and user click Kembali ke Booking button")
    public void user_click_lihat_riwayat_transaksi_and_user_click_kembali_ke_booking_button() throws InterruptedException {
        riwayatKost.clickLihatRiwayatTransaksi();
        riwayatKost.clickKembaliKeBooking();
    }

    @And("user click Chat Pemilik")
    public void user_click_chat_pemilik() throws InterruptedException {
        riwayatKost.clickChatPemilik();
    }

    @Then("user will see Chat room opened and close Chat room")
    public void user_will_see_chat_room_opened_and_close_chat_room() throws InterruptedException {
        riwayatKost.clickBackAndClickCloseChatRoom();
    }

    @When("user click Booking Ulang")
    public void user_click_booking_ulang() throws InterruptedException {
        riwayatKost.clickBookingUlang();
    }

    @Then("user will open new tab and go to Booking form")
    public void user_will_open_new_tab_and_go_to_booking_form(){
        riwayatKost.getTitleBookingForm();
    }

    @Then("user will see empty state")
    public void user_will_see_empty_state(){
        Assert.assertEquals(riwayatKost.getEmptyStateTitle(), "Belum Ada Kos", "Empty state title is not correct");
        Assert.assertEquals(riwayatKost.getEmptyStateSubtitle(), "Semua kos yang pernah kamu sewa di Mamikos nantinya akan muncul di halaman ini.", "Empty state title is not correct");
    }

    @Then("there will be a review menu with the title {string}")
    public void there_will_be_a_review_menu_with_the_title(String title){
        Assert.assertEquals(riwayatKost.getTitleRiwayatKosReviewText().replaceAll("\n",""),title, "title is not correct");
    }

    @When("user click review on riwayat kos page")
    public void user_click_review_on_riwayat_kos_page() throws InterruptedException {
        riwayatKost.clickOnRiwayatKosReviewText();
    }

    @Then("user will see review form")
    public void user_will_see_review_form(){
        Assert.assertEquals(riwayatKost.getTitleReviewText(),"Tulis review kamu lebih lanjut\n*", "title is not correct");
    }

    @And("user verify Kost Review entry point is not displayed")
    public void user_verify_Kost_Review_entry_point_is_not_displayed() throws InterruptedException {
        Assert.assertFalse(riwayatKost.isKostReviewEntryPointNotDisplayed());
    }

}