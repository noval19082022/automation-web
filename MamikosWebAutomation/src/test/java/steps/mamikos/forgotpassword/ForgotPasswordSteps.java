package steps.mamikos.forgotpassword;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.forgotpassword.ForgotPasswordPO;
import utilities.ThreadManager;


public class ForgotPasswordSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private ForgotPasswordPO forgotPasswordPO = new ForgotPasswordPO(driver);

    @And("user fill their registered phone number {string} and click send button")
    public void user_fill_their_registered_phone_number_and_click_send_button(String phone) throws InterruptedException {
        forgotPasswordPO.fillOutRegisteredPhoneNumberAndClickSendButton(phone);
    }

    @Then("user verify and click button resend OTP {string}")
    public void user_verify_and_click_button_resend_OTP(String text) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getResendOTPButton(), text, "Code verification text is not equal to " + text);
        forgotPasswordPO.waitAndClickResendOTP();
    }

    @And("user click otp via sms on page {string}")
    public void user_click_otp_via_sms(String title) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getTitlePage(), title, "Page title is not equal to " + title);
        forgotPasswordPO.clicksendOTPviaSMS();
    }

    @Then("user click otp via wa on page {string}")
    public void user_click_otp_via_wa(String title) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getTitlePage(), title, "Page title is not equal to " + title);
        forgotPasswordPO.clicksendOTPviaWA();
    }

    @And("user verify on page {string}")
    public void user_verify_on_page(String title) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getTitlePage(), title, "Page title is not equal to " + title);
    }

    @Then("user get error message {string}")
    public void user_get_error_message(String message){
        Assert.assertEquals(forgotPasswordPO.getErrorMessage(), message, "Error message is not equal to " + message);
    }

    @And("user fill their unregistered phone number {string}")
    public void user_fill_their_unregistered_phone_number(String phone) {
        forgotPasswordPO.fillOutUnregisteredPhoneNumber(phone);
    }

    @Then("user verify otp form appear on page {string}")
    public void user_verify_otp_form_appear_on_page(String title) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getTitlePage2(), title, "Page title is not equal to " + title);
    }

    @And("user input invalid OTP {string} {string} {string} {string}")
    public void user_input_invalid_OTP(String code1, String code2, String code3, String code4) throws InterruptedException {
        forgotPasswordPO.fillOTP(code1, code2, code3, code4);
      //  Assert.assertTrue(forgotPasswordPO.isOTPInputAppear(), "OTP Input form is not appear");
    }

    @Then("user verify invalid OTP message {string} {string}")
    public void user_verify_invalid_OTP_message(String message1, String message2) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getOTPErrorMessage().substring(0,22), message1, "OTP error message is not equal to " + message1);
        Assert.assertEquals(forgotPasswordPO.getOTPErrorMessage().substring(23,70), message2, "OTP error message is not equal to " + message2);
    }

    @Then("user see button choose verify method is disabled")
    public void user_see_button_choose_verify_method_is_disabled() {
        Assert.assertFalse(forgotPasswordPO.isSendButtonEnable(), "Send button is still enable");
    }

    @And("user input invalid code otp {string}")
    public void user_input_invalid_code_otp(String otp) throws InterruptedException {
        forgotPasswordPO.fillInvalidOTP(otp);
    }

    @Then("user click back button on page otp")
    public void user_click_back_button_otp() throws InterruptedException {
        forgotPasswordPO.clickBackbutton();
    }

    @Then("user see popup verifikasi batalkan proses {string}")
    public void user_see_popup_verifikasi_batalkan(String textPopup) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getPopupBatalkanProcess(), textPopup);
    }

    @Then("user click ya, batalkan")
    public void user_click_batalkan_process() throws InterruptedException {
        forgotPasswordPO.clickBatalkanbutton();
    }

    @Then("user see toast message {string} {string} in forgot password")
    public void user_see_toast_message(String toastMessage1, String toastMessage2) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.toastMessageText().substring(0,12), toastMessage1, "OTP error message is not equal to " + toastMessage1);
        Assert.assertEquals(forgotPasswordPO.toastMessageText().substring(16,61), toastMessage2, "OTP error message is not equal to " + toastMessage2);
    }

    @Then("user verify otp form appear on page OTP {string}")
    public void user_verify_otp_form_appear_on_page_otp(String title) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getTitlePage(), title, "Page title is not equal to " + title);
    }

    @When("user click underline {string}")
    public void user_click_underline_cs_wa(String csWA) throws InterruptedException {
        forgotPasswordPO.clickCsWaButton(csWA);
    }

    @Then("user directed to wa and verify pretext {string}")
    public void user_directed_and_verify_pretext_wa(String title) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getpretextWa(), title, "Page title is not equal to " + title);
    }

    @Then("user verify otp form appear on page send OTP {string}")
    public void user_verify_send_otp(String title) throws InterruptedException {
        Assert.assertEquals(forgotPasswordPO.getOtptext(), title, "Page title is not equal to " + title);
    }
}
