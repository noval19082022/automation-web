package steps.mamikos.jobvacancy;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import pageobjects.mamikos.jobvacancy.JobVacancyDetailPO;
import pageobjects.mamikos.jobvacancy.LandingJobPO;
import utilities.ThreadManager;

public class JobVacancyDetailSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JobVacancyDetailPO job = new JobVacancyDetailPO(driver);
    private LandingJobPO LandingJob = new LandingJobPO(driver);

    @When("user clicks on Search Ads Dropdown")
    public void userClicksOnSearchAdsDropdown() throws InterruptedException{
        job.clickOnSearchAdsDropDown();
    }

    @And("user clicks on Job Vacancy Dropdown Item List")
    public void userClicksOnJobVacancyItemList() throws InterruptedException {
        job.clickOnJobVacancyItemList();
    }

    @When("user clicks on Job Vacancy List with Vacancy Title {string}")
    public void userClicksOnOneOfJobVacancyList(String jobName) throws InterruptedException {
        job.clickOneOfJobVacancies(jobName);
    }

    @And("user clicks on Apply Job")
    public void userClicksOnApplyJob() throws InterruptedException {
        job.clickOnApplyJobButton();
    }

    @And("user clicks on Next Button")
    public void userClicksOnNextButton() throws InterruptedException {
        job.clickOnNextButton();
    }

    @And("user fill Owner Address field with {string}")
    public void userFillOwnerAddressFieldWith(String ownerAddress) {
        job.fillPropertyAddress(ownerAddress);
    }

    @And("user fill Last Education {string}, Skill Description {string}, Work Experience {string}, Last Salary {string} and Expected Salary {string}")
    public void userFillLastEducationSkillDescriptionWorkExperienceLastSalaryExpectedSalaryAndUploadCVPath(String lastEdu, String skillDesc, String workExp, String lastSal, String expectedSal) throws InterruptedException {
        job.fillSecondForm(lastEdu, skillDesc, workExp, lastSal, expectedSal);
    }

    @Then("user verify Job Application status is success {string} {string}")
    public void userVerifyJobApplicationStatusIsSuccess(String modalTitle, String modalContent) {
        Assert.assertEquals(job.getSuccessModalTitle(), modalTitle, "Modal Title is not equal to " + modalTitle);
        Assert.assertEquals(job.getSuccessModalContent(), modalContent, "Modal Content is not equal to " + modalContent);
    }

    @And("user click modal confirmation button")
    public void userClickModalConfirmationButton() throws InterruptedException {
        job.clickOnModalButtonConfirm();
    }

    @Then("user check the Apply Job Button and the button text changed to {string} and Apply Job Button is disabled")
    public void userCheckTheApplyJobButtonAndTheButtonTextChangedToAndApplyJobButtonIs(String buttonStat) {
        Assert.assertEquals(job.getApplyJobButtonText(), buttonStat, "Button Text is not equal to " + buttonStat);
        Assert.assertTrue(job.isGetApplyJobButtonDisabled(),"This Button is not disabled");
    }

    @Then("user verify Company Name {string}, Company Address {string}, Vacancy Name {string} and Last Education {string}")
    public void userVerifyCompanyNameCompanyAddressVacancyNameAndLastEducation(String companyName, String companyAddress, String vacancyName, String lastEdu) {
        Assert.assertEquals(job.getCompanyName(), companyName, "Company Name is not equal to " + companyName);
        Assert.assertEquals(job.getCompanyAddress(), companyAddress, "Company Address is not equal to " + companyAddress);
        Assert.assertEquals(job.getVacancyName(), vacancyName, "Vacancy Name is not equal to " + vacancyName);
        Assert.assertEquals(job.getLastEducation(), lastEdu, "Last Education is not equal to " + lastEdu);
    }

    @And("user click on Show Job Location button")
    public void userClickOnShowJobLocationButton() throws InterruptedException {
        job.clickOnShowLocationButton();
    }

    @Then("user verify that job is inactive {string}")
    public void userVerifyThatJobIsInactive(String inactive) {
        Assert.assertEquals(job.getInactiveMessage(), inactive, "Page Message is not equal to " + inactive);
    }

    @And("user clicks on the first job")
    public void user_clicks_on_the_first_job() throws InterruptedException {
        LandingJob.clickOnExpDateButton();
    }

    @Then("user navigates to jobs details")
    public void user_navigates_to_jobs_details() {
        Assert.assertTrue(LandingJob.isJobTitleAppear(), "Judul Beda");
    }

    @And("user click on search ads")
    public void user_click_on_search_ads() throws InterruptedException {
        LandingJob.clickSearchAdsButton();
    }

    @Given("user click on search jobs")
    public void user_click_on_search_jobs() throws InterruptedException {
        LandingJob.clickSearchAdsButton();
    }

    @When("user clicks on Another Jobs Ads")
    public void user_clicks_on_another_JobsAds() throws InterruptedException{
        LandingJob.clickAnotherJobsButton();
    }

    @When("user clicks on another job ads")
    public void user_clicks_on_another_job_ads() throws InterruptedException {
        LandingJob.clickAnotherJobsButton();
    }
}
