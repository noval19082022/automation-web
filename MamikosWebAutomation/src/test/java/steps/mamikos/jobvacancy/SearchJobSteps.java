package steps.mamikos.jobvacancy;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pageobjects.mamikos.jobvacancy.SearchJobPO;
import utilities.ThreadManager;

public class SearchJobSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SearchJobPO searchJob = new SearchJobPO(driver);

    @And("user clicks on search bar")
    public void userClicksOnSearchBar() throws InterruptedException {
        searchJob.clickOnSearchBar();
    }

    @Then("User set text with keyword {string}")
    public void userSetTextWithKeyword(String keyword) throws InterruptedException {
        searchJob.setTextOnSearchBar(keyword);

    }

    @Then("should display the result job list")
    public void shouldDisplayTheResultJobList() {
        Assert.assertTrue(searchJob.isSearchResultPresent(), "Expected result is not equals!!");
    }

    @And("user clicks on job status filter dropdown")
    public void userClicksOnJobStatusFilterDropdown() throws InterruptedException {
        searchJob.clickOnJobStatusFilter();
    }

    @Then("display the list of job status :")
    public void displayTheListOfJobStatus(List<String> jobStatus) {
        List<String> webJobStatus = searchJob.listJobStatus();
        Assert.assertEquals(webJobStatus, jobStatus, "Job Status not match");
    }

    @Then("display the list of job status is {string}")
    public void display_the_list_of_job_status_is(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @And("user clicks on last education filter dropdown")
    public void userClicksOnLastEducationFilterDropdown() throws InterruptedException {
        searchJob.clickOnLastEducationFilter();
    }

    @Then("display the list of education :")
    public void displayTheListOfEducation(List<String> lastEducation) {
        List<String> webLastEducation = searchJob.listEducation();
        Assert.assertEquals(webLastEducation, lastEducation, "Educations not match");
    }

    @And("user clicks on sort by filter dropdown")
    public void userClicksOnSortByFilterDropdown() throws InterruptedException {
        searchJob.clickOnSortByFilter();
    }

    @Then("display the list of sort by filter :")
    public void displayTheListOfSortByFilter(List<String> sortBy) {
        List<String> webSortBy = searchJob.listSortBy();
        Assert.assertEquals(webSortBy, sortBy, "Sort by list filter not match");
    }


    @Then("user check the amount joblist display in first page")
    public void userCheckTheAmountJoblistDisplayInFirstPage() {
        searchJob.getJobListAmountInOnePage();
    }

    @And("check the attribute on job vacancy widget before login")
    public void checkTheAttributeOnJobVacancyWidgetBeforeLogin() throws InterruptedException {
        for(int i = 1; i <= searchJob.getJobListAmountInOnePage(); i++){
            Assert.assertTrue(searchJob.isCompanyLogoImgPresent(i), "Element Company logo list not present!");
            Assert.assertTrue(searchJob.isJobStatusPresent(i), "Element Job Status list not present!");
            Assert.assertTrue(searchJob.isCompanyNamePresent(i), "Element Company name list not present!");
            Assert.assertTrue(searchJob.isApplyViaElementPresent(i), "Element Apply via list not present!");
            Assert.assertTrue(searchJob.isCompanyLocationPresent(i), "Element Location list not present!");
            Assert.assertTrue(searchJob.isSalaryElementPresent(i), "Element salary list not present!");
            Assert.assertTrue(searchJob.isExpiredJobElementPresent(i), "Element Expired date list not present!");
            Assert.assertTrue(searchJob.isPostJobElementPresent(i), "Element Post date list not present!");
        }
    }

    @And("user click on next {int}")
    public void userClickOnNextPage(int page) throws InterruptedException {
        searchJob.clickOnPagination(page);
    }

    @Then("display the job list {string} in on page :")
    public void displayTheJobListNumberInOnPage(String joblistNumber) {
        String jobList = searchJob.getPageAmountNumber();
        String expected = joblistNumber;
        Assert.assertEquals(jobList, expected, "Joblist in one page not match!");
    }
}
