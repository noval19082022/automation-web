package steps.mamikos.chat;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pageobjects.mamikos.account.LoginPO;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.owner.common.ChatPopUpPO;
import pageobjects.mamikos.tenant.ApartmentDetails.ApartmentDetailsPO;
import pageobjects.mamikos.tenant.booking.BookingFormPO;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;
import java.text.ParseException;
import java.util.List;

public class ChatSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private LoginPO login = new LoginPO(driver);
    private KosDetailPO kosDetail = new KosDetailPO(driver);
    private ChatPopUpPO chatPopUp = new ChatPopUpPO(driver);
    private BookingFormPO bookingForm = new BookingFormPO(driver);
    private HeaderPO header = new HeaderPO(driver);
    private KosDetailPO kosDetailPO = new KosDetailPO(driver);
    private ApartmentDetailsPO aptDetail = new ApartmentDetailsPO(driver);
    private static JavaHelpers java = new JavaHelpers();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    //Test Data
    private String chatPropertyFile1="src/test/resources/testdata/mamikos/chat.properties";
    private String chatKostName13 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName13_" + Constants.ENV);
    private String listQuestion = JavaHelpers.getPropertyValue(chatPropertyFile1,"listQuestion_" + Constants.ENV);
    private int imageSize;
    @FindBy(xpath = "//*[@id=\"tooltipContent\"]/div[2]/div/button")
    private WebElement closeTooltipBroadcast;


    @And("user click chat in kos detail")
    public void user_click_chat_in_kos_detail() throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        kosDetail.clickChatKos();
    }

    @Then("user see login pop up")
    public void userSeeLoginPopUp() throws InterruptedException {
        Assert.assertTrue(login.checkLoginPopUp(), "Login pop up not appear");
    }

    @Then("user see phone number field and selectable question options :")
    public void user_see_phone_number_field_and_selectable_question_options(List<String> questions) {
        List<String> questionsList = chatPopUp.listQuestions();
        for (int i=0; i<questions.size(); i++) {
            String expect = questions.get(i).replaceAll("\\s", "");
            Assert.assertEquals(questionsList.get(i), expect, "Question " + i + " not match");
        }
    }

    @And("user select question {string}")
    public void userSelectQuestion(String text) throws InterruptedException {
        chatPopUp.clickQuestion(text);
        chatPopUp.clickSend();
    }

    @Then("chat room appear with latest message {string} or {string}")
    public void chat_room_appear_with_latest_message_or(String message, String message2) throws InterruptedException {
        String lastChat = chatPopUp.getLatestChatText().replaceAll("\\s", "");
        String expected = message.replaceAll("\\s", "");
        String expected2 = message2.replaceAll("\\s", "");
        if(lastChat.contains(expected)){
            Assert.assertTrue(lastChat.contains(expected), "Auto reply text is wrong");
        } else {
            Assert.assertTrue(lastChat.contains(expected2), "Auto reply text is wrong");
        }
    }

    @Then("chat room appear with latest message {string}")
    public void chat_room_appear_with_latest_message(String message) throws InterruptedException {
//        String lastChat = chatPopUp.getLatestChatText().trim().replaceAll("\\s", "");
//        String expected = message.replaceAll("\\s", "");
//        Assert.assertTrue(lastChat.contains(expected), "Auto reply text is wrong");
        Assert.assertTrue(chatPopUp.getLatestChatText().trim().replaceAll("\\s", "")
                .contains(message.replaceAll("\\s", "")), "Kos address in title is wrong");
    }

    @Then("it will redirect to Booking page")
    public void itWillRedirectToBookingPage() {
        Assert.assertTrue(bookingForm.isBookingFormPresent(), "Not redirected to Booking form page");
    }

    @And("send button become {string}")
    public void sendButtonBecome(String label) {
        Assert.assertTrue(chatPopUp.verifySendLabel(label.replaceAll("\\s", "")), "Button text is wrong");
    }

    @And("user enter text {string} in chat page")
    public void userEnterTextInChatPage(String chatMsg) throws InterruptedException {
        chatPopUp.insertChatText(chatMsg);
    }

    @And("owner/tenant user click chat from {string} in chatlist")
    public void ownerUserClickChatFromInChatlist(String title) throws InterruptedException {
        if(title.equalsIgnoreCase("tenant"))
        {
            title = Constants.TENANT_FACEBOOK_NAME;
        }
        else if (title.equalsIgnoreCase("owner"))
        {
            title = chatKostName13;
        }
        chatPopUp.clickChatRoomRoom(title);
        if (chatPopUp.clickNoChatRoomRoom()){
            chatPopUp.clickChatRoomRoom2(title);
        }
    }

    @And("user close chat pop up")
    public void userCloseChatPopUp() throws InterruptedException {
        chatPopUp.clickBack();
        chatPopUp.clickClose();
    }

    @When("tenant user navigates to Chat page")
    public void tenantUserNavigatesToChatPage() throws InterruptedException {
        header.clickChat();
    }

    @Then("user close chat pop up without back button")
    public void user_close_chat_pop_up_without_back_button() throws InterruptedException {
        chatPopUp.clickClose();
    }

    @When("user click Chat in apartment detail")
    public void user_click_chat_in_apartment_detail() throws InterruptedException {
        aptDetail.clickChatApt();
    }

    @Then("user see chat list appear")
    public void user_see_chat_list_appear() {
            if  (chatPopUp.isChatListPresent()) {
                Assert.assertTrue(chatPopUp.isChatListPresent(), "Chat list page is not opened");
        }else{
                Assert.assertTrue(chatPopUp.isChatListPresent2(), "Chat list page is not opened");
            }
    }

    @And("user sees the room card is present in chat room")
    public void user_sees_the_room_card_is_present_in_chat_room() {
        Assert.assertTrue(chatPopUp.isRoomCardPresent(), "Room card is not present!");
    }

    @Then("user clicks the Lihat Iklan button and redirect to detail property")
    public void user_clicks_the_lihat_iklan_button_and_redirect_to_detail_property() throws InterruptedException {
        String propName = chatPopUp.getRoomCardPropName();
        chatPopUp.clickLihatIklanButton();
        Assert.assertTrue(kosDetailPO.getTextKostTitle().contains(propName), "The property name is not equals with room card!");
    }

    @Then("user clicks the Booking button and redirect to booking form")
    public void user_clicks_the_booking_button_and_redirect_to_booking_form() throws InterruptedException {
        chatPopUp.clickBookingButton();
        Assert.assertTrue(bookingForm.isBookingFormFromChatPresent(), "Booking form is not present!");
    }

    @Then("user sees the Booking button disable")
    public void user_sees_the_booking_button_disable() {
        Assert.assertTrue(chatPopUp.isBookingButtonDisablePresent(), "booking button is not disable!");
    }

    @And("user sees the room card is NOT present in apartemen chat room")
    public void user_sees_the_room_card_is_NOT_present_in_apartemen_chat_room() {
        Assert.assertFalse(chatPopUp.isRoomCardPresent(), "Room card should not present!");
    }

    @Then("user see Contact us pop up is appear")
    public void user_see_Contact_us_pop_up_is_appear() {
        Assert.assertTrue(chatPopUp.isContactUsPresent(), "Contact CS pop up is not appear");
    }

    @When("user click chat button in top bar")
    public void user_click_chat_button_in_top_bar() throws InterruptedException {
        header.clickChatOwner();
    }
    @And("user click FTUE mars")
    public void click_ftue_mars() throws InterruptedException {
        header.FTUE();
    }

    @And("search chat in chatlist {string}")
    public void search_chat_in_chatlist(String inputText) throws InterruptedException {
        header.searchChat(inputText);
//        if (header.noClickSearchChat()) {
//            selenium.clickOn(closeTooltipBroadcast);
//            header.searchChat(inputText);
//        }
    }
    @Then("user see chat empty image")
    public void user_see_chat_empty_image() {
        Assert.assertTrue(chatPopUp.isEmptyChatImagePresent(), "Empty chat image is not appear");
    }

    @Then("user see text {string} in empty chat page")
    public void user_see_text_in_empty_chat_page(String text) {
        Assert.assertEquals(chatPopUp.getEmptyChatTitle(), text, "Empty chat title is wrong/missing");
    }

    @Then("user see text {string} in empty chat description")
    public void user_see_text_in_empty_chat_description(String desc) {
        Assert.assertEquals(chatPopUp.getEmptyChatDescription().trim(), desc, "Empty chat description is wrong/missing");
    }

    @Then("user see indicator {string} in bottom of empty chat page")
    public void user_see_indicator_in_bottom_of_empty_chat_page(String text) {
        Assert.assertEquals(chatPopUp.getEmptyChatIndicator(), text, "Empty chat indicator is wrong/missing");
    }

    @When("tenant click on first index chat on chat list")
    public void tenant_click_on_first_index_chat_on_chat_list() throws InterruptedException {
        chatPopUp.clickChatIndexNumber("1");
    }

    @Then("chat room interface is blocked and quota {string}")
    public void chat_room_interface(String quota) throws InterruptedException {
        Assert.assertEquals(chatPopUp.getTextLastQuota(), quota, "1 chat room");
    }

    @Then("chat room interface is no blocked {string}")
    public void chat_room_interface_free_quota(String freeQuota) throws InterruptedException {
        Assert.assertEquals(chatPopUp.getTextFreeQuota(), freeQuota, "Anda bisa berbalas chat tanpa kuota di chat room ini");
    }


    @When("tenant upload image and tap on sent button")
    public void tenant_upload_image_and_tap_on_sent_button() throws InterruptedException {
        imageSize = chatPopUp.getImageSentSize();
        chatPopUp.uploadImageChat();
        chatPopUp.clicksOnSendImageButton();
    }

    @Then("tenant can sees picture is sent")
    public void tenant_can_sees_picture_is_sent() throws InterruptedException {
        chatPopUp.waitTillImageLoadingDissapear();
        Assert.assertTrue(chatPopUp.getImageSentSize() > imageSize);
    }

    @When("user input time survey {string}")
    public void user_input_time_survey(String time) throws InterruptedException {
        chatPopUp.inputSurveyTime(time);
    }

    @When("user click ubah jadwal on survey form")
    public void user_click_ubah_jadwal_on_survey_fprm() throws InterruptedException {
        chatPopUp.clickBtnUbahJadwal();
    }

    @Then("survei text generated with data time survey {string} inputted by user previously")
    public void survei_text_generated_with_data_inputted_by_user_previously(String surveyTime) throws ParseException, InterruptedException {
        String previousChat = chatPopUp.getFormSurveyInputed();
        String surveyDate = java.updateTimeLocal("EEEEE, dd MMMM yyyy",java.getTimeStamp("EEEEE, dd MMMM yyyy"),"EEEEE, dd MMMM yyyy","id",0,1,0,0,0);
        String expected = "Waktu Survei: "+surveyDate+", pukul "+surveyTime.replaceAll(":",".")+" WIB";
        Assert.assertTrue(previousChat.contains(expected), "Auto reply text is wrong");
    }

    //---------------chat label---------------//
    @Then("question {string} is not displayed")
    public void question_is_not_displayed(String question){
        Assert.assertFalse(chatPopUp.isQuestionDisplayed(question), "Question is not displayed");
    }

    @And("owner goes to Chat Page")
    public void owner_goes_to_chat_page() throws InterruptedException {
        chatPopUp.clickChatOnHeader();
    }

    @And("owner clicked that tenant")
    public void owner_clicked_that_tenant() throws InterruptedException {
        chatPopUp.clickChatList();
    }

    @Then("owner can see label with {string}")
    public void owner_can_see_label_with(String label) throws InterruptedException {
        Assert.assertEquals(chatPopUp.getChatLabel(label), chatPopUp.getChatLabel(label), "Chat label does not match");
    }

    @And("user batalkan survey if the survey already submitted")
    public void user_batalkan_survey_if_the_survey_already_submitted() throws InterruptedException {
        chatPopUp.clickOnBatalkanSurvey();
    }

    @And("user click Survei Kos button")
    public void user_click_survei_kos_button() throws InterruptedException {
        chatPopUp.clickOnSurveiKosButton();
    }
    @Given("owner non GP {string}")
    public void user_login_owner_nonGP(String nonGP) throws InterruptedException {
      //  Assert.assertEquals(chatPopUp.ownerNonGp(), "Kos name field is still enable");
        Assert.assertTrue(chatPopUp.getNonGP().trim().replaceAll("\\s", "")
                .contains(nonGP.replaceAll("\\s", "")), "Kos address in title is wrong");
    }

    @Then("user did not see last seen owner on chatroom")
    public void userDidNotSeeLastSeenOwnerOnChatroom() {
        Assert.assertNotEquals(chatPopUp.isOwnerLastSeenChatroomDisplayed(), "Owner Last Seen is displayed");
    }

    @Then("user see last seen owner on chatroom")
    public void userSeeLastSeenOwnerOnChatroom() {
        Assert.assertTrue(chatPopUp.isOwnerLastSeenChatroomDisplayed(), "Owner Last Seen is displayed");
    }

    @Then("user validate message header is {string}")
    public void userValidateMessageHeaderIs(String header) throws InterruptedException {
        String lastChat = chatPopUp.getHeaderChatText().replaceAll("\\s", "");
        String expected = header.replaceAll("\\s", "");
        Assert.assertTrue(lastChat.contains(expected), "Chat Header is not equal");
    }

    @Then("user validate message header is not {string}")
    public void userValidateMessageHeaderIsNot(String header) throws InterruptedException {
        String lastChat = chatPopUp.getHeaderChatText().replaceAll("\\s", "");
        String expected = header.replaceAll("\\s", "");
        Assert.assertNotEquals(lastChat.contains(expected), "Chat Header is not equal");
    }

    @Then("user see FTUE Pop Up before send chat")
    public void userSeeFTUEPopUpBeforeSendChat() {
        Assert.assertTrue(chatPopUp.isDescFTUESendChatPresent(), "FTUE Continue Send Chat Description is not displayed");
        Assert.assertTrue(chatPopUp.isCloseBtnFTUESendChatPresent(), "Close button in FTUE Continue Send Chat is not displayed");
        Assert.assertTrue(chatPopUp.isBackBtnFTUESendChatPresent(), "Back Button in FTUE Continue Send Chat is not displayed");
        Assert.assertTrue(chatPopUp.isContinueBtnFTUESendChatPresent(), "Send Chat Button in FTUE Continue Send Chat is not displayed");
    }

    @And("user click back on FTUE Pop Up before send chat")
    public void userClickBackOnFTUEPopUpBeforeSendChat() {
        chatPopUp.clickBackFTUESendChat();
    }

    @And("user click send chat button")
    public void userClickSendChatButton() {
        chatPopUp.clickSendChat();
    }
}
