package steps.mamikos.mamiads.cekpropertisekitar;

import io.cucumber.java.en.And;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikos.mamiads.cekpropertisekitar.CekPropertiSekitarPO;
import utilities.ThreadManager;

public class CekPropertiSekitarSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private CekPropertiSekitarPO cekPropertiSekitarPO = new CekPropertiSekitarPO(driver);


    @And("user click menu Cek Properti Sekitar")
    public void user_click_menu_cek_properti_sekitar() throws InterruptedException  {
        cekPropertiSekitarPO.clickOnCekPropertiSekitarMenu();
    }
}
