package steps.mamikos.mamiads;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.mamiads.UpgradePremiumPO;
import pageobjects.mamikos.owner.common.AddPopUpPO;
import pageobjects.mamikos.owner.kos.AddKosPO;
import pageobjects.mamikos.owner.kos.KostListPO;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import pageobjects.mamikosAdmin.KostOwnerPO;
import pageobjects.mamikosAdmin.LeftMenuPO;
import pageobjects.mamikosAdmin.LoginPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class UpgradePremiumSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private UpgradePremiumPO upgradePremium = new UpgradePremiumPO(driver);
    private AddKosPO addkos = new AddKosPO(driver);
    private AddPopUpPO popUp = new AddPopUpPO(driver);
    private JavaHelpers java = new JavaHelpers();
    private KostListPO kosList = new KostListPO(driver);
    private String randomTextKosName = "";
    private String randomTextKosType = "";
    private String imageSrc;
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private LoginPO login = new LoginPO(driver);
    private LeftMenuPO leftMenu = new pageobjects.mamikosAdmin.LeftMenuPO(driver);
    private KostOwnerPO kostOwner = new KostOwnerPO(driver);
    private HeaderPO header = new HeaderPO(driver);
    private KosDetailPO kosDetailPO = new KosDetailPO(driver);
    private pageobjects.mamikos.account.LoginPO loginpo = new pageobjects.mamikos.account.LoginPO(driver);

    @Then("user clicks on Upgrade Premium button")
    public void user_clicks_on_upgrade_premium_button() throws InterruptedException {
        upgradePremium.clickOnUpgradeBtn();
    }

    @And("user verify warning text on Premium pop up")
    public void user_verify_warning_text_on_premium_pop_up() {
        Assert.assertEquals(upgradePremium.getPremiumWarning(), "Pastikan Anda memiliki kost atau apartemen aktif terlebih dahulu", "Warning message doesn't match");
    }

    @And("user switch tingkatkan klik toggle")
    public void user_switch_tingkatkan_klik_toggle() throws InterruptedException {
        upgradePremium.clickOnSwitchBtn();
    }

    @And("user click on confirmation button")
    public void user_click_on_confirmation_button() throws InterruptedException {
        upgradePremium.clickOnConfirmationBtn();
    }

    @And("user click on ganti paket button")
    public void user_click_on_ganti_paket_button() throws InterruptedException {
        upgradePremium.clickOnGantiPaketBtn();
    }

    @And("user select on {string} balance amount and price {string}")
    public void user_select_on_balance_amount_and_price(String text, String balancePrice) throws InterruptedException {
        upgradePremium.clickBalance(text, balancePrice);
    }

    @And("user clicks on OK button")
    public void user_clicks_on_ok_button() throws InterruptedException {
        upgradePremium.clickOnOKBtn();
    }

    @Then("user verify buy saldo {string}")
    public void user_verify_buy_saldo(String text) {
        Assert.assertTrue(upgradePremium.verifyBuySaldo(text), "Nominal doesn't match!");
    }

    @And("user verify pop up edit alokasi saldo")
    public void user_verify_pop_up_edit_alokasi_saldo() throws InterruptedException {
        Assert.assertEquals(upgradePremium.getEditAlokasiPopUpMessage(), "Yang Dialokasikan di Data Iklan Ini", "Warning message doesn't match");;
    }

    @And("user clicks Batal button")
    public void user_clicks_batal_button() throws InterruptedException {
        upgradePremium.clickOnBatalBtn();

    }


    @And("user click on tambah saldo button")
    public void user_click_on_tambah_saldo_button() throws InterruptedException {
        upgradePremium.clickOnTambahSaldoBtn();
    }

    @And("user clicks on Batal button")
    public void user_clicks_on_batal_button() throws InterruptedException {
        upgradePremium.clickOnBatalSaldoBtn();
    }

    @Then("user verify tambah saldo button")
    public void user_verify_tambah_saldo_button() {
        Assert.assertEquals(upgradePremium.getTambahSaldoText(), "Tambah Saldo", "Wrong text information");
    }

    @And("user clicks on ok Allocated saldo button")
    public void user_clicks_on_ok_allocated_saldo_Button() throws InterruptedException {
        upgradePremium.clickOnOkAllocatedBtn();
    }

    @And("user clicks on Kembali ke List Kos Button")
    public void user_clicks_on_kembali_ke_list_kos_Button() throws InterruptedException {
        upgradePremium.clickOnBacktolistiklanBtn();
    }

    @Then("display status Aktif on the kos")
    public void display_status_aktif_on_the_kos() throws InterruptedException {
        Assert.assertEquals(upgradePremium.getVerifyAllocatedSaldo(), "Verify Allocated Saldo", "Not allocated saldo");
    }

    @And("user view is display loading")
    public void user_view_is_display_loading() throws InterruptedException {
        upgradePremium.getDisplayLoading();
    }

    @And("user click on Kos menu")
    public void user_click_on_kos_menu() throws InterruptedException {
        upgradePremium.clickOnKosMenu();
    }

    @Then("user verify warning text on Premium pop up tambah saldo")
    public void user_verify_warning_text_on_premium_pop_up_tambah_saldo() {
        Assert.assertEquals(upgradePremium.getPremiumWarningTambahSaldo(), "Mohon selesaikan proses pembelian paket Anda terlebih dahulu.", "Warning message doesn't match");
    }

    @And("user click on OK button")
    public void user_click_on_ok_button() throws InterruptedException {
        upgradePremium.clickOnOKButtonPopUp();
    }

    @And("user click on premium button")
    public void user_click_on_premium_button() throws InterruptedException {
        upgradePremium.clickOnPremiumButton();
    }

    @Then("user verify warning text on Premium pop up buy premium")
    public void user_verify_warning_text_on_premium_pop_up_buy_premium() {
        Assert.assertEquals(upgradePremium.getPremiumWarningTambahSaldo(), "Mohon selesaikan proses pembelian saldo Anda terlebih dahulu.", "Warning message doesn't match");
    }

    @And("user click Lihat Selengkapnya button")
    public void user_click_lihat_selengkapnya_button() throws InterruptedException {
        if (upgradePremium.NoFoundLihatSelengkapnyaButton()) {
            selenium.navigateToPage(Constants.ADMIN_URL);
            login.enterCredentialsAndClickOnLoginButton(Constants.BACKOFFICE_LOGIN_EMAIL, Constants.BACKOFFICE_LOGIN_PASSWORD);
            leftMenu.clickOnKostOwnerMenu();
            kostOwner.searchKosName("Kose Putri Automation");
            kostOwner.clickOnFirstVerifyButton();
            selenium.navigateToPage(Constants.MAMIKOS_URL);
            header.logoutAsAOwner();
            header.clickOnEnterButton();
            loginpo.popUpOwnerLogin();
            loginpo.loginAsOwnerToApplication(Constants.OWNER_PHONE,Constants.OWNER_PASSWORD);
            kosDetailPO.clickOnCloseButton();
            addkos.clickOnMenuPropertySaya();
            addkos.clickOnMenuKost();
            kosList.clickOnSearchKosTextBox();
            kosList.inputKosName("Kose Putri Automation");
            kosList.clickKosName();
            upgradePremium.clickOnLihatSelengkapnyaButton();
        }else {
            upgradePremium.clickOnLihatSelengkapnyaButton();
        }
    }

    @And("user click Atur Premium button")
    public void user_click_atur_premium_button() throws InterruptedException {
        upgradePremium.clickOnAturPremiumButton();
    }

    @And("user input {string} on form allocation saldo")
    public void user_input_on_form_allocation_saldo(String saldo) throws InterruptedException {
        upgradePremium.clickOnAllocationField(saldo);
    }

    @And("user click konfirmasi button")
    public void user_click_konfirmasi_button() throws InterruptedException {
        upgradePremium.clickOnkonfirmasiAllocationButton();
    }

    @And("user verify Kelola Alokasi Saldo is display")
    public void user_verify_kelola_alokasi_saldo_is_display() {
            Assert.assertEquals(upgradePremium.getTextKelolaAlokasiSaldo(), "Kelola Alokasi Saldo", "Warning message doesn't match");

    }


    @And("user click Kelola Alokasi Saldo")
    public void user_click_kelola_alokasi_saldo() throws InterruptedException {
        upgradePremium.clickOnkelolaAlokasiSaldoButton();
    }

    @And("user view text {string}")
    public void user_view_text(String arg0) {
        Assert.assertEquals(upgradePremium.getTextminimumAlokasiSaldoRp5000(), "Minimum Alokasi Saldo Rp5.000", "Warning message doesn't match");
    }

    @And("user click icon back on form alokasi")
    public void user_click_icon_back_on_form_slokasi() throws InterruptedException {
        upgradePremium.clickOnbacktoAlokasiSaldoButton();
    }

    @And("user click on Konfirmasi Pembayaran button")
    public void user_click_on_konfirmasi_pembayaran_button() throws InterruptedException {
        upgradePremium.clickOnKonfirmasiPembayaranButton();
    }

    @And("user verify Bayar button is appear")
    public void user_verify_bayar_button_is_appear() throws InterruptedException {
        Assert.assertTrue(upgradePremium.isBayarButtonVisible(), "Warning message doesn't match");
    }

    @And("user click on Ubah button")
    public void user_click_on_ubah_button() throws InterruptedException {
        upgradePremium.clickOnUbahButton();
    }

    @And("user click on Beli button on package number {string}")
    public void user_click_on_beli_button_on_package_number(String text) throws InterruptedException {
        upgradePremium.clickBeliButton(text);
    }

    @And("user verify buy premium with nominal {string}")
    public void user_verify_buy_premium_with_nominal(String text) {
        Assert.assertTrue(text.contains(".000"), "Nominal doesn't match");

    }

    @And("user click on Konfirmasi button")
    public void user_click_on_konfirmasi_button() throws InterruptedException {
        upgradePremium.clickOnKonfirmasi();
    }

    @And("user select on {string} balance amount with price {string}")
    public void userSelectOnBalanceAmountWithPrice(String balanceAmount, String balancePrice) throws InterruptedException {
        upgradePremium.clickOnBalancePackage(balanceAmount, balancePrice);
    }

    @And("user click on Batal button on pop up iklan teratas tidak aktif")
    public void user_click_on_batal_button_on_pop_up_iklan_teratas_tidak_aktif() throws InterruptedException {
        upgradePremium.clickOnBatalNonAktifIklanTeratas();
    }

    @And("user check Jangan tampilkan ini kembali")
    public void user_check_jangan_tampilkan_ini_kembali() throws InterruptedException {
        upgradePremium.clickOnCheckbox();
    }

    @And("user click on Atur Premium button on dashboard")
    public void user_click_on_atur_premium_button_on_dashboard() throws InterruptedException {
        upgradePremium.clickAturPremiumDashboard();
    }

    @And("user verify the warning {string} is appear")
    public void user_verify_the_warning_is_appear(String text) {
        Assert.assertEquals(upgradePremium.getTextWarningMessagePopUp(), text, "Warning message doesn't match");
    }

    @And("user close the pop up warning")
    public void user_close_the_popUp_warning() throws InterruptedException {
        upgradePremium.clickOnXWarningPopUp();
    }

    @And("user close the pop up booking langsung")
    public void userCloseThePopUpBookingLangsung() throws InterruptedException {
        if (upgradePremium.isBookingLangsungPopUpAppear()) {
            upgradePremium.clickOnCloseBookingLangsungPopUp();
        }
    }

    @And("user click Property Saya menu")
    public void user_click_property_saya_menu() throws InterruptedException {
        upgradePremium.clickOnpropertySayaMenu();
    }

    @Then("user will be verify dropdown in property saya")
    public void user_will_be_verify_dropdown_in_property_saya() {
        Assert.assertTrue(upgradePremium.isPropertyMenuDropdownShowing(), "Dropdown is not showing");
    }

    @And("user click Statistik menu")
    public void user_click_statistik_menu() throws InterruptedException {
        upgradePremium.clickOnStatistikMenu();
    }

    @And("user click Goldplus statistik")
    public void user_click_goldplus_statistik() throws InterruptedException {
        upgradePremium.clickOngoldplusStatistik();
    }

    @And("user click lihat detail statisik")
    public void user_click_lihat_detail_statisik() throws InterruptedException {
        upgradePremium.clickOnlihatdetailStatistik();
    }

    @And("user verify detail statistik with {string}")
    public void user_verify_detail_statistik_with(String text) {
        Assert.assertTrue(text.contains("Goldplus"));
    }

    @And("user view Statistik kunjungan iklan")
    public void user_view_statistik_kunjungan_iklan() throws InterruptedException {
        upgradePremium.clickOnstatistikKunjungan();
    }

    @And("user view Statistik chat masuk")
    public void user_view_statistik_chat_masuk() throws InterruptedException {
        upgradePremium.clickOnstatistikChat();
    }

    @And("user view Statistik peminat kos")
    public void user_view_statistik_peminat_kos() throws InterruptedException {
        upgradePremium.clickOnstatistikPeminat();

    }

    @Then("user view informastion text")
    public void user_view_informastion_text() {
        Assert.assertEquals(upgradePremium.getStatistikText(), "Tidak Ada Statistik Kos", "Warning message doesn't match");
    }

    @And("user view description text")
    public void user_view_description_text() {
        Assert.assertEquals(upgradePremium.getDesStatistikText(), "Statistik hanya dapat diakses oleh pengguna GoldPlus atau Premium.", "Warning message doesn't match");

    }

    @Then("user click on Kelola Promo button")
    public void user_click_on_kelola_promo_button() throws InterruptedException {
        upgradePremium.clickOnaKelolaPromoBtn();
    }

    @And("user enter promo {string} on Judul Promo")
    public void user_enter_promo_on_judul_promo(String judulPromo) throws InterruptedException {
        upgradePremium.enterJudulPromo(judulPromo);

    }

    @And("user enter {string} on Detail Promo")
    public void user_enter_on_detail_promo(String detailPromo) throws InterruptedException {
        upgradePremium.enterDetailPromo(detailPromo);
    }

    @Then("user click Pasang Promo Button")
    public void user_click_pasang_promo_button() throws InterruptedException {
        upgradePremium.clickPasangPromoBtn();

    }
    @And("user select the promo start date")
    public void user_select_the_promo_start_date() throws InterruptedException {
        upgradePremium.clickOnstartDateBtn();
        upgradePremium.chooseNextDay();
    }

        @And("user select the promo end date")
        public void user_select_the_promo_end_date() throws InterruptedException {
            upgradePremium.clickOnendDateBtn();
            upgradePremium.chooseNextDay();
        }

    @Then("user view {string} status")
    public void use_view_menunggu_verifikasi_status(String status) {
        upgradePremium.getStatusText();
        Assert.assertEquals(upgradePremium.getStatusText(),status,"status is not equals");
    }


}







