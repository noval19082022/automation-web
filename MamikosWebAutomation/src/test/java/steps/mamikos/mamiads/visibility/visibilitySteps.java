package steps.mamikos.mamiads.visibility;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.mamiads.BeliSaldoPO;
import pageobjects.mamikos.mamiads.visibility.VisibilityPO;
import utilities.ThreadManager;

public class visibilitySteps {
    private WebDriver driver = ThreadManager.getDriver();
    private VisibilityPO visibilityPO = new VisibilityPO(driver);

    @And("user verify message {string} in saldo MamiAds")
    public void user_verify_message_in_saldo_mamiAds(String messageVisibility) throws InterruptedException{
        Assert.assertEquals(visibilityPO.getVisibilityMessage(), messageVisibility, "message dosn't match!");
    }

    @And("user verify title {string} in saldo MamiAds")
    public void userVerifyTitleInSaldoMamiAds(String titleText) {
        Assert.assertEquals(visibilityPO.getTitleVisibilityText(), titleText, "title text doesn't match!");
    }
}
