package steps.mamikos.mamiads.voucherowner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.voucherOwner.VoucherOwnerPO;
import utilities.ThreadManager;

public class VoucherOwnerSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private VoucherOwnerPO voucherOwnerPO = new VoucherOwnerPO(driver);

    @Then("user verify {string} is displayed")
    public void user_verify_is_displayed(String popUpString) {
        Assert.assertTrue(voucherOwnerPO.isVoucherPopUpPresent(popUpString), "Voucher pop up disappears!");
    }

    @When("user click {string} on Punya kode voucher?")
    public void user_click_on_punya_kode_voucher(String masukkanVoucher) throws InterruptedException {
        voucherOwnerPO.clickOnMasukkanVoucher(masukkanVoucher);
    }

    @And("user input {string} as kode voucher")
    public void user_input_as_kode_voucher(String voucherCode) {
        voucherOwnerPO.inputVoucherCode(voucherCode);
    }

    @And("user click Pakai button")
    public void user_click_pakai_button() throws InterruptedException {
        voucherOwnerPO.clickOnPakaiVoucherButton();
    }

    @Then("validate the warning {string}")
    public void validate_the_warning(String warningText) {
        Assert.assertEquals(voucherOwnerPO.getMessageWarningVoucher(), warningText, "Warning message doesn't match!");
    }

    @When("user clear the voucher code")
    public void user_clear_the_voucherCode() {
        voucherOwnerPO.clearVoucherCode();
    }

    @And("user click on icon close")
    public void user_click_on_icon_close() throws InterruptedException {
        voucherOwnerPO.clickOnIconClose();
    }
}
