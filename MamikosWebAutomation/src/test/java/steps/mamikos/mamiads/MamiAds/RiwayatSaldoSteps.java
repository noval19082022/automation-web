package steps.mamikos.mamiads.MamiAds;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.mamiads.BeliSaldoPO;
import pageobjects.mamikos.mamiads.mamiads.RiwayatSaldoPO;
import utilities.ThreadManager;

public class RiwayatSaldoSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private RiwayatSaldoPO riwayatSaldoPO = new RiwayatSaldoPO(driver);
    private BeliSaldoPO beliSaldoPO = new BeliSaldoPO(driver);
    private Integer riwayatBeforeBeliSaldo;

    @And("validate status transaction mamiads is {string} with price {string}")
    public void validate_status_transaction_mamiads_with_price(String status, String price) {
        Assert.assertEquals(riwayatSaldoPO.gettransactionList(1), "Saldo "+beliSaldoPO.getSaldo());
        Assert.assertEquals(riwayatSaldoPO.gettransactionList(2), price);
        Assert.assertEquals(riwayatSaldoPO.gettransactionList(3), status);
    }

    @Then("user verify status transaction mamiads is {string}")
    public void user_verify_status_transaction_mamiadsIs(String transactionStatusText) {
        Assert.assertEquals(riwayatSaldoPO.getTransactionStatusText(), transactionStatusText, "transaction status doesn't match!");
    }

    @And("user click back button on Riwayat page")
    public void user_click_back_button_on_riwayat_page() throws InterruptedException {
        riwayatSaldoPO.clickOnBackRiwaytButton();
    }

    @Then("user verify count of riwayat before beli saldo")
    public void user_verify_count_of_riwayat_before_beli_saldo() {
        riwayatBeforeBeliSaldo = riwayatSaldoPO.getCountRiwayatBeliSaldo();
    }

    @Then("user verify count of riwayat added {int}")
    public void user_verify_count_of_riwayat_added(Integer numberAdded) {
        int riwayatAfterBeliSaldo = riwayatSaldoPO.getCountRiwayatBeliSaldo();
        Assert.assertEquals(riwayatAfterBeliSaldo, (riwayatBeforeBeliSaldo + numberAdded), "Count of riwayat doesn't Match");
    }

    @And("user click on the {string} transaction")
    public void user_click_on_the_transaction(String number) throws InterruptedException {
        riwayatSaldoPO.clickOnTransaction(number);
    }

    @Then("user validate a tab is {string}")
    public void user_validate_a_tab_is(String title) {
        Assert.assertEquals(riwayatSaldoPO.getTextNewTab(), title);
    }
}
