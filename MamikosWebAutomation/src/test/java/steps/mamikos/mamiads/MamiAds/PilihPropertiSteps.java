package steps.mamikos.mamiads.MamiAds;

import io.cucumber.java.en.And;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.mamiads.PilihPropertiPO;
import utilities.ThreadManager;

public class PilihPropertiSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private PilihPropertiPO pilihPropertiPO = new PilihPropertiPO(driver);

    @And("user click search box then input keyword {string} and enter")
    public void user_click_search_box_then_input_keyboard_and_enter(String keyword) throws InterruptedException {
        pilihPropertiPO.enterTextToSearchProperti(keyword);
    }

    @And("do validation {string} with filter {string} on search page")
    public void do_validation_with_filter_on_search_page(String validation, String keyword) throws InterruptedException {
        switch (validation){
            case "Advertised":
                do_validation_with_filter_on_search_page("Not Found",keyword);
                pilihPropertiPO.clickOnBackButton();
                Assert.assertTrue(pilihPropertiPO.getListAdvertisedProperties().contains(keyword));
                break;
            case "Not Found" :
                Assert.assertEquals(pilihPropertiPO.getTextTitle(),"Properti Tidak Ditemukan");
                Assert.assertEquals(pilihPropertiPO.getTextMessage(),"Maaf, kami tidak menemukan properti yang Anda cari. Coba cari dengan nama lain.");
                break;
            case "Property Full":
                Assert.assertEquals(pilihPropertiPO.getPropertyNameLabel(),keyword);
                Assert.assertTrue(pilihPropertiPO.isPropertyListUnavailableVisible(), "List property unavailable is not present!");
                Assert.assertTrue(pilihPropertiPO.getTextPropertyUnavailable().contains("Kamar Penuh"),"property does not contains kamar penuh");
                Assert.assertEquals(pilihPropertiPO.getBackgroundColorProperty(),"#f6f6f6");
                break;
            case "Available":
                Assert.assertEquals(pilihPropertiPO.getPropertyNameLabel(),keyword);
                Assert.assertFalse(pilihPropertiPO.isPropertyListUnavailableVisible(), "List property is present!");
                Assert.assertEquals(pilihPropertiPO.getBackgroundColorProperty(),"#ffffff");
                break;
            case "All Advertised":
                Assert.assertEquals(pilihPropertiPO.getTextTitle(),"Semua Properti Sudah Diiklankan");
                Assert.assertEquals(pilihPropertiPO.getTextMessage(),"Properti yang belum diiklankan akan muncul di halaman ini.");
                break;
            case "Cancel Search":
                do_validation_with_filter_on_search_page("Not Found",keyword);
                Assert.assertTrue(pilihPropertiPO.isBatalkanTextVisible());
                pilihPropertiPO.clickOnBatalkanText();
                do_validation_with_filter_on_search_page("All Advertised",keyword);
                Assert.assertFalse(pilihPropertiPO.isBatalkanTextVisible());
                break;
        }
    }
}
