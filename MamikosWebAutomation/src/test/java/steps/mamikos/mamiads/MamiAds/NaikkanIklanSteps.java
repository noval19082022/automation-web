package steps.mamikos.mamiads.MamiAds;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.mamiads.NaikkanIklanPO;
import utilities.ThreadManager;
import java.util.List;
import java.util.Map;

public class NaikkanIklanSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private NaikkanIklanPO naikkanIklanPO = new NaikkanIklanPO(driver);

    @And("user click toggle button posisi iklan")
    public void user_click_toggle_button_posisi_iklan() throws InterruptedException {
        naikkanIklanPO.clickPosisiIklanToggle();
    }

    @And("user cek status toggle iklan {string} is {string}") public void user_cek_status_toggle_iklan_is(String adsName, String posisiIklan) throws InterruptedException {
        if (posisiIklan.equals("naik")) {
            Assert.assertEquals(naikkanIklanPO.getPosisiIklan(adsName, posisiIklan), "Naik", "Posisi iklan doesn't available");
        } else if (posisiIklan.equals("tidak-naik")) {
            Assert.assertEquals(naikkanIklanPO.getPosisiIklan(adsName, posisiIklan), "Tidak Naik", "Posisi iklan doesn't available");
        }
    }

    @And("user verify the toast {string}")
    public void user_verify_the_toast(String messageToastOffIklan) {
        Assert.assertEquals(naikkanIklanPO.getTextMessageToastOnOffIklan(), messageToastOffIklan, "Message doesn't match!");
    }

    @And("user verify wording message {string} is {string}")
    public void userVerifyWordingMessageIs(String propertyName, String messageText) {
        Assert.assertEquals(naikkanIklanPO.getMessageTextPosisIklan(propertyName), messageText, "Message doesn't match!");
    }

    @And("user verify Saldo MamiAds")
    public void userVerifySaldoMamiAds() {
        Assert.assertEquals(naikkanIklanPO.getTextSaldoMamiAds(), "Saldo MamiAds:", "Text doesn't match!");
    }

    @And("user verify MamiAds text")
    public void userVerifyMamiAdsText() {
        Assert.assertEquals(naikkanIklanPO.getTextMamiAdsTitle(), "MamiAds", "Text doesn't match!");
    }

    @And("user click Naikkan Iklan on properti saya")
    public void userclicknaikkaniklanonpropertisaya() throws InterruptedException {
        naikkanIklanPO.clickOnNaikkanIklanPropertiSayaButton();
    }

    @And("user can see default filter is {string}")
    public void user_can_see_default_filter_is(String defaultFilter) {
        Assert.assertEquals(naikkanIklanPO.getTeksDefaultFilter(), "Semua Iklan");
    }

    @And("user verify the toggle iklan {string} is {string}")
    public void user_verify_the_toggle_iklan_is(String adsName, String toggleStatus) {
        Assert.assertTrue(naikkanIklanPO.getToggleStatus(adsName, toggleStatus), "toggle doesn't match!");
    }

    @And("user verify the wording iklan {string} is {string}")
    public void user_verify_the_wording_iklan_is(String adsName, String adsStatusDesc) {
        Assert.assertEquals(naikkanIklanPO.getAdsStatusDesc(adsName), adsStatusDesc, "Ads status description doesn't match!");
    }

    @And("user verify the wording anggaran of iklan {string} is {string}")
    public void user_verify_the_wording_anggaran_of_iklan_is(String adsName, String anggaranDesc) throws InterruptedException {
        Assert.assertEquals(naikkanIklanPO.getTextAnggaranDesc(adsName), anggaranDesc, "Anggaran description doesn't match!");
    }

    @When("user click {string} toggle the {string}")
    public void user_click_toggle_the(String toggleStatus, String adsName) throws InterruptedException {
        naikkanIklanPO.clickToggleTheAds(toggleStatus, adsName);
    }

    @Then("user verify the pop up switch {string} toggle iklan {string} is displayed")
    public void user_verify_the_pop_up_switch_toggle_iklan_is_displayed(String action, String adsName) {
        int saldoMa = naikkanIklanPO.getSaldoMaText();
        if (action.equals("on") && saldoMa >= 5000){
            Assert.assertEquals(naikkanIklanPO.getTextSwitchTogglePopUp(action), "Jika Anda menonaktifkannya, posisi iklan " + adsName + " tidak dinaikkan di hasil pencarian.");
        } else if (action.equals("off") && saldoMa >= 5000) {
            Assert.assertEquals(naikkanIklanPO.getTextSwitchTogglePopUp(action), "Anggaran MamiAds untuk " + adsName + " akan diaktifkan");
        } else{
            Assert.assertEquals(naikkanIklanPO.getTitleBeliSaldoPopUp(),"Anda belum bisa menaikkan iklan.");
        }
    }

    @When("user click {string} button")
    public void user_click_button(String actionButton) throws InterruptedException {
        naikkanIklanPO.clickActionButtonInPopUp(actionButton);
    }

    @And("user verify the wording iklan penuh {string} is {string}")
    public void user_verify_the_wording_iklan_penuh_is(String adsName, String adsDesc) {
        Assert.assertEquals(naikkanIklanPO.isFullOcuppancyActiveAds(adsName), adsDesc, "Kamar Penuh doesn't match!");
    }

    @And("ads list rooms as expected")
    public void ads_list_rooms_as_expected(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        int i = 0;
        int j = 0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(naikkanIklanPO.listAds("adsName", i), content.get("adsName"));
            Assert.assertEquals(naikkanIklanPO.listAds("posisiIklan", i), content.get("posisiIklan"));

            try {
                if (!content.get("currentToggle").equals("-")) {
                    System.out.println(content.get("currentToggle"));
                    Assert.assertTrue(naikkanIklanPO.listAdsToggle(content.get("currentToggle"), j), content.get("currentToggle"));
                }
            } catch (java.lang.NullPointerException ignored1) {

            }

            try {
                if (!content.get("availRoom").equals("-")) {
                    Assert.assertEquals(naikkanIklanPO.listAds("availRoom", j), content.get("availRoom"));
                    j++;
                }
            } catch (java.lang.NullPointerException ignored1) {

            }

            try {
                if (!content.get("currentStatusSaldo").equals("-")) {
                    Assert.assertEquals(naikkanIklanPO.listCurrentStatusSaldo(content.get("adsName")), content.get("currentStatusSaldo"));

                }
            } catch (java.lang.NullPointerException ignored2) {

            }
            i++;
        }
    }

    @Then("user verify the wording iklan kamar penuh {string} is {string}")
    public void user_verify_the_wording_iklan_kamar_penuh_is(String adsName, String adsDesc) throws InterruptedException {
        Assert.assertEquals(naikkanIklanPO.isFullOcuppancy(adsName), adsDesc, "Kamar Penuh doesn't match!");
    }

    @When("user click icon close")
    public void user_click_icon_close() throws InterruptedException {
        naikkanIklanPO.clickIconClose();
    }

    @Then("verify the saldo mamiads with condition {string} and value is {int}")
    public void verify_the_saldo_mamiads_with_condition_and_value_is(String comparisonSymbol, int minimalSaldo) {
        int saldoMa = naikkanIklanPO.getSaldoMaText();
        switch (comparisonSymbol){
            case "<":
                Assert.assertTrue(saldoMa < minimalSaldo, "Saldo MamiAds greatherThan Saldo Maksimal");
                break;
            case ">=":
                Assert.assertTrue(saldoMa >= minimalSaldo, "Saldo MamiAds lessThan Saldo Maksimal");
                break;
        }
    }

    @When("user click back arrow button")
    public void user_click_back_arrow_button() throws InterruptedException {
        naikkanIklanPO.clickOnBackArrawButton();
    }

    @And("user waiting the loading screen")
    public void userWaitingTheLoadingScreen() throws InterruptedException {
        naikkanIklanPO.isLoadingFinished();
    }

    @And("user verify the wording is {string} and {string}")
    public void user_verify_the_wording_is_and(String title, String subTitle) {
        Assert.assertEquals(naikkanIklanPO.getTitleText(), title);
        Assert.assertEquals(naikkanIklanPO.getSubTitleText(), subTitle);
    }

    @Then("user click question {string}")
    public void user_click_question(String questionText) throws InterruptedException {
        naikkanIklanPO.clickOnQustionText(questionText);
    }

    @Then("user verify answer text {string}")
    public void user_verify_answer_text(String answerText) {
        Assert.assertEquals(naikkanIklanPO.getAnswerText(answerText), answerText);
    }
}