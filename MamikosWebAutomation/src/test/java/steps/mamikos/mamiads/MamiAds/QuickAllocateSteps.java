package steps.mamikos.mamiads.MamiAds;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.mamiads.QuickAllocatePO;
import utilities.ThreadManager;

public class QuickAllocateSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private QuickAllocatePO quickAllocatePO = new QuickAllocatePO(driver);

    @Then("user verify the alokasi title is {string}")
    public void user_verify_the_alokasi_title_is(String alokasiTitleText) {
        Assert.assertEquals(quickAllocatePO.getAlokasiTitleText(alokasiTitleText), alokasiTitleText, "Alokasi title doesn't match!");
    }

    @And("user verify the toggle is {string}")
    public void user_verify_the_toggle_is(String toggleAdsStatus) {
        Assert.assertTrue(quickAllocatePO.getToggleAdsStatus(toggleAdsStatus), "toggle ads status doesn't match!");
    }

    @And("user verify the wording ads is {string}")
    public void user_verify_the_wording_ads_is(String adsDescText) {
        switch (adsDescText){
            case "Kamar penuh":
                Assert.assertEquals(quickAllocatePO.getAdsDescText(adsDescText), "Kamar penuh. Silakan nonaktifkan jika tidak ingin menaikkan posisi iklan ini.");
                break;
        }
    }
}
