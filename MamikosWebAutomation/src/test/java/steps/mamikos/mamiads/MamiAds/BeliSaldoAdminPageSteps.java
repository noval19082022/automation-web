package steps.mamikos.mamiads.MamiAds;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.backoffice.LeftMenuPO;
import pageobjects.mamikos.mamiads.mamiads.BeliSaldoAdminPagePO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class BeliSaldoAdminPageSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private BeliSaldoAdminPagePO beliSaldoPO = new BeliSaldoAdminPagePO(driver);
    private LeftMenuPO left = new LeftMenuPO(driver);

    //Payment Properties
    private String payment="src/test/resources/testdata/mamikos/payment.properties";
    private String _ownerNumber_payment = JavaHelpers.getPropertyValue(payment,"ownerNumber_payment_staging");
    private String invoiceNumber = JavaHelpers.getPropertyValue(payment,"invoiceNumber");
    private String gpInvoiceNumber = JavaHelpers.getPropertyValue(payment,"gpInvoiceNumber");
    private String invoiceNumberExpired = JavaHelpers.getPropertyValue(payment,"invoiceNumberExpired");



    @And("user open menu package invoice list")
    public void userOpenMenuPackageInvoiceList() throws InterruptedException{
        beliSaldoPO.clickOnPackageInvoiceListMenu();
    }

    @And("user search invoice on MamiPAY Premium Package Invoice List")
    public void userSearchInvoiceOnMamiPAYPremiumPackageInvoiceList() throws InterruptedException{
        beliSaldoPO.searchInvoice();
    }

    @Then("system display status {string}")
    public void systemDisplayStatus(String status) throws InterruptedException{
        beliSaldoPO.verifyStatus(status);
    }

    @When("user choose {string} on filter search by")
    public void userChooseOnFilterSearchBy(String searchBy) throws InterruptedException{
        beliSaldoPO.chooseSearchBy(searchBy);
    }

    @And("user input phone input phone number")
    public void userInputPhoneInputPhoneNumber() throws InterruptedException{
        beliSaldoPO.inputPhoneNumber(_ownerNumber_payment);
    }

    @And("user click button search invoice")
    public void userClickButtonSearchInvoice() throws InterruptedException{
        beliSaldoPO.clickOnSearchInvoiceButton();
    }

    @Then("system display package invoice list sort by phone number")
    public void systemDisplayFieldPackageInvoiceList() throws InterruptedException{
        beliSaldoPO.verifyListShortByPhoneNumber(_ownerNumber_payment);
    }

    @And("user input phone input invalid phone number")
    public void userInputPhoneInputInvalidPhoneNumber() throws InterruptedException{
        beliSaldoPO.inputInvalidPhoneNumber(_ownerNumber_payment);
    }

    @Then("system display package invoice list sort by invalid phone number")
    public void systemDisplayPackageInvoiceListSortByInvalidPhoneNumber() throws InterruptedException{
        beliSaldoPO.verifyListShortByInvalidPhoneNumber();
    }

    @When("user choose {string} on filter by status")
    public void userChooseOnFilterByStatus(String status) throws InterruptedException{
        beliSaldoPO.chooseByStatus(status);
    }

    @Then("system display package invoice list filter by status {string}")
    public void systemDisplayPackageInvoiceListFilterByStatus(String status) throws InterruptedException{
        beliSaldoPO.verifyListFilterByStatusUnpaid(status);
    }

    @And("system display package invoice list filter valid premium package invoice")
    public void system_display_package_invoice_list_filter_valid_premium_package_invoice() throws InterruptedException{
        Assert.assertEquals(beliSaldoPO.getInvoiceNumber(), invoiceNumber , "invoice number don't match");
    }

    @And("user input valid invoice number")
    public void userInputValidInvoiceNumber() throws InterruptedException{
        beliSaldoPO.inputInvoiceNumber(invoiceNumber);
    }

    @And("user input invalid invoice number")
    public void userInputInvalidInvoiceNumber() throws InterruptedException{
        beliSaldoPO.inputInvalidInvoiceNumber(invoiceNumber);
    }

    @And("system display package invoice list filter invalid premium package invoice")
    public void system_display_package_invoice_list_filter_invalid_premium_package_invoice() throws InterruptedException{
        beliSaldoPO.verifyListShortByInvalidPhoneNumber();
    }

    @Given("user click button reset")
    public void userClickButtonReset() throws InterruptedException{
        beliSaldoPO.clickOnResetButton();
    }

    @And("user input gp invoice number")
    public void userInputGpInvoiceNumber() throws InterruptedException{
        beliSaldoPO.inputGpInvoiceNumber(gpInvoiceNumber);
    }

    @Then("system display package invoice list search invoice use GP Ivoice")
    public void systemDisplayPackageInvoiceListSearchInvoiceUseGPIvoice() throws InterruptedException{
        beliSaldoPO.verifyListShortByInvalidPhoneNumber();
    }

    @And("user check invoice number expired")
    public void userCheckInvoiceNumberExpired() throws InterruptedException{
        beliSaldoPO.inputGpInvoiceNumber(invoiceNumberExpired);
    }

    @Then("system display invoice number expired with status {string}")
    public void systemDisplayInvoiceNumberExpiredWithStatus(String status) throws InterruptedException{
        Assert.assertEquals(beliSaldoPO.getStatus(),status,"status don't match");
    }

    @And("user due date from and to {string}, {string}")
    public void user_due_date_from_and_to (String dueDatefrom, String dueDateTo) throws InterruptedException {
        beliSaldoPO.userInputDueDate(dueDatefrom, dueDateTo);

    }

    @Then("system display package invoice list based on expiry date {string}")
    public void system_display_package_invoice_list_based_on_expiry_date (String expiryDate) throws  InterruptedException {
        beliSaldoPO.systemDisplayInvoiceBasedOnExpiryDate(expiryDate);
    }
}
