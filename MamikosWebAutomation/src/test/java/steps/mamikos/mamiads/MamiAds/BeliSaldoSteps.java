package steps.mamikos.mamiads.MamiAds;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.mamiads.BeliSaldoPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;
import java.util.List;
import java.util.Map;

public class BeliSaldoSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private BeliSaldoPO beliSaldoPO = new BeliSaldoPO(driver);
    SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @And("user choose saldo {string}")
    public void userChooseSaldo(String typeSaldo) throws InterruptedException{
        beliSaldoPO.chooseSaldo(typeSaldo);
    }

    @And("user click back button")
    public void userClickBackButton() throws InterruptedException {
        beliSaldoPO.clickOnBackButton();
    }

    @And("user click Bayar button")
    public void userClickBayarButton() throws InterruptedException {
        beliSaldoPO.clickOnBayarButton();
    }

    @Then("user verify title {string}")
    public void userVerifyTitle(String text) {
        Assert.assertEquals(beliSaldoPO.getDetailTagihanText(), "Detail Tagihan", "Title page doesn't match");
    }

    @And("favorit saldo is {string}")
    public void favorit_saldo_is(String saldo){
        Assert.assertTrue(beliSaldoPO.favoriteSaldo(saldo));
    }

    @And("detail list saldo as expected")
    public void user_see_below_data_is_correct_as_text(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        int i=0;int j=0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(beliSaldoPO.listSaldo("price",i),content.get("price"));
            Assert.assertEquals(beliSaldoPO.listSaldo("priceInRp",i),content.get("priceInRp"));
            try{
                if(!content.get("disc").isEmpty()){
                    Assert.assertEquals(beliSaldoPO.listSaldo("disc",j),content.get("disc"));
                    Assert.assertEquals(beliSaldoPO.listSaldo("discPrice",j),content.get("discPrice"));
                    j++;
                }
            } catch (java.lang.NullPointerException ignored) { }
            i++;
        }
    }

    @And("validate detail tagihan")
    public void validate_detail_tagihan(){
        Assert.assertEquals(beliSaldoPO.detailTagihan(1),"Saldo "+beliSaldoPO.getSaldo());
        Assert.assertEquals(beliSaldoPO.detailTagihan(2),"Saldo "+beliSaldoPO.getSaldo());
        Assert.assertEquals(beliSaldoPO.detailTagihan(3),"Rp "+beliSaldoPO.getSaldo());
        Assert.assertEquals(beliSaldoPO.detailTagihan(4),"Rp "+beliSaldoPO.getSaldo());
        Assert.assertEquals(beliSaldoPO.detailTagihan(5),"Bayar Sekarang Rp "+beliSaldoPO.getSaldo());
    }

    @And("validate {string} before choose payment method")
    public void validate_before_choose_payment_method(String product) throws InterruptedException {
        beliSaldoPO.setTotalPembayaran(beliSaldoPO.getPembayaranText("Total Pembayaran"));
        Assert.assertTrue(beliSaldoPO.getPembayaranText("No. Invoice").contains("PRE"));
        Assert.assertEquals(beliSaldoPO.getPembayaranText("Jenis Pembayaran"),"Bayar "+ product);
        Assert.assertEquals(beliSaldoPO.getPembayaranText("Metode Pembayaran"),"Belum dipilih");
        if (product.equals("Saldo MamiAds")){
            beliSaldoPO.setAllRincianPembayaran();
            Assert.assertTrue(beliSaldoPO.getAllRincianPembayaran().contains("Saldo "+beliSaldoPO.getSaldo()));
        }

    }

    @And("validate transaction Bayar {string} success with {string}")
    public void validate_transaction_success(String product,String paymentMethod) throws InterruptedException {
        Assert.assertTrue(beliSaldoPO.isSelesaiButtonAvailable());
        if(product.equals("Saldo MamiAds")){
            Assert.assertEquals(beliSaldoPO.getAllRincianPembayaran(), beliSaldoPO.setAllRincianPembayaran());
        }
        Assert.assertTrue(beliSaldoPO.getPembayaranText("No. Invoice").contains("PRE"));
        Assert.assertEquals(beliSaldoPO.getPembayaranText("Jenis Pembayaran"),"Bayar " + product);
        Assert.assertEquals(beliSaldoPO.getPembayaranText("Metode Pembayaran"), paymentMethod);
        Assert.assertEquals(beliSaldoPO.getPembayaranText("Status Transaksi"),"Lunas");
    }

    @And("user redirected to owner's mamiads dashboard")
    public void user_redirected_to_owners_mamiads_dashboard(){
        selenium.switchToWindow(2);
        Assert.assertEquals(selenium.getURL(),"https://owner-jambu.kerupux.com/mamiads");
    }

    @And("user click on Bayar Sekarang button")
    public void user_click_on_bayar_sekarang_button() throws InterruptedException {
        beliSaldoPO.clickOnBayarButton();
    }

    @And("validate {string} after apply voucher {string}")
    public void validate_after_apply_voucher(String product, String voucherCode) throws InterruptedException{
        Assert.assertTrue(beliSaldoPO.getPembayaranText("No. Invoice").contains("PRE"));
        Assert.assertEquals(beliSaldoPO.getPembayaranText("Jenis Pembayaran"),"Bayar "+ product);
        Assert.assertEquals(beliSaldoPO.getVoucherApplied("Voucher"), voucherCode);
        if (product.equals("Saldo MamiAds")){
            beliSaldoPO.setAllRincianPembayaran();
            Assert.assertTrue(beliSaldoPO.getAllRincianPembayaran().contains("Saldo "+beliSaldoPO.getSaldo()));
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(1),"Saldo "+beliSaldoPO.getSaldo());
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(2),"Rp"+beliSaldoPO.getSaldo());
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(3),voucherCode);
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(4),"-Rp"+beliSaldoPO.getSaldo());
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(5), "Total Pembayaran");
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(6),"Rp0");
        }
    }

    @Then("validate transaction {string} after success with {string} voucher")
    public void validate_transaction_after_success_with_voucher(String product, String voucherCode) throws InterruptedException {
        Assert.assertTrue(beliSaldoPO.getPembayaranText("No. Invoice").contains("PRE"));
        Assert.assertEquals(beliSaldoPO.getPembayaranText("Jenis Pembayaran"),"Bayar "+ product);
        Assert.assertEquals(beliSaldoPO.getMetodePembayaran("Metode Pembayaran"), "Bank Lainnya");

        if (product.equals("Saldo MamiAds")){
            beliSaldoPO.setAllRincianPembayaran();
            Assert.assertTrue(beliSaldoPO.getAllRincianPembayaran().contains("Saldo "+beliSaldoPO.getSaldo()));
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(9),"Saldo "+beliSaldoPO.getSaldo());
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(10),"Rp"+beliSaldoPO.getSaldo());
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(11),voucherCode);
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(12),"-Rp"+beliSaldoPO.getSaldo());
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(13), "Total Pembayaran");
            Assert.assertEquals(beliSaldoPO.getRincianPembayaran(14),"Rp0");
        }
    }
}
