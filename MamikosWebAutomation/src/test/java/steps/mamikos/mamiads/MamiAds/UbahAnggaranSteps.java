package steps.mamikos.mamiads.MamiAds;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.mamiads.UbahAnggaranPO;
import utilities.ThreadManager;

public class UbahAnggaranSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private UbahAnggaranPO ubahAnggaranPO = new UbahAnggaranPO(driver);

    @And("user set anggaran harian to {string}")
    public void user_set_anggaran_harian_to(String anggaran) {
        ubahAnggaranPO.setAnggaranHarianForm(anggaran);
    }

    @And("do validation message {string} on form set anggaran")
    public void do_validation_on_text_field(String message){
        Assert.assertEquals(ubahAnggaranPO.getAnggaranTextBoxMessage(),message);
    }

    @Then("user verify the form anggaran is displayed")
    public void user_verify_the_form_anggaran_is_displayed() {
        Assert.assertTrue(ubahAnggaranPO.isFormAnggaranDisplayed(), "Form anggaran doesn't displayed!");
    }

    @When("user click Ubah on iklan {string}")
    public void user_click_ubah_on_iklan(String adsName) throws InterruptedException {
        ubahAnggaranPO.clickOnUbahAdsButton(adsName);
    }

    @When("user click Ya,Ganti button")
    public void user_click_ya_ganti_button() throws InterruptedException {
        ubahAnggaranPO.clickOnYaGantiButton();
    }

    @And("user choose Metode Anggaran {string}")
    public void user_choose_metode_anggaran(String arg0) throws InterruptedException {
        ubahAnggaranPO.clickOnMetodeAnggaranButton();
    }
}
