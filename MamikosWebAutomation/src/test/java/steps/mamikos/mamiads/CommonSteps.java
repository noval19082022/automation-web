package steps.mamikos.mamiads;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.CommonPremiumPO;
import pageobjects.mamikos.mamiads.mamiads.NaikkanIklanPO;
import pageobjects.mamikos.mamiads.mamiads.RiwayatSaldoPO;
import utilities.ThreadManager;

public class CommonSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private CommonPremiumPO commonPremiumPO = new CommonPremiumPO(driver);
    private RiwayatSaldoPO riwayatSaldoPO = new RiwayatSaldoPO(driver);
    private NaikkanIklanPO naikkanIklanPO = new NaikkanIklanPO(driver);

    @And("user close pop up on boarding mamiads")
    public void user_close_pop_up_on_boarding_mamiads() throws InterruptedException  {
        commonPremiumPO.clickOnCloseOnBoardingMamiAdsPopUp();
    }

    @And("pop up on boarding mamiads disappeared")
    public void pop_up_on_boarding_mamiads_dissapeared(){
        commonPremiumPO.isOnBoardingMamiAdsPopUpAppear();
    }

    @And("user click {string}")
    public void user_click(String menu) throws InterruptedException  {
        commonPremiumPO.clickOnText(menu);
    }

    @When("user wait Pilih Saldo buttons are loaded")
    public void user_wait_Pilih_Saldo_buttons_are_loaded() {
        commonPremiumPO.waitTillPilihSaldoButtonAreLoaded();
    }

    @And("user see Title {string} with message {string} on page {string}")
    public void user_see_Title_with_message(String title, String message, String page) {
        switch (page){
            case "selesai":
                Assert.assertEquals(riwayatSaldoPO.getTextTitleSelesaiTransaksi(), title);
                Assert.assertEquals(riwayatSaldoPO.getTextMessageSelesaiTransaksi(), message); break;
            default:
                Assert.assertEquals(commonPremiumPO.getTitleText(), title);
                Assert.assertEquals(commonPremiumPO.getMessageText(), message);
                break;
        }
    }

    @And("do validation {string} on pop up message is {string}")
    public void do_validation_on_naikkan_iklan_page_is(String validation, String saldo) {
        switch (validation){
            case "No Property" :
                Assert.assertEquals(naikkanIklanPO.getTextTitlePopUp(),"Anda Belum Memiliki Properti Aktif");
                Assert.assertEquals(naikkanIklanPO.getTextMessagePopUp(""),"Tambahkan properti terlebih dahulu.");
                break;
            case "No Saldo":
                Assert.assertEquals(naikkanIklanPO.getTextTitlePopUp(),"Anda belum bisa mengubah anggaran");
                Assert.assertEquals(naikkanIklanPO.getTextMessagePopUp("Minimum harus ada saldo 5.000"),"Minimum harus ada saldo 5.000 untuk mengubah anggaran. Silakan beli saldo terlebih dahulu.");
                break;
            case "Daily Budget" :
                Assert.assertEquals(naikkanIklanPO.getTextTitlePopUp(),"Anda yakin mengganti anggaran harian menjadi Rp"+saldo+"?");
                Assert.assertEquals(naikkanIklanPO.getTextMessagePopUp("harian"),"Jika Anda pilih “Ya, Ganti” anggaran harian baru akan langsung aktif.");
                break;
            case "Saldo Maksimal" :
                Assert.assertEquals(naikkanIklanPO.getTextTitlePopUp(),"Anda yakin mengganti anggaran harian menjadi saldo maksimal?");
                Assert.assertEquals(naikkanIklanPO.getTextMessagePopUp("maksimal"),"Jika Anda pilih “Ya, Ganti” anggaran harian diganti ke saldo maksimal akan langsung aktif.");
                break;
            case "Daily Budget Status Not Active" :
                Assert.assertEquals(naikkanIklanPO.getTextTitlePopUp(),"Anda yakin mengganti anggaran harian menjadi Rp"+saldo+"?");
                Assert.assertEquals(naikkanIklanPO.getTextMessagePopUp("maksimal"),"Jika Anda pilih “Ya, Ganti” maka jumlah anggaran harian akan langsung diganti.");
                break;
            case "Saldo Maksimal Status Not Active" :
                Assert.assertEquals(naikkanIklanPO.getTextTitlePopUp(),"Anda yakin mengganti anggaran harian menjadi saldo maksimal?");
                Assert.assertEquals(naikkanIklanPO.getTextMessagePopUp("harian"),"Jika Anda pilih “Ya, Ganti” anggaran harian diganti ke saldo maksimal.");
                break;
            case "On Toggle Saldo Less Than 5000 Never Allocate" :
                Assert.assertEquals(naikkanIklanPO.getTextTitlePopUp(), "Anda belum bisa menaikkan iklan.");
                Assert.assertEquals(naikkanIklanPO.getTextMessagePopUp("menaikkan posisi iklan"), "Silakan beli saldo terlebih dahulu untuk dapat menaikkan posisi iklan properti Anda.");
                break;
        }
    }

    @And("validate text {string} is available")
    public void mamiads_text_available(String text) {
        Assert.assertTrue(commonPremiumPO.containText(text).matches(text));
    }

    @And("tap back button on panduan Mamiads.")
    public void tap_back_button_on_panduan_mamiads() throws InterruptedException  {
        commonPremiumPO.clickOnPanduanMamiAdsBackButton();
    }

    @And("tap \"Next\" button inside \"Cara menggunakan MamiAds\".")
    public void tap_next_button_on_panduan_mamiads() throws InterruptedException {
        commonPremiumPO.clickOncaraMenggunakanMamiadsNextButton();
    }

}