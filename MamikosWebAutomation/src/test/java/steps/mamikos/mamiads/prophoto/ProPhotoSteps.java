package steps.mamikos.mamiads.prophoto;

import io.cucumber.java.en.And;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.mamiads.mamiads.BeliSaldoPO;
import pageobjects.mamikos.mamiads.prophoto.ProPhotoPO;
import utilities.ThreadManager;

public class ProPhotoSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ProPhotoPO proPhotoPO = new ProPhotoPO(driver);
    private BeliSaldoPO beliSaldoPO = new BeliSaldoPO(driver);

    @And("user verify title Pilih Paket Pro Photo")
    public void userChooseSaldo() throws InterruptedException{
        Assert.assertEquals(proPhotoPO.getTextTitlePilihProPhoto(), "Pilih Paket Pro Photo","Title doesn't match!");
    }

    @And("user click Beli on {string}")
    public void userClickBeliOn(String proPhotoPackageName) throws InterruptedException {
        proPhotoPO.clickOnBeliProPhoto(proPhotoPackageName);
    }

    @And("user verify detail tagihan pro photo is {string} with price {string}")
    public void userVerifyDetailTagihanProPhotoIsWithPrice(String proPhotoPackageName, String proPhotoPrice) {
        beliSaldoPO.setSaldo(proPhotoPrice);
        Assert.assertEquals(beliSaldoPO.detailTagihan(1), proPhotoPackageName);
        Assert.assertEquals(beliSaldoPO.detailTagihan(2), proPhotoPackageName);
        Assert.assertEquals(beliSaldoPO.detailTagihan(4),"Rp "+beliSaldoPO.getSaldo());
        Assert.assertEquals(beliSaldoPO.detailTagihan(5),"Bayar Sekarang Rp "+beliSaldoPO.getSaldo());
    }
}
