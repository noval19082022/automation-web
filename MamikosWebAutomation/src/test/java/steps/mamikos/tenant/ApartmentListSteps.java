package steps.mamikos.tenant;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.tenant.ApartmentDetails.ApartmentDetailsPO;
import pageobjects.mamikos.tenant.HistoryPO;
import pageobjects.mamikos.tenant.search.ApartmentListingPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class ApartmentListSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ApartmentListingPO listing = new ApartmentListingPO(driver);
    private ApartmentDetailsPO AptDetails = new ApartmentDetailsPO(driver);
    private HistoryPO history = new HistoryPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private HeaderPO header = new HeaderPO(driver);
    private String apartmentName;

    @When("user search apartment using keyword {string}")
    public void user_search_apartment_using_keyword(String text) throws InterruptedException {
        listing.searchApartment(text);
    }

    @Then("system display the {int} highest apartment lists have keyword {string} on detail address")
    public void system_display_the_highest_apartment_lists_have_keyword_on_detail_address(int numberOfList, String text) {
        for (int i = 1; i <= numberOfList; i++) {
            Assert.assertTrue(listing.getApartmentDetails(i).contains(text), "Apartment detail have keyword " + text);
        }
    }

    @And("user click on the selected apartment to go to the apartment details")
    public void user_click_on_the_selected_apartment_to_go_to_the_apartment_details() throws InterruptedException {
        AptDetails.clickDetailsApt();
        this.apartmentName = AptDetails.getApartmentTitle();
    }

    @When("user click on Hubungi Pengelola button")
    public void user_click_on_hubungi_pengelola_button() throws InterruptedException {
        AptDetails.clickContactApt();
    }

    @Then("Pop up {string} appears after click on Hubungi Pengelola")
    public void popup_appears_after_click_on_hubungi_pengelola(String hubungiPengelola) {
        Assert.assertTrue(AptDetails.verifyContactApt(hubungiPengelola), "Wrong Hubungi Apartemen Pop Up Message");
    }

    @Then("user see love icon become red")
    public void user_see_love_icon_become_red() {
        Assert.assertTrue(AptDetails.isLoveRed(), "Love icon is not red");
    }

    @When("user click on favourite button and saved number increase by {int}")
    public void user_click_on_favourite_button_and_saved_number_increase_by(Integer favouriteAdd) throws InterruptedException {
        int noFavouriteBefore = AptDetails.getApartmentFavouriteNumber();
        AptDetails.clickLoveBtn();
        int noFavouriteAfter = AptDetails.getApartmentFavouriteNumber();
        assertEquals(noFavouriteAfter, noFavouriteBefore + favouriteAdd,
                "Favourite number is not increase");
    }

    @When("user click on favorite button")
    public void user_click_on_favorite_button() throws InterruptedException {
        AptDetails.clickOnFavoriteButton();
    }

    @And("user click on unfavorite button")
    public void user_click_on_unfavorite_button(){
        AptDetails.clickOnUnFavoriteButton();
    }

    @Then("user see favorites apartment is in first list {string}")
    public void user_get_error_message(String message) throws InterruptedException {
        Assert.assertEquals(history.getFirstApartmentName(), message, "Ke unik");
    }

    @When("user click first apartment in favourite list and go to apartment details")
    public void user_click_first_apartment_in_favourite_list_and_go_to_apartment_details() throws InterruptedException {
        this.apartmentName = history.getFirstApartmentName();
        history.clickDetailsApt();
        AptDetails.waitTitleApartmentAppear();
    }

    @And("user click on unfavourite button and saved number decrease by {int}")
    public void user_click_on_unfavourite_button_and_saved_number_decrease_by(Integer favouriteAdd) throws InterruptedException {
        int noFavouriteBefore = AptDetails.getApartmentFavouriteNumber();
        AptDetails.clickLoveBtn();
        selenium.hardWait(5);
        int noFavouriteAfter = AptDetails.getApartmentFavouriteNumber();
        assertEquals(noFavouriteAfter, noFavouriteBefore - favouriteAdd,
                "Favourite number is not decrease");
    }

    @And("user go back to favourite tab")
    public void user_go_back_to_favourite_tab() {
        selenium.switchToWindow(1);
    }


    @Then("user doesn't see the apartment in favourite list")
    public void user_doesn_t_see_the_apartment_in_favourite_list() throws InterruptedException {
        if (!history.isFavouriteListEmpty()) {
            Assert.assertEquals(history.getFirstApartmentName(), this.apartmentName, "Fail to unfavourite apartment");
        } else {
            Assert.assertTrue(history.isFavouriteListEmpty(), "Fail to unfavourite apartment");
        }
    }

    @When("user click on the Search Ads Dropdown")
    public void user_click_on_the_search_ads_dropdown() throws InterruptedException {
        AptDetails.clickSearchAds();

    }

    @And("user click on the tenant profile")
    public void user_click_on_the_tenant_profile() throws InterruptedException {
        header.clickProfileDropdown();
    }

    @Then("user verify the Search Ads Dropdown {string}")
    public void user_get_list_dropdown_name_kos(String message){
        Assert.assertEquals(AptDetails.searchListDropdownNameKos(), message, "Kos");
    }
}