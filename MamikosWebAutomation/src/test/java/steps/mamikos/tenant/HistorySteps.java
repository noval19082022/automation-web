package steps.mamikos.tenant;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.account.LoginPO;
import pageobjects.mamikos.tenant.HistoryPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.List;

public class HistorySteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HistoryPO history = new HistoryPO(driver);
    private LoginPO login = new LoginPO(driver);

    private String favoriteKostPropertyFile1="src/test/resources/testdata/mamikos/favoritekost.properties";
    private String favoriteKostName = JavaHelpers.getPropertyValue(favoriteKostPropertyFile1,"kostName_" + Constants.ENV);

    @Then("I can see {string} in list number {int}")
    public void i_can_see_in_list_history(String type, int listNumber) {
        String kostName = "";
        if(type.equalsIgnoreCase("favoritekost"))
        {
            kostName = favoriteKostName;
        }
        Assert.assertEquals(history.getKosName(listNumber).toLowerCase(), kostName.toLowerCase(), "Kos name is not" + kostName);
    }

    @Then("system display tab")
    public void system_display_tab(List<String> tab) {
        history.waitTillFavoriteTabAppear();
        for (int i = 0; i < tab.size(); i++) {
            Assert.assertEquals(history.getTabTextByIndex(i), tab.get(i), "Tab " + tab.get(i) + " is not present");
        }
    }

    @And("I wait for property list to be loaded")
    public void iWaitForPropertyListToBeLoaded() {
        history.waitTiilPropertyListLoaded();
    }

    @Then("I should not see {string} in the property list number {int}")
    public void iShouldNotSeeInThePropertyList(String type, int listNumber) {
        String kostName = "";
        if(type.equalsIgnoreCase("favoritekost"))
        {
            kostName = favoriteKostName;
        }
        history.waitTiilPropertyListLoaded();
        Assert.assertNotEquals(history.getKosName2(listNumber), kostName, "Kost name is still" + kostName);
    }

    @Then("user see login pop up in favorite page")
    public void user_see_login_popUp_in_favorite_page() {
        login.checkLoginPopUpFromFavoritePage();
    }
}