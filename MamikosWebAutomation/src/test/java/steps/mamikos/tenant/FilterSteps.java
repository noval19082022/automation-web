package steps.mamikos.tenant;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageobjects.mamikos.tenant.search.FilterListingPO;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.List;

public class FilterSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private FilterListingPO filter = new FilterListingPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user filter apartment by {string} is {string}")
    public void user_filter_apartment_by_is(String filterBy, String filterValue) throws InterruptedException {
        filter.filterApartment(filterBy, filterValue);
    }

    @Then("system displays the {int} highest apartment lists by {string} is {string}")
    public void system_displays_the_highest_apartment_lists_by_is(int maxNumberOfList, String filterBy, String filterValue) throws InterruptedException {
        int price1 = 0;
        int numberOfList = filter.getNumberOfElements(filterBy);
        if(numberOfList > maxNumberOfList){
            numberOfList = maxNumberOfList;
        }
        for (int i = 1 ; i <= numberOfList ; i++){
            if (filterBy.equals("price")){
                int price2 = JavaHelpers.extractNumber(filter.getApartmentDetails(i, filterBy));

                if (filterValue.equals("Harga Termurah") && i > 1){
                    Assert.assertTrue(price1 <= price2, "Price Property 1 (" + price1 + ") should be less than or equal to Price Property 2 (" + price2 + ")");
                }else if (filterValue.equals("Harga Termahal") && i > 1){
                    Assert.assertTrue(price1 >= price2, "Price Property 1 (" + price1 + ") should be greater than or equal to Price Property 2 (" + price2 + ")");
                }
                price1 = price2;
            }else if (filterBy.equals("unit type")) {
                Assert.assertTrue(filter.getApartmentDetails(i, filterBy).contains(filterValue), "Filter Apartment By " + filterBy + " for index " + i + " is not " + filterValue);
            } else{
                Assert.assertEquals(filter.getApartmentDetails(i, filterBy), filterValue, "Filter Apartment By " + filterBy + " for index " + i + " is not " + filterValue);
            }
        }
    }

    @When("user filter apartment by {string} min {string} max {string}")
    public void user_filter_apartment_by_min_max(String filterBy, String minPrice, String maxPrice) throws InterruptedException {
        filter.filterApartment(filterBy, minPrice, maxPrice);
    }

    @When("user click on set button")
    public void user_click_on_set_button() throws InterruptedException {
        selenium.hardWait(4);
        filter.clickOnSetButton();
        selenium.hardWait(3);
    }

    @Then("system displays the {int} highest apartment lists by {string} with min price {string} max price {string}")
    public void system_displays_the_highest_apartment_lists_by_with_min_price_max_price(Integer numberOfList, String filterBy, String min, String max) throws InterruptedException {
        for (int i = 1 ; i <= numberOfList ; i++) {
            int price = JavaHelpers.extractNumber(filter.getApartmentDetails(i, filterBy));
            int maxPrice = Integer.parseInt(max);
            int minPrice = Integer.parseInt(min);
            Assert.assertTrue(price >= minPrice && price <= maxPrice, "Price (" + price + ") should be greater than or equal to Minimal Price (" + minPrice + ") &&  less than or equal to Maximal Price (" + maxPrice + ")");
            Assert.assertEquals(filter.getApartmentDetails(i, "time period"), "bulan", "Filter Apartment By Price Range should be display time period / bulan");
        }
    }

    @When("user click mamikos.com logo")
    public void user_click_mamikos_com_logo() throws InterruptedException {
        filter.clickOnMamikosLogo();
    }

    @When("user select {string} on filter Kota dan Area")
    public void user_select_on_filter_Kota_dan_Area(String city) throws InterruptedException{
        filter.clickCityText();
        filter.enterCityOnFilter(city);
    }

    @Then("user see same result will appear on the list")
    public void user_see_same_result_will_appear_on_the_list(List<String> listValidationArea) {
        for(int i = 0; i < listValidationArea.size(); i++) {
            System.out.println(filter.getCityAndAreaValidationOnList(i));
            Assert.assertEquals(filter.getCityAndAreaValidationOnList(i), listValidationArea.get(i),"Area validation not match");
        }
    }

}
