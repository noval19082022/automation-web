package steps.mamikos.tenant;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.List;

public class SearchListingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SearchListingPO searchListing = new SearchListingPO(driver);
    private KosDetailPO kosDetail = new KosDetailPO(driver);

    //Test Data
    private String bookingPropertyFile1 = "src/test/resources/testdata/mamikos/booking.properties";
    private String bookingKostName = JavaHelpers.getPropertyValue(bookingPropertyFile1, "kostName_" + Constants.ENV);
    private String kostNameSectionBooking = JavaHelpers.getPropertyValue(bookingPropertyFile1, "kostNameSectionBooking_" + Constants.ENV);

    private String favoriteKostPropertyFile1 = "src/test/resources/testdata/mamikos/favoritekost.properties";
    private String favoriteKostName = JavaHelpers.getPropertyValue(favoriteKostPropertyFile1, "kostName_" + Constants.ENV);

    private String bookingDifferentGenderFile1 = "src/test/resources/testdata/mamikos/OB.properties";
    private String bookingKostDifferentGenderMale = JavaHelpers.getPropertyValue(bookingDifferentGenderFile1, "kostNameMale_" + Constants.ENV);
    private String bookingKostDifferentGenderFemale = JavaHelpers.getPropertyValue(bookingDifferentGenderFile1, "kostNameFemale_" + Constants.ENV);
    private String kostBaruDilihat = JavaHelpers.getPropertyValue(bookingDifferentGenderFile1, "kostBaruDilihat_" + Constants.ENV);
    private String kostDraftHomepage8235 = JavaHelpers.getPropertyValue(bookingPropertyFile1, "kostDraftHomepage8235_" + Constants.ENV);

    private String loginFromHomePagePropertyFile1 = "src/test/resources/testdata/mamikos/loginFromHomePage.properties";
    private String searchInput = JavaHelpers.getPropertyValue(loginFromHomePagePropertyFile1, "searchInput_" + Constants.ENV);
    private String searchResult = JavaHelpers.getPropertyValue(loginFromHomePagePropertyFile1, "searchResult_" + Constants.ENV);

    //Test Data Payment
    private String payment = "src/test/resources/testdata/mamikos/payment.properties";
    private String propertyName_ = JavaHelpers.getPropertyValue(payment, "propertyName_" + Constants.ENV);
    private String propertyName_BNI = JavaHelpers.getPropertyValue(payment, "propertyName_BNI_" + Constants.ENV);
    private String propertyNameMars_ = JavaHelpers.getPropertyValue(payment, "propertyNameMars_" + Constants.ENV);

    //Test Data Apply Voucher
    private String voucher = "src/test/resources/testdata/mamikos/voucherku.properties";
    private String kostNameForApplyVoucherBBK = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherBBK_" + Constants.ENV);
    private String kostNameForApplyVoucherMamirooms = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherMamirooms_" + Constants.ENV);
    private String kostNameForApplyVoucherPremium = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherPremium_" + Constants.ENV);
    private String kostNameForApplyVoucherMamichecker = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherVerifiedByMamichecker_" + Constants.ENV);
    private String kostNameForApplyVoucherGaransi = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherGaransi_" + Constants.ENV);
    private String kostNameForApplyVoucherGoldPlus = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherGoldPlus_" + Constants.ENV);
    private String kostNameForApplyVoucherNewGoldPlus1 = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherNewGoldPlus1_" + Constants.ENV);
    private String kostNameForApplyVoucherNewGoldPlus2 = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherNewGoldPlus2_" + Constants.ENV);
    private String kostNameForApplyVoucherNewGoldPlus3 = JavaHelpers.getPropertyValue(voucher, "kostNameForApplyVoucherNewGoldPlus3_" + Constants.ENV);

    //Test Data MamiPoin
    private String mamipoin = "src/test/resources/testdata/mamikos/mamipoin.properties";
    private String kostNameForApplyMamiPoin = JavaHelpers.getPropertyValue(mamipoin, "kostNameForApplyMamiPoin_" + Constants.ENV);

    //Test Data OB
    private String OBBooking = "src/test/resources/testdata/mamikos/OB.properties";
    private String OBOwnerData = "src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
    private String kostOBFemale = JavaHelpers.getPropertyValue(OBBooking, "kostNameBookingFemale_" + Constants.ENV);
    private String kostNameNonPasutri = JavaHelpers.getPropertyValue(OBBooking, "kostMaleNonPasutri_" + Constants.ENV);
    private String kostNameOB = JavaHelpers.getPropertyValue(OBBooking, "kostNameOB_" + Constants.ENV);
    private String kostNameOBWithDP = JavaHelpers.getPropertyValue(OBBooking, "ancientWeb_" + Constants.ENV);
    private String kostNameOBWithDPAT = JavaHelpers.getPropertyValue(OBBooking, "ancientWebAT_" + Constants.ENV);
    private String obMixKost = JavaHelpers.getPropertyValue(OBBooking, "obMixKost_" + Constants.ENV);
    private String obOwnerRejectKost = JavaHelpers.getPropertyValue(OBOwnerData, "wendyWildRitKostName_" + Constants.ENV);
    private String obOtherPrice = JavaHelpers.getPropertyValue(OBOwnerData, "ancient_" + Constants.ENV);
    private String obDOTFKost = JavaHelpers.getPropertyValue(OBOwnerData, "wildRiftDOTF_" + Constants.ENV);
    private String obKostMhsValidation = JavaHelpers.getPropertyValue(OBBooking, "kostMhsValidation_" + Constants.ENV);
    private String obKostKaryawan = JavaHelpers.getPropertyValue(OBBooking, "kostKaryawan_" + Constants.ENV);
    private String obKostPromoNgebut= JavaHelpers.getPropertyValue(OBBooking, "kostPromoNgebut_" + Constants.ENV);
    private String obKostBookingAddtional = JavaHelpers.getPropertyValue(OBOwnerData, "kostBookingAddtional_" + Constants.ENV);
    private String obKostSayaHomepageReject = JavaHelpers.getPropertyValue(OBBooking, "kosHomepageReject_" + Constants.ENV);
    private String obKostRejectFull = JavaHelpers.getPropertyValue(OBOwnerData, "ownerRejectFullReason_" + Constants.ENV);
    private String obKostSayaCancelHomepage = JavaHelpers.getPropertyValue(OBOwnerData, "kostCancelHomepage_" + Constants.ENV);
    private String obKostSayaHomepageRejectNWaiting = JavaHelpers.getPropertyValue(OBBooking, "kostReject&Waiting_" + Constants.ENV);

    //Test Data Discovery Communication
    private String DC = "src/test/resources/testdata/mamikos/BX.properties";
    private String DC_Automation_A = JavaHelpers.getPropertyValue(DC, "DC_Automation_A_" + Constants.ENV);
    private String DC_Automation_B = JavaHelpers.getPropertyValue(DC, "DC_Automation_B_" + Constants.ENV);
    private String DC_Automation_Gubeng_A = JavaHelpers.getPropertyValue(DC, "DC_Automation_Gubeng_A_" + Constants.ENV);
    private String BX_Automation_PLM_A = JavaHelpers.getPropertyValue(DC, "BX_Automation_PLM_A_" + Constants.ENV);
    private String BX_Automation_PLM_B = JavaHelpers.getPropertyValue(DC, "BX_Automation_PLM_B_" + Constants.ENV);
    private String BX_Automation_Promo_A = JavaHelpers.getPropertyValue(DC, "BX_Automation_Promo_A_" + Constants.ENV);

    //Tenant Engagement Test Data
    private String tengOwner = "src/test/resources/testdata/mamikos/tenant-engagement-owner.properties";
    private String tengBookingKost = JavaHelpers.getPropertyValue(tengOwner, "kost_name_manage_bills_" + Constants.ENV);
    private String adminFirstInvoiceKost = JavaHelpers.getPropertyValue(tengOwner, "adminFirstInvoiceKostName_" + Constants.ENV);
    private String settlementInvoiceKost = JavaHelpers.getPropertyValue(tengOwner, "settlementInvoiceKostName_" + Constants.ENV);
    private String invoiceDetailKost = JavaHelpers.getPropertyValue(tengOwner, "invoiceDetailKostName_" + Constants.ENV);
    private String invoiceDetailKostDP = JavaHelpers.getPropertyValue(tengOwner, "invoiceDetailDPKostName_" + Constants.ENV);
    private String invoiceDetailKostDPDepositAddPrice = JavaHelpers.getPropertyValue(tengOwner, "invoiceDetailDPDepositAddFee_" + Constants.ENV);
    private String tengAddsOnKost = JavaHelpers.getPropertyValue(tengOwner, "addsOnKost_" + Constants.ENV);
    private String tengKostApik = JavaHelpers.getPropertyValue(tengOwner, "tengKostApik_" + Constants.ENV);
    private String tengKostSinggahSini = JavaHelpers.getPropertyValue(tengOwner, "tengKostSinggahSini_" + Constants.ENV);

    @And("I search property with name {string} and select matching result to go to kos details page")
    public void i_search_property_name_and_select_matching_result(String type) throws InterruptedException {
        String kostName = "";
        if (type.equalsIgnoreCase("booking")) {
            //prefer to substring here, instead of change the props file (just in case someone use the props)
            kostName = bookingKostName;
        } else if (type.equalsIgnoreCase("favoritekost")) {
            kostName = favoriteKostName;
        } else if (type.equalsIgnoreCase("sectionBooking")) {
            kostName = kostNameSectionBooking;
        } else if (type.equalsIgnoreCase("female_test_data")) {
            kostName = bookingKostDifferentGenderFemale;
        } else if (type.equalsIgnoreCase("male_test_data")) {
            kostName = bookingKostDifferentGenderMale;
        } else if (type.equalsIgnoreCase("payment")) {
            kostName = propertyName_;
        } else if (type.equalsIgnoreCase("mars kos")) {
            kostName = propertyNameMars_;
        } else if (type.equalsIgnoreCase("paymentBNI")) {
            kostName = propertyName_BNI;
        } else if (type.equalsIgnoreCase("applyVoucherBBK")) {
            kostName = kostNameForApplyVoucherBBK;
        } else if (type.equalsIgnoreCase("applyVoucherMamirooms")) {
            kostName = kostNameForApplyVoucherMamirooms;
        } else if (type.equalsIgnoreCase("applyVoucherPremium")) {
            kostName = kostNameForApplyVoucherPremium;
        } else if (type.equalsIgnoreCase("applyVoucherMamichecker")) {
            kostName = kostNameForApplyVoucherMamichecker;
        } else if (type.equalsIgnoreCase("applyVoucherGaransi")) {
            kostName = kostNameForApplyVoucherGaransi;
        } else if (type.equalsIgnoreCase("applyVoucherGoldPlus")) {
            kostName = kostNameForApplyVoucherGoldPlus;
        } else if (type.equalsIgnoreCase("applyVoucherNewGoldPlus1")) {
            kostName = kostNameForApplyVoucherNewGoldPlus1;
        } else if (type.equalsIgnoreCase("applyVoucherNewGoldPlus2")) {
            kostName = kostNameForApplyVoucherNewGoldPlus2;
        } else if (type.equalsIgnoreCase("applyVoucherNewGoldPlus3")) {
            kostName = kostNameForApplyVoucherNewGoldPlus3;
        } else if (type.equalsIgnoreCase("baruDilihat")) {
            kostName = kostBaruDilihat;
        } else if (type.equalsIgnoreCase("ob booking female")) {
            kostName = kostOBFemale;
        } else if (type.equalsIgnoreCase("DC A")) {
            kostName = DC_Automation_A;
        } else if (type.equalsIgnoreCase("ob booking male non pasutri")) {
            kostName = kostNameNonPasutri;
        } else if (type.equalsIgnoreCase("booking kost ob")) {
            kostName = kostNameOB;
        } else if (type.equalsIgnoreCase("ob additional price kost with dp")) {
            kostName = kostNameOBWithDP;
        } else if (type.equalsIgnoreCase("ob mix kost")) {
            kostName = obMixKost;
        } else if (type.equalsIgnoreCase("ob owner reject")) {
            kostName = obOwnerRejectKost;
        } else if (type.equalsIgnoreCase("DC B")) {
            kostName = DC_Automation_B;
        } else if (type.equalsIgnoreCase("applyMamipoin")) {
            kostName = kostNameForApplyMamiPoin;
        } else if (type.equalsIgnoreCase("ob other price")) {
            kostName = obOtherPrice;
        } else if (type.equalsIgnoreCase("ob dotf")) {
            kostName = obDOTFKost;
        } else if (type.equalsIgnoreCase("ob additional price kost with dp automation")) {
            kostName = kostNameOBWithDPAT;
        } else if (type.equalsIgnoreCase("DC Gubeng A")) {
            kostName = DC_Automation_Gubeng_A;
        } else if (type.equalsIgnoreCase("PLM A")) {
            kostName = BX_Automation_PLM_A;
        } else if (type.equalsIgnoreCase("PLM B")) {
            kostName = BX_Automation_PLM_B;
        } else if (type.equalsIgnoreCase("Promo A")) {
            kostName = BX_Automation_Promo_A;
        } else if (type.equalsIgnoreCase("teng booking")) {
            kostName = tengBookingKost;
        } else if (type.equalsIgnoreCase("teng admin first invoice")) {
            kostName = adminFirstInvoiceKost;
        } else if (type.equalsIgnoreCase("teng settlement invoice kost")) {
            kostName = settlementInvoiceKost;
        } else if (type.equalsIgnoreCase("teng invoice kost detail")) {
            kostName = invoiceDetailKost;
        } else if (type.equalsIgnoreCase("teng invoice kost detail dp")) {
            kostName = invoiceDetailKostDP;
        } else if (type.equalsIgnoreCase("teng invoice detail kost dp deposit additional price")) {
            kostName = invoiceDetailKostDPDepositAddPrice;
        } else if (type.equalsIgnoreCase("teng adds on kost")) {
            kostName = tengAddsOnKost;
        } else if (type.equalsIgnoreCase("OB kost mhs validation")) {
            kostName = obKostMhsValidation;
        } else if (type.equalsIgnoreCase("OB kost karyawan")) {
            kostName = obKostKaryawan;
        } else if (type.equalsIgnoreCase("OB kost promo ngebut")) {
            kostName = obKostPromoNgebut;
        } else if (type.equalsIgnoreCase("OB kost booking additional")){
            kostName = obKostBookingAddtional;
        } else if (type.equalsIgnoreCase("teng kost singgah sini")) {
            kostName = tengKostSinggahSini;
        }
        else if (type.equalsIgnoreCase("teng kost apik")) {
            kostName = tengKostApik;
        } else if (type.equalsIgnoreCase("ob kos reject full")) {
            kostName = obKostRejectFull;
        }
        else if (type.equalsIgnoreCase("ob kost saya homepage reject")){
            kostName = obKostSayaHomepageReject;
        }
        else if (type.equalsIgnoreCase("ob draft n waiting homepage")){
            kostName = kostDraftHomepage8235;
        }
        else if (type.equalsIgnoreCase("ob kost saya homepage cancel")){
            kostName = obKostSayaCancelHomepage;
        }
        else if (type.equalsIgnoreCase("ob kost saya homepage reject n waiting")){
            kostName = obKostSayaHomepageRejectNWaiting;
        }
        else {
            kostName = type;
        }
        searchListing.searchPropertyAndSelectResult(kostName, false);
    }

    @And("I search property with name {string} and click one of results {string}")
    public void i_search_property_with_name_and_click_one_of_results(String type, String type2) throws InterruptedException {
        String searchKey = "";
        String resultList = "";
        if (type.equalsIgnoreCase("loginfromhomepage")) {
            searchKey = searchInput;
        }
        searchListing.setTextInInputSearchListing(searchKey);
        if (type2.equalsIgnoreCase("searchkey")) {
            resultList = searchResult;
        }
        searchListing.clickResultLists(resultList);
    }

    @And("User click BBK pop up")
    public void userClickIUnderstandPopUp() throws InterruptedException {
        searchListing.clickFTUEKosListingPopUp();
    }

    @And("User click a kost with tag Book Now")
    public void userClickAKostWithTagBookNow() throws InterruptedException {
        searchListing.clickOnKostWithBookNowTag();
    }

    @Then("user check the legend of map price cluster")
    public void user_check_the_legend_of_map_price_cluster(List<String> wording) {
        for (int i = 0; i < wording.size(); i++) {
            Assert.assertTrue(searchListing.isLegendPresent(wording.get(i)), "Cluster " + wording + " is not present");
            Assert.assertEquals(searchListing.getLegendDesc(wording.get(i)), wording.get(i), "Cluster icon not equal to " + wording.get(i));
        }
    }

    @And("user check the legend of map description cluster")
    public void user_check_the_legend_of_map_description_cluster(List<String> wording) {
        for (int i = 0; i < wording.size(); i++) {
            Assert.assertTrue(searchListing.isLegendDescPresent(wording.get(i)), "Description " + wording + " is not present");
            Assert.assertEquals(searchListing.getLegendDescText(wording.get(i)), wording.get(i), "Description text not equal to " + wording.get(i));
        }
    }

    @And("user check the legend of map information cluster")
    public void user_check_the_legend_of_map_information_cluster(List<String> wording) {
        for (int i = 0; i < wording.size(); i++) {
            Assert.assertTrue(searchListing.isLegendInformationPresent(wording.get(i)), "Information " + wording + " is not present");
            Assert.assertTrue(searchListing.getLegendInformationText(wording.get(i)).contains(wording.get(i)), "Information text not equal to " + wording.get(i));
        }
    }

    @And("user close the legend map")
    public void user_close_the_legend_map() throws InterruptedException {
        searchListing.clickMapLegendButton();
    }

    @Then("user see the pop up closed")
    public void user_see_the_pop_up_closed() {
        Assert.assertFalse(searchListing.isMapLegendPresent(), "Map Legend still appears!");
    }

    @Then("title listing that appear have location in {string}")
    public void title_listing_that_appear_have_location_in(String city) {
        Assert.assertTrue(searchListing.getTitleListingResult().contains(city), "Title Listing Result is not equals with the keyword!");
    }

    @Then("user validates the result is {string}")
    public void user_validates_the_result_is(String title) {
        Assert.assertEquals(searchListing.getTitleAreaText(), title, "Title area is not equals with keyword!");
    }

    @And("user sees nominatim map on landing boundaries")
    public void user_sees_nominatim_map_on_landing_boundaries() {
        Assert.assertTrue(searchListing.isNominatimMapPresent(), "Nominatim maps is not present on 3rd coloum! ");
        Assert.assertTrue(searchListing.isSearchByMapButtonPresent(), "Search by map button is not present!");
    }

    @And("user sees kos listing up to {int}")
    public void user_sees_kos_listing_up_to(int listing) {
        Assert.assertTrue(searchListing.isBottomKostListingPresent(listing), "Bottom kost list is not present!");
    }

    @And("user sees back to top and see more property buttons")
    public void user_sees_back_to_top_and_see_more_property_buttons() {
        Assert.assertTrue(searchListing.isSeeMorePropertyPresent(), "Button Lihat lebih banyak lagi is not present!");
        Assert.assertTrue(searchListing.isBacktoTopPresent(), "Button back to top is not present!");
    }

    @And("user clicks and validates see more property and back to top buttons")
    public void user_clicks_and_validates_see_more_property_and_back_to_top_buttons() throws InterruptedException {
        searchListing.clickSeeMorePropertyButton();
        Assert.assertTrue(searchListing.isMoreKostListingPresent(), "More property on bottom is not present!");
        searchListing.clickBackToTopButton();
        Assert.assertTrue(searchListing.isNominatimMapPresent(), "Nominatim maps is not present on 3rd coloum! ");
    }

    @When("user set filter price and input {string} , {string}")
    public void user_set_filter_price_and_input(String price1, String price2) throws InterruptedException {
        searchListing.clickFilterPrice();
        searchListing.inputMinPrice(price1);
        searchListing.inputMaxPrice(price2);
        searchListing.clickSavePriceFilter();
    }

    @Then("user sees kos listing is just 1")
    public void user_sees_kos_listing_is_just() throws InterruptedException {
        Assert.assertFalse(searchListing.isSecondKosListingPresent(), "Kos Listing result is not 1");
    }

    @And("user sees nominatim map is on center of kos listing")
    public void user_sees_nominatim_map_is_on_center_of_kos_listing() {
        Assert.assertTrue(searchListing.isNominatimMapPresentOncenter(), "Map on center is not present!");
        Assert.assertTrue(searchListing.isSearchByMapButtonPresent(), "Button search by maps is not present!");
    }

    @And("user sees messages {string} in landing boundaries if result is one")
    public void user_sees_messages_in_landing_boundaries_if_result_is_one(String alert) {
        Assert.assertTrue(searchListing.isAlertMessageNominatimPresent(alert), "Alert message is not present!");
        Assert.assertTrue(searchListing.isResetFilterButtonPresent(), "Reset filter button is not present!");
    }

    @When("user clicks reset button")
    public void user_clicks_reset_button() throws InterruptedException {
        searchListing.clickResetFilterButton();
    }

    @And("user sees the empty state in area boundaries landing page {string}")
    public void user_sees_the_empty_state_in_area_boundaries_landing_page(String emptyStateText) throws InterruptedException {
        Assert.assertTrue(searchListing.isEmptyStateTextAreaBoundariesLandingPagePresent(emptyStateText), "Empty State Text is not present!");
    }

    @When("user selects filter {string}")
    public void user_selects_filter(String filter) throws InterruptedException {
        searchListing.clickFilterMamiroomsButton();
    }

    @When("user clicks search by maps button")
    public void user_clicks_search_by_maps_button() throws InterruptedException {
        searchListing.clickSearchByMapsButton();
        kosDetail.clickClosePromoNgebutPopUp();
    }

    @When("user selects sorting {string} in area boundaries landing page")
    public void user_selects_sorting_in_area_boundaries_landing_page(String sorting) throws InterruptedException {
        searchListing.selectsSortingOption(sorting);
    }

    @Then("user validates the price of first listing is cheaper than the last listing in area boundaries landing page")
    public void user_validates_the_price_of_first_listing_is_cheaper_than_the_last_listing_in_area_boundaries_landing_page() {
        Assert.assertTrue(searchListing.getFirstPricePropertyListing() < searchListing.getLastPricePropertyListing(), "First number is not smaller than last number!");
    }

    @And("user validates the price of first listing is cheaper than the last listing in listing property page")
    public void user_validates_the_price_of_first_listing_is_cheaper_than_the_last_listing_in_listing_property_page() {
        Assert.assertTrue(searchListing.getFirstPricePropertyPageListing() < searchListing.getLastPricePropertyPageListing(), "First number is not smaller than last number!");
    }

    @Then("user validates the price of first listing is more expensive than the last listing in area boundaries landing page")
    public void user_validates_the_price_of_first_listing_is_more_expensive_than_the_last_listing_in_area_boundaries_landing_page() {
        Assert.assertTrue(searchListing.getFirstPricePropertyListing() > searchListing.getLastPricePropertyListing(), "First number is cheaper than last number!");
    }

    @And("user validates the price of first listing is more expensive than the last listing in listing property page")
    public void user_validates_the_price_of_first_listing_is_more_expensive_than_the_last_listing_in_listing_property_page() {
        Assert.assertTrue(searchListing.getFirstPricePropertyPageListing() > searchListing.getLastPricePropertyPageListing(), "First number is cheaper than last number!");
    }

    @And("user sets filter gender {string}")
    public void user_sets_filter_gender(String gender) throws InterruptedException {
        searchListing.clickFilterByGender(gender);
    }

    @Then("user validates the result kos gender is {string}")
    public void user_validates_the_result_kos_gender_is(String gender) {
        List<String> resultList = searchListing.getGenderInListing(gender);
        for (String a : resultList) {
            Assert.assertTrue(a.contains(gender), "Search result " + a + " not in correct gender");
        }
    }

    @And("user sets filter top facility {string}")
    public void user_sets_filter_top_facility(String fac) throws InterruptedException {
        searchListing.clickFilterByFacility(fac);
    }

    @Then("user validates the result kos facility is {string}")
    public void user_validates_the_result_kos_facility_is(String fac) throws InterruptedException {
        List<String> resultList = searchListing.getFacilityInKostListing(fac);
        for (String a : resultList) {
            Assert.assertTrue(a.contains(fac), "Search result " + a + " not in correct facility");
        }
    }

    @When("user selects first kost on listing")
    public void user_selects_first_kost_on_listing() throws InterruptedException {
        searchListing.clickFirstKost();
    }

    @When("user click on the Kos Andalan filter")
    public void user_click_on_the_Kos_Andalan_filter() throws InterruptedException {
        searchListing.clickFilterGPButton();
    }

    @When("user turn on the toggler button")
    public void user_turn_on_the_toggler_button() throws InterruptedException {
        searchListing.clickAndalanActivatedToggle();
    }

    @Then("user validated the result kos have label {string}")
    public void user_validated_the_result_kos_have_label(String filter){
        List<String> resultList = searchListing.getFilterLabelInListing(filter);
        for (String a : resultList) {
            Assert.assertTrue(a.contains(filter), "Search result " + a + " not in correct filter");
        }
    }

    @And("user activate elite filter")
    public void userActivateEliteFilter () throws InterruptedException {
        searchListing.activateEliteFilter();
    }


    @Then("user see elite label on kos card")
    public void userSeeEliteLabelOnKosCard () {
        Assert.assertTrue(searchListing.isEliteLabelPresent(), "Elite Label is not displyed");
    }

    @Then("user see text empty result {string}")
    public void user_see_text_empty_result(String textKostNotFound) throws InterruptedException {
        Assert.assertEquals(searchListing.getTextKostNotFound(), textKostNotFound, "Title not appears");
    }

    @Then("user see text empty result filter {string}")
    public void user_see_text_empty_result_filter(String textFilterSuggest) throws InterruptedException {
        Assert.assertEquals(searchListing.getTextFilterSuggest(), textFilterSuggest, "Title not appears");
    }

    @Then("user click on the Kos Pilihan filter")
    public void user_click_on_the_Kos_Pilihan_filter() throws InterruptedException {
        searchListing.clickActivatePilihan();
    }

    @Then("user sets filter top kos rule {string}")
    public void user_sets_filter_top_kos_rule(String kosRule) throws InterruptedException {
        searchListing.clickFilterByKosRule(kosRule);
    }

    @Then("user see {string}")
    public void user_see(String apartementNotFound) throws InterruptedException {
        Assert.assertEquals(searchListing.getTextApartementNotFound(), apartementNotFound, "Title not appears");
    }

    @Then("user search for keyword apartemen {string}")
    public void user_search_for_keyword_apartemen(String areaCity) {
        searchListing.enterAreaCity(areaCity);
    }

    @When("user input {string} at form input keyword")
    public void user_input_at_form_input_keyword(String keywordArea) {
        searchListing.enterKeywordArea(keywordArea);
    }

    @Then("user see badge apartemen for result")
    public void user_see_badge_apartemen_for_result() {
        Assert.assertTrue(searchListing.isBadgeApartementPresent(), "badge apartement is not displyed");
    }

    @And("user selects sorting {string} in kost listing")
    public void userSelectsSortingInKostListing(String sorting) throws InterruptedException {
        searchListing.selectsSortingOption(sorting);
    }

    @Then("user sees kos listing is more than 1")
    public void userSeesKosListingIsMoreThan() throws InterruptedException {
        Assert.assertTrue(searchListing.isSecondKosListingPresent(), "Kost Listing is still 1");
    }
}


