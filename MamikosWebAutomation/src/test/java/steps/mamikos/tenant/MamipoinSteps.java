package steps.mamikos.tenant;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.tenant.mamipoin.MamipoinTenantPO;
import utilities.ThreadManager;
import java.util.List;


public class MamipoinSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private MamipoinTenantPO mamipoinTenantPO = new MamipoinTenantPO(driver);

    @Then("user verify mamipoin tenant entry point is not displayed")
    public void user_verify_mamipoin_tenant_entry_point_is_not_displayed() throws InterruptedException {
        Assert.assertFalse(mamipoinTenantPO.isMamipoinTenantEntryPointNotDisplayed());
    }

    @Then("user verify mamipoin tenant entry point is displayed")
    public void user_verify_mamipoin_tenant_entry_point_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isMamipoinTenantEntryPointNotDisplayed());
    }

    @Then("user verify the amount of poin owned by the tenant is {string}")
    public void user_verify_the_amount_of_poin_owned_by_the_tenant_is(String amountOfPoin) {
        Assert.assertEquals(mamipoinTenantPO.verifyAmountOfPoinOwnedByTenant(amountOfPoin), amountOfPoin, "Amount of poin text not equals!");
    }

    @Then("user verify the amount of poin saya is {string}")
    public void user_verify_the_amount_of_poin_saya_is(String amountOfPoin) {
        Assert.assertEquals(mamipoinTenantPO.verifyAmountOfPoinSaya(amountOfPoin), amountOfPoin, "Amount of poin text not equals!");
    }

    @And("user clicks on mamipoin tenant entry point button")
    public void user_clicks_on_mamipoin_tenant_entry_point_button() throws InterruptedException {
        mamipoinTenantPO.clickOnMamipoinTenantEntryPointButton();
    }

    @When("user verify title in the mamipoin tenant landing page is displayed")
    public void user_verify_title_in_the_mamipoin_tenant_landing_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTitleInTheMamipoinTenantLandingPageDisplayed());
    }

    @When("user verify informasi poin button is displayed")
    public void user_verify_informasi_poin_button_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isInformasiPoinButtonDisplayed());
    }

    @When("user clicks on informasi poin button")
    public void user_clicks_on_informasi_poin_button() throws InterruptedException {
        mamipoinTenantPO.clickOnInformasiPoinButton();
    }

    @When("user verify title in the informasi poin page is displayed")
    public void user_verify_title_in_the_informasi_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTitleInTheInformasiPoinPageDisplayed());
    }

    @When("user verify subtitle in the informasi poin page is displayed")
    public void user_verify_subtitle_in_the_informasi_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isSubtitleInTheInformasiPoinPageDisplayed());
    }

    @When("user verify table title tanggal kedaluwarsa is displayed")
    public void user_verify_table_title_tanggal_kedaluwarsa_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTableTitleTanggalKedaluwarsaDisplayed());
    }

    @When("user verify table title jumlah mamipoin is displayed")
    public void user_verify_table_title_jumlah_mamipoin_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTableTitleJumlahMamipoinDisplayed());
    }

    @When("user verify title tidak ada poin yang tersedia is displayed")
    public void user_verify_title_tidak_ada_poin_yang_tersedia_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTitleTidakAdaPoinYangTersediaDisplayed());
    }

    @And("user verify subtitle tidak ada poin yang tersedia is displayed")
    public void user_verify_subtitle_tidak_ada_poin_yang_tersedia_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isSubtitleTidakAdaPoinYangTersediaDisplayed());
    }

    @And("user verify button lihat caranya is displayed")
    public void user_verify_button_lihat_caranya_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isButtonLihatCaranyaDisplayed());
    }

    @When("user clicks on lihat caranya button")
    public void user_clicks_on_lihat_caranya_button() throws InterruptedException {
        mamipoinTenantPO.clickOnLihatCaranyaButton();
    }

    @When("user verify riwayat poin button is displayed")
    public void user_verify_riwayat_poin_button_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isRiwayatPoinButtonDisplayed());
    }

    @When("user clicks on riwayat poin button")
    public void user_clicks_on_riwayat_poin_button() throws InterruptedException {
        mamipoinTenantPO.clickOnRiwayatPoinButton();
    }

    @When("user verify title in the riwayat poin page is displayed")
    public void user_verify_title_in_the_riwayat_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTitleInTheRiwayatPoinPageDisplayed());
    }

    @When("user verify filter semua in the riwayat poin page is displayed")
    public void user_verify_filter_semua_in_the_riwayat_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isFilterSemuaInTheRiwayatPoinPageDisplayed());
    }

    @When("user verify filter poin diterima in the riwayat poin page is displayed")
    public void user_verify_filter_poin_diterima_in_the_riwayat_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isFilterPoinDiterimaInTheRiwayatPoinPageDisplayed());
    }

    @When("user verify filter poin ditukar in the riwayat poin page is displayed")
    public void user_verify_filter_poin_ditukar_in_the_riwayat_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isFilterPoinDitukarInTheRiwayatPoinPageDisplayed());
    }

    @When("user verify filter poin kedaluwarsa in the riwayat poin page is displayed")
    public void user_verify_filter_poin_kedaluwarsa_in_the_riwayat_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isFilterPoinKedaluwarsaInTheRiwayatPoinPageDisplayed());
    }

    @When("user verify title riwayat masih kosong is displayed")
    public void user_verify_title_riwayat_masih_kosong_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTitleRiwayatMasihKosongDisplayed());
    }

    @When("user verify subtitle riwayat masih kosong is displayed")
    public void user_verify_subtitle_riwayat_masih_kosong_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isSubtitleRiwayatMasihKosongDisplayed());
    }

    @When("user verify dapatkan poin button is displayed")
    public void user_verify_dapatkan_poin_button_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isDapatkanPoinButtonDisplayed());
    }

    @When("user clicks on dapatkan poin button")
    public void user_clicks_on_dapatkan_poin_button() throws InterruptedException {
        mamipoinTenantPO.clickOnDapatkanPoinButton();
    }

    @When("user verify title in the dapatkan poin page is displayed")
    public void user_verify_title_in_the_dapatkan_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTitleInTheDapatkanPoinPageDisplayed());
    }

    @When("user verify tab petunjuk in the dapatkan poin page is displayed")
    public void user_verify_tab_petunjuk_in_the_dapatkan_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTabPetunjukInTheDapatkanPoinPageDisplayed());
    }

    @When("user verify tab syarat dan ketentuan in the dapatkan poin page is displayed")
    public void user_verify_tab_syarat_dan_ketentuan_in_the_dapatkan_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isTabSyaratDanKetentuanInTheDapatkanPoinPageDisplayed());
    }

    @When("user verify link pusat bantuan in the dapatkan poin page is displayed")
    public void user_verify_link_pusat_bantuan_in_the_dapatkan_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinTenantPO.isLinkPusatBantuanInTheDapatkanPoinPageDisplayed());
    }

    @When("user clicks link on pusat bantuan")
    public void user_clicks_link_on_pusat_bantuan() throws InterruptedException {
        mamipoinTenantPO.clickLinkOnPusatBantuan();
    }

    @And("user verify dapatkan poin headline {string}")
    public void user_verify_dapatkan_poin_headline(String headline) {
        Assert.assertEquals(mamipoinTenantPO.getDapatkanPointHeadline(), headline, "Headline text is not equal to " + headline);
    }

    @Then("user verify dapatkan poin subtitle {string}")
    public void user_verify_dapatkan_poin_subtitle(String subtitle) {
        Assert.assertEquals(mamipoinTenantPO.getDapatkanPoinSubtitle(), subtitle, "Subtitle text is not equal to " + subtitle);
    }

    @Then("user verify content on dapatkan poin page")
    public void user_verify_content_on_dapatkan_poin_page() {
        List<WebElement> element = mamipoinTenantPO.content;
        for (int i = 0; i < mamipoinTenantPO.content.size(); i++) {
            Assert.assertTrue(mamipoinTenantPO.isContentOnDapatkanPoinAppear(element.get(i)), "All contents on dapatkan poin page are not appeared");
        }
        Assert.assertTrue(mamipoinTenantPO.isFooterOnDapatkanPoinAppear(), "Footer on dapatkan poin page is not appeared");
    }

    @When("user click link {string}")
    public void user_click_link(String link) {
        mamipoinTenantPO.clickLinkOnPetunjuk(link);
    }

    @Then("user verify expired point information on mamipoin landing page {string}")
    public void user_verify_expired_point_information_on_mamipoin_landing_page(String desc) throws InterruptedException {
        Assert.assertEquals(mamipoinTenantPO.getExpiredPointInfoOnLandingPage(), desc, "Expired information point not equal to " + desc);
    }

    @Then("user verify expired point on information point page")
    public void user_verify_expired_point_on_information_point_page(List<String> date) {
        List<WebElement> element = mamipoinTenantPO.expDateList;
        for (int i = 0; i < mamipoinTenantPO.expDateList.size(); i++) {
            Assert.assertEquals(mamipoinTenantPO.getExpiredDate(element.get(i)), date.get(i), "Expired point date is not equal to " + date.get(i));
        }
    }

    @And("user verify only {int} list of points that displayed")
    public void user_verify_only_list_of_points_that_displayed(int listCount) {
        List<WebElement> element = mamipoinTenantPO.expDateList;
        int counter =0;
        for (int i = 0; i < mamipoinTenantPO.expDateList.size(); i++) {
            Assert.assertTrue(mamipoinTenantPO.isExpiredDateListAppear(element.get(i)), "Expired date lists are not appeared");
            counter++;
        }
        Assert.assertTrue(counter <= listCount, "Expired date list is not display " + listCount + " items");
    }

    @And("user verify {string} on list of point histories")
    public void user_verify_list_of_point_histories_on_riwayat_poin_page(String title) throws InterruptedException {
        mamipoinTenantPO.scrollToLastItemOnPointHistoryList(title);
        Assert.assertTrue(mamipoinTenantPO.isLastItemOnPointHistoryListAppear(), "Point history lists are not appeared");
    }

    @And("user verify {string} class on selected filter {string}")
    public void user_verify_class_on_selected_filter(String attribute, String filter) {
        Assert.assertEquals(mamipoinTenantPO.getFilterElementAttribute(filter).substring(30,47), attribute, "Selected filter doesn't have " + attribute + " value");
        Assert.assertEquals(mamipoinTenantPO.getFilterText(filter), filter, "Selected filter is not equal to " + filter);
    }

    @When("user click {string} filter")
    public void user_click_filter(String filter) throws InterruptedException {
        mamipoinTenantPO.clickOnFilters(filter);
    }

    @And("user click mamipoin toggle")
    public void user_click_mamipoin_toggle() throws InterruptedException{
        mamipoinTenantPO.clickOnMamiPoinToggle();
    }

    @Then("user see total potongan mamipoin {string}")
    public void user_see_total_potongan_mamipoin(String totalMamipoin) {
        Assert.assertEquals(mamipoinTenantPO.getTotalDiscountMamipoin(), totalMamipoin, "total discount mamipoint is not match");
    }

    @And("user verify point history grouped by months")
    public void user_verify_point_history_grouped_by_months(List<String> date) throws InterruptedException {
        for (int i = 0; i < date.size(); i++) {
            Assert.assertTrue(mamipoinTenantPO.isPointHistoryDateAppear(date.get(i)), "Point history is not grouped by month");
            Assert.assertEquals(mamipoinTenantPO.getPointHistoryDateText(date.get(i)), date.get(i), "Point history date is not equal to " + date.get(i));
        }
    }

    @Then("user scroll down to pusat bantuan")
    public void user_scroll_down_to_pusat_bantuan() {
        Assert.assertTrue(mamipoinTenantPO.isLinkPusatBantuanAppear(), "Link pusat bantuan is not appeared");
    }

    @And("user verify only the header that is sticky or {string}")
    public void user_verify_only_the_header_that_is_sticky_or_(String attrib) {
        Assert.assertEquals(mamipoinTenantPO.getHeaderElementAttribute().substring(16,21), attrib, "the header is not sticky");
    }

    @And("user click on tab Syarat dan Ketentuan")
    public void user_click_on_tab_() throws InterruptedException {
        mamipoinTenantPO.clickOnSyaratDanKetentuanTab();
    }

    @Then("user verify {string} has {string} attribute")
    public void user_verify_input_has_input_attribute(String text, String attrib) throws InterruptedException {
        Assert.assertEquals(mamipoinTenantPO.getSyaratDanKetentuanTabText(), text, "Tab content is not equal to " + text);
        Assert.assertEquals(mamipoinTenantPO.getSyaratDanKetentuanAttribute().substring(18,24), attrib, "Syarat dan Ketentuan doesn't have " + attrib + " attribute");
    }

    @And("user verify expired point information on point information page {string}")
    public void user_verify_expired_point_information_on_point_information_page(String title) {
        Assert.assertEquals(mamipoinTenantPO.getNoPointAvailableTitleText(), title, "Title text is not equal to " + title);
    }

    @Then("display MamiPoin with text {string}")
    public void display_mami_poin_with_text(String textMamipoin) throws InterruptedException{
        Assert.assertEquals(mamipoinTenantPO.getTextNoHaveMamipoin(),textMamipoin,"Text is not equal to " +textMamipoin);
    }

    @Then("user see the points that have been used")
    public void user_see_the_points_that_have_been_used() throws InterruptedException{
        Assert.assertTrue(mamipoinTenantPO.isInformasiPoinUsedDisplayed());
    }

    @Then("user see {string} at section mamipoin")
    public void user_see_at_section_mamipoin(String doesntHaveMamipoin) throws InterruptedException {
        Assert.assertEquals(mamipoinTenantPO.getTextDoesntHaveMamipoinPayment(),doesntHaveMamipoin,"Text is not equal to "+doesntHaveMamipoin);

    }

    @Then("user not see mamipoin toggle")
    public void user_not_see_mamipoin_toggle() {
        Assert.assertFalse(mamipoinTenantPO.isMamipoinToggleAppear(), "Toggle poin is appear");
    }

    @Then("user not see total potongan mamipoin")
    public void user_not_see_total_potongan_mamipoin() {
        Assert.assertFalse(mamipoinTenantPO.isDiscountMamipoinAppear(),"Discount mamipoin is appear");
    }

}