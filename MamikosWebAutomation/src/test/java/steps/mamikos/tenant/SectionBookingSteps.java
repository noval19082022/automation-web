package steps.mamikos.tenant;

import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.tenant.booking.SectionBookingPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class SectionBookingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SectionBookingPO sectionBookingPO = new SectionBookingPO(driver);
    private HeaderPO header = new HeaderPO(driver);

    //Test Data
    private String propertyFile1="src/test/resources/testdata/mamikos/booking.properties";
    private String remainingRoom = JavaHelpers.getPropertyValue(propertyFile1,"remainingRoom_" + Constants.ENV);
    private String kostPrice = JavaHelpers.getPropertyValue(propertyFile1,"kostPriceSectionBooking_" + Constants.ENV);
    private String pemilikKost = JavaHelpers.getPropertyValue(propertyFile1,"pemilikKost_" + Constants.ENV);

    @Then("kost detail screen displayed with Remaining room, Room price, Owner kost")
    public void kost_detail_screen_displayed_with_remaining_room_room_price_owner_kost() throws InterruptedException {
        Assert.assertEquals(sectionBookingPO.getDefaultPrice(), "Rp" + kostPrice, "Room Price doesn't match");
        Assert.assertEquals(sectionBookingPO.getOwnerKost(),  pemilikKost, "Pemilik Kost doesn't match");
        Assert.assertTrue(sectionBookingPO.isRentSubmitButtonAvailable(), "Kost is full ");
    }
}
