package steps.mamikos.tenant;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import pageobjects.mamikos.tenant.home.HomePO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import pageobjects.mamikos.tenant.search.SearchPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.io.IOException;
import java.util.List;

public class SearchSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HomePO home = new HomePO(driver);
    private SearchPO search = new SearchPO(driver);
    private SearchListingPO searchList = new SearchListingPO(driver);
    private KosDetailPO kosDetail = new KosDetailPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    private String chatPropertyFile1="src/test/resources/testdata/mamikos/chat.properties";
    private String chatKostName1 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName1_" + Constants.ENV);
    private String chatKostName2 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName2_" + Constants.ENV);
    private String chatKostName3 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName3_" + Constants.ENV);
    private String chatKostName4 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName4_" + Constants.ENV);
    private String chatKostName5 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName5_" + Constants.ENV);
    private String chatKostName6 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName6_" + Constants.ENV);
    private String chatKostName7 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName7_" + Constants.ENV);
    private String chatKostName8 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName8_" + Constants.ENV);
    private String chatKostName9 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName9_" + Constants.ENV);
    private String chatKostName10 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName10_" + Constants.ENV);
    private String chatKostName11 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName11_" + Constants.ENV);
    private String chatKostName12 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName12_" + Constants.ENV);
    private String chatKostName13 = JavaHelpers.getPropertyValue(chatPropertyFile1,"chatKostName13_" + Constants.ENV);
    private String nonGPMars1 = JavaHelpers.getPropertyValue(chatPropertyFile1,"nonGPMars1_" + Constants.ENV);
    private String aptMars1 = JavaHelpers.getPropertyValue(chatPropertyFile1,"aptMars1_" + Constants.ENV);

    private String FTUEProperty="src/test/resources/testdata/mamikos/FTUEBookingBenefit.properties";
    private String FTUEKostName1 = JavaHelpers.getPropertyValue(FTUEProperty,"FTUEKostName1_" + Constants.ENV);
    private String FTUEKostName2 = JavaHelpers.getPropertyValue(FTUEProperty,"FTUEKostName2_" + Constants.ENV);
    private String FTUEKostName3 = JavaHelpers.getPropertyValue(FTUEProperty,"FTUEKostName3_" + Constants.ENV);
    private String FTUEKostName4 = JavaHelpers.getPropertyValue(FTUEProperty,"FTUEKostName4_" + Constants.ENV);

    @When("user clicks search bar")
    public void user_clicks_search_bar() throws InterruptedException, IOException {
        home.clickSearchBar();
    }

    @When("user choose {string} on dropdown list ads search")
    public void user_choose_on_dropdown_list_ads_search(String option) throws InterruptedException {
        home.selectOptionAdsSearch(option);
    }

    @And("user clicks Area")
    public void userClicksArea() throws InterruptedException {
        search.clickSearchBar();
    }

    @Then("After user click City name, city name will expand and Area name listed below it.")
    public void afterUserClickCityNameCityNameWillExpandAndAreaNameListedBelowIt(DataTable cityName) throws InterruptedException {
        List<List<String>> listCity = cityName.asLists(String.class);
        // Loop through the list of city
        for (int j=0; j<listCity.size(); j++) {
            // Tap on first list (table header)
            search.userTapCity(listCity.get(0).get(j));
            // Loop all city below header and check appearance in UI
            for (int i=j+1; i<listCity.size(); i++) {
                Assert.assertTrue(search.checkElementbyText(listCity.get(i).get(j)), "City not appear in dropdown.");
            }
        }
    }

    @Then("under popular search, there's this city :")
    public void underPopularSearchThereSThisCity(List<String> popularCity) {
        List<String> webPupularCity = search.listPopularCity();
        Assert.assertEquals(webPupularCity, popularCity, "Popular search city not match");
    }

    @And("user clicks button {string} below")
    public void userClicksCityUnderPopularSearch(String city) throws InterruptedException {
        search.clickPopularCity(city);
        searchList.clickFTUEKosListingPopUp();
    }

    @Then("listing that appear have location in {string}")
    public void listingThatAppearHaveLocationIn(String city) throws InterruptedException {
        List<String> addressList = search.listKostAddress();
        for (String a : addressList) {
            Assert.assertTrue(a.toLowerCase().contains(city.toLowerCase()), "Search result " + a + " not in correct location");
        }

    }

    @Then("listing that appear have location in {string} or {string}")
    public void listing_taht_appear_have_location_in(String city1, String city2) throws InterruptedException {
        List<String> addressList = search.listKostAddress();
        for (String a : addressList) {
            if (a.contains(city1.toLowerCase())){
                Assert.assertTrue(a.contains(city1.toLowerCase()), "Search result " + a + " not in correct location");
            } else {
                Assert.assertTrue(a.contains(city2.toLowerCase()), "Search result " + a + " not in correct location");
            }
        }
    }

    @And("user clicks Station & Stop")
    public void userClicksStationStop() throws InterruptedException {
        search.clickStationTab();
    }



    @Then("user verify popular station & stop")
    public void userVerifyPopularStationStop(List<String> stationName) {
        for (int i=0; i < stationName.size() ;i++){
            Assert.assertTrue(search.isPopularStationPresent(stationName.get(i)), "Station " + stationName + " is not present");
            Assert.assertEquals(search.getPopularStation(stationName.get(i)), stationName.get(i), "Station name not equal to " + stationName.get(i));
        }

    }

    @Then("user verify station lists by cities")
    public void userVerifyStationListsByCities(DataTable stationName) throws InterruptedException {
        List<List<String>> stationList = stationName.asLists(String.class);
        for (int i=0 ; i < stationList.size() ; i++){
            search.clickOnCities(stationList.get(0).get(i));
            for (int j = i + 1 ; j < stationList.size() ; j++){
                Assert.assertTrue(search.isEachStationFromCitiesPresent(stationList.get(j).get(i)), "Station name is not appear in dropdown");
                Assert.assertEquals(search.getEachStationFromCities(stationList.get(j).get(i)), stationList.get(j).get(i), "Station name is not equal to " +stationList.get(j).get(i));
            }
        }
    }

    @And("user clicks Campus")
    public void userClicksCampus() throws InterruptedException {
        search.clickOnCampusTab();
    }

    @Then("user verify popular campus")
    public void userVerifyPopularCampus(List<String> campusName) {
        for(int i = 0; i < campusName.size() ; i++){
            Assert.assertTrue(search.isPopularCampusPresent(campusName.get(i)), "Campus Name "+ campusName + " is not present");
            Assert.assertEquals(search.getPopularCampus(campusName.get(i)), campusName.get(i), "Campus name is not equal to " + campusName.get(i));
        }
    }

    @Then("user verify campus lists by cities")
    public void userVerifyCampusListsByCities(DataTable campus) throws InterruptedException {
        List<List<String>> campusList = campus.asLists(String.class);
        for(int i = 0; i < campusList.size() ; i++){
            search.clickOnCities(campusList.get(0).get(i));
            for (int j = i + 1; j < campusList.size() ; j++){
                Assert.assertTrue(search.isEachCampusFromCitiesPresent(campusList.get(j).get(i)), "Campus name is not appear in dropdown");
                Assert.assertEquals(search.getEachCampusFromCities(campusList.get(j).get(i)), campusList.get(j).get(i), "Campus name is not equal to " + campusList.get(j).get(i));
            }
        }
    }

    @And("user click one of the listing")
    public void user_click_one_of_the_listing() throws InterruptedException
    {
        searchList.clickFirstKost();
    }

    @And("user filter kos by Type {string}")
    public void userFilterKosByType(String gender) throws InterruptedException {
        searchList.clickFilterByGender(gender);
    }

    @And("user search for Kost with name {string} and selects matching result")
    public void userSearchForKostWithNameAndSelectsMatchingResult(String kosName) throws InterruptedException {
        if(kosName.equalsIgnoreCase("chatKostName1"))
        {
            kosName = chatKostName1;
        }
        else if(kosName.equalsIgnoreCase("chatKostName2"))
        {
            kosName = chatKostName2;
        }
        else if(kosName.equalsIgnoreCase("chatKostName3"))
        {
            kosName = chatKostName3;
        }
        else if(kosName.equalsIgnoreCase("chatKostName4"))
        {
            kosName = chatKostName4;
        }
        else if(kosName.equalsIgnoreCase("chatKostName5"))
        {
            kosName = chatKostName5;
        }
        else if(kosName.equalsIgnoreCase("chatKostName6"))
        {
            kosName = chatKostName6;
        }
        else if(kosName.equalsIgnoreCase("chatKostName7"))
        {
            kosName = chatKostName7;
        }
        else if(kosName.equalsIgnoreCase("chatKostName8"))
        {
            kosName = chatKostName8;
        }
        else if(kosName.equalsIgnoreCase("chatKostName9"))
        {
            kosName = chatKostName9;
        }
        else if(kosName.equalsIgnoreCase("chatKostName10"))
        {
            kosName = chatKostName10;
        }
        else if(kosName.equalsIgnoreCase("chatKostName11"))
        {
            kosName = chatKostName11;
        }
        else if(kosName.equalsIgnoreCase("chatKostName12"))
        {
            kosName = chatKostName12;
        }
        else if(kosName.equalsIgnoreCase("chatKostName13"))
        {
            kosName = chatKostName13;
        }
        else if(kosName.equalsIgnoreCase("nonGPMars1"))
        {
            kosName = nonGPMars1;
        }
        else if(kosName.equalsIgnoreCase("aptMars1"))
        {
            kosName = aptMars1;
        }
        search.enterTextToSearchAndSelectResult(kosName);
        kosDetail.clickClosePromoPopUp();
        kosDetail.dismissFTUEScreen();
    }

    @And("user clicks on Jogja city button on popular area section")
    public void user_clicks_on_jogja_city_button_on_popular_area_section() throws InterruptedException{
        search.clickOnKosJogjaButton();
        searchList.clickFTUEKosListingPopUp();
    }

    @When("user clicks on dropdown search another category")
    public void user_clicks_on_dropdown_search_another_category() throws InterruptedException{
        search.clickSearchAnotherCategoryDropdown();
    }
    @Then("user verify dropdown search another category")
    public void user_verify_dropdown_search_another_category(List<String> searchAnotherCategory) {
        List<String> dropdownAnotherCategory = search.searchAnotherCategory();
        Assert.assertEquals(dropdownAnotherCategory, searchAnotherCategory, "Search Another Category not match");
    }

    @Then("user verify filter another category")
    public void user_verify_filter_another_category(List<String> filterCategory) throws InterruptedException {
        Assert.assertTrue(search.filterIsPresent(), "Filter  not present");
    }

    @Then("user see look around list kos jogja")
    public void user_see_look_around_list_kos_jogja() throws InterruptedException {
        Assert.assertTrue(search.listKostJoga(), "list kost jogja");
    }


    @When("user clicks on dropdown kost type")
    public void user_clicks_on_dropdown_kost_type() throws InterruptedException{
        search.clickKostTypeDropdown();
    }

    @When("user clicks on filter kost type")
    public void user_clicks_on_filter_kost_type() throws InterruptedException {
        search.clickOnKosTypeFilter();
    }

    @When("user clicks on time period filter")
    public void user_clicks_on_time_period_filter() throws InterruptedException {
        search.clickOnTimePeriodFilter();
    }

    @Then("user verify dropdown kost type")
    public void user_verify_dropdown_kost_type(List<String> kostType) {
        List<String> dropdownKostType = search.dropdownKostType();
        Assert.assertEquals(dropdownKostType, kostType, "Kost Type not match");
    }

    @Then("user verify kost type display option")
    public void user_verify_kost_type_display_option(List<String> kostType) throws InterruptedException {
        for (String s : kostType) {
            Assert.assertTrue(search.kostTypeFilterIsPresent(s), "Filter type not present");
        }
    }

    @Then("user verify filter by time period display option")
    public void user_verify_filter_by_time_period_display_option(List<String> timePeriod) throws InterruptedException {
        for (String s : timePeriod) {
            Assert.assertTrue(search.kostTimePeriodFilterIsPresent(s), "Filter type not present");
        }
    }

    @And("user search for keyword {string}")
    public void user_search_for_keyword(String city) throws InterruptedException {
        search.enterTextToSearchAndSelectResultCity(city);
        searchList.clickFTUEKosListingPopUp();
    }

    @When("user clicks on dropdown time period")
    public void user_clicks_on_dropdown_time_period() throws InterruptedException{
        search.clickOnTimePeriodDropdown();
    }

    @Then("user verify dropdown time period")
    public void user_verify_dropdown_time_period(List<String> timePeriod) {
        List<String> dropdownTimePeriod = search.dropdownTimePeriod();
        Assert.assertEquals(dropdownTimePeriod, timePeriod, "Time Period not match") ;
    }

    @When("user clicks on dropdown sorting")
    public void user_clicks_on_dropdown_sorting() throws InterruptedException{
        search.clickSortingDropdown();
    }

    @Then("user verify dropdown sorting")
    public void user_verify_dropdown_sorting(List<String> sorting){
        List<String> sortingDropdown = search.sortingDropdown();
        Assert.assertEquals(sortingDropdown, sorting, "Sorting not match") ;
    }

    @And("user clicks city {string}")
    public void user_clicks_city(String city) throws InterruptedException {
        search.clickCity(city);
    }

    @And("user clicks {string} below the city")
    public void user_clicks_below_the_city(String area) throws InterruptedException {
        search.clickAreaBelow(area);
        searchList.clickFTUEKosListingPopUp();
    }

    @Then("should display the result list of keyword {string}")
    public void should_display_the_result_list_of_keyword(String keyword) {
        List<String> resultList = search.listResultKeyword();
        for (String a : resultList) {
            Assert.assertTrue(a.contains(keyword), "Search result " + a + " not in correct location");
        }
    }

    @Then("should display the result exception {string}")
    public void should_display_the_result_exception(String info) {
        Assert.assertEquals(search.getExceptionText(), info, "The exception text is not equals!");
    }

    @And("user search for random keyword {string}")
    public void user_search_for_random_keyword(String word) throws InterruptedException {
        search.enterRandomChar(word);
    }

    @And("user clear the text on searchbar")
    public void user_clear_the_text_on_searchbar() throws InterruptedException {
        search.clickXIcon();
    }

    @Then("user see searchbar is empty")
    public void user_see_searchbar_is_empty() {
        search.isSearchbarEmpty();
    }

    @And("should display the result name list of keyword {string}")
    public void should_display_the_result_name_list_of_keyword(String keyword) {
        List<String> resultList = search.listResultNameKeyword();
        for (String p : resultList) {
            Assert.assertTrue(p.toLowerCase().contains(keyword), "Search result " + p + " not in correct location");
        }
    }

    @Then("user click the search result based on name")
    public void user_click_the_search_result_based_on_name() throws InterruptedException {
        search.clickTheFirstResultBasedOnName();
        kosDetail.clickClosePromoPopUp();
        searchList.clickFTUEKosListingPopUp();
//        kosDetail.dismissFTUEScreen();
    }

    @And("should display the result area list of keyword {string}")
    public void should_display_the_result_area_list_of_keyword(String keyword) {
        List<String> resultList = search.listResultAreaKeyword();
        for (String a : resultList) {
            Assert.assertTrue(a.toLowerCase().contains(keyword.toLowerCase()), "Search result " + a + " not in correct location");
        }
    }

    @Then("user click the search result based on area")
    public void user_click_the_search_result_based_on_area() throws InterruptedException {
        search.clickTheFirstResultBasedOnArea();
        searchList.clickFTUEKosListingPopUp();
    }

    @When("user search for Kost with name {string} and selects matching result without dimiss FTUE")
    public void user_search_for_kost_with_name_and_selects_matching_result_without_dimiss_FTUE(String kosName) throws InterruptedException{
        if(kosName.equalsIgnoreCase("FTUEKostName1"))
        {
            kosName = FTUEKostName1;
        }
        else if(kosName.equalsIgnoreCase("FTUEKostName2"))
        {
            kosName = FTUEKostName2;
        }
        else if(kosName.equalsIgnoreCase("FTUEKostName3"))
        {
            kosName = FTUEKostName3;
        }
        else if(kosName.equalsIgnoreCase("FTUEKostName4"))
        {
            kosName = FTUEKostName4;
        }
        search.enterTextToSearchAndSelectResult(kosName);
        kosDetail.clickClosePromoPopUp();
    }

    @Then("user clicks the {string} button and the description will appears {string}")
    public void user_clicks_the_button_and_the_description_will_appears(String filter, String text) throws InterruptedException {
        if(filter.equalsIgnoreCase("Kos Andalan"))
        {
            searchList.clickFilterGPButton();
            String desc = searchList.getFilterGPText().replaceAll("\\s", "");
            String expected = text.toLowerCase().replaceAll("\\s", "");
            Assert.assertTrue(desc.contains(expected), "Description Gold Plus text is wrong");
        }
        else if(filter.equalsIgnoreCase("Kos Pilihan"))
        {
            searchList.clickFilterElite();
            String desc = searchList.getFilterEliteText().replaceAll("\\s", "");
            String expected = text.toLowerCase().replaceAll("\\s", "");
            Assert.assertTrue(desc.contains(expected), "Description Kos Pilihan text is wrong");
        }
        else if(filter.equalsIgnoreCase("Promo Ngebut"))
        {
            searchList.clickFilterFSButton();
            String desc = searchList.getFilterPromoNgebutText().replaceAll("\\s", "");
            String expected = text.toLowerCase().replaceAll("\\s", "");
            Assert.assertTrue(desc.contains(expected), "Description Promo Ngebut text is wrong");
        }
    }

    @Then("listing that appear have no {string} property")
    public void listing_that_appear_have_no_property(String apt) throws InterruptedException {
        List<String> addressList = search.listKostAddress();
        for (String a : addressList) {
            Assert.assertFalse(a.toLowerCase().contains(apt.toLowerCase()), "Search result " + a + " not in correct location");
        }
    }

    @Then("user sees the facilities on kos card are {string} or {string} or {string}")
    public void user_sees_the_facilities_on_kos_card_are_or_or(String facility1, String facility2, String facility3) {
        List<String> addressList = search.listKostFacilities();
        for (String a : addressList) {
            if (a.contains(facility1)){
                Assert.assertTrue(a.contains(facility1), "Search result " + a + " not in correct facility");
            } else if (a.contains(facility2)){
                Assert.assertTrue(a.contains(facility2), "Search result " + a + " not in correct facility");
            } else if(a.contains(facility3)) {
                Assert.assertTrue(a.contains(facility3), "Search result " + a + " not in correct facility");
            }
        }
    }

    @And("user see listing kos recommendation arround kos with detail {string}")
    public void userSeeListingKosRecommendationArroundKosWithDetail(String text){
        Assert.assertTrue(searchList.getRecommendationKosList().contains(text), "Recomendation Title in list is not equals!");
    }

    @Then("user redirected to search page")
    public void user_redirected_to_search_page() {
        String titlePage = driver.getTitle().toLowerCase();
        Assert.assertTrue(titlePage.contains("cari kost"));
    }

    @And("user click the search result based on area {string}")
    public void user_click_the_search_result_based_on_area(String area) throws InterruptedException {
        search.clickAreaFromkeywordBelow(area);
        searchList.clickFTUEKosListingPopUp();
    }

    @And("user click the search suggestion based on subarea {string}")
    public void user_click_the_search_suggestion_based_on_subarea(String subarea) throws InterruptedException {
        search.clickSubAreaBasedOnKeyword(subarea);
        searchList.clickFTUEKosListingPopUp();
    }
}
