package steps.mamikos.tenant.booking;

import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.tenant.booking.SelfDataBookingPO;
import utilities.ThreadManager;

public class SelfDataSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SelfDataBookingPO selfData = new SelfDataBookingPO(driver);

    @Then("tenant can sees self data page")
    public void tenant_can_sees_self_data_page() {
        Assert.assertTrue(selfData.isSelfDataIsActivePresent(), "Tenant is not in self data page section");
    }

    @Then("tenant can sees gender selection")
    public void tenant_can_sees_gender_selection() {
        Assert.assertTrue(selfData.isGenderRadioButtonPresent(), "Gender selected radio button is not present");
    }

    @Then("tenant click on {string} radio button")
    public void tenant_click_on_x_gender_button(String gender) throws InterruptedException {
        String radio = "";

        if(gender.equalsIgnoreCase("male")) {
            radio = "Laki-laki";
        }
        else if(gender.equalsIgnoreCase("female")) {
            radio = "Perempuan";
        }
        selfData.clickOnRadioButton(radio);
    }

    @Then("tenant can sees {string} radio button is selected")
    public void tenant_can_sees_gender_radio_button_is_selected(String gender) {
        String  genderText = "";
        if(gender.equalsIgnoreCase("male")) {
            genderText = "Laki-laki";
        }
        else if(gender.equalsIgnoreCase("female")) {
            genderText = "Perempuan";
        }
        Assert.assertEquals(selfData.getSelectedRadioButtonText(), genderText);
    }
}
