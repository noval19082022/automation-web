package steps.mamikos.tenant.profile;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.tenant.profile.KosSayaPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import static org.testng.Assert.assertEquals;

public class KosSayaSteps {
    WebDriver driver = ThreadManager.getDriver();
    KosSayaPO kosSaya = new KosSayaPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private JavaHelpers java = new JavaHelpers();

    //Test data OB for contract section at kos saya
    private String propertyFile1="src/test/resources/testdata/mamikos/OB.properties";
    private String propertyFile2="src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
    private String totalBiayaSewa = JavaHelpers.getPropertyValue(propertyFile1, "totalBiayaSewa_" + Constants.ENV);
    private String hargaSewa = JavaHelpers.getPropertyValue(propertyFile1, "hargaSewa_" + Constants.ENV);
    private String listrikName = JavaHelpers.getPropertyValue(propertyFile1, "listrikName_" + Constants.ENV);
    private String listrikPrice = JavaHelpers.getPropertyValue(propertyFile1, "listrik_" + Constants.ENV);
    private String sampahName = JavaHelpers.getPropertyValue(propertyFile1, "sampahName_" + Constants.ENV);
    private String sampahPrice = JavaHelpers.getPropertyValue(propertyFile1, "sampah_" + Constants.ENV);
    private String totalBiayaSewaFullPay = JavaHelpers.getPropertyValue(propertyFile2, "totalBiayaSewaFullPay_" + Constants.ENV);
    private String hargaSewaFullPay = JavaHelpers.getPropertyValue(propertyFile2, "hargaSewaFullPay_" + Constants.ENV);
    private String biayaLain1FullPay = JavaHelpers.getPropertyValue(propertyFile2, "biayaLain1FullPay_" + Constants.ENV);
    private String biayaLainPrice1FullPay = JavaHelpers.getPropertyValue(propertyFile2, "biayaLain1PriceFullPay_" + Constants.ENV);
    private String totalBiayaSewaDpst = JavaHelpers.getPropertyValue(propertyFile2, "totalBiayaSewaDpst_" + Constants.ENV);
    private String hargaSewaDpst = JavaHelpers.getPropertyValue(propertyFile2, "hargaSewaDpst_" + Constants.ENV);
    private String biayaLain1Dpst = JavaHelpers.getPropertyValue(propertyFile2, "biayaLain1Dpst_" + Constants.ENV);
    private String biayaLainPrice1Dpst = JavaHelpers.getPropertyValue(propertyFile2, "biayaLain1PriceDpst_" + Constants.ENV);

    //Test data OB for verification phone number at kos saya
    private String tenantPhoneNumAtOwner = JavaHelpers.getPropertyValue(propertyFile1, "tenantPhoneNumAtOwner_" + Constants.ENV);
    private String tenantPhoneNumAtKosSaya = JavaHelpers.getPropertyValue(propertyFile1, "tenantPhoneNumAtKosSaya_" + Constants.ENV);

    @When("tenant clicks on kost saya")
    public void tenant_clicks_on_kost_saya() throws InterruptedException {
        kosSaya.clickOnKosSayaButton();
    }

    @When("tenant clicks on tagihan")
    public void tenant_clicks_on_tagihan() throws InterruptedException {
        kosSaya.clickOnBillsTab();
    }

    @When("tenant clicks on Bayar button")
    public void tenant_clicks_on_Bayar_button() throws InterruptedException {
        kosSaya.clickOnPayButtonFirstBills();
    }

    @And("user click Mau Coba Dong section at homepage")
    public void user_click_mau_coba_dong_section_at_homepage() throws InterruptedException {
        kosSaya.clickMauCobaDong();
    }

    @Then("user redirect to kos saya page")
    public void user_redirect_to_kos_saya_page(){
        Assert.assertEquals(kosSaya.getTitleKosSaya(), "Kos Saya", "you are not in kos saya page");
    }

    @And("user navigates to Profil page")
    public void user_navigates_to_profil_page() throws InterruptedException {
        kosSaya.clickProfil();
    }

    @Then("user see title kos saya page")
    public void user_click_see_title_kos_saya_page(){
        Assert.assertEquals(kosSaya.getTitleKosSaya(), "Kos Saya", "you are not in kos saya page");
    }

    @And("user see welcome section at kos saya")
    public void user_see_welcome_section_at_kos_saya(){
        Assert.assertEquals(kosSaya.getWelcomeTitle(), kosSaya.getWelcomeTitle());
    }

    @When("user click Lihat informasi kos")
    public void user_click_lihat_informasi_kos() throws InterruptedException {
        kosSaya.clickLihatInformasiKos();
    }

    @Then("user will redirect to informasi kos page")
    public void user_will_redirect_to_informasi_kos_page() throws InterruptedException {
        Assert.assertEquals(kosSaya.getInformasiKosTitle(), "Informasi Kos", "you are not in informasi kos page");
        kosSaya.clickBackInformasiKos();
    }

    @When("user see review kos section")
    public void user_see_review_kos_section(){
        Assert.assertEquals(kosSaya.getReviewCard(), kosSaya.getReviewCard(), "review card is fullfill");
    }

    @And("user see Aktivitas di Kos Saya")
    public void user_see_aktivitas_di_kos_saya(){
        Assert.assertEquals(kosSaya.getAktivitasKosSaya(), "Aktivitas di Kos Saya", "the title is not match");
    }

    @Then("user see Tagihan kos, Kontrak, Chat pemilik, Bantuan, Forum")
    public void user_see_tagihan_kos_kontrak_chat_pemilik_bantuan_forum(){
        Assert.assertEquals(kosSaya.getTagihanKos(), "Tagihan kos", "the title is not match");
        Assert.assertEquals(kosSaya.getKontrak(), "Kontrak", "the title is not match");
        Assert.assertEquals(kosSaya.getChatPemilik(), "Chat pemilik", "the title is not match");
        Assert.assertEquals(kosSaya.getBantuan(), "Bantuan", "the title is not match");
        Assert.assertEquals(kosSaya.getForum(), "Forum", "the title is not match");
    }

    @Then("user can see empty state kost saya")
    public void user_can_see_empty_state_kost_saya(){
        Assert.assertEquals(kosSaya.getEmptyState(),"Kamu belum menyewa kos", "the title is not match");
    }


    @Then("user can see shortcut homepage with {string}")
    public void user_can_see_shortcut_homepage_with_x(String content) throws InterruptedException{
        if(content.equalsIgnoreCase("Pengajuan sewa lagi dicek pemilik")) {
            selenium.hardWait(3);
            int i;
            for (i=0; i<=10; i++){
                selenium.hardWait(2);
                selenium.pageScrollUsingCoordinate(0, 500);
                selenium.hardWait(2);
                if (!kosSaya.isWaitingConfirmationHomepagePresent()){
                    selenium.refreshPage();
                } else if (kosSaya.isWaitingConfirmationHomepagePresent()){
                    selenium.hardWait(4);
                    Assert.assertEquals(kosSaya.getKostSayaHomepageTitle(),"Pengajuan sewa lagi dicek pemilik", "the title is not match");
                    selenium.hardWait(2);
                    kosSaya.checkWaitingSection();
                    selenium.hardWait(2);
                    break;
                }
            }
        }
        else if(content.equalsIgnoreCase("Mau lanjut ajukan sewa di kos ini?")) {
            selenium.hardWait(3);
            int i;
            for (i=0; i<=10; i++){
                selenium.hardWait(2);
                selenium.pageScrollUsingCoordinate(0, 500);
                selenium.hardWait(2);
                if (!kosSaya.isDraftBookingHomepagePresent()){
                    selenium.refreshPage();
                } else if (kosSaya.isDraftBookingHomepagePresent()){
                    selenium.hardWait(3);
                    Assert.assertEquals(kosSaya.getDraftBookingTitle(),"Mau lanjut ajukan sewa di kos ini?", "the title is not match");
                    selenium.hardWait(2);
                    kosSaya.clickAjukanSewa();
                    selenium.hardWait(2);
                    break;
                }
            }
        }
        else if(content.equalsIgnoreCase("Yah, pengajuan sewamu ditolak")) {
            Assert.assertEquals(kosSaya.getDraftBookingTitle(),"Yah, pengajuan sewamu ditolak", "the title  is not match");
            Assert.assertEquals(kosSaya.getRejectReasonTitle(), "Alasan pengajuan sewa ditolak", "the reject reason tittle is not match");
            kosSaya.clickCariKosLainButton();
        }
        else if(content.equalsIgnoreCase("Hore! Pengajuan sewamu diterima")) {
            Assert.assertEquals(kosSaya.getDraftBookingTitle(),"Hore! Pengajuan sewamu diterima", "the title  is not match");
            kosSaya.clickBayarDisini();
        }

    }

    @And("user check promo ngebut label")
    public void user_check_promo_ngebut_label() throws InterruptedException {
        Assert.assertEquals(kosSaya.getFlashSaleLabel(),"Promo Ngebut", "flash sale label is not match");
    }

    @And("user click Kontrak section")
    public void user_click_kontrak_section() throws InterruptedException {
        kosSaya.clickKontrak();
    }

    @Then("user redirect to Kontrak page")
    public void user_redirect_to_kontrak_page(){
        kosSaya.getTitleKontrak();
    }

    @And("user check kost type, kost name, kost address, and room number")
    public void user_check_kost_type_kost_name_kost_address_and_room_number(){
        Assert.assertEquals(kosSaya.getKostType(), kosSaya.getKostType(), "kost type is not match");
        Assert.assertEquals(kosSaya.getKostName(), kosSaya.getKostName(), "kost name is not match");
        Assert.assertEquals(kosSaya.getKostAddress(), kosSaya.getKostAddress(), "kost address is not match");
        Assert.assertEquals(kosSaya.getRoomNumber(), kosSaya.getRoomNumber(), "room number is not match");
    }

    @And("user check total biaya, harga sewa, and biaya lain")
    public void user_check_total_biaya_harga_sewa_and_biaya_lain(){
        Assert.assertEquals(kosSaya.getTotalBiaya(), kosSaya.getTotalBiaya(), "the wording is not match");
        Assert.assertEquals(kosSaya.getTotalBiayaPrice(), "Rp" + totalBiayaSewa + " / Bulan", "the price is not match");
        Assert.assertEquals(kosSaya.getRincianBiaya(), kosSaya.getRincianBiaya(), "the wording is not match");
        Assert.assertEquals(kosSaya.getHargaSewa(), kosSaya.getHargaSewa(), "the wording is not match");
        Assert.assertEquals(kosSaya.getHargaSewaPrice(), hargaSewa, "the price is not match");
        assertEquals(kosSaya.getBiayaLainListrik(), listrikName, "the wording is not match");
        Assert.assertEquals(kosSaya.getListrikPrice(), listrikPrice, "the price is not match");
        Assert.assertEquals(kosSaya.getBiayaLainSampah(), sampahName, "the wording is not match");
        Assert.assertEquals(kosSaya.getSampahPrice(), sampahPrice, "the price is not match");
    }

    @And("user check tanggal penagihan")
    public void user_check_tanggal_penagihan(){
        Assert.assertEquals(kosSaya.getTglPenagihanTitle(), kosSaya.getTglPenagihanTitle(), "the wording of tanggal penagihan is not match");
        Assert.assertEquals(kosSaya.getTglPenagihanSubTitle(), kosSaya.getTglPenagihanSubTitle(), "the wording of tanggal penagihan is not match");
    }

    @And("user check mulai sewa, durasi sewa, and sewa berakhir")
    public void user_check_mulai_sewa_durasi_sewa_and_sewa_berakhir(){
        Assert.assertEquals(kosSaya.getMulaiSewa(), kosSaya.getMulaiSewa(), "the wording or the date is not match");
        Assert.assertEquals(kosSaya.getDurasiSewa(), kosSaya.getDurasiSewa(), "the wording or the date is not match");
        Assert.assertEquals(kosSaya.getSewaBerakhir(), kosSaya.getSewaBerakhir(), "the wording or the date is not match");
    }

    @And("user also check denda")
    public void user_also_check_denda(){
        Assert.assertEquals(kosSaya.getAturanDendaTitle(), "Aturan Denda", "the wording is not match");
        Assert.assertEquals(kosSaya.getAturanDendaPrice(), "Denda diberlakukan jika penyewa membayar tagihan Kos melewati 1 bulan dari tanggal tagihan, nominal denda adalah Rp50.000", "the wording is not match");
    }

    @And("user see Ajukan berhenti button and user click this button")
    public void user_see_ajukan_berhenti_button_and_user_click_this_button() throws InterruptedException {
        kosSaya.clickAjukanHentiBtn();
        Assert.assertEquals(kosSaya.getAjukanHentiPage(), kosSaya.getAjukanHentiPage(), "you are not in ajukan henti sewa page");
        kosSaya.clickCloseBtn();
    }
    @Then("user will redirect to lihat informasi kos page")
    public void user_will_redirect_to_lihat_informasi_kos_page() throws InterruptedException {
        Assert.assertEquals(kosSaya.getInformasiKosTitle(), "Informasi Kos", "you are not in informasi kos page");
    }

    @And("user will see kost type, kost name, and kost address")
    public void user_will_see_kost_type_kost_name_and_kost_address() throws InterruptedException{
        Assert.assertEquals(kosSaya.getKostTypeInfoKos(), kosSaya.getKostTypeInfoKos(), "The kos type is different");
        Assert.assertEquals(kosSaya.getKostNameInfoKos(), kosSaya.getKostNameInfoKos(), "The kos name is different");
        Assert.assertEquals(kosSaya.getKostAddressTitle(), kosSaya.getKostAddressTitle(), "The wording of kost address title is not match");
        Assert.assertEquals(kosSaya.getKostAddressInfoKos(), kosSaya.getKostAddressInfoKos(), "The kos address is different");
    }

    @And("user will see Peraturan umum kos")
    public void user_will_see_peraturan_umum_kos(){
        Assert.assertEquals(kosSaya.getPeraturanUmum(), kosSaya.getPeraturanUmum(), "The wording of kost address title is not match");
    }

    @When("user click Lihat semua fasilitas button")
    public void user_click_lihat_semua_fasilitas_button() throws InterruptedException {
        kosSaya.clickLihatSemuaFas();
    }

    @Then("user will see Fasilitas kamar")
    public void user_will_see_fasilitas_kamar(){
        Assert.assertEquals(kosSaya.getFasilitasKamar(), kosSaya.getFasilitasKamar(), "The wording is not match");
    }

    @And("user will see Fasilitas kamar mandi")
    public void user_will_see_fasilitas_kamar_mandi(){
        Assert.assertEquals(kosSaya.getFasilitasKmrMandi(), kosSaya.getFasilitasKmrMandi(), "The wording is not match");
    }

    @And("user will see Fasilitas umum")
    public void user_will_see_fasilitas_umum(){
        Assert.assertEquals(kosSaya.getFasilitasUmum(), kosSaya.getFasilitasUmum(), "The wording is not match");
    }

    @And("user will see parkir")
    public void user_will_see_parkir(){
        Assert.assertEquals(kosSaya.getFasilitasParkir(), kosSaya.getFasilitasParkir(), "The wording is not match");
    }

    @When("user can see Lihat pengajuan sewa lainnya text")
    public void user_can_see_lihat_pengajuan_sewa_text(){
        Assert.assertEquals(kosSaya.getSeeOtherBookingText(),"Lihat pengajuan sewa lainnya", "The wording is not match" );
    }

    @And("user click on Lihat pengajuan sewa lainnya button")
    public void user_click_on_lihat_pengajuan_sewa_lainnya_button() throws InterruptedException {
        kosSaya.clickLihatPengajuanSewaButton();
    }

    //-------------Tenant Input Unique Code----------------//
    @When("user click Masukkan kode dari pemilik button")
    public void user_click_masukkan_kode_dari_pemilik_button() throws InterruptedException {
        kosSaya.clickInputCode();
    }

    @And("user input valid unique code {string} and click Kirim Kode unik button")
    public void user_input_valid_unique_code_and_click_kirim_kode_unik_button(String uniqueCode) throws InterruptedException {
        Assert.assertEquals(kosSaya.getTitleInputUniqueCode(), kosSaya.getTitleInputUniqueCode(), "Title and subtitle are not match");
        Assert.assertEquals(kosSaya.getSubtitleInputUniqueCode(), kosSaya.getSubtitleInputUniqueCode(), "Title and subtitle are not match");
        kosSaya.fieldUniqueCode(uniqueCode);
    }

    @Then("user check verification tenant phone number at owner and tenant phone number at kos saya")
    public void user_check_verification_tenant_phone_number_at_owner_and_tenant_phone_number_at_kos_saya() throws InterruptedException {
        Assert.assertEquals(kosSaya.getTitleVerification(), kosSaya.getTitleVerification(), "Title is not match");
        Assert.assertEquals(kosSaya.getTenantPhoneNum1(), tenantPhoneNumAtOwner, "Tenant's phone number is not match");
        Assert.assertEquals(kosSaya.getTenantPhoneNum2(), tenantPhoneNumAtKosSaya + ".", "Tenant's phone number is not match");
        Assert.assertEquals(kosSaya.getAlertVerif(), kosSaya.getAlertVerif(), "Verification alert is not match");
        Assert.assertEquals(kosSaya.getOtpWording(), kosSaya.getOtpWording(), "OTP wording is not match");
    }

    @When("user click Kirim OTP button")
    public void user_click_kirim_otp_button() throws InterruptedException {
        kosSaya.clickOtpBtn();
    }

    @And("user click verification via SMS")
    public void user_click_verification_via_sms() throws InterruptedException {
        Assert.assertEquals(kosSaya.getTitleVerifMethod(), kosSaya.getTitleVerifMethod(), "Title is not match");
        Assert.assertEquals(kosSaya.getSubtitleVerifMethod(), kosSaya.getSubtitleVerifMethod(), "Subtitle is not match");
        Assert.assertEquals(kosSaya.getViaSms(), kosSaya.getViaSms(), "Wording is not match");
        kosSaya.clickViaSms();
    }

    @Then("user will see OTP fields")
    public void user_will_see_otp_fields(){
        Assert.assertEquals(kosSaya.getTitleOtpSms(), kosSaya.getTitleOtpSms(), "Title is not match");
    }

    @When("user click Back on OTP page")
    public void user_click_back_on_top_page() throws InterruptedException {
        kosSaya.clickBack();
        Assert.assertEquals(kosSaya.getTitlePopUpBtlVerif(), kosSaya.getTitlePopUpBtlVerif(), "Title is not match");
        kosSaya.clickYaBtl();
    }

    @And("user click All Back button until first page")
    public void user_click_all_back_button_until_first_page() throws InterruptedException{
        kosSaya.clickBackKosSaya();
    }

    @Then("user will see kos saya is still empty")
    public void user_will_see_kos_see_is_still_empty(){
        Assert.assertEquals(kosSaya.getEmptyState(), kosSaya.getEmptyState(), "Empty state is not match");
    }

    @And("user clicks on forum menu")
    public void user_clicks_on_forum_menu() throws InterruptedException{
        kosSaya.clickForumButton();
    }

    @Then("user will see pop up for upcoming forum feature")
    public void user_will_see_popup_for_upcoming_forum_feature() throws InterruptedException {
        Assert.assertEquals(kosSaya.getPopupForumTitle(), "Fitur ini sedang kami kembangkan");
        Assert.assertEquals(kosSaya.getInformationNotTickTitle(), "Saya tertarik. Ingatkan saya saat fitur sudah tersedia.");
    }

    @When("user clicks on Oke button")
    public void user_clicks_on_oke_button() throws InterruptedException {
        kosSaya.clickOKButtonOnForum();
    }

    @And("user tick on checkbox pop up upcoming")
    public void user_tick_on_checkbox_popup_upcoming() throws InterruptedException {
        kosSaya.clickCheckboxForum();
        kosSaya.clickOKButtonOnForum();
    }

    @Then("user will see information about upcoming forum feature")
    public void user_will_see_information_about_upcoming_forum_feature() throws InterruptedException {
        Assert.assertEquals(kosSaya.getInformationAfterTickCheckboxForum(), "Kami akan memberitahu Anda saat fitur ini sudah tersedia.");
    }

    @And("user click Check in at Kos saya page")
    public void user_click_check_in_at_kos_saya_page() throws InterruptedException {
        kosSaya.clickCheckinKosSaya();
    }

    //--------------------------------------check full payment-------------------------------------//
    @And("user check total biaya, harga sewa, and biaya lain full payment")
    public void user_check_total_biaya_harga_sewa_and_biaya_lain_full_payment(){
        Assert.assertEquals(kosSaya.getTotalBiaya(), kosSaya.getTotalBiaya(), "the wording is not match");
        Assert.assertEquals(kosSaya.getTotalBiayaPrice(), "Rp" + totalBiayaSewaFullPay + " / Bulan", "the price is not match");
        Assert.assertEquals(kosSaya.getRincianBiaya(), kosSaya.getRincianBiaya(), "the wording is not match");
        Assert.assertEquals(kosSaya.getHargaSewa(), kosSaya.getHargaSewa(), "the wording is not match");
        Assert.assertEquals(kosSaya.getHargaSewaPrice(), hargaSewaFullPay, "the price is not match");
        Assert.assertEquals(kosSaya.getBiayaLain1(), biayaLain1FullPay, "the wording is not match");
        Assert.assertEquals(kosSaya.getBiayaLainPrice1(), biayaLainPrice1FullPay, "the price is not match");
    }

    //--------------------------------------check DPST payment-------------------------------------//
    @And("user check total biaya, harga sewa, and biaya lain on kost dpst")
    public void user_check_total_biaya_harga_sewa_and_biaya_lain_on_kost_dpst(){
        Assert.assertEquals(kosSaya.getTotalBiaya(), kosSaya.getTotalBiaya(), "the wording is not match");
        Assert.assertEquals(kosSaya.getTotalBiayaPrice(), "Rp" + totalBiayaSewaDpst + " / Bulan", "the price is not match");
        Assert.assertEquals(kosSaya.getRincianBiaya(), kosSaya.getRincianBiaya(), "the wording is not match");
        Assert.assertEquals(kosSaya.getHargaSewa(), kosSaya.getHargaSewa(), "the wording is not match");
        Assert.assertEquals(kosSaya.getHargaSewaPrice(), hargaSewaDpst, "the price is not match");
        Assert.assertEquals(kosSaya.getBiayaLain1(), biayaLain1Dpst, "the wording is not match");
        Assert.assertEquals(kosSaya.getBiayaLainPrice1(), biayaLainPrice1Dpst, "the price is not match");
    }

    //---------------------Chat Pemilik-------------//
    @And("user clicks on Chat pemilik menu")
    public void user_clicks_on_chat_pemilik() throws InterruptedException{
        kosSaya.clickChatPemilikMenu();
    }

    @Then("user can see Chat list title")
    public void user_can_see_chat_list_title() throws InterruptedException{
        Assert.assertEquals(kosSaya.getChatListTitle(), "Chat");
    }

    @And("user clicks on Bantuan menu")
    public void user_clicks_on_bantuan_menu() throws InterruptedException{
        kosSaya.clickBantuanMenu();
    }

    @Then("user can see bantuan list with secation {string}, {string} and {string}")
    public void user_can_see_bantuan_list_with_x_y_z(String text1, String text2, String text3) throws InterruptedException{
        Assert.assertEquals(kosSaya.getCheckinSubtitle(), text1, "not appear Checkin subtitle");
        Assert.assertEquals(kosSaya.getPaidAndBillingSubtitle(), text2, "not appears Pembayaran and tagihan subtitle");
        Assert.assertEquals(kosSaya.getContractSubtitle(), text3, "not appears Kontrak sewa subtitle");
    }

    @And("user clicks on {string}")
    public void user_clicks_on_x(String text) throws InterruptedException {
        if(text.equalsIgnoreCase("Mengapa saya harus check-in?")) {
            kosSaya.clickOnQuestion1();
        }
        else if(text.equalsIgnoreCase("Bagaimana cara membatalkan check-in?")){
            kosSaya.clickOnQuestion2();
        }
        else if(text.equalsIgnoreCase("Apakah saya harus membayar uang sewa melalui Mamikos?")){
            kosSaya.clickOnQuestion3();
        }
        else if(text.equalsIgnoreCase("Bagaimana cara melihat tagihan/invoice sewa kos saya?")){
            kosSaya.clickOnQuestion4();
        }
        else if(text.equalsIgnoreCase("Bagaimana cara check-out di kos?")){
            kosSaya.clickOnQuestion5();
        }
        else if(text.equalsIgnoreCase("Kontrak saya akan segera berakhir, dan saya ingin lanjut ngekos. Apa yang harus saya lakukan?")){
            kosSaya.clickOnQuestion6();
        }
    }

    @Then("user can see {string} on mamihelp page")
    public void user_can_see_x_on_mamihelp_page(String title) throws InterruptedException {
        if(title.equalsIgnoreCase("Mengapa saya harus check-in?")){
            selenium.switchToWindow(1);
            selenium.hardWait(2);
            Assert.assertTrue(selenium.getURL().contains("mengapa-saya-harus-melakukan-check-in"));
            selenium.switchToWindow(1);
            selenium.hardWait(2);
        }
        else if(title.equalsIgnoreCase("Bagaimana cara membatalkan check-in?")){
            selenium.switchToWindow(2);
            selenium.hardWait(2);
            Assert.assertTrue(selenium.getURL().contains("bagaimana-cara-membatalkan-check-in"));
            selenium.switchToWindow(1);
            selenium.hardWait(2);
        }
        else if(title.equalsIgnoreCase("Apakah saya harus membayar uang sewa melalui Mamikos?")){
            selenium.switchToWindow(0);
            selenium.hardWait(2);
            Assert.assertTrue(selenium.getURL().contains("apakah-saya-harus-membayar-uang-sewa-melalui-mamikos"));
            selenium.switchToWindow(1);
            selenium.hardWait(2);
        }
        else if(title.equalsIgnoreCase("Bagaimana cara melihat tagihan/invoice sewa kos saya?")){
            selenium.switchToWindow(0);
            selenium.hardWait(2);
            Assert.assertTrue(selenium.getURL().contains("bagaimana-cara-melihat-tagihan-invoice-sewa-kos-saya"));
            selenium.switchToWindow(1);
            selenium.hardWait(2);
        }
        else if(title.equalsIgnoreCase("Bagaimana cara check-out di kos?")){
            selenium.switchToWindow(0);
            selenium.hardWait(2);
            Assert.assertTrue(selenium.getURL().contains("bagaimana-cara-check-out-di-kos"));
            selenium.switchToWindow(1);
            selenium.hardWait(2);
        }
        else if(title.equalsIgnoreCase("Kontrak saya akan segera berakhir, dan saya ingin lanjut ngekos. Apa yang harus saya lakukan?")){
            selenium.switchToWindow(0);
            selenium.hardWait(2);
            Assert.assertTrue(selenium.getURL().contains("kontrak-saya-akan-segera-berakhir-dan-saya-ingin-lanjut-ngekos-apa-yang-harus-saya-lakukan"));
            selenium.switchToWindow(1);
            selenium.hardWait(2);
        }
        else if(title.equalsIgnoreCase("pusat bantuan")){
            selenium.switchToWindow(0);
            selenium.hardWait(2);
            Assert.assertTrue(selenium.getURL().contains("penyewa"));
            selenium.hardWait(2);
        }
        else if(title.equalsIgnoreCase("bagaimana cara mengajukan refund")){
            selenium.switchToWindow(0);
            selenium.hardWait(2);
            Assert.assertTrue(selenium.getURL().contains("bagaimana-cara-mengajukan-refund-2"));
            selenium.hardWait(2);
        }
        else if(title.equalsIgnoreCase("syarat ketentuan tinggal di singgah sini")){
            selenium.switchToWindow(0);
            selenium.hardWait(2);
            Assert.assertTrue(selenium.getURL().contains("syarat-dan-ketentuan-tinggal-di-singgahsini-dan-apik"));
            selenium.hardWait(2);
        }
        else if(title.equalsIgnoreCase("refund policy")){
            selenium.switchToWindow(0);
            selenium.hardWait(1);
            Assert.assertTrue(selenium.getURL().contains("assets/_external-use/refund-policy/refund-policy-controlled-property.png"));
            selenium.hardWait(1);
        }
    }

    @And("user click on Contact Cs")
    public void user_click_on_contact_cs() throws InterruptedException {
        kosSaya.clickContactCsButton();
    }

    @When("tenant clicks on Bayar di sini")
    public void tenant_clicks_on_Bayar_di_sini() throws InterruptedException {
        kosSaya.clickOnButtonBayarDiSini();
    }

    @When("tenant clicks Bayar button on Kos Saya")
    public void tenant_clicks_bayar_button_on_kos_saya() throws InterruptedException {
        kosSaya.clickBayarButtonOnKosSaya();
    }

}
