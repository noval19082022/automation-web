package steps.mamikos.tenant.profile;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.tenant.profile.VerifikasiAkunPO;
import utilities.ThreadManager;

public class VerifikasiAkunSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private VerifikasiAkunPO verifikasiAkun = new VerifikasiAkunPO(driver);

    @And("user click verifikasi akun menu")
    public void user_click_verifikasi_akun_menu () throws InterruptedException {
        verifikasiAkun.clickVerifikasiAkunMenu();
    }

    @And("user click on verifikasi button")
    public void user_click_verifikasi_akun_button () throws InterruptedException {
        verifikasiAkun.clickVerifikasiButton();
    }

    @And("user change email with wrong format email {string}")
    public void user_change_email_with_wrong_format_email (String email) throws InterruptedException {
        verifikasiAkun.fillOutEmailField(email);
    }

    @Then("user get red alert error message {string}")
    public void user_get_red_alert_error_message(String message){
        Assert.assertEquals(verifikasiAkun.getErrorMessageEmail(), message, "Email error message is not equal to " + message);
    }

    @And("user change email with already registered email {string}")
    public void user_change_email_with_already_registered_email (String email) throws InterruptedException {
        verifikasiAkun.fillOutEmailField(email);
        verifikasiAkun.clickVerifikasiButton2();
    }

    @Then("user get pop up error message {string}")
    public void user_get_pop_up_error_message(String message) throws InterruptedException{
        Assert.assertEquals(verifikasiAkun.getErrorMessageEmailRegistered(), message, "Error message should be :" + message);
        verifikasiAkun.clickOKErrorMessageButton();
    }

    @And("user click on ubah phone number button")
    public void user_click_on_ubah_phone_number_button () throws InterruptedException {
        verifikasiAkun.clickUbahPhoneNumberButton();
    }

    @And("user empty phone number field")
    public void user_empty_phone_number_field () {
        verifikasiAkun.emptyPhoneNumberField();
    }

    @Then("user get red alert error message {string} on phonenumber section")
    public void user_get_red_alert_error_message_on_phonenumber_section (String message) throws InterruptedException{
        Assert.assertEquals(verifikasiAkun.getErrorMessagePhoneNumber(), message, "Error message should be :" + message);
    }

    @And("user change phone number with {string}")
    public void user_change_phone_number_with (String phoneNumber) throws InterruptedException {
        verifikasiAkun.fillOutPhoneNumberField(phoneNumber);
    }

    @And("user click on ubah button")
    public void user_click_on_ubah_button () throws InterruptedException {
        verifikasiAkun.clickUbahPhoneNumberButton2();
    }

    @Then("user verify OTP verification message was sent {string}")
    public void user_verify_OTP_verification_message_was_sent(String message){
        Assert.assertEquals(verifikasiAkun.getOTPVerificationMessage(), message, "OTP Verification message is not equal to " + message);
    }

    @And("user click on kartu identitas")
    public void user_click_on_kartu_identitas() throws InterruptedException {
        verifikasiAkun.clickOnUploadIDCard();
    }

    @When("user click continue on pop up")
    public void user_click_continue_on_pop_up() throws InterruptedException {
        verifikasiAkun.clickContinueOnPopUp();
    }

    @And("user click camera button to take a photo")
    public void user_click_camera_button_to_take_a_photo() throws InterruptedException {
        verifikasiAkun.clickOnCameraButton();
    }

    @And("user click save photo button")
    public void user_click_save_photo_button() throws InterruptedException {
        verifikasiAkun.clickOnSavePhotoButton();
    }

    @When("user click on uploaded photo")
    public void user_click_on_uploaded_photo() throws InterruptedException {
        verifikasiAkun.clickOnUploadedPhoto();
    }

    @Then("user verify the uploaded photo")
    public void user_verify_the_uploaded_photo() {
        Assert.assertTrue(verifikasiAkun.isPhotoBoxAppear(), "Photo box is not appeared");
    }

    @And("user click on resend OTP text")
    public void user_click_on_resend_OTP_text() throws InterruptedException {
        verifikasiAkun.clickOnSendBackOTPText();
    }

    @And("user click jenis identitas {string}")
    public void user_click_jenis_identitas(String type) throws InterruptedException {
        verifikasiAkun.clickOnIdentityType(type);
    }

    @And("user click popup {string} button")
    public void user_click_popup_ya_button(String Yes) throws InterruptedException {
        verifikasiAkun.clickOnYesButton(Yes);
    }

    @Then("user see popup confirmation closed")
    public void user_see_popup_confirmation_closed() {
        Assert.assertTrue(verifikasiAkun.popupConfirmationClosed(), "Verifikasi Identitas");
    }

    @Then("user verify OTP countdown should start on {string}")
    public void user_verify_OTP_countdown(String countdown) throws InterruptedException {
        Assert.assertTrue(verifikasiAkun.isCountdownCounterTextAppear(), "Countdown counter text is not appear");
        Assert.assertEquals(verifikasiAkun.getCountdownCounterText().substring(0,3), countdown, "Countdown counter not started on " + countdown);
    }

    @And("user clear email input field")
    public void user_clear_email_input_field() {
        verifikasiAkun.emptyEmailField();
    }
}
