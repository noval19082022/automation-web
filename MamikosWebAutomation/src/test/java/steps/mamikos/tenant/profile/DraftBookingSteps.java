package steps.mamikos.tenant.profile;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.tenant.profile.BookingHistoryPO;
import pageobjects.mamikos.tenant.profile.DraftBookingPO;
import utilities.ThreadManager;

public class DraftBookingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private DraftBookingPO draftBooking = new DraftBookingPO(driver);
    private BookingHistoryPO bookingHistory = new BookingHistoryPO(driver);

    @Then("tenant should reached draft booking page")
    public void tenant_should_reached_draft_boooking_page() throws InterruptedException {
        Assert.assertTrue(draftBooking.isInBookingSection(), "Not in Draft Booking section");
    }

    @Then("tenant can see {string} as kost name and kost location")
    public void tenant_can_see_text_as_kost_name_and_kost_location(String text) {
        Assert.assertEquals(draftBooking.getKostNameAndLocation(), text, "Text is not equal to " + draftBooking.getKostNameAndLocation());
    }

    @When("I/tenant/user click on booking sidebar button")
    public void xClickOnBookingSidebarButton() throws InterruptedException{
        bookingHistory.clickOnBookingSidebarButton();
    }
}
