package steps.mamikos.tenant.profile;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.tenant.profile.TenantProfilePO;
import pageobjects.mamikos.tenant.profile.EditProfilePO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EditProfileSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private TenantProfilePO tenantProfilePO = new TenantProfilePO(driver);
    private EditProfilePO editProfilePO = new EditProfilePO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @When("user click edit profile")
    public void user_click_edit_profil() throws InterruptedException {
        tenantProfilePO.clickEditProfilButton();
    }
    @When("user select university/office name {string}")
    public void user_select_university_office_name(String universityName) throws InterruptedException {
        editProfilePO.selectUniversityOrOfficeName(universityName);
    }

    @When("user input university/office name {string}")
    public void user_input_university_office_name(String universityName) throws InterruptedException {
        editProfilePO.inputUniversityOrOfficeName(universityName);
    }

    @Then("user verify pop up save profil {string} and {string}")
    public void user_verify_pop_up_save_profil_and(String titleMessage, String contentMessage) {
        Assert.assertTrue(editProfilePO.checkSuccessIconPopUp(), "Icon success is not present");
        Assert.assertEquals(editProfilePO.getTitleMessagePopUp(), titleMessage,"Title message is false");
        Assert.assertEquals(editProfilePO.getContentMessagePopUp(), contentMessage, "Content message is false");
    }

    @And("user click profile dropdown button")
    public void user_click_profile_dropdown_button() throws InterruptedException {
        editProfilePO.userClickProfileDropdownButton();
    }

    @And("user choose profession {string}")
    public void user_choose_profession_karyawan(String Profession) throws InterruptedException {
        editProfilePO.userChooseProfession(Profession);
    }

    @And("user click dropdown pilih universitas")
    public void user_click_dropdown_universitas() throws InterruptedException {
        editProfilePO.userClickDropdownUniversitas();
    }

    @And("user click dropdown pilih instansi")
    public void user_click_dropdown_instansi() throws InterruptedException {
        editProfilePO.userClickDropdownInstansi();
    }

    @And("user fills {string}")
    public void user_fills_characters(String lainnya) throws InterruptedException {
        editProfilePO.userFillsCharacters(lainnya);
    }

    @And("user click lainnya on dropdown")
    public void user_click_lainnya() throws InterruptedException {
        editProfilePO.userClickLainnya();
    }

    @And("user input phone number darurat less than {string} character")
    public void user_input_phone_number_less_than_8(String phone) throws InterruptedException {
        editProfilePO.userInputPhoneNumberLessThan8(phone);
    }

    @And("user input phone number darurat more than {string} character")
    public void user_input_phone_number_more_than_14(String phone) throws InterruptedException {
        editProfilePO.userInputPhoneNumberMoreThan14(phone);
    }

    @And("user click radio button laki-laki")
    public void user_click_radio_button_pria() throws InterruptedException {
        editProfilePO.userClickRadioButtonPria();
    }

    @And("user click simpan button")
    public void user_click_simpan_button() throws InterruptedException {
        editProfilePO.userClickSimpanButton();
    }

    @And("user click on last education tenant")
    public void user_click_last_education_tenant() throws InterruptedException {
        editProfilePO.userClickLastEducationTenant();
    }

    @And("user select S1")
    public void user_select_s1() throws InterruptedException {
        editProfilePO.userSelectS1();
    }

    @And("user click field full name")
    public void user_click_field_fullname() throws InterruptedException {
        editProfilePO.userClickFieldFullName();
    }

    @And("user delete previous full name")
    public void user_delete_previous_full_name() throws InterruptedException {
        editProfilePO.userDeletePreviousFullName();
    }

    @And("user fills fullname with number {string}")
    public void user_fills_full_name_with_number(String noval1908) throws InterruptedException {
        editProfilePO.userFullNameWithNumber(noval1908);
    }

    @And("user click kota asal dropdown")
    public void user_click_kota_asal_dropdown() throws InterruptedException {
        editProfilePO.userClickKotaAsalDropdown();
    }

    @And("user search city {string}")
    public void user_search_city(String Tangerang) {
        editProfilePO.userSearchCity(Tangerang);
    }

    @And("user select city kabupaten aceh barat")
    public void user_select_city() throws InterruptedException {
        editProfilePO.userSelectCity();
    }

    @And("user input profession {string}")
    public void user_input_profession(String Wiraswasta) throws InterruptedException {
        editProfilePO.userInputProfession(Wiraswasta);
    }

    @And("user no choose profession")
    public void user_no_choose_profession() throws InterruptedException {
        editProfilePO.userNoChooseProfession();
    }

    @And("user choose universitas indonesia")
    public void user_choose_universitas_indonesia() throws InterruptedException {
        editProfilePO.userChooseUniversitasIndonesia();
    }

    @Then("user see message error {string}")
    public void user_get_error_message(String message){
        Assert.assertEquals(editProfilePO.messageErrorFullName(), message, "Error message is not equal to " + message);
    }

    @Then("user see pop up message profil disimpan")
    public void popup_message_profil_disimpan () {
        Assert.assertEquals(editProfilePO.popupMessageProfileDisimpan(), "Profil Disimpan", "Profil Disimpan");
    }

    @And("user fill {string} on custom university input")
    public void user_fills_1_characters(String text) throws InterruptedException {
        editProfilePO.userFillsSingleCharacter(text);
    }

    @Then("Dropdown will displayed max 7 list office name")
    public void user_view_room_list () {
        Assert.assertTrue(editProfilePO.dropdownWillDisplayed(), "element is displayed");
    }

    @Then("user see validation message error less than 8")
    public void validation_message_less_than () {
        Assert.assertEquals(editProfilePO.validationMessageNoHpDaruratLesThan8(), "No. Handphone Darurat minimal mengandung 8 karakter.", "No. Handphone Darurat minimal mengandung 8 karakter.");
    }

    @Then("user see validation message error more than 14")
    public void validation_message_more_than () throws InterruptedException {
        Assert.assertEquals(editProfilePO.validationMessageNoHpDaruratMoreThan14(), "No. Handphone Darurat tidak boleh lebih dari 14 karakter.", "No. Handphone Darurat tidak boleh lebih dari 14 karakter.");
    }

    @Then("user see search results for the city Tangerang")
    public void user_see_result_city_tangerang () {
        Assert.assertEquals(editProfilePO.userSeeResultCityTangerang(), "Kota Tangerang", "Kota Tangerang");
    }

    @Then("user see button simpan edit profile disable")
    public void user_see_button_simpan_edit_profile_disable () {
        Assert.assertEquals(editProfilePO.userSeeButtonSimpanEditProfileDisable(), "Simpan", "Simpan");
    }

    @And("user fills {string} in search dropdown")
    public void user_fills_not_matched(String text) throws InterruptedException {
        editProfilePO.userFillsNotMatches(text);
    }

    @And("user fills {string} and search found")
    public void userFillsMatched(String text) throws InterruptedException {
        editProfilePO.userFillsMatched(text);
    }

    @Then("user verify message {string} in search dropdown")
    public void user_verify_message_in_search_dropdown(String message) throws InterruptedException {
        Assert.assertEquals(editProfilePO.getDropdownResultNotFoundText(message), message, "Dropdown message is not equal to " + message);
    }

    @Then("user verify dropdown results list contains {string}")
    public void user_verify_dropdown_results_list_contains(String text) {
       Assert.assertTrue(editProfilePO.isDropdownResultsListContains(text), "Dropdown results not containing " + text);
    }

    @Then("user verify button simpan is not {string}")
    public void user_verify_button_simpan_is_not(String attribute) {
        Assert.assertFalse(editProfilePO.isButtonSimpanDisabled(attribute), "Button simpan is " + attribute);
    }

    @And("user click on marital status dropdown")
    public void user_click_on_marital_status_dropdown() throws InterruptedException {
        editProfilePO.clickOnMaritalStatusDropdown();
    }

    @And("user click icon calendar")
    public void user_click_icon_calendar() throws InterruptedException {
        editProfilePO.clickIconCalendar();
    }

    @And("user choose date of birth")
    public void user_choose_date_of_birth() throws InterruptedException, ParseException {
        editProfilePO.chooseDateOfBirth();
    }

    @Then("user see martial status by default is belum kawin")
    public void user_see_default_status_belum_kawin () {
        Assert.assertEquals(editProfilePO.seeDefaultStatusBelumKawin(), "Belum Kawin", "Belum Kawin");
    }

    @And("user select {string}")
    public void userSelect(String marital) throws InterruptedException {
        editProfilePO.selectMaritalStatus(marital);
    }
    @And("user fill {string} on custom instansi input")
    public void user_fills_pekerjaan_1_characters(String text) throws InterruptedException {
        editProfilePO.fillPekerjaanSingleCharacter(text);
    }

    @And("user select instansi {string}")
    public void user_select_office(String instansi) throws InterruptedException {
        editProfilePO.selectOffice(instansi);
    }

    @And("user click on ok button on pop up message profil disimpan")
    public void user_click_on_ok_button_on_popup_message_profile_disimpan() throws InterruptedException {
        editProfilePO.clickOnOkButtonOnPopupMessage();
    }

    @Then("user can see information {string}")
    public void user_can_see_information_x(String text)throws InterruptedException {
        Assert.assertEquals(editProfilePO.getNoDataText(), text, "appears data");
    }

    @When("user can see error message {string} on jobs")
    public void user_can_see_error_message_x_on_jobs(String text) {
        Assert.assertEquals(editProfilePO.getErrroMessageOnJobs(), text, "not appears error message");
    }

}
