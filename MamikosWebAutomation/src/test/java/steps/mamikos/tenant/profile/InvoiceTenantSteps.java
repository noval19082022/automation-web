package steps.mamikos.tenant.profile;

import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.voucherku.TenantInvoicePO;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.ArrayList;
import java.util.List;

public class InvoiceTenantSteps {
    WebDriver driver = ThreadManager.getDriver();
    TenantInvoicePO tenant = new TenantInvoicePO(driver);
    SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @Then("tenant/owner can sees total cost is equal to basic amount + additional price + admin fee")
    public void tenant_can_sees_total_cost_is_equal_to_basic_amount_additional_price_admin_fee() {
        selenium.switchToWindow(2);
        int totalCost = JavaHelpers.extractNumber(tenant.getTotalCost());
        int adminCost = JavaHelpers.extractNumber(tenant.getAdminFeeCost());
        int rentCostPerPeriod = JavaHelpers.extractNumber(tenant.getPerPeriodCost());
        int additionalPriceCost = 50000;
        Assert.assertEquals(totalCost - additionalPriceCost, adminCost + rentCostPerPeriod);
    }

    @Then("^tenant can sees total cost is equal to basic amount, admin fee plus additional price below$")
    public void tenant_can_sees_total_cost_is_equal_to_basic_amount_plus(List<Integer> priceList) {
        ArrayList<String> tabs = new ArrayList<>(selenium.getWindowHandles());
        selenium.switchToWindow(tabs.size());

        int totalCost = JavaHelpers.extractNumber(tenant.getTotalCost());
        int adminCost = JavaHelpers.extractNumber(tenant.getAdminFeeCost());
        int rentCostPerPeriod = JavaHelpers.extractNumber(tenant.getPerPeriodCost());
        int additionalPriceCost = 0;
        for (int number : priceList) {
            additionalPriceCost += number;
        }
        Assert.assertEquals( adminCost + rentCostPerPeriod, totalCost - additionalPriceCost);
    }

    @Then("tenant can sees toast with cannot use voucher text with delete button")
    public void tenant_can_sees_toast_with_cannot_use_voucher_text_with_delete_button() {
        Assert.assertEquals(tenant.getActiveToastContentText(), "Kode voucher tidak bisa digunakan.\n" + "Silakan hapus voucher.");
        Assert.assertTrue(tenant.isToastHapusButtonVisible());
    }
}
