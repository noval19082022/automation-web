package steps.mamikos.tenant;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.tenant.UbahPasswordPO;
import pageobjects.mamikos.tenant.profile.TenantProfilePO;
import utilities.ThreadManager;

public class UbahPasswordSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private UbahPasswordPO ubahPassword = new UbahPasswordPO(driver);
    private TenantProfilePO tenantProfilePO = new TenantProfilePO(driver);

    @And("user clicks on pengaturan button")
    public void user_clicks_on_pengaturan_button() throws InterruptedException {
        ubahPassword.clickPengaturanButton();
    }

    @And("user clicks on ubah button")
    public void user_clicks_on_ubah_button() throws InterruptedException {
        ubahPassword.clickUbahButton();
    }

    @And("user fills password lama {string}")
    public void user_fills_on_password_lama(String password) throws InterruptedException {
        ubahPassword.userFillsPasswordLama(password);

    }
    @And("user fills password baru {string}")
    public void user_fills_password_baru(String newPassword) throws InterruptedException {
        ubahPassword.userFillsPasswordBaru(newPassword);
    }

    @And("user fills password confirm {string}")
    public void user_fills_password_confirm(String confirmPassword) throws InterruptedException {
        ubahPassword.userFillsPasswordConfirm(confirmPassword);
    }

    @And("user clicks on Simpan button ubah password")
    public void user_clicks_on_simpan_button() throws InterruptedException {
        ubahPassword.clickOnSimpanButton();
    }
    @Then("user see message success {string}")
    public void user_see_message_success(String message) throws InterruptedException {
        Assert.assertEquals(ubahPassword.messageSuccessUbahPassword(), message, "Error message is not equal to " + message);
    }
    @Then("user auto logout and redirect to homepage {string}")
    public void user_auto_logout(String message) throws InterruptedException {
        Assert.assertEquals(ubahPassword.autoLogout(), message, "Error message is not equal to " + message);
    }
    @Then("user see message error more than {string}")
    public void user_see_message_error_more_than(String message) throws InterruptedException {
        Assert.assertEquals(ubahPassword.messageErrorMoreThan(), message, "Error message is not equal to " + message);
        Assert.assertTrue(ubahPassword.isChangePasswordSubmitButtonDisabled(), "Change password submit button is not disabled");
    }
    @Then("user see message error special character {string}")
    public void user_see_message_error_special_character(String message) throws InterruptedException {
        Assert.assertEquals(ubahPassword.messageErrorSpecialCharacter(), message, "Error message is not equal to " + message);
    }

    @Then("user verify setting menu is hidden for social")
    public void user_verify_setting_menu_is_hidden() {
        Assert.assertFalse(tenantProfilePO.isSettingMenuAppear(), "Tenant setting menu is appeared");
    }
}


