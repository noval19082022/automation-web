package steps.mamikos.tenant;

import org.openqa.selenium.WebDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import pageobjects.mamikos.tenant.search.SearchListingPO;
import utilities.Constants;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;


import static org.testng.Assert.*;

public class KosDetailSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private KosDetailPO kosDetailPO = new KosDetailPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private SearchListingPO searchList = new SearchListingPO(driver);

    @Then("I should reached kos detail page")
    public void i_should_reached_kos_detail_page() throws InterruptedException {
        selenium.hardWait(2);
        kosDetailPO.dismissFTUEScreen();
        selenium.hardWait(5);
        assertTrue(kosDetailPO.isInKosDetail(), "You are not in kos detail page");
    }

    @When("I click on continue button until I understand button to dismiss FTUE pop up")
    public void i_click_on_continue_button_until_I_understand_button_to_dismiss_FTUE_pop_up() throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
    }

    @And("I click on favourite button")
    public void i_click_on_favorite_button() throws InterruptedException {
        kosDetailPO.clickOnFavouriteButton();
        if (kosDetailPO.unFavouriteButton()) {
            kosDetailPO.clickOnFavouriteButton();
        }
    }

    @And("I see grey favourite button")
    public void iSeeGreyButtonFavouriteButton() throws InterruptedException {
        assertTrue(kosDetailPO.isKosNotFavourite(), "Kos already in favourite stat");
    }

    @And("I click on favourite button with not login condition")
    public void iClickOnFavouriteButtonWithNotLoginCondition() throws InterruptedException {
        if (kosDetailPO.isKosNotFavourite()) {
            kosDetailPO.clickOnFavouriteButton();
        }
    }

    @And("I can see red favourite button")
    public void iCanSeeRedFavouriteButton() throws InterruptedException {
        assertTrue(kosDetailPO.isKosFavourite(), "Kos not in favourite stat");
    }

    @Then("Tenant can see love button change to unfavourite condition")
    public void tenant_can_see_love_button_should_go_back_to_unfavourite_condition() throws InterruptedException {
        if (kosDetailPO.isKosFavourite()) {
            kosDetailPO.clickOnFavouriteButton();
        }
    }

    @And("tenant can see {string}")
    public void tenantCanSeeKostTitle(String kostGender) {
        assertEquals(kosDetailPO.getKostTypeText(), kostGender, "Title not equals");
    }

    @Then("tenant can see confirmation popup different gender with information {string}")
    public void tenantCanSeeConfimationPopupDifferentGender(String message) throws InterruptedException {
        selenium.hardWait(1);
        assertEquals(kosDetailPO.getPopupDifferentGenderText(), message, "Popup not equals");
    }

    @And("tenant click on close icon")
    public void tenantClickOnCloseIcon() throws InterruptedException {
        kosDetailPO.clickOnCloseBtn();
    }

    @And("user scroll to view maps")
    public void user_scroll_to_view_maps() throws InterruptedException {
        kosDetailPO.scrollViewMaps();
    }

    @And("user click view maps")
    public void user_click_view_maps() throws InterruptedException {
        kosDetailPO.clickViewMaps();
    }

    @Then("verify popup login displayed")
    public void verify_popup_login_displayed() throws InterruptedException {
        assertTrue(kosDetailPO.loginPopupPresent(), "Nomor Handphone");
    }

    @Then("verify maps displayed")
    public void verify_maps_displayed() throws InterruptedException {
        assertTrue(kosDetailPO.pointerMapsPresent(), "Maps is not present");
    }

    @Then("tenant should see monthly rent duration")
    public void tenant_should_see_daily_rent_duration() throws InterruptedException {
        assertTrue(kosDetailPO.isDailyRentPricePresent(), "Monthly price is not present");
    }

    @When("user click ask address")
    public void user_click_ask_address() throws InterruptedException {
        kosDetailPO.clickAskAddress();
    }

    @Then("FTUE Booking Benefit appear with slides image")
    public void ftue_booking_benefit_appear_with_slides_image() throws InterruptedException {
        kosDetailPO.scrollToKostName();
        Assert.assertTrue(kosDetailPO.isFTUEImage1Present(), "Image 1 is not present!");
        kosDetailPO.clickContinueBtn();
        Assert.assertTrue(kosDetailPO.isFTUEImage2Present(), "Image 2 is not present!");
        kosDetailPO.clickContinueBtn();
        Assert.assertTrue(kosDetailPO.isFTUEImage3Present(), "Image 3 is not present!");
        kosDetailPO.clickContinueBtn();
        Assert.assertTrue(kosDetailPO.isFTUEImage4Present(), "Image 4 is not present!");
        kosDetailPO.clickContinueBtn();
        Assert.assertTrue(kosDetailPO.isFTUEImage5Present(), "Image 5 is not present!");
        kosDetailPO.clickContinueBtn();
    }

    @Then("FTUE Booking Benefit will not appear")
    public void ftue_booking_benefit_will_not_appear() throws InterruptedException {
        kosDetailPO.scrollToKostName();
        Assert.assertFalse(kosDetailPO.isFTUE_screenPresent(), "FTUE will should not present!");
    }

    @And("user click pelajari lebih lanjut button")
    public void user_click_pelajari_lebih_lanjut_button() throws InterruptedException {
        kosDetailPO.clickPelajariLebihLanjutButton();
    }

    @And("user click Saya Mengerti button FTUE")
    public void user_click_saya_mengerti_button_FTUE() throws InterruptedException {
        kosDetailPO.clickUnderstand();
    }

    @And("I click on share button")
    public void i_click_on_share_button() throws InterruptedException {
        kosDetailPO.clickShareButton();
    }

    @And("I click share on facebook")
    public void i_click_share_on_facebook() throws InterruptedException {
        kosDetailPO.clickShareFacebookButton();
    }

    @Then("I see the facebook page")
    public void i_see_the_facebook_page() throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isFacebookWindowPresent().contains("facebook"), "Share facebook didn't work!");
    }

    @And("user can see kos rule list on detail kos")
    public void user_can_see_kos_rule_list_on_detail_kos() throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isKosRulePresent(), "Kos rule is not present!");
    }

    @Then("I should reached map section and see button {string} text")
    public void i_should_reached_map_section_and_see_button_text(String textButton) throws InterruptedException {
        Assert.assertEquals(kosDetailPO.getMapButtonText(textButton), textButton, "Map button text not equals!");
    }

    @When("I click see map button")
    public void i_click_see_map_button() throws InterruptedException {
        kosDetailPO.clickOnSeeMapButton();
    }

    @And("user can see kos rule image on detail kos")
    public void user_can_see_kos_rule_image_on_detail_kos() {
        Assert.assertTrue(kosDetailPO.isKosRuleImagePresent(), "Kos rule image is not present!");
    }

    @And("user click kos rule image on detail kos")
    public void user_click_kos_rule_image_on_detail_kos() throws InterruptedException{
        kosDetailPO.clickOnKosRuleImage();
        Assert.assertTrue(kosDetailPO.isPopUpImgKosRulePresent(), "Kos rule pop up Image is not present!");
    }

    @And("user swipe the image of kos rule")
    public void user_swipe_the_image_of_kos_rule() throws InterruptedException {
        kosDetailPO.clickOnArrowNextButton();
        kosDetailPO.clickOnArrowPrevButton();
    }

    @Then("I should reached kos report section")
    public void i_should_reached_kos_report_section() {
        Assert.assertTrue(kosDetailPO.isKosReportPresent(), "Kos report is not present");
    }

    @Then("I should reached owner lower section")
    public void i_should_reached_owner_lower_section() {
        kosDetailPO.isOwnerSectionPresent();
    }

    @And("I click button report kos")
    public void i_click_button_report_kos() throws InterruptedException {
        kosDetailPO.clickOnKosReportButton();
    }

    @And("user enter text {string} in form kos report")
    public void user_enter_text_in_form_kos_report(String textReport) throws InterruptedException {
        kosDetailPO.clickOnCheckBox();
        kosDetailPO.insertReportText (textReport);
    }

    @And("click button send report")
    public void click_button_send_report() throws InterruptedException {
        kosDetailPO.clickOnSendReportButton();
        selenium.hardWait(1);
    }

    @Then("display pop up confirmation already have send report kos")
    public void display_pop_up_confirmation_already_have_send_report_kos() {
        Assert.assertTrue(kosDetailPO.isReportConfirmationPresent(), "Pop Up Confirmation send report is not present");
    }

    @Then("I can see info kos tambahan")
     public void i_can_see_info_kos_tambahan() {
        Assert.assertTrue(kosDetailPO.isInfoTambahanPresent(), "Info Tambahan kos is not present");
        Assert.assertTrue(kosDetailPO.isSelengkapnyaButtonPresent(), "Button selengkapnya is not present");
    }

    @And("I validate the elements of owner section")
    public void i_validate_the_elements_of_owner_section() {
        Assert.assertTrue(kosDetailPO.isOwnerNameDisplayed(), "Owner name not present!");
        Assert.assertTrue(kosDetailPO.isOwnerPictureDisplayed(), "Owner picture not present!");
        Assert.assertTrue(kosDetailPO.isOwnerStatusDisplayed(), "Owner status not present!");
        Assert.assertTrue(kosDetailPO.isNumberTransactionDisplayed(), "Number of transaction not present!");
        Assert.assertTrue(kosDetailPO.isBookingProcessedDisplayed(), "Booking processed not present!");
        Assert.assertTrue(kosDetailPO.isBookingChanceDisplayed(), "Booking chance not present!");
    }

    @When("I click on about statistics button")
    public void i_click_on_about_statistics_button() throws InterruptedException {
        kosDetailPO.clickStatisticsDetailButton();
    }

    @And("I see statistics details")
    public void i_see_statistics_details() {
        Assert.assertTrue(kosDetailPO.isStatisticsModalDisplayed(), "Statistics modal not present!");
    }

    @Then("I close statistics modal")
    public void i_close_statistics_modal() throws InterruptedException {
        kosDetailPO.closeStatisticsModal();
    }

    @And("user click button selengkapnya to expand the description")
    public void user_click_button_selengkapnya_to_expand_the_description() throws InterruptedException {
            kosDetailPO.clickOnInfoTambahanButton();
            selenium.hardWait(1);
    }

    @And("user see description wording info kos {string}")
    public void user_see_description_wording_info_kos(String text) {
        Assert.assertEquals(kosDetailPO.getInfoTambahanLabel(), text, "Sembunyikan Label is not equals");
    }

    @Then("I can see facility section on detail page")
    public void i_can_see_facility_section_on_detail_page() throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isFacilitiyTitleDisplayed(), "Facility title is not displayed!");
        Assert.assertTrue(kosDetailPO.isFacilitiyPriceInfoDisplayed(), "Facility price info is not displayed!");
        Assert.assertTrue(kosDetailPO.isFacilitiyRoomSizeDisplayed(), "Facility room size is not displayed!");
        Assert.assertTrue(kosDetailPO.isFacilitiyRoomDisplayed(), "Facility room is not displayed!");
        Assert.assertTrue(kosDetailPO.isFacilitiySeeAllButtonDisplayed(), "Facility see all button is not displayed!");
    }

    @When("i click see all facility")
    public void i_click_see_all_facility() throws InterruptedException {
        kosDetailPO.clickFacilitySeeAllButton();
    }

    @And("user click see all facility button")
    public void user_click_all_facility_button() throws InterruptedException {
        kosDetailPO.clickSeeAllFacility();
    }

    @Then("user see all facility share section")
    public void user_see_all_facility_share_section() throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isSharedFacilitiyTitleDisplayed(), "Facility shared title is not displayed!");
        Assert.assertTrue(kosDetailPO.isSharedFacilitiyDescDisplayed(), "Facility description title is not displayed!");
        Assert.assertTrue(kosDetailPO.isSharedFacilitiyPopUpDisplayed(), "Facility pop up is not displayed!");


    }
    @And("I can see Lihat semua foto")
    public void i_can_see_lihat_semua_foto() {
        Assert.assertTrue(kosDetailPO.isSeeAllPhotoButtonPresent(), "Button Lihat semua foto is not present");
    }

    @And("user click button lihat semua foto")
    public void user_click_button_lihat_semua_foto() throws InterruptedException{
        kosDetailPO.clickOnSeeAllButton();
    }

    @Then("display detail gallery")
    public void display_detail_gallery() {
        Assert.assertTrue(kosDetailPO.isCloseButtonPresent(), "Button close is not present");
        Assert.assertTrue(kosDetailPO.isBuildingPhotosPresent(), "Foto Bangunan is not present");
        Assert.assertTrue(kosDetailPO.isRoomPhotosPresent(), "Foto Kamar is not present");
        Assert.assertTrue(kosDetailPO.isBathroomPhotosPresent(), "Foto Kamar Mandi is not present");
        Assert.assertTrue(kosDetailPO.isOthersPhotosPresent(), "Foto Lainnya is not present");
    }

    @And("user click foto bangunan")
    public void user_click_foto_bangunan() throws InterruptedException {
        kosDetailPO.clickOnDetailPhotoButton();
    }

    @And("user swipe the image of detail gallery photo")
    public void user_swipe_the_image_of_detail_gallery_photo() {
        kosDetailPO.clickOnArrowPhotoGalleryNextButton();
    }

    @Then("I can see Kost Recommendation")
    public void i_can_see_kost_recommendation() {
        Assert.assertTrue(kosDetailPO.isLihatSemuaKosButtonPresent(), "Button Lihat semua is not present");
        Assert.assertTrue(kosDetailPO.isArrowRecommendationButtonPresent(), "arrow button is not present");
        Assert.assertTrue(kosDetailPO.isListPhotoRecommendationKosPresent(), "Foto kos recommendation is not present");
    }

    @And("user see description recomendation kos {string}")
    public void user_see_description_recomendation_kos(String text) {
        Assert.assertEquals(kosDetailPO.getRecommendationKosLabel(),text, "Recommendation kos label in detail is not equals!");
    }

    @And("user click button lihat semua kos recommendation")
    public void user_click_button_lihat_semua_kos_recommendation() throws InterruptedException {
        kosDetailPO.clickOnSeeAllRecommendation();
    }

    @And("I see set price {string}")
    public void i_see_set_price(String text) {
        Assert.assertEquals(kosDetailPO.getPriceType(), text, "Rp 9,80 juta - Rp 10,20 juta");
    }

    @And("user click on next button and display next recommendation kos")
    public void user_click_on_next_button_and_display_next_recommendation_kos() throws InterruptedException {
        kosDetailPO.clickOnArrowRecommendationNextButton();
        Assert.assertFalse(kosDetailPO.isFirstKostCardRecommendationPresent(), "First Kost Card still display");
    }

    @And("user clicks on previous button and display first page recomendation kos")
    public void user_clicks_on_previous_button_and_display_first_page_recomendation_kos() throws InterruptedException {
        kosDetailPO.clickOnArrowRecommendationPreviousButton();
        Assert.assertFalse(kosDetailPO.isNextRecommendationElementPresent(), "Next Kost Card still display");
    }

    @Then("user sees total price property")
    public void user_sees_total_price_property() {
        Assert.assertTrue(kosDetailPO.isTotalPricePresent(), "Total Price is not present!");

    }

    @When("user sees form booking date")
    public void user_sees_form_booking_date() throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isFormBookingDatePresent(), "Form booking date is not present!");
        kosDetailPO.clickOnBookingDate();
    }

    @Then("user validates description {string}")
    public void user_validates_description(String desc) throws InterruptedException{
//        kosDetailPO.clickOnBookingDate();
        Assert.assertEquals(kosDetailPO.getDescBookingDateText(desc).toLowerCase(), desc.toLowerCase(), "Description is not equal / present!");
    }

    @And("user sees date")
    public void user_sees_date() {
        Assert.assertTrue(kosDetailPO.isDateBookingPresent(), "Date Booking is not present!");
    }

    @And("user sees alert message {string}")
    public void user_sees_alert_message(String alert) throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isAlertBookingDateTextPresent(alert), "Alert is not equal / present!");
        kosDetailPO.clickOnBookingDate();
    }

    @When("user sees form booking duration")
    public void user_sees_form_booking_duration() {
        Assert.assertTrue(kosDetailPO.isFormBookingDurationPresent(), "Booking duration form is not present!");
    }

    @Then("user validates the title is suitable with duration that chosen")
    public void user_validates_the_title_is_suitable_with_duration_that_chosen() {
        String duration = kosDetailPO.getBookingDurationText();
        Assert.assertTrue(kosDetailPO.getTotalPriceDurationText().contains(duration), "Duration text is not equals!");
        Assert.assertTrue(kosDetailPO.isMainPricePresent(), "Main price is not displayed");
    }

    @And("user sees booking button")
    public void user_sees_booking_button() {
        Assert.assertTrue(kosDetailPO.isBookingButtonPresent(), "Booking button is not present!");
    }

    @Then("user sees breadcrumbs level is correct {string} > {string} > {string}")
    public void user_sees_breadcrumbs_level_is_correct(String home, String town, String property) {
        Assert.assertTrue(kosDetailPO.getHomeBreadcrumbsText().contains(home), "Home breadcrumb is not present!");
        Assert.assertTrue(kosDetailPO.getTownBreadcrumbsText().contains(town), "Town breadcrumb is not present!");
        Assert.assertEquals(kosDetailPO.getPropNameBreadcrumbsText(), kosDetailPO.getKostName(), "Property name breadcrumb is not present!");
    }

    @And("user click town level breadcrumb, it will navigates to landing search page")
    public void user_click_town_level_breadcrumb_it_will_navigates_to_landing_search_page() throws InterruptedException {
        String town = kosDetailPO.getTownBreadcrumbsText();
        kosDetailPO.clickTownBreadcrumbs();
        Assert.assertEquals(searchList.getTownTitleText(), town, "Landing page redirection is not equals!");
        selenium.back();
    }

    @And("user click home level breadcrumbs, it will navigates to Homepage")
    public void user_click_home_level_breadcrumbs_it_will_navigates_to_homepage() throws InterruptedException {
        kosDetailPO.clickHomeBreadcrumbs();
        Assert.assertTrue(selenium.getURL().contains(Constants.MAMIKOS_URL), "URL doesnt match!");
    }

    @Then("user validates {string} is appear in kos detail")
    public void user_validates_is_appear_in_kos_detail(String facilityShare) throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        Assert.assertTrue(kosDetailPO.isTopFacilitySharePresent(facilityShare), "Facility Share is not present on detail kost");
    }

    @And("I see filter mix gender")
    public void iSeeFilterMixGender() {
        Assert.assertTrue(kosDetailPO.isMixGenderDisplay(), "Mixed Gender is not display");
    }

    @Then("user can see owner badges on kos detail")
    public void user_can_see_owner_badges_on_kos_detail() throws InterruptedException{
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.scrollToKostName();
        Assert.assertTrue(kosDetailPO.ownerBadgesAsPresent(), "owner badges is not display");
    }

    @Then("user can see {string} on kos detail")
    public void user_can_see_x_on_kos_detail( String subTitle) throws InterruptedException {
        Assert.assertEquals(kosDetailPO.getOwnerBadesSubTitle(), subTitle,subTitle +"not present");
    }

    @Then("I can see elite badge kos")
    public void iCanSeeEliteBadgeKos() {
        Assert.assertTrue(kosDetailPO.isEliteBadgePresent(), "Elite Badge is not displayed");
    }

    @Then("user can see apik badge kos")
    public void user_can_see_apik_badge_kos() {
        Assert.assertTrue(kosDetailPO.isApikBadgePresent(),"Apik Badge is not displayed");
    }

    @Then("I should reached owner badges section")
    public void i_should_reached_owner_badges_section() throws InterruptedException {
        kosDetailPO.scrolltOWnerSection();
        Assert.assertTrue(kosDetailPO.ownerBadgesSectionAsPresent(),"owner section is not displayed");
    }

    @Then("I see Owner name, last online status,  Owner Profile picture, Number of success booking transaction")
    public void i_see_Owner_name_last_online_status_Owner_Profile_picture_Number_of_success_booking_transaction() throws InterruptedException {
        kosDetailPO.scrollToKostName();
        Assert.assertTrue(kosDetailPO.isOwnerNameDisplayed(), "Owner name is not displayed");
        Assert.assertTrue(kosDetailPO.isOwnerPictureDisplayed(), "Owner Picture is not displayed");
        Assert.assertTrue(kosDetailPO.isNumberTransactionDisplayed(),"Number of transaction is not displayed");

    }

    @And("I see owner statement")
    public void i_see_owner_statement() throws InterruptedException {
        kosDetailPO.scrollToKostName();
        Assert.assertTrue(kosDetailPO.isOwnerStatement(), "Owner Statement is not displayed");
    }

    @Then("user validates {string} is appear at kos detail rule")
    public void user_validates_is_appear_at_kos_detail_rule(String kosRule) throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        Assert.assertTrue(kosDetailPO.isTopKosRulePresent(kosRule), "Kos Rule is not present on detail kost");
    }

    @Then("user can see singgahsini badge kos")
    public void userCanSeeSinggahsiniBadgeKos() {
        Assert.assertTrue(kosDetailPO.isSinggahsiniBadgePresent(),"Singgahsini Badge is not displayed");
    }

    @And("user should reached benefit section")
    public void userShouldReachedBenefitSection() throws InterruptedException {
        kosDetailPO.scrollToBenefitSection();
    }

    @Then("user see benefit title, benefit description")
    public void userSeeBenefitTitleBenefitDescription() {
        Assert.assertTrue(kosDetailPO.isBenefitTitlePresent(), "Title Of Kos Benefit is not displayed");
        Assert.assertTrue(kosDetailPO.isBenefitDescPresent(), "Description of Kos Benefit is not displayed");
    }

    @Then("user should reach plm carousel section")
    public void user_should_reach_plm_carousel_section() throws InterruptedException {
        kosDetailPO.scrollToCarouselSection();
    }

    @Then("user click on button {string}")
    public void user_click_on_button(String string) throws InterruptedException {
        kosDetailPO.clickOnAjukanSewaCarousel();
        selenium.closeTabWindowBrowser();
        selenium.switchToLastWindow();
    }

    @Then("user see the appearence of the kos card carousel")
    public void userSeeTheAppearenceOfTheKosCardCarousel() {
        Assert.assertTrue(kosDetailPO.isCarouselPhotoDisplayed(), "Carousel Room Photo is not displayed");
        Assert.assertTrue(kosDetailPO.isCarouselRoomGenderDisplayed(), "Carousel Room Gender is not displayed");
        Assert.assertTrue(kosDetailPO.isCarouselRoomTypeDisplayed(), "Carousel Room Type is not displayed");
        Assert.assertTrue(kosDetailPO.isCarouselRoomFacilityDisplayed(), "Carousel Room Facilities is not displayed");
        Assert.assertTrue(kosDetailPO.isCarouselRoomPriceDisplayed(), "Carousel Room Price is not displayed");
        Assert.assertTrue(kosDetailPO.isCarouselSeeDetailButtonDisplayed(), "Carousel See Detail Button is not displayed");
        Assert.assertTrue(kosDetailPO.isCarouselBookingButtonDisplayed(), "Carousel Booking Button is not displayed");
    }

    @When("user click on button {string} after login")
    public void user_click_on_button_after_login(String string) throws InterruptedException {
        kosDetailPO.clickOnAjukanSewaCarousel();
        selenium.closeTabWindowBrowser();
    }


    @Then("user redirect to page {string}")
    public void user_redirect_to_page(String string) throws InterruptedException {
       Assert.assertTrue(kosDetailPO.isBookingRequestPageShow(), "Page Booking Request is not displayed");
    }

    @Then("user click on swipe bar")
    public void user_click_on_swipe_bar() throws InterruptedException {
        kosDetailPO.clickSwipePLM();
    }

    @Then("user click button {string}")
    public void user_click_button(String string) throws InterruptedException {
        kosDetailPO.clickOnSeeAnotherType();
    }

    @Then("user click button {string} at first list")
    public void user_click_button_at_first_list(String string) throws InterruptedException {
        kosDetailPO.clickOnFirstAJukanSewa();
        selenium.closeTabWindowBrowser();
    }

    @Then("user validates {string} is appear in kos detail at section facility room")
    public void user_validates_is_appear_in_kos_detail_at_section_facility_room(String facilityRoom) throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        Assert.assertTrue(kosDetailPO.isTopFacilityRooomPresent(facilityRoom), "Facility Room is not present on detail kost");
    }

    @Then("user validates {string} is appear in kos detail at section facility bathroom")
    public void user_validates_is_appear_in_kos_detail_at_section_facility_bathroom(String facilityBathRoom) throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        Assert.assertTrue(kosDetailPO.isTopFacilityBathRooomPresent(facilityBathRoom), "Facility Room is not present on detail kost");
    }

    @And("user click on button see detail on carousel")
    public void userClickOnButtonSeeDetailOnCarousel() throws InterruptedException {
        kosDetailPO.clickOnSeeDetailCarousel();
        selenium.closeTabWindowBrowser();
    }

    @And("user click on kos card on other type")
    public void userClickOnKosCardOnOtherType() throws InterruptedException {
        kosDetailPO.clickOnKosCardOtherType();
        selenium.closeTabWindowBrowser();
    }

    @Then("user see the appearence of the kos card on other type page")
    public void userSeeTheAppearenceOfTheKosCardOnOtherTypePage() {
        Assert.assertTrue(kosDetailPO.isOtherTypePhotoDisplayed(), "Carousel Other Type Room Photo is not displayed");
        Assert.assertTrue(kosDetailPO.isOtherTypeRoomGenderDisplayed(), "Carousel Other Type Room Gender is not displayed");
        Assert.assertTrue(kosDetailPO.isOtherTypeRoomTypeDisplayed(), "Carousel Other Type Room Type is not displayed");
        Assert.assertTrue(kosDetailPO.isOtherTypeRoomFacilityDisplayed(), "Carousel Other Type Room Facilities is not displayed");
        Assert.assertTrue(kosDetailPO.isOtherTypeRoomPriceDisplayed(), "Carousel Other Type Room Price is not displayed");
        Assert.assertTrue(kosDetailPO.isOtherTypeBookingButtonDisplayed(), "Carousel Other Type Booking Button is not displayed");
    }

    @Then("user can see button {string}")
    public void user_can_see_button(String string) {
        Assert.assertTrue(kosDetailPO.isKosRuleButtonShow(),"Kos Rule Button is not displayed");
    }

    @Then("user see POI at map section")
    public void user_see_POI_at_map_section() {
        Assert.assertTrue(kosDetailPO.isPOILandmarkShow(),"POI Landmark is not displayed");
    }

    @Then("user can see facility share section on detail page")
    public void user_can_see_facility_share_section_on_detail_page() {
        Assert.assertTrue(kosDetailPO.isFacShareShow(),"Facility Share is not displayed");
    }

    @Then("user click see all facility share")
    public void user_click_see_all_facility_share() throws InterruptedException {
        kosDetailPO.clickOnButtonFacShare();
    }

    @Then("user can see facility parking section on detail page")
    public void user_can_see_facility_parking_section_on_detail_page() {
        Assert.assertTrue(kosDetailPO.isFacParkingDisplayed(),"Facility Parking Section is not displayed");
        Assert.assertTrue(kosDetailPO.isFacParkingTitleDisplayed(),"Facility Parking Title Section is not displayed");
    }

    @Then("user see promo owner section")
    public void user_see_promo_owner_section() {
        Assert.assertTrue(kosDetailPO.isPromoOwnerSectionDisplayed(),"Promo Owner Section is not displayed");

    }

    @Then("user see pop up promo owner")
    public void user_see_pop_up_promo_owner() {
        Assert.assertTrue(kosDetailPO.isTitlePopUpPromoOwnerDisplayed(),"Pop Up Promo Owner Section is not displayed");

    }

    @Then("user click text {string}")
    public void user_click_text(String string) throws InterruptedException {
        kosDetailPO.clickOnTextPopUpPromoOwner();
    }

    @Then("user click button {string} at promo owner")
    public void user_click_button_at_promo_owner(String string) throws InterruptedException {
        kosDetailPO.clickOnButtonPromoOwner();
    }

    @Then("user see {string} pop up")
    public void user_see_pop_up(String string) throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isChatKostPopUpDisplayed(),"Pop Up Hubungi Kos Ini Section is not displayed");
    }

    @And("user can see facility room section on detail page")
    public void userCanSeeFacilityRoomSectionOnDetailPage() {
        Assert.assertTrue(kosDetailPO.isFacRoomShow(),"Room Facility section is not displayed");
    }

    @And("user click see all facility room")
    public void userClickSeeAllFacilityRoom() throws InterruptedException {
        kosDetailPO.clickOnSeeAllButtonFacRoom();
    }

    @And("user see all facility room section")
    public void userSeeAllFacilityRoomSection() {
        Assert.assertTrue(kosDetailPO.isRoomFacilitiyPopUpDisplayed(),"Room Facility pop up is not displayed");
        Assert.assertTrue(kosDetailPO.isRoomFacilitiyIconDisplayed(),"Room Facility icon is not displayed");
        Assert.assertTrue(kosDetailPO.isRoomFacilitiyNameDisplayed(),"Room Facility name is not displayed");
    }

    @Then("user can see facility bath section on detail page")
    public void userCanSeeFacilityBathSectionOnDetailPage() {
        Assert.assertTrue(kosDetailPO.isFacBathShow(),"Bathroom Facility section is not displayed");
        Assert.assertTrue(kosDetailPO.isBathFacilitiyIconDisplayed(),"Bathroom Facility icon is not displayed");
        Assert.assertTrue(kosDetailPO.isBathFacilitiyNameDisplayed(),"Bathroom Facility name is not displayed");
    }

    @Then("user can see facility notes on detail kos")
    public void userCanSeeFacilityNotesOnDetailKos() throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isFacilityNotesSectionDisplayed(),"Facility Notes section is not displayed");
        Assert.assertTrue(kosDetailPO.isFacilityNotesDescDisplayed(),"Facility Notes Description is not displayed");
    }

    @And("user can see more facility notes button")
    public void userCanSeeMoreFacilityNotesButton() throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isExpandFacNotesDisplayed(),"Facility Notes expand button is not displayed");
    }

    @And("user click on see more facility notes button")
    public void userClickOnSeeMoreFacilityNotesButton() throws InterruptedException {
        kosDetailPO.clickOnExpandFacNotes();
    }

    @And("user see the more facility notes button is not present")
    public void userSeeTheMoreFacilityNotesButtonIsNotPresent() throws InterruptedException {
        Assert.assertFalse(kosDetailPO.isExpandFacNotesDisplayed(),"Facility Notes expand button is displayed");
    }

    @Then("user can see owner story on detail kos")
    public void userCanSeeOwnerStoryOnDetailKos() throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isOwnerStorySectionDisplayed(),"Owner Story section is not displayed");
        Assert.assertTrue(kosDetailPO.isOwnerStoryDescDisplayed(),"Owner Story description is not displayed");
    }

    @And("user can see more owner story button")
    public void userCanSeeMoreOwnerStoryButton() throws InterruptedException {
        Assert.assertTrue(kosDetailPO.isExpandOwnerStoryDisplayed(),"Owner Story expand button is not displayed");
    }

    @And("user click on see more owner story button")
    public void userClickOnSeeMoreOwnerStoryButton() throws InterruptedException {
        kosDetailPO.clickOnExpandOwnerStory();
    }

    @And("user see the more owner story button is not present")
    public void userSeeTheMoreOwnerStoryButtonIsNotPresent() throws InterruptedException {
        Assert.assertFalse(kosDetailPO.isExpandOwnerStoryDisplayed(),"Owner Story expand button section is displayed");
    }

    @Then("user can see refund policy and click on Bagaimana keuntungan button")
    public void user_can_see_refund_policy_and_click_on_bagaimana_keuntungan_button() throws InterruptedException{
        kosDetailPO.dismissFTUEScreen();
        kosDetailPO.refundPolicyPresent();
        kosDetailPO.clickRefundLink();
    }

    @When("user can see refund information and click on kebijakan refund mamikos")
    public void user_can_see_refund_information_and_click_on_kebijakan_refund_mamikos() throws InterruptedException{
        kosDetailPO.getAndClickTnCRefund();
    }

    @And("user can see overview section on detail page")
    public void userCanSeeOverviewSectionOnDetailPage() throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        Assert.assertTrue(kosDetailPO.isPropertyGenderDisplayed(),"Property Gender Label is not displayed");
        Assert.assertTrue(kosDetailPO.isPropertyLocationDisplayed(),"Property Location Label is not displayed");
        Assert.assertTrue(kosDetailPO.isRoomAvailabilityDisplayed(),"Property Room AvailabilityLabel is not displayed");
    }

    @When("user can see refund policy with {string}")
    public void user_can_see_refund_policy_with_x(String text) throws InterruptedException {
        if(text.equalsIgnoreCase("Refund sebelum check-in")) {
            kosDetailPO.getSSRefundBeforeCheckin();
        }
        else if(text.equalsIgnoreCase("DP tidak dapat di-refund")) {
            kosDetailPO.getSSRefundDP();
        }
        else if(text.equalsIgnoreCase("Bayar langsung lunas dapat di-refund")) {
            kosDetailPO.getAndClickSSRefundPayDirectly();
        }
    }

    @And("user click on ketentuan waktu berikut")
    public void user_click_on_ketentuan_waktu_berikut() throws InterruptedException {
        kosDetailPO.clickOnTimeCondition();
    }

    @Then("user did not see last online owner on detail page")
    public void userDidNotSeeLastOnlineOwnerOnDetailPage() {
        Assert.assertNotEquals(kosDetailPO.isOwnerLastSeeDetailDisplayed(), "Owner Last Seen is displayed");
    }

    @Then("user see last seen owner on detail page")
    public void userSeeLastSeenOwnerOnDetailPage() throws InterruptedException {
        kosDetailPO.dismissFTUEScreen();
        Assert.assertTrue(kosDetailPO.isOwnerLastSeeDetailDisplayed(), "Owner Last Seen is displayed");
    }
}
