package steps.mamikos.payment;

import io.cucumber.java.bs.A;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.sl.In;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.mamikos.payment.BillingDetailsPO;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class BillingDetailsSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private BillingDetailsPO billing = new BillingDetailsPO(driver);
    private KosDetailPO kosDetailPO = new KosDetailPO(driver);
    private JavaHelpers java = new JavaHelpers();


    @When("user click confirmation button on billing details")
    public void user_click_confirmation_button_on_billing_details() throws InterruptedException {
 //       kosDetailPO.dismissFTUEScreen();
        billing.clickOnConfirmationButton();
//        billing.clickOnConfirmationTerminateContractButton();
    }

    @Then("system display warning message {string} on billing details")
    public void system_display_warning_message_on_billing_details(String message) throws InterruptedException {
        Assert.assertEquals(billing.getWarningMessage(), message, "Message not " + message);
    }

    @When("user click terminated contract")
    public void user_click_terminated_contract() throws InterruptedException {
//        kosDetailPO.dismissFTUEScreen();
        billing.clickOnTerminateContract();
    }

    @When("owner select reason terminate contract {string} and click terminate contract button")
    public void owner_select_reason_terminate_contract_and_click_terminate_contract_button(String terminatedReason) throws InterruptedException, ParseException {
        billing.selectTerminateReason(terminatedReason);
        if (terminatedReason.equals("Lainnya")){
            billing.inputReasonReason(terminatedReason);
        }
        billing.selectDate();
        billing.clickOnTerminateContractButton();
    }

    @Then("user will see disbursement alert and see Kapan uang masuk ke rekening saya? and clicks on disbursement link")
    public void user_will_see_disbursement_alert_and_see_kapan_uang_masuk_ke_rekening_saya_and_clicks_on_disbursement_link() throws InterruptedException{
        Assert.assertEquals(billing.getDisbursementAlert(), billing.getDisbursementAlert(), "The disbursement alert is not same");
        billing.clicksDisbursementLink();
    }

    @And("user redirect to disbursement link")
    public void user_redirect_disbursement_link(){
        Assert.assertEquals(billing.getDisbursementLinkPage(), billing.getDisbursementLinkPage(), "The disbursement link is not expected");
    }
}
