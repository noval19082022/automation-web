package steps.mamikos.payment;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikos.payment.AllInvoicePO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class AllInvoiceSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private AllInvoicePO invoice = new AllInvoicePO(driver);

    @And("admin open menu all invoice")
    public void admin_open_menu_all_invoice () throws InterruptedException {
        invoice.openAllInvoice();
    }

    @And("admin select invoice number")
    public void admin_select_invoice_number () throws  InterruptedException {
        invoice.selectInvoice ();
    }

    @And("admin input invoice number {string}")
    public void admin_input_invoice_number (String number) throws  InterruptedException {
        invoice.inputInvoice(number);
    }

    @And("admin click button cari invoice")
    public void admin_click_button_cari_invoice () throws  InterruptedException {
        invoice.clickCariInvoice();
    }

    @And("admin click button reset")
    public void admin_click_button_reset () throws InterruptedException {
        invoice.clickReset();
    }

    @Then("admin verify data transaction")
    public void admin_verify_data_transaction () throws  InterruptedException {
        invoice.verifyDataTransaction();
    }

    @Then("admin get blank screen")
    public void admin_get_blank_screen () throws  InterruptedException {
        invoice.getBlankScreen();
    }

    @And("admin select invoice core")
    public void admin_select_invoice_core () throws  InterruptedException {
        invoice.selectCode();
    }

    @And("admin click change status")
    public void admin_click_change_status () throws InterruptedException {
        invoice.clickChangeStatus ();
    }

    @And("admin change unpaid to paid")
    public void admin_change_unpaid_to_paid () throws InterruptedException {
        invoice.changeToPaid ();
    }

    @And("admin input date and time {string}")
    public void admin_input_date_and_time  (String date) throws  InterruptedException {
        invoice.inputDateAndTime(date);
    }

    @And("admin submit change")
    public void admin_submit_change () throws  InterruptedException {
        invoice.clickSubmitChange();
    }
    @Then("invoice will changes to {string}")
    public void invoice_will_changes_to (String status) throws  InterruptedException {
        invoice.showInvoiceAfterChange (status);
    }

    @And("admin change paid to unpaid")
    public void admin_change_paid_to_unpaid () throws  InterruptedException {
        invoice.changeToUnpaid ();
    }


    @And("admin click checkbox not in mamipay")
    public void admin_click_checkbox_not_in_mamipay () throws InterruptedException {
        invoice.checklistNotInMamipay () ;
    }

    @And("admin choose method {string}")
    public void admin_choose_method (String method) throws  InterruptedException {
        invoice.selectPayment(method);
    }

    @Then("admin will get data transatcion with method {string}")
    public void admin_will_get_data_transatcion_with_method (String resultMethod) throws InterruptedException {
        invoice.showResultData(resultMethod);
    }

    @And("admin choose date picker {string} and {string}")
    public void admin_choose_date_picker (String From, String To) throws InterruptedException {
        invoice.choosescheduleDate(From, To);
    }

    @Then("data transaction appeared")
    public void data_transaction_appeared () throws  InterruptedException {
        invoice.showDataBaseOnSchduleDate ();
    }

    @And("admin input amount from and to {string} and {string}")
    public void admin_input_amount_from_and_to (String nominalFrom, String nominalTo) throws InterruptedException {
        invoice.inputValueAmount(nominalFrom, nominalTo);

    }

    @Then("appeared data with amount {string}")
    public void appeared_data_with_amount (String dataNominal) throws  InterruptedException {
        invoice.showRsultBasedOnNominal (dataNominal) ;
    }

    @And("admin choose status {string}")
    public void admin_choose_status (String statusTransaction) throws InterruptedException {
        invoice.selectDetailStatus (statusTransaction);
    }

    @Then("appeared data transaction with status {string}")
    public void appeared_data_transaction_with_status (String dataStatus){
        invoice.resultDataBasedOnStatus (dataStatus);
    }

    @And("admin choose order type {string}")
    public void admin_choose_order_type (String type) throws InterruptedException {
        invoice.selectOrderType (type);
    }

    @Then("appeared data transaction with order type {string}")
    public void appeared_data_transaction_with_order_type (String resultType) throws InterruptedException {
        invoice.resultDataBasedOnOrderType(resultType);
    }

    @Then("admin will get data invoice {string}")
    public void admin_will_get_data_invoice (String otherInvoiceBooking) throws InterruptedException {
        invoice.getDataInvoice (otherInvoiceBooking);
    }









}
