package steps.mamikos.payment;

import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.mamikos.payment.ManageBillsPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class ManageBillsSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ManageBillsPO bills = new ManageBillsPO(driver);

    private String weeklyPayment="src/test/resources/testdata/mamikos/payment.properties";
    private String nameTenant_ = JavaHelpers.getPropertyValue(weeklyPayment,"nameTenant_" + Constants.ENV);

    @When("user confirm {string} with payment status {string} for tenant {string}")
    public void user_confirm_with_payment_status_for_tenant(String contractStatus, String billStatus, String tenantName) throws InterruptedException {
        String name = "";
        if (tenantName.equals("tenantPayment")){
            name = nameTenant_;
        }

        bills.enterAndSearch(name);
        boolean found = false;

        bills.clickOnLastPage();

        do {
            int numberOfList = bills.getNumberListOfBills();

            for (int i = 1; i <= numberOfList; i++) {
                if (contractStatus.equals("Telah Check-in kos")){
                    if (bills.getAttributeValueContractStatus(i).equals("--status-checkin") && bills.getTenantName(i).contains(name) && bills.getBillStatus(i).equals(billStatus)) {
                        if(bills.checkinContractStatus(i).equals(contractStatus)){
                            bills.clickOnBillsData(i);
                            found = true;
                            break;
                        }
                    }

                } else{
                    if (bills.getAttributeValueContractStatus(i).equals("--status-waiting") && bills.getTenantName(i).contains(name) && bills.getBillStatus(i).equals(billStatus)) {
                        if(bills.waitingContractStatus(i).equals(contractStatus)){
                            bills.clickOnBillsData(i);
                            found = true;
                            break;
                        }
                    }
                }

            }
            if (!found){
                bills.clickOnPrevPageIcon();
            }
        }while(!found);
    }
}
