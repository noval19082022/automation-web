package steps.mamikos.payment;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.sl.In;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.payment.ManualPayoutPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class ManualPayoutSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private ManualPayoutPO manualpayout = new ManualPayoutPO(driver);

    @And("user open menu manual payout")
    public void user_open_menu_manual_payout () throws  InterruptedException {
        manualpayout.openMenuManualPayout ();
    }

    @Then("user will see detail on manual payout menu")
    public void user_will_see_detail_on_manual_payout_menu () throws  InterruptedException{
        manualpayout.userSeeDetailManualPayout ();
    }

    @And("user select data manual payout {string}")
    public void user_select_data_manual_payout (String searchBy) throws InterruptedException {
        manualpayout.userSelectDataManualPayout(searchBy);
    }

    @And("user input invoice number {string}")
    public void user_input_invoice_number (String invoice) throws  InterruptedException {
        manualpayout.inputInvoice (invoice);
    }

    @And("user click button search")
    public void user_click_button_search () throws InterruptedException {
        manualpayout.clickSearch ();
    }

    @Then("user will verify that invoice number")
    public void user_will_verify_that_invoice_number () throws InterruptedException {
        manualpayout.userWillVerifyInvoice ();
    }

    @Then("all column search back to default")
    public void all_column_search_back_to_default () throws InterruptedException {
        manualpayout.columnSearchRestarted ();
    }

    @And("user input account name {string}")
    public void user_input_account_name (String name) throws InterruptedException {
        manualpayout.inputAccountName(name);
    }

    @Then("user will verify that account name")
    public void user_will_verify_that_account_name () throws InterruptedException {
        manualpayout.userVerifyAccountName ();
    }

    @And("user select transaction type {string}")
    public void user_select_transaction_type (String type) throws InterruptedException {
        manualpayout.userSelectTransactionType (type);
    }

    @Then("user verify transaction with {string}")
    public void user_verify_transaction_with (String resultType) throws InterruptedException {
        manualpayout.userVerifyTransactionType (resultType);
    }

    @And("user select transaction status {string}")
    public void user_select_transaction_status (String status) throws InterruptedException {
        manualpayout.userSelectTransactionSttaus (status);
    }

    @Then("user verify transaction with status {string}")
    public void user_verify_transaction_with_status (String resultStatus) throws InterruptedException {
        manualpayout.verifyResultStatus(resultStatus);
    }

    @And("user input create date {string} and {string}")
    public void  user_input_create_date (String dateFrom, String dateTo) throws  InterruptedException {
        manualpayout.userInputCreateDate(dateFrom, dateTo);
    }

    @Then("user verify transaction based on create date from {string} to {string}")
    public void user_verify_transaction_based_on_create_date (String createFrom, String createTo) throws  InterruptedException {
        manualpayout.vefirytTransactionbyCreateDate (createFrom, createTo);
    }

    @And("user click button create manual payout")
    public void user_click_button_create_manual_payout () throws InterruptedException {
        manualpayout.clickButtonCreatePayout ();
    }

    @And("user choose type {string}")
    public void user_choose_type_disbursement (String payoutType) throws InterruptedException {
        manualpayout.choosePayoutType (payoutType);
    }

    @And("user input account number and account name {string}, {string}")
    public void user_input_account_number_and_account_name (String accnumber, String accname) throws InterruptedException{
        manualpayout.inputAccNumberAndName (accnumber, accname);
    }
    @And("user choose bank account {string}")
    public void user_choose_bank_account (String mandiri) throws InterruptedException {
        manualpayout.chooseBankName (mandiri);
    }

    @And("user input amount reason and invoice number {string}, {string}, {string}")
    public void user_input_amount_reason_and_invoice_number (String amount, String reason, String invoice) throws InterruptedException {
        manualpayout.inputAmountReasonAndInvoice (amount, reason, invoice);
    }

    @And("user click button create payout")
    public void user_click_button_create_payout () throws InterruptedException {
        manualpayout.createPayout ();
    }

    @Then("payout created with status and user get message {string}, {string}")
    public void payout_created_with_status_and_user_get_message (String successCreated, String statusPending) throws InterruptedException {
        manualpayout.payoutCreated (successCreated, statusPending);

    }

    @And("user cancel payout transaction")
    public void user_cancel_payout_transaction () throws InterruptedException {
        manualpayout.cancelPayout ();

    }

    @Then("transaction successfully cancel and user get message {string}, {string}")
    public void transaction_successfully_cancel_and_user_get_message (String successCancelled, String statusCancel) throws InterruptedException {
        manualpayout.payoutTransactionCancelled (successCancelled, statusCancel);
    }

    @And("user click change type")
    public void user_click_change_type () throws InterruptedException {
        manualpayout.changeType ();
    }

    @And("user change payout type to {string}")
    public void user_change_payout_type_to (String payoutType) throws InterruptedException {
        manualpayout.chooseNewType (payoutType);
    }
    @And("user click button submit")
    public void user_click_button_submit () throws InterruptedException {
        manualpayout.submitChanges ();
    }

    @Then("payout type is updated and user will get message {string}, {string}")
    public void payout_type_is_updated_and_user_will_get_message (String messageSuccess, String typeUpdated) throws InterruptedException {
        manualpayout.payoutTypeUpdated (messageSuccess, typeUpdated);
    }
    @And("user click change invoice")
    public void user_click_change_invoice () throws InterruptedException {
        manualpayout.changeInvoice ();
    }
    @And("user input new invoice number {string}")
    public void user_input_new_invoice_number (String newInvoiceNumber) throws InterruptedException {
        manualpayout.inputNewInvoice(newInvoiceNumber);
    }

    @Then("invoice number will updated and user get success message {string}, {string}")
    public void invoice_number_will_updated_and_user_get_success_message (String messageSuccess, String newInvoice) throws InterruptedException {
        manualpayout.invoiceUpdated (messageSuccess, newInvoice);
    }

    @And("user click transfer")
    public void user_click_transfer () throws InterruptedException {
        manualpayout.buttonTransfer ();
    }

    @Then("transaction successfully process and user get message {string}, {string}")
    public void transaction_successfully_process_and_user_get_message (String successProcess, String statusProcessing) throws InterruptedException{
        manualpayout.transactionProcessed(successProcess, statusProcessing);
    }

    @And("user click button edit")
    public void user_click_button_edit () throws InterruptedException {
        manualpayout.clickButtonEdit ();
    }

    @And("user input amount and reason {string}, {string}")
    public void user_input_amount_and_reason (String changeAmount, String changeReason) throws InterruptedException {
        manualpayout.inputNewAmountAndReason (changeAmount, changeReason);
    }

    @Then("payout updated with new data {string}, {string}, {string}, {string}, {string}")
    public void payout_updated_with_new_data (String newBankAcc, String newBankName, String newAccName, String newAmount, String newReason) throws InterruptedException {
        manualpayout.shownNewData(newBankAcc, newBankName, newAccName, newAmount, newReason);
    }

    @And("payout not created and user get error message {string}")
    public void payout_not_created_and_user_get_error_message (String messageNeedAllowTransfer) throws  InterruptedException {
        manualpayout.payoutNotCreatedAndGetErrorMessage (messageNeedAllowTransfer);
    }

    @Then("payout not created and user will get error message {string}")
    public void payout_not_created_and_user_will_get_error_message (String errorMinAmount) throws InterruptedException {
        manualpayout.userGetMessageErrorMinAmount (errorMinAmount);
    }

    @And("user click button cancel form payout")
    public void user_click_button_cancel_form_payout () throws InterruptedException {
        manualpayout.CancelFormPayout ();
    }

    @Then("error massage to input mandatory data will appear {string}, {string}")
    public void error_massage_to_input_mandatory_data_will_appear (String errorAmount, String errorReason) throws InterruptedException{
        manualpayout.appearMessageToInputMandatoryData (errorAmount, errorReason);
    }

}