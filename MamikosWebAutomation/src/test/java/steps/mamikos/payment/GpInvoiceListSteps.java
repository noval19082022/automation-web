package steps.mamikos.payment;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.sl.In;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.payment.GpInvoiceListPO;
import utilities.ThreadManager;

public class GpInvoiceListSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private GpInvoiceListPO gpinvoicelist = new GpInvoiceListPO(driver);

    @And("user open menu GP invoice")
    public void user_open_menu_GP_invoice () throws InterruptedException {
        gpinvoicelist.openMenuGpInvoice ();
    }

    @And("user input GP invoice number {string}")
    public void user_input_GP_invoice_number (String GPinvoice) throws  InterruptedException  {
        gpinvoicelist.inputGPinvoice(GPinvoice);
    }

    @And("user click button cari GPinvoice")
    public void user_click_button_cari_GPinvoice () throws InterruptedException {
        gpinvoicelist.clickCariGpInvoice ();
    }

    @Then("user will verify detail GP invoice")
    public void user_will_verify_detail_GP_invoice () throws InterruptedException {
        gpinvoicelist.verifyGpInvoiceDetail ();
    }

    @Then("user will get blank data")
    public void user_will_get_blank_data () throws InterruptedException {
        gpinvoicelist.getBlankData ();
    }

    @And("user input owner number {string}")
    public void user_input_owner_number (String OwnerNumber) throws  InterruptedException  {
        gpinvoicelist.inputOwnerNumber(OwnerNumber);
    }

    @And("user choose owner number")
    public void user_choose_owner_number () throws InterruptedException {
        gpinvoicelist.selectOwnerNumber ();
    }

    @And("user choose invoice code")
    public void user_choose_invoice_code () throws InterruptedException {
        gpinvoicelist.selectInvoiceCode ();
    }

    @And("user input invoice code {string}")
    public void user_input_invoice_code (String InvoiceCode) throws  InterruptedException {
        gpinvoicelist.inputInvoiceCode(InvoiceCode);
    }

    @And("user click reset button")
    public void user_click_reset_button () throws  InterruptedException {
        gpinvoicelist.clickButtonReset ();
    }

    @And("user column search by will reset to invoice number and data search restarted")
    public void user_column_search_by_will_reset_to_invoice_number_and_data_search_restarted () throws InterruptedException {
        gpinvoicelist.searchByBackToRestarted ();
    }

    @And("user choose {string}")
    public void user_choose_status_unpaid (String GPstatus) throws InterruptedException {
        gpinvoicelist.userSelectStatusGP (GPstatus);
    }

    @Then("will show data transaction with {string}")
    public void will_show_data_transaction_with (String resultGPInvoice) throws InterruptedException {
        gpinvoicelist.willShowAccordingGPStatus (resultGPInvoice);
    }

    @And("user choose package type {string}")
    public void user_choose_package_type (String gpType) throws  InterruptedException {
        gpinvoicelist.selectGPtype (gpType);
    }

    @Then("will show data transaction with package type {string}")
    public void will_show_data_transaction_with_package_type (String gpDetail) throws  InterruptedException {
        gpinvoicelist.shownGPdetail (gpDetail);
    }

    @And("input date detail {string} and {string}")
    public void input_date_detail (String date1, String date2) throws InterruptedException{
        gpinvoicelist.chooseDueDateFrom (date1,date2);
    }


    @Then("will show data transaction according detail date")
    public void will_show_data_transaction_according_detail_date () throws InterruptedException {
        gpinvoicelist.showDetailTransactionAccordingDetailDate ();
    }

    @Then("user input phone number {string} at gp recurring")
    public void user_input_phone_number_at_gp_recurring(String phoneNumberGP) throws InterruptedException {
       gpinvoicelist.inputPhoneNumberGP(phoneNumberGP);
    }

    @Then("user click button reset at gp recurring")
    public void user_click_button_reset_at_gp_recurring() throws InterruptedException {
        gpinvoicelist.clickButtonResetGP();
    }

    @Then("user input phone number {string} at gp recurring tools")
    public void user_input_phone_number_at_gp_recurring_tools(String phoneNumberGPrecurring) throws InterruptedException {
        gpinvoicelist.inputPhoneNumberGPrecurring(phoneNumberGPrecurring);
    }

    @Then("user click button recurring at gp recurring tools")
    public void user_click_button_recurring_at_gp_recurring_tools() throws InterruptedException {
        gpinvoicelist.clickButtonRecurringGP();
    }

    @Then("user see text {string} at gp recurring tools")
    public void user_see_text_at_gp_recurring_tools(String notifSuccsess) throws InterruptedException {
        Assert.assertEquals(gpinvoicelist.getTexSuccsessNotif(), notifSuccsess, "Text is not equal");
    }

}
