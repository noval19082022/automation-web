package steps.mamikos.payment;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.payment.invoiceSecurityPO;
import utilities.ThreadManager;

public class invoiceSecurityStep {
    private WebDriver driver = ThreadManager.getDriver();
    private invoiceSecurityPO invoiceSecurity = new invoiceSecurityPO(driver);

    @And("admin click Link {string}")
    public void admin_click_link_invoice(String shortlink) throws InterruptedException {
        invoiceSecurity.clickLinkInvoice(shortlink);
    }

    @And("admin click first shortlink")
    public void admin_click_first_shortlink() throws InterruptedException {
        invoiceSecurity.clickFirstShortlink();
    }

    @And("user click Riwayat button")
    public void user_click_riwayat_button() throws InterruptedException {
        invoiceSecurity.clickRiwayatButton();
    }

    @And("user click Selesai button")
    public void user_click_selesai_button() throws InterruptedException {
        invoiceSecurity.clickSelesaiButton();
    }

    @And("user choose invoice paid from list selesai")
    public void user_choose_invoice_Paid() throws InterruptedException {
        invoiceSecurity.chooseInvoicePaid();
    }

    @And("user choose invoice unpaid from list selesai")
    public void user_choose_invoice_Unpaid() throws InterruptedException {
        invoiceSecurity.chooseInvoiceUnpaid();
    }

    @And("user click dalam proses button")
    public void user_dalam_proses_button() throws InterruptedException {
        invoiceSecurity.dalamProsesButton();
    }

    @And("admin open menu Goldplus invoice list")
    public void admin_open_menu_gp_invoice_list() throws InterruptedException {
        invoiceSecurity.openMenuGpInvoiceList();
    }

    @Then("admin see shown invoice not found {string} {string}")
    public void admin_see_shown_invoice_not_found(String message1, String message2) throws InterruptedException {
        if (invoiceSecurity.isInvoiceKedaluarsa()) {
            Assert.assertEquals(invoiceSecurity.showInvoiceUnpaidKedaluarsa(), message1, "Invoice Kedaluwarsa");
        } else {
            Assert.assertEquals(invoiceSecurity.showInvoiceTidakDitemukan(), message2, "Invoice Tidak Ditemukan");
        }
    }

    @Then("admin see shown invoice Paid {string} {string} {string}")
    public void admin_see_shown_invoice_paid(String message1, String message2, String message3) throws InterruptedException {
        if (invoiceSecurity.isInvoiceTidakDitemukan()) {
            Assert.assertEquals(invoiceSecurity.showInvoiceTidakDitemukan(), message2, "Invoice Tidak Ditemukan");
        } else if (invoiceSecurity.isInvoicePaid()) {
            Assert.assertEquals(invoiceSecurity.getInvoicePaid(), message1, "Pembayaran Berhasil");
        } else {
            Assert.assertEquals(invoiceSecurity.showInvoiceUnpaidKedaluarsa(), message3, "Invoice Kedaluwarsa");
    }
}

    @Then("user see shown invoice unpaid {string} {string} {string}")
    public void admin_see_shown_invoice_unpaid(String message1, String message2, String message3) throws InterruptedException {
        if (invoiceSecurity.isInvoiceKedaluarsa()) {
            Assert.assertEquals(invoiceSecurity.showInvoiceUnpaidKedaluarsa(), message2, "Invoice Kedaluwarsa");
        } else if (invoiceSecurity.isInvoiceTidakDitemukan()){
            Assert.assertEquals(invoiceSecurity.showInvoiceTidakDitemukan(), message3, "Invoice Tidak Ditemukan");
        } else {
            Assert.assertEquals(invoiceSecurity.showInvoiceUnpaid(), message1, "Pembayaran");
            }
        }
    }
