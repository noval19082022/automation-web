package steps.mamikos.payment;

import dataobjects.Invoice;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.sl.In;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.bni.BankChannelSimulatorPO;
import pageobjects.creditCard.SimulatorCreditCardPO;
import pageobjects.dana.SimulatorDanaPO;
import pageobjects.mamikos.mamiads.mamiads.BeliSaldoAdminPagePO;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import pageobjects.mamikos.tenant.payment.PaymentMethodPO;
import pageobjects.mandiri.BankChannelSimulatorMandiriPO;
import pageobjects.permata.BankChannelSimulatorPermataPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.ArrayList;
import java.util.List;

public class PaymentMethodSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    JavaHelpers java;
    private PaymentMethodPO payment = new PaymentMethodPO(driver);
    private BankChannelSimulatorPO bni = new BankChannelSimulatorPO(driver);
    private BankChannelSimulatorMandiriPO mandiri = new BankChannelSimulatorMandiriPO(driver);
    private BankChannelSimulatorPermataPO permata = new BankChannelSimulatorPermataPO(driver);
    private SimulatorDanaPO dana = new SimulatorDanaPO(driver);
    private SimulatorCreditCardPO creditCard = new SimulatorCreditCardPO(driver);
    private KosDetailPO kosDetailPO = new KosDetailPO(driver);
    private BeliSaldoAdminPagePO beliSaldoPO = new BeliSaldoAdminPagePO(driver);





    //Test Data Payment
    private String paymentData="src/test/resources/testdata/mamikos/payment.properties";
    private String kostPrice_weekly_ = JavaHelpers.getPropertyValue(paymentData,"kostPrice_weekly_" + Constants.ENV);
    private String kostPrice_weekly_dp = JavaHelpers.getPropertyValue(paymentData,"kostPrice_weekly_dp_" + Constants.ENV);
    private String kostPrice_monthly_ = JavaHelpers.getPropertyValue(paymentData,"kostPrice_monthly_" + Constants.ENV);
    private String kostPrice_monthly_dp = JavaHelpers.getPropertyValue(paymentData,"kostPrice_monthly_dp_" + Constants.ENV);
    private String kostPrice_quarterly_ = JavaHelpers.getPropertyValue(paymentData,"kostPrice_quarterly_" + Constants.ENV);
    private String kostPrice_quarterly_dp = JavaHelpers.getPropertyValue(paymentData,"kostPrice_quarterly_dp_" + Constants.ENV);
    private String kostPrice_sixMonths_ = JavaHelpers.getPropertyValue(paymentData,"kostPrice_sixMonths_" + Constants.ENV);
    private String kostPrice_sixMonths_dp = JavaHelpers.getPropertyValue(paymentData,"kostPrice_sixMonths_dp_" + Constants.ENV);
    private String kostPrice_annually_ = JavaHelpers.getPropertyValue(paymentData,"kostPrice_annually_" + Constants.ENV);
    private String kostPrice_annually_bni_ = JavaHelpers.getPropertyValue(paymentData,"kostPrice_annually_bni_" + Constants.ENV);
    private String kostPrice_annually_dp = JavaHelpers.getPropertyValue(paymentData,"kostPrice_annually_dp_" + Constants.ENV);
    private String kostAdminFee_ = JavaHelpers.getPropertyValue(paymentData,"kostAdminFee_" + Constants.ENV);
    private String invoiceAdminFee_ = JavaHelpers.getPropertyValue(paymentData,"invoiceAdminFee_" + Constants.ENV);
    private String depositFee_ = JavaHelpers.getPropertyValue(paymentData,"depositFee_" + Constants.ENV);
    private String penaltyFee_ = JavaHelpers.getPropertyValue(paymentData,"penaltyFee_" + Constants.ENV);
    private String additionalCosts_ = JavaHelpers.getPropertyValue(paymentData,"additionalCosts_" + Constants.ENV);
    private String downPayment_ = JavaHelpers.getPropertyValue(paymentData,"downPayment_" + Constants.ENV);
    private String voucherCode_ = JavaHelpers.getPropertyValue(paymentData,"voucherCode_" + Constants.ENV);
    private String amountVoucher_ = JavaHelpers.getPropertyValue(paymentData,"amountVoucher_" + Constants.ENV);
    private String kostPrice_TENG_ = JavaHelpers.getPropertyValue(paymentData,"kostPrice_TENG_" + Constants.ENV);

    //Data tenant payment
    private String nameTenant_ = JavaHelpers.getPropertyValue(paymentData,"nameTenant_" + Constants.ENV);
//    private String nameTENGTenant_ = JavaHelpers.getPropertyValue(paymentData,"nameTENGTenant_" + Constants.ENV);

    //Test Data Loyalty
    private String voucherkuData="src/test/resources/testdata/mamikos/voucherku.properties";
    private String roomPriceForApplyVoucherBBK_ = JavaHelpers.getPropertyValue(voucherkuData,"roomPriceForApplyVoucherBBK_" + Constants.ENV);
    private String tenantNameApplyVoucher_ = JavaHelpers.getPropertyValue(voucherkuData,"tenantNameForApplyVoucherBBK_" + Constants.ENV);
    private String downPaymentApplyVoucher_ = JavaHelpers.getPropertyValue(voucherkuData,"downPaymentApplyVoucher_" + Constants.ENV);
    private String nameTENGTenant_ = JavaHelpers.getPropertyValue(voucherkuData,"nameTENGTenant_" + Constants.ENV);


    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String kostPriceConstultant = JavaHelpers.getPropertyValue(consultantProperty,"monthlyPrice_Griyatobelo_"+ Constants.ENV);

    //Test Data Booking and Billing (OB)
    private String bnbProperty = "src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
    private String kostFullPayKosSaya_ = JavaHelpers.getPropertyValue(bnbProperty, "kostBookingAddtional_" + Constants.ENV);
    private String additionalCostBnb_ = JavaHelpers.getPropertyValue(bnbProperty, "additionalCostBnb_" + Constants.ENV);
    private String depositFeeBnb_ = JavaHelpers.getPropertyValue(bnbProperty, "depositFeeBnb_" + Constants.ENV);
    private String kostAdminFeeBnb_ = JavaHelpers.getPropertyValue(bnbProperty, "kostAdminFeeBnb_" + Constants.ENV);
    private String invoiceAdminFeeBnb_ = JavaHelpers.getPropertyValue(bnbProperty, "invoiceAdminFeeBnb_" + Constants.ENV);

    @When("user select payment method {string} for {string}")
    public void user_select_payment_method_for(String paymentMethod, String totalPaid) throws InterruptedException {
        String paymentURL = "";
        String getTotalAmount;
        int totalAmount;
        int totalAmountBnB;
        int kosRent = 0;
        payment.selectPaymentMethod(paymentMethod);
        if (totalPaid.equals("paymentW1_voucher")){
            payment.inputVoucher(voucherCode_);

        }
        payment.clickOnPayNowButton();
        switch (totalPaid){
            case "paymentW1":
            case "paymentW2":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_weekly_))) +
                        Integer.parseInt(additionalCosts_) +
                        Integer.parseInt(depositFee_);
                break;
            case "paymentW1DP":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_weekly_dp)));
                break;
            case "paymentW1_voucher":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_weekly_))) -
                Integer.parseInt(String.valueOf(java.extractNumber(amountVoucher_)));
                break;
            case "paymentB1":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_monthly_)));
                break;
            case "paymentB1DP":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_monthly_dp)));
                break;
            case "payment3B1":
            case "payment3B2":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_quarterly_))) +
                        (Integer.parseInt(additionalCosts_) * 3)+
                        Integer.parseInt(depositFee_);
                break;
            case "payment6B1":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_sixMonths_)));
                break;
            case "paymentT1":
            case "paymentT2":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_annually_))) +
                        (Integer.parseInt(additionalCosts_) * 12);
                      //  Integer.parseInt(depositFee_);
                break;
            case "paymentPF":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_annually_bni_)));
                break;
            case "paymentB2":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_monthly_))) +
                        Integer.parseInt(additionalCosts_) +
                        Integer.parseInt(depositFee_);
                break;
            case "payment6B2":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_sixMonths_))) +
                        Integer.parseInt(additionalCosts_) +
                        Integer.parseInt(depositFee_);
                break;
            case "kostBnB":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostFullPayKosSaya_))) +
                        Integer.parseInt(additionalCostBnb_) +
                        Integer.parseInt(depositFeeBnb_);
                break;
            case "paymentB3":
            case "payment3B3":
            case "payment6B3":
            case "paymentT3":
                kosRent = Integer.parseInt(downPayment_);
                break;
            case "paymentVoucherkuB1":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(roomPriceForApplyVoucherBBK_)));
                break;
            case "paymentVoucherkuB2":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(downPaymentApplyVoucher_)));
                break;
            case "consultant":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPriceConstultant)));
                break;
            case "paymentTENG":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_TENG_)));
                break;
        }

        if (totalPaid.equalsIgnoreCase("invoice manual")){
            totalAmount = Integer.parseInt(Invoice.getTotalAmount());
        } else {
            totalAmount = kosRent +
                    Integer.parseInt(kostAdminFee_) +
                    Integer.parseInt(invoiceAdminFee_);
        }

        if(paymentMethod.equals("BNI")){
            String virtualAccountNumber = payment.getVirtualAccountNumber();
            paymentURL = Constants.BNI_PAYMENT;
            selenium.navigateToPage(paymentURL);
            bni.enterTextVirtualAccount(virtualAccountNumber);
            bni.clickOnSearchVirtualAccount();
            if (totalPaid.equals("paymentVoucherkuB1")){
                Assert.assertEquals(bni.getName(), tenantNameApplyVoucher_, "Tenant name is " + bni.getName());
            }
            else if (totalPaid.equals("paymentTENG")) {
                Assert.assertEquals(bni.getName(), nameTENGTenant_, "Tenant name is " + bni.getName());
                System.out.println("nama akun" + bni.getName());
            }
            else{
                Assert.assertEquals(bni.getName(), nameTenant_, "Tenant name is " + bni.getName());
            }
            getTotalAmount = String.valueOf(java.extractNumber(bni.getTotalAmountLabel()));
            Assert.assertEquals(Integer.parseInt(getTotalAmount), totalAmount, "Total amount is " + getTotalAmount);
            bni.enterTextPaymentAmount(getTotalAmount);
            bni.clickOnPaymentButton();

        }else if(paymentMethod.equals("Mandiri")){
            String virtualAccountNumber = payment.getVirtualAccountNumber();
            String kodePerusahaan = payment.getKodePerusahaan();
            paymentURL = Constants.MANDIRI_PAYMENT;
            selenium.navigateToPage(paymentURL);
            mandiri.enterTextBillerCodeMandiri(kodePerusahaan);
            mandiri.enterTextVirtualAccountMandiri(virtualAccountNumber);
            mandiri.clickOnSearchVirtualAccountMandiri();
            getTotalAmount = String.valueOf(java.extractNumber(mandiri.getTotalAmountLabel()));
            Assert.assertEquals(Integer.parseInt(getTotalAmount), totalAmount, "Total amount is " + getTotalAmount);
            mandiri.clickOnPaymentMandiriButton();

        }else if(paymentMethod.equals("Permata")){
            String virtualAccountNumber = payment.getVirtualAccountNumberPermata();
            paymentURL = Constants.PERMATA_PAYMENT;
            selenium.navigateToPage(paymentURL);
            permata.enterTextVirtualAccount(virtualAccountNumber);
            permata.clickOnSearchVirtualAccountPermata();
            getTotalAmount = String.valueOf(java.extractNumber(permata.getTotalAmountLabel()));
            String getTotalAmountNew = getTotalAmount.substring(0, getTotalAmount.length() - 2);
            Assert.assertEquals(Integer.parseInt(getTotalAmountNew), totalAmount, "Total amount is " + getTotalAmount);
            permata.clickOnPaymentPermataButton();
        }else if(paymentMethod.equals("OVO")){
            beliSaldoPO.invoiceNumberMamiads = bni.getInvoiceNumberMamiAds();
            bni.clickSayaSudahBayar();
            bni.clickToConfirm();
        }else if(paymentMethod.equals("DANA Production")){
            dana.clickBayarViaDana();
            dana.inputPhoneNumberAndPin();
            getTotalAmount = String.valueOf(java.extractNumber(dana.getTotalAmountLabel()));
            Assert.assertEquals(Integer.parseInt(getTotalAmount), totalAmount, "Total amount is " + getTotalAmount);
            dana.clickOnPaymentDanaButton();
        }else if(paymentMethod.equals("DANA")){
            dana.clickBayarViaDana();
            dana.clickPaymentProcess();
        }else if(paymentMethod.equals("LinkAja")){
            dana.clickBayarViaLinkAja();
            dana.clickPaymentProcess();
        }else if(paymentMethod.equals("Kartu Kredit")){
            creditCard.enterPassword();
        }
    }

    @When("user select repayment method {string} for {string}")
    public void user_select_repayment_method_for(String paymentMethod, String tenantName) throws InterruptedException {
        String paymentURL = "";
        String getTotalAmount = "";
        int totalAmount;
        int kosRent = 0;
        selenium.switchToWindow(2);
        payment.selectPaymentMethod(paymentMethod);
        payment.clickOnPayNowButton();
        switch (tenantName){
            case "paymentW2":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_weekly_)));
                break;
            case "paymentB3":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_monthly_))) ;
                break;
            case "payment3B3":
            case "payment3ST":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_quarterly_dp)))+
                        (Integer.parseInt(additionalCosts_) * 3);
                break;
            case "payment6B3":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_sixMonths_)));
                break;
            case "paymentT3":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_annually_dp))) +
                          (Integer.parseInt(additionalCosts_) * 12);
                break;
            case "paymentB2":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_monthly_dp))) +
                          Integer.parseInt(additionalCosts_);
                break;
            case "paymentWST":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_weekly_dp))) +
                          Integer.parseInt(additionalCosts_);
                break;
            case "payment6ST":
                kosRent = Integer.parseInt(String.valueOf(java.extractNumber(kostPrice_sixMonths_dp))) +
                          (Integer.parseInt(additionalCosts_) * 6);
                break;

        }
        totalAmount = (kosRent) +
                Integer.parseInt(depositFee_) +
                Integer.parseInt(kostAdminFee_) +
                Integer.parseInt(invoiceAdminFee_) ;
        if(paymentMethod.equals("BNI")){
            String virtualAccountNumber = payment.getVirtualAccountNumber();
            paymentURL = Constants.BNI_PAYMENT;
            selenium.navigateToPage(paymentURL);
            bni.enterTextVirtualAccount(virtualAccountNumber);
            bni.clickOnSearchVirtualAccount();
            Assert.assertEquals(bni.getName(), nameTenant_, "Total amount is " + bni.getName());
            getTotalAmount = String.valueOf(java.extractNumber(bni.getTotalAmountLabel()));
            Assert.assertEquals(Integer.parseInt(getTotalAmount), totalAmount, "Total amount is " + getTotalAmount);
            bni.enterTextPaymentAmount(getTotalAmount);
            bni.clickOnPaymentButton();// worker must run
        }else if(paymentMethod.equals("Mandiri")){
            String virtualAccountNumber = payment.getVirtualAccountNumber();
            String kodePerusahaan = payment.getKodePerusahaan();
            paymentURL = Constants.MANDIRI_PAYMENT;
            selenium.navigateToPage(paymentURL);
            mandiri.enterTextBillerCodeMandiri(kodePerusahaan);
            mandiri.enterTextVirtualAccountMandiri(virtualAccountNumber);
            mandiri.clickOnSearchVirtualAccountMandiri();
            getTotalAmount = String.valueOf(java.extractNumber(mandiri.getTotalAmountLabel()));
            Assert.assertEquals(Integer.parseInt(getTotalAmount), totalAmount, "Total amount is " + getTotalAmount);
            mandiri.clickOnPaymentMandiriButton();

        }else if(paymentMethod.equals("Permata")){
            String virtualAccountNumber = payment.getVirtualAccountNumberPermata();
            paymentURL = Constants.PERMATA_PAYMENT;
            selenium.navigateToPage(paymentURL);
            permata.enterTextVirtualAccount(virtualAccountNumber);
            permata.clickOnSearchVirtualAccountPermata();
            getTotalAmount = String.valueOf(java.extractNumber(permata.getTotalAmountLabel()));
            String getTotalAmountNew = getTotalAmount.substring(0, getTotalAmount.length() - 2);
            Assert.assertEquals(Integer.parseInt(getTotalAmountNew), totalAmount, "Total amount is " + getTotalAmount);
            permata.clickOnPaymentPermataButton();
        }else if(paymentMethod.equals("OVO")){
            bni.clickSayaSudahBayar();
            bni.clickToConfirm();
        }else if(paymentMethod.equals("DANA Production")){
            dana.clickBayarViaDana();
            dana.inputPhoneNumberAndPinRepayment();
            getTotalAmount = String.valueOf(java.extractNumber(dana.getTotalAmountLabel()));
            Assert.assertEquals(Integer.parseInt(getTotalAmount), totalAmount, "Total amount is " + getTotalAmount);
            dana.clickOnPaymentDanaButton();
        }else if(paymentMethod.equals("DANA")){
            dana.clickBayarViaDana();
            dana.clickRepaymentProcess();
        }else if(paymentMethod.equals("LinkAja")){
            dana.clickBayarViaLinkAja();
            dana.clickRepaymentProcess();
        }else if(paymentMethod.equals("Kartu Kredit")){
            creditCard.enterPassword();
        }

    }

    @Then("user make bill payments using {string}")
    public void user_make_bill_payments_using(String paymentMethod) throws InterruptedException {
        String paymentURL = "";
        String getTotalAmount = "";
        payment.selectPaymentMethod(paymentMethod);
        payment.clickOnPayNowButton();
        if (paymentMethod.equals("BNI")) {
            String virtualAccountNumber = payment.getVirtualAccountNumber();
            paymentURL = Constants.BNI_PAYMENT;
            selenium.navigateToPage(paymentURL);
            bni.enterTextVirtualAccount(virtualAccountNumber);
            bni.clickOnSearchVirtualAccount();
            getTotalAmount = String.valueOf(JavaHelpers.extractNumber(bni.getTotalAmountLabel()));
            bni.enterTextPaymentAmount(getTotalAmount);
            bni.clickOnPaymentButton();
        }
        else if(paymentMethod.equals("Mandiri")) {
            String virtualAccountNumber = payment.getVirtualAccountNumber();
            String kodePerusahaan = payment.getKodePerusahaan();
            paymentURL = Constants.MANDIRI_PAYMENT;
            selenium.navigateToPage(paymentURL);
            mandiri.enterTextBillerCodeMandiri(kodePerusahaan);
            mandiri.enterTextVirtualAccountMandiri(virtualAccountNumber);
            mandiri.clickOnSearchVirtualAccountMandiri();
            mandiri.clickOnPaymentMandiriButton();
        }
    }

    @Then("user teng make bill payments using {string}")
    public void user_teng_make_bill_payments_using(String paymentMethod) throws InterruptedException {
        String paymentURL = "";
        String getTotalAmount = "";
        payment.selectPaymentMethod(paymentMethod);
        payment.clickOnPayNowButton();
        if (paymentMethod.equals("BNI")) {
            String virtualAccountNumber = payment.getVirtualAccountNumber();
            paymentURL = Constants.BNI_PAYMENT;
            selenium.openNewTab();
            ArrayList<String> tabs = new ArrayList<>(selenium.getWindowHandles());
            selenium.switchToWindow(tabs.size());
            selenium.navigateToPage(paymentURL);
            bni.enterTextVirtualAccount(virtualAccountNumber);
            bni.clickOnSearchVirtualAccount();
            getTotalAmount = String.valueOf(JavaHelpers.extractNumber(bni.getTotalAmountLabel()));
            bni.enterTextPaymentAmount(getTotalAmount);
            bni.clickOnPaymentButton();
        }
        else if(paymentMethod.equals("Mandiri")) {
            String virtualAccountNumber = payment.getVirtualAccountNumber();
            String kodePerusahaan = payment.getKodePerusahaan();
            paymentURL = Constants.MANDIRI_PAYMENT;
            selenium.openNewTab();
            selenium.switchToWindow(3);
            selenium.navigateToPage(paymentURL);
            mandiri.enterTextBillerCodeMandiri(kodePerusahaan);
            mandiri.enterTextVirtualAccountMandiri(virtualAccountNumber);
            mandiri.clickOnSearchVirtualAccountMandiri();
            mandiri.clickOnPaymentMandiriButton();
        }
    }

    @Given("user select payments method using {string}")
    public void user_select_payments_method_using(String paymentMethod) throws InterruptedException {
        payment.selectPaymentMethod(paymentMethod);
    }

    @When("user click on pay now button")
    public void user_click_on_pay_now_button() throws InterruptedException {
        payment.clickOnPayNowButton();
    }

    @Then("system display payment using {string} is {string}")
    public void system_display_payment_using_is(String payment, String message) throws  InterruptedException{
        if(payment.equals("BNI")){//success
            Assert.assertTrue(bni.getPaymentMessage().contains(message), "Payment not success");
        }else if(payment.equals("Mandiri")){//Success Transaction
            Assert.assertTrue(mandiri.getPaymentMessage().contains(message),"Payment not success");
        }else if(payment.equals("Permata")){//Transaksi Sukses
            Assert.assertTrue(permata.getPaymentMessage().contains(message),"Payment not success");
        }else if(payment.equals("OVO") || payment.equals("DANA") || payment.equals("Kartu Kredit")){//Pembayaran Berhasil
            Assert.assertTrue(creditCard.getPaymentMessage().contains(message),"Payment not success");
        }
    }

    @When("user remove voucher by toast message")
    public void user_remove_voucher_by_toast_message() throws InterruptedException {
        payment.clickOnDeleteOnToastMessage();
    }

    @Then("system display toast message {string}")
    public void system_display_toast_message(String message) {
        Assert.assertEquals(payment.getToastMessage(), message, "Toast not match");
    }

    @Then("tenant can not sees add on price on payment page")
    public void tenant_can_not_sees_add_on_price_on_payment_page() {
        ArrayList<String> tabs = new ArrayList<>(selenium.getWindowHandles());
        selenium.switchToWindow(tabs.size());
        int basicAmount = payment.getBasicPrice();
        int adminFee = payment.getAdminPrice();
        int totalAmount = payment.getSubTotal();

        Assert.assertEquals(basicAmount + adminFee, totalAmount, "Basic amount + admin fee is not equal with total amount");
    }

    @When("tenant goes to invoice page with payment method is {string}")
    public void tenant_goes_to_invoice_page_with_payment_method_is(String bankName) throws InterruptedException {
        payment.selectPaymentMethod(bankName);
        payment.clickOnPayNowButton();
    }

    @Then("tenant can sees add ons fee with name {string}")
    public void tenant_can_sees_add_ons_fee_with_name(String addOnsName) {
        ArrayList<String> tabs = new ArrayList<>(selenium.getWindowHandles());
        selenium.switchToWindow(tabs.size());
        Assert.assertTrue(payment.isAddOnsWithName(addOnsName), "Add ons name is not present");
    }

    @When("tenant clicks on I already Paid")
    public void tenant_clicks_on_I_already_Paid() throws InterruptedException {
        ArrayList<String> tabs = new ArrayList<>(selenium.getWindowHandles());
        selenium.switchToWindow(tabs.size() - 1);
        payment.clickOnAlreadyPaidButton();
    }

    @Then("tenant can sees payment success")
    public void tenant_can_sees_payment_success() throws InterruptedException {
        selenium.hardWait(5);
        Assert.assertEquals(payment.getSuccesPaidText(), "Pembayaran Berhasil", "No complete payment text visible");
    }

    @Then("tenant can sees add ons paid as {string}")
    public void tenant_can_sees_add_ons_payment_as(String addOnsPriceType) {
        Assert.assertTrue(payment.isAddOnsPaidWithName(addOnsPriceType), addOnsPriceType + " Add ons is not present");
    }

    @When("tenant can not sees price with name {string} on invoice page")
    public void tenant_can_not_sees_price_with_name_on_invoice_page(String addOnsPriceType) {
        Assert.assertFalse(payment.isAddOnsPaidWithName(addOnsPriceType), addOnsPriceType + " Add ons is present");
    }

    @Then("tenant can sees add ons fee with name {string} and price {string}")
    public void tenant_can_sees_add_ons_fee_with_name_and_price(String addOnsName, String price) {
        ArrayList<String> tabs = new ArrayList<>(selenium.getWindowHandles());
        selenium.switchToWindow(tabs.size());
        Assert.assertTrue(payment.isAddOnsWithName(addOnsName), "Add ons name is not present");
        Assert.assertEquals(payment.getPriceNumberWithName(addOnsName), price);
    }

    @Then("^tenant can sees total cost is equal to :$")
    public void tenant_can_sees_total_cost_is_equal_to(List<String> priceList) {
        int totalCostCombine = 0;
        int totalCost = Integer.parseInt(payment.getAdditionalPriceNumberText("Total Pembayaran"));
        for(int i = 0; i < 3; i++) {
            int toCount = 0;
            if(priceList.get(i).contains("Admin Fee")) {
                toCount = Integer.parseInt(payment.getAdditionalPriceNumberText(priceList.get(i)));
            }
            else {
                toCount = Integer.parseInt(payment.getPriceNumberWithName(priceList.get(i)));
            }
            totalCostCombine = totalCostCombine + toCount;
        }
        Assert.assertEquals(totalCost, totalCostCombine, "Price is not equals");
    }

    @Then("user close payment tab")
    public void user_close_payment_tab() throws InterruptedException {
        selenium.closeTabWindowBrowser();
        selenium.switchToWindow(1);
    }
}
