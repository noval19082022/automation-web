package steps.mamikos.payment;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.payment.MyKostPO;
import pageobjects.mamikos.tenant.booking.BookingListPO;
import pageobjects.mamikos.tenant.booking.KosDetailPO;
import pageobjects.mamikos.tenant.profile.TenantProfilePO;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public class MyKostSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private MyKostPO myKost = new MyKostPO(driver);
    private BookingListPO booking = new BookingListPO(driver);
    private HeaderPO header = new HeaderPO(driver);
    private TenantProfilePO tenantProfile = new TenantProfilePO(driver);
    private JavaHelpers java = new JavaHelpers();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private KosDetailPO kosDetailPO;

    @Then("system display terminate contract link")
    public void system_display_terminate_contract_link() {
        myKost.displayTerminateContract();
    }

    @When("user clicks link terminate contract with rental period is {string}")
    public void user_clicks_link_terminate_contract_with_rental_period_is(String rentDuration) throws InterruptedException {
        Assert.assertEquals(booking.getRentalPeriod(1), rentDuration, "Rent duration not " + rentDuration);
        booking.clickOnFirstMyKostButton();
        myKost.clickOnBillTab();
        myKost.clickOnTerminateContract();
    }

    @When("user select reason terminate contract {string}")
    public void user_select_reason_terminate_contract(String reason) throws InterruptedException {
        myKost.selectReasonTerminateContract(reason);
    }

    @When("user select date out from boarding house")
    public void user_select_date_out_from_boarding_house() throws InterruptedException, ParseException {
//        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
//        myKost.selectDate(today);
        myKost.selectDate();
    }

    @When("user give a rating {int} for {string}")
    public void user_give_a_rating_for(Integer point, String option) throws InterruptedException {
        myKost.selectRating(point, option);
    }

    @When("user input {string} on field kost review and click terminate contract button")
    public void user_input_on_field_kost_review_and_click_terminate_contract_button(String text) throws InterruptedException {
        myKost.enterKostReview(text);
        myKost.clickOnTerminateContractButton();
        //kosDetailPO.dismissFTUEScreen();
    }

    @And("user clicks ajukan berhenti sewa button")
    public void user_clicks_ajukan_berhenti_sewabutton() throws InterruptedException {
        myKost.clickOnAjukanBerhentiSewa();
    }

    @Then("system display message {string} on bill tab")
    public void system_display_message_on_bill_tab(String message) {
        Assert.assertTrue(myKost.getMessage().contains(message), "Bills message not contains " + message);
    }

    @When("user navigates to My Kost page")
    public void user_navigates_to_my_kost_page() throws InterruptedException {
        header.navigateToTenantProfile();
        tenantProfile.clickOnMyKostMenu();
    }

    @And("user click on billing tab")
    public void user_click_on_billing_tab() throws InterruptedException {
        myKost.clickOnBillTab();
    }

    @And("user click on tagihan tab")
    public void user_click_on_tagihan_tab() throws InterruptedException {
        myKost.clickOnTagihanTab();
    }

    @And("user click sudah di bayar")
    public void user_click_sudah_dibayar() throws InterruptedException {
        myKost.clickOnSudahDibayar();
    }

    @And("user click on-time")
    public void user_click_one_time() throws InterruptedException {
        myKost.clickOnOneTime();
    }

    @And("user clicks beri review untuk kost")
    public void user_click_on_beri_review_untuk_kost() throws InterruptedException {
        myKost.clickOnReviewKost();
    }

    @And("user click pay for next month reccuring payment")
    public void user_click_pay_for_next_month_reccuring_payment() {
        myKost.clickPayButtonForNextPayment();
    }

    @When("user make a payment from bill tab")
    public void user_make_a_payment_from_bill_tab() throws InterruptedException {
        myKost.clickOnBillTab();
        myKost.clickOnPayButton();
        selenium.switchToWindow(2);
    }

    @And("user click to give kost review")
    public void user_click_to_give_kost_review () throws InterruptedException {
        myKost.clickToReviewKost ();
    }

    @And("user click selesai button")
    public void user_click_selesai_button () throws InterruptedException {
        myKost.clickSelesaiButton ();
    }

    @And("user click button terminate")
    public void user_click_button_terminate () throws InterruptedException {
        myKost.clickTerminateButton ();
    }

    @Then("user get popup message {string}")
    public void user_get_popup_message (String popupMessage) throws InterruptedException {
        Assert.assertTrue(myKost.messageRequestTerminate().contains(popupMessage), "user get popup message" + popupMessage);
    }

    @When("user click kontrak on kost saya page")
    public void user_click_kontrak_on_kost_saya_page() throws InterruptedException {
        myKost.clickKontrakMenu();
    }

    @When("user click ajukan berhenti sewa on kontrak saya page")
    public void user_click_ajukan_berhenti_sewa_on_kos_saya_page () throws InterruptedException {
        myKost.clickAjukanBerhentiSewaText();
    }

    @Then("user see ajukan berhenti sewa button is disabled")
    public void user_see_edit_finished_button_is_disabled() {
        Assert.assertTrue(myKost.isAjukanBerhentiSewaButtonDisabled(), "ajukan berhenti sewa button is not disabled");
    }


    @Then("user see at review page contains:")
    public void user_see_at_review_page_contains(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        int i=0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(myKost.getAllReviewPage(i),content.get("Review Page"),"Review page should contain" + content.get("Review Page"));
            i++;
        }
    }

    @Then("there will be a Kost Review submitted with the title {string}")
    public void there_will_be_a_kost_review_submitted_with_the_title(String title){
        Assert.assertEquals(myKost.getTitleKostReviewSubmittedText().replaceAll("\n",""),title, "title is not correct");
    }

    @And("there will be a Kost Review submitted with the stars amount {string}")
    public void there_will_be_a_kost_review_submitted_with_the_stars_amount(String title){
        Assert.assertEquals(myKost.getStarsKostReviewSubmittedText().replaceAll("\n",""),title, "title is not correct");
    }

    @And("user click close button on Berhenti Sewa page")
    public void user_click_close_button_on_berhenti_sewa_page() throws InterruptedException {
        myKost.clickOnCloseButton();
    }

    @And("user click on Bayar di sini")
    public void user_click_on_Bayar_di_sini() throws InterruptedException {
        myKost.clickOnBayarDiSiniButton();
    }

}
