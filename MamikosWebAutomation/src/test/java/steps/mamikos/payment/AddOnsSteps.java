package steps.mamikos.payment;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.sl.In;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.payment.AddOnsPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class AddOnsSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AddOnsPO addOns = new AddOnsPO(driver);

    //Payment Properties
    private String payment="src/test/resources/testdata/mamikos/payment.properties";
    private String text50CharName = JavaHelpers.getPropertyValue(payment,"text50CharName");
    private String text50CharInputName = JavaHelpers.getPropertyValue(payment,"text50CharInputName");
    private String text50CharDes = JavaHelpers.getPropertyValue(payment,"text50CharDes");
    private String text50CharInputDes = JavaHelpers.getPropertyValue(payment,"text50CharInputDes");
    private String text300CharNote = JavaHelpers.getPropertyValue(payment,"text300CharNote");
    private String text50CharInputNote = JavaHelpers.getPropertyValue(payment,"text50CharInputNote");


    @When("user open menu add ons list")
    public void user_open_menu_add_ons_list() throws InterruptedException{
        addOns.clickOnAddOnsListMenu();
    }

    @When("user see Menu add ons list and there will some fields")
    public void user_see_Menu_add_ons_list_and_there_will_some_fields() throws InterruptedException{
        addOns.checkFieldsOnAddOnsList();
    }

    @And("user click button create add ons")
    public void user_click_button_create_add_ons() throws InterruptedException{
        addOns.clickOnCreateAddOnsButton();
    }

    @Then("there will popup error appear {string}")
    public void there_will_popup_error_appear_please_completed_madatory_field(String popUp) throws InterruptedException {
        Assert.assertEquals(addOns.getMessagePopUpMandatoryField(), popUp, "not match");
        addOns.clickOnClosePopUpButton();
    }

    @And("user input add ons description {string}")
    public void user_input_add_ons_description (String description) throws InterruptedException{
        addOns.enterAddOnsDescription(description);
    }

    @And("user input add ons price {string}")
    public void user_input_add_ons_price (String price) throws InterruptedException{
        addOns.enterAddOnsPrice(price);
    }

    @And("user input add ons name {string}")
    public void user_input_add_ons_name (String name) throws InterruptedException{
        addOns.enterAddOnsName(name);
    }

    @And("user click button edit on add ons list")
    public void user_click_button_edit_on_add_ons_list () throws InterruptedException{
        addOns.clickEditAddOnsButton();
    }

    @Then("user will direct to edit add ons form and there will some fields")
    public void user_will_direct_to_edit_add_ons_form_and_there_will_some_fields() throws InterruptedException {
        addOns.checkFieldsEditOnAddOnsForm();
    }

    @Given("user edit with new name in name field {string}")
    public void user_edit_with_new_name_in_name_field(String name) throws InterruptedException{
        addOns.enterAddOnsName(name);
    }

    @When("user click update add ons")
    public void user_click_update_add_ons() throws InterruptedException{
        addOns.clickUpdateAddOnsButton();
    }

    @When("user click yes on popup")
    public void user_click_yes_on_popup() throws InterruptedException{
        addOns.clickYesUpdateAddOnsButton();
    }

    @When("user click cancel on popup")
    public void user_click_cancel_on_popup() throws InterruptedException{
        addOns.clickCancelUpdateAddOnsButton();
    }

    @When("user click close on popup")
    public void user_click_close_on_popup() throws InterruptedException{
        addOns.clickCloseUpdateAddOnsButton();
    }

    @Then("user will direct to list add ons and showing alert success updated new add ons")
    public void user_will_direct_to_list_add_ons_and_showing_alert_success_updated_new_add_ons() throws InterruptedException{
        addOns.verifyUpdateAddOns();
    }

    @Given("user edit with new description in description field {string}")
    public void user_edit_with_new_description_in_description_field(String description) throws InterruptedException{
        addOns.enterAddOnsDescription(description);
    }

    @Given("user edit with new price in price field {string}")
    public void user_edit_with_new_price_in_price_field(String price) throws InterruptedException{
        addOns.enterAddOnsPrice(price);
    }

    @Given("user edit with new notes in notes field {string}")
    public void user_edit_with_new_notes_in_notes_field(String notes) throws InterruptedException{
        addOns.enterAddOnsNotes(notes);
    }

    @And("user click button cancel on edit add ons page")
    public void user_click_button_cancel_on_edit_add_ons_page( ) throws InterruptedException{
        addOns.clickCancelAddOnsButton();
    }


    @Given("user can't input add ons name more than 50 char")
    public void userCanTInputAddOnsNameMoreThanChar() throws InterruptedException{
        addOns.enterAddOnsName(text50CharName);
        Assert.assertEquals(addOns.getAddOnsName(),text50CharInputName,"don't match");
    }

    @When("user can't input add ons description more than 50 char")
    public void userCanTInputAddOnsDescriptionMoreThanChar() throws InterruptedException{
        addOns.enterAddOnsDescription(text50CharDes);
        Assert.assertEquals(addOns.getAddOnsDescription(),text50CharInputDes,"don't match");
    }

    @Then("user can't input add ons notes more than 300 char")
    public void userCanTInputAddOnsNotesMoreThanChar() throws InterruptedException{
        addOns.enterAddOnsNotes(text300CharNote);
        Assert.assertEquals(addOns.getAddOnsNotes(),text50CharInputNote,"don't match");
    }

    @And("user click button delete on add ons list")
    public void user_click_button_delete_on_add_ons_list () throws InterruptedException{
        addOns.clickDeleteAddOnsButton();
    }

    @Then("system display popup delete add ons")
    public void systemDisplayPopupDeleteAddOns() throws InterruptedException{
        addOns.showDeleteAddOnsPopup();
    }

    @And("system display success delete add ons")
    public void systemDisplaySuccessDeleteAddOns() throws InterruptedException{
        addOns.deleteAndSuccessDelete();
    }

    @And("user input note for add ons {string}")
    public void user_input_note_for_add_ons (String inputNote) throws InterruptedException {
        addOns.userInputNote(inputNote);
    }

    @Then("user click yes on the popup create addons")
    public void user_click_yes_on_the_popup_create_addons () throws InterruptedException {
        addOns.clickYesToCreate ();
    }
}
