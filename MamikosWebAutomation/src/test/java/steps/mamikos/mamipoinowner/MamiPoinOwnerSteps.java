package steps.mamikos.mamipoinowner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.AssertJUnit;
import pageobjects.mamikos.mamipoinowner.MamiPoinOwnerPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class MamiPoinOwnerSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private MamiPoinOwnerPO mamipoinOwner = new MamiPoinOwnerPO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    @And("user click MamiPoin widget")
    public void user_click_MamiPoin_widget() throws InterruptedException
    {
        mamipoinOwner.clickOnMamiPoinWidget();
        selenium.hardWait(3);
    }

    @And("user verify MamiPoin widget is displayed")
    public void user_verify_MamiPoin_widget_is_displayed() throws InterruptedException
        {
            Assert.assertTrue(mamipoinOwner.verifyMamiPoinWidgetDisplayed(), "MamiPoin widget is not displayed");
            selenium.hardWait(3);
        }

    @And("user verify Pelajari text is displayed")
    public void user_verify_Pelajari_text_is_displayed() throws InterruptedException
        {
            Assert.assertTrue(mamipoinOwner.verifyPelajariText(), "Pelajari text is not displayed");
            selenium.hardWait(3);
        }

    @And("user verify point is 0")
    public void user_verify_point_is_0() throws InterruptedException
        {
            Assert.assertTrue(mamipoinOwner.verifyPointIs0(), "Point displayed is not 0");
            selenium.hardWait(3);
        }

    @And("user verify Pelajari text is not displayed")
        public void
        user_verify_Pelajari_text_is_not_displayed() throws InterruptedException
    {
        Assert.assertFalse(mamipoinOwner.verifyPelajariText(), "Pelajari text is displayed");
        selenium.hardWait(3);
    }

    @And("user verify Tukar Poin text is displayed")
    public void user_verify_Tukar_Poin_text_is_displayed() throws InterruptedException
    {
        Assert.assertTrue(mamipoinOwner.verifyTukarPoinText(), "Tukar Poin text is not displayed");
        selenium.hardWait(3);
    }

    @And("user verify point is >= 1")
    public void user_verify_point_is_not_0() throws InterruptedException
    {
        Assert.assertTrue(mamipoinOwner.verifyPointIsNot0(), "Point displayed is incorrect");
        selenium.hardWait(3);
    }

    @And("user verify Tukar Poin text is not displayed")
    public void
    user_verify_Tukar_Poin_text_is_not_displayed() throws InterruptedException
    {
        Assert.assertFalse(mamipoinOwner.verifyTukarPoinText(), "Tukar Poin text is displayed");
        selenium.hardWait(3);
    }

    @Then("user verify MamiPoin onboarding is appear")
    public void user_verify_MamiPoin_onboarding_is_appear() throws InterruptedException {
        //Mamipoin
        Assert.assertEquals(mamipoinOwner.getMamiPoinLandingPageOnboardingText(),"Ini adalah jumlah poin Anda saat ini yang dapat Anda tukarkan dengan berbagai hadiah.");
        mamipoinOwner.clickOnNextButton();
        selenium.hardWait(2);

        //Riwayat Hadiah
        Assert.assertEquals(mamipoinOwner.getRewardHistoryOnboardingText(),"Cek seluruh hadiah yang telah Anda tukarkan dengan poin Anda di sini.");
        mamipoinOwner.clickOnNextButton();
        selenium.hardWait(2);

        //Riwayat Poin
        Assert.assertEquals(mamipoinOwner.getPointHistoryOnboardingText(),"Semua poin yang didapat dan aktivitas yang telah dilakukan tercatat di sini.");
        mamipoinOwner.clickOnNextButton();
        selenium.hardWait(2);

        //Syarat dan Ketentuan
        Assert.assertEquals(mamipoinOwner.getTermAndConditionOnboardingText(),"Pelajari cara-cara untuk mendapatkan poin di bagian ini.");
        mamipoinOwner.clickOnNextButton();
        selenium.hardWait(2);

        //Tukar Poin
        Assert.assertEquals(mamipoinOwner.getRedeemPointOnboardingText(),"Anda dapat menukar poin Anda dengan hadiah di bagian ini.");
        mamipoinOwner.clickOnFinishButton();
        selenium.hardWait(2);
    }

    @Then("user verify tukar poin onboarding is appear")
    public void user_verify_tukar_poin_onboarding_is_appear() throws InterruptedException {
        //Poin Anda
        Assert.assertEquals(mamipoinOwner.getPoinAndaOnboardingText(),"Pastikan poin Anda cukup untuk ditukarkan dengan hadiah yang Anda inginkan.");
        mamipoinOwner.clickOnNextButton();
        selenium.hardWait(2);

        //Hadiah Bisa Ditukar
        Assert.assertEquals(mamipoinOwner.getHadiahBisaDitukarOnboardingText(),"Anda dapat menukar poin Anda sesuai dengan jumlah yang dibutuhkan hadiah terkait.");
        mamipoinOwner.clickOnNextButton();
        selenium.hardWait(2);

        //Bantuan
        Assert.assertEquals(mamipoinOwner.getBantuanOnboardingText(),"Tekan tombol Bantuan untuk kembali mempelajari cara penukaran poin.");
        mamipoinOwner.clickOnSelesaiButton();
        selenium.hardWait(2);
    }

    @When("user verify title in the mamipoin owner landing page is displayed")
    public void user_verify_title_in_the_mamipoin_tenant_landing_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinOwner.isTitleInTheMamipoinOwnerLandingPageDisplayed());
    }

    @And("user verify tukar poin button is displayed")
    public void user_verify_tukar_poin_button_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinOwner.isTukarPoinButtonDisplayed());
    }

    @When("user clicks on tukar poin button")
    public void user_clicks_on_tukar_poin_button() throws InterruptedException {
        mamipoinOwner.clickOnTukarPoinButton();
        selenium.hardWait(3);
    }

    @And("user verify riwayat hadiah button is displayed")
    public void user_verify_riwayat_hadiah_button_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinOwner.isRiwayatHadiahButtonDisplayed());
    }

    @And("user verify riwayat poin owner button is displayed")
    public void user_verify_riwayat_poin_button_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinOwner.isRiwayatPoinButtonDisplayed());
    }

    @And("user verify syarat dan ketentuan button is displayed")
    public void user_verify_syarat_dan_ketentuan_button_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinOwner.isSyaratDanKetentuanButtonDisplayed());
    }

    @And("user verify title in the tukar poin page is displayed")
    public void user_verify_title_in_the_tukar_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinOwner.isTitleInTheTukarPoinPageDisplayed());
    }

    @And("user verify logo in the tukar poin page is displayed")
    public void user_verify_logo_in_the_tukar_poin_page_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinOwner.isLogoInTheTukarPoinPageDisplayed());
    }

    @And("user verify the amount of MamiPoin Anda is {string}")
    public void user_verify_the_amount_of_MamiPoin_Anda_is(String amountOfMamipoinAnda) {
        Assert.assertEquals(mamipoinOwner.verifyAmountOfMamipoinAnda(amountOfMamipoinAnda),"123.456 Poin");

    }

    @And("user verify bantuan button is displayed")
    public void user_verify_bantuan_button_is_displayed() throws InterruptedException {
        Assert.assertTrue(mamipoinOwner.isBantuanButtonDisplayed());
    }

    @Then("user click tab Riwayat Poin")
    public void user_click_tab_Riwayat_Poin() throws InterruptedException {
       mamipoinOwner.clickOnTabRiwayatPoin();
    }

    @Then("user see poin deduction history is displayed on MamiPoin History")
    public void user_see_poin_deduction_history_is_displayed_on_MamiPoin_History() throws InterruptedException {
       Assert.assertTrue(mamipoinOwner.isHistoryDeductionDisplayed());
    }

    @Then("user see poin return history is received on Mamipoin History")
    public void user_see_poin_return_history_is_received_on_Mamipoin_History() throws InterruptedException {
       Assert.assertTrue(mamipoinOwner.isHistoryAdditionDisplayed());
    }

    @When("user verify point is > {int}")
    public void user_verify_point_is(Integer discountMamipoinOwner) throws InterruptedException {
        selenium.hardWait(3);
        Assert.assertTrue(mamipoinOwner.getMamipoinOwnerText() > discountMamipoinOwner);
    }

    @Then("user back to owner dashboard from mamipoin")
    public void user_back_to_owner_dashboard_from_mamipoin() throws InterruptedException {
        mamipoinOwner.clicOnIconBackMamipoin();
    }

    @Then("user verify point owner is < {int}")
    public void user_verify_point_owner_is(Integer discountMamipoinOwner) throws InterruptedException {
        selenium.hardWait(3);
        Assert.assertTrue(mamipoinOwner.getMamipoinOwnerLessThanText() < discountMamipoinOwner);
    }

}

