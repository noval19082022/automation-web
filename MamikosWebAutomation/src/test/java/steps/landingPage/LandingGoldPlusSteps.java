package steps.landingPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import io.cucumber.java.en.Then;
import pageobjects.mamikos.landingpage.GoldPlusPO;
import utilities.Constants;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.util.List;
import java.util.Map;

public class LandingGoldPlusSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private GoldPlusPO goldPlus = new GoldPlusPO(driver);
    private SeleniumHelpers selenium;


    @Then("user verify the appearance of the GoldPlus Banner")
    public void user_verify_the_appearance_of_the_GoldPlus_Banner() {
        Assert.assertTrue(goldPlus.isGoldPlusBannerAppear(), "GoldPlus Banner isn't appear");
    }

    @And("user click on Learn More button")
    public void user_click_on_Learn_More_button() throws InterruptedException {
        goldPlus.clickOnTheLearnMoreButton();
    }

    @When("user click Daftar GoldPlus field")
    public void user_click_daftar_gold_plus_field() throws InterruptedException {
        goldPlus.clickOnRegisterGoldPlus();
    }

    @And("user redirect to List Package")
    public void user_redirect_to_list_package() throws InterruptedException {
        Assert.assertTrue(goldPlus.pageListGoldPlusAppear(), "Page GoldPlus isn't appear");
    }

    @And("user click pilih paket on {string}")
    public void user_click_pilih_paket_on_gold_plus(String gp) throws InterruptedException {
        if (gp.equalsIgnoreCase("GoldPlus 1")) {
            goldPlus.choosePaketGoldPlus(1);
        } else if (gp.equalsIgnoreCase("GoldPlus 2")) {
            goldPlus.choosePaketGoldPlus(0);
        }
    }

    @And("user click lihat detail manfaat GP1")
    public void user_click_lihat_detail_manfaat_GP1() throws InterruptedException {
        goldPlus.lihatDetailLainnyaGP1();
    }


    @Then("user redirect to Detail Tagihan")
    public void user_redirect_to_detail_tagihan() throws InterruptedException {
        Assert.assertTrue(goldPlus.pageDetailTagihanGoldPlusAppear(), "Page Detail Tagihan GoldPlus isn't appear");
    }

    @And("user see total tagihan is {string}")
    public void user_see_total_tagihan_is(String totalTagihan) throws InterruptedException {
        Assert.assertEquals(goldPlus.getGoldplus1Price(), totalTagihan, "Gold Plus 1 price is not correct");
    }

    @And("user click on ubah package gold plus button")
    public void user_click_on_ubah_package_gold_plus_button() throws InterruptedException {
        goldPlus.clickOnUbahGoldPlus();
    }

    @And("user uncheck checkbox Syarat dan Ketentuan")
    public void user_uncheck_check_box_syarat_dan_ketentuan() throws InterruptedException {
        goldPlus.clickOnUncheckCheckBox();
    }

    @And("user check checkbox Syarat dan Ketentuan")
    public void user_check_check_box_syarat_dan_ketentuan() throws InterruptedException {
        goldPlus.clickOnUncheckCheckBox();
    }

    @And("user will get toast message {string}")
    public void user_will_get_toast_message(String messageToast) {
        Assert.assertTrue(goldPlus.isWarningToastAppear(), "warning toast isn't appear");
        Assert.assertEquals(goldPlus.getTextToast(), messageToast, "Warning message doesn't match");
    }

    @And("system display button Bayar Sekarang is disable")
    public void system_display_button_bayar_ssekarang_is_disable() {
        Assert.assertTrue(goldPlus.isPayNowButtonDisable(), "button pay now is enabled");
    }

    @And("user click \"Pelajari caranya\" in GP registration page")
    public void user_click_pelajari_caranya_in_gp_registration_page() throws InterruptedException {
        goldPlus.clickOnPelajariCaranya();
    }

    @And("user click {string} in Panduan fitur GP")
    public void user_click_in_gp_panduan_fitur_gp(String panduan) throws InterruptedException {
        goldPlus.clickOnPanduanFitur(panduan);
    }


    @And("user see e-card to know how-to-use is as expected")
    public void user_see_ecard_to_know_how_to_use_goldplus_guide(DataTable dataTable) throws InterruptedException {
        List<Map<String, String>> table = dataTable.asMaps();
        for (Map<String, String> content : table) {
            Assert.assertEquals(goldPlus.goldplusGuideDetailHowToUse("title"), content.get("title"));
            Assert.assertEquals(goldPlus.goldplusGuideDetailHowToUse("desc"), content.get("desc"));
            Assert.assertEquals(goldPlus.goldplusGuideDetailHowToUse("stepCard"), content.get("title"));
            goldPlus.clickSwiperButtonGuideGp();
        }
    }

    @And("user click check manfaat lainnya {string}")
    public void user_click_check_manfaat_lainnya(String gp) throws InterruptedException {
        if (gp.equalsIgnoreCase("GoldPlus 1")) {
            goldPlus.clickOnCheckOtherBenefits(0);
        } else if (gp.equalsIgnoreCase("GoldPlus 2")) {
            goldPlus.clickOnCheckOtherBenefits(1);
        }
    }

    @Then("system redirect to page manfaat lainnya {string}")
    public void system_redirect_to_page_manfaat_lainnya(String textHeader) {
        Assert.assertEquals(goldPlus.getTextHeader(), textHeader, "Text doesn't match");
    }

    @And("system display button pilih paket goldplus")
    public void system_display_button_pilih_paket_goldplus() {
        Assert.assertTrue(goldPlus.isChooseGpButtonAppear(), "button choose gp isn't appear");
    }

    @Then("system showing popup detail manfaat GP1")
    public void system_showing_popup_detail_manfaat_GP1() {
        Assert.assertTrue(goldPlus.popupManfaatGP1(), "popup manfaat GP1 isn't showing");
    }


    @Then("user see Syarat dan Ketentuan goldplus {string}")
    public void user_see_syarat_dan_ketentuan_goldplus(String syaratKetentuan) {
        Assert.assertEquals(goldPlus.getTextSyaratDanKetentuan(), syaratKetentuan, "Text doesn't match");
    }

    @And("system display button Bayar Sekarang is enable")
    public void system_display_button_bayar_ssekarang_is_enable() {
        Assert.assertTrue(goldPlus.isPayNowButtonEnable(), "button pay now is disabled");
    }

    @And("user see list \"Panduan Fitur di Goldplus\" is as expected")
    public void user_see_list_panduan_fitur_di_goldplus_is_as_expected(DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        int i = 0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(goldPlus.listPanduanFiturGoldPlus("title", i), content.get("title"));
            Assert.assertEquals(goldPlus.listPanduanFiturGoldPlus("content", i), content.get("content"));
            i++;
        }
    }

    @Given("user click mamikos goldplus label")
    public void user_click_mamikos_goldplus_label() throws InterruptedException {
        goldPlus.clickOnMamikosGPLabel();
    }

    @When("user click Lihat Selengkapnya kos goldplus anda")
    public void user_click_lihat_selengkapnya_kos_goldplus_anda() throws InterruptedException {
        goldPlus.clickOnSeeDetailGoldPlusAnda();
    }

    @Then("user see page title is {string} on paket goldplus Page")
    public void user_see_page_title_on_paket_goldplus_page(String title) {
        Assert.assertEquals(goldPlus.getPagePaketGoldPlusTitle(), title, "title is not match");
    }

    @And("user see status goldplus is {string} on tab semua paket gold plus anda")
    public void user_see_on_tab_semua_paket_gold_plus_anda(String statusGP) {
        Assert.assertEquals(goldPlus.getStatusPaketGoldPlus(), statusGP, "status paket goldplus is not match");
    }

    @And("user click button ajukan ganti paket")
    public void user_click_button_ajukan_ganti_paket() throws InterruptedException {
        goldPlus.clickOnChangePaketGoldPlus();
    }

    @Then("user see text {string} on popup ajukan ganti paket goldplus")
    public void user_see_text_on_popup_ajukan_ganti_paket_goldplus(String textPopup) {
        Assert.assertEquals(goldPlus.getTextChangePaketGoldPlus(), textPopup, "Text ajukan ganti paket goldplus on popup is not match");
    }

    @When("user click tab {string} on page paket goldplus anda")
    public void user_click_tab_on_page_paket_goldplus_anda(String typeFilter) throws InterruptedException {
        goldPlus.clickOnTypeFilter(typeFilter);
    }

    @Then("user see status goldplus is {string} on tab sedang diproses paket gold plus anda")
    public void user_see_status_goldplus_on_tab_sedang_diproses(String statusGP) {
        Assert.assertEquals(goldPlus.getStatusMenungguPembayaranGP(), statusGP, "status paket goldplus is not match");
    }

    @Then("user see text {string} on page goldplus anda")
    public void user_see_text_on_page_goldplus_anda(String textTotalGP) throws InterruptedException {
        Assert.assertEquals(goldPlus.getTextTotalPropertyGoldPlus(), textTotalGP, "text total property terdaftar is not match");
    }

    @And("user click button bayar sekarang")
    public void user_click_button_bayar_sekarang() throws InterruptedException {
        goldPlus.clickOnBayarSekarangButton();
    }

    @And("user terminate active goldplus")
    public void use_terminate_active_goldplus() throws InterruptedException {
        goldPlus.clickOnActionButton();
        if (goldPlus.isNoTerminateContract()) {
            goldPlus.clickOnTerminateContract();
            goldPlus.clickOnYesButton();
        }else {
            goldPlus.navigateToPage();
        }
    }

    @Then("system display success terminate contract {string}")
    public void system_display_success_terminate_contract(String successText) {
        Assert.assertEquals(goldPlus.getTextSuccessTerminateContractGP(), successText, "text success terminate contract gp is not match");
    }

    @And("user choose saldo {string} on GoldPlus section")
    public void user_choose_saldo_on_goldplus_section(String saldo) throws InterruptedException {
      goldPlus.chooseSaldo(saldo);
    }

    @And("user view detail list saldo MamiAds")
    public void user_view_detail_list_saldo_mamiads( DataTable dataTable) {
        List<Map<String, String>> table = dataTable.asMaps();
        int i = 0;
        int j = 0;
        for (Map<String, String> content : table) {
            Assert.assertEquals(goldPlus.mamiadsSaldo("saldo", i), content.get("saldo"));
            Assert.assertEquals(goldPlus.mamiadsSaldo("cashback", i), content.get("cashback"));
            Assert.assertEquals(goldPlus.mamiadsSaldo("salePrice", i), content.get("salePrice"));
            Assert.assertEquals(goldPlus.mamiadsSaldo("saving", i), content.get("saving"));
            try {
                if (!content.get("disc").isEmpty()) {
                    Assert.assertEquals(goldPlus.mamiadsSaldo("disc", j), content.get("disc"));
                    Assert.assertEquals(goldPlus.mamiadsSaldo("discPriceMamiAds", j), content.get("discPriceMamiAds"));
                    j++;
                }
            } catch (java.lang.NullPointerException ignored) {
            }
            i++;
        }
    }

    @Then("user verify the {string} and the price is {string} already {string} on Rincian Pembayaran")
    public void user_verify_the_and_the_price_is_already_on_rincian_pembayaran(String rincian, String saldo, String validation) {
        switch (validation){
            case "choosen":
                Assert.assertEquals(goldPlus.getTextRinicianMamiAds(), rincian, "rincian MamiAds  is not match");
                Assert.assertEquals(goldPlus.getTextSaldoMamiAds(), saldo, "saldo MamiAds  is not match");
            break;
            case "removed":
                Assert.assertFalse(goldPlus.isRincianNotVisible(), "rincian Mamiads doesn't removed");
                Assert.assertFalse(goldPlus.isSaldoNotVisible(), "saldo MamiAds doesn't removed");

            break;
        }
    }

    @And("user verify the {string} is selected")
    public void userVerifyTheIsSelected(String gpPackage) {
        Assert.assertEquals(goldPlus.getGpPackage(), gpPackage, "GP package selected doesn't match!");
    }

    @Then("user verify text {string} on section pilihan anda is appear")
    public void user_verify_text_on_section_pilihan_anda_is_appear(String textPilihanAnda) throws InterruptedException {
        Assert.assertEquals(goldPlus.getPilihanAndaTitle(), textPilihanAnda, "Text doesn't match!");
    }

    @Then("user verify text {string} on section rincian pembayaran is appear")
    public void user_verify_text_on_section_rincian_pembayaran_is_appear(String textRincianBayar) throws InterruptedException {
        Assert.assertEquals(goldPlus.getRincianPembayaranText(), textRincianBayar, "Text doesn't match!");
    }

    @Then("user see text {string} at detail tagihan GP")
    public void user_see_text_at_detail_tagihan_GP(String textSyaratGP) {
       Assert.assertEquals(goldPlus.getSyaratKetentuanGPText(), textSyaratGP, "Text doesn't match!");
    }

    @Then("user see {string} button")
    public void user_see_button(String string) {
        Assert.assertTrue(goldPlus.isBayarSekarangButtonAppear(), "Button bayar sekarang doesn't appear");
    }

    @Then("user click Lihat selengkapnya at section pembayaran tagihan")
    public void user_click_Lihat_selengkapnya_at_section_pembayaran_tagihan() throws InterruptedException {
        goldPlus.clickLihatSelengkapnyaRincianPembayaran();
    }

    @Then("user click tab selesai")
    public void user_click_tab_selesai() throws InterruptedException {
        goldPlus.clickTabSelesai();
    }

    @Then("user see list detail tagihan already paid")
    public void user_see_list_detail_tagihan_already_paid() {
        Assert.assertTrue(goldPlus.isTablePembayaranPresent(), "Table not present!");
    }

    @Then("user see box black with text {string}")
    public void user_see_box_black_with_text(String textUpgradeGp2) {
        Assert.assertEquals(goldPlus.getTextUpgradeGP2(), textUpgradeGp2, "Text  is not match");
    }

    @Then("user click button {string} at box black")
    public void user_click_button_at_box_black(String string) throws InterruptedException {
        goldPlus.clickUpgradeGp2Black();
    }

    @Then("user click back detail tagihan")
    public void user_click_back_detail_tagihan() throws InterruptedException {
        goldPlus.clickBackDetailTagihan();
    }

    @Then("user click lihat selengkapnya at section property terdaftar")
    public void user_click_lihat_selengkapnya_at_section_property_terdaftar() throws InterruptedException {
        goldPlus.clickLihatSelengkapnyaProperty();
    }

    @Then("user click button close pop up upgrade goldplus dua")
    public void user_click_button_close_pop_up_upgrade_goldplus_dua() throws InterruptedException {
        goldPlus.clickIconCloseUpgradeGP();
    }

    @Then("user click button {string} at section goldplus list")
    public void user_click_button_at_section_goldplus_list(String string) throws InterruptedException {
        goldPlus.clickUpgradeGp2Black();
    }

    @Then("user see pop up upgrage goldplus dua")
    public void user_see_pop_up_upgrage_goldplus_dua() {
        Assert.assertTrue(goldPlus.isPopUpUpgradeGPPresent(),"pop up not present!");
    }

    @Then("user click button upgrade goldlplus dua at pop up")
    public void user_click_button_upgrade_goldlplus_dua_at_pop_up() throws InterruptedException {
        goldPlus.clickUpgradeGp2PopUP();
    }

    @Then("user click widget GP {string}")
    public void user_click_widget_GP(String string) throws InterruptedException {
        goldPlus.clickOnWaitPaymentGoldPlus();
    }

    @Then("user click back at GP page")
    public void user_click_back_at_GP_page() throws InterruptedException {
        goldPlus.clickIconBackGP();

    }

    @Then("user see text {string} at page detail tagihan GP")
    public void user_see_text_at_page_detail_tagihan_GP(String gpText) {
        Assert.assertEquals(goldPlus.getTextGoldplusPackage(), gpText, "package GP doesnt match!");

    }
    @Then("user see text description {string} at page detail tagihan GP")
    public void user_see_text_description_at_page_detail_tagihan_GP(String gpDesc) {
        Assert.assertEquals(goldPlus.getTextGoldplusPackageDesc(), gpDesc, "description package doesnt match!");
    }

    @Then("user see button Bayar Sekarang")
    public void user_see_button_Bayar_Sekarang() {
        Assert.assertTrue(goldPlus.isButtonBayarSekarangPresent(), "Button doesnt appear!");
    }

    @Then("user validate transaction {string} after success redirect")
    public void user_validate_transaction_after_success_redirect(String product) throws InterruptedException {
        Assert.assertEquals(goldPlus.getPembayaranTextGoldplus("Jenis Pembayaran"),"Paket "+ product);
        Assert.assertTrue(goldPlus.getPembayaranTextGoldplus("No. Invoice").contains("GP"));

    }

    @When("user click text {string} at goldplus page")
    public void user_click_text_at_goldplus_page(String string) throws InterruptedException {
        goldPlus.clickOnTextSyarat();
    }

    @Then("user see pop up syarat dan ketentuan umum")
    public void user_see_pop_up_syarat_dan_ketentuan_umum() {
        Assert.assertTrue(goldPlus.isPopUpSyaratPresent(), "Pop Up doesnt appear!");
    }

    @When("user click icon close at pop up syarat ketentuan umum")
    public void user_click_icon_close_at_pop_up_syarat_ketentuan_umum() throws InterruptedException {
       goldPlus.clickOnIconCLosePopUp();
    }

    @When("user click button saya mengerti at syarat ketentuan umum")
    public void user_click_button_saya_mengerti_at_syarat_ketentuan_umum() throws InterruptedException {
       goldPlus.clickOnButtonSayaMengerti();
    }

}