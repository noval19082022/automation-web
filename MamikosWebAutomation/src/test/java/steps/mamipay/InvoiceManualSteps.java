package steps.mamipay;

import io.cucumber.java.en.*;
import io.cucumber.java.et.Ja;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamipay.CreateManualInvoicePO;
import pageobjects.mamipay.InvoiceManualPO;
import pageobjects.mamipay.UbahStatusInvoicePO;
import utilities.Constants;
import dataobjects.Invoice;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class InvoiceManualSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private InvoiceManualPO manualInvoice = new InvoiceManualPO(driver);
    private CreateManualInvoicePO createManualInvoice = new CreateManualInvoicePO(driver);
    private UbahStatusInvoicePO ubahStatusInvoice = new UbahStatusInvoicePO(driver);

    //test data
    private String invoiceManual ="src/test/resources/testdata/mamipay/invoiceManual.properties";
    private String invoiceManualPageURL = JavaHelpers.getPropertyValue(invoiceManual, "invoiceManualURL_" + Constants.ENV);
    private String buatInvoiceManualPageURL = JavaHelpers.getPropertyValue(invoiceManual, "buatInvoiceManualURL_" + Constants.ENV);
    private String listingNamePMAN = JavaHelpers.getPropertyValue(invoiceManual, "listingName_" + Constants.ENV);
    private String tenantNamePMAN = JavaHelpers.getPropertyValue(invoiceManual, "tenantName_" + Constants.ENV);
    private String tenantNoHPPMAN = JavaHelpers.getPropertyValue(invoiceManual, "tenantNoHP_" + Constants.ENV);
    private String tenantNoKamarPMAN = JavaHelpers.getPropertyValue(invoiceManual, "tenantNoKamar_" + Constants.ENV);
    private String biayaTambahanToast = JavaHelpers.getPropertyValue(invoiceManual, "biayaTambahanToast_" + Constants.ENV);
    private String biayaSewaToast = JavaHelpers.getPropertyValue(invoiceManual, "biayaSewaToast_" + Constants.ENV);
    private String ubahBiayaToast = JavaHelpers.getPropertyValue(invoiceManual, "ubahBiayaToast_" + Constants.ENV);
    private String ubahStatusInvoiceToast = JavaHelpers.getPropertyValue(invoiceManual, "ubahStatusInvoiceToast_" + Constants.ENV);
    private String char256 = JavaHelpers.getPropertyValue(invoiceManual, "char256_" + Constants.ENV);
    private String char255 = JavaHelpers.getPropertyValue(invoiceManual, "char255_" + Constants.ENV);
    private String popUpTitleConfirmation = JavaHelpers.getPropertyValue(invoiceManual, "changeInvPopupTitle_" + Constants.ENV);
    private String popUpSubtitleConfirmation = JavaHelpers.getPropertyValue(invoiceManual, "changeInvPopupSubtitle_" + Constants.ENV);
    private String buatNKirimTitle = JavaHelpers.getPropertyValue(invoiceManual, "title_" + Constants.ENV);
    private String buatNKirimSubtitle = JavaHelpers.getPropertyValue(invoiceManual, "subtitle_" + Constants.ENV);
    private String invoiceManualToast = JavaHelpers.getPropertyValue(invoiceManual, "invoiceManualToast_" + Constants.ENV);
    private String deleteBiayaTambahanTitle = JavaHelpers.getPropertyValue(invoiceManual, "deleteTambahanTitle_" + Constants.ENV);
    private String deleteBiayaTambahanSubtitle = JavaHelpers.getPropertyValue(invoiceManual, "deleteTambahanSubtitle_" + Constants.ENV);
    private String deleteBiayaSewaTitle = JavaHelpers.getPropertyValue(invoiceManual, "deleteSewaTitle_" + Constants.ENV);
    private String deleteBiayaSewaSubtitle = JavaHelpers.getPropertyValue(invoiceManual, "deleteSewaSubtitle_" + Constants.ENV);
    private String deleteBiayaToast = JavaHelpers.getPropertyValue(invoiceManual, "deleteBiayaToast_" + Constants.ENV);
    private String emptyStateBiayaTambahan = JavaHelpers.getPropertyValue(invoiceManual, "emptyStateBiayaTambahan_" + Constants.ENV);
    private String emptyStateBiayaSewa = JavaHelpers.getPropertyValue(invoiceManual, "emptyStateBiayaSewa_" + Constants.ENV);
    private String titleFilterInvManual = JavaHelpers.getPropertyValue(invoiceManual, "titleFilterInvManual_" + Constants.ENV);
    private String subtitleFilterInvManual = JavaHelpers.getPropertyValue(invoiceManual, "subtitleFilterInvManual_" + Constants.ENV);
    private String statusInvTitle = JavaHelpers.getPropertyValue(invoiceManual, "statusInvTitle_" + Constants.ENV);
    private String statusInvPlaceHolder = JavaHelpers.getPropertyValue(invoiceManual, "statusInvPlaceHolder_" + Constants.ENV);
    private String jenisBiayaTitle = JavaHelpers.getPropertyValue(invoiceManual, "jenisBiayaTitle_" + Constants.ENV);
    private String jenisBiayaPlaceHolder = JavaHelpers.getPropertyValue(invoiceManual, "jenisBiayaPlaceHolder_" + Constants.ENV);
    private String invCreatedTitle = JavaHelpers.getPropertyValue(invoiceManual, "invCreatedTitle_" + Constants.ENV);
    private String tglMulaiTitle = JavaHelpers.getPropertyValue(invoiceManual, "tglMulaiTitle_" + Constants.ENV);
    private String tglAkhirTitle = JavaHelpers.getPropertyValue(invoiceManual, "tglAkhirTitle_" + Constants.ENV);

    //invoice manual list
    @Then("invoice manual page should be open")
    public void invoice_manual_page_should_be_open() throws InterruptedException {
        Assert.assertEquals(manualInvoice.getURL(),invoiceManualPageURL);
        Assert.assertEquals(manualInvoice.getInvoicePageTitle(),"Invoice Manual");
    }

    @When("user click buat invoice")
    public void user_click_buat_invoice() throws InterruptedException {
        manualInvoice.clickBuatInvoice();
    }

    @Then("user redirect to Buat Invoice manual page")
    public void user_redirect_to_Buat_Invoice_manual_page() throws InterruptedException {
        Assert.assertEquals(manualInvoice.getURL(),buatInvoiceManualPageURL);
        Assert.assertEquals(createManualInvoice.getBuatInvoicePageTitle(),"Buat Invoice");
    }

    @When("user select jenis invoice {string}")
    public void user_select_jenis_invoice(String jenisInvoice) throws InterruptedException {
        manualInvoice.chooseJenisInvoice(jenisInvoice);
    }
    //end of invoice manual list

    //create invoice manual
    @When("user select listing {string}")
    public void user_select_listing(String listingName) throws InterruptedException {
        switch (listingName){
            case "PMAN" :
                createManualInvoice.setListingName(listingNamePMAN);
                break;
            default:
                createManualInvoice.setListingName(listingName);
        }
    }

    @When("user select nama penyewa {string}")
    public void user_select_nama_penyewa(String tenantName) throws InterruptedException {
        switch (tenantName){
            case "PMAN" :
                createManualInvoice.setTenantName(tenantNamePMAN);
                break;
            default:
                createManualInvoice.setTenantName(tenantName);
        }
    }

    @Then("no HP penyewa should be autofill {string}")
    public void no_HP_penyewa_should_be_autofill(String noHP) throws InterruptedException {
        switch (noHP){
            case "PMAN" :
                Assert.assertEquals(createManualInvoice.getTenantNoHP(),tenantNoHPPMAN);
                break;
            default:
                Assert.assertEquals(createManualInvoice.getTenantNoHP(),noHP);
        }
    }

    @Then("no HP penyewa field is disabled")
    public void no_HP_penyewa_field_is_disabled() throws InterruptedException {
        Assert.assertTrue(createManualInvoice.isNoHPPenyewaDisabled());
    }

    @Then("no kamar should be auto fill {string}")
    public void no_kamar_should_be_auto_fill(String noKamar) throws InterruptedException {
        switch (noKamar){
            case "PMAN" :
                Assert.assertEquals(createManualInvoice.getTenantNoKamar(),tenantNoKamarPMAN);
                break;
            default:
                Assert.assertEquals(createManualInvoice.getTenantNoKamar(),noKamar);
        }
    }

    @Then("no kamar field is disabled")
    public void no_kamar_field_is_disabled() throws InterruptedException {
        Assert.assertTrue(createManualInvoice.isNoKamarPenyewaDisabled());
    }

    @When("click buat invoice back button")
    public void click_buat_invoice_back_button() throws InterruptedException {
        createManualInvoice.clickBackButton();
    }

    @Then("exit buat invoice confirmation pop up should be appear")
    public void exit_buat_invoice_confirmation_pop_up_should_be_appear() throws InterruptedException {
        Assert.assertTrue(createManualInvoice.isExitConfirmationPopUpAppear());
    }

    @Then("confirmation pop up title is {string}")
    public void confirmation_pop_up_title_is(String title) throws InterruptedException {
        Assert.assertEquals(createManualInvoice.getExitConfirmationPopUpTitile(),title);
    }

    @Then("confirmation pop up description is {string}")
    public void confirmation_pop_up_description_is(String desc) throws InterruptedException {
        Assert.assertEquals(createManualInvoice.getExitConfirmationPopUpDesc(),desc);
    }

    @Then("have button {string} and {string}")
    public void have_button_and(String greenButton, String whiteButton) throws InterruptedException {
        Assert.assertEquals(createManualInvoice.getGreenButtonConfirmation(),greenButton);
        Assert.assertEquals(createManualInvoice.getWhiteButtonConfirmation(),whiteButton);
    }

    @When("user choose {string} in exit buat invoice confirmation pop up")
    public void user_choose_in_exit_buat_invoice_confirmation_pop_up(String action) throws InterruptedException {
        createManualInvoice.clickConfirmationPopUp(action);
    }

    @Then("user stay in buat invoice manual page")
    public void user_stay_in_buat_invoice_manual_page() throws InterruptedException {
        Assert.assertEquals(manualInvoice.getURL(),buatInvoiceManualPageURL);
        Assert.assertEquals(createManualInvoice.getBuatInvoicePageTitle(),"Buat Invoice");
    }

    @Then("user redirect to invoice manual page")
    public void user_redirect_to_invoice_manual_page() throws InterruptedException {
        Assert.assertEquals(manualInvoice.getURL(),invoiceManualPageURL);
        Assert.assertEquals(manualInvoice.getInvoicePageTitle(),"Invoice Manual");
    }
    //end of create invoice manual

    //---create biaya tambahan/sewa---//
    @And("user click Tambah button")
    public void user_click_tambah_button() throws InterruptedException {
        createManualInvoice.clickTambah();
    }

    @When("user click edit button invoice manual")
    public void user_click_edit_button_invoice_manual() throws InterruptedException {
        createManualInvoice.clickEditButton();
    }
    @When("user choose nama biaya {string}")
    public void user_choose_nama_biaya(String namaBiaya) throws InterruptedException {
        if (!(namaBiaya.equalsIgnoreCase("-"))){
            createManualInvoice.chooseNamaBiaya(namaBiaya);
        }
    }

    @And("user fill Lainnya field {string}")
    public void user_fill_Lainnya_field(String lainnya){
        if (!(lainnya.equalsIgnoreCase("-"))){
            createManualInvoice.inputLainnyaTxt(lainnya);
        }
    }

    @And("user choose periode awal {string}")
    public void user_choose_periode_awal(String date) throws InterruptedException {
        if (date.equalsIgnoreCase("today")){
            //get today date
            SimpleDateFormat today = new SimpleDateFormat("d");
            Date dates = new Date();
            createManualInvoice.setTglPeriodeAwal(today.format(dates));
        } else if (date.equalsIgnoreCase("tomorrow")) {
            //get tomorrow date
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            Date dt = calendar.getTime();
            SimpleDateFormat tomorrow = new SimpleDateFormat("d");
            createManualInvoice.setTglPeriodeAwal(tomorrow.format(dt));
        }
    }

    @And("user choose periode akhir {string}")
    public void user_choose_periode_akhir(String date) throws InterruptedException {
        if (date.equalsIgnoreCase("tomorrow")){
            //get tomorrow date
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            Date dt = calendar.getTime();
            SimpleDateFormat tomorrow = new SimpleDateFormat("d");
            createManualInvoice.setTglPeriodeAkhir(tomorrow.format(dt));
        } else if (date.equalsIgnoreCase("day after tomorrow")) {
            //get tomorrow date
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 2);
            Date dt = calendar.getTime();
            SimpleDateFormat tomorrow = new SimpleDateFormat("d");
            createManualInvoice.setTglPeriodeAkhir(tomorrow.format(dt));
        }
    }

    @And("user fill the durasi biaya {string}")
    public void user_fill_the_durasi_biaya(String durasiBiaya) throws InterruptedException {
        if (!durasiBiaya.equalsIgnoreCase("-")){
            if (durasiBiaya.equalsIgnoreCase("more than 255 characters")){
                createManualInvoice.fillDurasiBiaya(char256);
            } else {
                createManualInvoice.fillDurasiBiaya(durasiBiaya);
            }
        }
    }

    @When("user empty durasi biaya")
    public void user_empty_durasi_biaya() {
        createManualInvoice.emptyDurasiBiaya();
    }

    @And("user input jumlah biaya {string}")
    public void user_input_jumlah_biaya(String jmlBiaya){
        createManualInvoice.inputJmlBiaya(jmlBiaya);
    }

    @And("user click Tambah button on the Biaya Tambahan/Sewa pop up")
    public void user_click_Tambah_button_on_the_Biaya_Tambahan_pop_up() throws InterruptedException {
        createManualInvoice.clickTambahBiayaTmbhn();
    }

    @Then("the {string} toast will be displayed")
    public void the_toast_will_be_displayed(String name){
        if (name.equalsIgnoreCase("Biaya Tambahan")){
            Assert.assertEquals(createManualInvoice.getToastBiaya(), biayaTambahanToast, "The toast message does not match");
        } else if (name.equalsIgnoreCase("Biaya Sewa")){
            Assert.assertEquals(createManualInvoice.getToastBiaya(), biayaSewaToast, "The toast message does not match");
        } else if (name.equalsIgnoreCase("Ubah Biaya")) {
            Assert.assertEquals(createManualInvoice.getToastBiaya(), ubahBiayaToast, "The toast message does not match");
        } else if (name.equalsIgnoreCase("Ubah Status Invoice")) {
            Assert.assertEquals(createManualInvoice.getToastBiaya(), ubahStatusInvoiceToast, "The toast message does not match");
        } else {
            Assert.assertEquals(manualInvoice.getInvoiceManualToast(), invoiceManualToast, "The toast message does not match");
        }
    }

    @And("user go to last page")
    public void user_go_to_last_page() throws InterruptedException {
        manualInvoice.goToLastPage();
    }

    @And("user will see nama biaya {string} in the biaya tambahan/sewa table")
    public void user_will_see_nama_biaya_in_the_biaya_tambahan_table(String namaBiaya){
        Assert.assertEquals(createManualInvoice.getNamaBiayaTable(), namaBiaya, "The nama biaya does not match");
    }

    @And("user will see periode awal date {string} in the biaya tambahan/sewa table")
    public void user_will_see_periode_awal_in_the_biaya_tambahan_table(String date){
        if (date.equalsIgnoreCase("today")){
            //get today
            SimpleDateFormat today = new SimpleDateFormat("dd/MM/yyyy");
            Date dates = new Date();

            Assert.assertEquals(createManualInvoice.getTglPeriodeAwal(), today.format(dates));
        } else if (date.equalsIgnoreCase("tomorrow")) {
            //get tomorrow
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            SimpleDateFormat tomorrow = new SimpleDateFormat("dd/MM/yyyy");
            Date dt = calendar.getTime();

            Assert.assertEquals(createManualInvoice.getTglPeriodeAwal(), tomorrow.format(dt));
        } else {
            Assert.assertEquals(createManualInvoice.getTglPeriodeAwal(), "-");
        }
    }

    @And("user will see periode akhir date {string} in the biaya tambahan/sewa table")
    public void user_will_see_periode_akhir_date_in_the_biaya_tambahan_table(String date){
        if (date.equalsIgnoreCase("tomorrow")){
            //get tomorrow
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            SimpleDateFormat tomorrow = new SimpleDateFormat("dd/MM/yyyy");
            Date dt = calendar.getTime();

            Assert.assertEquals(createManualInvoice.getTglPeriodeAkhir(), tomorrow.format(dt));
        } else if (date.equalsIgnoreCase("day after tomorrow")) {
            //get day after tomorrow
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 2);
            SimpleDateFormat tomorrow = new SimpleDateFormat("dd/MM/yyyy");
            Date dt = calendar.getTime();

            Assert.assertEquals(createManualInvoice.getTglPeriodeAkhir(), tomorrow.format(dt));
        } else {
            Assert.assertEquals(createManualInvoice.getTglPeriodeAkhir(), "-");
        }
    }

    @And("user will see jumlah biaya {string} in the biaya tambahan/sewa table")
    public void user_will_see_jumlah_biaya_in_the_biaya_tambahan_table(String jmlBiaya){
        Assert.assertEquals(createManualInvoice.getJmlBiayaTable(), jmlBiaya, "The jumlah biaya does not match");
    }

    @And("user will see disburse to pemilik {string} in the biaya tambahan table")
    public void user_will_see_disburse_to_pemilik_in_the_biaya_tambahan_table(String disburseToPmlk){
        if (!(disburseToPmlk.equalsIgnoreCase("-"))){
            Assert.assertEquals(createManualInvoice.getDisburseToPemilikTable(), disburseToPmlk, "The disburse to pemilik does not match");
        }
    }

    @Then("durasi biaya should be only contains {string}")
    public void durasi_biaya_should_be_only_contains(String content) throws InterruptedException {
        if (content.equalsIgnoreCase("max 255 characters")){
            Assert.assertEquals(createManualInvoice.getDurasiBiaya(),char255);
        } else {
            Assert.assertEquals(createManualInvoice.getDurasiBiaya(),content);
        }
    }

    @Then("counter show {string}")
    public void counter_show(String counter) throws InterruptedException {
        Assert.assertEquals(createManualInvoice.getCounterDurasiBiaya(),counter);
    }

    @Then("periode awal should be disabled")
    public void periode_awal_should_be_disabled() throws InterruptedException {
        Assert.assertTrue(createManualInvoice.isPeriodeAwalDisabled());
    }

    @Then("periode akhir should be disabled")
    public void periode_akhir_should_be_disabled() throws InterruptedException {
        Assert.assertTrue(createManualInvoice.isPeriodeAkhirDisabled());
    }

    @When("user {string} modal tambah biaya")
    public void user_modal_tambah_biaya(String button) throws InterruptedException {
        if (button.equalsIgnoreCase("close")){
            createManualInvoice.closeModalTambahBiaya();
        } else if (button.equalsIgnoreCase("kembali")){
            createManualInvoice.kembaliModalTambahBiaya();
        }
    }

    @Then("tambah biaya modal closed")
    public void tambah_biaya_modal_closed() throws InterruptedException {
        Assert.assertFalse(createManualInvoice.isModalTambahBiayaClosed());
    }

    @Then("all field is empty")
    public void all_field_is_empty() throws InterruptedException {
        Assert.assertEquals(createManualInvoice.getNamaBiayaText(),"Pilih nama biaya");
        Assert.assertEquals(createManualInvoice.getPeriodeAwalValue(),"");
        Assert.assertTrue(createManualInvoice.isPeriodeAkhirDisabled());
        Assert.assertEquals(createManualInvoice.getDurasiBiayaValue(),"");
        Assert.assertEquals(createManualInvoice.getJumlahBiayaValue(),"");
    }

    @Then("{string} {string} is listed in row {int}")
    public void is_listed_in_row(String biaya, String name, Integer row) throws InterruptedException {
        if (biaya.equalsIgnoreCase("biaya tambahan")){
            Assert.assertEquals(createManualInvoice.getNamaBiayaTable2(row), name, "The nama biaya does not match");
        } else {
            Assert.assertEquals(createManualInvoice.getNamaBiayaTable(row), name, "The nama biaya does not match");
        }
    }
    //---end of create biaya tambahan/sewa---//

    //---required field biaya tambahan---//
    @Then("user will see nama biaya error message {string}")
    public void user_will_see_nama_biaya_error_message(String namaBiaya){
        if (!(namaBiaya.equalsIgnoreCase("-"))){
            Assert.assertEquals(createManualInvoice.getNamaBiayaErrMsg(), namaBiaya, "The error message is not message");
        }
    }

    @And("user will see periode awal error message {string}")
    public void user_will_see_periode_awal_error_message(String periode){
        if (!(periode.equalsIgnoreCase("-"))){
            Assert.assertEquals(createManualInvoice.getPeriodeAwalErrMsg(),periode, "The error message is not message");
        }
    }

    @And("user will see periode akhir error message {string}")
    public void user_will_see_periode_akhir_error_message(String periode){
        if (!(periode.equalsIgnoreCase("-"))){
            Assert.assertEquals(createManualInvoice.getPeriodeAkhirErrMsg(), periode, "The error message is not message");
        }
    }

    @And("user will see jumlah biaya error message {string}")
    public void user_will_see_jumlah_biaya_error_message(String jmlBiaya){
        if (!(jmlBiaya.equalsIgnoreCase("-"))){
            Assert.assertEquals(createManualInvoice.getJumlahBiayaErrMsg(), jmlBiaya, "The error message is not message");
        }
    }
    //---end of required field biaya tambahan---//

    //---change invoice type---//
    @When("the pop up confirmation will be displayed")
    public void the_pop_up_confirmation_will_be_displayed(){
        Assert.assertEquals(manualInvoice.getPopUpTitleConfirm(), popUpTitleConfirmation, "The pop up title confirmation does not match");
        Assert.assertEquals(manualInvoice.getPopUpSubtitleConfirm(), popUpSubtitleConfirmation, "The pop up title confirmation does not match");
    }

    @And("user click {string} button on the pop up confirmation")
    public void user_click_button_on_the_pop_up_confirmation(String button) throws InterruptedException {
        if (button.equalsIgnoreCase("Batal")){
            manualInvoice.clickBatalOnPopUp();
        } else {
            manualInvoice.clickLanjutkanOnPopUp();
        }
    }

    @Then("user will directed to {string} section")
    public void user_will_directed_to_section(String invType){
        if (invType.equalsIgnoreCase("Biaya Sewa")){
            Assert.assertEquals(manualInvoice.getBiayaSewaTable(), manualInvoice.getBiayaSewaTable(), "The biaya table does not match");
            Assert.assertEquals(manualInvoice.getEmptyState(), manualInvoice.getEmptyState(), "The empty state wording does not match");
        } else {
            Assert.assertEquals(manualInvoice.getBiayaTambahanTable(), manualInvoice.getBiayaTambahanTable(), "The biaya table does not match");
            Assert.assertEquals(manualInvoice.getEmptyState(), manualInvoice.getEmptyState(), "The empty state wording does not match");
        }
    }

    @Then("user will see empty state on the biaya {string} table")
    public void user_will_see_empty_state_on_the_biaya_table(String invType){
        Assert.assertEquals(manualInvoice.getEmptyState(), manualInvoice.getEmptyState(), "The empty state wording does not match");
    }

    @Then("the pop up confirmation will not be displayed")
    public void the_pop_up_confirmation_will_not_be_displayed(){
        Assert.assertEquals(manualInvoice.isPopUpConfirmationDisplay(), manualInvoice.isPopUpConfirmationDisplay(), "The pop up confirmation displayed, it should not displayed");
    }
    //---end of change invoice type---//

    //---buat dan kirim---//
    @When("user click Buat dan Kirim button")
    public void user_click_Buat_dan_Kirim_button() throws InterruptedException {
        manualInvoice.clickBuatDanKirim();
    }

    @Then("user will see Buat dan Kirim Invoice pop up {string}")
    public void user_will_see_Buat_dan_Kirim_Invoice_pop_up(String jnsInv){
        Assert.assertEquals(manualInvoice.getBuatNKirimTitle(), buatNKirimTitle, "The title does not match");
        Assert.assertEquals(manualInvoice.getBuatNKirimSubtitle(), buatNKirimSubtitle, "The subtitle does not match");
        Assert.assertEquals(manualInvoice.getNamaListing(), listingNamePMAN, "The nama listing does not match");
        Assert.assertEquals(manualInvoice.getNamaPenyewa(), tenantNamePMAN, "The nama penyewa does not match");
        Assert.assertEquals(manualInvoice.getNoHPPenyewa(), tenantNoHPPMAN, "The nomor HP does not match");
        Assert.assertEquals(manualInvoice.getNoKamar(), tenantNoKamarPMAN, "The nomor kamar does not match");
        Assert.assertEquals(manualInvoice.getJenisInvoice(), jnsInv, "The jenis invoice does not match");
        Assert.assertEquals(manualInvoice.getTableTitle(), jnsInv, "The table title does not match");
    }

    @Then("invoice pop up is closed")
    public void invoice_pop_up_is_closed() throws InterruptedException {
        Assert.assertFalse(manualInvoice.isInvoicePopClosed());
    }

    @When("user click {string} button in invoice pop up")
    public void user_click_button_in_invoice_pop_up(String button) throws InterruptedException {
        if (button.equalsIgnoreCase("Close")){
            manualInvoice.clickCloseBtn();
        } else if (button.equalsIgnoreCase("Kembali")){
            manualInvoice.clickKembaliBtn();
        } else if (button.equalsIgnoreCase("Buat dan Kirim")){
            manualInvoice.clickBuatNKirimBtn();
        } else {
            System.out.println("The action is unavailable in invoice pop up");
        }
    }

    @And("the invoice manual will be created with {string}, {string}, and {string}")
    public void the_invoice_manual_will_be_created_with_and(String invType, String totalInv, String statusInv){
        Assert.assertEquals(manualInvoice.getDetailPenyewaName(), tenantNamePMAN, "The detail penyewa data does not match");
        Assert.assertEquals(manualInvoice.getDetailPenyewaNoHp(), tenantNoHPPMAN, "The detail penyewa data does not match");
        Assert.assertEquals(manualInvoice.getNamaListingTable(), listingNamePMAN, "The nama listing data does not match");
        Assert.assertEquals(manualInvoice.getJenisBiaya(), invType, "The jenis biaya does not match");
        Assert.assertEquals(manualInvoice.getTotalInv(), totalInv, "The total invoice does not match");
        Assert.assertEquals(manualInvoice.getStatusInv(), statusInv, "The status invoice does not match");
    }

    @And("the user will see {string} with {string} and {string} detail when hovering Jenis Biaya")
    public void the_user_will_see_detail_when_hovering_Jenis_Biaya(String invType, String biaya, String price) throws InterruptedException {
        manualInvoice.hoverJnsBiaya();
        Assert.assertEquals(manualInvoice.getBiayaDetailTitle(), invType, "The biaya detail does not match");
        Assert.assertEquals(manualInvoice.getBiayaDetailList(), biaya, "The biaya detail does not match");
        Assert.assertEquals(manualInvoice.getBiayaDetailPrice(), price, "The biaya detail does not match");
    }
    //---end buat dan kirim---//

    //---disable buat dan kirim button---//
    @Then("the Buat dan Kirim button is disabled")
    public void the_Buat_dan_Kirim_button_is_disbled(){
        Assert.assertFalse(manualInvoice.isBuatNKirimDisabled());
    }
    //---end disable buat dan kirim button---//

    @When("user save invoice manual number")
    public void user_save_invoice_manual_number() throws InterruptedException {
        Invoice.setInvoiceNumber(manualInvoice.getInvoiceNumber());
    }

    @Then("user save total amount invoice number")
    public void user_save_total_amount_invoice_number() {
        Invoice.setTotalAmount(manualInvoice.getTotalAmount());
    }

    //---invoice page on tenant side---//
    @When("user click invoice number with unpaid status")
    public void user_click_invoice_number_with_unpaid_status() throws InterruptedException {
        manualInvoice.clickInvoiceNumber();
    }

    @Then("user will see Jenis Pembayaran {string} and Total Pembayaran {string}")
    public void user_will_see_Jenis_Pembayaran_and_Total_Pembayaran(String invType, String price){
        manualInvoice.switchPage();
        Assert.assertEquals(manualInvoice.getJnsPmbayaran(), invType, "The jenis pembayaran does not match");
        Assert.assertEquals(manualInvoice.getTotalPmbayaran(), price, "The total pembayaran does not match");
    }

    @And("user will see Listing name, Rincian Pembayaran {string} {string} and Total Pembayaran {string}")
    public void user_will_see_Listing_name_Rincian_Pembayaran_and_Total_Pembayaran(String invType, String biaya, String price){
        Assert.assertEquals(manualInvoice.getListingNameInInvoice(), listingNamePMAN, "The nama listing data does not match");
        Assert.assertEquals(manualInvoice.getRincianPmbayaranInvType(), invType, "The jenis pembayaran does not match");
        Assert.assertEquals(manualInvoice.getRincianPmbayaranBiaya(), biaya, "The biaya data does not match");
        Assert.assertEquals(manualInvoice.getRincianPmbayaranTotal(), price, "The total pembayaran does not match");
        Assert.assertEquals(manualInvoice.getRincianPmbayaranTotal2(), price, "The total pembayaran does not match");
    }
    //---end of invoice page on tenant side---//

    //---delete biaya tambahan & biaya sewa---//
    @When("user click delete button on invoice manual")
    public void user_click_delete_button_on_invoice_manual() throws InterruptedException {
        manualInvoice.clickDeleteBiaya();
    }

    @And("user will see {string} delete confirmation pop up")
    public void user_will_see_delete_confirmation_pop_up(String delete){
        if (delete.equalsIgnoreCase("biaya tambahan")){
            Assert.assertEquals(manualInvoice.getDeleteBiayaTitle(), deleteBiayaTambahanTitle, "The delete biaya title does not match");
            Assert.assertEquals(manualInvoice.getDeleteBiayaSubtitle(), deleteBiayaTambahanSubtitle, "The delete biaya title does not match");
        } else {
            Assert.assertEquals(manualInvoice.getDeleteBiayaTitle(), deleteBiayaSewaTitle, "The delete biaya title does not match");
            Assert.assertEquals(manualInvoice.getDeleteBiayaSubtitle(), deleteBiayaSewaSubtitle, "The delete biaya title does not match");
        }
    }

    @And("user click {string} on delete confirmation pop up")
    public void user_click_on_delete_confirmation_pop_up(String deleteBtn) throws InterruptedException {
        if (deleteBtn.equalsIgnoreCase("Batal")){
            manualInvoice.clickBatal();
        } else if (deleteBtn.equalsIgnoreCase("Hapus")) {
            manualInvoice.clickHapus();
        } else {
            System.out.println("The action is unavailable in deletion pop up");
        }
    }

    @Then("delete confirmation pop up is closed")
    public void delete_confirmation_pop_up_is_closed() throws InterruptedException {
        Assert.assertFalse(manualInvoice.isDeletePopAppears());
    }

    @And("the deletion toast will be displayed")
    public void the_deletion_toast_will_be_displayed(){
        Assert.assertEquals(manualInvoice.getDeleteBiayaToast(), deleteBiayaToast, "The deletion toast does not match");
    }

    @And("the empty state {string} will be displayed")
    public void the_empty_state_will_be_displayed(String biaya){
        if (biaya.equalsIgnoreCase("biaya tambahan")){
            Assert.assertEquals(manualInvoice.getEmptyState(), emptyStateBiayaTambahan, "The empty state wording does not match");
        } else {
            Assert.assertEquals(manualInvoice.getEmptyState(), emptyStateBiayaSewa, "The empty state wording does not match");
        }
    }

    @When("user delete all biaya tambahan or sewa on invoice manual")
    public void user_delete_all_biaya_tambahan_or_sewa_on_invoice_manual() throws InterruptedException {
        manualInvoice.deleteAllBiaya();
    }
    //---end of delete biaya tambahan & biaya sewa---//

    //---Search on invoice manual---//
    @When("user choose search by {string}")
    public void user_choose_search_by(String search) throws InterruptedException {
        manualInvoice.chooseSearchBy(search);
    }

    @And("user input search value with {string}")
    public void user_input_search_value_with(String search) throws InterruptedException {
        manualInvoice.enterSearchValue(search);
    }

    @And("user click search button or hit enter")
    public void user_search_button_or_hit_enter() throws InterruptedException {
        manualInvoice.clickSearch();
    }

    @Then("the result will be displayed according the search value {string} {string} {string}")
    public void the_reulst_will_be_displayed_according_the_search_value(String coloumn1, String coloumn2, String coloumn3){
        Assert.assertEquals(manualInvoice.getSearchResult1(), coloumn1, "The result is not match");
        Assert.assertEquals(manualInvoice.getDetailPenyewaName(), coloumn2, "The result is not match");
        Assert.assertEquals(manualInvoice.getNamaListingTable(), coloumn3, "The result is not match");
    }

    @Then("the result will be displayed according the search value {string}")
    public void the_result_will_be_displayed_according_the_search_value(String result){
        if (result.equalsIgnoreCase("Data yang dicari tidak ditemukan")){
            Assert.assertEquals(manualInvoice.getNoFound(), result, "The result is not match");
        } else {
            Assert.assertEquals(manualInvoice.getNamaListingTable(), result, "The result is not match");
        }
    }
    //---End of search on invoice manual---//

    //Ubah Status Invoice
    @When("user click action button invoice manual")
    public void user_click_action_button_invoice_manual() throws InterruptedException {
        manualInvoice.clickActionButton();
    }

    @When("choose action {string}")
    public void choose_action(String button) throws InterruptedException {
        manualInvoice.chooseAction(button);
    }

    @When("user set tanggal pembayaran {string}")
    public void user_set_tanggal_pembayaran(String date) throws InterruptedException {
        if (date.equalsIgnoreCase("today")){
            //get today date
            SimpleDateFormat today = new SimpleDateFormat("d");
            Date dates = new Date();
            createManualInvoice.setTglPeriodeAwal(today.format(dates));
        }
    }

    @When("user set waktu pembayaran {string}")
    public void user_set_waktu_pembayaran(String time) throws InterruptedException {
        ubahStatusInvoice.setPaidTime(time);
    }

    @When("user click simpan ubah status invoice manual")
    public void user_click_simpan_ubah_status_invoice_manual() throws InterruptedException {
        ubahStatusInvoice.clickSimpanButton();
    }

    @Then("status invoice manual {string}")
    public void status_invoice_manual(String status) {
        Assert.assertEquals(manualInvoice.getStatusInvoice(),status);
    }

    @Then("paid date at {string}, {string}")
    public void paid_date_at(String date, String time) {
        if (date.equalsIgnoreCase("today")){
            SimpleDateFormat today = new SimpleDateFormat("dd/MM/yyyy");
            Date day = new Date();
            String expectedDate = "at "+today.format(day)+", "+time;

            Assert.assertEquals(manualInvoice.getPaidDate(),expectedDate);
        }
    }

    @When("user close ubah status invoice")
    public void user_close_ubah_status_invoice() throws InterruptedException {
        manualInvoice.closeUbahStatusInvoiceModal();
    }

    @When("user click kembali from ubah status invoice")
    public void user_click_kembali_from_ubah_status_invoice() throws InterruptedException {
        manualInvoice.clickKembaliUbahStatusInvoice();
    }
    //End of Ubah Status Invoice

    //---Filter Invoice Manual---//
    @And("user click Filter in invoice manual")
    public void user_click_Filter_in_invoice_manual() throws InterruptedException {
        manualInvoice.clickFilterInv();
    }

    @Then("user will see filter pop up displayed")
    public void user_will_see_filter_pop_up_displayed() throws InterruptedException {
        Assert.assertTrue(manualInvoice.isFilterPopUpAppears());
    }

    @And("user will see the title and subtitle filter")
    public void user_will_see_the_title_and_subtitle_filter(){
        Assert.assertEquals(manualInvoice.getFilterTitle(), titleFilterInvManual, "Title does not match");
        Assert.assertEquals(manualInvoice.getFilterSubtitle(), subtitleFilterInvManual, "Subtitle does not match");
    }

    @And("user will see Status Invoice, Jenis Biaya, Tanggal Invoice title and place holder")
    public void user_will_see_Status_Invoice_Jenis_Biaya_Tanggal_Invoice_Title_and_place_holder(){
        Assert.assertEquals(manualInvoice.getStatusInvTitle(), statusInvTitle, "The status invoice title does not match");
        Assert.assertEquals(manualInvoice.getStatusInvPlaceHolder(), statusInvPlaceHolder, "The status invoice place holder does not match");
        Assert.assertEquals(manualInvoice.getJenisBiayaTitle(), jenisBiayaTitle, "The jenis biaya title does not match");
        Assert.assertEquals(manualInvoice.getJenisBiayaPlaceHolder(), jenisBiayaPlaceHolder, "The jenis biaya place holder does not match");
        Assert.assertEquals(manualInvoice.getInvCreatedTitle(), invCreatedTitle, "The tanggal invoice dibuat title does not match");
        Assert.assertEquals(manualInvoice.getTglMulaiTitle(), tglMulaiTitle, "The tanggal mulai title does not match");
        Assert.assertEquals(manualInvoice.getTglAkhirTitle(), tglAkhirTitle, "The tanggal akhir title does not match");
    }

    @And("user click {string} button on Filter")
    public void user_click_button_on_Filter(String btn) throws InterruptedException {
        if (btn.equalsIgnoreCase("Close")){
            manualInvoice.clickOnClose();
        } else if (btn.equalsIgnoreCase("Terapkan")) {
            manualInvoice.clickOnTerapkan();
        } else if (btn.equalsIgnoreCase("Main Reset")) {
            manualInvoice.clickOnMainReset();
        } else {
            manualInvoice.clickOnReset();
        }
    }

    @Then("user will not see filter pop up displayed")
    public void user_will_not_see_filter_pop_up_displayed() throws InterruptedException {
        Assert.assertFalse(manualInvoice.isFilterPopUpAppears());
    }

    @Then("user will see invoice status {string} result")
    public void user_will_see_invoice_status_result(String statusInv){
        Assert.assertEquals(manualInvoice.getStatusInv(), statusInv, "The status invoice does not match");
    }

    @Then("the filter counter will disappears")
    public void the_filter_counter_will_disappears(){
        Assert.assertFalse(manualInvoice.isFilterCounterDisplayed());
    }

    @When("user click {string} dropdown")
    public void user_click_dropdown(String filter) throws InterruptedException {
        if (filter.equalsIgnoreCase("Status Invoice")){
            manualInvoice.clickStatusInvDropdown();
        } else {
            manualInvoice.clickJnsBiayaDropdown();
        }
    }

    @And("user tick the {string} dropdown {string}")
    public void user_tick_the_dropdown(String dropdown, String tickDropDwn) throws InterruptedException {
        if (dropdown.equalsIgnoreCase("Invoice Status")){
            manualInvoice.tickOnInvStatus(tickDropDwn);
        } else if (dropdown.equalsIgnoreCase("Jenis Biaya")) {
            manualInvoice.tickOnInvStatus(tickDropDwn);
        }
    }

    @Then("the {string} {string} will be displayed according to the filter")
    public void the_will_be_displayed_according_to_the_filter(String filter, String dropdown){
        if (filter.equalsIgnoreCase("Status Invoice")){
            if (dropdown.equalsIgnoreCase("Paid")){
                Assert.assertEquals(manualInvoice.getInvStatusPaid(), dropdown, "The status invoice does not match");
            } else {
                Assert.assertEquals(manualInvoice.getStatusInv(), dropdown, "The status invoice does not match");
            }
        } else if (filter.equalsIgnoreCase("Jenis Biaya")) {
            Assert.assertEquals(manualInvoice.getJenisBiaya(), dropdown, "The jenis biaya does not match");
        } else {
            if (dropdown.equalsIgnoreCase("today")){
                SimpleDateFormat today = new SimpleDateFormat("dd/MM/yyyy");
                Date day = new Date();
                String expectedDate = today.format(day);

                Assert.assertEquals(manualInvoice.getCreatedBy(),expectedDate);
            }
        }
    }

    @And("user select the date for {string}")
    public void user_select_the_date_for(String date) throws InterruptedException {
        if (date.equalsIgnoreCase("today")){
            //get today date
            SimpleDateFormat today = new SimpleDateFormat("d");
            Date dates = new Date();
            createManualInvoice.setTglPeriodeAwal(today.format(dates));
        } else if (date.equalsIgnoreCase("tomorrow")) {
            //get tomorrow date
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, 1);
            Date dt = calendar.getTime();
            SimpleDateFormat tomorrow = new SimpleDateFormat("d");
            createManualInvoice.setTglPeriodeAkhir(tomorrow.format(dt));
        }
    }

    @Then("the {string}, {string}, {string}, {string} will be displayed according to the search and filter")
    public void the_will_be_displayed_according_to_the_search_and_filter(String result, String result2, String result3, String result4){
        Assert.assertEquals(manualInvoice.getNamaListingTable(), result, "The result is not match");
        Assert.assertEquals(manualInvoice.getStatusInv(), result2, "The status invoice does not match");
        Assert.assertEquals(manualInvoice.getJenisBiaya(), result3, "The jenis biaya does not match");

        if (result4.equalsIgnoreCase("today")){
            SimpleDateFormat today = new SimpleDateFormat("dd/MM/yyyy");
            Date day = new Date();
            String expectedDate = today.format(day);

            Assert.assertEquals(manualInvoice.getCreatedBy(),expectedDate);
        }
    }
    //---End of Filter Invoice Manual---//
}
