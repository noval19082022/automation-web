package steps.mamipay;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamipay.FilterVoucherPO;
import pageobjects.backoffice.voucherdiscount.ListVoucherPO;
import pageobjects.backoffice.voucherdiscount.AddVoucherFormPO;
import pageobjects.mamipay.PartnerVoucherPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

public class VoucherSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JavaHelpers java = new JavaHelpers();
    private ListVoucherPO list = new ListVoucherPO(driver);
    private AddVoucherFormPO update = new AddVoucherFormPO(driver);
    private FilterVoucherPO filterVoucherPO = new FilterVoucherPO(driver);
    private PartnerVoucherPO partnerVoucherPO = new PartnerVoucherPO(driver);

    //test data
    private String voucherPrefix;

    @Given("user deactivate single voucher {string}")
    public void user_deactivate_single_voucher(String voucherCode) throws InterruptedException {
        list.clickOnSingleVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.clickOnActiveCheckbox();
        update.clickOnEditSingleVoucherButton();
        update.clickOnYesDoItButton();
    }

    @Given("user activate mass voucher {string}")
    public void user_activate_mass_voucher(String voucherCode) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        if(list.statusIsNotActive()){
            list.clickOnUpdateIcon();
            update.clickOnActiveCheckbox();
            update.clickOnEditMassVoucherButton();
            update.clickOnYesDoItButton();
        }
    }

    @Given("user edit voucher {string} and set end date is {string}")
    public void user_edit_voucher_and_set_end_date_is(String voucherCode, String endDate) throws InterruptedException, ParseException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.enterStartDate();
        update.enterEndDate(endDate);
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @When("user edit voucher {string} and set target profession is {string}")
    public void user_edit_voucher_and_set_target_profession_is(String voucherCode, String profession) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.selectProfessionTarget(profession);
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @Given("user edit voucher {string} and revert minimum transaction")
    public void user_edit_voucher_and_revert_minimum_transaction(String voucherCode) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.enterMinimumTransaction("500000");
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @When("user edit voucher {string} and fill field not applicable for {string}")
    public void user_edit_voucher_and_fill_field_not_applicable_for(String voucherCode, String email) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.enterTenantEmailApplicableFor("");
        update.enterTenantEmailNotApplicableFor(email);
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @Given("user edit voucher {string} and fill field applicable for {string}")
    public void user_edit_voucher_and_fill_field_applicable_for(String voucherCode, String email) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.enterTenantEmailNotApplicableFor("");
        update.enterTenantEmailApplicableFor(email);
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @When("user edit voucher {string} and update minimum transaction")
    public void user_edit_voucher_and_update_minimum_transaction(String voucherCode) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.enterMinimumTransaction("11000000");
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @When("user edit voucher {string} and set payment rules {string}")
    public void user_edit_voucher_and_set_payment_rules(String voucherCode, String paymentRules) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();

        switch (paymentRules){
            case "For First DP":
                update.selectForFirstDPPaymentRules();
                break;
            case "For First Full Paid":
                update.selectForFullPaidPaymentRules();
                break;
            case "For First Settlement":
                update.selectForFirstSettlementPaymentRules();
                break;
            case "For Recurring":
                update.selectForRecurringPaymentRules();
                break;
        }
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @When("user edit voucher {string} and uncheck payment rules {string}")
    public void user_edit_voucher_and_uncheck_payment_rules(String voucherCode, String paymentRules) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        String rulesStatus = list.getPaymentRules();
        list.clickOnUpdateIcon();

        switch (paymentRules){
            case "For First Settlement":
                paymentRules = "For First Paid";
                break;
            case "For First Full Paid":
                paymentRules = "Pelunasan";
                break;
            case "For Recurring":
                paymentRules = "Recurring";
                break;
            default:
                update.selectForFirstDPPaymentRules();
        }

        if (rulesStatus.contains(paymentRules)){
            switch (paymentRules){
                case "For First Paid":
                    update.selectForFirstSettlementPaymentRules();
                    break;
                case "Pelunasan":
                    update.selectForFullPaidPaymentRules();
                    break;
                case "Recurring":
                    update.selectForRecurringPaymentRules();
                    break;
                default:
                    update.selectForFirstDPPaymentRules();
            }
        }
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @When("user edit voucher {string} and set payment rules {string} only")
    public void user_edit_voucher_and_set_payment_rules_only(String voucherCode, String paymentRules) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        String getPaymentRulesActive = list.getPaymentRules();
        System.out.println(getPaymentRulesActive);
        list.clickOnUpdateIcon();

        switch (paymentRules){
            case "For First Settlement":
                paymentRules = "For First Paid";
                break;
            case "For First Full Paid":
                paymentRules = "Pelunasan";
                break;
            case "For Recurring":
                paymentRules = "Recurring";
                break;
            default:
                paymentRules = "For DP";
        }

        switch (paymentRules) {
            case "Recurring":
                if (getPaymentRulesActive.contains("For DP")) {
                    update.selectForFirstDPPaymentRules();
                }
                if (getPaymentRulesActive.contains("Pelunasan")) {
                    update.selectForFirstSettlementPaymentRules();
                }
                if (getPaymentRulesActive.contains("For First Paid")) {
                    update.selectForFullPaidPaymentRules();
                }
                if (!getPaymentRulesActive.contains("Recurring")) {
                    update.selectForRecurringPaymentRules();
                }
                break;
            case "Pelunasan":
                if (getPaymentRulesActive.contains("For DP")) {
                    update.selectForFirstDPPaymentRules();
                }
                if (!getPaymentRulesActive.contains("Pelunasan")) {
                    update.selectForFirstSettlementPaymentRules();
                }
                if (getPaymentRulesActive.contains("For First Paid")) {
                    update.selectForFullPaidPaymentRules();
                }
                if (getPaymentRulesActive.contains("Recurring")) {
                    update.selectForRecurringPaymentRules();
                }
                break;
            case "For First Paid":
                if (getPaymentRulesActive.contains("For DP")) {
                    update.selectForFirstDPPaymentRules();
                }
                if (getPaymentRulesActive.contains("Pelunasan")) {
                    update.selectForFirstSettlementPaymentRules();
                }
                if (!getPaymentRulesActive.contains("For First Paid")) {
                    update.selectForFullPaidPaymentRules();
                }
                if (getPaymentRulesActive.contains("Recurring")) {
                    update.selectForRecurringPaymentRules();
                }
                break;
            default:
                if (!getPaymentRulesActive.contains("For DP")) {
                    update.selectForFirstDPPaymentRules();
                }
                if (getPaymentRulesActive.contains("Pelunasan")) {
                    update.selectForFirstSettlementPaymentRules();
                }
                if (getPaymentRulesActive.contains("For First Paid")) {
                    update.selectForFullPaidPaymentRules();
                }
                if (getPaymentRulesActive.contains("Recurring")) {
                    update.selectForRecurringPaymentRules();
                }
                break;
        }

        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @Given("user deactivate mass voucher {string}")
    public void user_deactivate_mass_voucher(String voucherCode) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.clickOnActiveCheckbox();
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @Given("user edit voucher {string} and set voucher aplicable on {string} city")
    public void user_edit_voucher_and_set_voucher_aplicable_on_city(String voucherCode, String city) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.selectVoucherAplicableOnCity(city);
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @When("user edit voucher {string} and set voucher not aplicable on {string} city")
    public void user_edit_voucher_and_set_voucher_not_aplicable_on_city(String voucherCode, String city) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        update.selectVoucherNotAplicableOnCity(city);
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }

    @Then("user see {string} filter list option on voucher menu:")
    public void user_see_all_filter_list_option_are(String filter, List<String> options) {
        List<String> actualOptions = filterVoucherPO.getAllFilterOptions(filter);
        for (int i=0; i<options.size(); i++) {
            Assert.assertEquals(actualOptions.get(i), options.get(i), "filter "+filter+" option is wrong, it should be " + options.get(i));
        }
    }

    @When("user choose to filter {string} on voucher menu with value {string}")
    public void user_choose_to_filter_on_voucher_menu_with_value(String filter, String value) throws InterruptedException {
        filterVoucherPO.chooseDropDownFilterVoucher(filter,value);
        list.clickOnSearchButton();
    }

    @When("user click reset filter button on voucher page")
    public void user_reset_filter_button_on_voucher_pag() throws InterruptedException {
        filterVoucherPO.clickResetFilterButton();
    }
    @Then("voucher with selected filter value {string} is displayed")
    public void voucher_with_selected_filter_value_is_displayed(String value) throws InterruptedException {
        Assert.assertTrue(filterVoucherPO.getVoucherListTable().contains(value));
    }

    @Then("list table voucher doesn't contain {string}")
    public void list_table_voucher_doesnt_contain(String value) throws InterruptedException {
        Assert.assertFalse(filterVoucherPO.getVoucherListTable().contains(value));
    }

    @When("admin master goes to Single Voucher tab")
    public void admin_master_goes_to_Single_Voucher_tab() throws InterruptedException {
        list.clickOnSingleVoucherButton();
    }

    @When("admin clicks on add single voucher button")
    public void admin_clicks_on_add_single_voucher_button() throws InterruptedException {
        list.clicksOnAddSingleVoucherButton();
    }

    @When("admin master inputs voucher campaign name to {string}")
    public void admin_master_inputs_voucher_campaign_name_to(String campaignName) {
        update.inputCampaignName(campaignName);
    }

    @When("admin master sets campaign date to {string} and end date to {string}")
    public void admin_master_sets_campaign_date_to_and_end_date_to(String startDate, String endDate) throws ParseException, InterruptedException {
        update.chooseStartDate(startDate);
        update.enterEndDate(endDate);
    }

    @When("admin master select campaign team to {string}")
    public void admin_master_select_campaign_team_to(String campaignTeam) throws InterruptedException {
        update.selectCampaignTeam(campaignTeam);
    }

    @When("admin master enter voucher prefix to {string}")
    public void admin_master_enter_voucher_prefix_to(String prefix) throws ParseException {
        int length = 4;
        boolean useLetters = true;
        boolean useNumbers = false;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
        voucherPrefix = prefix + generatedString.toUpperCase();
        update.enterVoucherPrefix(voucherPrefix);
    }

    @When("admin master enter total targeted voucher {string}")
    public void admin_master_enter_total_targeted_voucher(String voucherTarget) {
        update.enterVoucherTotalTarget(voucherTarget);
    }

    @When("^admin master tick payment rules :$")
    public void admin_master_tick_payment_rules(List<String> paymentRules) throws InterruptedException {
        for (String paymentRule : paymentRules) {
            update.tickOnPaymentRules(paymentRule);
        }
    }

    @When("^admin select contract rules :$")
    public void admin_select_contract_rules(List<String> contractRules) throws InterruptedException {
        for (String contractRule : contractRules) {
            update.tickOnContractRules(contractRule);
        }
    }

    @When("admin select discount type {string}")
    public void admin_select_discount_type(String discountType) {
        update.selectDiscountType(discountType);
    }

    @When("admin master input discount amount {string}")
    public void admin_master_input_discount_amount(String discountAmount) {
        update.inputDiscountAmount(discountAmount);
    }

    @When("admin master input minimum transaction {string}")
    public void admin_master_input_minimum_transaction(String minimumTransaction) {
        update.inputMinimumTransaction(minimumTransaction);
    }

    @When("^admin master tick important check box :$")
    public void admin_master_tick_important_check_box(List<String> importantRules) throws InterruptedException {
        for (String importantRule : importantRules) {
            update.tickOnImportantRules(importantRule);
        }
    }

    @When("admin master untick important check box :")
    public void admin_master_untick_important_check_box(List<String> importantRules) throws InterruptedException {
        for (String importantRule : importantRules) {
            if (update.isImportantRuleChecked(importantRule)) {
                update.tickOnImportantRules(importantRule);
            }
        }
    }

    @When("admin master clicks on add single voucher button")
    public void admin_master_clicks_on_add_single_voucher_button() throws InterruptedException {
        update.clicksOnAddSingleVoucherButton();
    }

    @When("admin master input maximum amount for voucher discount {string}")
    public void admin_master_input_maximum_amount_for_voucher_discount(String maximumUsage) {
        update.inputMaximumAmount(maximumUsage);
    }

    @When("admin master input total each quota to {string} and daily each quota to {string}")
    public void admin_master_input_total_each_quota_to_and_daily_each_quota_to(String totalEachQuota, String dailyEachQuota) throws InterruptedException {
        update.inputTotalEachQuota(totalEachQuota);
        update.inputDailyEachQuota(dailyEachQuota);
    }


    @Then("user edit voucher {string} and set voucher code is {string}")
    public void user_edit_voucher_and_set_voucher_code_is(String voucher, String voucherCode) throws InterruptedException, ParseException {
        list.enterVoucherCode(voucher);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        partnerVoucherPO.enterVoucherCode(voucherCode);
        partnerVoucherPO.clickOnUpdateVoucherButton();
        update.clickOnYesDoItButton();
    }

    @Then("failed update voucher and display text validation {string}")
    public void failed_update_voucher_and_display_text_validation(String message) {
        Assert.assertEquals(partnerVoucherPO.getMessageError(), message, "message is not match");
    }

    @Then("user edit voucher {string} and set campaign title is {string}")
    public void user_edit_voucher_and_set_campaign_itle_is(String voucher, String campaignTitle) throws InterruptedException, ParseException {
        list.enterVoucherCode(voucher);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        partnerVoucherPO.enterCampaignTitle(campaignTitle);
    }

    @And("user click published check box")
    public void user_click_published_check_box() throws InterruptedException{
        if(!partnerVoucherPO.checkPublishIsCheckedOrNot()){
            partnerVoucherPO.clickOnPublishCheckBox();
            partnerVoucherPO.clickOnUpdateVoucherButton();
            update.clickOnYesDoItButton();
        }else if(partnerVoucherPO.checkPublishIsCheckedOrNot()){
            partnerVoucherPO.clickOnUpdateVoucherButton();
            update.clickOnYesDoItButton();
        }
    }

    @Then("message success update voucher is present")
    public void message_success_update_voucher_is_present() {
        Assert.assertTrue(partnerVoucherPO.messageSuccessUpdateIsDisplayed(),"not present");
    }

    @When("admin master fills prefix or campaign name input filter to {string}")
    public void admin_master_fills_prefix_or_campaign_name_input_filter_to(String prefixCampaignName) {
        list.enterCampaignNameFilter(prefixCampaignName);
    }

    @When("admin master clicks in search button in voucher list page")
    public void admin_master_clicks_in_search_button_in_voucher_list_page() throws InterruptedException {
        list.clickOnSearchButton();
    }

    @When("admin master clicks on edit pencil icon index {string} in voucher list result")
    public void admin_master_clicks_on_edit_pencil_icon_index_in_voucher_list_result(String index) throws InterruptedException {
        list.clickOnUpdateIconIndex(index);
    }

    @Then("admin master can sees campaign name is {string}")
    public void admin_master_can_sees_campaign_name_is(String campaignName) {
        Assert.assertEquals(update.getCampaignNameText(), campaignName, "Campaign name is not equal");
    }

    @Then("admin master can sees campaign date is {string} and end date to {string}")
    public void admin_master_can_sees_campaign_date_is_and_end_date_to(String startDate, String endDate) throws ParseException {
        String todayDate;
        if (startDate.equalsIgnoreCase("today")) {
            todayDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "yyyy/MM/dd", 0, 0, 0, 0);;
        }
        else {
            todayDate = startDate;
        }
        String dateToCompare = update.getCampaignStartDate().split(" ")[0];

        Assert.assertEquals(dateToCompare, todayDate);
        if (!endDate.equalsIgnoreCase("")) {
            Assert.assertEquals(endDate, endDate);
        }
    }

    @Then("admin master can sees campaign team is set to {string} with prefix contain {string}")
    public void admin_master_can_sees_campaign_team_is_set_to_with_prefix_contain(String campaignTeam, String voucherPrefix) {
        Assert.assertEquals(update.getSelectedCampaignTeamText(), campaignTeam);
        Assert.assertTrue(update.getVoucherPrefixText().contains(voucherPrefix));
    }

    @Then("admin master can sees total targeted voucher is {string}")
    public void admin_master_can_sees_total_targeted_voucher_is(String totalTarget) {
        Assert.assertEquals(update.getVoucerTotalTargetText(), totalTarget);
    }

    @Then("^admin master can sees payment rules ticked :$")
    public void admin_master_can_sees_payment_rules_ticked(List<String> paymentRules) {
        for (String paymentRule : paymentRules) {
            Assert.assertTrue(update.isPaymentRulesTicked(paymentRule));
        }
    }

    @Then("^admin master can sees contract rules ticked :$")
    public void admin_master_can_sees_contract_rules_ticked(List<String> contractRules) {
        for (String contractRule : contractRules) {
            Assert.assertTrue(update.isContractRulesTicked(contractRule));
        }
    }

    @Then("admin master can sees discount type {string} with amount is {string}")
    public void admin_master_can_sees_discount_type_with_amount_is(String discountType, String discountAmount) {
        Assert.assertEquals(update.getDiscountTypeText(), discountType);
        Assert.assertEquals(update.getDiscountAmountText(), discountAmount);
    }

    @Then("admin master can sees total each quota is {string} with daily each quota is {string}")
    public void admin_master_can_sees_total_each_quota_is_with_daily_each_quota_is(String eachQuota, String dailyEachQuota) {
        Assert.assertEquals(update.getEachQuotaText(), eachQuota);
        Assert.assertEquals(update.getDailyEachQuota(), dailyEachQuota);
    }

    @Then("admin master can sees maximum amount is {string} and minimum {string} for transaction")
    public void admin_master_can_sees_maximum_amount_is_and_minimum_for_transaction(String maximumTransaction, String minimumTransaction) {
        Assert.assertEquals(update.getInputMaximumTransactionText().trim(), maximumTransaction);
        Assert.assertEquals(update.getInputMinimumTransactionText().trim(), minimumTransaction);
    }

    @Then("admin master can sees important rules is ticked :")
    public void admin_master_can_sees_important_rules_is_ticked(List<String> importantRules) {
        for (String importantRule : importantRules) {
            Assert.assertTrue(update.isImportantRuleChecked(importantRule));
        }
    }

    @Then("admin master can sees important rules is not ticked :")
    public void admin_master_can_sees_important_rules_is_not_ticked(List<String> importantRules) {
        for (String importanRule : importantRules) {
            Assert.assertFalse(update.isImportantRuleChecked(importanRule));
        }
    }


    @Then("system display error message voucher code already exists {string}")
    public void system_display_error_message_voucher_code_aalready_exists(String messageError) throws InterruptedException{
        Assert.assertEquals(partnerVoucherPO.getMessageError(), messageError, "message is not match");
    }

    @Then("admin master can sees update voucher confirmation pop-up")
    public void admin_master_can_sees_update_voucher_confirmation_pop_up() {
        Assert.assertTrue(update.isVoucherUpdateConfirmation());
    }

    @When("admin master clicks on go back button")
    public void admin_master_clics_on_go_back_button() throws InterruptedException {
        update.clicksOnNoGoBackButton();
    }

    @Then("admin master can not sees update voucher confirmation pop-up")
    public void admin_master_can_not_sees_update_voucher_confirmation_pop_up() {
        Assert.assertFalse(update.isVoucherUpdateConfirmation());
    }

    @When("admin master clicks on Yes, Do It! button")
    public void admin_master_clicks_on_Yes_Do_It_button() throws InterruptedException {
        update.clickOnYesDoItButton();
    }

    @When("admin master clicks on voucher list icon index {string}")
    public void admin_master_clicks_on_voucher_list_icon_index(String index) throws InterruptedException {
        list.clicksOnVoucherListIndex(index);
    }

    @Then("admin can sees vouchers status are {string}")
    public void admin_can_sees_vouchers_status_are_active(String status) {
        int voucherListSize = list.getVoucherListVisible();
        for(int i = 0; i < voucherListSize; i++) {
            Assert.assertEquals(list.getVoucherListStatusIndex(String.valueOf(i+1)), status);
        }
    }

    @When("^admin master input targeted email below :$")
    public void admin_master_input_targeted_email_below(List<String> listEmails) {
        for (String listEmail : listEmails) {
            update.inputVoucherTargetEmail(listEmail);
        }
    }

    @When("admin master upload voucher campaign image")
    public void admin_master_upload_voucher_campaign_image() throws InterruptedException {
        update.uploadCampaignImage();
    }

    @When("admin master input campaign title to {string}")
    public void admin_master_input_campaign_title_to(String campaignTitle) {
        update.inputCampaignTitle(campaignTitle);
    }

    @When("admin master input campaign term & conditions to {string}")
    public void admin_master_input_campaign_term_conditions_to(String termAndCondition) {
        update.inputCampaignTermAndConditions(termAndCondition);
    }

    @And("user input voucher code {string}")
    public void user_input_voucher_code(String voucherCode) {
        partnerVoucherPO.enterVoucherCode(voucherCode);
    }

    @And("user input total quota {string}")
    public void user_input_total_quota(String totalQuota) {
        partnerVoucherPO.enterTotalQuota(totalQuota);
    }

    @Then("user see validation error field is required")
    public void user_see_validation_error_field_is_required(List<String> listValidationRequired) {
        for(int i = 0; i < listValidationRequired.size(); i++) {
            Assert.assertEquals(partnerVoucherPO.getMessageValidationFieldRequired(i), listValidationRequired.get(i),"Messaga validation not match");
        }
    }

    @And("user clicks on add voucher partner")
    public void user_clicks_on_add_voucher_partner() throws InterruptedException{
        partnerVoucherPO.clicksOnAddVoucherPartnerButton();
    }

    @And("user clicks on add voucher")
    public void user_clicks_on_add_voucher() throws InterruptedException{
        partnerVoucherPO.clicksOnAddVoucherButton();
    }

    @And("user click published check box on page create voucher")
    public void user_click_published_check_box_on_page_create_voucher() throws InterruptedException{
        partnerVoucherPO.clickOnPublishCheckBox();
    }

    @When("admin master clicks on add mass voucher button")
    public void admin_master_clicks_on_add_mass_voucher_button() throws InterruptedException {
        list.clicksOnAddMassVoucherButton();
    }

    @When("admin master inputs mass voucher code {string}")
    public void admin_master_inputs_mass_voucher_code(String prefix) {
        int length = 4;
        boolean useLetters = true;
        boolean useNumbers = false;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
        voucherPrefix = prefix + generatedString.toUpperCase();
        update.inputMassVoucherCode(voucherPrefix);
    }

    @When("admin master search id, voucher code or campaign name to {string}")
    public void admin_master_search_id_voucher_code_or_campaign_name_to(String idVoucherCodeCampaignName) {
        list.enterCampaignNameFilter(idVoucherCodeCampaignName);
    }

    @When("admin master clicks on add mass voucher button in voucher form")
    public void admin_master_clicks_on_add_mass_voucher_button_in_voucher_form() {
        update.clicksOnAddSingleVoucherButton();
    }

    @Then("admin master can sees campaign team is set to {string} with voucher code contains {string}")
    public void admin_master_can_sees_campaign_team_is_set_to_with_voucher_code_contains(String campaignTeam, String voucherCode) {
        Assert.assertEquals(update.getSelectedCampaignTeamText(), campaignTeam);
        Assert.assertTrue(update.getVoucherCode().contains(voucherCode));
    }

    @Then("admin can sees first index voucher status in mass voucher is {string}")
    public void admin_can_sees_first_index_voucher_status_in_mass_voucher_is(String status) {
        Assert.assertEquals(list.getVoucherListStatusIndex("1"), status);
    }

    @When("admin master upload mass voucher csv file")
    public void admin_master_upload_mass_voucher_csv_file() throws InterruptedException {
        update.uploadMassVoucherCSVFile();
    }

    @When("admin master add or set room type to {string}")
    public void admin_master_add_or_set_room_type_to(String roomType) throws InterruptedException {
        if (update.getActiveRoomTypeApplicableFor().size() > 0) {
            update.removeApplicableForRoomTypeIndex("1");
            update.applicableForSetRoomType(roomType);
        }
        else {
            update.applicableForSetRoomType(roomType);
        }
    }

    @When("admin master change minimum type of contract period to {string}")
    public void admin_master_change_minimum_type_of_contract_period_to(String period) {
        update.setDropdownMinimumContractPeriod(period);
    }

    @When("user edit voucher {string} and set voucher {string} on {string} kost")
    public void user_edit_voucher_and_set_voucher_applicable_or_not_on_kost(String voucherCode, String applicableOrNot, String kost) throws InterruptedException {
        list.clickOnMassVoucherButton();
        list.enterVoucherCode(voucherCode);
        list.clickOnSearchButton();
        list.clickOnUpdateIcon();
        if(applicableOrNot.equals("applicable")){
            update.selectVoucherAplicableOnKost(kost);
        } else if(applicableOrNot.equals("not applicable")){
            update.selectVoucherNotAplicableOnKost(kost);
        }
        update.clickOnEditMassVoucherButton();
        update.clickOnYesDoItButton();
    }
}