package steps.mamipay;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamipay.CommonPO;
import utilities.ThreadManager;

public class CommonSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private CommonPO common = new CommonPO(driver);

    @Then("System display alert message {string} on mamipay web")
    public void system_display_alert_message_on_mamipay_web(String message) {
        Assert.assertTrue(common.getMessageAlert().contains(message), "Message not match");
    }
}
