package steps.mamipay;

import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamipay.AllInvoiceListPO;
import pageobjects.mamipay.CommonPO;
import utilities.Constants;
import dataobjects.Invoice;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.util.List;
import java.util.Map;

public class AllInvoiceListSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AllInvoiceListPO allInvoice = new AllInvoiceListPO(driver);
    private CommonPO common = new CommonPO(driver);

    //test data
    private String allInvoiceProperties ="src/test/resources/testdata/mamipay/allInvoiceList.properties";
    private String allInvoicePageURL = JavaHelpers.getPropertyValue(allInvoiceProperties, "allInvoiceListURL_" + Constants.ENV);


    //invoice manual
    @Then("all invoice list page should be open")
    public void all_invoice_list_page_should_be_open() throws InterruptedException {
        Assert.assertEquals(common.getURL(),allInvoicePageURL);
        Assert.assertEquals(allInvoice.getPageTitle(),"MamiPAY All Invoice List");
    }

    @When("user choose filter by order type {string}")
    public void user_choose_filter_by_order_type(String type) throws InterruptedException {
        allInvoice.setOrderTypeFilter(type);
    }

    @When("click cari invoice")
    public void click_cari_invoice() throws InterruptedException {
        allInvoice.clickCariInvoice();
    }

    @Then("only display list of Invoice Manual")
    public void only_display_list_of_Invoice_Manual() {
        int total = allInvoice.countList();
        for (int i = 1; i<=total ; i++){
            Assert.assertTrue(allInvoice.isOrderTypeInvoiceManual(i));
            if (allInvoice.isOrderTypeInvoiceManual(i) == false){
                break;
            }
        }
    }

    @Then("display Invoice Manual in the list")
    public void display_Invoice_Manual_in_the_list() {
        int total = allInvoice.countList();
        int i;
        for (i = 1; i<=total ; i++){
            if (allInvoice.isOrderTypeInvoiceManual(i) == true){
                Assert.assertTrue(allInvoice.isOrderTypeInvoiceManual(i));
                break;
            }
        }
    }

    @When("user search invoice manual number")
    public void user_search_invoice_manual_number() throws InterruptedException {
        allInvoice.setSearchValue(Invoice.getInvoiceNumber());
    }

    @Then("user verify invoice number is correct")
    public void user_verify_invoice_number_is_correct() throws InterruptedException {
        Assert.assertEquals(allInvoice.getInvoiceNumber(),Invoice.getInvoiceNumber());
    }

    @Then("invoice manual status is {string}")
    public void invoice_manual_status_is(String status) throws InterruptedException {
        Assert.assertEquals(allInvoice.getInvoiceStatus(),status);
    }

    @When("user click change status")
    public void user_click_change_status() throws InterruptedException {
        allInvoice.clickChangeStatus();
    }

    @When("set invoice manual to {string}")
    public void set_invoice_manual_to(String status) throws InterruptedException {
        allInvoice.setInvoiceStatus(status);
    }

    @When("fill invoice manual paid date")
    public void fill_invoice_manual_paid_date() throws InterruptedException {
        allInvoice.setPaidDate("today");
    }

    @When("submit change status invoice manual")
    public void submit_change_status_invoice_manual() throws InterruptedException {
        allInvoice.clickSubmitChange();
    }

    @When("user click shortlink")
    public void user_click_shortlink() throws InterruptedException {
        allInvoice.clickShortlink();
    }

    @When("user click view log")
    public void user_click_view_log() throws InterruptedException {
        allInvoice.clickViewLog();
    }

    @Then("contains data invoice with correct invoice number that already paid")
    public void contains_data_invoice_with_correct_invoice_number_that_already_paid() {
        Assert.assertEquals(allInvoice.getDataInvoice("Invoice Number"),Invoice.getInvoiceNumber());
        Assert.assertEquals(allInvoice.getDataInvoice("Status"),"paid");
        Assert.assertEquals(allInvoice.getDataInvoice("Order Type"),"mamipay_cp_manual_invoice");
        Assert.assertEquals(allInvoice.getDataInvoice("Amount"),"50000");
        Assert.assertEquals(allInvoice.getDataInvoice("Paid Amount"),"50000");
    }

    @Then("contains data revision history")
    public void contains_data_revision_history(io.cucumber.datatable.DataTable dataTable) {
        List<Map<String, String>> data = dataTable.asMaps(String.class,String.class);
            for (Map<String, String> form : data) {
                String changedBy = form.get("Changed by");
                String changer = form.get("Changer role");
                String changes = form.get("What changed");
                String oldValue = form.get("Old Value");
                String newValue = form.get("New Value");
                String row = form.get("row");

                Assert.assertEquals(allInvoice.getDataRevisionHistory("Changed by",row),changedBy);
                Assert.assertEquals(allInvoice.getDataRevisionHistory("Changer role",row),changer);
                Assert.assertEquals(allInvoice.getDataRevisionHistory("What changed",row),changes);
                Assert.assertEquals(allInvoice.getDataRevisionHistory("Old Value",row),oldValue);
                Assert.assertEquals(allInvoice.getDataRevisionHistory("New Value",row),newValue);
            }
    }

    @Then("contains data invoice paymnet history from {string}")
    public void contains_data_invoice_paymnet_history_from(String paymentMethod) {
        if (paymentMethod.equalsIgnoreCase("Mandiri")){
            Assert.assertEquals(allInvoice.getDataInvoicePaymentHistory("Payment Method"),"midtrans_api_mandiri");
            Assert.assertEquals(allInvoice.getDataInvoicePaymentHistory("Amount"),"50000");
            Assert.assertEquals(allInvoice.getDataInvoicePaymentHistory("Status"),"paid");
            Assert.assertEquals(allInvoice.getDataInvoicePaymentHistory("Paid Amount"),"50000");
        }
    }
    //end of invoice manual
}
