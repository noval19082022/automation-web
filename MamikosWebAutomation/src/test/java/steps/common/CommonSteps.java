package steps.common;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pageobjects.consultanttools.HomePagePO;
import pageobjects.mamikos.common.BannerPO;
import pageobjects.mamikos.owner.common.helpCenterPO;
import utilities.Constants;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class CommonSteps {

	private WebDriver driver = ThreadManager.getDriver();
	private SeleniumHelpers selenium = new SeleniumHelpers(driver);
	private helpCenterPO helpCenter = new helpCenterPO(driver);
	private BannerPO banner = new BannerPO(driver);
	private HomePagePO homepage = new HomePagePO(driver);

		/**
		 * <p> If base url https://owner-jambu.kerupux.com then format should be owner /{path}
		 * <p> If base url https://jambu.kerupux.com then format should be mamikos /{path}
		 * <p> use switch condition when url cant be define with if condition
		 * <p> make sure both environment are match
		 */
	@Given("user navigates to {string}")
    public void user_navigates_to(String application) throws InterruptedException {
        if (application.contains("owner /")) {
        	application = Constants.OWNER_URL + application.substring(6);
        } else if (application.contains("mamikos /")) {
            application = Constants.MAMIKOS_URL + application.substring(8);
        } else if (application.contains("consultant /")) {
            application = Constants.CONSULTANT_URL;
		}

        switch (application) {
            case "SBMPTN page":
                application = Constants.MAMIKOS_URL + "/download-soal/download-soal-sbmptn-tkpa-saintek-soshum-dan-campuran-beserta-pembahasannya-gratis";
                break;
            case "backoffice":
                application = Constants.BACKOFFICE_URL;
                break;
            case "mamikos2":
                application = Constants.MAMIKOS_URL2;
                break;
            case "mamikos admin":
                application = Constants.ADMIN_URL;
                break;
            case "promo":
                application = "https://promo.mamikos.com/";
                break;
			case "Custom Extend Contract":
				application = "https://padang2.kerupux.com/backoffice/contract/custom-extend/36321";
				break;
			case "GP Recurring Tools":
				application = "https://pay-jambu.kerupux.com/backoffice/testing-tools/goldplus-recurring";
				break;
            case "pms singgahsini":
                application = Constants.PMS_SINGGAHSINI_URL;
                break;
            case "singgahSini":
                application = Constants.SINGGAHSINI_URL;
                break;
			case "big flip bussiness":
				application = "https://business.flip.id/";
				break;
			default:
                try {
                    Assert.assertNotNull(application);
                } catch (Exception e) {
                    throw new IllegalArgumentException("Please provide valid direct access address");
                }
        }
        selenium.navigateToPage(application);
        selenium.hardWait(3);
        if (Constants.ENV.equals("prod")) {
            selenium.hardWait(8);
            banner.skipBannerPromPromoParty();
            homepage.clickOnLaterButton();
        }
    }

	@Then("user redirected to {string}")
	public void user_redirected_to(String url) throws InterruptedException {
		String actualUrl= selenium.getURL();
		if (url.charAt(0) == '/') {
			Assert.assertEquals(actualUrl, Constants.MAMIKOS_URL + url, "Url doesn't match");
		}
		else if (url.equals("owner page")) {
			if (Constants.ENV.equals("prod")) {
				Assert.assertEquals(actualUrl, ("https://owner." + Constants.MAMIKOS_URL.substring(8)) + "/", "Url doesn't match");
			}
			else {
				Assert.assertEquals(actualUrl, ("https://owner-" + Constants.MAMIKOS_URL.substring(8)) + "/", "Url doesn't match");
			}
		}
		else if (url.contains("owner /")) {
			if (Constants.ENV.equals("prod")) {
				Assert.assertEquals(actualUrl, ("https://owner." + Constants.MAMIKOS_URL.substring(8)) + url.substring(6), "Url doesn't match");
			}
			else {
				Assert.assertTrue(actualUrl.contains(("https://owner-" + Constants.MAMIKOS_URL.substring(8)) + url.substring(6)), "Url doesn't match");
			}
		}
		else if (url.contains("goldplus")) {
			Assert.assertEquals(actualUrl, ("https://mamikos.com/goldplus/"), "Url doesn't match");
		}

		else if (url.equals("upgrade gp")) {
			Assert.assertEquals(actualUrl, ("https://owner-" + Constants.MAMIKOS_URL.substring(8)) + "/goldplus/submission/packages/gp2?gp_upgrade_modal=true", "Url doesn't match");

		}

		else if (url.contains("twitter")) {
			Assert.assertEquals(actualUrl, ("https://twitter.com/mamikosapp"), "Url doesn't match");
		}
		else if (url.contains("instagram")) {
			if (Constants.ENV.equals("prod")) {
				Assert.assertEquals(actualUrl, ("https://www.instagram.com/accounts/login/"), "Url doesn't match");
			}else if (Constants.ENV.equals("prod")){
				Assert.assertEquals(actualUrl, ("https://www.instagram.com/accounts/login/?next=%2Fmamikosapp%2F"), "Url doesn't match");
			} else {
				Assert.assertEquals(actualUrl, ("https://www.instagram.com/mamikosapp/"), "Url doesn't match");
			}
		}
		else if (url.contains("facebook")) {
			Assert.assertEquals(actualUrl, ("https://www.facebook.com/mamikosapp/?fref=ts"), "Url doesn't match");
		}
		else if (url.contains("sanjunipero")) {
			Assert.assertEquals(actualUrl, ("https://jambu.kerupux.com/admin/sanjunipero/parent"), "Url doesn't match");
		}else if(url.contains("admin kos review")){
			Assert.assertEquals(actualUrl, (Constants.MAMIKOS_URL) + "/admin/review?#review", "Url doesn't match");
		}
		else if (url.contains("apartemen")) {
			Assert.assertEquals(actualUrl, ("https://jambu.kerupux.com/apartemen"), "Url doesn't match");
		}

		else if (url.equals("new owner dashboard page")) {
			if (Constants.ENV.equals("prod")) {
				Assert.assertEquals(actualUrl, ("https://owner." + Constants.MAMIKOS_URL2.substring(8)) + "/", "Url doesn't match");
			}
			else {
				Assert.assertEquals(actualUrl, ("https://owner-" + Constants.MAMIKOS_URL2.substring(8)) + "/", "Url doesn't match");
			}
		}
		else if (url.equals("gp submission")) {
			if (Constants.ENV.equals("prod")) {
				Assert.assertEquals(actualUrl, ("https://owner." + Constants.MAMIKOS_URL.substring(8)) + "/goldplus/submission", "Url doesn't match");
			}
			else {
				Assert.assertEquals(actualUrl, ("https://owner-" + Constants.MAMIKOS_URL.substring(8)) + "/goldplus/submission/packages", "Url doesn't match");
			}
		}
		else if (url.equals("dashboard gp")) {
			if (Constants.ENV.equals("prod")) {
				Assert.assertEquals(actualUrl, ("https://owner." + Constants.MAMIKOS_URL.substring(8)) + "/goldplus", "Url doesn't match");
			}
			else {
				Assert.assertEquals(actualUrl, ("https://owner-" + Constants.MAMIKOS_URL.substring(8)) + "/goldplus", "Url doesn't match");
			}
		}
		else if (url.contains("help")) {
			Assert.assertEquals(actualUrl, ("https://help.mamikos.com/"), "Url doesn't match");
		}
		else if (url.contains("broadcast chat faq")) {
			Assert.assertEquals(actualUrl, ("https://help.mamikos.com/category/pemilik/fitur-dan-layanan-lain/broadcast-chat"), "Url doesn't match");
		}

		else if (url.contains("bc selengkapnya")) {
			Assert.assertEquals(actualUrl, ("https://help.mamikos.com/post/berapa-kali-saya-bisa-mengirimkan-broadcast-chat"), "Url doesn't match");
		}

		else if (url.contains("owner HCenter")) {
			Assert.assertEquals(actualUrl, ("https://help.mamikos.com/pemilik"), "Url doesn't match");
		}
		else if (url.contains("kos review")){
			if (Constants.ENV.equals("prod")) {
				Assert.assertEquals(actualUrl, ("https://owner." + Constants.MAMIKOS_URL.substring(8)) + "/kos/reviews", "Url doesn't match");
			}
			else {
				Assert.assertEquals(actualUrl, ("https://owner-" + Constants.MAMIKOS_URL.substring(8)) + "/kos/reviews", "Url doesn't match");
			}
		} else if(url.contains("notification page")){
			Assert.assertEquals(actualUrl, Constants.MAMIKOS_URL + "/ownerpage/notification", "Url doesn't match");
		} else if(url.contains("add data kos")){
			Assert.assertEquals(actualUrl, ("https://owner-" + Constants.MAMIKOS_URL.substring(8)) + "/kos/create?prevPage=Iklan%20Saya", "Url doesn't match");
		}else if(url.contains("mamikos singgahsini")){
			selenium.switchToWindow(2);
			selenium.waitForJavascriptToLoad();
			url = selenium.getURL();
			selenium.hardWait(2);
			Assert.assertTrue(url.contains("/kos/singgahsini"), ("Url doesn't match"));
		}
		else if (url.contains("blog")) {
			Assert.assertEquals(actualUrl, ("https://mamikos.com/info/"), "Url doesn't match");
		}
		else if (url.contains("corporate")) {
			Assert.assertEquals(actualUrl, ("https://mamikos.com/info/mamikos-corporate-accommodation/"), "Url doesn't match");
		}
		else if (url.contains("kebijakan privasi")) {
			Assert.assertEquals(actualUrl, ("https://help.mamikos.com/post/kebijakan-privasi-mamikos"), "Url doesn't match");
		}
		else if (url.contains("syarat ketentuan")) {
			Assert.assertEquals(actualUrl, ("https://help.mamikos.com/post/syarat-dan-ketentuan-umum"), "Url doesn't match");
		}
		else if (url.contains("broadcast chat faq")) {
			Assert.assertEquals(actualUrl, ("https://help.mamikos.com/category/pemilik/fitur-dan-layanan-lain/broadcast-chat"), "Url doesn't match");
		}
		else if (url.contains("google play")) {
			Assert.assertEquals(actualUrl, ("https://play.google.com/store/apps/details?id=com.git.mami.kos&utm_campaign=DAppAndroFooter&utm_source=DownloadAppFooter&utm_medium=DownloadAppFooter&utm_term=DownloadAppFooter"), "Url doesn't match");
		}
		else if (url.contains("app store")) {
			selenium.hardWait(2);
			Assert.assertEquals(actualUrl, ("https://apps.apple.com/id/app/mami-kos/id1055272843"), "Url doesn't match");
		}
		else if (url.contains("whatsapp admin")) {
			Assert.assertEquals(actualUrl, ("https://api.whatsapp.com/send/?phone=6281325111171&text&type=phone_number&app_absent=0"), "Url doesn't match");
		}
		else if (url.contains("tentang-kami") || url.contains("career") || url.contains("mamiads")) {
			Assert.assertEquals(actualUrl, ("https://jambu.kerupux.com/" + url), "Url doesn't match");
		}
		else if  (url.contains("google maps")) {
			System.out.println(actualUrl);
			Assert.assertTrue(actualUrl.contains("https://www.google.com/maps/"));
		}
		else if (url.contains("whatsapp chat")){
			Assert.assertTrue(actualUrl.contains("https://api.whatsapp.com/"));
		}
		else if (url.equals("singgah sini property detail")){
			System.out.println(actualUrl);
			Assert.assertTrue(actualUrl.contains("https://sini-jambu.kerupux.com/property-detail/"));
		}
		else if (url.contains("disbursement")) {
			System.out.println(actualUrl);
			Assert.assertEquals(actualUrl, ("https://mamikos.com/info/informasi-pencairan-dana-dari-mamikos-ke-akun-pemilik-kos/?utm_medium=BannerEventOwner&utm_source=homebannerowner&utm_campaign=pencairandanakeakunpemilik"), "Url doesn't match");
		}
		else {
			Assert.assertEquals(actualUrl, url, "Url doesn't match");
		}
	}
	@When("user click Pusat Bantuan")
	public void userClickPusatBantuan() throws InterruptedException {
		helpCenter.clickhelpCenter();
	}

	@When("user click back button on device")
	public void user_click_back_button_on_device() {
		selenium.back();
	}

	@When("user/tenant/owner refresh page")
	public void x_refresh_page() {
		selenium.refreshPage();
	}

	@When("user/tenant/owner switch tab to {string}")
	public void x_switch_tab_to(String tab) {
		selenium.switchToWindow(Integer.parseInt(tab));
	}
}
