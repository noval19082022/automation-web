package steps.common;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.mamikos.common.HeaderPO;
import pageobjects.mamikos.tenant.profile.TenantProfilePO;
import pageobjects.mamikos.tenant.profile.BookingHistoryPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class BookingHistorySteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HeaderPO header = new HeaderPO(driver);
    private BookingHistoryPO history = new BookingHistoryPO(driver);
    private TenantProfilePO tenantProfile = new TenantProfilePO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private JavaHelpers java = new JavaHelpers();


    //Test Data
    private String propertyFile1="src/test/resources/testdata/mamikos/OB.properties";
    private String obOwner = "src/test/resources/testdata/occupancy-and-billing/ownerKost.properties";
    private String obTenant = "src/test/resources/testdata/occupancy-and-billing/tenant.properties";
    private String kostName = JavaHelpers.getPropertyValue(propertyFile1,"kostNameBookingFemale_" + Constants.ENV);
    private String duration = JavaHelpers.getPropertyValue(propertyFile1,"duration_" + Constants.ENV);
    private String paymentText = JavaHelpers.getPropertyValue(propertyFile1,"paymentText_" + Constants.ENV);
    private String status = JavaHelpers.getPropertyValue(propertyFile1,"statusAfterConfirmation_" + Constants.ENV);
    private String newlySeen = JavaHelpers.getPropertyValue(obOwner, "wendyWildRitKostName_" + Constants.ENV);
    private String tenantNameNeedConfirmation = JavaHelpers.getPropertyValue(obTenant, "saktiTenantName_" + Constants.ENV);
    private String tenantPhoneNeedConfirmation = JavaHelpers.getPropertyValue(obTenant, "saktiTenantPhone_" + Constants.ENV);

    //Teng Test Data
    private String tengOwner = "src/test/resources/testdata/mamikos/tenant-engagement-owner.properties";
    private String kostNameCheckin = "";
    private String kostNameAddsOnTarget = JavaHelpers.getPropertyValue(tengOwner, "addsOnKost_" + Constants.ENV);
    private String kostNameInvoiceDetailIrish = JavaHelpers.getPropertyValue(tengOwner, "invoiceDetailDPDepositAddFee_" + Constants.ENV);
    private String kostNameAddsOnRecurring = JavaHelpers.getPropertyValue(tengOwner, "invoiceDetailKostName_" + Constants.ENV);
    private String kostNameInvoiceDP = JavaHelpers.getPropertyValue(tengOwner, "invoiceDetailDPKostName_" + Constants.ENV);

    @When("user navigates to Booking History page")
    public void user_navigates_to_booking_history_page() throws InterruptedException {
        tenantProfile.clickOnBookingMenu();
    }

    @Then("booking is displayed with Status , Room name, Duration  Payment expiry time on Booking listing page")
    public void bookingIsDisplayedWithStatusRoomNameDurationPaymentExpiryTimeAsOnBookingListingPage() {
        Assert.assertEquals(history.getStatusValue(), status,"Status doesn't match");
        Assert.assertEquals(history.getRoomName(), kostName,"Room name doesn't match");
        Assert.assertEquals(history.getDuration(), duration,"Duration doesn't match");
        Assert.assertEquals(history.getPaymentExpiryTimeText(), paymentText,"Payment text doesn't match");
    }

    @Then("tenant/user/I should reached history booking page")
    public void x_should_reached_history_booking_page() throws InterruptedException{
        history.waitUntilHistoryBookingSectionVisible();
        Assert.assertTrue(history.isInHistoryBookingSection(), "You are not in history booking page");
    }

    @Then("tenant/user/I cancel active booking if it available")
    public void x_cancel_active_booking_if_it_available() throws InterruptedException{
        history.cancelActiveBooking();
    }

    @Then("user check booking status is rejected by owner with reason {string}")
    public void user_check_booking_status_is_rejected_by_owner(String rejecReason) throws InterruptedException {
        Assert.assertEquals(history.getBookingStatusHeaderFirstKostList(), "Pemilik Menolak");
        Assert.assertEquals(history.getRejectReasonOnDetailsFirstKostList(), rejecReason);
    }

    @Then("user check booking status is canceled with reason {string}")
    public void user_check_booking_status_is_canceled(String rejecReason) throws InterruptedException {
        Assert.assertEquals(history.getBookingStatusHeaderFirstKostList(), "Dibatalkan");
        Assert.assertEquals(history.getRejectReasonOnDetailsFirstKostList(), rejecReason);
    }

    @Then("tenant can sees need confirmation booking is for kost {string}")
    public void tenant_can_sees_need_confirmation_booking_is_for_kost(String kostName) {
        String kostNameBooking;
        if(kostName.equalsIgnoreCase("newly seen")) {
            kostNameBooking = newlySeen;
        }
        else {
            kostNameBooking = kostName;
        }
        Assert.assertEquals(history.getRoomNameNeedConfirmation(), kostNameBooking, "Name is not equal to " + kostNameBooking);
    }

    @When("tenant cancel all need confirmation booking request")
    public void tenant_cancel_all_need_confirmation_booking_request() throws InterruptedException {
        history.cancelAllBookingWithDefaultReason();
    }

    @Then("tenant get new notification and with text {string}")
    public void tenant_get_new_notification_and_with_text(String notifText) {
        history.clickNotifCenter();
        Assert.assertEquals(history.getFirstNotificationText(), notifText);
        history.closeNotification();
    }

    @Then("tenant can sees need confirmation status with kost is {string}")
    public void tenant_can_sees_need_confirmation_status_with_kost_is(String kostName) throws ParseException {
        String kostNameAssert = "";
        if (kostName.equalsIgnoreCase("need confirmation")) {
            kostNameAssert = newlySeen;
        }
        else {
            kostNameAssert = kostName;
        }
        String tomorrowDate = java.updateTimeLocal("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "d MMM yyyy", "en", 0, 1, 0, 0, 0);
        Assert.assertEquals(history.getRoomNameNeedConfirmation(), kostNameAssert);
        Assert.assertEquals(history.getRoomStatusNeedConfirmation(), "Kamar Belum Dikonfirmasi");
        Assert.assertEquals(history.getCheckinDateNeedConfirmation(), tomorrowDate);
        Assert.assertEquals(history.getRentDurationNeedConfirmation(), "1 Bulan");
    }

    @When("tenant clicks on chat button on need confirmation booking status")
    public void tenant_clicks_on_chat_button_on_need_confirmation_booking_status() {
        history.clicksOnNeedConfirmationChatButton();
    }

    @Then("tenant can sees chat box with kost is {string}")
    public void tenant_can_sees_chat_box_with_kost_is(String kostName) {
        String kostNameAssert = "";
        if (kostName.equalsIgnoreCase("need confirmation")) {
            kostNameAssert = newlySeen;
        }
        else {
            kostNameAssert = kostName;
        }
        Assert.assertEquals(history.getKostNameOnChatBox(), kostNameAssert);
        Assert.assertTrue(history.isBoxChat());
    }

    @When("tenant dismiss chat box")
    public void tenant_dismiss_chat_box() throws InterruptedException {
        history.dismissChatBox();
    }

    @When("tenant click on see complete button wait for confirmation booking")
    public void tenant_click_on_see_complete_button_wait_for_confirmation_booking() {
        history.clickOnSeeCompleteButtonNeedConfirmationBooking();
    }

    @Then("tenant can sees booking price, see facilities, new info, renter information, booking detail")
    public void tenant_can_sees_booking_price_see_facilities_new_info_renter_information_booking_detail() throws ParseException {
        String tomorrowDate = java.updateTimeLocal("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "d MMM yyyy", "en", 0,1, 0, 0, 0);
        String nextMonthTomorrowDate = java.updateTimeLocal("yyyy MMM dd", java.getTimeStamp("yyyy MMM dd"), "d MMM yyyy", "en", 1,1, 0, 0, 0);
        Assert.assertEquals(history.getExpandedBookingPrice(), "Rp900.000 /bulan");
        Assert.assertTrue(history.isSeeFacilitiesButton());
        Assert.assertTrue(history.isSeeNewInformationButton());
        Assert.assertEquals(history.getExpandedRenterName(), tenantNameNeedConfirmation);
        Assert.assertEquals(history.getExpandedPhoneNumber(), tenantPhoneNeedConfirmation);
        Assert.assertTrue(history.getExpandedBookingDetailElement("1").contains("MAMI"));
        Assert.assertTrue(history.getExpandedBookingDetailElement("2").contains(tomorrowDate));
        Assert.assertTrue(history.getExpandedBookingDetailElement("3").contains(nextMonthTomorrowDate));
        Assert.assertEquals(history.getExpandedBookingDetailElement("4"), "1 Bulan");
        Assert.assertEquals(history.getExpandedBookingDetailElement("5"), "Bulanan");
    }

    @When("tenant clicks on see facilities")
    public void tenant_clicks_on_see_facilities() throws InterruptedException {
        history.clicksOnExpandedFacilities();
    }

    @Then("tenant can sees kost facilities")
    public void tenant_can_sees_kost_facilities() throws InterruptedException {
        Assert.assertTrue(history.isFacilitiesBox());
        history.clicksOnPopUpXButton();
    }

    @When("tenant clicks on see new information")
    public void tenant_clicks_on_see_new_information() throws InterruptedException {
        history.clicksSeeNewestInformationExpandedBooking();
    }

    @When("tenant clicks on see less button")
    public void tenant_clicks_on_see_less_button() {
        history.clicksOnSeeLessButton();
    }

    @Then("tenant can sees details booking is collapsed")
    public void tenant_can_sees_details_booking_is_collapsed() {
        Assert.assertFalse(history.isBookingDetailExpanded());
    }

    @When("tenant click Bayar Sekarang button")
    public void tenant_click_Bayar_Sekarang_button() throws InterruptedException {
        history.clickOnNeedPaymentButton();
    }

    @When("tenant clicks on Bayar Pelunasan Sekarang button")
    public void tenant_clicks_on_Bayar_Pelunasan_Sekarang_button() throws InterruptedException {
        history.clickOnPayRepaymentButton();
    }

    @When("user check-in at kost {string}")
    public void user_check_in_at_kost(String kostName) throws InterruptedException {
        switch(kostName.toLowerCase()) {
            case "teng adds on":
                kostNameCheckin = kostNameAddsOnTarget;
                break;
            case "teng invoice detail":
                kostNameCheckin = kostNameInvoiceDetailIrish;
                break;
            case "teng adds on recurring":
                kostNameCheckin = kostNameAddsOnRecurring;
                break;
            case "teng invoice dp":
                kostNameCheckin = kostNameInvoiceDP;
                break;
            default:
                kostNameCheckin = kostName;
                break;
        }
        history.clickOnCheckinButtonKost(kostNameCheckin);
    }

}
