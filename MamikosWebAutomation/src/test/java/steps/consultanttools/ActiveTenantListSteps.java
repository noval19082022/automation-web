package steps.consultanttools;

import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.ActiveTenantListPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class ActiveTenantListSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JavaHelpers java = new JavaHelpers();
    private ActiveTenantListPO  activeTenant = new ActiveTenantListPO(driver);

    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String tenantProperty="src/test/resources/testdata/consultant/tenant.properties";
    private String tenantName = JavaHelpers.getPropertyValue(tenantProperty,"tenantName_yudha_" + Constants.ENV);
    private String kosName = JavaHelpers.getPropertyValue(consultantProperty,"kosName_tobelo_" + Constants.ENV);

    @When("user sort newest tenant")
    public void user_sort_newest_tenant() throws InterruptedException {
        activeTenant.clickOnFilterIcon();
        activeTenant.clickOnShortNewestTenant();
        activeTenant.closeShortOption();
    }

    @When("system display newest contract appears at the top of the contract list")
    public void system_display_newest_contract_appears_at_the_top_of_the_contract_list() throws ParseException {
        Assert.assertEquals(activeTenant.getTenantName(0), tenantName, "Tenant name not equals");
        Assert.assertEquals(activeTenant.getKosName(0), kosName, "Kos name not equals");
        String today = java.getTimeStamp("dd MMMM yyy");
        Assert.assertEquals(activeTenant.getDueDate(0), today, "Due date not equals");
    }
}
