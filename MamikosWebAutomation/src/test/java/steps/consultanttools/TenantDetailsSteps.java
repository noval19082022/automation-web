package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.ListPotentialTenantPO;
import pageobjects.consultanttools.TenantDetailsPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TenantDetailsSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JavaHelpers java = new JavaHelpers();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private TenantDetailsPO tenantDetails = new TenantDetailsPO(driver);
    private ListPotentialTenantPO listPotentialTenant = new ListPotentialTenantPO(driver);

    //Test Data Consultant
    private String potentialTenant="src/test/resources/testdata/consultant/potentialTenant.properties";

    //Default

    private String emailDefault = JavaHelpers.getPropertyValue(potentialTenant, "emailDefault_" + Constants.ENV);
    private String nameDefault = JavaHelpers.getPropertyValue(potentialTenant, "nameDefault_" + Constants.ENV);
    private String genderDefault = JavaHelpers.getPropertyValue(potentialTenant, "genderDefault_" + Constants.ENV);
    private String occupationDefault = JavaHelpers.getPropertyValue(potentialTenant, "occupationDefault_" + Constants.ENV);
    private String rentalPeriodDefault = JavaHelpers.getPropertyValue(potentialTenant, "rentalPeriodDefault_" + Constants.ENV);
    private String rentalPriceDefault = JavaHelpers.getPropertyValue(potentialTenant, "rentalPriceDefault_" + Constants.ENV);
    private String notesDefault = JavaHelpers.getPropertyValue(potentialTenant, "notesDefault_" + Constants.ENV);
    private String notesDetailsDefault = JavaHelpers.getPropertyValue(potentialTenant, "notesDetailsDefault_" + Constants.ENV);
    private String contractStatusDefault = JavaHelpers.getPropertyValue(potentialTenant, "contractStatusDefault_" + Constants.ENV);
    private String statusDefault = JavaHelpers.getPropertyValue(potentialTenant, "statusDefault_" + Constants.ENV);
    private String searchKosDefault = JavaHelpers.getPropertyValue(potentialTenant, "searchKosNameDefault_" + Constants.ENV);
    private String kosNameDefault = JavaHelpers.getPropertyValue(potentialTenant, "kosNameDefault_" + Constants.ENV);
    private String dueDateDefault = JavaHelpers.getPropertyValue(potentialTenant, "dueDateDefault_" + Constants.ENV);
    private String ownerNameDefault = JavaHelpers.getPropertyValue(potentialTenant, "ownerNameDefault_" + Constants.ENV);
    private String ownerPhoneNumberDefault = JavaHelpers.getPropertyValue(potentialTenant, "ownerPhoneNumberDefault_" + Constants.ENV);
    private String tenantPhoneNumberDefault = JavaHelpers.getPropertyValue(potentialTenant, "tenantPhoneNumberDefault_" + Constants.ENV);

    //Update
    private String emailUpdate = JavaHelpers.getPropertyValue(potentialTenant, "emailUpdate_" + Constants.ENV);
    private String nameUpdate = JavaHelpers.getPropertyValue(potentialTenant, "nameUpdate_" + Constants.ENV);
    private String genderUpdate = JavaHelpers.getPropertyValue(potentialTenant, "genderUpdate_" + Constants.ENV);
    private String occupationUpdate = JavaHelpers.getPropertyValue(potentialTenant, "occupationUpdate_" + Constants.ENV);
    private String rentalPeriodUpdate = JavaHelpers.getPropertyValue(potentialTenant, "rentalPeriodUpdate_" + Constants.ENV);
    private String rentalPriceUpdate = JavaHelpers.getPropertyValue(potentialTenant, "rentalPriceUpdate_" + Constants.ENV);
    private String notesUpdate = JavaHelpers.getPropertyValue(potentialTenant, "notesDetailsUpdate_" + Constants.ENV);
    private String notesDetailsUpdate = JavaHelpers.getPropertyValue(potentialTenant, "notesUpdate_" + Constants.ENV);
    private String contractStatusUpdate = JavaHelpers.getPropertyValue(potentialTenant, "contractStatusUpdate_" + Constants.ENV);
    private String statusUpdate = JavaHelpers.getPropertyValue(potentialTenant, "statusUpdate_" + Constants.ENV);
    private String searchKosUpdate = JavaHelpers.getPropertyValue(potentialTenant, "searchKosNameUpdate_" + Constants.ENV);
    private String kosNameUpdate = JavaHelpers.getPropertyValue(potentialTenant, "kosNameUpdate_" + Constants.ENV);
    private String dueDateUpdate = JavaHelpers.getPropertyValue(potentialTenant, "dueDateUpdate_" + Constants.ENV);
    private String ownerNameUpdate = JavaHelpers.getPropertyValue(potentialTenant, "ownerNameUpdate_" + Constants.ENV);
    private String ownerPhoneNumberUpdate = JavaHelpers.getPropertyValue(potentialTenant, "ownerPhoneNumberUpdate_" + Constants.ENV);
    private String tenantPhoneNumberUpdate = JavaHelpers.getPropertyValue(potentialTenant, "tenantPhoneNumberUpdate_" + Constants.ENV);

    @When("user revert data potential tenant")
    public void user_revert_data_potential_tenant() throws InterruptedException, ParseException {
        if (tenantDetails.getName().equals(nameUpdate)){
            tenantDetails.clickOnUpdateTenantDetailsButton();
            tenantDetails.enterEmail(emailDefault);
            tenantDetails.enterName(nameDefault);
            tenantDetails.selectGender(genderDefault);
            tenantDetails.selectOccupation(occupationDefault);
            tenantDetails.selectRentalPeriod(rentalPeriodDefault);
            tenantDetails.enterRentalPrice(rentalPriceDefault);
            String tomorrow = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
            tenantDetails.selectDate(tomorrow);
            tenantDetails.selectKos(searchKosDefault, kosNameDefault);
            tenantDetails.selectNotes(notesDefault);
            tenantDetails.enterAndSaveNotes(notesDetailsDefault);
            tenantDetails.clickSaveButton();
        }
    }

    @When("user update data potential tenant")
    public void user_update_data_potential_tenant() throws InterruptedException, ParseException {
        tenantDetails.clickOnUpdateTenantDetailsButton();
        tenantDetails.enterEmail(emailUpdate);
        tenantDetails.enterName(nameUpdate);
        tenantDetails.selectGender(genderUpdate);
        tenantDetails.selectOccupation(occupationUpdate);
        tenantDetails.selectRentalPeriod(rentalPeriodUpdate);
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        tenantDetails.selectDate(today);
        tenantDetails.selectKos(searchKosUpdate, kosNameUpdate);
        tenantDetails.enterRentalPrice(rentalPriceUpdate);
        tenantDetails.selectNotes(notesUpdate);
        tenantDetails.enterAndSaveNotes(notesDetailsUpdate);
        tenantDetails.clickSaveButton();
    }

    @Then("verify data potential tenant")
    public void verify_data_potential_tenant() throws InterruptedException, ParseException {
        selenium.hardWait(3);
        Assert.assertEquals(tenantDetails.getName(), nameUpdate, "Name not equals");
        Assert.assertEquals(tenantDetails.getStatus(), statusUpdate, "Status not equals");
        Assert.assertEquals(tenantDetails.getContract(), contractStatusUpdate, "Contract not equals");
        Assert.assertEquals(tenantDetails.getKosName().toLowerCase(), kosNameUpdate.toLowerCase(), "Kos Name not equals");
        Assert.assertEquals(tenantDetails.getRentalPeriod(), rentalPeriodUpdate,"Rental period not equals");
        Assert.assertEquals(Integer.toString(tenantDetails.getPrice()), rentalPriceUpdate, "Rental price not equals");
        Calendar cal = GregorianCalendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MMM yyyy");

        Date currentMonth = new Date();
        cal.setTime(currentMonth);

        // Add prev month
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1);
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)-1);
        String prevMonthAsString = df.format(cal.getTime());
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        if (today.length()==1){
            today =  "0" + today;
        }
        System.out.println(prevMonthAsString);
        today = today + " " + prevMonthAsString;
        Assert.assertEquals(tenantDetails.getDueDate(), today, "Due date not equals");
        Assert.assertEquals(tenantDetails.ownerName(), ownerNameUpdate, "Owner Name not equals");
        Assert.assertEquals(tenantDetails.getOwnersNumberPhone(), ownerPhoneNumberUpdate, "Owners number phone not equals");
        Assert.assertEquals(tenantDetails.getGender(), genderUpdate, "Gender not equals");
        Assert.assertEquals(tenantDetails.getTenantsPhoneNUmber(), tenantPhoneNumberUpdate, "Tenant number phone not equals");
        Assert.assertEquals(tenantDetails.getEmail(), emailUpdate, "Email not equals");
        Assert.assertEquals(tenantDetails.getOccupation(), occupationUpdate, "Occupation not equals");
        Assert.assertTrue(tenantDetails.getNotes().contains(notesUpdate), "Notes not equals");
        Assert.assertTrue(tenantDetails.getNotesDetails().contains(notesDetailsUpdate), "Notes details not equals");
    }

    @Then("verify data new potential tenant")
    public void verify_data_new_potential_tenant() throws InterruptedException {
        Assert.assertEquals(tenantDetails.getName(), nameDefault, "Name not equals");
        Assert.assertEquals(tenantDetails.getStatus(), statusDefault, "Status not equals");
        Assert.assertEquals(tenantDetails.getContract(), contractStatusDefault, "Contract not equals");
        Assert.assertEquals(tenantDetails.getKosName().trim(), kosNameDefault.trim(), "Kos Name not equals");
        Assert.assertEquals(tenantDetails.getRentalPeriod(), rentalPeriodDefault,"Rental period not equals");
        Assert.assertEquals(Integer.toString(tenantDetails.getPrice()), rentalPriceDefault, "Rental price not equals");
        String today = java.getTimeStamp("dd MMM YYYY");
        Assert.assertEquals(tenantDetails.getDueDate(), today, "Due date not equals");
        Assert.assertEquals(tenantDetails.ownerName(), ownerNameDefault, "Owner Name not equals");
        Assert.assertEquals(tenantDetails.getOwnersNumberPhone(), ownerPhoneNumberDefault, "Owners number phone not equals");
        Assert.assertEquals(tenantDetails.getGender(), genderDefault, "Gender not equals");
        Assert.assertEquals(tenantDetails.getTenantsPhoneNUmber(), tenantPhoneNumberDefault, "Tenant number phone not equals");
        Assert.assertEquals(tenantDetails.getEmail(), emailDefault, "Email not equals");
        Assert.assertEquals(tenantDetails.getOccupation(), occupationDefault, "Occupation not equals");
        Assert.assertTrue(tenantDetails.getNotes().contains(notesDefault), "Notes not equals");
        Assert.assertTrue(tenantDetails.getNotesDetails().contains(notesDetailsDefault), "Notes details not equals");
    }
}
