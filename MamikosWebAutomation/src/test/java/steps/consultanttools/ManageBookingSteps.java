package steps.consultanttools;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.ManageBookingPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class ManageBookingSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ManageBookingPO managebooking = new ManageBookingPO(driver);

    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String tenantProperty="src/test/resources/testdata/consultant/tenant.properties";
    private String tenantHPCrm = JavaHelpers.getPropertyValue(tenantProperty,"tenatHp_crm_" + Constants.ENV);
    private String tagihanDeposit = JavaHelpers.getPropertyValue(consultantProperty,"tagihan_deposit_" + Constants.ENV);
    private String tagihanDP = JavaHelpers.getPropertyValue(consultantProperty,"tagihan_dp_" + Constants.ENV);
    private String tagihanBiayaLain = JavaHelpers.getPropertyValue(consultantProperty,"tagihan_biayalain_" + Constants.ENV);

    @When("search booking data by Automation CRM")
    public void search_booking_data_by_Automation_CRM() throws InterruptedException {
        managebooking.searchBookingData(tenantHPCrm);
        managebooking.clickBookingTab("Butuh Konfirmasi");
    }

    @When("search booking data {string} by Automation CRM")
    public void search_booking_data_by_Automation_CRM(String tab) throws InterruptedException {
        managebooking.searchBookingData(tenantHPCrm);
        managebooking.clickBookingTab(tab);
    }

    @When("user see booking details")
    public void user_see_booking_details() throws InterruptedException {
        managebooking.clickLihatDetailButton();
    }

    @When("user accept booking")
    public void user_accept_booking() throws InterruptedException {
        managebooking.acceptBooking();
    }

    @When("user view contract details")
    public void user_view_contract_details() throws InterruptedException {
        managebooking.viewContract();
    }

    @When("user choose {string} room")
    public void user_choose_room(String room) throws InterruptedException {
        if(room.equalsIgnoreCase("Pilih ditempat")){
            managebooking.selectRoomPilihDitempat();
        } else {
            managebooking.selectRoomNumber(room);
        }
    }

    @When("user simpan kontrak")
    public void user_simpan_kontrak() throws InterruptedException {
        managebooking.clickSimpanKontrakButton();
    }

    @Then("user got error/success message {string}")
    public void user_got_error_message(String errorMessage) {
        Assert.assertEquals(managebooking.getRoomFullErrorMessage(),errorMessage,"error message is different");
    }

    @And("user reject booking")
    public void userRejectBooking() throws InterruptedException {
        managebooking.tolakBooking();
    }

    @And("user choose reason {string}")
    public void userChooseReason(String reason) throws InterruptedException {
        managebooking.chooseReason(reason);
    }

    @Then("booking status become {string}")
    public void bookingStatusBecome(String status) {
        Assert.assertEquals(status,managebooking.getBookingStatus(),"booking status is not equal");
    }

    @And("contains reason {string}")
    public void containsReason(String reason) {
        Assert.assertEquals(reason,managebooking.getRejectBookingReasonMessage(),"reject reason is not equal");
    }

    @And("room set as {string}")
    public void roomSetAs(String room) throws InterruptedException {
        managebooking.clickBackArrow();
        managebooking.clickBookingTab("Tunggu Pembayaran");
        Assert.assertEquals(managebooking.getRoomName(),room, "Room Name is different");
    }

    @And("user fill the reason {string}")
    public void userFillTheReason(String customReason) throws InterruptedException {
        managebooking.setCustomReason(customReason);
    }

    @Then("simpan button is active")
    public void simpanButtonIsActive() {
        managebooking.isSimpanButtonActive();
    }

    @And("user redirect to detail booking page")
    public void userRedirectToDetailBookingPage() {
        Assert.assertEquals(managebooking.getPageTitle(),"Detail Booking", "Page title not equal");
    }

    @And("user add deposit {string}")
    public void userAddDeposit(String deposit) {
        managebooking.addDeposit(deposit);
    }

    @And("user add denda keterlambatan {string} if it pass {string} {string}")
    public void userAddDendaKeterlambatanIfItPass(String fee, String value, String type) throws InterruptedException {
        managebooking.addDenda(fee,value,type);
    }

    @And("user add biaya lain {string} {string}")
    public void userAddBiayaLain(String name, String value) throws InterruptedException {
        managebooking.addBiayaLain(name,value);
    }

    @And("user add DP {string}")
    public void userAddDP(String value) throws InterruptedException {
        managebooking.addDP(value);
    }

    @And("deposit recorded in contract")
    public void depositRecordedInContract() {
        Assert.assertEquals(managebooking.getDepositInTagihan(),tagihanDeposit,"deposit is not as expected");
    }

    @And("DP recorded in contract")
    public void dpRecordedInContract() {
        Assert.assertEquals(managebooking.getDPInTagihan(),tagihanDP,"DP is not as expected");
    }

    @And("biaya lain recorded in contract")
    public void biayaLainRecordedInContract() {
        Assert.assertEquals(managebooking.getBiayaLainInTagihan(),tagihanBiayaLain,"Biaya lain is not as expected");
    }

    @When("user filter booking {string}")
    public void user_filter_booking(String propertyType) throws InterruptedException {
        managebooking.clickOnFilterIcon();
        managebooking.selectFilterPropertyType(propertyType);
        managebooking.clickOnApplyButton();
    }

    @Then("system display booking have property type {string}")
    public void system_display_booking_have_property_type(String propertyType) throws InterruptedException {
        int count = managebooking.countBookingList();
        for (int i = 0; i < count; i++){
            Assert.assertEquals(managebooking.getPropertyType(i), propertyType, "Property type not equals");
        }
    }
}
