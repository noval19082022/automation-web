package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.FilterActiveTaskPO;
import pageobjects.consultanttools.ManageTaskPO;
import utilities.ThreadManager;

public class FilterActiveTaskSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ManageTaskPO manageTask = new ManageTaskPO(driver);
    private FilterActiveTaskPO filterTask = new FilterActiveTaskPO(driver);

    @When("user filter active task is {string}")
    public void user_filter_active_task_is(String dataType) throws InterruptedException {
        manageTask.clickFilterButton();
        filterTask.clickFilterBy(dataType);
        filterTask.clickSubmitFilter();
    }

    @Then("system display active task with data type {string}")
    public void system_display_active_task_with_data_type(String dataType) throws InterruptedException {
        int numberOfList = manageTask.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            Assert.assertTrue(manageTask.getDataTypeTask(i).contains(dataType), "Tenant name not contains " + dataType);
            System.out.println("Ini = " + i + manageTask.getDataTypeTask(i));
        }
    }
}
