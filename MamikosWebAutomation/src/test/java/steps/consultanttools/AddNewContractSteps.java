package steps.consultanttools;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.AddNewContractPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class AddNewContractSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private AddNewContractPO newContract = new AddNewContractPO(driver);

    //test data
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String tenantProperty="src/test/resources/testdata/consultant/tenant.properties";
    private String propertyName = JavaHelpers.getPropertyValue(consultantProperty,"propertyName_"+ Constants.ENV);
    private String kosName = JavaHelpers.getPropertyValue(consultantProperty,"kosName_tobelo_" + Constants.ENV);
    private String tenantName = JavaHelpers.getPropertyValue(tenantProperty,"tenantName_yudha_" + Constants.ENV);
    private String  tenantHP = JavaHelpers.getPropertyValue(tenantProperty,"tenantHp_yudha_" + Constants.ENV);
    private String  tenantHpNorecord = JavaHelpers.getPropertyValue(tenantProperty,"tenantHp_noRecord_" + Constants.ENV);
    private String  tenantEmail = JavaHelpers.getPropertyValue(tenantProperty,"tenantEmail_yudha_" + Constants.ENV);
    private String  tenantJK = JavaHelpers.getPropertyValue(tenantProperty,"tenantJk_yudha_" + Constants.ENV);
    private String  tenantImg1 = JavaHelpers.getPropertyValue(tenantProperty,"tenantImage1_yudha_" + Constants.ENV);
    private String  tenantImg2 = JavaHelpers.getPropertyValue(tenantProperty,"tenantImage2_yudha_" + Constants.ENV);
    private String  tenantStatus = JavaHelpers.getPropertyValue(tenantProperty,"tenantStatus_yudha_" + Constants.ENV);
    private String  tenantOkupasi = JavaHelpers.getPropertyValue(tenantProperty,"tenantOkupasi_yudha_" + Constants.ENV);
    private String  errorMessageName = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageTenantName_" + Constants.ENV);
    private String  errorMessageNohp = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageTenantNohp_" + Constants.ENV);
    private String  errorMessageEmail = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageTenantEmail_" + Constants.ENV);
    private String  errorMessagePenanggungJawab = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageTenantPenanggungJawab_" + Constants.ENV);
    private String  errorMessageKontakPenanggungJawab = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageTenantKontakPenanggungJawab_" + Constants.ENV);

    @When("user input kos name {string}")
    public void user_input_kos_name(String property) throws InterruptedException {
        newContract.setTextOnFieldKosName(property);
        newContract.clickOnSearchPropertyButton();
    }

    @Then("system display back arrow and search kos button is disable")
    public void system_display_back_arrow_and_search_kos_button_is_disable() {
        newContract.backButtonArrowIsAppeared();
        newContract.formSearchPropertyIsDisable();
    }

    @When("user click on back button arrow")
    public void user_click_on_back_button_arrow() throws InterruptedException {
        newContract.clickOnBackButtonArrow();
    }

    @When("user input consultant kos name")
    public void user_input_consultant_kos_name() throws InterruptedException {
        newContract.setTextOnFieldKosName(kosName);
        newContract.clickOnSearchPropertyButton();
    }

    @Then("system display kos list {string}")
    public void system_display_Kos_list(String namaKos) throws InterruptedException {
        int numberOfList = newContract.getNumberOfList();

        for (int i = 1 ; i <= numberOfList ; i++){
            Assert.assertTrue(newContract.getKosNameLabel(i).toLowerCase().contains(namaKos.toLowerCase()), "Kos name not contains " + namaKos);
        }
    }

    @When("user choose kos consultant")
    public void user_choose_kos_consultant() throws InterruptedException {
        newContract.clickOnKostList(kosName);
        newContract.clickOnLanjutkanButton();
    }

    @When("user input registered phone number")
    public void user_input_registered_phone_number() throws InterruptedException {
        newContract.setTextOnFieldNoHp(tenantHP);
        newContract.clickOnLanjutkanButton();
    }

    @When("user input not registered phone number")
    public void user_input_not_registered_phone_number() throws InterruptedException {
        newContract.setTextOnFieldNoHp(tenantHpNorecord);
        newContract.clickOnLanjutkanButton();
    }

    @Then("user directed to create new contract form")
    public void user_directed_to_create_new_contract_form() {
        Assert.assertTrue(newContract.isFormCreateContractPageAppear(), "Form create contract page not appeared");
    }

    @Then("tenant data already filled")
    public void tenant_data_already_filled() {
        Assert.assertEquals(newContract.getTenantName(),tenantName,"Wrong name");
        Assert.assertEquals(newContract.getTenantHp(),tenantHP,"Wrong No HP");
        Assert.assertEquals(newContract.getTenantEmail(),tenantEmail,"Wrong email");
        Assert.assertEquals(newContract.getTenantJK(),tenantJK,"Wrong Gender");
        Assert.assertEquals(newContract.getTenantImg1(),tenantImg1,"Wrong First Image ");
        Assert.assertEquals(newContract.getTenantImg2(),tenantImg2,"Wrong Second Image");
        Assert.assertEquals(newContract.getTenantStatus(),tenantStatus, "Wrong Status");
        Assert.assertEquals(newContract.getTenantOkupasi(),tenantOkupasi,"Wrong Occupation");
    }

    @Then("tenant data is empty")
    public void tenant_data_is_empty() {
        Assert.assertEquals(newContract.getTenantName(),"","name not empty");
        Assert.assertEquals(newContract.getTenantHp(),tenantHpNorecord,"No HP not match");
        Assert.assertEquals(newContract.getTenantEmail(),"","email not empty");
        Assert.assertEquals(newContract.getTenantJK(),"Pilih jenis kelamin","Gender already chosen");
        Assert.assertFalse(newContract.getEmptyImg1());
        Assert.assertFalse(newContract.getEmptyImg2());
        Assert.assertEquals(newContract.getTenantStatus(),"Pilih status", "Status already chosen");
        Assert.assertEquals(newContract.getTenantOkupasi(),"Okupasi","Occupation already chosen");
    }

    @And("user empty {string}")
    public void user_empty(String field) throws InterruptedException {
        newContract.setEmpty(field);
    }

    @Then("there is a blank validation error message on {string}")
    public void there_is_a_blank_validation_error_message_on(String field) {
        switch (field) {
            case "fieldName":
                Assert.assertEquals(newContract.getNameError(), errorMessageName, "name error message not match");
                Assert.assertTrue(newContract.isSelanjutnyaButtonDisabled());
                break;
            case "fieldHP":
                Assert.assertEquals(newContract.getTenantHpError(), errorMessageNohp, "NoHP error message not match");
                Assert.assertTrue(newContract.isSelanjutnyaButtonDisabled());
                break;
            case "fieldEmail":
                Assert.assertEquals(newContract.getEmailError(), errorMessageEmail, "Email error message not match");
                Assert.assertTrue(newContract.isSelanjutnyaButtonDisabled());
                break;
            case "fieldGender":
            case "fieldStatus":
            case "fieldOccupation":
                Assert.assertTrue(newContract.isSelanjutnyaButtonDisabled());
                break;
            case "fieldNamaPenanggungJawab":
                Assert.assertEquals(newContract.getPenanggungJawabError(), errorMessagePenanggungJawab, "Penanggung Jawab error message not match");
                Assert.assertTrue(newContract.isSelanjutnyaButtonDisabled());
                break;
            case "fieldKontakPenanggungJawab":
                Assert.assertEquals(newContract.getKontakPenanggungJawabError(), errorMessageKontakPenanggungJawab, "Penanggung Jawab error message not match");
                Assert.assertTrue(newContract.isSelanjutnyaButtonDisabled());
                break;
            default:
                System.out.println("No Field Match");
        }
    }

    @Then("system display kos not found page")
    public void system_display_kos_not_found_page() throws InterruptedException {
        Assert.assertTrue(newContract.getKosEmptyPage());
    }

    @And("user skip personal data")
    public void userSkipPersonalData() throws InterruptedException {
        newContract.clickOnNextPersonalDataButton();
    }

    @And("user choose room number")
    public void userChooseRoomNumber() throws InterruptedException {
        newContract.setRoomNumberInContract();
    }

    @And("user choose check in date")
    public void userChooseCheckInDate() throws InterruptedException {
        newContract.setCheckInDateToday();
    }

    @And("user choose rent duration {string}")
    public void userChooseRentDuration(String durasi) throws InterruptedException {
        newContract.setRentDurationOneMonth(durasi);
    }

    @And("user add fixed cost {string} {string}")
    public void userAddFixedCost(String nama, String jumlah) throws InterruptedException {
        newContract.addFixedCost(nama,jumlah);
    }

    @Then("fixed cost added {string} {string}")
    public void fixedCostAdded(String name, String amount) {
        Assert.assertEquals(newContract.getBiayaTetapName(),name);
        Assert.assertEquals(newContract.getBiayaTetapAmount(),amount);
    }

    @And("user add additional cost {string} {string}")
    public void userAddAdditionalCost(String name, String amount) throws InterruptedException {
        newContract.addAdditionalCost(name,amount);
    }

    @Then("additional cost added {string} {string}")
    public void additionalCostAdded(String name, String amount) {
        Assert.assertEquals(newContract.getBiayaLainName(),name);
        Assert.assertEquals(newContract.getBiayaLainAmount(),amount);
    }

    @Then("system display penalty cost disappeared")
    public void system_display_penalty_cost_disappeared() {
        int duration = 3;
        Assert.assertFalse(newContract.penaltyCostIsNotInteractAble(duration), "Element is interact table");
        Assert.assertFalse(newContract.penaltyRangeIsNotInteractAble(duration), "Element is interact table");
        Assert.assertFalse(newContract.penaltyDurationIsNotInteractAble(duration), "Element is interact table");
    }

    @When("user click on penalty cost")
    public void user_activate_penalty_cost() throws InterruptedException {
        newContract.activatePenaltyCost();
    }

    @And("user add penalty cost {string} per {string} {string}")
    public void userAddPenaltyCostPer(String amount, String durasi, String unitDurasi) throws InterruptedException {
        newContract.addPenaltyCost(amount,durasi,unitDurasi);
    }

    @Then("system display save contract button is disable")
    public void system_display_save_contract_button_is_disable() throws InterruptedException {
        Assert.assertTrue(newContract.saveContractButtonIsDisable());
    }

    @When("user input amount penalty cost {string}")
    public void user_input_amount_penalty_cost(String amount) throws InterruptedException {
        newContract.enterAmountPenaltyCost(amount);
    }

    @When("user input range penalty cost")
    public void user_input_range_penalty_cost() throws InterruptedException {
        newContract.enterRangePenaltyCost("1");
    }

    @When("user input range penalty cost {string} and period penalty cost {string}")
    public void user_input_range_penalty_cost_and_period_penalty_cost(String range, String period) throws InterruptedException {
        newContract.enterRangeAndPeriodPenaltyCost(range, period);
    }

    @Then("system display error message range duration {string}")
    public void system_display_error_message_range_duration(String message) {
        Assert.assertEquals(newContract.getDurationErrorMessage(), message, "Message not match");
    }

    @And("user click tidak terimakasih button")
    public void userClickTidakTerimakasihButton() throws InterruptedException {
        newContract.clickOnTidakTerimakasih();
    }

    @And("user lihat detail kontrak")
    public void userLihatDetailKontrak() throws InterruptedException {
        newContract.clickOnLihatDetailKontrak();
    }

    @And("user ubah biaya lain")
    public void userUbahBiayaLain() throws InterruptedException {
        newContract.clickOnUbahBiayaLain();
    }

    @Then("fixed cost contains {string} {string}")
    public void fixedCostContains(String name, String amount) {
        Assert.assertEquals(newContract.getFixCostName(),name,"fix cost name is different");
        Assert.assertEquals(newContract.getFixCostAmount(),amount,"fix cost amount is different");
    }

    @And("additional cost contains {string} {string}")
    public void additionalCostContains(String name, String amount) {
        Assert.assertEquals(newContract.getAdditionalName(),name,"additional cost name is different");
        Assert.assertEquals(newContract.getAdditionalAmount(),amount,"additional cost amount is different");
    }

    @Then("no fixed cost added")
    public void no_fixed_cost_added() {
        Assert.assertFalse(newContract.isTambahanBiayaTetapAppear());
    }

    @Then("no additional cost added")
    public void no_additional_cost_added() {
        Assert.assertFalse(newContract.isTambahanBiayaLainAppear());
    }

    @Then("system display property not found")
    public void system_display_property_not_found() {
        newContract.propertyNotFound();
    }

    @Then("system display form search property")
    public void system_display_form_search_property() {
        newContract.formSearchPropertyIsAppeared();
    }

    @Then("system display next button add new contract is disable")
    public void system_display_next_button_add_new_contract_is_disable() {
        newContract.nextButtonIsDisable();
    }
}
