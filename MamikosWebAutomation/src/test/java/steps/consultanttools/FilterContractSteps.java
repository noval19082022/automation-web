package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.FilterContractPO;
import pageobjects.consultanttools.InvoicePO;
import pageobjects.consultanttools.ManageContractPO;
import utilities.ThreadManager;

import java.text.ParseException;

public class FilterContractSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private ManageContractPO manageContract = new ManageContractPO(driver);
    private FilterContractPO filterContract = new FilterContractPO(driver);
    private InvoicePO invoice = new InvoicePO(driver);

    @When("user filter contract with contract status is {string}")
    public void user_filter_contract_with_contract_status_is(String contractStatus) throws InterruptedException, ParseException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(contractStatus);
//        String twoWeekAgo = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", -14, 0, 0, 0);
//        filterContract.selectStartDate(twoWeekAgo);
        filterContract.clickOnSubmitFilter();
    }

    @When("user filter contract with payment status is {string}")
    public void user_filter_contract_with_payment_status_is(String paymentStatus) throws InterruptedException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(paymentStatus);
        filterContract.clickOnSubmitFilter();
    }

    @When("user filter contract with created by {string}")
    public void user_filter_contract_with_created_by(String createdBy) throws InterruptedException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(createdBy);
        filterContract.clickOnSubmitFilter();
    }

    @Then("system display contract list with contract status is {string}")
    public void system_display_contract_list_with_contract_status_is(String contractStatus) throws InterruptedException {
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            System.out.println("Lisa Data ke " + i);
            Assert.assertEquals(manageContract.getLeftStatus(i), contractStatus, "Property contract status " + i + " not " + contractStatus);
        }
    }

    @Then("system display contract list with payment status is {string}")
    public void system_display_contract_list_with_payment_status_is(String paymentStatus) throws InterruptedException {
        String invoiceStatus;
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            System.out.println("Lisa Data ke " + i);
            if (paymentStatus.equals("Dibayar di Luar MamiPAY")){
                invoiceStatus = "Bayar di Luar MamiPAY";
                if (!manageContract.rightStatusIsPresent(i)) {
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(paymentStatus), "Not found invoice status " + paymentStatus + " on contract " + i);
                } else {
                    Assert.assertEquals((manageContract.getRightStatus(i)), invoiceStatus, "Contract status property " + i + " not " + invoiceStatus);
                }
            }else{
                if (!manageContract.getLeftStatus(i).equals(paymentStatus)) {
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(paymentStatus), "Not found invoice status " + paymentStatus + " on contract " + i);
                }else {
                    Assert.assertEquals((manageContract.getLeftStatus(i)), paymentStatus, "Contract status property " + i + " not " + paymentStatus);
                }
            }
        }
    }

    @Then("system display contract list with contract status is {string} or {string}")
    public void system_display_contract_list_with_contract_status_is_or(String paidStatus, String notPaidStatus) throws InterruptedException {
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            System.out.println("Lisa Data ke " + i);
            String leftStatus = manageContract.getLeftStatus(i);
            if(leftStatus.equals(paidStatus)) {
                Assert.assertEquals(manageContract.getLeftStatus(i), paidStatus, "Contract status not " + paidStatus);
            } else {
                Assert.assertEquals(manageContract.getLeftStatus(i), notPaidStatus, "Contract status not  " + notPaidStatus);
            }
            Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
        }
    }

    @Then("system display contract list not created by {string} or {string}")
    public void system_display_contract_list_not_created_by_or(String createdBy1, String createdBy2) throws InterruptedException {
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            System.out.println("Lisa Data ke " + i);
            Assert.assertNotEquals(manageContract.getCreatedBy(i), createdBy1, "Created by " + createdBy1);
            Assert.assertNotEquals(manageContract.getCreatedBy(i), createdBy2, "Created by " + createdBy2);
        }
    }

    @Then("system display contract list with created by {string}")
    public void system_display_contract_list_with_created_by(String createdBy) throws InterruptedException {
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            System.out.println("Lisa Data ke " + i);
            Assert.assertEquals(manageContract.getCreatedBy(i), createdBy, "Not created by " + createdBy);
        }
    }

    @When("user filter contract by {string} and {string}")
    public void user_filter_contract_by_and(String filter1, String filter2) throws InterruptedException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(filter1);
        filterContract.clickOnFilterOption(filter2);
        filterContract.clickOnSubmitFilter();
    }

    @When("user filter contract by {string} and {string} and {string}")
    public void user_filter_contract_by_and_and(String filter1, String filter2, String filter3) throws InterruptedException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(filter1);
        filterContract.clickOnFilterOption(filter2);
        filterContract.clickOnFilterOption(filter3);
        filterContract.clickOnSubmitFilter();
    }

    @Then("system displays list of contracts according to the two selected filter criteria {string}")
    public void system_displays_list_of_contracts_according_to_the_two_selected_filter_criteria(String filterResult) throws InterruptedException {
        String invoiceStatus;
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            switch (filterResult){
                case "Aktif, Dibatalkan":
                    if (!manageContract.terminateContractButtonIsPresent(i)) {
                        Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    }else{
                        Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    }
                    break;
                case "Aktif, Sewa Berakhir":
                    if (!manageContract.terminateContractButtonIsPresent(i)){
                        Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    }else{
                        Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    }
                    break;
                case "Dibatalkan, Sewa Berakhir":
                    if (manageContract.getLeftStatus(i).equals("Dibatalkan")){
                        Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    }else{
                        Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    }
                    break;
                case "Aktif, Dibayar di Luar MamiPAY":
                    invoiceStatus = "Dibayar di Luar MamiPAY";
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    if (manageContract.rightStatusIsPresent(i)){
                        Assert.assertEquals(manageContract.getRightStatus(i), "Bayar di Luar MamiPAY", "Filter contract is failed");
                    }else{
                        manageContract.clickOnPropertyList(i);
                        Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    }
                    break;
                case "Dibatalkan, Dibayar di Luar MamiPAY":
                    invoiceStatus = "Dibayar di Luar MamiPAY";
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    break;
                case "Sewa Berakhir, Dibayar di Luar MamiPAY":
                    invoiceStatus = "Dibayar di Luar MamiPAY";
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    break;
                case "Aktif, Dibayar":
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibayar", "Filter contract is failed");
                    break;
                case "Dibatalkan, Dibayar":
                    invoiceStatus = "Dibayar";
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    break;
                case "Sewa Berakhir, Dibayar":
                    invoiceStatus = "Dibayar";
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    break;
                case "Aktif, Belum Dibayar":
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Belum Dibayar", "Filter contract is failed");
                    break;
                case "Dibatalkan, Belum Dibayar":
                    invoiceStatus = "Belum Dibayar";
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    break;
                case "Sewa Berakhir, Belum Dibayar":
                    invoiceStatus = "Belum Dibayar";
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    break;
                case "Aktif, Konsultan":
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                    break;
                case "Dibatalkan, Konsultan":
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                    break;
                case "Sewa Berakhir, Konsultan":
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                    break;
                case "Aktif, Calon Penyewa":
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                    break;
                case "Dibatalkan, Calon Penyewa":
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                    break;
                case "Sewa Berakhir, Calon Penyewa":
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                    break;
                case "Aktif, Pemilik Kos":
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                    break;
                case "Dibatalkan, Pemilik Kos":
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                    break;
                case "Sewa Berakhir, Pemilik Kos":
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                    break;
            }
        }
    }

    @Then("system displays list of contracts according to the three selected filter criteria {string}")
    public void system_displays_list_of_contracts_according_to_the_three_selected_filter_criteria(String filterResult) throws InterruptedException {
        String invoiceStatus;
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            if ("Aktif, Dibatalkan, Sewa Berakhir".equals(filterResult)) {
                if (manageContract.terminateContractButtonIsPresent(i)) {
                    if (manageContract.getLeftStatus(i).equals("Dibayar")) {
                        Assert.assertEquals(manageContract.getLeftStatus(i), "Dibayar", "Filter contract is failed");
                    } else {
                        Assert.assertEquals(manageContract.getLeftStatus(i), "Belum Dibayar", "Filter contract is failed");
                    }
                } else {
                    if (manageContract.getLeftStatus(i).equals("Dibatalkan")) {
                        Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    } else {
                        Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    }
                }
            }else if (filterResult.equals("Aktif, Dibatalkan, Dibayar di Luar MamiPAY")){
                invoiceStatus = "Dibayar di Luar MamiPAY";
                if (!manageContract.rightStatusIsPresent(i)) {
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }else{
                    Assert.assertEquals(manageContract.getRightStatus(i), "Bayar di Luar MamiPAY", "Payment status invoice not Bayar di Luar MamiPay");
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }
                if (manageContract.getLeftStatus(i).equals("Dibatalkan")){
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }
            }else if (filterResult.equals("Dibatalkan, Sewa Berakhir, Dibayar di Luar MamiPAY")){
                invoiceStatus = "Dibayar di Luar MamiPAY";
                if (manageContract.getLeftStatus(i).equals("Dibatalkan")){
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }
            } else if (filterResult.equals("Aktif, Sewa Berakhir, Dibayar di Luar MamiPAY")){
                invoiceStatus = "Dibayar di Luar MamiPAY";
                if (manageContract.rightStatusIsPresent(i)){
                    Assert.assertEquals(manageContract.getRightStatus(i), "Bayar di Luar MamiPAY", "Payment status invoice " + i + " not Dibayar di Luar MamiPAY");
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }
            }else if (filterResult.equals("Aktif, Dibatalkan, Dibayar")){
                invoiceStatus = "Dibayar";
                if (manageContract.getLeftStatus(i).equals("Dibatalkan")){
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibayar", "Filter contract is failed");
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }
            }else if (filterResult.equals("Dibatalkan, Sewa Berakhir, Dibayar")){
                invoiceStatus = "Dibayar";
                if (manageContract.getLeftStatus(i).equals("Dibatalkan")){
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Aktif, Sewa Berakhir, Dibayar")){
                invoiceStatus = "Dibayar";
                if (manageContract.getLeftStatus(i).equals("Dibayar")){
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }
            }else if (filterResult.equals("Aktif, Dibatalkan, Belum Dibayar")){
                invoiceStatus = "Belum Dibayar";
                if (manageContract.getLeftStatus(i).equals("Belum Dibayar")){
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Dibatalkan, Sewa Berakhir, Belum Dibayar")){
                invoiceStatus = "Belum Dibayar";
                if (manageContract.getLeftStatus(i).equals("Dibatalkan")){
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            } else if (filterResult.equals("Aktif, Sewa Berakhir, Belum Dibayar")){
                invoiceStatus = "Belum Dibayar";
                if (manageContract.getLeftStatus(i).equals("Belum Dibayar")){
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Aktif, Dibatalkan, Konsultan")){
                Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
            }else if (filterResult.equals("Dibatalkan, Sewa Berakhir, Konsultan")){
                Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
            }else if (filterResult.equals("Aktif, Sewa Berakhir, Konsultan")){
                if (manageContract.getLeftStatus(i).equals("Sewa Berakhir")){
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                }else{
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
            }else if (filterResult.equals("Aktif, Dibatalkan, Calon Penyewa")){
                if (manageContract.getLeftStatus(i).equals("Dibatalkan")){
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                }else{
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }
                Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
            }else if (filterResult.equals("Dibatalkan, Sewa Berakhir, Calon Penyewa")){
                if (manageContract.getLeftStatus(i).equals("Dibatalkan")){
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                }
                Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
            }else if (filterResult.equals("Aktif, Sewa Berakhir, Calon Penyewa")){
                if (manageContract.getLeftStatus(i).equals("Sewa Berakhir")){
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                }else{
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }
                Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
            }else if (filterResult.equals("Aktif, Dibatalkan, Pemilik Kos")){
                Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
            }else if (filterResult.equals("Dibatalkan, Sewa Berakhir, Pemilik Kos")){
                if (manageContract.getCreatedBy(i).equals("Pemilik Kos")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                }else{
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }
                Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
            }else if (filterResult.equals("Aktif, Sewa Berakhir, Pemilik Kos")){
                if (manageContract.getLeftStatus(i).equals("Sewa Berakhir")){
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                }else{
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                }
                Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
            }else if (filterResult.equals("Aktif, Dibayar di Luar MamiPAY, Dibayar")){
                Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                Assert.assertEquals(manageContract.getLeftStatus(i),"Dibayar", "Payment status not Dibayar ");
            }else if (filterResult.equals("Aktif, Dibayar, Belum Dibayar")){
                Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                if (manageContract.getLeftStatus(i).equals("Dibayar")){
                    Assert.assertEquals(manageContract.getLeftStatus(i),"Dibayar", "Payment status not Dibayar ");
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i),"Belum Dibayar", "Payment status not Dibayar");
                }
            }else if (filterResult.equals("Aktif, Dibayar di Luar MamiPAY, Belum Dibayar")){
                Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                Assert.assertEquals(manageContract.getLeftStatus(i),"Belum Dibayar", "Payment status not Dibayar");
            }else if (filterResult.equals("Dibatalkan, Dibayar di Luar MamiPAY, Dibayar")){
                invoiceStatus = "Dibayar";
                Assert.assertEquals(manageContract.getLeftStatus(i),"Dibatalkan", "Payment status not Dibayar");
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Sewa Berakhir, Dibayar di Luar MamiPAY, Dibayar")){
                invoiceStatus = "Dibayar";
                Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Dibatalkan, Dibayar, Belum Dibayar")){
                Assert.assertEquals(manageContract.getLeftStatus(i),"Dibatalkan", "Payment status not Dibatalkan");
            }else if (filterResult.equals("Dibatalkan, Dibayar di Luar MamiPAY, Belum Dibayar")){
                invoiceStatus = "Belum Dibayar";
                Assert.assertEquals(manageContract.getLeftStatus(i),"Dibatalkan", "Payment status not Dibatalkan");
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Sewa Berakhir, Dibayar di Luar MamiPAY, Dibayar ")){
                invoiceStatus = "Dibayar";
                Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Sewa Berakhir, Dibayar, Belum Dibayar")){
                Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
            }else if (filterResult.equals("Sewa Berakhir, Dibayar di Luar MamiPAY, Belum Dibayar")){
                Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
            }else if (filterResult.equals("Dibayar di Luar MamiPAY, Dibayar, Konsultan")){
                invoiceStatus = "Dibayar";
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Dibayar, Belum Dibayar , Konsultan ")){
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
            }else if (filterResult.equals("Dibayar di Luar MamiPAY, Belum Dibayar, Konsultan")){
                invoiceStatus = "Dibayar di Luar MamiPAY";
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                if (manageContract.getLeftStatus(i).equals("Dibayar")){
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }else if (manageContract.getLeftStatus(i).equals("Sewa Berakhir")){
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice("Belum Dibayar"), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }else {
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Belum Dibayar", "Filter contract is failed");
                }
            }else if (filterResult.equals("Dibayar di Luar MamiPAY, Dibayar, Calon Penyewa")){
                invoiceStatus = "Dibayar";
                Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                if (manageContract.getLeftStatus(i).equals("Dibayar")){
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibayar", "Filter contract is failed");
                }else {
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }
            }else if (filterResult.equals("Dibayar, Belum Dibayar, Calon Penyewa")){
                Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
            }else if (filterResult.equals("Dibayar di Luar MamiPAY, Belum Dibayar, Calon Penyewa")){
                invoiceStatus = "Belum Dibayar";
                Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                if (manageContract.getLeftStatus(i).equals("Dibayar")){
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice("Dibayar di Luar MamiPAY"), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }else{
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }
            }else if (filterResult.equals("Dibayar di Luar MamiPAY, Dibayar, Pemilik Kos")){
                invoiceStatus = "Dibayar";
                Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                if (manageContract.getLeftStatus(i).equals("Sewa Berakhir")){
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }else{
                    Assert.assertEquals(manageContract.getLeftStatus(i), invoiceStatus, "Filter contract is failed");
                }
            }else if (filterResult.equals("Dibayar, Belum Dibayar, Pemilik Kos")){
                Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
            }else if (filterResult.equals("Dibayar di Luar MamiPAY, Belum Dibayar, Pemilik Kos")){
                invoiceStatus = "Belum Dibayar";
                Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                if (manageContract.getLeftStatus(i).equals("Dibayar")){
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice("Dibayar di Luar MamiPAY"), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }else{
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                }
            }else if (filterResult.equals("Aktif, Konsultan, Calon Penyewa")){
                Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa"))
                Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                else{
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                }
            }else if (filterResult.equals("Aktif, Calon Penyewa, Pemilik Kos")){
                Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa"))
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                else{
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by not Pemilik Kos");
                }
            }else if (filterResult.equals("Aktif, Konsultan, Pemilik Kos")){
                Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                if (manageContract.getCreatedBy(i).equals("Pemilik Kos"))
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                else{
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                }
            }else if (filterResult.equals("Dibatalkan, Konsultan, Calon Penyewa")){
                Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
            }else if (filterResult.equals("Dibatalkan, Calon Penyewa, Pemilik Kos")){
                Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa")) {
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }else{
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                }
            }else if (filterResult.equals("Dibatalkan, Konsultan, Pemilik Kos")){
                Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
            }else if (filterResult.equals("Sewa Berakhir, Konsultan, Calon Penyewa")){
                Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa")) {
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }else{
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                }
            }else if (filterResult.equals("Sewa Berakhir, Calon Penyewa, Pemilik Kos")){
                Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa")) {
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }else{
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                }
            }else if (filterResult.equals("Sewa Berakhir, Konsultan, Pemilik Kos")){
                Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                if (manageContract.getCreatedBy(i).equals("Pemilik Kos")) {
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                }else{
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                }
            }else if (filterResult.equals("Dibayar di Luar MamiPAY, Konsultan, Calon Penyewa")){
                invoiceStatus = "Dibayar di Luar MamiPAY";
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }else{
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Dibayar di Luar MamiPAY, Calon Penyewa, Pemilik Kos")){
                invoiceStatus = "Dibayar di Luar MamiPAY";
                if (manageContract.getCreatedBy(i).equals("Pemilik Kos")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                }else{
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Dibayar di Luar MamiPAY, Konsultan, Pemilik Kos")){
                invoiceStatus = "Dibayar di Luar MamiPAY";
                if (manageContract.getCreatedBy(i).equals("Pemilik Kos")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                }else{
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Dibayar, Konsultan, Calon Penyewa")){
                invoiceStatus = "Dibayar";
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }else{
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Dibayar, Calon Penyewa, Pemilik Kos")){
                invoiceStatus = "Dibayar";
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }else{
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Dibayar, Konsultan, Pemilik Kos")){
                invoiceStatus = "Dibayar";
                if (manageContract.getCreatedBy(i).equals("Pemilik Kos")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                }else{
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Belum Dibayar, Konsultan, Calon Penyewa")){
                invoiceStatus = "Belum Dibayar";
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }else{
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Belum Dibayar, Calon Penyewa, Pemilik Kos")){
                invoiceStatus = "Belum Dibayar";
                if (manageContract.getCreatedBy(i).equals("Calon Penyewa")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                }else{
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }else if (filterResult.equals("Belum Dibayar, Konsultan, Pemilik Kos ")){
                invoiceStatus = "Belum Dibayar";
                if (manageContract.getCreatedBy(i).equals("Pemilik Kos")){
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                }else{
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                }
                manageContract.clickOnPropertyList(i);
                Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
            }
        }
    }

    @Then("system display contract list according to the filter criteria which contract status {string} and {string}")
    public void system_display_contract_list_according_to_the_filter_criteria_which_contract_status_and(String cancelStatus, String terminateStatus) throws InterruptedException {
        int numberOfList = manageContract.getNumberOfList();
        for (int i = 1; i <= numberOfList; i++) {
            String leftStatus = manageContract.getLeftStatus(i);
            if(leftStatus.equals(cancelStatus)) {
                Assert.assertEquals(manageContract.getLeftStatus(i), cancelStatus, "Contract status not " + cancelStatus);
            } else if (leftStatus.equals(terminateStatus)) {
                Assert.assertEquals(manageContract.getLeftStatus(i), terminateStatus, "Contract status not  " + terminateStatus);
            } else{
                Assert.assertNotEquals(manageContract.getCreatedBy(i), terminateStatus, "Created by " + terminateStatus);
            }
        }
    }

    @When("user filter contract by {string} or {string} or {string}")
    public void user_filter_contract_by_or_or(String filterBy1, String filterBy2, String filterBy3) throws InterruptedException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(filterBy1);
        filterContract.clickOnFilterOption(filterBy2);
        filterContract.clickOnFilterOption(filterBy3);
        filterContract.clickOnSubmitFilter();
    }

    @When("user filter contract by {string} or {string} and {string}")
    public void user_filter_contract_by_or_and(String filterBy1, String filterBy2, String filterBy3) throws InterruptedException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(filterBy1);
        filterContract.clickOnFilterOption(filterBy2);
        filterContract.clickOnFilterOption(filterBy3);
        filterContract.clickOnSubmitFilter();
    }

    @When("user filter contract by {string} and {string} or {string}")
    public void user_filter_contract_by_and_or(String filterBy1, String filterBy2, String filterBy3) throws InterruptedException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(filterBy1);
        filterContract.clickOnFilterOption(filterBy2);
        filterContract.clickOnFilterOption(filterBy3);
        filterContract.clickOnSubmitFilter();
    }

    @Then("system display contract list according to the filter criteria which contract status {string} or {string} or {string} or {string}")
    public void system_display_contract_list_according_to_the_filter_criteria_which_contract_status_or_or_or(String paidStatus, String notPaidStatus, String cancelStatus, String terminateStatus) throws InterruptedException {
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            String leftStatus = manageContract.getLeftStatus(i);
            if(leftStatus.equals(paidStatus)) {
                Assert.assertEquals(manageContract.getLeftStatus(i), paidStatus, "Contract status not " + paidStatus);
            } else if(leftStatus.equals(notPaidStatus)){
                Assert.assertEquals(manageContract.getLeftStatus(i), notPaidStatus, "Contract status not  " + notPaidStatus);
            } else if(leftStatus.equals(cancelStatus)){
                Assert.assertEquals(manageContract.getLeftStatus(i), cancelStatus, "Contract status not  " + cancelStatus);
            }else{
                Assert.assertEquals(manageContract.getLeftStatus(i), terminateStatus, "Contract status not  " + terminateStatus);
            }
        }
    }

    @When("user filter contract by {string} and {string} and {string} and {string}")
    public void user_filter_contract_by_and_and_and(String filterBy1, String filterBy2, String filterBy3, String filterBy4) throws InterruptedException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(filterBy1);
        filterContract.clickOnFilterOption(filterBy2);
        filterContract.clickOnFilterOption(filterBy3);
        filterContract.clickOnFilterOption(filterBy4);
        filterContract.clickOnSubmitFilter();
    }

    @Then("system displays list of contracts according to the four selected filter criteria {string}")
    public void system_displays_list_of_contracts_according_to_the_four_selected_filter_criteria(String filterResult) throws InterruptedException {
        String invoiceStatus;
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            switch (filterResult) {
                case "Aktif, Dibatalkan, Sewa Berakhir, Dibayar di Luar MamiPAY":
                case "Dibayar di Luar MamiPAY, Konsultan, Calon Penyewa, Pemilik Kos":
                    invoiceStatus = "Dibayar di Luar MamiPAY";
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    break;
                case "Aktif, Dibatalkan, Sewa Berakhir, Dibayar":
                case "Dibayar, Konsultan, Calon Penyewa, Pemilik Kos":
                    invoiceStatus = "Dibayar";
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    break;
                case "Aktif, Dibatalkan, Sewa Berakhir, Belum Bayar":
                case "Belum Dibayar, Konsultan, Calon Penyewa, Pemilik Kos":
                    invoiceStatus = "Belum Dibayar";
                    manageContract.clickOnPropertyList(i);
                    Assert.assertTrue(invoice.findInvoice(invoiceStatus), "Not found invoice status " + invoiceStatus + " on contract " + i);
                    break;
                case "Aktif, Dibatalkan, Sewa Berakhir, Konsultan":
                case "Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar, Konsultan":
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created by Pemilik Kos");
                    Assert.assertNotEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created by Calon Penyewa");
                    break;
                case "Aktif, Dibatalkan, Sewa Berakhir, Calon Penyewa":
                case "Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar, Calon Penyewa":
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Calon Penyewa", "Created not by Calon Penyewa");
                    break;
                case "Aktif, Dibatalkan, Sewa Berakhir, Pemilik Kos":
                case "Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar, Pemilik Kos":
                    Assert.assertEquals(manageContract.getCreatedBy(i), "Pemilik Kos", "Created not by Pemilik Kos");
                    break;
                case "Aktif, Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar ":
                    if (manageContract.getLeftStatus(i).equalsIgnoreCase("Dibayar")) {
                        Assert.assertEquals(manageContract.getLeftStatus(i), "Belum Dibayar");
                    }
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    break;
                case "Dibatalkan, Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar":
                case "Dibatalkan, Konsultan, Calon Penyewa, Pemilik Kos":
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Dibatalkan", "Filter contract is failed");
                    break;
                case "Sewa Berakhir, Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar":
                case "Sewa Berakhir, Konsultan, Calon Penyewa, Pemilik Kos":
                    Assert.assertEquals(manageContract.getLeftStatus(i), "Sewa Berakhir", "Filter contract is failed");
                    break;
                default:
                    Assert.assertTrue(manageContract.terminateContractButtonIsPresent(i), "Terminate contract button not present");
                    break;
            }
        }
    }

    @When("user filter contract by {string} and {string} or {string} or {string}")
    public void user_filter_contract_by_and_or_or(String filterBy1, String filterBy2, String filterBy3, String filterBy4) throws InterruptedException {
        manageContract.clickOnBurgerFilter();
        filterContract.clickOnFilterOption(filterBy1);
        filterContract.clickOnFilterOption(filterBy2);
        filterContract.clickOnFilterOption(filterBy3);
        filterContract.clickOnFilterOption(filterBy4);
        filterContract.clickOnSubmitFilter();
    }
}