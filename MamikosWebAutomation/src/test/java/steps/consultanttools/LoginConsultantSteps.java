package steps.consultanttools;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.ConsultantLoginPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class LoginConsultantSteps {

    JavaHelpers java;
    SeleniumHelpers selenium;
    private WebDriver driver = ThreadManager.getDriver();
    private ConsultantLoginPO login = new ConsultantLoginPO(driver);

    //Test Data Consultant
    private String consultantProperty = "src/test/resources/testdata/consultant/datauser.properties";
    private String consultantEmail_ = JavaHelpers.getPropertyValue(consultantProperty,"consultantEmail_" + Constants.ENV);
    private String consultantPassword_ = JavaHelpers.getPropertyValue(consultantProperty,"consultantPassword_" + Constants.ENV);
    private String consultant2Email_ = JavaHelpers.getPropertyValue(consultantProperty,"consultant2Email_" + Constants.ENV);
    private String consultant2Password_ = JavaHelpers.getPropertyValue(consultantProperty,"consultant2Password_" + Constants.ENV);
    private String consultant3Email_ = JavaHelpers.getPropertyValue(consultantProperty,"consultant3Email_" + Constants.ENV);
    private String consultant3Password_ = JavaHelpers.getPropertyValue(consultantProperty,"consultant3Password_" + Constants.ENV);

    @Given("user {string} login as a consultant")
    public void user_login_as_a_consultant(String consultanName) throws InterruptedException {
        String email="";
        String password="";

        if (login.phpDebuggerPresent()){
            login.clickOnClosePHPDebugger();
        }

        if(consultanName.equalsIgnoreCase("consultant1")){
            email = consultantEmail_;
            password = consultantPassword_;
        }else if(consultanName.equalsIgnoreCase("consultant2")){
            email = consultant2Email_;
            password = consultant2Password_;
        }else if(consultanName.equalsIgnoreCase("consultant3")){
            email = consultant3Email_;
            password = consultant3Password_;
        }
        login.enterEmailPassword(email,password);
        login.clickOnLoginButton();
    }

    @When("user input email {string} password {string} and click login button")
    public void user_input_email_password_and_click_login_button(String email, String password) throws InterruptedException {
        login.enterEmailPassword(email,password);
        login.clickOnLoginButton();
    }

    @Then("system display error message {string} and display try again button")
    public void system_display_error_message_and_display_try_again_button(String errorMessage) {
        Assert.assertEquals(java.replaceEnter(login.getErrorMessage()), errorMessage, "Message not match");
        Assert.assertTrue(login.tryAgainButtonIsPresent(), "Try again button not present");
    }

    @When("user click try again button")
    public void user_click_try_again_button() throws InterruptedException {
        login.clickOnTryAgainButton();
    }

    @Then("system display login page with blank form")
    public void system_display_login_page_with_blank_form() {
        Assert.assertEquals(login.getEmailTextBox(), "", "Email field not blank");
        Assert.assertEquals(login.getPasswordTextBox(), "", "Email field not blank");
    }
}
