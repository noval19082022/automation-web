package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.SearchTenantPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class SearchTenantSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SearchTenantPO search = new SearchTenantPO(driver);

    private String consultantTask="src/test/resources/testdata/consultant/consultant.properties";
    private String activeTenantName = JavaHelpers.getPropertyValue(consultantTask,"activeTenantName_" + Constants.ENV);
    private String activeTenantEmail = JavaHelpers.getPropertyValue(consultantTask,"activeTenantEmail_" + Constants.ENV);

    @When("user search tenant by name")
    public void user_search_tenant_by_name() throws InterruptedException {
        search.clickOnSearchBox();
        search.setTextOnSearchTextBox(activeTenantName);
    }

    @Then("system display list tenant active according to tenant name")
    public void system_display_list_tenant_active_according_to_tenant_name() throws InterruptedException {
        int numberOfList = search.getNumberOfList();

        for (int i = 1 ; i <= numberOfList ; i++){
            search.clickOnTenantList(i);
            Assert.assertTrue(search.getTenantName().contains(activeTenantName), "Tenant name not contains " + activeTenantName);
            search.clickOnBackIcon();
        }
    }

    @When("user search tenant by email")
    public void user_search_tenant_by_email() throws InterruptedException {
        search.clickOnSearchBox();
        search.setTextOnSearchTextBox(activeTenantEmail);
    }


    @Then("system display list tenant active according to tenant email")
    public void system_display_list_tenant_active_according_to_tenant_email() throws InterruptedException {
        int numberOfList = search.getNumberOfList();

        for (int i = 1 ; i <= numberOfList ; i++){
            search.clickOnTenantList(i);
            Assert.assertTrue(search.getTenantEmail().contains(activeTenantEmail), "Tenant email not contains " + activeTenantEmail);
            search.clickOnBackIcon();
        }
    }

    @When("user search tenant by {string}")
    public void user_search_tenant_by(String keyword) throws InterruptedException {
        search.clickOnSearchBox();
        search.setTextOnSearchTextBox(keyword);
    }

    @Then("system display tenant active with name or email {string}")
    public void system_display_tenant_active_with_name_or_email(String tenant) throws InterruptedException {
        int numberOfList = search.getNumberOfList();
        boolean found = false;

        for (int i = 1 ; i <= numberOfList ; i++){
            search.clickOnTenantList(i);
            search.getTenantName();
            search.getTenantEmail();
            if (search.getTenantName().contains(tenant) || search.getTenantEmail().contains(tenant)) {
                found = true;
            }
            Assert.assertTrue(found, "Tenant name or tenant email contains " + tenant);
            search.clickOnBackIcon();
        }
    }

    @Then("system display tenant not found")
    public void system_display_tenant_not_found() {
        search.tenantNotFoundIsPresent();
    }
}
