package steps.consultanttools;

import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.consultanttools.AddPotentialTenantPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class AddPotentialTenantSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private JavaHelpers java = new JavaHelpers();
    private AddPotentialTenantPO addPotentialTenant = new AddPotentialTenantPO(driver);

    //Test Data Consultant
    private String potentialTenant="src/test/resources/testdata/consultant/potentialTenant.properties";
    private String tenantPhoneNumber = JavaHelpers.getPropertyValue(potentialTenant, "tenantPhoneNumberDefault_" + Constants.ENV);
    private String emailDefault = JavaHelpers.getPropertyValue(potentialTenant, "emailDefault_" + Constants.ENV);
    private String nameDefault = JavaHelpers.getPropertyValue(potentialTenant, "nameDefault_" + Constants.ENV);
    private String genderDefault = JavaHelpers.getPropertyValue(potentialTenant, "genderDefault_" + Constants.ENV);
    private String occupationDefault = JavaHelpers.getPropertyValue(potentialTenant, "occupationDefault_" + Constants.ENV);
    private String kosName = JavaHelpers.getPropertyValue(potentialTenant, "kosNameDefault_" + Constants.ENV);
    private String rentalPeriodDefault = JavaHelpers.getPropertyValue(potentialTenant, "rentalPeriodDefault_" + Constants.ENV);
    private String rentalPriceDefault = JavaHelpers.getPropertyValue(potentialTenant, "rentalPriceDefault_" + Constants.ENV);
    private String notesDefault = JavaHelpers.getPropertyValue(potentialTenant, "notesDefault_" + Constants.ENV);
    private String notesDetailsDefault = JavaHelpers.getPropertyValue(potentialTenant, "notesDetailsDefault_" + Constants.ENV);

    @When("user input data potential tenant")
    public void user_input_data_potential_tenant() throws InterruptedException, ParseException {
        addPotentialTenant.enterPhoneNumber(tenantPhoneNumber);
        addPotentialTenant.clickOnNextButton();
        addPotentialTenant.enterEmail(emailDefault);
        addPotentialTenant.enterName(nameDefault);
        addPotentialTenant.selectGender(genderDefault);
        addPotentialTenant.selectOccupation(occupationDefault);
        addPotentialTenant.selectKos(kosName);
        addPotentialTenant.selectRentalPeriod(rentalPeriodDefault);
        addPotentialTenant.enterRentalPrice(rentalPriceDefault);
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        addPotentialTenant.selectDate(today);
        addPotentialTenant.selectNotes(notesDefault);
        addPotentialTenant.enterAndSaveNotes(notesDetailsDefault);
        addPotentialTenant.clickOnAddTenantButton();
    }
}
