package steps.consultanttools;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.*;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class ActivateTaskSteps {
    private JavaHelpers java = new JavaHelpers();
    private WebDriver driver = ThreadManager.getDriver();
    private ManageTaskPO manageTask = new ManageTaskPO(driver);
    private TaskListPO taskList = new TaskListPO(driver);
    private TaskDetailPO taskDetail  = new TaskDetailPO(driver);
    private SearchActiveTaskPO searchTask = new SearchActiveTaskPO(driver);
    private InvoicePO invoiceList = new InvoicePO(driver);
    private PropertyDetailPO propertyDetail = new PropertyDetailPO(driver);
    private  TaskHistoryPO taskHistory = new TaskHistoryPO(driver);

    //Test Data Activity Management
    private String taskManagement="src/test/resources/testdata/consultant/taskManagement.properties";
    private String taskPotentialTenant_ = JavaHelpers.getPropertyValue(taskManagement,"taskPotentialTenant_" + Constants.ENV);
    private String funnelPotentialTenant_ = JavaHelpers.getPropertyValue(taskManagement,"funnelPotentialTenant_" + Constants.ENV);
    private String taskProperty_ = JavaHelpers.getPropertyValue(taskManagement,"taskProperty_" + Constants.ENV);
    private String funnelProperty_ = JavaHelpers.getPropertyValue(taskManagement,"funnelProperty_" + Constants.ENV);
    private String hyperlinkPropertyDetail_ = JavaHelpers.getPropertyValue(taskManagement,"hyperlinkPropertyDetail_" + Constants.ENV);
    private String taskContract_ = JavaHelpers.getPropertyValue(taskManagement,"taskContract_" + Constants.ENV);
    private String funnelContract_ = JavaHelpers.getPropertyValue(taskManagement,"funnelContract_" + Constants.ENV);
    private String hyperlinkContractDetail_ = JavaHelpers.getPropertyValue(taskManagement,"hyperlinkContractDetail_" + Constants.ENV);
    private String taskPotentialOwner_ = JavaHelpers.getPropertyValue(taskManagement,"taskPotentialOwner_" + Constants.ENV);
    private String funnelPotentialOwner_ = JavaHelpers.getPropertyValue(taskManagement,"funnelPotentialOwner_" + Constants.ENV);
    private String secondTaskPotentialOwner_ = JavaHelpers.getPropertyValue(taskManagement,"taskPotentialOwner2_" + Constants.ENV);
    private String secondFunnelPotentialOwner_ = JavaHelpers.getPropertyValue(taskManagement,"funnelPotentialOwner2_" + Constants.ENV);
    private String taskPotentialProperty_ = JavaHelpers.getPropertyValue(taskManagement,"taskPotentialProperty_" + Constants.ENV);
    private String funnelPotentialProperty_ = JavaHelpers.getPropertyValue(taskManagement,"funnelPotentialProperty_" + Constants.ENV);


    @And("user move task to task list")
    public void user_move_task_to_task_list() throws InterruptedException {
        int numberOfList = searchTask.getNumberOfList();

        for (int i = 1 ; i <= numberOfList ; i++){
            if (taskContract_.equals(searchTask.getTaskName(i)) ||
                    taskPotentialOwner_.equals(searchTask.getTaskName(i)) ||
                    secondTaskPotentialOwner_.equals(searchTask.getTaskName(i)) ||
                    taskPotentialTenant_.equals(searchTask.getTaskName(i)) ||
                    taskPotentialProperty_.equals(searchTask.getTaskName(i)) ||
                    taskProperty_.equals(searchTask.getTaskName(i)))
            {
                searchTask.clickTask();
                if (!taskDetail.getStageTitle().equals("Tahap 1: Tugas Aktif")){
                    for (int j = 0; j<2; j++){
                        taskDetail.clickNextStageButton();
                        if (j==0){
                            taskDetail.clickOptionActiveTask();
                        }else{
                            taskDetail.clickOnTaskListOption();
                        }
                        taskDetail.clickMoveButton();
                        taskDetail.clickWarningButton();
                    }
                }else {
                    taskDetail.clickNextStageButton();
                    taskDetail.clickOnTaskListOption();
                    taskDetail.clickMoveButton();
                    taskDetail.clickWarningButton();
                }
            }
            Assert.assertEquals(taskDetail.getStageTitle(), "Daftar Tugas", "Task title not Daftar Tugas");
        }
    }

    @When("user move task potential owner to task list")
    public void user_move_task_potential_owner_to_task_list() throws InterruptedException {
        //int numberOfList = searchTask.getNumberOfList();
        if (!taskDetail.getStageTitle().equals("Daftar Tugas")){
            for (int j = 0; j<2; j++){
                taskDetail.clickNextStageButton();
                if (j==0){
                    taskDetail.clickOptionActiveTask();
                }else{
                    taskDetail.clickOnTaskListOption();
                }
                taskDetail.clickMoveButton();
                taskDetail.clickWarningButton();
            }
        }
        Assert.assertEquals(taskDetail.getStageTitle(), "Daftar Tugas", "Task title not Daftar Tugas");
    }

    @When("user activate task potential tenant")
    public void user_activate_task_potential_tenant() throws InterruptedException {
        manageTask.clickSeeAllFunnel();
        int numberOfFunnel = manageTask.getNumberOfFunnel();
        for (int i = 1 ; i <= numberOfFunnel ; i++) {
            if (funnelPotentialTenant_.equals(manageTask.getFunnel(i))) {
                manageTask.clickOnFunnelCard(manageTask.getFunnel(i));
                break;
            }
        }
        taskList.clickSeeAllTaskButton();
        taskList.searchTaskList(taskPotentialTenant_);
        taskDetail.clickActivateTaskButton();
    }

    @When("user activate task property")
    public void user_activate_task_property() throws InterruptedException {
        manageTask.clickSeeAllFunnel();
        int numberOfFunnel = manageTask.getNumberOfFunnel();
        for (int i = 1 ; i <= numberOfFunnel ; i++) {
            if (funnelProperty_.equals(manageTask.getFunnel(i))) {
                manageTask.clickOnFunnelCard(manageTask.getFunnel(i));
                break;
            }
        }
        taskList.clickSeeAllTaskButton();
        taskList.searchTaskList(taskProperty_);
        taskDetail.clickActivateTaskButton();
    }

    @When("user activate task contract")
    public void user_activate_task_contract() throws InterruptedException {
        manageTask.clickSeeAllFunnel();
        int numberOfFunnel = manageTask.getNumberOfFunnel();
        for (int i = 1 ; i <= numberOfFunnel ; i++) {
            if (funnelContract_.equals(manageTask.getFunnel(i))) {
                manageTask.clickOnFunnelCard(manageTask.getFunnel(i));
                break;
            }
        }
        taskList.clickSeeAllTaskButton();
        taskList.searchTaskList(taskContract_);
        taskDetail.clickActivateTaskButton();
        taskDetail.clickNextStageButton();
    }

    @When("user move task to active task stage")
    public void user_move_task_to_active_task_stage() throws InterruptedException {
        taskDetail.clickNextStageButton();
        taskDetail.clickOptionActiveTask();
        taskDetail.clickMoveButton();
        taskDetail.clickWarningButton();
    }

    @When("user move with skip one stage")
    public void user_move_with_skip_one_stage() throws InterruptedException {
        taskDetail.clickNextStageButton();
        taskDetail.selectJumpStage();
        taskDetail.clickMoveButton();
        taskDetail.clickWarningButton();
    }

    @When("user fill activity form")
    public void user_fill_activity_form() throws InterruptedException {
        taskDetail.selectOptionActivityForm("Tidak");
        taskDetail.selectOptionActivityForm("Sudah bayar ke pemilik");
        taskDetail.enterNotes("Done");
        taskDetail.clickOnSaveButton();
    }

    @When("user move task to next stage")
    public void user_move_task_to_next_stage() throws InterruptedException {
        taskDetail.clickNextStageButton();
        taskDetail.clickMoveButton();
    }

    @When("user fill activity contact form")
    public void user_fill_activity_contact_form() throws InterruptedException, ParseException {
        taskDetail.selectOptionActivityForm("Tidak");
        taskDetail.selectOptionActivityForm("SMS");
        taskDetail.selectOptionActivityForm("Baru (First Paid)");
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        taskDetail.selectDateMonthlyPayment(today);
        taskDetail.enterNotes("Done");
        taskDetail.clickOnSaveButton();
    }

    @When("user fill form has been contacted for potential tenant")
    public void user_fill_form_has_been_contacted_for_potential_tenant() throws InterruptedException, ParseException {
        taskDetail.selectOptionActivityForm("Tidak");
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        taskDetail.selectContactDate(today);
        taskDetail.enterCallingTime("0600");
        taskDetail.selectOptionActivityForm("Telepon");
        taskDetail.enterNotes("Done");
        taskDetail.enterPhoneNumber("082256756454");
        taskDetail.selectOriginalDate(today);
        taskDetail.clickOnSaveButton();
    }

    @When("user update form has been contacted for potential tenant")
    public void user_update_form_has_been_contacted_for_potential_tenant() throws InterruptedException, ParseException {
        taskDetail.clickOnUpdateDataButton();
        taskDetail.selectOptionActivityForm("Ya");
        String tomorrow = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        taskDetail.selectContactDate(tomorrow);
        taskDetail.enterCallingTime("0700");
        taskDetail.selectOptionActivityForm("SMS");
        taskDetail.enterNotes("Update");
        taskDetail.enterPhoneNumber("082256756455");
        taskDetail.selectOriginalDate(tomorrow);
        taskDetail.clickOnSaveButton();
    }

    @When("user fill form has been contacted for data property")
    public void user_fill_form_has_been_contacted_for_data_property() throws InterruptedException, ParseException {
        taskDetail.selectOptionActivityForm("Tidak");
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        taskDetail.selectContactDate(today);
        taskDetail.enterCallingTime("0600");
        taskDetail.selectOptionActivityForm("Telepon");
        taskDetail.enterNotes("Done");
        taskDetail.clickOnSaveButton();
    }

    @When("user fill form data owner agreement for data property")
    public void user_fill_form_data_owner_agreement_for_data_property() throws InterruptedException {
        taskDetail.selectOptionActivityForm("Tidak");
        taskDetail.enterReasonNotAgree("Ribet");
        taskDetail.selectOptionActivityForm("GP3");
        taskDetail.clickOnSaveButton();
    }

    @When("user fill form owner details for potential owner")
    public void user_fill_form_owner_details_for_potential_owner() throws InterruptedException {
        taskDetail.enterTestNotes("Test");
        taskDetail.enterTestTime("0600");
        taskDetail.clickOnSaveButton();
    }

    @When("user fill form property details for potential property")
    public void user_fill_form_property_details_for_potential_property() throws ParseException, InterruptedException {
        taskDetail.enterTestTime("0600");
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        taskDetail.selectTestDate(today);
        taskDetail.enterAddress("Jl. Kasmaran");
        taskDetail.clickOnSaveButton();
    }

    @When("user fill form input data property for potential property")
    public void user_fill_form_input_data_property_for_potential_property() throws InterruptedException {
        taskDetail.selectOptionActivityForm("Tidak");
        taskDetail.fillFieldContent("Notes");
        taskDetail.enterOwnerNumber("081245454567");
        taskDetail.clickOnSaveButton();
    }

    @When("user fill form input data potential owner")
    public void user_fill_form_input_data_potential_owner() throws InterruptedException {
        taskDetail.enterInputTextField("Text");
        taskDetail.clickOnSaveButton();
    }

    @When("user fill form data validation potential tenant")
    public void user_fill_form_data_validation_potential_tenant() throws InterruptedException, ParseException {
        taskDetail.selectOptionActivityForm("Tidak");
        taskDetail.enterReasonsNotValidated("Gak Jelas");
        taskDetail.enterName("Consultant A");
        taskDetail.enterEmail("cons@email.com");
        taskDetail.selectOptionActivityForm("Laki-laki");
        taskDetail.enterRentalPrice("250000");
        taskDetail.enterRentalDuration("1");
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        taskDetail.selectDueDate(today);
        taskDetail.enterNotes("Done");
        taskDetail.clickOnSaveButton();
    }

    @When("user activate task potential owner")
    public void user_activate_task_potential_owner() throws InterruptedException {
        manageTask.clickSeeAllFunnel();
        int numberOfFunnel = manageTask.getNumberOfFunnel();
        for (int i = 1 ; i <= numberOfFunnel ; i++) {
            if (funnelPotentialOwner_.equals(manageTask.getFunnel(i))) {
                manageTask.clickOnFunnelCard(manageTask.getFunnel(i));
                break;
            }
        }
        taskList.clickSeeAllTaskButton();
        taskList.searchTaskList(taskPotentialOwner_);
        taskDetail.clickActivateTaskButton();
    }

    @When("user activate task potential property")
    public void user_activate_task_potential_property() throws InterruptedException {
        manageTask.clickSeeAllFunnel();
        int numberOfFunnel = manageTask.getNumberOfFunnel();
        for (int i = 1 ; i <= numberOfFunnel ; i++) {
            if (funnelPotentialProperty_.equals(manageTask.getFunnel(i))) {
                manageTask.clickOnFunnelCard(manageTask.getFunnel(i));
                break;
            }
        }
        taskList.clickSeeAllTaskButton();
        taskList.searchTaskList(taskPotentialProperty_);
        taskDetail.clickActivateTaskButton();
    }

    @Then("user go to detail task on second funnel")
    public void user_go_to_detail_task_on_second_funnel() throws InterruptedException {
        manageTask.clickSeeAllFunnel();
        int numberOfFunnel = manageTask.getNumberOfFunnel();
        for (int i = 1 ; i <= numberOfFunnel ; i++) {
            if (secondFunnelPotentialOwner_.equals(manageTask.getFunnel(i))) {
                manageTask.clickOnFunnelCard(manageTask.getFunnel(i));
                break;
            }
        }
        taskList.clickSeeAllTaskButton();
        taskList.searchTaskList(secondTaskPotentialOwner_);
    }

    @Then("user verify stage name is {string}")
    public void user_verify_stage_name_is(String stageTitle) throws InterruptedException {
        Assert.assertEquals(taskDetail.getStageTitle(), stageTitle, "Task title not " + stageTitle);
    }

    @When("user want to update the form and cancel")
    public void user_want_to_update_the_form_and_cancel() throws InterruptedException {
        taskDetail.clickOnUpdateDataButton();
        taskDetail.clickOnCancelButton();
    }

    @Then("user verify data form has been contacted for potential tenant")
    public void user_verify_data_form_has_been_contacted_for_potential_tenant() throws ParseException, InterruptedException {
        Assert.assertEquals(taskDetail.getValueReportField(0), "Tidak", "This field not equals");
        String today = java.getTimeStamp("MMM dd, yyy");
        Assert.assertEquals(taskDetail.getValueReportField(1), today, "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(2), "06:00", "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(3), "Telepon", "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(4), "Done", "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(5), "082256756454", "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(6), today, "This field not equals");
    }

    @Then("user verify updated data form has been contacted for potential tenant")
    public void user_verify_updated_data_form_has_been_contacted_for_potential_tenant() throws ParseException, InterruptedException {
        Assert.assertEquals(taskDetail.getValueReportField(0), "Ya", "This field not equals");
        String date = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "dd", 1, 0, 0, 0);
        String tomorrow = java.getTimeStamp("MMM " + date + ", yyy");
        Assert.assertEquals(taskDetail.getValueReportField(1), tomorrow, "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(2), "07:00", "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(3), "SMS", "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(4), "Update", "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(5), "082256756455", "This field not equals");
        Assert.assertEquals(taskDetail.getValueReportField(6), tomorrow, "This field not equals");
    }

    @When("user search task potential tenant on list active task")
    public void user_search_task_potential_tenant_on_list_active_task() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(taskPotentialTenant_);
        searchTask.clickDataTenant();
    }

    @When("user search task property on list active task")
    public void user_search_task_property_on_list_active_task() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(taskProperty_);
        searchTask.clickDataProperty();
    }

    @When("user search task contract on list active task")
    public void user_search_task_contract_on_list_active_task() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(taskContract_);
        searchTask.clickDataContract();
    }

    @When("user search task potential owner on list active task")
    public void user_search_task_potential_owner_on_list_active_task() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(taskPotentialOwner_);
        searchTask.clickDataPotentialOwner();
    }

    @When("user search task on second funnel potential owner on list active task")
    public void user_search_task_on_second_funnel_potential_owner_on_list_active_task() throws InterruptedException {
        manageTask.clickSeeAllFunnel();
        int numberOfFunnel = manageTask.getNumberOfFunnel();
        for (int i = 1 ; i <= numberOfFunnel ; i++) {
            if (secondFunnelPotentialOwner_.equals(manageTask.getFunnel(i))) {
                manageTask.clickOnFunnelCard(manageTask.getFunnel(i));
                break;
            }
        }
        taskList.clickSeeAllTaskButton();
        taskList.searchTaskList(secondTaskPotentialOwner_);
    }

    @When("user search second task potential owner on list active task")
    public void user_search_second_task_potential_owner_on_list_active_task() throws InterruptedException {
        manageTask.clickSeeAllFunnel();
        int numberOfFunnel = manageTask.getNumberOfFunnel();
        for (int i = 1 ; i <= numberOfFunnel ; i++) {
            if (secondFunnelPotentialOwner_.equals(manageTask.getFunnel(i))) {
                manageTask.clickOnFunnelCard(manageTask.getFunnel(i));
                break;
            }
        }
        taskList.clickSeeAllTaskButton();
        taskList.searchTaskList(secondTaskPotentialOwner_);
    }

    @When("user search task potential property on list active task")
    public void user_search_task_potential_property_on_list_active_task() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(taskPotentialProperty_);
        searchTask.clickDataPotentialProperty();
    }

    @When("user click the search result")
    public void user_click_the_search_result() throws InterruptedException {
        searchTask.clickTask();
    }

    @Then("user verify task name {string}")
    public void user_verify_task_name(String taskName) throws InterruptedException {
        Assert.assertTrue(taskDetail.getTaskDetail().contains(taskName), "Tenant name not contains " + taskName);
    }

    @Then("user verify search result is task potential tenant")
    public void user_verify_search_result_is_task_potential_tenant() throws InterruptedException {
        Assert.assertTrue(taskDetail.getTaskDetail().contains(taskPotentialTenant_), "Tenant name not contains " + taskPotentialTenant_);
    }

    @Then("user verify search result is task property")
    public void user_verify_search_result_is_task_property() throws InterruptedException {
        Assert.assertTrue(taskDetail.getTaskDetail().contains(taskProperty_), "Tenant name not contains " + taskProperty_);
    }

    @Then("user verify search result is task contract")
    public void user_verify_search_result_is_task_contract() throws InterruptedException {
        Assert.assertTrue(taskDetail.getTaskDetail().contains(taskContract_), "Tenant name not contains " + taskContract_);
    }

    @Then("user verify search result is task potential owner")
    public void user_verify_search_result_is_task_potential_owner() throws InterruptedException {
        Assert.assertTrue(taskDetail.getTaskDetail().contains(taskPotentialOwner_), "Tenant name not contains " + taskPotentialOwner_);
    }

    @Then("user verify search result is second task potential owner")
    public void user_verify_search_result_is_second_task_potential_owner() throws InterruptedException {
        Assert.assertTrue(taskDetail.getTaskDetail().contains(secondTaskPotentialOwner_), "Tenant name not contains " + taskPotentialOwner_);
    }

    @Then("user verify search result is task potential property")
    public void user_verify_search_result_is_task_potential_property() throws InterruptedException {
        Assert.assertTrue(taskDetail.getTaskDetail().contains(taskPotentialProperty_), "Tenant name not contains " + taskPotentialProperty_);
    }

    @When("user move active task to task list stage")
    public void user_move_active_task_to_task_list_stage() throws InterruptedException {
        taskDetail.clickNextStageButton();
        taskDetail.clickOnTaskListOption();
        taskDetail.clickMoveButton();
        taskDetail.clickWarningButton();
    }

    @Then("user verify funnel name is {string}")
    public void user_verify_funnel_name_is(String backlogTitle) throws InterruptedException {
        Assert.assertEquals(taskDetail.getStageTitle(), backlogTitle, "Task title not " + backlogTitle);
    }

    @When("user click contract detail")
    public void user_click_contract_detail() throws InterruptedException {
        taskDetail.clickContractHyperlink();
    }

    @Then("system display hyperlink contract detail")
    public void system_display_hyperlink_contract_detail() throws InterruptedException {
        Assert.assertTrue(invoiceList.getDetailContractHyperlink().contains(hyperlinkContractDetail_),"Hyperlink is different");
    }

    @When("user click property hyperlink button")
    public void user_click_property_hyperlink_button() throws InterruptedException {
        taskDetail.clickPropertyHyperlink();
    }

    @Then("system display hyperlink property detail")
    public void system_display_hyperlink_property_detail() throws InterruptedException {
        Assert.assertTrue(propertyDetail.getDetailPropertyHyperlink().contains(hyperlinkPropertyDetail_),"Hyperlink is different");
    }

    @When("user click history task")
    public void user_click_history_task() {
        taskDetail.clickHistoryTask();
    }

    @Then("user verify task status is {string} and back to manage task page")
    public void user_verify_task_status_is_and_back_to_manage_task_page(String activeTask) throws InterruptedException {
        Assert.assertEquals(taskHistory.getStatusTask(), activeTask, "Task status not "+ activeTask);
        taskDetail.backToManageTaskPage();
    }

    @Then("user verify task status is {string}")
    public void user_verify_task_status_is(String activeTask) throws InterruptedException {
        Assert.assertEquals(taskHistory.getStatusTask(), activeTask, "Task status not "+ activeTask);
    }
}
