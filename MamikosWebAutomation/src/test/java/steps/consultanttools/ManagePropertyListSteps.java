package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.ManagePropertyListPO;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.text.ParseException;
import java.util.Date;

public class ManagePropertyListSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ManagePropertyListPO propertyList = new ManagePropertyListPO(driver);

    @When("user short property by {string}")
    public void user_short_property_by(String sortBy) throws InterruptedException {
        propertyList.clickOnFilterAndShortIcon();
        propertyList.clickOnShortRoomAvailability(sortBy);
        propertyList.clickOnApplyButton();
    }

    @Then("system short property by room availability")
    public void system_short_property_by_room_availability() throws ParseException, InterruptedException {
        int countPropertyList = propertyList.countPropertyList();

        Date lastUpdate1 = null;
        Date lastUpdate2 = null;
        for (int i = 0; i < countPropertyList; i++) {
            lastUpdate1 = propertyList.getLastUpdate(i);
            System.out.println("Time " + i + "= " + lastUpdate1);
            if (i>=1){
                Assert.assertTrue((lastUpdate1.compareTo(lastUpdate2) <= 0), "Last updates not sort newest");
            }
            lastUpdate2 = lastUpdate1;
        }
    }

    @Then("system short property by most empty room")
    public void system_short_property_by_most_empty_room() throws InterruptedException {
        int countPropertyList = propertyList.countPropertyList();
        System.out.println(countPropertyList);
        int emptyRoom2 = 0;

        for (int i = 1; i<= countPropertyList; i++){
            System.out.println("Time " + i + "= " + propertyList.getEmptyRoom(i));
            int emptyRoom1 = propertyList.getEmptyRoom(i);
            if (i>0){
                Assert.assertTrue(emptyRoom1 >= emptyRoom2, "Empty room in first property less then second property");
            }
        }
    }
}
