package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.HomePagePO;
import utilities.ThreadManager;

import java.util.List;

public class HomePageSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HomePagePO homepage = new HomePagePO(driver);

    @Then("system display label {string}")
    public void system_display_label(String text) throws InterruptedException {
        Assert.assertEquals(homepage.getLabel(), text, text + " Not Present");
    }

    @When("user access to menu {string}")
    public void user_access_to_menu(String menu) throws InterruptedException {
        homepage.clickOnBurgerMenu();
        homepage.clickOnLaterButton();
        if (menu.equals("Penyewa Aktif")){
            homepage.clickOnMenu("Kelola Tenant");
        } else if (menu.equals("Pemilik Potensial") ||
                menu.equals("Property Potensial") ||
                menu.equals("Penyewa Potensial")){
            homepage.clickOnMenu("Data Leads");
            homepage.clickOnMenu(menu);
        }else {
            homepage.clickOnMenu(menu);
        }
    }

    @When("user click hamburger menu button")
    public void user_click_hamburger_menu_button() throws InterruptedException {
        homepage.clickOnBurgerMenu();
    }

    @Then("system display menu")
    public void system_display_menu(List<String> menu) throws InterruptedException {
        for ( int i = 0; i < menu.size() ; i++){
            Assert.assertEquals(homepage.menuIsPresent(i+1), menu.get(i), "Menu not match");
        }
    }

    @Then("system display sub menu {string}")
    public void system_display_sub_menu(String menu, List<String> subMenu) {
        for ( int i = 0; i < subMenu.size() ; i++){
            Assert.assertEquals(homepage.subMenuIsPresent(menu, i+1), subMenu.get(i), " Sub menu not match");
        }
    }
}
