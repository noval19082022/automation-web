package steps.consultanttools;

import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.TaskManagementPO;
import utilities.Constants;
import utilities.ThreadManager;

public class TaskManagementSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private TaskManagementPO taskManagement = new TaskManagementPO(driver);

    @Then("system display task management content")
    public void system_display_task_management_content() throws InterruptedException {
        Assert.assertEquals(taskManagement.getPageTitle(), "Kelola Tugas", "Title not match");
        Assert.assertTrue(taskManagement.fieldSearchIsPresent(), "Not found");
        Assert.assertTrue(taskManagement.consultantNameIsPresent(), "Not found");
        Assert.assertTrue(taskManagement.consultantRolesIsPresent(), "Not found");
        Assert.assertTrue(taskManagement.funnelCardIsPresent(), "Not found");
        Assert.assertEquals(taskManagement.getLabelSeeAllFunnel(), "Lihat Semua Funnel", "Label not match");
        Assert.assertEquals(taskManagement.getLabelListActiveTask(), "Daftar Tugas Aktif", "Label not match");
        Assert.assertTrue(taskManagement.dataTypeFilterIsPresent(), "Not found");
        Assert.assertTrue(taskManagement.listActiveTaskIsPresent(), "Not found");
        if (Constants.ENV.equals("staging")){
            Assert.assertTrue(taskManagement.paginationIsPresent(), "Not found");
        }
    }
}
