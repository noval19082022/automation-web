package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.PropertyPricePO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class PropertyPriceSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private PropertyPricePO price = new PropertyPricePO(driver);
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);

    //Test Data
    private String consultantProperty = "src/test/resources/testdata/consultant/consultant.properties";
    private String defaultDailyPrice = JavaHelpers.getPropertyValue(consultantProperty, "dailyPrice_tobelo_" + Constants.ENV);
    private String defaultDailyPriceValue = JavaHelpers.getPropertyValue(consultantProperty, "dailyPriceValue_tobelo_" + Constants.ENV);
    private String defaultWeeklyPrice = JavaHelpers.getPropertyValue(consultantProperty, "weeklyPrice_tobelo_" + Constants.ENV);
    private String defaultWeeklyPriceValue = JavaHelpers.getPropertyValue(consultantProperty, "weeklyPriceValue_tobelo_" + Constants.ENV);
    private String defaultMonthlyPrice = JavaHelpers.getPropertyValue(consultantProperty, "monthlyPrice_tobelo_" + Constants.ENV);
    private String defaultMonthlyPriceValue = JavaHelpers.getPropertyValue(consultantProperty, "monthlyPriceValue_tobelo_" + Constants.ENV);
    private String defaultThreeMonthlyPrice = JavaHelpers.getPropertyValue(consultantProperty, "threeMonthlyPrice_tobelo_" + Constants.ENV);
    private String defaultThreeMonthlyPriceValue = JavaHelpers.getPropertyValue(consultantProperty, "threeMonthlyPriceValue_tobelo_" + Constants.ENV);
    private String defaultSixMonthlyPrice = JavaHelpers.getPropertyValue(consultantProperty, "sixMonthlyPrice_tobelo_" + Constants.ENV);
    private String defaultSixMonthlyPriceValue = JavaHelpers.getPropertyValue(consultantProperty, "sixMonthlyPriceValue_tobelo_" + Constants.ENV);
    private String defaultAnnualPrice = JavaHelpers.getPropertyValue(consultantProperty, "annualPrice_tobelo_" + Constants.ENV);
    private String defaultAnnualPriceValue = JavaHelpers.getPropertyValue(consultantProperty, "annualPriceValue_tobelo_" + Constants.ENV);

    @When("user click on update kamar dan harga button")
    public void user_click_on_update_kamar_dan_harga_button() throws InterruptedException {
        price.clickOnUpdateKamarDanHarga();
    }

    @Then("user verify system display price in field {string} is {string}")
    public void user_verify_system_display_price_in_field_is(String rentType, String value) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(2);
        switch (rentType) {
            case "Perhari":
                Assert.assertEquals(price.getDailyPrice(), value, rentType + " Price not match");
                break;
            case "Perminggu":
                Assert.assertEquals(price.getWeeklyPrice(), value, rentType + " Price not match");
                break;
            case "Perbulan":
                Assert.assertEquals(price.getMonthlyPrice(), value, rentType + " Price not match");
                break;
            case "Per 3 bulan":
                Assert.assertEquals(price.getThreeMonthlyPrice(), value, rentType + " Price not match");
                break;
            case "Per 6 bulan":
                Assert.assertEquals(price.getSixMonthlyPrice(), value, rentType + " Price not match");
                break;
            case "Pertahun":
                Assert.assertEquals(price.getAnnualPrice(), value, rentType + " Price not match");
                break;
        }
    }

    @When("user set empty price to field {string}")
    public void user_set_empty_price_to_field(String rentType) throws InterruptedException {
        switch (rentType) {
            case "Perhari":
                price.emptyDailyPrice();
                break;
            case "Perminggu":
                price.emptyWeeklyPrice();
                break;
            case "Perbulan":
                price.emptyMonthlyPrice();
                break;
            case "Per 3 bulan":
                price.emptyThreeMonthlyPrice();
                break;
            case "Per 6 bulan":
                price.emptySixMonthlyPrice();
                break;
            case "Pertahun":
                price.emptyAnnualPrice();
                break;
            default:
                System.out.println("No Rent Type Match");
        }
    }

    @When("user set harga {string} using alphabet")
    public void user_set_harga_using_alphabet(String rentType) throws InterruptedException {
        price.setPriceToAlphabet(rentType);
    }

    @When("user set harga {string} to {string}")
    public void user_set_harga_to(String rentType, String value) throws InterruptedException {
        switch (rentType){
            case "Perhari":
                price.setDailyPrice(value);
                break;
            case "Perminggu":
                price.setWeeklyPrice(value);
                break;
            case "Perbulan":
                price.setMonthlyPrice(value);
                break;
            case "Per 3 bulan":
                price.setThreeMonthlyPrice(value);
                break;
            case "Per 6 bulan":
                price.setSixMonthlyPrice(value);
                break;
            case "Pertahun":
                price.setAnnualPrice(value);
                break;
        }
    }

    @Then("user revert harga {string}")
    public void user_revert_harga(String rentType) throws InterruptedException {
        switch (rentType) {
            case "Perhari":
                price.setDailyPrice(defaultDailyPrice);
                break;
            case "Perminggu":
                price.setWeeklyPrice(defaultWeeklyPrice);
                break;
            case "Perbulan":
                price.setMonthlyPrice(defaultMonthlyPrice);
                break;
            case "Per 3 bulan":
                price.setThreeMonthlyPrice(defaultThreeMonthlyPrice);
                break;
            case "Per 6 bulan":
                price.setSixMonthlyPrice(defaultSixMonthlyPrice);
                break;
            case "Pertahun":
                price.setAnnualPrice(defaultAnnualPrice);
                break;
        }
        selenium.hardWait(2);
    }

    @Then("user verify field {string} display default price")
    public void user_verify_field_display_default_price(String rentType) throws InterruptedException {
        price.clickOnUpdateKamarDanHarga();
        selenium.hardWait(3);
        switch (rentType) {
            case "Perhari":
                Assert.assertEquals(price.getDailyPrice(), defaultDailyPriceValue, "Daily price not match");
                break;
            case "Perminggu":
                Assert.assertEquals(price.getWeeklyPrice(), defaultWeeklyPriceValue, "Weekly price not match");
                break;
            case "Perbulan":
                Assert.assertEquals(price.getMonthlyPrice(), defaultMonthlyPriceValue, "Monthly price not match");
                break;
            case "Per 3 bulan":
                Assert.assertEquals(price.getThreeMonthlyPrice(), defaultThreeMonthlyPriceValue, "3 Monthly price not match");
                break;
            case "Per 6 bulan":
                Assert.assertEquals(price.getSixMonthlyPrice(), defaultSixMonthlyPriceValue, "6 Monthly price not match");
                break;
            case "Pertahun":
                Assert.assertEquals(price.getAnnualPrice(), defaultAnnualPriceValue, "Annual price not match");
                break;
        }
    }
}
