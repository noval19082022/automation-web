package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.SalesMotionPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class SalesMotionSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private SalesMotionPO salesmotion = new SalesMotionPO(driver);

    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String salesMotionName = JavaHelpers.getPropertyValue(consultantProperty,"salesMotionName_" + Constants.ENV);
    private String dataProperty = JavaHelpers.getPropertyValue(consultantProperty,"dataProperty_" + Constants.ENV);
    private String dataDBET = JavaHelpers.getPropertyValue(consultantProperty,"dataDBET_" + Constants.ENV);
    private String dataContract = JavaHelpers.getPropertyValue(consultantProperty,"dataContract_" + Constants.ENV);

    @When("user search sales motion to submit report")
    public void user_search_sales_motion_to_submit_report() throws InterruptedException {
        salesmotion.searchSalesMotion(salesMotionName);
        salesmotion.clickAddReport();
    }

    @When("user submit contract as not interested report of sales motion")
    public void user_submit_contract_as_not_interested_report_of_sales_motion() throws InterruptedException {
        salesmotion.setContractType();
        salesmotion.searchDataAssociate(dataContract);
        salesmotion.submitNotInterestedReport();
    }

    @When("user submit dbet as follow up report of sales motion")
    public void user_submit_dbet_as_follow_up_report_of_sales_motion() throws InterruptedException {
        salesmotion.setDbetType();
        salesmotion.searchDataAssociate(dataDBET);
        salesmotion.submitFollowUpReport();
    }

    @When("user submit property as interested report of sales motion")
    public void user_submit_property_as_interested_report_of_sales_motion() throws InterruptedException {
        salesmotion.searchDataAssociate(dataProperty);
        salesmotion.submitInterestedReport();
    }

    @Then("system display {string}")
    public void system_display(String text) throws InterruptedException {
        Assert.assertEquals(salesmotion.getTitlePage(), text, text + " Not Present");
    }
}
