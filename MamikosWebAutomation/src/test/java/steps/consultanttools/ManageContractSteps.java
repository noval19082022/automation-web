package steps.consultanttools;

import dataobjects.Consultant;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.InvoicePO;
import pageobjects.consultanttools.ManageContractPO;
import pageobjects.consultanttools.SearchContractPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class ManageContractSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ManageContractPO manageContract = new ManageContractPO(driver);
    private SearchContractPO searchContract = new SearchContractPO(driver);
    private InvoicePO invoice = new InvoicePO(driver);
    private Consultant data;

    private String tenantProperty="src/test/resources/testdata/consultant/tenant.properties";
    private String tenantHPCRM = JavaHelpers.getPropertyValue(tenantProperty,"tenatHp_crm_" + Constants.ENV);

    public ManageContractSteps(Consultant data) {
        this.data = data;
    }

    @When("user click on create new contract button")
    public void user_click_on_create_new_contract_button() throws InterruptedException {
        manageContract.clickOnAddContractButton();
    }

    @Then("system display list of contract")
    public void system_display_list_of_contract() {
        manageContract.listOfContractIsAppeared();
    }

    @When("user cancel contract from {string}")
    public void user_cancel_contract_from(String page) throws InterruptedException {
        if (page.equalsIgnoreCase("detail contract")){
            manageContract.cancelContractInDetails();
        } else if (page.equalsIgnoreCase("list contract")){
            manageContract.backFromDetailKontrak();
            manageContract.clickOnSearchBox();
            searchContract.setTextOnSearchTextBox(tenantHPCRM);
            manageContract.cancelContractInList();
        }
    }

    @Then("contract canceled/terminated")
    public void contract_canceled() {
        Assert.assertFalse(manageContract.isBatalkanKontrakButtonExist());
        Assert.assertFalse(manageContract.isAkhiriKontrakButtonExist());
    }

    @Then("contract from {string} have status {string}")
    public void contract_from_have_status(String page, String status) throws InterruptedException {
        if (page.equalsIgnoreCase("detail contract")){
            manageContract.backFromDetailKontrak();
            manageContract.clickOnSearchBox();
            searchContract.setTextOnSearchTextBox(tenantHPCRM);
            Assert.assertEquals(manageContract.getFirstDibatalkanContractStatus(),status,"Status is not as expected");
        }else if (page.equalsIgnoreCase("list contract")){
            manageContract.clickOnSearchBox();
            searchContract.setTextOnSearchTextBox(tenantHPCRM);
            Assert.assertEquals(manageContract.getFirstDibatalkanContractStatus(),status,"Status is not as expected");
        }

    }

    @And("user terminate contract from detail contract page")
    public void userTerminateContractFromDetailContractPage() throws InterruptedException {
        manageContract.terminateContractInDetail();
    }

    @And("contract have status {string}")
    public void contractHaveStatus(String status) throws InterruptedException {
        manageContract.backFromDetailKontrak();
        manageContract.clickOnSearchBox();
        searchContract.setTextOnSearchTextBox(tenantHPCRM);
        Assert.assertEquals(manageContract.getFirstDibatalkanContractStatus(),status,"Status is not as expected");
    }

    @When("user add fix cost")
    public void user_add_fix_cost() throws InterruptedException {
        manageContract.clickOnPropertyList(1);
        data.amountOfBill = invoice.getAmountOfTheBill();
        invoice.clickOnUpdateAdditionalCost();
        invoice.addAdditionalCost("Wifi", 1000);
        invoice.clickOnSaveAdditionalCostButton();
    }

    @When("user delete additional fixed cost")
    public void user_delete_additional_fixed_cost() throws InterruptedException {
        data.amountOfBill = invoice.getAmountOfTheBill();
        invoice.clickOnUpdateAdditionalCost();
        invoice.deleteAdditionalFixedCost();
        invoice.clickOnSaveAdditionalCostButton();
    }

    @Then("system display new total amount")
    public void system_display_new_total_amount() {
        Assert.assertEquals(invoice.getAmountOfTheBill(),data.amountOfBill + 1000, "The amount of the bill not match");
    }

    @Then("system display total amount after delete additional fixed cost")
    public void system_display_total_amount_after_delete_additional_fixed_cost() {
        Assert.assertEquals(invoice.getAmountOfTheBill(),data.amountOfBill - 1000, "The amount of the bill not match");
    }
}
