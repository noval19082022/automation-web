package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.PropertyDetailPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class PropertyDetailSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private PropertyDetailPO details = new PropertyDetailPO(driver);

    //Test Data
    private String consultantProperty="src/test/resources/testdata.consultant/consultant.properties";
    private String _propertyID = JavaHelpers.getPropertyValue(consultantProperty,"propertyID_" + Constants.ENV);
    private String _propertyName = JavaHelpers.getPropertyValue(consultantProperty,"propertyName_" + Constants.ENV);
    private String _kostType = JavaHelpers.getPropertyValue(consultantProperty,"kostType_" + Constants.ENV);
    private String _totalRoom = JavaHelpers.getPropertyValue(consultantProperty, "totalRoom_" + Constants.ENV);
    private String _roomFilled = JavaHelpers.getPropertyValue(consultantProperty, "roomFilled_" + Constants.ENV);
    private String _emptyRoom = JavaHelpers.getPropertyValue(consultantProperty, "emptyRoom_" + Constants.ENV);
    private String _ownerName = JavaHelpers.getPropertyValue(consultantProperty, "ownerName_" + Constants.ENV);
    private String _ownerNumber = JavaHelpers.getPropertyValue(consultantProperty, "ownerNumber_" + Constants.ENV);
    private String _managerName = JavaHelpers.getPropertyValue(consultantProperty, "managerName_" + Constants.ENV);
    private String _managerNumber = JavaHelpers.getPropertyValue(consultantProperty, "managerNumber_" + Constants.ENV);
    private String _area = JavaHelpers.getPropertyValue(consultantProperty, "area_" + Constants.ENV);
    private String _address = JavaHelpers.getPropertyValue(consultantProperty, "address_" + Constants.ENV);
    private String _monthlyPrice = JavaHelpers.getPropertyValue(consultantProperty, "monthlyPrice_" + Constants.ENV);
    private String _dailyPrice = JavaHelpers.getPropertyValue(consultantProperty, "dailyPrice_" + Constants.ENV);
    private String _weeklyPrice = JavaHelpers.getPropertyValue(consultantProperty, "weeklyPrice_" + Constants.ENV);
    private String _threeMonthlyPrice = JavaHelpers.getPropertyValue(consultantProperty, "threeMonthlyPrice_" + Constants.ENV);
    private String _sixMonthlyPrice = JavaHelpers.getPropertyValue(consultantProperty, "sixMonthlyPrice_" + Constants.ENV);
    private String _annualPrice = JavaHelpers.getPropertyValue(consultantProperty, "annualPrice_" + Constants.ENV);

    @Then("system display property details")
    public void system_display_property_details() {
        Assert.assertTrue(details.getPropertyID().contains(_propertyID), "Property ID contains " + _propertyID);
        Assert.assertEquals(details.getPropertyTitle(), _propertyName,"Property name not " + _propertyName);
        Assert.assertEquals(details.getPropertyType(), _kostType, "Property name not " + _kostType);
        Assert.assertEquals(details.getTotalRoom(), _totalRoom, "Total room not " + _totalRoom);
        Assert.assertEquals(details.getRoomFilled(), _roomFilled, "Room filled not " + _roomFilled);
        Assert.assertEquals(details.getEmptyRoom(), _emptyRoom, "Empty room not " + _emptyRoom);
        Assert.assertEquals(details.getOwnerName(), _ownerName, "Owner name not " + _ownerName);
        Assert.assertEquals(details.getOwnerNumber(), _ownerNumber, "Owner name not " + _ownerNumber);
        Assert.assertEquals(details.getManagerName(), _managerName, "Owner name not " + _managerName);
        Assert.assertEquals(details.getManagerNumber(), _managerNumber, "Owner name not " + _managerNumber);
        Assert.assertEquals(details.getArea(), _area, "Owner name not " + _area);
        Assert.assertEquals(details.getAddress(), _address, "Owner name not " + _address);
        Assert.assertEquals(details.getMonthlyPrice(), _monthlyPrice, "Owner name not " + _monthlyPrice);
        Assert.assertEquals(details.getDailyPrice(), _dailyPrice, "Owner name not " + _dailyPrice);
        Assert.assertEquals(details.getWeeklyPrice(), _weeklyPrice, "Owner name not " + _weeklyPrice);
        Assert.assertEquals(details.getThreeMonthlyPrice(), _threeMonthlyPrice, "Owner name not " + _threeMonthlyPrice);
        Assert.assertEquals(details.getSixMonthlyPrice(), _sixMonthlyPrice, "Owner name not " + _sixMonthlyPrice);
        Assert.assertEquals(details.getAnnualPrice(), _annualPrice, "Owner name not " + _annualPrice);
    }

    @When("user click property hyperlink")
    public void user_click_property_hyperlink() throws InterruptedException {
        details.clickOnPropertyHyperlink();
    }

    @When("user click update kos detail button")
    public void user_click_update_kos_detail_button() throws InterruptedException {
        details.clickOnUpdateKosDetail();
    }

    @When("user click update room allotment button")
    public void user_click_update_room_allotment_button() throws InterruptedException {
        details.clickOnUpdateRoom();
    }

}
