package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.UpdateKostPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class UpdateKostSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private UpdateKostPO update = new UpdateKostPO(driver);

    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String propertyName_ = JavaHelpers.getPropertyValue(consultantProperty,"propertyName_" + Constants.ENV);
    private String dangerAlertMessage_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessage_" + Constants.ENV);
    private String dangerAlertMessageFullAddressMustBeFill_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageFullAddressMustBeFill_" + Constants.ENV);
    private String dangerAlertMessageFullAddressMinimumCharacter_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageFullAddressMinimumCharacter_" + Constants.ENV);
    private String dangerAlertMessagePropertyNameMustBeFill_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessagePropertyNameMustBeFill_" + Constants.ENV);
    private String dangerAlertMessagePropertyNameMinimumCharacter_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessagePropertyNameMinimumCharacter_" + Constants.ENV);
    private String dangerAlertMessageRoomSizeFormat_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageRoomSizeFormat_" + Constants.ENV);
    private String dangerAlertMessageTotalRoomMustBeGreaterThanZero_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageTotalRoomMustBeGreaterThanZero_" + Constants.ENV);
    private String dangerAlertMessageEmptyRoomCanNotMoreThanTotalRoom_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageEmptyRoomCanNotMoreThanTotalRoom_" + Constants.ENV);
    private String dangerAlertMessageMonthlyPriceMustBeFill_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageMonthlyPriceMustBeFill_" + Constants.ENV);
    private String dangerAlertMessageDailyPriceMustBeFill_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageDailyPriceMustBeFill_" + Constants.ENV);
    private String dangerAlertMessageWeeklyPriceMustBeFill_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageWeeklyPriceMustBeFill_" + Constants.ENV);
    private String dangerAlertMessageQuarterlyPriceMustBeFill_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageQuarterlyPriceMustBeFill_" + Constants.ENV);
    private String dangerAlertMessageSixMonthlyPriceMustBeFill_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageSixMonthlyPriceMustBeFill_" + Constants.ENV);
    private String dangerAlertMessageAnnuallyPriceMustBeFill_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageAnnuallyPriceMustBeFill_" + Constants.ENV);
    private String dangerAlertMessageMonthlyPriceMustBeGreaterThan_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageMonthlyPriceMustBeGreaterThan_" + Constants.ENV);
    private String dangerAlertMessageDailyPriceMustBeGreaterThan_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageDailyPriceMustBeGreaterThan_" + Constants.ENV);
    private String dangerAlertMessageWeeklyPriceMustBeGreaterThan_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageWeeklyPriceMustBeGreaterThan_" + Constants.ENV);
    private String dangerAlertMessageQuarterlyPriceMustBeGreaterThan_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageQuarterlyPriceMustBeGreaterThan_" + Constants.ENV);
    private String dangerAlertMessageSixMonthlyPriceMustBeGreaterThan_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageSixMonthlyPriceMustBeGreaterThan_" + Constants.ENV);
    private String dangerAlertMessageSixAnnuallyPriceMustBeGreaterThan_ = JavaHelpers.getPropertyValue(consultantProperty,"dangerAlertMessageSixAnnuallyPriceMustBeGreaterThan_" + Constants.ENV);

    private String totalRoom_ = JavaHelpers.getPropertyValue(consultantProperty,"totalRoom_" + Constants.ENV);

    @When("user input {string} to field  full address")
    public void user_input_to_field_full_address(String fullAddress) throws InterruptedException {
        update.enterFullAddress(fullAddress);
    }

    @When("user click delete on keyboard")
    public void user_click_delete_on_keyboard() {
        update.sendKeysBackSpace();
    }

    @When("user input {string} to field  property name")
    public void user_input_to_field_property_name(String propertyName) throws InterruptedException {
        update.enterPropertyName(propertyName);
    }

    @Then("system display error message minimum character on field full address")
    public void system_display_error_message_minimum_character_on_field_full_address() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageFullAddressMinimumCharacter_), "Error message not present");
    }

    @Then("system display error message full address must be fill")
    public void system_display_error_message_full_address_must_be_fill() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageFullAddressMustBeFill_), "Error message not present");
    }

    @Then("system display error message minimum character on field property name")
    public void system_display_error_message_minimum_character_on_field_property_name() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessagePropertyNameMinimumCharacter_), "Error message not present");
    }

    @Then("system display error message property name must be fill")
    public void system_display_error_message_property_name_must_be_fill() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessagePropertyNameMustBeFill_), "Error message not present");
    }

    @Then("system display error message please fill room size appropriate format")
    public void system_display_error_message_please_fill_room_size_appropriate_format() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageRoomSizeFormat_), "Error message not present");
    }

    @Then("system display error message total room must be greater than zero")
    public void system_display_error_message_total_room_must_be_greater_than_zero() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageTotalRoomMustBeGreaterThanZero_), "Error message not present");
    }

    @Then("system display error message empty room cannot more than total room")
    public void system_display_error_message_empty_room_cannot_more_than_total_room() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageEmptyRoomCanNotMoreThanTotalRoom_), "Error message not present");
    }

    @Then("system display error message and next button is disable")
    public void system_display_error_message_and_next_button_is_disable() {
        Assert.assertEquals(update.getErrorMessage(), dangerAlertMessage_, "Message is different");
        Assert.assertTrue(update.isNextButtonDisable(), "Button is enable");
    }

    @Then("system display next button is disable")
    public void system_display_next_button_is_disable() {
        Assert.assertTrue(update.isNextButtonDisable(), "Button is enable");
    }

    @Then("system display next button is enable")
    public void system_display_next_button_is_enable() {
        Assert.assertFalse(update.isNextButtonDisable(), "Button is disable");
    }

    @When("user click next button")
    public void user_click_next_button() {
        update.clickOnNextButton();
    }

    @When("user click cancel button on page update property")
    public void user_click_cancel_button_on_page_update_property() throws InterruptedException {
        update.clickOnCancelButton();
    }

    @When("user revert property name")
    public void user_revert_property_name() throws InterruptedException {
        update.enterPropertyName(propertyName_);
    }

    @When("user input {string} to field room size")
    public void user_input_to_field_room_size(String roomSize) {
        update.enterRoomSize(roomSize);
    }

    @When("select room size is {string}")
    public void select_room_size_is(String roomSize) throws InterruptedException {
        update.selectRoomSize(roomSize);
    }

    @When("user input {string} to field total room")
    public void user_input_to_field_total_room(String totalRoom) {
        update.enterTotalRoom(totalRoom);
    }

    @When("user revert total room")
    public void user_revert_total_room() {
        update.enterTotalRoom(totalRoom_);
    }

    @When("user input empty room more than total room")
    public void user_input_empty_room_more_than_total_room() throws InterruptedException {
        int emptyRoom = Integer.parseInt(totalRoom_) + 1;
        update.enterEmptyRoom(String.valueOf(emptyRoom));
    }

    @When("user input empty room equal total room")
    public void user_input_empty_room_equal_total_room() throws InterruptedException {
        update.enterEmptyRoom(totalRoom_);
    }

    @When("user input {string} to field empty room")
    public void user_input_to_field_empty_room(String emptyRoom) throws InterruptedException {
        update.enterEmptyRoom(emptyRoom);
    }

    @When("user input {string} to field monthly price")
    public void user_input_to_field_monthly_price(String monthlyPrice) throws InterruptedException {
        update.enterMonthlyPrice(monthlyPrice);
    }

    @When("user input {string} to field daily price")
    public void user_input_to_field_daily_price(String dailyPrice) {
        update.enterDailyPrice(dailyPrice);
    }

    @When("user input {string} to field weekly price")
    public void user_input_to_field_weekly_price(String weeklyPrice) {
        update.enterWeeklyPrice(weeklyPrice);
    }

    @When("user input {string} to field quarterly price")
    public void user_input_to_field_quarterly_price(String quarterlyPrice) {
        update.enterQuarterlyPrice(quarterlyPrice);
    }

    @When("user input {string} to field six monthly price")
    public void user_input_to_field_six_monthly_price(String sixMonthlyPrice) {
        update.enterSixMonthlyPrice(sixMonthlyPrice);
    }

    @When("user input {string} to field annually price")
    public void user_input_to_field_annually_price(String annuallyPrice) {
        update.enterAnnuallyPrice(annuallyPrice);
    }

    @Then("system display error message monthly price must be filled")
    public void system_display_error_message_monthly_price_must_be_filled() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageMonthlyPriceMustBeFill_), "Error message not present");
    }

    @Then("system display error message monthly price minimum")
    public void system_display_error_message_monthly_price_minimum() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageMonthlyPriceMustBeGreaterThan_), "Error message not present");
    }

    @Then("system display error message daily price minimum")
    public void system_display_error_message_daily_price_minimum() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageDailyPriceMustBeGreaterThan_), "Error message not present");
    }

    @Then("system display error message weekly price minimum")
    public void system_display_error_message_weekly_price_minimum() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageWeeklyPriceMustBeGreaterThan_), "Error message not present");
    }

    @Then("system display error message quarterly price minimum")
    public void system_display_error_message_quarterly_price_minimum() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageQuarterlyPriceMustBeGreaterThan_), "Error message not present");
    }

    @Then("system display error message six monthly price minimum")
    public void system_display_error_message_six_monthly_price_minimum() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageSixMonthlyPriceMustBeGreaterThan_), "Error message not present");
    }

    @Then("system display error message annually price minimum")
    public void system_display_error_message_annually_price_minimum() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageSixAnnuallyPriceMustBeGreaterThan_), "Error message not present");
    }

    @Then("system display error message daily price must be filled")
    public void system_display_error_message_daily_price_must_be_filled() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageDailyPriceMustBeFill_), "Error message not present");
    }

    @Then("system display error message weekly price must be filled")
    public void system_display_error_message_weekly_price_must_be_filled() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageWeeklyPriceMustBeFill_), "Error message not present");
    }

    @Then("system display error message quarterly price must be filled")
    public void system_display_error_message_quarterly_price_must_be_filled() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageQuarterlyPriceMustBeFill_), "Error message not present");
    }

    @Then("system display error message six monthly price must be filled")
    public void system_display_error_message_six_monthly_price_must_be_filled() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageSixMonthlyPriceMustBeFill_), "Error message not present");
    }

    @Then("system display error message annually price must be filled")
    public void system_display_error_message_annually_price_must_be_filled() throws InterruptedException {
        Assert.assertTrue(update.errorMessageIsPresent(dangerAlertMessageAnnuallyPriceMustBeFill_), "Error message not present");
    }
}
