package steps.consultanttools;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.ManagePropertyPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class ManagePropertySteps {

    private WebDriver driver = ThreadManager.getDriver();
    private ManagePropertyPO manageProperty = new ManagePropertyPO(driver);

    //Test Data Consultant
    private String consultantProperty="src/test/resources/testdata/consultant/consultant.properties";
    private String _propertyID = JavaHelpers.getPropertyValue(consultantProperty,"propertyID_" + Constants.ENV);
    private String _propertyName = JavaHelpers.getPropertyValue(consultantProperty,"propertyName_" + Constants.ENV);
    private String consultantPropertyName = JavaHelpers.getPropertyValue(consultantProperty,"kosName_tobelo_" + Constants.ENV);

    public Integer totalKamarTerisi = 0;

    @When("user search property using keyword property name")
    public void user_search_property_using_keyword_property_name() throws InterruptedException {
        manageProperty.enterAndSearchKost(_propertyName);
    }

    @When("user search consultant property")
    public void user_search_consultant_property() throws InterruptedException {
        manageProperty.enterAndSearchKost(consultantPropertyName);
    }

    @When("user search property {string}")
    public void user_search_property(String keyword) throws InterruptedException {
        manageProperty.enterAndSearchKost(keyword);
    }

    @When("user choose property {string}")
    public void user_choose_property(String keyword) throws InterruptedException {
        int numberOfList = manageProperty.getNumberOfList();
        for (int i = 1 ; i <= numberOfList ; i++){
            if(keyword.equals(manageProperty.getPropertyName(i))){
                manageProperty.clickOnProperty(i);
                break;
            }
        }
    }

    @Then("system display property name and verification status is {string}")
    public void system_display_property_name_and_verification_status_is(String verificationStatus) throws InterruptedException {
        int numberOfList = manageProperty.getNumberOfList();

        for (int i = 1 ; i <= numberOfList ; i++){
            if(_propertyName.equals(manageProperty.getPropertyName(i)) && verificationStatus.equals(manageProperty.getVerificationStatus(i))){
                manageProperty.clickOnProperty(i);
                break;
            }
        }
    }

    @When("user choose consultant property")
    public void user_choose_consultant_property() throws InterruptedException {
        int numberOfList = manageProperty.getNumberOfList();
        for (int i = 1 ; i <= numberOfList ; i++) {
            if (consultantPropertyName.equals(manageProperty.getPropertyName(i))) {
                manageProperty.clickOnProperty(i);
                break;
            }
        }
    }

    @When("user click update ketersediaan kamar button")
    public void user_click_update_ketersediaan_kamar_button() throws InterruptedException {
        manageProperty.clickUpdateKetersediaanKamar();
    }

    @When("user set all room to sudah berpenghuni")
    public void user_set_all_room_to_sudah_berpenghuni() throws InterruptedException {
        manageProperty.clickTabKosong();
        manageProperty.setAllRoomSudahBerpenghuni();
    }

    @Then("user set {int} room to available")
    public void user_set_room_to_available(Integer counter) throws InterruptedException {
        manageProperty.clickTabTerisi();
        manageProperty.setNRoomToAvailable(counter);
    }

    @And("user save total room terisi")
    public void userSaveTotalRoomTerisi() throws InterruptedException {
        totalKamarTerisi = manageProperty.getTotalRoomTerisi();
    }

    @And("total number room terisi increases by one")
    public void totalNumberRoomTerisiIncreasesByOne() throws InterruptedException {
        Assert.assertEquals(manageProperty.getTotalRoomTerisi(),(totalKamarTerisi+=1),"total kamar terisi tidak bertambah");
    }

    @And("user check at least {int} room available")
    public void userCheckAtLeastRoomAvailable(int availableRoom) throws InterruptedException {
        manageProperty.clickTabKosong();
        if(manageProperty.isKamarTidakDitemukanExist()){
            manageProperty.clickTabTerisi();
            manageProperty.setNRoomToAvailable(1);
        }
    }
}
