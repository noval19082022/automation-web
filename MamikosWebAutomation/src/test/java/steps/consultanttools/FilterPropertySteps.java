package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.*;
import utilities.ThreadManager;

public class FilterPropertySteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ManagePropertyPO manageProperty = new ManagePropertyPO(driver);
    private FilterPropertyPO filterProperty = new FilterPropertyPO(driver);

    @When("user filter property by kost level {string}")
    public void user_filter_property_by_kost_level(String kostLevel) throws InterruptedException {
        manageProperty.clickOnFilterButton();
        filterProperty.clickOnFilterParameter(kostLevel);
        filterProperty.clickOnApplyButton();
    }

    @Then("system display property with kost level {string}")
    public void system_display_property_with_kost_level(String kostLevel) throws InterruptedException {
        int numberOfList = manageProperty.getNumberOfList();

        for (int i = 1; i <= numberOfList; i++) {
            Assert.assertTrue(manageProperty.getKostLevel(i).equals(kostLevel), "Kost Level not " + kostLevel);
        }
    }
}
