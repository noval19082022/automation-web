package steps.consultanttools;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.*;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class SearchActiveTaskSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ManageTaskPO manageTask = new ManageTaskPO(driver);
    private SearchActiveTaskPO searchTask = new SearchActiveTaskPO(driver);
    private TaskDetailPO taskDetail  = new TaskDetailPO(driver);

    //Test Data Activity Management
    private String consultantTask="src/test/resources/testdata/consultant/taskManagement.properties";
    private String warningMessageMinimumKeyword = JavaHelpers.getPropertyValue(consultantTask,"dangerAlertMessageKeywordSearchMinimumCharacter_" + Constants.ENV);
    private String warningMessageNotSelectDataType = JavaHelpers.getPropertyValue(consultantTask,"dangerAlertMessageNotChooseDataTypeSearch_" + Constants.ENV);
    private String contractPhoneNumberTenant = JavaHelpers.getPropertyValue(consultantTask,"contractPhoneNumberTenant_" + Constants.ENV);
    private String propertyPhoneNumberOwner = JavaHelpers.getPropertyValue(consultantTask,"propertyPhoneNumberOwner_" + Constants.ENV);
    private String potentialTenantPhoneNumberTenant = JavaHelpers.getPropertyValue(consultantTask,"potentialTenantPhoneNumberTenant_" + Constants.ENV);
    private String potentialOwnerPhoneNumberOwner = JavaHelpers.getPropertyValue(consultantTask,"potentialOwnerPhoneNumberOwner_" + Constants.ENV);
    private String potentialPropertyPhoneNumberOwner = JavaHelpers.getPropertyValue(consultantTask,"potentialPropertyPhoneNumberOwner_" + Constants.ENV);

    @And("user search active task with keyword {string}")
    public void user_search_active_task_with_keyword(String keyword) throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(keyword);
        searchTask.clickDataProperty();
    }

    @Then("system display warning message minimum character")
    public void system_display_warning_message_minimum_character() throws InterruptedException {
        Assert.assertEquals(searchTask.getErrorMessage(), warningMessageMinimumKeyword, "error message not" + warningMessageMinimumKeyword);
    }

    @When("user search active task with keyword {string} without select data type")
    public void user_search_active_task_with_keyword_without_select_data_type(String keyword) throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskWithoutSelectDataType(keyword);
    }

    @Then("system display warning message choose data type")
    public void system_display_warning_message_choose_data_type() throws InterruptedException {
        Assert.assertEquals(searchTask.getErrorMessage(), warningMessageNotSelectDataType, "error message not" + warningMessageNotSelectDataType);
    }

    @Then("system display empty state {string}")
    public void system_display_empty_state(String emptyState) {
        Assert.assertTrue(searchTask.getEmptyState().contains(emptyState), "Empt state not contains " + emptyState);
    }

    @When("user search task contract by tenant phone number")
    public void user_search_task_contract_by_tenant_phone_number() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(contractPhoneNumberTenant);
        searchTask.clickDataContract();
    }

    @Then("user verify search result is contract with that tenant phone number")
    public void user_verify_search_result_is_contract_with_that_tenant_phone_number() throws InterruptedException {
        searchTask.clickTaskOnSearchResult();
        Assert.assertTrue(taskDetail.getTaskDetail().contains(contractPhoneNumberTenant), "Detail task not contains " + contractPhoneNumberTenant);
    }

    @When("user search task property by owner phone number")
    public void user_search_task_property_by_owner_phone_number() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(propertyPhoneNumberOwner);
        searchTask.clickDataProperty();
    }

    @Then("user verify search result is property with that owner phone number")
    public void user_verify_search_result_is_property_with_that_owner_phone_number() throws InterruptedException {
        searchTask.clickTaskOnSearchResult();
        Assert.assertTrue(taskDetail.getTaskDetail().contains(propertyPhoneNumberOwner), "Detail task not contains " + propertyPhoneNumberOwner);
    }

    @When("user search task potential tenant by tenant phone number")
    public void user_search_task_potential_tenant_by_tenant_phone_number() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(potentialTenantPhoneNumberTenant);
        searchTask.clickDataTenant();
    }

    @Then("user verify search result is potential tenant with that tenant phone number")
    public void user_verify_search_result_is_potential_tenant_with_that_tenant_phone_number() throws InterruptedException {
        searchTask.clickTaskOnSearchResult();
        Assert.assertTrue(taskDetail.getTaskDetail().contains(potentialTenantPhoneNumberTenant), "Detail task not contains " + potentialTenantPhoneNumberTenant);
    }

    @When("user search task potential owner by owner phone number")
    public void user_search_task_potential_owner_by_owner_phone_number() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(potentialOwnerPhoneNumberOwner);
        searchTask.clickDataPotentialOwner();
    }

    @Then("user verify search result is potential owner with that owner phone number")
    public void user_verify_search_result_is_potential_owner_with_that_owner_phone_number() throws InterruptedException {
        searchTask.clickTaskOnSearchResult();
        Assert.assertTrue(taskDetail.getTaskDetail().contains(potentialOwnerPhoneNumberOwner), "Detail Task not contains " + potentialOwnerPhoneNumberOwner);
    }

    @When("user search task potential property by owner phone number")
    public void user_search_task_potential_property_by_owner_phone_number() throws InterruptedException {
        manageTask.clickSearchFieldActiveTask();
        searchTask.searchActiveTaskField(potentialPropertyPhoneNumberOwner);
        searchTask.clickDataPotentialProperty();
    }

    @Then("user verify search result is potential property with that owner phone number")
    public void user_verify_search_result_is_potential_property_with_that_owner_phone_number() throws InterruptedException {
        searchTask.clickTaskOnSearchResult();
        Assert.assertTrue(taskDetail.getTaskDetail().contains(potentialPropertyPhoneNumberOwner), "Detail task not contains " + potentialPropertyPhoneNumberOwner);
    }

}
