package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.RoomAllotmentPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class RoomAllotmentSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private RoomAllotmentPO roomAllotment = new RoomAllotmentPO(driver);

    //Test Data
    private String consultantProperty = "src/test/resources/testdata/consultant/consultant.properties";
    private String defaultRoom = JavaHelpers.getPropertyValue(consultantProperty, "defaultRoomName_" + Constants.ENV);
    private String updateRoom = JavaHelpers.getPropertyValue(consultantProperty, "updateRoomName_" + Constants.ENV);
    private String defaultFloor = JavaHelpers.getPropertyValue(consultantProperty, "defaultFloorName_" + Constants.ENV);
    private String updateFloor = JavaHelpers.getPropertyValue(consultantProperty, "updateFloorName_" + Constants.ENV);
    private String longRoomName = JavaHelpers.getPropertyValue(consultantProperty, "longRoomName_" + Constants.ENV);
    private String warningMessageMaxLength = JavaHelpers.getPropertyValue(consultantProperty, "warningMessageMaxLength_" + Constants.ENV);
    private String existingRoomName = JavaHelpers.getPropertyValue(consultantProperty, "existingRoomName_" + Constants.ENV);
    private String warningMessageDuplicateRoomName = JavaHelpers.getPropertyValue(consultantProperty, "warningMessageDuplicateRoomName_" + Constants.ENV);
    private String warningMessageEmptyRoomName = JavaHelpers.getPropertyValue(consultantProperty, "warningMessageEmptyRoomName_" + Constants.ENV);
    private String newRoomName = JavaHelpers.getPropertyValue(consultantProperty, "newRoomName_" + Constants.ENV);
    private String noRoom = JavaHelpers.getPropertyValue(consultantProperty, "noRoomFound_" + Constants.ENV);
    private String successUpdateMessage = JavaHelpers.getPropertyValue(consultantProperty, "successUpdateMessage_" + Constants.ENV);

    @When("user update room name and room floor on room allotment page")
    public void user_update_room_name_and_room_floor_on_room_allotment_page() throws InterruptedException {
        roomAllotment.setRoomNameBlank();
        roomAllotment.setRoomName(updateRoom);
        roomAllotment.setFloorNameBlank();
        roomAllotment.setFloorName(updateFloor);
        roomAllotment.clickOnUpdateButton();
    }

    @Then("user verify room name and room floor updated")
    public void user_verify_room_name_updated() throws InterruptedException {
        roomAllotment.enterAndSearchRoomName(updateRoom);
        Assert.assertEquals(roomAllotment.getRoomName(), updateRoom, "room name not match");
        Assert.assertEquals(roomAllotment.getFloorName(), updateFloor, "floor name not match");
    }

    @Then("user revert room name and floor name to default")
    public void user_revert_room_name_and_floor_name_to_default() throws InterruptedException {
        roomAllotment.clearSearchRoomName();
        roomAllotment.setRoomNameBlank();
        roomAllotment.setRoomName(defaultRoom);
        roomAllotment.setFloorNameBlank();
        roomAllotment.setFloorName(defaultFloor);
        roomAllotment.clickOnUpdateButton();
    }

    @Then("user verify room name and floor name set to default")
    public void user_verify_room_name_and_floor_name_set_to_default() throws InterruptedException {
        roomAllotment.enterAndSearchRoomName(defaultRoom);
        Assert.assertEquals(roomAllotment.getRoomName(), defaultRoom, "room name not match");
        Assert.assertEquals(roomAllotment.getFloorName(), defaultFloor, "floor name not match");
    }

    @When("user set room name more than maximum character")
    public void user_set_room_name_more_than_maximum_character() throws InterruptedException {
        roomAllotment.setRoomNameBlank();
        roomAllotment.setRoomName(longRoomName);
    }

    @Then("user verify there is error message maximum character")
    public void user_verify_there_is_error_message_maximum_character() throws InterruptedException {
        Assert.assertEquals(roomAllotment.getErrorMessage(), warningMessageMaxLength, "error message not match");
    }

    @Then("user set room name with existing room name")
    public void user_set_room_name_with_existing_room_name() throws InterruptedException {
        roomAllotment.setRoomNameBlank();
        roomAllotment.setRoomName(existingRoomName);
    }

    @Then("user verify there is error message duplicate room name")
    public void user_verify_there_is_error_message_duplicate_room_name() throws InterruptedException {
        Assert.assertEquals(roomAllotment.getErrorMessage(), warningMessageDuplicateRoomName, "error message not match");
    }

    @When("user set room name blank")
    public void user_set_room_name_blank() throws InterruptedException {
        roomAllotment.setRoomNameBlank();
    }

    @Then("user verify there is error message room name can not blank")
    public void user_verify_there_is_error_message_room_name_can_not_blank() throws InterruptedException {
        Assert.assertEquals(roomAllotment.getEmptyRoomErrorMessage(), warningMessageEmptyRoomName, "error message not match");
    }

    @When("user back to previous page")
    public void user_back_to_previous_page() throws InterruptedException {
        roomAllotment.clickOnBackButton();
    }

    @Then("user verify still on room allotment page")
    public void user_verify_still_on_room_allotment_page() throws InterruptedException {
        roomAllotment.clickOnDisagreeButton();
        Assert.assertEquals(roomAllotment.getTitlePage(), "Update Ketersediaan Kamar", "page title not match");
    }

    @Then("user verify update kost price page display")
    public void user_verify_update_kost_price_page_display() throws InterruptedException {
        roomAllotment.clickOnAgreeButton();
        Assert.assertEquals(roomAllotment.getTitlePage(), "Update Kamar dan Harga", "page title not match");
    }

    @When("user add new room")
    public void user_add_new_room() throws InterruptedException {
        roomAllotment.clickOnAddRoomButton();
        roomAllotment.clickOnAgreeAddRoomButton();
        roomAllotment.setRoomName(newRoomName);
        roomAllotment.clickOnUpdateButton();
    }

    @Then("user verify new room display on room list")
    public void user_verify_new_room_display_on_room_list() throws InterruptedException {
        roomAllotment.enterAndSearchRoomName(newRoomName);
        Assert.assertEquals(roomAllotment.getRoomName(), newRoomName, "room name not match");
    }

    @When("user mark room occupied")
    public void user_mark_room_occupied() throws InterruptedException {
        roomAllotment.clickOnOccupiedRoomButton();
        roomAllotment.clickOnConfirmOccupiedButton();
        roomAllotment.clickOnUpdateButton();
    }

    @Then("user verify update room occupied success")
    public void user_verify_update_room_occupied_success() throws InterruptedException {
        Assert.assertEquals(roomAllotment.getSuccessMessage(), successUpdateMessage, "message not match");
    }

    @When("user delete room")
    public void user_delete_room() throws InterruptedException {
        roomAllotment.clickOnDeleteRoomButton();
        roomAllotment.clickOnAgreeDeleteButton();
    }

    @Then("user verify room not found on room list")
    public void user_verify_room_not_found_on_room_list() throws InterruptedException {
        Assert.assertEquals(roomAllotment.getEmptyState(), noRoom, "empty state not match");
    }

    @When("delete room if room have been existing")
    public void delete_room_if_room_have_been_existing() throws InterruptedException {
        if(roomAllotment.getRoomName().equals(newRoomName))
        {
            roomAllotment.enterAndSearchRoomName(newRoomName);
            roomAllotment.clickOnDeleteRoomButton();
            roomAllotment.clickOnAgreeDeleteButton();
        }
    }
}
