package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobjects.consultanttools.AddNewContractPO;
import pageobjects.consultanttools.ListPotentialTenantPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class ListPotentialTenantSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private ListPotentialTenantPO listPotentialTenant = new ListPotentialTenantPO(driver);
    private AddNewContractPO newContract = new AddNewContractPO(driver);

    //Test Data
    private String consultant = "src/test/resources/testdata/consultant/consultant.properties";
    private String tenantProperties = "src/test/resources/testdata/consultant/tenant.properties";
    private String potentialTenantPhoneNumber = JavaHelpers.getPropertyValue(tenantProperties, "tenantHp_yudha_" + Constants.ENV);

    //Test Data Consultant
    private String potentialTenant="src/test/resources/testdata/consultant/potentialTenant.properties";
    private String numberPhoneNewPotentialTenant = JavaHelpers.getPropertyValue(potentialTenant, "tenantPhoneNumberDefault_" + Constants.ENV);
    private String tenantPhoneNumber = JavaHelpers.getPropertyValue(potentialTenant, "tenantPhoneNumberUpdate_" + Constants.ENV);


    @When("user search potential tenant by phone number {string}")
    public void user_search_potential_tenant_by_phone_number(String tenant) throws InterruptedException {
        if (tenant.equals("potentialTenant")){
            listPotentialTenant.searchTenant(potentialTenantPhoneNumber);
        }
    }

    @When("user create contract")
    public void user_create_contract() throws InterruptedException {
        listPotentialTenant.clickOnCreateContractButton();
        newContract.clickOnNextPersonalDataButton();
        newContract.setRoomNumberInContract();
        newContract.setCheckInDateToday();
        newContract.setRentDurationOneMonth("1 Bulan");
        newContract.clickOnSaveContractButton();
    }

    @Then("user click on save contract button")
    public void user_click_on_save_contract_button() throws InterruptedException {
        newContract.clickOnSaveContractButton();
    }

    @Then("system display contract successfully created")
    public void system_display_contract_successfully_created() {
        newContract.contractSuccessfullyCreatedIsAppeared();
    }

    @Then("system display create contract button is disable")
    public void system_display_create_contract_button_is_disable() {
        listPotentialTenant.createContractButtonIsDisabled();
    }

    @When("user search and choose property potential")
    public void user_search_and_choose_property_potential() throws InterruptedException {
        listPotentialTenant.searchTenantPotential(tenantPhoneNumber);
        listPotentialTenant.selectPotentialTenant();
    }

    @When("user click add new potential tenant button")
    public void user_click_add_new_potential_tenant_button() throws InterruptedException {
        listPotentialTenant.clickOnAddPotentialTenant();
    }

    @When("user select new potential tenant")
    public void user_select_new_potential_tenant() throws InterruptedException {
        listPotentialTenant.searchTenantPotential(numberPhoneNewPotentialTenant);
        listPotentialTenant.selectPotentialTenant();
    }
}
