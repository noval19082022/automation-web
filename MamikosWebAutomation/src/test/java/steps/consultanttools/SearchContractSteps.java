package steps.consultanttools;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.consultanttools.ActiveTenantDetailPO;
import pageobjects.consultanttools.InvoicePO;
import pageobjects.consultanttools.ManageContractPO;
import pageobjects.consultanttools.SearchContractPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class SearchContractSteps {

    private WebDriver driver = ThreadManager.getDriver();
    private ManageContractPO manageContract = new ManageContractPO(driver);
    private SearchContractPO searchContract = new SearchContractPO(driver);
    private InvoicePO invoiceContract = new InvoicePO(driver);
    private ActiveTenantDetailPO activeTenantDetail = new ActiveTenantDetailPO(driver);

    //Test Data
    private String consultant ="src/test/resources/testdata/consultant/consultant.properties";
    private String propertyName_ = JavaHelpers.getPropertyValue(consultant,"propertyName_" + Constants.ENV);
    private String tenantName_ = JavaHelpers.getPropertyValue(consultant,"activeTenantName_" + Constants.ENV);
    private String tenantPhoneNumber_ = JavaHelpers.getPropertyValue(consultant,"activeTenantPhoneNumber_" + Constants.ENV);

    @When("user search tenant contract by property name")
    public void user_search_tenant_contract_by_property_name() throws InterruptedException {
        manageContract.clickOnSearchBox();
        searchContract.setTextOnSearchTextBox(propertyName_);
    }

    @Then("system display contract list according to property name")
    public void system_display_contract_list_according_to_property_name() throws InterruptedException {
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1 ; i <= numberOfList ; i++){
            Assert.assertEquals(manageContract.getPropertyNameLabel(i), propertyName_, "Property not " + propertyName_);
        }
    }

    @When("user search tenant contract by tenant name")
    public void user_search_tenant_contract_by_tenant_name() throws InterruptedException {
        manageContract.clickOnSearchBox();
        searchContract.setTextOnSearchTextBox(tenantName_);
    }

    @Then("system display contract list according to tenant name")
    public void system_display_contract_list_according_to_tenant_name() throws InterruptedException {
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1 ; i <= numberOfList ; i++){
            Assert.assertEquals(manageContract.getTenantNameLabel(i), tenantName_, "Tenant Name not " + tenantName_);
        }
    }

    @When("user search tenant contract by tenant phone number")
    public void user_search_tenant_contract_by_tenant_phone_number() throws InterruptedException {
        manageContract.clickOnSearchBox();
        searchContract.setTextOnSearchTextBox(tenantPhoneNumber_);
    }

    @Then("system display contract list according to tenant phone number")
    public void system_display_contract_list_according_to_tenant_phone_number() throws InterruptedException {
        int numberOfList = manageContract.getNumberOfList();

        for (int i = 1 ; i <= numberOfList ; i++){
            if(manageContract.getLeftStatus(i).equals("Dibayar")) {
                manageContract.clickOnPropertyList(i);
                invoiceContract.clickOnSeeDetailTenantButton();
                Assert.assertEquals(activeTenantDetail.getTenantPhoneNumber(), tenantPhoneNumber_, "Tenant Phone Number not " + tenantPhoneNumber_);
                activeTenantDetail.clickOnBackToPrevPageButton();
                invoiceContract.clickOnBackToPrevPageIcon();
                break;
            }
            else if(manageContract.getLeftStatus(i).equals("Belum Dibayar")){
                manageContract.clickOnPropertyList(i);
                invoiceContract.clickOnSeeDetailTenantButton();
                Assert.assertEquals(activeTenantDetail.getTenantPhoneNumber(), tenantPhoneNumber_, "Tenant Phone Number not " + tenantPhoneNumber_);
                activeTenantDetail.clickOnBackToPrevPageButton();
                invoiceContract.clickOnBackToPrevPageIcon();
                break;
            }
            else{
                Assert.assertEquals(manageContract.getTenantNameLabel(i), tenantName_, "Tenant Name not " + tenantName_);
            }
        }
    }

    @When("user search tenant contract {string}")
    public void user_search_tenant_contract(String keyword) throws InterruptedException {
        manageContract.waitLoadingComplete();
        manageContract.clickOnSearchBox();
        searchContract.setTextOnSearchTextBox(keyword);
    }

    @Then("system display contract not found page")
    public void system_display_contract_not_found_page() {
        Assert.assertEquals(manageContract.getEmptyDataLabel(), "Belum Ada Kontrak", "Empty State Not" + "Belum Ada Kontrak");
    }

    @Then("user terminate contract if there is {string} contract")
    public void userTerminateContractIfThereIsContract(String status) throws InterruptedException {
        manageContract.waitLoadingComplete();
        if(manageContract.getFirstContractStatus().equalsIgnoreCase(status)){
            manageContract.terminateContractInList();
        }
    }
}
