package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import utilities.DriverManager;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.io.IOException;

public class Hooks
{
	DriverManager drivermanager = new DriverManager();
	JavaHelpers java = new JavaHelpers();
	
	@Before
    public void setUp(Scenario scenario) throws IOException, InterruptedException 
	{
		System.out.println("");
		//System.out.println("Scenario execute at: " + java.getLocalTimePipelineRun("yyyy-MM-dd HH:mm:ss"));
		drivermanager.setUp(ThreadManager.getBrowser());
		System.out.println(scenario.getName());
		System.out.println(scenario.getId());
    }
	
	@After
	public void tearDown(Scenario scenario) 
	{
		if (scenario.isFailed())
		{
			byte[] screenshotBytes = ((TakesScreenshot) ThreadManager.getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshotBytes, "image/png",scenario.getName());
		}

		drivermanager.tearDown();
		//System.out.println("Scenario end at: " + java.getLocalTimePipelineRun("yyyy-MM-dd HH:mm:ss"));
		System.out.println("");
	}
}
