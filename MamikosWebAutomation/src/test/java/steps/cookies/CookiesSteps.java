package steps.cookies;

import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import utilities.CookiesHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

import java.io.IOException;

public class CookiesSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SeleniumHelpers selenium = new SeleniumHelpers(driver);
    private CookiesHelpers cookies = new CookiesHelpers(driver);

    @When("store cookies data to {string} with name {string}")
    public void store_cookies_data(String pathName, String fileName) throws IOException {
        cookies.storeCookiesData(pathName, fileName);
    }

    @When("write cookies data from path {string} with file name {string}")
    public void write_cookied_data(String pathName, String fileName) throws IOException, InterruptedException {
        cookies.writeCookiesData(pathName, fileName);
    }

    @When("user clear all cookies data")
    public void user_clear_all_cookies_data() throws InterruptedException {
        cookies.clearAllCookiesData();
    }

}
