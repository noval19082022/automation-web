package steps.ownersini;

import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobjects.ownersini.NavbarPO;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.ThreadManager;

public class navbarSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private NavbarPO navbar = new NavbarPO(driver);

    //PMAN-Ownersini
    private String ownersini ="src/test/resources/testdata/ownersini/ownersiniAccount.properties";
    private String ownersiniDashboardUrl = JavaHelpers.getPropertyValue(ownersini, "ownersini_home_url_" + Constants.ENV);
    private String ownersDashboardUrl = JavaHelpers.getPropertyValue(ownersini, "owner_home_url_" + Constants.ENV);

    @Then("user redirect to ownersini dashboard")
    public void user_redirect_to_ownersini_dashboard() throws InterruptedException {
        Assert.assertEquals(navbar.getURL(),ownersiniDashboardUrl,"URL not equal with ownersini dashboard URL");
    }

    @When("user click profile")
    public void user_click_profile() throws InterruptedException {
        navbar.clickProfile();
    }

    @When("user click Kembali ke mamikos.com button")
    public void user_click_Kembali_ke_mamikos_com_button() throws InterruptedException {
        navbar.clickKembaliKeMamikos();
    }

    @Then("user redirect to owner dashboard")
    public void user_redirect_to_owner_dashboard() throws InterruptedException {
        Assert.assertEquals(navbar.getURL(),ownersDashboardUrl,"URL not equal with owner dashboard URL");
    }
}
