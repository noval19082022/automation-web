@singgahsini @regression @pman

Feature: Room Level

  @pman-prod @TEST_PMAN-3282
  Scenario: Admin - Verify Component of Page List Room Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Room Level" sub menu of management level
    Then system display content page list room level

  @TEST_PMAN-3266
  Scenario Outline: Verify Pagination in Room Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Room Level" sub menu of management level
    And user click on page number "<number>" list room level
    Then system display list room level page number "<number>" is active

    Examples:
      | number |
      | 2      |
      | 3      |