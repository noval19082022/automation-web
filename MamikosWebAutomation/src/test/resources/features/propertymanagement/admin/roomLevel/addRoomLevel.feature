@singgahsini @regression @pman

Feature: Update Room Level
  @TEST_PMAN-3286
  Scenario: Add Room Level Without Fill Level Name
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Room Level" sub menu of management level
    And user update room level with empty level name
    Then System display danger alert message "The name field is required."