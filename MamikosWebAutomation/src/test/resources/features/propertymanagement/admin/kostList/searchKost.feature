@singgahsini @regression @pman @pman-prod

Feature: Search Kost List
  @TEST_PMAN-3288
  Scenario: Admin - Kost List - Search Kost List
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Kost List" sub menu of management level
    And user search by kost name on kost list page
    Then system display kost list contains name