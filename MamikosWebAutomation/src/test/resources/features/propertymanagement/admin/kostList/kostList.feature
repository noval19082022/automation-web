@singgahsini @regression @pman @pman-prod

Feature: Kost List
  @TEST_PMAN-3269
  Scenario: Admin - Verify Component of Page Kost List
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Kost List" sub menu of management level
    Then system display content page kost list

  @TEST_PMAN-3274
  Scenario Outline: Verify pagination in Kost List
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Kost List" sub menu of management level
    And user click on page number "<number>" kost list
    Then system display kost list page number "<number>" is active

    Examples:
      | number |
      | 2      |
      | 3      |
      | 4      |

  @TEST_PMAN-3287
  Scenario: Verify pagination in Room List
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Kost List" sub menu of management level
    And user search kost pman
    And user click kost list actions "Room List"
    And user click on page number "2" kost list
    Then system display kost list page number "2" is active

  @TEST_PMAN-3268
  Scenario: Edit Kost Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Kost List" sub menu of management level
    And user search kost pman
    And user click edit kost level
    And change level to "Mamikos Goldplus 1 PROMO"
    And user save edit kost level
    # activate below steps if you need to run using headless chrome, no need if using chrome or firefox
    And user cancel pop up assign room level
    And user search kost pman
    Then level should be "Mamikos Goldplus 1 PROMO"
    #Scenario: revert back to Regular
    When user search kost pman
    And user click edit kost level
    And change level to "Reguler"
    And user save edit kost level
    # activate below steps if you need to run using headless chrome, no need if using chrome or firefox
    And user cancel pop up assign room level
    When user search kost pman
    Then level should be "Reguler"

  @TEST_PMAN-3276
  Scenario: Edit Room Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Kost List" sub menu of management level
    And user search kost pman
    And user click kost list actions "Room List"
    And user search room by room name "RoomAutomate"
    And user click room list action "Edit Room Level"
    And change level to "Mamikos Goldplus 1 PROMO"
    And user save edit kost level
    And user search room by room name "RoomAutomate"
    Then level should be "Mamikos Goldplus 1 PROMO"
    #Scenario: revert back room level
    When user search room by room name "RoomAutomate"
    And user click room list action "Edit Room Level"
    And change level to "Regular"
    And user save edit kost level
    And user search room by room name "RoomAutomate"
    Then level should be "Regular"

  @TEST_PMAN-3278
  Scenario: Assign All Rooms
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Kost List" sub menu of management level
    And user search kost pman
    And user click kost list actions "Room List"
    And user click Assign All button
    And change level to "Mamikos Goldplus 1 PROMO"
    And user save edit level popup
    Then all level should be change to "Mamikos Goldplus 1 PROMO"
    #Scenario: revert back all room to Regular
    When user click Assign All button
    And change level to "Regular"
    And user save edit level popup
    Then all level should be change to "Regular"