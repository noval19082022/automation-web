@singgahsini @regression @pman @pman-prod

Feature: Content FAQ
  @TEST_PMAN-3290
  Scenario: Verify FAQ Content
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "FAQ" sub menu of management level
    Then system display management level content