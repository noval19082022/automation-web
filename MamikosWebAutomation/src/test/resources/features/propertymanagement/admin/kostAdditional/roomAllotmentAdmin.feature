@regression @pman @admin @room-allotment-admin

  Feature: Kost Additional - Room Allotment
    Background: Terminate Tenant Contract and cancel booking if exist
      #checking contract
      Given user navigates to "backoffice"
      And user login  as a Admin via credentials
      And user click on Search Contract Menu form left bar
      Then user Navigate "Search Contract" page
      And user search by renter phone "085242455775"
      And user click on Cancel the Contract Button from action column
      #checking booking
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "PMAN Tenant"
      And user navigates to "mamikos /user/booking/"
      And tenant cancel all need confirmation booking request

    @TEST_PMAN-5503
    Scenario: Pilih Ditempat not assign to out of order room
      #create booking
      Given user navigates to "mamikos /"
      And user clicks search bar
      And I search property with name "Khusus Automation Tipe A" and select matching result to go to kos details page
      And user create booking for today
      And user selects T&C checkbox and clicks on Book button
      Then user navigates to main page after booking
      #mark room as out of order
      Given user navigates to "pms singgahsini"
      And user login as "pman admin"
      When user search property using ID "PMAN"
      And user click search property
      And user click homepage action button "Ketersediaan Kamar"
      Then user redirect to room allotment page
      When user set room "A1" as out of order 3 days
      Then room "A1" set as out of order
      #accept from data booking using room pilih ditempat
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin consultant"
      When user access bangkerupux menu "Data Booking"
      And user show filter data booking
      And user set filter kost type by "All Testing"
      And user search data booking using tenant phone "crm"
      And user apply filter
      And user go to confirm booking via action button
      And user navigates to "Down Payment" confirmation page data booking admin
      And user batalkan down payment
      And user navigates to "konfirmasi" confirmation page data booking admin
      And user confirm booking from admin
      #check Room A1 and A2 Status in kost additional
      When user access bangkerupux menu "Kost Additional"
      And user search "Kost Apik Khusus Automation PMAN Tipe A" in kost additional
      And user click on atur ketersediaan button
      Then status room "A1" should be equal to "Out of order"
      And status room "A2" should be equal to "Terisi"