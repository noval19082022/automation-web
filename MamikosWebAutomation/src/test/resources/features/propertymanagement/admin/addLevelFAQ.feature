@singgahsini @regression @pman

Feature: Add FAQ

  Background: Navigate to FAQ Menu
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "FAQ" sub menu of management level

  Scenario: Delete FAQ if already exist
    Given user delete level faq

  @TEST_PMAN-3283
  Scenario: Add FAQ
    When user add level faq
    Then System display alert message "FAQ successfully added."
    And system display new level faq

  @TEST_PMAN-3253
  Scenario: Delete FAQ
    When user delete level faq
    Then System display alert message "FAQ successfully deleted."
    And system display new level faq is not found