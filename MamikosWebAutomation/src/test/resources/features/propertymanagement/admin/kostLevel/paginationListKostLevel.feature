@singgahsini @regression @pman

Feature: Pagination List Kost Level
  @TEST_PMAN-3289
  Scenario Outline: Verify pagination in Kost Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Kost Level" sub menu of management level
    And user click on page number "<number>" list kost level
    Then system display list kost level page number "<number>" is active

    Examples:
      | number |
      | 2      |
      | 3      |
      | 4      |