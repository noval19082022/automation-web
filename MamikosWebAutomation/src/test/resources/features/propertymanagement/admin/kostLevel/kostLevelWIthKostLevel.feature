@singgahsini @regression @pman

Feature: Kost Level with Room Level

  Background: Navigate to Kost Level Menu
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access menu "Kost Level" sub menu of management level

  Scenario: Delete kost level if already exist
    Given user delete kost level

  @TEST_PMAN-1882
  Scenario: Add Kost Level
    When user create kost level
    Then system display new kost level

  @TEST_PMAN-3270
  Scenario: Update Kost Level Without Fill Level Name
    When user update kost level with empty level name
    Then System display popup message "The name field is required."

  @TEST_PMAN-1817
  Scenario: Edit Kost Level
    When user update kost level
    Then system display updated kost level

  @TEST_PMAN-1819
  Scenario: Delete Kost Level
    When user delete kost level
    Then System display alert message "Level successfully deleted."
    And system display new kost level is not found