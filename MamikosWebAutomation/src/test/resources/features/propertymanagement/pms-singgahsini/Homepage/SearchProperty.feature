@regression @pman @pman-prod @pms @homepagepms

Feature: Homepage
  Background: Navigate to PMS
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"

  @TEST_PMAN-3587
  Scenario: Search using property ID
    When user search property using ID "PMAN"
    And user click search property
    Then property with ID "PMAN" should be found
    #Scenario: Search using invalid property ID
    When user search property using ID "0101"
    And user click search property
    Then property empty page should be displayed

  @TEST_PMAN-5250
  Scenario: Search using property name
    When user search property using name "PMAN"
    And user click search property
    Then property with name "PMAN" should be found
    #Scenario: Search using incomplete property name
    When user search property using name "abcd"
    And user click search property
    Then property empty page should be displayed

  @TEST_PMAN-5251
  Scenario: Reset search value
    When user search property using ID "PMAN"
    And user click x button in search property
    Then search property field should be empty
    When user search property using name "PMAN"
    And user click x button in search property
    Then search property field should be empty