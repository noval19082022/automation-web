@regression @pman @pman-prod @pms @homepagepms

Feature: Homepage
  Background: Navigate to PMS
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"

  @TEST_PMAN-3597
  Scenario: View Detail Property Page
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Lihat Detail"
    Then user redirect to detail property page

  @TEST_PMAN-3598
  Scenario: View Room Allotment Page
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Ketersediaan Kamar"
    Then user redirect to room allotment page

  @TEST_PMAN-3583
  Scenario: Apply Some Filters
    When user clicks button filter
    And user inputs values into filter using data "PMAN"
    And user clicks Terapkan
    Then property with ID "PMAN" should be found