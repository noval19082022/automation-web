@regression @pman @pms @owner-expenditure

  Feature: Add Owner Expenditure
    Background: Navigate to PMS
      Given user navigates to "pms singgahsini"
      And user login as "pman admin"
      And user open other transaction menu

    @TEST_PMAN-6038
    Scenario: Add valid owner expenditure
      When user add new data owner expenditure
      And user choose tipe pengajuan cashout "Reimbursement"
      And user select property "Khusus"
      And user input multiple pengeluaran :
        | no  | Kategori Pengeluaran        | Nama Pengeluaran  | Kuantitas | Nominal Pengeluaran | Status Persediaan | Jenis Produk  |
        | 1   | Administrasi & Iuran Kos    | Wifi              | 1         | 50000               | Non Stock         | LSSS          |
        | 2   | Amenities Penyewa           | Sabun Mandi       | 1         | 20000               | Stock             | LSAP          |
        | 3   | Bahan Pembersih Kos & Dapur | Wipol             | 1         | 20000               | Stock             | PC            |
      And user upload "valid" lampiran
      And user input no invoice biaya "AT/PMAN/1001"
      And user input tujuan transfer "Ayu Putri (Internal Mamikos)"
      And user click tambah data
      Then confirmation pop up owner expenditure appear
      And should have confirmation title "Yakin ingin tambahkan data ini?"
      And should have confirmation description "Data yang ditambahkan akan dilanjutkan ke tahap konfirmasi."
      #cancel confirmation add owner expenditure
      When user "cancel" tambah data owner expenditure
      Then confirmation pop up should closed
      And tambah data button should be enable
      #confirm add owner expenditure
      When user click tambah data
      And user "confirm" tambah data owner expenditure
      Then toast message "Data berhasil ditambah" should be appear
      Then new owner expenditure record should be on the first list contains:
        | Tipe Pengajuan Cash Out | Nama Properti                                     | Total Pengeluaran |
        | Reimbursement           | Kost Apik Khusus Automation PMAN Halmahera Utara  | Rp90.000          |
      And should contains nama pengeluaran:
        | Wifi        |
        | Sabun Mandi |
        | Wipol       |