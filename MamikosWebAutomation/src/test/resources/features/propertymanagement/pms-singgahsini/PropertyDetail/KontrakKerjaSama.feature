@regression @pman @pms @detailpropertypms

Feature: Property Detail - Kontrak Kerja Sama
  Background: Navigate to PMS
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"

  @TEST_PMAN-3950
  Scenario: See and Edit Kontrak Kerja Sama - Profil Pemilik
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Lihat Detail"
    Then user redirect to detail property page
    When user click property detail tab "Kontrak Kerja Sama"
    Then profil pemilik section match with profil pemilik "PMAN"
    #Scenario: Ubah profil pemilik
    When user click ubah profil pemilik
    And user change profil pemilik using profil pemilik "PMAN Edit"
    Then profil pemilik section match with profil pemilik "PMAN Edit"
    #Scenario: Revert back profil pemilik
    When user click ubah profil pemilik
    And user change profil pemilik using profil pemilik "PMAN"
    Then profil pemilik section match with profil pemilik "PMAN"

  @TEST_PMAN-3948
  Scenario: See and Edit Kontrak Kerja Sama - Informasi Transfer Pendapatan
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Lihat Detail"
    Then user redirect to detail property page
    When user click property detail tab "Kontrak Kerja Sama"
    Then informasi transfer pendapatan section match with informasi transfer pendapatan "PMAN"
    #Scenario: edit informasi transfer pendapatan
    When user click ubah informasi transfer pendapatan
    And user change informasi transfer pendapatan "PMAN edit"
    Then informasi transfer pendapatan section match with informasi transfer pendapatan "PMAN edit"
    #Scenario: revert back informasi transfer pendapatan
    When user click ubah informasi transfer pendapatan
    And user change informasi transfer pendapatan "PMAN"
    Then informasi transfer pendapatan section match with informasi transfer pendapatan "PMAN"

  @TEST_PMAN-3949
  Scenario: See and Edit Kontrak Kerja Sama - Detail Kerja Sama
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Lihat Detail"
    Then user redirect to detail property page
    When user click property detail tab "Kontrak Kerja Sama"
    And user scroll to detail kerja sama section
    Then detail kerja sama section match with detail kerja sama "PMAN"
    # Scenario: Edit Detail Kerja Sama
    When user click ubah on detail kerja sama section
    And user change detail kerja sama using detail "PMAN Edit"
    Then user scroll to detail kerja sama section
    And detail kerja sama section match with detail kerja sama "PMAN Edit"
    # Scenario: Revert back Detail Kerja Sama
    When user click ubah on detail kerja sama section
    And user change detail kerja sama using detail "PMAN"
    Then user scroll to detail kerja sama section
    And detail kerja sama section match with detail kerja sama "PMAN"

  @TEST_PMAN-4041
  Scenario: See and Edit Kontrak Kerja Sama - Detail Kerja Sama Hybrid
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Lihat Detail"
    Then user redirect to detail property page
    When user click property detail tab "Kontrak Kerja Sama"
    And user scroll to detail kerja sama section
    Then detail kerja sama section match with detail kerja sama "PMAN"
    # Scenario: Edit Detail Kerja Sama - Hybrid
    When user click ubah on detail kerja sama section
    And user change details on Model kerja sama hybrid using detail "PMAN Hybrid on"
    Then user scroll to detail kerja sama section
    And detail kerja sama section match with detail kerja sama "PMAN Hybrid on"
    # Scenario: Revert back Detail Kerja Sama - Hybrid
    When user click ubah on detail kerja sama section
    And user change details on Model kerja sama hybrid using detail "PMAN Hybrid off"
    Then user scroll to detail kerja sama section
    And detail kerja sama section match with detail kerja sama "PMAN"

  @TEST_PMAN-4559
  Scenario: See, Add, Edit, and Delete Kontrak Kerja Sama - Biaya Tambahan
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Lihat Detail"
    Then user redirect to detail property page
    When user click property detail tab "Kontrak Kerja Sama"
    And user scroll to biaya tambahan section
    Then biaya tambahan section match with biaya tambahan "PMAN"
    #Scenario: Add new biaya tambahan
    When user click ubah biaya tambahan
    And user click Tambah Biaya button
    And user add biaya tambahan "Token Listrik" "50000"
    Then biaya tambahan "Token Listrik" with amount "Rp 50.000" should be appear
    #Scenario: Edit biaya tambahan
    When user click action button and ubah in biaya tambahan "Token Listrik"
    And user edit biaya tambahan "Pulsa Listrik" "60000"
    Then biaya tambahan "Pulsa Listrik" with amount "Rp 60.000" should be appear
    #Scenario: Delete biaya tambahan
    When user click action button and hapus in biaya tambahan "Pulsa Listrik"
    And user confirm hapus biaya tambahan
    Then biaya tambahan "Pulsa Listrik" should not exist in list

  @TEST_PMAN-3852
  Scenario: See Kontrak Kerja Sama - Rincian Tipe Kamar dan Harga
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Lihat Detail"
    Then user redirect to detail property page
    When user click property detail tab "Kontrak Kerja Sama"
    And user scroll to rincian harga kamar section
    Then rincian harga kamar section match with rincian harga kamar "PMAN"