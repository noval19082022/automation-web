@PMAN-5246 @regression @pman @pms @detailpropertypms @overview

Feature: Overview
  Background: Navigate to PMS
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"

  @TEST_PMAN-4621
  Scenario: See Overview Tab - Edit Penanggung Jawab
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Lihat Detail"
    Then user redirect to detail property page
    And penanggung jawab section match with profil penanggung jawab "PMAN"
    #Scenario: Edit Profil Penanggung Jawab
    When user click button edit on penanggung jawab section
    And user change profil penanggung jawab using profil penanggung jawab "PMAN Edit"
    Then penanggung jawab section match with profil penanggung jawab "PMAN Edit"
    #Scenario: Revert back Profil Penanggung Jawab
    When user click button edit on penanggung jawab section
    And user change profil penanggung jawab using profil penanggung jawab "PMAN"
    Then penanggung jawab section match with profil penanggung jawab "PMAN"

  @TEST_PMAN-4620
  Scenario: See Overview Tab - Edit Profile Property
    When user search property using ID "PMAN"
    And user click search property
    And user click homepage action button "Lihat Detail"
    Then user redirect to detail property page
    And profile property section match with profile property "PMAN"
    #Scenario: Edit Profile Properti
    When user click button ubah on profile property section
    And user change profile property using profile property "PMAN Edit"
    Then profile property section match with profile property "PMAN Edit"
    #Scenario: Revert back Profile Properti
    When user click button ubah on profile property section
    And user change profile property using profile property "PMAN"
    Then profile property section match with profile property "PMAN"