@regression @pman @pman-prod @pms @rolemanagement

  Feature: Role Management
    Background: Navigate to PMS
      Given user navigates to "pms singgahsini"
      And user login as "pman admin"

    @TEST_PMAN-3690
    Scenario: Add Role
      When user open role management menu
      #Scenario: Back from add role
      And user click on Tambah Role
      And user add new pms role name "Automation Test"
      And user choose permissions
        | Akses Homepage - Lihat Detail Properti  |
        | Akses Homepage - Unduh CSV              |
        | Akses Ketersediaan Kamar                |
      And user click back to role list
      And user search role "Automation Test"
      Then role tidak ditemukan page should be appear
      #Scenario: Reset permissions
      When user click on Tambah Role
      And user add new pms role name "Automation Test"
      And user choose permissions
        | Akses Homepage - Lihat Detail Properti  |
        | Akses Homepage - Unduh CSV              |
        | Akses Ketersediaan Kamar                |
      And user reset role permissions
      Then all permissions role are unchecked
      #scenario: Add New Role
      When user add new pms role name "Automation Test"
      And user choose permissions
        | Akses Homepage - Lihat Detail Properti  |
        | Akses Homepage - Unduh CSV              |
        | Akses Ketersediaan Kamar                |
      And user click simpan role
      Then toast message "Role berhasil disimpan." should be appear
      When user search role "Automation Test"
      Then Role "Automation Test" should be exist on the list

    @TEST_PMAN-5189
    Scenario: Add existing Role
      When user open role management menu
      And user click on Tambah Role
      And user add new pms role name "Automation Test"
      And user click simpan role
      Then error message "Nama role sudah digunakan. Masukkan nama role yang lain." should be appear

    @TEST_PMAN-3684
    Scenario: Search Role
      When user open role management menu
      And user search role "Automation Test"
      Then Role "Automation Test" should be exist on the list

    @TEST_PMAN-3687
    Scenario: Edit Role
      When user open role management menu
      And user search role "Automation Test"
      And user click action button
      And user choose role action "Edit"
      Then nama role should be "Automation Test"
      And permissions should be checked:
        | Akses Homepage - Lihat Detail Properti  |
        | Akses Homepage - Unduh CSV              |
        | Akses Ketersediaan Kamar                |
      #Scenario: edit role name with existing role
      When user change name to existing role name
      And user click simpan role
      Then error message "Nama role sudah digunakan. Masukkan nama role yang lain." should be appear
      #Scenario: edit role name using new role name
      When user change name to "Automation Test Edit"
      And user click simpan role
      Then toast message "Perubahan berhasil disimpan." should be appear
      When user search role "Automation Test Edit"
      Then Role "Automation Test Edit" should be exist on the list
      #revert role name
      When user click action button
      And user choose role action "Edit"
      And user change name to "Automation Test"
      And user click simpan role
      When user search role "Automation Test"
      Then Role "Automation Test" should be exist on the list

    @TEST_PMAN-3685
    Scenario: Assign Member to Role
      When user open role management menu
      And user search role "Automation Test"
      And user click action button
      And user choose role action "Atur Member"
      #scenario: add invalid member
      And user add member "invalid member"
      Then error message "Member tidak ditemukan" should be appear below tambah member field
      #scenario: add valid member
      When user add member "yudha"
      Then member "yudha" added

    @TEST_PMAN-3689
    Scenario: Check Button Availability According to Permissions
      When user logout
      And user login as "yudha"
      And user search singgahsini property
      Then button are available
        | Lihat Detail            |
        | Ketersediaan Kamar      |
        | Unduh CSV               |

    @TEST_PMAN-3695
    Scenario: Delete Member
      When user open role management menu
      And user search role "Automation Test"
      And user click action button
      And user choose role action "Atur Member"
      #Scenario: cancel hapus member
      When user cancel hapus member
      Then member "yudha" added
      #Scenario: confirm hapus member
      And user hapus member "yudha"
      Then member "yudha" removed from the list

    @TEST_PMAN-5322
    Scenario: Check Button Availability According to Permissions
      When user logout
      And user login as "yudha"
      And user search singgahsini property
      Then button are not available
        | Lihat Detail            |
        | Ketersediaan Kamar      |
        | Unduh CSV               |

    @TEST_PMAN-3692
    Scenario: Delete Role
      When user open role management menu
      And user search role "Automation Test"
      And user click action button
      And user choose role action "Hapus"
      #Scenario: cancel hapus role
      And user cancel hapus role
      Then Role "Automation Test" should be exist on the list
      #Scenario: confirm hapus role
      And user click action button
      And user choose role action "Hapus"
      And user confirm hapus role
      Then role tidak ditemukan page should be appear