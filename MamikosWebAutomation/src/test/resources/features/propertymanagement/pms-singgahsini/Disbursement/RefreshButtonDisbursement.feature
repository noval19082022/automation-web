@regression @pman @pms @disbursement @refreshDisbursement

Feature: PMS Disbursement Refresh Button
  Background: Navigate to PMS
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"

  @TEST_PMAN-5571
  Scenario: Button Refresh Availablity
    #Aprrove from list disbursement
    When user clicks disbursement menu on PMS navbar
    And user search disbursement property using name "PMAN"
    Then user verify Status data pendapatan is "Unapproved"
    When user click disbursement action button "Approve"
    And user confirm approve disbursement
    And user click disbursement action button "Lihat Detail"
    Then refresh button is not available
    When user clicks disbursement menu on PMS navbar
    And user search disbursement property using name "PMAN"
    And user click disbursement action button "Unapprove"
    And user confirm approve disbursement
    And user click disbursement action button "Lihat Detail"
    Then refresh button is available
    #Approve from detail disbursement
    When user clicks disbursement menu on PMS navbar
    And user search disbursement property using name "PMAN"
    Then user verify Status data pendapatan is "Unapproved"
    When user click disbursement action button "Lihat Detail"
    And user click approve Disbursement in detail disbursement
    And user confirm approve disbursement
    Then refresh button is not available
    When user click unapprove Disbursement in detail disbursement
    And user confirm approve disbursement
    Then refresh button is available