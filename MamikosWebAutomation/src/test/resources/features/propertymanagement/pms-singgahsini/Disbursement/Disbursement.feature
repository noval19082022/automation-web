@regression @pman @pman-prod @pms @disbursement

Feature: Disbursement PMS
  Background: Navigate to PMS
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"

  @TEST_PMAN-4473
  Scenario: Tambahan Pendapatan for Owner
    When user clicks disbursement menu on PMS navbar
    And user search disbursement property using name "PMAN"
    Then search result should match with profile "PMAN"
    #Scenario: Create Tambah Pendapatan
    When user click disbursement action button "Lihat Detail"
    And user redirect to Detail Transfer Pendapatan for "PMAN"
    And user clicks button Tambahkan at section "Tambahan Pendapatan"
    And user inputs Tambahan Pendapatan using detail "PMAN"
    Then Tambahan Pendapatan section will contain data as "PMAN"
    # Scenario: Edit Tambahan Pendapatan
    When user click tambahan pendapatan action button "Ubah"
    And user inputs Tambahan Pendapatan using detail "PMAN Edit"
    Then Tambahan Pendapatan section will contain data as "PMAN Edit"
    # Scenario: Delete Tambahan Pendapatan
    When user click tambahan pendapatan action button "Hapus"
    And user delete Tambahan Pendapatan
    Then Tambahan Pendapatan "PMAN Edit" will disappear from list