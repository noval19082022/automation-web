@regression @pman @pman-prod @pms @disbursement @disbursementNote

Feature: Disbursement note PMS
  Background: Navigate to PMS
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"

  @TEST_PMAN-5495
  Scenario: Valid notes when character NOT NULL & <=1500
    When user clicks disbursement menu on PMS navbar
    And user search disbursement property using name "PMAN"
    Then search result should match with profile "PMAN"
    When user click disbursement action button "Lihat Detail"
    And user redirect to Detail Transfer Pendapatan for "PMAN"
    And user click Ubah button at Keterangan tambahan untuk owner
    And user click Detail Keterangan field
    And user input characters "note <= 1500"
    And user click Simpan button in Keterangan Tambahan popup
    Then the data will be saved and toast appears
    And user will see Keterangan Tambahan value "note <= 1500"

  @TEST_PMAN-5572
  Scenario: Invalid notes when character NULL
    When user clicks disbursement menu on PMS navbar
    And user search disbursement property using name "Disbursement Prop"
    Then search result should match with profile "Disbursement Prop"
    When user click disbursement action button "Lihat Detail"
    And user redirect to Detail Transfer Pendapatan for "Disbursement Prop"
    And user will see Keterangan tambahan empty state
    And user click Ubah button at Keterangan tambahan untuk owner
    Then the Simpan button is disable

  @TEST_PMAN-5570
  Scenario: Invalid notes when character > 1500
    When user clicks disbursement menu on PMS navbar
    And user search disbursement property using name "PMAN"
    Then search result should match with profile "PMAN"
    When user click disbursement action button "Lihat Detail"
    And user redirect to Detail Transfer Pendapatan for "PMAN"
    And user click Ubah button at Keterangan tambahan untuk owner
    And user click Detail Keterangan field
    And user input characters "note > 1500"
    Then user will see the error message cannot be inputted more than 1500
    #Scenario: Invalid notes when user change character to NULL
    When user delete all note
    Then the empty state error message will be displayed