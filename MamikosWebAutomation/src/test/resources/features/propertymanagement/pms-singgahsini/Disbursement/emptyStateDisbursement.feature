@regression @pman @pman-prod @pms @disbursement

Feature: [PMS][Disbursement] Check empty state in the transfer pendapatan pemilik table

  Background: Navigate to PMS
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"

  @TEST_PMAN-5447
  Scenario: [PMS][Disbursement] Check empty state in the transfer pendapatan pemilik table
    When user clicks disbursement menu on PMS navbar
    And user click for next month disbursement
    And user search disbursement property using name "PMAN"
    Then search result should showing empty state