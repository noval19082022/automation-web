@regression @pman @pman-prod @ownersini

  Feature: Profile Navbar
    Background: Login to Owner Dashboard Pillar 1
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user fills out owner login as "ownersini" and click on Enter button
      Then user redirect to ownersini dashboard

    @TEST_PMAN-5491
    Scenario: Check Redirection to Owner Pillar 2
      When user click profile
      And user click Kembali ke mamikos.com button
      Then user redirect to owner dashboard