@singgahsini @regression @pman

Feature: See and Redirect to Another Section of Singgahsini Landing Page

  @TEST_PMAN-3185
  Scenario: Click on hyperlink Tentang Kami and System Auto Scroll to Tentang Kami Section
    Given user navigates to "singgahSini"
    When user click on hyperlink "Tentang Kami"
    Then system display "Tentang Kami" section

#  Scenario: Click on hyperlink Cara Bergabung and System Auto Scroll to Cara Bergabung Section
#    When user click on hyperlink "Cara Bergabung"
#    Then system display "Cara Bergabung" section

#  Scenario: Click on hyperlink Galeri and System Auto Scroll to Cara Galeri
#    When user click on hyperlink "Galeri"
#    Then system display "Galeri" section

#  Scenario: System display join now button
    #Then system display join now button

#  Scenario: Click hyperlink Manfaat on footer and System Auto Scroll to Section Manfaat
    When user click hyperlink "Manfaat" on footer
    Then system auto scroll up to section "Manfaat"

#  Scenario: Click hyperlink Tentang Kami on footer and System Auto Scroll to Section Tentang Kami
    When user click hyperlink "Tentang Kami" on footer
    Then system auto scroll up to section "Tentang Kami"

#  Scenario: Click hyperlink Cara Bergabung on footer and System Auto Scroll to Section Cara Bergabung
#    When user click hyperlink "Cara Bergabung" on footer
#    Then system auto scroll up to section "Cara Bergabung"

#  Scenario: Click hyperlink Pusat Informasi on footer and System Auto Scroll to Section Pusat Informasi
    When user click hyperlink "Tanya Jawab" on footer
    Then system auto scroll up to section "Tanya Jawab"

#  Scenario: Click hyperlink Galeri on footer and System Auto Scroll to Section Galeri
#    When user click hyperlink "Galeri" on footer
#    Then system auto scroll up to section "Galeri"

#  Scenario: Click on Social Media Facebook Icon and Open Social Media Facebook on New Tab
    When user click social media "Facebook"
    Then system open social media "Facebook" on new tab

#  Scenario: Click on Social Media Twitter Icon and Open Social Media Twitter on New Tab
    When user click social media "Twitter"
    Then system open social media "Twitter" on new tab

#  Scenario: Click on Social Media Instagram Icon and Open Social Media Instagram on New Tab
    When user click social media "Instagram"
    Then system open social media "Instagram" on new tab

#  Scenario: Click on Social Media Youtube Icon and Open Social Media Youtube on New Tab
    When user click social media "Youtube"
    Then system open social media "Youtube" on new tab
