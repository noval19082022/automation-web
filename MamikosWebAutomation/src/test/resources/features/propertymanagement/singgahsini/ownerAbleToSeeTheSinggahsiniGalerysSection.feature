@singgahsini @regression

Feature: Owner Able to See the Singgahsini Galery's Section

  @TEST_PMAN-3178
  Scenario: Owner Able to See the Singgahsini Galery's Section and contains
    Given user navigates to "singgahSini"
    When user scroll to "Galeri" section
    Then system display content "Galeri" section

#  Scenario: Click on View More Button on Gallery Section
    When user click on view more button
    Then user redirected to "mamikos singgahsini"
