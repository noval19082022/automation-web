@singgahsini @regression @pman @pman-prod

Feature: Owner Able to See the Singgahsini FAQ Section

  @TEST_PMAN-3176
  Scenario: Owner Able to See the Singgahsini FAQ Section and contains
    Given user navigates to "singgahSini"
    When user scroll to "FAQ" section
    Then system display content "FAQ" section