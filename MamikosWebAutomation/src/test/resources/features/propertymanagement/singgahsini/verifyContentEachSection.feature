@singgahsini @regression @pman @pman-prod

Feature: See Each Content Section
  Background: Navigate to Singgahsini
    Given user navigates to "singgahSini"

  @TEST_PMAN-3175
  Scenario: Verify Content of Benefits Sections
    When user click on hyperlink "Manfaat"
    Then system display "Manfaat" section
    And system display content "Manfaat" section

  @TEST_PMAN-3184
  Scenario: Verify Content of Introduction Sections
    When user scroll to "Owner Should Join Singgahsini" section
    Then system display "Owner Should Join Singgahsini" section
    And system display content "Owner Should Join the Singgahsini" section

  @TEST_PMAN-3180
  Scenario: Verify Content of Owner Testimonial
    When user scroll to "Testimonial" section
    Then system display "Testimonial" section
    And system display content "Testimonial" section

  @TEST_PMAN-3177
  Scenario: Verify Content of How to Join the Singgahsini Sections
    When user scroll to "Cara Bergabung" section
    Then system display "Cara Bergabung" section
    And system display content "Cara Bergabung" section
