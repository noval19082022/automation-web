@singgahsini @regression @pman @pman-prod

Feature: Join SinggahSini

  @checkErrorMessageWhenRegisterFormEmpty @TEST_PMAN-3202
  Scenario: Register - Verify title and description
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And system display description "Ingin tahu lebih lanjut dan daftar Singgahsini atau Apik? Silakan isi data diri dan kos yang ingin didaftarkan. Tim kami akan menghubungi Anda."

#  Scenario: Register - Check Error Message when Register form is Empty
    And user click button register
    Then user validate error message on empty register owner form
      | Masukkan nama lengkap Anda terlebih dahulu.      |
      | Masukkan no. HP Anda terlebih dahulu.            |
      | Masukkan nama kos Anda terlebih dahulu.          |
      | Anda belum memasukkan kabupaten / kota kos Anda. |
      | Anda belum memasukkan alamat kos Anda.           |

  @fillFormSinggahSini @TEST_PMAN-3203
  Scenario: Register - Fill form Register SinggahSini
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user input name with value "[Testing] Fathul"
    And user input phone number with value "081231231231"
    And user input kost name with value "Kost Testing"
    And user input city with value "Kota Jakarta Timur"
    And user select subdistrict "Pasar Rebo"
    And user select village "Pekayon"
    And user input address with value "jalan jalan"
    And user click button register
    Then user validate modal title equals to "Anda berhasil mengisi form pendaftaran"
    And user click on oke button
    Then user validate home page title equals to "Rekan Mengembangkan Bisnis Kos Anda"

  @userInput1CharacterOnFullNameForm @TEST_PMAN-3192
  Scenario: Register - User Input only 1 character on Full Name Form
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user input name with value "a"
    And user click button register
    Then user validate error message on full name form is equal to "Nama lengkap minimal mengandung 3 karakter."

  @userInputMoreThan50CharacterOnFullNameForm @TEST_PMAN-3193
  Scenario: Register - User Input more than 50 character on Full Name Form
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user input name with value "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    And user click button register
    Then user validate error message on full name form is equal to "Nama lengkap tidak boleh lebih dari 50 karakter."

  @userInput1CharacterOnPhoneNumberForm @TEST_PMAN-3194
  Scenario: Register - User Input only 1 character on Phone Number Form
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user input phone number with value "1"
    And user click button register
    Then user validate error message on phone number form is equal to "Harus terdiri dari 9-15 angka."

  @userInputMoreThan15CharacterOnPhoneNumberForm @TEST_PMAN-3195
  Scenario: Register - User Input more than 15 character on Phone Number Form
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user input phone number with value "1234567890123456"
    And user click button register
    Then user validate error message on phone number form is equal to "Harus terdiri dari 9-15 angka."

  @userInput1CharacterOnKostNameForm @TEST_PMAN-3196
  Scenario: Register - User Input only 1 character on Kost Name Form
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user input kost name with value "a"
    And user click button register
    Then user validate error message on kost name form is equal to "Nama kos minimal mengandung 3 karakter."

  @userInputMoreThan50CharacterOnKostNameForm @TEST_PMAN-3197
  Scenario: Register - User Input more than 50 character on Kost Name Form
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user input kost name with value "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    And user click button register
    Then user validate error message on kost name form is equal to "Nama kos tidak boleh lebih dari 30 karakter."

  @userInput1CharacterOnAddressForm @TEST_PMAN-3198
  Scenario: Register - User Input only 1 character on Address Form
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user input address with value "a"
    And user click button register
    Then user validate error message on address form is equal to "Alamat lengkap Kos minimal mengandung 3 karakter."

  @userBackToHomePage @TEST_PMAN-3199
  Scenario: Register - User Back to Homepage
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user click on back button
    Then user validate modal title equals to "Anda yakin ingin keluar dari halaman ini?"
    And user click on exit button
    Then user validate home page title equals to "Rekan Mengembangkan Bisnis Kos Anda"

  @userBackToRegistrationForm @TEST_PMAN-3200
  Scenario: Register - User Back to Registration Form
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user click on back button
    Then user validate modal title equals to "Anda yakin ingin keluar dari halaman ini?"
    And user click on next button
    Then user validate page title equals to "Selamat datang!"

  @userValidateErrorMessageOnSearchForm @TEST_PMAN-3201
  Scenario: Register - User Validate Error Message on Search Form
    Given user navigates to "singgahSini"
    And user click button gabung sekarang
    Then user validate page title equals to "Selamat datang!"
    And user input name with value "[Testing] Fathul"
    And user input phone number with value "081231231231"
    And user input kost name with value "Kost Testing"
    And user input city with wrong value "asd"
    Then user validate error message city search form is equal to "Kota tidak ditemukan"