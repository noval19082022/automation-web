@regression @pman @mamipay @invoice-manual @biayaTambahan

Feature: Invoice Manual - Biaya Tambahan
  Background: Navigate to Invoice Manual
    Given user navigates to "backoffice"
    When user login Backoffice as a "PMAN02" via credentials
    And user access mamipay menu "Invoice Manual"
    Then invoice manual page should be open
    #click Buat Invoice button
    When user click buat invoice
    Then user redirect to Buat Invoice manual page
    When user select listing "PMAN"
    And user select nama penyewa "PMAN"
    Then no HP penyewa should be autofill "PMAN"
    And no HP penyewa field is disabled
    And no kamar should be auto fill "PMAN"
    And no kamar field is disabled
    And user select jenis invoice "Biaya Tambahan"

  @TEST_PMAN-5595 @pman-prod
  Scenario Outline: Create Biaya Tambahan with Autofilled disburse to owner
    When user click Tambah button
    And user choose nama biaya "<nama biaya>"
    And user fill Lainnya field "<lainnya>"
    And user choose periode awal "<awal>"
    And user choose periode akhir "<akhir>"
    And user fill the durasi biaya "<durasi biaya>"
    And user input jumlah biaya "<jumlah biaya>"
    And user click Tambah button on the Biaya Tambahan pop up
    Then the "Biaya Tambahan" toast will be displayed
    #check biaya tambahan data in the biaya tambahan table
    And user will see nama biaya "<nama biaya table>" in the biaya tambahan table
    And user will see periode awal date "<awal table>" in the biaya tambahan table
    And user will see periode akhir date "<akhir table>" in the biaya tambahan table
    And user will see jumlah biaya "<jumlah biaya table>" in the biaya tambahan table
    And user will see disburse to pemilik "<disburse to pemilik>" in the biaya tambahan table

    Examples:
      | nama biaya                                  | lainnya | awal  | akhir     | durasi biaya  | jumlah biaya  | nama biaya table                                    | awal table  | akhir table | jumlah biaya table  | disburse to pemilik |
      | Parkir Mobil                                | -       | today | tomorrow  | 3 hari        | 25000         | Parkir Mobil (3 hari)                               | today       | tomorrow    | Rp25.000            | Ya                  |
      | Parkir Motor                                | -       | today | tomorrow  | -             | 10000         | Parkir Motor                                        | today       | tomorrow    | Rp10.000            | Ya                  |
      | Sekamar Berdua                              | -       | today | tomorrow  | 2 Minggu      | 50000         | Sekamar Berdua (2 Minggu)                           | today       | tomorrow    | Rp50.000            | Ya                  |
      | Tamu Menginap                               | -       | today | tomorrow  | 1 BULAN       | 45990         | Tamu Menginap (1 BULAN)                             | today       | tomorrow    | Rp45.990            | Ya                  |
      | Listrik                                     | -       | today | tomorrow  | -             | 7000          | Listrik                                             | today       | tomorrow    | Rp7.000             | Ya                  |
      | Air                                         | -       | today | tomorrow  | 11 hari       | 11000         | Air (11 hari)                                       | today       | tomorrow    | Rp11.000            | Ya                  |
      | Wifi                                        | -       | today | tomorrow  | seminggu      | 55000         | Wifi (seminggu)                                     | today       | tomorrow    | Rp55.000            | Ya                  |
      | Laundry                                     | -       | today | tomorrow  | -             | 35000         | Laundry                                             | today       | tomorrow    | Rp35.000            | Tidak               |
      | Deposit                                     | -       | -     | -         | sehari        | 100000        | Deposit (sehari)                                    | -           | -           | Rp100.000           | Tidak               |
      | Penggantian kerusakan/kehilangan fasilitas  | -       | today | tomorrow  | Kursi         | 50000         | Penggantian kerusakan/kehilangan fasilitas (Kursi)  | today       | tomorrow    | Rp50.000            | Tidak               |
      | Lainnya                                     | sampah  | today | tomorrow  | 1 hari        | 7500          | sampah (1 hari)                                     | today       | tomorrow    | Rp7.500             | Ya                  |

  @TEST_PMAN-5697 @pman-prod
  Scenario Outline: Check required fields in the biaya tambahan
    When user click Tambah button
    And user choose nama biaya "<nama biaya>"
    And user choose periode awal "<awal>"
    And user choose periode akhir "<akhir>"
    And user fill the durasi biaya "<durasi biaya>"
    And user input jumlah biaya "<jumlah biaya>"
    And user click Tambah button on the Biaya Tambahan pop up
    #check biaya tambahan data in the biaya tambahan table
    Then user will see nama biaya error message "<nama biaya error msg>"
    And user will see periode awal error message "<awal error msg>"
    And user will see periode akhir error message "<akhir error msg>"
    And user will see jumlah biaya error message "<jumlah biaya error msg>"

    Examples:
      | nama biaya      | awal  | akhir     | durasi biaya  | jumlah biaya  | nama biaya error msg            | awal error msg                    | akhir error msg                   | jumlah biaya error msg            |
      | -               | -     | -         | -             | -             | Nama biaya tidak boleh kosong.  | Periode awal tidak boleh kosong.  | Periode akhir tidak boleh kosong. | Jumlah biaya tidak boleh kosong.  |
      | Parkir Mobil    | -     | -         | -             | -             | -                               | Periode awal tidak boleh kosong.  | Periode akhir tidak boleh kosong. | Jumlah biaya tidak boleh kosong.  |
      | -               | today | -         | -             | -             | Nama biaya tidak boleh kosong.  | -                                 | Periode akhir tidak boleh kosong. | Jumlah biaya tidak boleh kosong.  |
      | -               | -     | -         | 1 hari        | -             | Nama biaya tidak boleh kosong.  | Periode awal tidak boleh kosong.  | Periode akhir tidak boleh kosong. | Jumlah biaya tidak boleh kosong.  |
      | -               | -     | -         | -             | 11000         | Nama biaya tidak boleh kosong.  | Periode awal tidak boleh kosong.  | Periode akhir tidak boleh kosong. | -                                 |

  @TEST_PMAN-5771 @pman-prod
  Scenario Outline: <button> modal tambah biaya sewa
    When user click Tambah button
    And user choose nama biaya "Parkir Mobil"
    And user choose periode awal "today"
    And user choose periode akhir "tomorrow"
    And user fill the durasi biaya "Park Fee"
    And user input jumlah biaya "25000"
    And user "<button>" modal tambah biaya
    Then tambah biaya modal closed
    When user click Tambah button
    Then all field is empty

    Examples:
      | button      |
      | close       |
      | kembali     |

  @TEST_PMAN-5773 @pman-prod
  Scenario: Periode is disabled when choose Deposit
    When user click Tambah button
    And user choose nama biaya "Deposit"
    Then periode awal should be disabled
    And periode akhir should be disabled

  @TEST_PMAN-5962 @pman-prod
  Scenario Outline: delete biaya tambahan
    When user click Tambah button
    And user choose nama biaya "<nama biaya>"
    And user fill Lainnya field "<lainnya>"
    And user choose periode awal "<awal>"
    And user choose periode akhir "<akhir>"
    And user fill the durasi biaya "<durasi biaya>"
    And user input jumlah biaya "<jumlah biaya>"
    And user click Tambah button on the Biaya Tambahan pop up
    Then the "Biaya Tambahan" toast will be displayed
    #delete biaya tambahan
    When user click delete button on invoice manual
    And user will see "biaya tambahan" delete confirmation pop up
    And user click "Batal" on delete confirmation pop up
    Then delete confirmation pop up is closed
    When user click delete button on invoice manual
    And user click "Hapus" on delete confirmation pop up
    Then the deletion toast will be displayed
    And delete confirmation pop up is closed
    And the empty state "biaya tambahan" will be displayed

    Examples:
      | nama biaya      | lainnya | awal  | akhir     | durasi biaya  | jumlah biaya  |
      | Parkir Mobil    | -       | today | tomorrow  | 3 hari        | 25000         |

  @TEST_PMAN-5964 @pman-prod
  Scenario: delete multiple biaya tambahan
    When user click Tambah button
    And user choose nama biaya "Parkir Mobil"
    And user choose periode awal "today"
    And user choose periode akhir "tomorrow"
    And user fill the durasi biaya "automation pman"
    And user input jumlah biaya "50000"
    And user click Tambah button on the Biaya Sewa pop up
    Then the "Biaya Tambahan" toast will be displayed
    And "biaya tambahan" "Parkir Mobil (automation pman)" is listed in row 1
    #add biaya Parkir Motor
    When user click Tambah button
    And user choose nama biaya "Parkir Motor"
    And user choose periode awal "today"
    And user choose periode akhir "tomorrow"
    And user fill the durasi biaya "automation pman"
    And user input jumlah biaya "50000"
    And user click Tambah button on the Biaya Sewa pop up
    Then the "Biaya Tambahan" toast will be displayed
    And "biaya tambahan" "Parkir Motor (automation pman)" is listed in row 2
    #delete all biaya tambahan
    When user delete all biaya tambahan or sewa on invoice manual
    And the empty state "biaya tambahan" will be displayed

  @TEST_PMAN-5992 @pman-prod
  Scenario: add multiple biaya tambahan
    When user click Tambah button
    And user choose nama biaya "Parkir Mobil"
    And user choose periode awal "today"
    And user choose periode akhir "tomorrow"
    And user fill the durasi biaya "automation pman"
    And user input jumlah biaya "50000"
    And user click Tambah button on the Biaya Sewa pop up
    Then the "Biaya Tambahan" toast will be displayed
    And "biaya tambahan" "Parkir Mobil (automation pman)" is listed in row 1
    #add biaya Laundry
    When user click Tambah button
    And user choose nama biaya "Laundry"
    And user choose periode awal "today"
    And user choose periode akhir "tomorrow"
    And user fill the durasi biaya "automation pman"
    And user input jumlah biaya "50000"
    And user click Tambah button on the Biaya Sewa pop up
    Then the "Biaya Tambahan" toast will be displayed
    And "biaya tambahan" "Laundry (automation pman)" is listed in row 2
    #add biaya Deposit
    When user click Tambah button
    And user choose nama biaya "Deposit"
    And user fill the durasi biaya "automation pman"
    And user input jumlah biaya "50000"
    And user click Tambah button on the Biaya Sewa pop up
    Then the "Biaya Tambahan" toast will be displayed
    And "biaya tambahan" "Deposit (automation pman)" is listed in row 3
    #add biaya Lainnya
    When user click Tambah button
    And user choose nama biaya "Lainnya"
    And user fill Lainnya field "Sampah"
    And user fill the durasi biaya "automation pman"
    And user input jumlah biaya "50000"
    And user click Tambah button on the Biaya Sewa pop up
    Then the "Biaya Tambahan" toast will be displayed
    And "biaya tambahan" "Sampah (automation pman)" is listed in row 4

    @TEST_PMAN-6055 @pman-prod
    Scenario: Edit Biaya Tambahan
      #add biaya tambahan
      When user click Tambah button
      And user choose nama biaya "Parkir Mobil"
      And user choose periode awal "today"
      And user choose periode akhir "tomorrow"
      And user fill the durasi biaya "1 Hari"
      And user input jumlah biaya "50000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Biaya Tambahan" toast will be displayed
      And "biaya tambahan" "Parkir Mobil (1 Hari)" is listed in row 1
      And user will see nama biaya "Parkir Mobil (1 Hari)" in the biaya tambahan table
      And user will see periode awal date "today" in the biaya tambahan table
      And user will see periode akhir date "tomorrow" in the biaya tambahan table
      And user will see jumlah biaya "Rp50.000" in the biaya tambahan table
      And user will see disburse to pemilik "Ya" in the biaya tambahan table
      #edit biaya tambahan to laundry
      When user click edit button invoice manual
      And user choose nama biaya "Laundry"
      And user choose periode awal "tomorrow"
      And user choose periode akhir "day after tomorrow"
      And user fill the durasi biaya "5 Kg"
      And user input jumlah biaya "25000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Ubah Biaya" toast will be displayed
      And "biaya tambahan" "Laundry (5 Kg)" is listed in row 1
      And user will see nama biaya "Laundry (5 Kg)" in the biaya tambahan table
      And user will see periode awal date "tomorrow" in the biaya tambahan table
      And user will see periode akhir date "day after tomorrow" in the biaya tambahan table
      And user will see jumlah biaya "Rp25.000" in the biaya tambahan table
      And user will see disburse to pemilik "Tidak" in the biaya tambahan table
      #edit biaya tambahan to Lainnya
      When user click edit button invoice manual
      And user choose nama biaya "Lainnya"
      And user fill Lainnya field "Sampah"
      And user empty durasi biaya
      And user input jumlah biaya "30000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Ubah Biaya" toast will be displayed
      And "biaya tambahan" "Sampah" is listed in row 1
      And user will see nama biaya "Sampah" in the biaya tambahan table
      And user will see periode awal date "tomorrow" in the biaya tambahan table
      And user will see periode akhir date "day after tomorrow" in the biaya tambahan table
      And user will see jumlah biaya "Rp30.000" in the biaya tambahan table
      And user will see disburse to pemilik "Ya" in the biaya tambahan table