@regression @pman @mamipay @invoice-manual

  Feature: Invoice Manual
    Background: Navigate to Invoice Manual
      Given user navigates to "backoffice"
      When user login Backoffice as a "PMAN01" via credentials
      And user access mamipay menu "Invoice Manual"
      Then invoice manual page should be open

    @TEST_PMAN-5684 @pman-prod
    Scenario: Auto fill No HP and Nomor Kamar
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      When user select listing "PMAN"
      And user select nama penyewa "PMAN"
      When user select jenis invoice "Biaya Tambahan"
      Then no HP penyewa should be autofill "PMAN"
      And no HP penyewa field is disabled
      And no kamar should be auto fill "PMAN"
      And no kamar field is disabled

    @TEST_PMAN-5655 @pman-prod
    Scenario Outline: Back from Create Invoice Manual <Jenis Invoice>
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      #back if no biaya added yet
      And user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And click buat invoice back button
      Then invoice manual page should be open
      #back if there is biaya tambahan
      When user click buat invoice
      And user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "<Jenis Invoice>"
      When user click Tambah button
      And user choose nama biaya "<Jenis Biaya Sewa>"
      And user choose periode awal "<Periode Awal>"
      And user choose periode akhir "<Periode Akhir>"
      And user fill the durasi biaya "<Durasi Biaya>"
      And user input jumlah biaya "<Jumlah Biaya>"
      And user click Tambah button on the Biaya Tambahan pop up
      Then the "<Jenis Invoice>" toast will be displayed
      When click buat invoice back button
      Then exit buat invoice confirmation pop up should be appear
      And confirmation pop up title is "Yakin keluar dari halaman ini?"
      And confirmation pop up description is "Invoice yang dibuat tidak akan tersimpan dan tidak dapat dikembalikan."
      And have button "Tidak" and "Ya"
      #cancel exit confirmation pop up
      When user choose "Tidak" in exit buat invoice confirmation pop up
      Then user stay in buat invoice manual page
      #confirm exit confirmation pop up
      When click buat invoice back button
      And user choose "Ya" in exit buat invoice confirmation pop up
      Then user redirect to invoice manual page

      Examples:
      | Jenis Invoice   | Jenis Biaya Sewa        | Periode Awal  | Periode Akhir   | Durasi Biaya  | Jumlah Biaya  |
      | Biaya Tambahan  | Parkir Mobil            | today         | tomorrow        | 3 hari        | 25000         |
      | Biaya Sewa      | Perpanjang sewa harian  | today         | tomorrow        | 2 Hari        | 500000        |

    @TEST_PMAN-5744 @pman-prod
    Scenario Outline: Durasi Biaya in <Jenis Invoice> max 255 characters
      When user click buat invoice
      And user select jenis invoice "<Jenis Invoice>"
      And user click Tambah button
      And user fill the durasi biaya "more than 255 characters"
      Then durasi biaya should be only contains "max 255 characters"
      And counter show "255 / 255"

      Examples:
        | Jenis Invoice   |
        | Biaya Tambahan  |
        | Biaya Sewa      |

    @TEST_PMAN-5657 @pman-prod
    Scenario Outline: Change Jenis Invoice - When There Are Biaya Tambahan & Biaya Sewa Data
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      And user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "<Jenis Invoice>"
      #create biaya tambahan & biaya sewa
      When user click Tambah button
      And user choose nama biaya "<Jenis Biaya Sewa>"
      And user choose periode awal "<Periode Awal>"
      And user choose periode akhir "<Periode Akhir>"
      And user fill the durasi biaya "<Durasi Biaya>"
      And user input jumlah biaya "<Jumlah Biaya>"
      And user click Tambah button on the Biaya Tambahan pop up
      Then the "<Jenis Invoice>" toast will be displayed
      #change invoice type
      When user select jenis invoice "<Change Invoice>"
      Then the pop up confirmation will be displayed
      And user click "Batal" button on the pop up confirmation
      And user select jenis invoice "<Change Invoice>"
      And user click "Lanjutkan" button on the pop up confirmation
      Then user will directed to "<Change Invoice>" section
      When user select jenis invoice "<Jenis Invoice>"
      Then user will see empty state on the biaya "<Jenis Invoice>" table

      Examples:
        | Jenis Invoice   | Jenis Biaya Sewa        | Periode Awal  | Periode Akhir   | Durasi Biaya  | Jumlah Biaya  | Change Invoice  |
        | Biaya Tambahan  | Parkir Mobil            | today         | tomorrow        | 3 hari        | 25000         | Biaya Sewa      |
        | Biaya Sewa      | Perpanjang sewa harian  | today         | tomorrow        | 2 Hari        | 500000        | Biaya Tambahan  |

    @TEST_PMAN-5745 @pman-prod
    Scenario Outline: Change Jenis Invoice - When There Are No Biaya Tambahan & Biaya Sewa Data
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      And user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "<Jenis Invoice>"
      #change invoice type
      When user select jenis invoice "<Change Invoice>"
      Then the pop up confirmation will not be displayed
      And user will directed to "<Change Invoice>" section

      Examples:
        | Jenis Invoice   | Change Invoice  |
        | Biaya Tambahan  | Biaya Sewa      |
        | Biaya Sewa      | Biaya Tambahan  |

    @TEST_PMAN-5689
    Scenario Outline: Click Buat dan Kirim button
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      When user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "<Jenis Invoice>"
      #create biaya tambahan & biaya sewa
      When user click Tambah button
      And user choose nama biaya "<Jenis Biaya Sewa>"
      And user choose periode awal "<Periode Awal>"
      And user choose periode akhir "<Periode Akhir>"
      And user fill the durasi biaya "<Durasi Biaya>"
      And user input jumlah biaya "<Jumlah Biaya>"
      And user click Tambah button on the Biaya Tambahan pop up
      #click Buat dan Kirim button
      When user click Buat dan Kirim button
      #check invoice data
      Then user will see Buat dan Kirim Invoice pop up "<Jenis Invoice>"
      And user will see nama biaya "<nama biaya table>" in the biaya tambahan table
      And user will see periode awal date "<awal table>" in the biaya tambahan table
      And user will see periode akhir date "<akhir table>" in the biaya tambahan table
      And user will see jumlah biaya "<jumlah biaya table>" in the biaya tambahan table
      And user will see disburse to pemilik "<disburse to pemilik>" in the biaya tambahan table
      #check close and kembali button in buat & kirim invoice pop up
      When user click "Close" button in invoice pop up
      Then invoice pop up is closed
      When user click Buat dan Kirim button
      And user click "Kembali" button in invoice pop up
      Then invoice pop up is closed
      When user click Buat dan Kirim button
      And user click "Buat dan Kirim" button in invoice pop up
      Then the "Invoice Manual" toast will be displayed
      And user go to last page
      And the invoice manual will be created with "<Jenis Invoice>", "<jumlah biaya table>", and "<status invoice>"
      And the user will see "<Jenis Invoice>" with "<nama biaya table>" and "<jumlah biaya table>" detail when hovering Jenis Biaya

      Examples:
        | Jenis Invoice   | Jenis Biaya Sewa        | Periode Awal  | Periode Akhir   | Durasi Biaya  | Jumlah Biaya  | nama biaya table          | awal table  | akhir table | jumlah biaya table  | disburse to pemilik | status invoice  |
        | Biaya Tambahan  | Parkir Mobil            | today         | tomorrow        | 3 hari        | 25000         | Parkir Mobil (3 hari)     | today       | tomorrow    | Rp25.000            | Ya                  | Unpaid          |
        | Biaya Sewa      | Perpanjang sewa harian  | today         | tomorrow        | 2 Hari        | 500000        | Perpanjang sewa harian (2 Hari)  | today       | tomorrow    | Rp500.000           | -                   | Unpaid          |

    @TEST_PMAN-5822
    Scenario Outline: Disable Buat dan Kirim button
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      When user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "<Jenis Invoice>"
      Then the Buat dan Kirim button is disabled

      Examples:
        | Jenis Invoice   |
        | Biaya Tambahan  |
        | Biaya Sewa      |

    @TEST_PMAN-5766
    Scenario Outline: Check invoice data in invoice page
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      When user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "<Jenis Invoice>"
      #create biaya tambahan & biaya sewa
      When user click Tambah button
      And user choose nama biaya "<Jenis Biaya Sewa>"
      And user choose periode awal "<Periode Awal>"
      And user choose periode akhir "<Periode Akhir>"
      And user fill the durasi biaya "<Durasi Biaya>"
      And user input jumlah biaya "<Jumlah Biaya>"
      And user click Tambah button on the Biaya Tambahan pop up
      #click Buat dan Kirim button
      When user click Buat dan Kirim button
      And user click "Buat dan Kirim" button in invoice pop up
      Then the "Invoice Manual" toast will be displayed
      And user go to last page
      And the invoice manual will be created with "<Jenis Invoice>", "<jumlah biaya table>", and "<status invoice>"
      #click invoice number
      When user click invoice number with unpaid status
      Then user will see Jenis Pembayaran "<Jenis Invoice>" and Total Pembayaran "<jumlah biaya table>"
      And user will see Listing name, Rincian Pembayaran "<Jenis Invoice>" "<nama biaya table>" and Total Pembayaran "<jumlah biaya table>"

      Examples:
        | Jenis Invoice   | Jenis Biaya Sewa        | Periode Awal  | Periode Akhir   | Durasi Biaya  | Jumlah Biaya  | nama biaya table          | jumlah biaya table  | status invoice  |
        | Biaya Tambahan  | Parkir Mobil            | today         | tomorrow        | 3 hari        | 25000         | Parkir Mobil (3 hari)     | Rp25.000            | Unpaid          |
        | Biaya Sewa      | Perpanjang sewa harian  | today         | tomorrow        | 2 Hari        | 500000        | Perpanjang sewa harian (2 Hari)  | Rp500.000           | Unpaid          |

    @TEST_PMAN-6045
    Scenario Outline: Search invoice manual
      When user choose search by "<search by>"
      And user input search value with "<search value>"
      And user click search button or hit enter
      Then the result will be displayed according the search value "<result coloumn 1>" "<result coloumn 2>" "<result coloumn 3>"

      Examples:
        | search by     | search value                                              | result coloumn 1          | result coloumn 2          | result coloumn 3                                         |
        | Nomor Invoice | MI/49220517/2022/09/80637                                 | MI/49220517/2022/09/80637 | Indah Trivena Tampubolon  | Kost Apik Khusus Automation PMAN Tipe A Halmahera Utara  |
        | Nama Penyewa  | Indah Trivena Tampubolon                                  | MI/49220517/2022/09/80637 | Indah Trivena Tampubolon  | Kost Apik Khusus Automation PMAN Tipe A Halmahera Utara  |
        | Nama Listing  | Kost Apik Khusus Automation PMAN Tipe A Halmahera Utara   | MI/49220517/2022/09/80637 | Indah Trivena Tampubolon  | Kost Apik Khusus Automation PMAN Tipe A Halmahera Utara  |

    @TEST_PMAN-6046
    Scenario Outline: Search Nama Listing per word
      And user choose search by "<search by>"
      And user input search value with "<search value>"
      And user click search button or hit enter
      Then the result will be displayed according the search value "<result>"

      Examples:
        | search by     | search value      | result                                        |
        | Nama Listing  | Singgahsini       | Data yang dicari tidak ditemukan              |
        | Nama Listing  | omen              | Kost Singgahsini Omen Tipe B Halmahera Utara  |
        | Nama Listing  | Omen tipe c       | Kost Singgahsini Omen Tipe C Halmahera Utara  |
        | Nama Listing  | Halmahera Utara   | Data yang dicari tidak ditemukan              |
        #search no found
        | Nomor Invoice | 12345             | Data yang dicari tidak ditemukan              |
        | Nama Penyewa  | asdf yohoo        | Data yang dicari tidak ditemukan              |

    @TEST_PMAN-6212
    Scenario: Filter Invoice Manual
      When user click Filter in invoice manual
      Then user will see filter pop up displayed
      And user will see the title and subtitle filter
      And user will see Status Invoice, Jenis Biaya, Tanggal Invoice title and place holder
      And user click "Close" button on Filter
      Then user will not see filter pop up displayed
      And user click Filter in invoice manual
      #Default Filter Unpaid
      And user click "Terapkan" button on Filter
      Then user will see invoice status "Unpaid" result
      And user click Filter in invoice manual
      And user click "Reset" button on Filter
      Then the filter counter will disappears

    @TEST_PMAN-6213
    Scenario Outline: Check Status Invoice in filter
      When user click "Main Reset" button on Filter
      And user click Filter in invoice manual
      Then user will see filter pop up displayed
      When user click "Status Invoice" dropdown
      And user tick the "Invoice Status" dropdown "<pick status>"
      And user click "Terapkan" button on Filter
      Then the "Status Invoice" "<result>" will be displayed according to the filter

      Examples:
        | pick status  | result  |
        | Paid         | Paid    |
        | Unpaid       | Unpaid  |
        | Expired      | Expired |

    @TEST_PMAN-6214
    Scenario Outline: Check Jenis Biaya in filter
      When user click "Main Reset" button on Filter
      And user click Filter in invoice manual
      Then user will see filter pop up displayed
      When user click "Jenis Biaya" dropdown
      And user tick the "Jenis Biaya" dropdown "<pick status>"
      And user click "Terapkan" button on Filter
      Then the "Jenis Biaya" "<result>" will be displayed according to the filter

      Examples:
        | pick status     | result          |
        | Biaya Tambahan  | Biaya Tambahan  |
        | Biaya Sewa      | Biaya Sewa      |

    @TEST_PMAN-6215
    Scenario Outline: Filter Tanggal Mulai and Tanggal Akhir
      When user click "Main Reset" button on Filter
      And user click Filter in invoice manual
      Then user will see filter pop up displayed
      When user select the date for "<tgl mulai>"
      And user select the date for "<tgl akhir>"
      And user click "Terapkan" button on Filter
      Then the "Tanggal Invoice Dibuat" "<result>" will be displayed according to the filter

      Examples:
        | tgl mulai | tgl akhir | result  |
        | today     | tomorrow  | today   |

    @TEST_PMAN-5900
    Scenario Outline: Search and Filter Invoice Manual
      When user choose search by "<search by>"
      And user input search value with "<search value>"
      And user click Filter in invoice manual
      Then user will see filter pop up displayed
      When user click "Jenis Biaya" dropdown
      And user tick the "Jenis Biaya" dropdown "<pick status>"
      And user select the date for "<tgl mulai>"
      And user select the date for "<tgl akhir>"
      And user click "Terapkan" button on Filter
      Then the "<result>", "<result2>", "<result3>", "<result4>" will be displayed according to the search and filter

      Examples:
        | search by     | search value                                              | pick status     | tgl mulai | tgl akhir | result                                                   | result2  | result3         | result4 |
        | Nama Listing  | Kost Apik Khusus Automation PMAN Tipe A Halmahera Utara   | Biaya Tambahan  | today     | tomorrow  | Kost Apik Khusus Automation PMAN Tipe A Halmahera Utara  | Unpaid   | Biaya Tambahan  | today   |

    @TEST_PMAN-6144
    Scenario: Ubah Status Invoice from Unpaid to Paid
      #change status invoice
      When user go to last page
      And user click action button invoice manual
      And choose action "Ubah Status"
      And user set tanggal pembayaran "today"
      And user set waktu pembayaran "1000"
      And user click simpan ubah status invoice manual
      Then the "Ubah Status Invoice" toast will be displayed
      And status invoice manual "Paid"
      And paid date at "today", "10:00"

    @TEST_PMAN-6253
    Scenario: Ubah Status Invoice from Expired to Paid
      #filter expired invoice
      When user click Filter in invoice manual
      Then user will see filter pop up displayed
      When user click "Status Invoice" dropdown
      And user tick the "Invoice Status" dropdown "Unpaid"
      And user tick the "Invoice Status" dropdown "Expired"
      And user click "Terapkan" button on Filter
      #close pop up
      When user go to last page
      And user click action button invoice manual
      And choose action "Ubah Status"
      And user set tanggal pembayaran "today"
      And user set waktu pembayaran "1000"
      When user close ubah status invoice
      Then status invoice manual "Expired"
      #click kembali
      When user click action button invoice manual
      And choose action "Ubah Status"
      And user set tanggal pembayaran "today"
      And user set waktu pembayaran "1000"
      And user click kembali from ubah status invoice
      Then status invoice manual "Expired"
      #confirm change status from Expired to Paid
      When user click action button invoice manual
      And choose action "Ubah Status"
      And user set tanggal pembayaran "today"
      And user set waktu pembayaran "1000"
      And user click simpan ubah status invoice manual
      Then the "Ubah Status Invoice" toast will be displayed
      And status invoice manual "Paid"
      And paid date at "today", "10:00"