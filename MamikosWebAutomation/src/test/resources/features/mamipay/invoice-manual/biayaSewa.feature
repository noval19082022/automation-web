@regression @pman @mamipay @invoice-manual @biaya-sewa

  Feature: Invoice Manual - Biaya Sewa
    Background: Navigate to Invoice Manual
      Given user navigates to "backoffice"
      When user login Backoffice as a "PMAN03" via credentials
      And user access mamipay menu "Invoice Manual"
      Then invoice manual page should be open

    @TEST_PMAN-5626 @pman-prod
    Scenario Outline: Add Biaya Sewa in Invoice Manual
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      When user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "Biaya Sewa"
      And user click Tambah button
      And user choose nama biaya "<Jenis Biaya Sewa>"
      And user fill Lainnya field "<Lainnya>"
      And user choose periode awal "<Periode Awal>"
      And user choose periode akhir "<Periode Akhir>"
      And user fill the durasi biaya "<Durasi Biaya>"
      And user input jumlah biaya "<Jumlah Biaya>"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Biaya Sewa" toast will be displayed
      And user will see nama biaya "<nama biaya table>" in the biaya sewa table
      And user will see periode awal date "<Periode Awal>" in the biaya sewa table
      And user will see periode akhir date "<Periode Akhir>" in the biaya sewa table
      And user will see jumlah biaya "<Biaya Show>" in the biaya sewa table

      Examples:
        | Jenis Biaya Sewa                  | Lainnya | Periode Awal  | Periode Akhir | Durasi Biaya    | Jumlah Biaya  | nama biaya table                            | Biaya Show  |
        | Perpanjang sewa harian            | -       | today         | tomorrow      | 2 Hari          | 500000        | Perpanjang sewa harian (2 Hari)                    | Rp500.000   |
        | Kekurangan biaya sewa kamar       | -       | -             | -             | 3 Hari          | 500000        | Kekurangan biaya sewa kamar (3 Hari)          | Rp500.000   |
        | Pindah tipe kamar/kos (relokasi)  | -       | today         | tomorrow      | 2A ke 1A        | 20000         | Pindah tipe kamar/kos (relokasi) (2A ke 1A) | Rp20.000    |
        | Lainnya                           | Sampah  | today         | tomorrow      | Tambahan Kasur  | 100000        | Sampah (Tambahan Kasur)                     | Rp100.000   |

    @TEST_PMAN-5743 @pman-prod
    Scenario: Periode is disabled when choose Jenis Biaya Kekurangan biaya sewa kamar
      When user click buat invoice
      And user select jenis invoice "Biaya Sewa"
      And user click Tambah button
      And user choose nama biaya "Kekurangan biaya sewa kamar"
      Then periode awal should be disabled
      And periode akhir should be disabled

    @TEST_PMAN-5767 @pman-prod
    Scenario Outline: <button> modal tambah biaya sewa
      When user click buat invoice
      And user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "Biaya Sewa"
      And user click Tambah button
      And user choose nama biaya "Perpanjang sewa harian"
      And user choose periode awal "today"
      And user choose periode akhir "tomorrow"
      And user fill the durasi biaya "Extend"
      And user input jumlah biaya "100000"
      And user "<button>" modal tambah biaya
      Then tambah biaya modal closed
      When user click Tambah button
      Then all field is empty

      Examples:
        | button      |
        | close       |
        | kembali     |

    @TEST_PMAN-5784 @pman-prod
    Scenario Outline: Check required fields in the Biaya Sewa
      When user click buat invoice
      And user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "Biaya Sewa"
      And user click Tambah button
      And user choose nama biaya "<nama biaya>"
      And user fill Lainnya field "<Lainnya>"
      And user choose periode awal "<awal>"
      And user choose periode akhir "<akhir>"
      And user fill the durasi biaya "<durasi biaya>"
      And user input jumlah biaya "<jumlah biaya>"
      And user click Tambah button on the Biaya Sewa pop up
      #check biaya sewa error message in modal
      Then user will see nama biaya error message "<nama biaya error msg>"
      And user will see periode awal error message "<awal error msg>"
      And user will see periode akhir error message "<akhir error msg>"
      And user will see jumlah biaya error message "<jumlah biaya error msg>"

      Examples:
        | nama biaya                  | Lainnya         | awal  | akhir     | durasi biaya  | jumlah biaya  | nama biaya error msg            | awal error msg                    | akhir error msg                   | jumlah biaya error msg            |
        | -                           | -               | -     | -         | -             | -             | Nama biaya tidak boleh kosong.  | -                                 | -                                 | Jumlah biaya tidak boleh kosong.  |
        | Perpanjang sewa harian      | -               | -     | -         | -             | -             | -                               | Periode awal tidak boleh kosong.  | Periode akhir tidak boleh kosong. | Jumlah biaya tidak boleh kosong.  |
        | Perpanjang sewa harian      | -               | today | -         | -             | -             | -                               | -                                 | Periode akhir tidak boleh kosong. | Jumlah biaya tidak boleh kosong.  |
        | Perpanjang sewa harian      | -               | today | tomorrow  | -             | -             | -                               | -                                 | -                                 | Jumlah biaya tidak boleh kosong.  |
        | Kekurangan biaya sewa kamar | -               | -     | -         | -             | -             | -                               | -                                 | -                                 | Jumlah biaya tidak boleh kosong.  |
        | Lainnya                     | -               | -     | -         | -             | -             | Nama biaya tidak boleh kosong.  | -                                 | -                                 | Jumlah biaya tidak boleh kosong.  |
        | Lainnya                     | Check Required  | -     | -         | -             | -             | -                               | -                                 | -                                 | Jumlah biaya tidak boleh kosong.  |

    @TEST_PMAN-5775 @pman-prod
    Scenario: Add multiple biaya sewa
      When user click buat invoice
      And user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "Biaya Sewa"
      #add biaya Perpanjang sewa harian
      When user click Tambah button
      And user choose nama biaya "Perpanjang sewa harian"
      And user choose periode awal "today"
      And user choose periode akhir "tomorrow"
      And user fill the durasi biaya "automation pman"
      And user input jumlah biaya "50000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Biaya Sewa" toast will be displayed
      And "biaya sewa" "Perpanjang sewa harian (automation pman)" is listed in row 1
      #add biaya Kekurangan biaya sewa kamar
      When user click Tambah button
      And user choose nama biaya "Kekurangan biaya sewa kamar"
      And user fill the durasi biaya "automation pman"
      And user input jumlah biaya "50000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Biaya Sewa" toast will be displayed
      And "biaya sewa" "Kekurangan biaya sewa kamar (automation pman)" is listed in row 2
      #add biaya Tambahan Lainnya
      When user click Tambah button
      And user choose nama biaya "Lainnya"
      And user fill Lainnya field "Ganti Lampu"
      And user fill the durasi biaya "automation pman"
      And user input jumlah biaya "100000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Biaya Sewa" toast will be displayed
      And "biaya sewa" "Ganti Lampu (automation pman)" is listed in row 3

    @TEST_PMAN-6039 @pman-prod
    Scenario: Edit Biaya Sewa
      When user click buat invoice
      And user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "Biaya Sewa"
      #add biaya Perpanjang sewa harian
      When user click Tambah button
      And user choose nama biaya "Perpanjang sewa harian"
      And user choose periode awal "today"
      And user choose periode akhir "tomorrow"
      And user fill the durasi biaya "automation pman"
      And user input jumlah biaya "50000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Biaya Sewa" toast will be displayed
      And "biaya sewa" "Perpanjang sewa harian (automation pman)" is listed in row 1
      And user will see nama biaya "Perpanjang sewa harian (automation pman)" in the biaya sewa table
      And user will see periode awal date "today" in the biaya sewa table
      And user will see periode akhir date "tomorrow" in the biaya sewa table
      And user will see jumlah biaya "Rp50.000" in the biaya sewa table
      #edit biaya sewa
      When user click edit button invoice manual
      And user choose nama biaya "Kekurangan biaya sewa kamar"
      And user fill the durasi biaya "automation pman"
      And user input jumlah biaya "20000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Ubah Biaya" toast will be displayed
      And "biaya sewa" "Kekurangan biaya sewa kamar (automation pman)" is listed in row 1
      And user will see nama biaya "Kekurangan biaya sewa kamar (automation pman)" in the biaya sewa table
      And user will see periode awal date "-" in the biaya sewa table
      And user will see periode akhir date "-" in the biaya sewa table
      And user will see jumlah biaya "Rp20.000" in the biaya sewa table

    @TEST_PMAN-5965 @pman-prod
    Scenario Outline: delete biaya sewa
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      When user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "Biaya Sewa"
      And user click Tambah button
      And user choose nama biaya "<Jenis Biaya Sewa>"
      And user fill Lainnya field "<Lainnya>"
      And user choose periode awal "<Periode Awal>"
      And user choose periode akhir "<Periode Akhir>"
      And user fill the durasi biaya "<Durasi Biaya>"
      And user input jumlah biaya "<Jumlah Biaya>"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Biaya Sewa" toast will be displayed
      #delete biaya sewa
      When user click delete button on invoice manual
      And user will see "biaya sewa" delete confirmation pop up
      And user click "Batal" on delete confirmation pop up
      Then delete confirmation pop up is closed
      When user click delete button on invoice manual
      And user click "Hapus" on delete confirmation pop up
      Then the deletion toast will be displayed
      And delete confirmation pop up is closed
      And the empty state "biaya sewa" will be displayed

      Examples:
        | Jenis Biaya Sewa          | Lainnya | Periode Awal  | Periode Akhir | Durasi Biaya    | Jumlah Biaya  |
        | Perpanjang sewa harian    | -       | today         | tomorrow      | 2 Hari          | 500000        |

    @TEST_PMAN-5966 @pman-prod
    Scenario: delete multiple biaya sewa
      When user click buat invoice
      And user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "Biaya Sewa"
      #add biaya Perpanjang sewa harian
      When user click Tambah button
      And user choose nama biaya "Perpanjang sewa harian"
      And user choose periode awal "today"
      And user choose periode akhir "tomorrow"
      And user fill the durasi biaya "automation pman"
      And user input jumlah biaya "50000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Biaya Sewa" toast will be displayed
      And "biaya sewa" "Perpanjang sewa harian (automation pman)" is listed in row 1
      #add biaya Kekurangan biaya sewa kamar
      When user click Tambah button
      And user choose nama biaya "Kekurangan biaya sewa kamar"
      And user fill the durasi biaya "automation pman"
      And user input jumlah biaya "50000"
      And user click Tambah button on the Biaya Sewa pop up
      Then the "Biaya Sewa" toast will be displayed
      And "biaya sewa" "Kekurangan biaya sewa kamar (automation pman)" is listed in row 2
      #delete all biaya sewa
      When user delete all biaya tambahan or sewa on invoice manual
      And the empty state "biaya sewa" will be displayed