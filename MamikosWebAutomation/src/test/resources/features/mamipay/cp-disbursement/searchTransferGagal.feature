@regression @pman @mamipay @cp-disbursement @transfer-gagal

  Feature: CP Disbursement - Search Failed Disbursement Data
    Background: Navigate to CP Disbursement Menu
      Given user navigates to "backoffice"
      When user login as a consultant via credentials
      And user access menu CP Disbursement

    @TEST_PMAN-3314
    Scenario: Search using invalid property name
      When user open "Transfer Gagal" tab
      And user choose search cp disbursement by "Nama Property"
      And user write search cp disbursement keyword "abc123"
      Then should be show "Transfer Gagal" empty page

    @TEST_PMAN-3313
    Scenario: Search using valid property name
      When user open "Transfer Gagal" tab
      And user choose search cp disbursement by "Nama Property"
      And user write search cp disbursement keyword "Khusus Automation"
      Then should be show all disbursement with nama property "PMAN"
