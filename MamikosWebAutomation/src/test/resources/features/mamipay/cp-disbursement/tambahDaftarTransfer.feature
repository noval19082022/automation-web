@regression @pman @mamipay @cp-disbursement @tambah-daftar-transfer

  Feature: CP Disbursement - Add Disbursement Data
    Background: Tambah Data Transfer
      Given user navigates to "backoffice"
      When user login as a consultant via credentials
      And user access menu CP Disbursement
      And user tambah data transfer

    @TEST_PMAN-3328
    Scenario: Change invalid to valid property
      When user set nama property CP "abcd"
      Then should display nama property error message "Mohon periksa kembali nama/level kos"
      When user set nama property CP "khu"
      Then should showing property suggestion "Kost Apik Khusus Automation PMAN Halmahera Utara"
      When user choose suggestion
      Then should auto fill Kost "Kost Apik Khusus Automation PMAN Halmahera Utara" data

    @TEST_PMAN-3340
    Scenario: Ensure data not deleted when popup closed
      When user set nama property CP "khu"
      And user choose suggestion
      And user fill total pendapatan "PMAN"
      And user select tipe transaksi "PMAN"
      And user select tanggal transfer ke pemilik "today"
      And user close tambah data transfer pop up
      And user tambah data transfer
      Then data should not delete

    @TEST_PMAN-3333
    Scenario: input > 50 characters in tipe transaksi lainnya
      When user select tipe transaksi "50+ char"
      Then user can only input tipe transaksi lainnya with 50 characters

    @TEST_PMAN-3340
    Scenario: Submit with valid data
      When user set nama property CP "khu"
      And user choose suggestion
      And user fill total pendapatan "PMAN"
      And user select tipe transaksi "PMAN"
      And user select tanggal transfer ke pemilik "today"
      And user click Tambahkan button
      Then the data successfully added on Transfer Pendapatan table