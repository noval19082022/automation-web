@regression @pman @mamipay @cp-disbursement @daftar-transfer

  Feature: CP Disbursement - Search Disbursement Data
    Background: Navigate to CP Disbursement Menu
      Given user navigates to "backoffice"
      When user login as a consultant via credentials
      And user access menu CP Disbursement

    @TEST_PMAN-3294
    Scenario: Search using invalid property name
      When user choose search cp disbursement by "Nama Property"
      And user write search cp disbursement keyword "abc123"
      Then should be show "Daftar Transfer" empty page

    @TEST_PMAN-3293
    Scenario: Search using valid property name
      When user choose search cp disbursement by "Nama Property"
      And user write search cp disbursement keyword "Khusus Automation"
      Then should be show all disbursement with nama property "PMAN"

    @TEST_PMAN-3298
    Scenario: Search using invalid account name
      When user choose search cp disbursement by "Nama Pemilik Rekening"
      And user write search cp disbursement keyword "abc123"
      Then should be show "Daftar Transfer" empty page

    @TEST_PMAN-3297
    Scenario: Search using valid account name
      When user choose search cp disbursement by "Nama Pemilik Rekening"
      And user write search cp disbursement keyword "Yudha Ferroza"
      Then should be show all disbursement with nama pemilik rekening "PMAN"

    @TEST_PMAN-3296
    Scenario: Search using invalid account number
      When user choose search cp disbursement by "Nomor Rekening"
      And user write search cp disbursement keyword "1234567"
      Then should be show "Daftar Transfer" empty page

    @TEST_PMAN-3295
    Scenario: Search using valid account number
      When user choose search cp disbursement by "Nomor Rekening"
      And user write search cp disbursement keyword "10000245429"
      Then should be show all disbursement with nomor rekening "PMAN"