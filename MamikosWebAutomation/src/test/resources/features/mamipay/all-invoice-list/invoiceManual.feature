@regression @pman @mamipay @all-invoice-list

  Feature: All Invoice List - Invoice Manual

    @TEST_PMAN-5875 @pman-prod
    Scenario: Filter Invoice Manual
      Given user navigates to "backoffice"
      When user login as a consultant via credentials
      And user access mamipay menu "All Invoice List"
      Then all invoice list page should be open
      When user choose filter by order type "Invoice Manual"
      And click cari invoice
      Then only display list of Invoice Manual

    @TEST_PMAN-5876
    Scenario: Change status invoice manual
      #create invoice manual
      Given user navigates to "backoffice"
      When user login as a consultant via credentials
      And user access mamipay menu "Invoice Manual"
      Then invoice manual page should be open
      When user click buat invoice
      Then user redirect to Buat Invoice manual page
      When user select listing "PMAN"
      And user select nama penyewa "PMAN"
      And user select jenis invoice "Biaya Sewa"
      And user click Tambah button
      And user choose nama biaya "Perpanjang sewa harian"
      And user choose periode awal "today"
      And user choose periode akhir "tomorrow"
      And user fill the durasi biaya "automation pman"
      And user input jumlah biaya "50000"
      And user click Tambah button on the Biaya Sewa pop up
      And user click Buat dan Kirim button
      And user click "Buat dan Kirim" button in invoice pop up
      Then the "Invoice Manual" toast will be displayed
      And user go to last page
      And user save invoice manual number
      And user save total amount invoice number
      #change status invoice manual unpaid to paid
      When user access mamipay menu "All Invoice List"
      And user search invoice manual number
      And click cari invoice
      Then user verify invoice number is correct
      And invoice manual status is "unpaid"
      When user click change status
      And set invoice manual to "Paid"
      And fill invoice manual paid date
      And submit change status invoice manual
      Then invoice manual status is "paid"
      #change status invoice manual paid to unpaid
      When user click change status
      And set invoice manual to "Unpaid"
      And fill invoice manual paid date
      And submit change status invoice manual
      Then invoice manual status is "unpaid"

    @TEST_PMAN-5877
    Scenario: View log Invoice manual
      Given user navigates to "backoffice"
      And user login as a consultant via credentials
      When user access mamipay menu "All Invoice List"
      And user search invoice manual number
      And click cari invoice
      Then user verify invoice number is correct
      #paid using mandiri
      When user click shortlink
      And user select payment method "Mandiri" for "invoice manual"
      Then system display payment using "Mandiri" is "Success Transaction"
      And user close payment tab
      When user search invoice manual number
      And click cari invoice
      Then invoice manual status is "paid"
      #view log
      When user click view log
      Then contains data invoice with correct invoice number that already paid
      And contains data revision history
        | row | Changed by            | Changer role    | What changed    | Old Value   | New Value	|
        | 1   | Admin Automation CRM  | administrator   | status          | unpaid      | paid      |
        | 3   | Admin Automation CRM  | administrator   | status          | paid        | unpaid    |
        | 5   | system                | system          | status          | unpaid      | paid      |
      And contains data invoice paymnet history from "Mandiri"

    @TEST_PMAN-5813
    Scenario: Invoice manual in All Invoice List Menu
      Given user navigates to "backoffice"
      When user login as a consultant via credentials
      And user access mamipay menu "All Invoice List"
      Then all invoice list page should be open
      And display Invoice Manual in the list