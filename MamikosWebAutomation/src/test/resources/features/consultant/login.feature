@regression @consultant @crm-staging @crm-prod @login-ct

Feature: Login Consultant

  Scenario: Positive Case Login as Consultant
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    Then system display label "Konsultan Mamikos"

  Scenario Outline: Negative Case Login with <Email> Password <Password> and System Display <Error Message>
    Given user navigates to "consultant /"
    When user input email "<Password>" password "<Email>" and click login button
    Then system display error message "<Error Message>" and display try again button

    Examples:
      | Email            | Password | Error Message                                       |
      |                  | password | Data yang kamu masukkan salah Harap periksa kembali |
      | xxx@mamikos.com  | password | Data yang kamu masukkan salah Harap periksa kembali |
      | yudha.mamikos.com | password | Data yang kamu masukkan salah Harap periksa kembali |
      | yudha@mamikos.com |          | Data yang kamu masukkan salah Harap periksa kembali |
      | yudha@mamikos.com | password | Data yang kamu masukkan salah Harap periksa kembali |
      | yudha@mamikos.com | password | Data yang kamu masukkan salah Harap periksa kembali |
      | yudha@mamikos.com  | password | Data yang kamu masukkan salah Harap periksa kembali |
      | yudha@mamikos.com | xxxxxxxx | Data yang kamu masukkan salah Harap periksa kembali |

