@regression @consultant @crm-staging @crm-prod

Feature: Verify Menu and Sub Menu Consultant Management Tools

  Scenario: Verify menu <Menu> is displayed
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user click hamburger menu button
    Then system display menu
      | Dashboard       |
      | Kelola Properti |
      | Data Leads      |
      | Kelola Booking  |
      | Kelola Tenant   |
      | Kelola Kontrak  |
      | Kelola Tugas    |
      | Sales Motion    |
      | Logout          |

  Scenario: Verify menu Kelola Tenant have sub menu <Sub menu>
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Data Leads"
    Then system display sub menu "Data Leads"
      | Pemilik Potensial   |
      | Properti Potensial  |
      | Penyewa Potensial   |
