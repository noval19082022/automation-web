@regression @consultant @crm-staging

Feature: Update Data Potential Tenant

  Background: Revert Data Default
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Penyewa Potensial"
    And user search and choose property potential
    And user revert data potential tenant

  Scenario: Update Dara Potential Tenant
    And user update data potential tenant
    Then verify data potential tenant
