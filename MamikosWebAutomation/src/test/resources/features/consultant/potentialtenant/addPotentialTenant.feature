@regression @consultant @crm-staging

Feature: Add Data Potential Tenant

  Scenario: Add New Data Potential Tenant
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Penyewa Potensial"
    And user click add new potential tenant button
    And user input data potential tenant
    And user select new potential tenant
    Then verify data new potential tenant
