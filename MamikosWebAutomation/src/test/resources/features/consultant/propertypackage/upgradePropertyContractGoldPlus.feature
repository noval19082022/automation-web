@regression @consultant @crm-staging @crm-prod @update-property-package

Feature: Upgrade Property Contract Gold Plus

  Background: Terminate Contract if there is contract active owner
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user search contract by owner phone number
    And user click terminate property contract

  Scenario: Upgrade Property Contract Gold Plus 1 to Gold Plus 2
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user click add new package button
    And user input registered owner phone number to validate
    And user add data property contract with property level Goldplus one
    And user assign kost and room level to property level
    And user access to Property Package menu
    And user search contract by owner phone number
    And user upgrade property contract
    And user search contract by owner phone number
    And user click see detail property contract
    Then user verify that kost Level is "GoldPlus 2"

  Scenario: Un-assign Kost level from Property Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user click add new package button
    And user input registered owner phone number to validate
    And user add data property contract with property level Goldplus one
    And user assign kost and room level to property level
    Then user verify that kost Level is "GoldPlus 1"
    When user unassign goldplus level
    Then user verify that kost Level is "Reguler"