@regression @consultant @crm-staging

Feature: Unregister Owner Package

  Scenario: Negative Case - Input Unregister Owner Phone Number
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user click add new package button
    And user input "082555123" on owner phone number to validate
    Then user verify there is warning message "Owner tidak dapat ditemukan"