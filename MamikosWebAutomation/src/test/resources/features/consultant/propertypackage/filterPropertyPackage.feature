@regression @consultant @crm-staging @crm-prod

Feature: Filter Property Package

  Scenario Outline: Filter property package by status <Package Status>
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user filter property package by status "<Package Status>"
    Then system displays list of property package with contract status "<Package Status>"
    Examples:
      | Package Status |
      | active         |
      | inactive       |
      | terminated     |

  Scenario Outline: Filter property package by kost level <Kost Level>
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user filter property package by kost level "<Kost Level>"
    Then system displays list of property package with kost level "<Kost Level>"
    Examples:
      | Kost Level                   |
      | Mamikos Goldplus 1 PROMO     |
      | Mamikos Goldplus 2 PROMO     |
      | Mamikos Goldplus 3 PROMO     |