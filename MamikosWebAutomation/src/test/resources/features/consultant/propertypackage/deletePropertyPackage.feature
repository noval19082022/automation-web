@regression @consultant @crm-staging @crm-prod

Feature: Delete Property Package

  Background: Terminate Contract if there is contract active owner
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user search contract by owner phone number
    And user click terminate property contract

  Scenario: Delete property package
    When user access to Property Package menu
    And user click add new package button
    And user input registered owner phone number to validate
    And user add data property contract with property level Goldplus two
    And user assign kost and room level to property level
    And user access to Property Package menu
    And user search contract by owner phone number
    And user click delete contract
    Then user verify success delete contract appear