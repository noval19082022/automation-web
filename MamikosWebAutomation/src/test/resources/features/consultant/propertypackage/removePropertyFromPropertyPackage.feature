@regression @consultant @crm-staging

Feature: Remove Property from Existing Property Package

  Background: Terminate Contract if there is contract active owner
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user search contract by owner phone number
    And user click terminate property contract

  Scenario: Remove Property from existing property package GP 2
    When user click add new package button
    And user input registered owner phone number to validate
    And user add data property contract with property level Goldplus two
    And user assign kost and room level to property level
    And user access to Property Package menu
    And user search contract by owner phone number
    And user add new property
    And user select property that have been added
    And user assign kost and room level to property level
    And user access to Property Package menu
    And user search contract by owner phone number
    And user remove one property
    Then user verify property does not appear on the list