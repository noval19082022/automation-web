@regression @consultant @crm-staging @crm-prod @LG4

Feature: Search Property Package Gold Plus

  Scenario: Search Property Contract by owner name
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user search contract by owner name "valid"
    Then system displays list of property package with correct owner name

    @searchInvalidPropertyNameByOwner
  Scenario: [Package-Admin][Property Package List] Search Invalid Property Name (By Owner)
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user search contract by owner name "invalid"
    Then no list property package appear

  Scenario: Search Property Contract by owner phone
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user search contract by owner phone
    Then system displays list of property package with correct owner phone

  Scenario: Search Property Contract by property name
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user search contract by property name
    Then system displays list of property package with correct property name