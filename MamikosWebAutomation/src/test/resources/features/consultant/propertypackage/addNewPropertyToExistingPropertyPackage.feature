@regression @consultant @crm-staging @add-property-package

Feature: Add New Property to Existing Property Package

  Background: Terminate Contract if there is contract active owner
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user search contract by owner phone number
    And user click terminate property contract

  Scenario: Add new property to existing property package GP 3
    When user click add new package button
    And user input registered owner phone number to validate
    And user add data property contract with property level Goldplus three
    And user assign kost and room level to property level
    And user access to Property Package menu
    And user search contract by owner phone number
    And user add new property
    And user select property that have been added
    And user assign kost and room level to property level
    And user select property that have been added
    Then user verify that kost Level is "New GoldPlus 3"