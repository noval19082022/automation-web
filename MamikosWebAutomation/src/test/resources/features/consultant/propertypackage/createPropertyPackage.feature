@regression @consultant @crm-staging

Feature: Create Property Contract Gold Plus

  Background: Terminate Contract if there is contract active owner
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Property Package menu
    And user search contract by owner phone number
    And user click terminate property contract

  Scenario: Create Property Contract Property Level Mamikos Gold Plus 1
    When user access to Property Package menu
    And user click add new package button
    And user input registered owner phone number to validate
    And user add data property contract with property level Goldplus one
    And user assign kost and room level to property level
    Then user verify that kost Level is "GoldPlus 1"

  Scenario: Create Property Contract Property Level Mamikos Gold Plus 2
    When user access to Property Package menu
    And user click add new package button
    And user input registered owner phone number to validate
    And user add data property contract with property level Goldplus two
    And user assign kost and room level to property level
    Then user verify that kost Level is "GoldPlus 2"

  Scenario: Create Property Contract Property Level Mamikos Gold Plus 3
    When user access to Property Package menu
    And user click add new package button
    And user input registered owner phone number to validate
    And user add data property contract with property level Goldplus three
    And user assign kost and room level to property level
    Then user verify that kost Level is "GoldPlus 3"