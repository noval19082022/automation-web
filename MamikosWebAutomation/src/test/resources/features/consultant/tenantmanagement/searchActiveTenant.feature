@regression @consultant @crm-staging @crm-prod

  Feature: Search Active Tenant

    Scenario: Search Active Tenant by Name
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Tenant"
      And user search tenant by name
      Then system display list tenant active according to tenant name

    Scenario: Search Active Tenant by Email
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Tenant"
      And user search tenant by email
      Then system display list tenant active according to tenant email

    Scenario: Search Active Tenant by Name or Email
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Tenant"
      And user search tenant by "ull"
      Then system display tenant active with name or email "ull"

    Scenario: Negative Case - Search Active Tenant by Invalid Name
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Tenant"
      And user search tenant by "XXX---xxx"
      Then system display tenant not found

    Scenario: Negative Case - Search Active Tenant by Invalid Email
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Tenant"
      And user search tenant by "tidakditemukan@gmail.com"
      Then system display tenant not found

    Scenario: Negative Case - Search Active Tenant with Type > 100 Character on Search Field
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Tenant"
      And user search tenant by "Seperti dikutip dari World of Buzz, kejadian yang sangat memilukan terjadi di sebuah rumah sakit Malaysia."
      Then system display tenant not found