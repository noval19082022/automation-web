@regression @consultant @crm-staging @add-hostile-owner

Feature: Add Hostile Owner

  Background: Reset score hostile owner
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to hostile owner menu
    And user search by owner phone on hostile owner page
    And user reset score hostile owner

  Scenario: Add hostile owner with hostile reason fraud
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to hostile owner menu
    And user click Add button
    And user input hostile owner data
    And user select hostile reason "Indikasi fraud"
    And user click set button
    And user search by owner phone on hostile owner page
    Then user verify owner id on listing hostile owner correct and hostile reason is "Indikasi fraud"
    When user access to kost owner menu
    And user search by owner phone on kost owner page
    Then user verify that owner have hostile label

  Scenario: Add hostile owner with hostile reason Competitor
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to hostile owner menu
    And user click Add button
    And user input hostile owner data
    And user select hostile reason "Indikasi kompetitor"
    And user click set button
    And user search by owner phone on hostile owner page
    Then user verify owner id on listing hostile owner correct and hostile reason is "Indikasi kompetitor"
    When user access to kost owner menu
    And user search by owner phone on kost owner page
    Then user verify that owner have hostile label

  Scenario: Add hostile owner with hostile reason Payment Issue
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to hostile owner menu
    And user click Add button
    And user input hostile owner data
    And user select hostile reason "Tidak bayar minimum charging GP"
    And user click set button
    And user search by owner phone on hostile owner page
    Then user verify owner id on listing hostile owner correct and hostile reason is "Tidak bayar minimum charging GP"
    When user access to kost owner menu
    And user search by owner phone on kost owner page
    Then user verify that owner have hostile label

  Scenario: Add hostile owner with hostile reason Terbukti Fraud
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to hostile owner menu
    And user click Add button
    And user input hostile owner data
    And user select hostile reason "Terbukti Fraud"
    And user click set button
    And user search by owner phone on hostile owner page
    Then user verify owner id on listing hostile owner correct and hostile reason is "Terbukti Fraud"
    When user access to kost owner menu
    And user search by owner phone on kost owner page
    Then user verify that owner have hostile label
