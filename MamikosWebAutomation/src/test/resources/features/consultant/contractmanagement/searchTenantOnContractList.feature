@regression @consultant @crm-staging @crm-prod @search-contract

Feature: Search Tenant on Contract List

  Scenario: User Search Tenant Contract by Property Name
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Kontrak"
    And user search tenant contract by property name
    Then system display contract list according to property name

  Scenario: User Search Tenant Contract by Tenant Name
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Kontrak"
    And user search tenant contract by tenant name
    Then system display contract list according to tenant name

  Scenario: User Search Tenant Contract by Tenant Phone Number
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Kontrak"
    And user search tenant contract by tenant phone number
    Then system display contract list according to tenant phone number

  Scenario: User Search Tenant Contract with invalid data
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Kontrak"
    And user search tenant contract "lorem ipsum"
    Then system display contract not found page