@regression @consultant @crm-staging @crm-prod @add-additional-cost

Feature: Update Additional Fixed Cost

  Background: Deleting existing booking data in admin if exist
    Given user navigates to "backoffice"
    And user login as a consultant via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number
    And user terminate active contract

#  Scenario: Add Contract from Potential Tenant not have Contract Active Yet
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Penyewa Potensial"
    And user search potential tenant by phone number "potentialTenant"
    And user create contract
    Then system display contract successfully created

  Scenario: Add Fixed Cost on Contract Status is Not Paid
    Given user navigates to "consultant /"
    When user access to menu "Kelola Kontrak"
    And user filter contract with payment status is "Belum Dibayar"
    And user add fix cost
    Then system display new total amount

#  Scenario: Update Fixed Cost on Contract Status is Not Paid
    And user delete additional fixed cost
    Then system display total amount after delete additional fixed cost