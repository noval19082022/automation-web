@regression @consultant

Feature: Filter Contract Four Parameter

  Scenario Outline: User filter contract by <Filter1> and <Filter2> and <Filter3> and <Filter4>
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user filter contract by "<Filter1>" and "<Filter2>" and "<Filter3>" and "<Filter4>"
    Then system displays list of contracts according to the four selected filter criteria "<Filter Result>"

    Examples:
      | Filter1                 | Filter2                 | Filter3                 | Filter4                 | Filter Result                                                   |
      | Aktif                   | Dibatalkan              | Sewa Berakhir           | Dibayar di Luar MamiPAY | Aktif, Dibatalkan, Sewa Berakhir, Dibayar di Luar MamiPAY       |
      | Aktif                   | Dibatalkan              | Sewa Berakhir           | Dibayar                 | Aktif, Dibatalkan, Sewa Berakhir, Dibayar                       |
      | Aktif                   | Dibatalkan              | Sewa Berakhir           | Belum Dibayar           | Aktif, Dibatalkan, Sewa Berakhir, Belum Bayar                   |
      | Aktif                   | Dibatalkan              | Sewa Berakhir           | Konsultan               | Aktif, Dibatalkan, Sewa Berakhir, Konsultan                     |
      | Aktif                   | Dibatalkan              | Sewa Berakhir           | Calon Penyewa           | Aktif, Dibatalkan, Sewa Berakhir, Calon Penyewa                 |
      | Aktif                   | Dibatalkan              | Sewa Berakhir           | Pemilik Kos             | Aktif, Dibatalkan, Sewa Berakhir, Pemilik Kos                   |
      | Aktif                   | Dibayar di Luar MamiPAY | Dibayar                 | Belum Dibayar           | Aktif, Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar          |
      | Dibatalkan              | Dibayar di Luar MamiPAY | Dibayar                 | Belum Dibayar           | Dibatalkan, Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar     |
      | Sewa Berakhir           | Dibayar di Luar MamiPAY | Dibayar                 | Belum Dibayar           | Sewa Berakhir, Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar  |
      | Dibayar di Luar MamiPAY | Dibayar                 | Belum Dibayar           | Konsultan               | Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar, Konsultan      |
      | Dibayar di Luar MamiPAY | Dibayar                 | Belum Dibayar           | Calon Penyewa           | Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar, Calon Penyewa  |
      | Dibayar di Luar MamiPAY | Dibayar                 | Belum Dibayar           | Pemilik Kos             | Dibayar di Luar MamiPAY, Dibayar, Belum Dibayar, Pemilik Kos    |
      | Dibayar di Luar MamiPAY | Konsultan               | Calon Penyewa           | Pemilik Kos             | Dibayar di Luar MamiPAY, Konsultan, Calon Penyewa, Pemilik Kos  |
      | Dibayar                 | Konsultan               | Calon Penyewa           | Pemilik Kos             | Dibayar, Konsultan, Calon Penyewa, Pemilik Kos                  |
      | Belum Dibayar           | Konsultan               | Calon Penyewa           | Pemilik Kos             | Belum Dibayar, Konsultan, Calon Penyewa, Pemilik Kos            |
      | Aktif                   | Konsultan               | Calon Penyewa           | Pemilik Kos             | Aktif, Konsultan, Calon Penyewa, Pemilik Kos                    |
      | Dibatalkan              | Konsultan               | Calon Penyewa           | Pemilik Kos             | Dibatalkan, Konsultan, Calon Penyewa, Pemilik Kos               |
      | Sewa Berakhir           | Konsultan               | Calon Penyewa           | Pemilik Kos             | Sewa Berakhir, Konsultan, Calon Penyewa, Pemilik Kos            |