@regression @consultant @add-new-contract @crm-staging @crm-prod

Feature: Add New Contract

  Background: Check If User Have Active Contract
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user search tenant contract "082242455775"
    Then user terminate contract if there is "Belum Dibayar" contract

  Scenario: Input Active Kos Name
    Given user navigates to "consultant /"
    When user access to menu "Kelola Kontrak"
    And user click on create new contract button
    And user input kos name "Tobelo"
    Then system display kos list "Tobelo"

  Scenario: Input Unactive Kos Name
    Given user navigates to "consultant /"
    When user access to menu "Kelola Kontrak"
    And user click on create new contract button
    And user input kos name "bebasaja"
    Then system display kos not found page

  Scenario: Input Valid Nomor HP Which Have Recorded As Potential Tenant
    Given user navigates to "consultant /"
    When user access to menu "Kelola Kontrak"
    And user click on create new contract button
    When user input consultant kos name
    When user choose kos consultant
    And user input registered phone number
    Then user directed to create new contract form
    And tenant data already filled

  Scenario: Input Valid Nomor HP Which Not Have Recorded As Potential Tenant
    Given user navigates to "consultant /"
    When user access to menu "Kelola Kontrak"
    And user click on create new contract button
    When user input consultant kos name
    When user choose kos consultant
    And user input not registered phone number
    Then user directed to create new contract form
    And tenant data is empty

    Scenario Outline: Blank Field Validation On "<Field>"
      Given user navigates to "consultant /"
      When user access to menu "Kelola Kontrak"
      And user click on create new contract button
      When user input consultant kos name
      When user choose kos consultant
      When user input registered phone number
      And user empty "<Field>"
      Then there is a blank validation error message on "<Field>"

      Examples:
        | Field                      |
        | fieldName                  |
        | fieldHP                    |
        | fieldEmail                 |
        | fieldGender                |
        | fieldStatus                |
        | fieldOccupation            |
        | fieldNamaPenanggungJawab   |
        | fieldKontakPenanggungJawab |

      Scenario: Add new contract from mamipay admin
        Given user navigates to "backoffice"
        When user login as a consultant via credentials
        And user access search contract menu
        And user search contract by tenant phone number
        And check tenant have active contract
        And user click add contract button
        And user choose property
        And user input tenant phone number
        And user fill detail sewa
        And user skip tambahan biaya
        And user confirm contract
        Then user search contract by tenant phone number
        And validate there is new active contract
        And user terminate active contract