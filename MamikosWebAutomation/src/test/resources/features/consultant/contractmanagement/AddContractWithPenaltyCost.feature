@regression @consultant @crm-staging @crm-prod @add-penalty-cost

Feature: Add Contract with Penalty Cost

  Background: Check if user have active contract
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Kontrak"
    And user search tenant contract "082242455775"
    Then user terminate contract if there is "Belum Dibayar" contract

  Scenario: Add Contract with Penalty Cost
    And user click on create new contract button
    And user input kos name "Tobelo"
    And user choose kos consultant
    And user input registered phone number
    And user skip personal data
    And user choose room number
    And user choose check in date
    And user choose rent duration "1 Bulan"

#  Scenario: Verify Penalty Cost Disappear
    Then system display penalty cost disappeared

#  Scenario: Verify save button disable when user activate penalty cost
    When user click on penalty cost
    Then system display save contract button is disable

#  Scenario: Verify save button disable after input amount penalty cost
    When user input amount penalty cost "1000"
    Then system display save contract button is disable

#  Scenario: Verify save button disable after input duration penalty cost
    When user input range penalty cost
    Then system display save contract button is disable

#  Scenario: Verify system display message error Nilai Durasi tidak boleh lebih dari 366.
    When user input range penalty cost "367" and period penalty cost "Hari"
    Then system display save contract button is disable
    And system display error message range duration "Nilai Durasi tidak boleh lebih dari 366."

#  Scenario: Verify system display message error Nilai Durasi tidak boleh lebih dari 52.
    When user input range penalty cost "53" and period penalty cost "Minggu"
    Then system display save contract button is disable
    And system display error message range duration "Nilai Durasi tidak boleh lebih dari 52."

#  Scenario: Verify system display message error Nilai Durasi tidak boleh lebih dari 12.
    When user input range penalty cost "13" and period penalty cost "Bulan"
    Then system display save contract button is disable
    And system display error message range duration "Nilai Durasi tidak boleh lebih dari 12."

#  Scenario: Deactivate toggle penalty cost
    When user click on penalty cost
    Then system display penalty cost disappeared

#  Scenario: Create contract successfully
    And user add penalty cost "10000" per "10" "Hari"
    And user click on save contract button
    Then system display contract successfully created