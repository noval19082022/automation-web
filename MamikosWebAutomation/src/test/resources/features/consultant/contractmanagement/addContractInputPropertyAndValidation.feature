@regression @consultant @crm-staging @crm-prod @property-validation

Feature: Add Contract Input Property and Validation

  Scenario: System Display Back Button and Search Kos Button is Disable
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user click on create new contract button
    Then system display back arrow and search kos button is disable

#  Scenario: Icon Back Arrow on Create Contract Page Redirect to List of Contract
    When user click on back button arrow
    Then system display list of contract

#  Scenario: Display message "Kost tidak ditemukan" when user Input un-active property or property active but owner not activate mamipay
    When user click on create new contract button
    And user input kos name "ABCDE"
    Then system display property not found

#  Scenario: Before select any property, next button is disable
    When user click on back button arrow
    And user input kos name "Tobelo"
    Then system display next button add new contract is disable

#  Scenario: Click on Back Button Arrow Redirect to Search Property Form
    When user click on back button arrow
    Then system display form search property