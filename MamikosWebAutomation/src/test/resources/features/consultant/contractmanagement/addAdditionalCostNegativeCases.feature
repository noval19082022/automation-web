@regression @consultant @crm-staging @crm-prod @add-additional-cost-negative

Feature: Add Additional Cost Negative Case
  Background: Check if user have active contract
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Kontrak"
    And user search tenant contract "082242455775"
    Then user terminate contract if there is "Belum Dibayar" contract

  Scenario: Left name biaya tetap blank
    When user click on create new contract button
    And user input kos name "Tobelo"
    And user choose kos consultant
    And user input registered phone number
    And user skip personal data
    And user choose room number
    And user choose check in date
    And user choose rent duration "1 Bulan"
    And user add fixed cost "" "100000"
    Then no fixed cost added

#  Scenario: Left amount biaya tetap blank
    When user add fixed cost "Wifi" ""
    Then no fixed cost added

#  Scenario: Left name biaya lain blank
    When user add additional cost "" "50000"
    Then no additional cost added

#  Scenario: Left amount biaya lain blank
    When user add additional cost "Kebersihan" ""
    Then no additional cost added