@regression @consultant

Feature: Filter Contract with Three Parameter

  Scenario Outline: User filter contract by <Filter1> and <Filter2> and <Filter3>
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user filter contract by "<Filter1>" and "<Filter2>" and "<Filter3>"
    Then system displays list of contracts according to the three selected filter criteria "<Filter Result>"

    Examples:
      | Filter1                 | Filter2                 | Filter3                 | Filter Result                                         |
      | Aktif                   | Dibatalkan              | Sewa Berakhir           | Aktif, Dibatalkan, Sewa Berakhir                      |
      | Aktif                   | Dibatalkan              | Dibayar di Luar MamiPAY | Aktif, Dibatalkan, Dibayar di Luar MamiPAY            |
      | Dibatalkan              | Sewa Berakhir           | Dibayar di Luar MamiPAY | Dibatalkan, Sewa Berakhir, Dibayar di Luar MamiPAY    |
      | Aktif                   | Sewa Berakhir           | Dibayar di Luar MamiPAY | Aktif, Sewa Berakhir, Dibayar di Luar MamiPAY         |
      | Aktif                   | Dibatalkan              | Dibayar                 | Aktif, Dibatalkan, Dibayar                            |
      | Dibatalkan              | Sewa Berakhir           | Dibayar                 | Dibatalkan, Sewa Berakhir, Dibayar                    |
      | Aktif                   | Sewa Berakhir           | Dibayar                 | Aktif, Sewa Berakhir, Dibayar                         |
      | Aktif                   | Dibatalkan              | Belum Dibayar           | Aktif, Dibatalkan, Belum Dibayar                      |
      | Dibatalkan              | Sewa Berakhir           | Belum Dibayar           | Dibatalkan, Sewa Berakhir, Belum Dibayar              |
      | Aktif                   | Sewa Berakhir           | Belum Dibayar           | Aktif, Sewa Berakhir, Belum Dibayar                   |
      | Aktif                   | Dibatalkan              | Konsultan               | Aktif, Dibatalkan, Konsultan                          |
      | Dibatalkan              | Sewa Berakhir           | Konsultan               | Dibatalkan, Sewa Berakhir, Konsultan                  |
      | Aktif                   | Sewa Berakhir           | Konsultan               | Aktif, Sewa Berakhir, Konsultan                       |
      | Aktif                   | Dibatalkan              | Calon Penyewa           | Aktif, Dibatalkan, Calon Penyewa                      |
      | Dibatalkan              | Sewa Berakhir           | Calon Penyewa           | Dibatalkan, Sewa Berakhir, Calon Penyewa              |
      | Aktif                   | Sewa Berakhir           | Calon Penyewa           | Aktif, Sewa Berakhir, Calon Penyewa                   |
      | Aktif                   | Dibatalkan              | Pemilik Kos             | Aktif, Dibatalkan, Pemilik Kos                        |
      | Dibatalkan              | Sewa Berakhir           | Pemilik Kos             | Dibatalkan, Sewa Berakhir, Pemilik Kos                |
      | Aktif                   | Sewa Berakhir           | Pemilik Kos             | Aktif, Sewa Berakhir, Pemilik Kos                     |
      | Aktif                   | Dibayar di Luar MamiPAY | Dibayar                 | Aktif, Dibayar di Luar MamiPAY, Dibayar               |
      | Aktif                   | Dibayar                 | Belum Dibayar           | Aktif, Dibayar, Belum Dibayar                         |
      | Aktif                   | Dibayar di Luar MamiPAY | Belum Dibayar           | Aktif, Dibayar di Luar MamiPAY, Belum Dibayar         |
      | Dibatalkan              | Dibayar di Luar MamiPAY | Dibayar                 | Dibatalkan, Dibayar di Luar MamiPAY, Dibayar          |
      | Dibatalkan              | Dibayar                 | Belum Dibayar           | Dibatalkan, Dibayar, Belum Dibayar                    |
      | Dibatalkan              | Dibayar di Luar MamiPAY | Belum Dibayar           | Dibatalkan, Dibayar di Luar MamiPAY, Belum Dibayar    |
      | Sewa Berakhir           | Dibayar di Luar MamiPAY | Dibayar                 | Sewa Berakhir, Dibayar di Luar MamiPAY, Dibayar       |
      | Sewa Berakhir           | Dibayar                 | Belum Dibayar           | Sewa Berakhir, Dibayar, Belum Dibayar                 |
      | Sewa Berakhir           | Dibayar di Luar MamiPAY | Belum Dibayar           | Sewa Berakhir, Dibayar di Luar MamiPAY, Belum Dibayar |
      | Dibayar di Luar MamiPAY | Dibayar                 | Konsultan               | Dibayar di Luar MamiPAY, Dibayar, Konsultan           |
      | Dibayar                 | Belum Dibayar           | Konsultan               | Dibayar, Belum Dibayar , Konsultan                    |
      | Dibayar di Luar MamiPAY | Belum Dibayar           | Konsultan               | Dibayar di Luar MamiPAY, Belum Dibayar, Konsultan     |
      | Dibayar di Luar MamiPAY | Dibayar                 | Calon Penyewa           | Dibayar di Luar MamiPAY, Dibayar, Calon Penyewa       |
      | Dibayar                 | Belum Dibayar           | Calon Penyewa           | Dibayar, Belum Dibayar, Calon Penyewa                 |
      | Dibayar di Luar MamiPAY | Belum Dibayar           | Calon Penyewa           | Dibayar di Luar MamiPAY, Belum Dibayar, Calon Penyewa |
      | Dibayar di Luar MamiPAY | Dibayar                 | Pemilik Kos             | Dibayar di Luar MamiPAY, Dibayar, Pemilik Kos         |
      | Dibayar                 | Belum Dibayar           | Pemilik Kos             | Dibayar, Belum Dibayar, Pemilik Kos                   |
      | Dibayar di Luar MamiPAY | Belum Dibayar           | Pemilik Kos             | Dibayar di Luar MamiPAY, Belum Dibayar, Pemilik Kos   |
      | Aktif                   | Konsultan               | Calon Penyewa           | Aktif, Konsultan, Calon Penyewa                       |
      | Aktif                   | Calon Penyewa           | Pemilik Kos             | Aktif, Calon Penyewa, Pemilik Kos                     |
      | Aktif                   | Konsultan               | Pemilik Kos             | Aktif, Konsultan, Pemilik Kos                         |
      | Dibatalkan              | Konsultan               | Calon Penyewa           | Dibatalkan, Konsultan, Calon Penyewa                  |
      | Dibatalkan              | Calon Penyewa           | Pemilik Kos             | Dibatalkan, Calon Penyewa, Pemilik Kos                |
      | Dibatalkan              | Konsultan               | Pemilik Kos             | Dibatalkan, Konsultan, Pemilik Kos                    |
      | Sewa Berakhir           | Konsultan               | Calon Penyewa           | Sewa Berakhir, Konsultan, Calon Penyewa               |
      | Sewa Berakhir           | Calon Penyewa           | Pemilik Kos             | Sewa Berakhir, Calon Penyewa, Pemilik Kos             |
      | Sewa Berakhir           | Konsultan               | Pemilik Kos             | Sewa Berakhir, Konsultan, Pemilik Kos                 |
      | Dibayar di Luar MamiPAY | Konsultan               | Calon Penyewa           | Dibayar di Luar MamiPAY, Konsultan, Calon Penyewa     |
      | Dibayar di Luar MamiPAY | Calon Penyewa           | Pemilik Kos             | Dibayar di Luar MamiPAY, Calon Penyewa, Pemilik Kos   |
      | Dibayar di Luar MamiPAY | Konsultan               | Pemilik Kos             | Dibayar di Luar MamiPAY, Konsultan, Pemilik Kos       |
      | Dibayar                 | Konsultan               | Calon Penyewa           | Dibayar, Konsultan, Calon Penyewa                     |
      | Dibayar                 | Calon Penyewa           | Pemilik Kos             | Dibayar, Calon Penyewa, Pemilik Kos                   |
      | Dibayar                 | Konsultan               | Pemilik Kos             | Dibayar, Konsultan, Pemilik Kos                       |
      | Belum Dibayar           | Konsultan               | Calon Penyewa           | Belum Dibayar, Konsultan, Calon Penyewa               |
      | Belum Dibayar           | Calon Penyewa           | Pemilik Kos             | Belum Dibayar, Calon Penyewa, Pemilik Kos             |
      | Belum Dibayar           | Konsultan               | Pemilik Kos             | Belum Dibayar, Konsultan, Pemilik Kos                 |