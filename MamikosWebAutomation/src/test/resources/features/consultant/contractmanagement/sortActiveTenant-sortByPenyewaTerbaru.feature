@regression @consultant @crm-staging @crm-prod @add-penalty-cost

Feature: Short Newest Active Tenant

  Background: Check if user have active contract
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Kontrak"
    And user search tenant contract "082242455775"
    Then user terminate contract if there is "Belum Dibayar" contract

  Scenario: Add Contract and Verify The Newest Contract Appears at The Top of The Contract List
    And user click on create new contract button
    And user input kos name "Tobelo"
    And user choose kos consultant
    And user input registered phone number
    And user skip personal data
    And user choose room number
    And user choose check in date
    And user choose rent duration "1 Bulan"
    And user click on save contract button

#   Scenario: The Newest Contract Appears at The Top of The Contract List
    Given user navigates to "consultant /"
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Tenant"
    And user sort newest tenant
    And system display newest contract appears at the top of the contract list