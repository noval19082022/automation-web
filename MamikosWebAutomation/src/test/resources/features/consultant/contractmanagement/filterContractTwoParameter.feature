@regression @consultant

Feature: Filter Contract with Two Parameter

  Scenario Outline: User filter contract by <Filter1> and <Filter2>
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user filter contract by "<Filter1>" and "<Filter2>"
    Then system displays list of contracts according to the two selected filter criteria "<Filter Result>"

    Examples:
      | Filter1         | Filter2                 | Filter Result                         |
      | Aktif           | Dibatalkan              | Aktif, Dibatalkan                     |
      | Aktif           | Sewa Berakhir           | Aktif, Sewa Berakhir                  |
      | Dibatalkan      | Sewa Berakhir           | Dibatalkan, Sewa Berakhir             |
      | Aktif           | Dibayar di Luar MamiPAY | Aktif, Dibayar di Luar MamiPAY        |
      | Dibatalkan      | Dibayar di Luar MamiPAY | Dibatalkan, Dibayar di Luar MamiPAY   |
      | Sewa Berakhir   | Dibayar di Luar MamiPAY | Sewa Berakhir, Dibayar di Luar MamiPAY|
      | Aktif           | Dibayar                 | Aktif, Dibayar                        |
      | Dibatalkan      | Dibayar                 | Dibatalkan, Dibayar                   |
      | Sewa Berakhir   | Dibayar                 | Sewa Berakhir, Dibayar                |
      | Aktif           | Belum Dibayar           | Aktif, Belum Dibayar                  |
      | Dibatalkan      | Belum Dibayar           | Dibatalkan, Belum Dibayar             |
      | Sewa Berakhir   | Belum Dibayar           | Sewa Berakhir, Belum Dibayar          |
      | Aktif           | Konsultan               | Aktif, Konsultan                      |
      | Dibatalkan      | Konsultan               | Dibatalkan, Konsultan                 |
      | Sewa Berakhir   | Konsultan               | Sewa Berakhir, Konsultan              |
      | Aktif           | Calon Penyewa           | Aktif, Calon Penyewa                  |
      | Dibatalkan      | Calon Penyewa           | Dibatalkan, Calon Penyewa             |
      | Sewa Berakhir   | Calon Penyewa           | Sewa Berakhir, Calon Penyewa          |
      | Aktif           | Pemilik Kos             | Aktif, Pemilik Kos                    |
      | Dibatalkan      | Pemilik Kos             | Dibatalkan, Pemilik Kos               |
      | Sewa Berakhir   | Pemilik Kos             | Sewa Berakhir, Pemilik Kos            |