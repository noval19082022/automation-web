@regression @consultant

Feature: Filter Contract with One Parameter

  Scenario: User filter Contract Status is active
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user filter contract with contract status is "Aktif"
    Then system display contract list with contract status is "Dibayar" or "Belum Dibayar"

  Scenario Outline: user filter contract by status <Contract Status>
    Given user navigates to "consultant /"
    When user "dewy" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user filter contract with contract status is "<Contract Status>"
    Then system display contract list with contract status is "<Contract Status>"

    Examples:
      | Contract Status |
      | Dibatalkan      |
      | Sewa Berakhir   |

  Scenario Outline: user filter contract by status <Payment Status>
    Given user navigates to "consultant /"
    When user "dewy" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user filter contract with payment status is "<Payment Status>"
    Then system display contract list with payment status is "<Payment Status>"

    Examples:
      | Payment Status          |
      | Dibayar                 |
      | Dibayar di Luar MamiPAY |
      | Belum Dibayar           |

  Scenario: user filter contract created by consultant
    Given user navigates to "consultant /"
    When user "dewy" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user filter contract with created by "Konsultan"
    Then system display contract list not created by "Pemilik Kos" or "Calon Penyewa"

  Scenario Outline: user filter contract created by <Created By>
    Given user navigates to "consultant /"
    When user "dewy" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user filter contract with created by "<Created By>"
    Then system display contract list with created by "<Created By>"

    Examples:
      | Created By |
      | Calon Penyewa  |
      | Pemilik Kos    |