@regression @consultant @crm-staging @crm-prod @add-additional-cost

  Feature: Add Additional Cost

    Background: Check if user have active contract
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Kontrak"
      And user search tenant contract "082242455775"
      Then user terminate contract if there is "Belum Dibayar" contract

    Scenario: Add fixed cost
      And user click on create new contract button
      And user input kos name "Tobelo"
      And user choose kos consultant
      And user input registered phone number
      And user skip personal data
      And user choose room number
      And user choose check in date
      And user choose rent duration "1 Bulan"
      And user add fixed cost "Wifi" "100000"
      Then fixed cost added "Wifi" "Rp 100.000"

#    Scenario: Add additional cost
      And user add additional cost "Kebersihan" "50000"
      Then additional cost added "Kebersihan" "Rp 50.000"

#    Scenario: Add penalty cost
      And user add penalty cost "10000" per "10" "Hari"

#    Scenario: Verify additional cost and fix cost in detail contract
      And user click on save contract button
      And user click tidak terimakasih button
      And user search tenant contract "082242455775"
      And user lihat detail kontrak
      And user ubah biaya lain
      Then fixed cost contains "Wifi" "Rp 100.000"
      And additional cost contains "Kebersihan" "Rp 50.000"