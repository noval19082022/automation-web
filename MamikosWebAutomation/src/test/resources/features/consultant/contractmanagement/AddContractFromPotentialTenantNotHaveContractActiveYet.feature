@regression @consultant @crm-staging @crm-@crm-prod @add-contract

Feature: Add Contract from Potential Tenant not have Contract Active Yet

  Background: Deleting existing booking data in admin if exist
    Given user navigates to "backoffice"
    And user login as a consultant via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number
    And user terminate active contract

  Scenario: Add Contract from Potential Tenant not have Contract Active Yet
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Penyewa Potensial"
    And user search potential tenant by phone number "potentialTenant"
    And user create contract
    Then system display contract successfully created

#  Scenario: Verify Create Contract Button is Disable after Successfully Create Contract
    Given user navigates to "consultant /"
    When user access to menu "Penyewa Potensial"
    And user search potential tenant by phone number "potentialTenant"
    Then system display create contract button is disable
