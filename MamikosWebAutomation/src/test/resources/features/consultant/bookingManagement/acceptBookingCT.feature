@regression @consultant @booking-management-CT @accept-booking @crm-staging @crm-prod
  
  Feature: Accept Booking From Consultant Tools

    Background: Deleting existing booking data in admin if exist
      Given user navigates to "backoffice"
      And user login as a consultant via credentials
      And user click on Search Contract Menu form left bar
      Then user Navigate "Search Contract" page
      And user search by Kost name "Griya Tobelo"
      And user click on Cancel the Contract Button from action column

#    Scenario:Check at least 1 room available to book
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Properti"
      And user search property "Tobelo"
      And user choose property "Griya Tobelo"
      And user click on update kamar dan harga button
      And user click update ketersediaan kamar button
      And user check at least 1 room available

#    Scenario: Cancel booking if tenant have booking
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "tenant CRM"
      And user navigates to "mamikos /user/booking/"
      And user cancel booking

#    Scenario: Create booking from tenant CRM
      Given user navigates to "mamikos /"
      When user clicks search bar
      And I search property with name "Griya Tobelo" and select matching result to go to kos details page
      And user clicks on Booking button on Kost details page
      And user selects T&C checkbox and clicks on Book button
      Then system display successfully booking

    @acceptwhenfull
    Scenario: Accept booking when room of property full
      Given user navigates to "consultant /"
      And user access to menu "Kelola Properti"
      And user search property "Tobelo"
      And user choose property "Griya Tobelo"
      And user click on update kamar dan harga button
      And user click update ketersediaan kamar button
      And user set all room to sudah berpenghuni

#    Scenario: accept booking when room is full
      And user navigates to "consultant /"
      And user access to menu "Kelola Booking"
      And search booking data by Automation CRM
      And user see booking details
      And user accept booking
      And user choose "Pilih ditempat" room
      And user simpan kontrak
      Then user got error message "Tidak bisa terima booking karena kamar penuh."

#    Scenario: set room to available
      And user navigates to "consultant /"
      And user access to menu "Kelola Properti"
      And user search property "Tobelo"
      And user choose property "Griya Tobelo"
      And user click on update kamar dan harga button
      And user click update ketersediaan kamar button
      And user set 3 room to available

    @acceptpilihditempat
    Scenario: Accept booking using pilih ditempat room
      Given user navigates to "consultant /"
      And user access to menu "Kelola Properti"
      And user search property "Tobelo"
      And user choose property "Griya Tobelo"
      And user save total room terisi

#    Scenario: accept booking
      And user navigates to "consultant /"
      And user access to menu "Kelola Booking"
      And search booking data by Automation CRM
      And user see booking details
      And user accept booking
      And user choose "Pilih ditempat" room
      And user simpan kontrak
      Then user got success message "Kontrak telah disimpan"
      And booking status become "Tunggu Pembayaran"
      And room set as "Pilih di tempat"

#    Scenario: check total room terisi
      And user access to menu "Kelola Properti"
      And user search property "Tobelo"
      And user choose property "Griya Tobelo"
      And total number room terisi increases by one

      @acceptonlyfillmandatoryfield
      Scenario: Accept booking with only fill mandatory field
        Given user navigates to "consultant /"
        When user access to menu "Kelola Booking"
        And search booking data by Automation CRM
        And user see booking details
        And user accept booking
        And user choose "3" room
        Then simpan button is active
        And user simpan kontrak
        Then user got success message "Kontrak telah disimpan"
        And user redirect to detail booking page
        And booking status become "Tunggu Pembayaran"
        And room set as "3"

      @acceptfillallfield
      Scenario: Accept booking with fill all field
        Given user navigates to "consultant /"
        When user access to menu "Kelola Booking"
        And search booking data by Automation CRM
        And user see booking details
        And user accept booking
        And user choose "3" room
        And user add deposit "10000"
        And user add denda keterlambatan "50000" if it pass "1" "Minggu"
        And user add biaya lain "Wifi" "50000"
        And user add DP "20%"
        And user simpan kontrak
        Then user got success message "Kontrak telah disimpan"
        And user redirect to detail booking page
        And booking status become "Tunggu Pembayaran"
        And deposit recorded in contract
        And DP recorded in contract
        And biaya lain recorded in contract
        And room set as "3"