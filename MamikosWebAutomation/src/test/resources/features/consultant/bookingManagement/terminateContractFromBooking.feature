@regression @consultant @crm-staging @terminate-contract-from-booking

Feature: Terminate Contract from Booking

  Background: Deleting Existing Booking Data in Admin if Exist
    Given user navigates to "backoffice"
    And user login as a consultant via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by Kost name "Griya Tobelo"
    And user click on Cancel the Contract Button from action column

#  Scenario: Check at least 1 room available to book
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Properti"
    And user search property "Tobelo"
    And user choose property "Griya Tobelo"
    And user click on update kamar dan harga button
    And user click update ketersediaan kamar button
    And user check at least 1 room available

#  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "tenant CRM"
    And user navigates to "mamikos /user/booking/"
    And user cancel booking

#  Scenario: Create booking from tenant CRM
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "Griya Tobelo" and select matching result to go to kos details page
    And user clicks on Booking button on Kost details page
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

#  Scenario: Accept booking
    Given user navigates to "consultant /"
    When user access to menu "Kelola Booking"
    And search booking data by Automation CRM
    And user see booking details
    And user accept booking
    And user choose "1" room
    Then simpan button is active
    And user simpan kontrak
    Then user got success message "Kontrak telah disimpan"
    And user redirect to detail booking page

#  Scenario: Bayar booking
    Given user navigates to "mamikos /"
    When user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user select payment method "Mandiri" for "consultant"
    Then system display payment using "Mandiri" is "Success Transaction"

    Scenario: Terminate Contract from detail contract page
      Given user navigates to "consultant /"
      When user access to menu "Kelola Booking"
      And search booking data "Terbayar" by Automation CRM
      And user see booking details
      And user view contract details
      And user terminate contract from detail contract page
      Then contract terminated
      And contract have status "Sewa Berakhir"