@regression @consultant @crm-staging @crm-prod

Feature: Filter Booking Goldplus

  Scenario: Filter Booking Goldplus 1
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Booking"
    And user filter booking "Mamikos Goldplus 1"
    Then system display booking have property type "Mamikos Goldplus 1"