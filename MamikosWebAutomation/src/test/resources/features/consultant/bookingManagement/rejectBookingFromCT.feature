@regression @consultant @crm-staging @reject-booking-ct @crm-prod

  Feature: Reject Booking from Consultant Tools

  Background: Deleting existing booking data in admin if exist
    Given user navigates to "backoffice"
    And user login as a consultant via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by Kost name "Griya Tobelo"
    And user click on Cancel the Contract Button from action column

#    Scenario: Cancel booking if tenant have booking
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "tenant CRM"
      And user navigates to "mamikos /user/booking/"
      And user cancel booking

#   Scenario: Create booking from tenant CRM Reject booking using default reason
      Given user navigates to "mamikos /"
      When user clicks search bar
      And I search property with name "Griya Tobelo" and select matching result to go to kos details page
      And user clicks on Booking button on Kost details page
      And user selects T&C checkbox and clicks on Book button
      Then system display successfully booking

  @defaultreason
  Scenario: Reject booking using default reason
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Booking"
    And search booking data by Automation CRM
    And user see booking details
    And user reject booking
    And user choose reason "Kos penuh"
    Then booking status become "Ditolak Admin"
    And contains reason "Kos penuh"

  @customreason
  Scenario: Reject booking using custom reason
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Booking"
    And search booking data by Automation CRM
    And user see booking details
    And user reject booking
    And user choose reason "Lainnya"
    And user fill the reason "Test tolak booking using automation"
    Then booking status become "Ditolak Admin"
    And contains reason "Test tolak booking using automation"