@regression @consultant @crm-staging

Feature: Edit Sales Motion

  Background: Deactivate Sales Motion from list
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Sales Motion menu
    And user search sales motion
    And user set sales motion to not active and default assignee

  Scenario: Activate Sales Motion from Edit Sales Motion
    When user access to Sales Motion menu
    And user search sales motion
    And user click edit sales motion
    And user activate sales motion from edit page
    Then user verify sales motion status is "Active"
    And user verify sales motion updated by correct consultant

  Scenario: Edit Assignee sales motion from Detail Sales Motion
    When user access to Sales Motion menu
    And user search sales motion
    And user click detail sales motion
    And user edit assignee to other consultant
    Then user verify new assignee consultant appear on detail sales motion
    And user verify sales motion updated by correct consultant
