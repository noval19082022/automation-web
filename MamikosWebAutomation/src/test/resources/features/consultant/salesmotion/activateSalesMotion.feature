@regression @consultant @crm-staging

Feature: Activate Sales Motion from List

  Background: Deactivate Sales Motion from detail
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Sales Motion menu
    And user search sales motion
    And user set sales motion to not active from detail

  Scenario: Activate Sales Motion from List Sales Motion
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Sales Motion menu
    And user search sales motion
    And user activate sales motion from list page
    Then user verify sales motion status on list is "Active"