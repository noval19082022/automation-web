@regression @consultant @crm-staging

Feature: Create New Sales Motion

  Scenario: Create New Sales Motion with complete field
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to Sales Motion menu
    And user click Create New Sales Motion
    And user submit task sales motion complete
    And user submit consultant to handle task sales motion
    Then user can see "Detail Sales Motion and Assignee"