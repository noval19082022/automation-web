@regression @consultant @crm-staging

Feature: Submit Report on Sales Motion Menu

  Scenario: Submit not Interested Report With Contract as Data Associate
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    Then system display label "Konsultan Mamikos"
    When user access to menu "Sales Motion"
    And user search sales motion to submit report
    And user submit contract as not interested report of sales motion
    Then system display "Tambah Laporan"

  Scenario: Submit follow up report with DBET as data associate
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    Then system display label "Konsultan Mamikos"
    When user access to menu "Sales Motion"
    And user search sales motion to submit report
    And user submit dbet as follow up report of sales motion
    Then system display "Tambah Laporan"

  Scenario: Submit interested report with property as data associate
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    Then system display label "Konsultan Mamikos"
    When user access to menu "Sales Motion"
    And user search sales motion to submit report
    And user submit property as interested report of sales motion
    Then system display "Tambah Laporan"