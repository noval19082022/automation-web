@regression @consultant @crm-staging @activate-task-potential-owner

Feature: Activity Management Manage Data Type Potential Owner - Activate Same Task

  Background: Move First Active Task to Task List if Task Have Been Active Before
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task potential owner on list active task
    And user move task to task list

#  Scenario: Move Second Active Task to Task List if Task Have Been Active Before
    When user navigates to "consultant/activity"
    And user search second task potential owner on list active task
    And user move task potential owner to task list

  Scenario: Activate First Task Data Potential Owner from Detail Task
    When user navigates to "consultant/activity"
    And user activate task potential owner
    Then user verify stage name is "Tahap 1: Tugas Aktif"

#  Scenario: Check history status is active
    When user click history task
    Then user verify task status is "Tugas Aktif" and back to manage task page

#  Scenario: Search active task with data type Potential Owner
    When user search task potential owner on list active task
    And user click the search result
    Then user verify search result is task potential owner

#  Scenario: Verify Second Task Data Potential Owner Have a Status is Daftar Tugas
    And user navigates to "consultant/activity"
    And user go to detail task on second funnel
#    Then user verify stage name is "Daftar Tugas"
#    Verifikasi sementara dikomen karena masih bugs