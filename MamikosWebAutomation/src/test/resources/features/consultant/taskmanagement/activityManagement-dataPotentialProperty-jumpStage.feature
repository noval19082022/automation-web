@regression @consultant @crm-staging

Feature: Activity Management Manage Data Type Potential Property - Move Task to Current Stage + 1 (Jump)

  Background: Move Active Task to Task List if Task Have Been Active Before
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task potential property on list active task
    And user move task to task list

  Scenario: Activate Task Data Potential Property from Detail
    Given user navigates to "consultant /"
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Tugas"
    And user activate task potential property
    Then user verify stage name is "Tahap 1: Tugas Aktif"

#  Scenario: Check history status is active
    When user click history task
    Then user verify task status is "Tugas Aktif" and back to manage task page

#  Scenario: Search active task with data type Potential Property
    When user search task potential property on list active task
    And user click the search result
    Then user verify search result is task potential property

#  Scenario: Move Task to Current Stage + 1 (Jump)
    When user move with skip one stage
    And user fill form input data property for potential property
    Then user verify stage name is "Tahap 3: Test 2"

#  Scenario: Verify history status is contract sent
    When user click history task
    Then user verify task status is "Test 2"

#  Scenario: Move task to Task List(Backlog)
    When user back to previous page
    When user move task to active task stage
    Then user verify funnel name is "Tahap 1: Tugas Aktif"

#  Scenario: Move task to Task List(Backlog)
    When user move active task to task list stage
    Then user verify funnel name is "Daftar Tugas"
