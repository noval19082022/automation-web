@regression @consultant @crm-staging @crm-prod @search-active-task

Feature: Search Active Task

  Scenario: Validation Search Active Task Minimum Five Character
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search active task with keyword "mami"
    Then system display warning message minimum character

  Scenario: Validation Search Active Task without Select Data Type
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search active task with keyword "mamikos" without select data type
    Then system display warning message choose data type

  Scenario: Search Active Task with Invalid Data
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search active task with keyword "loremipsum"
    Then system display empty state "Data Tidak Ditemukan"

  Scenario: Search Active Task Contract by Tenant Phone Number
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task contract by tenant phone number
    Then user verify search result is contract with that tenant phone number

  Scenario: Search Active Task Property by Owner Phone Number
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task property by owner phone number
    Then user verify search result is property with that owner phone number

  Scenario: Search Active Task Potential Tenant by Tenant Phone Number
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task potential tenant by tenant phone number
    Then user verify search result is potential tenant with that tenant phone number

  Scenario: Search Active Task Potential Owner by Owner Phone Number
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task potential owner by owner phone number
    Then user verify search result is potential owner with that owner phone number

  Scenario: Search Active Task Potential Property by Owner Phone Number
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task potential property by owner phone number
    Then user verify search result is potential property with that owner phone number