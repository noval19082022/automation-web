@regression @consultant @crm-staging @activate-task-contract

Feature: Activity Management Manage Data Type Contract - Move Task to Current Stage + 1 (Jump)

  Background: Move active task to task list if task have been active before
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task contract on list active task
    And user move task to task list

  Scenario: Activate Task Data Contract from Detail and Move Task to Current Stage + 1 (Jump)
    Given user navigates to "consultant /"
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Tugas"
    And user activate task contract
    Then user verify stage name is "Tahap 1: Tugas Aktif"

#  Scenario: Verify history status is active
    When user click history task
    Then user verify task status is "Tugas Aktif" and back to manage task page

#  Scenario: Search active task
    When user search task contract on list active task
    And user click the search result
    Then user verify search result is task contract

#  Scenario: Move Task to Current Stage + 1 (Jump)
    When user move with skip one stage
    And user fill activity form
    Then user verify stage name is "Tahap 3: Kontrak Telah Dikirim"

#  Scenario: Verify history status is contract sent
    When user click history task
    Then user verify task status is "Kontrak Telah Dikirim"

#  Scenario: Move task to Active Task (Backlog)
    When user back to previous page
    When user move task to active task stage
    Then user verify funnel name is "Tahap 1: Tugas Aktif"

#  Scenario: Move task to Task List(Backlog)
    When user move active task to task list stage
    Then user verify funnel name is "Daftar Tugas"

#  Scenario: Verify hyperlink contract on detail task
    When user click contract detail
    Then system display hyperlink contract detail