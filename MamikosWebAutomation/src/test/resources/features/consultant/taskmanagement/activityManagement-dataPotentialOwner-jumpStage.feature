@regression @consultant @crm-staging @activate-task-potential-tenant

Feature: Activity Management Manage Data Type Potential Owner - Move Task to Current Stage + 1 (Jump)

  Background: Move Active Task to Task List if Task Have Been Active Before
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task potential owner on list active task
    And user move task to task list

  Scenario: Activate Task Data Potential Owner from Detail and Move Task to Current Stage + 1 (Jump)
    Given user navigates to "consultant /"
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Tugas"
    And user activate task potential owner
    Then user verify stage name is "Tahap 1: Tugas Aktif"

#  Scenario: Check history status is active
    When user click history task
    Then user verify task status is "Tugas Aktif" and back to manage task page

#  Scenario: Search active task with data type Potential Owner
    When user search task potential owner on list active task
    And user click the search result
    Then user verify search result is task potential owner

#  Scenario: Move Task to Current Stage + 1 (Jump)
    When user move with skip one stage
    And user fill form input data potential owner
    Then user verify stage name is "Tahap 3: Stage 2"

#  Scenario: Verify history status is contract sent
    When user click history task
    Then user verify task status is "Stage 2"

#  Scenario: Move task to Task List(Backlog)
    When user back to previous page
    When user move task to active task stage
    Then user verify funnel name is "Tahap 1: Tugas Aktif"

#  Scenario: Move task to Task List(Backlog)
    When user move active task to task list stage
    Then user verify funnel name is "Daftar Tugas"
