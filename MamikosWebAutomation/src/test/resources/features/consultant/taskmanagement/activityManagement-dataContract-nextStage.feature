@regression @consultant @crm-staging @activate-task-contract

Feature: Activity Management Manage Data Type Contract - Move Task to Next Stage

  Background: Move Active Task to Task list if Task Have Been Active Before
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task contract on list active task
    And user move task to task list

  Scenario: Activate Task Data Contract from Detail and Move Task to Next Stage
    Given user navigates to "consultant /"
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Tugas"
    And user activate task contract
    Then user verify stage name is "Tahap 1: Tugas Aktif"

#  Scenario: Verify history status is active
    When user click history task
    Then user verify task status is "Tugas Aktif" and back to manage task page

#  Scenario: Search active task
    When user search task contract on list active task
    And user click the search result
    Then user verify search result is task contract

#  Scenario: Move Task to Next Stage
    When user move task to next stage
    And user fill activity contact form
    Then user verify stage name is "Tahap 2: Telah Dihubungi"

#  Scenario: Verify history status is contract sent
    When user click history task
    Then user verify task status is "Telah Dihubungi"

#  Scenario: Move task to Task List(Backlog)
    When user back to previous page
    When user move task to active task stage
    Then user verify funnel name is "Tahap 1: Tugas Aktif"

    #  Scenario: Move task to Task List(Backlog)
#    When user back to previous page
    When user move active task to task list stage
    Then user verify funnel name is "Daftar Tugas"

#  Scenario: Verify hyperlink contract on detail task
    When user click contract detail
    Then system display hyperlink contract detail