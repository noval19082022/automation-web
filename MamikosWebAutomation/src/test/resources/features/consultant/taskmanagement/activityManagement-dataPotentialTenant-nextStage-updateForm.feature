@regression @consultant @crm-staging @activate-task-potential-tenant

Feature: Activity Management Manage Data Type Potential Tenant - Move Task to Next Stage - Update Form

  Background: Move Active Task to Task List if Task Have Been Active Before
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task potential tenant on list active task
    And user move task to task list

  Scenario: Activate Task Data Potential Tenant from Detail and Move Task to Next Stage
    Given user navigates to "consultant /"
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Tugas"
    And user activate task potential tenant
    Then user verify stage name is "Tahap 1: Tugas Aktif"

#  Scenario: Check history status is active
    When user click history task
    Then user verify task status is "Tugas Aktif" and back to manage task page

#  Scenario: Search active task
    When user search task potential tenant on list active task
     And user click the search result
    Then user verify search result is task potential tenant

#  Scenario: Move Task to Next Stage
    When user move task to next stage
    And user fill form has been contacted for potential tenant
    Then user verify data form has been contacted for potential tenant
    And user verify stage name is "Tahap 2: Telah Dihubungi"

#  Scenario: Want to update the form and cancel
    When user want to update the form and cancel
    Then user verify stage name is "Tahap 2: Telah Dihubungi"

#  Scenario: Update Data Form that Have Been Submit
    When user update form has been contacted for potential tenant
    Then user verify updated data form has been contacted for potential tenant
    And user verify stage name is "Tahap 2: Telah Dihubungi"

#  Scenario: Verify history status is been contacted
    When user click history task
    Then user verify task status is "Telah Dihubungi"

#  Scenario: Move task to Task List(Backlog)
    When user back to previous page
    When user move task to active task stage
    Then user verify funnel name is "Tahap 1: Tugas Aktif"

#  Scenario: Move task to Task List(Backlog)
    When user move active task to task list stage
    Then user verify funnel name is "Daftar Tugas"
