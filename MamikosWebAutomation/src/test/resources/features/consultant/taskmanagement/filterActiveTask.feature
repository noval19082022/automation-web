@regression @consultant @crm-staging @crm-prod

Feature: Filter Active Task

  Scenario Outline: Filter active task with data type is "<dataType>"
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Tugas"
    And user filter active task is "<dataType>"
    Then system display active task with data type "<result>"

    Examples:
      | dataType                | result                  |
      | Data Kontrak            | Data Kontrak            |
      | Data Properti           | Data Properti           |
      | Data Penyewa            | Data DBET               |
      | Data Properti Potensial | Data Properti Potensial |
      | Data Owner Potensial    | Data Owner Potensial    |

