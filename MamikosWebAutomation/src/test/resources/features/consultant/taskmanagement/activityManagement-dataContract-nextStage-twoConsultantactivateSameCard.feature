@regression @consultant @crm-staging @activate-task-property

Feature: Activity Management Manage Data Type Property - Two User as Consultant Activate Same Card

  Scenario: Move Active Task to Task List if Task Have Been Active Before and First Consultant Activate Task
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task contract on list active task
    And user move task to task list

#  Scenario: First Consultant Activate Task Data Contract From Detail
    Given user navigates to "consultant /"
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Tugas"
    And user activate task contract
    Then user verify stage name is "Tahap 1: Tugas Aktif"

#  Scenario: Verify history status is active
    When user click history task
    Then user verify task status is "Tugas Aktif" and back to manage task page

#  Scenario: Search active task
    When user search task contract on list active task
    And user click the search result
    Then user verify search result is task contract

  Scenario: Move Active Task to Task List if Task Have Been Active Before and Second Consultant Activate The Same Task
    Given user navigates to "consultant /"
    When user "consultant3" login as a consultant
    And user access to menu "Kelola Tugas"
    And user search task contract on list active task
    And user move task to task list

#  Scenario: Second Consultant Activate Task Data Contract From Detail
    Given user navigates to "consultant /"
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Tugas"
    And user activate task contract
    Then user verify stage name is "Tahap 1: Tugas Aktif"

#  Scenario: Verify history status is active
    When user click history task
    Then user verify task status is "Tugas Aktif" and back to manage task page

#  Scenario: Search active task
    When user search task contract on list active task
    And user click the search result
    Then user verify search result is task contract