@regression @consultant @crm-staging @consultant-notes

Feature: Consultant Notes Data Booking

  Background: Deleting existing booking data in admin if exist
    Given user navigates to "backoffice"
    And user login as a consultant via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by Kost name "Griya Tobelo"
    And user click on Cancel the Contract Button from action column

#  Scenario: Check at least 1 room available to book
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Properti"
    And user search property "Tobelo"
    And user choose property "Griya Tobelo"
    And user click on update kamar dan harga button
    And user click update ketersediaan kamar button
    And user check at least 1 room available

#  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "tenant CRM"
    And user navigates to "mamikos /user/booking/"
    And user cancel booking

#  Scenario: Create booking from tenant CRM
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "Griya Tobelo" and select matching result to go to kos details page
    And user clicks on Booking button on Kost details page
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Edit Notes with category remarks and remarks
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    When user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "CRM"
    And user set filter kost type by "All Testing"
    And user apply filter
    And user edit consultant notes
    And user set remarks to "Akan survei"
    And user set notes to "Besok lusa survei"
    And user save notes
    Then remarks set to "Akan survei"
    And note set to "Besok lusa survei"

#  Scenario: Updated By Remarks and Notes show in List
    And updated by tag and remarks set to "Admin Automation CRM"

#  Scenario: Edit Notes with category remarks only
    And user edit consultant notes
    And user set remarks to "Akan bayar"
    And user save notes
    Then remarks set to "Akan bayar"

#  Scenario: History Consultant Notes
    And user go to detail booking
    Then history notes row "1" is "will_pay" "Besok lusa survei"
    And history notes row "2" is "survey" "Besok lusa survei"