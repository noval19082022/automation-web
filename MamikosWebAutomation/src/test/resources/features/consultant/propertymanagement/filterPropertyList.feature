@regression @consultant @crm-staging @crm-prod

Feature: Filter property list

  Scenario Outline: Filter property list with kost level "<kost level>"
    Given user navigates to "consultant /"
    When user "consultant2" login as a consultant
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Properti"
    And user filter property by kost level "<kost level>"
    Then system display property with kost level "<kost level>"

    Examples:
      | kost level                   |
      | Mamikos Goldplus 1 PROMO     |
      | Mamikos Goldplus 2 PROMO     |
      | Mamikos Goldplus 3 PROMO     |