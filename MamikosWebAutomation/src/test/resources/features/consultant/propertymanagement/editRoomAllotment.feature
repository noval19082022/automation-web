@regression @consultant @crm-staging @crm-prod @room-allotment

Feature: Edit Room Allotment

  Scenario: Deleting existing booking data in admin if exist
    Given user navigates to "backoffice"
    And user login as a consultant via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by Kost name "Kost Tobelo Asri Village"
    And user click on Cancel the Contract Button from action column

  Scenario: Succes Edit Room
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Properti"
    And user search property "Tobelo"
    And user choose consultant property
    And user click on update kamar dan harga button
    And user click update room allotment button
    And user update room name and room floor on room allotment page
    And user back to previous page
    And user click update room allotment button
    Then user verify room name and room floor updated
    And user revert room name and floor name to default
    And user back to previous page
    And user click update room allotment button
    Then user verify room name and floor name set to default

  Scenario: Negative case edit room
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Properti"
    And user search property "Tobelo"
    And user choose consultant property
    And user click on update kamar dan harga button
    And user click update room allotment button

#  Scenario: input room name more than 50 character
    When user set room name more than maximum character
    Then user verify there is error message maximum character

#  Scenario: input room name with existing room name
    When user set room name with existing room name
    Then user verify there is error message duplicate room name

#  Scenario: left blank room name
    When user set room name blank
    Then user verify there is error message room name can not blank

#  Scenario: disagree to change page after edit without saving
    When user back to previous page
    Then user verify still on room allotment page

#  Scenario: agree to change page after edit without saving
    When user back to previous page
    Then user verify update kost price page display