
Feature: Update Property Detail

  Scenario: Update Property Detail
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    Then system display label "Konsultan Mamikos"

#  Scenario: Verify current property details
    When user access to menu "Kelola Properti"
    And user search property using keyword property name
    Then system display property name and verification status is "Verifikasi Berhasil"
    And system display property details

#  Scenario: Verify hyperlink property
    When user click property hyperlink
    Then system display hyperlink property detail page for booking

#  Scenario: Text box full address fill less than 10 character
    When user click update kos detail button
    And user input "XXX" to field  full address
    Then system display error message minimum character on field full address
    And system display error message and next button is disable

#  Scenario: Blank text box full address
    When user input "" to field  full address
    Then system display error message full address must be fill
    And system display error message and next button is disable

#  Scenario: Click button cancel on page update property
    When user click cancel button on page update property
    Then system display property details

#    Scenario: Click back button on device
      When user click update kos detail button
      And user click back button on device
      Then system display property details

#  Scenario: Text box full address fill less than 10 character
    When user click update kos detail button
    And user click next button
    And user input "XXX" to field  property name
    Then system display error message minimum character on field property name
    And system display error message and next button is disable

##  Scenario: Blank text box property name
#      And user input "" to field  property name
#      Then system display error message property name must be fill
#      And system display error message and next button is disable

#  Scenario: Input alphabet on room size
    When user revert property name
    And user input "YxY" to field room size
    Then system display error message please fill room size appropriate format
    And system display error message and next button is disable

#  Scenario: Input total room with 0
    When select room size is "4 x 4"
    And user input "0" to field total room
    Then system display error message total room must be greater than zero
    And system display error message and next button is disable

#  Scenario: Input empty room greater than total room
    When user revert total room
    And user input empty room more than total room
    Then system display error message empty room cannot more than total room
    And system display next button is disable

#  Scenario: Input empty room equal total room
    When user input empty room equal total room
    Then system display next button is enable

#  Scenario: Input empty room with 0
    When user input "0" to field empty room
    Then system display next button is enable