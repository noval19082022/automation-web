@regression @consultant @property-management @update-property-price @crm-staging @crm-prod

  Feature: Update Property Price

    Scenario Outline: Update Property Price
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Properti"
      And user search property "Tobelo"
      And user choose consultant property
      And user click on update kamar dan harga button
      And user set empty price to field "<rentType>"
      Then user verify system display price in field "<rentType>" is "Rp 0"

      When user set harga "<rentType>" using alphabet
      Then user verify system display price in field "<rentType>" is "Rp 0"

      When user set harga "<rentType>" to "10000"
      And user click on update kamar dan harga button
      Then user verify system display price in field "<rentType>" is "Rp 10.000"

      When user revert harga "<rentType>"
      Then user verify field "<rentType>" display default price

      Examples:
      |rentType   |
      |Perhari    |
      |Perminggu  |
      |Perbulan   |
      |Per 3 bulan|
      |Per 6 bulan|
      |Pertahun   |
