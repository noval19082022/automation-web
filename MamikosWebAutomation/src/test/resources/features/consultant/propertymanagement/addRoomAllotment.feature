@regression @consultant @crm-staging @crm-prod @room-allotment

Feature: Add Room Allotment

  Background: Delete room if room have been existing
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Properti"
    And user search property "Tobelo"
    And user choose consultant property
    And user click on update kamar dan harga button
    And user click update room allotment button
    And delete room if room have been existing

  Scenario: Success add new room
    When user add new room
    Then user verify new room display on room list

#  Scenario: Mark room occupied
    When user mark room occupied
    Then user verify update room occupied success

#  Scenario: Delete room
    When user delete room
    Then user verify room not found on room list

