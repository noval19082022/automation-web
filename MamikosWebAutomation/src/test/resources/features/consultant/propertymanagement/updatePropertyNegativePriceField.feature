
Feature: Update Property Detail - Negative case for price

  Scenario: Update Property Detail - Negative case for field price
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    And user access to menu "Kelola Properti"
    And user search property using keyword property name
    Then system display property name and verification status is "Verifikasi Berhasil"
    When user click update kos detail button
    And user click next button

##  Scenario: Blank field monthly
#    When user input "" to field monthly price
#    Then system display error message monthly price must be filled
#    And system display error message and next button is disable
#
##  Scenario: Input alphabet on field monthly
#    When user input "XXX" to field monthly price
#    Then system display error message monthly price must be filled
#    And system display error message and next button is disable

#  Scenario: Input price less than 9.999
    When user input "9999" to field monthly price
    Then system display error message monthly price minimum
    And system display error message and next button is disable

##  Scenario: Blank field daily
#    When user input "" to field daily price
#    Then system display error message daily price must be filled
#    And system display error message and next button is disable
#
##  Scenario: Input alphabet on field daily
#    When user input "XXX" to field daily price
#    Then system display error message daily price must be filled
#    And system display error message and next button is disable

#  Scenario: Input price less than 9.999
    When user input "9999" to field daily price
    Then system display error message daily price minimum
    And system display error message and next button is disable

##  Scenario: Blank field weekly
#    When user input "" to field weekly price
#    Then system display error message weekly price must be filled
#    And system display error message and next button is disable
#
##  Scenario: Input alphabet on field weekly
#    When user input "XXX" to field weekly price
#    Then system display error message weekly price must be filled
#    And system display error message and next button is disable

#  Scenario: Input price less than 9.999
    When user input "9999" to field weekly price
    Then system display error message weekly price minimum
    And system display error message and next button is disable

##  Scenario: Blank field quarterly
#    When user input "" to field quarterly price
#    Then system display error message quarterly price must be filled
#    And system display error message and next button is disable
#
##  Scenario: Input alphabet on field quarterly
#    When user input "XXX" to field quarterly price
#    Then system display error message quarterly price must be filled
#    And system display error message and next button is disable

#  Scenario: Input price less than 9.999
    When user input "9999" to field quarterly price
    Then system display error message quarterly price minimum
    And system display error message and next button is disable

##  Scenario: Blank field six monthly
#    When user input "" to field six monthly price
#    Then system display error message six monthly price must be filled
#    And system display error message and next button is disable
#
##  Scenario: Input alphabet on field six monthly
#    When user input "XXX" to field six monthly price
#    Then system display error message six monthly price must be filled
#    And system display error message and next button is disable

#  Scenario: Input price less than 9.999
    When user input "9999" to field six monthly price
    Then system display error message six monthly price minimum
    And system display error message and next button is disable

##  Scenario: Blank field annually
#    When user input "" to field annually price
#    Then system display error message annually price must be filled
#    And system display error message and next button is disable
#
##  Scenario: Input alphabet on field annually
#    When user input "XXX" to field annually price
#    Then system display error message annually price must be filled
#    And system display error message and next button is disable

#  Scenario: Input price less than 9.999
    When user input "9999" to field annually price
    Then system display error message annually price minimum
    And system display error message and next button is disable