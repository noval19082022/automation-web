@regression @consultant @crm-staging @crm-prod

Feature: Property Management - Shorting

  Scenario: Short by Room Availability
    Given user navigates to "consultant /"
    When user "consultant1" login as a consultant
    Then system display label "Konsultan Mamikos"
    When user access to menu "Kelola Properti"
    And user short property by "Update terbaru"
    Then system short property by room availability

#  Scenario: Short by Empty Room
    And user short property by "Kamar kosong terbanyak"
    Then system short property by most empty room