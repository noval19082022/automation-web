@regression @consultant @crm-staging @crm-prod @kost-tags-data-booking

  Feature: Kost Tags in Data Booking

    Background: Deleting existing booking data in admin if exist
      Given user navigates to "backoffice"
      And user login as a consultant via credentials
      And user click on Search Contract Menu form left bar
      Then user Navigate "Search Contract" page
      And user search by Kost name "Griya Tobelo"
      And user click on Cancel the Contract Button from action column

#  Scenario: Check at least 1 room available to book
      Given user navigates to "consultant /"
      When user "consultant1" login as a consultant
      And user access to menu "Kelola Properti"
      And user search property "Tobelo"
      And user choose property "Griya Tobelo"
      And user click on update kamar dan harga button
      And user click update ketersediaan kamar button
      And user check at least 1 room available

#    Scenario: Cancel booking if tenant have booking
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "tenant CRM"
      And user navigates to "mamikos /user/booking/"
      And user cancel booking

#    Scenario: Create booking from tenant CRM
      Given user navigates to "mamikos /"
      When user clicks search bar
      And I search property with name "Griya Tobelo" and select matching result to go to kos details page
      And user clicks on Booking button on Kost details page
      And user selects T&C checkbox and clicks on Book button
      Then system display successfully booking

      Scenario: Kost tags on data booking
        Given user navigates to "mamikos admin"
        And user logs in to Mamikos Admin via credentials as "admin consultant"
        When user access to data booking menu
        And user show filter data booking
        And user search data booking using tenant phone "CRM"
        And user set filter kost type by "All Testing"
        And user apply filter
        Then data booking contains all kost tags