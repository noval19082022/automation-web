@regression @sanjunipero

Feature: Sanjunipero

  @createNewParent
  Scenario: Create new parent
    Given user navigates to "mamikos admin"
    When user login  as a Admin bangkrupux via credentials
    And user access menu sanjunipero
    And user choose parent
    And user click add new parent
    And user fills "Automation Slug", "All_goldplus", "weekly", "Automation Tittle Tag", "Automation Tittle Header", "Automation Subtittle Header", "Akses 24 jam", "Automation FAQ", "Automation FAQ Answer"
    And user click checkbox Active Sanjunipero
    And user click save button Sanjunipero
    Then user redirected to "sanjunipero"

  @slugNameAlreadyExist
  Scenario: Slug name already exist
    Given user navigates to "mamikos admin"
    When user login  as a Admin bangkrupux via credentials
    And user access menu sanjunipero
    And user choose parent
    And user click add new parent
    And user fills mandatory already exist "Automation Slug", "All_goldplus", "weekly", "Automation Tittle Tag", "Automation Tittle Header", "Automation Subtittle Header", "Akses 24 jam", "Automation FAQ", "Automation FAQ Answer"
    And user click checkbox Active Sanjunipero
    And user click save button Sanjunipero
    And user check slug name already exist
    Then user redirected to "sanjunipero"

    @checkFilterGender
    Scenario: Check filter gender
      Given user navigates to "mamikos admin"
      When user login  as a Admin bangkrupux via credentials
      And user access menu sanjunipero
      And user choose parent
      And user click preview on action
      And user click semua tipe kost
      Then user view three gender

  @checkRoomList
  Scenario: Check room list
    Given user navigates to "mamikos admin"
    When user login  as a Admin bangkrupux via credentials
    And user access menu sanjunipero
    And user choose parent
    And user click preview on action
    And user click semua tipe kost
    Then user view room list

    @checkFilterDataKost
    Scenario: View filter data kost
      Given user navigates to "mamikos admin"
      When user login  as a Admin bangkrupux via credentials
      And user access menu sanjunipero
      And user choose parent
      And user click preview on action
      Then user view filter data kost on landing page

    @deactiveTypePage
    Scenario: View deactive type page
      Given user navigates to "mamikos admin"
      When user login  as a Admin bangkrupux via credentials
      And user access menu sanjunipero
      And user choose parent
      And user click deactive on action
      Then user view deactive type page

    @enableTypePage
    Scenario: View enable type page
      Given user navigates to "mamikos admin"
      When user login  as a Admin bangkrupux via credentials
      And user access menu sanjunipero
      And user choose parent
      And user click enable on action
      Then user view enable type page