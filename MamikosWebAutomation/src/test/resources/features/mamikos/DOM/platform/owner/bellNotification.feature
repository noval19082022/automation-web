@regression @bellnotification

  Feature: Bell Notification
    @seeAllNotificationOwner
    Scenario: Bell Notification - See all notification clicked
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user fills out owner login as "Owner No Listing" and click on Enter button
      When user click notification owner button
      And user click on see more notification
      Then user redirected to "notification page"