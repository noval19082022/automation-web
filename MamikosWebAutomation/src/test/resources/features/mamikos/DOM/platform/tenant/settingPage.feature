@regression @settingPage

  Feature: Tenant - Setting Page

    @changePasswordHidden
    Scenario: Setting Page - Change password hidden for social
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      When user logs in as Tenant via Facebook credentails as "master"
      And user click on the tenant profile
      And user click profile dropdown button
      Then user verify setting menu is hidden for social