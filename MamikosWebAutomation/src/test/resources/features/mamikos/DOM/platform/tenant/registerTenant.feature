@regression @tenant @register-tenant @essentiaTest

  Feature: Register Tenant
    @navigateToRegisterPageTenant
    Scenario: Register Tenant - Navigate To Register Page
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      Then user redirected to "/register-pencari?source=homepage"

    @noInputDataTenant
    Scenario: Register Tenant - No Input Data
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register " ", " ", " ", " "
      Then user verify error messages
      | Masukkan nama lengkap.    |
      | Masukkan nomor handphone. |
      | Masukkan alamat email.    |
      | Masukkan password.        |

    @blankNameTenant
    Scenario: Register Tenant - Blank name
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register " ", "08210391239921", "at@test.com", "qwerty123"
      Then user verify error messages
        | Masukkan nama lengkap. |

    @wrongNameTenant
    Scenario Outline: Register Tenant - Input name with symbol/number & Input name less than 3 char
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register "<Name>", "08210391239921", "at@test.com", "qwerty123"
      Then user verify error messages
      | <Error Message> |
      | <Error Message> |
      Examples:
      | Name       | Error Message              |
      | !@#$%3212  | Masukkan karakter alfabet. |
      | rh         | Minimal 3 karakter.        |

    @noInputPhone
    Scenario: Register Tenant - No input phone number
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register "Rheza Haryo Hanggara", " ", "at@test.com", "qwerty123"
      Then user verify error messages
        | Masukkan nomor handphone. |

    @phoneNumberNotUsing08AsPrefix
    Scenario: Register Tenant - Input phone number not using 08 as prefix
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register "Rheza Haryo Hanggara", "666", "at@test.com", "qwerty123"
      Then user verify error messages
        | Nomor handphone harus diawali dengan 08. |

    @nameMoreThan20Characters
    Scenario: Register Tenant - Name more than 20 characters
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register "Rheza Haryo Hanggara Aye Aye", "08210391239921", "at@test.com", "qwerty123"
      Then user verify name more than 20 characters

    @phoneLessThan8Char
    Scenario: Register Tenant - Phone less than 8 characters
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register "Rheza Haryo Hanggara Aye Aye", "0821", "at@test.com", "qwerty123"
      Then user verify error messages
        | Nomor handphone kurang dari 8 karakter. |

    @phoneMoreThan14Char
    Scenario: Register Tenant - Phone more than 14 characters
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register "Rheza Haryo Hanggara Aye Aye", "081239182938123", "at@test.com", "qwerty123"
      Then user verify error messages
        | Nomor handphone lebih dari 14 karakter. |

    @noInputPassword
    Scenario: Register Tenant - No input password
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "at@test.com", " "
      Then user verify error messages
        | Masukkan password. |

    @passwordLessThan8Char
    Scenario: Register Tenant - Input password less than 8 characters
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "at@test.com", "asd"
      Then user verify error messages
        | Password kurang dari 8 karakter |

    @passwordMoreThan8
    Scenario Outline: Register Tenant - Input password more than 8 characters & Input password not using numeric and symbols
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user clicks on Register as Tenant button
      And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "at@test.com", "<Password>"
      Then user verify password more than 8 characters
      Examples:
        | Password   |
        | qwerty123  |
        | 12345!@#$% |

      @showPasswordInputTenant
      Scenario: Register Tenant - Check eye icon
        Given user navigates to "mamikos /"
        When user clicks on Enter button
        And user clicks on Register as Tenant button
        And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "at@test.com", "qwerty123"
        And user click on show password button
        Then user verify password is equal or more than 8 characters

      @noInputEmail
      Scenario: Register Tenant - No input email
        Given user navigates to "mamikos /"
        When user clicks on Enter button
        And user clicks on Register as Tenant button
        And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", " ", "qwerty123"
        Then user verify error messages
          | Masukkan alamat email. |

      @wrongFormatEmail
      Scenario Outline: Register Tenant - Input email with wrong format
        Given user navigates to "mamikos /"
        When user clicks on Enter button
        And user clicks on Register as Tenant button
        And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "<Email>", "qwerty123"
        Then user verify error messages
          | <Error Message> |
          | <Error Message> |
        Examples:
        | Email             | Error Message |
        | asdasd.com        | Gunakan format email seperti: mami@mamikos.com |
        | draft@xyz.com.net | Penulisan alamat email salah.                  |

      @inputRegisteredEmail
      Scenario Outline: Register Tenant - Input registered email (Owner & Tenant)
        Given user navigates to "mamikos /"
        When user clicks on Enter button
        And user clicks on Register as Tenant button
        And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "<Email>", "qwerty123"
        Then user verify error messages
          | Alamat email ini sudah digunakan untuk verifikasi di akun lain. |
        Examples:
          | Email             |
          | rheza@mamikos.com |
          | rheza@rrr.com     |