@regression @loginTenant @essentiaTest
  Feature: Tenant - Login
    @loginByFB
    Scenario: Login - By Facebook
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user logs in as Tenant via Facebook credentails as "master"
      Then user redirected to "/#_=_"
      And user tenant profile picture is shown

#    @loginByGmail (if run via selenium browser can't login by gmail because of security)
#    Scenario: Login - By Gmail
#      Given user navigates to "mamikos /"
#      When user clicks on Enter button
#      And user logs in as Tenant via Gmail credentails as "platform"
#      Then user redirected to "/#_=_"
#      And user tenant profile picture is shown

    @noFillPassword
    Scenario: Login - No fill password
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as invalid Tenant via phone number as "TA No Fill Password"
      Then user verify login error messages "Password harus diisi."

    @wrongNumber
    Scenario: Login - Wrong number
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "TA Wrong Number"
      Then user verify login error messages "Nomor dan password tidak sesuai"

    @fromSBMPTNPage
     Scenario: Login - From SBMPTN Page
        Given user navigates to "SBMPTN page"
        When user clicks on Enter button
        Then user verify pop up "Masuk ke Mamikos" "Saya ingin masuk sebagai"

    @loginTenantWithNewPassword
    Scenario: login tenant with new password
      Given user navigates to "mamikos /"
      And user clicks on Enter button
      When user login in as Tenant via phone number as "UG Tenant"
      And user click profile on header
      And user click profile dropdown button
      And user clicks on pengaturan button
      And user clicks on ubah button
      And user fills password lama "qwerty123"
      And user fills password baru "qwerty111"
      And user fills password confirm "qwerty111"
      And user clicks on Simpan button ubah password
      And user clicks on Enter button
      And user login in as Tenant via phone number as "UG Tenant2"
      And user click profile on header
      And user click profile dropdown button
      And user clicks on pengaturan button
      And user clicks on ubah button
      And user fills password lama "qwerty111"
      And user fills password baru "qwerty123"
      And user fills password confirm "qwerty123"
      And user clicks on Simpan button ubah password
      Then user auto logout and redirect to homepage "Mau cari kos?"

    @fromTopNavbar
    Scenario: Pop up login - From Top Navbar
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      Then user verify pop up "Masuk ke Mamikos" "Saya ingin masuk sebagai"

    @popUpClose
    Scenario: Pop up login - Pop up close
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user click close on pop up login
      Then user verify pop up "Masuk ke Mamikos" "Saya ingin masuk sebagai" are not appeared

    @fromListingDetailPageClickFavorite
    Scenario: Pop up login - Click Favorite
      Given user navigates to "mamikos /"
      When user clicks search bar
      And I search property with name "loginfromhomepage" and click one of results "searchkey"
      And User click BBK pop up
      And User click a kost with tag Book Now
      And I should reached kos detail page
      And I click on favourite button with not login condition
      Then verify popup login displayed

    @fromListingDetailPageClickMaps
    Scenario: Pop up login - Click Maps
      Given user navigates to "mamikos /"
      When user clicks search bar
      And I search property with name "loginfromhomepage" and click one of results "searchkey"
      And User click BBK pop up
      And User click a kost with tag Book Now
      And I should reached kos detail page
      And user scroll to view maps
      And user click view maps
      And verify popup login displayed
      When I should reached kos report section
      And I click button report kos
      Then verify popup login displayed