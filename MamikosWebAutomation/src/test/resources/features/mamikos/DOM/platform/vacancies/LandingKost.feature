@regression @landingKos

Feature: Landing Kos - Popular city Kos

  @searchAnotherCategory
  Scenario: Verify filter category
    Given user navigates to "mamikos /"
    And user clicks on Jogja city button on popular area section
    Then user verify filter another category
      | Semua Tipe Kos  |
      | Bulanan         |
      | Harga           |
      | Fasilitas       |
      | Aturan Kos      |

  @searchKostType
  Scenario: Verify filter kost type option
    Given user navigates to "mamikos /"
    And user clicks on Jogja city button on popular area section
    When user clicks on filter kost type
    Then user verify kost type display option
      | Putra   |
      | Putri   |
      | Campur  |

  @searchTimePeriod
  Scenario: Verify filter time option
    Given user navigates to "mamikos /"
    And user clicks on Jogja city button on popular area section
    When user clicks on time period filter
    Then user verify filter by time period display option
      | Mingguan    |
      | Bulanan     |
      | Per 3 Bulan |
      | Per 3 Bulan |
      | Tahunan     |