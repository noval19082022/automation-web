@regression @addNewProperty

  Feature: Owner Choose Ads - Add New Property

    @chooseAddNewKos
    Scenario: Add New Property - Choose add new kos
      Given user navigates to "mamikos /"
      And user clicks on Enter button
      When user fills out owner login as "owner empty" and click on Enter button
      And user click on my property menu
      When user click on kos menu
      And user clicks on add new property ads
      And user clicks on add data on pop up
      Then user redirected to "add data kos"