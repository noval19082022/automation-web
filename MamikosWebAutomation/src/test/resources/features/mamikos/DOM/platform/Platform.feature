@DOM3
Feature: [Test-Execution][DOM] Web - Platform

	@TEST_DOM-400 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Positive Case Tenant Filter Apartment by Time Period
		Given user navigates to "mamikos /daftar/apartemen-di-jakarta"
		When user filter apartment by "time period" is "Harian"
		Then system displays the 5 highest apartment lists by "time period" is "hari"
		
		#  Scenario: Positive case tenant filter apartment by time period "Mingguan"
		When user filter apartment by "time period" is "Mingguan"
		Then system displays the 5 highest apartment lists by "time period" is "minggu"
		
		#  Scenario: Positive case tenant filter apartment by time period "Bulanan"
		When user filter apartment by "time period" is "Bulanan"
		Then system displays the 5 highest apartment lists by "time period" is "bulan"
		
		#  Scenario: Positive case tenant filter apartment by time period "Tahunan"
		When user filter apartment by "time period" is "Tahunan"
		Then system displays the 5 highest apartment lists by "time period" is "tahun"
	@TEST_DOM-398 @TESTSET_MT-1726 @TESTSET_UG-6247 @TESTSET_PF-1952 @TESTSET_PF-1400 @Automated @DOM3 @web-covered
	Scenario: [Test][Admin][SanJunipero] Create New Parent Using Virtual Tour, Allgoldplus. and Mami Checker Kost Type 
		Given user navigates to "mamikos admin"
		When user login  as a Admin bangkrupux via credentials
		And user access menu sanjunipero
		And user choose parent
		And user click add new parent
		And user fills "Automation Slug", "All_goldplus", "weekly", "Automation Tittle Tag", "Automation Tittle Header", "Automation Subtittle Header", "Akses 24 jam", "Automation FAQ", "Automation FAQ Answer"
		And user click checkbox Active Sanjunipero
		And user click save button Sanjunipero
		Then user redirected to "sanjunipero"
	@TEST_DOM-399 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Positive case tenant search apartment by Area
		Given user navigates to "mamikos /"
		When user choose "Apartemen" on dropdown list ads search
		Then user redirected to "/apartemen"
		When user select "Bandung" on filter Kota dan Area
		Then user see same result will appear on the list
		      |Sumur Bandung  |
		      |Sumur Bandung  |
		      |Sumur Bandung  |
		      |Sumur Bandung  |
		      |Lengkong       |
		      |Lengkong       |
		      |Lengkong       |
		      |Sumur Bandung  |
	@TEST_DOM-397 @Automated @DOM3 @web-covered
	Scenario: [Web][Landing Kos][Popular city] Search Another Category
		Given user navigates to "mamikos /"
		And user clicks on Jogja city button on popular area section
		Then user see look around list kos jogja
#		Then user verify filter another category
#		  | Semua Tipe Kos  |
#		  | Bulanan         |
#		  | Harga           |
#		  | Fasilitas       |
#		  | Aturan Kos      |
	@TEST_DOM-396 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][FB - Tennat login page]Login with FB
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user logs in as Tenant via Facebook credentails as "master"
		      Then user redirected to "/#_=_"
		      And user tenant profile picture is shown
	@TEST_DOM-395 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Positive Case Tenant Filter Apartment by Furniture
		Given user navigates to "mamikos /daftar/apartemen-di-jakarta"
		When user filter apartment by "furniture" is "Furnished"
		Then system displays the 5 highest apartment lists by "furniture" is "Furnished"
		
		#  Scenario: Positive case tenant search apartment filter by furniture "Semi Furnished"
		When user filter apartment by "furniture" is "Semi Furnished"
		Then system displays the 3 highest apartment lists by "furniture" is "Semi Furnished"
		
		#  Scenario: Positive case tenant search apartment filter by furniture "Not furnished"
		When user filter apartment by "furniture" is "Not furnished"
		Then system displays the 5 highest apartment lists by "furniture" is "Not Furnished"
	#already login
	@TEST_DOM-394 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Phone Number - verifikasi page]when Phone number is empty
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on ubah phone number button
		    And user empty phone number field
		    Then user get red alert error message "Nomor Handphone harus diisi." on phonenumber section
	@TEST_DOM-393 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Positive Case Tenant Sort Apartment by Price
		Given user navigates to "mamikos /daftar/apartemen-di-jakarta"
		When user filter apartment by "price" is "Acak"
		Then system displays the 5 highest apartment lists by "price" is "Acak"
		
		#  Scenario: Positive case tenant sort the list of apartments from the cheapest
		When user filter apartment by "price" is "Harga Termurah"
		Then system displays the 5 highest apartment lists by "price" is "Harga Termurah"
		
		#  Scenario: Positive case tenant sort the list of apartments most expensive
		When user filter apartment by "price" is "Harga Termahal"
		Then system displays the 5 highest apartment lists by "price" is "Harga Termahal"
	#already send OTP
	@TEST_DOM-392 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Phone Number - verifikasi page] Redirected to OTP
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on ubah phone number button
		    And user change phone number with "8239231283"
		    And user click on ubah button
		    Then user verify OTP verification message was sent "Kami telah mengirimkan Kode OTP ke nomor 08239231283"
	@TEST_DOM-391 @TESTSET_UG-6228 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Visit Page - Play Video
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And user click on video thumbnail
		Then user see pop up video player is shown and can play video
	@TEST_DOM-390 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Positive Case Tenant Filter Apartment by Unit Type
		Given user navigates to "mamikos /daftar/apartemen-di-jakarta"
		When user filter apartment by "unit type" is "1-Room Studio"
		Then system displays the 5 highest apartment lists by "unit type" is "1-Room Studio"
		
		#  Scenario: Positive case tenant search apartment filter by unit type "1 BR"
		When user filter apartment by "unit type" is "1 BR"
		Then system displays the 5 highest apartment lists by "unit type" is "1 BR"
		
		#  Scenario: Positive case tenant search apartment filter by unit type "2 BR"
		When user filter apartment by "unit type" is "2 BR"
		Then system displays the 5 highest apartment lists by "unit type" is "2 BR"
		
		#  Scenario: Positive case tenant search apartment filter by unit type "3 BR"
		When user filter apartment by "unit type" is "3 BR"
		Then system displays the 5 highest apartment lists by "unit type" is "3 BR"
		
		#  Scenario: Positive case tenant search apartment filter by unit type "4 BR"
		When user filter apartment by "unit type" is "4 BR"
		Then system displays the 1 highest apartment lists by "unit type" is "4 BR"
	#already login
	@TEST_DOM-389 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][email - verifikasi page ]when empty email
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on verifikasi button
		    And user clear email input field
		    Then user get red alert error message "Email harus diisi."
	@TEST_DOM-388 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Login as Tenant Can View Profile Picture and Option Dropdown Menu Profile
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user logs in as Tenant via Facebook credentails as "search"
		Then user redirected to "/#_=_"
		And user tenant profile picture is shown
		#  Scenario: Positive case option list on tenant profile
		When user click profile on header
		Then tenant profile dropdown display option
		  | Profil            |
		  | Riwayat Transaksi |
		  | Keluar            |
	#already login
	@TEST_DOM-387 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][email - verifikasi page ]email with wrong format
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on verifikasi button
		    And user change email with wrong format email "margaretha@mamikos"
		    Then user get red alert error message "Email harus berupa alamat surel yang benar."
	@TEST_DOM-385 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Tenant Hubungi Pengelola
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user logs in as Tenant via Facebook credentails as "master"
		And user choose "Apartemen" on dropdown list ads search
		And user click on the selected apartment to go to the apartment details
		When user click on Hubungi Pengelola button
		Then Pop up "Hubungi Apartemen" appears after click on Hubungi Pengelola
	#email already exist
	@TEST_DOM-386 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][email - verifikasi page ]Email already registered
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    When user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on verifikasi button
		    And user change email with already registered email "kharismamargaretha@gmail.com"
		    Then user get pop up error message "Email sudah digunakan oleh akun lain"
	@TEST_DOM-383 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Positive case tenant search apartment by keyword
		Given user navigates to "mamikos /"
		When user choose "Apartemen" on dropdown list ads search
		Then user redirected to "/apartemen"
		#  Scenario: Positive case tenant search by input keyword on field search apartment
		When user search apartment using keyword "Bandung"
		Then system display the 10 highest apartment lists have keyword "Bandung" on detail address
		#  Scenario: Positive case tenant click logo for redirect to home page
		When user click mamikos.com logo
		Then user redirected to "/"
	@TEST_DOM-381 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Tenant Verify Profile Dropdown
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user logs in as Tenant via Facebook credentails as "search"
		And user choose "Apartemen" on dropdown list ads search
		And user click on the selected apartment to go to the apartment details
		And user click on the tenant profile
		Then tenant profile dropdown display option
		  | Profil  |
		  | Keluar  |
	@TEST_DOM-382 @TESTSET_UG-6228 @AUTOMATED @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Footer - click App Store icon
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click "App Store" on the footer
		Then user redirected to "app store"
	@TEST_DOM-380 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Tenant Contact Apartment
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user logs in as Tenant via Facebook credentails as "master"
		And user choose "Apartemen" on dropdown list ads search
		And user click on the selected apartment to go to the apartment details
		And user click Chat in apartment detail
		Then user see chat list appear
	@TEST_DOM-379 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Tenant Verify Search Ads Dropdown
		Given user navigates to "mamikos /"
		When user choose "Apartemen" on dropdown list ads search
		And user click on the selected apartment to go to the apartment details
		And user click on the Search Ads Dropdown
		Then user verify the Search Ads Dropdown "Kos"
	#Already has account
	@TEST_DOM-378 @TESTSET_UG-4895 @TESTSET_UG-6221 @TESTSET_PF-1393 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Login]Tenant login wrong number
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
	          And user fills out tenant login with invalid "PF Wrong Number" and click on Enter button
		      Then user verify login error messages "Nomor dan password tidak sesuai"
	@TEST_DOM-377 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] Favorite an Apartment
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user logs in as Tenant via Facebook credentails as "master"
		And user choose "Apartemen" on dropdown list ads search
		And user click on the selected apartment to go to the apartment details
		When user click on favorite button
		Then user see love icon become red
		And user click Favourite tab
		Then user see favorites apartment is in first list "Apartemen"
	@TEST_DOM-376 @Automated @DOM3 @web-covered
	Scenario: [Web][Apartement] unFavorite an Apartment
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user logs in as Tenant via Facebook credentails as "master"
		And user choose "Apartemen" on dropdown list ads search
		And user click on the selected apartment to go to the apartment details
		And user click Favourite tab
		And user click first apartment in favourite list and go to apartment details
		And user click on unfavorite button
		And user go back to favourite tab
		And user refresh page
		Then user doesn't see the apartment in favourite list
	@TEST_DOM-375 @TESTSET_UG-6228 @AUTOMATED @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Footer - click Email Address
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click "email" on the footer
		Then user can see popup send email
	@TEST_DOM-374 @TESTSET_UG-6221 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Web][Login][Pop Up Login] Pop up; Close
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click close on pop up login
		Then user verify pop up "Masuk ke Mamikos" "Saya ingin masuk sebagai" are not appeared
	@TEST_DOM-373 @TESTSET_UG-6228 @AUTOMATED @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Footer - click Whatsapp number
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click "Whatsapp Number" on the footer
		Then user redirected to "whatsapp admin"
	@TEST_DOM-372 @Automated @DOM3 @web-covered
	Scenario: [Register Page][Daftar pemilik kost]No input name
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register " ", "08210391239921", "at@test.com", "qwerty123"
		      Then user verify error messages
		        | Masukkan nama lengkap. |
	@TEST_DOM-371 @Automated @DOM3 @web-covered
	Scenario: [Web][Pop up login]: Tenant - Click Maps
		Given user navigates to "mamikos /"
		      When user clicks search bar
		      And I search property with name "loginfromhomepage" and click one of results "searchkey"
		      And User click BBK pop up
		      And User click a kost with tag Book Now
		      And I should reached kos detail page
		      And user scroll to view maps
		      And user click view maps
		      And verify popup login displayed
		      When I should reached kos report section
		      And I click button report kos
		      Then verify popup login displayed
	#login with FB or Gmail
	@TEST_DOM-368 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pengaturan page  - Change password]hidden for login social
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      When user logs in as Tenant via Facebook credentails as "master"
		      And user click on the tenant profile
		      And user click profile dropdown button
		      Then user verify setting menu is hidden for social
	#Already has account
	@TEST_DOM-370 @TESTSET_UG-4895 @TESTSET_UG-6221 @TESTSET_PF-1393 @TESTSET_PF-1960 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Login]no fill password
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user login in as invalid Tenant via phone number as "TA No Fill Password"
		      Then user verify login error messages "Password harus diisi."
	@TEST_DOM-369 @Automated @DOM3 @web-covered
	Scenario: [Web][Login][Pop Up Login] Click Favorite
		Given user navigates to "mamikos /"
		      When user clicks search bar
		      And I search property with name "loginfromhomepage" and click one of results "searchkey"
		      And User click BBK pop up
		      And User click a kost with tag Book Now
		      And I should reached kos detail page
		      And I click on favourite button with not login condition
		      Then verify popup login displayed
	@TEST_DOM-367 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Register Page][Daftar pemilik kost]No input data
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register " ", " ", " ", " "
		      Then user verify error messages
		      | Masukkan nama lengkap.    |
		      | Masukkan nomor handphone. |
		      | Masukkan alamat email.    |
		      | Masukkan password.        |
	@TEST_DOM-366 @Automated @DOM3 @web-covered
	Scenario: [Login][Owner] Login From Homepage
		Given user navigates to "mamikos /room/kost-bantul-kost-campur-eksklusif-kos-danraneymu-mamitest-1?redirection_source=list%20kos%20result"
		When I should reached kos detail page
		And user scroll to view maps
		And user click view maps
		Then verify popup login displayed
	#Already has account
	@TEST_DOM-365 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][full name - Edit Profile] Change name tenant
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user click field full name
		And user delete previous full name
		And user fills fullname with number "Luna Classic"
		And user click simpan button
		Then user see pop up message profil disimpan
	#Already has account
	@TEST_DOM-364 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][full name - Edit Profile]blank name
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user click field full name
		And user delete previous full name
		Then user see message error "Nama Lengkap harus diisi."
	@TEST_DOM-363 @Automated @DOM3 @web-covered
	Scenario: [Web][login]: Tenant - Can see maps
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user clicks search bar
		And I search property with name "booking" and select matching result to go to kos details page
		And I should reached kos detail page
		And user scroll to view maps
		Then verify maps displayed

	@COSTRENT
	Scenario: [Admin][create new parent] Admin able to choose maximum of 3 combinations BIAYA SEWA filter
		Given user navigates to "mamikos admin"
		When user login  as a Admin bangkrupux via credentials
		And user access menu sanjunipero
		And user choose parent
		And user click add new parent
        And user click biaya sewa
		And user choose 3 biaya sewa "Daily" "Weekly" "Monthly"
		Then user view cost rent has been selected

	@TEST_DOM-362 @Automated @DOM3 @web-covered
	Scenario: [Test][Landing Page][SanJunipero] Check room list
		Given user navigates to "mamikos admin"
		When user login  as a Admin bangkrupux via credentials
		And user access menu sanjunipero
		And user choose parent
		And user click preview on action
		And user click semua tipe kost
		Then user view room list
	#Already has account
	@TEST_DOM-361 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][full name - Edit Profile]name with number
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user click field full name
		And user delete previous full name
		And user fills fullname with number "noval1908"
		And user click simpan button
		Then user see pop up message profil disimpan
	@TEST_DOM-360 @Automated @DOM3 @web-covered
	Scenario: [Web][Owner] Choose Add New Kos
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user fills out owner login as "owner empty" and click on Enter button
		And user click on my property menu
		When user click on kos menu
		And user clicks on add new property ads
		And user clicks on add data on pop up
		Then user redirected to "add data kos"
	@TEST_DOM-357 @TESTSET_MT-1726 @TESTSET_UG-6247 @TESTSET_PF-1952 @TESTSET_PF-1400 @Automated @DOM3 @web-covered
	Scenario: [Test][Admin][SanJunipero] User able to deactivate certain landing page
		Given user navigates to "mamikos admin"
		And user login  as a Admin bangkrupux via credentials
		When user access menu sanjunipero
		And user choose parent
		And user click deactive on action
		Then user view deactive type page
	@TEST_DOM-359 @Automated @DOM3 @web-covered
	Scenario: [Web][non login]: Login pop-up options appear
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "booking" and select matching result to go to kos details page
		And I should reached kos detail page
		And user scroll to view maps
		And user click view maps
		Then verify popup login displayed
	@TEST_DOM-358 @TESTSET_UG-6221 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Web][Login][Pop Up Login] From Listing Detail Page
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user clicks search bar
		And I search property with name "booking" and select matching result to go to kos details page
		And I should reached kos detail page
		And user scroll to view maps
		Then verify maps displayed
	#Logged in as tenant and have filled in all the personal data mandetory
	@TEST_DOM-356 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Lainnya- Edit Profile]not fill text box
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "UG Tenant"
		    And user click profile on header
		    And user click profile dropdown button
		    And user click edit profile
		    And user choose profession "mahasiswa"
		    And user click dropdown pilih universitas
		    And user fills "lainnya"
		    And user click lainnya on dropdown
		    And user fill " " on custom university input
		    Then user verify button simpan is not "disabled"
	#Non Login
	@TEST_DOM-355 @TESTSET_UG-4584 @TESTSET_UG-4894 @TESTSET_UG-6222 @TESTSET_PF-1393 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Forgot Password]Send OTP via SMS
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with tenant and click forgot password button
		And user fill their registered phone number "086476512341" and click send button
		And user click otp via sms on page "Pilih Metode Verifikasi"
		Then user verify otp form appear on page "Nomor handphone ini telah terdaftar sebagai akun pencari kos di Mamikos"
	#already login
	@TEST_DOM-354 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][DOB - Edit profile]change DOB
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "UG Tenant"
		    And user click profile on header
		    And user click profile dropdown button
		    And user click edit profile
		    And user click icon calendar
		    And user choose date of birth
		    And user click simpan button
		    Then user see pop up message profil disimpan
	#Non Login
	@TEST_DOM-353 @TESTSET_UG-4584 @TESTSET_UG-4894 @TESTSET_UG-6222 @TESTSET_PF-1393 @TESTSET_PF-1958 @TESTSET_PF-2238 @TESTSET_PF-2268 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Forgot Password] Send OTP Via WA
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with tenant and click forgot password button
		And user fill their registered phone number "089220220101" and click send button
		And user click otp via wa on page "Pilih Metode Verifikasi"
		Then user verify otp form appear on page "Nomor handphone ini telah terdaftar sebagai akun pencari kos di Mamikos"
	@TEST_DOM-349 @Automated @DOM3 @web-covered
	Scenario: [Test][Admin][SanJunipero] Slug name already exist
		Given user navigates to "mamikos admin"
		When user login  as a Admin bangkrupux via credentials
		And user access menu sanjunipero
		And user choose parent
		And user click add new parent
		And user fills mandatory already exist "Automation Slug", "All_goldplus", "weekly", "Automation Tittle Tag", "Automation Tittle Header", "Automation Subtittle Header", "Akses 24 jam", "Automation FAQ", "Automation FAQ Answer"
		And user click checkbox Active Sanjunipero
		And user click save button Sanjunipero
		And user check slug name already exist
		Then user redirected to "sanjunipero"
	#already login
	@TEST_DOM-350 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][City - Edit Profile ]Search city
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user click kota asal dropdown
		And user search city "Tangerang"
		Then user see search results for the city Tangerang
	#already login
	@TEST_DOM-351 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Gender - Edit Profile]change gender
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user click radio button laki-laki
		And user click simpan button
		Then user see pop up message profil disimpan
	#already login
	@TEST_DOM-352 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][City - Edit Profile ]change City
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user click kota asal dropdown
		And user select city kabupaten aceh barat
		And user click simpan button
		Then user see pop up message profil disimpan
	#Logged in as tenant
	@TEST_DOM-348 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Karyawan - Lainnya- Edit Profile]fill text box
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "karyawan"
		And user click dropdown pilih instansi
		And user fills "lainnya"
		And user click lainnya on dropdown
		And user fill "MM" on custom university input
		Then user verify button simpan is not "disabled"
	#already login
	@TEST_DOM-347 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Phone Number - verifikasi page]when phone number is empty
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on ubah phone number button
		    And user empty phone number field
		    Then user get red alert error message "Nomor Handphone harus diisi." on phonenumber section
	#already login
	@TEST_DOM-346 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Phone Number - verifikasi page]input number < 8 
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on ubah phone number button
		    And user change phone number with "8112069"
		    Then user get red alert error message "Nomor Handphone minimal mengandung 8 karakter." on phonenumber section
	#Register tenant
	@TEST_DOM-345 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Register]Redirect to Register page 
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      Then user redirected to "/register-pencari?source=homepage"
	@TEST_DOM-344 @TESTSET_UG-6221 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Web][Login][Pop Up Login] From Top Navbar
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      Then user verify pop up "Masuk ke Mamikos" "Saya ingin masuk sebagai"
	#Non Login
	@TEST_DOM-343 @TESTSET_UG-4584 @TESTSET_UG-4894 @TESTSET_UG-6222 @TESTSET_PF-1393 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Forgot Password]Use Invalid OTP
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with tenant and click forgot password button
		And user fill their registered phone number "089220220102" and click send button
		And user click otp via sms on page "Pilih Metode Verifikasi"
		And user input invalid OTP "1" "1" "1" "1"
		Then user verify invalid OTP message "Kode verifikasi salah." "Mohon masukkan kode verifikasi yang kami kirim."
	@TEST_DOM-342 @Automated @DOM3 @web-covered
	Scenario: [Web][Landing Kos][Popular city] Search Time Period
		Given user navigates to "mamikos /"
		And user clicks on Jogja city button on popular area section
		When user clicks on time period filter
		Then user see look around list kos jogja
#		Then user verify filter by time period display option
#		  | Mingguan    |
#		  | Bulanan     |
#		  | Per 3 Bulan |
#		  | Per 3 Bulan |
#		  | Tahunan     |
	@TEST_DOM-341 @Automated @DOM3 @web-covered
	Scenario: [Web][Landing Kos][Popular city] Search Kost Type
		Given user navigates to "mamikos /"
		And user clicks on Jogja city button on popular area section
		When user clicks on filter kost type
		Then user see look around list kos jogja
#		Then user verify kost type display option
#		  | Putra   |
#		  | Putri   |
#		  | Campur  |
	#Logged in as tenant
	@TEST_DOM-338 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Karyawan - Edit Profile]list office name
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "karyawan"
		And user click dropdown pilih instansi
		Then Dropdown will displayed max 7 list office name
	#Logged in as tenant
	@TEST_DOM-340 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Mahasiswa - Edit Profile]type office name not matched with list
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "mahasiswa"
		And user click dropdown pilih universitas
		And user fills "alkamal" in search dropdown
		Then user verify message "There is no data" in search dropdown
	#Logged in as tenant
	@TEST_DOM-339 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Lainnya- Edit Profile]Change Pekerjaan
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "UG Tenant"
		    And user click profile on header
		    And user click profile dropdown button
		    And user click edit profile
		    And user choose profession "karyawan"
		    And user click dropdown pilih instansi
		    And user fills "lainnya"
		    And user click lainnya on dropdown
		    And user fill "PT Adhi Karya (Persero) Tbk." on custom instansi input
		    And user click simpan button
		    Then user see pop up message profil disimpan
	@TEST_DOM-337 @TESTSET_UG-6228 @AUTOMATED @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Footer - click Google Play icon
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click "Google Play" on the footer
		Then user redirected to "google play"
	@TEST_DOM-336 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Karyawan - Edit Profile]type office name matched with list
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "mahasiswa"
		And user click dropdown pilih universitas
		And user fills "indonesia" in search dropdown
		Then user verify dropdown results list contains "Indonesia"
	#Logged in as tenant
	@TEST_DOM-335 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Mahasiswa - Lainnya- Edit Profile]fill text box
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "mahasiswa"
		And user click dropdown pilih universitas
		And user fills "lainnya"
		And user click lainnya on dropdown
		And user fill "A" on custom university input
		Then user verify button simpan is not "disabled"
	#phone number already exist
	@TEST_DOM-332 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Phone Number - verifikasi page]phone number already exist
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on ubah phone number button
		    And user change phone number with "898765432166"
		    And user click on ubah button
		    Then user get pop up error message "Maaf nomor sudah terdaftar"
	@TEST_DOM-331 @TESTSET_UG-4894 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Bell Notification] Bell icon - lihat semua clicked
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "Owner No Listing" and click on Enter button
		When user click notification owner button
		And user click on see more notification
		Then user redirected to "notification page"
	#already send OTP
	@TEST_DOM-334 @TESTSET_UG-4895 @TESTSET_UG-6227 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Phone Number - verifikasi page]Kirim ulang OTP
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on ubah phone number button
		    And user change phone number with "8239231283"
		    And user click on ubah button
		    And user verify OTP verification message was sent "Kami telah mengirimkan Kode OTP ke nomor 08239231283"
		    When user click on resend OTP text
		   # Then user verify OTP countdown should start on "02:"
		    Then user verify OTP verification message was sent "Kami telah mengirimkan Kode OTP ke nomor 08239231283"
	#already login
	@TEST_DOM-333 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Phone Number - verifikasi page]input number > 14
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "TA Verification"
		    And user click on the tenant profile
		    And user click profile dropdown button
		    And user click verifikasi akun menu
		    And user click on ubah phone number button
		    And user change phone number with "81120699900000"
		    And user click on ubah button
		    Then user get pop up error message "Maaf, Nomor tidak valid, silahkan gunakan nomor yang lain"
	#Logged in as tenant
	@TEST_DOM-329 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Karyawan - Edit Profile]Choose list office
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "karyawan"
		And user click dropdown pilih instansi
		And user select instansi "PT Adhi Karya (Persero) Tbk."
		And user click simpan button
		Then user see pop up message profil disimpan
	#Logged in as tenant
	@TEST_DOM-330 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Karyawan - Edit Profile]Change office name
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "karyawan"
		And user click dropdown pilih instansi
		And user select instansi "PT Adhi Karya (Persero) Tbk."
		And user click simpan button
		Then user see pop up message profil disimpan
	#already login
	@TEST_DOM-328 @TESTSET_UG-4895 @TESTSET_PF-1961 @Automated @DOM3 @web-covered
	Scenario: [Tenant][profesion - Edit Profile ]choose profesion lainnya
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "lainnya"
		And user input profession "Wiraswasta"
		And user click simpan button
		Then user see pop up message profil disimpan
	#Logged in as tenant
	@TEST_DOM-326 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Mahasiswa - Edit Profile]type university name not matched with list
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "mahasiswa"
		And user click dropdown pilih universitas
		And user fills "alkamal" in search dropdown
		Then user verify message "There is no data" in search dropdown
	#already login
	@TEST_DOM-327 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][profesion - Edit Profile ]Not choose profesion
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user no choose profession
		Then user see button simpan edit profile disable
	#Register tenant
	@TEST_DOM-325 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Nama Lengkap - Reg tenant]input name > 20
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "Rheza Haryo Hanggara Aye Aye", "08210391239921", "at@test.com", "qwerty123"
		      Then user verify name more than 20 characters
	#Logged in as tenant
	@TEST_DOM-324 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Mahasiswa - Edit Profile]type university name matched with list
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "mahasiswa"
		And user click dropdown pilih universitas
		And user fills "indonesia" in search dropdown
		Then user verify dropdown results list contains "Indonesia"
	#Logged in as tenant
	@TEST_DOM-323 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Mahasiswa - Edit Profile]Select Mahasiswa and choose universitas indonesia
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "mahasiswa"
		And user click dropdown pilih universitas
		Then user fills "indonesia" and search found
		And user choose universitas indonesia
		And user click simpan button
		Then user see pop up message profil disimpan
	@TEST_DOM-322 @TESTSET_UG-6228 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Footer - click Instagram icon
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click Instagram icon on the footer
		Then user redirected to "instagram"
	@TEST_DOM-321 @TESTSET_UG-6228 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Footer - click Twitter icon
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click Twitter icon on the footer
		Then user redirected to "twitter"
	#non login
	@TEST_DOM-320 @TESTSET_UG-4894 @TESTSET_UG-6222 @TESTSET_PF-1393 @TESTSET_PF-1958 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Forgot Password]Tenants (register via facebook) forget the password in tenant feature
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with tenant and click forgot password button
		And user fill their registered phone number "081223102002" and click send button
		Then user verify on page "Pilih Metode Verifikasi"
	@TEST_DOM-319 @TESTSET_UG-6228 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Footer - click Facebook icon
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click Facebook icon on the footer
		Then user redirected to "facebook"
	@TEST_DOM-318 @TESTSET_UG-6228 @AUTOMATED @DOM3 @web-covered
	Scenario Outline: [Web Owner][Campaign EnaknyaNgekos]: Footer - click Syarat dan Ketentuan - kebijakan privasi - corporate - blog - help - mamiads - career - tentang kami
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click "<button>" on the footer
		Then user redirected to "<url>"
		    Examples:
		      | button                      | url               |
		      | Tentang Kami                | tentang-kami      |
		      | Job Mamikos                 | career            |
		      | Promosikan Kost Anda        | mamiads           |
		      | Pusat Bantuan               | help              |
		      | Blog Mamikos                | blog              |
		      | Sewa Kos untuk Perusahaan   | corporate         |
		      | Kebijakan Privasi           | kebijakan privasi |
		      | Syarat dan Ketentuan Umum   | syarat ketentuan  |
	@TEST_DOM-316 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Promo page] User can open promo detail
		Given user navigates to "promo"
		And user see the promo title in first promo
		When user click see detail on first promo
		Then detail promo page opened with correct title
		And user see button booking now
	#Register tenant
	@TEST_DOM-317 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Password - Reg tenant]no fill password
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "at@test.com", " "
		      Then user verify error messages
		        | Masukkan password. |
	@TEST_DOM-312 @Automated @DOM3 @web-covered
	Scenario: [Test][Admin][SanJunipero] User able to enable certain landing page
		Given user navigates to "mamikos admin"
		When user login  as a Admin bangkrupux via credentials
		And user access menu sanjunipero
		And user choose parent
		And user click enable on action
		Then user view enable type page
	#Register tenant
	@TEST_DOM-315 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Nomor Handphone - Reg tenant]no fill no.HP
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "Rheza Haryo Hanggara", " ", "at@test.com", "qwerty123"
		      Then user verify error messages
		        | Masukkan nomor handphone. |
	@TEST_DOM-311 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Promo page] User can copy promo code
		Given user navigates to "promo"
		When user click SALIN on any promo
		Then promo code can be copied "ANDALANKU22"
	@TEST_DOM-314 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Promo page] Check pagination in promo page
		Given user navigates to "promo"
		When user click next page button
		Then next promo page will be opened
		When user click previous page button
		Then previous promo page will be opened
		When user click page index "2"
		Then promo page "2" will be opened
	#Register tenant
	@TEST_DOM-310 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Nomor Handphone - Reg tenant]input no.HP < 8
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "Rheza Haryo Hanggara Aye Aye", "0821", "at@test.com", "qwerty123"
		      Then user verify error messages
		        | Nomor handphone kurang dari 8 karakter. |
	#Non login
	@TEST_DOM-308 @TESTSET_PF-1393 @TESTSET_PF-1958 @Automated @DOM3 @web-covered
	Scenario Outline: [Web Tenant][Forgot Password - Phone Number] Phone Number Error Compilation
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with tenant and click forgot password button
		And user fill their unregistered phone number "<phone number>"
		Then user get error message "<error message>"
		    Examples:
		      | phone number    | error message |
		      | 0864765123419   | Masukkan nomor handphone yang terdaftar.                    |
		      | 089212312304    | Nomor HP ini sudah digunakan untuk verifikasi di akun lain. |
		      | 089876543217671 | Nomor handphone lebih dari 14 karakter.                     |
	@TEST_DOM-313 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Promo page] Check pagination in promo page
		Given user navigates to "promo"
		When user click next page button
		Then next promo page will be opened
		When user click previous page button
		Then previous promo page will be opened
		When user click page index "2"
		Then promo page "2" will be opened
	#Register tenant
	@TEST_DOM-309 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Nomor Handphone - Reg tenant]input no.HP > 14
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "Rheza Haryo Hanggara Aye Aye", "081239182938123", "at@test.com", "qwerty123"
		      Then user verify error messages
		        | Nomor handphone lebih dari 14 karakter. |
	#already login
	@TEST_DOM-307 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario Outline: [Tenant][Status - Edit profile]Change marital status
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user click on marital status dropdown
		And user select "<marital status>"
		And user click simpan button
		Then user see pop up message profil disimpan
		    Examples:
		      | marital status     |
		      | Belum Kawin        |
		      | Kawin              |
		      | Kawin Memiliki Anak|
	#New Account tenant and Access edit profile for 1st time
	@TEST_DOM-306 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Status - Edit Profile]Not choose marital status
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user click on marital status dropdown
		Then user see martial status by default is belum kawin
	@TEST_DOM-304 @TESTSET_UG-6228 @AUTOMATED @DOM3 @web-covered
	Scenario Outline: [Web Owner][Campaign EnaknyaNgekos]: Footer - click Promosikan Iklan Anda Gratis
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click "<button>" on the footer
		Then user redirected to "<url>"
		    Examples:
		      | button                      | url               |
		      | Tentang Kami                | tentang-kami      |
		      | Job Mamikos                 | career            |
		      | Promosikan Kost Anda        | mamiads           |
		      | Pusat Bantuan               | help              |
		      | Blog Mamikos                | blog              |
		      | Sewa Kos untuk Perusahaan   | corporate         |
		      | Kebijakan Privasi           | kebijakan privasi |
		      | Syarat dan Ketentuan Umum   | syarat ketentuan  |
	#already login
	@TEST_DOM-305 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Education - Edit Profile]last education
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user click on last education tenant
		And user select S1
		And user click simpan button
		Then user see pop up message profil disimpan
	#Register tenant
	@TEST_DOM-301 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario Outline: [Tenant][Nama Lengkap - Reg tenant]Input name with symbol/number & Input name less than 3 char 
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "<Name>", "08210391239921", "at@test.com", "qwerty123"
		      Then user verify error messages
		      | <Error Message> |
		      | <Error Message> |
		      Examples:
		      | Name       | Error Message              |
		      | !@#$%3212  | Masukkan karakter alfabet. |
		      | rh         | Minimal 3 karakter.        |
	@TEST_DOM-303 @TESTSET_UG-6228 @AUTOMATED @DOM3 @web-covered
	Scenario Outline: [Web Owner][Campaign EnaknyaNgekos]: Footer - click Pusat Bantuan
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click "<button>" on the footer
		Then user redirected to "<url>"
		    Examples:
		      | button                      | url               |
		      | Tentang Kami                | tentang-kami      |
		      | Job Mamikos                 | career            |
		      | Promosikan Kost Anda        | mamiads           |
		      | Pusat Bantuan               | help              |
		      | Blog Mamikos                | blog              |
		      | Sewa Kos untuk Perusahaan   | corporate         |
		      | Kebijakan Privasi           | kebijakan privasi |
		      | Syarat dan Ketentuan Umum   | syarat ketentuan  |
	@TEST_DOM-302 @TESTSET_UG-6228 @AUTOMATED @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: View Content - click Cari kos Singgahsini
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click "Singgahsini" on the footer
		Then user redirected to "https://singgahsini.id/"
	#already login
	@TEST_DOM-300 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][No.darurat - Edit Profile ]input number darurat < 8
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user input phone number darurat less than "0812123" character
		Then user see validation message error less than 8
	#Already has account
	@TEST_DOM-298 @TESTSET_PF-1393 @Automated @DOM3 @web-covered
	Scenario Outline: [WEB Tenant][Register] error message "Penulisan alamat email salah"
		Given user navigates to "mamikos /"
		        When user clicks on Enter button
		        And user clicks on Register as Tenant button
		        And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "<Email>", "qwerty123"
		        Then user verify error messages
		          | <Error Message> |
		          | <Error Message> |
		        Examples:
		        | Email             | Error Message |
		        | asdasd.com        | Gunakan format email seperti: mami@mamikos.com |
		        | draft@xyz.com.net | Penulisan alamat email salah.                  |
	#already login
	@TEST_DOM-299 @TESTSET_UG-4895 @TESTSET_UG-6226 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][No.darurat - Edit Profile ]input number darurat > 14
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user input phone number darurat more than "0812123123000014" character
		Then user see validation message error more than 14
	@TEST_DOM-297 @TESTSET_UG-6228 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Header - Scroll Page
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And scroll the page to Kenapa #EnaknyaNgekos section or more
		Then Mulai Cari Kos button is shown on the Navigation bar
	@TEST_DOM-296 @TESTSET_MT-1726 @TESTSET_UG-6247 @TESTSET_PF-1952 @TESTSET_PF-1400 @Automated @DOM3 @web-covered
	Scenario: [Test][Filter][Landing Page][SanJunipero] Check on Gender filter data kost in the landing page
		Given user navigates to "mamikos admin"
		When user login  as a Admin bangkrupux via credentials
		And user access menu sanjunipero
		And user choose parent
		And user click preview on action
		And user click semua tipe kost
		Then user view three gender
	@TEST_DOM-295 @TESTSET_MT-1726 @TESTSET_UG-6247 @TESTSET_PF-1952 @TESTSET_PF-1400 @Automated @DOM3 @web-covered
	Scenario: [Test][Filter][Landing Page][SanJunipero] Check on filter data kost in the landing page
		Given user navigates to "mamikos admin"
		And user login  as a Admin bangkrupux via credentials
		When user access menu sanjunipero
		And user choose parent
		And user click preview on action
		Then user view filter data kost on landing page
	#Already has account
	@TEST_DOM-294 @TESTSET_PF-1393 @Automated @DOM3 @web-covered
	Scenario Outline: [WEB Tenant][Register] error message "Gunakan format email seperti: mami@mamikos.com"
		Given user navigates to "mamikos /"
		        When user clicks on Enter button
		        And user clicks on Register as Tenant button
		        And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "<Email>", "qwerty123"
		        Then user verify error messages
		          | <Error Message> |
		          | <Error Message> |
		        Examples:
		        | Email             | Error Message |
		        | asdasd.com        | Gunakan format email seperti: mami@mamikos.com |
		        | draft@xyz.com.net | Penulisan alamat email salah.                  |
	@TEST_DOM-292 @TESTSET_UG-6228 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Header - click Booking Kos
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click Booking Kos button on the header
		Then user directed to menu booking "Mudah cari kostnya, Mudah bookingnya, Mudah bayarnya."
	@TEST_DOM-293 @TESTSET_UG-6228 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Header - click Fitur Unggulan
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click fitur unggulan button on the header
		Then page auto-scroll to Fitur Unggulan Promotion section "Fitur-fitur yang kamu pakai buat #EnaknyaNgekos"
	#Already has account
	@TEST_DOM-291 @TESTSET_PF-1393 @TESTSET_PF-1951 @Automated @DOM3 @web-covered
	Scenario: [WEB Tenant][Register] error message "Nomor handphone harus diawali dengan 08."
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "Rheza Haryo Hanggara", "666", "at@test.com", "qwerty123"
		      Then user verify error messages
		        | Nomor handphone harus diawali dengan 08. |
	@TEST_DOM-290 @TESTSET_UG-6228 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Header - click Produk dan Layanan
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click product dan layanan button on the header
		Then page auto-scroll to product dan layanan Promotion section "Kenapa #EnaknyaNgekos?"
	@TEST_DOM-289 @TESTSET_UG-6228 @Automated @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Header - click Promo
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click Promo button on the header
		Then page auto-scroll to Voucher Promotion section "#EnaknyaNgekos di Kos Andalan"
		And click on voucher banner
		Then user directed to detail promo page "Cari Kost Murah, Ada Promo Setiap Hari di Mamikos"
	@TEST_DOM-286 @TESTSET_UG-6222 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [WEB][Tenant Forgot Password]Resend OTP via WA
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with tenant and click forgot password button
		And user fill their registered phone number "086476512341" and click send button
		And user click otp via wa on page "Pilih Metode Verifikasi"
		Then user verify and click button resend OTP "Kirim ulang kode"
	#* Tenant already change password
	@TEST_DOM-288 @TESTSET_UG-4895 @TESTSET_UG-6221 @TESTSET_PF-1393 @TESTSET_PF-1960 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Login] Login tenant with new password 
		Given user navigates to "mamikos /"
		      And user clicks on Enter button
		      When user login in as Tenant via phone number as "UG Tenant"
		      And user click profile on header
		      And user click profile dropdown button
		      And user clicks on pengaturan button
		      And user clicks on ubah button
		      And user fills password lama "qwerty123"
		      And user fills password baru "qwerty111"
		      And user fills password confirm "qwerty111"
		      And user clicks on Simpan button ubah password
		      And user clicks on Enter button
		      And user login in as Tenant via phone number as "UG Tenant2"
		      And user click profile on header
		      And user click profile dropdown button
		      And user clicks on pengaturan button
		      And user clicks on ubah button
		      And user fills password lama "qwerty111"
		      And user fills password baru "qwerty123"
		      And user fills password confirm "qwerty123"
		      And user clicks on Simpan button ubah password
		      Then user auto logout and redirect to homepage "Mau cari kos?"
		#Already login tenant
	@TEST_DOM-271 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pengaturan page ]Ubah password valid
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user clicks on pengaturan button
		And user clicks on ubah button
		And user fills password lama "qwerty123"
		And user fills password baru "qwerty111"
		And user fills password confirm "qwerty111"
		And user clicks on Simpan button ubah password
		Then user auto logout and redirect to homepage "Mau cari kos?"
		#Already login tenant
	@TEST_DOM-384 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pengaturan page ] Restore Ubah password valid
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant2"
		And user click profile on header
		And user click profile dropdown button
		And user clicks on pengaturan button
		And user clicks on ubah button
		And user fills password lama "qwerty111"
		And user fills password baru "qwerty123"
		And user fills password confirm "qwerty123"
		And user clicks on Simpan button ubah password
		Then user auto logout and redirect to homepage "Mau cari kos?"
	#Already login tenant
	@TEST_DOM-287 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pengaturan page ]input password > 25
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "UG Tenant"
		    And user click profile on header
		    And user click profile dropdown button
		    And user clicks on pengaturan button
		    And user clicks on ubah button
		    And user fills password lama "qwerty123"
		    And user fills password baru "qwerty12345678901234567890123"
		    Then user see message error more than "Password maksimal 25 karakter"
	@TEST_DOM-285 @TESTSET_UG-6222 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [WEB][Tenant Forgot Password]Resend OTP via SMS
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with tenant and click forgot password button
		And user fill their registered phone number "086476512341" and click send button
		And user click otp via sms on page "Pilih Metode Verifikasi"
		Then user verify and click button resend OTP "Kirim ulang kode"
	#Non login
	@TEST_DOM-284 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Forgot Password] - User use number owner
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with owner and click forgot password button
		And user fill their unregistered phone number "081223102002"
		Then user get error message "Nomor HP tidak terdaftar sebagai pemilik kos."
	@TEST_DOM-282 @Automated @DOM3 @web-covered
	Scenario: [OD Revamp][Event Banner] Event banner redirection
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner empty" and click on Enter button
		And user go to event banner section
		Then banner order should be "Dari Mamikos"
		And user click on banner 1
		Then user redirected to "https://docs.google.com/forms/d/e/1FAIpQLSdGrn3lbLwSWxdb4tJ1hVJI7qi0nYW77sVXB0YsMXaA4tORKA/viewform"
	#Non login
	@TEST_DOM-283 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Forgot Password - Redirect To Forgot Password Page
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with tenant and click forgot password button
		Then user redirected to "/lupa-password-pencari"
	#Non login
	@TEST_DOM-281 @Automated @DOM3 @web-covered
	Scenario: [Web Tenant][Forgot Password - Phone Number]Error Message - Phone number is registered
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user click login with tenant and click forgot password button
		And user fill their registered phone number "086476512341" and click send button
		Then user verify on page "Pilih Metode Verifikasi"
	#Already login tenant
	@TEST_DOM-280 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pengaturan page ]input symbol/numeric
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user login in as Tenant via phone number as "UG Tenant"
		    And user click profile on header
		    And user click profile dropdown button
		    And user clicks on pengaturan button
		    And user clicks on ubah button
		    And user fills password lama "qwerty123"
		    And user fills password baru "!@#$%^&*"
		    And user fills password confirm "!@#$%^&*"
		    Then user see message error special character "Gunakan kombinasi huruf dan angka"
	@TEST_DOM-279 @Automated @DOM3 @web-covered
	Scenario: [OD Revamp][Event Banner] Event banner check content
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner empty" and click on Enter button
		And user go to event banner section
		Then banner order should be "Dari Mamikos"
	#Logged in as tenant
	@TEST_DOM-278 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Pekerjaan - Karyawan - Edit Profile] Dropdown list office name
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "UG Tenant"
		And user click profile on header
		And user click profile dropdown button
		And user click edit profile
		And user choose profession "karyawan"
		And user click dropdown pilih instansi
		Then Dropdown will displayed max 7 list office name
	@TEST_DOM-277 @Automated @DOM3 @web-covered
	Scenario: [Web][Non Login][Pop Up Login] From Listing Detail Page
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "booking" and select matching result to go to kos details page
		And I should reached kos detail page
		And user scroll to view maps
		And user click view maps
		Then verify popup login displayed
	#Register tenant
	@TEST_DOM-276 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario Outline: [Tenant][Password - Reg tenant]passowrd with symbol&numeric
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "at@test.com", "<Password>"
		      Then user verify password more than 8 characters
		      Examples:
		        | Password   |
		        | qwerty123  |
		        | 12345!@#$% |
	@TEST_DOM-274 @TESTSET_UG-6228 @AUTOMATED @DOM3 @web-covered
	Scenario: [Web Owner][Campaign EnaknyaNgekos]: Visit Page - click Mulai Cari Kos
		Given user navigates to "mamikos /enaknyangekos"
		When user is on the LP EnaknyaNgekos
		And click "Mulai Cari Kos" on the footer
		Then user redirected to "https://jambu.kerupux.com/kos/andalan"
	#Register tenant
	@TEST_DOM-275 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Password - Reg tenant]password < 8
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "at@test.com", "asd"
		      Then user verify error messages
		        | Password kurang dari 8 karakter |
	@TEST_DOM-273 @TESTSET_UG-6249 @TESTSET_PF-1400 @TESTSET_PF-1956 @Automated @DOM3 @web-covered
	Scenario: [OD Revamp][Event Banner] Event banner same order priority
		Given user navigates to "mamikos admin"
		When user login  as a Admin via credentials
		And user access menu event
		And user set banner 1 to order 1
		And user set banner 2 to order 1
		And user set banner 3 to order 1
		Then user see update banner "Success!"
	@TEST_DOM-272 @TESTSET_UG-6249 @TESTSET_PF-1400 @TESTSET_PF-1956 @Automated @DOM3 @web-covered
	Scenario: [OD Revamp][Event Banner] Event banner different order priority
		Given user navigates to "mamikos admin"
		When user login  as a Admin via credentials
		And user access menu event
		And user set banner 1 to order 1
		And user set banner 2 to order 2
		And user set banner 3 to order 3
		Then user see update banner "Success!"
	#Register tenant
	@TEST_DOM-270 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Email - Reg tenant]no fill email
		Given user navigates to "mamikos /"
		        When user clicks on Enter button
		        And user clicks on Register as Tenant button
		        And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", " ", "qwerty123"
		        Then user verify error messages
		          | Masukkan alamat email. |
	#Register tenant
	@TEST_DOM-269 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario Outline: [Tenant][Password - Reg tenant]password > 8
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user clicks on Register as Tenant button
		      And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "at@test.com", "<Password>"
		      Then user verify password more than 8 characters
		      Examples:
		        | Password   |
		        | qwerty123  |
		        | 12345!@#$% |
	#Register tenant
	@TEST_DOM-268 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][Password - Reg tenant]check eye icon
		Given user navigates to "mamikos /"
		        When user clicks on Enter button
		        And user clicks on Register as Tenant button
		        And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "at@test.com", "qwerty123"
		        And user click on show password button
		        Then user verify password is equal or more than 8 characters
	#when user open forum SBMPTN
	#page
	@TEST_DOM-267 @TESTSET_UG-4895 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario: [Tenant][SBMPTN page - menu masuk]open menu masuk
		Given user navigates to "SBMPTN page"
		When user clicks on Enter button
		Then user verify pop up "Masuk ke Mamikos" "Saya ingin masuk sebagai"
	#email sudah verify
	@TEST_DOM-266 @TESTSET_PF-1792 @Automated @DOM3 @web-covered
	Scenario Outline: [Tenant][Email - Reg tenant]input owner & tenant email 
		Given user navigates to "mamikos /"
		        When user clicks on Enter button
		        And user clicks on Register as Tenant button
		        And user fills out registration form without click register "Rheza Haryo Hanggara", "08210391239921", "<Email>", "qwerty123"
		        Then user verify error messages
		          | Alamat email ini sudah digunakan untuk verifikasi di akun lain. |
		        Examples:
		          | Email             |
		          | rheza@mamikos.com |
		          | rheza@rrr.com     |
	#Non Login
	@TEST_DOM-202 @TESTSET_UG-4584 @TESTSET_UG-4894 @TESTSET_UG-6222 @TESTSET_PF-1393 @TESTSET_PF-1958 @Automated @DOM3 @Web @web-covered
	Scenario Outline: [Web Tenant][Forgot Password]Set New Password using less than 25 character
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user click login with tenant and click forgot password button
		    And user fill their unregistered phone number "<phone number>"
		    Then user get error message "<error message>"
		    Examples:
		      | phone number    | error message |
		      | 0864765123419   | Masukkan nomor handphone yang terdaftar.                    |
		      | 089212312304    | Nomor HP ini sudah digunakan untuk verifikasi di akun lain. |
		      | 089876543217671 | Nomor handphone lebih dari 14 karakter.                     |
		      | 08128000323000000000000026 | Nomor handphone lebih dari 14 karakter.          |
		      | 08128 | Nomor handphone kurang dari 8 karakter.          |
	#Non Login
	@TEST_DOM-200 @TESTSET_UG-4584 @TESTSET_UG-4894 @TESTSET_UG-6222 @TESTSET_PF-1393 @TESTSET_PF-1958 @Automated @DOM3 @Web @web-covered
	Scenario Outline: [Web Tenant][Forgot Password]Set New Password using more than25 character
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user click login with tenant and click forgot password button
		    And user fill their unregistered phone number "<phone number>"
		    Then user get error message "<error message>"
		    Examples:
		      | phone number    | error message |
		      | 0864765123419   | Masukkan nomor handphone yang terdaftar.                    |
		      | 089212312304    | Nomor HP ini sudah digunakan untuk verifikasi di akun lain. |
		      | 089876543217671 | Nomor handphone lebih dari 14 karakter.                     |
		      | 08128000323000000000000026 | Nomor handphone lebih dari 14 karakter.          |
		      | 08128 | Nomor handphone kurang dari 8 karakter.          |
	#Non Login
	@TEST_DOM-203 @TESTSET_UG-4584 @TESTSET_UG-4894 @TESTSET_UG-6222 @TESTSET_PF-1393 @TESTSET_PF-1958 @Automated @DOM3 @Web @web-covered
	Scenario Outline: [Web Tenant][Forgot Password]Set New Password using less than 8 character
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user click login with tenant and click forgot password button
		    And user fill their unregistered phone number "<phone number>"
		    Then user get error message "<error message>"
		    Examples:
		      | phone number    | error message |
		      | 0864765123419   | Masukkan nomor handphone yang terdaftar.                    |
		      | 089212312304    | Nomor HP ini sudah digunakan untuk verifikasi di akun lain. |
		      | 089876543217671 | Nomor handphone lebih dari 14 karakter.                     |
		      | 08128000323000000000000026 | Nomor handphone lebih dari 14 karakter.          |
		      | 08128 | Nomor handphone kurang dari 8 karakter.          |
	#First time tenant that never use booking feature
#	@TEST_DOM-176 @TESTSET_UG-4895 @TESTSET_UG-6227 @TESTSET_PF-1962 @TESTSET_PF-1393 @Automated @DOM3 @web @web-covered
#	Scenario: [Web Tenant][Foto Identitas - Verifikasi Page]Update Jenis Identitas
#		Given user navigates to "mamikos /"
#		    When user clicks on Enter button
#		    And user login in as Tenant via phone number as "DOM Automation"
#		    And user navigates to "mamikos /user/verifikasi-akun"
#		    And user click on kartu identitas
#		    And user click continue on pop up
#		    And user click camera button to take a photo
#		    And user click save photo button
#		    And user click jenis identitas "SIM"
#		    And user click popup "Ya" button
#		    Then user see popup confirmation closed
