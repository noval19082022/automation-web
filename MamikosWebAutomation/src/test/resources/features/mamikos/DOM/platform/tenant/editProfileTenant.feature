@regression @editProfileTenant @essentiaTest
Feature: edit profile tenant

  @charactersNotMatch
  Scenario: charactersNotMatch
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "mahasiswa"
    And user click dropdown pilih universitas
    And user fills "alkamal" in search dropdown
    Then user verify message "There is no data" in search dropdown

  @charactersMatch
  Scenario: charactersMatch
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "mahasiswa"
    And user click dropdown pilih universitas
    And user fills "indonesia" in search dropdown
    Then user verify dropdown results list contains "Indonesia"

  @fillsTextBox
    Scenario: fills Text Box 1 character
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "UG Tenant"
      And user click profile on header
      And user click profile dropdown button
      And user click edit profile
      And user choose profession "mahasiswa"
      And user click dropdown pilih universitas
      And user fills "lainnya"
      And user click lainnya on dropdown
      And user fill "A" on custom university input
      Then user verify button simpan is not "disabled"

  @noFillsTextBox
  Scenario: no fills Text Box
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "mahasiswa"
    And user click dropdown pilih universitas
    And user fills "lainnya"
    And user click lainnya on dropdown
    And user fill " " on custom university input
    Then user verify button simpan is not "disabled"

  @listOfficeName
  Scenario: dropdown list office name
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "karyawan"
    And user click dropdown pilih instansi
    Then Dropdown will displayed max 7 list office name

  @inputPhoneNumberDaruratLessThan8
  Scenario: dropdown list office name
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user input phone number darurat less than "0812123" character
    Then user see validation message error less than 8

  @inputPhoneNumberDaruratMoreThan14
  Scenario: input PhoneNumber Darurat More Than 14
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user input phone number darurat more than "0812123123000014" character
    Then user see validation message error more than 14

  @changeGenderTenant
  Scenario: change gender laki-laki
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user click radio button laki-laki
    And user click simpan button
    Then user see pop up message profil disimpan

    @changeLastEducation
    Scenario: change last education
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "UG Tenant"
      And user click profile on header
      And user click profile dropdown button
      And user click edit profile
      And user click on last education tenant
      And user select S1
      And user click simpan button
      Then user see pop up message profil disimpan

  @fullNameTenantBlank
  Scenario: fullname tenant blank
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user click field full name
    And user delete previous full name
    Then user see message error "Nama Lengkap harus diisi."

  @inputFullNameWithNumber
  Scenario: input fullname with number
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user click field full name
    And user delete previous full name
    And user fills fullname with number "noval1908"
    And user click simpan button
    Then user see pop up message profil disimpan

  @changeNameTenant
  Scenario: change name tenant
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user click field full name
    And user delete previous full name
    And user fills fullname with number "Luna Classic"
    And user click simpan button
    Then user see pop up message profil disimpan

  @changeCity
  Scenario: change city profile tenant
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user click kota asal dropdown
    And user select city kabupaten aceh barat
    And user click simpan button
    Then user see pop up message profil disimpan

  @searchCity
  Scenario: search city profile tenant
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user click kota asal dropdown
    And user search city "Tangerang"
    Then user see search results for the city Tangerang

  @changeDateOfBirth
  Scenario: change date of birth
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user click icon calendar
    And user choose date of birth
    And user click simpan button
    Then user see pop up message profil disimpan

  @selectProfesiLainnya
  Scenario: select profession lainnya
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "lainnya"
    And user input profession "Wiraswasta"
    And user click simpan button
    Then user see pop up message profil disimpan

  @userChooseUniversitasIndonesia
  Scenario: user choose universitas indonesia
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "mahasiswa"
    And user click dropdown pilih universitas
    Then user fills "indonesia" and search found
    And user choose universitas indonesia
    And user click simpan button
    Then user see pop up message profil disimpan

  @notChooseProfession
  Scenario: user choose universitas indonesia
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user no choose profession
    Then user see button simpan edit profile disable

  @notChooseMartialStatus
  Scenario: not choose martial status
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user click on marital status dropdown
    Then user see martial status by default is belum kawin

  @changeMaritalStatus
  Scenario Outline: change marital status
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user click on marital status dropdown
    And user select "<marital status>"
    And user click simpan button
    Then user see pop up message profil disimpan
    Examples:
      | marital status     |
      | Belum Kawin        |
      | Kawin              |
      | Kawin Memiliki Anak|

  @fillsTextBoxPekerjaan
  Scenario: fills Text Box Pekerjaan with 1 character
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "karyawan"
    And user click dropdown pilih instansi
    And user fills "lainnya"
    And user click lainnya on dropdown
    And user fill "MM" on custom instansi input
    Then user verify button simpan is not "disabled"

  @changePekerjaan
  Scenario: change pekerjaan
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "karyawan"
    And user click dropdown pilih instansi
    And user fills "lainnya"
    And user click lainnya on dropdown
    And user fill "PT Adhi Karya (Persero) Tbk." on custom instansi input
    And user click simpan button
    Then user see pop up message profil disimpan

  @changeOfficeName
  Scenario: change office name
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "karyawan"
    And user click dropdown pilih instansi
    And user select instansi "PT Adhi Karya (Persero) Tbk."
    And user click simpan button
    Then user see pop up message profil disimpan



