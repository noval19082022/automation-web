@regression @ubahPasswordTenant
Feature: Ubah password tenant

  @ubahPasswordTenantValid
  Scenario: ubah password tenant valid
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user clicks on pengaturan button
    And user clicks on ubah button
    And user fills password lama "qwerty123"
    And user fills password baru "qwerty111"
    And user fills password confirm "qwerty111"
    And user clicks on Simpan button ubah password
    Then user auto logout and redirect to homepage "Mau cari kos?"


  @restoreUbahPasswordTenant
  Scenario: restore ubah password tenant valid
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Automation"
    And user click profile on header
    And user click profile dropdown button
    And user clicks on pengaturan button
    And user clicks on ubah button
    And user fills password lama "qwerty111"
    And user fills password baru "qwerty123"
    And user fills password confirm "qwerty123"
    And user clicks on Simpan button ubah password
    Then user auto logout and redirect to homepage "Mau cari kos?"

  @ubahPasswordMoreThan25Character
  Scenario: ubah Password More Than 25 Character
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user clicks on pengaturan button
    And user clicks on ubah button
    And user fills password lama "qwerty123"
    And user fills password baru "qwerty12345678901234567890123"
    Then user see message error more than "Password maksimal 25 karakter"

  @ubahPasswordWithSpecialCharacter
  Scenario: ubah Password With Special Character
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user clicks on pengaturan button
    And user clicks on ubah button
    And user fills password lama "qwerty123"
    And user fills password baru "!@#$%^&*"
    And user fills password confirm "!@#$%^&*"
    Then user see message error special character "Gunakan kombinasi huruf dan angka"