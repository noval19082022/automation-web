@regression @ownerDashboard @traffic-acquisition

Feature: Owner Dashboard

  @sameOrderPriorty
   Scenario: Event banner order priority
     Given user navigates to "mamikos admin"
     When user login  as a Admin via credentials
     And user access menu event
     And user set banner 1 to order 1
     And user set banner 2 to order 1
     And user set banner 3 to order 1
    Then banner order should be "Dari Mamikos"

   @bannerCheckContent
   Scenario: Event banner check content
     Given user navigates to "mamikos /"
     And user clicks on Enter button
     When user fills out owner login as "owner empty" and click on Enter button
     And user go to event banner section
     Then banner order should be "Dari Mamikos"

   @redirectionBanner
   Scenario: Event banner redirection
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner empty" and click on Enter button
    And user go to event banner section
    Then banner order should be "Dari Mamikos"
    And user click on banner 1
    Then user redirected to "https://docs.google.com/forms/d/e/1FAIpQLSewIiF7zsj-Sw43W7NJDKlljMUecZl6isUujzWOqS-ugWEgbA/viewform"

  @direferentOrderPriorty
  Scenario: Direferent order priority
    Given user navigates to "mamikos admin"
    When user login  as a Admin via credentials
    And user access menu event
    And user set banner 1 to order 1
    And user set banner 2 to order 2
    And user set banner 3 to order 3
    Then banner order should be "Dari Mamikos"

  @bannerCheckContent2
  Scenario: Event banner check content
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner empty" and click on Enter button
    And user go to event banner section
    Then banner order should be "Dari Mamikos"