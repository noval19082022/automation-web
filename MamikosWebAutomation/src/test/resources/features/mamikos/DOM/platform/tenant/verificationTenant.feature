@tenantVerification @regression

Feature:  Tenant - Verification
  @verificationResendOTP
  Scenario: Verification - Kirim ulang OTP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Verification"
    And user click on the tenant profile
    And user click profile dropdown button
    And user click verifikasi akun menu
    And user click on ubah phone number button
    And user change phone number with "8239231283"
    And user click on ubah button
    And user verify OTP verification message was sent "Kami telah mengirimkan Kode OTP ke nomor 08239231283"
    When user click on resend OTP text
   # Then user verify OTP countdown should start on "02:"
    Then user verify OTP verification message was sent "Kami telah mengirimkan Kode OTP ke nomor 08239231283"

  @verificationEmailWrongFormat
  Scenario: Verification - Email with wrong format
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Verification"
    And user click on the tenant profile
    And user click profile dropdown button
    And user click verifikasi akun menu
    And user click on verifikasi button
    And user change email with wrong format email "margaretha@mamikos"
    Then user get red alert error message "Email harus berupa alamat surel yang benar."

  @verificationEmailAlreadyRegistered
  Scenario: Verification - Email already registered
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Verification"
    And user click on the tenant profile
    And user click profile dropdown button
    And user click verifikasi akun menu
    And user click on verifikasi button
    And user change email with already registered email "kharismamargaretha@gmail.com"
    Then user get pop up error message "Email sudah digunakan oleh akun lain"

  @verificationPhoneNumberEmpty
  Scenario: Verification - when phone number is empty
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Verification"
    And user click on the tenant profile
    And user click profile dropdown button
    And user click verifikasi akun menu
    And user click on ubah phone number button
    And user empty phone number field
    Then user get red alert error message "Nomor Handphone harus diisi." on phonenumber section

  @verificationLessThan8DigitPhoneNumber
  Scenario: Verification - input number < 8
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Verification"
    And user click on the tenant profile
    And user click profile dropdown button
    And user click verifikasi akun menu
    And user click on ubah phone number button
    And user change phone number with "8112069"
    Then user get red alert error message "Nomor Handphone minimal mengandung 8 karakter." on phonenumber section

  @verificationMoreThan14DigitPhoneNumber
  Scenario: Verification - input number > 14
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Verification"
    And user click on the tenant profile
    And user click profile dropdown button
    And user click verifikasi akun menu
    And user click on ubah phone number button
    And user change phone number with "81120699900000"
    And user click on ubah button
    Then user get pop up error message "Maaf, Nomor tidak valid, silahkan gunakan nomor yang lain"

  @verificationPhoneNumberAlreadyRegistered
  Scenario: Verification - phone number already exist
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Verification"
    And user click on the tenant profile
    And user click profile dropdown button
    And user click verifikasi akun menu
    And user click on ubah phone number button
    And user change phone number with "898765432166"
    And user click on ubah button
    Then user get pop up error message "Maaf nomor sudah terdaftar"

  @verificationOTPPage
  Scenario: Verification - Redirected to OTP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Verification"
    And user click on the tenant profile
    And user click profile dropdown button
    And user click verifikasi akun menu
    And user click on ubah phone number button
    And user change phone number with "8239231283"
    And user click on ubah button
    Then user verify OTP verification message was sent "Kami telah mengirimkan Kode OTP ke nomor 08239231283"

# need improved
#  @viewUploadedIDCard
#  Scenario: Verification - View Uploaded ID Card
#    Given user navigates to "mamikos /"
#    When user clicks on Enter button
#    When user login in as Tenant via phone number as "TA Verification"
#    Given user navigates to "mamikos /user/verifikasi-akun"
#    And user click on kartu identitas
#    When user click continue on pop up
#    And user click camera button to take a photo
#    And user click save photo button
#    When user click on uploaded photo
#    Then user verify the uploaded photo

  @verificationEmptyEmail
  Scenario: Verification - when empty email
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "TA Verification"
    And user click on the tenant profile
    And user click profile dropdown button
    And user click verifikasi akun menu
    And user click on verifikasi button
    And user clear email input field
    Then user get red alert error message "Email harus diisi."

  @updateJenisIdentitas
  Scenario: Verification - Update Jenis Identitas
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "DOM Automation"
    And user navigates to "mamikos /user/verifikasi-akun"
    And user click on kartu identitas
    And user click continue on pop up
    And user click camera button to take a photo
    And user click save photo button
    And user click jenis identitas "SIM"
    And user click popup "Ya" button
    Then user see popup confirmation closed