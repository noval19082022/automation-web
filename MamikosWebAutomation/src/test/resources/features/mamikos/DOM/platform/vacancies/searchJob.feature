@regression @search_job_vacancy

Feature: Search and Filter Vacancy
  //this scenario is currently on hold
#  Scenario: Search Menggunakan Keyword
#    Given user navigates to "mamikos /loker"
#    And user clicks on search bar
#    And User set text with keyword "sales"
#    Then should display the result job list
#
#  Scenario: Filter Menggunakan Status Pekerjaan
#    Given user navigates to "mamikos /loker"
#    And user clicks on job status filter dropdown
#    Then display the list of job status :
#    | Semua         |
#    | Full Time     |
#    | Part Time     |
#    | Freelance     |
#    | Magang        |
#
#  Scenario: Filter Menggunakan Pendidikan Terakhir
#    Given user navigates to "mamikos /loker"
#    And user clicks on last education filter dropdown
#    Then display the list of education :
#      | Apa Saja      |
#      | SMA/SMK       |
#      | D1/D2/D3      |
#      | Sarjana/S1    |
#      | Master/S2     |
#      | Doktor/S3     |
#      | Mahasiswa     |
#      | Fresh Graduate|
#
#  Scenario: Urutkan Lowongan Pekerjaan
#    Given user navigates to "mamikos /loker"
#    And user clicks on sort by filter dropdown
#    Then display the list of sort by filter :
#      | Via Mamikos           |
#      | Dari Lowongan Terbaru |
#      | Dari Lowongan Terlama |
#
#
#  Scenario: Ensure the job list element atributte is present
#    Given user navigates to "mamikos /loker"
#    Then user check the amount joblist display in first page
#    And check the attribute on job vacancy widget before login
#
#  Scenario Outline: Ensure the pagination of job vacancy landing page
#    Given user navigates to "mamikos /loker"
#    And user click on next <page>
#    Then display the job list "<number>" in on page :
#
#    Examples:
#      |  page  |  number |
#      |    2   | 19 - 36 |
#      |    3   | 37 - 54 |
#      |    4   | 55 - 72 |