@promo

Feature: Promo page

  @copy-promo-code
  Scenario: User can copy promo code
    Given user navigates to "promo"
    When user click SALIN on any promo
    Then promo code can be copied

  @regression
  Scenario: Check pagination in promo page
    Given user navigates to "promo"
    When user click next page button
    Then next promo page will be opened
    When user click previous page button
    Then previous promo page will be opened
    When user click page index "2"
    Then promo page "2" will be opened

  @regression
  Scenario: User can open promo detail
    Given user navigates to "promo"
    And user see the promo title in first promo
    When user click see detail on first promo
    Then detail promo page opened with correct title
    And user see button booking now