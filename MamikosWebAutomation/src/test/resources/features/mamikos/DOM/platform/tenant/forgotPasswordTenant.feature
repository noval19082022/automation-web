@regression @tenantForgotPassword
Feature: Tenant - Forgot Password
  @tenantResendOTPviaSMS
  Scenario: Forgot Password - Resend OTP via SMS
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with tenant and click forgot password button
    And user fill their registered phone number "086476512341" and click send button
    And user click otp via sms on page "Pilih Metode Verifikasi"
    Then user verify and click button resend OTP "Kirim ulang kode"

  @tenantResendOTPviaWA
  Scenario: Forgot Password - Resend OTP via WA
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with tenant and click forgot password button
    And user fill their registered phone number "086476512341" and click send button
    And user click otp via wa on page "Pilih Metode Verifikasi"
    Then user verify and click button resend OTP "Kirim ulang kode"

  @tenantRedirectToForgotPasswordPage
  Scenario: Forgot Password - Redirect to forgot password page
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with tenant and click forgot password button
    Then user redirected to "/lupa-password-pencari"

  @tenantInputRegisteredPhoneNumber
  Scenario: Forgot Password - Input registered phone number
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with tenant and click forgot password button
    And user fill their registered phone number "086476512341" and click send button
    Then user verify on page "Pilih Metode Verifikasi"

  @tenantSendOTPviaWA
  Scenario: Forgot Password - Send OTP via WA
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with tenant and click forgot password button
    And user fill their registered phone number "089220220101" and click send button
    And user click otp via wa on page "Pilih Metode Verifikasi"
    Then user verify otp form appear on page "Nomor handphone ini telah terdaftar sebagai akun pencari kos di Mamikos"

  @tenantSendOTPviaSMS
  Scenario: Forgot Password - Send OTP via SMS
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with tenant and click forgot password button
    And user fill their registered phone number "086476512341" and click send button
    And user click otp via sms on page "Pilih Metode Verifikasi"
    Then user verify otp form appear on page "Nomor handphone ini telah terdaftar sebagai akun pencari kos di Mamikos"

  @inputInvalidOTP
  Scenario: Forgot Password - Input invalid OTP via SMS
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with tenant and click forgot password button
    And user fill their registered phone number "089220220102" and click send button
    And user click otp via sms on page "Pilih Metode Verifikasi"
    And user input invalid OTP "1" "1" "1" "1"
    Then user verify invalid OTP message "Kode verifikasi salah." "Mohon masukkan kode verifikasi yang kami kirim."

  @phoneNumberErrorCompilation
  Scenario Outline: Forgot Password - Use number not registered, Phone number duplicated, Phone number more than 14 characters
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with tenant and click forgot password button
    And user fill their unregistered phone number "<phone number>"
    Then user get error message "<error message>"
    Examples:
      | phone number    | error message |
      | 0864765123419   | Masukkan nomor handphone yang terdaftar.                    |
      | 089212312304    | Nomor HP ini sudah digunakan untuk verifikasi di akun lain. |
      | 089876543217671 | Nomor handphone lebih dari 14 karakter.                     |
      | 08128000323000000000000026 | Nomor handphone lebih dari 14 karakter.          |
      | 08128 | Nomor handphone kurang dari 8 karakter.          |

  @usePhoneNumberLoginFacebook
  Scenario: Forgot password - User use number login via facebook
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with tenant and click forgot password button
    And user fill their registered phone number "081223102002" and click send button
    Then user verify on page "Pilih Metode Verifikasi"

  @tenantForgotPasswordOnOwnerFeature
  Scenario: Forgot password - User use number owner
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "081223102002"
    Then user get error message "Nomor HP tidak terdaftar sebagai pemilik kos."