@DOM
Feature: Owner - Login

	@TEST_DOM-2241 @Automated @DOM @web-covered
	Scenario: [WEB][Login Owner] Login with valid credentials
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login as "master" and click on Enter button
		    Then user redirected to "owner page"
	@TEST_DOM-2242 @Automated @DOM @web-covered
	Scenario: [WEB][Login Owner] Login with invalid password
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login with invalid "password" and click on Enter button
		    Then user see the form input error "Nomor dan password tidak sesuai"
	@TEST_DOM-2243 @Automated @DOM @web-covered
	Scenario: [WEB][Login Owner] Owner Want to cancel login
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user click button pemilik kost
		    And user verify login form owner
		    And user click back button in login owner
		    And user click button close login form
		    Then user verify login form close
