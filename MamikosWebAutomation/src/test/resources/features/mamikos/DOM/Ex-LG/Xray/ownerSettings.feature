@DOM
Feature: Owner Settings

	Background:
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user fills out owner login as "master" and click on Enter button
		And user navigates to "owner /ownerpage/settings"

	@TEST_DOM-2286 @Automated @DOM @web-covered
	Scenario: [Setelan Akun] Email recommendation can be enabled and disabled
		When user click recommendation via email
		    Then user see toast message "Rekomendasi via email berhasil dinonaktifkan" in settings page
		    When user click recommendation via email
		    Then user see toast message "Rekomendasi via email berhasil diaktifkan" in settings page
	@TEST_DOM-2287 @Automated @DOM @web-covered
	Scenario: [Setelan Akun] Chat notification can be enabled and disabled
		When user click chat notification
		    Then user see toast message "Notifikasi via chat berhasil dinonaktifkan" in settings page
		    When user click chat notification
		    Then user see toast message "Notifikasi via chat berhasil diaktifkan" in settings page
	@TEST_DOM-2288 @Automated @DOM @web-covered
	Scenario: [Setelan Akun] SMS Update Kos can be enabled and disabled
		When user click SMS update kos
		    Then user see toast message "Notifikasi kos via SMS berhasil dinonaktifkan" in settings page
		    When user click SMS update kos
		    Then user see toast message "Notifikasi kos via SMS berhasil diaktifkan" in settings page
	@TEST_DOM-2289 @Automated @DOM @web-covered
	Scenario: [Setelan Akun] Text box "Nama" is inputed with invalid value
		When user clicks on Change Name button
		    And user fills out name input with "7777 88"
		    Then user see pop up error message "Mohon masukkan karakter alfabet" in settings page
		    And user click OK in pop up in settings page
		    When user fills out name input with "+-=*%"
		    Then user see pop up error message "Mohon masukkan karakter alfabet" in settings page
		    And user click OK in pop up in settings page
		    When user fills out name input with ""
		    Then user see pop up error message "Mohon masukkan karakter alfabet" in settings page
		    And user click OK in pop up in settings page
		    When user fills out name input with "aa"
		    Then user see pop up error message "Minimal 3 karakter" in settings page
		    And user click OK in pop up in settings page
	#1. Already log in as owner
	@TEST_DOM-2209 @Automated @DOM @web-covered
	Scenario: [Setelan Akun][Change Number] No input phone number
		Given user navigates to "mamikos /"
		    And user clicks on Enter button
		    When user fills out owner login as "owner gp 1" and click on Enter button
		    And user navigates to "owner /ownerpage/settings"
		    And user clicks on Change Nomor Handphone button
		    Then user see input phone number placeholder "Masukkan nomor handphone"
	@TEST_DOM-2211 @Automated @DOM @web-covered
	Scenario: [Setelan Akun][Change Number] Input registered owner number new flow
		Given user navigates to "mamikos /"
		    And user clicks on Enter button
		    When user fills out owner login as "owner gp 1" and click on Enter button
		    And user navigates to "owner /ownerpage/settings"
		    And user clicks on Change Nomor Handphone button
		    And user fills phone number owner registered new flow "0891202103"
		    And user clicks on Simpan button
		    Then user see message error validation "The phone number has already been taken."
	@TEST_DOM-2212 @Automated @DOM @web-covered
	Scenario: [Setelan Akun][Change Number] Input registered owner number old flow
		Given user navigates to "mamikos /"
		    And user clicks on Enter button
		    When user fills out owner login as "owner gp 1" and click on Enter button
		    And user navigates to "owner /ownerpage/settings"
		    And user clicks on Change Nomor Handphone button
		    And user fills phone number owner registered new flow "081345645601"
		    And user clicks on Simpan button
		    Then user see message error validation "The phone number has already been taken."
	@TEST_DOM-2213 @Automated @DOM @web-covered
	Scenario: [Setelan Akun][Change Number] Resend code verification
		Given user navigates to "mamikos /"
		    And user clicks on Enter button
		    When user fills out owner login as "owner gp 1" and click on Enter button
		    And user navigates to "owner /ownerpage/settings"
		    And user clicks on Change Nomor Handphone button
		    And user fills phone number owner registered new flow "08912021100"
		    And user clicks on Simpan button
		    And user see otp pop up
		    Then user click on resend otp button
	@TEST_DOM-2214 @Automated @DOM @web-covered
	Scenario: [Setelan Akun][Change Number] Special character and numeric
#		    And user clicks on Enter button
#		    When user fills out owner login as "owner gp 1" and click on Enter button
#		    And user navigates to "owner /ownerpage/settings"
		    And user clicks on Change Nomor Handphone button
		    And user fills phone number owner registered new flow "08134564!@#$%"
		    And user clicks on Simpan button
		    Then user see message error validation "Nomor handphone harus diawali dengan 08."
