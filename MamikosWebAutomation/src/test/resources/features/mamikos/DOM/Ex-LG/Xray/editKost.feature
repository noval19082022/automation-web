@DOM @editKost
Feature: Edit Kost

  Background:
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/kos"

  @TEST_DOM-2270 @DOM @EditKosInvalidFacility @automated @web-covered
  Scenario: [Web][Edit Kost] Edit kost fasilitas with invalid data
    Given user clicks button find your kos here
    And input name kos "Kos oke bebek Vviop Depok Sleman"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    When user click Edit in Facilities
    And user uncheck facilities under "Fasilitas Umum"
      | CCTV |
    And user uncheck facilities under "Fasilitas Kamar"
      | Air panas |
      | Kasur     |
      | Sofa      |
    And user uncheck facilities under "Fasilitas Kamar Mandi"
      | Bak mandi |
      | Bathup    |
      | Gayung    |
    And user uncheck facilities under "Parkir"
      | Parkir Mobil |
    Then user see edit finished button is disabled
    And user see "Fasilitas Umum" has warning title "Pilih Fasilitas" and description "Pilih minimal 1 fasilitas"
    And user see "Fasilitas Kamar" has warning title "Pilih Fasilitas" and description "Pilih minimal 1 fasilitas"
    And user see "Fasilitas Kamar Mandi" has warning title "Pilih Fasilitas" and description "Pilih minimal 1 fasilitas"

  @TEST_DOM-2271 @DOM @EditKos1to5NoChanges @automated @web-covered
  Scenario: [Web][Edit Kost] Edit kost with condition user wants to edit step 1-5 without change anything
    Given user clicks button find your kos here
    And input name kos "Kos oke bebek Tipe o"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    When user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    And user edit other popup data
    When user click back on pop up attention
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Facilities
    And user click Edit in Room Availability
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kos oke bebek Tipe o"
    And user clicks kos name from result
    Then user see kos with name "Kos oke bebek Tipe o", status "Aktif" and type "Kos Campur"

  @TEST_DOM-2272 @DOM @EditKosAddress @automated @web-covered
  Scenario: [Web][Edit Kost] Edit kost address with valid data
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Kos Address
    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user input address note "Perubahan agar diperiksa admin " and random text
    And user clicks on edit done button in bottom of add kos page
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Diperiksa Admin" and type "Kos Putra"
		      # Verify kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to kost owner menu
    And user search kos name "Kose Putri Automation" in admin kos owner page
    And user verify the kos in admin kos owner
    And user navigates to "mamikos /"
    And user logs out as a Owner user
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Kos Address
   # And user input kost location "Sleman" and clicks on first autocomplete result "Kabupaten Bantul" "Bantul"
    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user clicks on edit done button in bottom of add kos page
    And user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Aktif" and type "Kos Putra"
    And user see location kos address is in "Tobelo"

  @TEST_DOM-2273 @DOM @EditKosNewAddType @automated @web-covered
  Scenario: [Web][Edit Kost] Edit kost with condition user with new kost && PLM && user already finished edit data kost && doesnt have "tipe kamar"
    Given user clicks on Add Data button
    And user clicks on Add button
    And user selects Kost option and click on Add Data button
    And user click add new kos button
    When user fills kost name field with "Kose Putri Automation" and random text
    And user clicks on kost type icon "male"
    And user fill kos description with "kos terbaik hari raya"
    And user set kos rules :
      | Security |
    And user select kos year built "2020"
    And user clicks on next button in bottom of add kos page
    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Umum"
      | CCTV |
    And user check facilities under "Fasilitas Kamar"
      | AC |
    And user check facilities under "Fasilitas Kamar Mandi"
      | Air panas |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 4"
    And user input "15" to field total room
    And user input "10" to total available room
    And user clicks on next button in bottom of add kos page
    And user input monthly price with "300000" in add kos page
    And user clicks on next button in bottom of add kos page
    And user click done in success page
    Then user see kos with name "Kose Putri Automation" and random text, status "Diperiksa Admin" and type "Kos Putra"
		  # Verify kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to kost owner menu
    And user search kos name "Kose Putri Automation" in admin kos owner page
    And user filter edited - ready to verify
    And user verify the kos in admin kos owner
		  # Edit kos
    And user navigates to "mamikos /"
    And user logs out as a Owner user
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Data Kos
    And user see kos name field is disabled
    And user clicks checkbox room type
    And user input room type with "Tipe A"
    And user clicks on kost type icon "female"
    And user fill kos description with "kos asolole uwo uwo jos"
    And user set kos rules :
      | Laundry  |
      | Security |
    And user select kos year built "2020"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Kos Address
    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user input address note "Perubahan agar diperiksa admin " and random text
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Kos Photos
    And user insert photo image to "Foto bangunan tampak depan"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Room Photos
    And user insert photo image to "Foto depan kamar"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Facilities
    And user check facilities under "Fasilitas Umum"
      | Dapur  |
      | Kulkas |
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Room Availability
    And user clicks on kost size "3 x 4"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Diperiksa Admin" and type "Kos Putra"
    And user see location kos address is in "Tobelo"
		    # Delete kos in admin
    And user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to kost owner menu
    And user search kos name "Kose Putri Automation" in admin kos owner page
    And user delete the kos in admin kos owner

  @TEST_DOM-2274 @DOM @EditKosWithPLM @automated @web-covered
  Scenario Outline: [Web][Edit Kost] Edit kost with condition user with new kost && PLM && user already finished edit data kost && have "tipe kamar"
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    Then user see kos name field is disabled
   # And user see kos room type checkbox is disabled
    And user clicks checkbox room type
    And user input room type with random text
    And user clicks on kost type icon "<Gender>"
    And user fill kos description with "kos badadam badadam"
    And user set kos rules :
      | Laundry  |
      | Security |
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    And user input administrator name with "Jojojo"
    And user input administrator phone with "083333333777"
    And user click Edit in Kos Address
    And user click Keluar button on BC pop up
    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user input address note "Perubahan agar diperiksa admin " and random text
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Kos Photos
    And user insert photo image to "Foto bangunan tampak depan"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Room Photos
    And user insert photo image to "Foto depan kamar"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Facilities
    And user check facilities under "Fasilitas Umum"
      | Dapur  |
      | Kulkas |
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Room Availability
    And user clicks on kost size "<KostSize>"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Diperiksa Admin" and type "Kos <GenderID>"
    And user see location kos address is in "<Location>"
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to kost owner menu
    And user search kos name "Kose Putri Automation" in admin kos owner page
    And user verify the kos in admin kos owner
    Examples:
      | Gender | GenderID | Location | KostSize |
      | male   | Putra    | Tobelo   | 3 x 3    |
    #  | female | Putra    | Sleman   | 3 x 4    |

  @TEST_DOM-2275 @DOM @EditKosValidData @automated @web-covered
  Scenario: [Web][Edit Kost] Edit kost data kost with valid data
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    When user click Edit in Data Kos
    And user clicks checkbox room type
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user fill kos description with "kos badadam badadam"
    And user set kos rules :
      | ATM  |
      | Akses 24 Jam |
    And user click Edit in Kos Photos
    And user click Keluar button on BC pop up
    And user upload kos rules photo
    And user clicks delete photo in kos rule
    And user upload different kos rule photo and see kos rule photo is changed
    When user delete last kos rule photo
    And user click Edit in Data Kos
    And user click Keluar button on BC pop up
    And user clicks checkbox room type
    And user input room type with random text
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    And user input administrator name with "Mamamam"
    And user input administrator phone with "083333333770"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
		      # Verify kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to kost owner menu
    And user search kos name "Kose Putri Automation" in admin kos owner page
    Then user verify the kos in admin kos owner

  @TEST_DOM-2276 @DOM @EditKosInvalidData @automated @web-covered
  Scenario: [Web][Edit Kost] Edit kost data kost with invalid data
    Given user clicks button find your kos here
    And input name kos "Kost DOMeqGGD Tipe o"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
		 # When user click Edit in Data Kos
    And user clicks checkbox room type
    And user input room type with "   "
    Then user see error message "Anda belum mengisi nama tipe kamar ini." under data kos room type field
    When user input room type with "cccc()"
    Then user see error message "Tidak boleh menggunakan karakter spesial seperti / ( ) - . , ’ dan “" under data kos room type field
    When user input room type with "c"
    Then user see error message "Tidak boleh kurang dari 2 karakter." under data kos room type field
    When user input room type with "Tipe abcdefghijklmnopqrstu abcdefghijklmnopqrstu abcdefghijklmnopqrstu abcdefghijklmnopqrstu 12345678"
    Then user see error message "Tidak boleh lebih dari 100 karakter." under data kos room type field
    When user fill kos description with " "
    Then user see error message "Anda belum mengisi deskripsi kos." under kos description field
    And user clicks checkbox administrator kos
    And user input administrator name with " "
    And user input administrator phone with " "
    Then user see error message "Silakan isi nama pengelola." under kos manager name field
    And user see error message "Silakan isi nomor hp pengelola." under kos manager phone field
    And user input administrator name with "A"
    And user input administrator phone with "0891234"
    Then user see error message "Tidak boleh kurang dari 2 karakter." under kos manager name field
    And user see error message "Tidak boleh kurang dari 8 karakter." under kos manager phone field
    And user input administrator name with "Jajang sunandar slamet riyadhii"
    And user input administrator phone with "089123456789012345678"
    Then user see error message "Tidak boleh lebih dari 30 karakter." under kos manager name field
    And user see error message "Tidak boleh lebih dari 20 karakter." under kos manager phone field

  @TEST_DOM-2277 @DOM @EditKosInvalidAddress @automated @web-covered
  Scenario: [Web][Edit Kost] Edit kost address with invalid data
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Kos Address
    When user input kost location "DKI Jakarta"
    Then user see error message "Anda belum memilih Kabupaten/Kota." under kabupaten field
  #  And user see error message "Anda belum memilih Kecamatan." under kecamatan field
    And user see next button disable
    When user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user input address note "Belok kiri toko"
    Then user see error message "Tidak boleh kurang dari 20 karakter." under address note field
    When user input address note "Belok kiri dari took jaya abadi, lalu lurus sampai mentok, belok kanan kemudian lurus 200 meter, kemudian belok kiri. Belok kiri dari took jaya abadi, lalu lurus sampai mentok, belok kanan kemudian lurus 200 meter, kemudian belok kiri. Belok kiri dari took jaya abadi, lalu lurus sampai mentok smapai."
    Then user see error message "Tidak boleh lebih dari 300 karakter." under address note field
    And user see next button disable

  @TEST_DOM-2278 @DOM @EditKosInvalidRoomSize @automated @web-covered
  Scenario: [Web][Edit Kost] Edit kost room availability with invalid data
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Room Availability
    And user clicks on kost size "Lainnya"
    Then user see warning under room size "Ukuran kamar lainnya masih kosong."
    When user input other room size with "0" and "0"
    Then user see warning under room size "Isi angka 1-9"
    When user input other room size with "0,5" and "0,6"
    Then user see warning under room size "Gunakan titik, contoh \"3.5\""
    When user input other room size with "0.5" and "0.6"
    Then user see warning under room size "Format angka yang Anda masukkan tidak sesuai"

  @TEST_DOM-2279 @DOM @EditKosActive @automated @web-covered
  Scenario: [Web][Edit Kost] Edit Data Kost from entry point kost list when kost status == Active
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user see kos name field is disabled
    And user clicks on kost type icon "female"
    And user fill kos description with "kos asolole uwo uwo jos"
    And user set kos rules :
      | Laundry  |
      | Security |
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    And user input administrator name with "Mumumumu"
    And user input administrator phone with "083333333777"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Kos Address
    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user input address note "Perubahan agar diperiksa admin " and random text
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Kos Photos
    And user insert photo image to "Foto bangunan tampak depan"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Room Photos
    And user insert photo image to "Foto depan kamar"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Facilities
    And user check facilities under "Fasilitas Umum"
      | Dapur  |
      | Kulkas |
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click back on pop up attention
    And user click Edit in Room Availability
    And user clicks on kost size "3 x 4"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Diperiksa Admin" and type "Kos Putri"
    And user see kos address is in "Tobelo"
		  # Verify kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to kost owner menu
    And user search kos name "Kose Putri Automation" in admin kos owner page
    Then user verify the kos in admin kos owner

  @TEST_DOM-2280 @DOM @EditKosRejected @automated @web-covered
  Scenario: [Web][Edit Kost] Edit kost with condition user have PLM && Rejected Kost
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user see kos name field is disabled
    And user clicks checkbox room type
    And user input room type with "Tipe R" and random text
    And user clicks on kost type icon "female"
    And user fill kos description with "kos test rejected then edit"
    And user set kos rules :
      | Laundry |
    And user select kos year built "2020"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click skip in ask mamikos page
    And user click Edit in Kos Address
    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user input address note "Perubahan agar diperiksa admin " and random text
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click skip in ask mamikos page
    And user click Edit in Kos Photos
    And user insert photo image to "Foto bangunan tampak depan"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click skip in ask mamikos page
    And user click Edit in Room Photos
    And user insert photo image to "Foto depan kamar"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click skip in ask mamikos page
    And user click Edit in Facilities
    And user check facilities under "Fasilitas Umum"
      | Dapur  |
      | Kulkas |
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click skip in ask mamikos page
    And user click Edit in Room Availability
    And user clicks on kost size "3 x 4"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Diperiksa Admin" and type "Kos Putri"
    And user see kos address is in "Tobelo"
		    # Reject kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to kost owner menu
    And user search kos name "Kose Putri Automation" in admin kos owner page
    And user click reject button in admin kos owner page
    And user click checkbox "Posisi di peta kurang sesuai" in reject reason page below "Alamat Kos"
    And user click reject button in kos owner reject reason
    Then user click send in send reject pop up
    And user navigates to "mamikos /admin/owner"
    And user search kos name "Kose Putri Automation" in admin kos owner page
    Then user verify the kos in admin kos owner

  @TEST_DOM-2281 @DOM @EditKosByPassData @automated @web-covered
  Scenario: [Web][Edit Kost] Status kos == active && owner edit bypassing data
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user clicks on kost type icon "female"
    And user set kos rules :
      | Security |
    And user select kos year built "2019"
    And user input administrator name with "Mamamama"
    And user input administrator phone with "083333333771"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Aktif" and type "Kos Putri"
    When user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Kos Address
    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Aktif" and type "Kos Putri"
    When user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Facilities
    And user check facilities under "Fasilitas Umum"
      | Kulkas |
    And user check facilities under "Fasilitas Kamar"
      | Air panas |
    And user uncheck facilities under "Fasilitas Kamar Mandi"
      | Bathup |
    And user check facilities under "Parkir"
      | Parkir Mobil |
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Aktif" and type "Kos Putri"
    When user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Room Availability
    And user clicks on kost size "3 x 3"
    And user clicks on edit done button in bottom of add kos page
    Then user see success add data kos pop up with text "Data Kos Telah Diperbarui"
    When user click done in success page pop up of edit kos
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Aktif" and type "Kos Putri"
    And user see location kos address is in "Tobelo"

  @TEST_DOM-2282 @DOM @EditKosPhoto @automated @web-covered
  Scenario: [Web][Edit kos][Foto Kos] Edit kost foto kost with invalid data
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Kos Photos
    And user delete all kost photos
    Then user see warning message user need to complete the photo
    When user insert invalid photo image to "Foto bangunan tampak depan"
    And user insert invalid photo image to "Foto tampilan dalam bangunan"
    And user insert invalid photo image to "Foto tampak dari jalan"
    Then user see warning message invalid kos photo

  @TEST_DOM-2283 @DOM @EditKostPhotoRoom @automated @web-covered
  Scenario: [Web][Edit kos][Foto Kamar]Edit kost foto kamar with invalid data
    Given user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click button Edit Data Kos
    And user click Edit in Room Photos
    And user delete all kost photos
    Then user see warning message user need to complete the photo
    And user insert invalid photo image to "Foto depan kamar"
    And user insert invalid photo image to "Foto dalam kamar"
    And user insert invalid photo image to "Foto kamar mandi"
    Then user see warning message invalid kos photo
