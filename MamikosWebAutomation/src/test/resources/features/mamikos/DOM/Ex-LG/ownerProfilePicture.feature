@regression @ownerProfilePicture

Feature: Owner - Profile Picture

  Scenario: Profile Picture is null
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user click on owner popup
    Then user verify profile picture is null

  Scenario: Profile Picture is show
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "payment" and click on Enter button
    And user click on owner popup
    Then user verify profile picture is show