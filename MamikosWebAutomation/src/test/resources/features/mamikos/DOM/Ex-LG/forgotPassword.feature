@regression @ownerForgotPassword

Feature: Owner - Forgot Password

  @resendOTPviaSMS
  Scenario: Forgot Password - Resend OTP via SMS
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their registered phone number "083843666868" and click send button
    And user click otp via sms on page "Pilih Metode Verifikasi"
    Then user verify and click button resend OTP "Kirim ulang kode"

  @resendOTPviaWA
  Scenario: Forgot Password - Resend OTP via WA
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their registered phone number "083843666868" and click send button
    And user click otp via wa on page "Pilih Metode Verifikasi"
    Then user verify and click button resend OTP "Kirim ulang kode"

  @redirectToForgotPasswordPage
  Scenario: Forgot Password - Redirect to forgot password page
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    Then user redirected to "/lupa-password-pemilik"

  @inputRegisteredPhoneNumber
  Scenario: Forgot Password - Input registered phone number
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their registered phone number "0812345670005" and click send button
    Then user verify on page "Pilih Metode Verifikasi"

  @useNumberNotRegistered
  Scenario: Forgot Password - Use number not registered
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "08743333999"
    Then user get error message "Masukkan nomor handphone yang terdaftar."

  @wrongPhone
  Scenario: Forgot Password - Wrong phone number format
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "09129299222"
    Then user get error message "Nomor handphone harus diawali dengan 08"

  @phoneNumberLessthan8Char
  Scenario: Forgot Password - Phone number less than 8 characters
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "0812923"
    Then user get error message "Nomor handphone kurang dari 8 karakter."

  @phoneNumberContainsAlphabetOrSymbol
  Scenario: Forgot Password - Phone number contains alphabet or symbol
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "aaaa@@@@"
    Then user get error message "Nomor handphone hanya dapat diisi dengan angka"

  @phoneNumberLessThan8CharAndContainsAlphabetOrSymbol
  Scenario: Forgot Password - Phone number is less than 8 char and contains alphabet/symbol
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "08912an"
    Then user get error message "Nomor handphone hanya dapat diisi dengan angka"

  @phoneNumberMoreThan14Char
  Scenario: Forgot Password - Phone number is more than 14 char
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "089125555555555"
    Then user get error message "Nomor handphone lebih dari 14 karakter."

  @emptyPhoneForgotPass
  Scenario: Forgot Password - Input empty phone number
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "0855555555556"
    And user fill their unregistered phone number ""
    Then user see button choose verify method is disabled

  @sendOTPviaWA
  Scenario: Forgot Password - Send OTP via WA
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their registered phone number "083843666868" and click send button
    And user click otp via wa on page "Pilih Metode Verifikasi"
    Then user verify otp form appear on page send OTP "Mohon isi kolom berikut dengan kode verifikasi yang kami kirimkan ke ********6868 melalui WhatsApp."

  @sendOTPviaSMS
  Scenario: Forgot Password - Send OTP via sms
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their registered phone number "083843666868" and click send button
    And user click otp via sms on page "Pilih Metode Verifikasi"
    Then user verify otp form appear on page send OTP "Mohon isi kolom berikut dengan kode verifikasi yang kami kirimkan ke ********6868 melalui SMS."

  @ownerForgotPassword
  Scenario: Forgot password - User use number login via facebook on owner feature
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "085275574561"
    Then user get error message "Nomor HP tidak terdaftar sebagai pemilik kos."

  @invalidOTP
  Scenario: Forgot Password - Use Invalid OTP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their registered phone number "083843666868" and click send button
    And user click otp via wa on page "Pilih Metode Verifikasi"
    And user input invalid code otp "0000"
    Then user verify invalid OTP message "Kode verifikasi salah." "Mohon masukkan kode verifikasi yang kami kirim."

  @cancelVerificationOTP
  Scenario: Forgot Password - Cancel verification page input OTP and ubah password
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their registered phone number "083843666868" and click send button
    And user click otp via sms on page "Pilih Metode Verifikasi"
    And user click back button on page otp
    Then user see popup verifikasi batalkan proses "Yakin batalkan proses verifikasi?"

  @changeMethodSMStoSMS
  Scenario: Forgot Password - cancel verification and change metode OTP SMS to SMS
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their registered phone number "085941399178" and click send button
    And user click otp via sms on page "Pilih Metode Verifikasi"
    And user click back button on page otp
    And user click ya, batalkan
    And user click otp via sms on page "Pilih Metode Verifikasi"
    Then user see toast message "Mohon tunggu" "detik lagi untuk kirim ulang kode verifikasi." in forgot password

  @changeMethodSMStoWA
  Scenario: Forgot Password - cancel verification and change metode OTP SMS to WA
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their registered phone number "083843666868" and click send button
    And user click otp via sms on page "Pilih Metode Verifikasi"
    And user click back button on page otp
    And user click ya, batalkan
    And user click otp via wa on page "Pilih Metode Verifikasi"
    Then user verify otp form appear on page OTP "Verifikasi Nomor Handphone"

  @directToWA
  Scenario: Forgot Password - User direct to Whatsapp
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click login with owner and click forgot password button
    And user fill their unregistered phone number "085742883683"
    And user get error message "Nomor HP ini sudah digunakan untuk verifikasi di akun lain."
    When user click underline "Mohon hubungi CS Mamikos."
    Then user directed to wa and verify pretext "Halo, nomor handphone/email saya 085742883683 sudah pernah digunakan untuk verifikasi di akun lain. Mohon bantuannya."