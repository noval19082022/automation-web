@DOM2
Feature: Create Kos Mamipay Active

	Background:
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user fills out owner login as "master" and click on Enter button
		And user navigates to "owner /ownerpage/kos"
		And user clicks on Add Data button
		And user clicks on Add button
		And user selects Kost option and click on Add Data button

	@TEST_DOM-2200 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Create new kos with PLM and select room type "Buat Baru" from list room type && input invalid value room type
		Given user click add another type from kos "Kos oke bebek"
		    When user click "Buat Baru" in add new room type pop up and click next
		    And user see next button disable
		    And user input room type with "  "
		    Then user see error message "Anda belum mengisi nama tipe kamar ini." under room type field
		    And user see next button disable
		    When user input room type with "Tipe VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIP VVIPPm"
		    Then user see error message "Tidak boleh lebih dari 100 karakter." under room type field
		    When user input room type with "a"
		    Then user see error message "Tidak boleh kurang dari 2 karakter." under room type field
		    When user input room type with "()"
		    Then user see error message "Tidak boleh menggunakan karakter spesial seperti / ( ) - . , ’ dan “" under room type field
		    And user clicks on kost type icon "female"
		    When user input room type with "Tipe v"
		    Then user see error message "Tipe kamar tidak boleh sama." under room type field
	@TEST_DOM-2199 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Create new kos with PLM and select room type "Tipe A" from list room type && input invalid value room type
		Given user click add another type from kos "Kos oke bebek"
		    When user click "Tipe o" in add new room type pop up and click next
		    And user see next button disable
		    And user input room type with "  " in pop up
		    Then user see error message "Tipe kamar tidak boleh kosong." under room type field
		    And user see next button disable
		    When user input room type with "Tipe o" in pop up
		    Then user see error message "Tipe kamar tidak boleh sama." under room type field
#	@TEST_DOM-2198 @Automated @web-covered
#	Scenario: [Form add New Kost][Harga] Create new kost with status draft then delete it
#		Given user click add new kos button
#		    And user clicks checkbox room type
#		    And user fills kost name field with "Kost Draft AutoJava " and random text
#		    And user input room type with random text
#		    And user clicks on kost type icon "mix"
#		    And user fill kos description with "kos harusnya jadi draft"
#		    And user set kos rules :
#		      | Laundry |
#		    And user upload kos rules photo
#		    And user select kos year built "2020"
#		    And user clicks checkbox administrator kos
#		    When user input administrator name with "Omaiwa"
#		    And user input administrator phone with "089992223456"
#		    And user input other notes "Akan dihapus setelah terbuat"
#		    And user clicks on next button in bottom of add kos page
#		    And user click back button on device
#		    And user click back button on device
#		    And user click back button on device
#		    And user click back button on device
##		    And user navigates to "mamikos /"
#		    Then user see kos with name "Kost Draft AutoJava" and random text, status "Draft" and type "Kos Campur"
#		    And user see kos photo src contains "/assets/dummy-photo.png"
#		    And user see text "Iklan kos ini belum tampil di halaman pencarian kos." below complete kos data
#		    # Delete newly created kos draft
#		    When user delete first kos on the list
#		    Then user doesn't see kos with name "Kost Draft AutoJava" and random text, status "Draft" and type "Kos Campur" in first list
	@TEST_DOM-2197 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Check function pop up confirmation on screen form create kos
		Given user click add new kos button
		    And user fills kost name field with "Kost Coba AutoJava " and random text
		    When user click back button on device
		    Then user see pop up confirmation request attention
		    When user click continue input data on pop up
		    And user clicks on kost type icon "female"
		    And user click back button on device
		    And user click button Move Page in pop up
		    And user click add new kos button
		    And user fills kost name field with "Kost Pria AutoJava " and random text
		    And user clicks on kost type icon "male"
		    And user fill kos description with "kos terbaik di mamikos"
		    And user select kos year built "2020"
		    And user set kos rules :
		      | ATM |
		    And user clicks on next button in bottom of add kos page
		    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user click back button on device
		    And user click button Move Page in pop up
		    And user clicks on next button in bottom of add kos page
		    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user click back button on device
		    And user click button Move Page in pop up
		    And user fills kost name field with "Kost Pria AutoJava " and random text
		    And user clicks on next button in bottom of add kos page
			And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks next when photo submission pop up appear
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user clicks on next button in bottom of add kos page
		    And user check facilities under "Fasilitas Umum"
		      | Kompor |
		    And user click back button on device
		    And user click continue input data on pop up
		    And user check facilities under "Fasilitas Umum"
		      | CCTV |
		    And user check facilities under "Fasilitas Kamar"
		      | Asbak       |
		    And user check facilities under "Fasilitas Kamar Mandi"
		      | Air panas   |
		      | Shower      |
		    And user check facilities under "Parkir"
		      | Parkir Sapi |
		    And user clicks on next button in bottom of add kos page
		    And user clicks on kost size "3 x 3"
		    And user click back button on device
		    And user click continue input data on pop up
		    And user clicks on kost size "3 x 4"
		    And user clicks on next button in bottom of add kos page
		    And user clicks on kost size "3 x 4"
		    And user input "5" to field total room
		    And user clicks on next button in bottom of add kos page
		    And user input monthly price with "500000" in add kos page
		    And user click back button on device
		    And user click continue input data on pop up
		    And user input monthly price with "700000" in add kos page
		    And user click back button on device
		    And user click button Move Page in pop up
		    And user clicks on kost size "3 x 3"
		    And user clicks on next button in bottom of add kos page
		    And user input monthly price with "500000" in add kos page
		    And user clicks on next button in bottom of add kos page
		    And user click done in success page
		    Then user see kos with name "Kost Pria AutoJava" and random text, status "Diperiksa Admin" and type "Kos Putra"
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost owner menu
		    And user search kos name "Kost Pria AutoJava" in admin kos owner page
		    And user delete the kos in admin kos owner
	@TEST_DOM-2196 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Create new kos from Add New when user already active mamipay && all bbk kos active or has BBK Waiting for Verif
		Given user click add new kos button
		    And user clicks checkbox room type
		    And user fills kost name field with "Kost Wanita AutoJava " and random text
		    And user input room type with random text
		    And user clicks on kost type icon "female"
		    And user fill kos description with "kos terbaik hari raya"
		    And user set kos rules :
		      | Laundry  |
		      | Security |
		    And user upload kos rules photo
		    And user select kos year built "2020"
		    And user clicks checkbox administrator kos
		    When user input administrator name with "Nanik"
		    And user input administrator phone with "083333333777"
		    And user input other notes "Tidak menerima alien dan Ghoul"
		    And user clicks on next button in bottom of add kos page
		    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user input address note "Dekat rumah nanas bikini bottom"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user insert photo image to "Foto kamar mandi"
		    And user insert photo image to "Tambahan foto lain? boleh dong"
		    And user clicks on next button in bottom of add kos page
		    And user check facilities under "Fasilitas Umum"
		      | Air Jernih  |
		      | Kompor      |
		    And user check facilities under "Fasilitas Kamar"
		      | Kasur       |
		    And user check facilities under "Fasilitas Kamar Mandi"
		      | Air panas   |
		      | Shower      |
		    And user check facilities under "Parkir"
		      | Parkir Sapi |
		    And user clicks on next button in bottom of add kos page
		    And user clicks on kost size "3 x 4"
		    And user input "15" to field total room
		    And user input "10" to total available room
		    And user clicks on next button in bottom of add kos page
		    And user clicks checkbox minimum rent duration
		    And user select minimum rent duration "Min. 1 Hari"
		    And user click checkbox rent price other than monthly
		    And user input monthly price with "300000" in add kos page
		    And user input daily price with "50000" in add kos page
		    And user input weekly price with "200000" in add kos page
		    And user input three monthly price with "800000" in add kos page
		    And user input six monthly price with "1700000" in add kos page
		    And user input yearly price with "3000000" in add kos page
		    And user clicks checkbox additional price
		    And user insert cost name "Cuci Baju"
		    And user insert total cost "10000"
		    And user input deposit with "100000"
		    And user clicks checkbox down payment
		    And user select down payment percentage "30%"
		    And user input fine with "500000"
		    And user select payment expired date after "3" "Minggu"
		    And user input amount denda "50000"
		    And user clicks on next button in bottom of add kos page
		    And user click done in success page
		    Then user see kos with name "Kost Wanita AutoJava" and random text, status "Diperiksa Admin" and type "Kos Putri"
		    # Delete newly created kos in admin
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost owner menu
		    And user search kos name "Kost Wanita AutoJava" in admin kos owner page
		    And user delete the kos in admin kos owner
	@TEST_DOM-2195 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Create kos from existing type when user already active mamipay && all bbk kos active or has BBK Waiting for Verif
		Given user click add another type from kos "Kos oke bebek"
		    When user click "Tipe c" in add new room type pop up and click next
		    And user input room type with random text in add kos data pop up
		    And user clicks on next button in bottom of add kos page
		    And user click edit selesai button
		    And user click Edit in Facilities
		    And user uncheck facilities under "Fasilitas Kamar"
		      | AC     |
		    And user click edit selesai button
		    And user clicks on lanjutkan button in bottom of add kos page
		    And user click done in success page
		    Then user see kos with name "Kos oke bebek" and random text, status "Diperiksa Admin" and type "Kos Campur"
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost owner menu
		    And user search kos name "Kos oke bebek" in admin kos owner page
		    And user delete the kos in admin kos owner
	@TEST_DOM-2194 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Create new room type from "Buat baru" when user already active mamipay && all bbk kos active or has BBK Waiting for Verif
		Given user click add another type from kos "Kos Testing 123"
		    When user click "Buat Baru" in add new room type pop up and click next
		    And user input room type with random text
		    And user clicks on kost type icon "male"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user insert photo image to "Foto kamar mandi"
		    And user insert photo image to "Tambahan foto lain? boleh dong"
		    And user clicks on next button in bottom of add kos page
		    And user check facilities under "Fasilitas Kamar"
		      | AC |
		    And user clicks on next button in bottom of add kos page
		    And user clicks on kost size "3 x 4"
		    And user input "5" to field total room
		    And user input "5" to total available room
		    And user clicks on next button in bottom of add kos page
		    And user clicks checkbox minimum rent duration
		    And user select minimum rent duration "Min. 1 Hari"
		    And user click checkbox rent price other than monthly
		    And user input monthly price with "300000" in add kos page
		    And user input daily price with "50000" in add kos page
		    And user input weekly price with "200000" in add kos page
		    And user input three monthly price with "800000" in add kos page
		    And user input six monthly price with "1700000" in add kos page
		    And user input yearly price with "3000000" in add kos page
		    And user clicks checkbox additional price
		    And user insert cost name "Cuci Baju"
		    And user insert total cost "10000"
		    And user input deposit with "100000"
		    And user clicks checkbox down payment
		    And user select down payment percentage "30%"
		    And user input fine with "500000"
		    And user select payment expired date after "3" "Minggu"
		    And user input amount denda "50000"
		    And user clicks on next button in bottom of add kos page
		    And user click done in success page
		    Then user see kos with name "Kos Testing 123" and random text, status "Diperiksa Admin" and type "Kos Putra"
		    # Delete newly created kos in admin
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost owner menu
		    And user search kos name "Kos Testing 123" in admin kos owner page
		    And user delete the kos in admin kos owner
	@TEST_DOM-2191 @Automated @web-covered
	Scenario: [Form add New Kost][Harga]Add new kos and without input mandatory field in some entity on screen harga
		Given user click add new kos button
		    When user fills kost name field with "Kost Campur Auto " and random text
		    And user clicks on kost type icon "male"
		    And user fill kos description with "kos terbaik sejagad raya"
		    And user select kos year built "2011"
		    And user clicks on next button in bottom of add kos page
	     	And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user input address note "Dekat rumah nanas bikini bottom"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user clicks on next button in bottom of add kos page
		    And user uncheck facilities under "Fasilitas Umum"
		      | Air Jernih |
		      | Kompor     |
		    And user uncheck facilities under "Fasilitas Kamar"
		      | Kasur     |
		    And user uncheck facilities under "Fasilitas Kamar Mandi"
		      | Air panas |
		      | Shower    |
		    And user clicks on next button in bottom of add kos page
		    And user clicks on kost size "3 x 3"
		    And user input "7" to field total room
		    And user clicks on next button in bottom of add kos page
		    And user click checkbox rent price other than monthly
		    And user input monthly price with "0" in add kos page
		    And user input daily price with "0" in add kos page
		    And user input weekly price with "0" in add kos page
		    And user input three monthly price with "0" in add kos page
		    And user input six monthly price with "0" in add kos page
		    And user input yearly price with "0" in add kos page
		    And user clicks checkbox additional price
		    And user input deposit with "0"
		   # And user input fine with "0"
		    Then user see warning message in "Harga Per Bulan" is "Harga per bulan min. Rp50.000 dan maks. Rp100.000.000."
		    And user see warning message in "Harga Per Hari" is "Harga per hari min. Rp10.000 dan maks. Rp10.000.000."
		    And user see warning message in "Harga Per Minggu" is "Harga per minggu min. Rp50.000 dan maks. Rp50.000.000."
		    And user see warning message in "Harga Per 3 Bulan" is "Harga per 3 bulan min. Rp100.000 dan maks. Rp100.000.000."
		    And user see warning message in "Harga Per 6 Bulan" is "Harga per 6 bulan min. Rp100.000 dan maks. Rp100.000.000."
		    And user see warning message in "Harga Per Tahun" is "Harga per tahun min. Rp100.000 dan maks. Rp100.000.000."
		    And user see warning message in "Ada Biaya Tambahan?" is "Nama/total biaya masih kosong."
		    And user see warning message in "Ada Deposit" is "Harga deposit masih kosong."
		  #  And user see warning message in "Jumlah Denda" is "Jumlah Denda masih kosong."
		    # Delete newly created kos draft
		    And user navigates to "owner /ownerpage/kos"
		    And user delete first kos on the list
	@TEST_DOM-2192 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Input section data kost without input mandatory field
		Given user click add new kos button
		    When user clicks checkbox room type
		    Then user see next button disable
		    When user input kos name with "Kost salsa"
		    And user input room type with "Tipe A"
		    Then user see next button disable
		    And user clicks on kost type icon "female"
		    Then user see next button disable
		    And user fill kos description with "kos terbaik sejagad raya"
		    Then user see next button disable
		    And user select kos year built "2020"
		    And user clicks checkbox administrator kos
		    Then user see next button disable
		    When user input administrator name with "Nanik"
		    Then user see next button disable
	@TEST_DOM-2193 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Add new kos and input name kos already exist
		Given user click add new kos button
		    When user fills kost name field with "Kos oke bebek"
		    And user clicks on kost type icon "male"
		    Then user see validation message for existing kost name "Nama ini telah digunakan kos lain." in kost name field
	@TEST_DOM-2208 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Delete a "Rejected" kos
		Given user click add new kos button
		    And user clicks checkbox room type
		    And user fills kost name field with "Kost Rejected Del " and random text
		    And user input room type with random text
		    And user clicks on kost type icon "female"
		    And user fill kos description with "kos will reject then deleted"
		    And user set kos rules :
		      | Laundry  |
		    And user select kos year built "2020"
		    And user clicks on next button in bottom of add kos page
		    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user input address note "Dekat rumah nanas bikini bottom"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user insert photo image to "Foto kamar mandi"
		    And user clicks on next button in bottom of add kos page
		    And user check facilities under "Fasilitas Umum"
		      | Kompor  |
		    And user check facilities under "Fasilitas Kamar"
		      | Kasur    |
		    And user check facilities under "Fasilitas Kamar Mandi"
		      | Shower |
		    And user clicks on next button in bottom of add kos page
		    And user clicks on kost size "3 x 4"
		    And user input "15" to field total room
		    And user input "10" to total available room
		    And user clicks on next button in bottom of add kos page
		    And user input monthly price with "500000" in add kos page
		    And user clicks on next button in bottom of add kos page
		    And user click done in success page
		    Then user see kos with name "Kost Rejected Del" and random text, status "Diperiksa Admin" and type "Kos Putri"
		    # Reject kos in admin
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost owner menu
		    And user search kos name "Kost Rejected Del" in admin kos owner page
		    And user click reject button in admin kos owner page
		    And user click checkbox "Posisi di peta kurang sesuai" in reject reason page below "Alamat Kos"
		    And user click reject button in kos owner reject reason
		    Then user click send in send reject pop up
		    # Delete rejected kos in Web
		    And user navigates to "mamikos /"
		    And user logs out as a Owner user
		    And user clicks on Enter button
		    And user fills out owner login as "master" and click on Enter button
		    And user click button close popup
		    And user click left menu Property Saya
		    And user click Kos button
		    And user clicks button find your kos here
		    And input name kos "Kost Rejected Del"
		    And user clicks kos name from result
		    And user delete first kos on the list
	@TEST_DOM-2207 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Add new kost with condition user wants to duplicate tipe kamar from old kost && Data Tahun Kos Dibangun, Deskripsi Kos tidak ada
		Given user click add another type from kos "Kos oke bebek"
		    When user click "Tipe c" in add new room type pop up and click next
		    And user input room type with random text in add kos data pop up
		    And user clicks on next button in bottom of add kos page
		    Then user see kos description is disabled
		    And user see kos year built is disabled
		    When user clicks on next button in bottom of add kos page
		    And user navigates to "owner /ownerpage/kos"
		    Then user see kos with name "Kos oke bebek" and random text, status "Diperiksa Admin" and type "Kos Campur"
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost owner menu
		    And user search kos name "Kos oke bebek" in admin kos owner page
		    And user delete the kos in admin kos owner
	@TEST_DOM-2206 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Create from kos with status draft && all bbk kos active or has BBK Waiting for Verif
		Given user click add new kos button
		    And user clicks checkbox room type
		    And user fills kost name field with "Kost DraftMAct " and random text
		    And user input room type with random text
		    And user clicks on kost type icon "female"
		    And user fill kos description with "kos harusnya jadi draft then kos auto"
		    And user set kos rules :
		      | Laundry |
		    And user upload kos rules photo
		    And user select kos year built "2020"
		    And user clicks checkbox administrator kos
		    When user input administrator name with "Abababa"
		    And user input administrator phone with "08545432133334"
		    And user input other notes "Akan dihapus setelah terbuat dari automation"
		    And user clicks on next button in bottom of add kos page
		    And user navigates to "mamikos /ownerpage/kos"
		    Then user see kos with name "Kost DraftMAct" and random text, status "Draft" and type "Kos Putri"
		    When user click complete kos data in first kos list
		    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user input address note "Dekat rumah si bola bali bokooo"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user insert photo image to "Foto kamar mandi"
		    And user insert photo image to "Tambahan foto lain? boleh dong"
		    And user clicks on next button in bottom of add kos page
		    And user check facilities under "Fasilitas Umum"
		      | Kompor |
		    And user check facilities under "Fasilitas Kamar"
		      | Kasur |
		    And user check facilities under "Fasilitas Kamar Mandi"
		      | Shower |
		    And user check facilities under "Parkir"
		      | Parkir Sapi |
		    And user clicks on next button in bottom of add kos page
		    And user clicks on kost size "3 x 4"
		    And user input "10" to field total room
		    And user input "6" to total available room
		    And user clicks on next button in bottom of add kos page
		    And user input monthly price with "500000" in add kos page
		    And user clicks on next button in bottom of add kos page
		    And user click done in success page
		    Then user see kos with name "Kost DraftMAct" and random text, status "Diperiksa Admin" and type "Kos Putri"
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost owner menu
		    And user search kos name "Kost DraftMAct" in admin kos owner page
		    And user delete the kos in admin kos owner
	@TEST_DOM-2205 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Update and delete regulations kos
		Given user click add new kos button
		    And user fills kost name field with "Kost Rulecrud " and random text
		    And user clicks on kost type icon "female"
		    And user fill kos description with "kos badadam badadam"
		    And user set kos rules :
		      | Laundry  |
		      | Security |
		    And user select kos year built "2011"
		    And user clicks on next button in bottom of add kos page
	     	And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user input address note "Dekat rumah nanas bikini bottom"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user upload kos rules photo
		    And user click upload kos rules photo
		    Then user see button see photo, button change photo, and button delete photo
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks delete photo and see photo deleted in kos rule
		    And user upload different kos rule photo and see kos rule photo is changed
	@TEST_DOM-2204 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Delete an "Active" kos
		Given user click add new kos button
		    When user fills kost name field with "Kost Tester" and random text
		    And user clicks on kost type icon "female"
		    And user fill kos description with "kos terbaik hari ini mau didelete"
		    And user set kos rules :
		      | Security |
		    And user select kos year built "2020"
		    And user clicks on next button in bottom of add kos page
	    	And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user insert photo image to "Foto kamar mandi"
		    And user clicks on next button in bottom of add kos page
		    And user check facilities under "Fasilitas Umum"
		      | Kompor |
		    And user check facilities under "Fasilitas Kamar"
		      | Kasur |
		    And user check facilities under "Fasilitas Kamar Mandi"
		      | Shower |
		    And user clicks on next button in bottom of add kos page
		    And user clicks on kost size "3 x 4"
		    And user input "5" to field total room
		    And user input "4" to total available room
		    And user clicks on next button in bottom of add kos page
		    And user input monthly price with "300000" in add kos page
		    And user clicks on next button in bottom of add kos page
		    And user click done in success page
		 #   Then user see kos with name "KostNew WillDelete" and random text, status "Diperiksa Admin" and type "Kos Putri"
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost owner menu
		    And user search kos name "Kost Tester" in admin kos owner page
		    And user verify the kos in admin kos owner
		    And user navigates to "mamikos /"
		    And user logs out as a Owner user
		    And user clicks on Enter button
		    And user fills out owner login as "master" and click on Enter button
		    And user navigates to "mamikos /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kost Tester"
		    And user clicks kos name from result
		    Then user search kos with name "Kost Tester" and random text, status "Aktif" and type "Kos Putri"
		    When user click Lihat Selengkapnya button
		    And user delete first kos on the list
#		    Then user doesn't see kos with name "Kost Tester" and random text, status "Aktif" and type "Kos Putri" in first list
	@TEST_DOM-2203 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Add new kos withot select facilities and check warning
		Given user click add new kos button
		    And user fills kost name field with "Kost Price " and random text
		    And user clicks on kost type icon "male"
		    And user fill kos description with "kos price invalid high low"
		    And user select kos year built "2011"
		    And user clicks on next button in bottom of add kos page
	    	And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user clicks on next button in bottom of add kos page
		    When user check facilities under "Fasilitas Umum"
		      | Air Jernih |
		    And user check facilities under "Fasilitas Kamar"
		      | Kasur |
		    And user check facilities under "Fasilitas Kamar Mandi"
		      | Air panas |
		    And user uncheck facilities under "Fasilitas Umum"
		      | Air Jernih |
		    And user uncheck facilities under "Fasilitas Kamar"
		      | Kasur |
		    And user uncheck facilities under "Fasilitas Kamar Mandi"
		      | Air panas |
		    Then user see error message "Pilih Fasilitas", "Pilih minimal 1 fasilitas" under "Fasilitas Umum"
		    And user see error message "Pilih Fasilitas", "Pilih minimal 1 fasilitas" under "Fasilitas Kamar"
		    And user see error message "Pilih Fasilitas", "Pilih minimal 1 fasilitas" under "Fasilitas Kamar Mandi"
		    And user see next button disable
	@TEST_DOM-2202 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Check function pop up confirmation on screen update available room
		Given user click add new kos button
		    And user fills kost name field with "Kost PopUp RoomAllot " and random text
		    And user clicks on kost type icon "male"
		    And user fill kos description with "kos room invalid text"
		    And user select kos year built "2011"
		    And user clicks on next button in bottom of add kos page
		    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user clicks on next button in bottom of add kos page
		    And user uncheck facilities under "Fasilitas Umum"
		      | Air Jernih |
		    And user check facilities under "Fasilitas Umum"
			  | CCTV |
		    And user uncheck facilities under "Fasilitas Kamar"
		      | Kasur |
		    And user uncheck facilities under "Fasilitas Kamar Mandi"
		      | Shower |
		    And user clicks on next button in bottom of add kos page
		    And user clicks on kost size "3 x 3"
		    And user input "1" to field total room
		    When user click manage room availability button
		    And user fill room name or number in room allotment page with "Aaaaaaaa"
		    And user click back button on device
		    Then user see pop up confirmation request attention
		    When user click back on pop up attention
		    And user fill room name or number in room allotment page with "1Aaaaaaab"
		    And user click continue input data on pop up
		    And user click done in room availability
		    And user click back button on device
		    When user click continue input data on pop up
		    And user clicks on next button in bottom of add kos page
		    And user click back button on device
		    And user click manage room availability button
		    And user fill room floor with "11111"
		    And user click back button on device
		    And user click back on pop up attention
		    And user fill room floor with "22222"
		    And user click back button on device
		    When user click manage room availability button
		    And user clicks on next button in bottom of add kos page
		    When user click continue input data on pop up
		    And user clicks on next button in bottom of add kos page
		    And user input monthly price with "500000" in add kos page
		    And user click back button on device
		    When user click back on pop up attention
		    And user click back button on device
		    And user click button Move Page in pop up
		    And user clicks on next button in bottom of add kos page
		    When user click manage room availability button
		    And user see first room name or number is "1"
		    And user see first floor name or number is "1"
		    And user see first room already inhabited checkbox is "checked"
		    When user fill room name or number in room allotment page with "2Aaaaaaab"
		    And user fill room floor with "99"
		    And user click done in room availability
		    When user click manage room availability button
		    And user see first room name or number is "2Aaaaaaab"
		    And user see first floor name or number is "99"
		    And user click done in room availability
		    And user clicks on next button in bottom of add kos page
		    And user input monthly price with "500000" in add kos page
		    And user clicks on next button in bottom of add kos page
		    And user navigates to "owner /ownerpage/kos"
		    And user delete first kos on the list
	@TEST_DOM-2201 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] pdate room availability with invalid value
		Given user click add new kos button
		    And user fills kost name field with "Kost RoomInval " and random text
		    And user clicks on kost type icon "male"
		    And user fill kos description with "kos room invalid text"
		    And user select kos year built "2011"
		    And user clicks on next button in bottom of add kos page
	    	And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user clicks on next button in bottom of add kos page
		    And user uncheck facilities under "Fasilitas Umum"
		      | Air Jernih |
		    And user uncheck facilities under "Fasilitas Kamar"
		      | Kasur |
		    And user uncheck facilities under "Fasilitas Kamar Mandi"
		      | Air panas |
		    And user clicks on next button in bottom of add kos page
		    When user clicks on kost size "3 x 3"
		    # Scenario: Without input mandatory field and input invalid value in some entity on screen ketersediaan kamar
		    And user input "4" to field total room
		    And user input "7" to total available room
		    Then user see error message "Jumlah kamar tersedia tidak boleh lebih dari total kamar." under total available room
		    When user input "0" to field total room
		    And user input " " to total available room
		    Then user see error message "Jumlah total kamar tidak boleh kosong." under total room
		    And user see error message "Jumlah kamar tersedia tidak boleh kosong." under total available room
		    When user input "3,5" to field total room
		    And user input "2,2" to total available room
		    Then user see error message "Hanya boleh menggunakan angka." under total room
		    And user see error message "Hanya boleh menggunakan angka." under total available room
		    When user input "3" to field total room
		    And user input "2" to total available room
		    When user click manage room availability button
		    And user fill room name or number in room allotment page with "  "
		    Then user see error message "Nomor/nama masih kosong." under room type field
		    When user fill room name or number in room allotment page with "2"
		    Then user see error message "Nomor/nama sudah dipakai kamar lain." under room type field
		    When user fill room name or number in room allotment page with "123456789012345678901234567890123456789012345678901"
		    Then user see error message "Maksimal 50 karakter." under room type field
		    When user fill room name or number in room allotment page with "1"
		    And user fill room floor with "123456789012345678901234567890123456789012345678901"
		    Then user see error message "Maksimal 50 karakter." under floor field
	@TEST_DOM-2550 @Automated @web-covered
	Scenario: [Form add New Kost][Harga] Update room availability with invalid value
		Given user click add new kos button
		    And user fills kost name field with "Kost RoomInval " and random text
		    And user clicks on kost type icon "male"
		    And user fill kos description with "kos room invalid text"
		    And user select kos year built "2011"
		    And user clicks on next button in bottom of add kos page
	    	And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
		    And user clicks on next button in bottom of add kos page
		    And user clicks next when photo submission pop up appear
		    And user insert photo image to "Foto bangunan tampak depan"
		    And user insert photo image to "Foto tampilan dalam bangunan"
		    And user insert photo image to "Foto tampak dari jalan"
		    And user clicks on next button in bottom of add kos page
		    And user insert photo image to "Foto depan kamar"
		    And user insert photo image to "Foto dalam kamar"
		    And user clicks on next button in bottom of add kos page
		    And user uncheck facilities under "Fasilitas Umum"
		      | Air Jernih |
		    And user uncheck facilities under "Fasilitas Kamar"
		      | Kasur |
		    And user uncheck facilities under "Fasilitas Kamar Mandi"
		      | Air panas |
		    And user clicks on next button in bottom of add kos page
		    When user clicks on kost size "3 x 3"
		    And user input "4" to field total room
		    And user input "7" to total available room
		    Then user see error message "Jumlah kamar tersedia tidak boleh lebih dari total kamar." under total available room
		    When user input "0" to field total room
		    And user input " " to total available room
		    Then user see error message "Jumlah total kamar tidak boleh kosong." under total room
		    And user see error message "Jumlah kamar tersedia tidak boleh kosong." under total available room
		    When user input "3,5" to field total room
		    And user input "2,2" to total available room
		    Then user see error message "Hanya boleh menggunakan angka." under total room
		    And user see error message "Hanya boleh menggunakan angka." under total available room
		    When user input "3" to field total room
		    And user input "2" to total available room
		    When user click manage room availability button
		    And user fill room name or number in room allotment page with "  "
		    Then user see error message "Nomor/nama masih kosong." under room type field
		    When user fill room name or number in room allotment page with "2"
		    Then user see error message "Nomor/nama sudah dipakai kamar lain." under room type field
		    When user fill room name or number in room allotment page with "123456789012345678901234567890123456789012345678901"
		    Then user see error message "Maksimal 50 karakter." under room type field
		    When user fill room name or number in room allotment page with "1"
		    And user fill room floor with "123456789012345678901234567890123456789012345678901"
		    Then user see error message "Maksimal 50 karakter." under floor field
