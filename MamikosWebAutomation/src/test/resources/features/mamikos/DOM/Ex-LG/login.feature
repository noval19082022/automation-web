@regression @loginOwner @essentiatest
Feature: Owner - Login

  @loginOwner
  Scenario: Login with valid credentials
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    Then user redirected to "owner page"

  @invalidPassword
    Scenario: Login with invalid password
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login with invalid "password" and click on Enter button
    Then user see the form input error "Nomor dan password tidak sesuai"

  @cancelLogin
  Scenario: Owner Want to cancel login
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user click button pemilik kost
    And user verify login form owner
    And user click back button in login owner
    And user click button close login form
    Then user verify login form close