@regression @owner @newflowregisterowner
Feature: New Flow Register Owner

  @navigateToRegisterPage
  Scenario: New Flow Register Owner - Navigate To Register Page
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    Then user redirected to "/register-pemilik?source=homepage"

  @noinputdata
  Scenario: New Flow Register Owner - No Input Data
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    Then button daftar will be disable

#     @openCaptchaSection
#     Scenario: New Flow Register Owner - Fill form with valid data and open captcha box
#       Given user navigates to "mamikos /"
#       When user clicks on Enter button
#       And user clicks on Register button
#       And user fills out registration form without click register "Rheza Haryo Hanggara", "0812345670001", "rheza@mamikos.com", "asdasd123"
#       Then user verify captcha box

  @invalidNumber
  Scenario: New Flow Register Owner - Fill form with valid data except phone number
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "099999999999", "rheza@mamikos.com", "asdasd123"
    And user fills out registration form without click register "Eko Menggolo", "0123456789", "eko@mamikos.com", "qwerty123"
    Then user verify error messages
      | Nomor handphone harus diawali dengan 08. |

#      @clickRegisterTwice
#      Scenario: New Flow Register Owner - Fill form with valid data and click register twice
#        Given user navigates to "mamikos /"
#        When user clicks on Enter button
#        And user clicks on Register button
#        And user fills out registration form with "Rheza Haryo Hanggara", "082144651880", "rheza@mamikos.com", "asdasd123"
#        And user click Register on registration form
#        Then user verify captcha error messages

  @showPasswordInputOwner @LG-9360
  Scenario: New Flow Register Owner - Fill form with valid data and show password field
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "0812345670001", "rheza@mamikos.com", "asdasd123"
    And user click on show password button
    Then user verify password is equal or more than 8 characters

  @passwordMin8Char
  Scenario: New Flow Register Owner - Fill password input with less than 8 characters
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "0812345670001", "rheza@mamikos.com", "asd"
    Then user verify error messages
      | Password kurang dari 8 karakter |

  @inputEmailnotSurel
  Scenario: New Flow Register Owner - Input Email Not Surel
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "088888888779", "rheza@mamikos.com", "asdasd123"
    And user fills out registration form without click register "Rheza Haryo", "0812345670007", "rheza@mamiteam", "asdf12346"
    And user fills out registration form without click register "Rheza Haryo Hanggara", "0812345670001", "rhezamamikos", "asdf1234"
    Then user verify error messages
      | Gunakan format email seperti: mami@mamikos.com |

  @regEmailWrongFormat
  Scenario: New Flow Register Owner - Input wrong email format
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "088888888779", "rheza@mamikos.com", "asdasd123"
    And user fills out registration form without click register "Rheza Haryo Hanggara", "0812345670001", "rhezamamikos", "asdf1234"
    Then user verify error messages
      | Gunakan format email seperti: mami@mamikos.com |

  @nameLessThan3Char
  Scenario: New Flow Register Owner - Input name less than 3 characters
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "rh", "0821912328", "rheza@mamikos.com", "asdf1234"
    Then user verify error messages
     | Minimal 3 karakter. |

  @emailAlreadyUsed
  Scenario: New Flow Register Owner - Input Email Already Used
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "0812345670001", "rheza@bbb.com", "asdf1234"
    Then user verify error messages
      | Alamat email ini sudah digunakan untuk verifikasi di akun lain. |

  @correctEmail
  Scenario: New Flow Register Owner - Correct email
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "0812345670001", "jutawan@mamikos.com", "asdf1234"
    Then user validate email input

  @inputEmailisDeleted @checkEmailTitle
  Scenario: New Flow Register Owner - Check Email Title and Inputted email is deleted
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    Then user see email title is displayed

  @nameMoreThan25
  Scenario: New Flow Register Owner - Name more than 25 char
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara Aye Aye", "0812345670001", "jutawan@mamikos.com", "asdasd123"
    Then user verify name is equal or more than 25 characters

  @phoneNumberLessThan8Char
  Scenario: New Flow Register Owner - Phone number less than 8 characters
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "0819129", "rheza@hadehade.com", "asdasd123"
    Then user verify error messages
      | Nomor handphone kurang dari 8 karakter. |

  @registerPhoneMoreThan14Char
  Scenario: New Flow Register Owner - Phone number more than 14 characters
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "081113333444449", "rheza@hadehade.com", "asdasd123"
    Then user verify error messages
      | Nomor handphone lebih dari 14 karakter. |

  @phoneNumberAlreadyRegisteredAsOwner
  Scenario: New Flow Register Owner - Phone number already registered owner
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "0812345678999", "rheza@hadehade.com", "asdasd123"
    Then user verify error messages
      | Nomor handphone ini sudah digunakan untuk verifikasi di akun lain. |

  @phoneNumberAlreadyRegisteredAsTenant
  Scenario: New Flow Register Owner - Phone number already registered tenant
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user clicks on Register button
    And user fills out registration form without click register "Rheza Haryo Hanggara", "0898765432166", "rheza@hadehade.com", "asdasd123"
    Then user verify error messages
      | Nomor handphone ini sudah digunakan untuk verifikasi di akun lain. |