@DOM
Feature: Update Harga

	Background:
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user fills out owner login as "master" and click on Enter button

	@TEST_DOM-2511
	Scenario: [WEB][Update Harga] Access page "Update Harga" from entry point kost list when kost status == Inactive with update price
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Mamites Kos coba baru"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user click update kos button
		    And user click see other prices
		    Then user clicks update and user can sees toast on update price as "Harga berhasil diupdate"
		 #   When user refresh page
		    And user click see other prices
		    Then user see daily price is "50000"
		    And user see weekly price is "500000"
		    And user see monthly price is "1900000"
		    And user see three monthly price is "5000000"
		    And user see six monthly price is "10000000"
		    And user see yearly price is "20000000"
		    When user navigates to "mamikos /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Automation Kos"
		    And user clicks kos name from result
		    Then user see kos with name "Automation Kos", status "Aktif" and type "Kos Putra"
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost menu
		    And user search kos name "Automation Kos" in admin kos page
		    Then user thanos the kos in admin kos page
	@TEST_DOM-2510
	Scenario: [WEB][Update Harga] Access page "Update Harga" from entry point kost list when kost status == Inactive with update price
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kost TesterjTKbQ Ibu Halmahera Barat"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user click update kos button
		    And user click see other prices
		    And user input daily price with "100001"
		    And user input weekly price with "700000"
		    And user input monthly price with "2800000"
		    And user input three monthly price with "7500000"
		    And user input six monthly price with "15000000"
		    And user input yearly price with "30000000"
		    And user clicks update and user can sees toast on update price as "Harga berhasil diupdate"
		  #  When user refresh page
		    And user click update kos button
		    And user click see other prices
		    Then user see daily price is "100001"
		    And user see weekly price is "700000"
		    And user see monthly price is "2800000"
		    And user see three monthly price is "7500000"
		    And user see six monthly price is "15000000"
		    And user see yearly price is "30000000"
		    When user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin consultant"
		    And user access to kost menu
		    And user search kos name "Kose Mamiset Automation" in admin kos page
#		    Then user thanos the kos in admin kos page

	@TEST_DOM-2509
	Scenario: [WEB][Update Harga] Access page "Update Harga" from entry point kost list when kost status == Active without update price
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kost TesterjTKbQ Ibu Halmahera Barat"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user clicks Update Harga at kos
		    And user click see other prices
		    And user memorize daily, weekly, monthly, three monthly, six monthly, and yearly price
		    Then user clicks update and user can sees toast on update price as "Harga berhasil diupdate"
		    And user see daily, weekly, monthly, three monthly, six monthly, and yearly price is same with previous price
	@TEST_DOM-2508
	Scenario: [WEB][Update Harga] Owner can't update price when "promo ngebut" active
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kose Mamiset Automation"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user clicks Update Harga at kos
		    And user click see other prices
		    Then user see infobar in update price with text "Anda sedang mengikuti promo ngebut, harga tidak dapat diubah sampai promo berakhir"
		    When user click monthly price checkbox in edit price
		    And user clicks update and user can sees toast on update price as "Anda sedang mengikuti promo ngebut, harga tidak dapat diubah sampai promo berakhir"
		    And user see monthly price field is disabled
		    When user click annual price checkbox in edit price
		    And user clicks update and user can sees toast on update price as "Anda sedang mengikuti promo ngebut, harga tidak dapat diubah sampai promo berakhir"
		    And user see yearly price field is disabled
		    When user close infobar promo ngebut in update price
		    Then user clicks update and user can sees toast on update price as "Anda sedang mengikuti promo ngebut, harga tidak dapat diubah sampai promo berakhir"
	@TEST_DOM-2507
	Scenario: [WEB][Update Harga] Component screen atur harga
		Given user click on owner popup
		    When user clicks button set the price
		    Then user see button back
		    And user see text "Update Harga"
		    And user see textbox monthly price
		    And user see textlink other prices
		    And user click see other prices
		    And user see textbox daily price, weekly price, 3 monthly price, 6 monthly price, yearly price
		    And user see field other costs mothly, fine fee, deposit fee, DP fee
		    And user see button update price enable
	@TEST_DOM-2506
	Scenario Outline: [WEB][Update Harga] Update Price with Invalid value
		Given user click on owner popup
		    When user clicks button set the price
		    And user click see other prices
		    And user input daily price with "<Daily Price 1>"
		    And user input weekly price with "<Weekly Price 1>"
		    And user input monthly price with "<Monthly Price 1>"
		    And user input three monthly price with "<Three Monthly Price 1>"
		    And user input six monthly price with "<Six Monthly Price 1>"
		    And user input yearly price with "<Yearly Price 1>"
		    Then user see warning daily price with "Harga per hari min. Rp10.000 dan maks. Rp10.000.000."
		    And user see warning weekly price with "Harga per minggu min. Rp50.000 dan maks. Rp50.000.000."
		    And user see warning monthly price with "Harga per bulan min. Rp50.000 dan maks. Rp100.000.000."
		    And user see warning three monthly price with "Harga per 3 bulan min. Rp100.000 dan maks. Rp100.000.000."
		    And user see warning six monthly price with "Harga per 6 bulan min. Rp100.000 dan maks. Rp100.000.000."
		    And user see warning yearly price with "Harga per tahun min. Rp100.000 dan maks. Rp100.000.000."
		    And user see button update price disable
		    And user input daily price with "<Daily Price 2>"
		    And user input weekly price with "<Weekly Price 2>"
		    And user input monthly price with "<Monthly Price 2>"
		    And user input three monthly price with "<Three Monthly Price 2>"
		    And user input six monthly price with "<Six Monthly Price 2>"
		    And user input yearly price with "<Yearly Price 2>"
		    Then user see warning daily price with "Harga per hari min. Rp10.000 dan maks. Rp10.000.000."
		    And user see warning weekly price with "Harga per minggu min. Rp50.000 dan maks. Rp50.000.000."
		    And user see warning monthly price with "Harga per bulan min. Rp50.000 dan maks. Rp100.000.000."
		    And user see warning three monthly price with "Harga per 3 bulan min. Rp100.000 dan maks. Rp100.000.000."
		    And user see warning six monthly price with "Harga per 6 bulan min. Rp100.000 dan maks. Rp100.000.000."
		    And user see warning yearly price with "Harga per tahun min. Rp100.000 dan maks. Rp100.000.000."
		    And user see button update price disable
		    And user input daily price with "<Daily Price 3>"
		    And user input weekly price with "<Weekly Price 3>"
		    And user input monthly price with "<Monthly Price 3>"
		    And user input three monthly price with "<Three Monthly Price 3>"
		    And user input six monthly price with "<Six Monthly Price 3>"
		    And user input yearly price with "<Yearly Price 3>"
		    Then user see warning daily price with "Harga per hari min. Rp10.000 dan maks. Rp10.000.000."
		    And user see warning weekly price with "Harga per minggu min. Rp50.000 dan maks. Rp50.000.000."
		    And user see warning monthly price with "Harga per bulan min. Rp50.000 dan maks. Rp100.000.000."
		    And user see warning three monthly price with "Harga per 3 bulan min. Rp100.000 dan maks. Rp100.000.000."
		    And user see warning six monthly price with "Harga per 6 bulan min. Rp100.000 dan maks. Rp100.000.000."
		    And user see warning yearly price with "Harga per tahun min. Rp100.000 dan maks. Rp100.000.000."
		    And user see button update price disable
		    Examples:
		      | Daily Price 1 | Weekly Price 1 | Monthly Price 1 | Three Monthly Price 1 | Six Monthly Price 1 | Yearly Price 1 | Daily Price 2 | Weekly Price 2 | Monthly Price 2 | Three Monthly Price 2 | Six Monthly Price 2 | Yearly Price 2 | Daily Price 3 | Weekly Price 3 | Monthly Price 3 | Three Monthly Price 3 | Six Monthly Price 3 | Yearly Price 3 |
		      | 0             | 0              | 0               | 0                     | 0                   | 0              | 9999          | 49999          | 49999           | 99999                 | 99999               | 99999          | 10000001      | 50000001       | 100000001       | 100000001             | 100000001           | 100000001      |
	@TEST_DOM-2505
	Scenario Outline: [WEB][Update Harga] Update price kost
		Given user redirected to "owner page"
		    And user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kost TesterjTKbQ Ibu Halmahera Barat"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    When user clicks Update Harga at kos
		    And user click see other prices
		    Then user clicks update and user can sees toast on update price as "Harga berhasil diupdate"
		    And user input daily price with "<Daily Price>"
		    And user input weekly price with "<Weekly Price>"
		    And user input monthly price with "<Monthly Price>"
		    And user input three monthly price with "<Three Monthly Price>"
		    And user input six monthly price with "<Six Monthly Price>"
		    And user input yearly price with "<Yearly Price>"
		    Then user not see warning in daily price
		    And user not see warning in weekly price
		    And user not see warning in monthly price
		    And user not see warning in three monthly price
		    And user not see warning in six monthly price
		    And user not see warning in yearly price
		    Then user clicks update and user can sees toast on update price as "Harga berhasil diupdate"
		   # And user refresh page
		    Then user see daily price is "<Daily Price>"
		    And user see weekly price is "<Weekly Price>"
		    And user see monthly price is "<Monthly Price>"
		    And user see three monthly price is "<Three Monthly Price>"
		    And user see six monthly price is "<Six Monthly Price>"
		    And user see yearly price is "<Yearly Price>"
		    Examples:
		      | Daily Price | Weekly Price | Monthly Price | Three Monthly Price | Six Monthly Price | Yearly Price |
		      | 1000000     | 1500000      | 2500000       | 3000000             | 3500000           | 4000000      |
		      | 150000      | 500000       | 3000000       | 9000000             | 18000000          | 36000000     |
