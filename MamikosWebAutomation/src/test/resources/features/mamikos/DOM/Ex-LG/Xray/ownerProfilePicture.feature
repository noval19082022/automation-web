@DOM
Feature: Owner - Profile Picture

	@TEST_DOM-2284 @Automated @DOM @web-covered
	Scenario: [Setelan Akun][Profile Picture] Profile Picture is null
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login as "master" and click on Enter button
		    And user click on owner popup
		    Then user verify profile picture is null
	@TEST_DOM-2285 @Automated @DOM @web-covered
	Scenario: [Setelan Akun][Profile Picture] Profile Picture is show
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login as "payment" and click on Enter button
		    And user click on owner popup
		    Then user verify profile picture is show
