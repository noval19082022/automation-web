@DOM
Feature: Update Kamar

	Background:
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user fills out owner login as "master" and click on Enter button

	@TEST_DOM-2521
	Scenario: [WEB][Update Room] Access page "Update Kamar" from entry point kos list when kost status == Active by update rooms become unavailable
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kos Testing 123 Tipe B Danurejan Yogyakarta"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user clicks on update kamar button
		    And user enter text "1" on search bar in room allotment and hit enter
		    And user click edit button in first row of the table
		    And user tick already inhabited checkbox
		    And user click update room button in update room pop up
		    Then user can sees toast on update room as "Kosong" "Terisi" "Total Kamar 1"
		    When user enter text "" on search bar in room allotment and hit enter
		    Then user see total room is "20" "Total Kamar 20" in update room page
		    When user filter the room with "Kamar Kosong" in update room page
		    Then user see total room is "19" "Total Kamar 19" in update room page
		    When user filter the room with "Kamar Terisi" in update room page
		    Then user see total room is "1" "Total Kamar 1" in update room page
		    When user click edit button in first row of the table
		    And user tick already inhabited checkbox
		    And user click update room button in update room pop up
		    When user filter the room with "Kamar Kosong" in update room page
		    Then user see total room is "20" "Total Kamar 20" in update room page
		    When user filter the room with "Kamar Terisi" in update room page
		    Then user see total room is "0" "Total Kamar 0" in update room page
	@TEST_DOM-2520
	Scenario: [WEB][Update Room] Access page "Update Kamar" from entry point kos list when kost status == Active by add new rooms and delete it
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kos oke bebek Vviop Depok Sleman"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user clicks on update kamar button
		    And user click add room in room list
		    And user fill room name in room allotment page with "26"
		    And user click update room button in update room pop up
		    Then user can sees toast on update room as "Kosong" "Terisi" "Total Kamar 0"
		    And user see total room is "25" "Total Kamar 25" in update room page
		    When user delete room name or number "26" in room allotment
		    Then user can sees toast on update room as "Kosong" "Terisi" "Total Kamar 0"
		    And user see total room is "24" "Total Kamar 24" in update room page
	@TEST_DOM-2519
	Scenario: [WEB][Update Room] Access page "Update Kamar" from entry point kos list when kost status == Active by update text box "Lantai (Opsional)"
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kos Testing 123 Tipe B Danurejan Yogyakarta"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    When user clicks on update kamar button
		    When user enter text "1" on search bar in room allotment and hit enter
		    And user click edit button in first row of the table
		    And user fill room floor in room allotment page with "1"
		    And user click update room button in update room pop up
		    Then user can sees toast on update room as "Kosong" "Terisi" "Total Kamar 0"
		    And user see first floor name or number is in update room page
		    When user click edit button in first row of the table
		    And user fill room floor in room allotment page with "abcd"
		    And user click update room button in update room pop up
		    Then user can sees toast on update room as "Kosong" "Terisi" "Total Kamar 0"
	@TEST_DOM-2518
	Scenario: [WEB][Update Room] Access page "Update Kamar" from entry point kost list when kost status == Active by update text box "Nomor/ Nama Kamar?"
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kos Testing 123 Tipe B Danurejan Yogyakarta"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user clicks on update kamar button
		    And user enter text "1" on search bar in room allotment and hit enter
		    And user click edit button in first row of the table
		    And user fill room name in room allotment page with "001A"
		    And user click update room button in update room pop up
		    Then user can sees toast on update room as "Kosong" "Terisi" "Total Kamar 0"
		  #  And user see first room name or number is "001A" "1001A001A001A001A001A" in update room page
			And user see first room name or number is in update room page
		    When user click edit button in first row of the table
		    And user fill room name in room allotment page with "1"
		    And user click update room button in update room pop up
		    Then user can sees toast on update room as "Kosong" "Terisi" "Total Kamar 0"
	@TEST_DOM-2517
	Scenario: [WEB][Update Room] Filter on room allotments is always shown when total rooms == 0 or != 0
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kose Putra Automation"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    When user clicks on update kamar button
		    Then user see selected filter is "Semua Kamar" in room allotment page
		    And user see total room is "6" "Total Kamar 6" in update room page
		    When user filter the room with "Kamar Kosong" in update room page
		    Then user see total room is "6" "Total Kamar 6" in update room page
		    And user filter the room with "Kamar Terisi" in update room page
		    Then user see total room is "0" "Total Kamar 0" in update room page
		    And user see room list is empty in room allotment page
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And user click kos "Kose Full Automation" in update price list
		    And user click Lihat Selengkapnya button
		    And user clicks on update kamar button
		    And user filter the room with "Kamar Kosong" in update room page
		    Then user see total room is "0" "Total Kamar 0" in update room page
		    And user see room list is empty in room allotment page
	@TEST_DOM-2516
	Scenario: [WEB][Update Room]  Check box "Sudah Berpenghuni" is set to define room status when kost not in Goldplus Level
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Property Automation"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    When user clicks on update kamar button
		    Then user see total room is "5" "Total Kamar 5" in update room page
		    When user click edit button in first row of the table
		    And user tick already inhabited checkbox
		    And user click update room button in update room pop up
		    Then user can sees toast on update room as "Kosong" "Terisi" "Total Kamar 5"
		    And user see total room is "5" "Total Kamar 4" in update room page
		    When user filter the room with "Kamar Kosong" in update room page
		    Then user see total room is "4" "Total Kamar 4" in update room page
		    When user filter the room with "Kamar Terisi" in update room page
		    Then user see total room is "1" "Total Kamar 0" in update room page
		    When user click edit button in first row of the table
		    And user tick already inhabited checkbox
		    And user click update room button in update room pop up
		    Then user can sees toast on update room as "Kosong" "Terisi" "Total Kamar 0"
		    Then user see total room is "0" "Total Kamar 4" in update room page
		    When user filter the room with "Kamar Terisi" in update room page
		    Then user see total room is "1" "Total Kamar 4" in update room page
	@TEST_DOM-2515
	Scenario: [WEB][Update Room]  Show label goldplus if rooms set as goldplus from room list
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kose Mamiset Automation"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user clicks on update kamar button
		    And user search "GoldPlus" in update room
		    Then user see label "Goldplus" in room name
	@TEST_DOM-2514
	Scenario: [WEB][Update Room]  Text box "Nomor/Nama Kamar?" is edited with invalid value
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kos oke bebek AA Batre Depok Sleman"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user clicks on update kamar button
		    And user enter text "18" on search bar in room allotment and hit enter
		    And user click edit button in first row of the table
		    And user fill room name in room allotment page with "123456789012345678901234567890123456789012345678901"
		    Then user see error message "Maks. 50 karakter." under room name field in update room page
		    When user fill room name in room allotment page with "22"
		    Then user see error message "Nomor/ nama sudah dipakai kamar lain." under room name field in update room page
		    When user fill room name in room allotment page with " "
		    Then user see error message "Nomor/ nama masih kosong." under room name field in update room page
		    When user close update room pop up in room allotment
		    And user enter text "1" on search bar in room allotment and hit enter
		    And user click edit button in first row of the table
		    # Unable to edit "Nama/Nomor Kamar" when room related to contract
		    Then user see room name field is disabled in room allotment
		    And user see button update room is disabled
	@TEST_DOM-2513
	Scenario: [WEB][Update Room] Text box Floor (Optional) is edited with invalid value
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kost Cooling Efect"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user clicks on update kamar button
		    And user enter text "2" on search bar in room allotment and hit enter
		    And user click edit button in first row of the table
		    And user fill room floor in room allotment page with "123456789012345678901234567890123456789012345678901"
		    Then user see error message "Maks. 50 karakter." under floor field in update room page
		    When user close update room pop up in room allotment
		    And user enter text "1" on search bar in room allotment and hit enter
		    And user click edit button in first row of the table
		    # Unable to edit "Lantai (Opsional)" when room related to contract
		    Then user see floor field is disabled
		    And user see button update room is disabled
	@TEST_DOM-2512
	Scenario: [WEB][Update Room] Search function on page room allotments
		Given user redirected to "owner page"
		    When user navigates to "owner /ownerpage/kos"
		    And user clicks button find your kos here
		    And input name kos "Kose Mamiset Automation"
		    And user clicks kos name from result
		    And user click Lihat Selengkapnya button
		    And user clicks on update kamar button
		    When user enter text "3" on search bar in room allotment and hit enter
		    Then user see room list have room name that contains text "3"
		    When user enter text "?,.?" on search bar in room allotment and hit enter
		    Then user see room list is empty in room allotment page
		    When user enter text "sela" on search bar in room allotment and hit enter
		    Then user see room list is empty in room allotment page
