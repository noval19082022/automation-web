@regression @changeOwnerNumber
Feature: Owner Settings - Change Owner Number

  @noInputNumber
  Scenario: Change Owner Number - No input phone number
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "owner gp 1" and click on Enter button
    And user navigates to "owner /ownerpage/settings"
    And user clicks on Change Nomor Handphone button
    Then user see input phone number placeholder "Masukkan nomor handphone"

  @numberRegisteredOwnerNewFlow @LG-9372
  Scenario: Change Owner Number - Input registered owner number new flow
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "owner gp 1" and click on Enter button
    And user navigates to "owner /ownerpage/settings"
    And user clicks on Change Nomor Handphone button
    And user fills phone number owner registered new flow "0891202103"
    And user clicks on Simpan button
    Then user see message error validation "The phone number has already been taken."

  @numberRegisteredOwnerOldFlow
  Scenario: Change Owner Number - Input registered owner number old flow
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "owner gp 1" and click on Enter button
    And user navigates to "owner /ownerpage/settings"
    And user clicks on Change Nomor Handphone button
    And user fills phone number owner registered new flow "081345645601"
    And user clicks on Simpan button
    Then user see message error validation "The phone number has already been taken."

  @resendCodeVerification
  Scenario: Change Owner Number - Resend code verification
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "owner gp 1" and click on Enter button
    And user navigates to "owner /ownerpage/settings"
    And user clicks on Change Nomor Handphone button
    And user fills phone number owner registered new flow "08912021100"
    And user clicks on Simpan button
    And user see otp pop up
    Then user click on resend otp button

  @numberSpecialCharacterAndNumeric
  Scenario: Change Owner Number - Special character and numeric
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "owner gp 1" and click on Enter button
    And user navigates to "owner /ownerpage/settings"
    And user clicks on Change Nomor Handphone button
    And user fills phone number owner registered new flow "08134564!@#$%"
    And user clicks on Simpan button
    Then user see message error validation "Nomor handphone harus diawali dengan 08."
