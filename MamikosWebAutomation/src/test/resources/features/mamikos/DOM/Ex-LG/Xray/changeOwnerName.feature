@DOM
Feature: Change Owner Name

	@TEST_DOM-2215 @Automated @DOM @web-covered
	Scenario: [Setelan Akun][Change Name] Change Owner Name
		Given user navigates to "mamikos /"
		      When user clicks on Enter button
		      And user fills out owner login as "mamipay-notregistered" and click on Enter button
		      And user click on owner popup
		      And user clicks on Owner Settings button
		      When user clicks on Change Name button
		      And user fills out name input with "Change name test"
		      Then user verify username text as "Change name test"
		      When user clicks on Change Name button
		      And user fills out name input with "tiara"
		      Then user verify username text as "tiara"
