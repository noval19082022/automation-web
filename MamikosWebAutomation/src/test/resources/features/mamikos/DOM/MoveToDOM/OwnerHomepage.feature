@regression @homepage @hotfix @LG5

Feature: Homepage

  @navbarAfter
  Scenario: Positive case Check Navbar login as owner
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user click on owner popup
    Then check the header menu display on homepage owner
    And user see username in top right shows as "abcdefghijklmnopqrst"

  @redirectionElementNavbar
  Scenario: Redirection element header after login
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user click on owner popup
    And user click mamikos.com logo
    And user click booking kos button and redirection to "/booking"
    And user click back button in page
    And user click promo ads button and redirection to "/mamiads"
    And user click back button in page
    And user click download app button and redirection to "https://play.google.com/store/apps/details?id=com.git.mami.kos"
    And user click back button in page
    And user click help center button and redirection to "https://help.mamikos.com/pemilik"
    And user click back button in page
    And user click owner dashboard from homepage
    And user click left menu Property Saya
    Then user will be verify dropdown in property saya

  @ownerPage @listingGP
  Scenario: Click "halaman pemilik" will redirect to Owner page
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user click on owner popup
    And user click mamikos.com logo
    When user click profile on header
    Then user see dropdown with button owner page and exit
    When  user click owner page button
    Then user redirected to "owner page"

  @ownerChatCS @listingGP
  Scenario: Click "Chat CS" should open Contact Us Pop Up
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And user click Chat CS button
    Then user see Contact us pop up is appear

  @ownerSubMenu
  Scenario: Sub menu on owner page redirect to the right page
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    Then user redirected to "/ownerpage/kos"
    And user click back button on device
    And user click left menu Property Saya
    When user clicks on Owner Apartment Menu
    Then user redirected to "/ownerpage/apartment"
    And user click back button on device
    When user click mamipoin in owner's menu
    Then user redirected to "owner /mamipoin"
    And user click back button on device
    And user click left menu Kos Management
    When user clicks on Bills Menu
    Then user redirected to "owner /billing-management"
    And user click back button on device
    When user click notification owner button
    Then user see counter, link see all notification and latest 5 notification
    When user click owner username on header
    Then user see owner's name & phone number, text link "Setelan Akun" & "Logout"

  @OwnerDoesntHaveKost @listingGP
  Scenario: Check Add Kost button when owner doesn't have kost
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "mamipay-donthavekost" and click on Enter button
    And user click on owner popup
    When user clicks on Add Kos button
    Then user redirected to "owner /choose-property-type"

  @OwnerSeeWidgetRoomAllotment @listingGP
  Scenario: Check widget room allotment
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user click on owner popup
    And user scroll down homepage
    When user clicks widget set available room
    Then user see list active kos
    And user see button back
    Then user see screen update room
    When user click kos "Kos oke bebek BS9Bz Mlati Sleman" in update room list
    And user click textbox search
    Then user see name or number room
    And user see filter search room
    And user see floor
    And user see button add room
    And user see button edit
    And user see button delete

  @OwnerNameMoreThan30 @listingGP
  Scenario: If profile name more than 30 char show ellipsis
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "mamipay-BBKnotActive" and click on Enter button
    And user click on owner popup
    Then user see username in top right shows as "tiara lapan abcdefghijklmnopqr..."

  @OwnerGreeting @listingGP
  Scenario: Click username direct to setelan akun
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "master" and click on Enter button
    And user click on owner popup
    Then user see user's name "Halo, abcdefghijklmnopqrst" in owner dashboard
    When user click username in owner dashboard
    Then user redirected to "/ownerpage/settings"

  @OwnerProfileHeader @listingGP
  Scenario: Click profile name in top right will open dropdown menu
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "master" and click on Enter button
    And user click on owner popup
    And user click owner username on header
    Then user see owner's name & phone number, text link "Setelan Akun" & "Logout"

  @widgetWaktunyaMengelolaPropertiKosNonActive
  Scenario: Widget Waktunya Mengelola Properti - Kos non active 1 & Apartemen 0
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "mamipay-and-BBK-notRegistered" and click on Enter button
    And user click on owner popup
    Then user see widget waktunya mengelola properti is as expected
      | title                      | subtitle                       |
      | Atur Ketersediaan Kamar    | Mengelola data kamar kos       |
      | Atur Harga                 | Update harga sewa di iklan kos |
      | Daftar ke Booking Langsung | Mengaktifkan fitur Booking     |
      | Penyewa                    | Daftar kontrak penyewa kos     |
      | Tambah Penyewa             | Menambah kontrak penyewa       |
      | Pusat Bantuan              | Info bantuan seputar Mamikos   |

  @widgetWaktunyaMengelolaPropertiKosActive
  Scenario: Widget Waktunya Mengelola Properti - Kos active 1 & Apartemen 0
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "premiumnosaldo" and click on Enter button
    And user click on owner popup
    Then user see widget waktunya mengelola properti is as expected
      | title                   | subtitle                       |
      | Atur Ketersediaan Kamar | Mengelola data kamar kos       |
      | Atur Harga              | Update harga sewa di iklan kos |
      | Ubah Peraturan Masuk Kos| Aturan untuk calon penyewa     |
      | Penyewa                 | Daftar kontrak penyewa kos     |
      | Tambah Penyewa          | Menambah kontrak penyewa       |
      | Pusat Bantuan           | Info bantuan seputar Mamikos   |

  @widgetWaktunyaMengelolaPropertiKos0orVerification
  Scenario Outline: Widget Waktunya Mengelola Properti - Kos active 0/verification & Apartemen 0/1
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "<user>" and click on Enter button
    And user click on owner popup
    Then user see widget waktunya mengelola properti is as expected
      | title           | subtitle                     |
      | Tambah Properti | Buat Kos/Apartemen Anda      |
      | Pusat Bantuan   | Info bantuan seputar Mamikos |
    Examples:
      | user                 |
      | mamipay-donthavekost |
      | kostverification     |
      | premiumkos0apart1    |

  @functionBackatHomeSetprice
   Scenario: Check function back at home widget (Set Price)
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "premium" and click on Enter button
    And user click on owner popup
    And user clicks widget set price
    And user click back button on device
    Then user redirected to "owner page"

  @functionBackatHomeSetAvailableRoom
  Scenario: Check function back at home widget (Set Price)
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "premium" and click on Enter button
    And user click on owner popup
    And user clicks widget set available room
    And user click back button on device
    Then user redirected to "owner page"