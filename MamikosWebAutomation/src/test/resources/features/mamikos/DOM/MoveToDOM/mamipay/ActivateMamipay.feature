@regression @mamipay

  Feature: Activate Mamipay

    @activatemamipay
    Scenario: Activate Mamipay - Positive Flow
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user fills out owner login as "mamipay-notregistered" and click on Enter button
      And user navigates to "owner /kos/booking/register"
      And user clicks on button Next BBK
      And user fill out activate mamipay form with Bank Account Number "09182928329"
      And user fill out active mamipay form with  Bank Owner Name "Rheza Haryo Hanggara"
      And user select bank account with "BCA"
      And user clicks on Terms And Conditions checkbox in Mamipay form
      Then user see button for submitted data activate mamipay

     @bankAccountNumberInvalid @listingGP
     Scenario: Activate Mamipay - Invalid bank account number input
       Given user navigates to "mamikos /"
       When user clicks on Enter button
       And user fills out owner login as "mamipay-notregistered" and click on Enter button
       And user navigates to "owner /kos/booking/register"
       And user clicks on button Next BBK
       And user fill out activate mamipay form with Bank Account Number " "
       Then user see the form input error "Nomor rekening tidak boleh kosong." at form bank account activate mamipay
       And user fill out activate mamipay form with Bank Account Number "ABCHD"
       Then user see the form input error "Hanya diisi dengan angka" at form bank account activate mamipay

     @bankOwnerNameInvalid @listingGP
     Scenario: Activate Mamipay - Invalid bank account name input
       Given user navigates to "mamikos /"
       When user clicks on Enter button
       And user fills out owner login as "mamipay-notregistered" and click on Enter button
       And user navigates to "owner /kos/booking/register"
       And user clicks on button Next BBK
       And user fill out activate mamipay form with Bank Account Name " "
       Then user see form input error "Nama pemilik rekening tidak boleh kosong." at form Bank Account Name activae mamipay
       And user fill out activate mamipay form with Bank Account Name "C"
       Then user see form input error "Minimal 3 Karakter" at form Bank Account Name activae mamipay
       And user fill out activate mamipay form with Bank Account Name "123455"
       Then user see form input error "Hanya diisi dengan huruf" at form Bank Account Name activae mamipay

    @invalidNameRegisterMamipay @listingGP
    Scenario: Activate Mamipay - Invalid Name Register Mamipay
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user fills out owner login as "mamipay-notregistered" and click on Enter button
      And user click on owner popup
      And user click left menu Kos Management
      And user clicks on Manage Booking And Bills Menu
      And user clicks on button Next BBK
      And user input field name with " " at form activate mamipay
      Then user see the form input error "Nama lengkap tidak boleh kosong." at form activate mamipay
      And user input field name with "123456" at form activate mamipay
      Then user see the form input error "Hanya diisi dengan huruf" at form activate mamipay
      And user input field name with "*&^%^" at form activate mamipay
      Then user see the form input error "Hanya diisi dengan huruf" at form activate mamipay
      And user input field name with "AB" at form activate mamipay
      Then user see the form input error "Minimal 3 Karakter" at form activate mamipay
      And user input field name with "Rega Dian Naralia Sari Rega Dian Naralia Sari Rega Dian Naralia Sari Rega Dian Naralia Sari Rega Dian Naralia Sari Rega Dian Naralia Sari" at form activate mamipay
      Then user see the form input error "Maksimal diisi dengan 50 karakter" at form activate mamipay

    @t&cMandatoryInMamipay @listingGP
    Scenario: Terms & Conditions is mandatory fields if want to activate mamipay and booking langsung
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user fills out owner login as "mamipay-notregistered" and click on Enter button
      And user navigates to "owner /kos/booking/register"
      And user clicks on button Next BBK
      And user fill out activate mamipay form with Bank Account Number "09182928329"
      And user fill out active mamipay form with  Bank Owner Name "Rheza Haryo Hanggara"
      And user select bank account with "BCA"
      Then user see mamipay form information "Pastikan data Anda benar dan sesuai, agar uang pembayaran kos dapat ditransfer dengan lancar."
      And user see text "Saya menyetujui Syarat dan Ketentuan fitur Booking Langsung Mamikos" beside checkbox in term and condition mamipay form
      When user clicks on Terms And Conditions link in Mamipay form
      Then page redirect to page "https://help.mamikos.com/post/syarat-dan-ketentuan-bisa-booking-pemilik"
      When user switch to main window
      Then user see button for submitted data activate mamipay is disabled
      When user clicks on Terms And Conditions checkbox in Mamipay form
      Then user see button for submitted data activate mamipay