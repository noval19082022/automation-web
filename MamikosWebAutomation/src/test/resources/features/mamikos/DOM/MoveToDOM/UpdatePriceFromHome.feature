@regression @updatepriceDashboard @listingGP
Feature: Update Price from Dashboard

  Background:
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "owner gp 2" and click on Enter button

  @updatePriceDashboard
  Scenario Outline: Update price from home
    Given user redirected to "owner page"
    And user click on owner popup
    And user click update price in Home
    And user click kos "Kos Fathul Khair Tipe bala bala Jetis Yogyakarta" in update price list
    And user click see other prices
    When user input daily price with "<Daily Price>"
    And user input weekly price with "<Weekly Price>"
    And user input monthly price with "<Monthly Price>"
    And user input three monthly price with "<Three Monthly Price>"
    And user input six monthly price with "<Six Monthly Price>"
    And user input yearly price with "<Yearly Price>"
    And user click back button in page
    And user click continue input data on pop up
    And user clicks update and user can sees toast on update price as "Harga berhasil diupdate"
    And user refresh page
    Then user see daily price is "<Daily Price>"
    And user see weekly price is "<Weekly Price>"
    And user see monthly price is "<Monthly Price>"
    And user see three monthly price is "<Three Monthly Price>"
    And user see six monthly price is "<Six Monthly Price>"
    And user see yearly price is "<Yearly Price>"
    Examples:
      | Daily Price | Weekly Price | Monthly Price | Three Monthly Price | Six Monthly Price | Yearly Price |
      | 1000000     | 1500000      | 2500000       | 3000000             | 3500000           | 4000000      |
      | 150000      | 500000       | 3000000       | 9000000             | 18000000          | 36000000     |

  @changePricePromoNgebutHome
  Scenario: Owner can't update price from Dashboard when "promo ngebut" active
    Given user redirected to "owner page"
    And user navigates to "owner /ownerpage/kos"
    And user click kos "Kos Fathul Khair Tipe Ranginang Jetis Yogyakarta" in update price list
    And user click see other prices
    Then user see infobar in update price with text "Anda sedang mengikuti promo ngebut, harga tidak dapat diubah sampai promo berakhir"
    When user click monthly price checkbox in edit price
    Then user can sees toast on update price as "Promo ngebut aktif, tidak bisa update"
    And user see monthly price field is disabled
    When user click annual price checkbox in edit price
    Then user can sees toast on update price as "Promo ngebut aktif, tidak bisa update"
    And user see yearly price field is disabled
    When user close infobar promo ngebut in update price
    And user refresh page
    Then user see infobar in update price with text "Anda sedang mengikuti promo ngebut, harga tidak dapat diubah sampai promo berakhir"

  @updatePriceDashboardNoChanges
  Scenario: Access page "Update Harga" from entry point dashboard when kost status == Active without update price
    Given user redirected to "owner page"
    And user navigates to "owner /ownerpage/kos"
    When user click kos "Kos Fathul Khair Tipe Gehu Jetis Yogyakarta" in update price list
    And user click see other prices
    And user memorize daily, weekly, monthly, three monthly, six monthly, and yearly price
    Then user clicks update and user can sees toast on update price as "Harga berhasil diupdate"
    And user see daily, weekly, monthly, three monthly, six monthly, and yearly price is same with previous price
