@regression @addkos @listingGP

Feature: Add Kos From Home

  @addKosFromHome
  Scenario: Add new kost from Dashboard (status property kos diperiksa admin/reject && status apartment diperiksa admin/reject )
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "nonkostditolak" and click on Enter button
    When user clicks on Add Kos button
    When user click Kos in choose property pop up
    And user click create kos button in pop up
    And user click add new kos button
    And user clicks checkbox room type
    And user fills kost name field with "Kost Baru Auto " and random text
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user fill kos description with "kos terbaik hari raya"
    And user set kos rules :
      | Laundry  |
      | Security |
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    When user input administrator name with "Nanik"
    And user input administrator phone with "083333333777"
    And user input other notes "Tidak menerima alien dan Ghoul"
    And user clicks on next button in bottom of add kos page
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user input address note "Dekat rumah nanas bikini bottom"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user insert photo image to "Tambahan foto lain? boleh dong"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Umum":
      | Air Jernih |
    And user check facilities under "Fasilitas Kamar":
      | Kasur |
    And user check facilities under "Fasilitas Kamar Mandi":
      | Air panas |
    And user check facilities under "Parkir":
      | Parkir Sapi |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 4"
    And user input "15" to field total room
    And user input "10" to total available room
    And user clicks on next button in bottom of add kos page
    And user clicks checkbox minimum rent duration
    And user select minimum rent duration "Min. 1 Hari"
    And user click checkbox rent price other than monthly
    And user input monthly price with "300000" in add kos page
    And user input daily price with "50000" in add kos page
    And user input weekly price with "200000" in add kos page
    And user input three monthly price with "800000" in add kos page
    And user input six monthly price with "1700000" in add kos page
    And user input yearly price with "3000000" in add kos page
    And user clicks on next button in bottom of add kos page
    And user click done in success page
    #Please don't delete below step for maintenance purpose
    #Then user click on Kos menu
    Then user see kos with name "Kost Baru Auto" and random text, status "Diperiksa Admin" and type "Kos Putri"
    # Delete newly created kos in admin
    And user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "Kost Baru Auto" in admin kos owner page
    And user delete the kos in admin kos owner

  @addKosFromUpdatePrice
  Scenario: Add new kost from Dashboard (status property kos diperiksa admin/reject && status apartment diperiksa admin/reject )
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "owner apartment only" and click on Enter button
    And user click on owner popup
    When user click update price in Home
    And user click Add Kos in update price page
    Then user redirected to "owner /kos/create?prevPage=Dashboard"
    And user see add new kos button

  @LG-6520
  Scenario: [Form add New Kost][Data Kos]Check checkbox regulations kos and uploaded regulations kos with invalid value
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "nonkostditolak" and click on Enter button
    And user navigates to "owner /kos/create"
    And user click add new kos button
    And user clicks checkbox room type
    And user fills kost name field with "Invalid rules photo " and random text
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user fill kos description with "kos terbaik hari raya"
    And user set kos rules :
      | Akses 24 Jam |
    And user upload invalid kos rules photo
    Then user see warning message invalid kos photo

   @LG-6524
  Scenario: [Form add New Kost][Foto Kos]Upload photo with invalid photos
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "nonkostditolak" and click on Enter button
    And user navigates to "owner /kos/create"
    And user click add new kos button
    And user clicks checkbox room type
    And user fills kost name field with "Invalid rules photo " and random text
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user fill kos description with "kos terbaik hari raya"
    And user set kos rules :
      | Akses 24 Jam |
      #valid photo for next step
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks on next button in bottom of add kos page

      #input address
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear

      #invalid photo kost
    And user insert invalid photo image to "Foto bangunan tampak depan"
    And user insert invalid photo image to "Foto tampilan dalam bangunan"
    And user insert invalid photo image to "Foto tampak dari jalan"
    Then user see warning message invalid kos photo

    @LG-6526
  Scenario: [Form add New Kost][Foto Kamar]Upload photo with invalid photos
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "nonkostditolak" and click on Enter button
    And user navigates to "owner /kos/create"
    And user click add new kos button
    And user clicks checkbox room type
    And user fills kost name field with "Invalid rules photo " and random text
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user fill kos description with "kos terbaik hari raya"
    And user set kos rules :
      | Akses 24 Jam |
      #valid photo for next step
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks on next button in bottom of add kos page

      #input address
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear

      #valid photo kost
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"

      #invalid photo room
    And user clicks on next button in bottom of add kos page
    And user insert invalid photo image to "Foto depan kamar"
    And user insert invalid photo image to "Foto dalam kamar"
    And user insert invalid photo image to "Foto kamar mandi"
    Then user see warning message invalid kos photo
