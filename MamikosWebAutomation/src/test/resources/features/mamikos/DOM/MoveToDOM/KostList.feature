@regression @updateprice @listingGP

Feature: Kost List

  @ComponentActiveKos
  Scenario: Check components of active kost
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    Then user see kos with name "Kose Putri Automation", status "Aktif" and type "Kos Putri"
    And user see kos photo src contains "/2020-02-28/hBjfUdNZ-240x320.jpg"
    When user click see kos button
    Then user redirected to "/room/kost-kabupaten-bantul-kost-putri-murah-kose-putri-automation"
    When user click back button in page
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    Then user see statistic is "Semua Statistik" in kos list
    And user see all statistic option are :
      | Statistik Hari Ini |
      | Statistik Kemarin  |
      | Statistik 7 Hari   |
      | Statistik 14 Hari  |
      | Statistik 30 Hari  |
      | Semua Statistik    |
    When user click Chat in kos list
    Then user redirected to "/ownerpage/statistics/97236206#chat"
    When user click back button in page
    And user clicks button find your kos here
    And input name kos "Kose Putri Automation"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user click review in kost list
    Then user redirected to "/ownerpage/statistics/97236206#review"