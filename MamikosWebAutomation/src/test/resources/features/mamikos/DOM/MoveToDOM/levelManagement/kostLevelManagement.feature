@regression @kostLevel

Feature: Kost Level Management

  Scenario: Add Kost Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Kost Level menu
    And user clicks on Add Kost Level button
    And user fills out form create kost level for "testing" and click save
    And user clicks on save confirmation button
    And user clicks the ok button on the successful popup
    When user filter kost level by keyword "Kost Level Testing"
    And user clicks on Search Kost Level button
    Then user can see "created" kost level on Kost Level menu

  Scenario: Edit Kost Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Kost Level menu
    And user filter kost level by keyword "Kost Level Testing"
    And user clicks on Search Kost Level button
    And user clicks "edit" on Kost Level
    And user fills out form create kost level for "edit" and click save
    And user clicks on save confirmation button
    And user clicks the ok button on the successful popup
    When user filter kost level by keyword "Kost Level Testing Edited"
    And user clicks on Search Kost Level button
    Then user can see "edited" kost level on Kost Level menu

  Scenario: Delete Kost Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Kost Level menu
    And user filter kost level by keyword "Kost Level Testing Edited"
    And user clicks on Search Kost Level button
    And user clicks "delete" on Kost Level
    Then user verify alert success "Success!" and "Level successfully deleted."
    When user filter kost level by keyword "Kost Level Testing Edited"
    And user clicks on Search Kost Level button
    Then user verify no level displayed
