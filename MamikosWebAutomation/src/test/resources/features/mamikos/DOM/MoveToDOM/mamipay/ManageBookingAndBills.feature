@regression @mamipay @listingGP

  Feature: Manage Booking And Bills Menu
    @notregisteredmamipay
    Scenario: Manage Booking And Bills Menu - Not Registered Mamipay
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user fills out owner login as "mamipay-notregistered3" and click on Enter button
      And user click on owner popup
      And user click lengkapi Data Diri
      Then user verify menu Lengkapi Data Diri Anda

    @donthavekos
    Scenario: Manage Booking And Bills Menu - Registered Mamipay but don't have kost
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user fills out owner login as "mamipay-donthavekost" and click on Enter button
      And user navigates to "owner /ownerpage/kos"
      Then user verify button tambah data property

     @notactivatebooking
     Scenario: Manage Booking And Bills Menu - Registered Mamipay, have kost but not yet activate booking
       Given user navigates to "mamikos /"
       When user clicks on Enter button
       And user fills out owner login as "mamipay-notregistered3" and click on Enter button
       And user click on owner popup
       And user click lengkapi Data Diri
       Then user verify menu Lengkapi Data Diri Anda

    @allactive
     Scenario: Manage Booking And Bills Menu - Registered Mamipay, have kost and have activate booking
       Given user navigates to "mamikos /"
       When user clicks on Enter button
       And user fills out owner login as "mamipay-allactive" and click on Enter button
       And user click on owner popup
       And user click left menu Property Saya
       Then user verify on button Management kos
