@regression @listingGP

Feature: Create new kos with owner that hasn't activate mamipay

  Background:
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "mamipay-BBKnotActive" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And user clicks on Add Data button
    And user clicks on Add button
    And user selects Kost option and click on Add Data button

  @CreateKosExistTypeBBKNotActv
  Scenario: Create new room type from "Tipe A" && edit data kos && mamipay not active
    Given user click add another type from kos "KosAuto NoBBK"
    When user click "Onee" in add new room type pop up and click next
    And user input room type with random text in add kos data pop up
    And user clicks on next button in bottom of add kos page
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user see activate mamipay form with Bank Account Number "02823828282"
    And user see active mamipay form with Bank Owner Name "tiara lapan"
    And user see active mamipay form with Bank Name "CTBC (Chinatrust) Indonesia"
    And user click submit data button to activate mamipay
    And user click done in success page
    Then user see kos with name "KosAuto NoBBK" and random text, status "Diperiksa Admin" and type "Kos Putra"
      # Delete newly created kos in admin
    And user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "KosAuto NoBBK" in admin kos owner page
    And user delete the kos in admin kos owner
      # Reject BBK Kos
    And user refresh page
    And user search kos name "KosAuto NoBBK" in admin kos owner page
    And user click BBK Data button
    And user click reject BBK button in booking owner request
    And user click on first radio button in reject reason
    And user click reject BBK button in booking reject reason

  @CreateNewKosBBKNotActv
  Scenario: Create new kost when user already active mamipay && all bbk kos not active
    Given user click add new kos button
    And user fills kost name field with "Kost NoBBK New " and random text
    And user clicks checkbox room type
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user fill kos description with "Kos tanpa BBK automation"
    And user set kos rules :
      | Laundry |
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    When user input administrator name with "Omeii"
    And user input administrator phone with "083333373777"
    And user input other notes "Tidak menerima binatang piaraan"
    And user clicks on next button in bottom of add kos page
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user input address note "Dekat rumah rumahan dari onnta"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user insert photo image to "Tambahan foto lain? boleh dong"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Umum":
      | Dapur  |
      | Kulkas |
    And user check facilities under "Fasilitas Kamar":
      | AC   |
      | Sofa |
    And user check facilities under "Fasilitas Kamar Mandi":
      | Bathup |
      | Gayung |
    And user check facilities under "Parkir":
      | Parkir Mobil |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 4"
    And user input "11" to field total room
    And user input "10" to total available room
    And user clicks on next button in bottom of add kos page
    And user clicks checkbox minimum rent duration
    And user select minimum rent duration "Min. 1 Hari"
    And user click checkbox rent price other than monthly
    And user input monthly price with "300000" in add kos page
    And user input daily price with "50000" in add kos page
    And user input weekly price with "200000" in add kos page
    And user input three monthly price with "800000" in add kos page
    And user input six monthly price with "1700000" in add kos page
    And user input yearly price with "3000000" in add kos page
    And user clicks checkbox additional price
    And user insert cost name "Cuci Anak"
    And user insert total cost "10000"
    And user input deposit with "100000"
    And user clicks checkbox down payment
    And user select down payment percentage "30%"
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user see activate mamipay form with Bank Account Number "02823828282"
    And user see active mamipay form with Bank Owner Name "tiara lapan"
    And user see active mamipay form with Bank Name "CTBC (Chinatrust) Indonesia"
    And user click submit data button to activate mamipay
    And user click done in success page
    Then user see kos with name "Kost NoBBK New " and random text, status "Diperiksa Admin" and type "Kos Putri"
    # Delete newly created kos in admin
    And user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "Kost NoBBK New" in admin kos owner page
    And user delete the kos in admin kos owner
      # Reject BBK Kos
    And user refresh page
    And user search kos name "KosAuto NoBBK" in admin kos owner page
    And user click BBK Data button
    And user click reject BBK button in booking owner request
    And user click on first radio button in reject reason
    And user click reject BBK button in booking reject reason

  @CreateNewRoomTypeBBKNotActv
  Scenario: Create new room type from "Buat baru" when user already active mamipay && all bbk kos not active
    Given user click add another type from kos "KosAuto NoBBK"
    When user click "Buat Baru" in add new room type pop up and click next
    And user input room type with random text
    And user clicks on kost type icon "mix"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user insert photo image to "Tambahan foto lain? boleh dong"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Kamar":
      | AC   |
      | Sofa |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 3"
    And user input "5" to field total room
    And user input "4" to total available room
    And user clicks on next button in bottom of add kos page
    And user clicks checkbox minimum rent duration
    And user select minimum rent duration "Min. 1 Hari"
    And user click checkbox rent price other than monthly
    And user input monthly price with "300000" in add kos page
    And user input daily price with "50000" in add kos page
    And user input weekly price with "200000" in add kos page
    And user input three monthly price with "800000" in add kos page
    And user input six monthly price with "1700000" in add kos page
    And user input yearly price with "3000000" in add kos page
    And user clicks checkbox additional price
    And user insert cost name "Cuci Anak"
    And user insert total cost "10000"
    And user input deposit with "100000"
    And user clicks checkbox down payment
    And user select down payment percentage "30%"
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user see activate mamipay form with Bank Account Number "02823828282"
    And user see active mamipay form with Bank Owner Name "tiara lapan"
    And user see active mamipay form with Bank Name "CTBC (Chinatrust) Indonesia"
    And user click submit data button to activate mamipay
    And user click done in success page
    Then user see kos with name "KosAuto NoBBK" and random text, status "Diperiksa Admin" and type "Kos Campur"
    # Delete newly created kos in admin
    And user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "KosAuto NoBBK" in admin kos owner page
    And user delete the kos in admin kos owner
      # Reject BBK Kos
    And user refresh page
    And user search kos name "KosAuto NoBBK" in admin kos owner page
    And user click BBK Data button
    And user click reject BBK button in booking owner request
    And user click on first radio button in reject reason
    And user click reject BBK button in booking reject reason

  @CreateKosFromDraftBBKInactv
  Scenario: Create from kos with status draft && mamipay active && all bbk kos not active
    Given user click add new kos button
    And user clicks checkbox room type
    And user fills kost name field with "Kost DraftBBKInAct " and random text
    And user input room type with random text
    And user clicks on kost type icon "mix"
    And user fill kos description with "kos harusnya jadi draft then kos bbk off"
    And user set kos rules :
      | Laundry |
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    When user input administrator name with "Hulahulahulala"
    And user input administrator phone with "08545432137334"
    And user input other notes "Akan dihapus setelah terbuat dari automation no bbk"
    And user clicks on next button in bottom of add kos page
    And user click back button on device
    And user click back button on device
    And user click back button on device
    And user click back button on device
    Then user see kos with name "Kost DraftBBKInAct" and random text, status "Draft" and type "Kos Campur"
    When user click complete kos data in first kos list
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user input address note "Dekat rumah si jumbala jumbala jumbahe"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user insert photo image to "Tambahan foto lain? boleh dong"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Umum":
      | Dapur |
    And user check facilities under "Fasilitas Kamar":
      | AC |
    And user check facilities under "Fasilitas Kamar Mandi":
      | Bathup |
      | Gayung |
    And user check facilities under "Parkir":
      | Parkir Mobil |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 4"
    And user input "11" to field total room
    And user input "7" to total available room
    And user clicks on next button in bottom of add kos page
    And user input monthly price with "500000" in add kos page
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user see activate mamipay form with Bank Account Number "02823828282"
    And user see active mamipay form with Bank Owner Name "tiara lapan"
    And user see active mamipay form with Bank Name "CTBC (Chinatrust) Indonesia"
    And user click submit data button to activate mamipay
    And user click done in success page
    Then user see kos with name "Kost DraftBBKInAct" and random text, status "Diperiksa Admin" and type "Kos Campur"
    # Delete newly created kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "Kost DraftBBKInAct" in admin kos owner page
    And user delete the kos in admin kos owner
    # Reject BBK Kos
    And user refresh page
    And user search kos name "KosAuto NoBBK" in admin kos owner page
    And user click BBK Data button
    And user click reject BBK button in booking owner request
    And user click on first radio button in reject reason
    And user click reject BBK button in booking reject reason