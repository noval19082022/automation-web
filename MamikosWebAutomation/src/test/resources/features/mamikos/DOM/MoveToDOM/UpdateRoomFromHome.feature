@regression @updateRoomDashboard @listingGP @LG2
Feature: Update Room from Dashboard

  Background:
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user click on owner popup

  @addRoomAndDelete
  Scenario: Access page "Update Kamar" from entry point dashboard when kost status == Active by add new rooms and delete it
    Given user redirected to "owner page"
    And user click update room availability in Home
    And user click kos "Kos oke bebek Vviop Depok Sleman" in update price list
    When user click add room in room list
    And user fill room name in room allotment page with "26"
    And user click update room button in update room pop up
    Then user can sees toast on update room as "Kamar berhasil ditambahkan"
    And user see total room is "25" in update room page
    When user delete room name or number "26" in room allotment
    Then user can sees toast on update room as "Kamar berhasil dihapus"
    And user see total room is "24" in update room page

  @updateFloorFromHome
  Scenario: Access page "Update Kamar" from entry point dashboard when kost status == Active by update text box "Lantai (Opsional)"
    Given user redirected to "owner page"
    And user click update room availability in Home
    And user click kos "Kos Testing 123 Tipe B Danurejan Yogyakarta" in update price list
    When user enter text "1" on search bar in room allotment and hit enter
    And user click edit button in first row of the table
    And user fill room floor in room allotment page with "abcd"
    And user click update room button in update room pop up
    Then user can sees toast on update room as "Anda berhasil update data kamar"
    And user see first floor name or number is "abcd" in update room page
    When user click edit button in first row of the table
    And user fill room floor in room allotment page with "1"
    And user click update room button in update room pop up
    Then user can sees toast on update room as "Anda berhasil update data kamar"

  @updateRoomNameFromHome
  Scenario: Access page "Update Kamar" from entry point dashboard when kost status == Active by update text box "Nomor/ Nama Kamar?"
    Given user redirected to "owner page"
    And user click update room availability in Home
    And user click kos "Kos Testing 123 Tipe B Danurejan Yogyakarta" in update price list
    When user enter text "1" on search bar in room allotment and hit enter
    And user click edit button in first row of the table
    And user fill room name in room allotment page with "001A"
    And user click update room button in update room pop up
    Then user can sees toast on update room as "Anda berhasil update data kamar"
    And user see first room name or number is "001A" in update room page
    When user click edit button in first row of the table
    And user fill room name in room allotment page with "1"
    And user click update room button in update room pop up
    Then user can sees toast on update room as "Anda berhasil update data kamar"

  @updateRoomFilledFromHome
  Scenario: Access page "Update Kamar" from entry point dashboard when kost status == Active by update rooms become unavailable
    Given user redirected to "owner page"
    And user click update room availability in Home
    And user click kos "Kos Testing 123 Tipe B Danurejan Yogyakarta" in update price list
    When user enter text "1" on search bar in room allotment and hit enter
    And user click edit button in first row of the table
    And user tick already inhabited checkbox
    And user click update room button in update room pop up
    Then user can sees toast on update room as "Kamar Terisi Bertambah 1"
    When user enter text "" on search bar in room allotment and hit enter
    Then user see total room is "20" in update room page
    When user filter the room with "Kamar Kosong" in update room page
    Then user see total room is "18" in update room page
    When user filter the room with "Kamar Terisi" in update room page
    Then user see total room is "2" in update room page
    When user click edit button in first row of the table
    And user tick already inhabited checkbox
    And user click update room button in update room pop up
    Then user can sees toast on update room as "Kamar Kosong Bertambah 1"
    When user filter the room with "Kamar Kosong" in update room page
    Then user see total room is "19" in update room page
    When user filter the room with "Kamar Terisi" in update room page
    Then user see total room is "1" in update room page