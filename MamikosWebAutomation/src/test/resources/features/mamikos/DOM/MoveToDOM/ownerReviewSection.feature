@regression @review-section @listingGP
Feature: Owner - Review Section

  @doesnthavereview-listingcardclicked
  Scenario: Review Section - Doesn't have any review and Listing card clicked
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "mamipay-notregistered" and click on Enter button
    And user click on owner popup
    And user click on rating card details
    Then user validate review section with "Tidak ada data di List." and "Untuk member non premium hanya ditampilkan list 2 hari terakhir saja."

  @yourkosrating-listingcardclicked
  Scenario: Review Section - Your kost rating and Listing card clicked
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "Owner Kos Review" and click on Enter button
    And user click on owner popup
    Then user verify there are only 2 review lists
    When user click one of review lists
    Then user should see the review detail page

  @seeAllRating
  Scenario: Review Section - See all rating
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "Owner Kos Review" and click on Enter button
    And user click on owner popup
    Then user verify there are only 2 review lists
    And user click "Lihat semua ulasan" on Owner Kost Review
    Then user redirected to "kos review"

  @ownerHasMoreThan2Listing
  Scenario: Review Section - Owner has > 2 listings
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "Owner Kos Review" and click on Enter button
    And user click on owner popup
    Then user verify there are only 2 review lists
    And user click "Lihat semua ulasan" on Owner Kost Review
    Then user verify there are more than 2 review lists

  @ownerHasOneOrTwoListings
  Scenario: Review Section - Owner has 1 - 2 listings
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "Owner Kos Review" and click on Enter button
    And user click on owner popup
    Then user verify there are only 2 review lists
    And user click "Lihat semua ulasan" on Owner Kost Review
    Then user verify there are 1 or 2 review lists

  @ownerDoesntHaveAnyListing
  Scenario: Review Section - Owner doesn't have any listing
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "Owner No Listing" and click on Enter button
    And user click on owner popup
    And user verify there is no kos review section
    Then user verify there is "Waktunya Mengelola Properti" section