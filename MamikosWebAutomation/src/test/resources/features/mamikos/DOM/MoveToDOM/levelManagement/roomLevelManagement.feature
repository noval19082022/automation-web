@regression @roomLevel @pman

Feature: Room Level Management
  @TEST_PMAN-3279
  Scenario: Add Room Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin automation"
    When user clicks on Room Level menu
    And user clicks on Add Room Level button
    And user fills out form create room level for "testing" and click save
    Then user verify alert success "Success!" and "Room Level was successfully added."
    When user filter room level by keyword "Room Level Testing"
    And user clicks on Search Room Level button
    Then user can see "created" room level on Room Level menu

  @TEST_PMAN-3255
  Scenario: Edit Room Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin automation"
    When user clicks on Room Level menu
    And user filter room level by keyword "Room Level Testing"
    And user clicks on Search Room Level button
    And user clicks "edit" on Room Level
    And user fills out form create room level for "edit" and click save
    Then user verify alert success "Success!" and "Room Level was successfully updated."
    When user filter room level by keyword "Room Level Edited"
    And user clicks on Search Room Level button
    Then user can see "edited" room level on Room Level menu

  @TEST_PMAN-3267
  Scenario: Delete Room Level
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin automation"
    When user clicks on Room Level menu
    And user filter room level by keyword "Room Level Edited"
    And user clicks on Search Room Level button
    And user clicks "delete" on Room Level
    Then user verify alert success "Success!" and "Room Level was successfully deleted."
    When user filter room level by keyword "Room Level Edited"
    And user clicks on Search Room Level button
    Then user verify no level displayed