@regression @owner @appartment @listingGP @prod @LG2
Feature: Owner - Add Apartment

  @addapart
  Scenario: Owner - Add apartment with valid data
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/apartment"
    And user clicks on Add Data button
    And user clicks on Add button
    And user selects Appartment option and click on Add Data button
    And user fills appartment form with data appartment name "test data", unit name "unit test data"

  @backPageApartment
  Scenario: New Owner wants to click back after at page create apartment
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "mamipay-donthavekost" and click on Enter button
    And user click on owner popup
    And user clicks on Add Kos button
    And user choose Apartment on page pilih jenis properti
    And user clicks button Buat Apartment
    And user click back button in page
    Then user see page title is "Pilih Jenis Properti" on choose property type page

  @listApartement
  Scenario: Owner - See list appartement
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/apartment"
    Then user see list apartemen
