@regression @listingGP @DOM2

Feature: Create new kos with owner that doesn't activate mamipay and BBK

  @CreateKosWithRemoteTrue
  Scenario: Check T&C remote condition with status true
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "mamipay-and-BBK-notRegistered" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And user clicks on Add Data button
    And user clicks on Add button
    And user selects Kost option and click on Add Data button
    And user click add new kos button
    And user clicks checkbox room type
    And user fills kost name field with "Kost noBBK noMamipay " and random text
    And user input room type with random text
    And user clicks on kost type icon "male"
    And user fill kos description with "kos harusnya Kost noBBK noMamipay"
    And user set kos rules :
      | Laundry |
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    When user input administrator name with "Wowewo"
    And user input administrator phone with "08545432133333"
    And user input other notes "Akan dihapus setelah terbuat"
    And user clicks on next button in bottom of add kos page
    And user input kost location "Tobelo" and clicks on first autocomplete result "Kabupaten Halmahera Barat" "Ibu"
    And user input address note "Dekat rumah awewena aweweee"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user insert photo image to "Tambahan foto lain? boleh dong"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Umum"
      | Dapur |
    And user check facilities under "Fasilitas Kamar"
      | AC |
    And user check facilities under "Fasilitas Kamar Mandi"
      | Bathup |
      | Gayung |
    And user check facilities under "Parkir"
      | Parkir Mobil |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 4"
    And user input "10" to field total room
    And user input "6" to total available room
    And user clicks on next button in bottom of add kos page
    And user input monthly price with "500000" in add kos page
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    Then user see next button disable
      # Delete newly created kos draft
    And user navigates to "owner /ownerpage/kos"
    And user delete first kos on the list