@regression @owner @appartment @listingGP @LG2
Feature: Edit Apartment

  @editApartNoChanges
  Scenario: Edit Apartment without edit data and then submit data
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/apartment"
    When user clicks button find your apartment here
    And input name apartment "gundam"
    And user clicks kos name from result
    And user click Edit Data Apartment
    And user clicks on Submit button in edit apartment
    When user click done in success page pop up of edit apartment
    And user clicks button find your kos here
    And input name kos "gundam"
    And user clicks kos name from result
    Then user see apartment with name "gundam" and status "Diperiksa Admin"
    # Verify Apartment in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search apartment name "gundam" in admin kos owner page
    Then user verify the kos in admin kos owner

  @editApartWithChanges
  Scenario: Edit Apartment with edit valid data
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user click on owner popup
    And user click left menu Property Saya
    And user clicks on Owner Apartment Menu
    When user clicks button find your apartment here
    And input name apartment "b23a"
    And user clicks kos name from result
    And user click Edit Data Apartment
    And user select unit type "2 BR"
    And user fill apartment's floor with "23"
    And user fill apartment's size with "33" m2
    And user fill apartment's description with "Oke oke oke wwwww"
    And user clicks on Submit button in edit apartment
    And user click done in success page pop up of edit apartment
    And user clicks button find your kos here
    And input name kos "b23a"
    And user clicks kos name from result
    Then user see apartment with name "b23a" and status "Diperiksa Admin"
    # Verify Apartment in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search apartment name "b23a" in admin kos owner page
    Then user verify the kos in admin kos owner