@regression @goldPlus @listingGP
Feature: New Owner Create Kost and Apartment

  Background:
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "mamipay-donthavekost" and click on Enter button

  @backPage
  Scenario: New Owner wants to click back after at page create kost
    Given user clicks on Add Kos button
    When user click back button in page
    Then check the header menu display on homepage owner

  @icnClose
  Scenario: Click button close on screen "Pilih Jenis Properti"
    Given user clicks on Add Kos button
    When user click icon close on page pilih jenis properti
    Then check the header menu display on homepage owner

  @formAddKosWhenKos0
  Scenario: Add new kost from Menu Kos (property kos == 0)
    Given user click left menu Property Saya
    When user click menu Kost
    And user click left menu Property Saya
    When user click menu Kost
    And user selects Kost option and click on Add Data button
    And user click add new kos button
    Then user redirected to "owner /kos/create?step=1"



