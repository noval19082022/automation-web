@regression @refund
Feature: Refund Payment Ovo

Scenario: Cancel Contract for refund
Given user navigates to "backoffice"
When user login  as a Admin via credentials
And user access search contract menu
And user search for "payment" and cancel contract

#  Scenario: Cancel booking if tenant have booking
Given user navigates to "mamikos /"
When user clicks on Enter button
And user logs in as Tenant via Facebook credentails as "tenantPayment"
When user navigates to "mamikos /user/booking/"
And user cancel booking

#  Scenario: Tenant booking and payment for weekly period
Given user navigates to "mamikos /"
When user clicks search bar
And I search property with name "payment" and select matching result to go to kos details page
And user choose boarding date is "today" and clicks on Booking button on Kost details page
And user select payment period "Per Minggu"
And user selects T&C checkbox and clicks on Book button
Then system display successfully booking

#  Scenario: Owner accept booking from tenant
Given user navigates to "mamikos /"
When user logs out as a Tenant user
And user clicks on Enter button
And user fills out owner login as "payment" and click on Enter button
And user navigates to booking request and filter booking need confirmation
And user clicks accept button on "payment" booking
And select first room available and clicks on next button
And user click save button

#  Scenario: Tenant pay boarding house for weekly rent
Given user navigates to "mamikos /"
When user logs out as a Tenant user
And user clicks on Enter button
And user logs in as Tenant via Facebook credentails as "tenantPayment"
And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
And user select payment method "OVO" for "paymentW1"
Then system display payment using "OVO" is "Pembayaran Berhasil"

#  Scenario: data booking
Given user navigates to "mamikos admin"
When user login  as a Admin bangkrupux via credentials
And user access to data booking menu
And user show filter data booking
And user search data booking using tenant phone "tenant refund"
And user clicks on Search button
And user set refund reason for data booking

#  Scenario: Admin edit paid amount & uncheck admin fee
Given user navigates to "backoffice"
When user click on Search Contract Menu form left bar
And admin go to Paid Invoice list refund Page
And user search by "Tenant Phone Number" and input field "083139263046" refund menu
And user click search button
And user click on refund button
And user input nomor rekening "123456" and pemilik rekening "testing automation refund"
And user choose one of reason list
And user click refund and transfer button
Then user see message "Refund transaction created."

#  Scenario: Admin payment from bigflip
Given user navigates to "big flip bussiness"
When user login as Admin Bigflip via credentials
And user switch to flip for business Test Mode
And user go to riwayat transaksi domestic page
And user click force sucsess transaction
Then user see that refund transaction is success

#  Scenario: Admin see that refund transaction in success
Given user navigates to "backoffice"
When user click on Search Contract Menu form left bar
And admin go to Paid Invoice list refund Page
And user click tab "Transferred" on refund menu
Then user verify that refund transaction is done