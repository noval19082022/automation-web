@regression @refundSucsess
Feature: Refund

  @seeTransactionFlipOnFailedTab
  Scenario: Positive case direction Failed Tab on refund menu
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Failed" on refund menu
    Then user successed visit Failed tab see detail columns

  @exportReport
  Scenario: Positive case export report popup refund  menu
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user click button export
    And user choose one list date on the popup
    And user click button download
    Then user will get message success download and file exported send email

  @exportReportBefore1Hour
  Scenario: Negative case refund export report before 1 hour on popup refund  menu
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user click button export
    And user choose one list date on the popup
    And user click button download
    Then user will get error message

  @noInputBankAccount
  Scenario: No input bank account
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click search button
    And user click on refund button
    And user click input account name "noval"
    And user click refund and transfer button
    Then popup close and there will error massage "The refund account field is required when cc transaction id is not present."

  @noInputAccountName
  Scenario: No input account name
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click search button
    And user click on refund button
    And user click input account number "300100500"
    And user click refund and transfer button
    Then popup close and there will error massage "The refund account name field is required when cc transaction id is not present."

  @clickBackButtonPopupRefund
  Scenario: No input account name
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click on refund button
    And user click on back button popup refund
    Then admin go to refund Page "Daftar Invoice Refund"

  @clickCloseButtonPopupRefund
  Scenario: No input account name
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click on refund button
    And user click close on popup
    Then admin go to refund Page "Daftar Invoice Refund"

  @directionTabToTransferredTab
  Scenario: Direction Transferred Tab on refund menu
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    Then user successed visit "transferred tab" and user can see detail columns

  @transactionFlipOnTransferredTab
  Scenario: Transaction flip on Transferred Tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user search by "Tenant Phone Number" and input field "083829167577" refund menu
    And user click search button
    Then user successed visit "transferred tab using flip transaction" and user can see detail columns

  @transactionCreditCardOnTransferredTab
  Scenario: Transaction Credit card on Transferred Tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user search by "Tenant Phone Number" and input field "083829167577" refund menu
    And user click search button
    Then user successed visit "transferred tab using credit card transaction" and user can see detail columns

  @downloadReceiptFlipOnTransferredTab
  Scenario: Download receipt Transaction flip on Transferred Tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user search by "Tenant Phone Number" and input field "083829167577" refund menu
    And user click search button
    And user click button receipt
#    Then user successed download receipt - (need improvement for popup success from FE)

  @popupRefundSectionBank
  Scenario: User choose the bank
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click on refund button
    And user click on dropdown bank name
    And user click section bank "BCA"
    And user click on dropdown bank name
    And user input bank name "BANK MANTAP (Mandiri Taspen)"
    And user click section bank "BANK MANTAP (Mandiri Taspen)"
    Then popup list bank closed and bank selected

  @inputInvalidBankName
  Scenario: input invalid bank name
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click on refund button
    And user click on dropdown bank name
    And user input bank name "Bank Noval"
    Then user will get message not result found