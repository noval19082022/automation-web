@premiumPackage @regression

Feature: Premium Package
  @paidPremiumPackage
  Scenario: Owner paid premium paket
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "payment" and click on Enter button
    And user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    And user click "Beli Saldo"
    And user choose saldo "Rp6.000"
    And user click Bayar button
    And user select payment method "OVO" for "paymentW1"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

#  Scenario: Admin check the transaction on Menu Premium Package status paid
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user search invoice on MamiPAY Premium Package Invoice List
    Then system display status "paid"

  @unpaidPremiumPackage
  Scenario: Owner unpaid premium paket
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "payment" and click on Enter button
    And user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    And user click "Beli Saldo"
    And user choose saldo "Rp6.000"
    And user click Bayar button
    And user select payment method "OVO" for "paymentW1"

#  Scenario: Admin check the transaction on Menu Premium Package status unpaid
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user search invoice on MamiPAY Premium Package Invoice List
    Then system display status "unpaid"

  @filterValidOwnerNumber
  Scenario: Filter valid owner number premium
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user choose "Owner Phone Number" on filter search by
    And user input phone input phone number
    And user click button search invoice
    Then system display package invoice list sort by phone number

  @filterInvalidOwnerNumber
  Scenario: Filter invalid owner number premium
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user choose "Owner Phone Number" on filter search by
    And user input phone input invalid phone number
    And user click button search invoice
    Then system display package invoice list sort by invalid phone number

  @filterStatusUnpaid
  Scenario: Filter by status unpaid
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user choose "Unpaid" on filter by status
    And user click button search invoice
    Then system display package invoice list filter by status "unpaid"

  @filterValidPremiumPackageInvoice
  Scenario: Filter valid premium package invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user input valid invoice number
    And user click button search invoice
    Then system display package invoice list filter valid premium package invoice

  @filterInvalidPremiumPackageInvoice
  Scenario: Filter invalid premium package invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user input invalid invoice number
    And user click button search invoice
    Then system display package invoice list filter invalid premium package invoice

  @filterInvalidPremiumPackageGpInvoice
  Scenario: Filter Invalid premium package invoice use GP Invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user input gp invoice number
    And user click button search invoice
    Then system display package invoice list search invoice use GP Ivoice

  @filterStatusExpired
  Scenario: Admin search expired invoice number
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user check invoice number expired
    And user click button search invoice
    Then system display invoice number expired with status "expired"

