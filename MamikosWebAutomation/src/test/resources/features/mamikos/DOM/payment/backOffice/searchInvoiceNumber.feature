@allinvoice @backoffice @invoice @regression
  Feature: search invoice

    Scenario: search invoice with use invoice number
      Given user navigates to "backoffice"
      When user login  as a Admin via credentials
      And admin open menu all invoice
      And admin select invoice number
      And admin input invoice number "ST/90743755/2020/11/0031"
      And admin click button cari invoice
      Then admin verify data transaction


#   Scenario: admin wrong input invoice
        And admin click button reset
        And admin select invoice number
        And admin input invoice number "ST/90743755/2020/11/003"
        And admin click button cari invoice
        Then admin get blank screen


#    Scenario: search invoice with use invoice code
      And admin click button reset
      And admin select invoice core
      And admin input invoice number "915000"
      And admin click button cari invoice
      Then admin get blank screen


#    Scenario: search invoice with use invalid invoice code
      And admin click button reset
      And admin select invoice core
      And admin input invoice number "915"
      And admin click button cari invoice
      Then admin verify data transaction

#    Scenario: search invoice GP
      And admin click button reset
      And admin select invoice number
      And admin input invoice number "GP2/20210225/00002146/8312"
      And admin click button cari invoice
      Then admin will get data invoice "GP2/20210225/00002146/8312"

#    Scenario: search wrong invoice GP
      And admin click button reset
      And admin select invoice number
      And admin input invoice number "GP2/20210225/00002146/111"
      And admin click button cari invoice
      Then admin get blank screen

#    Scenario: search Premium invoice
      And admin click button reset
      And admin select invoice number
      And admin input invoice number "PRE/20210727/48346/36691"
      And admin click button cari invoice
      Then admin will get data invoice "PRE/20210727/48346/36691"

#    Scenario: search wrong premium invoice
      And admin click button reset
      And admin select invoice number
      And admin input invoice number "PRE/20210727/48346/111"
      And admin click button cari invoice
      Then admin get blank screen

#      Scenario: search transaction with use method BNI
        And admin click button reset
        And admin choose method "bni"
        And admin click button cari invoice
        Then admin will get data transatcion with method "bni"

#     Scenario: search transaction with use method mandiri
        And admin click button reset
        And admin choose method "mandiri"
        And admin click button cari invoice
        Then admin will get data transatcion with method "mandiri"

#      Scenario: search transaction with use method indomaret
        And admin click button reset
        And admin choose method "indomaret"
        And admin click button cari invoice
        Then admin will get data transatcion with method "indomaret"

#      Scenario: search transaction with use method other bank
        And admin click button reset
        And admin choose method "other"
        And admin click button cari invoice
        Then admin will get data transatcion with method "other"

#      Scenario: search transaction with use method permata
        And admin click button reset
        And admin choose method "permata"
        And admin click button cari invoice
        Then admin will get data transatcion with method "permata"

#      Scenario: search transaction with use method gopay
        And admin click button reset
        And admin choose method "gopay"
        And admin click button cari invoice
        Then admin will get data transatcion with method "gopay"

#      Scenario: search transaction with use method alfamart
        And admin click button reset
        And admin choose method "alfamart"
        And admin click button cari invoice
        Then admin will get data transatcion with method "alfamart"

#      Scenario: search transaction with use method lawson
        And admin click button reset
        And admin choose method "lawson"
        And admin click button cari invoice
        Then admin will get data transatcion with method "lawson"

#      Scenario: search transaction with use method dandan
        And admin click button reset
        And admin choose method "dandan"
        And admin click button cari invoice
        Then admin will get data transatcion with method "dandan"

#      Scenario: search transaction with use method linkaja
        And admin click button reset
        And admin choose method "linkaja"
        And admin click button cari invoice
        Then admin will get data transatcion with method "linkaja"

#      Scenario: search transaction with use method bri
        And admin click button reset
        And admin choose method "bri"
        And admin click button cari invoice
        Then admin will get data transatcion with method "bri"

#      Scenario: search transaction with use method dana
        And admin click button reset
        And admin choose method "dana"
        And admin click button cari invoice
        Then admin will get data transatcion with method "dana"

#      Scenario: search transaction with use method ovo
        And admin click button reset
        And admin choose method "ovo"
        And admin click button cari invoice
        Then admin will get data transatcion with method "ovo"

#      Scenario: search transaction with use method credit_card
        And admin click button reset
        And admin choose method "credit_card"
        And admin click button cari invoice
        Then admin will get data transatcion with method "credit_card"

#      Scenario: search transaction based on schedule date
        And admin click button reset
        And admin choose date picker "2021-07-01" and "2021-07-19"
        And admin click button cari invoice
        Then data transaction appeared

#      Scenario: search transaction based on nomila transaction
         And admin click button reset
         And admin input amount from and to "50000" and "50000"
         And admin click button cari invoice
         Then appeared data with amount "50000"

#      Scenario: search transaction based on status unpaid
          And admin click button reset
          And admin choose status "Unpaid"
          And admin click button cari invoice
          Then appeared data transaction with status "unpaid"

#      Scenario: search transaction based on status paid
          And admin click button reset
          And admin choose status "Paid"
          And admin click button cari invoice
          Then appeared data transaction with status "paid"

#      Scenario: search transaction based on status expired
          And admin click button reset
          And admin choose status "Expired"
          And admin click button cari invoice
          Then appeared data transaction with status "expired"

#      Scenario: search transaction baded on order type pengajuan sewa
          And admin click button reset
          And admin choose order type "Pengajuan Sewa"
          And admin click button cari invoice
          Then appeared data transaction with order type "Bayar Sewa Kos"

#      Scenario: search transaction baded on order type ayar Paket Premium
         And admin click button reset
         And admin choose order type "Bayar Paket Premium"
         And admin click button cari invoice
         Then appeared data transaction with order type " Bayar Saldo MamiAds "

#      Scenario: change transaction from unpaid to paid
          And admin click button reset
          And admin select invoice number
          And admin input invoice number "45111793/2021/04/0018"
          And admin click button cari invoice
          And admin click change status
          And admin change unpaid to paid
          And admin input date and time "2021-02-04 16:35:11"
          And admin submit change
          Then invoice will changes to "paid"



#    Scenario: change transaction from paid to unpaid
          And admin open menu all invoice
          And admin select invoice number
          And admin input invoice number "45111793/2021/04/0018"
          And admin click button cari invoice
          And admin click change status
          And admin change paid to unpaid
          And admin input date and time "2021-02-04 16:35:11"
          And admin submit change
          Then invoice will changes to "unpaid"


#    Scenario: change transaction from unpaid to paid not in mamipay
          And admin open menu all invoice
          And admin select invoice number
          And admin input invoice number "45111793/2021/04/0018"
          And admin click button cari invoice
          And admin click change status
          And admin change unpaid to paid
          And admin click checkbox not in mamipay
          And admin input date and time "2021-02-04 16:35:11"
          And admin submit change
          Then invoice will changes to "paid"

#    Scenario: change transaction from paid to unpaid
          And admin open menu all invoice
          And admin select invoice number
          And admin input invoice number "45111793/2021/04/0018"
          And admin click button cari invoice
          And admin click change status
          And admin change paid to unpaid
          And admin input date and time "2021-02-04 16:35:11"
          And admin submit change
          Then invoice will changes to "unpaid"

