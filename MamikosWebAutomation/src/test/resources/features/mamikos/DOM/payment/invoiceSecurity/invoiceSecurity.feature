@regression @invoiceSecurity
Feature: back office - invoice security
  @openFromAllInvoice
  Scenario Outline: back office - Open Invoice Expired Booking from All invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin open menu all invoice
    And admin click button reset
    And admin choose status "<status>"
    And admin input invoice number "<invoice>"
    And admin click button cari invoice
    And admin click Link Invoice
    Then admin see shown invoice not found "Invoice Kedaluwarsa" "Invoice Tidak Ditemukan"
    Examples:
      | status  | invoice                  |
      | Expired | PRE/20220221/50864/80817 |
      | Paid    | DP/19024270/2022/02/0185 |
      | Unpaid  | 44331680/2022/03/0017    |


  @openFromPremiumInvoicePaid
  Scenario: owner - Open Invoice paid froms list premium
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "premium" and click on Enter button
    And user click on owner popup
    And user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    And user click Riwayat button
    And user click Selesai button
    And user choose invoice paid from list selesai
    Then admin see shown invoice Paid "Pembayaran Berhasil" "Invoice Tidak Ditemukan"

  @openFromPremiumInvoiceUnpaid
  Scenario: owner - Open invoice unpaid froms list premium
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "premium" and click on Enter button
    And user click on owner popup
    And user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    And user click Riwayat button
    And user click dalam proses button
    And user choose invoice unpaid from list selesai
    Then user see shown invoice unpaid "Pembayaran" "Invoice Kedaluwarsa" "Invoice Tidak Ditemukan"

   @openInvoiceGp
   Scenario Outline: owner - Open invoice unpaid froms list GP
     Given user navigates to "backoffice"
     When user login  as a Admin via credentials
     And admin open menu Goldplus invoice list
     And admin click button reset
     And admin choose status "<status>"
     And admin click button cari invoice
     And admin click Link Invoice
     Then user see shown invoice unpaid "Pembayaran" "Invoice Kedaluwarsa" "Invoice Tidak Ditemukan"
     Examples:
       | status  |
       | Expired |
       | Unpaid  |

  @openInvoicePaidGp
  Scenario: owner - Open invoice paid froms list GP
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin open menu Goldplus invoice list
    And admin click button reset
    And admin choose status "Paid"
    And admin input invoice number "GP2/20220217/00005909/3769"
    And admin click button cari invoice
    And admin click Link Invoice
    Then admin see shown invoice Paid "Pembayaran Berhasil" "Invoice Tidak Ditemukan"

  @openFromSearchInvoiceExpired
  Scenario Outline: back office - Open Invoice unpaid Booking from search invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Search Invoice Menu form left bar
    And admin choose status "<status>"
    And admin input invoice number "<invoice>"
    And admin click button cari invoice
    And admin click Link Invoice
#    And admin click shortlink "<shortlink>"
    Then admin see shown invoice Paid "Pembayaran Berhasil" "Invoice Tidak Ditemukan"
    Examples:
      | status  | invoice               |
      | Expired | 34716463/2022/02/0009 |
      | Paid    | 12865544/2022/02/0082 |
      | Unpaid  | 57653153/2022/02/0003 |
