@DOM2
Feature: [Test-Execution][DOM] Web - AT Payment Staging v2235.0

  @TEST_DOM-623 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] cancel Extend Contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220220105"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click button extend contract
    Then user navigates to "Custom Extend Contract"

  @TEST_DOM-624 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] Search Data Tenant Based On Period
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    Then user choose contract date period
    And user select date period "Today"
    And user click search button
    Then user choose contract date period
    And user select date period "Yesterday"
    And user click search button
    Then user Navigate "Search Contract" page

  @TEST_DOM-622 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] See detail pop up Apik
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220211208"
    And user click search button
    And user click edit deposit button
    Then user will see detail pop up edit deposit Apik "Edit Deposit for Confirm to Finance"

  @TEST_DOM-621 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] input Bank Edit Deposit
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden
    And user click drop down bank name and choose one bank
    Then user see dropdown will be close

  @TEST_DOM-620 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] Input Damage Details more than 200 characters
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220211208"
    And user click search button
    And user click edit deposit button
    And user Input damage details "characters more than 200"
    Then user see maximal length "200"

  @TEST_DOM-619 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] Admin simpan draft
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden
    And user input nomor rekening "1550000036"
    And user input nama pemilik rekening "Noval"
    And user input transfer date
    And user click on simpan Draft button
    Then user will see message "Berhasil disimpan sebagai draf"

  @TEST_DOM-618 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] see Sisa Deposit
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220211208"
    And user click search button
    And user click edit deposit button
    Then user will see detail pop up edit deposit Apik "Edit Deposit for Confirm to Finance"
    And user input biaya kerusakan "50000"
    Then user will see sisa deposit

  @TEST_DOM-617 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] see Data Contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And user click see log button
    Then user will see detail data contract

  @TEST_DOM-616 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] see Popup Terminate Contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220220101"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click akhiri contract button
    Then user see new popup termination appeared

  @TEST_DOM-615 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] search Based On Period
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220220101"
    And user fills kost level "SinggahSini"
    And user click search button
    Then user redirect to search contract menu detail

  @TEST_DOM-614 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] See detail pop up Mamirooms
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081177778888"
    And user fills kost level "mamirooms"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden

  @TEST_DOM-613 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] Search Data Tenant Based On Kost Level
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user fills kost level "Mamikos Goldplus 2"
    And user click search button
    Then user verify that detail kos is "Mamikos Goldplus 2"

  @TEST_DOM-612 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] See detail pop up Singgah Sini
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden

  @TEST_DOM-611 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] input Name Rekening Detail Edit Deposit
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden
    And user input nama pemilik rekening "Noval"

  @TEST_DOM-610 @Automated @web-covered
  Scenario Outline: [BackOffice][Search Contract][Edit Deposit] search Valid Input
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "<searchBy>" and input field "<input>"
    And user click search button
    Then user will get data detail
    Examples:
      | searchBy               | input                 |
      | Kost Name              | Kost Princess         |
      | Owner Phone Number     | 083843666900          |
      | Renter Phone Number    | 083139263046          |
      | Renter Name            | Ullrich               |
      | Related Invoice Number | 83900841/2021/12/0043 |

  @TEST_DOM-609 @Automated @web-covered
  Scenario Outline: [BackOffice][Search Contract][Edit Deposit] search invalid Input
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "<searchBy>" and input field "<input>"
    And user click search button
    Then user will get blank data detail
    Examples:
      | searchBy               | input               |
      | Kost Name              | kost anggun         |
      | Owner Phone Number     | 0856220211208       |
      | Renter Phone Number    | 0856220211208       |
      | Renter Name            | embul owner         |
      | Related Invoice Number | 83900841/2021/12/00 |
      | Related Invoice Code   | 83900841            |

  @TEST_DOM-608 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit]input Nomor Rekening Detail Edit Deposit
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden
    And user input nomor rekening "1550000036"

  @TEST_DOM-607 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit Deposit] see Lihat Akhiri Kontrak Disable
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220220105"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click akhiri contract button
    Then user see akhiri kontrak button disabled
	#input detail kerusakan in  pop up "Deposit for confirm to finance" more then 200 character
  @TEST_DOM-727 @TESTSET_PAY-3276 @TESTSET_PAY-5269 @TESTSET_PF-1394 @TESTSET_PF-2238 @Automated @web-covered
  Scenario: [BackOffice][Search Contract][Edit deposit] Input detail kerusakan detail pop up more than 200 character
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220211208"
    And user click search button
    And user click edit deposit button
    And user Input damage details "characters more than 200"
    Then user see maximal length "200"

  @TEST_DOM-732
  Scenario: [BackOffice][Search Contract][Edit deposit] See Deposit Button for contract terminated
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220211208"
    And user click search button
    Then user will see button "Edit Deposit"

  @TEST_DOM-640 @Automated @web-covered
  Scenario: [BackOffice][Refund] Input invalid bank name
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click on refund button
    And user click on dropdown bank name
    And user input bank name "Bank Noval"
    Then user will get message not result found

  @TEST_DOM-638 @Automated @web-covered
  Scenario: [BackOffice][Refund] export Report
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user click button export
    And user choose one list date on the popup
    And user click button download
    Then user will get message success download and file exported send email

  @TEST_DOM-633 @Automated @web-covered
  Scenario: [BackOffice][Refund] Refund Payment Credit Card
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access search contract menu
    And user search for "payment" and cancel contract

		#  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "tenantPayment"
    And user navigates to "mamikos /user/booking/"
    And user cancel booking

		#  Scenario: Tenant booking and payment for weekly period
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "payment" and select matching result to go to kos details page
    And user choose boarding date is "today" and clicks on Booking button on Kost details page
    And user select payment period "Per Minggu"
    And user selects T&C checkbox and clicks on Book button
    Then user navigates to main page after booking

#  Scenario: Admin accept booking from tenant
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin DOM"
    And user navigates to "mamikos /admin/booking/users"
    And user show filter data booking
    And user search data booking using tenant phone "tenant confirm booking"
    And user click "Cari" button
    And user go to confirm booking via action button
    And user click lanjutkan button in data booking
    And user show filter data booking


		#  Scenario: Tenant pay boarding house for weekly rent
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "tenantPayment"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user select payment method "Kartu Kredit" for "paymentW1"
    Then system display payment using "Kartu Kredit" is "Pembayaran"

		#  Scenario: data booking
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "tenant refund"
    And user clicks on Search button
    And user set refund reason for data booking

		#  Scenario: Admin edit paid amount & uncheck admin fee
    Given user navigates to "backoffice"
    When user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click search button
    And user click on refund button
    And user click on uncheck admin fee
    And user edit paid amount credit card "20000"
    And user choose one of reason list
    And user click refund and transfer button
    Then user see message "Refund transaction created."

  @TEST_DOM-639 @Automated @web-covered
  Scenario: [BackOffice][Refund] see Transaction Flip On Failed Tab
		#  Scenario: Cancel Contract for refund
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access search contract menu
    And user search for "payment" and cancel contract

		#  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "tenantPayment"
    When user navigates to "mamikos /user/booking/"
    And user cancel booking

		#  Scenario: Tenant booking and payment for weekly period
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "payment" and select matching result to go to kos details page
    And user choose boarding date is "today" and clicks on Booking button on Kost details page
    And user select payment period "Per Minggu"
    And user selects T&C checkbox and clicks on Book button
    Then user navigates to main page after booking

		#  Scenario: Admin accept booking from tenant
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin DOM"
    And user navigates to "mamikos /admin/booking/users"
    And user show filter data booking
    And user search data booking using tenant phone "tenant confirm booking"
    And user search data booking with status "Booked"
    And user click "Cari" button
    And user go to confirm booking via action button
    And user click lanjutkan button in data booking
    And user show filter data booking

		#  Scenario: Tenant pay boarding house for weekly rent
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "tenantPayment"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user select payment method "OVO" for "paymentW1"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

		#  Scenario: data booking
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "tenant refund"
    And user clicks on Search button
    And user set refund reason for data booking

		#  Scenario: Admin edit paid amount & uncheck admin fee
    Given user navigates to "backoffice"
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user search by "Tenant Phone Number" and input field "083139263046" refund menu
    And user click search button
    And user click on refund button
    And user input nomor rekening "123456" and pemilik rekening "testing automation refund"
    And user choose one of reason list
    And user click refund and transfer button
    Then user see message "Refund transaction created."

		#  Scenario: Admin payment from bigflip
    Given user navigates to "big flip bussiness"
    When user login as Admin Bigflip via credentials
    And user switch to flip for business Test Mode
    And user go to riwayat transaksi domestic page
    And user click force sucsess transaction
    Then user see that refund transaction is success

		#  Scenario: Admin see that refund transaction in success
    Given user navigates to "backoffice"
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    Then user verify that refund transaction is done

  @TEST_DOM-637 @Automated @web-covered
  Scenario: [BackOffice][Refund] export Report
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user click button export
    And user choose one list date on the popup
    And user click button download
    Then user will get message success download and file exported send email

  @TEST_DOM-636 @Automated @web-covered
  Scenario: [BackOffice][Refund] export Report Before 1 Hour
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user click button export
    And user choose one list date on the popup
    And user click button download
    Then user will get error message

  @TEST_DOM-635 @Automated @web-covered
  Scenario: [BackOffice][Refund] transaction CreditCard On Transferred Tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user search by "Tenant Phone Number" and input field "083829167577" refund menu
    And user click search button
    Then user successed visit "transferred tab using credit card transaction" and user can see detail columns

  @TEST_DOM-634 @Automated @web-covered
  Scenario: [BackOffice][Refund] click Close Button Popup Refund
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click on refund button
    And user click close on popup
    Then admin go to refund Page "Daftar Invoice Refund"

  @TEST_DOM-632 @Automated @web-covered
  Scenario: [BackOffice][Refund] download Receipt Flip On Transferred Tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user search by "Tenant Phone Number" and input field "083829167577" refund menu
    And user click search button
    And user click button receipt
		#    Then user successed download receipt - (need improvement for popup success from FE)
  @TEST_DOM-631 @Automated @web-covered
  Scenario: [BackOffice][Refund] Refund Payment Ovo
		#  Scenario: Cancel Contract for refund
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access search contract menu
    And user search for "payment" and cancel contract

		#  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "tenantPayment"
    When user navigates to "mamikos /user/booking/"
    And user cancel booking

		#  Scenario: Tenant booking and payment for weekly period
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "payment" and select matching result to go to kos details page
    And user choose boarding date is "today" and clicks on Booking button on Kost details page
    And user select payment period "Per Minggu"
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

#  Scenario: Admin accept booking from tenant
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin DOM"
    And user navigates to "mamikos /admin/booking/users"
    And user show filter data booking
    And user search data booking using tenant phone "tenant confirm booking"
    And user click "Cari" button
    And user go to confirm booking via action button
    And user click lanjutkan button in data booking
    And user show filter data booking

		#  Scenario: Tenant pay boarding house for weekly rent
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "tenantPayment"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user select payment method "OVO" for "paymentW1"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

		#  Scenario: data booking
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "tenant refund"
    And user clicks on Search button
    And user set refund reason for data booking

		#  Scenario: Admin edit paid amount & uncheck admin fee
    Given user navigates to "backoffice"
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user search by "Tenant Phone Number" and input field "083139263046" refund menu
    And user click search button
    And user click on refund button
    And user input nomor rekening "123456" and pemilik rekening "testing automation refund"
    And user choose one of reason list
    And user click refund and transfer button
    Then user see message "Refund transaction created."

		#  Scenario: Admin payment from bigflip
    Given user navigates to "big flip bussiness"
    When user login as Admin Bigflip via credentials
    And user switch to flip for business Test Mode
    And user go to riwayat transaksi domestic page
    And user click force sucsess transaction
    Then user see that refund transaction is success

		#  Scenario: Admin see that refund transaction in success
    Given user navigates to "backoffice"
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    Then user verify that refund transaction is done

  @TEST_DOM-630 @Automated @web-covered
  Scenario: [BackOffice][Refund] direction Tab To Transferred Tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    Then user successed visit "transferred tab" and user can see detail columns

  @TEST_DOM-629 @Automated @web-covered
  Scenario: [BackOffice][Refund] popup refund section bank
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click on refund button
    And user click on dropdown bank name
    And user click section bank "BCA"
    And user click on dropdown bank name
    And user input bank name "BANK MANTAP (Mandiri Taspen)"
    And user click section bank "BANK MANTAP (Mandiri Taspen)"
    Then popup list bank closed and bank selected

  @TEST_DOM-628 @Automated @web-covered
  Scenario: [BackOffice][Refund] transaction Flip On Transferred Tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin go to Paid Invoice list refund Page
    And user click tab "Transferred" on refund menu
    And user search by "Tenant Phone Number" and input field "083829167577" refund menu
    And user click search button
    Then user successed visit "transferred tab using flip transaction" and user can see detail columns

  @TEST_DOM-627 @Automated @web-covered
  Scenario: [BackOffice][Refund] no Input Bank Account
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click search button
    And user click on refund button
    And user click input account name "noval"
    And user click refund and transfer button
    Then popup close and there will error massage "The refund account field is required when cc transaction id is not present."

  @TEST_DOM-626 @Automated @web-covered
  Scenario: [BackOffice][Refund] no Input Account Name
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click search button
    And user click on refund button
    And user click input account number "300100500"
    And user click refund and transfer button
    Then popup close and there will error massage "The refund account name field is required when cc transaction id is not present."

  @TEST_DOM-625 @Automated @web-covered
  Scenario: [BackOffice][Refund] click Back Button Popup Refund
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And admin go to Paid Invoice list refund Page
    And user click on refund button
    And user click on back button popup refund
    Then admin go to refund Page "Daftar Invoice Refund"

  @TEST_DOM-650 @Automated @web-covered
  Scenario: [BackOffice][Property Level] Create Property Level
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin DOM"
    And user access property management menu
    And user search property name "payment squad 1"
    Then system display property name "payment squad 1"

  @TEST_DOM-651 @Automated @web-covered
  Scenario: [BackOffice][Discount Admin Fee] Discount admin fee recuring booking
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Search Invoice Menu form left bar
    And user search by "Renter Phone Number" and input field "089220220201"
    And user click button search invoice
    And user click detail fee button
    Then user see discount "GP2 Staging" in menu detail fee

  @TEST_DOM-649 @Automated @web-covered
  Scenario: [BackOffice][Manual Payout]: Create manual payout
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu manual payout
    Then user will see detail on manual payout menu

		#      Scenario: Search invoice on manual payout menu
    And user select data manual payout "Invoice Number"
    And user input invoice number "79370282/2021/04/0037"
    And user click button search
    Then user will verify that invoice number

		#      Scenario: search by account name
    And user click reset button
    Then all column search back to default
    And user select data manual payout "Account Name"
    And user input account name "tytyty"
    And user click button search
    Then user will verify that account name

		#      Scenario: search based on disbursement type
    And user click reset button
    Then all column search back to default
    And user select transaction type "Disbursement"
    And user click button search
    Then user verify transaction with "Disbursement"

		#      Scenario: search based on refund type
    And user click reset button
    Then all column search back to default
    And user select transaction type "Refund"
    And user click button search
    Then user verify transaction with "Refund"

		#      Scenario: search based on refund outside mamipay type
    And user click reset button
    Then all column search back to default
    And user select transaction type "Refund Outside MamiPAY"
    And user click button search
    Then user verify transaction with "Refund Outside MamiPAY"

		#      Scenario: Search base on staus transfered
    And user click reset button
    Then all column search back to default
    And user select transaction status "Transferred"
    And user click button search
    Then user verify transaction with status "transferred"

		#      Scenario: Search base on staus Processing
    And user click reset button
    Then all column search back to default
    And user select transaction status "Processing"
    And user click button search
    Then user verify transaction with status "processing"

		#      Scenario: Search base on status Failed
    And user click reset button
    Then all column search back to default
    And user select transaction status "Failed"
    And user click button search
    Then user verify transaction with status "failed"

		#       Scenario: search based on create date
    And user click reset button
    Then all column search back to default
    And user input create date "2021-04-12" and "2021-04-13"
    And user click button search
    Then user verify transaction based on create date from "2021-04-12 16:06:20" to "2021-04-12 09:06:10"
		#        Scenario: create payout without input mandatory data
    And user click button create manual payout
    And user choose type "- Please Select -"
    And user input account number and account name "", ""
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "", "", ""
    And user click button create payout
    Then error massage to input mandatory data will appear "Amount required.", "Reason required."
    And user click button cancel form payout
    Then all column search back to default


		  #      Scenario: create payout with invoice still not allow transfer
    And user click button create manual payout
    And user choose type "Disbursement"
    And user input account number and account name "test AT", "4343353553223"
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "11000", "testing AT", "DP/61392246/2021/05/0037"
    And user click button create payout
    Then payout not created and user get error message "Not allowed to create transfer."

		    #    Scenario: create payout with amount less than 10000
    And user click button create manual payout
    And user choose type "Disbursement"
    And user input account number and account name "test AT", "4343353553223"
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "1000", "testing AT", "79370282/2021/04/0037"
    And user click button create payout
    Then payout not created and user will get error message "Amount minimal 10000."
    And user click button cancel form payout
    Then all column search back to default

		#        Scenario: create manual payout with type disbursement
    And user click button create manual payout
    And user choose type "Disbursement"
    And user input account number and account name "test AT", "4343353553223"
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
    And user click button create payout
    Then payout created with status and user get message "Payout ready to be processed.", "pending"
    And user cancel payout transaction
    Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

		#        Scenario: Create manual payout with type deposit return
    And user click button create manual payout
    And user choose type "Deposit Return"
    And user input account number and account name "test AT", "4343353553223"
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
    And user click button create payout
    Then payout created with status and user get message "Payout ready to be processed.", "pending"
    And user cancel payout transaction
    Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

		#        Scenario: create manual payout with type Refund
    And user click button create manual payout
    And user choose type "Refund"
    And user input account number and account name "test AT", "4343353553223"
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
    And user click button create payout
    Then payout created with status and user get message "Payout ready to be processed.", "pending"
    And user cancel payout transaction
    Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

		#        Scenario: create manual payout with type Refund Outside MamiPAY
    And user click button create manual payout
    And user choose type "Refund Outside MamiPAY"
    And user input account number and account name "test AT", "4343353553223"
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
    And user click button create payout
    Then payout created with status and user get message "Payout ready to be processed.", "pending"
    And user cancel payout transaction
    Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

		#        Scenario: create manual payout with type Refund Charging
    And user click button create manual payout
    And user choose type "Refund Charging"
    And user input account number and account name "test AT", "4343353553223"
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
    And user click button create payout
    Then payout created with status and user get message "Payout ready to be processed.", "pending"
    And user cancel payout transaction
    Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

		#        Scenario: create manual payout with type Payout to Tenant
    And user click button create manual payout
    And user choose type "Payout to Tenant"
    And user input account number and account name "test AT", "4343353553223"
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
    And user click button create payout
    Then payout created with status and user get message "Payout ready to be processed.", "pending"
    And user cancel payout transaction
    Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

		#        Scenario: create manual payout with type Additional Payout to Owner
    And user click button create manual payout
    And user choose type "Additional Payout to Owner"
    And user input account number and account name "test AT", "4343353553223"
    And user choose bank account "Mandiri"
    And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
    And user click button create payout
    Then payout created with status and user get message "Payout ready to be processed.", "pending"

		#          Scenario: change payout type from Additional Payout to Owner to disburse
    And user click change type
    And user change payout type to "Disbursement"
    And user click button submit
    Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Disbursement"

		#          Scenario: change payout type from disburse to refund
    And user click change type
    And user change payout type to "Refund"
    And user click button submit
    Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Refund"

		#          Scenario: change payout type from refund to Refund Outside MamiPAY
    And user click change type
    And user change payout type to "Refund Outside MamiPAY"
    And user click button submit
    Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Refund Outside MamiPAY"


		#          Scenario: change payout type from Refund Outside MamiPAY to Refund Charging
    And user click change type
    And user change payout type to "Refund Charging"
    And user click button submit
    Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Refund Charging"

		#          Scenario: change payout type from Refund Charging to Payout to Tenant
    And user click change type
    And user change payout type to "Payout to Tenant"
    And user click button submit
    Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Payout to Tenant"

		#          Scenario: change payout type from Payout to Tenant to Additional Payout to Owner
    And user click change type
    And user change payout type to "Additional Payout to Owner "
    And user click button submit
    Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Additional Payout to Owner"

		#           Scenario: change invoice number
    And user click change invoice
    And user input new invoice number "18806898/2021/07/0002"
    And user click button submit
    Then invoice number will updated and user get success message "Data telah berhasil diupdate.", "18806898/2021/07/0002"

		#            Scenario: change bank name, account, amount, amd reason
    And user click button edit
    And user input account number and account name "test AT AT", "111111111111"
    And user choose bank account "BRI"
    And user input amount and reason "12011", "change reason AT"
    And user click button create payout
    Then payout updated with new data "111111111111", "bri", "test AT AT", "12011", "change reason AT"

		#            Scenario: click transfer on payout transaction
    And user click transfer
    Then transaction successfully process and user get message "Payout is processing.", "processing"

  @TEST_DOM-648 @Automated @web-covered
  Scenario: [BackOffice][Add Ons List] Create add ons without fill mandatory fields
    Given user navigates to "backoffice"
    When user login as a Admin backoffice
    And user open menu add ons list
    And user see Menu add ons list and there will some fields
    And user click button create add ons
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

		#  Scenario: Create add ons without fill add ons name and price
    Given user input add ons description "monthly laundry"
    When user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

		#  Scenario: create add ons without fill add ons name and description
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons price "50000"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

		#  Scenario: Create add ons without fill price
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons name "payment"
    And user input add ons description "monthly laundry"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

		#  Scenario: Create add ons without fill add ons name
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons description "monthly laundry"
    And user input add ons price "50000"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

		#  Scenario: Create add ons without fill description
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons name "payment"
    And user input add ons price "50000"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

		#  Scenario: create add ons without fill add ons description and price
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons name "payment"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

		#    Scenario: create add ons until success
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons name "payment test AT"
    And user input add ons description "monthly laundry"
    And user input add ons price "50000"
    And user input note for add ons "testing testing AT"
    And user click button create add ons
    And user click yes on the popup create addons

  @TEST_DOM-647 @Automated @web-covered
  Scenario: [BackOffice][Search Invoice] search invoice with use invoice number
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin open menu all invoice
    And admin select invoice number
    And admin input invoice number "ST/90743755/2020/11/0031"
    And admin click button cari invoice
    Then admin verify data transaction


		#   Scenario: admin wrong input invoice
    And admin click button reset
    And admin select invoice number
    And admin input invoice number "ST/90743755/2020/11/003"
    And admin click button cari invoice
    Then admin get blank screen


		#    Scenario: search invoice with use invoice code
    And admin click button reset
    And admin select invoice core
    And admin input invoice number "915000"
    And admin click button cari invoice
    Then admin get blank screen


		#    Scenario: search invoice with use invalid invoice code
    And admin click button reset
    And admin select invoice core
    And admin input invoice number "915"
    And admin click button cari invoice
    Then admin verify data transaction

		#    Scenario: search invoice GP
    And admin click button reset
    And admin select invoice number
    And admin input invoice number "GP2/20210225/00002146/8312"
    And admin click button cari invoice
    Then admin will get data invoice "GP2/20210225/00002146/8312"

		#    Scenario: search wrong invoice GP
    And admin click button reset
    And admin select invoice number
    And admin input invoice number "GP2/20210225/00002146/111"
    And admin click button cari invoice
    Then admin get blank screen

		#    Scenario: search Premium invoice
    And admin click button reset
    And admin select invoice number
    And admin input invoice number "PRE/20210727/48346/36691"
    And admin click button cari invoice
    Then admin will get data invoice "PRE/20210727/48346/36691"

		#    Scenario: search wrong premium invoice
    And admin click button reset
    And admin select invoice number
    And admin input invoice number "PRE/20210727/48346/111"
    And admin click button cari invoice
    Then admin get blank screen

		#      Scenario: search transaction with use method BNI
    And admin click button reset
    And admin choose method "bni"
    And admin click button cari invoice
    Then admin will get data transatcion with method "bni"

		#     Scenario: search transaction with use method mandiri
    And admin click button reset
    And admin choose method "mandiri"
    And admin click button cari invoice
    Then admin will get data transatcion with method "mandiri"

		#      Scenario: search transaction with use method indomaret
    And admin click button reset
    And admin choose method "indomaret"
    And admin click button cari invoice
    Then admin will get data transatcion with method "indomaret"

		#      Scenario: search transaction with use method other bank
    And admin click button reset
    And admin choose method "other"
    And admin click button cari invoice
    Then admin will get data transatcion with method "other"

		#      Scenario: search transaction with use method permata
    And admin click button reset
    And admin choose method "permata"
    And admin click button cari invoice
    Then admin will get data transatcion with method "permata"

		#      Scenario: search transaction with use method gopay
    And admin click button reset
    And admin choose method "gopay"
    And admin click button cari invoice
    Then admin will get data transatcion with method "gopay"

		#      Scenario: search transaction with use method alfamart
    And admin click button reset
    And admin choose method "alfamart"
    And admin click button cari invoice
    Then admin will get data transatcion with method "alfamart"

		#      Scenario: search transaction with use method lawson
    And admin click button reset
    And admin choose method "lawson"
    And admin click button cari invoice
    Then admin will get data transatcion with method "lawson"

		#      Scenario: search transaction with use method dandan
    And admin click button reset
    And admin choose method "dandan"
    And admin click button cari invoice
    Then admin will get data transatcion with method "dandan"

		#      Scenario: search transaction with use method linkaja
    And admin click button reset
    And admin choose method "linkaja"
    And admin click button cari invoice
    Then admin will get data transatcion with method "linkaja"

		#      Scenario: search transaction with use method bri
    And admin click button reset
    And admin choose method "bri"
    And admin click button cari invoice
    Then admin will get data transatcion with method "bri"

		#      Scenario: search transaction with use method dana
    And admin click button reset
    And admin choose method "dana"
    And admin click button cari invoice
    Then admin will get data transatcion with method "dana"

		#      Scenario: search transaction with use method ovo
    And admin click button reset
    And admin choose method "ovo"
    And admin click button cari invoice
    Then admin will get data transatcion with method "ovo"

		#      Scenario: search transaction with use method credit_card
    And admin click button reset
    And admin choose method "credit_card"
    And admin click button cari invoice
    Then admin will get data transatcion with method "credit_card"

		#      Scenario: search transaction based on schedule date
    And admin click button reset
    And admin choose date picker "2021-07-01" and "2021-07-19"
    And admin click button cari invoice
    Then data transaction appeared

		#      Scenario: search transaction based on nomila transaction
    And admin click button reset
    And admin input amount from and to "50000" and "50000"
    And admin click button cari invoice
    Then appeared data with amount "50000"

		#      Scenario: search transaction based on status unpaid
    And admin click button reset
    And admin choose status "Unpaid"
    And admin click button cari invoice
    Then appeared data transaction with status "unpaid"

		#      Scenario: search transaction based on status paid
    And admin click button reset
    And admin choose status "Paid"
    And admin click button cari invoice
    Then appeared data transaction with status "paid"

		#      Scenario: search transaction based on status expired
    And admin click button reset
    And admin choose status "Expired"
    And admin click button cari invoice
    Then appeared data transaction with status "expired"

		#      Scenario: search transaction baded on order type pengajuan sewa
    And admin click button reset
    And admin choose order type "Pengajuan Sewa"
    And admin click button cari invoice
    Then appeared data transaction with order type "Bayar Sewa Kos"

		#      Scenario: search transaction baded on order type ayar Paket Premium
    And admin click button reset
    And admin choose order type "Bayar Paket Premium"
    And admin click button cari invoice
    Then appeared data transaction with order type " Bayar Saldo MamiAds "

		#      Scenario: change transaction from unpaid to paid
    And admin click button reset
    And admin select invoice number
    And admin input invoice number "45111793/2021/04/0018"
    And admin click button cari invoice
    And admin click change status
    And admin change unpaid to paid
    And admin input date and time "2021-02-04 16:35:11"
    And admin submit change
    Then invoice will changes to "paid"

		#    Scenario: change transaction from paid to unpaid
    And admin open menu all invoice
    And admin select invoice number
    And admin input invoice number "45111793/2021/04/0018"
    And admin click button cari invoice
    And admin click change status
    And admin change paid to unpaid
    And admin input date and time "2021-02-04 16:35:11"
    And admin submit change
    Then invoice will changes to "unpaid"


		#    Scenario: change transaction from unpaid to paid not in mamipay
    And admin open menu all invoice
    And admin select invoice number
    And admin input invoice number "45111793/2021/04/0018"
    And admin click button cari invoice
    And admin click change status
    And admin change unpaid to paid
    And admin click checkbox not in mamipay
    And admin input date and time "2021-02-04 16:35:11"
    And admin submit change
    Then invoice will changes to "paid"

		#    Scenario: change transaction from paid to unpaid
    And admin open menu all invoice
    And admin select invoice number
    And admin input invoice number "45111793/2021/04/0018"
    And admin click button cari invoice
    And admin click change status
    And admin change paid to unpaid
    And admin input date and time "2021-02-04 16:35:11"
    And admin submit change
    Then invoice will changes to "unpaid"

  @TEST_DOM-646 @Automated @web-covered
  Scenario: [BackOffice][Discount Admin Fee] Admin edit invoice discount
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on invoice admin fee discount Menu form left bar
    And user click on edit button
    And user input discount amount "999"
    And user click on save button
    Then user see message "Success."

  @TEST_DOM-645 @Automated @web-covered
  Scenario: [BackOffice][Discount Admin Fee] Admin delete invoice discount
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on invoice admin fee discount Menu form left bar
    And user click on delete button
    Then user see message "Success."

  @TEST_DOM-644 @Automated @web-covered
  Scenario: [BackOffice][Discount Admin Fee] Recurring booking discount admin fee
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant3"
    And user click profile on header
    And user click profile dropdown button
    And tenant clicks on kost saya
    And user click on tagihan tab
    And user click sudah di bayar
    And user click on-time
    Then user see discount "GP2 Staging" in detail tagihan

  @TEST_DOM-643 @Automated @web-covered
  Scenario: [BackOffice][Add Ons List] Click button delete on add ons menu
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu add ons list
    And user click button delete on add ons list
    Then system display popup delete add ons
    And system display success delete add ons

  @TEST_DOM-642 @Automated @web-covered
  Scenario: [BackOffice][Add Ons List] visit form edit add ons
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu add ons list
    And user click button edit on add ons list
    Then user will direct to edit add ons form and there will some fields

		#  Scenario: Positive case Edit add ons Name
    Given user edit with new name in name field "Laundry"
    When user click update add ons
    And user click yes on popup
    Then user will direct to list add ons and showing alert success updated new add ons

		#  Scenario: Positive case Edit add ons description
    Given user click button edit on add ons list
    And user edit with new description in description field "Bersih dan Rapi"
    When user click update add ons
    And user click yes on popup
    Then user will direct to list add ons and showing alert success updated new add ons

		#  Scenario: Positive case Edit add ons price
    Given user click button edit on add ons list
    And user edit with new price in price field "50000"
    When user click update add ons
    And user click yes on popup
    Then user will direct to list add ons and showing alert success updated new add ons

		#  Scenario: Positive case Edit add ons notes
    Given user click button edit on add ons list
    And user edit with new notes in notes field "Cepat selesai AT"
    When user click update add ons
    And user click yes on popup
    Then user will direct to list add ons and showing alert success updated new add ons

		#  Scenario: Positive case Click button cancel on edit page
    Given user click button edit on add ons list
    And user edit with new notes in notes field "Cepat selesai AT"
    When user click button cancel on edit add ons page
    Then user see Menu add ons list and there will some fields

		#  Scenario: Positive case Click button cancel on pop up edit
    Given user click button edit on add ons list
    And user edit with new notes in notes field "Bersih"
    When user click update add ons
    And user click cancel on popup
    Then user will direct to edit add ons form and there will some fields

		#  Scenario: Positive case Click button close on pop up edit
    Given user click update add ons
    When user click close on popup
    Then user will direct to edit add ons form and there will some fields

		#  Scenario: Negative case fill add ons name more than 50 char
    Given user can't input add ons name more than 50 char
    When user can't input add ons description more than 50 char
    Then user can't input add ons notes more than 300 char

  @TEST_DOM-641 @Automated @web-covered
  Scenario: [BackOffice][Discount Admin Fee] Admin create invoice discount
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on invoice admin fee discount Menu form left bar
    And user click on add discount setting button
    And user input discount name "Discount Regression"
    And user select kost level
    And user input discount amount "800"
    And user click on checkbox is booking and is non booking transaction
    And user click on save button
    Then user see message "Success."

  @TEST_DOM-680 @Automated @web-covered
  Scenario: [Owner][Payment premium] Filter valid owner number premium
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user choose "Owner Phone Number" on filter search by
    And user input phone input phone number
    And user click button search invoice
    Then system display package invoice list sort by phone number

  @TEST_DOM-679 @Automated @web-covered
  Scenario: [Owner][Payment premium] Filter invalid owner number premium
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user choose "Owner Phone Number" on filter search by
    And user input phone input invalid phone number
    And user click button search invoice
    Then system display package invoice list sort by invalid phone number

  @TEST_DOM-678 @Automated @web-covered
  Scenario: [Owner][Payment premium] Admin search expired invoice number
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user check invoice number expired
    And user click button search invoice
    Then system display invoice number expired with status "expired"

  @TEST_DOM-677 @Automated @web-covered
  Scenario: [Owner][Payment premium] Owner paid premium paket
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "payment" and click on Enter button
    And user click on owner popup
    And user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    And user click "Beli Saldo"
    And user choose saldo "Rp6.000"
    And user click Bayar button
    And user select payment method "OVO" for "paymentW1"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

		#  Scenario: Admin check the transaction on Menu Premium Package status paid
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user search invoice on MamiPAY Premium Package Invoice List
    Then system display status "paid"

  @TEST_DOM-675 @Automated @web-covered
  Scenario: [Owner][Payment premium] Filter valid premium package invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user input valid invoice number
    And user click button search invoice
    Then system display package invoice list filter valid premium package invoice

  @TEST_DOM-676 @Automated @web-covered
  Scenario: [Owner][Payment premium] Filter by status unpaid
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user choose "Unpaid" on filter by status
    And user click button search invoice
    Then system display package invoice list filter by status "unpaid"

  @TEST_DOM-673 @Automated @web-covered
  Scenario: [Owner][Payment premium] Filter Invalid premium package invoice use GP Invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user input gp invoice number
    And user click button search invoice
    Then system display package invoice list search invoice use GP Ivoice

  @TEST_DOM-672 @Automated @web-covered
  Scenario: [Owner][Payment premium] Owner unpaid premium paket
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "payment" and click on Enter button
    And user click on owner popup
    And user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    And user click "Beli Saldo"
    And user choose saldo "Rp6.000"
    And user click Bayar button
    And user select payment method "OVO" for "paymentW1"

		#  Scenario: Admin check the transaction on Menu Premium Package status unpaid
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user search invoice on MamiPAY Premium Package Invoice List
    Then system display status "unpaid"

  @TEST_DOM-674 @Automated @web-covered
  Scenario: [Owner][Payment premium] Filter invalid premium package invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu package invoice list
    And user input invalid invoice number
    And user click button search invoice
    Then system display package invoice list filter invalid premium package invoice

  @TEST_DOM-688 @Automated @web-covered
  Scenario Outline: [BackOffice][Invoice Security] Open Invoice unpaid Booking from search invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Search Invoice Menu form left bar
    And admin choose status "<status>"
    And admin input invoice number "<invoice>"
    And admin click button cari invoice
    And admin click Link "<shortlink>"
		#    And admin click shortlink "<shortlink>"
    Then admin see shown invoice Paid "Pembayaran Berhasil" "Invoice Tidak Ditemukan" "Invoice Kedaluwarsa"
    Examples:
      | status  | invoice               | shortlink |
      | Expired | 34716463/2022/02/0009 | https://pay-jambu.kerupux.com/invoice/fTbwj |
      | Paid    | 12865544/2022/02/0082 | https://pay-jambu.kerupux.com/invoice/lTRwj |
      | Unpaid  | 57653153/2022/02/0003 | https://pay-jambu.kerupux.com/invoice/9H9CP |

  @TEST_DOM-687 @Automated @web-covered
  Scenario Outline: [BackOffice][Invoice Security] Open invoice Paid froms list GP
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin open menu Goldplus invoice list
    And admin click button reset
    And admin choose status "<status>"
    And admin input invoice number "<invoice>"
    And admin click button cari invoice
    And admin click Link "<shortlink>"
    Then admin see shown invoice Paid "Pembayaran Berhasil" "Invoice Tidak Ditemukan" "Invoice Kedaluwarsa"
    Examples:
      | status  | invoice               | shortlink |
      | Paid | GP2/20220217/00005909/3769 | https://pay-jambu.kerupux.com/invoice/select-payment/48481?signature=83511d18f0cb74ebd3c45c6d1ae0c3904672603da4a699ee9b5fb3a945352323 |

  @TEST_DOM-686 @Automated @web-covered
  Scenario Outline: [BackOffice][Invoice Security] Open Invoice Expired Booking from All invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin open menu all invoice
    And admin click button reset
    And admin choose status "<status>"
    And admin input invoice number "<invoice>"
    And admin click button cari invoice
    And admin click Link "<shortlink>"
    Then admin see shown invoice not found "Invoice Kedaluwarsa" "Invoice Tidak Ditemukan"
    Examples:
      | status  | invoice                  | shortlink |
      | Expired | PRE/20220221/50864/80817 | https://pay-jambu.kerupux.com/invoice/select-payment/48611?signature=bab937d125dd83f785beaf0e53fe3405a0aea1aad95f4f8183f3b7c586c38c54 |
      | Paid    | DP/19024270/2022/02/0185 | https://pay-jambu.kerupux.com/invoice/select-payment/48731?signature=995fe6af375ef93d58afcafa5b34a96766644988aa6e02f67262b12229aaf434 |
      | Unpaid  | 44331680/2022/03/0017    | https://pay-jambu.kerupux.com/invoice/select-payment/46750?signature=081102610d26b3bdcaffc1ecd5ad29e277f179b2b6b9978878aab219183a5ff6 |

  @TEST_DOM-685 @Automated @web-covered
  Scenario: [BackOffice][Invoice Security] Open Invoice paid froms list premium
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "premium" and click on Enter button
    And user click on owner popup
    And user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    And user click Riwayat button
    And user click Selesai button
    And user choose invoice paid from list selesai
    Then admin see shown invoice Paid "Pembayaran Berhasil" "Invoice Tidak Ditemukan" "Invoice Kedaluwarsa"


  @TEST_DOM-684 @Automated @web-covered
  Scenario: BackOffice][Invoice Security] Open invoice unpaid froms list premium
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "premium" and click on Enter button
    And user click on owner popup
    And user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    And user click Riwayat button
    And user click dalam proses button
    And user choose invoice unpaid from list selesai
    Then user see shown invoice unpaid "Pembayaran" "Invoice Kedaluwarsa" "Invoice Tidak Ditemukan"

  @TEST_DOM-683 @Automated @web-covered
  Scenario Outline: [BackOffice][Invoice Security] Open invoice unpaid froms list GP
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin open menu Goldplus invoice list
    And admin click button reset
    And admin choose status "<status>"
    And admin click button cari invoice
    And admin click first shortlink
    Then user see shown invoice unpaid "Pembayaran" "Invoice Kedaluwarsa" "Invoice Tidak Ditemukan"
    Examples:
      | status  |
      | Expired |
      | Unpaid  |
