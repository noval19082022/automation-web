@addOns @regression
Feature: Add Ons

  Scenario: Create add ons without fill mandatory fields
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu add ons list
    And user see Menu add ons list and there will some fields
    And user click button create add ons
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

#  Scenario: Create add ons without fill add ons name and price
    Given user input add ons description "monthly laundry"
    When user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

#  Scenario: create add ons without fill add ons name and description
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons price "50000"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

#  Scenario: Create add ons without fill price
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons name "payment"
    And user input add ons description "monthly laundry"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

#  Scenario: Create add ons without fill add ons name
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons description "monthly laundry"
    And user input add ons price "50000"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

#  Scenario: Create add ons without fill description
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons name "payment"
    And user input add ons price "50000"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

#  Scenario: create add ons without fill add ons description and price
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons name "payment"
    And user click button create add ons
    Then there will popup error appear "Please complete all mandatory fields"

#    Scenario: create add ons until success
    Given user open menu add ons list
    When user click button create add ons
    And user input add ons name "payment test AT"
    And user input add ons description "monthly laundry"
    And user input add ons price "50000"
    And user input note for add ons "testing testing AT"
    And user click button create add ons
    And user click yes on the popup create addons





