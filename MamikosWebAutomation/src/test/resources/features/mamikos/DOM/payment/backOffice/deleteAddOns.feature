@addOns @regression
Feature: Delete Add Ons

  Scenario: Positive case click button delete on add ons menu
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu add ons list
    And user click button delete on add ons list
    Then system display popup delete add ons
    And system display success delete add ons