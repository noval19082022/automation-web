@manualpayout @regression
  Feature: inquiry and create manual payout
    Scenario: Visit manual payout
      Given user navigates to "backoffice"
      When user login  as a Admin via credentials
      And user open menu manual payout
      Then user will see detail on manual payout menu

#      Scenario: Search invoice on manual payout menu
        And user select data manual payout "Invoice Number"
        And user input invoice number "79370282/2021/04/0037"
        And user click button search
        Then user will verify that invoice number

#      Scenario: search by account name
        And user click reset button
        Then all column search back to default
        And user select data manual payout "Account Name"
        And user input account name "tytyty"
        And user click button search
        Then user will verify that account name

#      Scenario: search based on disbursement type
        And user click reset button
        Then all column search back to default
        And user select transaction type "Disbursement"
        And user click button search
        Then user verify transaction with "Disbursement"

#      Scenario: search based on refund type
        And user click reset button
        Then all column search back to default
        And user select transaction type "Refund"
        And user click button search
        Then user verify transaction with "Refund"

#      Scenario: search based on refund outside mamipay type
        And user click reset button
        Then all column search back to default
        And user select transaction type "Refund Outside MamiPAY"
        And user click button search
        Then user verify transaction with "Refund Outside MamiPAY"

#      Scenario: Search base on staus transfered
        And user click reset button
        Then all column search back to default
        And user select transaction status "Transferred"
        And user click button search
        Then user verify transaction with status "transferred"

#      Scenario: Search base on staus Processing
        And user click reset button
        Then all column search back to default
        And user select transaction status "Processing"
        And user click button search
        Then user verify transaction with status "processing"

#      Scenario: Search base on status Failed
        And user click reset button
        Then all column search back to default
        And user select transaction status "Failed"
        And user click button search
        Then user verify transaction with status "failed"

#       Scenario: search based on create date
         And user click reset button
         Then all column search back to default
         And user input create date "2021-04-12" and "2021-04-13"
         And user click button search
         Then user verify transaction based on create date from "2021-04-12 16:06:20" to "2021-04-12 16:06:10"

#        Scenario: create payout without input mandatory data
          And user click button create manual payout
          And user choose type "- Please Select -"
          And user input account number and account name "", ""
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "", "", ""
          And user click button create payout
          Then error massage to input mandatory data will appear "Amount required.", "Reason required."
          And user click button cancel form payout
          Then all column search back to default


  #      Scenario: create payout with invoice still not allow transfer
          And user click button create manual payout
          And user choose type "Disbursement"
          And user input account number and account name "test AT", "4343353553223"
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "11000", "testing AT", "DP/61392246/2021/05/0037"
          And user click button create payout
          Then payout not created and user get error message "Not allowed to create transfer."

    #    Scenario: create payout with amount less than 10000
          And user click button create manual payout
          And user choose type "Disbursement"
          And user input account number and account name "test AT", "4343353553223"
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "1000", "testing AT", "79370282/2021/04/0037"
          And user click button create payout
          Then payout not created and user will get error message "Amount minimal 10000."
          And user click button cancel form payout
          Then all column search back to default

#        Scenario: create manual payout with type disbursement
          And user click button create manual payout
          And user choose type "Disbursement"
          And user input account number and account name "test AT", "4343353553223"
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
          And user click button create payout
          Then payout created with status and user get message "Payout ready to be processed.", "pending"
          And user cancel payout transaction
          Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

#        Scenario: Create manual payout with type deposit return
          And user click button create manual payout
          And user choose type "Deposit Return"
          And user input account number and account name "test AT", "4343353553223"
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
          And user click button create payout
          Then payout created with status and user get message "Payout ready to be processed.", "pending"
          And user cancel payout transaction
          Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

#        Scenario: create manual payout with type Refund
          And user click button create manual payout
          And user choose type "Refund"
          And user input account number and account name "test AT", "4343353553223"
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
          And user click button create payout
          Then payout created with status and user get message "Payout ready to be processed.", "pending"
          And user cancel payout transaction
          Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

#        Scenario: create manual payout with type Refund Outside MamiPAY
          And user click button create manual payout
          And user choose type "Refund Outside MamiPAY"
          And user input account number and account name "test AT", "4343353553223"
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
          And user click button create payout
          Then payout created with status and user get message "Payout ready to be processed.", "pending"
          And user cancel payout transaction
          Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

#        Scenario: create manual payout with type Refund Charging
          And user click button create manual payout
          And user choose type "Refund Charging"
          And user input account number and account name "test AT", "4343353553223"
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
          And user click button create payout
          Then payout created with status and user get message "Payout ready to be processed.", "pending"
          And user cancel payout transaction
          Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

#        Scenario: create manual payout with type Payout to Tenant
          And user click button create manual payout
          And user choose type "Payout to Tenant"
          And user input account number and account name "test AT", "4343353553223"
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
          And user click button create payout
          Then payout created with status and user get message "Payout ready to be processed.", "pending"
          And user cancel payout transaction
          Then transaction successfully cancel and user get message "Payout cancelled.", "cancel"

#        Scenario: create manual payout with type Additional Payout to Owner
          And user click button create manual payout
          And user choose type "Additional Payout to Owner"
          And user input account number and account name "test AT", "4343353553223"
          And user choose bank account "Mandiri"
          And user input amount reason and invoice number "11000", "testing AT", "79370282/2021/04/0037"
          And user click button create payout
          Then payout created with status and user get message "Payout ready to be processed.", "pending"

#          Scenario: change payout type from Additional Payout to Owner to disburse
            And user click change type
            And user change payout type to "Disbursement"
            And user click button submit
            Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Disbursement"

#          Scenario: change payout type from disburse to refund
            And user click change type
            And user change payout type to "Refund"
            And user click button submit
            Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Refund"

#          Scenario: change payout type from refund to Refund Outside MamiPAY
            And user click change type
            And user change payout type to "Refund Outside MamiPAY"
            And user click button submit
            Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Refund Outside MamiPAY"


#          Scenario: change payout type from Refund Outside MamiPAY to Refund Charging
            And user click change type
            And user change payout type to "Refund Charging"
            And user click button submit
            Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Refund Charging"

#          Scenario: change payout type from Refund Charging to Payout to Tenant
            And user click change type
            And user change payout type to "Payout to Tenant"
            And user click button submit
            Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Payout to Tenant"

#          Scenario: change payout type from Payout to Tenant to Additional Payout to Owner
            And user click change type
            And user change payout type to "Additional Payout to Owner "
            And user click button submit
            Then payout type is updated and user will get message "Data telah berhasil diupdate.", "Additional Payout to Owner"

#           Scenario: change invoice number
             And user click change invoice
             And user input new invoice number "18806898/2021/07/0002"
             And user click button submit
             Then invoice number will updated and user get success message "Data telah berhasil diupdate.", "18806898/2021/07/0002"

#            Scenario: change bank name, account, amount, amd reason
              And user click button edit
              And user input account number and account name "test AT AT", "111111111111"
              And user choose bank account "BRI"
              And user input amount and reason "12011", "change reason AT"
              And user click button create payout
              Then payout updated with new data "111111111111", "bri", "test AT AT", "12011", "change reason AT"

#            Scenario: click transfer on payout transaction
              And user click transfer
              Then transaction successfully process and user get message "Payout is processing.", "processing"





