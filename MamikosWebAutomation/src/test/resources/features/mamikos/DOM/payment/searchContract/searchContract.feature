@regression @searchContractPf @dom
Feature: search contract

    #  Deleting existing booking
  @adminBatalkanContract
  Scenario: cancel contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access search contract menu
    And user search for "payment" and cancel contract

#  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "PF batalkan kontrak"
    When user navigates to "mamikos /user/booking/"
    And user cancel booking

#  Scenario: Tenant booking and payment for weekly period
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "payment" and select matching result to go to kos details page
    And user choose boarding date is "today" and clicks on Booking button on Kost details page
    And user select payment period "Per Minggu"
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

#  Scenario: Owner accept booking from tenant
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "payment" and click on Enter button
    And user navigates to booking request and filter booking need confirmation
    And user clicks accept button on "payment" booking
    And select first room available and clicks on next button
    And user enters deposit fee, penalty fee, additional costs, down payment, and click on save button
    And user navigates to "mamikos /"
    And user logs out as a Tenant user

#   Scenario: Invoice not paid yet and Admin batalkan contract
    Given user navigates to "backoffice"
    When user click on Search Contract Menu form left bar
    And user Navigate "Search Contract" page
    And user search for "payment" and cancel contract
    Then user see popup succsessful batalkan and status contract change to cancelled

  @seeDetailPopupApik
  Scenario: Admin See detail pop up
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220211208"
    And user click search button
    And user click edit deposit button
    Then user will see detail pop up edit deposit Apik "Edit Deposit for Confirm to Finance"

  @seeDetailPopupForMamirooms
  Scenario: Admin See detail pop up
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081177778888"
    And user fills kost level "mamirooms"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden

  @seeDetailPopupForSinggahsini
  Scenario: Admin See detail pop up
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden

  @inputNameRekeningDetailEditDeposit
  Scenario: Admin See detail pop up
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden
    And user input nama pemilik rekening "Noval"

  @inputNomorRekeningDetailEditDeposit
  Scenario: Admin See detail pop up
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden
    And user input nomor rekening "1550000036"

  @inputBankEditDeposit
  Scenario: Admin See detail pop up
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden
    And user click drop down bank name and choose one bank
    Then user see dropdown will be close

#  @transferDataToNewOwner
#  Scenario: Transfer Data To New Owner
#    Given user navigates to "backoffice"
#    And user login  as a Admin via credentials
#    And user click on Search Contract Menu form left bar
#    Then user Navigate "Search Contract" page
#    And user search renter phone number "081280003230"
#    And user fills kost level "SinggahSini"
#    And user click search button
#    And user click ganti owner button
#    And user input new owner phone number "083843666900"
#    And user click cari alternatif pemilik
#    And user click proses ganti pemilik
#    Then user redirected to menu search contract
#    And user search owner phone number "083843666900"
#    And user fills kost level "SinggahSini"
#    And user click search button
#    And user click ganti owner button
#    And user input new owner phone number "0891202112"
#    And user click cari alternatif pemilik
#    And user click proses ganti pemilik
#    Then user redirected to menu search contract

#  @cancelTransferDataToNewOwner
#  Scenario: Cancel Admin ganti owner
#    Given user navigates to "backoffice"
#    And user login  as a Admin via credentials
#    And user click on Search Contract Menu form left bar
#    Then user Navigate "Search Contract" page
#    And user search renter phone number "081280003230"
#    And user fills kost level "SinggahSini"
#    And user click search button
#    And user click ganti owner button
#    And user input new owner phone number "083843666900"
#    And user click cari alternatif pemilik
#    And user click cancel proses ganti pemilik
#    Then data other owner will be gone

  @seeSisaDeposit
  Scenario: Admin see sisa deposit
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220211208"
    And user click search button
    And user click edit deposit button
    Then user will see detail pop up edit deposit Apik "Edit Deposit for Confirm to Finance"
    And user input biaya kerusakan "50000"
    Then user will see sisa deposit

  @seeDataContract
  Scenario: Admin see data contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    And user click see log button
    Then user will see detail data contract

  @seePopupTerminateAkhiriContract
  Scenario: Admin See detail pop up
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user fills kost level "APIK"
    And user click search button
    And user click akhiri contract button
    Then user see new popup termination appeared

  @seeLihatAkhiriKontrakDisable
  Scenario: Admin See detail pop up
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220220105"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click akhiri contract button
    Then user see akhiri kontrak button disabled

  @SearchDataTenantBasedOnPeriod
  Scenario: Admin search data tenant based on period custome range
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    Then user choose contract date period
    And user select date period "Today"
    And user click search button
    Then user choose contract date period
    And user select date period "Yesterday"
    And user click search button
    Then user Navigate "Search Contract" page

  @cancelExtendContract
  Scenario: Cancel Extend Contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220220105"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click button extend contract
    Then user redirect to search contract menu detail

  @searchBasedOnPeriod
  Scenario: Search base on period
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220220101"
    And user fills kost level "SinggahSini"
    And user click search button
    Then user redirect to search contract menu detail

  @SearchDataTenantBasedOnKostLevel
  Scenario: Admin search data tenant based on period custome range
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    When user fills kost level "Mamikos Goldplus 2"
    And user click search button
    Then user verify that detail kos is "Mamikos Goldplus 2"

  @searchValidInput
  Scenario Outline: Search by valid input
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "<searchBy>" and input field "<input>"
    And user click search button
    Then user will get data detail
    Examples:
      | searchBy              | input                 |
      | Kost Name             | Kost Princess         |
      | Owner Phone Number    | 083843666900          |
      | Renter Phone Number   | 083139263046          |
      | Renter Name           | Ullrich               |
      | Related Invoice Number| 83900841/2021/12/0043 |


  @searchInvalidInput
  Scenario Outline: Search by invalid input
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "<searchBy>" and input field "<input>"
    And user click search button
    Then user will get blank data detail
    Examples:
      | searchBy              | input               |
      | Kost Name             | kost anggun         |
      | Owner Phone Number    | 0856220211208       |
      | Renter Phone Number   | 0856220211208       |
      | Renter Name           | embul owner         |
      | Related Invoice Number| 83900841/2021/12/00 |
      | Related Invoice Code  | 83900841            |

  @InputDamageDetails
  Scenario: Input Damage Details more than 200 characters
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "089220211208"
    And user click search button
    And user click edit deposit button
    And user Input damage details "characters more than 200"
    Then user see maximal length "200"

  @AdminSimpanDraft
  Scenario: Admin simpan draft
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by "Renter Phone Number" and input field "081280003230"
    And user fills kost level "SinggahSini"
    And user click search button
    And user click edit deposit button
    Then user will see Konfirmasi Sisa Deposit button hidden
    And user input nomor rekening "1550000036"
    And user input nama pemilik rekening "Noval"
    And user input transfer date
    And user click on simpan Draft button
    Then user will see message "Berhasil disimpan sebagai draf"