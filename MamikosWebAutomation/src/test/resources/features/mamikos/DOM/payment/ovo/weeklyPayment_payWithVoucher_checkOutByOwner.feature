@regression  @weeklyPayment

Feature: Weekly rent - pay with voucher - Checkout By Owner

#  Deleting existing booking
  Background:cancel contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access search contract menu
    And user search for "payment" and cancel contract

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "tenantPayment"
    When user navigates to "mamikos /user/booking/"
    And user cancel booking

#  Scenario: Tenant booking and payment for weekly period
    Given user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "payment" and select matching result to go to kos details page
    And user choose boarding date is "today" and clicks on Booking button on Kost details page
    And user select payment period "Per Minggu"
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

#  Scenario: Owner accept booking from tenant
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "payment" and click on Enter button
    And user navigates to booking request and filter booking need confirmation
    And user select kost with name "kost payment automation casablanca campur"
    And user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button
    And user navigates to "mamikos /"
    And user logs out as a Owner user

#  Scenario: Tenant pay boarding house for weekly rent
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "tenantPayment"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user select payment method "OVO" for "paymentW1"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

#  Scenario: Tenant check in kost weekly payment
    Given user navigates to "mamikos /"
    When user navigates to Booking History page
    And user click check in button with rental period is "1 Minggu"
    Then system display terminate contract link

# Scenario: Owner Checkout in Weekly Payment
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "payment" and click on Enter button
    And user navigates to Booking Request page
    And user click tab "Terbayar" and see contract
    And user click terminated contract
    And owner select reason terminate contract "Selesai Menyewa" and click terminate contract button
    Then system display warning message "Kontrak dihentikan" on billing details