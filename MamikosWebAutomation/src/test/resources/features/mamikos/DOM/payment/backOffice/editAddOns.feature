@addOns @regression
Feature: Edit Add Ons

  Scenario: Positive case visit form edit add ons
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu add ons list
    And user click button edit on add ons list
    Then user will direct to edit add ons form and there will some fields

#  Scenario: Positive case Edit add ons Name
    Given user edit with new name in name field "Laundry"
    When user click update add ons
    And user click yes on popup
    Then user will direct to list add ons and showing alert success updated new add ons

#  Scenario: Positive case Edit add ons description
    Given user click button edit on add ons list
    And user edit with new description in description field "Bersih dan Rapi"
    When user click update add ons
    And user click yes on popup
    Then user will direct to list add ons and showing alert success updated new add ons

#  Scenario: Positive case Edit add ons price
    Given user click button edit on add ons list
    And user edit with new price in price field "50000"
    When user click update add ons
    And user click yes on popup
    Then user will direct to list add ons and showing alert success updated new add ons

#  Scenario: Positive case Edit add ons notes
    Given user click button edit on add ons list
    And user edit with new notes in notes field "Cepat selesai AT"
    When user click update add ons
    And user click yes on popup
    Then user will direct to list add ons and showing alert success updated new add ons

#  Scenario: Positive case Click button cancel on edit page
    Given user click button edit on add ons list
    And user edit with new notes in notes field "Cepat selesai AT"
    When user click button cancel on edit add ons page
    Then user see Menu add ons list and there will some fields

#  Scenario: Positive case Click button cancel on pop up edit
    Given user click button edit on add ons list
    And user edit with new notes in notes field "Bersih"
    When user click update add ons
    And user click cancel on popup
    Then user will direct to edit add ons form and there will some fields

#  Scenario: Positive case Click button close on pop up edit
    Given user click update add ons
    When user click close on popup
    Then user will direct to edit add ons form and there will some fields

#  Scenario: Negative case fill add ons name more than 50 char
    Given user can't input add ons name more than 50 char
    When user can't input add ons description more than 50 char
    Then user can't input add ons notes more than 300 char
    
