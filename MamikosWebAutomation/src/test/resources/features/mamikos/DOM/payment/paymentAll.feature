@payment @DOM3
Feature: [Test-Execution][Payment] Web -  Staging Automation Annually rent all Payment

  @extendContract
  Scenario: extend contract from admin
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access search contract menu
    And user search by "Renter Phone Number" and input field "0892202104"
    And user click search button
    And user click button extend contract
    And user fills duration "8" month
    And user click extend button
    Then user Navigate "Search Contract" page

  @paymentBni
  Scenario: Tenant pay kos BNI
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user navigates to "mamikos /user/kost-saya/billing"
    And tenant clicks on Bayar button
    And user select payment method "BNI" for "paymentPF"
    Then system display payment using "BNI" is "Transaction success. VA number 9881012892202104 has been paid with amount IDR 1006000"

  @paymentCreditCard
  Scenario: Tenant pay kos credit card
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user navigates to "mamikos /user/kost-saya/billing"
    And tenant clicks on Bayar button
    And user select payment method "Kartu Kredit" for "paymentPF"
    Then system display payment using "Kartu Kredit" is "Pembayaran"

  @paymentDana
  Scenario: Tenant pay kos Dana
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user navigates to "mamikos /user/kost-saya/billing"
    And tenant clicks on Bayar button
    And user select payment method "DANA" for "paymentPF"
    Then system display payment using "DANA" is "Pembayaran Berhasil"

  @paymentLinkAja
  Scenario: Tenant pay kos LinkAja
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user navigates to "mamikos /user/kost-saya/billing"
    And tenant clicks on Bayar button
    And user select payment method "LinkAja" for "paymentPF"
    Then system display payment using "LinkAja" is "Pembayaran Berhasil"

  @paymentMandiri
  Scenario: Tenant pay kos mandiri
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user navigates to "mamikos /user/kost-saya/billing"
    And tenant clicks on Bayar button
    And user select payment method "Mandiri" for "paymentPF"
    Then system display payment using "Mandiri" is "Success Transaction"

  @paymentOvo
  Scenario: Tenant pay kos ovo
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user navigates to "mamikos /user/kost-saya/billing"
    And tenant clicks on Bayar button
    And user select payment method "OVO" for "paymentPF"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

  @paymentPermata
  Scenario: Tenant pay kos permata
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant"
    And user navigates to "mamikos /user/kost-saya/billing"
    And tenant clicks on Bayar button
    And user select payment method "Permata" for "paymentPF"
    Then system display payment using "Permata" is "Transaksi Sukses"