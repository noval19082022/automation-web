@regression @discountAdminFee
Feature: Discount Admin Fee

  @createDiscountAdminFee
  Scenario: Admin create invoice discount
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on invoice admin fee discount Menu form left bar
    And user click on add discount setting button
    And user input discount name "Discount Regression"
    And user select kost level
    And user input discount amount "800"
    And user click on checkbox is booking and is non booking transaction
    And user click on save button
    Then user see message "Success."

  @editDiscountAdminFee
  Scenario: Admin edit invoice discount
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on invoice admin fee discount Menu form left bar
    And user click on edit button
    And user input discount amount "999"
    And user click on save button
    Then user see message "Success."

  @deleteDiscountAdminFee
  Scenario: Admin delete invoice discount
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on invoice admin fee discount Menu form left bar
    And user click on delete button
    Then user see message "Success."

  @recuringBookingDiscountAdminFee
  Scenario: Recurring booking discount admin fee
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "UG Tenant3"
    And user click profile on header
    And user click profile dropdown button
    And tenant clicks on kost saya
    And user click on tagihan tab
    And user click sudah di bayar
    And user click on-time
    Then user see discount "GP2 Staging" in detail tagihan

  @checkDiscountAdminFeeRecurring
  Scenario: discount admin fee recuring booking
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Search Invoice Menu form left bar
    And user search by "Renter Phone Number" and input field "089220220201"
    And user click button search invoice
    And user click detail fee button
    Then user see discount "GP2 Staging" in menu detail fee