Feature: Navbar Tenant

	@TEST_DOM-1857 @Automated @DOM4 @Web @discovery-platform @navbar @navbar-before-login @navbar-search
	Scenario: [Dweb][Navbar] Check Navbar in Search Page Before login
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user search for keyword "UGM"
		Then navbar before login appears
	@TEST_DOM-1868 @Automated @DOM4 @Web @discovery-platform @navbar @navbar-before-login @navbar-other
	Scenario Outline: [Dweb][Navbar]Check Navbar in url Page Before login 1
		Given user navigates to "mamikos /<url>"
		Then navbar kost page before login appears
		Examples:
		  | page       | url         |
		  | Kost       | /kost       |
		  | Booking    | /booking    |
	@TEST_DOM-1859 @Automated @DOM4 @Web @discovery-platform @navbar @navbar-before-login @navbar-other
	Scenario Outline: [Dweb][Navbar]Check Navbar in url Page Before login 2
		Given user navigates to "mamikos /<url>"
		Then navbar before login appears
		Examples:
		  | page       | url         |
		  | Promo kost | /promo-kost |
		  | Favorite   | /history    |
	@TEST_DOM-1866 @Automated @DOM4 @Web @discovery-platform @navbar @navbar-before-login @navbar-kost-detail
	Scenario: [Dweb][Kost Detail][Navbar]Check Navbar in Kost Detail Page Before login
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "favoritekost" and select matching result to go to kos details page
		And I should reached kos detail page
		Then navbar before login appears
	@TEST_DOM-1852 @Automated @DOM4 @Web @discovery-platform @navbar @navbar-after-login @navbar-search
	Scenario: [Dweb][Navbar]Check Navbar in Search Page After login
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		And user clicks search bar
		And user search for keyword "UGM"
		Then navbar after login appears
	@TEST_DOM-1850 @Automated @DOM4 @Web @discovery-platform @navbar @navbar-after-login @navbar-other
	Scenario Outline: [Dweb][Navbar]Check Navbar in url Page After login
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		And user navigates to "mamikos /<url>"
		Then navbar after login appears
		Examples:
		  | page       | url        |
		  | Kost       | kost       |
		  | Booking    | booking    |
		  | Promo kost | promo-kost |
		  | Favorite   | history    |
	@TEST_DOM-1847 @Automated @DOM4 @Web @discovery-platform @navbar @navbar-after-login @navbar-kost-detail
	Scenario: [Dweb][Kost Detail][Navbar] Check Navbar in Kost Detail Page After login
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		And user clicks search bar
		And I search property with name "favoritekost" and select matching result to go to kos details page
		And I should reached kos detail page
		Then navbar after login appears
