Feature: FTUE Booking Benefit

	@TEST_DOM-1718 @DOM4 @FTUE-Booking @automated @discovery-platform @web
	Scenario: [Dweb][Kost Detail][FTUE Booking Benefit] Finish FTUE
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		When user clicks search bar
		And user search for Kost with name "FTUEKostName2" and selects matching result without dimiss FTUE
		And FTUE Booking Benefit appear with slides image
		And user click Saya Mengerti button FTUE
		Then FTUE Booking Benefit will not appear
	@TEST_DOM-1716 @DOM4 @FTUE-Booking @automated @discovery-platform @web
	Scenario: [Dweb][Kost Detail][FTUE Booking Benefit] Second time open BBK Kos before finish
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		When user clicks search bar
		And user search for Kost with name "FTUEKostName1" and selects matching result without dimiss FTUE
		And FTUE Booking Benefit appear with slides image
		And user navigates to "mamikos /"
		When user clicks search bar
		And user search for Kost with name "FTUEKostName2" and selects matching result without dimiss FTUE
		Then FTUE Booking Benefit appear with slides image
	@TEST_DOM-1719 @DOM4 @FTUE-Booking @automated @discovery-platform @web
	Scenario: [Dweb][Kost Detail][FTUE Booking Benefit] Second time open BBK Kos after finish
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		When user clicks search bar
		And user search for Kost with name "chatKostName1" and selects matching result
		And user navigates to "mamikos /"
		When user clicks search bar
		And user search for Kost with name "FTUEKostName2" and selects matching result without dimiss FTUE
		Then FTUE Booking Benefit will not appear
	@TEST_DOM-1717 @DOM4 @FTUE-Booking @automated @discovery-platform @web
	Scenario: Dweb][Kost Detail][FTUE Booking Benefit] First time open Non BBK Kos, FTUE BB will not appear
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		When user clicks search bar
		And user search for Kost with name "FTUEKostName3" and selects matching result without dimiss FTUE
		Then FTUE Booking Benefit will not appear
	@TEST_DOM-1715 @DOM4 @FTUE-Booking @automated @discovery-platform @web
	Scenario: [Dweb][Kost Detail][FTUE Booking Benefit] First time open BBK Kos
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		When user clicks search bar
		And user search for Kost with name "FTUEKostName2" and selects matching result without dimiss FTUE
		Then FTUE Booking Benefit appear with slides image
	@TEST_DOM-1714 @DOM4 @FTUE-Booking @automated @discovery-platform @web
	Scenario: [Dweb][Kost Detail][FTUE Booking Benefit] FTUE will not appear on apartemen
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		When user choose "Apartemen" on dropdown list ads search
		And user search apartment using keyword "rane"
		And user click on the selected apartment to go to the apartment details
		Then FTUE Booking Benefit will not appear
