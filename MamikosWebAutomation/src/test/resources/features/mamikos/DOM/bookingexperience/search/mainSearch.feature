Feature: Main Search

	@TEST_DOM-1844 @Automated @DOM4 @Web @discovery-platform @search @search-suggest
	Scenario: [Dweb][Search]5 Suggestion List should appear
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user search for random keyword "Semarang"
		Then should display the result list of keyword "Semarang"
	@TEST_DOM-1853 @Automated @DOM4 @Web @discovery-platform @search @search-except-suggest
	Scenario: [Dweb][Search] Typing exception character
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user search for random keyword "asdfadjsade"
		Then should display the result exception "Tidak menemukan nama tempat / nama kost yang sesuai."
	@TEST_DOM-1849 @Automated @DOM4 @Web @discovery-platform @search @search-clear-text
	Scenario: [Dweb][Search] Reset text on searchbar
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user search for random keyword "Jakarta"
		And user clear the text on searchbar
		Then user see searchbar is empty
	@TEST_DOM-1848 @Automated @DOM4 @Web @discovery-platform @search @search-auto-area
	Scenario: [Dweb][Search] Select Autocomplete for "Area"
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user search for random keyword "Sindu"
		And should display the result area list of keyword "sindu"
		Then user click the search result based on area
	@TEST_DOM-1846 @Automated @DOM4 @Web @discovery-platform @search @search-auto-nama
	Scenario: [Dweb][Search] Select Autocomplete for "Nama"
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user search for random keyword "ran"
		And should display the result name list of keyword "ran"
		Then user click the search result based on name
	@TEST_DOM-1871 @Automated @DOM4 @Web @discovery-platform @search @search-area-list
	Scenario: [Dweb][Search] Search Kos - Area List have correct dropdown
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Area
		Then After user click City name, city name will expand and Area name listed below it.
		  | Bali     | Balikpapan         | Banyumas           | Cilegon    | Tasikmalaya |
		  | Kuta     | Balikpapan Kota    | Purwokerto Barat   | Purwakarta | Cibeureum   |
		  | Denpasar | Balikpapan Utara   | Purwokerto Timur   | Jombang    | Tawang      |
		  | Renon    | Balikpapan Tengah  | Purwokerto Selatan | Grogol     | Mangkubumi  |
		  | Seminyak | Balikpapan Selatan | Purwokerto Utara   | Citangkil  | Cihideung   |
	@TEST_DOM-1869 @Automated @DOM4 @Web @discovery-platform @search @search-pupular-list
	Scenario: [Dweb][Search] Search Kos - Popular area have correct city
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Area
		Then under popular search, there's this city :
		  | Yogyakarta     |
		  | Jakarta        |
		  | Bandung        |
		  | Surabaya       |
		  | JakartaSelatan |
		  | Malang         |
		  | Semarang       |
		  | Makassar       |
		  | Medan          |
	@TEST_DOM-1862 @Automated @DOM4 @Web @discovery-platform @search @search-popular-city
	Scenario: [Dweb][Search] Search Kos in popular area have correct search result
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Area
		And user clicks button "Malang" below
		Then listing that appear have location in "Malang"
	@TEST_DOM-1861 @Automated @DOM4 @Web @discovery-platform @search @search-popular-area-city
	Scenario: [Dweb][Search] Search Kos - Area based on city list
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Area
		And user clicks city "Bali"
		And user clicks "Denpasar" below the city
		Then listing that appear have location in "Denpasar"
	@TEST_DOM-1860 @Automated @DOM4 @Web @discovery-platform @search @search-check-appearance-apartment
	Scenario Outline: [Dweb][Search] Check appearance of Apartemen property on listing with keyword city
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user search for random keyword "<city>"
		Then user click the search result based on area
		Then listing that appear have no "Apartemen" property
		Examples:
		  | city            |
		  | Jakarta         |
		  | Jakarta Selatan |
		  | Surabaya        |
		  | Bandung         |
		  | Yogyakarta      |
		  | Jakarta Pusat   |
		  | Semarang        |
		  | Jakarta Barat   |
		  | Malang          |
		  | UGM             |
		  | Tebet           |
		  | Kontrakan       |
		  | Bekasi          |
		  | UNDIP           |
	@TEST_DOM-1858 @Automated @DOM4 @Web @discovery-platform @search @search-result
	Scenario: [Dweb][Search] Check search result
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Area
		And user clicks button "Malang" below
		Then title listing that appear have location in "Malang"
	@TEST_DOM-1865 @Automated @DOM4 @Web @discovery-platform @search @search-check-label-elite
	Scenario: [Dweb][Search] Check elite label on kost list
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Area
		And user clicks button "Yogyakarta" below
		And user activate elite filter
		Then user see elite label on kos card
	@TEST_DOM-1864 @Automated @DOM4 @Web @discovery-platform @search @search-popular-campus-list
	Scenario: [Dweb][Search] Search Kos - Popular campus
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Campus
		Then user verify popular campus
		  | UGM              |
		  | UNPAD Jatinangor |
		  | STAN Jakarta     |
		  | UNAIR            |
		  | UB               |
		  | UNY              |
		  | UI               |
		  | UNDIP            |
		  | ITB              |
		  | UMY              |
	@TEST_DOM-1863 @Automated @DOM4 @Web @discovery-platform @search @search-campus-city
	Scenario: [Dweb][Search]Search Kos - Campus Lists By City
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Campus
		Then user verify campus lists by cities
		  | Bali         | Bandung            | Bogor | Depok           | Jakarta       | Jakarta Pusat |
		  | ISI Denpasar | UNPAD Jatinangor   | IPB   | Gundar Margonda | STAN Jakarta  | UI Salemba    |
	@TEST_DOM-1856 @Automated @DOM4 @Web @discovery-platform @search @search-popular-campus-result
	Scenario: [Dweb][Search]Search Kos - Popular campus result
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Campus
		And user clicks button "UNDIP" below
		Then listing that appear have location in "Semarang"
	@TEST_DOM-1855 @Automated @DOM4 @Web @discovery-platform @search @search-campus-city-result
	Scenario: [Dweb][Search]Search Kos - Popular campus based on city list
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Campus
		And user clicks city "Bali"
		And user clicks "ISI Denpasar" below the city
		Then listing that appear have location in "Denpasar" or "Bali"
	@TEST_DOM-1854 @Automated @DOM4 @Web @discovery-platform @search @search-popular-station
	Scenario: [Dweb][Search]Search Kos - Popular station and stop
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Station & Stop
		Then user verify popular station & stop
		  | Halte Harmoni Central   |
		  | Halte Cawang Uki        |
		  | Halte Kuningan Barat    |
		  | Stasiun MRT Lebak Bulus |
		  | Stasiun MRT Bundaran HI |
		  | Halte Transjakarta Kota |
		  | Halte Bendungan Hilir   |
		  | Halte Halimun           |
		  | Halte Monas             |
		  | Stasiun MRT Blok M      |
	@TEST_DOM-1845 @Automated @DOM4 @Web @discovery-platform @search @search-stasiiun-list
	Scenario: [Dweb][Search] Search Kos - Station Lists By City
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Station & Stop
		Then user verify station lists by cities
		  | Bandung              | Jakarta                       | Malang                  | Semarang                | Surabaya               | Yogyakarta          |
		  | Stasiun Cikudapateuh | Stasiun MRT Setiabudi         | Stasiun Ngebruk         | Stasiun Tawang          | Stasiun Gubeng         | Stasiun Wates       |
		  | Stasiun Ciroyom      | Stasiun Dukuh Atas / Sudirman | Stasiun Kepanjen        | Stasiun Jerakah         | Stasiun Pasar Turi     | Stasiun Lempuyangan |
		  | Stasiun Andir        | Halte Monas                   | Stasiun Malang Kotabaru | Stasiun Alastua         | Stasiun Surabaya Kota  | Stasiun Rewulu      |
		  | Stasiun Cimindi      | Stasiun MRT Bendungan Hilir   | Stasiun Pakisaji        | Stasiun Semarang Gudang | Stasiun Wonokromo      | Stasiun Patukan     |
	@TEST_DOM-1851 @Automated @DOM4 @Web @discovery-platform @search @search-result-halte
	Scenario: [Dweb][Search] Search Kos based on Stasiun and stop
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Station & Stop
		And user clicks button "Halte Halimun" below
		Then listing that appear have location in "Jakarta"
	@TEST_DOM-1867 @Automated @DOM4 @Web @discovery-platform @search @search-result-station-city
	Scenario: [Dweb][Search] Search Kos - Station based on city list
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user clicks Station & Stop
		And user clicks city "Bandung"
		And user clicks "Stasiun Cimekar" below the city
		Then listing that appear have location in "Bandung"