Feature: areaBoundaries

	@TEST_DOM-1748 @Automated @DOM4 @Web @area-1 @area-boundaries @discovery-platform @listing-kos
	Scenario: [Dweb][Listing Kos][Area Boundaries]Check the landing of area boundaries if the result is 1
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for random keyword "Padang"
		And user click the search result based on area "Padang Bulan"
		Then user validates the result is "Kos di Padang Bulan"
		When user set filter price and input "750000" , "750000"
		Then user sees kos listing is just 1
		And user sees nominatim map is on center of kos listing
		And user sees messages "Coba kurangi atau hapus filter untuk hasil pencarian yang lebih" in landing boundaries if result is one
		When user clicks reset button
		Then user sees kos listing is more than 1
	@TEST_DOM-1747 @Automated @DOM4 @Web @area-0 @area-boundaries @discovery-platform @listing-kos
	Scenario: [Dweb][Listing Kos][Area Boundaries]Check the landing of area boundaries if the result is 0
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for random keyword "Lumajang"
		And user click the search suggestion based on subarea "Kabupaten Wonosobo"
		Then user validates the result is "Kos di Lumajang"
		And user sees the empty state in area boundaries landing page "Belum Ada Kos di Area Ini"
	@TEST_DOM-1749 @Automated @DOM4 @Web @area-20 @area-boundaries @discovery-platform @listing-kos
	Scenario: [Dweb][Listing Kos][Area Boundaries]Check the landing of area boundaries if the result is more than 20 kos
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "Bogor"
		Then user validates the result is "Kos di Bogor"
		And user sees nominatim map on landing boundaries
		And user sees kos listing up to 16
		And user sees back to top and see more property buttons
		And user clicks and validates see more property and back to top buttons
	@TEST_DOM-1746 @Automated @DOM4 @Web @area-boundaries @discovery-platform @landing-maps-boundaries @listing-kos
	Scenario: [Dweb][Listing Kos][Area Boundaries]Check the redirection of search by maps button on area boundaries landing page
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "Bogor"
		Then user validates the result is "Kos di Bogor"
		And user sees nominatim map on landing boundaries
		When user clicks search by maps button
		Then listing that appear have location in "Bogor"
	@TEST_DOM-1745 @Automated @DOM4 @Web @area-0 @area-boundaries @area-filter-0 @discovery-platform @listing-kos
	Scenario: [Dweb][Listing Kos][Area Boundaries]Check the landing of area boundaries if the result is 0 with Filter
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for random keyword "Padang"
		And user click the search result based on area "Padang Bulan"
		Then user validates the result is "Kos di Padang Bulan"
		When user selects filter "Singgahsini"
		Then user sees the empty state in area boundaries landing page "Kos Tidak Ditemukan"
	@TEST_DOM-1744 @Automated @DOM4 @Web @area-boundaries @discovery-platform @listing-kos @price-min-boundaries
	Scenario: [Dweb][Listing Kos][Area Boundaries]Check the sorting minimum to maximum price on area boundaries landing page
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "Bogor"
		Then user validates the result is "Kos di Bogor"
		When user selects sorting "Harga termurah" in area boundaries landing page
		Then user validates the price of first listing is cheaper than the last listing in area boundaries landing page
		When user clicks search by maps button
		Then user validates the result is "Kos di area sesuai peta"
		And listing that appear have location in "Bogor"
		And user selects sorting "Harga termurah" in kost listing
		And user validates the price of first listing is cheaper than the last listing in listing property page
	@TEST_DOM-1743 @Automated @DOM4 @Web @area-boundaries @discovery-platform @listing-kos @price-max-boundaries
	Scenario: [Dweb][Listing Kos][Area Boundaries]Check the sorting maximum to minimum price on area boundaries landing page
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "Depok"
		Then user validates the result is "Kos di Depok"
		When user selects sorting "Harga termahal" in area boundaries landing page
		Then user validates the price of first listing is more expensive than the last listing in area boundaries landing page
		When user clicks search by maps button
		Then user validates the result is "Kos di area sesuai peta"
		And listing that appear have location in "Depok"
		And user selects sorting "Harga termahal" in kost listing
		And user validates the price of first listing is more expensive than the last listing in listing property page
