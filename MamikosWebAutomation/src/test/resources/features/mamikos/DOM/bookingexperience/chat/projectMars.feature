@DOM5
Feature: Mars Project

	Background:
	Given user navigates to "mamikos admin"
	And user logs in to Mamikos Admin via credentials as "admin ob"
	When user access to Property Package menu
	And user search owner property name "Kost alogoz" on property package
	And user terminate active goldplus

	@TEST_DOM-3538 @Automated @DOM5 @MARS-DOM @Web @chatRoomBlocked @discovery-platform
	Scenario: [Web][Mars] Chatroom is blocked
	Given user navigates to "mamikos /"
	And user clicks on Enter button
	And user login in as Tenant via phone number as "mars tenant"
	And user clicks search bar
	And I search property with name "mars kos" and select matching result to go to kos details page
	And user click chat in kos detail
	And user select question "Cara menghubungi pemilik?"
	And chat room appear with latest message "Mohon tunggu balasan dari pemilik kos ini."
	And user navigates to "mamikos /"
	And user logs out as a Tenant user
	And user clicks on Enter button
	And user fills out owner login as "mars owner" and click on Enter button
	And user click chat button in top bar
	And search chat in chatlist "Chat blocked"
	And tenant click on first index chat on chat list
	Then chat room appear with latest message "Mohon tunggu balasan dari pemilik kos ini."
	And chat room interface is blocked and quota "1 chat room"
	@TEST_DOM-3539 @Automated @DOM5 @MARS-DOM @Web @chatRoomHaveReplyChat @discovery-platform
	Scenario: [Web][Mars] Chatroom is no blocked
	Given user navigates to "mamikos /"
	And user clicks on Enter button
	And user fills out owner login as "mars owner" and click on Enter button
	And owner non GP "mamikos"
	And user click chat button in top bar
	And search chat in chatlist "apple"
	And tenant click on first index chat on chat list
	Then chat room appear with latest message "Experiment"
	And chat room interface is no blocked "Anda bisa berbalas chat tanpa kuota di chat room ini"
	@TEST_DOM-3540 @Automated @DOM5 @MARS-DOM @Web @chatRoomNoBlocked @discovery-platform
	Scenario: [Web][Mars] Chatroom is no blocked owner GP
	Given user navigates to "mamikos /"
	And user clicks on Enter button
	And user fills out owner login as "mars owner" and click on Enter button
	And owner non GP "mamikos"
	And user click chat button in top bar
	And search chat in chatlist "Chat blocked"
	And tenant click on first index chat on chat list
	Then chat room appear with latest message "Mohon tunggu balasan dari pemilik kos ini."
	And chat room interface is blocked and quota "1 chat room"
	And user click Daftar GoldPlus field
	And user redirect to List Package
	And user click pilih paket on "Goldplus 2"
	Then user redirect to Detail Tagihan
	And user click button bayar sekarang
	And user select payment method "OVO" for "PayLGAutomation"
	And system display payment using "OVO" is "Pembayaran Berhasil"
	And user navigates to "owner /"
	And user click button close popup
	And user click chat button in top bar
	And user click FTUE mars
	And search chat in chatlist "Chat blocked"
	And tenant click on first index chat on chat list
	Then chat room appear with latest message "Mohon tunggu balasan dari pemilik kos ini."
	@TEST_DOM-3541 @Automated @DOM5 @HideLastSeenChatroom @MARS-DOM @Web @discovery-platform
	Scenario: [Web][Mars] Hide owner last seen on chatroom
	Given user navigates to "mamikos /"
	When user clicks on Enter button
	And user login in as Tenant via phone number as "DC Automation"
	And user clicks search bar
	And user search for Kost with name "nonGPMars1" and selects matching result
	And user click chat in kos detail
	And user select question "Cara menghubungi pemilik?"
	And chat room appear with latest message "Mohon tunggu balasan dari pemilik kos ini."
	Then user did not see last seen owner on chatroom
	@TEST_DOM-3542 @Automated @DOM5 @MARS-DOM @ShowAptLastSeenChatroom @Web @discovery-platform
	Scenario: [Web][Mars] Show owner last seen on apartment chatroom
	Given user navigates to "mamikos /"
	When user clicks on Enter button
	And user login in as Tenant via phone number as "DC Automation"
	And user clicks search bar
	And user search for Kost with name "aptMars1" and selects matching result
	And user click Chat in apartment detail
	And user select question "Boleh tahu alamat lengkap apartemen ini?"
	And chat room appear with latest message "Alamat lengkapnya adalah"
	And user close chat pop up
	And user click Chat in apartment detail
	And user select question "Boleh tahu alamat lengkap apartemen ini?"
	And chat room appear with latest message "Alamat lengkapnya adalah"
	Then user see last seen owner on chatroom
	@TEST_DOM-3543 @Automated @DOM5 @MARS-DOM @ShowLastSeenDetailPage @Web @discovery-platform
	Scenario: [Web][Mars] Show owner last seen on detail page - GP Kos
	Given user navigates to "mamikos /"
	And user clicks search bar
	When user search for keyword "UGM"
	And user click on the Kos Andalan filter
	And user turn on the toggler button
	When user selects first kost on listing
	Then user see last seen owner on detail page
	@TEST_DOM-3544 @Automated @CheckChatHeaderP2Kost @DOM5 @MARS-DOM @Web @discovery-platform
	Scenario: [Web][Mars] Check header of chat on P2 Kost
	Given user navigates to "mamikos /"
	When user clicks on Enter button
	And user login in as Tenant via phone number as "DC Automation"
	And user clicks search bar
	And user search for Kost with name "nonGPMars1" and selects matching result
	And user click chat in kos detail
	And user select question "Cara menghubungi pemilik?"
	And chat room appear with latest message "Mohon tunggu balasan dari pemilik kos ini."
	Then user validate message header is "Balasan otomatis"
	@TEST_DOM-3545 @Automated @CheckChatHeaderP1Kost @DOM5 @MARS-DOM @Web @discovery-platform
	Scenario: [Web][Mars] Check header of chat on P1 Kost
	Given user navigates to "mamikos /"
	When user clicks on Enter button
	And user login in as Tenant via phone number as "DC Automation"
	And user clicks search bar
	And user search for Kost with name "chatKostName12" and selects matching result
	And user click chat in kos detail
	And user select question "Cara menghubungi pemilik?"
	Then user validate message header is not "Balasan otomatis"
	@TEST_DOM-3546 @Automated @CheckExperimentKostAutoreply @DOM5 @MARS-DOM @Web @discovery-platform
	Scenario: [Web][Mars] Check Experiment Kost Autoreply
	Given user navigates to "mamikos /"
	When user clicks on Enter button
	And user login in as Tenant via phone number as "DC Automation"
	And user clicks search bar
	And user search for Kost with name "nonGPMars1" and selects matching result
	And user click chat in kos detail
	And user select question "Cara menghubungi pemilik?"
	And chat room appear with latest message "Mohon tunggu balasan dari pemilik kos ini."
	@TEST_DOM-3547 @Automated @DOM5 @FTUEContinueChat @MARS-DOM @Web @discovery-platform
	Scenario: [Web][Mars] Check functionality FTUE Pop Up Before Send Chat
	Given user navigates to "mamikos /"
	And user clicks on Enter button
	And user fills out owner login as "mars owner 2" and click on Enter button
	And owner non GP "mamikos"
	And user click chat button in top bar
	And tenant click on first index chat on chat list
	When user enter text "Reply by Automation" in chat page
	Then user see FTUE Pop Up before send chat
	And user click back on FTUE Pop Up before send chat
	And user click send chat button
	Then user see FTUE Pop Up before send chat
