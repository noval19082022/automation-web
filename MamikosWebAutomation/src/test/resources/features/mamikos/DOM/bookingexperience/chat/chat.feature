Feature: Chat and Chat Optimization

	@TEST_DOM-1737 @DOM4 @Web @automated @autoreply-chat @chat @chat-owner-emphty @chat-tenant @discovery-platform @kost-details
	Scenario: [Dweb][Kost Detail][Chat] Chat details when chat room not exists
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user fills out owner login as "mamipay-donthavekost" and click on Enter button
		When user click chat button in top bar
		Then user see chat empty image
#		And user see text "Kamu belum punya percakapan" in empty chat page
		And user see text "Tidak ada percakapan saat ini." in empty chat description
		And user see indicator "Chat kosong" in bottom of empty chat page
	@TEST_DOM-1738 @DOM4 @Web @automated @chat @chat-tenant @discovery-platform @kost-details
	Scenario: [Dweb][Kost Detail][Chat] Show login pop up when click chat button without login
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user search for Kost with name "chatKostName1" and selects matching result
		And user click chat in kos detail
		Then user see login pop up
	@TEST_DOM-1736 @DOM4 @Web @automated @chat @chat-tenant @discovery-platform @kost-details @list-question-chat
	Scenario: [Dweb][Kost Detail][Chat] Show all selectable questions before chat
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		And user clicks search bar
		And user search for Kost with name "chatKostName1" and selects matching result
		When user click chat in kos detail
		Then user see phone number field and selectable question options :
		  | Saya butuh cepat nih. Bisa booking sekarang? |
		  | Ada diskon untuk kos ini?                    |
		  | Masih ada kamar?                             |
		  | Alamat kos di mana?                          |
		  | Cara menghubungi pemilik?                    |
		  | Boleh tanya-tanya dulu?                      |
		  | Bisa pasutri?                                |
		  | Boleh bawa hewan?                            |
	@TEST_DOM-1735 @DOM4 @Web @automated @autoreply-chat @chat @chat-instant-booking @chat-tenant @discovery-platform @kost-details
	Scenario: [Dweb][Kost Detail][Chat] Redirect to booking form page when contact kos with instant booking
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		And user clicks search bar
		And user search for Kost with name "chatKostName8" and selects matching result
		When user click chat in kos detail
		And send button become "Ajukan Sewa"
		And user select question "Saya butuh cepat nih. Bisa booking sekarang?"
		Then it will redirect to Booking page
	@TEST_DOM-1734 @DOM4 @Web @automated @autoreply-chat @chat @chat-tenant @chat-to-owner @discovery-platform @kost-details
	Scenario: [Dweb][Kost Detail][Chat] Tenant can send message to Owner
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		And user clicks search bar
		And user search for Kost with name "chatKostName13" and selects matching result
		When user click chat in kos detail
		And user select question "Cara menghubungi pemilik?"
		And chat room appear with latest message "Chatroom ini telah terhubung dengan pemilik kost, Anda dapat mengajukan pertanyaan dan berkomunikasi dengan pemilik iklan secara real time atau hubungi"
		And user enter text "Boleh minta nomor yang bisa dihubungi?" in chat page
		And user close chat pop up
		And user navigates to "mamikos /"
		And user logs out as a Tenant user
		And user clicks on Enter button
		And user fills out owner login as "master" and click on Enter button
		And user click on owner popup
		And user click chat button in top bar
		And search chat in chatlist "Raney Arora"
		And owner user click chat from "tenant" in chatlist
		Then chat room appear with latest message "Boleh minta nomor yang bisa dihubungi?"
		When user enter text "My phone is 00000000001" in chat page
		And user close chat pop up
		And user navigates to "mamikos /"
		And user logs out as a Owner user
		And user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		And tenant user navigates to Chat page
		And tenant user click chat from "owner" in chatlist
		Then chat room appear with latest message "My phone is 00000000001"
	@TEST_DOM-1733 @DOM4 @Web @automated @autoreply-chat @chat @chat-tenant @discovery-platform @kost-details
	Scenario Outline: [Dweb][Kost Detail][Chat]Check autoreply text after select question <name>
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		And user clicks search bar
		And user search for Kost with name "<property>" and selects matching result
		When user click chat in kos detail
		And user select question "<question>"
		Then chat room appear with latest message "<autoreply text>"
		Examples:
		  | name                     | property      | question                  | autoreply text                                                                                                                                          |
		  | Ada diskon               | chatKostName1 | Ada diskon untuk kos ini? | Diskon yang berlaku saat ini:                                                                                                                           |
		  | Masih ada kamar          | chatKostName1 | Masih ada kamar?          | Ada. Di kos ini masih ada 6 kamar kosong, sesuai update dari pemilik pada                                                                               |
		  | Tanya-tanya dulu         | chatKostName1 | Boleh tanya-tanya dulu?   | Boleh dong. Silakan tanya apapun. Chat ini dibaca langsung oleh pemilik kos.                                                                            |
		  | Cara menghubungi pemilik | chatKostName1 | Cara menghubungi pemilik? | Chatroom ini telah terhubung dengan pemilik kost, Anda dapat mengajukan pertanyaan dan berkomunikasi dengan pemilik iklan secara real time atau hubungi |
		  | Alamat kos di mana       | chatKostName1 | Alamat kos di mana?       | beralamat di                                                                                               											   |
		  | Bisa pasutri             | chatKostName3 | Bisa pasutri?             | Pasutri bisa menyewa kos ini.                                                                                                                           |
		  | Tidak bisa pasutri       | chatKostName1 | Bisa pasutri?             | Pasutri tidak bisa menyewa kos ini.                                                                                                                     |
		  | Boleh bawa hewan         | chatKostName3 | Boleh bawa hewan?         | Kamu boleh membawa hewan ke kos ini.                                                                                                                    |
		  | Tidak boleh bawa hewan   | chatKostName1 | Boleh bawa hewan?         | Kamu tidak boleh membawa hewan ke kos ini.                                                                                                              |
	@TEST_DOM-1742 @DOM4 @Web @automated @chat @chat-optimization @chat-tenant @discovery-platform @kost-details @roomCardFull
	Scenario: [Dweb][Kost Detail][Chat]Check functionality of booking button disable
		Given user navigates to "mamikos /"
		And user maximize the screen size
		And user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		When user clicks search bar
		And user search for Kost with name "chatKostName9" and selects matching result
		And user click chat in kos detail
		And user select question "Boleh tanya-tanya dulu?"
		Then chat room appear with latest message "Boleh dong. Silakan tanya apapun. Chat ini dibaca langsung oleh pemilik kos."
		And user sees the room card is present in chat room
		And user sees the Booking button disable
	@TEST_DOM-1741 @DOM4 @Web @automated @chat @chat-optimization @chat-tenant @discovery-platform @kost-details @roomCardActive
	Scenario: [Dweb][Kost Detail][Chat] Check functionality of booking button active
		Given user navigates to "mamikos /"
		And user maximize the screen size
		And user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		When user clicks search bar
		And user search for Kost with name "chatKostName8" and selects matching result
		And user click chat in kos detail
		And user select question "Boleh tanya-tanya dulu?"
		And chat room appear with latest message "Boleh dong. Silakan tanya apapun. Chat ini dibaca langsung oleh pemilik kos."
		And user sees the room card is present in chat room
		Then user clicks the Booking button and redirect to booking form
		And user select checkin date for today
		And user selects T&C checkbox and clicks on Book button
		And user navigates to "mamikos /user/booking/"
		And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
	@TEST_DOM-1740 @DOM4 @Web @automated @chat @chat-optimization @chat-tenant @discovery-platform @kost-details @roomCardbutton
	Scenario Outline: [Dweb][Kost Detail][Chat] Check functionality Lihat detail button on BBK and Non BBK
		Given user navigates to "mamikos /"
		And user maximize the screen size
		And user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		When user clicks search bar
		And user search for Kost with name "<property>" and selects matching result
		And user click chat in kos detail
		And user select question "Boleh tanya-tanya dulu?"
		Then chat room appear with latest message "Boleh dong. Silakan tanya apapun. Chat ini dibaca langsung oleh pemilik kos."
		And user sees the room card is present in chat room
		And user clicks the Lihat Iklan button and redirect to detail property
		Examples:
		  | property       |
		  | chatKostName8  |
		  | chatKostName10 |
	@TEST_DOM-1739 @DOM4 @Web @automated @chat @chat-optimization @chat-tenant @discovery-platform @kost-details @roomCardApartemen
	Scenario: [Dweb][Apartemen Detail][Chat] Check roomcard on apartemen should not display
		Given user navigates to "mamikos /"
		And user maximize the screen size
		And user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		When user choose "Apartemen" on dropdown list ads search
		And user search apartment using keyword "rane"
		And user click on the selected apartment to go to the apartment details
		And user click on Hubungi Pengelola button
		And user select question "Boleh tahu alamat lengkap apartemen ini?"
		Then chat room appear with latest message "Hai, terima kasih sudah berminat pada apartemen ini. Alamat lengkapnya adalah"
		And user sees the room card is NOT present in apartemen chat room