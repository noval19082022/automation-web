Feature: Homepage

	@TEST_DOM-1754 @Automated @DOM4 @Web @discovery-platform @footer @footer-mamikos @homepage
	Scenario: [Dweb][Homepage]Check redirection kebijakan privasi on homepage
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to bottom of mamikos
		And user click link kebijakan privasi section
		Then page redirect to page "https://help.mamikos.com/post/kebijakan-privasi-mamikos"
		And user switch to main window
		Then user click link syarat dan ketentuan then page redirect to page "https://help.mamikos.com/post/syarat-dan-ketentuan-umum"
	@TEST_DOM-1768 @Automated @DOM4 @Web @discovery-platform @header @header-after-login @homepage
	Scenario: [Dweb][Homepage]Check Booking header items after login
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		Then user check the header items display
		And user click cari iklan on header
		Then cari iklan dropdown display option
		And user click profile on header
		Then tenant profile dropdown display option
		  | Profil            |
		  | Riwayat Transaksi |
		  | Keluar            |
	@TEST_DOM-1766 @Automated @DOM4 @Web @discovery-platform @homepage @search
	Scenario: [Dweb][Homepage] Check element searchbar section
		Given user navigates to "mamikos /"
		And user see title wording "Mau cari kos?"
		And user see description wording "Dapatkan infonya dan langsung sewa di Mamikos."
		Then user see label wording "Masukan nama lokasi/area/alamat"
		And user see Cari button
	@TEST_DOM-1767 @Automated @DOM4 @Web @discovery-platform @footer-mamikos @homepage
	Scenario: [Dweb][Homepage]Check redirection footer tentang kami, career, promotion, and help center on homepage
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to bottom of mamikos
		And user click link tentang kami on footer section
		Then page redirect to page "/tentang-kami"
		And user switch to main window
		And user click link job mamikos on footer section
		Then page redirect to page "/career"
		And user switch to main window
		And user click link promosikan iklan anda gratis on footer section
		Then page redirect to page "/mamiads"
		And user switch to main window
		Then user click link pusat bantuan section then page redirect to page "https://help.mamikos.com/"
	@TEST_DOM-1765 @Automated @DOM4 @Web @discovery-platform @footer @footer-contactUs @homepage
	Scenario: [Dweb][Homepage]Check redirection Contact Us on homepage
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to bottom of mamikos
	#    And user click instagram
	#    And page redirect to page "https://www.instagram.com/mamikosapp/"
	#    Then user click facebook
	#    And page redirect to page "https://www.facebook.com/mamikosapp/"
		And user click twitter
		And page redirect to page "https://twitter.com/mamikosapp"
		And user sees e-mail is "cs@mamikos.com"
		And user sees whatsapp is "0813-2511-1171"
		And user sees copyright is "© 2022 Mamikos.com, All rights reserved"
	@TEST_DOM-1763 @Automated @DOM4 @Web @discovery-platform @homepage @promotion-kost
	Scenario: [Dweb][Homepage]check content and functionality of promotion section
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to promotion section
		Then user see element filter city promotion section
		And user see kos thumbnail promotion section
		And user see see all button promotion section
		And user clicks on next and previous button promotion
		And user click on Location button from promotion section
		Then user see location list same with "promoLoc"
		And user clicks on Lihat Semua button on promotion section
		Then page will redirect to "/promo-kost?city=Semua%20Kota"
		Then user click promo kos thumbnail and redirect to detail promo kos
	@TEST_DOM-1764 @Automated @DOM4 @Web @discovery-platform @homepage @recommendation
	Scenario: [Dweb][Homepage]Check content and functionality of promo kos recommendation section
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to recommendation section
		Then user see element filter city
		And user see kos thumbnail
		And user see see all button
		And user clicks on next and previous button recommendation
		And user click on Location button from home page
		Then user see location list same with "recommenLoc"
		Then user clicks on Lihat Semua button and redirect to Landing kost, according to the city that selected
	@TEST_DOM-1762 @Automated @DOM4 @Web @discovery-platform @homepage @popular-area
	Scenario: [Dweb][Homepage]Popular area have correct city
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to popular area section
		Then in popular area section, there's this city kos :
		  | Kos Yogyakarta |
		  | Kos Jakarta    |
		  | Kos Bandung    |
		  | Kos Surabaya   |
		  | Kos Malang     |
		  | Kos Semarang   |
		  | Kos Medan      |
		  | Lihat semua →  |
		And user clicks on city and redirect to Landing kost, according to the city that selected
		And user click back button in page
		And user clicks on Lihat Semua button on popular area section
		Then page redirect to page "/area"
	@TEST_DOM-1761 @Automated @DOM4 @Web @discovery-platform @homepage @popular-campus
	Scenario: [Dweb][Homepage]Check content and functionality Popular campus section
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to kost around campus section
		Then in kost near campus section, there's this campus :
		  | UGM   |
		  | UNDIP |
		  | UI    |
		  | UNPAD |
		  | STAN  |
		  | UB    |
		  | UNAIR |
		And in kost near campus section, there's this city campus :
		  | Jogja     |
		  | Semarang  |
		  | Depok     |
		  | Jatinangor |
		  | Jakarta   |
		  | Malang    |
		  | Surabaya  |
		And user clicks on Lihat Semua button on popular campus area section
		Then page redirect to page "/kampus"
	@TEST_DOM-1760 @Automated @DOM4 @Web @discovery-platform @header @header-redirection @header-redirection-after-login @homepage
	Scenario: [Dweb][Homepage] Check header redirection on homepage after login
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		And user clicks menu navbar "Favorit"
		Then user sees the url similar with "/history/favourite"
		And tenant user navigates to Chat page
		And chat room displayed
		Then user close chat pop up without back button
		And user click notifikasi on header
		And display notification list
		And user click mamikos.com logo
		And user click profile on header
		Then tenant profile dropdown display option
		  | Profil            |
		  | Riwayat Transaksi |
		  | Keluar            |
		And user choose "Profil" on dropdown list profile
		And user click mamikos.com logo
		And user choose "Riwayat Transaksi" on dropdown list profile
		Then user logs out as a Tenant user
	@TEST_DOM-1759 @Automated @DOM4 @Web @discovery-platform @header @header-before-login @homepage
	Scenario Outline: [Dweb][Homepage]Check header redirection menu on homepage before login
		Given user navigates to "mamikos /"
		When user clicks menu "<menu>"
		Then user sees the url similar with "<url>"
		Examples:
		  | menu     | url                                                            |
		  | Booking  | /booking                                                       |
		  | Download | https://play.google.com/store/apps/details?id=com.git.mami.kos |
	@TEST_DOM-1750 @Automated @DOM4 @Web @discovery-platform @homepage @register-section
	Scenario: [Dweb][Homepage]Click Daftar Kos Gratis button
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to kost register section
		Then user click on Daftar Kos Gratis and redirect to "/mamiads"
	@TEST_DOM-1757 @Automated @DOM4 @Web @discovery-platform @homepage @search @search-recommen
	Scenario: [Dweb][Homepage] Search Bar have correct search result
		Given user navigates to "mamikos /"
		When user clicks search bar
		And user search for keyword "Medan"
		Then listing that appear have location in "Medan"
	@TEST_DOM-1758 @Automated @DOM4 @Web @discovery-platform @footer @homepage
	Scenario: [Dweb][Homepage]Check the content on footer section homepage
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to bottom of mamikos
		And check the footer menu display on homepage
	@TEST_DOM-1756 @Automated @DOM4 @Web @discovery-platform @header @header-redirection-dropdown @homepage
	Scenario Outline: [Dweb][Homepage]Check header redirection Ads dropdown
		Given user navigates to "mamikos /"
		When user clicks menu search ads dropdown
		When user clicks menu navbar "<menu>"
		Then user sees the url similar with "<url>"
		Examples:
		  | menu           | url        |
		  | Kos            | /cari      |
		  | Apartemen      | /apartemen |
	@TEST_DOM-1755 @Automated @DOM4 @Web @discovery-platform @homepage @promo-banner
	Scenario: [Dweb][Homepage]Check previous button banner behaviour
		Given user navigates to "mamikos /"
		When user see the banner promo
		Then user clicks on previous button and it will display previous banner
		And user see the last banner promo
		Then user clicks on next button and it will display next banner
		And user clicks on Lihat Semua button
		Then page redirect to "https://promo.mamikos.com/"
	@TEST_DOM-1753 @Automated @DOM4 @Web @discovery-platform @homepage @store-mamikos
	Scenario: [Dweb][Homepage]Click button google store and app store on homepage
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to bottom of mamikos
		And user click link google play store on footer section and redirect to page "https://play.google.com/store/apps/details?id=com.git.mami.kos"
		And user switch to homepage
		And user reach homepage, and scroll to bottom of mamikos
		Then user click link app store on footer section and redirect to page "https://apps.apple.com/id/app/mami-kos/id1055272843"
	@TEST_DOM-1752 @Automated @DOM4 @Web @discovery-platform @header @header-redirection @homepage
	Scenario Outline: [Dweb][Homepage]Check header menu navbar redirection on homepage before login
		Given user navigates to "mamikos /"
		When user clicks menu navbar "<menu>"
		Then user sees the url similar with "<url>"
		Examples:
		  | menu                 | url                       |
		  | Pusat Bantuan        | https://help.mamikos.com/ |
		  | Syarat dan Ketentuan | /syarat-dan-ketentuan     |
	@TEST_DOM-1751 @Automated @DOM4 @Web @discovery-platform @homepage @testimonial
	Scenario: [Dweb][Homepage]Check element testimonial section
		Given user navigates to "mamikos /"
		When user reach homepage, and scroll to testimonial section
		And user see the testimonial elements
		Then user clicks on next button
		And user clicks on previous button