Feature: Filter and Sorting

	@TEST_DOM-1832 @Automated @DOM4 @Web @discovery-platform @filter @filter-description @listing-kos
	Scenario Outline: [Dweb][Listing Kos][Filter]Check description of filter
		Given user navigates to "mamikos /"
		And user maximize the screen size
		When user clicks search bar
		And user search for keyword "UGM"
		Then user clicks the "<filter>" button and the description will appears "<desc>"
		Examples:
		  | name         | filter       | desc                                                                                                       |
		  | Kos Andalan  | Kos Andalan  | Kos favorit dengan harga hemat, dengan berbagai pilihan tipe kamar. Tersebar di ratusan kota di Indonesia. |
		  | Kos Pilihan  | Kos Pilihan  | Rekomendasi iklan kos dari tim Mamikos.                                                                    |
		  | Promo Ngebut | Promo Ngebut | Dapat diskon pembayaran pertama harga sewa. Diskon hanya berlaku selama program berlangsung.               |
	@TEST_DOM-1836 @Automated @DOM4 @Web @discovery-platform @filter @filter-gender @listing-kos
	Scenario Outline: [Dweb][Listing Kos][Filter]Check filter gender and verify the result
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "UGM"
		And user sets filter gender "<gender>"
		Then user validates the result kos gender is "<gender>"
		Examples:
		  | gender |
		  | Putra  |
		  | Putri  |
		  | Campur |
	@TEST_DOM-1839 @Automated @DOM4 @Web @discovery-platform @filter @filter-facility-share @listing-kos
	Scenario Outline: [Dweb][Listing Kos][Filter] Check filter facilityShare and verify the result
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "UGM"
		And user sets filter top facility "<facilityShare>"
		Then user validates the result kos facility is "<facilityShare>"
		When user selects first kost on listing
		Then user validates "<facilityShare>" is appear in kos detail
		Examples:
		  | facilityShare |
		  | WiFi          |
	@TEST_DOM-1834 @Automated @DOM4 @Web @discovery-platform @filter @filter-kos-andalan @listing-kos
	Scenario: [Dweb][Listing Kos][Filter] Check filter kos andalan 
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "UGM"
		And user click on the Kos Andalan filter
		And user turn on the toggler button
		Then user validated the result kos have label "Kos Andalan"
	@TEST_DOM-1833 @Automated @DOM4 @Web @discovery-platform @filter @filter-andalan-elite @listing-kos
	Scenario: [Dweb][Listing Kos][Filter] Check activate filter kos andalan && kos elite
		Given user navigates to "mamikos /"
		And user clicks search bar
		And user clicks Area
		And user clicks button "Yogyakarta" below
		And user click on the Kos Andalan filter
		And user turn on the toggler button
		And user click on the Kos Pilihan filter
		Then user see text empty result "Kos Tidak Ditemukan"
		Then user see text empty result filter "Silahkan ubah filter untuk meningkatkan hasil pencarian kos."
	@TEST_DOM-1841 @Automated @DOM4 @Web @discovery-platform @filter @filter-kos-rule @listing-kos
	Scenario Outline: [Dweb][Listing Kos][Filter] Check filter kos rule and verify the result
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "UGM"
		And user sets filter top kos rule "<kos rule>"
		When user selects first kost on listing
		Then user validates "<kos rule>" is appear at kos detail rule
		Examples:
		  | kos rule     |
		  | Akses 24 Jam |
	@TEST_DOM-1843 @Automated @DOM4 @Web @discovery-platform @filter @filter-harga-termahal @listing-kos
	Scenario: [Dweb][Listing Kos][Sorting] check sorting function "Harga Termahal"
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "UGM"
		And user selects sorting "Harga termahal" in kost listing
		Then user validates the price of first listing is more expensive than the last listing in listing property page
	@TEST_DOM-1837 @Automated @DOM4 @Web @discovery-platform @filter @filter-harga-termurah @listing-kos
	Scenario: [Dweb][Listing Kos][Sorting] check sorting function "Harga Termurah"
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "UGM"
		And user selects sorting "Harga termurah" in kost listing
		Then user validates the price of first listing is cheaper than the last listing in listing property page
	@TEST_DOM-1840 @Automated @DOM4 @Web @discovery-platform @filter @filter-facility-room @listing-kos
	Scenario Outline: [Dweb][Listing Kos][Filter] Check filter facilityRoom and verify the result
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "UGM"
		And user sets filter top facility "<facilityRoom>"
		Then user validates the result kos facility is "<facilityRoom>"
		When user selects first kost on listing
		Then user validates "<facilityRoom>" is appear in kos detail at section facility room
		Examples:
		  | facilityRoom |
		  | Kasur        |
	@TEST_DOM-1838 @Automated @DOM4 @Web @discovery-platform @filter @filter-bathroom @listing-kos
	Scenario Outline: [Dweb][Listing Kos][Filter] Check filter facilityBathRoom and verify the result
		Given user navigates to "mamikos /"
		And user clicks search bar
		When user search for keyword "UGM"
		And user sets filter top facility "<facilityBathRoom>"
		Then user validates the result kos facility is "<facilityBathRoom>"
		When user selects first kost on listing
		Then user validates "<facilityBathRoom>" is appear in kos detail at section facility bathroom
		Examples:
		  | facilityBathRoom |
		  | K. Mandi Dalam   |