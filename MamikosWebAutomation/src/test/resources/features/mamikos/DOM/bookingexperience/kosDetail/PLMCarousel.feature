Feature: Check PLM Carousel

	@TEST_DOM-1727 @DOM4, @automated, @discovery-platform, @plm-carousel @web,
	Scenario: [Dweb][Kost Detail][PLM Caorusel Check detail page from other type
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "PLM A" and select matching result to go to kos details page
		And I should reached kos detail page
		And user should reach plm carousel section
		And user click on swipe bar
		And user click button "Lihat tipe kamar lain"
		And user click on kos card on other type
		Then I should reached kos detail page
	@TEST_DOM-1724 @DOM4, @automated, @discovery-platform, @plm-carousel @web,
	Scenario: [Dweb][Kost Detail][PLM Caorusel] Check booking without condition login form page lihat tipe kamar lain
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "PLM A" and select matching result to go to kos details page
		And I should reached kos detail page
		And user should reach plm carousel section
		And user click on swipe bar
		And user click button "Lihat tipe kamar lain"
		And user click button "Ajukan Sewa" at first list
		Then user see login pop up
	@TEST_DOM-1726 @DOM4, @automated, @discovery-platform, @plm-carousel @web,
	Scenario: [Dweb][Kost Detail][PLM Caorusel]  Check the appearence of kos card from other type
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "PLM A" and select matching result to go to kos details page
		And I should reached kos detail page
		And user should reach plm carousel section
		And user click on swipe bar
		And user click button "Lihat tipe kamar lain"
		Then user see the appearence of the kos card on other type page
	@TEST_DOM-1725 @DOM4, @automated, @discovery-platform, @plm-carousel @web,
	Scenario: [Dweb][Kost Detail][PLM Caorusel] Check the appearence of kos card on PLM
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "PLM A" and select matching result to go to kos details page
		And I should reached kos detail page
		And user should reach plm carousel section
		Then user see the appearence of the kos card carousel
	@TEST_DOM-1721 @DOM4, @automated, @discovery-platform, @plm-carousel @web,
	Scenario: [Dweb][Kost Detail][PLM Caorusel] Check booking with condition login form page lihat tipe kamar lain
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		And user clicks search bar
		And I search property with name "rumahwarna biru" and select matching result to go to kos details page
		And I should reached kos detail page
		And user should reach plm carousel section
		And user click on swipe bar
		And user click button "Lihat tipe kamar lain"
		And user click button "Ajukan Sewa" at first list
		Then user redirect to page "Pengajuan Sewa"
		And user select checkin date for today
		And user selects T&C checkbox and clicks on Book button
		And user navigates to "mamikos /user/booking/"
		And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
	@TEST_DOM-1720 @DOM4, @automated, @discovery-platform, @plm-carousel @web,
	Scenario: [Dweb][Kost Detail][PLM Caorusel] Check booking without login
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "PLM A" and select matching result to go to kos details page
		And I should reached kos detail page
		And user should reach plm carousel section
		And user click on button "Ajukan Sewa"
		Then user see login pop up
	@TEST_DOM-1723 @DOM4, @automated, @discovery-platform, @plm-carousel @web,
	Scenario: [Dweb][Kost Detail][PLM Caorusel] Check detail page from carousel
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "PLM A" and select matching result to go to kos details page
		And I should reached kos detail page
		And user should reach plm carousel section
		And user click on button see detail on carousel
		Then I should reached kos detail page
	@TEST_DOM-1722 @DOM4, @automated, @discovery-platform, @plm-carousel @web,
	Scenario: [Dweb][Kost Detail][PLM Caorusel] Check booking with condition login
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		When user login in as Tenant via phone number as "DC Automation"
		And user clicks search bar
		And I search property with name "PLM A" and select matching result to go to kos details page
		And I should reached kos detail page
		And user should reach plm carousel section
		And user click on button "Ajukan Sewa" after login
		Then user redirect to page "Pengajuan Sewa"