Feature: Search Apartemen

	@TEST_DOM-1872 @Automated @DOM4 @Web @discovery-platform @search @search-apart-notfound @search-apartment
	Scenario: [Dweb][Search][Apartemen] Search Apartemen not found
		Given user navigates to "mamikos /apartemen"
		And user input "Aceh" at form input keyword
		Then user see "Apartemen tidak ditemukan."
	@TEST_DOM-1870 @Automated @DOM4 @Web @discovery-platform @search @search-apart-bykeyword @search-apartment
	Scenario: [Dweb][Search][Apartemen] Search Apartemen by keyword
		Given user navigates to "mamikos /apartemen"
		And user input "Jakarta" at form input keyword
		Then user see badge apartemen for result
