Feature: Favorite and Share kost

	@TEST_DOM-1732 @DOM4 @automated @discovery-platform @favorite @web
	Scenario: [Dweb][Favorite] Tenant - Check Redirection without login
		Given user navigates to "mamikos /history/favourite"
		Then user see login pop up in favorite page
	@TEST_DOM-1729 @DOM4 @automated @discovery-platform @favorite @web
	Scenario: [Dweb][Kost Detail][FavoriteKost] Tenant - Login pop-up options appear
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "favoritekost" and select matching result to go to kos details page
		Then I should reached kos detail page
		And I see grey favourite button
		And I click on favourite button with not login condition
		Then user see login pop up
	@TEST_DOM-1728 @DOM4 @automated @discovery-platform @favorite @web
	Scenario: [Dweb][Kost Detail][FavoriteKost] Tenant Favourite a kos
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		When user clicks search bar
		And I search property with name "favoritekost" and select matching result to go to kos details page
		Then I should reached kos detail page
		And I click on favourite button
		And I click Favourite tab
		And I wait for property list to be loaded
		Then I can see "favoritekost" in list number 1
	@TEST_DOM-1731 @DOM4 @automated @discovery-platform @favorite @web
	Scenario: [Dweb][Kost Detail][FavoriteKost] Tenant Unfavourite a kos
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "DC Automation"
		And user clicks search bar
		And I search property with name "favoritekost" and select matching result to go to kos details page
		Then I should reached kos detail page
		And I click on favourite button
		And I can see red favourite button
		Then Tenant can see love button change to unfavourite condition
		And I click Favourite tab
		And I wait for property list to be loaded
		Then I should not see "favoritekost" in the property list number 1
	@TEST_DOM-1730 @DOM4 @automated @discovery-platform @favorite @web
	Scenario: [Dweb][Kost Detail][Share Kost] Tenant - Login pop-up options appear
		Given user navigates to "mamikos /"
		When user clicks search bar
		And I search property with name "favoritekost" and select matching result to go to kos details page
		Then I should reached kos detail page
		And I click on share button
		And I click share on facebook
		Then I see the facebook page