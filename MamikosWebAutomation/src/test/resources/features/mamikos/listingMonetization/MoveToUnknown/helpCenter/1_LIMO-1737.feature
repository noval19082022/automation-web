@regression @LIMO2 @pusatbantuanOwner
Feature: [Owner][Pusat Bantuan Owner] Owner accsess page pusat bantuan

  @TEST_LIMO-1737 @Automated @Web @listing-monetization
  Scenario: [Owner][Pusat Bantuan Owner] Owner accsess page pusat bantuan
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user click on owner popup
    When user click Pusat Bantuan
    Then user redirected to "owner HCenter"
