@regression @listingGP @LG3

Feature: Edit Kos BBK Rejected

  Background:
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "mamipay-BBKnotActive" and click on Enter button
    And user navigates to "owner /ownerpage/kos"

  @EditKosBBKRejected
  Scenario Outline: Edit kost when kost is BBK Rejected
    Given user clicks button find your kos here
    And input name kos "KosAuto NoBBK aaa"
    And user clicks kos name from result
    And user click button Edit Private Data
    When user clicks on button Next BBK
    Then user see activate mamipay form with Full Name "<FullName1>"
    And user see activate mamipay form with Bank Account Number "<BankNo1>"
    And user see active mamipay form with Bank Owner Name "<BankOwner1>"
    And user see active mamipay form with Bank Name "<BankName1>"
    And user input field name with "<FullName2>" at form activate mamipay
    And user fill out activate mamipay form with Bank Account Number "<BankNo2>"
    And user fill out active mamipay form with  Bank Owner Name "<BankOwner2>"
    And user select bank account with "<BankName2>"
    And user clicks on Terms And Conditions checkbox in Mamipay form
    And user click submit data button to activate mamipay
    And user click back in request activation sent page
    Then user see kos with name "KosAuto NoBBK aaa", status "Aktif" and type "Kos Putra"
      # Reject BBK Kos
    And user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "KosAuto NoBBK" in admin kos owner page
    And user click BBK Data button
    And user click reject BBK button in booking owner request
    And user click on first radio button in reject reason
    And user click reject BBK button in booking reject reason
    Examples:
      | FullName1                              | BankNo1     | BankOwner1   | BankName1                   | FullName2                              | BankNo2     | BankOwner2   | BankName2                   |
      | tiara lapan abcdefghijklmnopqrstuvwxyz | 02823828282 | tiara lapan  | CTBC (Chinatrust) Indonesia | eko lapan abcd                         | 8989898989  | Kiai Santang | BCA                         |
      | eko lapan abcd                         | 8989898989  | Kiai Santang | BCA                         | tiara lapan abcdefghijklmnopqrstuvwxyz | 02823828282 | tiara lapan  | CTBC (Chinatrust) Indonesia |