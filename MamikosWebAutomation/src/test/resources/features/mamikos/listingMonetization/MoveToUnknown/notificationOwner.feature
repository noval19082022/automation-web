@regression @bellnotification @LIMO2

Feature: [Owner Dashboard][Notification] See all notification from owner
	#Scenario: Bell Notification - See all notification clicked
  @TEST_LIMO-1734 @Automated @Web @listing-monetization @notifikasi-owner
  Scenario: [Owner Dashboard][Notification] See all notification from owner
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "Owner No Listing" and click on Enter button
    And user click notification owner button
    And user click on see more notification
    Then user redirected to "notification page"
