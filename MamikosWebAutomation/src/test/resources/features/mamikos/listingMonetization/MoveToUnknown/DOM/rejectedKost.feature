@regression @owner @kos @listingGP @LG2
Feature: Owner - Rejected Kos


  Background:
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    And user navigates to "owner /ownerpage/kos"

  @rejectedKos
  Scenario: Owner - Rejected Kos
    Given user clicks button find your kos here
    When input name kos "Kost Rejected Del 70etz 1xhmj"
    And user clicks kos name from result
    And user click delete kos
    And user click cancel to delete kos
    Then user see kos with name "Kost Rejected Del 70etz 1xhmj", status "Data Kos Ditolak" and type "Kos Putri"