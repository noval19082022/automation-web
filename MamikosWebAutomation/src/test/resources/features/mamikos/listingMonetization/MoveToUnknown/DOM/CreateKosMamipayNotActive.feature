@regression @listingGP @LG3

Feature: Create new kos with owner that hasn't activate mamipay

  Background:
    # Check if account has mamipay, if true then delete it
    And user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click Mamipay Owner List
    And user search owner phone "0812345670004" in mamipay owner list
    And user delete the mamipay data from first list
    # Owner login until add kos
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "mamipay-notregistered2" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And user clicks on Add Data button
    And user clicks on Add button
    And user selects Kost option and click on Add Data button

  @CreateKosExistTypeMamipayNotActv
  Scenario: Create new room type from "Tipe A" && edit data kos && mamipay not active
    Given user click add another type from kos "Kos berTipe"
    When user click "Tipe Apple" in add new room type pop up and click next
    And user input room type with random text in add kos data pop up
    And user clicks on next button in bottom of add kos page
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user clicks on edit done button in bottom of add kos page
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user fill out activate mamipay form with Bank Account Number "09182928329"
    And user fill out active mamipay form with  Bank Owner Name "Omaiwa wo shinderu"
    And user select bank account with "BCA"
    And user click submit data button to activate mamipay
    And user click skip in ask mamikos page
    And user click done in success page
    Then user see kos with name "Kos berTipe" and random text, status "Diperiksa Admin" and type "Kos Putri"
        # Delete newly created kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "Kos berTipe" in admin kos owner page
    And user delete the kos in admin kos owner
        # Delete mamipay data
    And user navigates to "backoffice"
    And user click Mamipay Owner List
    And user search owner phone "0812345670004" in mamipay owner list
    And user delete the mamipay data from first list

  @AddKosInvalidPrice
  Scenario: Add new kos and input price with less than the price range and more than the price range in some entity on screen harga
    Given user click add new kos button
    When user fills kost name field with "Kost Price " and random text
    And user clicks on kost type icon "male"
    And user fill kos description with "kos price invalid high low"
    And user select kos year built "2011"
    And user clicks on next button in bottom of add kos page
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user clicks on next button in bottom of add kos page
    And user uncheck facilities under "Fasilitas Umum":
      | Bak mandi |
    And user uncheck facilities under "Fasilitas Kamar":
      | Air panas |
    And user uncheck facilities under "Fasilitas Kamar Mandi":
      | Bathup |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 3"
    And user input "7" to field total room
    And user clicks on next button in bottom of add kos page
    And user click checkbox rent price other than monthly
    And user input monthly price with "49999" in add kos page
    And user input daily price with "9999" in add kos page
    And user input weekly price with "49999" in add kos page
    And user input three monthly price with "99999" in add kos page
    And user input six monthly price with "99999" in add kos page
    And user input yearly price with "99999" in add kos page
    Then user see warning message in "Harga Per Bulan" is "Harga per bulan min. Rp50.000 dan maks. Rp100.000.000."
    And user see warning message in "Harga Per Hari" is "Harga per hari min. Rp10.000 dan maks. Rp10.000.000."
    And user see warning message in "Harga Per Minggu" is "Harga per minggu min. Rp50.000 dan maks. Rp50.000.000."
    And user see warning message in "Harga Per 3 Bulan" is "Harga per 3 bulan min. Rp100.000 dan maks. Rp100.000.000."
    And user see warning message in "Harga Per 6 Bulan" is "Harga per 6 bulan min. Rp100.000 dan maks. Rp100.000.000."
    And user see warning message in "Harga Per Tahun" is "Harga per tahun min. Rp100.000 dan maks. Rp100.000.000."
    When user input monthly price with "100000001" in add kos page
    And user input daily price with "10000001" in add kos page
    And user input weekly price with "50000001" in add kos page
    And user input three monthly price with "100000001" in add kos page
    And user input six monthly price with "100000001" in add kos page
    And user input yearly price with "100000001" in add kos page
    Then user see warning message in "Harga Per Bulan" is "Harga per bulan min. Rp50.000 dan maks. Rp100.000.000."
    And user see warning message in "Harga Per Hari" is "Harga per hari min. Rp10.000 dan maks. Rp10.000.000."
    And user see warning message in "Harga Per Minggu" is "Harga per minggu min. Rp50.000 dan maks. Rp50.000.000."
    And user see warning message in "Harga Per 3 Bulan" is "Harga per 3 bulan min. Rp100.000 dan maks. Rp100.000.000."
    And user see warning message in "Harga Per 6 Bulan" is "Harga per 6 bulan min. Rp100.000 dan maks. Rp100.000.000."
    And user see warning message in "Harga Per Tahun" is "Harga per tahun min. Rp100.000 dan maks. Rp100.000.000."
        # Delete newly created kos draft
    And user navigates to "/ownerpage/kos"
    And user delete first kos on the list

  @CreateKosNewTypeMamipayNotActv
  Scenario: Create new room type from "Buat baru" && mamipay not active
    Given user click add another type from kos "Kos berTipe"
    When user click "Buat Baru" in add new room type pop up and click next
    And user input room type with random text
    And user clicks on kost type icon "male"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Kamar":
      | Air panas |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 3"
    And user input "9" to field total room
    And user input "5" to total available room
    And user clicks on next button in bottom of add kos page
    And user input monthly price with "800000" in add kos page
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user fill out activate mamipay form with Bank Account Number "09182928329"
    And user fill out active mamipay form with  Bank Owner Name "Omaiwa wo shinderu"
    And user select bank account with "BCA"
    And user click submit data button to activate mamipay
    And user click skip in ask mamikos page
    And user click done in success page
    Then user see kos with name "Kos berTipe" and random text, status "Diperiksa Admin" and type "Kos Putra"
        # Delete newly created kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "Kos berTipe" in admin kos owner page
    And user delete the kos in admin kos owner
        # Delete mamipay data
    And user navigates to "backoffice"
    And user click Mamipay Owner List
    And user search owner phone "0812345670004" in mamipay owner list
    And user delete the mamipay data from first list

  @CreateNewKosMamipayNotActv
  Scenario: Create new kost when user doesnt active mamipay
    Given user click add new kos button
    And user fills kost name field with "Kost AutoNew NoMamipay " and random text
    And user clicks checkbox room type
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user fill kos description with "Kos tanpa mamipay automation"
    And user set kos rules :
      | Laundry |
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    When user input administrator name with "Omeii"
    And user input administrator phone with "083333373777"
    And user input other notes "Tidak menerima binatang piaraan"
    And user clicks on next button in bottom of add kos page
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user input address note "Dekat rumah rumahan dari onnta"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user insert photo image to "Tambahan foto lain? boleh dong"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Umum":
      | Dapur  |
      | Kulkas |
    And user check facilities under "Fasilitas Kamar":
      | AC   |
      | Sofa |
    And user check facilities under "Fasilitas Kamar Mandi":
      | Bathup |
      | Gayung |
    And user check facilities under "Parkir":
      | Parkir Mobil |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 3"
    And user input "11" to field total room
    And user input "10" to total available room
    And user clicks on next button in bottom of add kos page
    And user clicks checkbox minimum rent duration
    And user select minimum rent duration "Min. 1 Hari"
    And user click checkbox rent price other than monthly
    And user input monthly price with "300000" in add kos page
    And user input daily price with "50000" in add kos page
    And user input weekly price with "200000" in add kos page
    And user input three monthly price with "800000" in add kos page
    And user input six monthly price with "1700000" in add kos page
    And user input yearly price with "3000000" in add kos page
    And user clicks checkbox additional price
    And user insert cost name "Cuci Anak"
    And user insert total cost "10000"
    And user input deposit with "100000"
    And user clicks checkbox down payment
    And user select down payment percentage "30%"
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user fill out activate mamipay form with Bank Account Number "09182928329"
    And user fill out active mamipay form with  Bank Owner Name "Omaiwa wo shinderu"
    And user select bank account with "BCA"
    And user click submit data button to activate mamipay
    And user click skip in ask mamikos page
    And user click done in success page
    Then user see kos with name "Kost AutoNew NoMamipay " and random text, status "Diperiksa Admin" and type "Kos Putri"
    # Delete newly created kos in admin
    And user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "Kost AutoNew NoMamipay" in admin kos owner page
    And user delete the kos in admin kos owner
        # Delete mamipay data
    And user navigates to "backoffice"
    And user click Mamipay Owner List
    And user search owner phone "0812345670004" in mamipay owner list
    And user delete the mamipay data from first list

  @AddKosNoMandatoryDataKos
  Scenario: Add new kos and without input mandatory field in some entity on screen data kos
    Given user click add new kos button
    When user fills kost name field with " "
    And user clicks checkbox room type
    And user input room type with " "
    Then user see error message "Anda belum mengisi nama kos." under kos name field
    And user see error message "Anda belum mengisi nama tipe kamar ini." under data kos room type field
    When user fills kost name field with "Test1234"
    And user input room type with "wagyu"
    And user clicks on kost type icon "female"
    And user fill kos description with " "
    Then user see error message "Anda belum mengisi deskripsi kos." under kos description field
    When user set kos rules :
      | Laundry |
    Then user see next button disable
    When user select kos year built "2020"
    And user clicks checkbox administrator kos
    And user input administrator name with " "
    And user input administrator phone with " "
    Then user see error message "Silakan isi nama pengelola." under kos manager name field
    And user see error message "Silakan isi nomor hp pengelola." under kos manager phone field

  @AddKosLessCharDataKos
  Scenario: Add new kos with less than minimal characters in some entity on screen data kos
    Given user click add new kos button
    When user fills kost name field with "Sana"
    And user clicks checkbox room type
    And user input room type with "c"
    Then user see error message "Tidak boleh kurang dari 5 karakter." under kos name field
    And user see error message "Tidak boleh kurang dari 2 karakter." under data kos room type field
    When user fills kost name field with "Test1234"
    And user input room type with "wagyu"
    And user clicks on kost type icon "female"
    And user fill kos description with " "
    Then user see error message "Anda belum mengisi deskripsi kos." under kos description field
    When user set kos rules :
      | Laundry |
    Then user see next button disable
    When user select kos year built "2020"
    And user clicks checkbox administrator kos
    And user input administrator name with "A"
    And user input administrator phone with "0891234"
    Then user see error message "Tidak boleh kurang dari 2 karakter." under kos manager name field
    And user see error message "Tidak boleh kurang dari 8 karakter." under kos manager phone field

  @AddKosSpecCharDataKos
  Scenario: Add new kos with special characters in some entity on screen data kos
    Given user click add new kos button
    When user fills kost name field with "Sana()"
    And user clicks checkbox room type
    And user input room type with "cccc()"
    Then user see error message "Tidak boleh menggunakan karakter spesial seperti / ( ) - . , ’ dan “" under kos name field
    And user see error message "Tidak boleh menggunakan karakter spesial seperti / ( ) - . , ’ dan “" under data kos room type field

  @AddKosMaxCharDataKos
  Scenario: Add new kos with more than maximal characters in some entity on screen data kos
    Given user click add new kos button
    When user fills kost name field with "Kost sana sani sini sana sani sini sana sani sini sana sani sini sana sani sini sana sani sini sana 1"
    And user clicks checkbox room type
    And user input room type with "Tipe abcdefghijklmnopqrstu abcdefghijklmnopqrstu abcdefghijklmnopqrstu abcdefghijklmnopqrstu 12345678"
    Then user see error message "Tidak boleh lebih dari 100 karakter." under kos name field
    And user see error message "Tidak boleh lebih dari 100 karakter." under data kos room type field
    When user fills kost name field with "Test1234"
    And user input room type with "wagyu"
    And user clicks on kost type icon "female"
    And user fill kos description with " "
    Then user see error message "Anda belum mengisi deskripsi kos." under kos description field
    When user set kos rules :
      | Laundry |
    Then user see next button disable
    When user select kos year built "2020"
    And user clicks checkbox administrator kos
    And user input administrator name with "Jajang sunandar slamet riyadhii"
    And user input administrator phone with "089123456789012345678"
    Then user see error message "Tidak boleh lebih dari 30 karakter." under kos manager name field
    And user see error message "Tidak boleh lebih dari 20 karakter." under kos manager phone field

  @CreateKosInvalidAddressNotes
  Scenario: Text box "Catatan Alamat" is inputed with invalid value
    Given user click add new kos button
    And user fills kost name field with "Kost AutoTest AddressNote " and random text
    And user clicks checkbox room type
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user fill kos description with "Kos tanpa mamipay automation"
    And user set kos rules :
      | Laundry |
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    And user input administrator name with "Omeii"
    And user input administrator phone with "083333373777"
    And user input other notes "Tidak menerima binatang piaraan"
    And user clicks on next button in bottom of add kos page
    And user input kost location "Tobelo" and clicks on first autocomplete result
    When user input address note "Belok kiri toko"
    Then user see error message "Tidak boleh kurang dari 20 karakter." under address note field
    When user input address note "Belok kiri dari took jaya abadi, lalu lurus sampai mentok, belok kanan kemudian lurus 200 meter, kemudian belok kiri. Belok kiri dari took jaya abadi, lalu lurus sampai mentok, belok kanan kemudian lurus 200 meter, kemudian belok kiri. Belok kiri dari took jaya abadi, lalu lurus sampai mentok smapai."
    Then user see error message "Tidak boleh lebih dari 300 karakter." under address note field
    And user see next button disable
    # Delete newly created kos draft
    And user navigates to "/ownerpage/kos"
    And user delete first kos on the list

  @CreateKosNoCity
  Scenario: Add kos without select regency/city and subdistrict
    Given user click add new kos button
    And user fills kost name field with "Kost AutoTest eCity " and random text
    And user clicks checkbox room type
    And user input room type with random text
    And user clicks on kost type icon "female"
    And user fill kos description with "Kos tanpa mamipay automation"
    And user set kos rules :
      | Laundry |
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    And user input administrator name with "Omeii"
    And user input administrator phone with "083333373777"
    And user input other notes "Tidak menerima binatang piaraan"
    And user clicks on next button in bottom of add kos page
    When user input kost location "DKI Jakarta" and clicks on first autocomplete result
    Then user see error message "Anda belum memilih Kabupaten/Kota." under kabupaten field
    And user see error message "Anda belum memilih Kecamatan." under kecamatan field
    And user see next button disable
    # Delete newly created kos draft
    And user navigates to "/ownerpage/kos"
    And user delete first kos on the list

  @CreateKosNewTypeSkipMamipay
  Scenario: Owner create new kos but skip fill mamipay data at form Lengkapi Data Diri && skip at page "Tanya Jawab"
    Given user click add new kos button
    And user fills kost name field with "Kost AutoSkipMami " and random text
    And user clicks checkbox room type
    And user input room type with random text
    And user clicks on kost type icon "male"
    And user fill kos description with "Kos tanpa mamipay automation"
    And user set kos rules :
      | Laundry |
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    When user input administrator name with "Sekip"
    And user input administrator phone with "083333377777"
    And user input other notes "Tidak menerima binatang piaraan"
    And user clicks on next button in bottom of add kos page
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user input address note "Dekat rumah nanas madu"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Umum":
      | Dapur |
    And user check facilities under "Fasilitas Kamar":
      | AC |
    And user check facilities under "Fasilitas Kamar Mandi":
      | Bathup |
    And user check facilities under "Parkir":
      | Parkir Mobil |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 3"
    And user input "9" to field total room
    And user input "5" to total available room
    And user clicks on next button in bottom of add kos page
    And user input monthly price with "800000" in add kos page
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user click skip in ask mamikos page
    And user click skip in ask mamikos page
    And user click done in success page
    Then user see kos with name "Kost AutoSkipMami " and random text, status "Diperiksa Admin" and type "Kos Putra"
        # Delete newly created kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "Kost AutoSkipMami" in admin kos owner page
    And user delete the kos in admin kos owner
    # Delete mamipay data
    And user navigates to "backoffice"
    And user click Mamipay Owner List
    And user search owner phone "0812345670004" in mamipay owner list
    And user delete the mamipay data from first list

  @CreateKosFromDraft
  Scenario: Owner wants to continue create kost with status "Draft" and skip mamipay
    Given user click add new kos button
    And user clicks checkbox room type
    And user fills kost name field with "Kost DraftNoMamipay " and random text
    And user input room type with random text
    And user clicks on kost type icon "mix"
    And user fill kos description with "kos harusnya jadi draft"
    And user set kos rules :
      | Laundry |
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    When user input administrator name with "Omaiwa"
    And user input administrator phone with "089992223456"
    And user input other notes "Akan dihapus setelah terbuat"
    And user clicks on next button in bottom of add kos page
    And user click back button on device
    And user click back button on device
    And user click back button on device
    And user click back button on device
    Then user see kos with name "Kost DraftNoMamipay" and random text, status "Draft" and type "Kos Campur"
    When user click complete kos data in first kos list
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user input address note "Dekat rumah simbah simbah"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user insert photo image to "Tambahan foto lain? boleh dong"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Umum":
      | Dapur  |
      | Kulkas |
    And user check facilities under "Fasilitas Kamar":
      | AC    |
      | Kasur |
      | Sofa  |
    And user check facilities under "Fasilitas Kamar Mandi":
      | Bathup |
      | Gayung |
    And user check facilities under "Parkir":
      | Parkir Mobil |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 4"
    And user input "15" to field total room
    And user input "10" to total available room
    And user clicks on next button in bottom of add kos page
    And user input monthly price with "300000" in add kos page
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user click skip in ask mamikos page
    And user click skip in ask mamikos page
    And user click done in success page
    Then user see kos with name "Kost DraftNoMamipay" and random text, status "Diperiksa Admin" and type "Kos Campur"
    # Delete newly created kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "Kost DraftNoMamipay" in admin kos owner page
    And user delete the kos in admin kos owner
    # Delete mamipay data
    And user navigates to "backoffice"
    And user click Mamipay Owner List
    And user search owner phone "0812345670004" in mamipay owner list
    And user delete the mamipay data from first list

  @CreateKosFromDraftActivMamipay
  Scenario: Create kos from kos with status draft && user doesn't active mamipay
    Given user click add new kos button
    And user clicks checkbox room type
    And user fills kost name field with "Kost DraftMpay Data " and random text
    And user input room type with random text
    And user clicks on kost type icon "male"
    And user fill kos description with "kos harusnya jadi draft then kos"
    And user set kos rules :
      | Laundry |
    And user upload kos rules photo
    And user select kos year built "2020"
    And user clicks checkbox administrator kos
    When user input administrator name with "Wowewo"
    And user input administrator phone with "08545432133333"
    And user input other notes "Akan dihapus setelah terbuat"
    And user clicks on next button in bottom of add kos page
    And user click back button on device
    And user click back button on device
    And user click back button on device
    And user click back button on device
    Then user see kos with name "Kost DraftMpay Data" and random text, status "Draft" and type "Kos Putra"
    When user click complete kos data in first kos list
    And user input kost location "Tobelo" and clicks on first autocomplete result
    And user input address note "Dekat rumah awewena aweweee"
    And user clicks on next button in bottom of add kos page
    And user clicks next when photo submission pop up appear
    And user insert photo image to "Foto bangunan tampak depan"
    And user insert photo image to "Foto tampilan dalam bangunan"
    And user insert photo image to "Foto tampak dari jalan"
    And user clicks on next button in bottom of add kos page
    And user insert photo image to "Foto depan kamar"
    And user insert photo image to "Foto dalam kamar"
    And user insert photo image to "Foto kamar mandi"
    And user insert photo image to "Tambahan foto lain? boleh dong"
    And user clicks on next button in bottom of add kos page
    And user check facilities under "Fasilitas Umum":
      | Dapur |
    And user check facilities under "Fasilitas Kamar":
      | AC |
    And user check facilities under "Fasilitas Kamar Mandi":
      | Bathup |
      | Gayung |
    And user check facilities under "Parkir":
      | Parkir Mobil |
    And user clicks on next button in bottom of add kos page
    And user clicks on kost size "3 x 4"
    And user input "10" to field total room
    And user input "6" to total available room
    And user clicks on next button in bottom of add kos page
    And user input monthly price with "500000" in add kos page
    And user clicks on next button in bottom of add kos page
    And user clicks on button Next BBK
    And user fill out activate mamipay form with Bank Account Number "898888765"
    And user fill out active mamipay form with  Bank Owner Name "Jojo Jono Joli"
    And user select bank account with "BCA"
    And user click submit data button to activate mamipay
    And user click skip in ask mamikos page
    And user click done in success page
    Then user see kos with name "Kost DraftMpay Data" and random text, status "Diperiksa Admin" and type "Kos Putra"
    # Delete newly created kos in admin
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to kost owner menu
    And user search kos name "Kost DraftMpay Data" in admin kos owner page
    And user delete the kos in admin kos owner
    # Delete mamipay data
    And user navigates to "backoffice"
    And user click Mamipay Owner List
    And user search owner phone "0812345670004" in mamipay owner list
    And user delete the mamipay data from first list