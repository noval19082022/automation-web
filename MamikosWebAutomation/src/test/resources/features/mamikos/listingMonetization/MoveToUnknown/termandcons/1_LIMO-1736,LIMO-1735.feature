@regression @LIMO2 @termsandcons
Feature: Terms and Cons owner

	#Scenario: Terms And Conditions - As User Logged In
  @TEST_LIMO-1736 @Automated @Web @listing-monetization
  Scenario: [Owner][Terms and Condition] Owner accsess page terms condition with login
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "master" and click on Enter button
    Then user redirected to "owner page"
    And user click on owner popup
    When user click mamikos.com logo
    Then user redirected to "/"
    When user clicks on Terms And Conditions
    Then user verify each of Term And Conditions
      | Pemilik                      | Penyewa                                       | Pemilik dan Penyewa       |
      | Syarat dan Ketentuan MamiPay | Syarat dan Ketentuan Booking Langsung Penyewa | Syarat dan Ketentuan Umum |
	#@termscons
	#Scenario: Term And Conditions - As User (Not Logged In)
  @TEST_LIMO-1735 @Automated @Web @listing-monetization
  Scenario: [Owner][Terms and Condition] Owner accsess page terms condition without login
    Given user navigates to "mamikos /"
    And user clicks on Terms And Conditions
    Then user verify each of Term And Conditions
      | Pemilik                      | Penyewa                                       | Pemilik dan Penyewa       |
      | Syarat dan Ketentuan MamiPay | Syarat dan Ketentuan Booking Langsung Penyewa | Syarat dan Ketentuan Umum |
