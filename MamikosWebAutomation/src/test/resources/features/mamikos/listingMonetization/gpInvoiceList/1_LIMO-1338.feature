@GPinvoiceList @invoice @listing-monetization @regression @LIMO2
Feature: [WEB][Mamipay][GP Invoice List] Check search function GP Invoice

  @TEST_LIMO-1338 @Automated @GP-Invoice-List @MamiPay @Web @essential-limo @listing-monetization @search-GP-Invoice
  Scenario: [WEB][Mamipay][GP Invoice List] Check search function GP Invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user open menu GP invoice
    And user input GP invoice number "GP2/20210225/00002146/8312"
    And user click button cari GPinvoice
    Then user will verify detail GP invoice
		
	#  Scenario: Search invalid Invoice GP
    And user open menu GP invoice
    And user input GP invoice number "GP2/20210225/00002146/83121111"
    And user click button cari GPinvoice
    Then user will get blank data

	#  Scenario: Search GP invoice with use owner number
    And user open menu GP invoice
    And user choose owner number
    And user input owner number "0833000008"
    And user click button cari GPinvoice
    Then user will verify detail GP invoice

	#  Scenario: Search GP invoice with use invalid owner number
    And user open menu GP invoice
    And user choose owner number
    And user input owner number "08330000811"
    And user click button cari GPinvoice
    Then user will get blank data

	#  Scenario: Search GP invoice with invoice code
    And user open menu GP invoice
    And user choose invoice code
    And user input invoice code "13837"
    And user click button cari GPinvoice
    Then user will verify detail GP invoice

	#  Scenario: Search invalid GP invoice with invalid invoice code
    And user open menu GP invoice
    And user choose invoice code
    And user input invoice code "1383711"
    And user click button cari GPinvoice
    Then user will get blank data

	#  Scenario: reset search detail
    And user open menu GP invoice
    And user choose invoice code
    And user input invoice code "13837"
    And user click reset button
    Then user column search by will reset to invoice number and data search restarted

	#  Scenario: Search unpaid GPinvoice
    And user open menu GP invoice
    And user choose "Unpaid"
    And user click button cari GPinvoice
    Then will show data transaction with "unpaid"

	#  Scenario: Search paid GPinvoice
    And user open menu GP invoice
    And user choose "Paid"
    And user click button cari GPinvoice
    Then will show data transaction with "paid"

   #  Scenario: Search expired GPinvoice
    And user open menu GP invoice
    And user choose "Expired"
    And user click button cari GPinvoice
    Then will show data transaction with "expired"

	#  Scenario: Search  GP1 invoice
	  And user open menu GP invoice
	  And user choose package type "gp1"
	  And user click button cari GPinvoice
	  Then will show data transaction with package type "GP1"

	#  Scenario: Search  GP2 invoice
	  And user open menu GP invoice
	  And user choose package type "gp2"
	  And user click button cari GPinvoice
	  Then will show data transaction with package type "GP2"

	#  Scenario: Search  GP3 invoice
	  And user open menu GP invoice
	  And user choose package type "gp3"
	  And user click button cari GPinvoice
	  Then will show data transaction with package type "GP3"

	#  Scenario: Search with due date
	  And user open menu GP invoice
	  And input date detail "2021-03-01" and "2021-03-10"
	  And user click button cari GPinvoice
	  Then will show data transaction according detail date
