@regression @pointExpiry @LIMO1 @LIMO1-staging

Feature: Manage Point Expiry

  @TEST_LIMO-1617
  Scenario: Update Point Expiry
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Expiry menu
    And user fill Owner Point Expiry in with "5"
    And user fill Tenant Point Expiry in with "10"
    And user click on Point Expiry Save button
    Then user verify allert success "Success!" and "Point Expiry successfully updated."
    And user fill Owner Point Expiry in with "2"
    And user fill Tenant Point Expiry in with "6"
    And user click on Point Expiry Save button
    Then user verify allert success "Success!" and "Point Expiry successfully updated."
