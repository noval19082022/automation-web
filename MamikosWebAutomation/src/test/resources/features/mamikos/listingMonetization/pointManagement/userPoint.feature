@regression @userPoint @LIMO1 @LIMO1-staging

Feature: Manage User Point

   @TEST_LIMO-1588
  Scenario: Filter User Point By Keyword
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    * user filter user point by keyword "tenant name"
    * user clicks on Search button
    Then system display list user point contains "Amanda"

  @TEST_LIMO-1619
  Scenario: Filter User Point By User
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    * user select filter User "Owner"
    * user clicks on Search button
    Then system display list user point as "Owner"
    When user select filter User "Tenant"
    * user clicks on Search button
    Then system display list user point as "Tenant"

  @TEST_LIMO-1620
  Scenario: Filter User Point By Status
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    * user select filter Status "Blacklist"
    * user clicks on Search button
    Then system display list user point as "Blacklist"
    When user select filter Status "Whitelist"
    * user clicks on Search button
    Then system display list user point as "Whitelist"

  @TEST_LIMO-1590
  Scenario: Sorting Total Point
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    * user clicks Total Point header
    Then user verify total point sorted "ascending"
    When user clicks Total Point header
    Then user verify total point sorted "descending"

  @TEST_LIMO-1585
  Scenario: Blacklist and Whitelist User Point
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    * user filter user point by keyword "phone number"
    * user clicks on Search button
    * user set the default status to Whitelist
    * user clicks on Point Management-User Point menu
    * user filter user point by keyword "phone number"
    * user clicks on Search button
    * user "Blacklist" user point and click "No, Go Back" on pop up confirmation
    Then system display list user point as "Whitelist"
    When user "Blacklist" user point and click "Yes, Do It!" on pop up confirmation
    * user filter user point by keyword "phone number"
    * user clicks on Search button
    Then system display list user point as "Blacklist"
    When user "Whitelist" user point and click "No, Go Back" on pop up confirmation
    Then system display list user point as "Blacklist"
    When user "Whitelist" user point and click "Yes, Do It!" on pop up confirmation
    * user filter user point by keyword "phone number"
    * user clicks on Search button
    Then system display list user point as "Whitelist"

  @TEST_LIMO-1587
  Scenario: Adjust Point Topup
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    * user filter user point by keyword "phone number"
    * user clicks on Search button
    * user clicks Adjust Point icon
    * user choose adjustment type "Topup"
    * user fills out point amount
    * user fills out note for "topup"
    * user clicks on Submit button on Adjust Point form
    Then user verify allert success "Success!" and "successfully updated."

  @TEST_LIMO-1583
  Scenario: Adjust Point Topdown
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    * user filter user point by keyword "phone number"
    * user clicks on Search button
    * user clicks Adjust Point icon
    * user choose adjustment type "Topdown"
    * user fills out point amount
    * user fills out note for "topdown"
    * user clicks on Submit button on Adjust Point form
    Then user verify allert success "Success!" and "successfully updated."

  @TEST_LIMO-1582
  Scenario: Manage User Point Display
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    Then user see Bulk Adjust Point button
    * user see Bulk Update Blacklist button
    * user see Keyword Filter
    * user see User Filter
    * user see Status Filter
    * user see Search button
    * user see Name
    * user see Email
    * user see Phone Number
    * user see User
    * user see Total Point
    * user see Status
    * user see Adjust Point icon
    * user see History icon
    * user see Pagination

    #Scenario: Manage Point History display TENG-1445
    When user clicks Total Point header
    * user clicks Total Point header
    * user click history icon on manage user point page
    Then user see at manage user point history contains:
      | Content                     |
      | Squilliam Fancyson as Owner |
      | Email: aditmami1@gmail.com  |
      | Phone: 082328130250         |
      | Date                        |
      | Activity                    |
      | Notes                       |
      | Point                       |
      | Total                       |

    #Scenario: Pagination manage user point filter TENG-1449
    When user choose to filter all activity with value "Admin Adjustment"
    Then history with selected filter value "Admin Adjustment" is displayed

    #Scenario: Pagination manage user point history TENG-1446
    When user click next page button on manage user point
    Then next manage user point page will be opened
    When user click previous page button on manage user point
    Then previous manage user point page will be opened
    When user click page index "2"
    Then manage user point page "2" will be opened

    #Scenario: Manage Point History Back button redirection TENG-1447
    When user click back to user point
    Then user redirected to "https://jambu.kerupux.com/admin/point/user/index?#point"

   #Scenario: Pagination manage user point TENG-1442
    When user click next page button on manage user point
    Then next manage user point page will be opened
    When user click previous page button on manage user point
    Then previous manage user point page will be opened
    When user click page index "2"
    Then manage user point page "2" will be opened

  @TEST_LIMO-1621
  Scenario: Bulk Update Blacklist
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    * user click Bulk Update Blacklist
    Then user see "Bulk Update Blacklist User" pop-up appear
    When admin master upload csv file "bulk_blacklist"
    * user click button submit csv bulk blacklist
    Then success Update Blacklist using csv

  @TEST_LIMO-1622
  Scenario: Bulk Adjust Point
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin consultant"
    When user clicks on Point Management-User Point menu
    * user click Bulk Adjust Point
    Then user see "Bulk Adjust Point" pop-up appear
    When admin master upload csv file "bulk_adjust_point"
    * user click button submit csv bulk adjust point
    Then success Update Blacklist using csv