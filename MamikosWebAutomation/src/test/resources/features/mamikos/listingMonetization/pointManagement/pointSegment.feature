@pointSegment

Feature: Manage Point Segment

  @TEST_LIMO-1601
  Scenario: Add New Segment
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Segment menu
    And user clicks on Add Segment button
    And user fills out form create segment for "add" and click save
    Then user verify allert success "Success!" and "Segment successfully added."
    And user verify new segment displayed

  @TEST_LIMO-1632
  Scenario: Verify New Segment Added on Manage Point Setting Page
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Setting menu
    Then user see new segment added displayed on Manage Point-Setting menu

  @TEST_LIMO-1602
  Scenario: Check Validation on Segment Type Field
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Segment menu
    And user clicks on Add Segment button
    And user fills out form create segment for "type empty" and click save
    Then user see segment validation "The segment field is required."
    When user fills out form create segment for "type special character" and click save
    Then user see segment validation "The segment may only contain letters, numbers, and dashes."

  @TEST_LIMO-1634
  Scenario: Check Validation on Segment Title Field
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Segment menu
    And user clicks on Add Segment button
    And user fills out form create segment for "title empty" and click save
    Then user see segment validation "The title field is required."

  @TEST_LIMO-1598
  Scenario: Edit Segment
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Segment menu
    And user click "edit" on segment
    And user fills out form create segment for "edit" and click save
    Then user verify allert success "Success!" and "Segment successfully updated."
    And user verify new segment displayed

  @TEST_LIMO-1600
  Scenario: Delete Segment
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Segment menu
    And user click "delete" on segment
    Then user verify allert success "Success!" and "Segment successfully deleted."
    And user verify new segment not displayed

  @TEST_LIMO-1635
  Scenario: Verify New Segment Deleted is not Present on Manage Point Setting Page
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Setting menu
    Then user see new segment added not displayed on Manage Point-Setting menu

  @TEST_LIMO-1595
  Scenario Outline: Add New Segment (Tenant Goldplus)
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Segment menu

    #Scenario: Manage Point Segment Table display TENG-1271 TENG-1264
    Then user see at point management segment contains:
      | Head Table   |
      | ID           |
      | Type         |
      | Title        |
      | Kost Level   |
      | Room Level   |
      | Target       |
      | Is Published |
      | Actions      |

    #Scenario: Add Segment LIMO-1601
    When user clicks on Add Segment button
    And user fills out form create segment for "<scenario>" and click save
    Then user verify allert success "Success!" and "Segment successfully added."
    And user verify new segment displayed

    #Delete new segment tenant LIMO-1600
    When user click "delete" on segment
    Then user verify allert success "Success!" and "Segment successfully deleted."
    And user verify new segment not displayed
    When user clicks on Point Management-Tenant Setting menu
    Then user see new segment added not displayed on Manage Point-Setting menu
    Examples:
      | scenario                    |
      | add tenant - New GoldPlus 1 |
      | add tenant - New GoldPlus 2 |
      | add tenant - New GoldPlus 3 |

  @TEST_LIMO-1616
  Scenario: Total Room checked And Terms and Condition setting Owner
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Setting menu
    And user check checkbox total room
    And user uncheck checkbox total room
    Then user can fill the field
    When user click button save Terms and Conditions
    Then system display success message

  @TENG-1384
  Scenario: Edit Tenant Activity data, Edit Tenant Status Activity And Terms & Condition setting Owner
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Tenant Setting menu
    And user input point limit install app tenant point setting "20"
    And user click cancel button install app and ok on alert
    Then user see Manage Tenant Point Setting Page
    When user input point limit install app tenant point setting "20"
    And user click save button install app and ok on alert
    Then user see Manage Tenant Point Setting Page
    And system display success message
    When user click button save Terms and Conditions Tenant Setting
    Then system display success message
