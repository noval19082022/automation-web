@regression @ownerRoomGroup @LIMO1

Feature: Manage Owner Room Group

  @TEST_LIMO-1624
  Scenario: Add New Owner Room Group
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Room Group menu
    And user clicks on Add Owner Room Group button
    And user fills out form create owner room group for "add" and click save
    Then user verify allert success "Success!" and "Owner Room Group successfully added."
    And user verify new room group "added" displayed

  @TEST_LIMO-1578
  Scenario: Verify New Owner Room Group Added on Manage Point Setting Page
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Setting menu
    Then user verify new room group "added" displayed

  @TEST_LIMO-1577
  Scenario: Add New Owner Room Group with Empty Field
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Room Group menu
    And user clicks on Add Owner Room Group button
    And user fills out form create owner room group for "empty" and click save
    Then user see validation
      | The floor field is required.  |
      | The ceil field is required.   |

  @TEST_LIMO-1572
  Scenario: Add New Owner Room Group with Floor Field Higher Than Cell Field
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Room Group menu
    And user clicks on Add Owner Room Group button
    And user fills out form create owner room group for "floor field higher" and click save
    Then user see validation
      | Floor must be lower or equal than ceil.  |

  @TEST_LIMO-1574
  Scenario: Edit Owner Room Group
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Room Group menu
    And user click "edit" on room group
    And user fills out form create owner room group for "edit" and click save
    Then user verify allert success "Success!" and "Owner Room Group successfully updated."
    And user verify new room group "updated" displayed

  @TEST_LIMO-1625
  Scenario: Verify New Owner Room Group added on Manage Point Setting Page
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Setting menu
    Then user verify new room group "updated" displayed
    Then user verify new room group "added" not displayed

  @TEST_LIMO-1573
  Scenario: Delete Owner Room Group
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Room Group menu
    And user click "delete" on room group
    Then user verify allert success "Success!" and "Owner Room Group successfully deleted."
    And user verify new room group "updated" not displayed

  @TEST_LIMO-1626
  Scenario: Verify New Owner Room Group Updated on Manage Point Setting Page
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Setting menu
    Then user verify new room group "updated" not displayed

  @TEST_LIMO-1575 @ownerRoomGroupPagination
  Scenario: Owner Room Group Pagination
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Room Group menu
    Then user verify the pagination of Owner Room Group

  @TEST_LIMO-1576 @managePointOwnerRoomDisplay
  Scenario: Manage Point Owner Room Display
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Room Group menu
    Then user verify Manage Point Owner Room Group page items
    | Add Owner Room Group |
    | ID                   |
    | Group                |
    | Actions              |