@pointManagement

  Feature: Point Management - Owner Point Blacklist
    @TEST_LIMO-1580
    Scenario: User Type Blacklist/Whitelist
      Given user navigates to "mamikos admin"
      * user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Owner Point Blacklist menu
      * user click switch status on "Blacklisted" user type
      Then user verify success alert on Manage Owner Point Blacklist with "Success! Blacklist Config successfully updated"
      * user revert back "[Kost Level] Super" to "Blacklisted"

    @TEST_LIMO-1581
    Scenario: Add User Type
      Given user navigates to "mamikos admin"
      * user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Owner Point Blacklist menu
      * user click on Add User Type button
      * user select user type data with "Churn"
      * user select status as "Blacklisted"
      * user click save button on Add User Type
      Then user verify success alert on Manage Owner Point Blacklist with "Success! Blacklist Config successfully created"
      When user click delete button on "[Kost Level] Churn"
      Then user verify success alert on Manage Owner Point Blacklist with "Success! Blacklist Config successfully deleted"

    @TEST_LIMO-1579
    Scenario: Point Blacklist Display
      Given user navigates to "mamikos admin"
      * user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Owner Point Blacklist menu
      Then user verify Manage Owner Point Blacklist page items
      | Head Table                   |
      | No                           |
      | User Type                    |
      | Kost Level ID / Custom Value |
      | Blacklist Status             |