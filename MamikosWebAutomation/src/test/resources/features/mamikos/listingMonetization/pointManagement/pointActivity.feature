@pointActivity

Feature: Manage Point Activity

  @TEST_LIMO-1610
  Scenario: Add New Activity Owner
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Activity menu
    And user clicks on Add Activity button
    And user fills out form create activity for "add owner target" and click save
    Then user verify allert success "Success!" and "Activity successfully added."
    When user filter activity by keyword "test_activity_owner"
    And user clicks on Search button
    Then user verify new activity displayed
      | test_activity_owner | Test Activity Owner | Test Activity Owner | OWNER | Active | 0 |

  @TEST_LIMO-1611
  Scenario: Add New Activity Tenant
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Activity menu
    And user clicks on Add Activity button
    And user fills out form create activity for "add tenant target" and click save
    Then user verify allert success "Success!" and "Activity successfully added."
    When user filter activity by keyword "test_activity_tenant"
    And user clicks on Search button
    Then user verify new activity displayed
      | test_activity_tenant | Test Activity Tenant | Test Activity Tenant | TENANT | Active | 0 |

  @TEST_LIMO-1627
  Scenario: Verify New Segment Added on Manage Point Setting Page
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Setting menu
    Then user see new activity added displayed on Manage Point-Owner Setting menu
    When user clicks on Point Management-Tenant Setting menu
    Then user see new activity added displayed on Manage Point-Tenant Setting menu

  @TEST_LIMO-1609
  Scenario: Add New Activity With Empty Field
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Activity menu
    And user clicks on Add Activity button
    And user fills out form create activity for "empty field" and click save
    Then user see validation
      | The Image field is required when Status is 1. |
      | The name field is required.                   |

  @TEST_LIMO-1630
  Scenario: Add New Activity with Key Already Exist
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Activity menu
    And user clicks on Add Activity button
    And user fills out form create activity for "add owner target" and click save
    Then user see validation
      | The Image field is required when Status is 1. |

  @TEST_LIMO-1608
  Scenario: Edit Activity
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Activity menu
    And user filter activity by keyword "test_activity_owner"
    And user clicks on Search button
    And user click "edit" on activity
    And user fills out form create activity for "edit" and click save
    Then user verify allert success "Success!" and "Activity successfully updated."
    When user filter activity by keyword "test_activity_owner"
    And user clicks on Search button
    Then user verify new activity displayed
      | test_activity_owner | Test Activity Owner Updated | Test Activity Owner | OWNER | Active | 0 |

  @TEST_LIMO-1604
  Scenario: Delete Activity
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Activity menu
    And user filter activity by keyword "test_activity_owner"
    And user clicks on Search button
    And user click "delete" on activity
    Then user verify allert success "Success!" and "Activity successfully deleted."
    When user filter activity by keyword "test_activity_tenant"
    And user clicks on Search button
    And user click "delete" on activity
    Then user verify allert success "Success!" and "Activity successfully deleted."
    When user filter activity by keyword "test_activity"
    And user clicks on Search button
    Then user verify no activity displayed

  @TEST_LIMO-1631
  Scenario: Verify Segment Deleted Not Present in Manage Point Setting Page
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Owner Setting menu
    Then user see new activity added not displayed on Manage Point-Owner Setting menu
    When user clicks on Point Management-Tenant Setting menu
    Then user see new activity added not displayed on Manage Point-Tenant Setting menu

  @TEST_LIMO-1612
  Scenario: Verify Point Activity Display
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Activity menu
    Then user see add activity button is displayed
    And user see keyword filter field is displayed
    And user see filter status button is displayed
    And user see search button is displayed
    And user see edit button is displayed
    And user see delete button is displayed
    And user see pagination is displayed
    And user see id column is displayed
    And user see key column is displayed
    And user see name column is displayed
    And user see title column is displayed
    And user see target column is displayed
    And user see status column is displayed
    And user see order column is displayed
    And user see actions column is displayed

  @TEST_LIMO-1605
  Scenario: Search Activity
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Point Management-Activity menu
    And user filter activity by keyword "tenant_test1"
    And user clicks on Search button
    Then user verify new activity displayed
      | tenant_test1 | Tenant Test | Tenant Test 1 | TENANT | Active | 6 |

