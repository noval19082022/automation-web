@regression @GpOnboarding @LIMO2 @listing-monetization
Feature: Owner Dashboard GP-ONboarding

  @TEST_LIMO-383 @Automated @GP-Onboarding @listing-monetization @web @web-covered
  Scenario: [Web Owner][GP-Onboarding] Owner visit “Panduan Fitur di GoldPlus” and click “Naikkan Iklan Anda”
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner gp 1" and click on Enter button
    And user click GP Label in Home
    And user click "Pelajari caranya" in GP registration page
    And user click "Naikkan Iklan Anda" in Panduan fitur GP
    Then user redirected to "owner /goldplus/guides/advertisement_usage"
    And user see e-card to know how-to-use is as expected
      | title                      | desc                                                           |
      | Kunjungi Menu MamiAds      | Klik Kelola pada halaman utama Mamikos, lalu pilih MamiAds.    |
      | Beli Saldo MamiAds         | Klik “Beli Saldo” pada halaman MamiAds.                        |
      | Pilih Iklan Properti Anda  | Anda bebas memilih properti yang ingin diiklankan.             |
      | Pilih Jenis Anggaran       | Anda dapat membatasi pemakaian saldo MamiAds sesuai kebutuhan. |
      | Anggaran Aktif Setiap Hari | Setiap hari, iklan dinaikkan dengan anggaran yang sama.        |
    When user click "Coba Sekarang"
    Then user redirected to "owner /mamiads"

  @TEST_LIMO-384 @Automated @listing-monetization @web @web-covered
  Scenario: [Web Owner][GP-Onboarding] Owner visit “Panduan Fitur di GoldPlus” and click “Memantau Performa Kos”
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner gp 1" and click on Enter button
    And user click GP Label in Home
    And user click "Pelajari caranya" in GP registration page
    And user click "Memantau Performa Kos" in Panduan fitur GP
    Then user redirected to "owner /goldplus/guides/performance_monitor"
    And user see e-card to know how-to-use is as expected
      | title                           | desc                                                          |
      | Klik menu “Statistik”           | Lihat menu di layar bawah, dan klik “Statistik”.              |
      | Klik “Statistik GoldPlus”       | Ada tiga jenis statistik, pilih “Statistik GoldPlus”.         |
      | Pilih Nama Kos                  | Klik kos yang Anda ingin lihat statistiknya.                  |
      | Selesai! Cek Statistik Kos Anda | Di halaman ini, Anda bisa melihat performa kos GoldPlus Anda. |
    When user click "Coba Sekarang"
    Then user redirected to "owner /goldplus/statistic"

  @TEST_LIMO-385 @Automated @listing-monetization @web @web-covered
  Scenario: [Web Owner][GP-Onboarding] Owner visit “Panduan Fitur di GoldPlus” and click “Cek Properti Sekitar”
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner gp 1" and click on Enter button
    And user click GP Label in Home
    And user click "Pelajari caranya" in GP registration page
    And user click "Cek Properti Sekitar" in Panduan fitur GP
    Then user redirected to "owner /goldplus/guides/property_check"
    And user see e-card to know how-to-use is as expected
      | title                        | desc                                                                |
      | Klik Menu "Kelola"           | Lihat menu di layar bawah, dan klik "Kelola".                       |
      | Pilih "Cek Properti Sekitar" | Lihat Fitur Promosi lalu klik di bagian fitur Cek Properti Sekitar. |
      | Pilih "Properti Anda"        | Data yang muncul disesuaikan dengan area properti yang dipilih.     |
      | Tentukan Periode             | Data yang ditampilkan mengikuti periode yang dipilh.                |
    When user click "Coba Sekarang"
    Then user redirected to "owner /check-nearby-property"

  @TEST_LIMO-386 @Automated @listing-monetization @web @web-covered
  Scenario: [Web Owner][GP-Onboarding] Owner visit “Panduan Fitur di GoldPlus” and click “Menggunakan MamiPoin”
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner gp 1" and click on Enter button
    And user click GP Label in Home
    And user click "Pelajari caranya" in GP registration page
    And user click "Menggunakan MamiPoin" in Panduan fitur GP
    Then user redirected to "owner /goldplus/guides/mamipoint_usage"
    And user see e-card to know how-to-use is as expected
      | title               | desc                                                         |
      | Klik “MamiPoin”     | Pilihan “MamiPoin” berada di halaman utama bagian atas.      |
      | Klik “Tukar Poin”   | Pilihan “Tukar Poin” ada di halaman MamiPoin.                |
      | Klik “Lihat Hadiah” | Pilih hadiah yang Anda ingin tukar dan klik di bagian kanan. |
      | Tukarkan Poin Anda  | Pastikan poin Anda cukup untuk ditukar dengan hadiahnya.     |
    When user click "Coba Sekarang"
    Then user redirected to "owner /mamipoin"

  @TEST_LIMO-387 @Automated @listing-monetization @web @web-covered
  Scenario: [Web Owner][GP-Onboarding] Owner visit “Panduan Fitur di GoldPlus” and click “Kelola Tagihan”
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner gp 1" and click on Enter button
    And user click GP Label in Home
    And user click "Pelajari caranya" in GP registration page
    And user click "Kelola Tagihan" in Panduan fitur GP
    Then user redirected to "owner /goldplus/guides/bill_management"
    And user see e-card to know how-to-use is as expected
      | title                 | desc                                                               |
      | Klik Menu “Kelola”    | Lihat menu di layar bawah, dan klik “Kelola”.                      |
      | Klik “Kelola Tagihan” | Di menu “Manajemen Kos”, pilih “Kelola Tagihan”.                   |
      | Pilih Nama Kos        | Klik nama kos untuk memilih kos yang Anda ingin lihat tagihannya.  |
      | Klik “Ingatkan Semua” | Untuk ingatkan semua penyewa di kos ini, klik tulisan warna hijau. |
    When user click "Coba Sekarang"
    Then user redirected to "owner /billing-management"

  @TEST_LIMO-379 @Automated @listing-monetization @web @web-covered
  Scenario: [Web Owner][GP-Onboarding] Panduan Fitur di GoldPlus clicked by owner
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner gp 1" and click on Enter button
    And user click GP Label in Home
    And user click "Pelajari caranya" in GP registration page
    Then user see list "Panduan Fitur di Goldplus" is as expected
      | title                 | content                                                                                 |
      | Naikkan Iklan Anda    | Gunakan MamiAds untuk memperluas jangkauan iklan.                                       |
      | Memantau Performa Kos | Lihat performa kos Anda setelah menggunakan GoldPlus.                                   |
      | Cek Properti Sekitar  | Anda dapat memantau kondisi bisnis kos sekitar.                                         |
      | Menggunakan MamiPoin  | Lebih banyak point yang Anda dapatkan bersama GoldPlus. Tukarkan dengan hadiah menarik. |
      | Kelola Tagihan        | Anda bisa melihat catatan tagihan penyewa dan mengirim pengingat pembayaran.            |

  @TEST_LIMO-370 @Automated @listing-monetization @web @web-covered
  #Scenario: [Web][GP-Onboarding] Goldplus Info Clicked

  Scenario Outline: Owner Dashboard GP On Boarding Info Untuk Anda
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "<user>" and click on Enter button
    And user click on owner popup
    Then user verify text "<infoUntukAnda>" on section info untuk anda is appear
    Then user redirected to "<link>"
    Examples:
      | user       | link                                                                     | infoUntukAnda                                                     |
      | notGP      | owner /goldplus/submission/packages/gp2?redirection_source=infountukanda | GoldPlus 2 diskon 15% hanya dengan voucher di halaman pembayaran! |
      | owner gp 1 | owner /goldplus                                                          | GoldPlus 2 diskon 15% hanya dengan voucher di halaman pembayaran! |
