@regression @LIMO2 @listing-monetization
Feature: [Owner GP Invoice][MamiPoin Owner]MamiPoin display when owner has Poin available

	#Feature: Owner - Mamipoin GP
	#Scenario: Mamipoin is available
  @TEST_LIMO-145 @automated @listing-monetization @mamipoin-owner @web
  Scenario: [Owner GP Invoice][MamiPoin Owner]MamiPoin display when owner has Poin available
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "payment" and click on Enter button
    And user click on owner popup
    Then user verify mamipoin is available
