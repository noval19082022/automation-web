@regression @daftarGP @LIMO2 @listing-monetization
Feature: Register GP 2

  @TEST_LIMO-44
  Scenario: Activated GP2 and terminate GP2
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner gold plus" and click on Enter button
    And user click on owner popup
    When user click Daftar GoldPlus field
    Then user redirect to List Package
    When user click pilih paket on "GoldPlus 2"
    Then user redirect to Detail Tagihan
    When user click button bayar sekarang

# Scenario payment
    Then  user click mamipoin toggle
    When user see total potongan mamipoin "-71500"
    And user select payment method "OVO" for "PayLGAutomation"
    Then system display payment using "OVO" is "Pembayaran Berhasil"
    When user navigates to "owner /"
    Then validate that owner have "GoldPlus 2"

#  Scenario: Terminate GP
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin ob"
    When user access to Property Package menu
    And user search owner property name "Kos Bapak Ramos" on property package
    And user terminate active goldplus
    Then system display success terminate contract "Contract has been terminated"

  @TEST_LIMO-147
  Scenario Outline: Verify detail tagihan page GP 1 or GP 2
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "notGP" and click on Enter button
    Then user navigates to "<link>"
    And user verify text "<pilihanAnda>" on section pilihan anda is appear
    And user verify text "<priceGP>" on section rincian pembayaran is appear
    And user see text "Saya memahami dan menyetujui Syarat dan Ketentuan Umum GoldPlus yang berlaku." at detail tagihan GP
    And user see "Bayar sekarang" button

    Examples:
      | link                                    | pilihanAnda                                 | priceGP   |
      | owner /goldplus/submission/packages/gp1 | Semua kos Anda aktif sebagai kos GoldPlus 1 | Rp59.000  |
      | owner /goldplus/submission/packages/gp2 | Semua kos Anda aktif sebagai kos GoldPlus 2 | Rp129.000 |

  @TEST_LIMO-143
  Scenario: Make sure when user click tnc back to detail tagihan again
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "notGP" and click on Enter button
    Then user navigates to "owner /goldplus/submission/packages/gp1"
    When user click text "syarat dan ketentuan umum" at goldplus page
    Then user see pop up syarat dan ketentuan umum
    When user click icon close at pop up syarat ketentuan umum
    Then user redirect to Detail Tagihan
    When user click text "syarat dan ketentuan umum" at goldplus page
    And user click button saya mengerti at syarat ketentuan umum
    Then user redirect to Detail Tagihan