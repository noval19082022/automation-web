@regression @daftarGP @LIMO2 @listing-monetization
Feature: Owner Dashboard GP-Widget

  @TEST_LIMO-1092
  Scenario Outline: Owner not yet join GP or hasn’t active kost
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "<user>" and click on Enter button
    And user click on owner popup
    Then daftar goldplus label is "<validate>"
    Examples:
      | user           | validate  |
      | nonkostditolak | dissapear |
      | premiumnosaldo | appear    |

  @TEST_LIMO-1667
  Scenario: Entry point widget GP "Daftar sekarang"
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "notGP" and click on Enter button
    And user click on owner popup
    And user click Daftar GoldPlus field
    Then user redirect to List Package

  @TEST_LIMO-1670
  Scenario: Make sure wording H-1 expired GP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "lastDayGP" and click on Enter button
    And user click on owner popup
    Then validate expired GP Expired Date Label is "Hari Terakhir"

  @TEST_LIMO-51
  Scenario:  Owner register GP1 until activated
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "terminatedGP" and click on Enter button
    And user click on owner popup
    When user click Daftar GoldPlus field
    And user redirect to List Package
    And user click pilih paket on "GoldPlus 1"
    And user redirect to Detail Tagihan
    And user click button bayar sekarang

#  Scenario: payment GP 1
    When user click mamipoin toggle
    Then user see total potongan mamipoin "-29000"
    When user select payment method "OVO" for "PayLGAutomation"
    Then system display payment using "OVO" is "Pembayaran Berhasil"
    And user navigates to "owner /"
    And validate that owner have "GoldPlus 1"
    And user click on owner popup
    When user click mamikos goldplus label
    And user click Lihat Selengkapnya kos goldplus anda
    Then user see page title is "Paket GoldPlus Anda" on paket goldplus Page
    And user see status goldplus is "Goldplus 1 Aktif" on tab semua paket gold plus anda

  @TEST_LIMO-1671
  Scenario: Terminate GP
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin ob"
    When user access to Property Package menu
    And user search owner property name "Kost Pemuja Rusman" on property package
    And user terminate active goldplus
    Then system display success terminate contract "Contract has been terminated"

  @TEST_LIMO-43
  Scenario:  check when owner doesnt have mamipoin, toogle mmaipoin not show
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "limo doesnt have poin" and click on Enter button
    And user click on owner popup
    When user click Daftar GoldPlus field
    And user redirect to List Package
    And user click pilih paket on "GoldPlus 1"
    And user redirect to Detail Tagihan
    And user click button bayar sekarang
    Then user see "Poin Anda masih 0. Lakukan aktivitas di Mamikos untuk mendapat poin." at section mamipoin

     #scenario delete or reset data GP
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    Then user navigates to "GP Recurring Tools"
    And user input phone number "082233545505" at gp recurring
    When user click button reset at gp recurring
    Then user see text "Reset success!" at gp recurring tools

  @TEST_LIMO-32
  Scenario: Unpaid GP invoice expired, terminated, or deleted after use MamiPoin
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "deleteGP" and click on Enter button
    And user click on owner popup
    When user click Daftar GoldPlus field
    And user redirect to List Package
    And user click pilih paket on "GoldPlus 1"
    And user redirect to Detail Tagihan
    And user click button bayar sekarang
    When user click mamipoin toggle
    Then user see total potongan mamipoin "-10000"
    And user navigates to "owner /"
    And user click MamiPoin widget
    Then user verify MamiPoin onboarding is appear
    And user click tab Riwayat Poin
    Then user see poin deduction history is displayed on MamiPoin History

    #scenario delete or reset data GP
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user navigates to "GP Recurring Tools"
    And user input phone number "082233545508" at gp recurring
    When user click button reset at gp recurring
    Then user see text "Reset success!" at gp recurring tools

    #scenario check mamipoin after delete or reset data GP
    Given user navigates to "mamikos /"
    And user navigates to "owner /"
    And user click MamiPoin widget
    And user click tab Riwayat Poin
    Then user see poin return history is received on Mamipoin History

  @TEST_LIMO-35
  Scenario: [Owner GP Invoice][MamiPoin Owner]MamiPoin not displayed when user is Blacklisted
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "limo blacklist owner poin" and click on Enter button
    Then user not see mamipoin toggle

  @TEST_LIMO-33
  Scenario: [Owner GP Invoice][MamiPoin Owner]Owner slide ON and OFF MamiPoin toggle on GP invoice
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "limo owner mamipoin" and click on Enter button
    And user click on owner popup
    And user click Daftar GoldPlus field
    And user redirect to List Package
    And user click pilih paket on "GoldPlus 1"
    And user redirect to Detail Tagihan
    And user click button bayar sekarang
    And user click mamipoin toggle
    And user see total potongan mamipoin "-25000"
    And user navigates to "owner /"
    And user click MamiPoin widget
    And user verify MamiPoin onboarding is appear
    And user click tab Riwayat Poin
    Then user see poin deduction history is displayed on MamiPoin History

    #scenario Mamipoin toggle is OFF
    Given user navigates to "owner /"
    And user click widget GP "Menunggu Pembayaran"
    And user clicks on mamipoin toggle button to OFF
    Then user not see total potongan mamipoin

     #scenario check mamipoin after delete or reset data GP
    Given user navigates to "mamikos /"
    And user navigates to "owner /"
    And user click MamiPoin widget
    And user click tab Riwayat Poin
    Then user see poin return history is received on Mamipoin History

    #scenario delete or reset data GP
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user navigates to "GP Recurring Tools"
    And user input phone number "082233545509" at gp recurring
    When user click button reset at gp recurring
    Then user see text "Reset success!" at gp recurring tools

  @TEST_LIMO-31
  Scenario: [Owner][GP recurring H-7][detail tagihan]
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "recurringGP" and click on Enter button
    And user click on owner popup
    And user click Daftar GoldPlus field
    And user redirect to List Package
    And user click pilih paket on "GoldPlus 1"
    And user redirect to Detail Tagihan
    And user click button bayar sekarang
    And user select payment method "OVO" for "PayLGAutomation"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

    #scenario wants to create gp recurring
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user navigates to "GP Recurring Tools"
    And user input phone number "082233545510" at gp recurring tools
    When user click button recurring at gp recurring tools
    Then user see text "Recurring contract created!" at gp recurring tools

    #scenario check widget GP redirect to detail tagihan
    Given user navigates to "owner /"
    And validate that owner have "GoldPlus 1"
    And user click on owner popup
    And user click mamikos goldplus label
    And user click back at GP page
    And user click mamikos goldplus label
    Then user redirect to Detail Tagihan

    #scenario terminate GP
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin ob"
    When user access to Property Package menu
    And user search owner property name "Kost AT 6 GP Recurring" on property package
    When user terminate active goldplus
    Then system display success terminate contract "Contract has been terminated"

     #scenario delete or reset data GP
    Given user navigates to "backoffice"
    When user navigates to "GP Recurring Tools"
    And user input phone number "082233545510" at gp recurring
    When user click button reset at gp recurring
    Then user see text "Reset success!" at gp recurring tools

  @TEST_LIMO-68
  Scenario: [Detail Tagihan][Bayar Sekarang Rpxxxxxx] Make sure owner GP tap bayar sekarang will redirect to invoice universal
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "limo not gp verify" and click on Enter button
    * user click on owner popup
    * user click Daftar GoldPlus field
    Then user redirect to List Package
    When user click pilih paket on "GoldPlus 1"
    Then user redirect to Detail Tagihan
    * user see text "GoldPlus 1" at page detail tagihan GP
    * user see text description "Semua kos Anda aktif sebagai kos GoldPlus 1" at page detail tagihan GP
    * user see button Bayar Sekarang
    When user click button bayar sekarang
    Then user validate transaction "GoldPlus 1" after success redirect

    #scenario delete or reset data GP
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user navigates to "GP Recurring Tools"
    And user input phone number "082233545512" at gp recurring
    When user click button reset at gp recurring
    Then user see text "Reset success!" at gp recurring tools
