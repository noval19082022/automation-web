@regression @LIMO1 @LIMO1-staging
Feature: Visibility

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @TEST_LIMO-291
  Scenario: Saldo mamiAds = 0 - Never purchase mamiAds - Never Allocated
    Given user fills out owner login as "newOwnerMamiads" and click on Enter button
    Then user verify message "Iklan Anda tenggelam? Pakai MamiAds!" in saldo MamiAds
    And user verify title "Lihat disini" in saldo MamiAds
    And user click on owner popup
    When user click on Saldo MamiAds button
    Then user redirected to "/mamiads?redirectionSource=Owner%20Dashboard"
#  MA-5028: To make sure Owner who never purchase saldo Mamiads, never allocate, saldo = 0 after back from Landing MamiAds will redirect to Owner Dashboard
    When user click back button on device
    Then user redirected to "owner page"
# Scenario: Redirect to LP on account Saldo mamiAds = 0 - Never purchase mamiAds - Never Allocated per session (MA-5029)
    When user logs out as a Owner user
    And user clicks on Enter button
    And user fills out owner login as "newOwnerMamiads" and click on Enter button
    Then user verify message "Iklan Anda tenggelam? Pakai MamiAds!" in saldo MamiAds
    And user verify title "Lihat disini" in saldo MamiAds
    When user click on Saldo MamiAds button
    Then user redirected to "/mamiads?redirectionSource=Owner%20Dashboard"
#  MA-5028: To make sure Owner who never purchase saldo Mamiads, never allocate, saldo = 0 after back from Landing MamiAds will redirect to Owner Dashboard
    When user click back button on device
    Then user redirected to "owner page"
# 2nd click on saldo mamiads button
    When user click on Saldo MamiAds button
    Then user redirected to "owner /mamiads"

  @TEST_LIMO-289
  Scenario: All listing full occupied
    Given user fills out owner login as "mamiadsListingFullOccupied" and click on Enter button
    Then user verify message "Pakai MamiAds, bikin iklan makin terlihat" in saldo MamiAds
    And user verify title "Rp25.000" in saldo MamiAds
    And user click on owner popup
    When user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    Then user redirected to "owner /mamiads"

  @TEST_LIMO-295
  Scenario: Never Purchase MamiAds and all listing is allocated
    Given user fills out owner login as "nonMamiadsAllListingIsAllocated" and click on Enter button
    Then user verify message "Pakai MamiAds, bikin iklan makin terlihat" in saldo MamiAds
    And user verify title "Rp75.000" in saldo MamiAds
    And user click on owner popup
    When user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    Then user redirected to "owner /mamiads"
    When user click "Riwayat"
    Then user see Title "Belum Ada Transaksi" with message "Transaksi yang masih dalam proses akan muncul di halaman ini." on page "dalam proses"
    When user click "Selesai"
    Then user see Title "Belum Ada Transaksi" with message "Transaksi yang sudah selesai akan muncul di halaman ini." on page "selesai"
    When user click back button on Riwayat page
    And user click "Semua Iklan"
  #Scenario Filter nonaktif when all ads is allocated (MA-5851)
    And user click "Iklan Nonaktif"
    Then user see Title "Semua Iklan Anda Sudah Naik" with message "Iklan properti Anda akan naik ke posisi yang lebih tinggi pada hasil pencarian." on page "mamiAds"

  @TEST_LIMO-296
  Scenario: Ever Purchase MamiAds and all listing is allocated
    Given user fills out owner login as "mamiadsAllListingIsAllocated" and click on Enter button
    Then user verify message "Pakai MamiAds, bikin iklan makin terlihat" in saldo MamiAds
    And user click on owner popup
    When user click on Saldo MamiAds button
    And user close pop up on boarding mamiads
    Then user redirected to "owner /mamiads"
    When user click "Riwayat"
    And user click "Selesai"
    Then user verify status transaction mamiads is "Lunas"
    When user click back button on Riwayat page
    And user click "Semua Iklan"
    And user click "Iklan Nonaktif"
    Then user see Title "Semua Iklan Anda Sudah Naik" with message "Iklan properti Anda akan naik ke posisi yang lebih tinggi pada hasil pencarian." on page "mamiAds"

  @TEST_LIMO-292
  Scenario: Never Purchase MamiAds, Saldo < 5000 and have active ads the first click will redirect to MamiAds Purchase
    Given user fills out owner login as "neverPurchaseMamiadsSaldoBelow5000" and click on Enter button
    Then user verify message "Beli saldo lagi yuk biar posisi iklan tetap naik" in saldo MamiAds
    And user click on owner popup
    When user click on Saldo MamiAds button
    Then user redirected to "owner /mamiads/balance"
    When user click on back button
    And user click "Coba Sekarang"
  #Scenario second click and next will redirect to MamiAds Dashboard (MA-5001)
    And user click on Home menu
    And user click on Saldo MamiAds button
    Then user redirected to "owner /mamiads"

  @TEST_LIMO-298
  Scenario: Ever Purchase MamiAds, Saldo < 5000 and have active ads the first click will redirect to MamiAds Purchase
    Given user fills out owner login as "everPurchaseMamiadsSaldoBelow5000" and click on Enter button
    Then user verify message "Beli saldo lagi yuk biar posisi iklan tetap naik" in saldo MamiAds
    And user click on owner popup
    When user click on Saldo MamiAds button
    Then user redirected to "owner /mamiads/balance"
    When user click on back button
    And user click "Coba Sekarang"
    And user click "Riwayat"
    And user click "Selesai"
    Then user verify status transaction mamiads is "Lunas"
    When user click back button on Riwayat page
    Then user redirected to "owner /mamiads"
    When user click on Home menu
    And user click on Saldo MamiAds button
    Then user redirected to "owner /mamiads"

  @TEST_LIMO-297
  Scenario: Never Purchase MamiAds, Saldo < 5000 and have not active ads the first click will redirect to MamiAds Purchase
    Given user fills out owner login as "neverPurchaseMamiadsHaveNotActiveAds" and click on Enter button
    Then user verify message "Beli saldo lagi yuk biar posisi iklan tetap naik" in saldo MamiAds
    And user click on owner popup
    When user click on Saldo MamiAds button
    Then user redirected to "owner /mamiads/balance"
    When user click on back button
    And user click "Coba Sekarang"
    And user click "Semua Iklan"
  #Scenario Filter aktif when all ads is not yet allocated (MA-5852)
    And user click "Iklan Aktif"
    Then user see Title "Anda Belum Beriklan" with message "Pasang anggaran dan naikkan iklan properti untuk menjangkau lebih banyak penyewa." on page "mamiAds"
    When user click "Riwayat"
    And user click "Selesai"
    Then user see Title "Belum Ada Transaksi" with message "Transaksi yang sudah selesai akan muncul di halaman ini." on page "selesai"
  #Scenario second click and next will redirect to MamiAds Dashboard (MA-5019)
    When user click on Home menu
    And user click on Saldo MamiAds button
    Then user redirected to "owner /mamiads"