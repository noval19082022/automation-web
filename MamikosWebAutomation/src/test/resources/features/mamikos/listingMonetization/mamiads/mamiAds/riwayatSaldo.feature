@regression @LIMO1
Feature: List Riwayat Transaction Account

  @TEST_LIMO-276
  Scenario: List Riwayat Transaction Account - Empty
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "nonpremium" and click on Enter button
    When user navigates to "owner /mamiads"
    #  Scenario: Coba Sekarang - Redirect to MamiAds Landing Page (4095)
    And user click "Coba Sekarang"
    And user see Title "Anda Belum Punya Properti" with message "Daftarkan dulu properti Anda di Mamikos untuk bisa memakai MamiAds." on page "mamiAds"
    And user close pop up on boarding mamiads
    And user click "Riwayat"
    And user see Title "Belum Ada Transaksi" with message "Transaksi yang masih dalam proses akan muncul di halaman ini." on page "dalam proses"
    And user click "Selesai"
    Then user see Title "Belum Ada Transaksi" with message "Transaksi yang sudah selesai akan muncul di halaman ini." on page "selesai"

  @TEST_LIMO-59
  Scenario: To make sure red counter badge if owner have on going transaction
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "premium" and click on Enter button
    Then user navigates to "owner /mamiads"
    When user click "Coba Sekarang"
    Then user redirected to "owner /mamiads"
    When user close pop up on boarding mamiads
    Then user verify count of riwayat before beli saldo
    When user click "Beli Saldo"
    * user choose saldo "Rp6.000"
    Then user verify title "Detail Tagihan"
    When user click "Bayar Sekarang"
    Then validate "Saldo MamiAds" before choose payment method
    When user navigates to "owner /mamiads"
    Then user verify count of riwayat added 1