@regression
Feature: ProPhoto

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @TEST_LIMO-277
  Scenario: Success buy prophoto
    Given user fills out owner login as "premium" and click on Enter button
    When user click "Fitur Promosi"
    And user click "Pro Photo"
    And user verify title Pilih Paket Pro Photo
    And user click Beli on "PRO-PHOTO A"
    Then user verify title "Detail Tagihan"
    And user verify detail tagihan pro photo is "PRO-PHOTO A" with price "1.000.000"
    And user click "Bayar Sekarang"
    Then validate "Paket Prophoto" before choose payment method
    And user select payment method "Kartu Kredit" for "Prophoto"
    And validate transaction Bayar "Paket Prophoto" success with "Kartu Kredit"

    @TEST_LIMO-278
    Scenario: Success ubah prophoto selected
      Given user fills out owner login as "premium" and click on Enter button
      When user click "Fitur Promosi"
      And user click "Pro Photo"
      And user verify title Pilih Paket Pro Photo
      And user click Beli on "PRO-PHOTO A"
      Then user verify title "Detail Tagihan"
      And user verify detail tagihan pro photo is "PRO-PHOTO A" with price "1.000.000"
      And user click "Ubah"
      And user click Beli on "PRO-PHOTO B"
      Then user verify title "Detail Tagihan"
      And user verify detail tagihan pro photo is "PRO-PHOTO B" with price "1.350.000"
      And user click "Bayar Sekarang"
      Then validate "Paket Prophoto" before choose payment method
      And user select payment method "Kartu Kredit" for "Prophoto"
      And validate transaction Bayar "Paket Prophoto" success with "Kartu Kredit"

