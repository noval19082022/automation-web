@regression @LIMO1 @LIMO1-staging
Feature: Quick Allocation on Properti Saya

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @TEST_LIMO-261
  Scenario: Verify ads ON but full occupancy
    Given user fills out owner login as "premium" and click on Enter button
    When user navigates to "owner /mamiads"
    Then user cek status toggle iklan "Kos Jajajadooo Segun Sorong" is "naik"
    And user verify the toggle iklan "Kos Jajajadooo Segun Sorong" is "on"
    And user verify the wording iklan penuh "Kos Jajajadooo Segun Sorong" is "Kamar Penuh. Silahkan nonaktifkan jika tidak ingin menaikkan posisi iklan ini"
    When user navigates to "owner /ownerpage/kos"
    And user clicks button find your kos here
    And input name kos "Kos Jajajadooo Segun Sorong"
    And user clicks kos name from result
    Then user verify the alokasi title is "MamiAds Aktif"
    And user verify the toggle is "on"
    And user verify the wording ads is "Kamar penuh."
    When user click Lihat Selengkapnya button
    Then user verify the alokasi title is "MamiAds Aktif"
    And user verify the toggle is "on"
    And user verify the wording ads is "Kamar penuh."

