@regression @LIMO1
Feature: Ubah Anggaran

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @TEST_LIMO-306
  Scenario Outline: Ubah Anggaran - Saldo 0
    Given user fills out owner login as "<user>" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    And user click "Ubah"
    Then do validation "<validation>" on pop up message is ""
    Examples:
      | user            | validation  |
      | newPhoneOwner   | No Saldo    |

  @TEST_LIMO-311
  Scenario: Ubah Anggaran - Update Batasan to Less Than Min-Max Amount
    Given user fills out owner login as "premium" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    And user click "Ubah"
    And user set anggaran harian to "4999"
    Then do validation message "Mohon masukkan minimum Rp5.000" on form set anggaran
   #Scenario MA-5735
    When user set anggaran harian to "10000001"
    Then do validation message "Maksimum Rp10.000.000" on form set anggaran

  @TEST_LIMO-308 @LIMO1-staging
  Scenario: Ubah Anggaran - Change saldo Maksimal to saldo Maksimal, Iklan its Naik & toggle ON Saldo burning = 0
    Given user fills out owner login as "premiumnewalokasi" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    Then user cek status toggle iklan "kost rane hana Mamitest" is "Naik"
    And user verify the toggle iklan "kost rane hana Mamitest" is "on"
    And user verify the wording iklan "kost rane hana Mamitest" is "Posisi iklan telah naik di hasil pencarian properti."
    And user verify the wording anggaran of iklan "kost rane hana Mamitest" is "Hari ini terpakai Rp0"
    When user click Ubah on iklan "kost rane hana Mamitest"
    And user click "Simpan Pengaturan" button
    Then user verify the toast "Tidak ada perubahan tipe anggaran"
  #Scenario change saldo maksimal to saldo maksimal, if iklan its Tidak Naik and toggle OFF
    When user cek status toggle iklan "Kos rane net Mamitest" is "Tidak Naik"
    Then user verify the toggle iklan "Kos rane net Mamitest" is "off"
    And user verify the wording iklan "Kos rane net Mamitest" is "Klik tombol untuk naikkan iklan"
  #Scenario To make sure wording while iklan Off by owner when set maximal budget and saldo burn = 0 (MA-5815)
    And user verify the wording anggaran of iklan "Kos rane net Mamitest" is "Tipe Anggaran: Saldo Maksimal"
    When user click Ubah on iklan "Kos rane net Mamitest"
    And user click "Simpan Pengaturan" button
    Then user verify the toast "Tidak ada perubahan tipe anggaran"
  #Scenario change daily budged to daily budged, Iklan its Naik & toggle ON saldo burning = 0 (MA-5796)
    When user cek status toggle iklan "kost rane dul Mamitest" is "Naik"
    Then user verify the toggle iklan "kost rane dul Mamitest" is "on"
    And user verify the wording iklan "kost rane dul Mamitest" is "Posisi iklan telah naik di hasil pencarian properti."
    And user verify the wording anggaran of iklan "kost rane dul Mamitest" is "Hari ini Rp0 sudah dipakai dari batas pemakaian saldo Rp20.000"
    When user click Ubah on iklan "kost rane dul Mamitest"
    And user click "Simpan Pengaturan" button
    Then user verify the toast "Tidak ada perubahan tipe anggaran"
  #Scenario change daily budged to daily budged, if iklan its Tidak Naik and toggle OFF
    When user cek status toggle iklan "Kos rane set Mamitest" is "Tidak Naik"
    Then user verify the toggle iklan "Kos rane set Mamitest" is "off"
    And user verify the wording iklan "Kos rane set Mamitest" is "Klik tombol untuk naikkan iklan"
  #Scenario To make sure wording while iklan Off by owner when set daily budget and saldo burn = 0 (MA-5814)
    And user verify the wording anggaran of iklan "Kos rane set Mamitest" is "Tipe Anggaran: Rp70.000 per-hari"
    When user click Ubah on iklan "Kos rane set Mamitest"
    And user click "Simpan Pengaturan" button
    Then user verify the toast "Tidak ada perubahan tipe anggaran"

  @TEST_LIMO-307
  Scenario: Hit Daily Budget is 0, Toggle ON and status Naik, Change to daily budged
      Given user fills out owner login as "premiumpayment" and click on Enter button
      When user navigates to "owner /mamiads"
      And user close pop up on boarding mamiads
      Then user cek status toggle iklan "Raney Hambura" is "Naik"
      And user verify the toggle iklan "Raney Hambura" is "on"
      And user verify the wording iklan "Raney Hambura" is "Posisi iklan telah naik di hasil pencarian properti."
      And user verify the wording anggaran of iklan "Raney Hambura" is "Hari ini Rp0 sudah dipakai dari batas pemakaian saldo Rp10.000"
      When user click Ubah on iklan "Raney Hambura"
      And user set anggaran harian to "5000"
      And user click "Simpan Pengaturan" button
      Then do validation "Daily Budget" on pop up message is "5.000"
      When user click Ya,Ganti button
      Then user verify the toast "Anggaran berhasil diubah"
      And user verify the wording anggaran of iklan "Raney Hambura" is "Hari ini Rp0 sudah dipakai dari batas pemakaian saldo Rp5.000"
  #Scenario: Hit Daily Budget is 0, Toggle ON and status Naik, Change Daily Budget to Saldo Maksimal (MA-5777)
      When user click Ubah on iklan "Raney Hambura"
      And user choose Metode Anggaran "Saldo Maksimal"
      And user click "Simpan Pengaturan" button
      Then do validation "Saldo Maksimal" on pop up message is ""
      When user click Ya,Ganti button
      Then user verify the toast "Anggaran berhasil diubah"
      And user cek status toggle iklan "Raney Hambura" is "Naik"
      And user verify the toggle iklan "Raney Hambura" is "on"
      And user verify the wording iklan "Raney Hambura" is "Posisi iklan telah naik di hasil pencarian properti."
      And user verify the wording anggaran of iklan "Raney Hambura" is "Hari ini terpakai Rp0"
  #Scenario: Hit Maksimal Budget is 0, Toggle ON status Naik, Change Saldo Maksimal to Daily Budget (MA-5780)
      When user click Ubah on iklan "Raney Hambura"
      And user choose Metode Anggaran "Dibatasi Harian"
      And user click "Simpan Pengaturan" button
      Then do validation "Daily Budget" on pop up message is "10.000"
      When user click Ya,Ganti button
      Then user verify the toast "Anggaran berhasil diubah"
      And user cek status toggle iklan "Raney Hambura" is "Naik"
      And user verify the toggle iklan "Raney Hambura" is "on"
      And user verify the wording iklan "Raney Hambura" is "Posisi iklan telah naik di hasil pencarian properti."
      And user verify the wording anggaran of iklan "Raney Hambura" is "Hari ini Rp0 sudah dipakai dari batas pemakaian saldo Rp10.000"

  @TEST_LIMO-309
  Scenario: Hit Daily Budget is 0, Toggle OFF and status Tidak Naik, Change Daily Budget to Saldo Maksimal
      Given user fills out owner login as "premiumpayment" and click on Enter button
      When user navigates to "owner /mamiads"
      And user close pop up on boarding mamiads
      Then user cek status toggle iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Tidak Naik"
      And user verify the toggle iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "off"
      And user verify the wording iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Klik tombol untuk naikkan iklan"
      And user verify the wording anggaran of iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Tipe Anggaran: Rp19.000 per-hari"
      When user click Ubah on iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara"
      And user choose Metode Anggaran "Saldo Maksimal"
      And user click "Simpan Pengaturan" button
      Then do validation "Saldo Maksimal Status Not Active" on pop up message is ""
      When user click Ya,Ganti button
      Then user verify the toast "Anggaran berhasil diubah"
      And user verify the wording anggaran of iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Tipe Anggaran: Saldo Maksimal"
  #Scenario: Hit Maksimal Budget is 0, Toggle OFF status Tidak Naik, Change Saldo Maksimal to Daily Budget (MA-5792)
      When user click Ubah on iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara"
      And user choose Metode Anggaran "Dibatasi Harian"
      And user set anggaran harian to "20000"
      And user click "Simpan Pengaturan" button
      Then do validation "Daily Budget Status Not Active " on pop up message is "20.000"
      When user click Ya,Ganti button
      Then user verify the toast "Anggaran berhasil diubah"
      And user cek status toggle iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Tidak Naik"
      And user verify the toggle iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "off"
      And user verify the wording iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Klik tombol untuk naikkan iklan"
      And user verify the wording anggaran of iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Tipe Anggaran: Rp20.000 per-hari"
  #Scenario: Toggle OFF and status Tidak Naik, Daily Budget Change to Daily Budged (MA-5786)
      When user click Ubah on iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara"
      And user set anggaran harian to "19000"
        And user click "Simpan Pengaturan" button
        Then do validation "Daily Budget Status Not Active " on pop up message is "19.000"
        When user click Ya,Ganti button
        Then user verify the toast "Anggaran berhasil diubah"
        And user cek status toggle iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Tidak Naik"
        And user verify the toggle iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "off"
        And user verify the wording iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Klik tombol untuk naikkan iklan"
        And user verify the wording anggaran of iklan "Kos Ayame Tipe MamiAds Tobelo Halmahera Utara" is "Tipe Anggaran: Rp19.000 per-hari"

  @TEST_LIMO-310 @LIMO1-staging
  Scenario: Success change anggaran when saldo MamiAds < 5000 for Non active ads
    Given user fills out owner login as "everPurchaseMamiadsSaldoBelow5000" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    Then verify the saldo mamiads with condition "<" and value is 5000
    When user click Ubah on iklan "Kos Raney Happyvirus Mamitest Tipe B Tobelo Halmahera Utara"
    And user choose Metode Anggaran "Saldo Maksimal"
    And user click "Simpan Pengaturan" button
    Then do validation "Saldo Maksimal Status Not Active" on pop up message is ""
    When user click Ya,Ganti button
    Then user verify the toast "Anggaran berhasil diubah"
    And user verify the wording anggaran of iklan "Kos Raney Happyvirus Mamitest Tipe B Tobelo Halmahera Utara" is "Tipe Anggaran: Saldo Maksimal"
    When user click Ubah on iklan "Kos Raney Happyvirus Mamitest Tipe B Tobelo Halmahera Utara"
    And user choose Metode Anggaran "Dibatasi Harian"
    And user set anggaran harian to "10000"
    And user click "Simpan Pengaturan" button
    Then do validation "Daily Budget Status Not Active " on pop up message is "10.000"
    When user click Ya,Ganti button
    Then user verify the toast "Anggaran berhasil diubah"
    And user verify the wording anggaran of iklan "Kos Raney Happyvirus Mamitest Tipe B Tobelo Halmahera Utara" is "Tipe Anggaran: Rp10.000 per-hari"
#Scenario: Success change anggaran when saldo MamiAds < 5000 for active ads
    And user click Ubah on iklan "Kos Raney Happyvirus Mamitest Tipe VIP1 Tobelo Halmahera Utara"
    Then do validation "No Saldo" on pop up message is ""
    When user click "Beli Saldo" button
    Then user redirected to "owner /mamiads/balance"