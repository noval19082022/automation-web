@regression
Feature: Pilih Properti

  @MA-4079
  Scenario Outline: Pilih Properti - Search (4079)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "<user>" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    And user click "Naikkan Iklan"
    And user click search box then input keyword "<keyword>" and enter
    Then do validation "<validation>" with filter "<keyword>" on search page
  Examples:
    | user            | keyword                            | validation     |
    | premium         | Olaf Property tipe 12 Segun Sorong | Available      |
    | premium         | Kos Jajajadooo Segun Sorong        | Property Full  |
    | premium         | Unregistered Property              | Not Found      |
    | premium         | Kos Raya Gasim                     | Advertised     |
    | nopropertiavail |                                    | All Advertised |
    | nopropertiavail | Test                               | Cancel Search  |
