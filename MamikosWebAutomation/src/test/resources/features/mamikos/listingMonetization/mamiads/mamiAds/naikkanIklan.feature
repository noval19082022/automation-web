@regression
Feature: Naikkan Iklan

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @MA-4050
  Scenario Outline: Naikkan Iklan - Pop Up Message (4050)
    Given user fills out owner login as "<user>" and click on Enter button
    When user click "Fitur Promosi"
    And user click "MamiAds"
    And user close pop up on boarding mamiads
    And user click "Naikkan Iklan"
    Then do validation "<validation>" on pop up message
    Examples:
      | user            | validation  |
      | nonpremium      | No Property |
      | premiumnosaldo  | No Saldo    |

    @posisiiklan
  Scenario: Posisi Iklan - Toggle Set ON (4092)
    Given user fills out owner login as "premiumnosaldo" and click on Enter button
    When user click "Fitur Promosi"
    And user click "MamiAds"
    And user close pop up on boarding mamiads
    And user click toggle button posisi iklan
    Then do validation "No Saldo" on pop up message


  @onofftoggle
  Scenario: Off toggle posisi iklan by user
    Given user fills out owner login as "premium" and click on Enter button
    When user click on Saldo MamiAds button
    And user close pop up on boarding mamiads

    # Scenario: Switch ON posisi iklan by user
    Then user cek status toggle iklan "kos samsam" is "Tidak Naik"
    And user switch on posisi iklan "kos samsam"
    Then user verify the toast "Iklan berhasil dinaikkan."
    And user cek status toggle iklan "kos samsam" is "Naik"
    Then user verify wording message "kos samsam" is "Posisi iklan telah naik di hasil pencarian properti."

    # Scenario: Switch OFF posisi iklan by user
    And user switch off posisi iklan "kos samsam"
    Then user verify the toast "Iklan berhenti dinaikkan."
    And user cek status toggle iklan "kos samsam" is "Tidak Naik"
    Then user verify wording message "kos samsam" is "Silahkan nyalakan tombol untuk menaikan iklan."

  @MA-4737 @MA-4738 @MA-4047
    Scenario: Naikkan Iklan On Properti Saya (MA-4737)
      Given user fills out owner login as "premium" and click on Enter button
      When user click "Properti Saya"
      And user click on kos menu
      Then user verify Saldo MamiAds
      When user click Naikkan Iklan on properti saya

#     Scenario Get MamiAds onboarding while owner first access mamiads dashboard and click Coba Sekarang MA-407
      And user click "Coba Sekarang"
      Then user redirected to "owner /mamiads"

      # Scenario Naikkan Iklan On Property Saya Apartman (MA-4738)
      When user click "Properti Saya"
      And user clicks on Owner Apartment Menu
      Then user verify Saldo MamiAds
      When user click Naikkan Iklan on properti saya
      Then user verify MamiAds text






