@regression @LIMO1 @LIMO1-staging
Feature: Favorit Rekomendasi

  Background:
    Given user navigates to "mamikos /"
    And user clicks on Enter button

  @TEST_LIMO-305
  Scenario: Tenant never lihat detail properti
    Given user login in as Tenant via phone number as "MA tenant never see detail property"
    When I click Favourite tab
    Then user verify the message is "Belum ada kos yang di favorit."
    When user click "Pernah Dilihat" tab
    Then user verify the message is "Belum ada kos yang dilihat."
    #Scenario: tenant never see detail properti on kos saya (@MA-5292)
    When user click profile on header
    And user click profile dropdown button
    Then user verify already on menu "Kos Saya"
    And user verify the Mulai cari dan sewa kos button
    And user verify the Masukkan kode dari pemilik

  @TEST_LIMO-302
  Scenario: There is no rekomendasi
    Given user login in as Tenant via phone number as "MA tenant no rekomendasi"
    When user clicks search bar
    And I search property with name "Silalay 123" and select matching result to go to kos details page
    And I click Favourite tab
    And user click "Pernah Dilihat" tab
    Then user verify the property with name "Silalay 123" is appear
    And user verify the Hapus History button
    When user click "Difavoritkan" tab
    Then user verify rekomendasi listing section didn't display
    #Scenario: There is no rekomendasi on kos saya (@MA-5291)
    When user click profile on header
    And user click profile dropdown button
    Then user verify already on menu "Kos Saya"
    And user verify rekomendasi listing section didn't display

  @TEST_LIMO-301
  Scenario: There is no rekomendasi booking cancel
    Given user login in as Tenant via phone number as "OB Cancel Tenant"
    When user clicks search bar
    And I search property with name "Kos Upik Merapi Tipe C" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    And user selects T&C checkbox and clicks on Book button
    Then user verify the "Pengajuan sewa berhasil dikirim"
    When user click on Lihat Status Pengajuan
    And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
    Then user verify the message "Booking Anda berhasil dibatalkan" on pop up
    And user verify the button success cancel booking is "Ok"
    When user click Ok button on success cancel booking pop up
    Then user check status booking

  @TEST_LIMO-304 @TEST_LIMO-303
  Scenario Outline: Kos saya page while booking contract is active
    Given user login in as Tenant via phone number as "<user>"
    When user clicks search bar
    And I search property with name "Kos Raney Momogi Tipe A" and select matching result to go to kos details page
    And I click Favourite tab
    And user click "Pernah Dilihat" tab
    Then user verify the property with name "Kos Raney Momogi Tipe A Danurejan Yogyakarta" is appear
    And user verify the Hapus History button
    When user click "Difavoritkan" tab
#   And do validation rekomendasi "<validation>" with maximal <countPerPage> per page and maximal page is <countPage>
    Then user verify rekomendasi listing section is displayed
    And user verify the rekomendasi listing max is 2 page
    And user verify the max 8 rekomendasi listing
    #MA-5289-Make sure rekomendasi on kos saya max 8 properti (MA-5289)
    When user click profile on header
    And user click profile dropdown button
    Then user verify already on menu "Kos Saya"
    And do validation rekomendasi "<validation>" on kos saya page
    And user verify the rekomendasi listing max is <countPerPage> page
    And user verify the max <maxPage> rekomendasi listing
    Examples:
      | user                    | validation    | countPerPage | maxPage |
      | tenant mamiads          | displayed     | 4            | 2       |
      | ob tenant dpst kos saya | not displayed | 0            | 0       |