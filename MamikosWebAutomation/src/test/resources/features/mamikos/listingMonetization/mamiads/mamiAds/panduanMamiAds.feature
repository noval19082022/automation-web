@regression @LIMO1
Feature: Panduan MamiAds

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @TEST_LIMO-286
  Scenario: Open Owner Non GP learn about MamiAds from "MamiAds" screen
    Given user fills out owner login as "newPhoneOwner" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    And user click "Pelajari di Sini"
    Then user redirected to "owner /goldplus/guides/advertisement_usage?redirect=mamikos"

#  Scenario: [Mamiads dashboard][Panduan Mamiads]: View "Panduan Mamiads" screen 4637
    Then validate text "Naikkan Posisi Iklan Properti dengan MamiAds" is available

  @TEST_LIMO-285
  Scenario: Open Owner Non GP learn about MamiAds from "Saldo MamiAds" menu
    Given user fills out owner login as "newPhoneOwner" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    And user click "Pelajari di Sini"
    Then user redirected to "owner /goldplus/guides/advertisement_usage?redirect=mamikos"

  @TEST_LIMO-284
  Scenario: Open Owner GP learn about MamiAds from "GP onboard" screen
    Given user fills out owner login as "premiumGP" and click on Enter button
    And user click on owner popup
    When user click GP Label in Home
    And user click "Pelajari caranya"
    And user click "Naikkan Iklan Anda"
    Then user redirected to "owner /goldplus/guides/advertisement_usage"

#  Scenario:  Back to "MamiAds" screen by tap the "Coba Sekarang" button MA-4689
    And user click "Coba Sekarang"
    Then user redirected to "owner /mamiads"

#  Scenario: Back to "MamiAds" screen by tap the back button 4690
    And user close pop up on boarding mamiads
    And user click "Pelajari di Sini"
    And tap back button on panduan Mamiads.
    Then user redirected to "owner /mamiads"

# Scenario: : Open the next slide inside "Cara Menggunakan MamiAds" section 4691
    And user click "Pelajari di Sini"
    Then validate text "Naikkan Posisi Iklan Properti dengan MamiAds" is available
    And tap "Next" button inside "Cara menggunakan MamiAds".
    Then validate text "Klik “Beli Saldo” pada halaman MamiAds." is available
