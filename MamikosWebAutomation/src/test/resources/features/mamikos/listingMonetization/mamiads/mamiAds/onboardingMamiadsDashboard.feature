@regression @LIMO1
Feature: Onboarding Mamiads Dashboard

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @TEST_LIMO-300
  Scenario: Click coba sekarang on onboarding mamiads dashboard
    Given user fills out owner login as "premium" and click on Enter button
    When user click on owner popup
    And user click on Saldo MamiAds button
    And user click "Coba Sekarang"
    Then user redirected to "owner /mamiads?redirectionSource=Owner%20Dashboard"

# Scenario: owner doesn't get onboarding while owner logout login (4049)
    And user logs out as a Owner user
    And user clicks on Enter button
    And user fills out owner login as "premium" and click on Enter button
    When user click on Saldo MamiAds button
    Then pop up on boarding mamiads disappeared

