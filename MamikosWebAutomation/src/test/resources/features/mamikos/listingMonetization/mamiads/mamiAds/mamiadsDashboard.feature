@regression @LIMO1
Feature: MamiAds Dashboard

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @TEST_LIMO-314
  Scenario: empty state if owner each filter while owner didn't have property
    Given user fills out owner login as "haveNotProperty" and click on Enter button
    When user navigates to "owner /mamiads"
    And user click "Coba Sekarang"
    And user close pop up on boarding mamiads
    Then user can see default filter is "Semua Iklan"
    And user see Title "Anda Belum Punya Properti" with message "Daftarkan dulu properti Anda di Mamikos untuk bisa memakai MamiAds." on page "mamiAds"
    When user click "Semua Iklan"
    And user click "Iklan Aktif"
    Then user see Title "Anda Belum Punya Properti" with message "Daftarkan dulu properti Anda di Mamikos untuk bisa memakai MamiAds." on page "mamiAds"
    When user click "Iklan Aktif"
    And user click "Iklan Nonaktif"
    Then user see Title "Anda Belum Punya Properti" with message "Daftarkan dulu properti Anda di Mamikos untuk bisa memakai MamiAds." on page "mamiAds"

  @TEST_LIMO-320 @TEST_LIMO-313 @LIMO1-staging
  Scenario Outline: Switch ON OFF ads while saldo burn = 0
    Given user fills out owner login as "premium" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    Then user cek status toggle iklan "<adsName>" is "<currentPosisiIklan>"
    And user verify the toggle iklan "<adsName>" is "<currentToggle>"
    #@MA-5814 @MA-5815 @MA-5763 Wording status desc and anggaran desc
    And user verify the wording iklan "<adsName>" is "<currentStatusDesc>"
    And user verify the wording anggaran of iklan "<adsName>" is "<currentAnggaranDesc>"
    When user click "<currentToggle>" toggle the "<adsName>"
    Then user verify the pop up switch "<currentToggle>" toggle iklan "<adsName>" is displayed
    When user click "<actionButton>" button
    Then user verify the toast "<messageToast>"
    And user cek status toggle iklan "<adsName>" is "<expectedPosisiIklan>"
    And user verify the toggle iklan "<adsName>" is "<expectedToggle>"
    And user verify the wording iklan "<adsName>" is "<expectedStatusDesc>"
    And user verify the wording anggaran of iklan "<adsName>" is "<expectedAnggaranDesc>"
    Examples:
      | adsName             | currentPosisiIklan | currentToggle | currentStatusDesc                                    | currentAnggaranDesc                                            | actionButton    | messageToast              | expectedPosisiIklan | expectedToggle | expectedStatusDesc                                   | expectedAnggaranDesc                                           |
      | kos jipyo           | tidak-naik         | off           | Klik tombol untuk naikkan iklan                      | Tipe Anggaran: Saldo Maksimal                                  | Aktifkan        | Iklan berhasil dinaikkan  | naik                | on             | Posisi iklan telah naik di hasil pencarian properti. | Hari ini terpakai Rp0                                          |
      | Kos Upik 449 Tipe A | tidak-naik         | off           | Klik tombol untuk naikkan iklan                      | Tipe Anggaran: Rp15.000 per-hari                               | Aktifkan        | Iklan berhasil dinaikkan  | naik                | on             | Posisi iklan telah naik di hasil pencarian properti. | Hari ini Rp0 sudah dipakai dari batas pemakaian saldo Rp15.000 |
      | kos jipyo           | naik               | on            | Posisi iklan telah naik di hasil pencarian properti. | Hari ini terpakai Rp0                                          | Ya, Nonaktifkan | Iklan berhenti dinaikkan. | tidak-naik          | off            | Klik tombol untuk naikkan iklan                      | Tipe Anggaran: Saldo Maksimal                                  |
      | Kos Upik 449 Tipe A | naik               | on            | Posisi iklan telah naik di hasil pencarian properti. | Hari ini Rp0 sudah dipakai dari batas pemakaian saldo Rp15.000 | Ya, Nonaktifkan | Iklan berhenti dinaikkan. | tidak-naik          | off            | Klik tombol untuk naikkan iklan                      | Tipe Anggaran: Rp15.000 per-hari                               |

  @TEST_LIMO-312 @LIMO1-staging
  Scenario: Set full occupancy and make sure the wording if condition ON OFF
    Given user fills out owner login as "premium" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    Then user cek status toggle iklan "Kos Ranise Mamitest Tobelo Halmahera Utara" is "Naik"
    And user verify the toggle iklan "Kos Ranise Mamitest Tobelo Halmahera Utara" is "on"
    And user verify the wording iklan "Kos Ranise Mamitest Tobelo Halmahera Utara" is "Posisi iklan telah naik di hasil pencarian properti."
    When user navigates to "owner /ownerpage/kos"
    And user clicks button find your kos here
    And input name kos "Kos Ranise Mamitest"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user clicks on update kamar button
    And owner click on edit button on room "Kosong"
    And owner tick on already occupied tickbox and click on update room
    And user waiting the loading screen
    #To make sure wording if ads is ON toggle but full occupancy (MA-5816)
    When user navigates to "owner /mamiads"
    Then user cek status toggle iklan "Kos Ranise Mamitest Tobelo Halmahera Utara" is "Naik"
    And user verify the toggle iklan "Kos Ranise Mamitest Tobelo Halmahera Utara" is "on"
    And user verify the wording iklan penuh "Kos Ranise Mamitest Tobelo Halmahera Utara" is "Kamar Penuh. Silahkan nonaktifkan jika tidak ingin menaikkan posisi iklan ini"
  #Switch OFF ads full occupancy (MA-5817)
    When user click "on" toggle the "Kos Ranise Mamitest Tobelo Halmahera Utara"
    And user click "Ya, Nonaktifkan" button
  #To make sure wording if ads full occupancy (MA-5819)
    Then user verify the wording iklan kamar penuh "Kos Ranise Mamitest Tobelo Halmahera Utara" is "Kamar Penuh"
  #Set available room in property full occupancy
    When user navigates to "owner /ownerpage/kos"
    And user clicks button find your kos here
    And input name kos "Kos Ranise Mamitest"
    And user clicks kos name from result
    And user click Lihat Selengkapnya button
    And user clicks on update kamar button
    And owner click on edit button on room "Terisi"
    And owner tick on already occupied tickbox and click on update room
    And user waiting the loading screen
    When user navigates to "owner /mamiads"
    Then user verify the wording iklan "Kos Ranise Mamitest Tobelo Halmahera Utara" is "Klik tombol untuk naikkan iklan"
    When user click "off" toggle the "Kos Ranise Mamitest Tobelo Halmahera Utara"
    And user click "Aktifkan" button

  @TEST_LIMO-317 @LIMO1-staging
  Scenario: See ads on filter nonaktif
    Given user fills out owner login as "premiumpayment" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    Then user can see default filter is "Semua Iklan"
    And ads list rooms as expected
      | adsName                                       | posisiIklan | currentToggle | availRoom                                                                     | currentStatusDesc                                    |
      | Kos Khalif Automation                         | Tidak Naik  | off           | -                                                                             | Klik tombol untuk naikkan iklan                      |
      | Kos Ayame Tipe MamiAds Tobelo Halmahera Utara | Tidak Naik  | off           | -                                                                             | Klik tombol untuk naikkan iklan                      |
      | Kos Ayame Tipe Umo Tobelo Halmahera Utara     | Tidak Naik  | off           | -                                                                             | Klik tombol untuk naikkan iklan                      |
      | Kos Ayame Tipe Mami Tobelo Halmahera Utara    | Naik        | on            | -                                                                             | Posisi iklan telah naik di hasil pencarian properti. |
      | Raney Hambura                                 | Naik        | on            | -                                                                             | Posisi iklan telah naik di hasil pencarian properti. |
      | MamiAds Ham                                   | Naik        | on            | Kamar Penuh. Silahkan nonaktifkan jika tidak ingin menaikkan posisi iklan ini | -                                                    |
      | MamiAds Bura                                  | Kamar Penuh | -             | -                                                                             | -                                                    |
    When user click "Semua Iklan"
    And user click "Iklan Aktif"
    Then ads list rooms as expected
      | adsName                                    | posisiIklan | currentToggle | availRoom                                                                     | currentStatusDesc                                    |
      | Kos Ayame Tipe Mami Tobelo Halmahera Utara | Naik        | on            | -                                                                             | Posisi iklan telah naik di hasil pencarian properti. |
      | MamiAds Ham                                | Naik        | on            | Kamar Penuh. Silahkan nonaktifkan jika tidak ingin menaikkan posisi iklan ini | -                                                    |
      | Raney Hambura                              | Naik        | on            | -                                                                             | Posisi iklan telah naik di hasil pencarian properti. |
    When user click "Iklan Aktif"
    And user click "Iklan Nonaktif"
    Then ads list rooms as expected
      | adsName                                       | posisiIklan | currentToggle | availRoom | currentStatusDesc               |
      | Kos Khalif Automation                         | Tidak Naik  | off           | -         | Klik tombol untuk naikkan iklan |
      | Kos Ayame Tipe MamiAds Tobelo Halmahera Utara | Tidak Naik  | off           | -         | Klik tombol untuk naikkan iklan |
      | Kos Ayame Tipe Umo Tobelo Halmahera Utara     | Tidak Naik  | off           | -         | Klik tombol untuk naikkan iklan |
      | MamiAds Bura                                  | Kamar Penuh | -             | -         | -                               |

  @TEST_LIMO-315
  Scenario: Switch ON and ubah anggaran the ads never allocate if saldo mamiads sufficient
    Given user fills out owner login as "premiumsaldo" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    Then user cek status toggle iklan "Kos Never Allocate Tobelo Halmahera Utara" is "tidak-naik"
    #MA-5766 to make sure the ads never allocate
    And user verify the toggle iklan "Kos Never Allocate Tobelo Halmahera Utara" is "off"
    And user verify the wording iklan "Kos Never Allocate Tobelo Halmahera Utara" is "Klik tombol untuk naikkan iklan"
    And user verify the wording anggaran of iklan "Kos Never Allocate Tobelo Halmahera Utara" is "Tipe Anggaran: Rp10.000 per-hari"
    When user click "off" toggle the "Kos Never Allocate Tobelo Halmahera Utara"
    Then user verify the pop up switch "off" toggle iklan "Kos Never Allocate Tobelo Halmahera Utara" is displayed
    When user click "Batal" button
    And user cek status toggle iklan "Kos Never Allocate Tobelo Halmahera Utara" is "tidak-naik"
    And user verify the toggle iklan "Kos Never Allocate Tobelo Halmahera Utara" is "off"
    And user verify the wording iklan "Kos Never Allocate Tobelo Halmahera Utara" is "Klik tombol untuk naikkan iklan"
    And user verify the wording anggaran of iklan "Kos Never Allocate Tobelo Halmahera Utara" is "Tipe Anggaran: Rp10.000 per-hari"
    #MA-5882 To make sure redirect to form anggaran while click ubah the ads never allocate
    When user click Ubah on iklan "Kos Never Allocate Tobelo Halmahera Utara"
    Then user verify the form anggaran is displayed
    When user click icon close

  @TEST_LIMO-319 @LIMO1-staging
  Scenario: To make sure wording while iklan ON toggle and already reach daily budget
    Given user fills out owner login as "premiumnewalokasi" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    Then user cek status toggle iklan "Kos raney chan mamitest" is "Tidak Naik"
    And user verify the toggle iklan "Kos raney chan mamitest" is "on"
    And user verify the wording iklan "Kos raney chan mamitest" is "Anggaran harian telah terpenuhi untuk hari ini dan akan naik kembali besok."

  @TEST_LIMO-318 @LIMO1-staging
  Scenario: Switch ON and ubah anggaran the ads never allocate if saldo mamiads insufficient
    Given user fills out owner login as "everPurchaseMamiadsSaldoBelow5000" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    Then verify the saldo mamiads with condition "<" and value is 5000
    And user cek status toggle iklan "Kos Never Allocate Tipe Insufficient Tobelo Halmahera Utara" is "tidak-naik"
    And user verify the toggle iklan "Kos Never Allocate Tipe Insufficient Tobelo Halmahera Utara" is "off"
    And user verify the wording iklan "Kos Never Allocate Tipe Insufficient Tobelo Halmahera Utara" is "Klik tombol untuk naikkan iklan"
    And user verify the wording anggaran of iklan "Kos Never Allocate Tipe Insufficient Tobelo Halmahera Utara" is "Tipe Anggaran: Rp10.000 per-hari"
    When user click "off" toggle the "Kos Never Allocate Tipe Insufficient Tobelo Halmahera Utara"
    Then user verify the pop up switch "off" toggle iklan "Kos Never Allocate Tobelo Halmahera Utara" is displayed
    And do validation "On Toggle Saldo Less Than 5000 Never Allocate" on pop up message is ""
    When user click "Beli Saldo" button
    #scenario: ubah anggaran the ads never allocate when saldo insufficient (MA-5992)
    And user click back arrow button
    Then user redirected to "owner /mamiads"
    When user click Ubah on iklan "Kos Never Allocate Tipe Insufficient Tobelo Halmahera Utara"
    Then do validation "No Saldo" on pop up message is ""
    When user click "Beli Saldo" button
    And user click back arrow button
    Then user redirected to "owner /mamiads"
    #Scenario: Switch toggle ON, when saldo is < 5000 on property ever allocate saldo (MA-6015)
    When user cek status toggle iklan "Kos Raney Happyvirus Mamitest Tipe A Tobelo Halmahera Utara" is "tidak-naik"
    Then user verify the toggle iklan "Kos Raney Happyvirus Mamitest Tipe A Tobelo Halmahera Utara" is "off"
    And user verify the wording iklan "Kos Raney Happyvirus Mamitest Tipe A Tobelo Halmahera Utara" is "Klik tombol untuk naikkan iklan"
    And user verify the wording anggaran of iklan "Kos Raney Happyvirus Mamitest Tipe A Tobelo Halmahera Utara" is "Tipe Anggaran: Saldo Maksimal"
    When user click "off" toggle the "Kos Raney Happyvirus Mamitest Tipe A Tobelo Halmahera Utara"
    Then do validation "On Toggle Saldo Less Than 5000 Never Allocate" on pop up message is ""
    When user click "Beli Saldo" button
    Then user redirected to "owner /mamiads/balance"

  @TEST_LIMO-316 @LIMO1-staging
  Scenario: Owner want to see Semua Iklan and saldo mamiads insufficient
    Given user fills out owner login as "saldomamiadsinsufficient" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    Then user can see default filter is "Semua Iklan"
    And ads list rooms as expected
      | adsName                                      | posisiIklan | currentToggle | currentStatusDesc                                 | currentStatusSaldo               |
      | Kos Caye Raney Tipe B Tobelo Halmahera Utara | Tidak Naik  | off           | Saldo MamiAds tidak mencukupi. Silahkan beli lagi | Tipe Anggaran: Saldo Maksimal    |
      | Kos Caye Raney Tipe C Tobelo Halmahera Utara | Tidak Naik  | off           | Saldo MamiAds tidak mencukupi. Silahkan beli lagi | Tipe Anggaran: Rp30.000 per-hari |
      | Kos Caye Raney Tipe A Tobelo Halmahera Utara | Tidak Naik  | off           | Saldo MamiAds tidak mencukupi. Silahkan beli lagi | Tipe Anggaran: Rp10.000 per-hari |
      | Kos Caye Raney Tipe E Tobelo Halmahera Utara | Kamar Penuh | -             | -                                                 | -                                |
    When user click "Semua Iklan"
    And user click "Iklan Aktif"
    Then user see Title "Anda Belum Beriklan" with message "Pasang anggaran dan naikkan iklan properti untuk menjangkau lebih banyak penyewa." on page "mamiAds"