@regression @mamiads
Feature: Cek Properti Sekitar

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @MA-4098
  Scenario: Cek Properti Sekitar (4098)
    Given user fills out owner login as "nonpremium" and click on Enter button
    When user navigates to "owner /cek-properti-sekitar"
    Then user see Title "Buka Cek Properti Sekitar di Aplikasi" with message "Untuk saat ini, fitur Cek Properti Sekitar hanya dapat digunakan di aplikasi Mamikos di Android dan iOS." on page "cek properti sekitar"