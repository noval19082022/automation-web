@regression @LIMO1 @LIMO1-staging
Feature: Beli Saldo

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @TEST_LIMO-274 @belisaldo
  Scenario: To make sure redirect to mamiads dashboard if tab Selesai button
    Given user fills out owner login as "premium" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    And user click "Beli Saldo"
    * user wait Pilih Saldo buttons are loaded

#   Scenario: Favorite Saldo 4086
    And favorit saldo is "Rp1.350.000"

#   Scenario: List Promo Saldo 4087
    And detail list saldo as expected
      | price   | priceInRp   | disc | discPrice   |
      | 6.000    | Rp6.000     |      |             |
      | 30.000   | Rp27.000    |  10% | Rp30.000    |
      | 50.000   | Rp50.000    |      |             |
      | 80.000   | Rp75.000    | 6%   | Rp80.000    |
      | 205.000  | Rp205.000   |      |             |
      | 300.000  | Rp276.000   | 8%   | Rp300.000   |
      | 850.000  | Rp850.000   |      |             |
      | 1.000.000 | Rp925.000   | 7%   | Rp1.000.000 |
      | 1.500.000 | Rp1.350.000 | 10%  | Rp1.500.000 |
      | 5.000.000 | Rp4.500.000 | 10%  | Rp5.000.000 |

#   Scenario: Change Saldo 4091
    Given user choose saldo "Rp27.000"
    And user click "Ubah"
    And user choose saldo "Rp6.000"
    Then validate detail tagihan

#   Scenario: Beli Saldo - Before Choose Payment Method (2664)
    And user click "Bayar Sekarang"
    Then validate "Saldo MamiAds" before choose payment method

#   Scenario: Beli Saldo - After Choose Payment Method (2678)
    And user select payment method "Kartu Kredit" for "Saldo MamiAds"

#   Scenario: Beli Saldo - Transaction Success (4089)
    And validate transaction Bayar "Saldo MamiAds" success with "Kartu Kredit"

#   Scenario: Beli Saldo - Click Selesai Button (4721)
    And user click "Selesai"
    Then user redirected to owner's mamiads dashboard
    And user click "Riwayat"
    And user click "Selesai"
    And validate status transaction mamiads is "Lunas" with price "Rp6.000"

  @TEST_LIMO-275 @belisaldo
  Scenario: Beli Saldo (2nd Transaction)
    Given user fills out owner login as "premium" and click on Enter button
    When user navigates to "owner /mamiads"
    And user close pop up on boarding mamiads
    And user click "Beli Saldo"
    And user choose saldo "Rp6.000"
    And user click "Bayar Sekarang"
    Then validate "Saldo MamiAds" before choose payment method
    And user select payment method "Kartu Kredit" for "Saldo MamiAds"
    And validate transaction Bayar "Saldo MamiAds" success with "Kartu Kredit"

  @TEST_LIMO-273
  Scenario: Cancel Buy Saldo
    Given user fills out owner login as "nonpremium" and click on Enter button
    When user navigates to "owner /mamiads"
#  Scenario: Coba Sekarang - Redirect to MamiAds Landing Page (4095)
    And user click "Coba Sekarang"
    Then user redirected to "owner /mamiads"
    And user close pop up on boarding mamiads
    And user click "Beli Saldo"
    And user choose saldo "Rp6.000"
    Then user verify title "Detail Tagihan"
    And user verify Bayar button is appear
    And user click back button
    And user click "MamiAds"
    And user click "Riwayat"
    And user see Title "Belum Ada Transaksi" with message "Transaksi yang masih dalam proses akan muncul di halaman ini." on page "dalam proses"
    And user click "Selesai"
    Then user see Title "Belum Ada Transaksi" with message "Transaksi yang sudah selesai akan muncul di halaman ini." on page "selesai"

  @TEST_LIMO-191
  Scenario: Buy saldo mamiAds using voucher
    Given user fills out owner login as "premium" and click on Enter button
    When user navigates to "owner /mamiads"
    * user click "Coba Sekarang"
    Then user redirected to "owner /mamiads"
    When user close pop up on boarding mamiads
    * user click "Beli Saldo"
    * user choose saldo "Rp6.000"
    Then user verify title "Detail Tagihan"
    When user click "Bayar Sekarang"
    Then validate "Saldo MamiAds" before choose payment method
    When user click "Masukkan"
    Then user verify "Voucher Anda" is displayed
    When user click "Masukkan" on Punya kode voucher?
    * user input "MAATSINGLEVOUCHER1" as kode voucher
    * user click Pakai button
    Then user verify the toast "Voucher Dipakai"
    * validate "Saldo MamiAds" after apply voucher "MAATSINGLEVOUCHER1"
    When user click on Bayar Sekarang button
    Then validate transaction "Saldo MamiAds" after success with "MAATSINGLEVOUCHER1" voucher
    When user click "Selesai"
    Then user redirected to owner's mamiads dashboard
    When user click "Riwayat"
    * user click "Selesai"
    Then validate status transaction mamiads is "Lunas" with price "Rp0"