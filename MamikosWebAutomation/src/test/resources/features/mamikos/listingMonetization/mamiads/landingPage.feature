@regression @LIMO1
Feature: MamiAds Landing Page

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"

#  @MA-4094 @MA-4096 @MA-4725 @MA-4095 @MA-4048
#  Scenario: Owner Dashboard - Info untuk Anda - Redirect to about MamiAds (4094)
#    Given user clicks on Enter button
#    When user fills out owner login as "premium" and click on Enter button
#    And user click on owner popup
#    And user click "Ingin menaikkan posisi iklan Anda? Cari tahu tentang MamiAds di sini"
#    Then user redirected to "/mamiads?prevPage=dashboard"
#
##  Scenario: Promosikan iklan Anda - Redirect to MamiAds Landing Page (4096)
#    And user click "Promosikan Iklan Anda"
#    Then user redirected to "/mamiads"
#
##  Scenario: Coba Sekarang - Redirect to MamiAds Landing Page (4095)
#    And user click "Coba Sekarang"
#    Then user redirected to "owner /mamiads"
#
##  Scenario: Close (X) on Onboarding Pop Up (4048)
#    And user close pop up on boarding mamiads
#    And user click back button on device
#    Then user redirected to "/mamiads"

  #Non Login as owner
  @TEST_LIMO-282
  Scenario: [Non-Login][Landing Page] Non Login - Redirect to MamiAds
    Given user click "Promosikan Iklan Anda"
    When user click "Coba Sekarang"
    And user fills out owner login as "premium" and click on Enter button
    And user click on owner popup
    Then user redirected to "owner page"
	#login or not login owner
  @TEST_LIMO-280
  Scenario: [MamiAds][Landing MamiAds] Make sure Accordion sole focus on one answer on Tanya Jawab
    Given user click "Promosikan Iklan Anda"
    When user verify the wording is "Tanya Jawab" and "Pertanyaan yang sering ditanyakan"
    And user click question "Bagaimana cara menaikkan Iklan menggunakan MamiAds?"
    Then user verify answer text "Untuk dapat menggunakan fitur MamiAds untuk menaikkan iklan, Anda cukup membeli Saldo MamiAds dan menganggarkannya ke iklan Anda."
    When user click question "Apa itu Saldo MamiAds?"
    Then user verify answer text "Saldo MamiAds adalah saldo yang digunakan untuk mengiklankan kos Anda agar dapat menjangkau lebih banyak pencari kos."
    When user click question "Dari mana saya dapat mengakses MamiAds?"
    Then user verify answer text "Anda dapat mengakses MamiAds lewat website maupun aplikasi Mamikos."
