@regression @LIMO2 @listing-monetization
Feature: Owner who has controlled room property

  @TEST_LIMO-1686 @controlledRoom
  Scenario: Make sure Owner who has controlled room property (APIK, Elite Plus, Elite Pre-Buy, Elite Deposit, Singgah Sini
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner gp 1" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And owner search "bukannya fathul ya" and click see complete button
    And user click button atur promo
    Then user see page title is "Kelola Promo" on Promo Page
    And user see status promo is "Belum Ada"