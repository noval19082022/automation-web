@regression @promoIklan @LIMO2 @listing-monetization
Feature: Owner Set Promo Kost

  @TEST_LIMO-1710 @gpOwnerPromo
  Scenario: Make sure GP owner will redirection on Promo iklan page
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner gp 1" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And owner search "bukannya fathul ya" and click see complete button
    And user click button atur promo
    Then user see page title is "Kelola Promo" on Promo Page

  @TEST_LIMO-1711
  Scenario: Make sure Non GP owner will redirection on Onboarding GP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner non gp" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And owner search "Dupe Kost Sapphire Bintaro Plaza Tipe B Pondok Aren Tangerang Selatan" and click see complete button
    And user click button atur promo
    Then user redirect to List Package
