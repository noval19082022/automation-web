@regression @goldPlus @goldplusRegister @LIMO2 @listing-monetization
Feature: Gold Plus

  Background:
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "limo not gp" and click on Enter button
    And user click on owner popup

   @TEST_LIMO-151 @chooseGP
  Scenario: Owner choose gold plus 1
    Given user click Daftar GoldPlus field
    When user redirect to List Package
    And user click pilih paket on "Goldplus 1"
    Then user redirect to Detail Tagihan
    And user see total tagihan is "59000"

  @TEST_LIMO-146 @changeGP
  Scenario: Owner can change package gold plus
    Given user click Daftar GoldPlus field
    When user redirect to List Package
    And user click pilih paket on "Goldplus 2"
    And user click on ubah package gold plus button
    And user click pilih paket on "Goldplus 1"
    Then user redirect to Detail Tagihan
    And user see total tagihan is "59000"

  @TEST_LIMO-148 @uncheckT&C
  Scenario: Owner Cek warning toast and button bayar sekarang disable if uncheck the checkbox
    Given user click Daftar GoldPlus field
    When user redirect to List Package
    And user click pilih paket on "Goldplus 1"
    And user redirect to Detail Tagihan
    And user uncheck checkbox Syarat dan Ketentuan
    Then system display button Bayar Sekarang is disable

  @TEST_LIMO-150 @checkBenefit
  Scenario: Make sure detail Check Other Benefits on GP1/2
    Given user click Daftar GoldPlus field
    When user redirect to List Package
    And user click lihat detail manfaat GP1
    Then system showing popup detail manfaat GP1
    And system display button pilih paket goldplus

  @TEST_LIMO-149 @checkAndUncheckT&C
  Scenario: Owner Cek syarat dan ketentuan before uncheck and check warning toast and button bayar sekarang disable if uncheck the checkbox
    Given user click Daftar GoldPlus field
    When user redirect to List Package
    And user click pilih paket on "Goldplus 2"
    And user redirect to Detail Tagihan
    And user uncheck checkbox Syarat dan Ketentuan
    And system display button Bayar Sekarang is disable
    And user check checkbox Syarat dan Ketentuan
    Then system display button Bayar Sekarang is enable