@regression @goldPlus @LIMO2 @listing-monetization
Feature: Page Gold Plus Aktiv

  Background:
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "owner gp 1" and click on Enter button
    And user click on owner popup


  @statusPaketGP
  Scenario: Filter Page Paket GoldPlus Anda
    Given user click mamikos goldplus label
    When user click Lihat Selengkapnya kos goldplus anda
    Then user see page title is "Paket GoldPlus Anda" on paket goldplus Page
    And user see status goldplus is "Goldplus 1 Aktif" on tab semua paket gold plus anda

  @TEST_LIMO-36 @checkAllFilter
  Scenario: Check all Filter Page Paket GoldPlus Anda (Semua, Aktif, Menunggu Pembayaran Sedan Diproses)
    Given user click mamikos goldplus label
    When user click Lihat Selengkapnya kos goldplus anda
    Then user see page title is "Paket GoldPlus Anda" on paket goldplus Page
    And user see status goldplus is "Goldplus 1 Aktif" on tab semua paket gold plus anda
    When user click tab "Aktif" on page paket goldplus anda
    Then user see status goldplus is "Goldplus 1 Aktif" on tab semua paket gold plus anda
    When user click tab "Menunggu pembayaran" on page paket goldplus anda
    Then user see status goldplus is "Kos GoldPlus Tidak Ditemukan" on tab sedang diproses paket gold plus anda
    When user click tab "Sedang Diproses" on page paket goldplus anda
    Then user see status goldplus is "Kos GoldPlus Tidak Ditemukan" on tab sedang diproses paket gold plus anda

  @TEST_LIMO-40 @labelPropertyTerdaftar
  Scenario: Count Property terdaftar GP
    When user click mamikos goldplus label
    Then user see text "Properti Terdaftar GoldPlus 1" on page goldplus anda

  @TEST_LIMO-49
  Scenario: Check list tagihan already paid
    Given user click mamikos goldplus label
    And user scroll down homepage
    And user click Lihat selengkapnya at section pembayaran tagihan
    And user click tab selesai
    Then user see list detail tagihan already paid

  @TEST_LIMO-48
  Scenario: Check pop up upgrade from GP1 to GP2
    Given user click mamikos goldplus label
    And user see box black with text "Maksimalkan marketing kos Anda dengan fitur GoldPlus 2 Upgrade GoldPlus 2"
    And user click button "Upgrade Goldplus 2" at box black
    And user see pop up upgrage goldplus dua
    And user click button upgrade goldlplus dua at pop up
    Then user redirected to "upgrade gp"

      #scenario check upgrade from section property terdaftar
    And user click back detail tagihan
    And user click lihat selengkapnya at section property terdaftar
    And user click button close pop up upgrade goldplus dua
    And user click button "Upgrade Goldplus 2" at section goldplus list
    And user see pop up upgrage goldplus dua
    And user click button upgrade goldlplus dua at pop up
    Then user redirected to "upgrade gp"
