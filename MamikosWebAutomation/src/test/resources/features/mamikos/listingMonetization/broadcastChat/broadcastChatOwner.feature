@listing-monetization @regression @LIMO2
Feature: Broadcast Chat Owner

	#{{ User GP2 click Broadcast Chat Entry Point From Fitur Promosi Page}}
	@TEST_LIMO-1211 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat Owner][Chat][Fitur Promosi Page][User GP 2]click Broadcast Chat entry point in Kelola Page
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login as "owner gp 2" and click on Enter button
		    And user click on owner popup
		    And user click "Fitur Promosi"
		    And user click "Broadcast Chat"
		    Then user redirected to "owner /broadcast-chat"

	#User No Kost Acktive click Broadcast Chat Entry Point From Fitur Promosi Page
	@TEST_LIMO-1530 @Broadcast-chat @automated @listing-monetization @web
	Scenario: [Broadcast Chat Owner][Chat][Fitur Promosi Page]User doesnt have kost active then accsess menu broadcast chat
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login as "owner Doesnt HaveActive Kost" and click on Enter button
		    And user navigates to "owner /broadcast-chat"
		    Then user verify pop up message "Anda belum memiliki kos aktif" is appear

	#{{User GP1 click Broadcast Chat Entry Point From Fitur Promosi Page}}
	@TEST_LIMO-1212 @Broadcast-chat @GP1 @automated @listing-monetization @web
	Scenario: [Broadcast Chat Owner][Chat][Fitur Promosi Page]User GP 1 click menu Broadcast Chat
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login as "owner gp 1" and click on Enter button
		    And user navigates to "owner /broadcast-chat"
		    Then user see button Lihat Detail Paket on Broadcast Chat Page is present
		    And user see button Ajukan Ganti Paket on Broadcast Chat Page is present

	#User reguler click Broadcast Chat Entry Point From Fitur Promosi page
	@TEST_LIMO-1210 @Broadcast-chat @automated @listing-monetization @web
	Scenario: [Broadcast Chat Owner][Fitur Promosi]User non GP with active kost click menu Broadcast Chat
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login as "notGP" and click on Enter button
		    And user navigates to "owner /broadcast-chat"
		    Then user see button Lihat Detail Paket on Broadcast Chat Page is present
		    And user see button Beli Paket on Broadcast Chat Page is present

	@TEST_LIMO-1209 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat Owner][Chat][Fitur Promosi Page]User GP 2 has an active recurring invoice click menu Broadcast Chat
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login as "lastDayGP" and click on Enter button
		    And user click on owner popup
		    And user click "Fitur Promosi"
		    And user click "Broadcast Chat"
		    Then user redirected to "owner /broadcast-chat"


    #{{User click Broadcast Chat Entry Point From Chat Page}}
	@TEST_LIMO-1208 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat Owner][Chat][User GP 2]click Broadcast Chat entry point in Chat Page
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user click on owner popup
		And user click chat button in top bar
		And the user click broadcast chat entry point
		Then user redirected to "owner /broadcast-chat?redirection_source=Halaman%20Chat"


	#Pre-condition: *
	#
	#* User login with owner already activated list
	#* User already finish onboarding slide Broadcast Chat
	@TEST_LIMO-1207 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat][Rincian Pesan]user want to see detail of status terkirim
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user navigates to "owner /broadcast-chat"
		And user click on Button Lihat Rincian Broadcast Chat
		Then user see Label Pesan BroadcastChat Terkirim is Present


	#Pre-condition: *
	#
	#* User login with owner already activated list
	#* User already finish onboarding slide Broadcast Chat
	@TEST_LIMO-1205 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat][Rincian Pesan]user want to click button baca selengkapnya && user can see alert kost dont have receipent
		# Scenario: [Broadcast Chat][Rincian Pesan]user want to click button baca selengkapnya
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user navigates to "owner /broadcast-chat"
		And user click "Baca selengkapnya"
		* tenant switch tab to "2"
		And user redirected to "bc selengkapnya"

		#  Scenario:[Broadcast Chat][Create Broadcast chat]User want to send broadcast chat for kost without have a recipient
		* tenant switch tab to "1"
		And user click on Button Tambah Broadcast Chat
		And user enter text "Kos Fathul Khair Tipe bala bala Jetis Yogyakarta" on BC list kos
		And user click "Kos Fathul Khair Tipe bala bala Jetis Yogyakarta"
		And user click button pilih kost
		Then user will see alert the kost dont have a recipient

		#User login with owner already activated list
	@TEST_LIMO-1202  @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat][Fitur Promosi][Bantuan & Tips]User click Bantuan & Tips at Chat Page
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user navigates to "owner /broadcast-chat"
		And user click on Button Tambah Broadcast Chat
		And user click on Button Bantuan Tips
		Then user redirected to "broadcast chat faq"


	#Pre-condition: *
	#
	#* User login with owner already activated list
	#* User already finish onboarding slide Broadcast Chat
	@TEST_LIMO-1203 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat][Select Kost]User Search kost with condition full room
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user navigates to "owner /broadcast-chat"
		And user click on Button Tambah Broadcast Chat
		And user enter text "Kos Fathul Khair Tipe Gehu Jetis Yogyakarta" on BC list kos
		Then user verify "Kos Fathul Khair Tipe Gehu Jetis Yogyakarta" is disable


	#Pre-condition: *
	#
	#* User login with owner already activated list
	#* User already finish onboarding slide Broadcast Chat
	@TEST_LIMO-1201 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat][View Receiver]user want to back from page view receiver
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user navigates to "owner /broadcast-chat"
		And user click on Button Tambah Broadcast Chat
		And user enter text "Kos Fathul Khair Jetis Yogyakarta" on BC list kos
		And user click "Kos Fathul Khair Jetis Yogyakarta"
		And user click Pilih Kost Button
		And user click Masukan Pesan button
		And user selects first message option
		And user click Pilih Pesan button
		And user click back arrow button on BC page
		And user click Tidak Jadi button on BC pop up
		And user click back arrow button on BC page
		And user click Keluar button on BC pop up
		Then user redirected to "owner /broadcast-chat/kos"


	#Pre-condition: *
	#
	#* User login with owner already activated list
	#* User already finish onboarding slide Broadcast Chat
	@TEST_LIMO-1192 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat][Select Kost]User search kost name in search field with invalid name
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user navigates to "owner /broadcast-chat"
		And user click on Button Tambah Broadcast Chat
		And user enter text "kost asal ga nemu" on BC list kos
		Then user see invalid search kost "Properti Tidak Ditemukan"


	#Pre-condition: *
	#
	#* User login with owner already activated list
	#* User already finish onboarding slide Broadcast Chat
	@TEST_LIMO-1189 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat][Select Message]User change message template on the list
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user navigates to "owner /broadcast-chat"
		And user click on Button Tambah Broadcast Chat
		And user enter text "Kos Fathul Khair Jetis Yogyakarta" on BC list kos
		And user click "Kos Fathul Khair Jetis Yogyakarta"
		And user click Pilih Kost Button
		And user click Masukan Pesan button
		And user selects first message option
		And user click Pilih Pesan button
		And user click Ubah Button on BC Page
		And user selects second message BC option
		And user click Pilih Pesan button
		Then user verify BC Textfield is not visible


	#Pre-condition: *
	#
	#* User login with owner already activated list
	#* User already finish onboarding slide Broadcast Chat
	@TEST_LIMO-1157 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario Outline: [Broadcast Chat][Create Broadcast chat]User want to input phone number/email/link on template message is editable
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user navigates to "owner /broadcast-chat"
		And user click on Button Tambah Broadcast Chat
		And user enter text "Kos Fathul Khair Jetis Yogyakarta" on BC list kos
		And user click "Kos Fathul Khair Jetis Yogyakarta"
		And user click Pilih Kost Button
		And user click Masukan Pesan button
		And user selects first message option
		And user click Pilih Pesan button
		And user input "<BC Messages>" on Broadcast Message
		And user click Preview Pesan Button
		Then user see "Mohon untuk tidak mengisi nomor handphone/email/link" under BC textfield
		Examples:
			|BC Messages|
			|Kirim email ke mamattheend@mail.com|
			|save no wa 08335005252|
			|foto kamar dan layanan ada di bit.ly/33xyYVH|


	#Pre-condition: *
	#
	#* User login with owner already activated list
	#* User already finish onboarding slide Broadcast Chat
	@TEST_LIMO-1145 @Broadcast-chat @GP2 @automated @listing-monetization @web
	Scenario: [Broadcast Chat][Create Broadcast chat]user want to back fro preview message menu
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "owner gp 2" and click on Enter button
		And user navigates to "owner /broadcast-chat"
		And user click on Button Tambah Broadcast Chat
		And user enter text "Kos Fathul Khair Jetis Yogyakarta" on BC list kos
		And user click "Kos Fathul Khair Jetis Yogyakarta"
		And user click Pilih Kost Button
		And user click Masukan Pesan button
		And user selects first message option
		And user click Pilih Pesan button
		And user input "Kopi Kapal Api dan Udud Jarcok Filter" on Broadcast Message
		And user click Preview Pesan Button
		Then user see "Kopi Kapal Api dan Udud Jarcok Filter" on Preview Broadcast Message
