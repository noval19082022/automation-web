@regression @LIMO1 @LIMO1-staging
Feature: Voucher

  Background: Login Page Mamikos
    Given user navigates to "mamikos /"
    When user clicks on Enter button

 @TEST_LIMO-193 @TEST_LIMO-177 @TEST_LIMO-175 @TEST_LIMO-187 @TEST_LIMO-181 @TEST_LIMO-178
 Scenario Outline: Apply invalid Voucher
    Given user fills out owner login as "premium" and click on Enter button
    When user navigates to "owner /mamiads"
    And user click "Coba Sekarang"
    Then user redirected to "owner /mamiads"
    And user close pop up on boarding mamiads
    * user click "Riwayat"
    * user click on the "1" transaction
    * user switch to 2 window
    * user click "Masukkan"
    Then user verify "Voucher Anda" is displayed
    When user click "Masukkan" on Punya kode voucher?
    * user input "<voucher code>" as kode voucher
    * user click Pakai button
    Then validate the warning "<warning message>"
    When user clear the voucher code
    And user click on icon close
    Examples:
#    1. Voucher doesn't active (LIMO-193)
#    2. Input invalid voucher code (LIMO-177)
#    3. Input empty voucher (LIMO-175)
#    4. Doens't buy minimal saldo the voucher (LIMO-187)
#    5. Voucher alredy expired (LIMO-181)
#    6. Quota voucher is 0 (LIMO-178)
      | voucher code | warning message |
      | MAATNOTACTIVEVOUCHER | Kode voucher tidak bisa digunakan.  |
      | MAATNOTACTIVEVOUCHER1 | Kode voucher tidak ditemukan. |
      |                       | Masukkan kode voucher.             |
      | MAATMINTRXVOUCHER1    | Belum mencapai minimal transaksi.  |
      | KIPUMASSEXPIREDINV    | Kode voucher tidak bisa digunakan. |
      | SANITYAPRIL           | Kuota voucher ini sudah habis.     |