@newowner-dashboard @regression @LIMO2 @listing-monetization @ownerDashboardGP
Feature: Ownerdashboard GP

	#Validate label gp on owner GP level 3
  @TEST_LIMO-1629 @GP @GP3 @automated @listing-monetization @web
  Scenario: [Widget GP][GP Active]Owner already join GP 3 And check redirection widget GP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner gp 3" and click on Enter button
    And user click on owner popup
    And user redirected to "owner /"
    Then validate that owner have "GoldPlus 3"

	#Validate label gp on owner GP level 2
  @TEST_LIMO-1628 @GP @GP2 @automated @listing-monetization @web
  Scenario: [Widget GP][GP Active]Owner already join GP 2 And check redirection widget GP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner gp 2" and click on Enter button
    And user click on owner popup
    And user redirected to "owner /"
    Then validate that owner have "GoldPlus 2"

	#Validate label gp on owner GP level 1
  @TEST_LIMO-39 @GP @GP1 @automated @listing-monetization @web
  Scenario: [Widget GP][GP Active]Owner already join GP 1 And check redirection widget GP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner gp 1" and click on Enter button
    And user click on owner popup
    And user redirected to "owner /"
    Then validate that owner have "GoldPlus 1"

	#@OwnerNonGPLabel
	#Scenario: Validate label if owner not join gp
  @TEST_LIMO-1661 @Automated @GP @Web @listing-monetization
  Scenario: [Goldplus][Owner Dashboard][Widget GP] Check owner non GP click widget Daftar GP then redirect to package list
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner non gp" and click on Enter button
    And user click on owner popup
    And user redirected to "owner /"
    And user click Daftar GoldPlus field
    Then user redirect to List Package

	#@OwnerGPClickGPLabel
	#Scenario: Owner GP Click GP Label
  @TEST_LIMO-1647 @Automated @GP @Web @listing-monetization
  Scenario: [Goldplus][Owner Dashboard][Widget GP] Check owner GP click widget GP then redirect to GP page
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner gp 1" and click on Enter button
    And user click on owner popup
    And user redirected to "owner /"
    And validate that owner have "GoldPlus 1"
    And user click GP Label in Home
    Then user redirected to "dashboard gp"

	#Scenario: Owner Click Daftar Sekarang, cek URL
  @TEST_LIMO-1646 @Automated @GP @Web @listing-monetization
  Scenario: [Goldplus][Owner Dashboard][Widget GP] Check owner non GP click widget Daftar GP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner non gp" and click on Enter button
    And user click on owner popup
    And user redirected to "owner /"
    And user click Daftar GoldPlus field
    Then user redirected to "gp submission"

	#@OwnerGPonReview
	#Scenario: Validate label if owner while in status Sedang Diproses
  @TEST_LIMO-1662 @GP @automated @listing-monetization @web @web-covered
  Scenario: [Owner][GP Widget]Check GP Widget "Sedang Diproses"
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner gp on review" and click on Enter button
    And user click on owner popup
    And user redirected to "owner /"
    Then validate that owner have "Sedang Diproses"

    #Scenario: Validate label if owner while in status Menunggu Pembayaran
  @TEST_LIMO-53 @GP @automated @listing-monetization @web @web-covered
    Scenario: [Owner][GP Widget]Check GP Widget "Menunggu Pembayaran"
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner gp login as "owner gp menunggu pembayaran" and click on Enter button
    And user click on owner popup
    And user redirected to "owner /"
    Then validate that owner have "Menunggu Pembayaran"