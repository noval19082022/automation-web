@listing-monetization @regression @LIMO2 @mamipoinOwnerEntryPoint @mamipoinOwner
Feature: MamiPoin Owner Entry Point

	@TEST_LIMO-2009 @Automated @Web @listing-monetization @mamipoin-owner
	Scenario: [Owner Dashboard] MamiPoin Owner FTUE for new user and point = 0
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    And user fills out owner login as "MamiPoin New Owner" and click on Enter button
		    Then user verify MamiPoin widget is displayed
		    And user verify Pelajari text is displayed
		    And user verify point is 0
		    When user click MamiPoin widget
		    And user click back button in page
		    Then user verify point is 0
		    And user verify Pelajari text is not displayed

	@TEST_LIMO-2010 @Automated @Web @listing-monetization @mamipoin-owner
	Scenario: [Owner Dashboard] MamiPoin Owner FTUE for new user and point >= 1
		Given user navigates to "mamikos /"
		    When user clicks on Enter button
		    * user fills out owner login as "MamiPoin Owner" and click on Enter button
		    Then user verify MamiPoin widget is displayed
		    * user verify Tukar Poin text is displayed
			* user verify point is >= 1
		    When user click MamiPoin widget
		    * user click back button in page
		    Then user verify point is >= 1
		    * user verify Tukar Poin text is not displayed
