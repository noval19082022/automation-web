@regression @LIMO2 @listing-monetization
Feature: Check maksimum discount mamipoin owner at payment page

  @TEST_LIMO-64
  Scenario: [Owner][Payment[Mamipoin Owner] Check maksimum discount mamipoin owner when paid GP 1 or Gp 2
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "limo owner mamipoin dua" and click on Enter button
    * user click on owner popup
    When user click MamiPoin widget
    Then user verify MamiPoin onboarding is appear
    * user verify point is > 29000
    * user back to owner dashboard from mamipoin
    When user click Daftar GoldPlus field
    Then user redirect to List Package
    When user click pilih paket on "GoldPlus 1"
    Then user redirect to Detail Tagihan
    When user click button bayar sekarang
    * user click mamipoin toggle
    Then user see total potongan mamipoin "-29000"

    #scenario delete or reset data GP
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user navigates to "GP Recurring Tools"
    * user input phone number "082233545504" at gp recurring
    Then user click button reset at gp recurring

    #scenario check mamipoin for GP 2
    Given user navigates to "owner /"
    When user click MamiPoin widget
    Then user verify point is > 71500
    * user back to owner dashboard from mamipoin
    When user click Daftar GoldPlus field
    Then user redirect to List Package
    When user click pilih paket on "GoldPlus 2"
    Then user redirect to Detail Tagihan
    When user click button bayar sekarang
    * user click mamipoin toggle
    Then user see total potongan mamipoin "-71500"

    #scenario delete or reset data GP
    Given user navigates to "backoffice"
    When user navigates to "GP Recurring Tools"
    * user input phone number "082233545504" at gp recurring
    Then user click button reset at gp recurring


  @TEST_LIMO-65
  Scenario: [Owner][Payment[Mamipoin Owner] Check maksimum discount mamipoin owner when poin < from discount maksimum
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "limo owner mamipoin tiga" and click on Enter button
    * user click on owner popup
    When user click MamiPoin widget
    Then user verify MamiPoin onboarding is appear
    * user verify point owner is < 29000
    * user back to owner dashboard from mamipoin
    When user click Daftar GoldPlus field
    Then user redirect to List Package
    When user click pilih paket on "GoldPlus 1"
    Then user redirect to Detail Tagihan
    When user click button bayar sekarang
    * user click mamipoin toggle
    Then user see total potongan mamipoin "-5000"

    #scenario delete or reset data GP
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user navigates to "GP Recurring Tools"
    * user input phone number "082233545511" at gp recurring
    Then user click button reset at gp recurring



