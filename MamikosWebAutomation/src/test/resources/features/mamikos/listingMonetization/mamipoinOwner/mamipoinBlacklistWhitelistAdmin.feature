@listing-monetization @regression @LIMO2 @mamipoinOwnerEntryPoint @mamipoinOwner
Feature: Mamipoint Blacklist and Whitelist Admin

	@TEST_LIMO-2014 @Automated @Web @listing-monetization @mamipoin-owner
	Scenario: [Admin] Change Blacklist to Whitelist
		Given user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin ob"
		    When user access to user point menu
		    And user filter owner user point name "Ramos Pembina Komsel"
		    And user click on whitelist to change blacklist
		    Then user see alert success
	@TEST_LIMO-2013 @Automated @Web @listing-monetization @mamipoin-owner
	Scenario: [Admin] Change Whitelist to Blacklist
		Given user navigates to "mamikos admin"
		    And user logs in to Mamikos Admin via credentials as "admin ob"
		    When user access to user point menu
		    And user filter owner user point name "Ramos Pembina Komsel"
		    And user click on blacklist to change whitelist
		    Then user see alert success
