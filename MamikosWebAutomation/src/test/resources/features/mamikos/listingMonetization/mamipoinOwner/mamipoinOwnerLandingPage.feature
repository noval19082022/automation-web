@listing-monetization @regression @LIMO2 @mamipoinOwnerEntryPoint @mamipoinOwner
Feature: [Owner Dashboard] MamiPoin Owner Landing Page

	@TEST_LIMO-2011 @Automated @Web @listing-monetization @mamipoin-owner
	Scenario: [Owner Dashboard] MamiPoin Owner Landing Page
		Given user navigates to "mamikos /"
		    And user clicks on Enter button
		    And user fills out owner login as "MamiPoin New Owner" and click on Enter button
		    When user click MamiPoin widget
		    Then user verify MamiPoin onboarding is appear
		    And user verify title in the mamipoin owner landing page is displayed
		    And user verify tukar poin button is displayed
		    And user verify riwayat hadiah button is displayed
		    And user verify riwayat poin owner button is displayed
		    And user verify syarat dan ketentuan button is displayed
