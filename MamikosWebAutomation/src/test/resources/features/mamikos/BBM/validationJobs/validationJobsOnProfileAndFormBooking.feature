@BBM5 @validationJobsLainnya
Feature: Edit profile for jobs as Lainnya

  @TEST_BBM-1504 @automated @booking-billing-management @edit-profile @jobsLainnya
  Scenario: [Edit Profil][Profesi] User can't Clicked on Simpan Button without Fill Nama Pekerjaan
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "bbm jobs lainnya"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    Then user see button simpan edit profile disable

  @TEST_BBM-1513 @automated @booking-billing-management @booking-form
  Scenario: [Pengajuan Sewa][Informasi Penyewa]Click Ajukan Sewa Button if user has not fill Deskripsi when user choose Lainnya as Pekerjaan
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "bbm jobs lainnya"
    And user clicks search bar
    And I search property with name "OB kost mhs validation" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    And user reach booking form
    Then user can see validation on jobs
    And user click on Ajukan Sewa
    Then user can see validation on jobs with "Masukkan pekerjaan untuk memproses pengajuan sewa."

  @TEST_BBM-1497 @automated @booking-billing-management @edit-profile @TEST_BBM-1499 @TEST_BBM-1501
  Scenario: [Edit Profil][Profesi] User able to change Profesi to Mahasiswa, Karyawan and Lainnya
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB Cancel Tenant"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
#	update to Lainnya @TEST_BBM-1497
    And user choose profession "karyawan"
    And user click simpan button
    Then user see pop up message profil disimpan
#	update to Lainnya @TEST_BBM-1501
    And user click on ok button on pop up message profil disimpan
    And user click edit profile
    And user choose profession "lainnya"
    And user input profession "Wiraswasta"
    And user click simpan button
    Then user see pop up message profil disimpan
    And user click on ok button on pop up message profil disimpan
#	update to Lainnya @TEST_BBM-1499
    And user click edit profile
    And user choose profession "mahasiswa"
    And user click simpan button
    Then user see pop up message profil disimpan

  @TEST_BBM-1500 @TEST_BBM-1498 @TEST_BBM-1502 @automated @booking-billing-management @booking-form
  Scenario: [Pengajuan Sewa][Informasi Penyewa]Change pekerjaan to Mahasiswa and Karyawan with invalid data instantion
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB Different Gender Female"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
#    Update to mahasiswa and invalid desciption @TEST_BBM-1500
    And user choose profession "karyawan"
    And user click dropdown pilih instansi
    And user fills "abc"
    Then user can see information "There is no data"
#    Update to mahasiswa and invalid desciption @TEST_BBM-1498
    When user navigates to "mamikos /"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "mahasiswa"
    And user click dropdown pilih universitas
    Then user fills "abc" and search found
    Then user can see information "There is no data"
#    Update to mahasiswa and invalid desciption @TEST_BBM-1502
    When user navigates to "mamikos /"
    And user click profile on header
    And user click profile dropdown button
    And user click edit profile
    And user choose profession "lainnya"
    And user input profession "Lorem Ipsum is simply dummy text of the printing and typesetting"
    Then user can see error message "Maksimal 50 karakter." on jobs