@regression @downPayment @occupancyAndBilling @manageBooking @OB
Feature: OB Denda

  @activatedDenda @BBM-969
  Scenario: Activated denda and input price and update denda, then delete denda (BBM-969)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    And user clicks denda toggle
    And user see information about denda
    And user input denda ammount "50000"
    And user choose Denda time
    And user input dibebankan with "4"
#    Then user sees toast as "Biaya berhasil diubah"
    And user see denda list "Rp50.000"
    And user clicks Ubah "denda" button
    And user input denda ammount "150000"
    And user input dibebankan with "5"
#    Then user sees toast as "Biaya berhasil diubah"
    And user see denda list "Rp150.000"
    And user delete "denda"
    And user see header pop up delete confirmation with "denda"
    And user clicks Kembali on delete confirmation
    And user delete "denda"
    And user clicks Hapus on delete confirmation
#    Then user sees toast as "Biaya berhasil dihapus"
    Then user cannot see "denda" on the list

  @denda @BBM-909
  Scenario: Check Penalty's Rules On Daily, Weekly, and Monthly (BBM-909)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "other price ob"
    And user clicks denda toggle
    Then owner can see "1" and "31" as max and min for "Hari" penalty
    Then owner can see "1" and "4" as max and min for "Minggu" penalty
    Then owner can see "1" and "12" as max and min for "Bulan" penalty