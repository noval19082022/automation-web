Feature: [Atur Booking][Reject Reason] Check reject booking reason Tanggal masuk/check-in kos terlalu dekat and have BSS varian

	Background:
		#@PRECOND_BNB-10897
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "ob tenant bss"
		And user clicks search bar
		And I search property with name "ob booking male non pasutri" and select matching result to go to kos details page
		And user select date "tomorrow" and rent type "Per bulan"
		And user clicks on Booking button on Kost details page
		And user selects T&C checkbox and clicks on Book button
		And user navigates to main page after booking
		And user logs out as a Tenant user

	@TEST_BNB-5367 @automated @partial-regression @web @xray-update
	Scenario: [Atur Booking][Reject Reason] Check reject booking reason Tanggal masuk/check-in kos terlalu dekat and have BSS varian
		Given user navigates to "mamikos /"
		And user clicks on Enter button
		And user fills out owner login as "owner mark room ob" and click on Enter button
		And user navigates to "mamikos /ownerpage/manage/all/booking"
		And user select kost with name "Kost Wanawana papua"
		And user clicks on Booking Details button
		And owner process to reject booking request
		And owner reject booking with bss reason
		Then user can see confirmation Atur Booking popup
		And user click on atur booking button
		Then user can see atur booking page
