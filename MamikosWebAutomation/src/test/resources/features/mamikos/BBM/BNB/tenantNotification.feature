@booking @tenantNotification @occupancyAndBilling1 @regression @OB
Feature: OB Tenant Notification

  Scenario: Delete All Need Confirmation Booking Request
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant notification"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

#  Scenario: Tenant Booking
    Given user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost mhs validation" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user logs out as a Tenant user

#  @MB-2255
#  Scenario: Check Reject Booking Notification
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user click on owner popup
    When user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "ob kost tenant notification"
    And owner process to reject booking request
    And owner choose other reason with costum reason "Saya sudah ada yang punya"
    And owner user navigates to owner page
    And user navigates to "mamikos /"
    And user logs out as a Owner user
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant notification"
    And user click notifikasi on header
    Then user verify notification text and click on it
    Then user redirected to search page
