@BBM5
Feature: BnB feature with background create booking twice

  # Create booking
  Background:
	#@PRECOND_BBM-1002
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request
    And user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob kost saya homepage reject n waiting" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost promo ngebut" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to "mamikos /"
    And user logs out as a Tenant user

  @TEST_BBM-891 @automated @kost-saya-revamp-phase1 @partial-regression @web @xray-update
  Scenario: [Home Page][Kost Saya Section ]Check Kost saya section when tenant have booking with  Reject status but have Booking menunggu konfirmasi
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "ob kost saya homepage reject n waiting"
    And owner process to reject booking request
    And owner choose other reason with costum reason "Saya sudah ada yang punya"
    And user navigates to "mamikos /"
    And user logs out as a Owner user
    And user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    Then user can see shortcut homepage with "Pengajuan sewa lagi dicek pemilik"
    And user navigates to "mamikos /user/booking/"
#    When user click lihat selengkapnya the riwayat booking
    And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
    Then tenant redirect to history booking page

