@regression @booking @manageBooking @additionalPriceDeposit @OB/BX @additionalPrice
Feature: OB Additional Price Deposit

  @additionalPriceDeposit @BBM-894 @denda
  Scenario: Activated deposit and input deposit price and save, then delete deposit (BBM-894)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "kost automation new"
    And user clicks deposit toggle
    And user see information about deposit
    And user input deposit amount "100000"
#    Then user sees toast as "Biaya berhasil diubah"
    And user see deposit list "Rp100.000"
    And user clicks Ubah "deposit" button
    And user input new deposit amount "150000"
    And user see deposit list "Rp150.000"
    And user delete "deposit"
    And user see header pop up delete confirmation with "deposit"
    And user clicks Kembali on delete confirmation
    And user delete "deposit"
    And user clicks Hapus on delete confirmation
#    Then user sees toast as "Biaya berhasil dihapus"
    And user cannot see "deposit" on the list