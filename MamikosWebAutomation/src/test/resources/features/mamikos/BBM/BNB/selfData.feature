@regression @booking @manageBooking @selfData @OB
Feature: OB Self Data Feature

  #Deleting existing booking
  Background:
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search for Kost with name
    And user click on Cancel the Contract Button from action column

  Scenario: Cancel Active Booking
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "gender female"
    And user navigates to "mamikos /user/booking/"
    Then tenant cancel active booking if it available

  @selfDataMixKost
  Scenario: Check mix kost
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "gender female"
    And user clicks search bar
    And I search property with name "ob mix kost" and select matching result to go to kos details page
    And user clicks on Booking button on Kost details page
    And user select "tomorrow" date as booking date
    And user fills out Rent Duration equals to 4 Bulan, and clicks on Next button
    Then tenant can sees self data page
    And tenant can sees gender selection
    When tenant click on "male" radio button
    Then tenant can sees "male" radio button is selected
    When tenant click on "female" radio button
    Then tenant can sees "female" radio button is selected

  Scenario: Delete Baru Dilihat Self Data
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "gender female"
    And user navigates to "mamikos /last-seen"
    And I click on trash icon
    And I clicks on Batalkan button
    And I click on trash icon
    And I clicks on Hapus button
