@regression @manageBooking @occupancyAndBilling @addTenantOB @OB
Feature: Add Tenant

  @BNB-2230
  Scenario: Add Tenant For Full Room (BNB-2230)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /add-tenant/on-boarding"
    And user click continue until start adding contract
    And user select kost "full kost" for tenant
    Then owner can sees full pop up restriction
    When owner clicks on change room's data on full room pop up restriction
    Then owner redirected to update room page
    Then owner can not sees full room pop up restriction

  @BNB-2229
  Scenario: Add Tenant For Different Gender (BNB-2229)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner mark room ob" and click on Enter button
    And user navigates to "owner /add-tenant/on-boarding"
    And user click continue until start adding contract
    And user select kost "female kost" for tenant
    And owner input phone number with "male phone number" and choose first available room and clicks on add renter button
    Then owner can sees different gender restriction pop-up