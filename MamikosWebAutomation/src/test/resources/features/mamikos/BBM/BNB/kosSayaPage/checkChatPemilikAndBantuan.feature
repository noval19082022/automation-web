@regression @booking @manageBooking @occupancyAndBilling @kosSaya @chatPemilikAndBantuan
Feature: Kost saya - chat pemilik and bantuan

  Background:
    #@PRECOND_BNB-10913
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB contract sect dbet"
    And user navigates to Profil page

  @TEST_BNB-8581 @Automated @kost-saya-revamp-phase2 @xray-update
  Scenario: [Kos Saya][Chat Pemilik]Check Chat Pemilik on kost saya page
    And tenant clicks on kost saya
    Then user see title kos saya page
    And user see welcome section at kos saya
    Then user see Tagihan kos, Kontrak, Chat pemilik, Bantuan, Forum
    And user clicks on Chat pemilik menu
    Then user can see Chat list title

  @TEST_BNB-8582 @Automated @kost-saya-revamp-phase2 @xray-update
  Scenario: [Kos Saya][Bantuan]Check Bantuan on kost saya page
    When tenant clicks on kost saya
    Then user see title kos saya page
    And user see welcome section at kos saya
    And user see Tagihan kos, Kontrak, Chat pemilik, Bantuan, Forum
    When user clicks on Bantuan menu
    Then user can see bantuan list with secation "Check-in", "Pembayaran dan tagihan" and "Kontrak sewa"
    When user clicks on "Mengapa saya harus check-in?"
    Then user can see "Mengapa saya harus melakukan check-in?" on mamihelp page
    When user clicks on "Bagaimana cara membatalkan check-in?"
    Then user can see "Bagaimana cara membatalkan check-in?" on mamihelp page
    When user clicks on "Apakah saya harus membayar uang sewa melalui Mamikos?"
    Then user can see "Apakah saya harus membayar uang sewa melalui Mamikos?" on mamihelp page
    When user clicks on "Bagaimana cara melihat tagihan/invoice sewa kos saya?"
    Then user can see "Bagaimana cara melihat tagihan/invoice sewa kos saya?" on mamihelp page
    When user clicks on "Bagaimana cara check-out di kos?"
    Then user can see "Bagaimana cara check-out di kos?" on mamihelp page
    When user clicks on "Kontrak saya akan segera berakhir, dan saya ingin lanjut ngekos. Apa yang harus saya lakukan?"
    Then user can see "Kontrak saya akan segera berakhir, dan saya ingin lanjut ngekos. Apa yang harus saya lakukan?" on mamihelp page
    When user click on Contact Cs
    Then user can see "pusat bantuan" on mamihelp page