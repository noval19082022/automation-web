@regression @occupancyAndBilling1 @penyewaList
  Feature: Contract status on Penyewa

    @waitingTerminateConfirmation @BNB-7301
    Scenario: check waiting terminated confirmation status
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user fills out owner login as "ob reject booking" and click on Enter button
      And user navigates to "owner /tenant-list"
      And user click on Kost name search
      And user search kost "kost nias waiting terminated confirmation Tobelo Utara Halmahera Utara"
      And user click on dropdown Filter box and select "Menghentikan kontrak sewa" filter
      Then user will see contract with status "Butuh konfirmasi berhenti sewa"
      And user will see alert terminatation
      When user click Selengkapnya button
      Then user will see Tolak and Konfirmasi button on detail penyewa page