@regression @ownerDashboard @occupancyAndBilling @disbursementInfo
Feature: Check content and link on info untuk anda for disbursement

  @disbursementInfo @BBM-898
  Scenario: Check content and link on info untuk anda for disbursement (BBM-898)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user click on owner popup
    And owner go to Kelola Tagihan page
    And owner choose kost for "kost makassar" on Tagihan page
    Then owner can see kost filter is selected with kost name "kost makassar check dpst kos saya automation Tobelo Utara Halmahera Utara"
    And owner choose month with "Mei"
    And user clicks Sudah bayar tab
    And user see Kapan uang masuk ke rekening saya? and clicks on disbursement link
    Then user redirect to disbursement link and clicks back
    And user clicks on billing invoice
    Then user will see disbursement alert and see Kapan uang masuk ke rekening saya? and clicks on disbursement link
    And user redirect to disbursement link