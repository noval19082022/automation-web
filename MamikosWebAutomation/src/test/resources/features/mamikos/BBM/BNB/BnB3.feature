#@bnb
@BBM5
Feature: BnB feature with background navigate profile page

  Background:
    #@PRECOND_BBM-999
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB contract sect dbet"
    And user navigates to Profil page

  @TEST_BBM-912 @Automated @kost-saya-revamp-phase2 @xray-update
  Scenario: [Kos Saya][Chat Pemilik]Check Chat Pemilik on kost saya page (BBM-912)
    And tenant clicks on kost saya
    Then user see title kos saya page
    And user see welcome section at kos saya
    Then user see Tagihan kos, Kontrak, Chat pemilik, Bantuan, Forum
    And user clicks on Chat pemilik menu
    Then user can see Chat list title

  @TEST_BBM-911 @Automated @kost-saya-revamp-phase2 @xray-update
  Scenario: [Kos Saya][Bantuan]Check Bantuan on kost saya page (BBM-911)
    When tenant clicks on kost saya
    Then user see title kos saya page
    And user see welcome section at kos saya
    And user see Tagihan kos, Kontrak, Chat pemilik, Bantuan, Forum
    When user clicks on Bantuan menu
    Then user can see bantuan list with secation "Check-in", "Pembayaran dan tagihan" and "Kontrak sewa"
    When user clicks on "Mengapa saya harus check-in?"
    Then user can see "Mengapa saya harus melakukan check-in?" on mamihelp page
    When user clicks on "Apakah saya harus membayar uang sewa melalui Mamikos?"
    Then user can see "Apakah saya harus membayar uang sewa melalui Mamikos?" on mamihelp page
    When user clicks on "Bagaimana cara check-out di kos?"
    Then user can see "Bagaimana cara check-out di kos?" on mamihelp page
    When user click on Contact Cs
    Then user can see "pusat bantuan" on mamihelp page

  @TEST_BBM-884 @automated @booking-and-billing @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Kost saya][Content] Content Kos Saya (BBM-884)
    And tenant clicks on kost saya
    Then user see title kos saya page
    And user see welcome section at kos saya
    When user click Lihat informasi kos
    Then user will redirect to informasi kos page
    When user see review kos section
    And user see Aktivitas di Kos Saya
    Then user see Tagihan kos, Kontrak, Chat pemilik, Bantuan, Forum

  @TEST_BBM-908 @automated @kost-saya-revamp-phase1 @partial-regression @web @xray-update
  Scenario: [Kost saya][Kontrak]Check kontrak section when tenant has contract from dbet (BBM-908)
    When user click Kontrak section
    Then user redirect to Kontrak page
    And user check kost type, kost name, kost address, and room number
    And user check total biaya, harga sewa, and biaya lain
    And user check tanggal penagihan
    And user check mulai sewa, durasi sewa, and sewa berakhir
#    And user also check denda
    And user see Ajukan berhenti button and user click this button
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
    And user search kost "kost bali for contract section Tobelo Utara Halmahera Utara"
    And user click Selengkapnya button on "Tenant Coba Pop Up Empat" contract
    And user click tab Kontrak sewa
    And user click Ubah kontrak penyewa button
    Then user check prices at penyewa owner are same to contract at kos saya