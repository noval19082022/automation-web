@regression @manageBooking @occupancyAndBilling1 @OB @booking
Feature: OB Newly Seen

  Background: Tenant Delete All Newly Seen
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB Baru Dilihat"
    And user navigates to "mamikos /user/booking/"
    When user clicks on Baru Dilihat section
    And tenant delete all newly seen from the list

  @newlySeen @BNB-2193
  Scenario: Booking From Newly Seen (BNB-2193)
    Given user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob owner reject" and select matching result to go to kos details page
    And tenant dismiss FTUE screen on kost details
    And user navigates to "mamikos /user/booking/"
    And user clicks on Baru Dilihat section
    Then tenant can sees kost name in list number 1 is "newly seen"
    When tenant clicks on direct booking button in list number 1
    And user select payment period "Per Bulan"
    And user select checkin date for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to "mamikos /"
    And user navigates to "mamikos /user/booking/"
    Then tenant can sees need confirmation booking is for kost "newly seen"
    And user clicks on Baru Dilihat section
    Then tenant can sees kost name in list number 1 is "newly seen"
    #Tenant cancel booking for next run
    And user navigates to "mamikos /user/booking/"
    When user click lihat selengkapnya the riwayat booking
    And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
    And user navigates to "mamikos /user/booking/"