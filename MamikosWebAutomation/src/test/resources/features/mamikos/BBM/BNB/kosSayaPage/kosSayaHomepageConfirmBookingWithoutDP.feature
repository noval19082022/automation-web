@regression @booking @manageBooking @occupancyAndBilling1 @kostSayaOnHomepage @kostSayaConfirm
Feature: Kost Saya on Homepage - Confirm Booking

  Background:
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by renter phone "08100000623"
    And user click on Cancel the Contract Button from action column

#  Tenant Booking kost without DP
#  Scenario: Create booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya confirm"
#  check if any booking butuh konfirmasi
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "booking kost ob" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user select payment period "Per Bulan"
    And user selects T&C checkbox and clicks on Book button
    Then user navigates to main page after booking
    And user navigates to "mamikos /"
    And user logs out as a Tenant user
#  Scenario: Confirm Booking
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "kost reykjavik"
    And user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button
    Then user will redirect to pengajuan booking page
    And user navigates to "mamikos /"
    And user logs out as a Owner user

  @checkKosSayaHomepageWhenConfirmBooking @BNB-8251
  Scenario: Check Kost saya section when tenant have Confirm Booking without DP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya confirm"
    And user navigates to "mamikos /"
    Then user can see shortcut homepage with "Hore! Pengajuan sewamu diterima"

   #Terminated contract
#  Scenario: Delete contract at backoffice
    Given user navigates to "backoffice"
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by renter phone "08100000623"
    And user click on Cancel the Contract Button from action column