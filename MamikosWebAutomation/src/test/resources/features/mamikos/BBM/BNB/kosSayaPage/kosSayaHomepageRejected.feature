@regression @booking @manageBooking @occupancyAndBilling1 @kostSayaOnHomepage @kostSayaRejected
Feature: Kost Saya on Homepage - Rejected

  Background:
    #@PRECOND_BNB-10917
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    # check if tenant still has a booking need confirmation
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request
    # continue create booking
    And user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob kost saya homepage reject" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "ob kost saya homepage reject"
    And owner process to reject booking request
    And owner clicks on T&C and redirect to Term And Condition Page
    Then owner can sees Term And Condition check box is not tick
    And owner choose other reason with costum reason "Saya sudah ada yang punya"
    And user navigates to "mamikos /"
    And user logs out as a Owner user

  @TEST_BNB-8245 @automated @booking-and-billing @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Home Page][Kost Saya Section ]Check Kost saya section when tenant have booking with  Reject status
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user navigates to "mamikos /"
    Then user can see shortcut homepage with "Yah, pengajuan sewamu ditolak"

  @TEST_BNB-8248 @automated @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Home Page][Kost Saya Section ]Check kost saya section on homepage when tenant have booking with reject status  > 1 booking request
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user navigates to "mamikos /"
    Then user can see shortcut homepage with "Yah, pengajuan sewamu ditolak"
    And user navigates to "mamikos /"
    Then user can see Lihat pengajuan sewa lainnya text
    And user click on Lihat pengajuan sewa lainnya button
    Then user should reached history booking page