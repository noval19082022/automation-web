@regression @ownerDashboard @occupancyAndBilling @manageBooking @OB
Feature: OB Owner Dashboard when have one kos not bbk

  @checkOwnerHaveOneKosNotBbk @BNB-4434
  Scenario: Check Waktu Mengelola section when owner have one kost not Bbk (BNB-4434)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob have not bbk" and click on Enter button
    And user click on owner popup
    Then user can see waktu mengelola section
    When user can see "Daftar ke Booking Langsung" menu in owner dashboard
    And user cliks widget booking register
    Then user can see manage direct booking popup



