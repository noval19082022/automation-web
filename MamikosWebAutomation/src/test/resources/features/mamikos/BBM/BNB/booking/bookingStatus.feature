@regression @manageBooking @occupancyBilling @bookingStatus @OB @booking

Feature: OB Booking Feature

  @needConfirmationBehavior @BNB-2175
  Scenario: Check Need Confirmation Booking Behavior (BNB-2175)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob owner reject" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to Booking History page
    Then tenant can sees need confirmation status with kost is "need confirmation"
    When tenant clicks on chat button on need confirmation booking status
    Then tenant can sees chat box with kost is "need confirmation"
    When tenant dismiss chat box
    And tenant click on see complete button wait for confirmation booking
    Then tenant can sees booking price, see facilities, new info, renter information, booking detail
    When tenant clicks on see facilities
    Then tenant can sees kost facilities
    When tenant clicks on see new information
    Then tenant can sees kost details with kost name "need confirmation"
    When tenant clicks on see less button
    Then tenant can sees details booking is collapsed
    When tenant cancel active booking if it available