@bnb @BBM6
Feature: BnB feature

  @OwnerBadgesNotLogin @OwnerBadges @BBM-500
  #checkOwnerBadges.feature
  Scenario: Check Owner Badges on Kos Detail when not login tenant (BBM-500)
    Given user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    Then user can see owner badges on kos detail

  @OwnerBadgesWithTenantLogin @OwnerBadges @BBM-498
  #checkOwnerBadges.feature
  Scenario: Check Owner Badges on Kos Detail when login tenant (BBM-498)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    Then user can see owner badges on kos detail

  @OwnerBadgesWithOwnerLogin @BBM-499
  #checkOwnerBadges.feature
  Scenario: Check Owner Badges on Kos Detail when login owner (BBM-499)
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    And user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    Then user can see owner badges on kos detail

  @activatedDenda @BBM-969
  #denda.feature
  Scenario: Activated denda and input price and update denda, then delete denda (BBM-969)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    And user clicks denda toggle
    And user see information about denda
    And user input denda ammount "50000"
    And user choose Denda time
    And user input dibebankan with "4"
#    Then user sees toast as "Biaya berhasil diubah"
    And user see denda list "Rp50.000"
    And user clicks Ubah "denda" button
    And user input denda ammount "150000"
    And user input dibebankan with "5"
#    Then user sees toast as "Biaya berhasil diubah"
    And user see denda list "Rp150.000"
    And user delete "denda"
    And user see header pop up delete confirmation with "denda"
    And user clicks Kembali on delete confirmation
    And user delete "denda"
    And user clicks Hapus on delete confirmation
#    Then user sees toast as "Biaya berhasil dihapus"
    Then user cannot see "denda" on the list

  @denda @BBM-909
  #denda.feature
  Scenario: Check Penalty's Rules On Daily, Weekly, and Monthly (BBM-909)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "other price ob"
    And user clicks denda toggle
    Then owner can see "1" and "31" as max and min for "Hari" penalty
    Then owner can see "1" and "4" as max and min for "Minggu" penalty
    Then owner can see "1" and "12" as max and min for "Bulan" penalty

  @additionalPriceDeposit @BBM-894
  #ownerAdditionalDeposit.feature
  Scenario: Activated deposit and input deposit price and save, then delete deposit (BBM-894)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "kost automation new"
    And user clicks deposit toggle
    And user see information about deposit
    And user input deposit amount "100000"
#    Then user sees toast as "Biaya berhasil diubah"
    And user see deposit list "Rp100.000"
    And user clicks Ubah "deposit" button
    And user input new deposit amount "150000"
    And user see deposit list "Rp150.000"
    And user delete "deposit"
    And user see header pop up delete confirmation with "deposit"
    And user clicks Kembali on delete confirmation
    And user delete "deposit"
    And user clicks Hapus on delete confirmation
#    Then user sees toast as "Biaya berhasil dihapus"
#    And user cannot see "deposit" on the list

  @BBM-918 @booking1
  #otherPrice.feature
  Scenario: Owner Add Additional Price With 20 Character And Price With Correct Data (BBM-918)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "other price ob"
    And user clicks on other price per month toggle
    And user input price name with "1234567890abcdefjkl", price with "50000"
#    Then user sees toast as "Biaya berhasil diubah"
    Then user can sees other price with name "1234567890abcdefjkl" and price "Rp50.000" show in the list

  @BBM-950
  #otherPrice.feature
  Scenario: Check Booking With Other Price On Admin Side (BBM-950)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob other price" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    And user selects T&C checkbox and clicks on Book button
    When user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "additional price ob"
    And user apply filter
    And user go to confirm booking via action button
    And user navigates to "tambahan biaya" confirmation page data booking admin
    Then user can sees other price with name "1234567890abcdefjkl" and price "Rp50.000" on tambahan biaya
    When user navigates to "konfirmasi" confirmation page data booking admin
    Then user can sees other price with name "1234567890abcdefjkl" and price "50000" on konfirmasi
    And user confirm booking from admin

  @BNB-3131 @BBM-947 @booking1
  #otherPrice.feature
  Scenario: Delete Active Other Additional Price (BBM-947)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "other price ob"
    And user delete active other additional price
    And tenant can not sees active other price

  @MB-3131 @deleteContractOtherPrice
  #otherPrice.feature
  Scenario: Delete contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "ob additional price phone number"
    And user click on Cancel the Contract Button from action column

  @filterPenyewa @BBM-902 @booking1
  #contractStatusOnPenyewaList.feature
  Scenario: check contract status on all filter (BBM-902)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user click on owner popup
    When user click on widget Penyewa
    Then user can see Penyewa list
#    And user click on Kost name search
    And user search kost "kost banda who dont have kos saya"
    And user click on dropdown Filter box and select "Sedang menyewa" filter
    Then user will see contract with status "Sedang menyewa"
    When user click on dropdown Filter box and select "Akan masuk" filter
    Then user click on dropdown Filter box and select "Menghentikan kontrak sewa" filter

  @downloadBiodata @BBM-885 @BBM-888
  #contractStatusOnPenyewaList.feature
  Scenario: download biodata fakedoor (BBM-885)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
    Then user can see Penyewa list
#    And user click on Kost name search
    And user search kost "kost banda who dont have kos saya"
    And user click download biodata penyewa button
    Then user will see pop up for upcoming feature
    When user tick on checkbox pop up
    Then user will see information about upcoming feature

  @waitingTerminateConfirmation @BBM-899
  #waitingTerminatedConfirmation.feature
  Scenario: check waiting terminated confirmation status (BBM-899)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
#    And user click on Kost name search
    And user search kost "kost nias waiting terminated confirmation Tobelo Utara Halmahera Utara"
    And user click on dropdown Filter box and select "Menghentikan kontrak sewa" filter
    Then user will see contract with status "Butuh konfirmasi berhenti sewa"
    And user will see alert terminatation
    When user click Selengkapnya button
    Then user will see Tolak and Konfirmasi button on detail penyewa page

  @checkContentOnWaktuMengelolaKos @BBM-971
  #checkContentOnWaktuMengelolaKos.feature
  Scenario: Check Waktu Mengelola section when owner have one kost not Bbk (BBM-971)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob have not bbk" and click on Enter button
    Then user can see waktu mengelola section
    And user click on owner popup
    When user can see "Atur Ketersedian Kamar" menu in owner dashboard
    And user clicks widget set available room
    Then user see screen update room
    And user click on Home menu
    When user can see "Atur Harga" menu in owner dashboard
    And user clicks widget set price
    Then user can see set price page
    And user click on Home menu
    When user can see "Daftar ke Booking Langsung" menu in owner dashboard
    And user cliks widget booking register
    Then user can see manage direct booking popup
    And user click on Home menu
    When user can see "Daftar kontrak penyewa kos" menu in owner dashboard
    And user click on widget Penyewa
    Then user can see "Penyewa di" page
    And user click on Home menu
    Then user can see "Tambah Penyewa" menu in owner dashboard
    When user can see "Pusat Bantuan" menu in owner dashboard
    And user click on widget help center
    Then user can see help center page

  @disbursementInfo @BBM-898
  #infoUntukAndaForDisbursment.feature
  Scenario: Check content and link on info untuk anda for disbursement (BBM-898)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user click on owner popup
    And owner go to Kelola Tagihan page
    And owner choose kost for "kost makassar" on Tagihan page
    Then owner can see kost filter is selected with kost name "kost makassar check dpst kos saya automation Tobelo Utara Halmahera Utara"
    And owner choose month with "Mei"
    And user clicks Sudah bayar tab
    And user see Kapan uang masuk ke rekening saya? and clicks on disbursement link
    Then user redirect to disbursement link and clicks back
    And user clicks on billing invoice
    Then user will see disbursement alert and see Kapan uang masuk ke rekening saya? and clicks on disbursement link
    And user redirect to disbursement link

  @checkContentLaporanKeuangan @BBM-974
  #laporanKeuangan.feature
  Scenario: Check content Laporan Keuangan (BBM-974)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user click on owner popup
    Then user can see pendapatan section
    And user click on lihat laporan keuangan
    Then user can see "Buka Laporan Keuangan di Aplikasi" and "Untuk saat ini, fitur Laporan Keuangan hanya dapat digunakan di aplikasi Mamikos di Android dan iOS."
    And user click on Home menu
    And user click on Laporan keuangan on manajemen kos menu
    Then user can see "Buka Laporan Keuangan di Aplikasi" and "Untuk saat ini, fitur Laporan Keuangan hanya dapat digunakan di aplikasi Mamikos di Android dan iOS."

  @checkOwnerHaveOneKosNotBbk @BBM-972
  #ownerHasOneThatHasnotBeenBbk.feature
  Scenario: Check Waktu Mengelola section when owner have one kost not Bbk (BBM-972)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob have not bbk" and click on Enter button
    And user click on owner popup
    Then user can see waktu mengelola section
    When user can see "Daftar ke Booking Langsung" menu in owner dashboard
    And user cliks widget booking register
    Then user can see manage direct booking popup

  @checkWaktuMengelolaWhenOwnerNotHaveBbkKos @BBM-973
  #ownerNotHaveBbkKos.feature
  Scenario: Check Waktu Mengelola section when owner not have BBK kos (BBM-973)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "Owner Not Bbk" and click on Enter button
    Then user can see waktu mengelola section
    When user clicks widget set available room
    Then user see screen update room
    And user click on Home menu
    Then user can see waktu mengelola section
    And user clicks widget set price
    Then user can see set price page
    And user click on Home menu
    Then user can see waktu mengelola section
    And user can see booking register widget
    And user cliks widget booking register
    And user can see manage direct booking popup
    And user click on Home menu
    Then user can see waktu mengelola section
    And user click on widget Penyewa
    Then user can see booking benefit page
    And user clicks on Owner Settings button
    And user click on Home menu
    Then user can see waktu mengelola section
    And user click Add Tenant Contract button in dashboard page
    Then user can see booking benefit page
    And user clicks on Owner Settings button
    And user click on Home menu
    Then user can see waktu mengelola section
    And user click on widget help center
    Then user can see help center page

  @BBM-883 @booking1
  #chatButuhResponPengajuanSewaLabel.feature
  Scenario: Delete All Need Confirmation Booking Request
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant chat bth respon pngajuan sewa label"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

#  Scenario: tenant booking kost
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost booking additional" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    Then user navigates to main page after booking

#  Scenario: Owner accept booking from tenant
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    Then user click on owner popup
    And owner goes to Chat Page
    And owner clicked that tenant
    Then owner can see label with "Butuh respon pengajuan sewa"

  @addAndMarkRoomKosGP  @BNB-2245
  #addRoom.feature
  Scenario: check when owner add and mark room at kos GP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    And owner goes to my property kos menu
    And owner search "mark room occupied" and click see complete button
    And owner click on update room
    And user click add room in room list
    And user fill room name in room allotment page with "Jupiter"
    And user fill room floor in room allotment page with "11011"
    And user tick on tickbox and click on Tambah kamar
    Then toast will be appears and the room is untick again

  @markRoomAsOccupied @markRoom @BBM-868
  #markRoom.feature
  Scenario: Mark BBK And Gold Plus Room As Occupied (BBM-868)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    And owner goes to my property kos menu
    And owner search "mark room occupied" and click see complete button
    And owner click on update room
    And owner click on edit button on room "Kosong"
    And owner tick on already occupied tickbox and click on update room
    Then owner can sees Pop-Up owner not add renter's data
    When owner click on Add Renter button
    Then owner redirected to Input Renter's Information form with kost name "mark kost gp"

  @occupancyAndBilling @markRoom @BBM-867 @booking1
  #markRoom.feature
  Scenario: Mark BBK And Non Gold Plus Room As Occupied (BBM-867)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And owner search "mark room not gp" and click see complete button
    And owner click on update room
    And owner click on edit button on room "Kosong"
    And owner tick on already occupied tickbox and click on update room
    Then owner can sees room is on "Terisi" status
    When owner click on edit button on room "Terisi"
    And owner tick on already occupied tickbox and click on update room
    Then owner can sees room is on "Kosong" status

  @updateRoomToast @markRoom @BBM-869 @booking1
  #updateRoom.feature
  Scenario: Check Update Room's Toast (BBM-869)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user navigates to update kamar page for kost "Dont Starve Together" list number "1"
    And user clicks on edit kost button for kost list number "1"
    And user click update room button in update room
#    Then user can sees toast on update room as "Anda berhasil update data kamar"

  @BBM-913
  #changeOwnersPhoneNumber.feature
  Scenario: change owner's number phone at unique code
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
#    And user click on Kost name search
    And user search kost "kost bandung"
    And user click Selengkapnya button
    And user click Kirim ulang kode hyperlink
    Then user will redirect to Kirim kode unik ke penyewa page
    And user will see old number of owner "0890000001001"
    When user click Ubah nomor HP hyperlink
    And user change owner's phone number into "0890000001002" and click Gunakan
    Then user will see new number phone of owner "0890000001002"
    When user click Ubah nomor HP hyperlink
    And user change owner's phone number into "0890000001001" and click Gunakan
    Then user will see the old number of owner "0890000001001"

  @BBM-895
  #warningDontHaveKosSayaAtSemuaFilter.feature
  Scenario: check warning who don't have kos saya at Semua filter (BBM-895)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
#    And user click on Kost name search
    And user search kost "kost banda"
    Then user will see wording of warning tenant who don't have kos saya at Semua filter
    When user owner click Siapa saja penyewa yang belum hyperlink
    Then user will redirect to Sedang menyewa filter and user will see wording of warning tenant who don't have kos saya

  @BBM-928
  #addTenant.feature
  Scenario: Add Tenant For Full Room (BBM-928)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /add-tenant/on-boarding"
    And user click continue until start adding contract
    And user select kost "full kost" for tenant
    Then owner can sees full pop up restriction
    When owner clicks on change room's data on full room pop up restriction
    Then owner redirected to update room page
    Then owner can not sees full room pop up restriction

  @BBM-927
  #addTenant.feature
  Scenario: Add Tenant For Different Gender (BBM-927)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner mark room ob" and click on Enter button
    And user navigates to "owner /add-tenant/on-boarding"
    And user click continue until start adding contract
    And user select kost "female kost" for tenant
    And owner input phone number with "male phone number" and choose first available room and clicks on add renter button
    Then owner can sees different gender restriction pop-up

  @TEST_BBM-921 @automated @kost-saya-revamp-phase1 @partial-regression @web @xray-update @booking1
  Scenario: [Kost saya][Kontrak]Check kontrak section when tenant paid DP & ST (BBM-921)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant dpst kos saya"
    And user navigates to Profil page
    And user click Kontrak section
    Then user redirect to Kontrak page
    And user check kost type, kost name, kost address, and room number
    And user check total biaya, harga sewa, and biaya lain on kost dpst
    And user check tanggal penagihan
    And user check mulai sewa, durasi sewa, and sewa berakhir
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
#    And user click on Kost name search
    And user search kost "kost makassar"
    And user click Selengkapnya button on "Tenant For Check Contract Dpst Automation" contract
    And user click tab Kontrak sewa
    And user click Ubah kontrak penyewa button
    Then user check prices at penyewa owner are same to contract at kos saya for dpst payment

  @fullPaymentKosSaya @BBM-919 @bookingerror
  #checkContractFullPayment.feature
  Scenario: Tenant pay invoice full payment (BBM-919)
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by Kost name "kost bima booking"
    And user click on Cancel the Contract Button from action column

# Scenario: Delete All Need Confirmation Booking Request
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant full pay kos saya"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

#  Scenario: tenant booking kost
#  Scenario: Tenant booking
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost booking additional" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    Then user navigates to main page after booking

#  Scenario: Owner accept booking from tenant
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "kost bima booking"
    And user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button
    Then user will redirect to pengajuan booking page

#  Scenario: Tenant pay invoice full payment
    Given user navigates to "mamikos /"
    When user logs out as a Owner user
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant full pay kos saya"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user select payment method "OVO" for "OBAutomation"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

#  Scenario: Tenant check in
    Given user navigates to "mamikos /"
    And user navigates to Profil page
    And tenant clicks on kost saya
    And user click Check in at Kos saya page
    Then user redirect to kos saya page

#  Scenario: check contract section from full payment
    Given user navigates to "mamikos /"
    And user navigates to Profil page
    And user click Kontrak section
    Then user redirect to Kontrak page
    And user check kost type, kost name, kost address, and room number
    And user check total biaya, harga sewa, and biaya lain full payment
    And user check tanggal penagihan
    And user check mulai sewa, durasi sewa, and sewa berakhir
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
#    And user click on Kost name search
    And user search kost "kost bima booking"
    And user click Selengkapnya button on "Tenant Untuk Full Payment Kos Saya Automation" contract
    And user click tab Kontrak sewa
    And user click Ubah kontrak penyewa button
    Then user check prices at penyewa owner are same to contract at kos saya for full payment

  @TEST_BBM-893 @Automated @kost-saya-revamp-phase2 @xray-update
  Scenario: [Kost Saya][Forum ]When tenant click Forum and not interested (BBM-893)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB contract sect dbet"
    And user navigates to Profil page
    And tenant clicks on kost saya
    And user clicks on forum menu
    Then user will see pop up for upcoming forum feature
    When user clicks on Oke button
    And user clicks on forum menu
    And user tick on checkbox pop up upcoming
    And user clicks on forum menu
    Then user will see information about upcoming forum feature

  @TEST_BBM-968 @automated @kost-saya-revamp-phase1 @web @xray-update @bookingerror
  Scenario: [Homepage ][Kost Saya Section ]Check Kos Saya on Homepage when have Draft booking = 1 (BBM-968)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya homepage"
    And user navigates to "mamikos /user/booking/"
    And user click on Draft menu
    And user click delete draft if any
    Then tenant cannot see "kost madiun buat draft homepage Tobelo Utara Halmahera Utara" as kost name and kost location
    When user navigates to "mamikos /"
    And user click Mau Coba Dong section at homepage
    Then user can see empty state kost saya
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob draft n waiting homepage" and select matching result to go to kos details page
    And user create booking for today
    And user click exit button
    And user check confirmation text title
    And user check confirmation text body
    And user click Save Draft
    Then user redirect to kost detail
    When user navigates to "mamikos /"
    Then user can see shortcut homepage with "Mau lanjut ajukan sewa di kos ini?"
    And user navigates to "mamikos /user/booking/"
    And user click on Draft menu
    And user click delete button on tab one draft booking
    And user click Hapus Draft on pop up delete draft booking
    Then tenant cannot see "kost madiun buat draft homepage Tobelo Utara Halmahera Utara" as kost name and kost location

  @TEST_BBM-882 @automated @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Homepage ][Kost Saya Section ]Check homepage when have total waiting confirmation booking = 1 (BBM-882)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kos saya homepage waiting confirm"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob draft n waiting homepage" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to "mamikos /"
    Then user can see shortcut homepage with "Pengajuan sewa lagi dicek pemilik"
    When user navigates to "mamikos /"
    And user navigates to "mamikos /user/booking/"
#    And user click lihat selengkapnya the riwayat booking
    And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
    Then tenant redirect to history booking page

  @TEST_BBM-905 @automated @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Homepage ][Kost Saya Section ]Check Draft booking on homepage when have 1 draft for kost Promo Ngebut (BBM-905)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kos saya homepage draft booking probut"
    And user navigates to "mamikos /user/booking/"
    And user click on Draft menu
    And user click delete draft if any
    Then tenant cannot see "OB kost promo ngebut" as kost name and kost location
    And user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost promo ngebut" and select matching result to go to kos details page
    And user create booking for today
    And user click exit button
    And user check confirmation text title
    And user check confirmation text body
    And user click Save Draft
    Then user redirect to kost detail
    When user navigates to "mamikos /"
    And user check promo ngebut label
    Then user can see shortcut homepage with "Mau lanjut ajukan sewa di kos ini?"
    And user navigates to "mamikos /user/booking/"
    And user click on Draft menu
    And user click delete button on tab one draft booking
    And user click Hapus Draft on pop up delete draft booking
    Then tenant cannot see "OB kost promo ngebut" as kost name and kost location

  @TEST_BBM-887 @automated @kost-saya-revamp-phase1 @web @xray-update @bookingerror
  Scenario: [Homepage ][Kost Saya Section ]Check Kos saya section when Menunggu konfirmasi Total booking = 1 show section for Kost Promo Ngebut
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kos saya homepage waiting confirm probut"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost promo ngebut" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to "mamikos /"
    Then user can see shortcut homepage with "Pengajuan sewa lagi dicek pemilik"
    When user navigates to "mamikos /"
    And user navigates to "mamikos /user/booking/"
#    When user click lihat selengkapnya the riwayat booking
    And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
    Then tenant redirect to history booking page

  @TEST_BBM-915 @automated @kost-saya-revamp-phase2 @web @xray-update
  Scenario: [Kos Saya][Fasilitas]Check Lihat semua fasilitas (BBM-915)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB contract sect dbet"
    And user navigates to Profil page
    And tenant clicks on kost saya
    When user click Lihat informasi kos
    Then user will redirect to lihat informasi kos page
    And user will see kost type, kost name, and kost address
    And user will see Peraturan umum kos
    When user click Lihat semua fasilitas button
    Then user will see Fasilitas kamar
    And user will see Fasilitas kamar mandi
    And user will see Fasilitas umum
    And user will see parkir

  @TEST_BBM-522 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa] Activated Boleh Bawa Anak (BBM-522)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user click on owner popup
    Then user can see waktu mengelola section
    And user can see "Ubah Peraturan Masuk Kos" menu on dashboard
    When user click on Ubah peraturan kos menu
    Then user can see atur booking page
    When user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "boleh bawa anak"
    And button "kamar hanya bagi penyewa" will disable
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Peraturan kos with "Boleh bawa anak "
    And user logs out as a Tenant user
    # nonactive bawa anak
    And user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "boleh bawa anak"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-521 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated Wajib Buku Nikah (BBM-521)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "Wajib sertakan buku nikah"
    And button "kamar hanya bagi penyewa" will disable
    Then "Boleh untuk pasutri" is activated
    When user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-525 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]  Nonactived Buku Nikah (BBM-525)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    When user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "bisa pasutri"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-527 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated KK (BBM-527)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "KK"
    And button "kamar hanya bagi penyewa" will disable
    Then "Boleh bawa anak" is activated
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-523 @automated @booking-and-billing @booking-stay-setting @web @xray-update @bookingerror
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Nonactivated Ktp KK And Buku Nikah (BBM-523)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "KTP"
    And user activated "bisa pasutri"
    And user activated "boleh bawa anak"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-539 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated Ktp KK And Buku Nikah (BBM-539)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "KTP"
    And user activated "Buku Nikah"
    And button "kamar hanya bagi penyewa" will disable
    And user activated "Kartu Keluarga"
    And button "Maks 2 orang/kamar" will disable
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-540 @automated @booking-and-billing @booking-stay-setting @web @xray-update @bookingerror
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa] Non Activated Ktp, KK And Buku Nikah (BBM-540)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "KTP"
    And user activated "bisa pasutri"
    And user activated "boleh bawa anak"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-534 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Owner set kriteria to able for Boleh pasutri (BBM-534)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "bisa pasutri"
    And button "kamar hanya bagi penyewa" will disable
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Peraturan kos with "Boleh pasutri"

  @TEST_BBM-531 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]  Nonactivated Pasutri (BBM-531)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "bisa pasutri"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-524 @automated @booking-and-billing @booking-stay-setting @web @xray-update @bookingerror
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated Special Kos for Karyawan (BBM-524)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "special kos kriteria"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Peraturan kos with "Khusus karyawan"
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "special kos kriteria"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-533 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated Special Kos for Mahasiswa (BBM-533)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "special kos kriteria"
    And user select on Mahasiswa
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Peraturan kos with "Khusus Mahasiswa"

  @nonActiveKriteria @OB-7948
  #activatedSpecailKos.feature
  Scenario: Nonactivated Special Kos
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "special kos kriteria"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-541 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Atur Booking][Kost Detail][Booking section] Change booking terdekat = 2 Minggu and Booking waktu terjauh 2 bulan (BBM-541)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user click on owner popup
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user clicks on Ubah waktu button
    And user nonactive booking today toogle
    And user choose minim checkin time with "Minggu"
    And user choose minim checkin date with "2"
    And user clicks on Simpan button on update checkin time popup
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user logs out as a Owner user
    And user navigates to "mamikos /"
    And user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Ketentuan Kos with "Waktu mulai ngekos terdekat:" and "Waktu mulai ngekos terjauh:"
    And user click on checkinbox and can see "2 minggu setelah pengajuan sewa."
    When user navigates to "mamikos /"
    And user logs out as a Tenant user
		#    owner nonactive checkin time
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user click on owner popup
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user clicks on Ubah waktu button
    And user nonactive booking today toogle
    And user clicks on Simpan button on update checkin time popup
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-526 @automated @booking-stay-setting @partial-regression @web @web-covered @xray-update
  Scenario: [Ubah peraturan masuk kos][Filter] Change kos setting on kost GP type (BBM-526)
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user select kost GP "Kost Norway Tobelo Utara Halmahera Utara"
    And user click Simpan at Peraturan Masuk Kos page
    Then user will see toast Peraturan Masuk Kos

  @TEST_BBM-536 @automated @booking-stay-setting @toAutomate,partial-regression @web @web-covered @xray-update
  Scenario: [Dashboard][Pengajuan Booking][Ubah peraturan masuk kos]check button name on dashboard and pengajuan booking page (BBM-536)
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user click on owner popup
    And user click Ubah Peraturan Masuk Kos section at dashboard
    Then user redirect to Peraturan Masuk Kos page
    When user click Pengajuan Booking
    And user click Ubah Aturan at pengajuan booking page
    Then user redirect to Peraturan Masuk Kos page

  #1. Given user already in Reward List Management
  @TEST_BNB-5675 @automated @loyalty @reward-list @web @web-coverable @xray-update
  Scenario: [Loyalty Reward Management][Reward List]Reward List Management page display
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin ob"
    When user access to Reward List menu
    Then system display Reward List Management page
    And user filter reward name "Reward Automation"
    And user click button filter reward
    And user click button update reward
    And user click checkbox Active
    Then user click button update reward on page detail reward
    And system display success add reward type

  @TEST_BNB-5672 @automated @loyalty @reward-type @web @web-coverable @xray-update
  Scenario: [Loyalty Reward Management][Reward Type]Add Reward Type and check existing key
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin ob"
    When user access to Reward Type menu
    And user click button add reward type
    And user input reward type key "automation_key"
    And user input reward type name "Reward Automation"
    And user click save button reward type
    And system display error message "Key already exist."
    And user input reward type key "$%"
    And user click save button reward type
    And system display error message "The key may only contain letters, numbers, and dashes."

  @TEST_BBM-538 @automated @web @xray-update @validationBooking
  Scenario: [Kost Detail][Validation Pop Up]Check validation for booking when tenant not have Job (BBM-538)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant job validation"
    And user clicks search bar
    And I search property with name "booking kost ob" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user can see popup confirmation with title content
    And user can see subtitle content

  @TEST_BBM-529 @automated @web @xray-update @validationBooking @karyawanValidation
  Scenario: [Kost Detail][Validation Pop Up]Check validation for Booking kost khusus Mahasiswa when tenant profile as Karyawan (BBM-529)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant karyawan validation"
    And user clicks search bar
    And I search property with name "booking kost ob" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user can see popup confirmation karyawan with title content
    And user can see popup confirmation karyawan subtitle content

  @TEST_BBM-535 @automated @web @xray-update @mahasiswaValidation @validationBooking
  Scenario: [Kost Detail][Validation Pop Up]Check validation for Booking kost khusus Karyawan when tenant profile as Mahasiswa (BBM-535)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant mahasiswa validation"
    And user clicks search bar
    And I search property with name "OB kost mhs validation" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user can see popup confirmation mahasiswa with title content
    And user can see popup confirmation mahasiswa subtitle content

  @TEST_BBM-530 @automated @web @xray-update @kosMahasiswaTenantLainnya @validationBooking
  Scenario: [Kost Detail][Validation Pop Up]Check validation for Booking kost khusus Mahasiswa / Karyawan when tenant profile as Lainnya (BBM-530)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant lainnnya validation"
    And user clicks search bar
    And I search property with name "OB kost mhs validation" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user reach booking form