@occupancyAndBilling @OB @hentikanKontrak
Feature: Owner Hentikan Kontrak Sewa

  Scenario: Delete contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "ob additional price phone number"
    And user click on Cancel the Contract Button from action column

  @manageBooking @cancelBookingHentikanContract
  Scenario: Owner hentikan kontrak from Add tenant from owner
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    When user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

  @manageBooking
  Scenario: Tenant booking Tahunan for Kost not set DP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user select payment period "Per Bulan"
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking

  @test123
  Scenario: Owner accept booking from tenant
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    And user click on owner popup
    When user navigates to "mamikos /ownerpage/manage/all/booking"
    Then user select kost with name "Kost andalusia spanyol eropa timur"
    And user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button

  Scenario: Tenant pay boarding house for weekly rent
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user select payment method "OVO" for "OBAutomation"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

 @BNB-2525
 Scenario: Owner Checkout Contract when contract from booking funnel (BNB-2525)
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    And user navigates to Booking Request page
    And user click tab "Terbayar" and see contract
    And user click terminated contract
    And owner select reason terminate contract "Selesai Menyewa" and click terminate contract button
    Then system display warning message "Kontrak dihentikan" on billing details