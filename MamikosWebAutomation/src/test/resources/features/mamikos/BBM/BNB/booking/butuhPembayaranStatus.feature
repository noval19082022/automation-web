@occupancyAndBilling @OB @butuhPembayaranStatus
Feature: Butuh Pembayaran status

  @butuhPembayaranStatus @BNB-2176
  Scenario: Delete contract at backoffice
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "ob tenant status"
    And user click on Cancel the Contract Button from action column

#  Scenario: Delete All Need Confirmation Booking Request
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant status"
    And user navigates to "mamikos /user/booking/"
    And user cancel booking

  Scenario: Tenant booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant status"
    And user clicks search bar
    And I search property with name "booking kost ob" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user select payment period "Per Bulan"
    And user selects T&C checkbox and clicks on Book button
    Then user navigates to main page after booking

  Scenario: Owner accept booking from tenant
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user click on owner popup
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "kost reykjavik"
    And user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button
    Then user will redirect to pengajuan booking page

  Scenario: Tenant check butuh pembayaran status(BNB-2176)
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant status"
    And user navigates to "mamikos /user/booking/"
    And user click Filter and choose "Butuh Pembayaran"
    Then user will see booking with "Butuh Pembayaran" status