@regression @booking @manageBooking @occupancyAndBilling @butuhResponPengajuanSewaLabel
Feature: Chat Butuh Respon Pengajuan Sewa label

  @BNB-9960
  Scenario: Delete All Need Confirmation Booking Request
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant chat bth respon pngajuan sewa label"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

#  Scenario: tenant booking kost
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost booking additional" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    Then user navigates to main page after booking

#  Scenario: Owner accept booking from tenant
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    Then user click on owner popup
    And owner goes to Chat Page
    And owner clicked that tenant
    Then owner can see label with "Butuh respon pengajuan sewa"