@occupancyAndBilling @OB @kosDetail @ownerBadges
  Feature: Owner badges on Kos Detail

  @OwnerBadgesNotLogin @OwnerBadges @BBM-500
  Scenario: Check Owner Badges on Kos Detail when not login tenant (BBM-500)
    Given user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    Then user can see owner badges on kos detail

    @OwnerBadgesWithTenantLogin @OwnerBadges @BBM-498
    Scenario: Check Owner Badges on Kos Detail when login tenant (BBM-498)
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "ob additional price"
      And user clicks search bar
      And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
      Then user can see owner badges on kos detail

    @OwnerBadgesWithOwnerLogin @OwnerBadges @BBM-499
    Scenario: Check Owner Badges on Kos Detail when login owner (BBM-499)
      Given user navigates to "mamikos /"
      And user clicks on Enter button
      And user fills out owner login as "ob booking female" and click on Enter button
      And user navigates to "mamikos /"
      And user clicks search bar
      And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
      Then user can see owner badges on kos detail