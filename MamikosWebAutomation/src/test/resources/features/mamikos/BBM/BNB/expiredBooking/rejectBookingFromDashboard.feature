@BBM6
Feature: Reject Booking from dashboard owner

	Background:
		#@PRECOND_BBM-993
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "ob tenant input unique code"
		And user navigates to "mamikos /user/booking/"
		And tenant cancel all need confirmation booking request
		And user navigates to "mamikos /"
		And user clicks search bar
		And I search property with name "booking kost ob" and select matching result to go to kos details page
		And user select date "tomorrow" and rent type "Per bulan"
		And user clicks on Booking button on Kost details page
		And user selects T&C checkbox and clicks on Book button
		And user navigates to main page after booking
		And user logs out as a Tenant user

	@TEST_BBM-14 @booking-and-billing @owner-expired-booking @web
	Scenario: [Owner dashboard][Ada yang menunggu]Reject Booking via Homepage (more than 1 waiting booking)
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user fills out owner login as "ob reject booking" and click on Enter button
		And user click on owner popup
		Then user can see pengajuan sewa detail on dashboard
		When user click on reject booking
		Then user click on tolak button on cta
		And owner choose other reason with costum reason "Saya sudah ada yang punya"
		And user click on owner popup
		Then user can see pengajuan sewa detail on dashboard