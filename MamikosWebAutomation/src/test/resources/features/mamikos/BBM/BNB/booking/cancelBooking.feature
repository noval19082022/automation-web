  @booking  @cancelBooking @cancelBookingWithLainnya @OB/BX @essentiaTest
  Feature: OB Cancel Booking

    @cancelBookingTenant @MB-2254
    Scenario: Booking kost and tenant cancel booking check redirection and notification
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "OB Cancel Tenant"
      And user clicks search bar
      And I search property with name "ob owner reject" and select matching result to go to kos details page
      And user choose booking date for tomorrow and proceed to booking form
      And user selects T&C checkbox and clicks on Book button
      And user click Chat Pemilik Kos
#      And user click close chat box
      And user navigates to "mamikos /user/booking/"
      #@MB-2254
      #Step below contains assertion for MB-2254
      When user click lihat selengkapnya the riwayat booking
      And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
      And user click Ok button on success cancel booking pop up
      Then tenant redirect to history booking page
      And tenant get new notification and with text "Booking berhasil dibatalkan"
      And user check status booking
      And user check reason cancellation "Merasa tidak cocok/tidak sesuai kriteria"

    @MB-2260 @cancelBookingTenant
    Scenario: Owner Check Notification After Tenant Cancel Booking
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user fills out owner login as "ob additional price" and click on Enter button
      And user click on owner popup
      Then owner can sees new notification
      And owner can sees first list notification title text contain "Booking dibatalkan untuk"
      When owner click on first list notification
      Then owner redirected to booking manage page

    @cancelBookingWithLainnya @MB-5230
    Scenario: Booking kost and tenant cancel booking with reason lainnya
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "OB Cancel Tenant"
      And user clicks search bar
      And I search property with name "booking kost ob" and select matching result to go to kos details page
      And user clicks on Booking button on Kost details page
      And user fills out Rent Duration equals to 4 Bulan, and clicks on Next button
      Then booking confirmation screen displayed with Kost name, Room price for cancel
      And user selects T&C checkbox and clicks on Book button
      And user click Chat Pemilik Kos
      And user click close chat box
      And user navigates to "mamikos /user/booking/"
      And user cancel booking with Lainnya and enter text "maaf saya batalkan booking ini" in lainnya box
      And user navigates to "mamikos /user/booking/"
      And user check status booking
      Then reason cancellation lainnya will appears "maaf saya batalkan booking ini"

    @MB-2571
    Scenario: Cancel Booking With Lainnya Reason From Admin
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "ob additional price"
      And user clicks search bar
      And I search property with name "ob owner reject" and select matching result to go to kos details page
      And user choose booking date for tomorrow and proceed to booking form
      And user selects T&C checkbox and clicks on Book button
      And user navigates to main page after booking
      And user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin consultant"
      And user access to data booking menu
      And user show filter data booking
      And user search data booking using tenant phone "additional price ob"
      And user apply filter
      And admin proceed to cancel booking
      And admin cancel booking with "Lainnya" and reason is "Saya cancel lewat admin"

    @MB-2571
    Scenario: Tenant Check Cancel Reason On Booking History After Cancel From Admin
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "ob additional price"
      And user navigates to Booking History page
      Then user check booking status is canceled with reason "Saya cancel lewat admin"