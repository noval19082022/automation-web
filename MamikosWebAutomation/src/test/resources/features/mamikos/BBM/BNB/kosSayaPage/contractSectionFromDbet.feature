@regression @booking @manageBooking @occupancyAndBilling1 @contentKosSaya
Feature: contract section from DBET

  Background:
    #@PRECOND_BNB-10913
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB contract sect dbet"
    And user navigates to Profil page

  @TEST_BNB-10914 @automated @booking-and-billing @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Kost saya][Content] Content Kos Saya
    And tenant clicks on kost saya
    Then user see title kos saya page
    And user see welcome section at kos saya
    When user click Lihat informasi kos
    Then user will redirect to informasi kos page
    When user see review kos section
    And user see Aktivitas di Kos Saya
    Then user see Tagihan kos, Kontrak, Chat pemilik, Bantuan, Forum

  @TEST_BNB-8587 @automated @kost-saya-revamp-phase1 @partial-regression @web @xray-update
  Scenario: [Kost saya][Kontrak]Check kontrak section when tenant has contract from dbet
    When user click Kontrak section
    Then user redirect to Kontrak page
    And user check kost type, kost name, kost address, and room number
    And user check total biaya, harga sewa, and biaya lain
    And user check tanggal penagihan
    And user check mulai sewa, durasi sewa, and sewa berakhir
    And user also check denda
    And user see Ajukan berhenti button and user click this button
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
    And user click on Kost name search
    And user search kost "kost bali for contract section Tobelo Utara Halmahera Utara"
    And user click Selengkapnya button on "Tenant Coba Pop Up Empat" contract
    And user click tab Kontrak sewa
    And user click Ubah kontrak penyewa button
    Then user check prices at penyewa owner are same to contract at kos saya