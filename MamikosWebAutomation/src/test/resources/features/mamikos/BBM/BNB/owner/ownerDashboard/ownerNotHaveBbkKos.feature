@regression @ownerDashboard @occupancyAndBilling @manageBooking @OB
Feature: OB Owner Dashboard

  @checkWaktuMengelolaWhenOwnerNotHaveBbkKos @BNB-4433
    Scenario: Check Waktu Mengelola section when owner not have BBK kos (BNB-4433)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "Owner Not Bbk" and click on Enter button
    Then user can see waktu mengelola section
    When user clicks widget set available room
    Then user see screen update room
    And user click on Home menu
    Then user can see waktu mengelola section
    And user clicks widget set price
    Then user can see set price page
    And user click on Home menu
    Then user can see waktu mengelola section
    And user can see booking register widget
    And user cliks widget booking register
    And user can see manage direct booking popup
    And user click on Home menu
    Then user can see waktu mengelola section
    And user click on widget Penyewa
    Then user can see booking benefit page
    And user clicks on Owner Settings button
    And user click on Home menu
    Then user can see waktu mengelola section
    And user click Add Tenant Contract button in dashboard page
    Then user can see booking benefit page
    And user clicks on Owner Settings button
    And user click on Home menu
    Then user can see waktu mengelola section
    And user click on widget help center
    Then user can see help center page