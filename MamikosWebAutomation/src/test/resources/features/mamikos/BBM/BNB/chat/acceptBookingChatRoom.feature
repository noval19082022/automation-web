@occupancyAndBilling @OB @BBM5 @BBM-5 @acceptFromChat
Feature: Accept Booking from Chat room

  Scenario: Delete contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "bbm accept from chat phone number"
    And user click on Cancel the Contract Button from action column

  @manageBooking @cancelBookingHentikanContract
  Scenario: Owner hentikan kontrak from Add tenant from owner
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "bbm accept from chat"
    When user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

  @manageBooking
  Scenario: Tenant booking Tahunan for Kost not set DP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "bbm accept from chat"
    And user clicks search bar
    And I search property with name "Kost Automation new" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user select payment period "Per Bulan"
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking

  @acceptBooking
  Scenario: Owner accept booking from Chat and see label on owner’s chat is “Belum Bayar Sewa Pertama (BBM-5)
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user fills out owner login as "ob booking female" and click on Enter button
    And user click on owner popup
    And user click chat button in top bar
    And owner user click chat from "Tenant Automation Accept Chat" in chatlist
    And user clicks on Accept button from chat room
    And select first room available and clicks on next button
    And user click save button
    And owner user navigates to owner page
    And user click chat button in top bar
    And owner user click chat from "Tenant Automation Accept Chat" in chatlist
    Then system display title "Belum bayar sewa pertama" after accept booking from chat room

#  Scenario: If label on owner’s chat is “Belum Bayar Sewa Pertama
    When Owner can see name of Tenant is "Tenant Automation Accept Chat"
    And Owner can see Kost name, harga kos, sisa kamar
    Then Owner can see button ubah kamar