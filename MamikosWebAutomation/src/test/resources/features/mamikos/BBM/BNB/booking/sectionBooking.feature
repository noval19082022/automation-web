@regression @booking @sectionBooking @manageBooking @OB
Feature: OB Section Booking

  @sectionBooking
  Scenario: Check section booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "master"
    And user clicks search bar
    And I search property with name "sectionBooking" and select matching result to go to kos details page
    Then I should reached kos detail page
    Then kost detail screen displayed with Remaining room, Room price, Owner kost
