@regression @manageBooking @occupancyAndBilling1 @ownerRejectBooking @OB/BX @rejectBooking
Feature: OB Reject Booking

Background:
  Given user navigates to "mamikos /"
  When user clicks on Enter button
  And user login in as Tenant via phone number as "ob owner reject booking"
  # check if tenant still has a booking need confirmation
  And user navigates to "mamikos /user/booking/"
  And tenant cancel all need confirmation booking request

  @MB-2331
  Scenario: OB Owner Reject Check Term And Condition Behavior
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "ob owner reject" and select matching result to go to kos details page
    And user create booking for today
    And user select payment period "Per Bulan"
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "owner test reject"
    And owner process to reject booking request
    Then owner clicks on T&C and redirect to Term And Condition Page
    And owner can sees Term And Condition check box is not tick
    When owner inputs other reason with "Saya sudah ada yang punya" and tick on Term And Condition check box
    And user click on close icon on popup reject
    And user activated rejected booking request filter