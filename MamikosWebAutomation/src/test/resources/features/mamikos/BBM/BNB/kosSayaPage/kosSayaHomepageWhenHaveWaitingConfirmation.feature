@regression @booking @manageBooking @occupancyAndBilling1 @kostSayaOnHomepage @draftAndWaiting
Feature: Kost Saya on Homepage

#  Background:
#    Given user navigates to "mamikos /"
#    When user clicks on Enter button
#    And user fills out owner login as "ob reject booking" and click on Enter button
#    And user navigates to "owner /ownerpage/kos"
#    And owner search "kost madiun buat draft homepage Tobelo Utara Halmahera Utara" and click see complete button
#    And owner click on update room
#    And owner click on edit button on room "Terisi"
#    And owner tick on already occupied tickbox and click on update room
#    And user logs out as a Owner user

  @TEST_BNB-8235 @automated @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Homepage ][Kost Saya Section ]Check Kos Saya on Homepage when have Draft booking = 1
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya homepage"
    And user navigates to "mamikos /user/booking/"
    And user click on Draft menu
    And user click delete draft if any
    Then tenant cannot see "kost madiun buat draft homepage Tobelo Utara Halmahera Utara" as kost name and kost location
    When user navigates to "mamikos /"
    And user click Mau Coba Dong section at homepage
    Then user can see empty state kost saya
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob draft n waiting homepage" and select matching result to go to kos details page
    And user create booking for today
    And user click exit button
    And user check confirmation text title
    And user check confirmation text body
    And user click Save Draft
    Then user redirect to kost detail
    When user navigates to "mamikos /"
    Then user can see shortcut homepage with "Mau lanjut ajukan sewa di kos ini?"
    And user navigates to "mamikos /user/booking/"
    And user click on Draft menu
    And user click delete button on tab one draft booking
    And user click Hapus Draft on pop up delete draft booking
    Then tenant cannot see "kost madiun buat draft homepage Tobelo Utara Halmahera Utara" as kost name and kost location

  @TEST_BNB-8239 @automated @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Homepage ][Kost Saya Section ]Check homepage when have total waiting confirmation booking = 1
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kos saya homepage waiting confirm"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob draft n waiting homepage" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to "mamikos /"
    Then user can see shortcut homepage with "Pengajuan sewa lagi dicek pemilik"
    When user navigates to "mamikos /"
    And user navigates to "mamikos /user/booking/"
    And user click lihat selengkapnya the riwayat booking
    And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
    Then tenant redirect to history booking page

  @TEST_BNB-8237 @automated @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Homepage ][Kost Saya Section ]Check Draft booking on homepage when have 1 draft for kost Promo Ngebut
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kos saya homepage draft booking probut"
    And user navigates to "mamikos /user/booking/"
    And user click on Draft menu
    And user click delete draft if any
    Then tenant cannot see "OB kost promo ngebut" as kost name and kost location
    And user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost promo ngebut" and select matching result to go to kos details page
    And user create booking for today
    And user click exit button
    And user check confirmation text title
    And user check confirmation text body
    And user click Save Draft
    Then user redirect to kost detail
    When user navigates to "mamikos /"
    And user check promo ngebut label
    Then user can see shortcut homepage with "Mau lanjut ajukan sewa di kos ini?"
    And user navigates to "mamikos /user/booking/"
    And user click on Draft menu
    And user click delete button on tab one draft booking
    And user click Hapus Draft on pop up delete draft booking
    Then tenant cannot see "OB kost promo ngebut" as kost name and kost location

  @TEST_BNB-8241 @automated @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Homepage ][Kost Saya Section ]Check Kos saya section when Menunggu konfirmasi Total booking = 1 show section for Kost Promo Ngebut
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kos saya homepage waiting confirm probut"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost promo ngebut" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to "mamikos /"
    Then user can see shortcut homepage with "Pengajuan sewa lagi dicek pemilik"
    When user navigates to "mamikos /"
    And user navigates to "mamikos /user/booking/"
    When user click lihat selengkapnya the riwayat booking
    And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
    Then tenant redirect to history booking page