@regression @manageBooking @occupancyAndBilling @OB
Feature: Kos saya revamp at owner penyewa

  @BNB-8926
  Scenario: check warning who don't have kos saya at Semua filter
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
    And user click on Kost name search
    And user search kost "banda"
    Then user will see wording of warning tenant who don't have kos saya at Semua filter
    When user owner click Siapa saja penyewa yang belum hyperlink
    Then user will redirect to Sedang menyewa filter and user will see wording of warning tenant who don't have kos saya