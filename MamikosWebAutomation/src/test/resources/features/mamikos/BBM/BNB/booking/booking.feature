@booking @bookingFeature @bookingFullFlow @OB @essentiaTest
Feature: OB Booking Full Flow

  #Deleting existing booking
  Background:
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search for Kost with name
    And user click on Cancel the Contract Button from action column

  @bookingNavigation @regression @manageBooking
  Scenario: Go back from booking page
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "master"
    And user clicks search bar
    And I search property with name "male_test_data" and select matching result to go to kos details page
    And user clicks on Booking button on Kost details page
    And user select "tomorrow" date as booking date
    And user click back button in Booking form page
    Then tenant can see pop-up booking cancel confirmation should appear

  @mainflow
  Scenario: Booking a Kost as a Tenant , approved by owner successfully
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "ob booking flow"
    And user clicks search bar
    And I search property with name "ob booking female" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    And user fills out Rent Duration equals to 4 Bulan, and clicks on Next button
    Then booking confirmation screen displayed with  Kost name ,Room price , Duration  ,Tenant Name  , Gender  , Phone
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    And user navigates to Booking Request page
    And user select kost with name "Kost Princess"
    Then booking is displayed with "female gender name" ,Status , Room name  ,Checkin Date as tomorrow date, Duration on Booking listing page
    When user clicks on Booking Details button
    Then booking is displayed with Booking ID starting with "#MAMI" , Room name , Room price  , Duration  , "female gender name"  , Phone, Status  on Booking details page
    When user clicks on Accept button
    And select first room available and clicks on next button
    And user enters deposite panalty info as deposite fee "100" , panalty fee "200" , panalty fee duration "1" , panalty fee duration type "Bulan" and click on Save
    And owner user navigates to owner page
    And user logs out as a Owner user
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "ob booking flow"
    When user navigates to Booking History page
    Then booking is displayed with Status , Room name, Duration  Payment expiry time on Booking listing page

  @deleteBaruDilihatForNextBooking
  Scenario: Delete Baru Dilihat Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "ob booking flow"
    And user navigates to Booking History page
    And user clicks on Baru Dilihat section
    And I click on trash icon
    And I clicks on Batalkan button
    And I click on trash icon
    And I clicks on Hapus button