Feature: [Loyalty Reward Management][Reward Type]Add Reward Type and check existing key

	#1. Given user already in Reward Type
	@TEST_BNB-5672 @automated @loyalty @reward-type @web @web-coverable @xray-update
	Scenario: [Loyalty Reward Management][Reward Type]Add Reward Type and check existing key
		Given user navigates to "mamikos admin"
		And user logs in to Mamikos Admin via credentials as "admin ob"
		When user access to Reward Type menu
		And user click button add reward type
		And user input reward type key "automation_key"
		And user input reward type name "Reward Automation"
		And user click save button reward type
		And system display error message "Key already exist."
		And user input reward type key "$%"
		And user click save button reward type
		And system display error message "The key may only contain letters, numbers, and dashes."
