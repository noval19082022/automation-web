@OB @validationBooking
Feature: Check validation booking

  @TEST_BBM-538 @automated @web @xray-update @validationBooking
  Scenario: [Kost Detail][Validation Pop Up]Check validation for booking when tenant not have Job (BBM-538)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant job validation"
    And user clicks search bar
    And I search property with name "booking kost ob" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user can see popup confirmation with title content
    And user can see subtitle content

  @genderValidation
  Scenario: Go back from booking page
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant empty state"
    And user clicks search bar
    And I search property with name "booking kost ob" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user can see popup confirmation gender with title content
    And user can see popup confirmation gender subtitle content

  @TEST_BBM-529 @automated @web @xray-update @validationBooking @karyawanValidation
  Scenario: [Kost Detail][Validation Pop Up]Check validation for Booking kost khusus Mahasiswa when tenant profile as Karyawan (BBM-529)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant karyawan validation"
    And user clicks search bar
    And I search property with name "booking kost ob" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user can see popup confirmation karyawan with title content
    And user can see popup confirmation karyawan subtitle content

  @TEST_BBM-535 @automated @web @xray-update @mahasiswaValidation @validationBooking
  Scenario: [Kost Detail][Validation Pop Up]Check validation for Booking kost khusus Karyawan when tenant profile as Mahasiswa (BBM-535)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant mahasiswa validation"
    And user clicks search bar
    And I search property with name "OB kost mhs validation" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user can see popup confirmation mahasiswa with title content
    And user can see popup confirmation mahasiswa subtitle content

  @kosKaryawanTenantLainnya @BNB-7993
  Scenario:Check validation for Booking kost khusus Karyawan when tenant profile as Lainnya (BNB-7995)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant lainnnya validation"
    And user clicks search bar
    And I search property with name "booking kost ob" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user reach booking form

  @TEST_BBM-530 @automated @web @xray-update @kosMahasiswaTenantLainnya @validationBooking
  Scenario: [Kost Detail][Validation Pop Up]Check validation for Booking kost khusus Mahasiswa / Karyawan when tenant profile as Lainnya (BBM-530)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB tenant lainnnya validation"
    And user clicks search bar
    And I search property with name "OB kost mhs validation" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    Then user reach booking form