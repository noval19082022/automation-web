@regression @manageBooking @riwayatKost
Feature: riwayat kost

  Scenario: check riwayat list and riwayat detail
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB Cancel Tenant"
    And user navigates to Booking History page
    And user click Riwayat Kos menu
    Then user will see Riwayat Kos page
    When user click review kost
    Then user will see review page and user click close on review page
    When user click Lihat Detail
    And user click Lihat Fasilitas and close pop up fasilitas
    And user click review kost on riwayat kost detail
    And user will see review page and user click close on review page
    And user click Lihat Riwayat Transaksi and user click Kembali ke Booking button
    And user click Chat Pemilik
    Then user will see Chat room opened and close Chat room
    When user click Booking Ulang
    Then user will open new tab and go to Booking form

   @emptyRiwayatKos
   Scenario: Check riwayat kos when empty condition
     Given user navigates to "mamikos /"
     When user clicks on Enter button
     And user login in as Tenant via phone number as "OB tenant empty state"
     And user navigates to Booking History page
     And user click Riwayat Kos menu
     Then user will see empty state