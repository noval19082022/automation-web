@regression @booking @manageBooking @priceAndRentDuration @OB
Feature: OB Price And Rent Duration

  Background:
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @clickYesButtonBookingCancelAndStillSubmitButton
  Scenario: Click on yes terminated button pop-up cancel booking and still submit button
    When user logs in as Tenant via Facebook credentails as "gender male"
    And user clicks search bar
    And I search property with name "male_test_data" and select matching result to go to kos details page
    Then I should reached kos detail page
    And tenant should see monthly rent duration
    When user clicks on Booking button on Kost details page
    And user select "tomorrow" date as booking date
    Then tenant can see Per Month is selected
    And tenant should not see daily rent duration
    When user click back button in Booking form page
    Then tenant can see pop-up booking cancel confirmation should appear
    When tenant click on Still submit button
    Then tenant can see booking form page
    When user click back button in Booking form page
    And tenant click on Save Draft button
    And tenant click on Lihat button to go to draft booking page
    Then tenant should reached draft booking page
    And tenant can see "Kost Bearger, Depok, Kabupaten Sleman" as kost name and kost location

    @checkRentPriceCount
    Scenario: Check rent price count
      When user logs in as Tenant via Facebook credentails as "gender female"
      And user clicks search bar
      And I search property with name "ob mix kost" and select matching result to go to kos details page
      Then I should reached kos detail page
      And tenant should see monthly rent duration
      When user clicks on Booking button on Kost details page
      And user select "tomorrow" date as booking date
      And tenant should not see daily rent duration
      Then tenant can see rent price list
        | Per Minggu   |
        | Per Bulan    |
        | Per 3 Bulan  |
        | Per 6 Bulan  |
        | Per Tahun    |

    @chooseRentPriceCount
    Scenario: Choose rent price count
      When user logs in as Tenant via Facebook credentails as "gender female"
      And user clicks search bar
      And I search property with name "ob mix kost" and select matching result to go to kos details page
      Then I should reached kos detail page
      And tenant should see monthly rent duration
      When user clicks on Booking button on Kost details page
      And user select "tomorrow" date as booking date
      Then tenant can click on rent price count available and can see additional information
        |   Dibayar seminggu sekali  |
        |   Dibayar sebulan sekali   |
        |   Dibayar 3 Bulan sekali   |
        |   Dibayar 6 Bulan sekali   |
        |   Dibayar setahun sekali   |
      And tenant can see duration updated according to selected rent count
        | 1 Minggu |
        | 1 Bulan  |
        | 3 Bulan  |
        | 6 Bulan  |
        | 1 Tahun  |
      And tenant can see price is equal in booking overview
      And tenant can see time to pay the bill is contain words to the selected duration
        | Per Minggu   |
        | Per Bulan    |
        | Per 3 Bulan  |
        | Per 6 Bulan  |
        | Per Tahun    |

  @deleteBaruDilihatPriceAndRentDuration
  Scenario Outline: Delete Baru Dilihat Price And Rent Duration
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "<login data>"
    And user navigates to "mamikos /last-seen"
    And I click on trash icon
    And I clicks on Batalkan button
    Then user can see confirmation delete popup
    And I click on trash icon
    And I clicks on Hapus button
    And user logs out as a Tenant user
    Examples:
      | login data     |
      | gender male    |
      | gender female  |