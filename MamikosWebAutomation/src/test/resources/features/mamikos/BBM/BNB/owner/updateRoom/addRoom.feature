@regression @manageBooking @addRoom
Feature: Add room and mark room at Kos GP

  @addAndMarkRoomKosGP @BNB-2245
  Scenario: check when owner add and mark room at kos GP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    And owner goes to my property kos menu
    And owner search "mark room occupied" and click see complete button
    And owner click on update room
    And user click add room in room list
    And user fill room name in room allotment page with "Jupiter"
    And user fill room floor in room allotment page with "11011"
    And user tick on tickbox and click on Tambah kamar
    Then toast will be appears and the room is untick again