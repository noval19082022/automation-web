@regression @booking @manageBooking @occupancyAndBilling1 @kostSayaOnHomepage @cancelAndDraft
Feature: Kost Saya on Homepage - Cancel and draft booking

  Background:
    #@PRECOND_BNB-10915
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya homepage"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

  @TEST_BNB-8250 @Automated @kost-saya-revamp-phase1 @web @xray-update
  Scenario: [Home Page][Kost Saya Section ]Check kost saya section when have Cancelled status and have Draft booking
    Given user navigates to "mamikos /"
		#    When user clicks on Enter button
		#    And user login in as Tenant via phone number as "ob kost saya homepage"
		#    And user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "ob kost saya homepage cancel" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost promo ngebut" and select matching result to go to kos details page
    And user create booking for today
    And user click exit button
    And user click Save Draft
    Then user redirect to kost detail
    When user navigates to "mamikos /"
    Then user can see shortcut homepage with "Pengajuan sewa lagi dicek pemilik"
    When user navigates to "mamikos /user/booking/"
    When user click lihat selengkapnya the riwayat booking
    And user cancel booking with reason "Merasa tidak cocok/tidak sesuai kriteria"
    Then tenant redirect to history booking page
    When user navigates to "mamikos /"
    Then user can see shortcut homepage with "Mau lanjut ajukan sewa di kos ini?"
    When user navigates to "mamikos /user/booking/"
    And user click on Draft menu
    And user click delete button on tab one draft booking
    And user click Hapus Draft on pop up delete draft booking
    And user navigates to "mamikos /"
    And user click Mau Coba Dong section at homepage
    Then user redirect to kos saya page