@regression @booking @manageBooking @baruDilihat @OB/BX
Feature: OB Baru Dilihat

  Background:
    Given user navigates to "mamikos /"
    When user clicks on Enter button

  @deleteKostBaruDilihat
  Scenario: Delete kost Baru Dilihat
    And user login in as Tenant via phone number as "OB Baru Dilihat"
    And user navigates to "mamikos /last-seen"
    And I click on trash icon
    And I clicks on Batalkan button
    Then user can see confirmation delete popup
    And I click on trash icon
    And I clicks on Hapus button

  @checkBlankBaruDilihatSection
  Scenario: Check empty data on Baru dilihat
    And user login in as Tenant via phone number as "OB Baru Dilihat"
    And user navigates to "mamikos /last-seen"
    And tenant delete all newly seen from the list
    Then user can see Empty Baru Dilihat information and can clicks on Cari Kost button
    And user logs out as a Tenant user

  @addBaruDilihatKostandCheckLihatDetail @BNB-2191
  Scenario: Add baru dilihat, Lihat Detail and Booking langsung to Draft Booking (BNB-2191)
    And user login in as Tenant via phone number as "OB Baru Dilihat"
    And user clicks search bar
    And I search property with name "baruDilihat" and select matching result to go to kos details page
    Then I should reached kos detail page
    And user navigates to "mamikos /last-seen"
    Then user can see Kost name, rent count type for Bulanan, Tanggal masuk, durasi sewa on baru dilihat page
    And user clicks on Lihat detail button
    Then I should reached kos detail page
    And I should reached kos baru dilihat page
    And user navigates to "mamikos /last-seen"
    And I clicks on Booking Langsung button
    Then I can see Booking form page
    And I clicks on Kembali button
    And I clicks on Simpan Draft button
    And user navigates to "mamikos //user/booking/draft"
    Then user can see Kost name, rent count type for Bulanan, Tanggal masuk, durasi sewa on draft page
    And user logs out as a Tenant user

  @bookingLangsungForDifferentGenderKost
  Scenario Outline: Booking Langsung for Different Gender Kost
    And user logs in as Tenant via Facebook credentails as "<login data>"
    And user clicks search bar
    And I search property with name "<kost data>" and select matching result to go to kos details page
    Then I should reached kos detail page
    And user navigates to "mamikos /last-seen"
    And I clicks on Booking Langsung button
    Then I should reached kos detail page
    Then tenant can see confirmation popup different gender with information "<text information>"
    And tenant click on close icon
    Then I should reached kos detail page
    And user navigates to "mamikos /last-seen"
    And I click on trash icon
    And I clicks on Batalkan button
    Then user can see confirmation delete popup
    And I click on trash icon
    And I clicks on Hapus button
    And user logs out as a Tenant user
    Examples:
      | login data         | kost data          | kost type    | text information                 |
      | gender female      | male_test_data     | Putra        | Mohon Maaf, Ini Kos Khusus Pria  |
      | gender male        | female_test_data   | Putri        | Mohon Maaf, Ini Kos Khusus Wanita |

  @bookingKosPenuhFromEntryPointBaruDilihat @mb-2102
  Scenario: Booking Kost Penuh form entry point Baru Dilihat
    And user logs in as Tenant via Facebook credentails as "gender male"
    And user clicks search bar
    And I search property with name "kost penuh" and select matching result to go to kos details page
    Then I should reached kos detail page
    And user navigates to "mamikos /last-seen"
    And user can see available room
    Then I can see disable button for Kost Penuh