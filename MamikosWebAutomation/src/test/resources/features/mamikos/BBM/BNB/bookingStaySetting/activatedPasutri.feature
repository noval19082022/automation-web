@occupancyAndBilling @OB @kosDetail @bookingDataSetting @kosPasutri
Feature: Owner Booking Data Setting Pasutri setting for Kost Pasutri

  @TEST_BB-534 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Owner set kriteria to able for Boleh pasutri
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "bisa pasutri"
    And button "kamar hanya bagi penyewa" will disable
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Peraturan kos with "Boleh pasutri"

  @TEST_BBM-531 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]  Nonactivated Pasutri (BNB-7951)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "bisa pasutri"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"