@downPayment @occupancyAndBilling1 @OB
Feature: Booking kost with DP Tidak Wajib

  @manageBooking @bookingDpTidakWajib
  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    When user navigates to "mamikos /user/booking/"
    And user cancel booking

  @manageBooking @bookingDpTidakWajib
  Scenario: Owner setting DP with Tidak Wajib
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    And user clicks on down payment toggle
    Then user can see DP popup
    When  user can see "Apakah Anda ingin mewajibkan DP untuk seluruh calon penyewa?" on dp pop up
    Then user can see default setting is "Tidak Wajib" and see "Jika tidak wajib, calon penyewa bisa memilih pembayaran dengan DP atau pembayaran penuh."
    And user clicks save button on additional price
    And user see "DP tidak wajib" on atur page

  @manageBooking @bookingDpTidakWajib @BNB-5107
  Scenario: Tenant booking kost DP with Tidak Wajib "Per 3 bulan"
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    Then user can see "Bisa bayar DP (uang muka) dulu" and "Biaya DP adalah 10% dari biaya yang dipilih." on kost detail
    And user select date "tomorrow" and rent type "Per 3 bulan"
    Then user can see Jika kamu membayar dengan DP, Uang Muka (DP), Pelunasan, Jika kamu pakai pembayaran penuh, Total Pembayaran pertama, Bisa bayar pakai DP "tiga bulan" and user validates the value
    And user clicks on Uang Muka
    And user can see Uang muka, biaya layanan mamikos, Total Pembayaran pertama on popup uang muka detail and user validates the value "tiga bulan"
    And user click button close popup
    When user clicks on Pelunasan
    And user can see pelunasan, biaya layanan mamikos, Total Pembayaran pertama on popup pelunasan detail and user validates the value "tiga bulan"
    When user clicks on Pembayaran penuh
    And user can see pembayaran penuh, biaya layanan mamikos, Total Pembayaran pertama on popup pembayaran penuh detail and user validates the value "tiga bulan"
    And user click button close popup
    And user clicks on Booking button on Kost details page
    And user select payment period "Per 3 Bulan"
    And user can see DP information with "Uang muka (DP)", "10% dari harga sewa", total DP on pengajuan sewa section "tiga bulan"
    And user click button close popup
    Then user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking

  @manageBooking @bookingDpTidakWajib
  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    When user navigates to "mamikos /user/booking/"
    And user cancel booking

  @manageBooking @bookingDpTidakWajib
  Scenario: Tenant booking kost DP with Tidak Wajib "Per minggu"
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    Then user can see "Bisa bayar DP (uang muka) dulu" and "Biaya DP adalah 10% dari biaya yang dipilih." on kost detail
    And user select date "tomorrow" and rent type "Per minggu"
    Then user can see Jika kamu membayar dengan DP, Uang Muka (DP), Pelunasan, Jika kamu pakai pembayaran penuh, Total Pembayaran pertama, Bisa bayar pakai DP "per minggu" and user validates the value
    And user clicks on Uang Muka
    And user can see Uang muka, biaya layanan mamikos, Total Pembayaran pertama on popup uang muka detail and user validates the value "per minggu"
    And user click button close popup
    When user clicks on Pelunasan
    And user can see pelunasan, biaya layanan mamikos, Total Pembayaran pertama on popup pelunasan detail and user validates the value "per minggu"
    When user clicks on Pembayaran penuh
    And user can see pembayaran penuh, biaya layanan mamikos, Total Pembayaran pertama on popup pembayaran penuh detail and user validates the value "per minggu"
    And user click button close popup
    And user clicks on Booking button on Kost details page
    And user select payment period "Per Minggu"
    And user can see DP information with "Uang muka (DP)", "10% dari harga sewa", total DP on pengajuan sewa section "per minggu"
    And user click button close popup
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking

  @manageBooking @bookingDpTidakWajib
  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    When user navigates to "mamikos /user/booking/"
    And user cancel booking

  @manageBooking @bookingDpTidakWajib
  Scenario: Delete Down Payment
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    Then user delete active down payment

  @manageBooking @bookingDpTidakWajib
  Scenario: Tenant booking Tahunan for Kost not set DP
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    And user select date "tomorrow" and rent type "Per tahun"
    Then user can see detail payment for rent type "Per tahun" not set DP
    And user clicks on total pembayaran pertama
    And user can see detail payment on pop up total pembayaran "Per tahun" non set DP
    And user click button close popup
    And user clicks on Booking button on Kost details page
    And user select payment period "Per Tahun"
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user navigates to "mamikos /user/booking/"
    And user cancel booking
