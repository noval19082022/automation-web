@BBM5
Feature: BnB feature with background go to kos saya page

  Background:
    #@PRECOND_BBM-1003
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant input unique code"
    And user navigates to Profil page
    Then user redirect to kos saya page

  @TEST_BBM-897 @automated @kost-saya-revamp-phase3 @partial-regression @web @xray-update
  Scenario: [Tenant side - Kost saya][Input Unique Code]Tenant input with valid unique code if has different phone number with owner (BBM-897)
    When user click Masukkan kode dari pemilik button
    And user input valid unique code "0QQ12E" and click Kirim Kode unik button
    Then user check verification tenant phone number at owner and tenant phone number at kos saya

  @TEST_BBM-892 @automated @kost-saya-revamp-phase3 @web @xray-update
  Scenario: [Tenant side - Kost saya][Verify phone number]Tenant input valid unique code if tenant not verification phone number, but tenant Cancel verification phone number
    When user click Masukkan kode dari pemilik button
    And user input valid unique code "0QQ12E" and click Kirim Kode unik button
    Then user check verification tenant phone number at owner and tenant phone number at kos saya
    When user click Kirim OTP button
    And user click verification via SMS
    Then user will see OTP fields
    When user click Back on OTP page
    And user click All Back button until first page
    Then user will see kos saya is still empty