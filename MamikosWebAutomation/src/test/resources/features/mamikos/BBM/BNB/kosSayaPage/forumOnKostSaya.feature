@regression @booking @manageBooking @occupancyAndBilling @kostSaya
Feature: Check Forum on Kost Saya

  @TEST_BNB-8613 @Automated @kost-saya-revamp-phase2 @xray-update
  Scenario: [Kost Saya][Forum ]When tenant click Forum and not interested
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB contract sect dbet"
    And user navigates to Profil page
    And tenant clicks on kost saya
    And user clicks on forum menu
    Then user will see pop up for upcoming forum feature
    When user clicks on Oke button
    And user clicks on forum menu
    And user tick on checkbox pop up upcoming
    And user clicks on forum menu
    Then user will see information about upcoming forum feature