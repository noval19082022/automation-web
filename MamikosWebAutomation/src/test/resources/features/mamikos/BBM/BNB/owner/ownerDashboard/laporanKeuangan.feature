@regression @ownerDashboard @occupancyAndBilling @manageBooking @OB
Feature: Check content Laporan Keuangan

  @checkContentLaporanKeuangan @BBM-974
  Scenario: Check content Laporan Keuangan(BBM-974)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user click on owner popup
    Then user can see pendapatan section
    And user click on lihat laporan keuangan
    Then user can see "Buka Laporan Keuangan di Aplikasi" and "Untuk saat ini, fitur Laporan Keuangan hanya dapat digunakan di aplikasi Mamikos di Android dan iOS."
    And user click on Home menu
    And user click on Laporan keuangan on manajemen kos menu
    Then user can see "Buka Laporan Keuangan di Aplikasi" and "Untuk saat ini, fitur Laporan Keuangan hanya dapat digunakan di aplikasi Mamikos di Android dan iOS."