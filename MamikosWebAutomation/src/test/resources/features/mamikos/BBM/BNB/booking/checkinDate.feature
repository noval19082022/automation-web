@regression @manageBooking @occupancyAndBilling1 @OB
Feature: OB Checking Date

  @checkFormDate @MB-2311
  Scenario: Checking Date On Booking Form After Choose Checkin Date On Kost Details
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob owner reject" and select matching result to go to kos details page
    And user select date "tomorrow" and rent type "Per bulan"

  @checkCheckinDateDisclaimer @MB-2314
  Scenario: Click On Rent Submit Button Without Choose Checkin Date
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob owner reject" and select matching result to go to kos details page
    And tenant clicks on rent submit button on kost detail
    Then tenant can see disclaimer on checkin date kost detail