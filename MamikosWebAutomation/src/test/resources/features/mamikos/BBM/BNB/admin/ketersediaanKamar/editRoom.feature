@BBM6 @ketersediaanKamar
Feature: Change the room on Data Ketersedian Kamar

  @TEST_BBM-1516 @toAutomated
  Scenario: [Data Ketersediaan Kamar][Edit Kost Room]Change the room name/number when room status is occupied with contract
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin ob"
    When user access bangkerupux menu "Kost Additional"
    And user search "kost zahira papua" in kost additional
    And user click on atur ketersediaan button
    When user click the edit button in room with status is occupied with room name "4"
    And user update room status to empty and click the update button
    Then user can see alert "Kamar dengan kontrak tidak bisa diubah." on atur kamar page

  @TEST_BBM-1517 @toAutomated
  Scenario: [Data Ketersediaan Kamar][Edit Kost Room]Change the room name/number when room status is occupied without contract
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin ob"
    When user access bangkerupux menu "Kost Additional"
    And user search "automation" in kost additional
    And user click on atur ketersediaan button
    When user click the edit button in room with status is occupied with room name "5"
    And user update lantai name to "2"
    Then user can see alert "Success! Kamar berhasil di-update" on atur kamar page
    When user click the edit button in room with status is occupied with room name "5"
    And user update lantai name to "1"
    Then user can see alert "Success! Kamar berhasil di-update" on atur kamar page