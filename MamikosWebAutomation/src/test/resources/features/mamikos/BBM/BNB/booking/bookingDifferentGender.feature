@regression @booking @manageBooking @OB
Feature: Booking Different Gender

  @bookingDifferentGender
  Scenario Outline: Booking for different Gender
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "<login data>"
    And user clicks search bar
    And I search property with name "<kost data>" and select matching result to go to kos details page
    Then I should reached kos detail page
    When user clicks on Booking button on Kost details page
    Then tenant can see confirmation popup different gender with information "<text information>"
    And tenant click on close icon
    Then I should reached kos detail page
    Examples:
      | login data                  | kost data          | kost type        | text information                   |
      | OB Different Gender Male    | female_test_data   | Kos Putri        | Mohon Maaf, Ini Kos Khusus Wanita  |
      | OB Different Gender Female  | male_test_data     | Kos Putra        | Mohon Maaf, Ini Kos Khusus Pria    |

  @deleteBaruDilihatForNextBookingDifferent @bookingDifferentGender
  Scenario Outline: Delete Baru Dilihat Booking Different Gender
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "<login data>"
    And user navigates to Booking History page
    And user clicks on Baru Dilihat section
    And I click on trash icon
    And I clicks on Batalkan button
    Then user can see confirmation delete popup
    And I click on trash icon
    And I clicks on Hapus button
    And user logs out as a Tenant user
    Examples:
      | login data                  |
      | OB Different Gender Male    |
      | OB Different Gender Female  |