@regression @manageBooking @OB @occupancyAndBilling @bookingDataSetting
Feature: Owner Booking Data Setting - Waktu Mulai Masuk Kos

  @TEST_BBM-541 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Atur Booking][Kost Detail][Booking section] Change booking terdekat = 2 Minggu and Booking waktu terjauh 2 bulan
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user click on owner popup
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user clicks on Ubah waktu button
    And user nonactive booking today toogle
    And user choose minim checkin time with "Minggu"
    And user choose minim checkin date with "2"
    And user clicks on Simpan button on update checkin time popup
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user logs out as a Owner user
    And user navigates to "mamikos /"
    And user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Ketentuan Kos with "Waktu mulai ngekos terdekat:" and "Waktu mulai ngekos terjauh:"
    And user click on checkinbox and can see "2 minggu setelah pengajuan sewa."
    When user navigates to "mamikos /"
    And user logs out as a Tenant user
		#    owner nonactive checkin time
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user click on owner popup
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user clicks on Ubah waktu button
    And user nonactive booking today toogle
    And user clicks on Simpan button on update checkin time popup
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"