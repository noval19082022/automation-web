@downPayment @occupancyAndBilling1 @OB @additionalPrice
Feature: OB Down Payment

  @regression @manageBooking  @activeDownPaymentAndCheckDefaultPrice @BNB-3148
  Scenario: Active Down Payment And Check Default Price (BNB-3148)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "additional price web"
    And user clicks on down payment toggle
    Then user can sees default price is "10% dari harga sewa"
    And user can sees down payment price list is :
      | DP Sewa Per Bulan           |
      | DP Sewa Per 3 Bulan         |
      | DP Sewa Per 6 Bulan |
      | DP Sewa Per Tahun           |
      | DP Sewa Per Minggu          |
    And user can sees down payment price number is :
      | Rp100.000   |
      | Rp280.000   |
      | Rp550.000   |
      | Rp1.100.000 |
      | Rp30.000    |
    When user clicks save button on additional price
#    Then user sees toast as "Biaya berhasil diubah"
    Then user can sees active DP is "DP 10%" with price range is "Rp30.000-Rp1.100.000"

  Scenario: Booking kost with active Down Payment(DP)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp" and select matching result to go to kos details page
    And user clicks on Booking button on Kost details page
    And user select payment period "Per Bulan"
    Then user can sees down payment text with price is "Rp101.000"
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking

  Scenario: Check Down Payment Price on Admin side
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "additional price ob"
    And user apply filter
    And user go to confirm booking via action button
    And user navigates to "down payment" confirmation page data booking admin
    Then user can sees down payment is "10% dari harga sewa" and price is "100000"
    When user navigates to "Konfirmasi" confirmation page data booking admin
    And user confirm booking from admin

  @regression @manageBooking
  Scenario: Delete Down Payment
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "additional price web"
    Then user delete active down payment
#    Then user sees toast as "Biaya berhasil diubah"

  @regression @manageBooking
  Scenario: Delete contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "ob additional price phone number"
    And user click on Cancel the Contract Button from action column

  Scenario: Delete kost Baru Dilihat for Down Payment
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user navigates to "mamikos /user/booking"
    And user clicks on Baru Dilihat section
    And I click on trash icon
    And I clicks on Batalkan button
    Then user can see confirmation delete popup
    And I click on trash icon
    And I clicks on Hapus button
    And user logs out as a Tenant user

  @regression @manageBooking @BNB-5125
  Scenario: Active Down Payment And Check Default Price (BNB-5125)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "kost automation"
    And user clicks on down payment toggle
    Then user can see DP popup
    When  user can see "Apakah Anda ingin mewajibkan DP untuk seluruh calon penyewa?" on dp pop up
    Then user can see default setting is "Tidak Wajib" and see "Jika tidak wajib, calon penyewa bisa memilih pembayaran dengan DP atau pembayaran penuh."

  @manageBooking
  Scenario: Owner setting DP with Tidak Wajib
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    And user clicks on down payment toggle
    Then user can see DP popup
    When  user can see "Apakah Anda ingin mewajibkan DP untuk seluruh calon penyewa?" on dp pop up
    Then user can see default setting is "Tidak Wajib" and see "Jika tidak wajib, calon penyewa bisa memilih pembayaran dengan DP atau pembayaran penuh."
    And user clicks save button on additional price
    And user see "DP tidak wajib" on atur page

  @manageBooking
  Scenario: Delete Down Payment
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    Then user delete active down payment

  @regression @manageBooking @BNB-5124
  Scenario: Owner setting DP with Wajib (BNB-5124)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    And user clicks on down payment toggle
    Then user can see DP popup
    And user choose DP setting with "Ya"
    And user clicks save button on additional price
    And user see "DP wajib" on atur page

  @BNB-5101 @manageBooking
  Scenario: Booking Mingguan for Kost Wajib DP (BNB-5101)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    Then user can see "Bisa bayar DP (uang muka) dulu" and "Biaya DP adalah 10% dari biaya yang dipilih." on kost detail
    When user select date "tomorrow" and rent type "Per minggu"
    Then user can see Uang muka, Pelunasan, Total Pembayaran pertama, Wajib bayar DP on kost detail
    When user clicks on Uang Muka
    Then user can see Uang muka, biaya layanan mamikos, Total Pembayaran pertama on popup uang muka detail and user validates the value
    When user click button close popup
    And user clicks on Pelunasan
    And user can see pelunasan, biaya layanan mamikos, Total Pembayaran pertama on popup pelunasan detail and user validates the value
    When user click button close popup
    Then user clicks on total pembayaran pertama
    And user can see Uang muka, biaya layanan mamikos, Total Pembayaran pertama on popup total pembayaran detail and user validates the value
    And user can see pelunasan, biaya layanan mamikos, Total Pembayaran pertama on popup total pembayaran detail and user validates the value
    And user can see Pembayaran selanjutnya, Harga sewa per Bulan (perbulan price), Biaya Layanan Mamikos, and Total pembayaran selanjutnya and user validates the value
    When user click button close popup
    And user clicks on Booking button on Kost details page
    And user select payment period "Per Minggu"
    And user can see Pembayaran Pertama (DP), Uang muka (DP), Biaya Layanan admin, Total Pembayaran Pertama (DP)
    And user can see Pelunasan with harga sewa - DP, Biaya Layanan Mamikos and Total Pembayaran pertama(Pelunasan)
    When user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking

  @manageBooking
  Scenario: Owner Confirm Booking With Active DP, Other Price, And Fine
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    And user navigates to Booking Request page
    And user select kost with name "Kost andalusia spanyol eropa timur"
    And user clicks on Booking Details button
    Then user can see booking detail and appears "Waktu Pemesanan:", "Mulai Sewa:", "Durasi Sewa:" and user validates the value
    And user clicks on Accept button
    And select first room available and clicks on next button
    Then user not see "Down Payment" on detail tagihan page
    And user click save button

  @manageBooking
  Scenario: Delete Down Payment
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    Then user delete active down payment

  @manageBooking
  Scenario: Delete contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "ob additional price kost with dp automation"
    And user click on Cancel the Contract Button from action column
