@regression @manageBooking @markRoom @OB
Feature: Mark BBK Room

  @markRoomAsOccupied @BBM-868
  Scenario: Mark BBK And Gold Plus Room As Occupied (BBM-868)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    And owner goes to my property kos menu
    And owner search "mark room occupied" and click see complete button
    And owner click on update room
    And owner click on edit button on room "Kosong"
    And owner tick on already occupied tickbox and click on update room
    Then owner can sees Pop-Up owner not add renter's data
    When owner click on Add Renter button
    Then owner redirected to Input Renter's Information form with kost name "mark kost gp"

  @occupancyAndBilling @BBM-867
  Scenario: Mark BBK And Non Gold Plus Room As Occupied (BBM-867)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button
    And user navigates to "owner /ownerpage/kos"
    And owner search "mark room not gp" and click see complete button
    And owner click on update room
    And owner click on edit button on room "Kosong"
    And owner tick on already occupied tickbox and click on update room
    Then owner can sees room is on "Terisi" status
    When owner click on edit button on room "Terisi"
    And owner tick on already occupied tickbox and click on update room
    Then owner can sees room is on "Kosong" status