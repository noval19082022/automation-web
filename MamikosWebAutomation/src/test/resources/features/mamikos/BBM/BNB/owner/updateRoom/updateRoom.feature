@owner @updateRoomAndPrice @regression @OB
Feature: Update Room

  @updateRoomToast @BBM-469
  Scenario: Check Update Room's Toast (BBM-469)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user navigates to update kamar page for kost "Dont Starve Together" list number "1"
    And user clicks on edit kost button for kost list number "1"
    And user clicks on update kamar button
    Then user can sees toast on update room as "Anda berhasil update data kamar"