@downPayment @occupancyAndBilling1 @OB @DPTidakWajibSakti
Feature: Booking kost with DP Tidak Wajib Bulanan

  Scenario: Delete contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "ob additional price phone number"
    And user click on Cancel the Contract Button from action column

  @manageBooking
  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    When user navigates to "mamikos /user/booking/"
    And user cancel booking

  @manageBooking @settingDPTidakWajib
  Scenario: Owner setting DP with Tidak Wajib
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    And user clicks on down payment toggle
    Then user can see DP popup
    When  user can see "Apakah Anda ingin mewajibkan DP untuk seluruh calon penyewa?" on dp pop up
    Then user can see default setting is "Tidak Wajib" and see "Jika tidak wajib, calon penyewa bisa memilih pembayaran dengan DP atau pembayaran penuh."
    And user clicks save button on additional price
    And user see "DP tidak wajib" on atur page

  @manageBooking @BookingWithNotDP @BNB-5105
  Scenario: Tenant booking kost DP with Tidak Wajib "Per Bulan"
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob additional price kost with dp automation" and select matching result to go to kos details page
    Then user can see "Bisa bayar DP (uang muka) dulu" and "Biaya DP adalah 10% dari biaya yang dipilih." on kost detail
    And user select date "tomorrow" and rent type "Per bulan"
    Then user can see Jika kamu membayar dengan DP, Uang Muka (DP), Pelunasan, Jika kamu pakai pembayaran penuh, Total Pembayaran pertama, Bisa bayar pakai DP "per bulan" and user validates the value
    And user clicks on Uang Muka
    And user can see Uang muka, biaya layanan mamikos, Total Pembayaran pertama on popup uang muka detail and user validates the value "per bulan"
    And user click button close popup
    When user clicks on Pelunasan
    And user can see pelunasan, biaya layanan mamikos, Total Pembayaran pertama on popup pelunasan detail and user validates the value "per bulan"
    When user clicks on Pembayaran penuh
    And user can see pembayaran penuh, biaya layanan mamikos, Total Pembayaran pertama on popup pembayaran penuh detail and user validates the value "per bulan"
    And user click button close popup
    And user clicks on Booking button on Kost details page
    And user select payment period "Per Bulan"
    And user can see DP information with "Uang muka (DP)", "10% dari harga sewa", total DP on pengajuan sewa section "per bulan"
    And user select "Pembayaran penuh" on type permbayaran pertama option
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking

  @manageBooking
  Scenario: Owner Confirm for Kost Tidak wajib DP
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button
    * user navigates to "owner /booking/request"
    And user select kost with name "Kost andalusia spanyol eropa timur"
    And user clicks on Booking Details button
    Then user not see "Uang muka(DP)" on detail tagihan page
    And user clicks on Accept button
    And select first room available and clicks on next button
    Then user not see "Down Payment" on detail tagihan page
    And user click save button

  @manageBooking
  Scenario: Delete Down Payment
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob booking female" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "Kost andalusia spanyol eropa timur"
    Then user delete active down payment

  @manageBooking
  Scenario: Delete contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "ob additional price phone number"
    And user click on Cancel the Contract Button from action column

