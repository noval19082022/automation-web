@regression @occupancyAndBilling @manageBooking @otherPrice @OB/BX @additionalPrice
Feature: OB Other Price

  @BNB-3131
  Scenario: Owner Add Additional Price With 20 Character And Price With Correct Data (BNB-3131)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "other price ob"
    And user clicks on other price per month toggle
    And user input price name with "1234567890abcdefjkl", price with "50000"
#    Then user sees toast as "Biaya berhasil diubah"
    Then user can sees other price with name "1234567890abcdefjkl" and price "Rp50.000" show in the list

  @BNB-5061
  Scenario: Check Booking With Other Price On Admin Side (BNB-5061)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "ob other price" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    And user selects T&C checkbox and clicks on Book button
    When user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "additional price ob"
    And user apply filter
    And user go to confirm booking via action button
    And user navigates to "tambahan biaya" confirmation page data booking admin
    Then user can sees other price with name "1234567890abcdefjkl" and price "Rp50.000" on tambahan biaya
    When user navigates to "konfirmasi" confirmation page data booking admin
    Then user can sees other price with name "1234567890abcdefjkl" and price "50000" on konfirmasi
    And user confirm booking from admin

  @BNB-3131 @BNB-3134
  Scenario: Delete Active Other Additional Price (BNB-3134)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button for check additional price
    And user clicks widget set price
    And user clicks on kost name "other price ob"
    And user delete active other additional price
    And tenant can not sees active other price

  @MB-3131 @deleteContractOtherPrice
  Scenario: Delete contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "ob additional price phone number"
    And user click on Cancel the Contract Button from action column