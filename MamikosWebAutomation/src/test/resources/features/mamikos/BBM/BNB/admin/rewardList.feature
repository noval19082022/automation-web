Feature: [Loyalty Reward Management][Reward List]Reward List Management page display

	#1. Given user already in Reward List Management
	@TEST_BNB-5675 @automated @loyalty @reward-list @web @web-coverable @xray-update
	Scenario: [Loyalty Reward Management][Reward List]Reward List Management page display
		Given user navigates to "mamikos admin"
		And user logs in to Mamikos Admin via credentials as "admin ob"
		When user access to Reward List menu
		Then system display Reward List Management page
		And user filter reward name "Reward Automation"
		And user click button filter reward
		And user click button update reward
		And user click checkbox Active
		Then user click button update reward on page detail reward
		And system display success add reward type
