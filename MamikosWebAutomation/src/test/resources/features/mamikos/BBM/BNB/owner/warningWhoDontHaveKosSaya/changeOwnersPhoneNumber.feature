@regression @manageBooking @occupancyAndBilling1 @changeOwnerNumberUnique
Feature: Kos saya revamp at owner penyewa unique code

  @BNB-8932
  Scenario: change owner's number phone at unique code
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
    And user search kost "bandung"
    And user click Selengkapnya button
    And user click Kirim ulang kode hyperlink
    Then user will redirect to Kirim kode unik ke penyewa page
    And user will see old number of owner "0890000001001"
    When user click Ubah nomor HP hyperlink
    And user change owner's phone number into "0890000001002" and click Gunakan
    Then user will see new number phone of owner "0890000001002"
    When user click Ubah nomor HP hyperlink
    And user change owner's phone number into "0890000001001" and click Gunakan
    Then user will see the old number of owner "0890000001001"