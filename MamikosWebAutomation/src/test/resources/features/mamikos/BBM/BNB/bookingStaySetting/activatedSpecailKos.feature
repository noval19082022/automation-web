@occupancyAndBilling @OB @kosDetail @bookingDataSetting @specialJobs
Feature: Owner Booking Data Setting - Kost Khusus

  @TEST_BBM-524 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated Special Kos for Karyawan
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "special kos kriteria"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Peraturan kos with "Khusus karyawan"
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "special kos kriteria"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BNB-7948 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated Special Kos for Mahasiswa
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "special kos kriteria"
    And user select on Mahasiswa
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Peraturan kos with "Khusus Mahasiswa"
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "special kos kriteria"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"