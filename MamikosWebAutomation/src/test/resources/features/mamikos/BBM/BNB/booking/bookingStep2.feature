@regression @booking @manageBooking @bookingStep2 @OB
Feature: OB Booking step 2 / Booking form fill page 2

  #Deleting existing booking
  Background:
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search for Kost with name
    And user click on Cancel the Contract Button from action column

  @bookingKostMaleFemale
  Scenario Outline: Booking kost male and female to check alert message
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "<Login Data>"
    And user clicks search bar
    And I search property with name "<Kost Data>" and select matching result to go to kos details page
    Then I should reached kos detail page
    When user clicks on Booking button on Kost details page
    Then tenant can see Per Month is selected
    When tenant set tomorrow date click on continue button
    Then tenant can see "<Gender>" gender is selected
    And tenant can see information that state Kost is only for "<Alert Message>"
    Examples:
    | Login Data    | Kost Data                     |Gender    | Alert Message                      |
    | gender male   |ob booking male non pasutri    |Laki-laki | hanya boleh disewa untuk Laki-Laki |
    | gender female |female_test_data               |Perempuan | hanya boleh disewa untuk Perempuan |

  @deleteBaruDilihatForNextBookingDifferent @bookingStep2
  Scenario Outline: Delete Baru Dilihat Booking Step 2
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "<login data>"
    And user navigates to Booking History page
    And user clicks on Baru Dilihat section
    And I click on trash icon
    And I clicks on Batalkan button
    Then user can see confirmation delete popup
    And I click on trash icon
    And I clicks on Hapus button
    And user logs out as a Tenant user
    Examples:
      | login data     |
      | gender male    |
      | gender female  |