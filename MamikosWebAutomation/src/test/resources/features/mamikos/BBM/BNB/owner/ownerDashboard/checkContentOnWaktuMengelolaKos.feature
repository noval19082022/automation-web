@regression @ownerDashboard @occupancyAndBilling @manageBooking @OB
Feature: Check content waktu mengelola kos

  @checkContentOnWaktuMengelolaKos @BBM-971
  Scenario: Check Waktu Mengelola section when owner have one kost not Bbk (BBM-971)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob have not bbk" and click on Enter button
    Then user can see waktu mengelola section
    And user click on owner popup
    When user can see "Atur Ketersedian Kamar" menu in owner dashboard
    And user clicks widget set available room
    Then user see screen update room
    And user click on Home menu
    When user can see "Atur Harga" menu in owner dashboard
    And user clicks widget set price
    Then user can see set price page
    And user click on Home menu
    When user can see "Daftar ke Booking Langsung" menu in owner dashboard
    And user cliks widget booking register
    Then user can see manage direct booking popup
    And user click on Home menu
    When user can see "Daftar kontrak penyewa kos" menu in owner dashboard
    And user click on widget Penyewa
    Then user can see "Penyewa di" page
    And user click on Home menu
    Then user can see "Tambah Penyewa" menu in owner dashboard
    When user can see "Pusat Bantuan" menu in owner dashboard
    And user click on widget help center
    Then user can see help center page


