@regression @booking @sectionBooking @manageBooking @OB @bookingDataSetting
  Feature: OB Booking Stay Setting

    @TEST_BBM-526 @automated @booking-stay-setting @partial-regression @web @web-covered @xray-update
    Scenario: [Ubah peraturan masuk kos][Filter] Change kos setting on kost GP type
      Given user navigates to "mamikos /"
      And user clicks on Enter button
      And user fills out owner login as "ob reject booking" and click on Enter button
      And user navigates to "mamikos /ownerpage/manage/all/booking"
      And user click Ubah Aturan at pengajuan booking page
      And user select kost GP "Kost Norway Tobelo Utara Halmahera Utara"
      And user click Simpan at Peraturan Masuk Kos page
      Then user will see toast Peraturan Masuk Kos