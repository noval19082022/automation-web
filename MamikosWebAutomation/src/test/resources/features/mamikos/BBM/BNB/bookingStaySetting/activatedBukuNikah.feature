@occupancyAndBilling @OB @kosDetail @bookingDataSetting @kosWajibBukuNikah
Feature: Owner Booking Data Setting Wajib Buku Nikah

  @TEST_BBM-521 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated Wajib Buku Nikah
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "Wajib sertakan buku nikah"
    And button "kamar hanya bagi penyewa" will disable
    Then "Boleh untuk pasutri" is activated
    When user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-525 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]  Nonactived Buku Nikah (BBM-525)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    When user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "bisa pasutri"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"