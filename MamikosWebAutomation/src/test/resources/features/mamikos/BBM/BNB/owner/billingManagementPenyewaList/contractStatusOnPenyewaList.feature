@regression @manageBooking  @penyewaList @bnb
Feature: Contract Status on Penyewa

  @filterPenyewa @BBM-902
  Scenario: check contract status on all filter (BBM-902)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user click on owner popup
    When user click on widget Penyewa
    Then user can see Penyewa list
#    And user click on Kost name search
    And user search kost "kost banda who dont have kos saya"
    And user click on dropdown Filter box and select "Sedang menyewa" filter
    Then user will see contract with status "Sedang menyewa"
    When user click on dropdown Filter box and select "Akan masuk" filter
    Then user click on dropdown Filter box and select "Menghentikan kontrak sewa" filter

  @downloadBiodata @BBM-888 @BBM-885
  Scenario: download biodata fakedoor (BBM-888) (BBM-885
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
    Then user can see Penyewa list
#    When user click on Kost name search
    And user search kost "kost banda who dont have kos saya"
    And user click download biodata penyewa button
    Then user will see pop up for upcoming feature
    When user tick on checkbox pop up
    Then user will see information about upcoming feature