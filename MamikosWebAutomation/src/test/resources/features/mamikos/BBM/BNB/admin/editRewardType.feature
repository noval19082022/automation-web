Feature: [Loyalty Reward Management][Reward Type]Edit Reward Type

	#1. Given user already in Reward Type
	# the data cannot be recycled, thus will be excluded from regression
	@TEST_BNB-5676 @automated @cannot-be-recycled @loyalty @reward-type @web @web-coverable @xray-update
	Scenario: [Loyalty Reward Management][Reward Type]Edit Reward Type
		Given user navigates to "mamikos admin"
		And user logs in to Mamikos Admin via credentials as "admin ob"
		And user access to Reward Type menu
		When user input reward type key "automation_key_new17" on search box
		And user click button search reward type
		And user click button edit reward type
		Then user input reward type key "automation_key_new18"
		And user input reward type name "Reward Automation New18"
		And user click save button reward type
		Then system display success add reward type
		And user access to Reward Type menu
		When user input reward type key "automation_key_new17" on search box
		And user click button search reward type
		And user click button delete on manage reward type
