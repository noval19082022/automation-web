@regression @manageBooking @occupancyAndBilling1 @OB
Feature: OB Booking With Active DP, Other Price, And Fine

  Scenario: Delete All Need Confirmation Booking Request
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    When user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

  @MB-3152
  Scenario: Owner Confirm Booking With Active DP, Other Price, And Fine
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob additional price"
    And user clicks search bar
    And I search property with name "OB kost booking additional" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "kost bima booking dp"
    And user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    Then owner can sees deposit is "Rp 250.000"
    And owner can sees lateness fine is "1 day" with price is "Rp 50.000"
    And owner can sees other price "Biaya bersihkan jendela" "Rp 50.000"