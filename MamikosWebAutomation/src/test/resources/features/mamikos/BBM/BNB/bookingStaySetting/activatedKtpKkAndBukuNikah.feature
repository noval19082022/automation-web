@occupancyAndBilling @OB @kosDetail @bookingDataSetting @kosKtpKKBukuNikah
Feature: Owner Booking Data Setting KK, KTP setting

  @TEST_BBM-539 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated Ktp KK And Buku Nikah
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "KTP"
    And user activated "Buku Nikah"
    And button "kamar hanya bagi penyewa" will disable
    And user activated "Kartu Keluarga"
    And button "Maks 2 orang/kamar" will disable
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BBM-540 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa] Non Activated Ktp, KK And Buku Nikah
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "KTP"
    And user activated "bisa pasutri"
    And user activated "boleh bawa anak"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"