@regression @draftBooking @manageBooking @closeButtonOnPengajuan1ContinueBookingAndSaveDraft @OB/BX
Feature: OB Draft Booking

  @closeButtonOnPengajuan1ContinueBookingAndSaveDraft
  Scenario: Go back from booking page
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "gender female"
    And user clicks search bar
    And I search property with name "female_test_data" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    And user click exit button
    And user check confirmation text title
    And user check confirmation text body
    And user click Continue Booking
    And user click exit button
    And user click Save Draft
    Then user redirect to kost detail
    And user check toast draft
    And tenant click on Lihat button to go to draft booking page
    Then tenant should reached draft booking page
    And tenant can see "Ancient Fuelweaver Web Automation, Ngaglik, Kabupaten Sleman" as kost name and kost location
    And user click Continue Booking from draft booking page
    Then user redirect to Form Booking Harga dan Masa Sewa
    And user click exit button
    And user click Save Draft
    Then user redirect to kost detail
    And user check Lihat button clickable
    And tenant click on Lihat button to go to draft booking page
    Then tenant should reached draft booking page
    And tenant can see "Ancient Fuelweaver Web Automation, Ngaglik, Kabupaten Sleman" as kost name and kost location
    And user click See Detail Kost from draft booking page
    Then user redirect to kost detail new tab
    And user click delete button on draft booking
    And check pop up title on delete draft booking
    And check pop up caption on delete draft booking
    And click close on pop up delete draft booking
    And user click delete button on tab one draft booking
    And user click Batalkan on pop up delete draft booking
    And user click delete button on tab one draft booking
    And user click Hapus Draft on pop up delete draft booking
    Then tenant cannot see "Ancient Fuelweaver Web Automation, Ngaglik, Kabupaten Sleman" as kost name and kost location

  Scenario: Delete Baru Dilihat Booking Draft Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "gender female"
    And user navigates to Booking History page
    And user clicks on Baru Dilihat section
    And I click on trash icon
    And I clicks on Batalkan button
    And I click on trash icon
    And I clicks on Hapus button