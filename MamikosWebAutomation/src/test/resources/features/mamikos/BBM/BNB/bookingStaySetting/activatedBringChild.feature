@occupancyAndBilling @OB @kosDetail @bookingDataSetting @kosBawaAnak
Feature: Owner Booking Data Setting Pasutri setting

  @TEST_BBM-522 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa] Activated Boleh Bawa Anak
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user click on owner popup
    Then user can see waktu mengelola section
    And user can see "Ubah Peraturan Masuk Kos" menu on dashboard
    When user click on Ubah peraturan kos menu
    Then user can see atur booking page
    When user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "boleh bawa anak"
    And button "kamar hanya bagi penyewa" will disable
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost karyawan" and select matching result to go to kos details page
    Then user can see Peraturan kos with "Boleh bawa anak "
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "boleh bawa anak"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"