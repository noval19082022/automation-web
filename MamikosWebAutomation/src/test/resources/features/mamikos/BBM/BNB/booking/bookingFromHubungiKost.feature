@regression @manageBooking @hubungiKostDetail @OB @booking
Feature: OB Booking From Hubungi Kost

  @BNB-2111
  Scenario: OB Booking Hubungi Kost From Detail Kost (BNB-2111)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB contract sect dbet"
    And user clicks search bar
    And I search property with name "OB kost booking additional" and select matching result to go to kos details page
    And user clicks on ask kost owner button
    Then user can sees Hubungi Kost pop-up
    Then user can sees "Saya butuh cepat nih. Bisa booking sekarang?" is selected
    When user click on Ajukan Sewa button
    Then user can sees booking form page