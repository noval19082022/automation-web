@regression @booking @manageBooking @occupancyAndBilling
Feature: contract DP ST payment at kos saya

  @TEST_BNB-8584 @automated @kost-saya-revamp-phase1 @partial-regression @web @xray-update
  Scenario: [Kost saya][Kontrak]Check kontrak section when tenant paid DP & ST
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant dpst kos saya"
    And user navigates to Profil page
    And user click Kontrak section
    Then user redirect to Kontrak page
    And user check kost type, kost name, kost address, and room number
    And user check total biaya, harga sewa, and biaya lain on kost dpst
    And user check tanggal penagihan
    And user check mulai sewa, durasi sewa, and sewa berakhir
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
    And user click on Kost name search
    And user search kost "kost makassar check"
    And user click Selengkapnya button on "Tenant For Check Contract Dpst Automation" contract
    And user click tab Kontrak sewa
    And user click Ubah kontrak penyewa button
    Then user check prices at penyewa owner are same to contract at kos saya for dpst payment