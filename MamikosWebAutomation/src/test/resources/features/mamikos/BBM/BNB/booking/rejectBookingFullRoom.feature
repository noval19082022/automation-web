@booking @rejectBooking @occupancyAndBilling1 @rejectFullRoom @OB/BX
Feature: OB Owner Reject Booking

  Background:
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob owner reject full"
#   if tenant have booking and cancel booking
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

  @ownerRejectBooking  @MB-3662
  Scenario: Booking And Reject Booking With Full Kost Reason - MB-3662
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "ob kos reject full" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button
    And user navigates to Booking Request page
    And user select kost with name "owner reject full reason"
    And owner process to reject booking request
    And owner reject booking with full room reason
    And owner goes to my property kos menu
    And owner search "owner reject full reason" and click see complete button
    Then owner verify room is zero
    When owner click on update room
    Then owner sees all kost is in "Terisi" status
    When owner change all filled status to available

  @adminRejectBooking @MB-5062
  Scenario: Admin Reject Booking With Full Kost Reason
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "ob kos reject full" and select matching result to go to kos details page
    And user create booking for today
    Then user selects T&C checkbox and clicks on Book button
    And user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "tenant reject on admin"
    And user apply filter
    And user process to reject booking
    And user choose "Kamar tidak tersedia untuk penyewa" and click on send button

  @adminRejectBooking @MB-5062
  Scenario: Tenant check Status After Reject Booking From Admin - MB-5062
    When user navigates to Booking History page
    Then user check booking status is rejected by owner with reason "Kamar tidak tersedia untuk penyewa"

  @adminRejectBooking @MB-50621
  Scenario: Owner Check Booking Status And Room Availability After Reject Booking From Admin - MB-50621
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button
    When user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "owner reject full reason"
    And user activated rejected booking request filter
    And user can sees rejected tenant with name "Tenant Owner Reject Full"
    Then owner can see reason on your reject is "Kamar tidak tersedia untuk penyewa"
    And owner goes to my property kos menu
    And owner search "owner reject full reason" and click see complete button
    And owner click on update room
    Then owner sees all kost is in "Kosong" status