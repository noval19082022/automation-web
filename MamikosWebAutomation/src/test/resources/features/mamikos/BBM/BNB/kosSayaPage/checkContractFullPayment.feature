@regression @booking @manageBooking @occupancyAndBilling @fullPaymentKosSaya @BNB-8585
Feature: contract full payment at kos saya

  @fullPaymentKosSaya @BNB-8585
  Scenario: Tenant pay invoice full payment
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search by Kost name "kost bima booking"
    And user click on Cancel the Contract Button from action column

# Scenario: Delete All Need Confirmation Booking Request
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant full pay kos saya"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

#  Scenario: tenant booking kost
#  Scenario: Tenant booking
    When user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "OB kost booking additional" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    Then user navigates to main page after booking

#  Scenario: Owner accept booking from tenant
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "kost bima booking"
    And user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button
    Then user will redirect to pengajuan booking page

#  Scenario: Tenant pay invoice full payment
    Given user navigates to "mamikos /"
    When user logs out as a Owner user
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob tenant full pay kos saya"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user select payment method "OVO" for "OBAutomation"
    Then system display payment using "OVO" is "Pembayaran Berhasil"

#  Scenario: Tenant check in
    Given user navigates to "mamikos /"
    And user navigates to Profil page
    And tenant clicks on kost saya
    And user click Check in at Kos saya page
    Then user redirect to kos saya page

#  Scenario: check contract section from full payment
    Given user navigates to "mamikos /"
    And user navigates to Profil page
    And user click Kontrak section
    Then user redirect to Kontrak page
    And user check kost type, kost name, kost address, and room number
    And user check total biaya, harga sewa, and biaya lain full payment
    And user check tanggal penagihan
    And user check mulai sewa, durasi sewa, and sewa berakhir
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob reject booking" and click on Enter button
    And user navigates to "owner /tenant-list"
    And user click on Kost name search
    And user search kost "kost bima booking"
    And user click Selengkapnya button on "Tenant Untuk Full Payment Kos Saya Automation" contract
    And user click tab Kontrak sewa
    And user click Ubah kontrak penyewa button
    Then user check prices at penyewa owner are same to contract at kos saya for full payment