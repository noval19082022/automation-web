@occupancyAndBilling @OB @kosDetail @bookingDataSetting @kosKK
Feature: Owner Booking Data Setting Kartu Keluarga

  @TEST_BBM-527 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Activated KK
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user choose kost name with "Kost Rini Dbet Owner Putri"
    And user activated "KK"
    And button "kamar hanya bagi penyewa" will disable
    Then "Boleh bawa anak" is activated
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"

  @TEST_BNB-7959 @automated @booking-and-billing @booking-stay-setting @web @xray-update
  Scenario: [Ubah peraturan masuk kos][Kriteria calon penyewa]Nonactivated Ktp KK And Buku Nikah
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "owner bds" and click on Enter button
    And user navigates to "mamikos /ownerpage/manage/all/booking"
    And user click Ubah Aturan at pengajuan booking page
    And user select and click kos with "Kost Rini Dbet Owner Campur"
    And user activated "KTP"
    And user activated "bisa pasutri"
    And user activated "boleh bawa anak"
    And user click on Simpan button on atur page
    Then user can see toast "Peraturan terbaru berhasil disimpan"