@regression @booking @sectionBooking @manageBooking @OB @bookingDataSetting
  Feature: OB Booking Stay Setting

    @TEST_BBM-536 @automated @booking-stay-setting @toAutomate,partial-regression @web @web-covered @xray-update
    Scenario: [Dashboard][Pengajuan Booking][Ubah peraturan masuk kos]check button name on dashboard and pengajuan booking page
      Given user navigates to "mamikos /"
      And user clicks on Enter button
      And user fills out owner login as "ob reject booking" and click on Enter button
      And user click on owner popup
      And user click Ubah Peraturan Masuk Kos section at dashboard
      Then user redirect to Peraturan Masuk Kos page
      When user click Pengajuan Booking
      And user click Ubah Aturan at pengajuan booking page
      Then user redirect to Peraturan Masuk Kos page