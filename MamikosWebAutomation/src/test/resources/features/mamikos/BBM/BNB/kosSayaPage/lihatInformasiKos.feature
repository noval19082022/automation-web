@regression @booking @manageBooking @occupancyAndBilling
Feature: lihat informasi kos at kos saya

  @TEST_BNB-8580 @automated @kost-saya-revamp-phase2 @web @xray-update
  Scenario: [Kos Saya][Fasilitas]Check Lihat semua fasilitas
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB contract sect dbet"
    And user navigates to Profil page
    And tenant clicks on kost saya
    When user click Lihat informasi kos
    Then user will redirect to lihat informasi kos page
    And user will see kost type, kost name, and kost address
    And user will see Peraturan umum kos
    When user click Lihat semua fasilitas button
    Then user will see Fasilitas kamar
    And user will see Fasilitas kamar mandi
    And user will see Fasilitas umum
    And user will see parkir