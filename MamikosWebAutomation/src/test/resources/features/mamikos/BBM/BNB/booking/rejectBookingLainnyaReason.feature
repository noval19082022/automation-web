@regression @manageBooking @occupancyAndBilling1 @rejectLainnya @OB/BX
Feature: OB Reject Booking With Lainnya Reason

  Scenario: Delete All Need Confirmation Booking Request
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

  @ownerRejectReasonWithLainnya @rejectReasonLainnya @MB-5060
  Scenario: Owner Reject Booking With Lainnya Reason
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user clicks search bar
    And I search property with name "ob owner reject" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button
    And user navigates to Booking Request page
    And user select kost with name "owner test reject"
    And owner process to reject booking request
    And owner clicks on T&C and redirect to Term And Condition Page
    Then owner can sees Term And Condition check box is not tick
    When owner choose other reason with costum reason "Saya sudah ada yang punya"
    And user activated rejected booking request filter
    Then user can sees rejected tenant with name "Tenant Delapan Sari"

#    the list is random, discussing on BE
#    Then owner can see reason on your reject is "Saya sudah ada yang punya"

  @tenantCheckRejectReasonOther @rejectReasonLainnya @MB-5060
  Scenario: Tenant Check Reject Reason After Owner Reject
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user navigates to "mamikos /user/booking"
    Then user check booking status is rejected by owner with reason "Saya sudah ada yang punya"

  @adminRejectLainnya @MB-2329
  Scenario: Admin Reject Booking With Lainnya Reason
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user clicks search bar
    And I search property with name "ob owner reject" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    When user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "ob rejected"
    And user apply filter
    And user process to reject booking
    And admin reject booking with "Lainnya" and reason is "Anda ditolak dari Admin"

  @tenantCheckRejectReasonOther @adminRejectLainnya @MB-2329
  Scenario: Tenant Check Reject Reason After Admin Reject
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user navigates to "mamikos /user/booking"
    Then user check booking status is rejected by owner with reason "Anda ditolak dari Admin"

  @ownerCheckRejectReasonLainnya @MB-2329
  Scenario: Owner Check Reject Reason After Admin Reject
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button
    When user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "owner test reject"
    And user activated rejected booking request filter
    Then user can sees rejected tenant with name "Tenant Delapan Sari"
    Then owner can see reason on your reject is "Anda ditolak dari Admin"

  @MB-2332 @MB-5432 @rejectNotInputReason
  Scenario: Owner Reject Not Input Reason
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user clicks search bar
    And I search property with name "ob owner reject" and select matching result to go to kos details page
    And user create booking for today
    And user selects T&C checkbox and clicks on Book button
    And user navigates to main page after booking
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button
    And user navigates to Booking Request page
    And user select kost with name "owner test reject"
    And owner process to reject booking request
    And owner tick on Term And Condition Check Box
    Then owner can see is yes reject button is not clickable

  @MB-2332 @MB-5432
  Scenario: Admin Reject Booking Not Input Reason
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin consultant"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "ob rejected"
    And user apply filter
    And user process to reject booking
    And admin reject booking with "Lainnya" and reason is ""

  @MB-2332 @MB-5432
  Scenario: Tenant Check Reject Reason After Reject With Other Empty Reason From Admin
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "ob kost saya rejected"
    And user navigates to "mamikos /user/booking"
    Then user check booking status is rejected by owner with reason ""

  @MB-2332 @MB-5432
  Scenario: Owner Check Reject Reason After Admin Reject With Other Empty Reason From Admin
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "ob additional price" and click on Enter button
    When user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "owner test reject"
    And user activated rejected booking request filter
    Then user can sees rejected tenant with name "Tenant Delapan Sari"

#    the list is random, discussing on BE
#    Then owner can see reason on your reject is "-"