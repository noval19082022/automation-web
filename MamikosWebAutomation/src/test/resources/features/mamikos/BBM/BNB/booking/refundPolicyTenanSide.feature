@BBM6 @regression @manageBooking @occupancyAndBilling @OB @refund-policy
  Feature: Update Refund Policy kost pilar 1 and reguler

    @tenantRefundPolicy @BBM-872
    Scenario: Check refund policy on Tenant side for kost Reguler
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "OB contract sect dbet"
      And user clicks search bar
      And I search property with name "OB kost promo ngebut" and select matching result to go to kos details page
      Then user can see refund policy and click on Bagaimana keuntungan button
      When user can see refund information and click on kebijakan refund mamikos
      Then user can see "bagaimana cara mengajukan refund" on mamihelp page


    @TEST_BBM-1776 @booking-billing-management @refund-policy @BBM-1778
    Scenario: [Kost Detail][Bisa Refund] Tenant can see Refund Info for Kost Pillar 1 and check if Refund Policy redirect to correct page for Pillar1
      Given user navigates to "mamikos /"
      When user clicks on Enter button
      And user login in as Tenant via phone number as "OB contract sect dbet"
      And user clicks search bar
      And I search property with name "teng kost singgah sini" and select matching result to go to kos details page
      Then user can see refund policy and click on Bagaimana keuntungan button
      When user can see refund policy with "Refund sebelum check-in"
      Then user can see refund policy with "DP tidak dapat di-refund"
      When user can see refund policy with "Bayar langsung lunas dapat di-refund"
      And user click on ketentuan waktu berikut
      Then user can see "refund policy" on mamihelp page