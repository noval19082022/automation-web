@regression @tenantTracker @BBM3

Feature: Main Page Filter Fase dan Status

  @TEST_BBM-578
  Scenario: Filter Fase Chat & Butuh Cepat (BBM-578)
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"
    When user click Tenant Communication menu
    And user choose "Chat" on filter tahapan and "Butuh Cepat" on filter status
    And user click terapkan
    And user click search button on main page filter
    Then user verify search result on main page bse contains butuh cepat
    And  user verify search result on main page bse contains chat

  @TEST_BBM-558
  Scenario: [BSE Tenant Tracker][Profile Page] Chat detail (BBM-558)
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "Adisinggahsini" in the search field on main page
    * user click search button on main page filter
    * user clicks on the tenant name on the first row
    And user choose "Chat" on filter tahapan and "Diskon" on filter status
    And user click terapkan
    And user click search button on main page filter
    And user click "Kost Adi Auto SinggahSini"
    Then detail displayed and contains
      | title                 | value                     |
      | Chat header           | Chat                      |
      | Listing name          | Kost Adi Auto SinggahSini |
      | Listing Photo         | available                 |
      | Listing gender policy | Campur                    |
      | Chat                  | Diskon                    |

  @TEST_BBM-577
  Scenario: Filter Fase Booking dan Butuh Konfirmasi (BBM-577)
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"
    When user click Tenant Communication menu
    And user choose "Booking" on filter tahapan and "Butuh Konfirmasi" on filter status
    And user click terapkan
    And user click search button on main page filter
    Then user verify search result on main page bse contains butuh konfirmasi
    And  user verify search result on main page bse contains booking

  @TEST_BBM-555
   Scenario: [BSE Tenant Tracker][Profile Page] Booking detail (BBM-555)
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "Adisinggahsini" in the search field on main page
    * user click search button on main page filter
    * user clicks on the tenant name on the first row
    And user choose "Booking" on filter tahapan and "Diakhiri" on filter status
    And user click terapkan
    And user click search button on main page filter
    And user click "Kost Adi Auto SinggahSini"
    Then detail displayed and contains
      | title                      | value                    |
      | Booking header             | Booking                  |
      | Booking Code               | #MAMI0606051976          |
      | Listing name               | Kost Adi Auto SinggahSini|
      | Listing Photo              | available                |
      | Listing gender policy      | Campur                   |
      | Phase Status               | Diakhiri                 |
      | Total tenant & tenant type | 1 Penyewa, -             |

  @TEST_BBM-1381
  Scenario: Filter Fase Survei dan Status Diajukan
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"
    When user click Tenant Communication menu
    And user choose "Survei" on filter tahapan and "Diajukan" on filter status
    And user click terapkan
    And user click search button on main page filter
    Then user verify search result on main page bse contains diajukan
    And  user verify search result on main page bse contains survei

  @TEST_BBM-559
  Scenario: [BSE Tenant Tracker][Profile Page] Survei detail (BBM-559)
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "Adisinggahsini" in the search field on main page
    * user click search button on main page filter
    * user clicks on the tenant name on the first row
    And user choose "Survei" on filter tahapan and "Dibatalkan" on filter status
    And user click terapkan
    And user click search button on main page filter
    And user click "Kost Singgahsini Laksana Adi Tipe A Halmahera Utara"
    Then detail displayed and contains
      | title                 | value                                               |
      | Survei header         | Survei                                              |
      | Listing name          | Kost Singgahsini Laksana Adi Tipe A Halmahera Utara |
      | Listing Photo         | available                                           |
      | Listing gender policy | Campur                                              |
      | Phase Status          | Dibatalkan                                          |
      | Survey Date           | 17 Sep 2022                                         |
#      | Survey Time           | 08:00                                               |
      | Kontak Penjaga        | Chat penjaga                                        |
    When user click "Chat penjaga"
    And user switch tab to "2"
    Then user redirected to "whatsapp chat"
    When user switch to main window
    And user click "Kecamatan Tobelo"
    And user switch tab to "2"
    Then user redirected to "google maps"

  @TEST_BBM-575
  Scenario: Filter Fase Checkin dan Status Sudah Checkin
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"
    When user click Tenant Communication menu
    And user choose "Check-in" on filter tahapan and "Sudah Check-in" on filter status
    And user click terapkan
    And user click search button on main page filter
    Then user verify search result on main page bse contains sudah check in
    And  user verify search result on main page bse contains checkin

  @TEST_BBM-557
  Scenario: [BSE Tenant Tracker][Profile Page] Check-out detail (BBM-557)
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "Adisinggahsini" in the search field on main page
    * user click search button on main page filter
    * user clicks on the tenant name on the first row
    And user choose "Check-out" on filter tahapan and "Sudah Check-out" on filter status
    And user click terapkan
    And user click search button on main page filter
    And user click "Kost Adi Auto SinggahSini"
    Then detail displayed and contains
      | title                      | value                    |
      | Checkin header             | Checkout                 |
      | Listing name               | Kost Adi Auto SinggahSini	         |
      | Listing Photo              | available                |
      | Listing gender policy      | Campur                   |
      | Phase Status               | Sudah Check-Out          |
      | Checkin date               | 23 Jun 2022              |
      | Total tenant & tenant type | 1 Penyewa, -             |
      | Room number                | Pilih di tempat          |
      | Invoice type               | Full Payment -\nDibayar  |
#      | Invoice due date           | 23 Jun 2022.\n00:00      |

  @TEST_BBM-1382
  Scenario: Filter Sudah Follow Up
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    And user click filter penyewa
    And user click Follow Up Status
    And user click Sudah Follow Up
    And user click terapkan
    And user click search button on main page filter
    And user click action Button
    Then user verify search result on main page bse contains tandai belum follow up

  @TEST_BBM-1383
  Scenario: Filter by BSE
    Given user navigates to "pms singgahsini"
    And user login as "pman admin"
    When user click Tenant Communication menu
    And user click filter penyewa
    And user click BSE Maya
    And user click terapkan
    And user click search button on main page filter
    Then user verify search result on main page bse contains BSE Maya