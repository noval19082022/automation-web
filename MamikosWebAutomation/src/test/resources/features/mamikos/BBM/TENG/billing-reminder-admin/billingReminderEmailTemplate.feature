@regression @billingReminder @BBM3

  @BBM-987 @BBM-989 @BBM-985 @BBM-983
Feature: Billing Reminder - Email Template

  Background: User arrived on Email Template page
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user click on Billing Reminder Template from left menu
    * user click "Email Template" submenu of Billing Reminder Template

  Scenario: user set the initial state to display Email template Day -1
    When user set the initial state to display Email template Day -1

#  BBM-987
  Scenario: Delete Template
    When user delete Email Template with content "untuk automation"

#  BBM-983
  Scenario: Add Template With Existing Day Period
    Given user click add Email Template button
    When user select Email day period with "0"
    * user fill Email Template subject with "untuk automation"
    * user fill Email Template content with "untuk automation content"
    * user click create Email Template button
    Then user verify add template callout error with "Cannot create template."

#  BBM-985
  Scenario: Add Template
    Given user click add Email Template button
    When user select Email day period with "-1"
    * user fill Email Template subject with "untuk automation"
    * user fill Email Template content with "untuk automation content"
    * user click create Email Template button
    Then user verify Email Template subject with "untuk automation"
    * user verify Email Template content with "untuk automation content"

#  BBM-989
  Scenario: Edit Template
    Given user click edit Email Template button
    When user fill Email Template subject with "untuk automation"
    * user fill Email Template content with "untuk automation content"
    * user click save on edit Email Template page
    Then user verify Email Template subject with "untuk automation"
    * user verify Email Template content with "untuk automation content"