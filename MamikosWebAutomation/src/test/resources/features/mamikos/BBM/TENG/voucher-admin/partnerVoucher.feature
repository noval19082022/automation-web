@regression @BBM4 @voucher @voucher-admin

Feature: Voucher Discount - Partner Voucher

  @BBM-823
  Scenario: Delete existing voucher code
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Partner Voucher" sub menu of voucher discount
    And user edit voucher "adiautomate" and set voucher code is ""
    Then failed update voucher and display text validation "The Voucher Code field is required."

  @BBM-828
  Scenario: Update unique voucher campaign data & published check box
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Partner Voucher" sub menu of voucher discount
    And user edit voucher "adiautomate" and set campaign title is "test"
    And user click published check box
    Then message success update voucher is present

  @BBM-827
  Scenario: Update unique voucher data with voucher code already exist
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Partner Voucher" sub menu of voucher discount
    And user edit voucher "adiautomate" and set voucher code is "imagelaravelpartner"
    Then system display error message voucher code already exists "Voucher Codes [imagelaravelpartner] already exists in other campaign."

  @BBM-814
  Scenario: Update unique voucher data
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Partner Voucher" sub menu of voucher discount
    And user edit voucher "ADIAUTOMATE" and set voucher code is "ADIAUTOMATE12345678"
    Then message success update voucher is present
    When user edit voucher "ADIAUTOMATE12345678" and set voucher code is "ADIAUTOMATE"
    Then message success update voucher is present

  @BBM-825
  Scenario: Add/update published voucher without fill campaign field
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Partner Voucher" sub menu of voucher discount
    And user clicks on add voucher partner
    And user input voucher code "testAutomation"
    And user input total quota "2"
    And user click published check box on page create voucher
    And user clicks on add voucher
    Then user see validation error field is required
    |The Campaign Title field is required.                |
    |The Campaign Image field is required.                |
    |The Campaign Terms & Conditions field is required.   |
    |The Campaign Name / Partner Name field is required.  |
    |The Start Date field is required.                    |
    |Manual Target require one of field in TARGET section.|

  @BBM-821
  Scenario: Add/update voucher without fill required field
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Partner Voucher" sub menu of voucher discount
    And user clicks on add voucher partner
    And user clicks on add voucher
    Then user see validation error field is required
      |The Campaign Name / Partner Name field is required.  |
      |The Voucher Code field is required.                  |
      |The Start Date field is required.                    |
      |The Total Quota field is required.                   |
      |Manual Target require one of field in TARGET section.|

  @BBM-824
  Scenario: Voucher code number less than email/user id
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Partner Voucher" sub menu of voucher discount
    And user edit voucher "adiautomate" and set voucher code is "voucherTest1,voucherTest2,voucherTest3"
    Then user see validation error field is required
      |Voucher General cannot have more than 1 code.  |
      |Email count must be the same with voucher code count   |

  @BBM-826
  Scenario: Add or Update voucher with more than 1 voucher code
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Partner Voucher" sub menu of voucher discount
    And user edit voucher "cobalagi1" and set voucher code is "cobalagi1,cobalagi1,cobalagi1"
    Then message success update voucher is present