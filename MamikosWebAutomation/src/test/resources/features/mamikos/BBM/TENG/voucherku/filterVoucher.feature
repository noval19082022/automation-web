@regression @BBM5 @filterVoucher

Feature: Filter Voucher

  @BBM-841 @BBM-843
  Scenario Outline: Filter Mass Voucher - Status
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    Then user see "<filter>" filter list option on voucher menu:
      | <value1> |
      | <value2> |
      | <value3> |
    When user choose to filter "<filter>" on voucher menu with value "<value1>"
    Then voucher with selected filter value "<value1>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value2>"
    Then voucher with selected filter value "<value2>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value3>"
    Then voucher with selected filter value "<value3>" is displayed
    Examples:
      | filter | value1 | value2 | value3     |
      | status | All    | Active | Not Active |

  @BBM-839
  Scenario Outline: Filter Mass Voucher - Rules
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    Then user see "<filter>" filter list option on voucher menu:
      | <value1> |
      | <value2> |
      | <value3> |
      | <value4> |
      | <value5> |
    When user choose to filter "<filter>" on voucher menu with value "<value1>"
    Then voucher with selected filter value "<value1>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value2>"
    Then voucher with selected filter value "<value2>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value3>"
    Then voucher with selected filter value "<value3>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value4>"
    Then voucher with selected filter value "<value4>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value5>"
    Then voucher with selected filter value "<value5>" is displayed
    Examples:
      | filter | value1 | value2         | value3 | value4    | value5    |
      | rules  | All    | For First Paid | For DP | Recurring | Pelunasan |

  @BBM-840
  Scenario Outline: Filter Mass Voucher - Team
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    Then user see "<filter>" filter list option on voucher menu:
      | <value1> |
      | <value2> |
      | <value3> |
      | <value4> |
      | <value5> |
    When user choose to filter "<filter>" on voucher menu with value "<value1>"
    Then voucher with selected filter value "<value1>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value2>"
    Then voucher with selected filter value "<value2>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value3>"
    Then voucher with selected filter value "<value3>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value4>"
    Then voucher with selected filter value "<value4>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<value5>"
    Then voucher with selected filter value "<value5>" is displayed
    Examples:
      | filter        | value1 | value2   | value3 | value4    | value5 |
      | campaign_team | All    | Business | HR     | Marketing | Other  |

  @BBM-829 @BBM-831
  Scenario Outline: Filter Mass Voucher - Voucher ID / Code / Campaign Name Filter
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And user choose to filter "<filter>" on voucher menu with value "<ID>"
    Then voucher with selected filter value "<ID>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<voucherCode>"
    Then voucher with selected filter value "<voucherCode>" is displayed
    When user choose to filter "<filter>" on voucher menu with value "<campaignName>"
    Then voucher with selected filter value "<campaignName>" is displayed

    #BBM-831
    When user choose to filter "<filter>" on voucher menu with value "<resetFilter>"
    Then voucher with selected filter value "<resetFilter>" is displayed
    When user click reset filter button on voucher page
    Then list table voucher doesn't contain "<resetFilter>"
    Examples:
      | filter           | ID    | voucherCode    | campaignName    | resetFilter       |
      | campaign_voucher | 34785 | automateFilter | automate Filter | There is no data. |