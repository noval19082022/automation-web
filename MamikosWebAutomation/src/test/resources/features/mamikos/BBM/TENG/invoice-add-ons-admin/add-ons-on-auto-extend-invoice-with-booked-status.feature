@regression @addons @BBM-1180 @BBM1

Feature: Add Ons Fee On Auto Extend Invoice With Booked Status

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost Add Ons
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Add Ons" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    * user navigates to Booking Request page
    * user select kost "Kost Adi Auto Add Ons" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

    #Add adds on part
  Scenario: Admin Master Add, Add Ons Fee On Auto Extend Invoice With Booked Status
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice details first index
    * admin adds custom add ons with name "adiautomation" price amount "100000"
    Then admin can sees add ons successfully added

    #Payment Part 1st month
  Scenario: Tenant Pay 1st Month Booking And Check Add Ons Fee On Auto Extend Invoice With Booked Status
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * user teng make bill payments using "Mandiri"
    #Payment Part 1st month

    #Checkin part
    * user navigates to "mamikos /user/booking/"
    * user check-in at kost "teng adds on"

  #add ons fee on recurring from add ons that added on first invoice with booked status
    Given user navigates to "mamikos /"
    When user navigates to "mamikos /user/booking/"
    * tenant clicks on kost saya
    * tenant clicks on tagihan
    * tenant clicks on Bayar button
    Then tenant can sees add ons fee with name "adiautomation" and price "100000"
    * tenant can sees total cost is equal to :
      | Harga Sewa          |
      | adiautomation  |
      | Admin Fee           |
    When tenant goes to invoice page with payment method is "BNI"
    Then tenant can sees add ons fee with name "adiautomation" and price "100000"