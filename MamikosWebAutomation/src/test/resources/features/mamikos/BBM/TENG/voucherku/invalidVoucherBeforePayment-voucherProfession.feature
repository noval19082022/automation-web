@regression @BBM7 @voucher

  @BBM-763
Feature: Invalid Voucher After Applied, Invalid Profession

  Scenario: Activate voucher AUTOPROFESSION
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOPROFESSION" and set target profession is "Mahasiswa"
    Then System display alert message "updated" on mamipay web

#  BBM-763
  Scenario: Tenant Input Voucher AUTOPROFESSION
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher goldplus"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    When user access voucher form
    * input "AUTOPROFESSION" to field voucher code
    * user click use button
    Then voucher applied successfully

  Scenario: Deactivate voucher AUTOPROFESSION
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOPROFESSION" and set target profession is "Karyawan"
    Then System display alert message "updated" on mamipay web

  Scenario: Hapus Button Functionality on Toast Message
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher goldplus"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    Then system display toast message "Kode voucher tidak bisa digunakan. Silakan hapus voucher."
    * system display icon voucher invalid
    When user remove voucher by toast message
    Then system display toast message "Voucher Dihapus"