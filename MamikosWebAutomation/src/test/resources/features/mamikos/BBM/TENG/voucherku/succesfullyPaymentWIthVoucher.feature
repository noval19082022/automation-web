@regression @BBM5 @voucher

Feature: Successfully Payment with Voucher

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user click on Search Contract Menu form left bar
    * user search contract by tenant phone number "adi TENG apply voucher dua"
    * admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher dua"
    When user navigates to "mamikos /user/booking"
    * user cancel booking

#  Scenario: Tenant Booking Regular
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Regular" and select matching result to go to kos details page
    * user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Voucher" and click on Enter button
    When user navigates to "owner /booking/request"
    * user select kost "Kost Adi Auto Regular" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

#  Scenario: Tenant Apply Voucher and Successfully Payment with Voucher
    Given user navigates to "mamikos /"
    * user logs out as a Tenant user
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher dua"
    When user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    * system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "VFORPAYMENT" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user make bill payments using "Mandiri"
    Then system display payment using "Mandiri" is "Success Transaction"