@regression @billingReminder @BBM3

  @BBM-988 @BBM-984 @BBM-982 @BBM-980
Feature: Billing Reminder - SMS Template

  Background: User arrived on SMS Template page
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user click on Billing Reminder Template from left menu
    * user click "SMS Template" submenu of Billing Reminder Template

  Scenario: user set the initial state to display SMS template Day -5
    When user set the initial state to display SMS template Day -5

#  BBM-988
  Scenario: Delete Template
      When user delete Email Template with content "untuk automation"

#  BBM-984
  Scenario: Add Template With Existing Day Period
    Given user click add SMS Template button
    When user select SMS day period with "-1"
    * user fill SMS Template content with "untuk automation"
    * user click create SMS Template button
    Then user verify add template callout error with "Cannot create template."

#  BBM-980
  Scenario: Add Template
    Given user click add SMS Template button
    When user select SMS day period with "-5"
    * user fill SMS Template content with "untuk automation"
    * user click create SMS Template button
    Then user verify SMS Template content with "untuk automation"

#  BBM-982
  Scenario: Edit Template
    Given user click edit SMS Template button
    When user fill SMS Template content with "untuk automation"
    * user click save on edit SMS Template page
    Then user verify SMS Template content with "untuk automation"