@regression @BBM4 @voucher @voucher-admin

Feature: Admin - Single Voucher

  @BBM-838
  Scenario: Create Targeted Voucher Fill Email Field
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And admin master goes to Single Voucher tab
    And admin clicks on add single voucher button
    And admin master inputs voucher campaign name to "teng-test-automation"
    And admin master sets campaign date to "today" and end date to ""
    And admin master select campaign team to "OTHER"
    And admin master enter voucher prefix to "BBM"
    And admin master enter total targeted voucher "2"
    And admin master tick payment rules :
      |booking_with_dp               |
      |booking                       |
      |pelunasan                     |
      |recurring                     |
    And admin select contract rules :
      | booking               |
      | consultant            |
      | owner                 |
      | tenant                |
    And admin select discount type "Percentage"
    And admin master input discount amount "50"
    And admin master input total each quota to "5" and daily each quota to "1"
    And admin master input maximum amount for voucher discount "500000"
    And admin master input minimum transaction "100000"
    And admin master input targeted email below :
      | putriani.laila@gmail.com |
      | tenantsla@gmail.com      |
    And admin master tick important check box :
      | is_active   |
      | is_testing  |
    And admin master clicks on add single voucher button
    Then admin can sees callout message is "New targeted voucher added!"