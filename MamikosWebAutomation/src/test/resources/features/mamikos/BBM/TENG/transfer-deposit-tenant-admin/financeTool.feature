@regression @TENGP2 @teng-admin-transfer-deposit-tenant
Feature: Finance Tool

  @TENG-2073 @TENG-2080 @TENG-2078
  Scenario: click detail Transfered Process Tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Transfer Process"
    Then user will be see search column and detail tab Process
    When user click button detail on field action
    Then user will be see pop up with title "Transfer Deposit"
    And user see tenant name, kost name, bank, nomor rekening, nama rekening, transfer due date, transfer success, sisa deposit and button kembali
    When user click button back on popup
    Then user will be see search column and detail tab Process

  @TENG-2075
  Scenario: click tandai sudah transfer in failed tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Failed"
    And user click button tandai sudah transfer on field action
    Then user will be see pop up with title "Transfer Deposit" on tab Failed

  @TENG-2076 @TENG-2083
  Scenario: click transfer ulang in failed tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Failed"
    And user click button transfer ulang on field action
    Then user will be see pop up with title "Transfer Deposit"
    And user see tenant name, kost name, bank, nomor rekening, nama rekening, transfer due date, transfer success, sisa deposit and button kembali
    When user click button back on popup
    Then user will be see search column and detail tab Failed
    When user click button transfer ulang on field action
    And user click button transfer ulang on popup
    Then user will be see search column and detail tab Failed

  @TENG-2077
  Scenario: click transfer ulang in failed tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Failed"
    Then user will be see search column and detail tab Failed

  @TENG-2074
  Scenario: click detail Transfered Process Tab
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Transfer Process"
    And user click button receipt on field action
    Then system display message success download "Bukti transfer didownload."

  @TENG-2081 @TENG-2082
  Scenario: click transfer on tab Confirmed
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Confirmed"
    And user click button transfer on field action
    Then user see tenant name, kost name, bank, nomor rekening, nama rekening, transfer due date, transfer success, sisa deposit and button kembali
    When user click button back on popup
    Then user will be see search column and detail tab Confirmed
    When user click button transfer on field action
    And user click button transfer ulang on popup
    Then system display message success download "Sisa deposit diproses"

  @TENG-2079
  Scenario: See detail popup Transfer deposit for manual by admin
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Transfer Process"
    When user click button detail on field action
    Then user will be see pop up with title "Transfer Deposit"
    And user will be see button download is not available

  @TENG-115 @TENG-108
  Scenario: Search data with use valid tenant number
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Confirmed"
    And user choose "Tenant Phone Number" on filter search by
    And user input search value "082124838922"
    And user click button search on page transfer sisa deposit
    Then system display transfer sisa deposit list "082124838922"
    When user input search value "082124838929"
    And user click button search on page transfer sisa deposit
    Then system display transfer sisa deposit list sort by invalid search value "Belum Ada Data"

  @TENG-111 @TENG-103
  Scenario: Search data with use valid and invalid Kost Name
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Confirmed"
    And user choose "Kost Name" on filter search by
    And user input search value "enam enam enam Bantul Bantul"
    And user click button search on page transfer sisa deposit
    Then system display transfer sisa deposit list "enam enam enam Bantul Bantul"
    When user input search value "Cibinong Kost"
    And user click button search on page transfer sisa deposit
    Then system display transfer sisa deposit list sort by invalid search value "Belum Ada Data"

  @TENG-109 @TENG-110
  Scenario: Search data with use valid and invalid Kost Name
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Confirmed"
    And user choose "Tenant Name" on filter search by
    And user input search value "Sanity Qa"
    And user click button search on page transfer sisa deposit
    Then system display transfer sisa deposit list "Sanity Qa"
    When user input search value "Sanity Test"
    And user click button search on page transfer sisa deposit
    Then system display transfer sisa deposit list sort by invalid search value "Belum Ada Data"

  @TENG-104 @TENG-105
  Scenario: Search data with use valid and invalid owner phone number
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And admin clicks on Transfer Deposit Tenant Menu form left bar
    And user click Tab "Confirmed"
    And user choose "Owbner Phone Number" on filter search by
    And user input search value "083320000006"
    And user click button search on page transfer sisa deposit
    Then system display transfer sisa deposit list "Sanity Qa"
    When user input search value "083320000010"
    And user click button search on page transfer sisa deposit
    Then system display transfer sisa deposit list sort by invalid search value "Belum Ada Data"
