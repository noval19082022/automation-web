@regression @tenantTracker @BBM3

Feature: SinggahSini - Tenant Tracker - Main Page Button Follow Up

  @BBM-566
  Scenario: Action Button Tandai Belum Follow Up
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Properti" on main page filter
    * user input "BSE" in the search field on main page
    * user click search button on main page filter
    * user click action Button
#    Set the initial state to belum follow up
    * user set the initial state to Tandai belum follow-up
#    Set to sudah follow up
    * user click action Button
    * user choose tandai sudah follow up on action button
    * user click action Button
    Then user verify search result on main page bse contains tandai belum follow up
#    Set to belum follow up
    When user click action Button
    * user choose tandai belum follow up on action button
    * user click action Button
    Then user verify search result on main page bse contains tandai sudah follow up