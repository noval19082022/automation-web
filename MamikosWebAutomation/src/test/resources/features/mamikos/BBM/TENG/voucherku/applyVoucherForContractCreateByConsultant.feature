@regression @BBM7 @voucher

  @BBM-677 @BBM-684 @BBM-686 @BBM-675
Feature: Apply Voucher For Contract Created By Consultant

#  BBM-686
  Scenario: Tenant Apply Voucher with Contract Rules from from Consultant
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher consultant"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOCONSULTANT" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  BBM-684
#  Scenario: Tenant Apply Voucher with Contract Rules from Owner
    When user access voucher form
    And input "AUTOOWNER" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  BBM-677
#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel
    And user access voucher form
    And input "AUTOBOOKFUNNEL" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher with Contract Rules from Tenant
    When user access voucher form
    And input "AUTOTENANT" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel and Consultant
    When user access voucher form
    And input "AUTOFUNNELCONS" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel and Owner
    When user access voucher form
    And input "AUTOFUNNELOWNER" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  BBM-675
#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel, Owner, and Consultant
    When user access voucher form
    When input "AUTOFUNOWNCONS" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"