@consultantMapping @TENGP3

  Feature: Consultant Mapping
    @createMapping1Kecamatan
    Scenario: Create a mapping that contains 1 kecamatan
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Consultant Mapping menu
      And user click on "Add Mapping" button
      And user fill add mapping data with:
      | BALI |
      | KABUPATEN BADUNG |
      | KUTA SELATAN     |
      | Noval Setiawan (noval@mamikos.com) |
      Then user verify success notification with "Success! Successfully created"

    @createMappingWithExistingKecamatan
    Scenario: Create a mapping containing kecamatan that have been mapped
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Consultant Mapping menu
      And user click on "Add Mapping" button
      And user fill add mapping data with:
        | BALI |
        | KABUPATEN BADUNG |
        | KUTA SELATAN |
        | Noval Setiawan (noval@mamikos.com) |
      Then user verify success notification with "Success! Successfully created"

    @addKecamatanInExistingMapping
    Scenario: Create a mapping containing kecamatan that have been mapped
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Consultant Mapping menu
      And user search mapping by consultant name with "Noval Setiawan"
      And user click edit profil on mapping result
      And user add kecamatan with "KUTA"
      Then user verify update success notification with "Success! Successfully update consultant mapping"

    @createMappingAllKecamatan
    Scenario: Create a mapping that contains All kecamatan
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Consultant Mapping menu
      And user click on "Add Mapping" button
      And user fill add mapping data with:
        | BALI |
        | KABUPATEN BADUNG |
        | Pilih Semua Kecamatan |
        | Noval Setiawan (noval@mamikos.com) |
      Then user verify success notification with "Success! Successfully created"
      And user search mapping by consultant name with "Noval Setiawan"
      And user click hapus profil on mapping result