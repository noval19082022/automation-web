@regression @mainPageFilter @BBM3

Feature: Main Page Add Tracker Status WA

  @BBM-570
  Scenario: Action Button Add Tracker Status WA
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Properti" on main page filter
    * user input "BSE" in the search field on main page
    * user click search button on main page filter
    * user click action Button
    * user click add tracker status WA
    * user filled "prioritaskan" in note field tracker WA status
    * user click Tambah in tracker status WA
    Then user verify add tracker status wa