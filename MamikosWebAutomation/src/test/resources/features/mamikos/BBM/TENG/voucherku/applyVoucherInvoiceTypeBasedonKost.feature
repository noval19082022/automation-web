@regression @BBM3 @voucher

  @BBM-599
Feature: Invoice Type based on Kost

  Scenario: Deleting existing booking
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "voucher base on user"
    And admin master terminate contract by today date

#  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "voucher base on user"
    And user navigates to "mamikos /user/booking/"
    And user cancel booking

  Scenario: Invoice Type based on kost
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user login in as Tenant via phone number as "voucher base on user"
    When user clicks search bar
    And I search property with name "applyVoucherBBK" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user input rent duration equals to 4
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

#  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "apply voucher" and click on Enter button
    * user navigates to "owner /booking/request"
    And user select kost "Kos Loyal Kretek" on booking request page
    When user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button

#    BBM-599
#  Scenario: Tenant Apply Voucher Invoice Type based on kost
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    And user clicks on Enter button
    And user login in as Tenant via phone number as "voucher base on user"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user access voucher form
    And input "VBKOSTFORFIRST" to field voucher code
    And user click use button
    Then voucher applied successfully