@regression @BBM7 @voucher

  @BBM-681
Feature: Apply Voucher For Kost Type BBK
#    BBM-681
  Scenario: Invoice BBK and Voucher Applicable for Kost Type BBK
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "VBBKFORBOOKING1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#    TENG-2516
#  Scenario: Invoice BBK and Voucher Not Applicable for Kost Type BBK
    When user access voucher form
    * input "VNABBKBOOKING1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Applicable for Kost Type MAMIROOMS
    When user access voucher form
    * input "VMRBOOKING1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Not Applicable for Kost Type MAMIROOMS
    When user access voucher form
    * input "VNMRBOOKING1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Applicable for Kost Type Premium
    When user access voucher form
    * input "VAFORPREMIUM1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Not Applicable for Kost Type Premium
    When user access voucher form
    *  input "VNAFORPREMIUM1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Applicable for Kost Type Verified By Mamichecker
    When user access voucher form
    * input "VAFORMCHECKER1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Not Applicable for Kost Type Verified By Mamichecker
    When user access voucher form
    * input "VNAFORMCHECKER1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Applicable for Kost Type Garansi
    When user access voucher form
    * input "VAFORGARANSI1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Not Applicable for Kost Type Garansi
    When user access voucher form
    *  input "VNAFORGARANSI1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Applicable for Kost Type Goldplus 1
    When user access voucher form
    * input "AUTOGP1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice BBK and Voucher Not Applicable for Kost Type Goldplus 1
    When user access voucher form
    * input "AUTOVNAGP1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"