@regression @BBM7 @voucher

  @BBM-736 @BBM-737 @BBM-640 @BBM-633 @BBM-630
Feature: Apply Voucher For Platform
#  BBM-736
  Scenario: Tenant Input Voucher Platform for Android and iOS
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOANDROIDXIOS" to field voucher code
    * user click use button
    Then system display voucher alert message "Hanya berlaku di aplikasi Android dan iOS."
    * system display remaining payment "before" use voucher for payment "monthly"

#  BBM-737
#  Scenario: Tenant Input Voucher Platform for iOS
    When user access voucher form
    * input "AUTOIOS" to field voucher code
    * user click use button
    Then system display voucher alert message "Hanya berlaku di aplikasi iOS."
    * system display remaining payment "before" use voucher for payment "monthly"

#  BBM-640
#  Scenario: Tenant Input Voucher Platform for Android
    When user access voucher form
    * input "AUTOANDROID" to field voucher code
    * user click use button
    Then system display voucher alert message "Hanya berlaku di aplikasi Android."
    * system display remaining payment "before" use voucher for payment "monthly"

#  BBM-633
#  Scenario: Tenant Input Voucher Platform for Web, Android, and iOS
    When user access voucher form
    * input "AUTOWEBADRIOS" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"

#  BBM-630
#  Scenario: Tenant Input Voucher Platform for Web
    When user remove voucher
    * user access voucher form
    * input "AUTOWEB" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    * user remove voucher