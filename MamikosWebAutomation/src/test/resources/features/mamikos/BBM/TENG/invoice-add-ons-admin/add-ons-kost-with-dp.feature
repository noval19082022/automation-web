@regression @addons @BBM-1417 @BBM-1418 @BBM1

Feature: Add Ons - Kost With DP

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost Add Ons
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto With DP Add Ons" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    * user navigates to Booking Request page
    * user select kost "Kost Adi Auto With DP Add Ons" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Tenant Pay 1st Month Booking For Add Ons
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * user make bill payments using "Mandiri"

    #Add adds on part to ST
  Scenario: Admin Master Add, Add Ons Kost With DP To Settlement Invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice settlement details
    * admin adds custom add ons with name "adiautomation" price amount "100000"
    Then admin can sees add ons successfully added
    When admin deletes additional other price with name below :
      | adiautomation |

  #BBM-1418
  #Scenario: Check Add Ons Fee That Already Deleted For Unpaid ST/Booked Invoice
    * user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * tenant clicks on Bayar Pelunasan Sekarang button
    But tenant can not sees price with name "adiautomation" on invoice page