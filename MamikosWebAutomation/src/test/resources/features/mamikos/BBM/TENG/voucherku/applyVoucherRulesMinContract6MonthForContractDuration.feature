@regression @BBM3 @voucher

Feature: Apply Voucher Semi-Annually For Contract Duration

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "adi TENG apply voucher"
    * admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost With Payment Period Semi Annually
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto SinggahSini" and select matching result to go to kos details page
    * user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user select payment period "Per 6 Bulan"
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Voucher" and click on Enter button
    * user navigates to "owner /booking/request"
    * user select kost "Kost Adi Auto SinggahSini" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

#    BBM-617
#  Scenario: Tenant Apply Voucher VSEMIANNUALLY
    Given user navigates to "mamikos /"
    * user logs out as a Tenant user
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    * user access voucher form
    * input "VSEMIANNUALLY" to field voucher code
    * user click use button
    Then voucher applied successfully

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "adi TENG apply voucher"
    * admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost With Payment Period Annually
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto SinggahSini" and select matching result to go to kos details page
    * user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user select payment period "Per Tahun"
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Voucher" and click on Enter button
    * user navigates to "owner /booking/request"
    * user select kost "Kost Adi Auto SinggahSini" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

#    BBM-617
#  Scenario: Tenant Apply Voucher VSEMIANNUALLY
    Given user navigates to "mamikos /"
    * user logs out as a Tenant user
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    * user access voucher form
    * input "VSEMIANNUALLY" to field voucher code
    * user click use button
    Then voucher applied successfully