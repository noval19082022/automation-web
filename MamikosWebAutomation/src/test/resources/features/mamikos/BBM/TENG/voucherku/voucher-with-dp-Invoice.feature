@regression @BBM5 @voucher

  @BBM-680
Feature: Voucher that has been used on the DP invoice cannot be used on invoice settlement

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user click on Search Contract Menu form left bar
    * user search contract by tenant phone number "adi TENG apply voucher dua"
    * admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher dua"
    When user navigates to "mamikos /user/booking"
    * user cancel booking

#  Scenario: Tenant Booking Kost With DP
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Voucher DP" and select matching result to go to kos details page
    * user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Voucher" and click on Enter button
    When user navigates to "owner /booking/request"
    * user select kost "Kost Adi Auto Voucher DP" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

#  Scenario: Tenant Apply Voucher DP on Invoice DP
    Given user navigates to "mamikos /"
    * user logs out as a Tenant user
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher dua"
    When user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    * user access voucher form
    * input "AUTODPST" to field voucher code
    * user click use button
    Then voucher applied successfully
    When user make bill payments using "Mandiri"

#  Scenario: Tenant Apply Voucher DP on Invoice Settlement
    When user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user access voucher form
    * input "AUTODPST" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."