@regression @BBM7 @voucher

  @BBM-641 @BBM-742
Feature: Apply Voucher For Kost Type Verified By Mamichecker

#  BBM-641
  Scenario: Invoice Verified By Mamichecker and Voucher Applicable for Verified By Mamichecker
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher mamiroom"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOMAMICHECK" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#    BBM-742
#  Scenario: Invoice Verified By Mamichecker and Voucher Not Applicable for Verified By Mamichecker
    When user access voucher form
    And input "VNAMAMICHECK" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"