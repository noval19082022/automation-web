@regression @BBM7 @voucher

  @BBM-755
Feature: Invalid Voucher After Applied, Invalid Voucher Kost

  Scenario: Activate Voucher AUTOKOSTINV
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOKOSTINV" and set voucher "applicable" on "Kost Adi Auto SinggahSini" kost
    Then System display alert message "updated" on mamipay web

#  BBM-755
  Scenario: Tenant Input Voucher AUTOKOSTINV
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOKOSTINV" to field voucher code
    * user click use button
    Then voucher applied successfully

  Scenario: Deactivate voucher AUTOKOSTINV
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOKOSTINV" and set voucher "not applicable" on "Kost Adi Auto SinggahSini" kost
    Then System display alert message "updated" on mamipay web

  Scenario: Hapus Button Functionality on Toast Message
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    Then system display toast message "Kode voucher tidak bisa digunakan. Silakan hapus voucher."
    * system display icon voucher invalid
    When user remove voucher by toast message
    Then system display toast message "Voucher Dihapus"