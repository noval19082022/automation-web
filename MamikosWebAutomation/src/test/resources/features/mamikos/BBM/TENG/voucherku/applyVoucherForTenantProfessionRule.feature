@regression @voucher @BBM7 @regressProd

  @BBM-638 @BBM-637 @BBM-634 @BBM-629 @BBM-587 @BBM-588 @BBM-646 @BBM-647 @BBM-626 @BBM-625
  @BBM-623 @BBM-631 @BBM-648 @BBM-649 @BBM-642 @BBM-643
Feature: Apply Voucher For Tenant Profession, Domain, and Email Rule

  Scenario: Edit Tenant Profession to Mahasiswa
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/edit-profil"
    * user choose profession "mahasiswa"
    * user select university name "Universitas Bengkulu"
    * user click save button
    Then user verify pop up save profil "Profil Disimpan" and "Kamu telah berhasil menyimpan profil"
    * user click on OK button

#   BBM-638
  Scenario: Invoice Mahasiswa and Voucher Applicable for Mahasiswa
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "VAFORMAHASISWA" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-637
#  Scenario: Invoice Mahasiswa and Voucher Not Applicable for Mahasiswa
    When user access voucher form
    * input "VNAFORMAHASISWA" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-634
#  Scenario: Invoice Mahasiswa and Voucher Applicable for Karyawan
    When user access voucher form
    * input "VAFORKARYAWAN" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-629
#  Scenario: Invoice Mahasiswa and Voucher Not Applicable for Karyawan
    When user access voucher form
    * input "VNAFORKARYAWAN" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-587
#  Scenario: Invoice Mahasiswa and Voucher Applicable for Tenant University
    When user access voucher form
    * input "VAFORUNIV" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-588
#  Scenario: Invoice Mahasiswa and Voucher Not Applicable for Tenant University
    When user access voucher form
    * input "VNAFORUNIV" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-646
#  Scenario: Invoice Mahasiswa and Voucher Applicable for Other Tenant University
    When user access voucher form
    * input "VAFOROTHERUNIV" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-647
#  Scenario: Invoice Mahasiswa and Voucher Not Applicable for Other Tenant University
    When user access voucher form
    * input "VNAFOROTHERUNIV" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

  Scenario: Edit Tenant Profession to Karyawan
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/edit-profil"
    And user choose profession "karyawan"
    And user select office name "Bukit Asam Tbk"
    * user click save button
    Then user verify pop up save profil "Profil Disimpan" and "Kamu telah berhasil menyimpan profil"
    * user click on OK button

#   BBM-626
  Scenario: Invoice Karyawan and Voucher Applicable for Karyawan
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "VAFORKARYAWAN" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-625
#  Scenario: Invoice Karyawan and Voucher Not Applicable for Karyawan
    When user access voucher form
    And input "VNAFORKARYAWAN" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#   BBM-623
#  Scenario: Invoice Karyawan and Voucher Applicable for Mahasiswa
    When user access voucher form
    * input "VAFORMAHASISWA" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-631
#  Scenario: Invoice Karyawan and Voucher Not Applicable for Mahasiswa
    When user access voucher form
    * input "VNAFORMAHASISWA" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-648
#  Scenario: Invoice Karyawan and Voucher Applicable for Tenant Company
    When user access voucher form
    * input "VAFORCOMP" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-649
#  Scenario: Invoice Karyawan and Voucher Not Applicable for Tenant Company
    When user access voucher form
    * input "VNAFORCOMP" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-642
#  Scenario: Invoice Karyawan and Voucher Applicable for Other Tenant Company
    When user access voucher form
    * input "VAFOROTHERCOMP" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#   BBM-643
#  Scenario: Invoice Karyawan and Voucher Not Applicable for Other Tenant Company
    When user access voucher form
    * input "VNAFOROTHERCOMP" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable for Other Tenant Email Domain
    When user access voucher form
    * input "VNAFOROTHERDOM" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable for Tenant Email
    When user access voucher form
    * input "VNAFORUSREMAIL" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable for Other Tenant Email
    When user access voucher form
    * input "VNAFOROTHEREM" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"