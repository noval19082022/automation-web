@regression @mamipoinTenant @BBM3

Feature: MamiPoin Tenant Entry Point

  @BBM-415
  Scenario: MamiPoin Tenant Entry Point When the User is Blacklisted
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG blacklist mamipoin"
    * user choose "Profil" on dropdown list profile
    Then user verify mamipoin tenant entry point is not displayed

  Scenario: MamiPoin Tenant Entry Point When the User is Whitelisted
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG whitelist mamipoin"
    * user choose "Profil" on dropdown list profile
    Then user verify mamipoin tenant entry point is displayed
    * user verify the amount of poin owned by the tenant is "123.456"
    When user clicks on mamipoin tenant entry point button
    Then user verify title in the mamipoin tenant landing page is displayed

  @BBM-424
  Scenario: MamiPoin Tenant Entry Point When the User Poin is Empty
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG empty mamipoin"
    * user choose "Profil" on dropdown list profile
    Then user verify mamipoin tenant entry point is displayed
    * user verify the amount of poin owned by the tenant is "0"
    When user clicks on mamipoin tenant entry point button
    Then user verify title in the mamipoin tenant landing page is displayed