@regression @BBM1 @addons

@BBM-1086
Feature: Add Ons - Extended Contract

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost Add Ons
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Add Ons" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    * user navigates to Booking Request page
    * user select kost "Kost Adi Auto Add Ons" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Tenant Pay 1st Month Booking For Add Ons
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * user make bill payments using "Mandiri"

#  Checkin part
    * user navigates to "mamikos /"
    * user navigates to "mamikos /user/booking/"
    * user check-in at kost "teng adds on"

  Scenario: Admin Master Add, Add Ons To Tenant Contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin clicks on add ons button on contract "1" index
    * admin add add ons "Laundry" to tenant contract

  Scenario: Tenant Pay Booking 2nd Month For Add Ons Flow Extended Contract
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant clicks on kost saya
    * tenant clicks on tagihan
    * tenant clicks on Bayar button
    * user make bill payments using "Mandiri"

#  Assertion part
  Scenario: Admin Master Verify That Add Ons Successfully Added To Tenant Contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user click on Search Contract Menu form left bar
    * user search contract by tenant phone number "0890867321212"
    * admin clicks on invoice number "3" on first index contract
    Then admin can sees add ons price with name "Laundry" price "50000"

#  Assertion part
  Scenario: Admin Verify That Add Ons Successfully Added To Tenant Contract
    Given user navigates to "mamikos admin"
    When user logs in to Mamikos Admin via credentials as "admin ob"
    * user access to data booking menu
    * user show filter data booking
    * user search data booking using tenant phone "Teng Adds On"
    * user apply filter
    * admin go to data booking detail for phone "0890867321212" first index
    * admin goes to invoice detail from contract log "3" index
    Then admin can sees add ons price with name "Laundry" price "50000"