@regression @billingManagement @billingManagementSorting @TENGP2
Feature: TENG Billing Management Sorting

  @TENG-891 @TENG-892 @TENG-893
  Scenario: Sort By Due Data, Name, And Room Number
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "teng manage bills" and click on Enter button
    And owner go to Kelola Tagihan page
    And owner set Kelola Tagihan filter month to "5" month and year to "2021"
    And owner sets sorting to "Kamar"
    Then owner can sees "Room" is sorted accordingly
    When owner sets sorting to "Nama Penyewa"
    Then owner can sees "Renter Name" is sorted accordingly
    When owner sets sorting to "Tanggal Jatuh Tempo"
    Then owner can sees "Due Date" is sorted accordingly