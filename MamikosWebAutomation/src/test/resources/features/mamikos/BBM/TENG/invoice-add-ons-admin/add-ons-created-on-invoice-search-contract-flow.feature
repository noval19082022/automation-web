@regression @TENG-34 @TENG-26 @TENG-27 @BBM1 @addons

Feature: Teng Add Ons Search Contract Flow

  Scenario: Tenant Login For Add Ons Search Contract Flow
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "teng invoice detail"
    * user navigates to "mamikos /user/booking/"
    * store cookies data to "/src/test/resources/testdata/cookies/" with name "tenant-teng-admin-invoice"

  Scenario: Owner Login For Add Ons Search Contract Flow
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "teng admin invoice" and click on Enter button
    * user navigates to "mamikos /ownerpage/manage/all/booking"
    * store cookies data to "/src/test/resources/testdata/cookies/" with name "teng-admin-invoice"

  Scenario: Admin Login For Add Ons Search Contract Flow
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin ob"
    * user access to data booking menu
    * store cookies data to "/src/test/resources/testdata/cookies/" with name "tenant-admin-admin-invoice"

  Scenario: Terminate Contract For Add Ons Search Contract Flow
    #Contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "087708777615"
    * admin master terminate contract by today date

    #Delete need confirmation booking
  Scenario: Tenant Cancel Booking For Add Ons Search Contract Flow
    Given user navigates to "mamikos /"
    * user clear all cookies data
    * write cookies data from path "/src/test/resources/testdata/cookies/" with file name "tenant-teng-admin-invoice"
    * write cookies data from path "/src/test/resources/testdata/cookies/" with file name "tenant-teng-admin-invoice"
    * tenant refresh page
    * user navigates to "mamikos /user/booking/"
    * tenant cancel all need confirmation booking request

  Scenario: Tenant Booking For Add Ons Search Contract Flow
    #Booking Part
    Given user navigates to "mamikos /"
    * write cookies data from path "/src/test/resources/testdata/cookies/" with file name "tenant-teng-admin-invoice"
    * tenant refresh page
    When user clicks search bar
    * I search property with name "teng adds on kost" and select matching result to go to kos details page
    * user clicks Booking button kost details page and booking for today date
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button

  Scenario: Owner Accept Booking For Add Ons Search Contract Flow
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "teng admin invoice" and click on Enter button
    * user navigates to "mamikos /ownerpage/manage/all/booking"
    * user select kost with name "teng adds on kost"
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button
    #Booking Part

    #Payment Part 1st month
  Scenario: Tenant Pay 1st Month Booking For Add Ons Search Contract Flow
    When user navigates to "mamikos /"
    * write cookies data from path "/src/test/resources/testdata/cookies/" with file name "tenant-teng-admin-invoice"
    * tenant refresh page
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * tenant switch tab to "2"
    * user make bill payments using "Mandiri"
    #Payment Part 1st month

    #Checkin part
    * user navigates to "mamikos /user/booking/"
    * user check-in at kost "teng adds on"

    #Add adds on part
  Scenario: Check Add Ons Search Contract Flow
    When user navigates to "backoffice"
    * user login  as a Admin via credentials
    * user click on Search Contract Menu form left bar
    * user search contract by tenant phone number "087708777615"
    * admin clicks on add ons button on contract "1" index
    * admin add add ons "cuci baju" to tenant contract
    * user click on Search Contract Menu form left bar
    * user search contract by tenant phone number "087708777615"
    * admin clicks on invoice number "2" on first index contract
    Then admin can sees add ons price with name "cuci baju" price "30000"

    #Admin side
    When user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin ob"
    * user access to data booking menu
    * user show filter data booking
    * user search data booking using tenant phone "Teng Adds On"
    * user apply filter
    * admin go to data booking detail for phone "087708777615" first index
    * admin goes to invoice detail from contract log "2" index
    Then admin can sees add ons price with name "cuci baju" price "30000"

  #TENG-26
  #Scenario: Delete Add Ons From Contract Flow
    When user navigates to "backoffice"
    * user click on Search Contract Menu form left bar
    * user search contract by tenant phone number "087708777615"
    * admin clicks on add ons button on contract "1" index
    * admin deletes add ons from contract flow
    Then admin can sees callout message is "Berhasil menghapus add on"
    When user navigates to "backoffice"
    * user click on Search Contract Menu form left bar
    * user search contract by tenant phone number "087708777615"
    * admin clicks on invoice number "2" on first index contract
    * tenant switch tab to "5"
    Then admin can not sees price with name "cuci baju" in invoice details

    When user navigates to "mamikos admin"
    * write cookies data from path "/src/test/resources/testdata/cookies/" with file name "tenant-admin-admin-invoice"
    * user navigates to "mamikos /admin/booking/users"
    * user navigates to "mamikos /admin/booking/users"
    * user show filter data booking
    * user search data booking using tenant phone "Teng Adds On"
    * user apply filter
    * admin go to data booking detail for phone "087708777615" first index
    * admin goes to invoice detail from contract log "2" index
    * tenant switch tab to "6"
    Then admin can not sees price with name "cuci baju" in invoice details