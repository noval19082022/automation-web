@regression @BBM4 @voucher @voucher-admin

Feature: Admin - Single Voucher

  @BBM-830 @BBM-842
  Scenario: Create Single Voucher Prefix Without Fill Email Field
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And admin master goes to Single Voucher tab
    And admin clicks on add single voucher button
    And admin master inputs voucher campaign name to "bbm-test-automation"
    And admin master sets campaign date to "today" and end date to ""
    And admin master select campaign team to "OTHER"
    And admin master enter voucher prefix to "BBM"
    And admin master enter total targeted voucher "10"
    And admin master tick payment rules :
      |booking_with_dp               |
      |booking                       |
      |pelunasan                     |
      |recurring                     |
    And admin select contract rules :
      | booking               |
      | consultant            |
      | owner                 |
      | tenant                |
    And admin select discount type "Percentage"
    And admin master input discount amount "50"
    And admin master input total each quota to "5" and daily each quota to "1"
    And admin master input maximum amount for voucher discount "500000"
    And admin master input minimum transaction "100000"
    And admin master tick important check box :
      | is_active   |
      | is_testing  |
    And admin master clicks on add single voucher button
    Then admin can sees callout message is "New targeted voucher added!"

  @BBM-836 @BBM-844 @BBM-842
  Scenario: Update Single Voucher
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And admin master goes to Single Voucher tab
    And admin master fills prefix or campaign name input filter to "BBM"
    And admin master clicks in search button in voucher list page
    And admin master clicks on edit pencil icon index "1" in voucher list result
    Then admin master can sees campaign name is "bbm-test-automation"
    And admin master can sees campaign date is "today" and end date to ""
    And admin master can sees campaign team is set to "OTHER" with prefix contain "BBM"
    And admin master can sees total targeted voucher is "10"
    And admin master can sees payment rules ticked :
      |booking_with_dp               |
      |booking                       |
      |pelunasan                     |
      |recurring                     |
    And admin master can sees contract rules ticked :
      | booking               |
      | consultant            |
      | owner                 |
      | tenant                |
    And admin master can sees discount type "Percentage" with amount is "50"
    And admin master can sees total each quota is "5" with daily each quota is "1"
    And admin master can sees maximum amount is "500000" and minimum "100000" for transaction
    And admin master can sees important rules is ticked :
      | is_active   |
      | is_testing  |
  #BBM-844
  #Scenario: Voucher Status Not Publish
    And admin master can sees important rules is not ticked :
      | public_campaign[is_published] |
  #BBM-842
  #Scenario: Voucher status is Active
    And admin master clicks on add single voucher button
    Then admin master can sees update voucher confirmation pop-up
    When admin master clicks on go back button
    Then admin master can not sees update voucher confirmation pop-up
    When admin master clicks on add single voucher button
    And admin master clicks on Yes, Do It! button
    Then admin can sees callout message is "Voucher bbm-test-automation updated"
    When admin master fills prefix or campaign name input filter to "BBM"
    And admin master clicks in search button in voucher list page
    And admin master clicks on voucher list icon index "1"
    Then admin can sees vouchers status are "Active"

  @BBM-833
  Scenario: Voucher Status Publish
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And admin master goes to Single Voucher tab
    And admin master fills prefix or campaign name input filter to "BBM"
    And admin master clicks in search button in voucher list page
    And admin master clicks on edit pencil icon index "1" in voucher list result
    And admin master tick important check box :
      | public_campaign[is_published]   |
    And admin master upload voucher campaign image
    And admin master input campaign title to "Krezi Ollie"
    And admin master input campaign term & conditions to "Terbullie gara gara bakso"
    And admin master clicks on add single voucher button
    And admin master clicks on Yes, Do It! button
    Then admin can sees callout message is "Voucher bbm-test-automation updated"

  @BBM-837
  Scenario: Voucher Status Inactive
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And admin master goes to Single Voucher tab
    And admin master fills prefix or campaign name input filter to "BBM"
    And admin master clicks in search button in voucher list page
    And admin master clicks on edit pencil icon index "1" in voucher list result
    And admin master untick important check box :
      | is_active   |
    And admin master clicks on add single voucher button
    And admin master clicks on Yes, Do It! button
    Then admin can sees callout message is "Voucher bbm-test-automation updated"
    When admin master fills prefix or campaign name input filter to "BBM"
    And admin master clicks in search button in voucher list page
    And admin master clicks on voucher list icon index "1"
    Then admin can sees vouchers status are "Not Active"