@regression @rescheduleRelocation @BBM3

Feature: Reschedule and Relocation

  @BBM-1155 @BBM-1154
  Scenario: Success Reschedule
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user access to data booking menu
    * user clicks the Booking Now button
    * user selects room by Nama Kost "Kost Adi Auto SinggahSini"
    * user fills phone number field on form booking by "0890867321227"
    * user clicks the Search button on form booking
    * user clicks the Next button on form booking step 2
    Then user will be taken to step 3
    When user choose booking type by "rescheduled" on form booking
    * user fills contract id on form booking by "39877"
    Then Contract Valid message on form booking is displayed
    When user fills date on form booking by "2030-08-25"
    * user clicks the Next button on form booking step 3
    * user clicks the Submit button on form booking
    Then user verify success message on data booking page is appear
    * user process to reject booking
    * user choose "Dokumen tidak lengkap" and click on send button

  @BBM-1156
  Scenario: Success Relocation
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user access to data booking menu
    * user clicks the Booking Now button
    * user selects room by Nama Kost "Kost Adi Auto SinggahSini"
    * user fills phone number field on form booking by "0890867321227"
    * user clicks the Search button on form booking
    * user clicks the Next button on form booking step 2
    Then user will be taken to step 3
    When user choose booking type by "relocated" on form booking
    * user fills contract id on form booking by "39877"
    Then Contract Valid message on form booking is displayed
    When user fills date on form booking by "2030-08-25"
    * user clicks the Next button on form booking step 3
    * user clicks the Submit button on form booking
    Then user verify success message on data booking page is appear
    * user process to reject booking
    * user choose "Dokumen tidak lengkap" and click on send button

  @BBM-1153
  Scenario: Reschedule and Contract ID is invalid
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user access to data booking menu
    * user clicks the Booking Now button
    * user selects room by Nama Kost "Kost Adi Auto SinggahSini"
    * user fills phone number field on form booking by "0890867321227"
    * user clicks the Search button on form booking
    * user clicks the Next button on form booking step 2
    Then user will be taken to step 3
    When user choose booking type by "rescheduled" on form booking
    * user fills contract id on form booking by "0"
    Then user verify pop up error on form booking "Cannot continue : contract not valid"

  @BBM-1152
  Scenario: Reschedule and Contract ID is still active
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user access to data booking menu
    * user clicks the Booking Now button
    * user selects room by Nama Kost "Kost Adi Auto SinggahSini"
    * user fills phone number field on form booking by "0890867321227"
    * user clicks the Search button on form booking
    * user clicks the Next button on form booking step 2
    Then user will be taken to step 3
    When user choose booking type by "rescheduled" on form booking
    * user fills contract id on form booking by "37309"
    Then user verify pop up error on form booking "Cannot continue : contract still active"