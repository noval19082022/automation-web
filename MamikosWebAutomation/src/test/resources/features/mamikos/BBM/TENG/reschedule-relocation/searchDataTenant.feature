@regression @rescheduleRelocation @BBM3

Feature: Search Data Tenant on Bangkerupux Admin

  @BBM-1157
  Scenario: Search by Registered Phone Number
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user access to data booking menu
    * user clicks the Booking Now button
    * user selects room by Nama Kost "Kost Adi Auto SinggahSini"
    * user fills phone number field on form booking by "087739881010"
    * user clicks the Search button on form booking
    * user clicks the Next button on form booking step 2
    Then user will be taken to step 3

  @BBM-1158
  Scenario: Search by not Registered Phone Number
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user access to data booking menu
    * user clicks the Booking Now button
    * user selects room by Nama Kost "Kost Adi Auto SinggahSini"
    * user fills phone number field on form booking by "087739889999"
    * user clicks the Search button on form booking
    * user verify pop up error on form booking "User not found"

  @BBM-1189
  Scenario: Search by Registered Name
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user access to data booking menu
    * user clicks the Booking Now button
    * user selects room by Nama Kost "Kost Adi Auto SinggahSini"
    * user choose search tenant by name on form booking
    * user fills phone number field on form booking by "adiSinggahSini"
    * user clicks the Search button on form booking
    * user clicks the Next button on form booking step 2
    Then user will be taken to step 3

  @BBM-1188
  Scenario: Search by not Registered Name
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user access to data booking menu
    * user clicks the Booking Now button
    * user selects room by Nama Kost "Kost Adi Auto SinggahSini"
    * user choose search tenant by name on form booking
    * user fills phone number field on form booking by "awdawdasdada"
    * user clicks the Search button on form booking
    * user verify pop up error on form booking "User not found"