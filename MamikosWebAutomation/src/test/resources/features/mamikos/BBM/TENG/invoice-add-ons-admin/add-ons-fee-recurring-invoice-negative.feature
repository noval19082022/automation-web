@regression @addons @BBM-1095 @BBM1

Feature: Add Ons - Fee Recurring Invoice Negative Scenario

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost Add Ons
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Add Ons" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    * user navigates to Booking Request page
    * user select kost "Kost Adi Auto Add Ons" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Tenant Pay 1st Month Booking For Add Ons
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * user make bill payments using "Mandiri"

  Scenario: Admin Master Add, Add Ons Fee Recurring For Auto Extend Invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice details first index
    * admin adds new fee with type "Add On" index "1"

  Scenario: Tenant Check-in To Kost For Add Ons Fee Recurring Auto Extend Invoice And Check Add Ons Requirement
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user check-in at kost "teng adds on"

   #Payment Part 2nd month
    * tenant clicks on kost saya
    * tenant clicks on tagihan
    * tenant clicks on Bayar button
    * user make bill payments using "Mandiri"
    * user navigates to "mamikos /"
    * user navigates to "mamikos /user/booking/"
    * tenant clicks on kost saya
    * tenant clicks on tagihan
    * tenant clicks on Bayar button
    Then tenant can not sees add on price on payment page
    When tenant goes to invoice page with payment method is "BNI"
    Then tenant can not sees add on price on payment page