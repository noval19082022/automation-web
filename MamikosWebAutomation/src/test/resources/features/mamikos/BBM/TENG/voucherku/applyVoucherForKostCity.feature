@regression @BBM4 @voucher

  @BBM-628 @BBM-652 @BBM-757
Feature: Apply Voucher For Kost City

  Scenario: Activate Voucher AUTOCITY
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOCITY" and set voucher aplicable on "Kabupaten Halmahera Utara" city
    Then System display alert message "updated" on mamipay web

#  BBM-628
  Scenario: Tenant Input Voucher AUTOCITY
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    When user access voucher form
    * input "AUTOCITY" to field voucher code
    * user click use button
    Then voucher applied successfully

  Scenario: Deactivate voucher AUTOCITY
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOCITY" and set voucher not aplicable on "Kabupaten Halmahera Utara" city
    Then System display alert message "updated" on mamipay web

#  BBM-652 BBM-757
  Scenario: Hapus Button Functionality on Toast Message
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    Then system display toast message "Kode voucher tidak bisa digunakan. Silakan hapus voucher."
    * system display icon voucher invalid
    When user remove voucher by toast message
    Then system display toast message "Voucher Dihapus"