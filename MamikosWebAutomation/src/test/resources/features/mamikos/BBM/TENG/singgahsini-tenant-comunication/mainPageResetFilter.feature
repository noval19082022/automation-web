@regression @tenantTracker @BBM3

Feature: SinggahSini - Tenant Tracker - Main Page Reset Filter

  @BBM-569
  Scenario: Reset button On filter Page
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Survei" on filter tahapan and "Diajukan" on filter status
    * user click terapkan
    * user click search button on main page filter
    * user click reset button in PMS Admin
    * user has reset the filter

  @BBM-568
Scenario: Reset Filter on Filter Menu
   Given user navigates to "pms singgahsini"
   * user login as "pman admin"
   When user click Tenant Communication menu
   * user choose "Nama Properti" on main page filter
   * user input "Kost Adi Auto Mamiroom" in the search field on main page
   * user click search button on main page filter
   Then user verify nama property on main page filter is "Kost Adi Auto Mamiroom"
   When user click reset button in PMS Admin
   Then user verify nama property on main page filter is not "Kost Adi Auto Mamiroom"