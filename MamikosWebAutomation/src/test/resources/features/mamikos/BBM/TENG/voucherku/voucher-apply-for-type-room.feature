@regression @voucher @BBM8

  @BBM-739
Feature: Voucher Invalid After Change Room Type

  Scenario: Delete Contract For Voucher Apply For Type Room
    #Contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "087708777615"
    * user click on Cancel the Contract Button from action column

  Scenario: Delete Booking For Voucher Apply For Type Room
    #Delete need confirmation booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "teng invoice detail"
    * user navigates to "mamikos /user/booking/"
    * tenant cancel all need confirmation booking request

  Scenario: Booking For Voucher Apply For Type Room
    #Booking Part
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "teng invoice detail"
    * user clicks search bar
    * I search property with name "teng kost apik" and select matching result to go to kos details page
    * user clicks Booking button kost details page and booking for today date
    * user selects T&C checkbox and clicks on Book button
    * user navigates to "mamikos /"

  Scenario: Owner Accept Booking For Voucher Apply For Type Room
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "teng admin invoice" and click on Enter button
    * user navigates to "mamikos /ownerpage/manage/all/booking"
    * user select kost with name "teng kost apik"
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button
    * owner user navigates to owner page
    #Booking Part

  #@changeroomtype
  Scenario: Tenant Apply Voucher For Voucher Apply For Type Room
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "teng invoice detail"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * user access voucher form
    * input "rezerotest" to field voucher code
    * user click use button

  Scenario: Admin Edit Voucher For For Voucher Apply For Type Room
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * admin master search id, voucher code or campaign name to "Rezero Mamikos Hajimaru"
    * admin master clicks in search button in voucher list page
    * admin master clicks on edit pencil icon index "1" in voucher list result
    * admin master add or set room type to "Basic"
    * admin master clicks on add mass voucher button in voucher form
    * admin master clicks on Yes, Do It! button

  #Scenario assertion part
  Scenario: Tenant Pay Invoice With Invalid Voucher For Type Room
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "teng invoice detail"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * tenant switch tab to "2"
    Then tenant can sees toast with cannot use voucher text with delete button

  Scenario: Admin Edit Voucher For For Voucher Apply For Type Room
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * admin master search id, voucher code or campaign name to "Rezero Mamikos Hajimaru"
    * admin master clicks in search button in voucher list page
    * admin master clicks on edit pencil icon index "1" in voucher list result
    * admin master add or set room type to "APIK"
    * admin master clicks on add mass voucher button in voucher form
    * admin master clicks on Yes, Do It! button