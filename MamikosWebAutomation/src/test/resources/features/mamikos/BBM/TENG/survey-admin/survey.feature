@regression @surveyAdmin @BBM4

  Feature: Admin Survey
    @BBM-512
    Scenario Outline: Change the survey status to waiting
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user click on edit survey button on "<name>"
      And user change survey status to "Waiting"
      Then user verify change survey success alert with "Success! Survey <name> (<kos>) berhasil dirubah"
      Examples:
        | name           | kos             |
        | Aditenant ubah | Kos Lempuyangan |

    @BBM-514
    Scenario Outline: Change the survey status to on survey
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user click on edit survey button on "<name>"
      And user change survey status to "On Survey"
      Then user verify change survey success alert with "Success! Survey <name> (<kos>) berhasil dirubah"
      Examples:
        | name           | kos             |
        | Aditenant ubah | Kos Lempuyangan |

    @BBM-518
    Scenario Outline: Change the survey status to cancelled
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user click on edit survey button on "<name>"
      And user change survey status to "Cancelled"
      Then user verify change survey success alert with "Success! Survey <name> (<kos>) berhasil dirubah"
      Examples:
        | name           | kos             |
        | Aditenant ubah | Kos Lempuyangan |

    @BBM-519
    Scenario Outline: Change the survey date
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user click on edit survey button on "<name>"
      And user change survey date to "Tomorrow"
      Then user verify change survey success alert with "Success! Survey <name> (<kos>) berhasil dirubah"
      Examples:
        | name           | kos             |
        | Aditenant ubah | Kos Lempuyangan |

    @BBM-515
    Scenario Outline: Change the survey time
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user click on edit survey button on "<name>"
      And user change survey time to "12:30"
      Then user verify change survey success alert with "Success! Survey <name> (<kos>) berhasil dirubah"
    Examples:
      | name           | kos             |
      | Aditenant ubah | Kos Lempuyangan |

    @BBM-517
    Scenario: Filter by tenant name
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user fill Tenant Name filter with "Eremes Guile"
      Then user verify Tenant Name filter result with "Eremes Guile"

    @BBM-516
    Scenario: Filter by survey status
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user choose "On Survey" on survey status filter
      Then user verify Status filter result with "Datang Survei"

    @BBM-510
    Scenario: Filter by survey date
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user choose "24 January 2022" on survey date filter
      Then user verify Survey Time filter result with "24 January 2022"

    @BBM-511
    Scenario: Filter by tenant phone number
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user fill Tenant Phone Number filter with "087739881010"
      Then user verify Tenant Phone Number filter result with "087739881010"

    @BBM-509
    Scenario: Filter by property name
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Tenant Survey menu
      And user fill Property Name filter with "Mindful Peaks"
      Then user verify Property Name filter result with "Mindful Peaks"