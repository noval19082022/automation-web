@regression @TENG @TENGP2 @tengReg
Feature: Rajawali Chat

  @TENG-3352
  Scenario: Send Image On Rajawali Chat
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "teng invoice detail"
    And tenant user navigates to Chat page
    And tenant click on first index chat on chat list
    And tenant upload image and tap on sent button
    Then tenant can sees picture is sent