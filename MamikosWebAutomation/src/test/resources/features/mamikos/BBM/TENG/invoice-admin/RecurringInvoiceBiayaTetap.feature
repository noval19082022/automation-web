@regression @BBM2 @BBM-1429

#  kost used: Kost Adi Auto Fpaid
#  (Kost Regular FullPaid without Additional Fee and Deposit)
Feature: Additional Price Biaya Tetap On Invoice Recurring

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost Regular FullPaid without Additional Fee and Deposit
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Fpaid" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 2
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    * user navigates to Booking Request page
    * user select kost "Kost Adi Auto Fpaid" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Add Additional Price Biaya Tetap On Invoice Recurring
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice details first index
    * admin adds new fee with type "Biaya Tetap" name "Listrik" cost "100000"

  Scenario: Tenant Pay 1st Month Booking
    When user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * user make bill payments using "Mandiri"

  Scenario: Tenant Check Additional Price Biaya Tetap Added By Admin On Invoice
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user check-in at kost "Kost Adi Auto Fpaid"
    * tenant clicks on kost saya
    * tenant clicks on tagihan
    * tenant clicks on Bayar button
    Then tenant can sees total cost is equal to basic amount, admin fee plus additional price below
      | 100000 |

  Scenario: Owner Check Additional Price Biaya Tetap Added By Admin On Manage Bills
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    * user navigates to "owner /billing-management"
    * owner sets kost filter to "Kost Adi Auto Fpaid"
    * owner set Kelola Tagihan filter month to "next" month
    * owner goes to bills details
    Then owner can sees total amount is basic amount plus other price
      | 100000 |