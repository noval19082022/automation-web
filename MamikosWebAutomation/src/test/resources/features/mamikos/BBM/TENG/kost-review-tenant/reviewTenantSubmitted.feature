@regression @kostReviewTenant @BBM3

Feature: Kost Review Submitted Display

  @BBM-70 @BBM-72
  Scenario: Kost Review Submitted Display on Kos Saya Page
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG kost review submitted"
    When user navigates to "mamikos /user/kost-saya"
    Then there will be a Kost Review submitted with the title "Kamu memberikan nilai:"
    * there will be a Kost Review submitted with the stars amount "3.0"

  @BBM-65
  Scenario: Kost Review Submitted Display on Berhenti Sewa Page
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG kost review submitted"
    When user navigates to "mamikos /user/kost-saya"
    * user click kontrak on kost saya page
    * user click ajukan berhenti sewa on kontrak saya page
    Then there will be a Kost Review submitted with the title "Kamu memberikan nilai:"
    * there will be a Kost Review submitted with the stars amount "3.0"

  @BBM-74
  Scenario: Kost Review Submitted Display on Riwayat Kos Page
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG kost review submitted"
    When user navigates to "mamikos /user/riwayat-kos"
    Then user verify Kost Review entry point is not displayed