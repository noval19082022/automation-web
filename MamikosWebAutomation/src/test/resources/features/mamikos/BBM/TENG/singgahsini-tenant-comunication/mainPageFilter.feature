@regression @tenantTracker @BBM3
  
Feature: SinggahSini - Tenant Tracker - Main Page Filter

  @BBM-560
  Scenario: Filter By Nama Penyewa
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "Adisinggahsini" in the search field on main page
    * user click search button on main page filter
    Then user verify nama penyewa on main page filter is "Adisinggahsini"

  @BBM-580
  Scenario: Filter By No HP Penyewa
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "No. HP Penyewa" on main page filter
    * user input "0890867321213" in the search field on main page
    * user click search button on main page filter
    Then user verify nama penyewa on main page filter is "Adisinggahsini"

  @BBM-579 @BBM-572
  Scenario: Filter By Nama Properti
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Properti" on main page filter
    * user input "Kost Adi Auto SinggahSini" in the search field on main page
    * user click search button on main page filter
    Then user verify nama property on main page filter is "Kost Adi Auto SinggahSini"

#   Scenario: [BSE Tenant Tracker][Main Page] Nama Properti redirection (BBM-572)
    When user click "Kost Adi Auto SinggahSini"
    * user switch tab to "2"
    Then user redirected to "singgah sini property detail"