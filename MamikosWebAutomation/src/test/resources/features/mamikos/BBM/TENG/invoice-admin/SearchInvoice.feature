@regression @BBM8

Feature: Search invoice
  @autoExtendTrue
  Scenario: Auto extend is true
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    When admin clicks on Search Invoice Menu form left bar
    And user select "True" on auto extend selection
    And user click search invoice button on search invoice admin page
    Then user verify search invoice results have auto extend "True", "success"

  @autoExtendFalse
  Scenario: Auto extend is true
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    When admin clicks on Search Invoice Menu form left bar
    And user select "False" on auto extend selection
    And user click search invoice button on search invoice admin page
    Then user verify search invoice results have auto extend "False", "danger"

  @reminderStatusInformationH-5
  Scenario: Reminder status information H-5
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    When admin clicks on Search Invoice Menu form left bar
    And admin search invoice with "Invoice Number" value "15906136/2021/11/0015"
    And admin clicks on see log button with link value "/52856"
    Then admin verify invoice log has "Billing Reminder H-5" as "Reminder Type"
    And user verify PN Template content with "Wah, 5 hari lagi sewa kos habis"

  @reminderStatusInformationH-1
  Scenario: Reminder status information H-1
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    When admin clicks on Search Invoice Menu form left bar
    And admin search invoice with "Invoice Number" value "15906136/2021/11/0015"
    And admin clicks on see log button with link value "/52856"
    Then admin verify invoice log has "Billing Reminder H-1" as "Reminder Type"
    And user verify PN Template content with "Udah coba bayar kosan yang anti ribet?"

  @billingReminderDueDate
  Scenario: Billing reminder due date
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    When admin clicks on Search Invoice Menu form left bar
    And admin search invoice with "Invoice Number" value "15906136/2021/11/0015"
    And admin clicks on see log button with link value "/52856"
    #    below step is disable due to whatsapp is disable in staging environment
    #    Then admin verify invoice log has "Billing Reminder Due Date" as "Reminder Type"
    Then user verify PN Template content with "Pake Mamikos, bayar kos bisa di mana aja"

  @pnReminderStatusInformation
  Scenario: PN reminder status information
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    When admin clicks on Search Invoice Menu form left bar
    And admin search invoice with "Invoice Number" value "15906136/2021/11/0015"
    And admin clicks on see log button with link value "/52856"
    Then admin verify PN reminder status information
    | Platform          | Content                                | Created At          | Reminder Type             | Status    |
    | Push Notification | Udah coba bayar kosan yang anti ribet? | 2021-09-07 09:05:06 | Billing Reminder Due Date | delivered |

  @whatsAppReminderStatusInformation
  Scenario: PN reminder status information
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    When admin clicks on Search Invoice Menu form left bar
    And admin search invoice with "Invoice Number" value "15906136/2021/11/0015"
    And admin clicks on see log button with link value "/52856"
    Then admin verify WhatsApp reminder status information
      | Platform          | Content                                                                                                                                                                                        | Created At          | Reminder Type                          | Status    |
      | WhatsApp          | Hai Laksana Adi, Udah coba bayar kosan yang anti ribet? Cuma beberapa klik, uang sewa langsung diterima pemilik. Yuk, cobain langsung di https://mamikos.com/user/kost-saya?tagihan=true&ch=08 | 2021-09-06 12:12:10 | Billing Reminder Without Voucher Today | pending   |
