@regression @surveyTenant @BBM5

Feature: Survey Tenant
  @BBM-486
  Scenario: Submit survei from chat template on Kost Detail
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG blacklist mamipoin"
#  Batalkan Survey If the Survey Already Submitted
    * tenant user navigates to Chat page
    * user click "Kost Adi Auto SinggahSini"
    * user batalkan survey if the survey already submitted
#  Submit Survey from chat template on kost detail
    * user clicks search bar
    * user search for Kost with name "Kost Adi Auto SinggahSini" and selects matching result
    * user click chat in kos detail
    * user select question "Saya ingin survei dulu"
    * user input time survey "12:00"
    * user click "Kirim form"
    Then chat room appear with latest message "Terima Kasih, Kak. :) Form survei kakak sudah kami terima. Tim kami akan segera menghubungi kakak melalui WhatsApp."

  @BBM-489
  Scenario: Reschedule survei from chat room
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG blacklist mamipoin"
#  Batalkan Survei If the Survei Already Submitted
    * tenant user navigates to Chat page
    * user click "Kost Adi Auto SinggahSini"
    * user batalkan survey if the survey already submitted
#  Submit Survei from chat room
    * user click Survei Kos button
    * user input time survey "15:00"
    * user click "Kirim form"
#  Reschedule Survei from chat room
    * user click "Ubah Jadwal"
    * user input time survey "9:00"
    * user click ubah jadwal on survey form
    Then survei text generated with data time survey "09.00" inputted by user previously
    * chat room appear with latest message "Terima Kasih, Kak. :) Form survei kakak sudah kami terima. Tim kami akan segera menghubungi kakak melalui WhatsApp."

  @BBM-507
  Scenario: Survey Fase and Status for Controlled Property
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Properti" on main page filter
    * user input "Kost Adi Auto SinggahSini" in the search field on main page
    * user click search button on main page filter
    Then user verify search result on main page contains:
      | Kost Adi Auto SinggahSini |
    * user verify search result on main page bse contains survei

  @BBM-488
  Scenario: Submit Survey for Uncontrolled Property
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG blacklist mamipoin"
    * user clicks search bar
    * user search for Kost with name "Kos Loyal Kretek" and selects matching result
    * user click chat in kos detail
    Then question "Saya ingin survei dulu" is not displayed

  @BBM-508
  Scenario: Survey Fase and Status for Uncontrolled Property
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Properti" on main page filter
    * user input "Kos Loyal Kretek" in the search field on main page
    * user click search button on main page filter
    Then user can see "Data Tidak Ditemukan" on page

  @BBM-490 @BBM-487
  Scenario: Access form survei form chat room
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG blacklist mamipoin"
#  Batalkan Survey If the Survey Already Submitted
    * tenant user navigates to Chat page
    * user click "Kost Adi Auto SinggahSini"
    * user batalkan survey if the survey already submitted
#  Submit Survey from chat room
    * user click Survei Kos button
    * user input time survey "15:00"
    * user click "Kirim form"
    Then chat room appear with latest message "Terima Kasih, Kak. :) Form survei kakak sudah kami terima. Tim kami akan segera menghubungi kakak melalui WhatsApp."
# Scenario: Cancel survei from chat room
    When tenant refresh page
    * tenant user navigates to Chat page
    * user click "Kost Adi Auto SinggahSini"
    * user click "Ubah Jadwal"
    * user click "Batalkan Survei"
    Then chat room appear with latest message "Survei Kost Adi Auto SinggahSini dibatalkan."