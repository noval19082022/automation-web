@regression @BBM7 @voucher

Feature: Apply Voucher For Kost Type Mamirooms

  Scenario: Invoice Mamirooms and Voucher Applicable for Mamirooms
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher mamiroom"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOMAMIROOM" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Mamirooms and Voucher Not Applicable for Mamirooms
    When user access voucher form
    And input "AUTOVNAMAMIROOM" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"