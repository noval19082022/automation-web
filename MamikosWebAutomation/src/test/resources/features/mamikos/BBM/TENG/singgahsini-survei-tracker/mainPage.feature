@regression @surveyTracker @BBM3

Feature: SinggahSini - Survei Tracker - Main Page

  @BBM-480
  Scenario: Pagination Functionality
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Survey Tracker menu
    Then user see pagination menu on Main Page is displayed
    When user click pagination number "2"
    Then user see display data row from 20 riwayat

  @BBM-481
  Scenario: Main Page Display
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Survey Tracker menu
    Then user see at Tenant Main Page Column contains
      | Head Table              |
      | Survey ID               |
      | Profil Pencari Kos      |
      | Nama Properti           |
      | Waktu Survei            |
      | Platform                |
      | Status                  |
      | Update Terakhir         |
      | Update Oleh             |
      | Action                  |

  @BBM-482
  Scenario: Filter By Nama Pencari Kos
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Survey Tracker menu
    * user choose "Nama Pencari Kos" filter in survei tracker
    * user input "Adisinggahsini" in the search field on main page
    * user click search button on main page filter
    Then user verify nama pencari kos from filter in survei tracker is "Adisinggahsini"

  @BBM-484
  Scenario: Filter By No. HP Pencari Kos
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Survey Tracker menu
    * user choose "No. HP Pencari Kos" filter in survei tracker
    * user input "0890867321213" in the search field on main page
    * user click search button on main page filter
    Then user verify nama pencari kos from filter in survei tracker is "Adisinggahsini"

  @BBM-483
  Scenario: Filter By Nama Properti
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Survey Tracker menu
    * user choose "Nama Properti" filter in survei tracker
    * user input "Kost Adi Auto SinggahSini" in the search field on main page
    * user click search button on main page filter
    Then user verify nama properti from filter in survei tracker is "Kost Adi Auto SinggahSini"

  @BBM-485
  Scenario: Filter by Status Tidak Ada Konfirmasi
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Survey Tracker menu
    * user click filter in homepage
    * user search "Tidak Ada Konfirmasi" in status field
    * user click terapkan survei
    * user click search button on main page filter survei
    Then user verify search result on main page bse contains Tidak Ada Konfirmasi