@regression @kostReviewTenant @BBM3

Feature: Kost Review Not Submitted Display

  @BBM-69 @BBM-63 @BBM-73 @BBM-64 @BM-63 @BM-68 @BBM-67 @BBM-71 @BBM-66
  Scenario: Kost Review Tenant
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "adi TENG kost review not submitted"
    And user click profile on header
    And user click profile dropdown button

    #Scenario: Riwayat Kos - The review menu page BBM-69
    And user click Riwayat Kos menu
    Then there will be a review menu with the title "Bagaimana pengalaman ngekosmu?"

    #Scenario: Riwayat Kos - The review menu redirection to review page BBM-63
    When user click review on riwayat kos page
    Then user will see review form

    #Scenario: Kos Saya - The review menu page BBM-73
    When user navigates to "mamikos /user/kost-saya"
    Then there will be a review menu with the title "Bagaimana pengalaman ngekosmu?Menyenangkan atau ada yang perlu ditingkatkan? Yuk, tulis review kamu."

    #Scenario: Kos Saya - The review menu redirection to review page BBM-67 BBM-66
    When user click review on riwayat kos page
    Then user will see review form

    #Scenario: Kos Saya (Ajukan Berhenti Sewa) - The review menu page BBM-64
    When user navigates to "mamikos /user/kost-saya"
    And user click kontrak on kost saya page
    And user click ajukan berhenti sewa on kontrak saya page
    Then there will be a review menu with the title "Bagaimana pengalaman ngekosmu?Menyenangkan atau ada yang perlu ditingkatkan? Yuk, tulis review kamu."

    #Scenario: Kos Saya (Ajukan Berhenti Sewa) - The review menu redirection to review page BM-63
    When user click review on riwayat kos page
    Then user will see review form

    #Scenario: Review Page BBM-71
    Then user see at review page contains:
      | Review Page       |
      | Kebersihan*       |
      | Keamanan*         |
      | Kenyamanan*       |
      | Fasilitas Kamar*  |
      | Fasilitas Umum*   |
      | Kesesuaian Harga* |

    #Scenario: Kos Saya (Ajukan Berhenti Sewa) - Ajukan Berhenti Sewa button is disabled if the tenant hasn't submitted a review yet BM-68
    Then user see ajukan berhenti sewa button is disabled