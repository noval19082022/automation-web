@regression @searchContract @BBM3

Feature: Search Contract
  @BBM-1395
  Scenario: Display contract id column
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user access search contract menu
    Then user verify "Contract Id" column is displayed
