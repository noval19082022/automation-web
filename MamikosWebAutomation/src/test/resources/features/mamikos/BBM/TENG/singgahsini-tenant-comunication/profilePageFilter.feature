@regression @tenantTracker @BBM3

Feature: SinggahSini - Tenant Tracker - Profile Page Filter

  @BBM-567
  Scenario: Filter By Nama Properti
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "adiSinggahSini" in the search field on main page
    * user click search button on main page filter
    * user clicks on the tenant name on the first row
    * user input "Kost Adi Manual" in the search field on main page
    * user click search button on main page filter
    Then user see pagination menu on Detail Tenant is displayed
    * user verify nama property on profile page filter is "Kost Adi Manual"