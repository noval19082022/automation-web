@regression @BBM2 @TENG-12 @TENG-9 @TENG-8
Feature: Settlement Invoice Additional Price Other Price / Biaya Lainnya

  Scenario: Terminate Contract For Settlement Invoice, Add Additional Price Biaya Lainnya And Biaya Tetap
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "087708777617"
    * user click on Cancel the Contract Button from action column

  Scenario: Tenant Cancel Booking For Settlement Invoice, Add Additional Price Biaya Lainnya And Biaya Tetap
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "teng settlement invoice"
    * user navigates to "mamikos /user/booking/"
    * tenant cancel all need confirmation booking request

  Scenario: Tenant Booking For Settlement Invoice, Add Additional Price Biaya Lainnya And Biaya Tetap
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "teng settlement invoice"
    * user clicks search bar
    * I search property with name "teng settlement invoice kost" and select matching result to go to kos details page
    * user clicks on Booking button on Kost details page
    * user selects T&C checkbox and clicks on Book button

  Scenario: Owner Accept Booking For Settlement Invoice, Add Additional Price Biaya Lainnya And Biaya Tetap
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "teng admin invoice" and click on Enter button
    * user navigates to "mamikos /ownerpage/manage/all/booking"
    * user select kost with name "settlement invoice kost"
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  @TENG-9
  Scenario: Admin Changed Basic Amount DP Settlement Invoice, Add Additional Price Biaya Lainnya And Biaya Tetap
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "087708777618"
    * admin clicks on invoice settlement details
    Then admin changes DP basic amount and verify total amount change on settlement invoice for tenant "087708777618"

  Scenario: Tenant Pay DP For Settlement Invoice, Add Additional Price Biaya Lainnya And Biaya Tetap
    When user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "teng settlement invoice"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * user make bill payments using "Mandiri"

  @TENG-8
  Scenario: Add Additional Price Other Price / Biaya Lainnya To Settlement Invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "087708777618"
    * admin clicks on invoice settlement details
    * admin adds new fee with type "Biaya Lainnya" name "Parkir Sampah Settlement" cost "50000"
    * admin adds new fee with type "Biaya Tetap" name "Nonton Turnament Esport Settlement" cost "50000"
    Then admin can sees total cost is basic amount + biaya tetap + biaya lainnya
  #TENG-8
  #Scenario: Edit Basic Amount Settlement
    Then admin edits basic amount and verify basic amount not changed

  Scenario: Tenant Check Additional Price Biaya Lainnya Add By Admin On Settlement Invoice
    When user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "teng settlement invoice"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * tenant clicks on Bayar Pelunasan Sekarang button
    Then tenant can sees total cost is equal to basic amount, admin fee plus additional price below
      | 50000 |
      | 50000 |

  Scenario: Owner Check Additional Price Other Price / Biaya Lainnya Add By Admin For Settlement Invoice On Manage Bills
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "teng admin invoice" and click on Enter button
    * user navigates to "owner /billing-management"
    * owner sets kost filter to "Kost Wild Rift Settlement"
    * owner set Kelola Tagihan filter month to "current" month and year to "current"
    * owner sets renter name to "Nunu And Willump"
    * owner goes to bills details
    Then owner can sees total amount is basic amount plus other price
      | 50000 |
      | 50000 |

  #Settlement Invoice Add Additional Price Biaya Lainnya And Biaya Tetap