@regression @voucher @BBM7

Feature: Apply Voucher For Invoice Reccuring

  Scenario: Invoice Reccuring and Voucher For First Full Paid
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "VTOTALUSAGE" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First Full Paid and Recurring
    When user access voucher form
    * input "VBOOKINGANDRECC1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First Full Paid and Settlement
    When user access voucher form
    * input "VBOOKINGANDSETT1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For Recurring
    When user access voucher form
    * input "VRECONLY1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For Settlement
    When user access voucher form
    * input "VSETTONLY1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For Recurring and Settlement
    When user access voucher form
    * input "VRECCANDSETT1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First Full Paid, Reccuring, and Settlement
    When user access voucher form
    * input "VBOOKRECCSETT1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First DP
    When user access voucher form
    * input "VDPONLY1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First DP and First Full Paid
    When user access voucher form
    * input "VFPANDDP1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First DP and Settlement
    When user access voucher form
    * input "VDPANDSETT1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First DP and Recurring
    When user access voucher form
    * input "VDPANDRECC1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First Full Paid, For First DP, and Settlement
    When user access voucher form
    * input "VFPDPANDSETT1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First Full Paid, For First DP, and Recurring
    When user access voucher form
    * input "VFPDPANDREC1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First DP, Settlement, and Recurring
    When user access voucher form
    * input "VDPSETTANDREC1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Invoice Reccuring and Voucher For First Full Paid, For First DP, Settlement, and Recurring
    When user access voucher form
    * input "VALLPRULES1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"