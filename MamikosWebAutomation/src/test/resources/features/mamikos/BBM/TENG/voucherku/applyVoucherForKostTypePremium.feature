@regression @voucher @BBM8

Feature: Apply Voucher For Kost Type Premium

  Background: Deleting existing booking
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search for "applyVoucherPremium" and cancel contract

  Scenario: Tenant Booking Kost Type Premium
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "apply voucher"
    When user clicks search bar
    And I search property with name "applyVoucherPremium" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user input rent duration equals to 4
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "premium" and click on Enter button
    * user navigates to "owner /booking/request"
    And user select kost "Kos Raya Gasim" on booking request page
    When user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button

#  Scenario: Tenant Apply Voucher for Kost Type Premium
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    And user clicks on Enter button
    When user logs in as Tenant via Facebook credentails as "apply voucher"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And system display remaining payment "before" use voucher for payment "monthlyNewGP"
    And user access voucher form
    And input "VAForPremium1" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthlyNewGP"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthlyNewGP"

#  Scenario: Tenant Apply Voucher Not Applicable Kost Type Premium
    When user access voucher form
    And input "VNAForPremium1" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthlyNewGP"

