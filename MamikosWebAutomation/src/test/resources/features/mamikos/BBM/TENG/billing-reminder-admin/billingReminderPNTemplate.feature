@regression @billingReminder @BBM3

  @BBM-978 @BBM-981 @BBM-979 @BBM-977
Feature: Billing Reminder - PN Template

  Background: User arrived on Email Template page
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user click on Billing Reminder Template from left menu
    * user click "PN Template" submenu of Billing Reminder Template

  Scenario: user set the initial state to display PN template Day -1
    When user set the initial state to display PN template Day -1

#  BBM-979
  Scenario: Delete Template
    When user delete PN Template with content "untuk automation"

#  BBM-977
  Scenario: Add Template With Existing Day Period
    Given user click add PN Template button
    When user select PN day period with "-5"
    * user fill PN Template title with "untuk automation"
    * user fill PN Template content with "untuk automation content"
    * user click create PN Template button
    Then user verify add template callout error with "Cannot create template."

#  BBM-978
  Scenario: Add Template
    Given user click add PN Template button
    When user select PN day period with "-1"
    * user fill PN Template title with "untuk automation"
    * user fill PN Template content with "untuk automation content"
    * user click create PN Template button
    Then user verify PN Template title with "untuk automation"
    * user verify PN Template content with "untuk automation content"

#  BBM-981
  Scenario: Edit Template
    Given user click edit PN Template button
    When user fill PN Template title with "untuk automation"
    * user fill PN Template content with "untuk automation content"
    * user click save on edit PN Template page
    Then user verify PN Template title with "untuk automation"
    * user verify PN Template content with "untuk automation content"