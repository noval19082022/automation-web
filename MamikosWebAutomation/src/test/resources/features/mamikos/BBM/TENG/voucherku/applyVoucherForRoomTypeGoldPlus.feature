@regression @voucher @BBM8

  @BBM-602
Feature: Apply Voucher For Room Type Gold Plus

  Background: Deleting existing booking
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search for "applyVoucherGoldPlus" and cancel contract

  Scenario Outline: Tenant Booking Kost For Room Type <example description>
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "apply voucher"
    When user clicks search bar
    And I search property with name "applyVoucherGoldPlus" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user input rent duration equals to 4
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

#  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "apply voucher" and click on Enter button
    * user navigates to "owner /booking/request"
    And user select kost "Kos Rampak Gasim" on booking request page
    When user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button

#  Scenario: Tenant Apply Voucher Applicable for Room Type
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    And user clicks on Enter button
    When user logs in as Tenant via Facebook credentails as "apply voucher"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And system display remaining payment "before" use voucher for payment "monthly"
    And user access voucher form
    And input "<voucher code applicable for room type>" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable for Room Type
    When user access voucher form
    And input "<voucher code not applicable for room type>" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Applicable For Other Room Type
    When input "<voucher code applicable for other room type>" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable For Other Room Type
    When input "<voucher code not applicable for other room type>" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"
    Examples:
      | example description | room number       | voucher code applicable for room type | voucher code not applicable for room type | voucher code applicable for other room type | voucher code not applicable for other room type |
      | Gold Plus 1         | Kamar 1 Lantai 1  | VAForRoomGP11                         | VNAForRoomGP11                            | VAForRoomGP21                               | VNAForRoomGP21                                  |
      | Gold Plus 2         | Kamar 2 Lantai 1  | VAForRoomGP21                         | VNAForRoomGP21                            | VAForRoomGP31                               | VNAForRoomGP31                                  |
      | Gold Plus 3         | Kamar 3 Lantai 1  | VAForRoomGP31                         | VNAForRoomGP31                            | VAForRoomGP41                               | VNAForRoomGP41                                  |
      | Gold Plus 4         | Kamar 4 Lantai 1  | VAForRoomGP41                         | VNAForRoomGP41                            | VAForRoomGP11                               | VNAForRoomGP11                                  |

