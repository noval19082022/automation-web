@regression @addons @TENG-65 @BBM1
Feature: Add Ons Disbursement Owner

  Scenario: Terminate Contract For Add Ons Disbursement Owner
    #Contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "087708777615"
    And user click on Cancel the Contract Button from action column

    #Delete need confirmation booking
  Scenario: Cancel Booking For Add Ons Disbursement Owner
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "teng invoice detail"
    * user navigates to "mamikos /user/booking/"
    And tenant cancel all need confirmation booking request

  Scenario: Tenant Booking For Add Ons Disbursement Owner
    #Booking Part
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "teng invoice detail"
    And user clicks search bar
    And I search property with name "teng adds on kost" and select matching result to go to kos details page
    And user clicks Booking button kost details page and booking for today date
    And user selects T&C checkbox and clicks on Book button

  Scenario: Owner Accept Booking For Add Ons Disbursement Owner
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "teng admin invoice" and click on Enter button
    * user navigates to "mamikos /ownerpage/manage/all/booking"
    And user select kost with name "teng adds on kost"
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button
    #Booking Part

    #Payment Part 1st month
  Scenario: Tenant Pay 1st Month Booking For Add Ons Disbursement Owner
    When user navigates to "mamikos /"
    And user clicks on Enter button
    And user login in as Tenant via phone number as "teng invoice detail"
    And user navigates to "mamikos /user/booking/"
    And tenant click Bayar Sekarang button
    And user make bill payments using "Mandiri"
    #Payment Part 1st month

    #Add adds on part
  Scenario: Admin Master Add, Add Ons To Tenant Contract
    When user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "087708777615"
    And admin backoffice add adds on to tenant contract

    #Checkin part
  Scenario: Tenant Checking To Kost And Pay 2nd Month Invoice For Add Ons Disbursement Owner
    When user navigates to "mamikos /"
    And user clicks on Enter button
    And user login in as Tenant via phone number as "teng invoice detail"
    And user navigates to "mamikos /user/booking/"
    * user check-in at kost "teng adds on"

    #Payment Part 2nd month
    And tenant clicks on kost saya
    And tenant clicks on tagihan
    And tenant clicks on Bayar button
    And user make bill payments using "Mandiri"

    #Admin allow diisbursement
  Scenario: Admin Allow Disbursement For Add Ons Disbursement Owner
    When user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin ob"
    And user access to data booking menu
    And user show filter data booking
    And user search data booking using tenant phone "Teng Adds On"
    And user apply filter
    And user allow disbursement on tenant "087708777615"

  Scenario: Owner Check Add Ons Disbursement Owner
    When user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "teng admin invoice" and click on Enter button
    * user navigates to "owner /billing-management"
    And owner set Kelola Tagihan filter month to "current" month and year to "current"
    And owner sets kost filter to "Yamie Panda Kost Deposit"
    And owner goes to bills details
    Then owner can not sees add ons with name "Laundry" in the price list
    When owner clicks on in mamikos tab
    And owner set Kelola Tagihan filter month to "current" month and year to "current"
    And owner sets kost filter to "Yamie Panda Kost Deposit"
    And owner goes to bills details
    Then owner can not sees add ons with name "Laundry" in the price list
    When owner clicks on transferred tab
    When owner set Kelola Tagihan filter month to "current" month and year to "current"
    And owner sets kost filter to "Yamie Panda Kost Deposit"
    And owner goes to bills details
    Then owner can not sees add ons with name "Laundry" in the price list
