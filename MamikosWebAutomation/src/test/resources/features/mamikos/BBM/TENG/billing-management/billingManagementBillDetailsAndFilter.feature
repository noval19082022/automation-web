@regression @billingManagement @billsDetailsAndFilter @TENGP2
Feature: Teng Bills Details Billing Management And Filter

  Background: Delete Contract
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "teng tenant phone number"
    And user click on Cancel the Contract Button from action column

  @payOutsideMamipay @TENG-902 @TENG-901 @TENG-882 @TENG-883 @TENG-884
  Scenario: Pay Outside Of Mamipay, Edit Additional Price, And Filter
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "teng booking"
    And user clicks search bar
    And I search property with name "teng booking" and select matching result to go to kos details page
    And user choose booking date for tomorrow and proceed to booking form
    And user selects T&C checkbox and clicks on Book button
    And user navigates to "mamikos /"
    And user logs out as a Tenant user
    When user clicks on Enter button
    And user fills out owner login as "teng manage bills" and click on Enter button
    And user navigates to Booking Request page
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user enters deposite panalty info as deposite fee "100" , panalty fee "200" , panalty fee duration "1" , panalty fee duration type "Bulan" and click on Save
    And owner user navigates to owner page
    And owner go to Kelola Tagihan page
    #TENG-882 line 32
    Then owner can see kost filter is selected with kost name "Kost Name Manage Bills"
    #TENG-883 line 34-35
    When owner set Kelola Tagihan filter month to "current" month and year to "current"
    Then owner can see result on Belum Bayar box
    #TENG-884 line 37-38
    When owner set Kelola Tagihan filter rent count by "Per Hari"
    Then owner can see result on Belum Bayar box
    When owner set Kelola Tagihan filter rent count by "Semua Hitungan Sewa"
    And owner goes to bills details
    #TENG-901 line 34-35
    And owner clicks on edit additional price
    Then owner can sees edit additional price pop-up
    When owner ticks on pay outside mamipay checkbox
    Then owner can sees pay outside mamipay pop-up
    When owner clicks on yes mark button on pay outside mamipay pop-up
    Then owner can sees payment status is "Telah Diterima di luar MamiPAY"

  @TENG-903
  Scenario: Edit Bank Transfer Data
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "teng edit bank transfer data" and click on Enter button
    And owner go to Kelola Tagihan page
    And owner set Kelola Tagihan filter month to "current" month and year to "current"
    And owner clicks on in mamikos tab
    And owner goes to bills details
    And owner clicks on bank transfer edit button
    Then owner can sees edit bank transfer pop-up

  @TENG-894
  Scenario: Bilss Details Go To Tenant Details
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "teng edit bank transfer data" and click on Enter button
    And owner go to Kelola Tagihan page
    And owner set Kelola Tagihan filter month to "current" month and year to "current"
    And owner goes to bills details
    And owner clicks on tenant box on bills details
    Then owner redirected to tenant biodata's page

  @TENG-900
  Scenario: Bill Details Edit Kost's Price
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "teng edit bank transfer data" and click on Enter button
    And owner go to Kelola Tagihan page
    And owner set Kelola Tagihan filter month to "current" month and year to "current"
    And owner goes to bills details
    And owner clicks on edit kost price button
    Then owner can see pop-up edit kost price form

  @TENG-886
  Scenario: Rent Count Filter By Month
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user fills out owner login as "teng edit bank transfer data" and click on Enter button
    And owner go to Kelola Tagihan page
    And owner set Kelola Tagihan filter month to "current" month and year to "current"
    And owner set Kelola Tagihan filter rent count by "Per Bulan"
    Then owner can see result on Belum Bayar box
