@regression @regressProd @BBM4 @voucher

  @BBM-610 @BBM-613 @BBM-612 @BBM-685 @BBM-688 @BBM-687 @BBM-607
  @BBM-608 @BBM-622 @BBM-624 @BBM-636 @BBM-632 @BBM-635 @BBM-627
  @BBM-678 @BBM-621 @BBM-639 @BBM-595 @BBM-590 @BBM-591 @BBM-589
  @BBM-644 @BBM-645 @BBM-596 @BBM-614
Feature: Apply Voucher For Booking Rule, Invoice Without DP

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    When user Navigate "Search Contract" page
    And user search contract by tenant phone number "adi TENG apply voucher dua"
    And admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "adi TENG apply voucher dua"
    And user navigates to "mamikos /user/booking/"
    Then Booking title is displayed
    When user cancel booking

#  Scenario: Tenant Booking Kost Regular
    Given user navigates to "mamikos /"
    When user clicks search bar
    And I search property with name "Kost Adi Auto Regular" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user input rent duration equals to 4
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "adi TENG Owner Voucher" and click on Enter button
    And user navigates to "owner /booking/request"
    When user select kost "Kost Adi Auto Regular" on booking request page
    And user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button

#    BBM-610
#  Scenario: Tenant Apply Voucher for First Paid and Total Usage Limit > 0
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user login in as Tenant via phone number as "adi TENG apply voucher dua"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And system display remaining payment "before" use voucher for payment "monthly"
    And user access voucher form
    And input "VTOTALUSAGE" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-613
#  Scenario: Tenant Apply Voucher for First Paid and Daily Usage Limit > 0
    When user access voucher form
    And input "VDAILYUSAGE" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-612
#  Scenario: Tenant Apply Voucher Code Not Exist
    When user access voucher form
    And input "VoucherCodeNotExist" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak ditemukan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-685
#  Scenario: Tenant Apply Voucher Date Not Started Yet
    When user access voucher form
    And input "AUTONOTSTART" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-688
#  Scenario: Tenant Apply Voucher Already Expired
    When user access voucher form
    And input "AUTOEXPIRED" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-687
#  Scenario: Tenant Apply Voucher Not Meet Minimum Amount Required
    When user access voucher form
    And input "AUTONOTMEETTRX" to field voucher code
    And user click use button
    Then system display voucher alert message "Belum mencapai minimal transaksi."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-607
#  Scenario: Tenant Apply Voucher not Meet Min. Contract Duration
    When user access voucher form
    And input "AUTONOTMEETDUR" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-608
#  Scenario: Tenant Apply Inactive Voucher
    When user access voucher form
    And input "VINACTIVE" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak ditemukan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-622
#  Scenario: Tenant Apply Voucher For First Paid and Reccuring Rule
    When user access voucher form
    And input "AUTOFULLPAIDREC" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-624
#  Scenario: Tenant Apply Voucher For First Paid and Settlement Rule
    When user access voucher form
    And input "AUTOFPAIDSETTLE" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-636
#  Scenario: Tenant Apply Voucher For Reccuring Rule
    When user access voucher form
    And input "AUTORECURRING" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-632
#  Scenario: Tenant Apply Voucher For Settlement Rule
    When user access voucher form
    And input "AUTOSETTLEMENT" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-635
#  Scenario: Tenant Apply Voucher For Reccuring and Settlement Rule
    When user access voucher form
    And input "AUTORECSETTLE" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-627
#  Scenario: Tenant Apply Voucher For First Paid, Reccuring and Settlement Rule
    When user access voucher form
    And input "AUTOFPAIDRECSET" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher For DP
    When user access voucher form
    And input "AUTODP" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-678
#  Scenario: Tenant Apply Voucher For First Paid and DP
    When user access voucher form
    And input "AUTOFPAIDDP" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher For DP And Settlement
    When user access voucher form
    And input "AUTODPSETTLE" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher For DP and Recurring
    When user access voucher form
    And input "AUTODPRECURRING" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher For First Paid, DP and Settlement
    When user access voucher form
    And input "AUTOFPAIDDPSET" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher For First Paid, DP and Recurring
    When user access voucher form
    And input "AUTOFPAIDDPREC" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher For DP, Settlement and Recurring
    When user access voucher form
    And input "AUTODPSETREC" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher For First Paid, DP, Settlement and Recurring
    When user access voucher form
    And input "AUTOALLPAYRULES" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-621
#  Scenario: Tenant Apply Voucher Applicable for Other Kost City
    When user access voucher form
    And input "AUTOOTHERCITY" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-639
#  Scenario: Tenant Apply Voucher Not Applicable for Other Kost City
    When user access voucher form
    And input "AUTONOOTHERCITY" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-595
#  Scenario: Tenant Apply Voucher Applicable for Kost Name
    When user access voucher form
    And input "AUTONAME" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-590
#  Scenario: Tenant Apply Voucher Not Applicable for Kost Name
    When user access voucher form
    And input "AUTONONAME" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-591
#  Scenario: Tenant Apply Voucher Applicable for Other Kost Name
    When user access voucher form
    And input "AUTOOTHERNAME" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-589
#  Scenario: Tenant Apply Voucher Not Applicable for Other Kost Name
    When user access voucher form
    And input "AUTONOOTHERNAME" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-644
#  Scenario: Tenant Apply Voucher Applicable for Tenant Email Domain
    When user access voucher form
    And input "AUTODOMAIN" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-645
#  Scenario: Tenant Apply Voucher Not Applicable for Tenant Email Domain
    When user access voucher form
    And input "AUTONODOMAIN" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-596
#  Scenario: Tenant Apply Voucher Applicable for Other Tenant Email Domain
    When user access voucher form
    And input "AUTOOTHERDOM" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Applicable for Tenant Email
    When user access voucher form
    And input "AUTOEMAIL" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-614
#  Scenario: Tenant Apply Voucher Applicable for Other Tenant Email
    When user access voucher form
    And input "AUTOOTHEREMAIL" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"