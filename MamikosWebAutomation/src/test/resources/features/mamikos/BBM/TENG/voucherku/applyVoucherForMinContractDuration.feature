@regression @voucher @BBM7

  @BBM-609
Feature: Apply Voucher For Invoice Meet Minimal Contract Duration

  Scenario: Invoice Yearly and Voucher For Quarterly
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks Bayar button on Kos Saya
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOQUARTERLY" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"