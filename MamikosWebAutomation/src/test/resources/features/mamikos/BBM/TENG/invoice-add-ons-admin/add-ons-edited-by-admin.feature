@regression @addons @BBM1

@BBM-1088 @BBM-1089 @BBM-1090 @BBM-1091 @BBM1
Feature: Add Ons - Edited By Admin

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost Add Ons
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Add Ons" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 2
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    * user navigates to Booking Request page
    * user select kost "Kost Adi Auto Add Ons" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Admin Master Add And Edit Add Ons Fee Through Detail Fee
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice details first index
    * admin adds custom add ons with name "adiautomation" price amount "100000"
    Then admin can sees add ons successfully added
    #BBM-1090
    * admin edit additional fee "adiautomation" price amount to "150000"
    Then admin master can sees additional success message is "Data fee telah berhasil diubah."
    When admin master edits additional fee "adiautomation" name to "Automation Edit Test"
    Then admin master can sees additional success message is "Data fee telah berhasil diubah."
    * admin master can sees add ons price with name "Automation Edit Test" price "150000" in invoice detail

  Scenario: Tenant Pay 1st Month And Checkin To The Kost For Add Ons Edited By Admin Master
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * user teng make bill payments using "Mandiri"

    When user navigates to "mamikos /"
    * user navigates to "mamikos /user/booking/"
    * user check-in at kost "teng adds on"

  #BBM-1089
  Scenario: Check Add Ons Edited By Admin Master For Recurring/Auto Extend Invoice
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice details first index
    * admin edit additional fee "Automation Edit Test" price amount to "100000"
    * admin master edits additional fee "Automation Edit Test" name to "Automation Add Ons"
    Then admin master can sees add ons price with name "Automation Add Ons" price "100000" in invoice detail

  #BBM-1091
  Scenario: Check Tenant Side After Add Ons Edited By Admin Master For Recurring/Auto Extend Invoice
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant clicks on kost saya
    * tenant clicks on tagihan
    * tenant clicks on Bayar button
    Then admin can sees add ons price with name "Automation Add Ons" price "100000"
    When tenant goes to invoice page with payment method is "BNI"
    Then admin can sees add ons price with name "Automation Add Ons" price "100000"