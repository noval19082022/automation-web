@regression @BBM2

@BBM-1323
Feature: Invoice Detail Kost With DP

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost With DP
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Add Ons" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    When user navigates to "owner /booking/request"
    * user select kost "Kost Adi Auto Add Ons" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Deposit Fee In Invoice Detail Page For DP / Uang Muka
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user navigates to "backoffice"
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice details second index
    Then admin can sees invoice details page
    * admin can sees "Invoice Number" on invoice details page
    * admin can sees "Renter" on invoice details page
    * admin can sees "Invoice Description" on invoice details page
    * admin can sees "Invoice Due Date" on invoice details page
    * admin can sees "Basic Amount" on invoice details page
    * admin can sees total cost is basic amount + admin fee