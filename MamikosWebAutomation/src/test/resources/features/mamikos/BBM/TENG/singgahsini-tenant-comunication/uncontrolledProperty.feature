@regression @tenantTracker @BBM3

Feature: SinggahSini - Tenant Tracker - Uncontrolled Property

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "adi TENG apply voucher"
    * admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Send Message to Kost Regular
    Given user navigates to "mamikos /"
    When user clicks search bar
    * user search for Kost with name "Kost Adi Auto Regular" and selects matching result
    * user click chat in kos detail
    * user select question "Cara menghubungi pemilik?"

#  Scenario: Tenant Booking Kost Regular
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Regular" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Voucher" and click on Enter button
    When user navigates to Booking Request page
    * user select kost "Kost Adi Auto Regular" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  #  Scenario: Tenant Pay First Invoice
    Given user navigates to "mamikos /"
    * user logs out as a Tenant user
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And user make bill payments using "Mandiri"

  Scenario: Tenant Check-in Kost
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user navigates to "mamikos /user/booking/"
    * user check-in at kost "Kost Adi Auto Regular"

  @BBM-574 @BBM-571 @BBM-556
  Scenario: Check-in Fase and Status for Uncontrolled Property
#  Scenario: Chat Fase and Status for Uncontrolled Property
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Properti" on main page filter
    * user input "Kost Adi Auto Regular" in the search field on main page
    * user click search button on main page filter
    Then user can see "Data Tidak Ditemukan" on page