@regression @regressProd @BBM8 @voucher

Feature: Voucher Saya Page

#  TENG-346 #TENG-347
  Scenario: Voucher List Display and Scrolling
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user logs in as Tenant via Facebook credentails as "voucher"
    * user choose "Profil" on dropdown list profile
    Then user see red voucher counter
    When user clicks Voucherku menu
    * user scroll down "Tersedia" voucher list
    * user scroll up "Tersedia" voucher list
    Then user see voucher list header
    * user see Tersedia tab
    * user see Terpakai tab
    * user see Kedaluwarsa tab
    * user see Promo Lainnya
    * user see Lihat button
    * user see voucher card
    * user see voucher list image
    * user see voucher list Expired Date
    * user see voucher list Kode Voucher label
    * user see voucher list Voucher code
    * user see voucher list Salin button
    When user click Terpakai tab
    * user scroll down "Terpakai" voucher list
    * user scroll up "Terpakai" voucher list
    Then user see voucher card
    * user see voucher list image
    * user see voucher list Terpakai label
    * user see voucher list disabled Kode Voucher label
    * user see voucher list disabled Voucher code
    * user see voucher list disabled Salin button
    When user click Kedaluwarsa tab
    * user scroll down "Kedaluwarsa" voucher list
    * user scroll up "Kedaluwarsa" voucher list
    Then user see voucher card
    * user see voucher list image
    * user see voucher list Kedaluwarsa label
    * user see voucher list disabled Kode Voucher label
    * user see voucher list disabled Voucher code
    * user see voucher list disabled Salin button

  #  Scenario: Redirect to Promo Mamikos Web
    When user clicks on Promo Lainnya button
    Then user verify on "https://promo.mamikos.com/"

  Scenario: Voucher Detail Display, Scrolling, and Salin Button Functionality
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user logs in as Tenant via Facebook credentails as "voucher"
    * user navigates to "mamikos /user/voucherku/"
    * user click Salin button from voucher list
    Then user see kode berhasil disalin toast in voucher list
    When user clicks voucher "AUTODATE" on voucher list
    Then user redirected to "/user/voucherku/detail"
    When user scroll down voucher detail
    * user click Salin button from voucher detail
    Then user see kode berhasil disalin toast in voucher detail
    When user scroll up voucher detail
    Then user see voucher detail Image banner
    * user see voucher detail Campaign Title
    * user see voucher detail Expired Date
    * user see voucher detail Syarat dan Ketentuan label
    * user see voucher detail Syarat dan Ketentuan description
    * user see voucher detail Kode Voucher label
    * user see voucher detail Voucher code
    * user see voucher detail Ticket icon
    * user see voucher detail Salin button
    When user click back button in page
    * user click Terpakai tab
    Then user see disabled Salin button in voucher list
    When user clicks Voucher Card in Terpakai tab
    Then user redirected to "/user/voucherku/detail"
    When user scroll down voucher detail
    Then user see disabled Salin button in voucher detail
    When user scroll up voucher detail
    Then user see voucher detail Image banner
    * user see voucher detail Campaign Title
    * user see voucher detail Terpakai Label
    * user see voucher detail Syarat dan Ketentuan label
    * user see voucher detail Syarat dan Ketentuan description
    * user see voucher detail disabled Kode Voucher label
    * user see voucher detail disabled Voucher code
    * user see voucher detail Ticket icon
    * user see disabled Salin button in voucher detail
    When user click back button in page
    * user click Kedaluwarsa tab
    Then user see disabled Salin button in voucher list
    When user clicks Voucher Card in Kedaluwarsa tab
    Then user redirected to "/user/voucherku/detail"
    When user scroll down voucher detail
    Then user see disabled Salin button in voucher detail
    When user scroll up voucher detail
    Then user see voucher detail Image banner
    * user see voucher detail Campaign Title
    * user see voucher detail Kedaluwarsa Label
    * user see voucher detail Syarat dan Ketentuan label
    * user see voucher detail Syarat dan Ketentuan description
    * user see voucher detail disabled Kode Voucher label
    * user see voucher detail disabled Voucher code
    * user see voucher detail Ticket icon
    * user see disabled Salin button in voucher detail

  Scenario: Empty State Landing Page displayed
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user logs in as Tenant via Facebook credentails as "nonvoucher"
    * user navigates to "mamikos /user/voucherku/"
    Then user see Tersedia Empty State Landing Page
    When user click Terpakai tab
    Then user see Terpakai Empty State Landing Page
    When user click Kedaluwarsa tab
    Then user see Kedaluwarsa Empty State Landing Page

  Scenario: Voucher With Expired Date
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user logs in as Tenant via Facebook credentails as "voucher"
    * user navigates to "mamikos /user/voucherku/"
    * user clicks voucher "AUTODATE" on voucher list
    * user scroll down voucher detail
    Then user see expired date voucher "Berlaku hingga 14 Mei 2034"

  Scenario: Voucher Without Expired Date
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user logs in as Tenant via Facebook credentails as "voucher"
    * user navigates to "mamikos /user/voucherku/"
    * user clicks voucher "TersediaAuto2" on voucher list
    * user scroll down voucher detail
    Then user see expired date voucher "Berlaku kapan saja"

  @TENG-321 @TENG-320
  Scenario: Tenant profession is MAHASISWA-Tenant has UNIVERSITY (TENG-321)
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user login in as Tenant via phone number as "TENG voucher"
    And user navigates to "mamikos /user/edit-profil"
    And user choose profession "Mahasiswa"
    And user input university name "Universitas Mamikos"
    Then user verify pop up save profil "Profil Disimpan" and "Kamu telah berhasil menyimpan profil"
    When user click on OK button
    And user clicks Voucherku menu
    Then user verify voucher is displayed
      | MassUniv      |
      | sinxmahasiswa |
    Then user verify voucher is not displayed
      | MassUniv2     |
      | MassCompanies |
      | sinxkaryawan  |

# Scenario: Tenant profession is KARYAWAN-Tenant has COMPANY (TENG-320)
    When user navigates to "mamikos /user/edit-profil"
    And user choose profession "Karyawan"
    And user select office name "PT. Mama Teknologi Property"
    Then user verify pop up save profil "Profil Disimpan" and "Kamu telah berhasil menyimpan profil"
    When user click on OK button
    And user clicks Voucherku menu
    Then user verify voucher is displayed
      | MassCompanies |
      | sinxkaryawan  |
    And user verify voucher is not displayed
      | MassUniv       |
      | MassCompanies2 |
      | sinxmahasiswa  |

  @TENG-322 @TENG-324 @TENG-325 @TENG-326 @TENG-327 @TENG-329 @TENG-330 @TENG-331 @TENG-332 @TENG-344 @TENG-341 @TENG-335
  Scenario: Mass Voucher APPLICABLE for tenant email
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "TENG voucher"
    * user navigates to "mamikos /user/voucherku/"
    Then user verify voucher is displayed
      | MassEmail |

    # Scenario: Mass Voucher APPLICABLE for tenant email domain TENG-324
    Then user verify voucher is displayed
      | MassDomain |

    # Scenario: Mass Voucher targeted for Kost City TENG-322
    Then user verify voucher is displayed
      | MassKostCity |

   # Scenario: Mass Voucher APPLICABLE for other tenant email TENG-326
    Then user verify voucher is not displayed
      | MassEmail2 |

    # Scenario: Mass Voucher APPLICABLE for other tenant email domain TENG-330
    Then user verify voucher is not displayed
      | MassDomain2 |

    # Scenario: Mass Voucher NOT APPLICABLE for tenant email TENG-327
    Then user verify voucher is not displayed
      | MassEmail3 |

    # Scenario: Mass Voucher NOT APPLICABLE for tenant email domain TENG-331
    Then user verify voucher is not displayed
      | MassDomain3 |

    # Scenario: Mass Voucher NOT APPLICABLE for other tenant email TENG-325
    Then user verify voucher is not displayed
      | MassEmail4 |

    # Scenario: Mass Voucher NOT APPLICABLE for other tenant email domain TENG-329
    Then user verify voucher is not displayed
      | MassDomain4 |

    # Scenario: Mass Voucher targeted for Kost Name (TENG-344)
    Then user verify voucher is displayed
      | MassKostName |

    # Scenario: Mass voucher total quota = 0 displayed in Terpakai tab (TENG-341)
    # Scenario: Mass voucher user quota = 0 displayed in Terpakai tab (TENG-335)
    When user click Terpakai tab
    Then user verify voucher is displayed
      | MassUsed |

  Scenario: Voucher Partner list Display
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user logs in as Tenant via Facebook credentails as "voucher"
    * user navigates to "mamikos /user/voucherku/"
    * user clicks on Partner tab
    Then user see voucher card
    * user see voucher list image
    * user see voucher list Expired Date
    * user see S&K Berlaku text

#  Scenario: Voucher Partner Detail Display
    When user click on Voucher Partner Card from the list
    Then user redirected to "/user/voucherku/detail"
    * user see voucher partner detail Image banner
    * user see voucher partner detail Campaign Title
    * user see voucher partner detail Expired Date
    * user see voucher partner detail Syarat dan Ketentuan label
    * user see voucher partner detail Syarat dan Ketentuan description
    * user see voucher partner detail Kode Voucher label
    * user see voucher partner detail Voucher code
    * user see voucher partner detail Ticket icon
    * user see voucher partner detail Salin button

    #  Scenario: Voucher Partner detail salin button
    When user click Salin button from voucher partner detail
    Then user see kode berhasil disalin toast in voucher partner detail
#
#  @TENG-339 @TENG-338 @TENG-334 @TENG-333 @TENG-328 @TENG-323 @TENG-337
#  Scenario: Single Voucher APPLICABLE for tenant email (TENG-339)
#    Given user navigates to "mamikos /"
#    And user clicks on Enter button
#    When user login in as Tenant via phone number as "TENG voucher"
#    And user click profile on header
#    And user choose "Profil" on dropdown list profile
#    And user clicks Voucherku menu
#    Then user verify voucher is displayed
#      | SingleEmail |
#
#    # Scenario: Single Voucher NOT APPLICABLE for other tenant email TENG-333
#    Then user verify voucher is displayed
#      | SingleEmail |
#
#    # Scenario: Single Voucher NOT APPLICABLE for tenant email (TENG-338)
#    Then user verify voucher is not displayed
#      | SingleVoucherNotAplicableForTenantMail |
#
#    # Scenario: Single Voucher APPLICABLE for other tenant email (TENG-334)
#    Then user verify voucher is not displayed
#      | SingleVoucherNotAplicableForTenantMail |
#
#     # Scenario: Single Voucher targeted for Kost City (TENG-328)
#    Then user verify voucher is displayed
#      | SingleKostCity |
#
#     # Scenario: Single Voucher targeted for Kost Name (TENG-323)
#    Then user verify voucher is displayed
#      | SingleKostName |
#
#    When user click Terpakai tab
#    Then user verify voucher is displayed
#      | SingleUsed1 |

