@regression @addons @BBM1

@BBM-995
Feature: Add Ons - Deleted by Admin

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    * user login as a consultant via credentials
    When user click on Search Contract Menu form left bar
    * user search contract by tenant phone number "adi TENG Tenant Add Ons"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    When user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost Add Ons
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Add Ons" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    * user navigates to Booking Request page
    * user select kost "Kost Adi Auto Add Ons" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Check Add Ons Delete By Admin Master
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice details first index
    * admin adds custom add ons with name "adiautomation" price amount "100000"
    Then admin can sees add ons successfully added
    When admin deletes additional other price with name below :
      | adiautomation |
    Then admin can not sees price with name "adiautomation" in invoice details