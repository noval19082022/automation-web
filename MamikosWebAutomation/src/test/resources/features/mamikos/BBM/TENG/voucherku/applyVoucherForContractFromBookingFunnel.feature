@regression @BBM4 @voucher

  @BBM-734 @BBM-735 @BBM-723 @BBM-770 @BBM-771 @BBM-772 @BBM-781
Feature: Apply Voucher For Contract Created From Booking Funnel

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search contract by tenant phone number "adi TENG apply voucher"
    And admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "adi TENG apply voucher"
    And user navigates to "mamikos /user/booking/"
    And user cancel booking

#  Scenario: Tenant Booking Kost BBK
    Given user navigates to "mamikos /"
    And user clicks search bar
    And I search property with name "applyVoucherBBK" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user input rent duration equals to 4
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "apply voucher" and click on Enter button
    * user navigates to "owner /booking/request"
    And user select kost "Kos Loyal Kretek" on booking request page
    When user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button

#    BBM-734
#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user login in as Tenant via phone number as "adi TENG apply voucher"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And system display remaining payment "before" use voucher for payment "monthly"
    And user access voucher form
    And input "VCTRFROMBF1" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-735
#  Scenario: Tenant Apply Voucher with Contract Rules from Consultant
    When user access voucher form
    And input "VCTRFROMCONS1" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-723
#  Scenario: Tenant Apply Voucher with Contract Rules from Owner
    When user access voucher form
    And input "VCTRFROMOWNER1" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-724
#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel and Consultant
    When user access voucher form
    And input "VCTRFROMBFC1" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-770
#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel and Owner
    When user access voucher form
    And input "VCTRFROMBFO1" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-771
#  Scenario: Tenant Apply Voucher with Contract Rules from Consultant and Owner
    When user access voucher form
    And input "VCTRFROMCO1" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-772
#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel, Owner, and Consultant
    When user access voucher form
    And input "VCTRFROMBFCO1" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#    BBM-781
#  Scenario: Tenant Apply Voucher with Contract Rules from Tenant Funnel
    When user access voucher form
    And input "AUTOFUNNEL" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"