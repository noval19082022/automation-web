@regression @mamipoinTenant @BBM3

Feature: Tenant Apply MamiPoin on Invoice

  @TEST_BBM-379 @TEST_BBM-383 @TEST_BBM-377 @TEST_BBM-375
  Scenario: Tenant Apply MamiPoin @BX/TENG/OB
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG whitelist mamipoin"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks Bayar button on Kos Saya
    * user remove voucher
    Then system display remaining payment "before" use mamipoin for payment "monthly"
    When user clicks on mamipoin toggle button to ON
    Then system display remaining payment "after" use mamipoin for payment "monthly"
    When user clicks on mamipoin toggle button to OFF
    Then system display remaining payment "before" use mamipoin for payment "monthly"

  @TEST_BBM-378
  Scenario: Point Estimate on Blackisted Tenant
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG blacklist mamipoin"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks Bayar button on Kos Saya
    Then tenant point estimate not displayed on invoice
