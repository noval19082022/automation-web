@regression @BBM2 @BBM-1416 @BBM-1415

#  kost used: Kost Adi Auto DP AddFee Deposit
#  (Kost Regular DP with Additional Fee and Deposit)
Feature: Deposit And Additional Fee in Invoice Detail Page for Paid DP And Paid Settlement

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost Regular DP with Additional Fee and Deposit
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto DP AddFee Deposit" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 1
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    When user navigates to "owner /booking/request"
    * user select kost "Kost Adi Auto DP AddFee Deposit" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Tenant Pay Down Payment For Invoice Detail Check After DP And Settlement Are Paid
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * user make bill payments using "Mandiri"

  Scenario: Invoice Detail Settlement Not Yet Paid
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice details first index
    Then admin can sees invoice details page
    * admin can sees "Invoice Number" on invoice details page
    * admin can sees "Renter" on invoice details page
    * admin can sees "Invoice Description" on invoice details page
    * admin can sees "Invoice Due Date" on invoice details page
    * admin can sees "Basic Amount" on invoice details page
    * admin can sees "Invoice Deposit Fee" on invoice details page
    * admin can sees total cost is basic amount + deposit fee + biaya tetap

  #Settlement Paid
  Scenario: Tenant Pay Settlement Invoice For Invoice Detail Check After DP And Settlement Are Paid
    When user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /"
    * user navigates to "mamikos /user/booking/"
    * tenant click Bayar Sekarang button
    * tenant clicks on Bayar Pelunasan Sekarang button
    * user make bill payments using "Mandiri"

  Scenario: Invoice Detail Settlement Paid
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin clicks on Search Invoice Menu form left bar
    * admin search invoice with "Renter Phone Number" value "0890867321212"
    * admin clicks on invoice details first index
    Then admin can sees invoice details page
    * admin can sees "Invoice Number" on invoice details page
    * admin can sees "Renter" on invoice details page
    * admin can sees "Invoice Description" on invoice details page
    * admin can sees "Invoice Due Date" on invoice details page
    * admin can sees "Basic Amount" on invoice details page
    * admin can sees "Invoice Deposit Fee" on invoice details page
    * admin can sees total cost is basic amount + deposit fee + biaya tetap + admin fee