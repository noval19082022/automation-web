@regression @BBM7 @voucher

  @BBM-758
Feature: Invalid Voucher After Applied, Invalid Minimum Transaction

  Scenario: Activate Voucher AUTOMINTRX
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOMINTRX" and revert minimum transaction
    Then System display alert message "updated" on mamipay web

#  BBM-758
  Scenario: Tenant Input Voucher AUTOMINTRX
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOMINTRX" to field voucher code
    * user click use button
    Then voucher applied successfully

  Scenario: Deactivate voucher AUTOMINTRX
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOMINTRX" and update minimum transaction
    Then System display alert message "updated" on mamipay web

  Scenario: Hapus Button Functionality on Toast Message
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    Then system display toast message "Belum mencapai minimal transaksi. Silakan hapus voucher."
    * system display icon voucher invalid
    When user remove voucher by toast message
    Then system display toast message "Voucher Dihapus"