@regression @BBM8 @voucher

Feature: Apply Voucher For Kost Type New Gold Plus

  Scenario Outline: Deleting existing booking For Kost Type <example description>
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search for "<kost type>" and cancel contract

#  Scenario: Tenant Booking Kost For Kost Type New Gold Plus
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user logs in as Tenant via Facebook credentails as "apply voucher"
    When user clicks search bar
    And I search property with name "<kost type>" and select matching result to go to kos details page
    And user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    And user input rent duration equals to 4
    And user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

#  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    And user clicks on Enter button
    And user fills out owner login as "apply voucher" and click on Enter button
    * user navigates to "owner /booking/request"
    And user select kost "<kost name>" on booking request page
    When user clicks on Booking Details button
    And user clicks on Accept button
    And select first room available and clicks on next button
    And user click save button

#  Scenario: Tenant Apply Voucher Applicable for Kost Type
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    And user clicks on Enter button
    When user logs in as Tenant via Facebook credentails as "apply voucher"
    And user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    And system display remaining payment "before" use voucher for payment "monthly"
    And user access voucher form
    And input "<voucher code applicable for kost type>" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable for Kost Type
    When user access voucher form
    And input "<voucher code not applicable for kost type>" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Applicable For Other Kost Type
    When input "<voucher code applicable for other kost type>" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable For Other Kost Type
    When input "<voucher code not applicable for other kost type>" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"
    Examples:
      | example description | kost type                 | kost name      | voucher code applicable for kost type | voucher code not applicable for kost type | voucher code applicable for other kost type | voucher code not applicable for other kost type |
      | New Gold Plus 1     | applyVoucherNewGoldPlus1  | Kos Huta Gasim | VAForNewGP11                          | VNAForNewGP11                             | VAForNewGP21                                | VNAForNewGP21                                   |
      | New Gold Plus 2     | applyVoucherNewGoldPlus2  | Kos Palipi     | VAForNewGP21                          | VNAForNewGP21                             | VAForNewGP31                                | VNAForNewGP31                                   |

  Scenario: Delete existing booking for Kost Type New GP3
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search for "applyVoucherNewGoldPlus3" and cancel contract

  Scenario: Owner Add Kost Tenant For New GP3
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user fills out owner login as "apply voucher" and click on Enter button
    When user click Add Tenant Contract button in dashboard page
    And user click continue until start adding contract
    And user select kost "applyVoucherNewGoldPlus3" for tenant
    And user input tenant information for "apply voucher"
    And user input payment information for "apply voucher"
    And user click next button on other cost information
    And user click save button on detail payment
    Then user verify pop up success save tenant data

#  Scenario: Tenant Apply Voucher Applicable for New GP3
    Given user navigates to "mamikos /"
    And user logs out as a Tenant user
    And user clicks on Enter button
    When user logs in as Tenant via Facebook credentails as "apply voucher"
    And user navigates to My Kost page
    And user click on billing tab
    And user click pay for next month reccuring payment
    Then system display remaining payment "before" use voucher for payment "monthly"
    When user access voucher form
    And input "VAForNewGP31" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable for New GP3
    When user access voucher form
    And input "VNAForNewGP31" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Applicable for New GP1
    When input "VAForNewGP11" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable for New GP1
    When input "VAForNewGP31" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

  Scenario: Delete existing booking for Kost Type New GP3
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    And user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    And user search for "applyVoucherNewGoldPlus3" and cancel contract

