@teng-cookies
Feature: Teng Cookies Test Data

  @teng-1
  Scenario: Get cookies owner 1
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "teng admin invoice" and click on Enter button
    * user navigates to "mamikos /ownerpage/manage/all/booking"
    * store cookies data to "/src/test/resources/testdata/cookies/" with name "teng-admin-invoice"


  @teng-1
  Scenario: Get cookies tenant 1
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "teng invoice detail"
    * user navigates to "mamikos /user/booking/"
    * store cookies data to "/src/test/resources/testdata/cookies/" with name "tenant-teng-admin-invoice"

  @teng-1
  Scenario: Get cookies admin 1
    Given user navigates to "mamikos admin"
    * user logs in to Mamikos Admin via credentials as "admin ob"
    * user access to data booking menu
    * store cookies data to "/src/test/resources/testdata/cookies/" with name "tenant-admin-admin-invoice"