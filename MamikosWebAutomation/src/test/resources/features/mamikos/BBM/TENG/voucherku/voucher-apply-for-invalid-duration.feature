@regression @voucher @BBM5

  @BBM-760
Feature: Invalid Voucher After Applied, Invalid Contract Period

  Scenario: Admin Edit Voucher AUTOCHNGEPERIOD and Change Contract Period to Monthly
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user access menu "Mamikos Voucher" sub menu of voucher discount
    * admin master search id, voucher code or campaign name to "AUTOCHNGEPERIOD"
    * admin master clicks in search button in voucher list page
    * admin master clicks on edit pencil icon index "1" in voucher list result
    * admin master change minimum type of contract period to "Monthly"
    * admin master clicks on add mass voucher button in voucher form
    * admin master clicks on Yes, Do It! button

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user click on Search Contract Menu form left bar
    Then user Navigate "Search Contract" page
    When user search contract by tenant phone number "adi TENG apply voucher"
    * admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user navigates to "mamikos /user/booking/"
    Then Booking title is displayed
    When user cancel booking

#  Scenario: Tenant Booking Kost Monthly
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Regular" and select matching result to go to kos details page
    * user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Voucher" and click on Enter button
    When user navigates to "owner /booking/request"
    * user select kost "Kost Adi Auto Regular" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

#  Scenario: Tenant Apply Voucher AUTOCHNGEPERIOD
    Given user navigates to "mamikos /"
    * user logs out as a Tenant user
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    * system display remaining payment "before" use voucher for payment "monthly"
    When user access voucher form
    * input "AUTOCHNGEPERIOD" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"

  Scenario: Admin Edit Voucher AUTOCHNGEPERIOD and Change Contract Period to Annually
    Given user navigates to "backoffice"
    * user login  as a Admin via credentials
    When user access menu "Mamikos Voucher" sub menu of voucher discount
    * admin master search id, voucher code or campaign name to "AUTOCHNGEPERIOD"
    * admin master clicks in search button in voucher list page
    * admin master clicks on edit pencil icon index "1" in voucher list result
    * admin master change minimum type of contract period to "Annually"
    * admin master clicks on add mass voucher button in voucher form
    * admin master clicks on Yes, Do It! button

  Scenario: Hapus Button Functionality on Toast Message
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    When user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    Then system display toast message "Kode voucher tidak bisa digunakan. Silakan hapus voucher."
    * system display icon voucher invalid
    When user remove voucher by toast message
    Then system display toast message "Voucher Dihapus"