@regression @kostReviewAdmin @BBM4

  Feature: Kost Review

    @BBM-544
    Scenario: Cancel create review
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Kost Review menu
      And user click on Create Review button
      And user fill first Create Review without contract data
        | No |
        | Without Contract |
        | Traveloka        |
        | Rheza Haryo Hanggara |
        | 0898765432166        |
        | Kost Adi Auto SinggahSini |
        | Today                |
        | Tomorrow             |
      And user fill second Create Review data
        | 5 |
        | 5 |
        | 5 |
        | 5 |
        | 5 |
        | 1 |
        | Just a test content that will match 25 characters of input |
      And user click cancel on Create Review section
      Then user redirected to "admin kos review"

    @BBM-547
    Scenario: Create review without contract
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Kost Review menu
      And user click on Create Review button
      And user fill first Create Review without contract data
      | No |
      | Without Contract |
      | Traveloka        |
      | Rheza Haryo Hanggara |
      | 0898765432166        |
      | Kost Adi Auto SinggahSini|
      | Today                |
      | Tomorrow             |
      And user fill second Create Review data
      | 5 |
      | 5 |
      | 5 |
      | 5 |
      | 5 |
      | 5 |
      | Just a test content that will match 25 characters of input |
      And user click save on Create Review Section
      Then user receive success alert for created kost review with text "Success! Review added successfully."

    @BBM-543
    Scenario: Edit review without contract
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Kost Review menu
      And user click on Edit Review button on "Just a test content that will match 25 characters of input"
      And user change Kost Review status to "Waiting"
      And user fill first Create Review without contract data
        | No |
        | Without Contract |
        | Tiket |
        | Rheza Haryo Hanggara Edit |
        | 0898765432166 |
        | Kost Adi Auto SinggahSini |
        | 7 |
        | 8 |
        And user fill second Create Review data
        | 2 |
        | 2 |
        | 2 |
        | 2 |
        | 2 |
        | 2 |
        | Just a test content that will match 25 characters of input edit |
        And user click save on Edit Review Section
        Then user receive success alert for updated kost review with text "Success! Review Updated"

    @BBM-548
    Scenario: Update kost review to Live
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Kost Review menu
      And user click Live button on "Just a test content that will match 25 characters of input edit"
      Then user receive success alert for kost review updated to live with text "Success! Review Updated to live"

    @BBM-549
    Scenario: Update kost review to Reject
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Kost Review menu
      And user click Reject button on "Just a test content that will match 25 characters of input edit"
      Then user receive success alert for kost review updated to reject with text "Success! Berhasil menolak review"

    @BBM-550
    Scenario: Delete kost review
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Kost Review menu
      And user click Delete Review button on "Just a test content that will match 25 characters of input edit"
      Then user receive success alert for deleted kost review with text "Success! Success, Review Deleted"

    @BBM-542
    Scenario: Create review with contract
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Kost Review menu
      And user click on Create Review button
      And user fill first Create Review with contract data
        | No |
        | With Contract |
        | 29265         |
      And user verify there are "disabled" input
      And user fill second Create Review data
        | 5 |
        | 5 |
        | 5 |
        | 5 |
        | 5 |
        | 5 |
        | Just a test content that will match 25 characters of input |
      And user click save on Create Review Section
      Then user receive success alert for created kost review with text "Success! Review added successfully."

    @BBM-545
    Scenario: Edit review with contract
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Kost Review menu
      And user click on Edit Review button on "Just a test content that will match 25 characters of input"
      And user change Kost Review status to "Waiting"
      And user fill first Create Review with contract data
        | No |
        | With Contract |
        | 29265         |
      And user verify there are " locked " input
      And user verify there are "disabled" input
      And user verify there are "tepicker" input
      And user fill second Create Review data
        | 2 |
        | 2 |
        | 2 |
        | 2 |
        | 2 |
        | 2 |
        | Just a test content that will match 25 characters of input edit |
      And user click save on Edit Review Section
      Then user receive success alert for updated kost review with text "Success! Review Updated"

    @BBM-550
    Scenario: Delete kost review
      Given user navigates to "mamikos admin"
      And user logs in to Mamikos Admin via credentials as "admin loyalty"
      When user click on Kost Review menu
      And user click Delete Review button on "Just a test content that will match 25 characters of input edit"
      Then user receive success alert for deleted kost review with text "Success! Success, Review Deleted"