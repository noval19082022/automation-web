@rajawalichatRoom @BBM8
Feature: Rajawali Chat Room

  @BBM-1496
  Scenario: Check if User can click on Kost Name in Rajawali Chat Room
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    And user go to Rajawali Chat Room
    When user click on the Group Chat
    Then user able to see Kos Name
    When user click on Kos Name
    Then user will directed to Kos Detail in new tab