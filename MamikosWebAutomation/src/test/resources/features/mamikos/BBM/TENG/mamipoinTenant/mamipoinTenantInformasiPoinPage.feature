@regression @mamipoinTenant @BBM3

Feature: MamiPoin Tenant Informasi Poin Page

  @BBM-409 @BBM-410 @BBM-412
  Scenario: MamiPoin Tenant Informasi Poin Page
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG whitelist mamipoin"
    * user navigates to "mamikos /user/mamipoin/expired"
    Then user verify title in the informasi poin page is displayed
    * user verify subtitle in the informasi poin page is displayed
    * user verify table title tanggal kedaluwarsa is displayed
    * user verify table title jumlah mamipoin is displayed
    # asterisk annotation used for make gherkin more elegantly readable
    # asterisk will substitute looping "And" annotation
    # e.g
    # And ...
    # And ...
    # And ...
    # And ...

#    TENG-187
#  Scenario: Description on Tanggal Kedaluwarsa
    * user verify expired point on information point page
      | 31 Okt 2040 |
      | 31 Mar 2041 |
      | 30 Apr 2043 |
      | 31 Mei 2045 |
      | 30 Jun 2047 |

#    BBM-410
#  Scenario: Point expiry lists if user has points
    * user verify only 5 list of points that displayed

  @TENG-200 @BBM-413 @TENG-201 @BBM-405
  Scenario: MamiPoin Tenant Informasi Poin Page When the User Poin is Empty
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG empty mamipoin"
    * user navigates to "mamikos /user/mamipoin/expired"
    Then user verify title in the informasi poin page is displayed
    * user verify subtitle in the informasi poin page is displayed
    * user verify subtitle tidak ada poin yang tersedia is displayed
    * user verify only the header that is sticky or "fixed"
    * user verify button lihat caranya is displayed
    * user clicks on lihat caranya button
    * user verify title in the dapatkan poin page is displayed