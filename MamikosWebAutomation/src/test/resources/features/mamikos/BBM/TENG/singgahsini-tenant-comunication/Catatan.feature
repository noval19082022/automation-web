@regression @BBM3 @tenantTracker

Feature: SinggahSini - Tenant Tracker - Add Catatan

  @BBM-564
  Scenario: Add Catatan on Detail Penyewa
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "adiSinggahSini" in the search field on main page
    * user click search button on main page filter
    * user click Tambah Catatan
    * user fill "prioritaskan" in note field
    * user click Simpan in note field
    Then user verify search result on main page bse contains Prioritaskan

#  @BBM-564
  Scenario: Delete Note
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "adiSinggahSini" in the search field on main page
    * user click search button on main page filter
    * user click Note Prioritaskan
    * user clear note field
    * user click Simpan in note field
    Then user verify search result on main page bse contains Tambah Catatan