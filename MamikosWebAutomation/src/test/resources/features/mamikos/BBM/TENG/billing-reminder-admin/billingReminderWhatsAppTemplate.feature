@regression @billingReminder @BBM3

Feature: Billing Reminder - WhatsApp Template

  Background: User arrived on WhatsApp Template page
    Given user navigates to "backoffice"
    * user login Backoffice as a "BBM01" via credentials
    When user click on Billing Reminder Template from left menu
    * user click "Whatsapp Template" submenu of Billing Reminder Template

  Scenario: user set the initial state to display Whatsapp template Day -5
    When user set the initial state to display Whatsapp template Day -5

  @deleteWATemplate @TEST_BBM-975
  Scenario: Delete Template
    When user delete WhatsApp Template with content "recurringbooking_voucher_d_plus_1_update"

  @addWATemplateWithExistingDayPeriod @TEST_BBM-990
    Scenario: Add Template With Existing Day Period
    Given user click add WhatsApp Template button
    When user select WA day period with "-1"
    * user select WhatsApp Template selection with "recurringbooking_voucher_d_minus_1_update"
    * user click create WhatsApp Template button
    Then user verify add template callout error with "Cannot create template."

  @addWATemplate @TEST_BBM-986
  Scenario: Add Template
    Given user click add WhatsApp Template button
    When user select WA day period with "-5"
    * user select WhatsApp Template selection with "recurringbooking_voucher_d_plus_1_update"
    * user click create WhatsApp Template button
    Then user verify WhatsApp Template content with "recurringbooking_voucher_d_plus_1_update"

  @editWATemplate @TEST_BBM-976
  Scenario: Edit Template
    Given user click edit WhatsApp Template button
    When user select WhatsApp Template selection with "recurringbooking_voucher_d_plus_1_update"
    * user click save on edit WhatsApp Template page
    Then user verify WhatsApp Template content with "recurringbooking_voucher_d_plus_1_update"