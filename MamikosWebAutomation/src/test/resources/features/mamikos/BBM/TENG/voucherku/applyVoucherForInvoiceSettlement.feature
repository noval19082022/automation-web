@regression @BBM4 @voucher

  Feature: Apply Voucher For Invoice Settlement

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user click on Search Contract Menu form left bar
    When user Navigate "Search Contract" page
    * user search contract by tenant phone number "adi TENG apply voucher dua"
    * admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher dua"
    * user navigates to "mamikos /user/booking/"
    Then Booking title is displayed
    When user cancel booking

#  Scenario: Tenant Booking Kost With DP
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Voucher DP" and select matching result to go to kos details page
    * user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Voucher" and click on Enter button
    * user navigates to "owner /booking/request"
    When user select kost "Kost Adi Auto Voucher DP" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  #  Scenario: Tenant Pay DP Invoice
    Given user navigates to "mamikos /"
    When user logs out as a Tenant user
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher dua"
    * user click on the tenant profile
    * user click profile dropdown button
    * user click Riwayat dan Draft Booking menu
    * user click Bayar Sekarang button
    * user make bill payments using "Mandiri"
    Then system display payment using "Mandiri" is "Success Transaction"

#  Scenario: Invoice Settlement and Voucher For First Full Paid
    When user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user access voucher form
    * input "VTOTALUSAGE" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."

#  Scenario: Invoice Settlement and Voucher For Recurring
    When user access voucher form
    * input "AUTORECURRING" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For Settlement
    When user access voucher form
    * input "AUTOSETTLEMENT" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For First Full Paid and Recurring
    When user access voucher form
    * input "AUTOFULLPAIDREC" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For First Full Paid and Settlement
    When user access voucher form
    * input "AUTOFPAIDSETTLE" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For Recurring and Settlement
    When user access voucher form
    * input "AUTORECSETTLE" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For First Full Paid, Reccuring, and Settlement
    When user access voucher form
    * input "AUTOFPAIDRECSET" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For DP
    When user access voucher form
    * input "AUTODP" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For First Full Paid and DP
    When user access voucher form
    * input "AUTOFPAIDDP" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For DP and Settlement
    When user access voucher form
    * input "AUTODPST" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For DP and Recurring
    When user access voucher form
    * input "AUTODPRECURRING" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For First Full Paid, DP, and Settlement
    When user access voucher form
    * input "AUTOFPAIDDPSET" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For First Full Paid, DP, and Recurring
    When user access voucher form
    * input "AUTOFPAIDDPREC" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For DP, Settlement, and Recurring
    When user access voucher form
    * input "AUTODPSETREC" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlySettlement"

#  Scenario: Invoice Settlement and Voucher For First Paid, DP, Settlement, and Recurring
    When user access voucher form
    * input "AUTOALLPAYRULES" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlySettlement"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlySettlement"