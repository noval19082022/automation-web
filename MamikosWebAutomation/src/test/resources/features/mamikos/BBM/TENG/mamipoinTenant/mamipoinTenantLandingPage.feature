@regression @mamipoinTenant @BBM3

Feature: MamiPoin Tenant Landing Page

  @TEST_BBM-418 @TEST_BBM-420 @TEST_BBM-411 @TEST_BBM-408 @TEST_BBM-406 @TEST_BBM-407
  Scenario: MamiPoin Tenant Landing Page
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG whitelist mamipoin"
    * user navigates to "mamikos /user/mamipoin"
    Then user verify title in the mamipoin tenant landing page is displayed
    * user verify the amount of poin saya is "123.456"
    * user verify informasi poin button is displayed
    * user verify riwayat poin button is displayed
    * user verify dapatkan poin button is displayed
    # asterisk annotation used for make gherkin more elegantly readable
    # asterisk will substitute looping "And" annotation
    # e.g
    # And ...
    # And ...
    # And ...
    # And ...

#    @TEST_BBM-418
#  Scenario: Information about points that will expire and Tenant has points
    * user verify expired point information on mamipoin landing page "1.000 poin kedaluwarsa pada 31 Okt 2040"

    @TEST_TENG-186
  Scenario: Information about points that will expire and Tenant has no point
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG empty mamipoin"
    * user navigates to "mamikos /user/mamipoin"
    Then user verify expired point information on mamipoin landing page "Tidak ada poin yang tersedia"

  Scenario: MamiPoin display on user without Poin
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG empty mamipoin"
    * user choose "Profil" on dropdown list profile
    * tenant clicks on kost saya
    * tenant clicks on tagihan
    * tenant clicks on Bayar button
    Then display MamiPoin with text "Poin kamu masih 0. Yuk, bayar dulu dan dapatkan poinnya."