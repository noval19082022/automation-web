@regression @BBM7 @voucher

  @BBM-604 @BBM-601
Feature: Apply Voucher For Kost Type GoldPlus

  Scenario: Tenant Apply Voucher Applicable for Kost Type GoldPlus 1
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher goldplus"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOGP1" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Not Applicable for Kost Type GoldPlus 1
    When user access voucher form
    * input "AUTOVNAGP1" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher Applicable for Kost City
    When user access voucher form
    * input "AUTOKOSTCITY" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlyGP"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlyGP"

#  Scenario: Tenant Apply Voucher Not Applicable for Kost City
    When user access voucher form
    * input "AUTOVNAKOSTCITY" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlyGP"