@regression @BBM7 @voucher

  @BBM-712 @BBM-702 @BBM-665 @BBM-666
Feature: Apply Voucher For Contract Created From Owner

  Scenario: Tenant Apply Voucher with Contract Rules from Owner
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher owner"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOOWNER" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  BBM-712
#  Scenario: Tenant Apply Voucher with Contract Rules from Consultant
    When user access voucher form
    And input "AUTOCONSULTANT" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel
    When user access voucher form
    And input "AUTOBOOKFUNNEL" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  BBM-702
#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel and Consultant
    When user access voucher form
    And input "AUTOFUNNELCONS" to field voucher code
    And user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    And system display remaining payment "before" use voucher for payment "monthly"

#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel and Owner
    When user access voucher form
    And input "AUTOFUNNELOWNER" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  BBM-665
#  Scenario: Tenant Apply Voucher with Contract Rules from Consultant and Owner
    When user access voucher form
    And input "AUTOCONSOWNER" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"

#  BBM-666
#  Scenario: Tenant Apply Voucher with Contract Rules from Booking Funnel, Owner, and Consultant
    When user access voucher form
    And input "AUTOFUNOWNCONS" to field voucher code
    And user click use button
    Then voucher applied successfully
    And system display remaining payment "after" use voucher for payment "monthly"
    When user remove voucher
    Then voucher removed successfully
    And system display remaining payment "before" use voucher for payment "monthly"