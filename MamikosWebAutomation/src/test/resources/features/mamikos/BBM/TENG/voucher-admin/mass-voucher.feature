@regression @BBM4 @voucher @voucher-admin

Feature: Admin - Mass Voucher

  @BBM-822
  Scenario: Create Mass Voucher Prefix Without Fill Email Field
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And admin master clicks on add mass voucher button
    And admin master inputs voucher campaign name to "bbm-test-mass-voucher-automation"
    And admin master sets campaign date to "today" and end date to ""
    And admin master select campaign team to "OTHER"
    And admin master inputs mass voucher code "BBM"
    And admin master tick payment rules :
      |booking_with_dp               |
      |booking                       |
      |pelunasan                     |
      |recurring                     |
    And admin select contract rules :
      | booking               |
      | consultant            |
      | owner                 |
      | tenant                |
    And admin select discount type "Percentage"
    And admin master input discount amount "50"
    And admin master input total each quota to "5" and daily each quota to "1"
    And admin master input maximum amount for voucher discount "500000"
    And admin master input minimum transaction "100000"
    And admin master tick important check box :
      | is_active   |
      | is_testing  |
    And admin master clicks on add mass voucher button in voucher form
    Then admin can sees callout message is "New mass voucher added!"

  @BBM-820 @BBM-817 @BBM-815
  Scenario: Update Mass Voucher
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And admin master search id, voucher code or campaign name to "BBM"
    And admin master clicks in search button in voucher list page
    And admin master clicks on edit pencil icon index "1" in voucher list result
    Then admin master can sees campaign name is "bbm-test-mass-voucher-automation"
    And admin master can sees campaign date is "today" and end date to ""
    And admin master can sees campaign team is set to "OTHER" with voucher code contains "BBM"
    And admin master can sees payment rules ticked :
      |booking_with_dp               |
      |booking                       |
      |pelunasan                     |
      |recurring                     |
    And admin master can sees contract rules ticked :
      | booking               |
      | consultant            |
      | owner                 |
      | tenant                |
    And admin master can sees discount type "Percentage" with amount is "50"
    And admin master can sees total each quota is "5" with daily each quota is "1"
    And admin master can sees maximum amount is "500000" and minimum "100000" for transaction
    And admin master can sees important rules is ticked :
      | is_active   |
      | is_testing  |
    #BBM-815
    #Scenario: Mass Voucher Status Not Publish
    Then admin master can sees important rules is not ticked :
      | public_campaign[is_published] |
    #BBM-817
    #Scenario: Voucher status is Active
    And admin master inputs mass voucher code "BBM"
    And admin master clicks on add mass voucher button in voucher form
    And admin master clicks on Yes, Do It! button
    Then admin can sees callout message contains "Voucher BBM updated"
    When admin master search id, voucher code or campaign name to "BBM"
    And admin master clicks in search button in voucher list page
    Then admin can sees first index voucher status in mass voucher is "Active"

  @BBM-819
  Scenario: Mass Voucher Status Publish
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And admin master search id, voucher code or campaign name to "BBM"
    And admin master clicks in search button in voucher list page
    And admin master clicks on edit pencil icon index "1" in voucher list result
    And admin master tick important check box :
      | public_campaign[is_published]   |
    And admin master upload voucher campaign image
    And admin master input campaign title to "Krezi Ollie"
    And admin master input campaign term & conditions to "Terbullie gara gara bakso di mass voucher"
    And admin master clicks on add mass voucher button in voucher form
    And admin master clicks on Yes, Do It! button
    Then admin can sees callout message contains "Voucher BBM updated"

  @BBM-818
  Scenario: Mass Voucher Renter Email(CSV)
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * admin master search id, voucher code or campaign name to "BBM"
    * admin master clicks in search button in voucher list page
    * admin master clicks on edit pencil icon index "1" in voucher list result
    * admin master upload mass voucher csv file
    * admin master clicks on add mass voucher button in voucher form
    * admin master clicks on Yes, Do It! button
    Then admin can sees callout message contains "Voucher BBM updated"

  @BBM-816
  Scenario: Mass Voucher Status Inactive
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    And user access menu "Mamikos Voucher" sub menu of voucher discount
    And admin master search id, voucher code or campaign name to "BBM"
    And admin master clicks in search button in voucher list page
    And admin master clicks on edit pencil icon index "1" in voucher list result
    And admin master untick important check box :
      | is_active   |
    And admin master clicks on add mass voucher button in voucher form
    And admin master clicks on Yes, Do It! button
    Then admin can sees callout message contains "Voucher BBM updated"
    When admin master search id, voucher code or campaign name to "BBM"
    And admin master clicks in search button in voucher list page
    Then admin can sees first index voucher status in mass voucher is "Not Active"