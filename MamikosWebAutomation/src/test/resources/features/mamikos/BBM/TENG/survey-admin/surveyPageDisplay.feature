@regression @surveyAdmin @BBM4

Feature: Admin Survey

  @BBM-513
  Scenario: Admin Survey - Survey Page Display
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user click on Tenant Survey menu
    Then user see at Tenant Survey table contains:
      | Head Table    |
      | Name          |
      | Phone Number  |
      | Kost Name     |
      | Survey Date   |
      | Survey Time   |
      | Status        |
      | Created At    |
      | Updated At    |
      | Updated By    |
    And user verify filter Tenant Name in the Tenant Survey Page is displayed
    And user verify filter Tenant Phone Number in the Tenant Survey Page is displayed
    And user verify filter Property Name in the Tenant Survey Page is displayed
    And user verify filter Survey Status in the Tenant Survey Page is displayed
    And user verify filter Survey Date in the Tenant Survey Page is displayed
    And user verify Search button in the Tenant Survey Page is displayed
    And user verify Reset Filter button in the Tenant Survey Page is displayed
    And user verify Create Survey button in the Tenant Survey Page is displayed
    And user verify Pagination button in the Tenant Survey Page is displayed