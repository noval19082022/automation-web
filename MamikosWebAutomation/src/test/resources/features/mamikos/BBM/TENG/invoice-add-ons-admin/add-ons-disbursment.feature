@regression @addons @BBM1

  @BBM-996 @BBM-997 @BBM-998
Feature: Add Ons - Disbursement

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    * user login as a consultant via credentials
    When user click on Search Contract Menu form left bar
    * user search contract by tenant phone number "adi TENG Tenant Add Ons"
    * admin master terminate contract by today date

  Scenario: Cancel Booking if Tenant Have Booking
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    When user navigates to "mamikos /user/booking/"
    * user cancel booking

#  Scenario: Tenant Booking Kost Add Ons
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Add Ons" and select matching result to go to kos details page
    * user choose boarding date is "today" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Add Ons" and click on Enter button
    * user navigates to Booking Request page
    * user select kost "Kost Adi Auto Add Ons" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Tenant Pay 1st Month Booking For Add Ons
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user click first notification with message "Booking Dikonfirmasi, Ayo Bayar Sekarang"
    * user make bill payments using "Mandiri"

  Scenario: Admin Master Add Add Ons to Tenant Contract
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user click on Search Contract Menu form left bar
    * user Navigate "Search Contract" page
    * user search contract by tenant phone number "0890867321212"
    * admin clicks on add ons button on contract "1" index
    * admin add add ons "Laundry" to tenant contract

  Scenario: Tenant Check-in
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG Tenant Add Ons"
    * user navigates to "mamikos /user/booking/"
    * user check-in at kost "teng adds on"

#  Tenant Pay 2nd Month Booking For Add Ons
    * tenant clicks on kost saya
    * tenant clicks on tagihan
    * tenant clicks on Bayar button
    * user make bill payments using "Mandiri"

#  Admin backoffice check disbursement add on price
  Scenario: Admin Master Check Disbursement Add On Price
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * admin go to Paid Invoice list Disbursment Page
    * admin clicks on Allow Disbursment Tab
    * admin filter by "Tenant Phone Number" with value "0890867321212"
    * admin clicks on transfer button
    #BBM-996
    Then admin can sees list cost below:
      | Harga Kos               |
      | Laundry                 |
      | Biaya Layanan Penyewa   |
      | Basic Payout            |
      | Total Disburse          |
    #BBM-997
    When admin can sees total disburse is not include add on + admin fee
    #BBM-998
#  Scenario: Check Add Ons On Disbursement Management
    * admin clicks on transfer now button
    * admin clicks on processed disbursement tab
    * admin filter by "Tenant Phone Number" with value "0890867321212"
    * admin clicks on detail button on processed section
    Then admin can sees total disburse is equal to kost price
    When admin clicks on paid disbursement tab
    * admin filter by "Tenant Phone Number" with value "0890867321212"
    * admin clicks on mark already transfer first index
    * admin choose today date and mark as transferred
    * admin filter by "Tenant Phone Number" with value "0890867321212"
    * admin clicks on action button first index transferred tab
    Then admin can not sees price with name "Laundry"