@regression @tenantTracker @BBM3

Feature: SinggahSini - Tenant Tracker - Main Page

  @BBM-551
  Scenario: Pagination Functionality
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    Then user see pagination menu on Main Page is displayed
    When user click pagination number "2"
    Then user see display data row from 20 riwayat

  @BBM-554
  Scenario: Column Name Complete
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    Then user see at Tenant Main Page Column contains
      | Head Table              |
      | Profil Penyewa          |
      | Nama Properti           |
      | Fase                    |
      | Status                  |
      | Tanggal Status          |
      | Catatan                 |
      | Owner                   |
      | Action                  |