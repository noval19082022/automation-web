@regression @mamipoinTenant @BBM3

Feature: MamiPoin Tenant Riwayat Poin Page

  @BBM-403 @BBM-404 @BBM-397 @BBM-398 @BBM-400 @BBM-402 @BBM-401
  Scenario: Tenant already on Riwayat Poin page
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG whitelist mamipoin"
    * user navigates to "mamikos /user/mamipoin/history"
    Then user verify title in the riwayat poin page is displayed
    * user verify filter semua in the riwayat poin page is displayed
    * user verify filter poin diterima in the riwayat poin page is displayed
    * user verify filter poin ditukar in the riwayat poin page is displayed
    * user verify filter poin kedaluwarsa in the riwayat poin page is displayed
    * user verify "b-tab__box-active" class on selected filter "Semua"
    # asterisk annotation used for make gherkin more elegantly readable
    # asterisk will substitute looping "And" annotation
    # e.g
    # And ...
    # And ...
    # And ...
    # And ...

#    BBM-398
#  Scenario: MamiPoin Grouped by Month
    * user verify point history grouped by months
      | Juni 2022       |

#    BBM-403
#  Scenario: Filter Poin Diterima
    When user navigates to "mamikos /user/mamipoin/history"
    * user click "Poin Diterima" filter
    Then user verify "b-tab__box-active" class on selected filter "Poin Diterima"
    * user verify "topup" on list of point histories

#    BBM-404
#  Scenario: Filter Poin Ditukar
    When user navigates to "mamikos /user/mamipoin/history"
    * user click "Poin Ditukar" filter
    Then user verify "b-tab__box-active" class on selected filter "Poin Ditukar"
    * user verify "Potongan MamiPoin" on list of point histories

#    BBM-397
#  Scenario: Filter Poin Kedaluwarsa
    When user navigates to "mamikos /user/mamipoin/history"
    * user click "Poin Kedaluwarsa" filter
    Then user verify "b-tab__box-active" class on selected filter "Poin Kedaluwarsa"
    * user verify subtitle riwayat masih kosong is displayed

  @BBM-399
  Scenario: MamiPoin Tenant Riwayat Poin Page When the User Poin is Empty
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG empty mamipoin"
    * user navigates to "mamikos /user/mamipoin/history"
    Then user verify title in the riwayat poin page is displayed
    * user verify filter semua in the riwayat poin page is displayed
    * user verify filter poin diterima in the riwayat poin page is displayed
    * user verify filter poin ditukar in the riwayat poin page is displayed
    * user verify filter poin kedaluwarsa in the riwayat poin page is displayed
    * user verify title riwayat masih kosong is displayed
    * user verify subtitle riwayat masih kosong is displayed