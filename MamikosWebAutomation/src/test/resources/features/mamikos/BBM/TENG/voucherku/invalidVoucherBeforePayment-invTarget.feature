@regression @BBM7 @voucher

  @BBM-762
Feature: Invalid Voucher After Applied, Invalid Email Target

  Scenario: Activate Voucher AUTOTARGINV
    Given user navigates to "backoffice"
    When user login  as a Admin via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOTARGINV" and fill field applicable for "adivouchersatu@gmail.com"
    Then System display alert message "updated" on mamipay web

#  BBM-762
  Scenario: Tenant Input Voucher AUTOTARGINV
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * user remove voucher
    Then system display remaining payment "before" use voucher for payment "monthly"
    * user access voucher form
    * input "AUTOTARGINV" to field voucher code
    * user click use button
    Then voucher applied successfully

  Scenario: Deactivate voucher AUTOTARGINV
    Given user navigates to "backoffice"
    When user login as a consultant via credentials
    * user access menu "Mamikos Voucher" sub menu of voucher discount
    * user edit voucher "AUTOTARGINV" and fill field not applicable for "adivouchersatu@gmail.com"
    Then System display alert message "updated" on mamipay web

  Scenario: Hapus Button Functionality on Toast Message
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    Then system display toast message "Kode voucher tidak bisa digunakan. Silakan hapus voucher."
    * system display icon voucher invalid
    When user remove voucher by toast message
    Then system display toast message "Voucher Dihapus"