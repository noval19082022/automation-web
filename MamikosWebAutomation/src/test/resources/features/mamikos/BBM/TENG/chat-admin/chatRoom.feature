@regression @chatRoom @loyalty @TENGP2

Feature: Chat Room

  Scenario: View image on Rajawali chat
    Given user navigates to "mamikos admin"
    And user logs in to Mamikos Admin via credentials as "admin loyalty"
    When user clicks on Chat-Chat Room menu
    And user click button consultant on field chat room
    And user click one of chat from chat list
    And user click last image on chat
    Then user see image on pop up
    When user click close on popup image
    Then pop up image will be close