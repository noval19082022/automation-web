@regression @tenantTracker @BBM3

Feature: SinggahSini - Tenant Tracker - Profile Page

  @BBM-561
  Scenario: Profile Page Display
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "Adisinggahsini" in the search field on main page
    * user click search button on main page filter
    * user clicks on the tenant name on the first row
    Then user verify search result on profile page bse contains "Adisinggahsini"

  @BBM-562
  Scenario: Pagination Functionality
    Given user navigates to "pms singgahsini"
    * user login as "pman admin"
    When user click Tenant Communication menu
    * user choose "Nama Penyewa" on main page filter
    * user input "Adisinggahsini" in the search field on main page
    * user click search button on main page filter
    * user clicks on the tenant name on the first row
    Then user see pagination menu on Detail Tenant is displayed
    When user click pagination number "2"
    Then user will be in the second pagination