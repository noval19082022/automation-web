@regression @BBM5 @voucher

Feature: Voucher Suggestion
  @BBM-475 @BBM-476 @BBM-477 @BBM-478 @BBM-700
  Scenario: Tenant have any eligible voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG voucher suggestion"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
#    BBM-478
    * tenant can sees voucher eligible contains "Anda memiliki"
    * tenant can sees voucher eligible contains "voucher"
    * tenant can sees voucher eligible is greater or equal than "1"
    When tenant clicks Masukkan to go to voucher selection
    * tenant clicks Pakai "1" index voucher list
#    BBM-476 assertion
    Then tenant can sees voucher status is green tick
    When user click delete voucher
    * tenant clicks Masukkan to go to voucher selection
    * tenant clicks Lihat Detail "1" index voucher list
#    TENG-548 assertion
    Then tenant can sees voucher detail section
    When tenant clicks Pakai button on voucher detail
#    BBM-477 assertion
    Then tenant can sees voucher status is green tick
#    #BBM-700
    When user click delete voucher
    * user access voucher form
    * user click use button
    Then system display voucher alert message "Masukkan kode voucher."
    When user access voucher form
    * input "TENGSALAH" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak ditemukan."

  Scenario: Tenant don't have any eligible voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG empty mamipoin"
    * user navigates to "mamikos /user/kost-saya/billing"
    * tenant clicks on Bayar button
    * tenant clicks Masukkan to go to voucher selection
    Then tenant can see voucher suggestion empty state