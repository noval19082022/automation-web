@regression @BBM4 @voucher

Feature: Apply Voucher For Booking Rule, Invoice With DP

  Scenario: Deleting Existing Booking
    Given user navigates to "backoffice"
    And user login  as a Admin via credentials
    When user click on Search Contract Menu form left bar
    And user Navigate "Search Contract" page
    * user search contract by tenant phone number "adi TENG apply voucher"
    * admin master terminate contract by today date

  Scenario: Cancel booking if tenant have booking
    Given user navigates to "mamikos /"
    And user clicks on Enter button
    And user login in as Tenant via phone number as "adi TENG apply voucher"
    When user navigates to "mamikos /user/booking/"
    Then Booking title is displayed
    When user cancel booking

#  Scenario: Tenant Booking Kost With DP
    Given user navigates to "mamikos /"
    When user clicks search bar
    * I search property with name "Kost Adi Auto Voucher DP" and select matching result to go to kos details page
    * user choose boarding date is "tomorrow" and clicks on Booking button on Kost details page
    * user input rent duration equals to 4
    * user selects T&C checkbox and clicks on Book button
    Then system display successfully booking

  Scenario: Owner Confirm Booking, and Tenant Apply Voucher
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user fills out owner login as "adi TENG Owner Voucher" and click on Enter button
    When user navigates to "owner /booking/request"
    * user select kost "Kost Adi Auto Voucher DP" on booking request page
    * user clicks on Booking Details button
    * user clicks on Accept button
    * select first room available and clicks on next button
    * user click save button

  Scenario: Invoice DP and Voucher For First Full Paid
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    * user login in as Tenant via phone number as "adi TENG apply voucher"
    * user click on the tenant profile
    * user click profile dropdown button
    * user click Riwayat dan Draft Booking menu
    * user click Bayar Sekarang button
    Then system display remaining payment "before" use voucher for payment "monthlyWithDP"
    * user access voucher form
    * input "AUTOFULLPAID" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For First Full Paid and Recurring Rule
    When user access voucher form
    * input "AUTOFULLPAIDREC" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For First Full Paid and Settlement Rule
    When user access voucher form
    * input "AUTOFPAIDSETTLE" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For Recurring Rule
    When user access voucher form
    * input "AUTORECURRING" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For Settlement Rule
    When user access voucher form
    * input "AUTOSETTLEMENT" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan"
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For Recurring and Settlement Rule
    When user access voucher form
    * input "AUTORECSETTLE" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For First Full Paid, Recurring, and Settlement Rule
    When user access voucher form
    * input "AUTOFPAIDRECSET" to field voucher code
    * user click use button
    Then system display voucher alert message "Kode voucher tidak bisa digunakan."
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For DP
    When user access voucher form
    * input "AUTODP" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlyWithDP"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For First Full Paid and DP
    When user access voucher form
    * input "AUTOFPAIDDP" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlyWithDP"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For DP and Settlement
    When user access voucher form
    * input "AUTODPSETTLE" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlyWithDP"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For DP and Recurring
    When user access voucher form
    * input "AUTODPRECURRING" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlyWithDP"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For First Full Paid, DP, and Settlement
    When user access voucher form
    * input "AUTOFPAIDDPSET" to field voucher code
    * user click use button
    Then system display remaining payment "after" use voucher for payment "monthlyWithDP"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For First Paid, DP, and Recurring
    When user access voucher form
    * input "AUTOFPAIDDPREC" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlyWithDP"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For DP, Settlement, and Recurring
    When user access voucher form
    * input "AUTODPSETREC" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlyWithDP"
    When user remove voucher
    Then voucher removed successfully
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"

#  Scenario: Invoice DP and Voucher For First Full Paid, DP, Settlement, and Recurring
    When user access voucher form
    * input "AUTOALLPAYRULES" to field voucher code
    * user click use button
    Then voucher applied successfully
    * system display remaining payment "after" use voucher for payment "monthlyWithDP"
    When user remove voucher
    * system display remaining payment "before" use voucher for payment "monthlyWithDP"