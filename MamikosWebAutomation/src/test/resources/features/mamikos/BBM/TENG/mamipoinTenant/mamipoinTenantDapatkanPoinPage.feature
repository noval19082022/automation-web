@regression @mamipoinTenant @BBM3

Feature: MamiPoin Tenant Dapatkan Poin Page

  @BBM-422 @BBM-423 @BBM-425 @BBM-417 @BBM-419 @BBM-416 @BBM-414
  Scenario: Tenant already on Dapatkan Poin page
    Given user navigates to "mamikos /"
    * user clicks on Enter button
    When user login in as Tenant via phone number as "adi TENG whitelist mamipoin"
    * user navigates to "mamikos /user/mamipoin/guideline"
    Then user verify title in the dapatkan poin page is displayed
    * user verify tab petunjuk in the dapatkan poin page is displayed
    * user verify tab syarat dan ketentuan in the dapatkan poin page is displayed
    * user verify link pusat bantuan in the dapatkan poin page is displayed
    # asterisk annotation used for make gherkin more elegantly readable
    # asterisk will substitute looping "And" annotation
    # e.g
    # And ...
    # And ...
    # And ...
    # And ...

#    BBM-422
#    Scenario: Headline on Dapatkan Poin Page
    * user verify dapatkan poin headline "Cara Mudah Mendapatkan MamiPoin"
    * user verify dapatkan poin subtitle "Kamu bisa mengumpulkan MamiPoin dengan melakukan aktivitas-aktivitas berikut."

#    BBM-423
#  Scenario: Content on Dapatkan Poin Page
    * user verify content on dapatkan poin page

#    BBM-425
#  Scenario: Scroll down behavior
    When user scroll down to pusat bantuan
    Then user verify only the header that is sticky or "fixed"

#    BBM-419 BBM-421
#  Scenario: Syarat dan ketentuan link redirection
    When user click on tab Syarat dan Ketentuan
    Then user verify "Syarat dan Ketentuan" has "active" attribute

#    BBM-417
#  Scenario: Pusat Bantuan redirection
    When user navigates to "mamikos /user/mamipoin/guideline"
    Then user verify title in the dapatkan poin page is displayed
    When user clicks link on pusat bantuan
    Then user redirected to "https://jambu.kerupux.com/user/mamipoin/guideline"