@BBM6 @kost-saya @booking1
Feature: Check TnC Refund for kost Singgah sini, apik and ELite

  @TEST_BBM-1193 @booking-billing-management @kost-saya @refund-policy @BBM-874 @BBM-873 @BBM-1777
  Scenario: [TnC Refund] [Detail Booking & Kost saya] Check TnC Refund for kost Singgah sini, apik and ELite
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "bbm kost refund singgahsini"
    And user navigates to "mamikos /user/booking/"
    And user click Filter and choose "Terbayar"
    And user click on Selengkapnya
    And user click on link refund
    Then user can see "syarat ketentuan tinggal di singgah sini" on mamihelp page
    When user navigates to "mamikos /"
    And user navigates to "mamikos /user/booking/"
    And tenant clicks on kost saya
    And user click on link refund
    Then user can see "syarat ketentuan tinggal di singgah sini" on mamihelp page