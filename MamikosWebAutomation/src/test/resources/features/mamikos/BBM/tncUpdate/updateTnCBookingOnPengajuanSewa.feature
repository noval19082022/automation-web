@BBM6 @BBM @tncUpdatePengajuanBooking
Feature: [TnC Booking][Detail Booking] Check Update TnC Booking on Pengajuan sewa page

	@TEST_BBM-1194 @TEST_BBM-1583 @booking-billing-management @booking-detail @tnc-update @toAutomate @booking
	Scenario: [TnC Booking][Detail Booking] Check TnC Booking on Pengajuan sewa page for kost reguler (BBM-1194)
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "OB tenant lainnnya validation"
		And user clicks search bar 
		And I search property with name "OB kost mhs validation" and select matching result to go to kos details page
		And user choose booking date for tomorrow and proceed to booking form
		Then user reach booking form
		And user click on Ajukan Sewa
		Then user can see Informasi popup
		When user can see tnc with "Syarat dan Ketentuan Umum"
		And user click on tnc "Syarat dan Ketentuan Umum"
		Then user can see tnc content with "Syarat dan Ketentuan Umum"

	@TEST_BBM-1582 @toAutomate
	Scenario: [Pengajuan Sewa][TnC Booking]Check TnC booking for kost Singgah sini, APIK and Kos Pilihan
		Given user navigates to "mamikos /"
		When user clicks on Enter button
		And user login in as Tenant via phone number as "OB tenant lainnnya validation"
		And user clicks search bar
		And I search property with name "teng kost singgah sini" and select matching result to go to kos details page
		And user choose booking date for tomorrow and proceed to booking form
		Then user reach booking form
		And user click on Ajukan Sewa
		Then user can see Informasi popup
		When user can see tnc with "Syarat dan Ketentuan Umum"
		And user click on tnc "Syarat dan Ketentuan Umum"
		Then user can see tnc content with "Syarat dan Ketentuan Umum"
		When user can see tnc with "Syarat dan Ketentuan Tinggal di Singgahsini, Apik, & Kos Pilihan"
		And user click on tnc "Syarat dan Ketentuan Tinggal di Singgahsini, Apik, & Kos Pilihan"
		Then user can see tnc content with "Syarat dan Ketentuan Tinggal di Singgahsini, APIK, dan Kos Pilihan"
