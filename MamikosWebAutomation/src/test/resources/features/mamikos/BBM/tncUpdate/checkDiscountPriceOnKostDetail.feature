@BBM6
Feature: Check harga coret on kost detail

  @TEST_BBM-1336 @Automated @discount_price @BBM-1337
  Scenario: [Kost Detail][Detail Price]Check harga coret on kost detail (BBM-1336)
    Given user navigates to "mamikos /"
    When user clicks on Enter button
    And user login in as Tenant via phone number as "OB contract sect dbet"
    And user clicks search bar
    And I search property with name "OB kost promo ngebut" and select matching result to go to kos details page
    Then user can see harga coret on detail price
    When user select date "tomorrow" and rent type "Per bulan"
    Then user can see harga coret on detail price
    When user select date "tomorrow" and rent type "Per 3 bulan"
    Then user cannot see harga coret on detail price
    When user select date "tomorrow" and rent type "Per minggu"
    Then user can see harga coret on detail price
    When user select date "tomorrow" and rent type "Per tahun"
    Then user cannot see harga coret on detail price