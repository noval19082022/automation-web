@regression @consultant
Feature: Filter Contract with All Parameter

  Scenario: User filter Contract Status is active
    Given user navigates to "consultant /"
    When user "dewy" login as a consultant
    When user access to menu "Kelola Kontrak"
    And user filter contract with contract status is "Aktif"
    Then system display contract list with contract status is "Dibayar" or "Belum Dibayar"