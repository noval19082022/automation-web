@cancelbooking
  Feature:  Admin Cancel Booking

    Scenario: Admin Cancel the booking from Data
      Given user navigates to "backoffice"
      And user login  as a Admin via credentials
      And user click on Search Contract Menu form left bar
      Then user Navigate "Search Contract" page
      And user search for Kost with name
      And user click on Cancel the Contract Button from action column