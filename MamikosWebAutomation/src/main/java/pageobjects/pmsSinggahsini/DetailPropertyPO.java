package pageobjects.pmsSinggahsini;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.security.cert.X509Certificate;
import java.util.List;

public class DetailPropertyPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public DetailPropertyPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy (xpath = "//h4[contains(text(),'Overview')]")
    private WebElement overviewTab;

    @FindBy (xpath = "//h4[contains(text(),'Kontrak Kerja Sama')]")
    private WebElement kontrakKerjaSamaTab;

    @FindBy (xpath = "//h4[contains(text(),'Detail Kos')]")
    private WebElement detailKosTab;

    @FindBy (xpath = "(//*[@class='bg-c-button bg-c-button--tertiary bg-c-button--md'])[2]")
    private WebElement ubahProfilPemilikButton;

    //kontrak kerja sama tab
    @FindBy (xpath = "//*[@class='bg-c-list-item__description']")
    private List<WebElement> kontrakKerjaSamaValue;

    @FindBy (xpath = "(//*[@class='bg-c-list-item__title bg-c-list-item__title--has-description'][contains(text(),'Nama')])[1]")
    private WebElement namaText;

    @FindBy(xpath = "//*[@class='bg-c-input__field']")
    private List<WebElement> ownerInputText;

    @FindBy (xpath = "//*[@placeholder='Masukkan Alamat']")
    private WebElement ownerAddressInputTextArea;

    @FindBy (xpath = "//*[@class='bg-c-select__trigger-text']")
    private List<WebElement> ownerInputDropdown;

    @FindBy (xpath = "//span[contains(text(),'Pilih Kota/Kabupaten di sini')]")
    private WebElement ownerCityEmptyDropdown;

    @FindBy (xpath = "//span[contains(text(),'Pilih kelurahan di sini')]")
    private WebElement ownerVillageEmptyDropdown;

    @FindBy (xpath = "//*[@class='bg-c-button bg-c-button--primary bg-c-button--md']")
    private List<WebElement> simpanButton;

    @FindBy (xpath = "//*[@class='bg-c-text bg-c-text--heading-5 ']")
    private List<WebElement> sectionTitle;

    @FindBy (xpath = "//*[@class='mb-16 bg-c-text bg-c-text--heading-5 ']")
    private WebElement rincianHargaTitle;

    @FindBy (xpath = "//*[@class='bg-c-list-item__title bg-c-list-item__title--has-description']")
    private List<WebElement> listItemTitle;

    @FindBy (xpath = "//*[@class='bg-c-list-item__description']")
    private List<WebElement> listItemValue;

    @FindBy (xpath = "//p[@class='bg-c-text bg-c-text--title-4 '][contains(text(), 'Ketentuan Komisi dari Penyewa DBET')]")
    private WebElement agreementHybridTitle;

    @FindBy (css = ".ss-table tbody tr td")
    private List<WebElement> tableTipeKamar;

    @FindBy (xpath = "//*[@class='bg-c-button bg-c-button--tertiary bg-c-button--md']")
    private List<WebElement> ubahButton;

    @FindBy (xpath = "//button[@class='bg-c-button mr-16 bg-c-button--tertiary bg-c-button--md']")
    private List<WebElement> detailKerjaSamaButtons;

    @FindBy (xpath = "//*[@class='bg-c-input__field']")
    private List<WebElement> detailKerjaSamaField;

    @FindBy (xpath = "//*[@class='bg-c-input__field']")
    private List<WebElement> informasiTransferPendapatanField;

    @FindBy (xpath = "//*[@class='bg-c-select__trigger-text']")
    private List<WebElement> informasiTransferPendapatanDropDown;

    @FindBy (xpath = "//div[@class='bg-c-field__label']")
    private List<WebElement> fieldTitle;
    //end of kontrak kerja sama tab

    // Overview Tab - START
    @FindBy (xpath = "//*[@class='bg-c-text bg-c-text--body-2 ']")
    private List<WebElement> profilePropertiValue;

    @FindBy (xpath = "//*[@class='bg-c-text bg-c-text--body-2 ']//a[contains(text(), 'Lihat Peta Lokasi')]")
    private WebElement profilePropertiMapLink;

    @FindBy (xpath = "//div[@class='trigger']")
    private List<WebElement> profilePropertiDropdown;

    @FindBy (xpath = "//p[@class='bg-c-text bg-c-text--body-2 ']")
    private List<WebElement> profilePropertiDropdownValue;

    @FindBy (xpath = "//textarea[@class='bg-c-textarea__field bg-c-textarea__field--lg']")
    private List<WebElement> profilePropertiTextArea;

    @FindBy (xpath = "//textarea[@class='bg-c-textarea__field bg-c-textarea__field--lg bg-c-textarea__field--error']")
    private WebElement profilePropertiTextAreaEmpty;

    @FindBy (xpath = "//*[@class='bg-c-list-item__description']")
    private List<WebElement> overviewValue;

    @FindBy (xpath = "//*[@class='bg-c-button bg-c-button--tertiary bg-c-button--sm'][contains(text(),'Edit')]")
    private WebElement editPenanggungJawabButton;

    @FindBy (xpath = "//*[@class='field-title bg-c-text bg-c-text--body-1 ']")
    private List<WebElement> detailProfilePropertiText;

    @FindBy (xpath = "//p[@class='flex align-center bg-c-text bg-c-text--heading-5 '][contains(text(),'Penanggung Jawab')]")
    private WebElement overviewPenanggungJawabCard;

    @FindBy (xpath = "//*[@class='bg-c-select__trigger-text']")
    private List<WebElement> penanggungJawabInputDropdown;

    @FindBy (xpath = "//*[@class='bg-c-button bg-c-button--primary bg-c-button--md']")
    private List<WebElement> simpanButtonPenanggungJawab;
    // Overview Tab - END

    //-------Informasi Pendapatan Transfer Properti--------//
    @FindBy (xpath = "//a[contains(., 'None')]")
    private List<WebElement> noneType;

    @FindBy (xpath = "//*[@disabled='disabled']")
    private List<WebElement> disableInput;

    @FindBy (xpath = "//input[@type='text']")
    private List<WebElement> enableInput;
    //-------Informasi Pendapatan Transfer Properti END--------//

    /**
     * choose tab menu
     * @param tab Overview,Kontrak Kerjasama, and Detail Kos
     * @throws InterruptedException
     */
    public void clickTab(String tab) throws InterruptedException {
        if (tab.equalsIgnoreCase("Overview")){
            selenium.clickOn(overviewTab);
        } else if (tab.equalsIgnoreCase("Kontrak Kerja Sama")){
            selenium.clickOn(kontrakKerjaSamaTab);
        } else if (tab.equalsIgnoreCase("Detail Kos")){
            selenium.clickOn(detailKosTab);
        }
    }

    // OVERVIEW Tab
    // Profile Properti - START
    /**
     * Get Property ID
     * @return String Property ID
     */
    public String getPropertyID() {
        selenium.waitTillElementIsVisible(profilePropertiValue.get(0));
        return selenium.getText(profilePropertiValue.get(0));
    }

    /**
     * Get Property Name
     * @return String Property Name
     */
    public String getPropertyName() {
        selenium.waitTillElementIsVisible(profilePropertiValue.get(1));
        return selenium.getText(profilePropertiValue.get(1));
    }

    /**
     * Get Property Product
     * @return String Property Product
     */
    public String getPropertyProduct() {
        selenium.waitTillElementIsVisible(profilePropertiValue.get(2));
        return selenium.getText(profilePropertiValue.get(2));
    }

    /**
     * Get Property Type
     * @return String Property Type
     */
    public String getPropertyType() {
        selenium.waitTillElementIsVisible(profilePropertiValue.get(3));
        return selenium.getText(profilePropertiValue.get(3));
    }

    /**
     * Get Property Tenant's Job
     * @return String Property Tenant's Job
     */
    public String getPropertyTenantJob() {
        selenium.waitTillElementIsVisible(profilePropertiValue.get(4));
        return selenium.getText(profilePropertiValue.get(4));
    }

    /**
     * Get Property Tenant's Religion
     * @return String Property Tenant's Religion
     */
    public String getPropertyTenantReligion() {
        selenium.waitTillElementIsVisible(profilePropertiValue.get(5));
        return selenium.getText(profilePropertiValue.get(5));
    }

    /**
     * Get Property Address
     * @return String Property Address
     */
    public String getPropertyAddress() {
        selenium.waitTillElementIsVisible(profilePropertiValue.get(6));
        return selenium.getText(profilePropertiValue.get(6));
    }

    /**
     * Get Property Location on Google Map
     * @return String Property Location on Google Map
     */
    public String getPropertyMap() {
        selenium.waitTillElementIsVisible(profilePropertiValue.get(7));
        String locationLink = profilePropertiMapLink.getAttribute("href");
        System.out.println(locationLink);
        return locationLink;
    }

    /**
     * Click Ubah to edit Profile Properti
     * @throws InterruptedException
     */
    public void clickUbahProfileProperti() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(ubahButton.get(0));
        selenium.clickOn(ubahButton.get(0));
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click Dropdown to select Tenant's Job or Tenant's Religion
     * @param indexDropdown
     * @throws InterruptedException
     */
    public void selectDropdown(Integer indexDropdown) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(profilePropertiDropdown.get(indexDropdown));
        WebElement tipe = driver.findElement(By.xpath("//div[@class='bg-c-field__label']//label[contains(text(), 'Tipe')]"));
        selenium.pageScrollInView(tipe);
        selenium.javascriptClickOn(profilePropertiDropdown.get(indexDropdown));
    }

    /**
     * Set Tenant's Job
     * @param pekerjaan
     * @throws InterruptedException
     */
    public void setPekerjaanPenyewa(String pekerjaan) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        if (pekerjaan.equalsIgnoreCase("Lainnya")) {
            WebElement pekerjaanCheckbox = driver.findElement(By.xpath("(//p[@class='bg-c-text bg-c-text--body-2 '][contains(text(), 'Lainnya')])[1]"));
            selenium.javascriptClickOn(pekerjaanCheckbox);
        } else {
            WebElement pekerjaanCheckbox = driver.findElement(By.xpath("//p[@class='bg-c-text bg-c-text--body-2 '][contains(text(), '"+pekerjaan+"')]"));
            selenium.javascriptClickOn(pekerjaanCheckbox);
        }
        selenium.hardWait(1);
    }

    /**
     * Set Tenant's Religion
     * @param agama
     * @throws InterruptedException
     */
    public void setAgamaPenyewa(String agama) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        if (agama.equalsIgnoreCase("Lainnya")) {
            WebElement pekerjaanCheckbox = driver.findElement(By.xpath("(//p[@class='bg-c-text bg-c-text--body-2 '][contains(text(), 'Lainnya')])[2]"));
            selenium.javascriptClickOn(pekerjaanCheckbox);
        } else {
            WebElement pekerjaanCheckbox = driver.findElement(By.xpath("//p[@class='bg-c-text bg-c-text--body-2 '][contains(text(), '"+agama+"')]"));
            selenium.javascriptClickOn(pekerjaanCheckbox);
        }
        selenium.hardWait(1);
    }

    /**
     * Set Property's Address or Property's Location on Google Map
     * @param value
     * @param index
     * @throws InterruptedException
     */
    public void setPropertyTextbox(String value, Integer index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(profilePropertiTextArea.get(index));
        selenium.clearTextField(profilePropertiTextArea.get(index));
        selenium.enterText(profilePropertiTextAreaEmpty, value, true);
    }

    /**
     * Save all edits on Profile Properti
     * @throws InterruptedException
     */
    public void saveEditProfileProperti() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(simpanButton.get(0));
        selenium.javascriptClickOn(simpanButton.get(0));
    }
    // Profile Properti - END

    // Penanggung Jawab - START
    /**
     * Get BSE Name
     * @return String BSE Name
     */
    public String getBSEName() {
        selenium.waitTillElementIsVisible(overviewPenanggungJawabCard,10);
        selenium.pageScrollInView(detailProfilePropertiText.get(7));
        selenium.waitTillElementIsVisible(overviewValue.get(6));
        return selenium.getText(overviewValue.get(6));
    }

    /**
     * Get BD Name
     * @return String BD Name
     */
    public String getBDName() {
        selenium.waitTillElementIsVisible(overviewValue.get(7));
        return selenium.getText(overviewValue.get(7));
    }

    /**
     * Get AS Name
     * @return String AS Name
     */
    public String getASName() {
        selenium.waitTillElementIsVisible(overviewValue.get(8));
        return selenium.getText(overviewValue.get(8));
    }

    /**
     * Get Hospitality Name
     * @return String Hospitality Name
     */
    public String getHospitalityName() {
        selenium.waitTillElementIsVisible(overviewValue.get(9));
        return selenium.getText(overviewValue.get(9));
    }

    /**
     * Click Edit on Penanggung Jawab section
     * @throws InterruptedException
     */
    public void clickEditPenanggungJawab() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(detailProfilePropertiText.get(7));
        selenium.waitTillElementIsClickable(editPenanggungJawabButton);
        selenium.clickOn(editPenanggungJawabButton);
    }

    /**
     * Set BSE Name
     * @param BSEName
     * @throws InterruptedException
     */
    public void setBSEName(String BSEName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(penanggungJawabInputDropdown.get(0));
        selenium.javascriptClickOn(penanggungJawabInputDropdown.get(0));
        selenium.clickOn(By.xpath("//ul/child::*[contains(., '" +BSEName+ "')]"));
    }

    /**
     * Set BD Name
     * @param BDName
     * @throws InterruptedException
     */
    public void setBDName(String BDName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(penanggungJawabInputDropdown.get(1));
        selenium.javascriptClickOn(penanggungJawabInputDropdown.get(1));
        selenium.clickOn(By.xpath("//ul/child::*[contains(., '" +BDName+ "')]"));
    }

    /**
     * Set AS Name
     * @param ASName
     * @throws InterruptedException
     */
    public void setASName(String ASName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(penanggungJawabInputDropdown.get(2));
        selenium.javascriptClickOn(penanggungJawabInputDropdown.get(2));
        selenium.clickOn(By.xpath("//ul/child::*[contains(., '" +ASName+ "')]"));
    }

    /**
     * Set Hospitality Name
     * @param hospitalityName
     * @throws InterruptedException
     */
    public void setHospitalityName(String hospitalityName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(penanggungJawabInputDropdown.get(3));
        selenium.javascriptClickOn(penanggungJawabInputDropdown.get(3));
        selenium.clickOn(By.xpath("(//ul)[5]/child::*[contains(., '" +hospitalityName+ "')]"));
    }

    /**
     * click Simpan on Penanggung Jawab profile and confirm pop up
     * @throws InterruptedException
     */
    public void saveEditPenanggungJawab() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(simpanButtonPenanggungJawab.get(0));
        selenium.waitTillElementIsVisible(simpanButtonPenanggungJawab.get(0));
        selenium.clickOn(simpanButtonPenanggungJawab.get(0));
        selenium.waitTillElementIsVisible(simpanButtonPenanggungJawab.get(1));
        selenium.clickOn(simpanButtonPenanggungJawab.get(1));
    }
    // Penanggung Jawab - END
    // OVERVIEW Tab


    /**
     * Get owner name
     * @return String owner name
     * @throws InterruptedException
     */
    public String getOwnerName() throws InterruptedException {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(0));
        return selenium.getText(kontrakKerjaSamaValue.get(0));
    }

    /**
     * Get owner phone number
     * @return String owner phone number
     */
    public String getOwnerPhoneNumber() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(1));
        return selenium.getText(kontrakKerjaSamaValue.get(1));
    }

    /**
     * Get owner Address
     * @return String owner Address
     */
    public String getOwnerAddress() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(2));
        return selenium.getText(kontrakKerjaSamaValue.get(2));
    }

    /**
     * Get owner Province
     * @return String owner province
     */
    public String getOwnerProvince() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(3));
        return selenium.getText(kontrakKerjaSamaValue.get(3));
    }

    /**
     * Get owner city
     * @return String owner city
     */
    public String getOwnerCity() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(4));
        return selenium.getText(kontrakKerjaSamaValue.get(4));
    }

    /**
     * Get owner Region
     * @return String owner region
     */
    public String getOwnerRegion() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(5));
        return selenium.getText(kontrakKerjaSamaValue.get(5));
    }

    /**
     * Get owner village
     * @return String owner village
     */
    public String getOwnerVillage() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(6));
        return selenium.getText(kontrakKerjaSamaValue.get(6));
    }

    /**
     * Click ubah profil pemilik
     * @throws InterruptedException
     */
    public void clickUbahProfilPemilik() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsClickable(ubahProfilPemilikButton);
        selenium.clickOn(ubahProfilPemilikButton);
    }

    /**
     * set Owner Name
     * @param ownerName
     */
    public void setOwnerName(String ownerName) throws InterruptedException {
        selenium.waitTillElementIsVisible(ownerInputText.get(0));
        selenium.clearTextField(ownerInputText.get(0));
        selenium.enterText(ownerInputText.get(0),ownerName,true);
    }

    /**
     * Set owner phone number
     * @param ownerPhoneNumber
     */
    public void setOwnerPhoneNumber(String ownerPhoneNumber) {
        selenium.waitTillElementIsVisible(ownerInputText.get(1));
        selenium.clearTextField(ownerInputText.get(1));
        selenium.enterText(ownerInputText.get(1),ownerPhoneNumber,true);
    }

    /**
     * Set owner address
     * @param ownerAddress
     */
    public void setOwnerAddress(String ownerAddress) {
        selenium.waitTillElementIsVisible(ownerAddressInputTextArea);
        selenium.clearTextField(ownerAddressInputTextArea);
        selenium.enterText(ownerAddressInputTextArea,ownerAddress,true);
    }

    /**
     * set owner province
     * @param ownerProvince
     * @throws InterruptedException
     */
    public void setOwnerProvince(String ownerProvince) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(0));
        selenium.javascriptClickOn(ownerInputDropdown.get(0));
        selenium.clickOn(By.xpath("(//ul)[2]/child::*[contains(., '" +ownerProvince+ "')]"));
    }

    /**
     * set owner city
     * @param ownerCity
     * @throws InterruptedException
     */
    public void setOwnerCity(String ownerCity) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(ownerCityEmptyDropdown);
        selenium.javascriptClickOn(ownerCityEmptyDropdown);
        selenium.clickOn(By.xpath("(//ul)[3]/child::*[contains(., '" +ownerCity+ "')]"));
    }

    /**
     * set owner region
     * @param ownerRegion
     * @throws InterruptedException
     */
    public void setOwnerRegion(String ownerRegion) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsClickable(ownerInputDropdown.get(2));
        selenium.javascriptClickOn(ownerInputDropdown.get(2));
        selenium.clickOn(By.xpath("(//ul)[4]/child::*[contains(., '" +ownerRegion+ "')]"));
        selenium.waitForJavascriptToLoad();
    }

    /**
     * set owner village
     * @param ownerVillage
     * @throws InterruptedException
     */
    public void setOwnerVillage(String ownerVillage) throws InterruptedException {
        selenium.waitTillElementIsClickable(ownerVillageEmptyDropdown);
        selenium.javascriptClickOn(ownerVillageEmptyDropdown);
        selenium.waitTillElementIsVisible(By.xpath("(//ul)[5]/child::*[contains(., '" +ownerVillage+ "')]"),3);
        selenium.clickOn(By.xpath("(//ul)[5]/child::*[contains(., '" +ownerVillage+ "')]"));
    }

    /**
     * click save owner profile and confirm pop up
     * @throws InterruptedException
     */
    public void saveEditOwnerProfile() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(simpanButton.get(0));
        selenium.waitTillElementIsVisible(simpanButton.get(0));
        selenium.clickOn(simpanButton.get(0));
        selenium.waitTillElementIsVisible(simpanButton.get(1));
        selenium.clickOn(simpanButton.get(1));
    }

    /**
     * get Nomor Rekening Value
     * @return String Nomor Rekening
     */
    public String getBankAccountNumber() throws InterruptedException {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(7));
        return selenium.getText(kontrakKerjaSamaValue.get(7));
    }

    /**
     * get Nama Bank
     * @return String Nama Bank
     */
    public String getBankName() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(8));
        return selenium.getText(kontrakKerjaSamaValue.get(8));
    }

    /**
     * get Cabang Bank
     * @return String Cabang Bank
     */
    public String getBankBranchName() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(9));
        return selenium.getText(kontrakKerjaSamaValue.get(9));
    }

    /**
     * get Nama Pemilik Rekening
     * @return String Nama Pemilik Rekening
     */
    public String getAccountOwnerName() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(10));
        return selenium.getText(kontrakKerjaSamaValue.get(10));
    }

    /**
     * get Tanggal Transfer ke Pemilik
     * @return String Tanggal Transfer ke Pemilik
     */
    public String getDisbursementDate() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(11));
        return selenium.getText(kontrakKerjaSamaValue.get(11));
    }

    /**
     * Scroll to Detail Kerja Sama section
     * @throws InterruptedException
     */
    public void scrollToDetailKerjaSama() throws InterruptedException {
        selenium.pageScrollInView(sectionTitle.get(2));
    }

    /**
     * Scroll to Biaya Tambahan section
     * @throws InterruptedException
     */
    public void scrollToBiayaTambahan() throws InterruptedException {
        selenium.pageScrollInView(kontrakKerjaSamaValue.get(25));
    }

    /**
     * Scroll to Rincian Harga Kamar section
     * @throws InterruptedException
     */
    public void scrollToRincianHargaKamar() throws InterruptedException {
        selenium.pageScrollInView(rincianHargaTitle);
    }

    /**
     * Get Jenis Produk
     * @return string jenis produk
     */
    public String getProduct() throws InterruptedException {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(12));
        return selenium.getText(kontrakKerjaSamaValue.get(12));
    }

    /**
     * get model kerja sama
     * @return string model kerja sama
     */
    public String getRevenueModel() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(13));
        return selenium.getText(kontrakKerjaSamaValue.get(13));
    }

    /**
     * get basic commission
     * @return string basic commission
     */
    public String getBasicCommission() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(14));
        return selenium.getText(kontrakKerjaSamaValue.get(14));
    }

    /**
     * get total room
     * @return string total room
     */
    public String getTotalRoom() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(15));
        return selenium.getText(kontrakKerjaSamaValue.get(15));
    }

    /**
     * get Tipe Add On JP
     * @return string Tipe Add On JP
     */
    public String getJPType() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(16));
        return selenium.getText(kontrakKerjaSamaValue.get(16));
    }

    /**
     * get Persentase Add On JP
     * @return string Persentase Add On JP
     */
    public String getJPPrecentage() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(17));
        return selenium.getText(kontrakKerjaSamaValue.get(17));
    }

    /**
     * get Jumlah Add On JP
     * @return string Jumlah Add On JP
     */
    public String getJPAmount() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(18));
        return selenium.getText(kontrakKerjaSamaValue.get(18));
    }

    /**
     * get Tipe Add On ADP
     * @return string Tipe Add On ADP
     */
    public String getADPType() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(19));
        return selenium.getText(kontrakKerjaSamaValue.get(19));
    }

    /**
     * get Persentase Add On ADP
     * @return string Persentase Add On ADP
     */
    public String getADPPrecentage() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(20));
        return selenium.getText(kontrakKerjaSamaValue.get(20));
    }

    /**
     * get Jumlah Add On ADP
     * @return string Jumlah Add On ADP
     */
    public String getADPAmount() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(21));
        return selenium.getText(kontrakKerjaSamaValue.get(21));
    }

    /**
     * get Pendapatan Pemilik
     * @return string Pendapatan Pemilik
     */
    public String getOwnerShare() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(22));
        return selenium.getText(kontrakKerjaSamaValue.get(22));
    }

    /**
     * get Pendapatan Mamikos
     * @return string Pendapatan Mamikos
     */
    public String getMamikosShare() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(23));
        return selenium.getText(kontrakKerjaSamaValue.get(23));
    }

    /////////
    /**
     * get DBET Pendapatan Pemilik
     * @return string DBET Pendapatan Pemilik
     */
    public String getDBETOwnerShare() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(24));
        return selenium.getText(kontrakKerjaSamaValue.get(24));
    }

    /**
     * get DBET Pendapatan Mamikos
     * @return string DBET Pendapatan Mamikos
     */
    public String getDBETMamikosShare() {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(25));
        return selenium.getText(kontrakKerjaSamaValue.get(25));
    }

    /**
     * get Jangka Waktu Kerja Sama
     * @return string Jangka Waktu Kerja Sama
     */
    public String getContractDuration(Integer index) {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(24+index));
        return selenium.getText(kontrakKerjaSamaValue.get(24+index));
    }

    /**
     * get Awal Kerja Sama
     * @return string Awal Kerja Sama
     */
    public String getStartDate(Integer index) {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(25+index));
        return selenium.getText(kontrakKerjaSamaValue.get(25+index));
    }

    /**
     * get Akhir Kerja Sama
     * @return string Akhir Kerja Sama
     */
    public String getEndDate(Integer index) {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(26+index));
        return selenium.getText(kontrakKerjaSamaValue.get(26+index));
    }

    /**
     * get Biaya Keanggotaan
     * @return string Biaya Keanggotaan
     */
    public String getMemberFee(Integer index) {
        selenium.waitTillElementIsVisible(kontrakKerjaSamaValue.get(27+index));
        return selenium.getText(kontrakKerjaSamaValue.get(27+index));
    }
    /////////

    /**
     * Get Name value of biaya tambahan
     * @param i index
     * @return String name biaya tambahan
     */
    public String getBiayaTambahanName(int i) throws InterruptedException {
        selenium.waitTillElementIsVisible(listItemTitle.get(28+i));
        return selenium.getText(listItemTitle.get(28+i));
    }

    /**
     * get Amount value of biaya tambahan
     * @param i index
     * @return String value biaya tambahan
     */
    public String getBiayaTambahanAmount(int i) {
        selenium.waitTillElementIsVisible(listItemValue.get(28+i));
        return selenium.getText(listItemValue.get(28+i));
    }

    /**
     * get value in first row of table
     * @param i index
     * @return String value every column from row 1
     */
    public String getTipeKamardanHargaTipeA(int i) throws InterruptedException {
        selenium.waitTillElementIsVisible(tableTipeKamar.get(i));
        return selenium.getText(tableTipeKamar.get(i));
    }

    /**
     * get value in second row of table
     * @param i index
     * @return String value every column from row 2
     */
    public String getTipeKamardanHargaTipeB(int i) {
        selenium.waitTillElementIsVisible(tableTipeKamar.get(i+12));
        return selenium.getText(tableTipeKamar.get(i+12));
    }

    /**
     * get value in third row of table
     * @param i index
     * @return String value every column from row 3
     */
    public String getTipeKamardanHargaTipeC(int i) {
        selenium.waitTillElementIsVisible(tableTipeKamar.get(i+24));
        return selenium.getText(tableTipeKamar.get(i+24));
    }

    public void clickUbahInformasiTransferPendapatan() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(ubahButton.get(2));
        selenium.clickOn(ubahButton.get(2));
        selenium.waitForJavascriptToLoad();
    }

    public void setAccountNumber(String bankAccNumber) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(informasiTransferPendapatanField.get(0));
        selenium.clearTextField(informasiTransferPendapatanField.get(0));
        selenium.enterText(informasiTransferPendapatanField.get(0),bankAccNumber,true);
    }

    public void setBankName(String bankName) throws InterruptedException {
        selenium.javascriptClickOn(informasiTransferPendapatanDropDown.get(0));
        selenium.pageScrollInView(By.xpath("(//ul)[2]/child::*[contains(., '" +bankName+ "')]"));
        selenium.clickOn(By.xpath("(//ul)[2]/child::*[contains(., '" +bankName+ "')]"));
    }

    public void setBranchName(String bankBranchName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(informasiTransferPendapatanField.get(2));
        selenium.clearTextField(informasiTransferPendapatanField.get(2));
        selenium.enterText(informasiTransferPendapatanField.get(2),bankBranchName,true);
    }

    public void setOwnerAccountName(String accountOwnerName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(informasiTransferPendapatanField.get(3));
        selenium.clearTextField(informasiTransferPendapatanField.get(3));
        selenium.enterText(informasiTransferPendapatanField.get(3),accountOwnerName,true);
    }

    public void setDisburseDate(String disbursementDate) throws InterruptedException {
        selenium.javascriptClickOn(informasiTransferPendapatanDropDown.get(1));
        selenium.pageScrollInView(By.xpath("(//ul)[3]/child::*[contains(., '" +disbursementDate+ "')]"));
        selenium.clickOn(By.xpath("(//ul)[3]/child::*[contains(., '" +disbursementDate+ "')]"));
    }

    public void saveEditInformasiTransferPendapatan() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(simpanButton.get(0));
        selenium.waitTillElementIsVisible(simpanButton.get(0));
        selenium.clickOn(simpanButton.get(0));
        selenium.waitTillElementIsVisible(simpanButton.get(1));
        selenium.clickOn(simpanButton.get(1));
    }

    public void clickUbahBiayaTambahan() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(ubahButton.get(3));
        selenium.clickOn(ubahButton.get(3));
    }

    /**
     * Click ubah Detail Kerja Sama
     * @param index
     * @throws InterruptedException
     */
    public void clickUbahDetailKerjaSama(Integer index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
        selenium.pageScrollInView(ubahButton.get(2));
        selenium.waitTillElementIsVisible(detailKerjaSamaButtons.get(index));
        selenium.clickOn(detailKerjaSamaButtons.get(index));
    }

    /**
     * Set Product
     * @param product
     * @throws InterruptedException
     */
    public void setProduct(String product) throws InterruptedException {
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(0));
        selenium.javascriptClickOn(ownerInputDropdown.get(0));
        selenium.clickOn(By.xpath("(//ul)[2]/child::*[contains(., '" +product+ "')]"));
        selenium.hardWait(1);
    }

    /**
     * Set Revenue Model
     * @param revenueModel
     * @throws InterruptedException
     */
    public void setRevenueModel(String revenueModel) throws InterruptedException {
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(1));
        selenium.javascriptClickOn(ownerInputDropdown.get(1));
        selenium.clickOn(By.xpath("(//ul)[3]/child::*[contains(., '" +revenueModel+ "')]"));
        selenium.hardWait(1);
    }

    /**
     * Set Basic Commission
     * @param basicCommission
     * @throws InterruptedException
     */
    public void setBasicCommission(String basicCommission) throws InterruptedException {
        selenium.waitTillElementIsVisible(detailKerjaSamaField.get(0));
        selenium.javascriptClickOn(detailKerjaSamaField.get(0));
        selenium.clearTextField(detailKerjaSamaField.get(0));
        selenium.enterText(detailKerjaSamaField.get(0), basicCommission, true);
        selenium.hardWait(1);
    }

    /**
     * Set JP Type
     * @param jpType
     * @throws InterruptedException
     */
    public void setJpType(String jpType) throws InterruptedException {
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(2));
        selenium.pageScrollInView(detailKerjaSamaField.get(1));
        selenium.javascriptClickOn(ownerInputDropdown.get(2));
        selenium.clickOn(By.xpath("(//ul)[4]/child::*[contains(., '" +jpType+ "')]"));
        selenium.hardWait(1);
    }

    /**
     * Set JP Percentage
     * @param jpPrecentage
     * @throws InterruptedException
     */
    public void setJpPercentage(String jpPrecentage) throws InterruptedException {
        selenium.waitTillElementIsVisible(detailKerjaSamaField.get(2));
        selenium.javascriptClickOn(detailKerjaSamaField.get(2));
        selenium.clearTextField(detailKerjaSamaField.get(2));
        selenium.enterText(detailKerjaSamaField.get(2), jpPrecentage, true);
        selenium.hardWait(1);
    }

    /**
     * Set JP Amount
     * @param jpAmount
     * @throws InterruptedException
     */
    public void setJpAmount(String jpAmount) throws InterruptedException {
        selenium.waitTillElementIsVisible(detailKerjaSamaField.get(3));
        selenium.javascriptClickOn(detailKerjaSamaField.get(3));
        selenium.clearTextField(detailKerjaSamaField.get(3));
        selenium.enterText(detailKerjaSamaField.get(3), jpAmount, true);
        selenium.hardWait(1);
    }

    /**
     * Set ADP Type
     * @param AdpType
     * @throws InterruptedException
     */
    public void setAdpType(String AdpType) throws InterruptedException {
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(3));
        selenium.javascriptClickOn(ownerInputDropdown.get(3));
        if (AdpType.equalsIgnoreCase("-")) {
            selenium.clickOn(By.xpath("(//ul)[5]/child::*[contains(., 'None')]"));
        } else {
            selenium.clickOn(By.xpath("(//ul)[5]/child::*[contains(., '" +AdpType+ "')]"));
        }
        selenium.hardWait(1);
    }

    /**
     * Set ADP Percentage
     * @param AdpPercentage
     * @throws InterruptedException
     */
    public void setAdpPercentage(String AdpPercentage) throws InterruptedException {
        selenium.waitTillElementIsVisible(detailKerjaSamaField.get(4));
        selenium.javascriptClickOn(detailKerjaSamaField.get(4));
        selenium.clearTextField(detailKerjaSamaField.get(4));
        selenium.enterText(detailKerjaSamaField.get(4), AdpPercentage, true);
        selenium.hardWait(1);
    }

    /**
     * Set ADP Amount
     * @param AdpAmount
     * @throws InterruptedException
     */
    public void setAdpAmount(String AdpAmount) throws InterruptedException {
        selenium.waitTillElementIsVisible(detailKerjaSamaField.get(5));
        selenium.javascriptClickOn(detailKerjaSamaField.get(5));
        selenium.clearTextField(detailKerjaSamaField.get(5));
        selenium.enterText(detailKerjaSamaField.get(5), AdpAmount, true);
        selenium.hardWait(1);
    }

    /**
     * Set Biaya Keanggotaan
     * @param memberFee
     * @throws InterruptedException
     */
    public void setMemberFee(String memberFee) throws InterruptedException {
        selenium.pageScrollInView(detailKerjaSamaField.get(5));
        selenium.waitTillElementIsVisible(detailKerjaSamaField.get(11));
        selenium.javascriptClickOn(detailKerjaSamaField.get(11));
        selenium.clearTextField(detailKerjaSamaField.get(11));
        selenium.enterText(detailKerjaSamaField.get(11), memberFee, true);
    }

    // Hybrid Revenue - START
    /**
     * Toggle the Model kerja sama hybrid
     * @throws InterruptedException
     */
    public void toggleHybrid (Boolean data) throws InterruptedException {
        selenium.pageScrollInView(fieldTitle.get(7));
        selenium.hardWait(2);
        if (data) {
            WebElement toggleNowOff = driver.findElement(By.xpath("//div[@class='bg-c-switch bg-c-switch--off']"));
            selenium.click(toggleNowOff);
        } else {
            WebElement toggleNowOn = driver.findElement(By.xpath("//div[@class='bg-c-switch bg-c-switch--on']"));
            selenium.click(toggleNowOn);
        }
    }

    /**
     * Fill Pendapatan Mamikos when Model kerja sama hybrid toggle is ON
     * @throws InterruptedException
     */
    public void fillPendapatanMamikos (String pendapatanMamikos) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(detailKerjaSamaField.get(9));
        selenium.clearTextField(detailKerjaSamaField.get(9));
        selenium.enterText(detailKerjaSamaField.get(9), "110", true);

        boolean errorMessage = driver.findElement(By.xpath("//div[@class='bg-c-field__message'][contains(text(), 'Tidak boleh melebihi 100%')]")).isDisplayed();
        if (errorMessage) {
            selenium.clearTextField(detailKerjaSamaField.get(9));
            selenium.enterText(detailKerjaSamaField.get(9), pendapatanMamikos, true);
        }
    }
    // Hybrid Revenue - END

    /**
     * Save all edits on Detail Kerja Sama
     * @throws InterruptedException
     */
    public void saveDetailKerjaSama() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(simpanButton.get(0));
        selenium.waitTillElementIsVisible(simpanButton.get(0));
        selenium.clickOn(simpanButton.get(0));
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementClickable(simpanButton.get(1), 20);
        selenium.clickOn(simpanButton.get(1));
    }

    //-------Informasi Pendapatan Transfer Properti--------//

    /**
     * Select "Commission/Static" Rate on model kerja sama
     * @param mks
     * @throws InterruptedException
     */
    public void revenueModelChanges(String mks) throws InterruptedException {
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(1));
        selenium.javascriptClickOn(ownerInputDropdown.get(1));
        selenium.javascriptClickOn(By.xpath("//a[contains(., '" +mks+ "')]"));
        selenium.hardWait(1);
    }

    /**
     * Select JP type to "Full A/Full B/Partial"
     * @param jpType
     * @throws InterruptedException
     */
    public void jpChanges(String jpType) throws InterruptedException {
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(2));
        selenium.pageScrollInView(detailKerjaSamaField.get(1));
        selenium.clickOn(ownerInputDropdown.get(2));
        WebElement jp = driver.findElement(By.xpath("(//ul)[4]/child::*[contains(., '" +jpType+ "')]"));
        selenium.clickOn(jp);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Input JP percentage
     * @param jpPercent
     */
    public void inputJPPercentage(String jpPercent){
        selenium.waitTillElementIsVisible(enableInput.get(2));
        selenium.enterText(enableInput.get(2), jpPercent, true);
    }

    /**
     * Input JP amount
     * @param jpAmount
     */
    public void inputJPAmount(String jpAmount){
        selenium.waitTillElementIsVisible(enableInput.get(3));
        selenium.enterText(enableInput.get(3), jpAmount, true);
    }

    /**
     * Select ADP type to "3 Bulan/6 Bulan"
     * @param adpType
     * @throws InterruptedException
     */
    public void adpChanges(String adpType) throws InterruptedException {
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(3));
        selenium.pageScrollInView(detailKerjaSamaField.get(1));
        selenium.clickOn(ownerInputDropdown.get(3));
        WebElement adp = driver.findElement(By.xpath("(//ul)[5]/child::*[contains(., '" +adpType+ "')]"));
        selenium.clickOn(adp);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Input ADP percentage
     * @param adpPercent
     */
    public void inputADPPercentage(String adpPercent){
        selenium.waitTillElementIsVisible(enableInput.get(4));
        selenium.enterText(enableInput.get(4), adpPercent, true);
    }

    /**
     * Input ADP amount
     * @param adpAmount
     */
    public void inputADPAmount(String adpAmount){
        selenium.waitTillElementIsVisible(enableInput.get(5));
        selenium.enterText(enableInput.get(5), adpAmount, true);
    }

    /**
     * Revert back into default setting
     * default setting -> refers to # Detail Kerja Sama # on dataUser.properties
     * @throws InterruptedException
     */
    public void revertBackToDefault() throws InterruptedException {
        //change the revenue model into default setting
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(1));
        selenium.javascriptClickOn(ownerInputDropdown.get(1));
        selenium.javascriptClickOn(By.xpath("//a[contains(., 'Static Rate')]"));
        selenium.waitForJavascriptToLoad();

        //change the jp type into default setting
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(2));
        selenium.pageScrollInView(detailKerjaSamaField.get(1));
        selenium.clickOn(ownerInputDropdown.get(2));
        WebElement jp = driver.findElement(By.xpath("(//ul)[4]/child::*[contains(., 'Full A')]"));
        selenium.clickOn(jp);
        selenium.waitForJavascriptToLoad();

        //change the jp percentage into default setting
        selenium.waitTillElementIsVisible(enableInput.get(2));
        selenium.enterText(enableInput.get(2), "5", true);

        //change the jp amount into default setting
        selenium.waitTillElementIsVisible(enableInput.get(3));
        selenium.enterText(enableInput.get(3), "4000000", true);

        //change the adp type into default setting
        selenium.waitTillElementIsVisible(ownerInputDropdown.get(3));
        selenium.pageScrollInView(detailKerjaSamaField.get(1));
        selenium.clickOn(ownerInputDropdown.get(3));
        WebElement adp = driver.findElement(By.xpath("(//ul)[5]/child::*[contains(., 'None')]"));
        selenium.clickOn(adp);
        selenium.waitForJavascriptToLoad();
    }
    //-------Informasi Pendapatan Transfer Properti END--------//
}
