package pageobjects.pmsSinggahsini;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class RoleListPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public RoleListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy (xpath = "//*[@data-testid='roleManagementSidebarMenu']")
    private WebElement roleMenuButton;

    @FindBy (xpath = "//*[@class='bg-c-button ml-16 bg-c-button--primary bg-c-button--lg']")
    private WebElement tambahRoleButton;

    @FindBy(xpath = "//*[@class='bg-c-toast__content']")
    private WebElement toastMessageText;

    @FindBy(xpath = "//*[@placeholder='Cari berdasarkan nama role']")
    private WebElement keywordText;

    @FindBy(xpath = "//button[contains(text(),'Cari')]")
    private WebElement cariButton;

    @FindBy(xpath = "//*[@class='ss-table']/tbody/tr/td")
    private WebElement roleNameText;

    @FindBy(xpath = "//*[@class='menu-activator']")
    private WebElement actionButton;

    @FindBy(xpath = "(//*[@class='bg-c-list-item bg-c-list-item--clickable'])[1]")
    private WebElement actionEditButton;

    @FindBy(xpath = "(//*[@class='bg-c-list-item bg-c-list-item--clickable'])[2]")
    private WebElement actionAturMemberButton;

    @FindBy(xpath = "(//*[@class='bg-c-list-item bg-c-list-item--clickable'])[3]")
    private WebElement actionHapusButton;

    @FindBy(xpath = "//*[@class='bg-c-button ml-16 bg-c-button--primary bg-c-button--md']")
    private WebElement confirmHapusRoleButton;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-button--tertiary bg-c-button--md']")
    private WebElement cancelHapusRoleButton;

    @FindBy(xpath = "//*[@class='no-data ta-c text-muted']")
    private WebElement roleNotFoundText;

    /**
     * wait for loading javascript to be load
     * @param index how much want to repeat
     * @throws InterruptedException
     */
    public void waitLoadingFinish(int index) throws InterruptedException {
        for(int i=0;i<index;i++){
            selenium.waitForJavascriptToLoad();
        }
    }

    /**
     * click Role Management Menu in sidebar
     * @throws InterruptedException
     */
    public void clickRoleMenu() {
        selenium.waitTillElementIsVisible(roleMenuButton,20);
        selenium.javascriptClickOn(roleMenuButton);
    }

    /**
     * click Tambah Role button
     * @throws InterruptedException
     */
    public void clickTambahRole() throws InterruptedException {
        selenium.clickOn(tambahRoleButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * get error message text
     * @return error message
     */
    public String getToastMessage() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(toastMessageText);
        return toastMessageText.getText();
    }

    /**
     * set keyoword in search field
     * @param keyword
     */
    public void setKeyword(String keyword) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.enterText(keywordText,keyword,true);
    }

    /**
     * click Cari button to search keyword
     */
    public void clickCari() throws InterruptedException {
        selenium.javascriptClickOn(cariButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Get role name in the list after search
     * @return 1st role name in the list
     * @throws InterruptedException
     */
    public String getRoleList() throws InterruptedException {
        waitLoadingFinish(2);
        selenium.waitTillElementIsVisible(roleNameText);
        return roleNameText.getText();
    }

    /**
     * click action button in the list
     * only for 1st row, you can use search first before click the action button
     */
    public void clickActionButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(actionButton);
    }

    /**
     * After click action button 3 options to edit, assign member, or hapus role
     * @param action
     */
    public void chooseAction(String action) throws InterruptedException {
        switch (action){
            case "Edit" :
                selenium.javascriptClickOn(actionEditButton);
                selenium.waitForJavascriptToLoad();
                break;
            case "Atur Member" :
                selenium.javascriptClickOn(actionAturMemberButton);
                selenium.waitForJavascriptToLoad();
                break;
            case "Hapus":
                selenium.javascriptClickOn(actionHapusButton);
                selenium.waitForJavascriptToLoad();
                break;
        }
    }

    /**
     * click hapus in hapus confirmation pop up
     * @throws InterruptedException
     */
    public void confirmHapusRole() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(confirmHapusRoleButton);
    }

    /**
     * check if empty state role list is displayed
     * @return boolean
     */
    public boolean ruleNotFound() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(roleNotFoundText);
        return roleNotFoundText.isDisplayed();
    }

    /**
     * click batal in hapus confirmation pop up
     */
    public void cancelHapusRole() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(cancelHapusRoleButton);
    }
}
