package pageobjects.pmsSinggahsini;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class TenantCommunicationPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public TenantCommunicationPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@data-testid='tenantCommunicationSidebarMenu']")
    private WebElement tenantCommunicationMenu;

    @FindBy(xpath = "//*[@class='action-bar__search-type bg-c-dropdown']")
    private WebElement mainPageFilterMenu;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-button--primary bg-c-button--lg bg-c-button--block'][contains(.,'Cari')]")
    private WebElement mainPageSearchButton;

    @FindBy(xpath = "//*[@class='bg-c-input__field']")
    private WebElement mainPageSearchField;

    @FindBy(xpath = "//tr[1]//td[2]/a")
    public List<WebElement> mainPageTableData;

    @FindBy(css = "tbody > tr:nth-of-type(1) .table-body__action-button")
    private WebElement actionButton;

    @FindBy(xpath = "//*[@class='bg-c-text bg-c-text--input-lg '][contains(.,'Tandai belum follow-up ')]")
    private WebElement tandaiBelumFollowUpButton;

    @FindBy(xpath = "//*[@class='bg-c-text bg-c-text--input-lg '][contains(.,'Tandai sudah follow-up ')]")
    private WebElement tandaiSudahFollowUpButton;

    @FindBy(xpath = "//span[.='Filter']")
    private WebElement penyewaFilter;

    @FindBy(xpath = "//a[contains(.,'Chat')]")
    private WebElement buttonTahapanChat;

    @FindBy(xpath = "//a[contains(.,'Butuh Cepat')]")
    private WebElement statusButuhCepat;

    @FindBy(xpath = "//button[.='Terapkan ']")
    private WebElement Terapkan;

    @FindBy(css = "tbody > tr:nth-of-type(1) .table-body__label")
    public WebElement resultFilter;

    @FindBy(xpath = "//a[contains(.,'Sudah Check-in')]")
    private WebElement checkin;

    @FindBy(xpath = "//span[contains(.,'Semua status')]")
    private WebElement FollowUpStatus;

    @FindBy(xpath = "//a[contains(.,'Sudah di-followup')]")
    private WebElement SudahDiFollowUp;

    @FindBy(xpath = "//a[contains(.,'Belum di-followup')]")
    private WebElement BelumDiFollowUp;

    @FindBy(xpath = "//*[contains(@class, 'phase-card')]")
    private WebElement detailText;

    @FindBy(xpath = "//*[contains(@class, 'phase-card__room-image')]")
    private WebElement detailRoomImage;

    @FindBy(xpath = "//button[.='Reset']")
    private WebElement ResetButton;

    @FindBy(xpath = "//*[@disabled='disabled'][contains(.,'Reset')]")
    private WebElement resetDisabled;

    @FindBy(xpath = "//span[contains(.,'Pilih BSE')]")
    private WebElement clickBSE;

    @FindBy(xpath = "//a[contains(.,'Account Manager Maya')]")
    private WebElement BSEMaya;

    @FindBy(css = "tbody > tr:nth-of-type(1) > td:nth-of-type(7)")
    public WebElement resultFilterOwner;

    @FindBy(xpath = "//*[@class='bg-c-empty-state']")
    private WebElement emptyStageTenantCommunicationText;

    @FindBy(xpath = "//a[contains(.,'+ Tambah Catatan')]")
    public WebElement buttonTambahCatatan;

    @FindBy(xpath = "//a[contains(.,'prioritaskan')]")
    public WebElement buttonPrioritaskan;

    @FindBy(css = ".bg-c-modal__body > .filter-action__wrapper > div:nth-of-type(2) .bg-c-select__trigger-text")
    public WebElement statusFilter;

    @FindBy(xpath = "//textarea[@id='textarea']")
    public WebElement fieldNote;

    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    public WebElement simpanNoteButton;

    @FindBy(xpath = "//a[contains(.,'prioritaskan')]")
    public WebElement prioritasText;

    @FindBy(css = "tbody > tr:nth-of-type(1) > td:nth-of-type(3)")
    public WebElement resultFilter2;

    @FindBy(xpath = "//*[@class='bg-c-empty-state__title']")
    public WebElement emptyPageText;

    @FindBy(css = ".bg-c-button--md")
    public WebElement trackChatWAButton;

    @FindBy(xpath = "//a[contains(.,'Gunakan nomor ini')]")
    public WebElement gunakanNoIniButton;

    @FindBy(xpath = "//button[.='Tambah']")
    private WebElement tambahButton;

    @FindBy(xpath = "//*[@data-testid='tenat-tracker-pagination']")
    private WebElement paginationMenuDetailTenant;

    @FindBy(xpath = "//p[contains(.,'Menampilkan 20 dari')]")
    public WebElement displayDataRow;

    @FindBy(xpath = "//tr[1]//td[1]//a[@class='bg-c-link bg-c-link--high']")
    public WebElement TenantNameOnTheFirstRow;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-pagination__item bg-c-button--primary bg-c-button--sm bg-c-pagination__item--selected']//span[contains(.,'2')]")
    public WebElement secondPagination;

    @FindBy(xpath = "//thead//tr//th//div//p")
    private List<WebElement> columName;

    @FindBy(xpath = "//*[@class='card-profile__table-title bg-c-text bg-c-text--title-3 ']")
    private WebElement textProfilePenyewa;

    @FindBy(css = ".tenant-profile__data > div:nth-of-type(2) > .tenant-profile__data-content")
    private WebElement textRenterName;

    @FindBy(css = ".tenant-profile__history-title > .bg-c-text")
    private WebElement textRiwayatPencarian;

    @FindBy(xpath = "//p[.='Tambah track status chat WA']")
    private WebElement tambahTrackerStatusWA;

    @FindBy(css = ".bg-c-textarea__field")
    public WebElement fieldNoteStatusWA;

    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    private WebElement buttonTambahStatusWA;

    @FindBy(css = "tbody > tr:nth-of-type(1) .table-body__ellipsis")
    private WebElement getTextPrioritaskan;

    @FindBy(xpath = "//*[@class='bg-c-pagination bg-c-pagination--center']")
    private WebElement paginationMenuMainPage;

    /**
     * click Tenant Communication menu
     * @throws InterruptedException
     */
    public void clickTenantCommunicationMenu() throws InterruptedException {
        selenium.clickOn(tenantCommunicationMenu);
        selenium.hardWait(6);
    }

    /**
     * click Main Page filter
     * @throws InterruptedException
     */
    public void clickMainPageFilter() throws InterruptedException {
        selenium.clickOn(mainPageFilterMenu);
    }

    /**
     * Select Main Page filter
     *
     * @throws InterruptedException
     */
    public void selectMainPageFilter(String mainPageFilter) throws InterruptedException {
        selenium.javascriptClickOn(By.xpath("//li/a[contains(., '" + mainPageFilter + "')]"));
        selenium.hardWait(3);
    }

    /**
     * Enter Text in search bar
     * @param keyword is text we want to search
     */
    public void inputSearchFieldMainPage(String keyword) {
        selenium.enterText(mainPageSearchField, keyword, false);
    }

    /**
     * click Search button main page filter
     * @throws InterruptedException
     */
    public void clickSearchButtonMainPageFilter() throws InterruptedException {
        selenium.clickOn(mainPageSearchButton);
        selenium.hardWait(5);
    }

    /**
     * click action button
     * @throws InterruptedException
     */
    public void clickActionButton() throws InterruptedException {
        selenium.clickOn(actionButton);
    }

    /**
     * click Action Button Tandai Belum FollowUp
     * @throws InterruptedException
     */
    public void clickTandaiBelumFollowUp() throws InterruptedException {
        selenium.clickOn(tandaiBelumFollowUpButton);
        selenium.hardWait(10);
    }

    /**
     * click Action Button Tandai Sudah FollowUp
     * @throws InterruptedException
     */
    public void clickTandaiSudahFollowUp() throws InterruptedException {
        selenium.clickOn(tandaiSudahFollowUpButton);
        selenium.hardWait(10);
    }

    /**
     * get Text "Filter Result"
     * @throws InterruptedException
     */
    public String getFilterResult() {
        return selenium.getText(resultFilter);
    }


    /**
     * get Text "Tandai Sudah FollowUp"
     * @throws InterruptedException
     */
    public String getTandaiSudahFollowUpButton() {
        return selenium.getText(tandaiSudahFollowUpButton);
    }

    /**
     * get Text "Tandai Belum FollowUp"
     * @throws InterruptedException
     */
    public String getTandaiBelumFollowUpButton() {
        return selenium.getText(tandaiBelumFollowUpButton);
    }
    /**
     * click Filter Penyewa
     * @throws InterruptedException
     */
    public void clickFilterPenyewa() throws InterruptedException {
        selenium.clickOn(penyewaFilter);
        selenium.hardWait(3);
    }

    /**
     * click Terapkan
     * @throws InterruptedException
     */
    public void  clickTerapkanButton() throws InterruptedException {
        selenium.clickOn(Terapkan);
    }

    /**
     * click Follow Up Status
     * @throws InterruptedException
     */
    public void clickFollowUpStatus() throws InterruptedException {
        selenium.clickOn(FollowUpStatus);
    }
    /**
     * click Sudah Di Follow Up
     * @throws InterruptedException
     */
    public void clickSudahDiFollowUp() throws InterruptedException {
        selenium.clickOn(SudahDiFollowUp);
    }

    /**
     * click Belum Di Follow Up
     * @throws InterruptedException
     */
    public void clickBelumDiFollowUp() throws InterruptedException {
        selenium.clickOn(BelumDiFollowUp);
    }

    /**
     * Get Text detail
     *
     * @return Detail chat, check-in, booking, survey
     */
    public String getDetailText (){
        return selenium.getText(detailText);
    }

    /**
     * Verify detail room image available
     * @return true if available
     */
    public boolean isDetailRoomImageAvailable(){
        return selenium.waitInCaseElementVisible(detailRoomImage,2) != null;
    }

    /**
     * click Reset
     * @throws InterruptedException
     */
    public void clickReset() throws InterruptedException {
        selenium.clickOn(ResetButton);
    }

    /**
     * get icon Reset is disabled
     * @throws InterruptedException
     */
    public void getIconResetIsDisabled() throws InterruptedException {
        selenium.waitTillElementIsVisible(resetDisabled, 2);
    }
    /**
     * click Filter BSE
     * @throws InterruptedException
     */
    public void clickBSE() throws InterruptedException {
        selenium.clickOn(clickBSE);
        selenium.clickOn(BSEMaya);
    }
    /**
     * get Text "Filter Result Owner"
     * @throws InterruptedException
     */
    public String getFilterResultOwner() {
        return selenium.getText(resultFilterOwner);
    }

    /** Get booking duration
     * @return String
     * <p>"Data Tidak Ditemukan"</p>
     * <p>"Mohon periksa kembali kata kunci yang Anda masukkan"</p>
     */
    public String getEmptyDataTenantCommunication(){
        return selenium.getText(emptyStageTenantCommunicationText);
    }
    /**
     * clear Note
     * @throws InterruptedException
     */
    public void clearNoteField() throws InterruptedException {
        selenium.clearTextField(fieldNote);
    }
    /**
     * click Tambah Catatan
     * @throws InterruptedException
     */
    public void clickTambahCatatan() throws InterruptedException {
        selenium.clickOn(buttonTambahCatatan);
    }

    /**
     * Enter Text in search bar note
     * @param keyword is text we want to search
     */
    public void enterTextNote(String keyword) {
        selenium.enterText(fieldNote, keyword, false);
    }
    /**
     * get Text "Filter Result Note"
     * @throws InterruptedException
     */
    public String getFilterResultNote() {
        return selenium.getText(prioritasText);
    }

    /**
     * get Text "+ Tambah Catatan"
     * @throws InterruptedException
     */
    public String getTambahCatatanText() {
        return selenium.getText(buttonTambahCatatan);
    }

    /**
     * Select Filter Tahapan
     *
     * @throws InterruptedException
     */
    public void selectFilterTahapan(String filterTahapan) throws InterruptedException {
        selenium.javascriptClickOn(By.xpath("//li/a[contains(., '" + filterTahapan + "')]"));
        selenium.hardWait(3);
    }

    /**
     * Select Filter Status
     *
     * @throws InterruptedException
     */
    public void selectFilterStatus(String filterStatus) throws InterruptedException {
        selenium.javascriptClickOn(By.xpath("//li/a[contains(., '" + filterStatus + "')]"));
        selenium.hardWait(3);
    }
    /**
     * click Filter Status
     * @throws InterruptedException
     */
    public void clickFilterStatus() throws InterruptedException {
        selenium.clickOn(statusFilter);
        selenium.hardWait(3);
    }
    /**
     * get Text "Filter Result2"
     * @throws InterruptedException
     */
    public String getFilterResult2() {
        return selenium.getText(resultFilter2);
    }
    /**
     * Click Prioritaskan Note
     */
    public void clickPrioritaskan() throws InterruptedException {
        selenium.clickOn(prioritasText);
    }

    /**
     * Click Simpan Note
     */
    public void clickSimpanNote() throws InterruptedException {
        selenium.clickOn(simpanNoteButton);
    }

    /**
     * get Text Empty Page
     * @throws InterruptedException
     */
    public String getEmptyPageText() {
        return selenium.getText(emptyPageText);
    }

    /**
     * Click on Track Status Chat WA
     */
    public void clickTrackStatusWAButton() throws InterruptedException {
        selenium.clickOn(trackChatWAButton);
    }

    /**
     * Enter Text in search bar note
     * @param data is text we want to input
     */
    public void enterTextTrackStatusWA(String data, String field) {
        WebElement element = driver.findElement(By.xpath("//*[@placeholder='"+field+"']"));
        selenium.enterText(element, data, false);
    }

    /**
     * Click on gunakan nomor ini on Track Status WA
     */
    public void clickGunakanNomorIniButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(gunakanNoIniButton);
    }

    /**
     * Choose kost name
     * @param kostName is text kost name
     */
    public void chooseKostName(String kostName) throws InterruptedException{
        selenium.hardWait(3);
        WebElement element = driver.findElement(By.xpath("//span[contains(.,'"+kostName+"')]"));
        selenium.clickOn(element);
    }

    /**
     * click Tambah Button
     * @throws InterruptedException
     */
    public void  clickTambahButton() throws InterruptedException {
        selenium.clickOn(tambahButton);
    }

    /**
     * Verify pagination menu is visible
     * @return true if actions column is visible
     */
    public boolean verifyPaginationMenuOnMainPage() {
        return selenium.waitInCaseElementVisible(paginationMenuMainPage, 3) != null;
    }

    /**
     * Select Pagination Number
     *
     * @throws InterruptedException
     */
    public void clickPaginationNumber(String paginationNumber) throws InterruptedException {
        selenium.javascriptClickOn(By.xpath("//button[contains(., '" + paginationNumber + "')][1]"));
        selenium.hardWait(3);
    }

    /**
     * Verify display data row
     * @return true if actions column is visible
     */
    public boolean verifyDisplayDataRow(){
        return selenium.waitInCaseElementVisible(displayDataRow, 3) != null;
    }

    /**
     * click Tenant Name on the First Row
     */
    public void clickTenantNameOnTheFirstRow() throws InterruptedException {
        selenium.clickOn(TenantNameOnTheFirstRow);
        selenium.hardWait(3);
    }

    /**
     * Verify 2nd Pagination
     * @return true if actions column is visible
     */
    public boolean verifySecondPagination(){
        return selenium.waitInCaseElementVisible(secondPagination, 3) != null;
    }

    /**
     * Get Property Name on Profile Page Filter
     * @return property name e.g. Mindful Peaks
     */
    public String getPropertyNameOnProfilePageFilter(String propertyName){
        return selenium.getText(By.xpath("//tr[1]//td[1][contains(text(), '"+propertyName+"')]"));
    }

    /**
     * Get Property Name on Main Page Filter
     * @return property name e.g. Mindful Peaks
     */
    public String getPropertyNameOnMainPageFilter(String propertyName){
        return selenium.getText(By.xpath("//tr[1]//td[2]/a[contains(text(), '"+propertyName+"')]"));
    }

    /**
     * Get Property Name on Main Page Filter
     * @return property name e.g. Mindful Peaks
     */
    public boolean getPropertyNameOnMainPageFilterIsNot(String propertyName){
        return selenium.waitInCaseElementPresent(By.xpath("//tr[1]//td[2]/a[contains(text(), '"+propertyName+"')]"), 5)!=null;
    }
    /**
     * Get Text of Head Table Segment by index
     * @param index - index head table
     * @return text of Head Table
     */
    public String getColumnName (int index){
        return selenium.getText(columName.get(index));
    }

    /**
     * get Text "TextProfilPenyewa"
     * @throws InterruptedException
     */
    public String getTextProfilPenyewa() {
        return selenium.getText(textProfilePenyewa);
    }

    /**
     * get Text Nama Penyewa
     * @throws InterruptedException
     */
    public String getRenterName(String namaPenyewa) {
        return selenium.getText(By.xpath("//*[@class='tenant-profile__title bg-c-text bg-c-text--heading-3 '][contains(.,'"+namaPenyewa+"')]"));
    }

    /**
     * get Text "RiwayatPencarianKos"
     * @throws InterruptedException
     */
    public String getTextRiwayatPencarianKos() {
        return selenium.getText(textRiwayatPencarian);
    }

    /**
     * get Tenant Name on Main Page Filter
     */
    public String getTenantNameOnMainPageFilter(String tenantName) {
        return selenium.getText(By.xpath("//tbody/tr[1]/td[1]/*[1][contains(.,'"+tenantName+"')]"));
    }

    /**
     * click Action Add tracker status WA
     * @throws InterruptedException
     */
    public void clickTambahTrackerWA() throws InterruptedException {
        selenium.clickOn(tambahTrackerStatusWA);
    }
    /**
     * Enter Text in tambah tracker status WA
     * @param keyword is text we want to search
     */
    public void enterTextNoteStatusWA(String keyword) {
        selenium.enterText(fieldNoteStatusWA, keyword, false);
    }
    /**
     * click Tambah on tracker status WA
     * @throws InterruptedException
     */
    public void clickTambahStatusWA() throws InterruptedException {
        selenium.clickOn(buttonTambahStatusWA);
    }

    /**
     * get Text "Prioritaskan"
     * @throws InterruptedException
     */
    public String getTextPrioritaskan() {
        return selenium.getText(getTextPrioritaskan);
    }

    /**
     * Check Tandai belum follow-up is present
     * @return true / false
     */
    public boolean isTandaiBelumFollowupPresent() {
        return selenium.waitInCaseElementVisible(tandaiBelumFollowUpButton, 10) != null;
    }

    /**
     * Set to Tandai belum follow-up
     * @throws InterruptedException
     */
    public void setTandaiBelumFollowUp() throws InterruptedException {
        if (isTandaiBelumFollowupPresent()) {
            selenium.clickOn(tandaiBelumFollowUpButton);
            selenium.hardWait(10);
        }
    }

    /**
     * Verify pagination menu is visible
     * @return true if actions column is visible
     */
    public boolean verifyPaginationMenuOnDetailTenant() {
        return selenium.waitInCaseElementVisible(paginationMenuDetailTenant, 3) != null;
    }
}