package pageobjects.pmsSinggahsini;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.text.CharacterIterator;
import java.text.ParseException;
import java.util.List;

public class RoomAllotmentPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private JavaHelpers java = new JavaHelpers();


    public RoomAllotmentPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='switch-container']")
    private WebElement outOFOrderButton;

    @FindBy(xpath = "//*[@class='bg-c-input__field']")
    private List<WebElement> inputField;

    @FindBy(xpath = "//*[@class='bg-c-button ml-16 bg-c-button--primary bg-c-button--md']")
    private WebElement submitOutOfOrderButton;

    @FindBy(xpath = "((//*[@class='cv-daily__head-label'])[1]/span)[2]")
    private WebElement dayTodayLabel;

    @FindBy(xpath = "//*[@class='next']")
    private List<WebElement> nextMonthButtonInDatepicker;

    /**
     * Set out of order active in selected room
     * @param room room name
     * @throws InterruptedException
     */
    public void setRoomOutOfOrder(String room) throws InterruptedException {
        String roomButton = "//div[@title='"+room+"']";
        //check if room already out of order
        if (isRoomHaveOutOfOrderStatus(room)){
            selenium.waitTillElementIsClickable(By.xpath(roomButton));
            selenium.clickOn(By.xpath(roomButton));
            selenium.waitTillElementIsClickable(outOFOrderButton);
            selenium.clickOn(outOFOrderButton);
            selenium.waitForJavascriptToLoad();
            selenium.hardWait(5);
            selenium.waitTillElementIsVisible(By.xpath(roomButton),15);
            selenium.waitForJavascriptToLoad();
        }
        // enable out of order in selected room
        selenium.waitTillElementIsClickable(By.xpath(roomButton));
        selenium.clickOn(By.xpath(roomButton));
        selenium.waitTillElementIsClickable(outOFOrderButton);
        selenium.clickOn(outOFOrderButton);
    }

    /**
     * set Reason in out of order modal
     * @param reason reason out of order
     */
    public void setOutOfOrderReason(String reason) {
        selenium.waitTillElementIsVisible(inputField.get(0));
        selenium.enterText(inputField.get(0),reason,true);
    }

    /**
     * Set Out Of Order Duration in out of order modal
     * @param duration in days (max 21 days) -> days available in calendar view
     *                 because the date limit display in calendar view
     *                 ex: 3 => start date = today, end date =  3 days from today
     * @throws ParseException
     * @throws InterruptedException
     */
    public void setOutOfOrderDuration(int duration) throws ParseException, InterruptedException {
        String endDate = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", duration, 0, 0, 0);

        //select start date (today)
        selenium.clickOn(inputField.get(1));
        if (dayTodayLabel.getText().equalsIgnoreCase("Sab")){
            selenium.javascriptClickOn(By.xpath("//*[@class='cell day today weekend sat']"));
        } else if (dayTodayLabel.getText().equalsIgnoreCase("Min")){
            selenium.javascriptClickOn(By.xpath("//*[@class='cell day today weekend sun']"));
        } else {
            selenium.javascriptClickOn(By.xpath("//*[@class='cell day today']"));
        }
        selenium.hardWait(1);

        //select end date (n days from today)
        selenium.clickOn(inputField.get(2));
        String element = "((//*[@class='cv-daily__head-label'])["+(duration+1)+"]/span)[2]";
        String dayUntilLabel = selenium.getText(By.xpath(element));

        if (dayUntilLabel.equalsIgnoreCase("Sab")){
            //try to click date weekend saturday
            try {
                selenium.javascriptClickOn(By.xpath("((//*[@class='picker-view'])[2]/div/div/div//*[@class='cell day weekend sat'][contains(text(),'"+endDate+"')])[1]"));
            }catch (Exception e){
                System.out.println("date is not weekend saturday");
                selenium.clickOn(nextMonthButtonInDatepicker.get(1));
                selenium.javascriptClickOn(By.xpath("((//*[@class='picker-view'])[2]/div/div/div//*[@class='cell day weekend sat'][contains(text(),'"+endDate+"')])[1]"));
            }

        } else if (dayUntilLabel.equalsIgnoreCase("Min")){
            //try to click date weekend sunday
            try {
                selenium.javascriptClickOn(By.xpath("((//*[@class='picker-view'])[2]/div/div/div//*[@class='cell day weekend sun'][contains(text(),'"+endDate+"')])[1]"));
            }catch (Exception e){
                System.out.println("date is not weekend sunday");
                selenium.clickOn(nextMonthButtonInDatepicker.get(1));
                selenium.javascriptClickOn(By.xpath("((//*[@class='picker-view'])[2]/div/div/div//*[@class='cell day weekend sun'][contains(text(),'"+endDate+"')])[1]"));
            }

        } else {
            //try to click date weekdays
            try {
                selenium.javascriptClickOn(By.xpath("((//*[@class='picker-view'])[2]/div/div/div//*[@class='cell day'][contains(text(),'"+endDate+"')])[1]"));
            }catch (Exception e){
                System.out.println("date is weekdays");
                selenium.clickOn(nextMonthButtonInDatepicker.get(1));
                selenium.javascriptClickOn(By.xpath("((//*[@class='picker-view'])[2]/div/div/div//*[@class='cell day'][contains(text(),'"+endDate+"')])[1]"));
            }
        }
        selenium.hardWait(1);
    }

    /**
     * click Simpan in out of order modal
     * @throws InterruptedException
     */
    public void submitOutOfOrder() throws InterruptedException {
        selenium.waitTillElementIsClickable(submitOutOfOrderButton);
        selenium.clickOn(submitOutOfOrderButton);
    }

    /**
     * check if room is out of order or not, by have out of order icon or not
     * @param room room name
     * @return Boolean true if have out of order mark, false if not
     */
    public Boolean isRoomHaveOutOfOrderStatus(String room) {
        String outOfOrderMark = "//div[@title='"+room+"']/p/span[@class='out-of-order-icon']";
        return selenium.isElementPresent(By.xpath(outOfOrderMark));
    }
}
