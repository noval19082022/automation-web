package pageobjects.pmsSinggahsini;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class CommonPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public CommonPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    /**
     * wait for loading javascript to be load
     * @param index how much want to repeat
     * @throws InterruptedException
     */
    public void waitMultipleJavascriptLoading(int index) throws InterruptedException {
        for(int i=0;i<index;i++){
            selenium.waitForJavascriptToLoad();
        }
    }
}
