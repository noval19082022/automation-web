package pageobjects.pmsSinggahsini;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class HomepagePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public HomepagePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//div[@class='bg-l-sidebar__item-icon']")
    private List<WebElement> mainNavIcons;

    @FindBy(xpath = "//*[@placeholder='Cari berdasarkan ID, Nama Properti']")
    private WebElement searchPropertyText;

    @FindBy(xpath = "//button[contains(text(),'Cari')]")
    private WebElement cariButton;

    @FindBy(xpath = "//*[@class='option-detail']")
    private WebElement actionButton;

    @FindBy(xpath = "(//div[@class='bg-c-list-item__description'])[1]")
    private WebElement lihatDetailButton;

    @FindBy(xpath = "(//div[@class='bg-c-list-item__description'])[2]")
    private WebElement ketersediaanKamarButton;

    @FindBy(xpath = "//button[contains(text(),'Unduh CSV')]")
    private WebElement unduhCsvButton;

    @FindBy(xpath = "(//table/tbody/tr/td)[1]")
    private WebElement firstPropertyID;

    @FindBy(xpath = "(//table/tbody/tr/td)[2]")
    private WebElement firstPropertyName;

    @FindBy(xpath = "//*[@class='bg-c-input__icon bg-c-input__icon--right bg-c-icon bg-c-icon--md bg-c-input__icon--clickable']")
    private WebElement clearSearchButton;

    @FindBy(xpath = "//*[@class='bg-c-text bg-c-text--title-2 ']")
    private WebElement dataTidakDitemukanText;

    @FindBy(xpath = "//*[@class='total-data bg-c-text bg-c-text--body-2 ']")
    private WebElement totalDataText;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-button--tertiary bg-c-button--lg bg-c-button--block']")
    private List<WebElement> homeButtons;

    @FindBy(xpath = "//input[@class='bg-c-input__field']")
    private List<WebElement> inputFields;

    @FindBy(xpath = "//span[@class='day__month_btn']")
    private List<WebElement> dateMonthButton;

    @FindBy(xpath = "//span[@class='month__year_btn up']")
    private WebElement monthYearButtonUp;

    @FindBy(xpath = "//div[@class='trigger']")
    private List<WebElement> dropdownFields;

    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    private WebElement terapkanButton;

    /**
     * set keyword to search field and click cari button
     * @param propertyname
     * @throws InterruptedException
     */
    public void searchProperty(String propertyname) throws InterruptedException {
        selenium.waitTillElementIsVisible(totalDataText);
        selenium.enterText(searchPropertyText,propertyname,true);
        selenium.clickOn(cariButton);
    }

    /**
     * check if button exist
     * @param button
     * @return
     * @throws InterruptedException
     */
    public boolean checkButtonExist(String button) throws InterruptedException {
        boolean exist = false;
        switch (button){
            case "Lihat Detail":
                if (selenium.isElementDisplayed(actionButton)){
                    selenium.javascriptClickOn(actionButton);
                    exist = selenium.isElementDisplayed(lihatDetailButton);
                    selenium.javascriptClickOn(actionButton);
                } else {
                    exist = false;
                }
                break;
            case "Ketersediaan Kamar":
                if (selenium.isElementDisplayed(actionButton)){
                    selenium.javascriptClickOn(actionButton);
                    exist = selenium.isElementDisplayed(ketersediaanKamarButton);
                    selenium.javascriptClickOn(actionButton);
                } else {
                    exist = false;
                }
                break;
            case "Unduh CSV":
                if (selenium.isElementDisplayed(unduhCsvButton)){
                    selenium.waitTillElementIsVisible(unduhCsvButton);
                    exist = selenium.isElementDisplayed(unduhCsvButton);
                } else {
                    exist = false;
                }
                break;
        }
        return exist;
    }

    /**
     * fill id in serach field
     * @param id property ID
     */
    public void searchPropertyByID(String id) throws InterruptedException {
        selenium.hardWait(5);
        selenium.waitTillElementIsVisible(totalDataText);
        selenium.enterText(searchPropertyText,id,true);
    }

    /**
     * fill name in search field
     * @param name property name
     */
    public void searchPropertyByName(String name) {
        selenium.waitTillElementIsVisible(totalDataText);
        selenium.enterText(searchPropertyText,name,true);
    }

    /**
     * Click Cari button to search property
     * @throws InterruptedException
     */
    public void clickSearchProperty() throws InterruptedException {
        selenium.clickOn(cariButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * get ID property in row 1
     * @return String ID Property in row 1
     */
    public String getPropertyIDFirstList() throws InterruptedException {
        return selenium.getText(firstPropertyID);
    }

    /**
     * get property name in row 1
     * @return String name property in row 1
     */
    public String getPropertyNameFirstList() throws InterruptedException {
        selenium.waitTillElementIsVisible(firstPropertyName);
        return selenium.getText(firstPropertyName);
    }

    /**
     * click x button to clear search field
     * @throws InterruptedException
     */
    public void clickClearSearch() throws InterruptedException {
        selenium.clickOn(clearSearchButton);
    }

    /**
     * Get value of value attribute in search field
     * @return String value
     */
    public String getSearchTextValue() {
        System.out.println(selenium.getElementAttributeValue(searchPropertyText,"value"));
        return selenium.getElementAttributeValue(searchPropertyText,"value");
    }

    /**
     * check "Data Tidak Ditemukan" text show or not
     * @return boolean, true if found, false if not found
     */
    public boolean emptyPageProperty() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(dataTidakDitemukanText);
        return selenium.isElementDisplayed(dataTidakDitemukanText);
    }

    /**
     * Click action button and choose action in Homepage
     * @param button choose button: Lihat Detail or Ketersediaan Kamar
     * @throws InterruptedException
     */
    public void clickHomeActionButton(String button) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        if (button.equalsIgnoreCase("Lihat Detail")){
            selenium.javascriptClickOn(actionButton);
            selenium.javascriptClickOn(lihatDetailButton);
        } else if (button.equalsIgnoreCase("Ketersediaan Kamar")){
            selenium.javascriptClickOn(actionButton);
            selenium.javascriptClickOn(ketersediaanKamarButton);
        } else {
            System.out.println("button not exist in action");
        }
    }

    public void clickNavbarMenu(Integer index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(mainNavIcons.get(index));
        selenium.javascriptClickOn(mainNavIcons.get(index));
    }

    /**
     * get URL
     * @return String URL
     */
    public String getURL() throws InterruptedException {
        return selenium.getURL();
    }

    // FILTER Start
    /**
     * Click Filter or Reset button
     * @param index of button
     * @throws InterruptedException
     */
    public void clickMainButton(Integer index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(homeButtons.get(index));
        selenium.javascriptClickOn(homeButtons.get(index));
    }

    /**
     * Inputting date to date field
     * @param fieldIndex index of field
     * @param value intended value (date) to input
     * @throws InterruptedException
     */
    public void inputDate(Integer fieldIndex, String value) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(1);
        selenium.javascriptSetValue(inputFields.get(fieldIndex), value);
    }

    /**
     * Selecting dropdown check box
     * @param data input value of dropdown
     * @param fieldName displayed name of dropdown
     * @param dropdownIndex index of dropdown
     * @throws InterruptedException
     */
    public void inputDataDropdown(String data, String fieldName, Integer dropdownIndex, String environment) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(inputFields.get(3));
        selenium.waitTillElementIsVisible(dropdownFields.get(dropdownIndex));
        selenium.javascriptClickOn(dropdownFields.get(dropdownIndex));
        selenium.hardWait(1);
        selenium.waitForJavascriptToLoad();
        if (fieldName.equalsIgnoreCase("propHosp") && environment.equalsIgnoreCase("staging")) {
            WebElement value = driver.findElement(By.xpath("(//p[@class='bg-c-text bg-c-text--body-2 '][contains(text(), '"+data+"')])[2]"));
            selenium.javascriptClickOn((value));
            selenium.pageScrollInView(inputFields.get(4));
        } else if (fieldName.equalsIgnoreCase("propKota")){
            WebElement value = driver.findElement(By.xpath("//a[@class='bg-c-link checkbox-list__no-icon bg-c-link--high-naked'][contains(text(), '" + data + "')]"));
            selenium.javascriptClickOn((value));
        } else {
            WebElement value = driver.findElement(By.xpath("//p[@class='bg-c-text bg-c-text--body-2 '][contains(text(), '" + data + "')]"));
            selenium.javascriptClickOn((value));
        }
        selenium.hardWait(1);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click Terapkan button on Filter section
     */
    public void applyFilter() throws InterruptedException {
        selenium.pageScrollInView(terapkanButton);
        selenium.waitTillElementIsVisible(terapkanButton);
        selenium.javascriptClickOn(terapkanButton);
        selenium.waitForJavascriptToLoad();
    }
    // FILTER End

}
