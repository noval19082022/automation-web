package pageobjects.pmsSinggahsini.OtherTransaction;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class AddOwnerExpenditurePO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public AddOwnerExpenditurePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "[data-testid='otherTransactionSidebarMenu']")
    private WebElement otherTransactionMenu;

    @FindBy(xpath = "//button[contains(text(),'Tambah Data')]")
    private WebElement tambahDataButton;

    @FindBy(xpath = "//*[@class='bg-c-select__trigger bg-c-select__trigger--lg']")
    private List<WebElement> selectDropdown;

    @FindBy(css = "input[placeholder='Pilih properti']")
    private WebElement searchPropertyField;

    @FindBy(xpath = "//*[@class='bg-c-dropdown__menu bg-c-dropdown__menu--open bg-c-dropdown__menu--scrollable bg-c-dropdown__menu--fit-to-trigger bg-c-dropdown__menu--text-lg']//*[@class='bg-c-dropdown__menu-item-content']")
    private List<WebElement> propertyNameSuggestion;

    @FindBy(xpath = "//label[contains(text(),'Total Pengeluaran')]")
    private WebElement totalPengeluaranText;

    @FindBy(css = "input[type='file']")
    private WebElement uploadInput;

    @FindBy(css = "input[data-testid='cost-invoice-number']")
    private WebElement invoiceNumberField;

    @FindBy(css = "[data-testid='add-expenditure']")
    private WebElement saveDataButton;

    @FindBy(css = ".bg-c-modal__footer div button")
    private List<WebElement> addConfirmationButton;

    @FindBy(css = "[data-testid='add-expense']")
    private WebElement tambahPengeluarnButton;

    @FindBy(css = ".bg-c-modal__inner")
    private WebElement confirmationPopUp;

    @FindBy(css = ".bg-c-modal__body-title")
    private WebElement confirmationPopUpTitle;

    @FindBy(css = ".bg-c-modal__body-description")
    private WebElement confirmationPopUpDescription;

    /**
     * click other transaction menu
     * @throws InterruptedException
     */
    public void clickOtherTransactionMenu() throws InterruptedException {
        selenium.waitTillElementIsClickable(otherTransactionMenu);
        selenium.clickOn(otherTransactionMenu);
    }

    /**
     * click tambah data
     * @throws InterruptedException
     */
    public void addNewDataOwnerExpenditure() throws InterruptedException {
        selenium.waitTillElementIsClickable(tambahDataButton);
        selenium.clickOn(tambahDataButton);
    }

    /**
     * select tipe pengajuan Cash Out
     * @param tipe Pembayaran Langsung, Reimbursement, Petty Cash
     * @throws InterruptedException
     */
    public void selectTipePengajuanCashout(String tipe) throws InterruptedException {
        String option = "//*[@class='bg-c-dropdown__menu-item bg-u-radius--md']/div[contains(text(),'"+tipe+"')]";
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(selectDropdown.get(0));
        selenium.javascriptClickOn(By.xpath(option));
    }

    /**
     * search and select property
     * @param name property name keyword
     * @throws InterruptedException
     */
    public void selectProperty(String name) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.click(searchPropertyField);
        selenium.enterText(searchPropertyField,name,true);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(2);
        selenium.javascriptClickOn(propertyNameSuggestion.get(0));
    }

    /**
     * Upload Lampiran with valid attachment
     * @throws InterruptedException
     */
    public void uploadValidImage() throws InterruptedException {
        String projectpath = System.getProperty("user.dir");
        String path = "/src/main/resources/images/stf.jpeg";
        selenium.pageScrollInView(totalPengeluaranText);
        uploadInput.sendKeys(projectpath+path);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Upload lampiran with invalid attachment extension
     * @throws InterruptedException
     */
    public void uploadInvalidImage() throws InterruptedException {
        String projectpath = System.getProperty("user.dir");
        String path = "/src/main/resources/file/massVoucherFile.csv";
        selenium.waitTillElementIsVisible(uploadInput);
        uploadInput.sendKeys(projectpath+path);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Input No. invoice biaya in Owner Expenditure
     * @param inv invoice number
     */
    public void inputInvoiceNumber(String inv) {
        selenium.enterText(invoiceNumberField,inv,true);
    }

    /**
     * select vendor
     * @param vendor vendor keyword
     */
    public void selectTujuanTransfer(String vendor) {
        String locator = "//*[@data-testid='vendor']/child::*//div[contains(text(),'"+vendor+"')]";
        selenium.pageScrollInView(By.xpath(locator));
        selenium.javascriptClickOn(By.xpath(locator));
    }

    /**
     * click Tambah Data to add owner expenditure
     * @throws InterruptedException
     */
    public void clickAddData() throws InterruptedException {
        selenium.clickOn(saveDataButton);
    }

    /**
     * Confirm tambah data owner expenditure
     * @throws InterruptedException
     */
    public void confirmAddOwnerExpenditure() throws InterruptedException {
        selenium.clickOn(addConfirmationButton.get(1));
    }

    /**
     * Cancel add owner expenditure
     * @throws InterruptedException
     */
    public void cancelAddOwnerExpenditure() throws InterruptedException {
        selenium.clickOn(addConfirmationButton.get(0));
    }

    /**
     * Choose kategori pengeluaran
     * @param category category name
     * @param no pengeluran ke-x
     * @throws InterruptedException
     */
    public void setKategoriPengeluaran(String category, String no) throws InterruptedException {
        Integer index = Integer.parseInt(no);
        String option = "(//*[@class='bg-c-dropdown__menu-item bg-u-radius--md']/div[contains(text(),'"+category+"')])["+index+"]";
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(By.xpath(option));
    }

    /**
     * input nama pengeluaran
     * @param name name
     * @param no pengeluaran ke-x
     */
    public void setNamaPengeluaran(String name, String no) {
        Integer index = Integer.parseInt(no);
        String element = "//*[@data-testid='expense-name-"+(index-1)+"']";
        selenium.enterText(By.xpath(element),name,true);
    }

    /**
     * input kuantitas
     * @param quantity kuantitas
     * @param no pengeluaran ke-x
     */
    public void setKuantitas(String quantity, String no) {
        Integer index = Integer.parseInt(no);
        String element = "//*[@data-testid='expense-qty-"+(index-1)+"']";
        selenium.enterText(By.xpath(element),quantity,true);
    }

    /**
     * input nominal pengeluaran
     * @param amount pengeluaran amount
     * @param no pengeluaran ke-x
     */
    public void setNominalPengeluaran(String amount, String no) {
        Integer index = Integer.parseInt(no);
        String element = "(//*[@data-testid='input-currency-masking'])["+index+"]";
        selenium.enterText(By.xpath(element),amount,true);
    }

    /**
     * Select status persediaan
     * @param status Non Stock / Stock
     * @param no pengeluaran ke-x
     * @throws InterruptedException
     */
    public void setStatusPersediaan(String status, String no) throws InterruptedException {
        Integer index = Integer.parseInt(no);
        String option = "//*[@data-testid='expense-stock-status-"+(index-1)+"']/child::*//div[normalize-space()='"+status+"']";
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(By.xpath(option));
    }

    /**
     * input jenis product
     * @param product LSSS,LSAP,SSSP,LMH,PC
     * @param no pengeluaran ke-x
     * @throws InterruptedException
     */
    public void setJenisProduk(String product, String no) throws InterruptedException {
        Integer index = Integer.parseInt(no);
        String option = "//*[@data-testid='expense-product-"+(index-1)+"']/child::*//div[contains(text(),'"+product+"')]";
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(By.xpath(option));
    }

    /**
     * click tambah pengeluaran button
     * @throws InterruptedException
     */
    public void addMorePengeluaran() throws InterruptedException {
        selenium.clickOn(tambahPengeluarnButton);
    }

    /**
     * Check if confirmation pop up when adding owner expenditure appear or not
     * @return true when appear, false when not appear
     * @throws InterruptedException
     */
    public boolean isAddOwnerExpenditurePopUpAppear() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementDisplayed(confirmationPopUp);
    }

    /**
     * get confirmation pop up title
     * @return String title
     */
    public String getConfirmationPopUpTitle() {
        return selenium.getText(confirmationPopUpTitle);
    }

    /**
     * get confirmation pop up description
     * @return String description
     */
    public String getDescriptionConfirmationPopUp() {
        return selenium.getText(confirmationPopUpDescription);
    }

    /**
     * check if tambah data button enable/disable
     * @return true if button disable, false if button enable
     * @throws InterruptedException
     */
    public boolean isTambahDataButtonEnable() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementAtrributePresent(saveDataButton,"disabled");
    }
}
