package pageobjects.pmsSinggahsini.OtherTransaction;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ListOwnerExpenditurePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ListOwnerExpenditurePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "(//*[@class='main-item'])[1]/td")
    private List<WebElement> ownerExpenditureListRow1;

    @FindBy(xpath = "((//*[@class='main-item'])[1]/td)[6]/p")
    private WebElement totalPengeluaranText;

    @FindBy(xpath = "((//*[@class='main-item'])[1]/td)[5]//li")
    private List<WebElement> namaPengeluaranList;

    /**
     * get tipe pengajuan cash out in first row
     * @return String tipe pengajuan cashout
     * @throws InterruptedException
     */
    public String getTipePengajuanCashOut() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(ownerExpenditureListRow1.get(2));
        return selenium.getText(ownerExpenditureListRow1.get(2));
    }

    /**
     * get Nama Property in first row
     * @return String nama property
     * @throws InterruptedException
     */
    public String getNamaProperty() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(ownerExpenditureListRow1.get(3));
        return selenium.getText(ownerExpenditureListRow1.get(3));
    }

    /**
     * get Total pengeluran in first row
     * @return String total pengeluaran
     * @throws InterruptedException
     */
    public String getTotalPengeluaran() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(totalPengeluaranText);
        return selenium.getText(totalPengeluaranText);
    }

    /**
     * get nama pengeluaran in first row
     * @param i index ke-i
     * @return String nama pengeluaran ke-i
     * @throws InterruptedException
     */
    public String getNamaPengeluran(int i) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(namaPengeluaranList.get(i-1));
        return selenium.getText(namaPengeluaranList.get(i-1));
    }
}
