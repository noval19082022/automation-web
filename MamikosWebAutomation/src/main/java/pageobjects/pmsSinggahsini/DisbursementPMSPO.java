package pageobjects.pmsSinggahsini;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.text.ParseException;
import java.util.List;

public class DisbursementPMSPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private JavaHelpers java = new JavaHelpers();

    public DisbursementPMSPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    // Disbursement Note
    private String disbursementNote = "src/test/resources/testdata/pms-singgahsini/disbursement.properties";
    private String riwayatPendapatanURL = JavaHelpers.getPropertyValue(disbursementNote, "riwayatTransferPendapatan_" + Constants.ENV);

    @FindBy(xpath = "//input[@placeholder='Cari Nama Properti']")
    private WebElement propertySearchBar;

    @FindBy(xpath = "//button[contains(text(),'Cari')]")
    private WebElement cariButton;

    @FindBy(xpath = "//table/tbody/tr/td[2]")
    private WebElement firstPropertyName;

    @FindBy(xpath = "//button[@class='table-action-menu__activator-btn']")
    private List<WebElement> actionButton;

    @FindBy(xpath = "//p[contains(., 'Tambahan Pendapatan')]/parent::div/following-sibling::*//button")
    private WebElement actionBtnTmbhPndptn;

    @FindBy(xpath = "//div[@class='bg-c-list-item__description']")
    private List<WebElement> actionMenu;

    @FindBy(xpath = "//p[contains(., 'Tambahan Pendapatan')]/parent::div/following-sibling::*//div[@class='bg-c-list-item__main']")
    private List<WebElement> actionMenuTmbhPndptn;

    @FindBy(xpath = "//p[@class='inline-block bg-c-text bg-c-text--heading-3 ']")
    private WebElement propertyNameHeader;

    @FindBy(xpath = "(//p[text()='Rincian Penjualan Kamar'])[1]")
    private WebElement rincianPenjualanKamarHeaderText;

    @FindBy(xpath = "//p[text()=' Biaya Lainnya ']")
    private WebElement biayaLainnyaHeaderText;

    @FindBy(xpath = "//p[text()='Total Biaya Pengurangan']")
    private WebElement biayaPenguranganOperasionalText;

    @FindBy(xpath = "//button[contains(text(), 'Tambahkan')]")
    private List<WebElement> containsTambahkanButtons;

    @FindBy(xpath = "//input[@class='bg-c-input__field']")
    private List<WebElement> tambahanPendapatanInputFields;

    @FindBy(xpath = "//button[@data-testid='simpan-btn']")
    private WebElement simpanButton;

    @FindBy(xpath = "((//table)[4]//child::tr/td)[1]")
    private WebElement namaTambahanPendapatan;

    @FindBy(xpath = "((//table)[4]//child::tr/td)[2]")
    private WebElement hargaSatuanTambahanPendapatan;

    @FindBy(xpath = "((//table)[4]//child::tr/td)[3]")
    private WebElement kuantitasTambahanPendapatan;

    @FindBy(xpath = "((//table)[4]//child::tr/td)[4]")
    private WebElement totalPendapatanTambahanPendapatan;

    @FindBy(xpath = "//button[text()='Hapus']")
    private WebElement hapusButton;

    @FindBy(xpath = "//*[@class='bg-c-skeleton bg-u-radius--md']")
    private WebElement loadingBar;

    @FindBy(css = ".next")
    private WebElement nextMonthDatePickerButton;

    @FindBy(xpath = "//*[@class='cell month'][contains(text(),'Januari')]")
    private WebElement monthJanuari;

    //----empty state disbursement----//
    @FindBy(xpath = "//*[@class='bg-c-datepicker']")
    private WebElement disbursementCalendar;

    @FindBy(xpath = "//*[@class='date-wrapper']")
    private WebElement calendarDropdown;

    @FindBy(xpath = "//*[@class='cell month selected']//following-sibling::*")
    private WebElement nextMonth;

    @FindBy(xpath = "//p[text()='Data Tidak Ditemukan']")
    private WebElement empStateTitle;

    @FindBy(xpath = "//*[@class='bg-c-text bg-c-text--body-2 ']")
    private WebElement empStateBody;

    @FindBy(xpath = "//button[contains(., 'Ubah')]")
    private WebElement ubahBtn;

    @FindBy(xpath = "//*[@class='bg-c-textarea']/textarea")
    private WebElement txtAreaDetKet;

    @FindBy(xpath = "//button[contains(., 'Simpan')]")
    private WebElement simpanBtn;

    @FindBy(xpath = "//*[@class='bg-c-toast__content'][contains(., 'Detail Keterangan berhasil ditambahkan.')]")
    private WebElement detKetToast;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--body-2 ']/span")
    private WebElement detKetAfterSimpan;

    @FindBy(xpath = "(//*[@class='ss-table']/tbody/tr/td)[3]/div")
    private WebElement statusDataPendapatanLabel;

    @FindBy(xpath = "//div[@class='flex justify-end']/child::*[2]")
    private WebElement confirmApproveDisbursement;

    @FindBy(xpath = "//*[@class='title-right']/button")
    private WebElement refreshDisbursementButton;

    @FindBy(xpath = "//*[@class='bg-c-button ml-16 bg-c-button--secondary bg-c-button--md']")
    private WebElement approveUnapproveButton;

    @FindBy(xpath = "//*[@class='bg-c-field__message']")
    private WebElement errorMsgNote;

    @FindBy(xpath = "//p[contains(., 'Tidak ada keterangan tambahan')]")
    private WebElement ketTambEmpState;

    @FindBy(xpath = "//button[@disabled='disabled'][contains(., 'Simpan')]")
    private WebElement simpanBtnDisable;

    //-------Informasi Pendapatan Transfer Properti--------//
    @FindBy (xpath = "//*[@class='list']//span")
    private WebElement expectMKSbooking;

    @FindBy (xpath = "//div[@class='bg-c-list-item__description']//li")
    private List<WebElement> revenueModelLbl;

    @FindBy (xpath = "//p[contains(., 'Informasi Transfer Pendapatan Properti')]")
    private WebElement informasiPndptnPropSect;
    //-------Informasi Pendapatan Transfer Properti END--------//

    /**
     * search property disbursement with specific name
     * @param propertyName
     * @throws InterruptedException
     */
    public void searchPropertyByName(String propertyName) throws InterruptedException {
        selenium.waitTillElementIsVisible(propertySearchBar);
        selenium.clearTextField(propertySearchBar);
        selenium.enterText(propertySearchBar, propertyName, true);
        selenium.clickOn(cariButton);
    }

    /**
     * check and return property name in row 1
     * @throws InterruptedException
     * @return String name property in row 1
     */
    public String getPropertyNameFirstList() throws InterruptedException {
        selenium.waitTillElementIsVisible(firstPropertyName);
        return selenium.getText(firstPropertyName);
    }

    /**
     * click kebab button
     * @param index
     */
    public void clickKebabBtn(int index){
        selenium.waitTillElementIsVisible(actionButton.get(index));
        selenium.javascriptClickOn(actionButton.get(index));
    }

    /**
     * click kebab button on tambahan pendapatan table
     */
    public void clickKebabBtnTmbhPndptn(){
        selenium.waitTillElementIsVisible(actionBtnTmbhPndptn);
        selenium.javascriptClickOn(actionBtnTmbhPndptn);
    }

    /**
     * click one of the action buttons
     * @param index
     * @throws InterruptedException
     */
    public void clickAction(Integer index) throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(actionMenu.get(index));
    }

    /**
     * click one of the action buttons on tambahan pendapatan table
     * @param index
     * @throws InterruptedException
     */
    public void clickActionTmbhPndptn(int index) throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(actionMenuTmbhPndptn.get(index));
    }

    /**
     * check and return property name in header
     * @throws InterruptedException
     * @return String name property title
     */
    public String getPropertyHeader() throws InterruptedException {
        selenium.waitTillElementIsVisible(propertyNameHeader);
        return selenium.getText(propertyNameHeader);
    }

    /**
     * scroll to Tambahan Pendapatan section and click Tambahkan button
     * @param index
     * @throws InterruptedException
     */
    public void clickTambahkan(Integer index) throws InterruptedException {
        selenium.waitTillElementIsVisible(containsTambahkanButtons.get(index));
        if (index == 0) {
            selenium.pageScrollInView(rincianPenjualanKamarHeaderText);
        } else if (index == 1) {
            selenium.pageScrollInView(biayaLainnyaHeaderText);
        } else if (index == 2) {
            selenium.pageScrollInView(biayaPenguranganOperasionalText);
        }
        selenium.hardWait(3);
        selenium.javascriptClickOn(containsTambahkanButtons.get(index));
    }

    /**
     * create new data in Tambahan Pendapatan
     * @param name
     * @param quantity
     * @param price
     * @throws InterruptedException
     */
    public void createTambahanPendapatan(String name, String quantity, String price) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(tambahanPendapatanInputFields.get(3));

        selenium.clearTextField(tambahanPendapatanInputFields.get(1));
        selenium.enterText(tambahanPendapatanInputFields.get(1), name, true);

        selenium.hardWait(1);
        selenium.clearTextField(tambahanPendapatanInputFields.get(2));
        selenium.enterText(tambahanPendapatanInputFields.get(2), String.valueOf(quantity), true);

        selenium.hardWait(1);
        selenium.clearTextField(tambahanPendapatanInputFields.get(3));
        selenium.enterText(tambahanPendapatanInputFields.get(3), String.valueOf(price), true);

        selenium.javascriptClickOn(simpanButton);
    }

    /**
     * check and return Nama Pendapatan in table Tambahan Pendapatan
     * @return String Nama Pendapatan
     */
    public String getNamaPendapatan() {
        selenium.waitTillElementIsVisible(namaTambahanPendapatan);
        return selenium.getText(namaTambahanPendapatan);
    }

    /**
     * check and return Harga Satuan in table Tambahan Pendapatan
     * @return String Harga Satuan
     */
    public String getHargaSatuan() {
        selenium.waitTillElementIsVisible(hargaSatuanTambahanPendapatan);
        return selenium.getText(hargaSatuanTambahanPendapatan);
    }

    /**
     * check and return Kuantitas in table Tambahan Pendapatan
     * @return String Kuantitas
     */
    public String getKuantitas() {
        selenium.waitTillElementIsVisible(kuantitasTambahanPendapatan);
        return selenium.getText(kuantitasTambahanPendapatan);
    }

    /**
     * check and return Total Pendapatan (individual data) in table Tambahan Pendapatan
     * @return String Total Pendapatan
     */
    public String getTotalPendapatan() {
        selenium.waitTillElementIsVisible(totalPendapatanTambahanPendapatan);
        return selenium.getText(totalPendapatanTambahanPendapatan);
    }

    /**
     * delete an entry on Tambahan Pendapatan
     * @throws InterruptedException
     */
    public void deleteEntry() throws InterruptedException {
        selenium.waitTillElementIsVisible(hapusButton);
        selenium.clickOn(hapusButton);
    }

    /**
     * check if Tambahan Pendapatan has been successfully deleted
     * @return Boolean status pendapatan
     */
    public boolean getStatusTambahanPendapatan(String data) throws InterruptedException {
        try {
            driver.findElement(By.xpath("//td[contains(text(),'"+data+"')]"));
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    //----empty state disbursement----//
    /**
     * click calendar in disbursement menu
     * and click for next month
     * @throws InterruptedException
     */
    public void clickCalendar() throws InterruptedException, ParseException {
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "MMMM", 0, 0, 0, 0);
        System.out.println(today);
        selenium.clickOn(disbursementCalendar);
        if (today.equalsIgnoreCase("December")){
            selenium.waitTillElementIsClickable(calendarDropdown);
            selenium.clickOn(nextMonthDatePickerButton);
            selenium.clickOn(monthJanuari);
        } else {
            selenium.waitTillElementIsClickable(calendarDropdown);
            selenium.clickOn(nextMonth);
        }
    }

    /**
     * get empty state disbursement title
     * @return empty state
     */
    public String getEmptyStateTitle(){
        return selenium.getText(empStateTitle);
    }

    /**
     * get empty state disbursement body
     * @return empty state
     */
    public String getEmptyStateBody(){
        return selenium.getText(empStateBody);
    }

    //-----disbursement note char limit-----//
    /**
     * click Ubah button at Keterangan Tambahan Untuk Owner
     * @throws InterruptedException
     */
    public void clickUbahAtKetTambahan() throws InterruptedException {
        selenium.clickOn(ubahBtn);
    }

    /**
     * click Text Area in detail keterangan
     * @throws InterruptedException
     */
    public void clickTxtAreaDetKet() throws InterruptedException {
        selenium.clickOn(txtAreaDetKet);
    }

    /**
     * enter text into Detail Keterangan <=1500 chars
     * @param note
     */
    public void enterTextDetKet(String note) throws InterruptedException {
        selenium.hardWait(2);
        selenium.enterText(txtAreaDetKet, note, true);
    }

    /**
     * click Simpan button at Keterangan Tambahan
     * @throws InterruptedException
     */
    public void clickSimpanKetTmbhn() throws InterruptedException {
        selenium.clickOn(simpanBtn);
    }

    /**
     * check toast wording
     * @return String detail keterangan toast
     */
    public String getToastDisbursementNote(){
        return selenium.getText(detKetToast);
    }

    /**
     * get detail keterangan after click Simpan
     * @return String Detail Keterangan value
     */
    public String getDetKetAfterSimpan(){
        return selenium.getText(detKetAfterSimpan);
    }

    /**
     * Get Status Data Pendapatan (Unapproved / Approved)
     * @return String Approved / Unapproved
     */
    public String getStatusDataPendapatan() {
        selenium.waitTillElementIsVisible(statusDataPendapatanLabel,10);
        return selenium.getText(statusDataPendapatanLabel);
    }

    /**
     * Confirm pop up Approve / Unapprove Disbursement
     * @throws InterruptedException
     */
    public void confirmApproveDisbursement() throws InterruptedException {
        selenium.waitTillElementIsVisible(confirmApproveDisbursement);
        selenium.clickOn(confirmApproveDisbursement);
    }

    /**
     * Check Button Refresh displayed or not
     * @return boolean
     * True if button display
     * False if button not display
     * @throws InterruptedException
     */
    public Boolean isRefreshButtonExist() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollUsingCoordinate(0,0);
        return selenium.isElementDisplayed(refreshDisbursementButton);
    }

    /**
     * Click Approve / Unapprove button in Detail Disbursement
     * @throws InterruptedException
     */
    public void clickApproveUnapproveDisbursement() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(approveUnapproveButton, 10);
        selenium.pageScrollInView(approveUnapproveButton);
        selenium.clickOn(approveUnapproveButton);
    }
    
     /** enter text into Detail Keterangan > 1500 chars
     * @param note
     */
    public void enterTextDetKetMoreThan(String note) throws InterruptedException {
        selenium.hardWait(2);
        selenium.enterText(txtAreaDetKet, note, false);
        selenium.sendKeyEnter(txtAreaDetKet);
    }

    /**
     * get the error message when user inputted > 1500
     * @return String error message note
     */
    public String getErrorMessageNote(){
        return selenium.getText(errorMsgNote);
    }

    /**
     * delete all words in disbursement note into empty state
     */
    public void deleteAllNote(){
        selenium.clearTextField(txtAreaDetKet);
    }

    /**
     * get the empty state error message
     * @return String empty state error message
     */
    public String getEmpStateErrorMsg(){
        return selenium.getText(errorMsgNote);
    }

    /**
     * get Keterangan Tambahan empty state
     * @return String Keterangan Tambahan empty state
     */
    public String getKetTambEmpState(){
        return selenium.getText(ketTambEmpState);
    }

    /**
     * get Simpan button disable
     * @return String simpan button disable
     */
    public String getSimpanBtnDisable(){
        return selenium.getText(simpanBtnDisable);
    }

    //-------Informasi Pendapatan Transfer Properti--------//
    public void openNewTabRwytTransPndptn(){
        String riwayatTransferPndptnUrl = "";
        riwayatTransferPndptnUrl = riwayatPendapatanURL;
        selenium.openNewTab();
        selenium.switchToWindow(2);
        selenium.navigateToPage(riwayatTransferPndptnUrl);
    }

    /**
     * Click Refresh Halaman ini button
     * @throws InterruptedException
     */
    public void clickRefreshHlmnBtn() throws InterruptedException {
        selenium.waitInCaseElementClickable(refreshDisbursementButton, 10);
        selenium.clickOn(refreshDisbursementButton);
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementVisible(informasiPndptnPropSect, 10);
        selenium.hardWait(3);
    }

    /**
     * Get String Model Kerja Sama (MKS) on Hybrid Booking
     * @return String hybrid Booking
     */
    public String getMKSBooking() {
        String full = selenium.getText(revenueModelLbl.get(0));
        String result = full.substring(9);
        return result;
    }

    /**
     * Get String Model Kerja Sama (MKS) on Hybrid DBET
     * @return String hybrid DBET
     */
    public String getMKSDbet(){
        String full = selenium.getText(revenueModelLbl.get(1));
        String result = full.substring(6);
        return result;
    }

    /**
     * Get String Add On
     * @return String Add On label
     */
    public String getAddOnJP() {
        String full = selenium.getText(revenueModelLbl.get(2));
        return full;
    }

    /**
     * Get String Add On
     * @return String Add On label
     */
    public String getAddOnADP() {
        String full = selenium.getText(revenueModelLbl.get(3));
        return full;
    }

    /**
     * Switch to another window
     * @param window
     */
    public void switchToWindow(int window) {
        selenium.switchToWindow(window);
    }
    //-------Informasi Pendapatan Transfer Properti END--------//
}
