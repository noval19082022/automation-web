package pageobjects.pmsSinggahsini;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class SurveiTrackerPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SurveiTrackerPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//*[@data-testid='surveyTrackerSidebarMenu']")
    private WebElement surveyTrackerMenu;

    @FindBy(xpath = "//*[@class='survey-action__search-type bg-c-dropdown']")
    private WebElement mainPageFilterMenu;

    @FindBy(css = ".survey-action__filter")
    private WebElement filterHomePage;

    @FindBy(xpath = "//span[contains(.,'Pilih status survei')]")
    private WebElement statusSurvei;

    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    private WebElement Terapkan;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-button--primary bg-c-button--lg bg-c-button--block'][contains(.,'Cari')]")
    private WebElement mainPageSearchButton;

    @FindBy(xpath = "//tbody[1]/tr[1]//div[@class='table-body__label bg-c-label bg-c-label--rainbow bg-c-label--rainbow-red']")
    private WebElement resultTidakAdaKonfirm;

    /**
     * click Survey Tracker menu
     * @throws InterruptedException
     */
    public void clickSurveyTrackerMenu() throws InterruptedException {
        selenium.clickOn(surveyTrackerMenu);
        selenium.hardWait(6);
    }

    /**
     * click Main Page filter
     * @throws InterruptedException
     */
    public void clickMainPageFilter() throws InterruptedException {
        selenium.clickOn(mainPageFilterMenu);
    }

    /**
     * Select Main Page filter
     *
     * @throws InterruptedException
     */
    public void selectMainPageFilter(String mainPageFilter) throws InterruptedException {
        selenium.javascriptClickOn(By.xpath("//li/a[contains(., '" + mainPageFilter + "')]"));
        selenium.hardWait(3);
    }

    /**
     * get Tenant Name on Main Page Filter
     */
    public String getTenantNameOnMainPageFilter(String tenantName) {
        return selenium.getText(By.xpath("//tbody/tr[1]/td[2]//a[1][contains(.,'"+tenantName+"')]"));
    }

    /**
     * get Property Name on Main Page Filter
     */
    public String getPropertyNameOnMainPageFilter(String propertyName) {
        return selenium.getText(By.xpath("//tbody/tr[1]/td[3]//a[1][contains(.,'"+propertyName+"')]"));
    }

    /**
     * click Filter Homepage
     * @throws InterruptedException
     */
    public void clickFilterHomePage() throws InterruptedException {
        selenium.clickOn(filterHomePage);
    }
    /**
     * click Survey Tracker menu
     * @throws InterruptedException
     */
    public void clickFilterStatus() throws InterruptedException {
        selenium.clickOn(statusSurvei);
    }

    /**
     * Select Filter Status
     *
     * @throws InterruptedException
     */
    public void selectFilterStatus(String filterStatus) throws InterruptedException {
        selenium.javascriptClickOn(By.xpath("//li/a[contains(., '" + filterStatus + "')]"));
        selenium.hardWait(3);
    }
    /**
     * click Terapkan
     * @throws InterruptedException
     */
    public void  clickTerapkanButton() throws InterruptedException {
        selenium.clickOn(Terapkan);
    }

    /**
     * click Search button main page filter
     * @throws InterruptedException
     */
    public void clickSearchButtonMainPageFilter() throws InterruptedException {
        selenium.clickOn(mainPageSearchButton);
        selenium.hardWait(5);
    }

    /**
     * get Text "Filter Result2"
     * @throws InterruptedException
     */
    public String geTidakAdaKonfirm() {
        return selenium.getText(resultTidakAdaKonfirm);
    }
}
