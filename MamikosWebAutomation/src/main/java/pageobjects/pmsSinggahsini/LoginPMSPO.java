package pageobjects.pmsSinggahsini;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class LoginPMSPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public LoginPMSPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@placeholder='Email']")
    private WebElement emailText;

    @FindBy(xpath = "//input[@placeholder='Password']")
    private WebElement passwordText;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loginButton;

    @FindBy(xpath = "//*[@class='mk-header__profile']")
    private WebElement profileButton;

    @FindBy(xpath = "//*[@class='mk-header-user__dropdown-section mk-header-user__logout']")
    private WebElement keluarButton;

    /**
     * set text in email field
     * @param email
     */
    public void setEmail(String email) {
        selenium.enterText(emailText,email,true);
    }

    /**
     * set text in password field
     * @param password
     */
    public void setPassword(String password) {
        selenium.enterText(passwordText,password,true);
    }

    /**
     * click login button
     * @throws InterruptedException
     */
    public void clickLoginButton() throws InterruptedException {
        selenium.clickOn(loginButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * logout user
     */
    public void userLogOut() {
        selenium.javascriptClickOn(profileButton);
        selenium.javascriptClickOn(keluarButton);
    }
}
