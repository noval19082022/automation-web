package pageobjects.pmsSinggahsini;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class AddMemberPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AddMemberPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@placeholder='Cari berdasarkan nama lengkap/email']")
    private WebElement memberEmailText;

    @FindBy(xpath = "//button[contains(text(),'Tambah')]")
    private WebElement tambahMemberButton;

    @FindBy(xpath = "//div[@class='bg-c-field__message']")
    private WebElement errorMessageText;

    @FindBy(xpath = "(//*[@class='ss-table']/tbody/tr/td)[1]")
    private WebElement nameText;

    @FindBy(xpath = "(//*[@class='ss-table']/tbody/tr/td)[2]")
    private WebElement emailText;

    @FindBy(xpath = "//*[@class='menu-activator']")
    private WebElement deleteMemberButton;

    @FindBy(xpath = "//*[@class='no-data']")
    private WebElement noDataMemberText;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-button--tertiary bg-c-button--md']")
    private WebElement cancelHapusMemberButton;

    @FindBy(xpath = "//*[@class='bg-c-button ml-16 bg-c-button--primary bg-c-button--md']")
    private WebElement confirmHapusMemberButton;

    /**
     * set member email field
     * @param member
     */
    public void setMemberEmail(String member) {
        selenium.enterText(memberEmailText,member,true);
    }

    /**
     * click Tambah Member Button
     * @throws InterruptedException
     */
    public void clickTambahButton() throws InterruptedException {
        selenium.clickOn(tambahMemberButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * get error meesage in member field
     * @return
     */
    public String getMemberNotFoundMessage() {
        return errorMessageText.getText();
    }

    /**
     * get Member name
     * @return String member name
     */
    public String getMemberName() {
        selenium.waitTillElementIsVisible(deleteMemberButton,20);
        return nameText.getText();

    }

    /**
     * get Member email
     * @return String member email
     */
    public String getMemberEmail() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(emailText);
        return emailText.getText();
    }

    /**
     * click delete member button
     */
    public void deleteMember() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(deleteMemberButton);
    }

    /**
     * click batal button in hapus member confirmation pop up
     * @throws InterruptedException
     */
    public void cancelHapusMember() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(cancelHapusMemberButton);
    }

    /**
     * click hapus button in hapus member confirmation pop up
     * @throws InterruptedException
     */
    public void confirmHapusMember() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(confirmHapusMemberButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * check if there is member name displayed
     * @param name member name
     * @return boolean true if exist, false if not exist
     */
    public boolean memberNotDisplayed(String name) throws InterruptedException {
        boolean exist = true;
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(noDataMemberText,20);
        if (selenium.isElementDisplayed(nameText) && nameText.getText().equalsIgnoreCase(name)){
            exist = true;
        } else {
            exist = false;
        }
        return exist;
    }
}
