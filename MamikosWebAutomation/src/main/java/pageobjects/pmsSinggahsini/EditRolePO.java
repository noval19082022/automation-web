package pageobjects.pmsSinggahsini;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class EditRolePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public EditRolePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@placeholder='Masukkan nama role']")
    private WebElement roleNameText;

    @FindBy(xpath = "//*[@class='checkbox bg-c-checkbox bg-c-checkbox--checked']/label/p")
    private List<WebElement> permissionNameText;

    @FindBy(xpath = "//*[@class='checkbox bg-c-checkbox bg-c-checkbox--checked']")
    private List<WebElement> permissionCheckBox;

    /**
     * Get role name in nama role field
     * @return String role name
     */
    public String getRoleName() {
        return selenium.getElementAttributeValue(roleNameText,"value");
    }

    /**
     * check if permission checkbox is checked or not
     * @param permission
     * @return boolean checked, true if permission checked,false if permission not checked
     */
    public boolean getPermissionCheck(String permission) {
        boolean checked = false;
        for (int i=0;i< permissionNameText.size();i++){
            if (permissionNameText.get(i).getText().equalsIgnoreCase(permission)){
                checked = selenium.isElementDisplayed(permissionCheckBox.get(i));
            }
        }
        return checked;
    }

    /**
     * set role name in nama role field
     * @param roleName
     */
    public void setRoleName(String roleName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.enterText(roleNameText,roleName,true);
    }
}
