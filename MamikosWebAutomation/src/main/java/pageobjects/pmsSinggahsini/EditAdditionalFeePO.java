package pageobjects.pmsSinggahsini;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class EditAdditionalFeePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public EditAdditionalFeePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy (xpath = "//*[@class='bg-c-button bg-c-button--secondary bg-c-button--md']")
    private WebElement tambahBiayaButton;

    @FindBy (xpath = "//*[@class='bg-c-input__field']")
    private List<WebElement> additionalFeeTextField;

    @FindBy (xpath = "//*[@class='bg-c-button bg-c-button--primary bg-c-button--md']")
    private WebElement tambahBiayaButtonPopUp, confirmHapusBiayaTambahanButton;

    @FindBy (xpath = "//tbody /tr")
    private List<WebElement> additionalFeeList;

    @FindBy (xpath = "//tbody /tr /td")
    private List<WebElement> additionalFeeData;

    @FindBy (xpath = "//*[@class='bg-c-list-item__description']")
    private List<WebElement> actionOptionButton;

    public void clickTambahBiaya() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(tambahBiayaButton);
        selenium.clickOn(tambahBiayaButton);
    }

    public void setNamaBiayaTambahan(String feeName) {
        selenium.waitTillElementIsVisible(additionalFeeTextField.get(0));
        selenium.clearTextField(additionalFeeTextField.get(0));
        selenium.enterText(additionalFeeTextField.get(0),feeName,true);
    }

    public void setHarga(String feeAmount) {
        selenium.waitTillElementIsVisible(additionalFeeTextField.get(1));
        selenium.clearTextField(additionalFeeTextField.get(1));
        selenium.enterText(additionalFeeTextField.get(1),feeAmount,true);
    }

    public void clickTambahPopUp() throws InterruptedException {
        selenium.clickOn(tambahBiayaButtonPopUp);
        selenium.waitForJavascriptToLoad();
    }

    public String getLastFeeNameText() throws InterruptedException {
        int index = additionalFeeList.size();
        return selenium.getText(additionalFeeData.get(3*(index-1)));
    }

    public String getLastFeeAmountText() throws InterruptedException {
        int index = additionalFeeList.size();
        return selenium.getText(additionalFeeData.get(3*(index-1)+1));
    }

    public void clickActionButton(String feeName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        int row = additionalFeeList.size();
        for (int i=0; i<row;i++){
            if (additionalFeeData.get(3*(row-1)).getText().equalsIgnoreCase(feeName)){
                selenium.javascriptClickOn(additionalFeeData.get(3*(row-1)+2));
            }
        }
    }

    public void clickUbahBiayaTambahan(String feeName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        int row = additionalFeeList.size();
        for (int i=0; i<row;i++){
            if (additionalFeeData.get(3*(row-1)).getText().equalsIgnoreCase(feeName)){
                selenium.javascriptClickOn(actionOptionButton.get((2*i)));
            }
        }
    }

    public void clickHapusBiayaTambahan(String feeName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        int row = additionalFeeList.size();
        for (int i=0; i<row;i++){
            if (additionalFeeData.get(3*(row-1)).getText().equalsIgnoreCase(feeName)){
                selenium.javascriptClickOn(actionOptionButton.get((2*i)+1));
            }
        }
    }

    public void confirmHapusBiayaTambahan() throws InterruptedException {
        selenium.clickOn(confirmHapusBiayaTambahanButton);
    }

    public boolean isFeeNameExist(String feeName) throws InterruptedException {
        selenium.hardWait(1);
        boolean result = true;
        int row = additionalFeeList.size();
        System.out.println("row "+row);
        for (int i=0; i<row;i++) {
            String name = selenium.getText(additionalFeeData.get(3*(row-1)));
            if (name.equalsIgnoreCase(feeName)){
                result = true;
            } else {
                result = false;
            }
        }
        System.out.println("resultc :"+result);
        return result;
    }
}
