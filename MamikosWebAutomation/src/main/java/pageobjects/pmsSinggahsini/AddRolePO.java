package pageobjects.pmsSinggahsini;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class AddRolePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AddRolePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@placeholder='Masukkan nama role']")
    private WebElement roleNameText;

    @FindBy(xpath = "//*[@class='checkbox bg-c-checkbox']/label/p")
    private List<WebElement> permissionListCheckBox;

    @FindBy(xpath = "//div[@class='back-button']")
    private WebElement backArrowButton;

    @FindBy(xpath = "//button[contains(text(),'Reset')]")
    private WebElement resetPermissionButton;

    @FindBy(xpath = "//button[contains(text(),'Simpan')]")
    private WebElement simpanRoleButton;

    @FindBy(xpath = "//*[@class='bg-c-field__message']")
    private WebElement errorMessasgeText;

    /**
     * set role name
     * @param rolename
     */
    public void setRoleName(String rolename) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.enterText(roleNameText,rolename,true);
    }

    /**
     * check permission according to the name
     * assign permission name in feature file, should be exact name
     * @param permission
     * @throws InterruptedException
     */
    public void setPermissions(List<String> permission) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        for (int x=0;x< permission.size();x++){
            for (int i=0;i<permissionListCheckBox.size();i++){
                String permissionName = permission.get(x);
                String permissionlist = permissionListCheckBox.get(i).getText();
                if (permissionlist.equalsIgnoreCase(permissionName)){
                    selenium.javascriptClickOn(By.xpath("(//*[@class='checkbox bg-c-checkbox']/label/p)["+(i+1)+"]"));
                }
            }
        }
    }

    /**
     * click back button arrow on the top
     */
    public void clickBackButton() throws InterruptedException {
        selenium.javascriptClickOn(backArrowButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * click Reset button to reset permission form
     * @throws InterruptedException
     */
    public void clickResetPermissionButton() throws InterruptedException {
        selenium.pageScrollInView(resetPermissionButton);
        selenium.clickOn(resetPermissionButton);
    }

    /**
     * check if there is checked permissions
     * @return present of checked permission (true/false)
     */
    public boolean noCheckedPermission() throws InterruptedException {
        boolean present = true;
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(backArrowButton);
        if(selenium.isElementPresent(By.xpath("//*[@class='permission-list']//div[@class='checkbox bg-c-checkbox bg-c-checkbox--checked']"))){
            present = false;
        }
        return present;
    }

    /**
     * click Simpan button to save role
     * @throws InterruptedException
     */
    public void clickSimpanButton() throws InterruptedException {
        selenium.pageScrollInView(simpanRoleButton);
        selenium.clickOn(simpanRoleButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * get error message in role name field
     * @return
     */
    public String getErrorMessage() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(backArrowButton);
        return errorMessasgeText.getText();
    }
}
