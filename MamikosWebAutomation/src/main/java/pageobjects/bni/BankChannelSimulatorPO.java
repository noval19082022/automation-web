package pageobjects.bni;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BankChannelSimulatorPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public BankChannelSimulatorPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "paymentsimulatorform-virtual_account")
    private WebElement billingNumberTextBox;

    @FindBy(id = "name")
    private WebElement nameLabel;

    @FindBy(id = "searchva")
    private WebElement searchVirtualAccountButton;

    @FindBy(id = "billing_amount")
    private WebElement paymentAmountLabel;

    @FindBy(id = "paymentsimulatorform-total_paid")
    private WebElement paymentAmountTextBox;

    @FindBy(xpath = "//*[@class='btn btn-success flag-button']")
    private WebElement paymentButton;

    @FindBy(xpath = "//*[@class='note note-success']/p")
    private WebElement bniPaymentMessage;

    @FindBy(xpath = "//*[contains(text(),'Saya Sudah Bayar')]")
    private WebElement sayaSudahBayarButton;

    @FindBy(xpath = "//button[normalize-space()='Sudah Bayar']")
    private WebElement sudahButton;

    @FindBy(css = ".bg-c-text.bg-c-text--heading-1")
    private WebElement sudahBayarMessage;

    @FindBy(xpath = "(//*[@class='invoice-text'])[1]")
    private WebElement invoiceNumberText;

    @FindBy(xpath = "(//*[@class='bg-c-text bg-c-text--body-2 '])[1]")
    private WebElement invoiceNumberMamiAdsText;

    @FindBy(xpath = "(//p[@class='bg-c-text bg-c-text--body-2 '])[1]")
    private WebElement invoiceNumberSuccessText;

    /**
     * Enter virtual account number bni
     *@param virtualAccountNumber is numeric generate by system
     */
    public void enterTextVirtualAccount(String virtualAccountNumber) {
        selenium.enterText(billingNumberTextBox, virtualAccountNumber, false);
    }

    /**
     * Click button for search data by virtual account number
     *@throws InterruptedException
     */
    public void clickOnSearchVirtualAccount() throws InterruptedException {
        selenium.clickOn(searchVirtualAccountButton);
    }

    /**
     * Get total amount
     *@return  total amount
     */
    public String getTotalAmountLabel() {
        return selenium.getText(paymentAmountLabel);
    }

    /**
     * Enter payment amount to text box
     *@param   paymentAmount is total amount
     */
    public void enterTextPaymentAmount(String paymentAmount) {
        selenium.enterText(paymentAmountTextBox, paymentAmount, false);
    }

    /**
     * Click payment button
     *@throws InterruptedException
     */
    public void clickOnPaymentButton() throws InterruptedException {
        selenium.clickOn(paymentButton);
    }

    /**
     * Get tenant name on payment page
     *@return tenant name
     */
    public String getName() {
        return selenium.getText(nameLabel);
    }

    /**
     * Get message payment confirmation
     *@return message confirmation
     */
    public String getPaymentMessage() {
        return selenium.getText(bniPaymentMessage);
    }

    /**
     * click btn Saya Sudah Bayar
     */
    public void clickSayaSudahBayar() throws InterruptedException{
        selenium.hardWait(10);
        selenium.pageScrollInView(sayaSudahBayarButton);
        if (selenium.waitInCaseElementVisible(sayaSudahBayarButton, 10)!=null) {
            selenium.clickOn(sayaSudahBayarButton);
        }
    }

    /**
     * confirm Saya Sudah Bayar
     */
    public void clickToConfirm() throws InterruptedException{
            selenium.hardWait(3);
            selenium.clickOn(sudahButton);
           // selenium.refreshPage();
        }

    /**
     * Get message payment confirmation
     *@return message confirmation saya sudah bayar
     */
    public String getMessage() {
        selenium.waitInCaseElementVisible(sudahBayarMessage,10);
        return selenium.getText(sudahBayarMessage);
    }

    /**
     * Get no Invoice
     *@return nomor Invoice
     */
    public String getInvoiceNumber() throws InterruptedException{
        return selenium.getText(invoiceNumberText);
    }

    /**
     * Get no Invoice mami ads
     *@return nomor Invoice
     */
    public String getInvoiceNumberMamiAds() throws InterruptedException{
        String invoiceNumber = "";
        if (selenium.isElementDisplayed(invoiceNumberText)) {
            invoiceNumber = selenium.getText(invoiceNumberText);
        } else if (selenium.isElementDisplayed(invoiceNumberSuccessText)) {
            invoiceNumber = selenium.getText(invoiceNumberSuccessText);
        }
        return invoiceNumber;
    }


}
