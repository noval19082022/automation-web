package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.List;

public class InvoicePO {
    JavaHelpers java;
    WebDriver driver;
    SeleniumHelpers selenium;

    public InvoicePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@id='invoice-year']/option")
    private List<WebElement> yearsList;

    @FindBy(xpath = "//*[@class='invoice-item']")
    private List<WebElement> invoiceListItem;

    @FindBy(xpath = "//*[@class='invoice-list']")
    private List<WebElement> invoiceList;

    @FindBy(xpath = "//*[@class='mamikos-icon icon-arrow-right-long']")
    private WebElement backToPrevPageIcon;

    @FindBy(id = "invoice-year")
    private WebElement yearsDropdownList;

    @FindBy(xpath = "//*[@class='name']")
    private WebElement tenantName;

    @FindBy(xpath = "(//p[@class='link-indicator'])")
    private WebElement seeDetailTenantButton;

    @FindBy(xpath = "//div[@class='change-price']//div[contains(text(),'Ubah Biaya Lain')]")
    private WebElement updateAdditionalCostHyperLink;

    @FindBy(xpath = "//h4[@class='other-costs__activator link-action']//span[text()='Tambah Biaya Tetap']")
    private WebElement addFixCost;

    @FindBy(xpath = "(//input[@data-vv-as='Nama Biaya'])[1]")
    private WebElement costNameEditText;

    @FindBy(id = "Jumlah")
    private WebElement costPrice;

    @FindBy(xpath = "(//h4[@class='link-action btn-save'])[1]")
    private WebElement saveFixCostButton;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-block']")
    private WebElement saveInvoiceButton;

    @FindBy(xpath = "(//div[@class='flex price']/div)[1]")
    private WebElement amountOfTheBillLabel;

    @FindBy(xpath = "(//div[@class='action__right'])[1]")
    private WebElement actionOptions;

    @FindBy(xpath = "//li[@class='list__tile red--text']")
    private WebElement deleteOption;

    /**
     * Get list of years
     * @throws InterruptedException
     * @return numberOfElements is number of invoice list
     */
    public int getNumberOfYears() throws InterruptedException {
        selenium.hardWait(2);
        return yearsList.size();
    }

    /**
     * Seklect year to display invoices according to the year
     */
    public void selectYears(int index) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//*[@id='invoice-year']/option[" + index + "]"));
        selenium.clickOn(yearsDropdownList);
        selenium.clickOn(element);
    }

    /**
     * Get list of invoice
     * @throws InterruptedException
     * @return numberOfElements is number of invoice list
     */
    public int getNumberOfList() throws InterruptedException {
        selenium.hardWait(2);
        return invoiceListItem.size();
    }

    /**
     * Get left invoice status
     * @return text invoice status
     */
    public String getLeftInvoiceStatus(int index) throws InterruptedException {
        if (index == 1){
            selenium.hardWait(3);
        }
        By element = By.xpath("(//*[@class='invoice-item'])[" + index + "]/div[1]/div[1]");
        selenium.waitInCaseElementVisible(By.xpath("//*[@class='invoice-item']"), 15);
        selenium.waitTillElementIsVisible(element, 15);
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Verify right invoice status is present
     * @return boolean
     */
    public boolean rightStatusIsPresent(int index) throws InterruptedException {
        By element = By.xpath("(//*[@class='invoice-item'])[" + index + "]/div[1]/div[2]");
        return selenium.isElementPresent(element);
    }

    /**
     * Get right invoice status
     * @return text invoice status
     */
    public String getRightInvoiceStatus(int index) {
        return selenium.getText(By.xpath("(//*[@class='invoice-item'])[" + index + "]/div[1]/div[2]")).trim();
    }

    /**
     * Click element back to previous page icon
     * @throws InterruptedException
     */
    public void clickOnBackToPrevPageIcon() throws InterruptedException {
        selenium.clickOn(backToPrevPageIcon);
    }

    /**
     * Find the appropriate payment status
     * @param expectedInvoiceStatus is expected invoice status
     * @throws InterruptedException
     */
    public boolean findInvoice(String expectedInvoiceStatus) throws InterruptedException {
        boolean foundData = false;
        int numberOfYears = this.getNumberOfYears();
        outer:
        for (int j = 1; j <= numberOfYears; j++) {
            if (j > 1) {
                this.selectYears(j);
            }
            int numberOfInvoice = this.getNumberOfList();
            for (int k = 1; k <= numberOfInvoice; k++) {
                if (expectedInvoiceStatus.equals("Dibayar di Luar MamiPAY")) {
                    if (this.rightStatusIsPresent(k) && this.getRightInvoiceStatus(k).equals(expectedInvoiceStatus)) {
                        foundData = true;
                        break outer;
                    }
                } else {
                    if (this.getLeftInvoiceStatus(k).equals(expectedInvoiceStatus)){
                        foundData = true;
                        break outer;
                    }
                }
            }
        }
        this.clickOnBackToPrevPageIcon();
        return foundData;
    }

    /**
     * Get detail contract hyperlink
     * @throws InterruptedException
     */
    public String getDetailContractHyperlink() throws InterruptedException {
        selenium.hardWait(2);
        selenium.switchToWindow(2);
        selenium.waitForJavascriptToLoad();
        String url = selenium.getURL();
        selenium.closeTabWindowBrowser();
        return url;
    }

    /**
     * Click on detailTenantButton
     * @throws InterruptedException
     */
    public void clickOnSeeDetailTenantButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(seeDetailTenantButton);
        selenium.clickOn(seeDetailTenantButton);
    }

    /**
     * Click on update additional cost
     * @throws InterruptedException
     */
    public void clickOnUpdateAdditionalCost() throws InterruptedException {
        selenium.clickOn(updateAdditionalCostHyperLink);
    }

    /**
     * Add additional cost
     * @throws InterruptedException
     */
    public void addAdditionalCost(String costName, int price) throws InterruptedException {
        selenium.clickOn(addFixCost);
        selenium.enterText(costNameEditText, costName, true);
        selenium.enterText(costPrice, Integer.toString(price), true);
        selenium.clickOn(saveFixCostButton);
    }

    /**
     * Click on save button to save all cost changes
     * @throws InterruptedException
     */
    public void clickOnSaveAdditionalCostButton() throws InterruptedException {
        selenium.clickOn(saveInvoiceButton);
    }

    /**
     * Get the amount of the bill
     */
    public int getAmountOfTheBill() {
        return java.extractNumber(selenium.getText(amountOfTheBillLabel));
    }

    /**
     * Delete additional fixed cost
     */
    public void deleteAdditionalFixedCost() throws InterruptedException {
        selenium.clickOn(actionOptions);
        selenium.clickOn(deleteOption);
    }
}
