package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class SearchTenantPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SearchTenantPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='el-center flex-col']/h3")
    private WebElement tenantListIsBlankLabel;

    @FindBy(xpath = "//*[@class='input-control']")
    private WebElement searchTextBox2;

    @FindBy(xpath = "//*[@class='input__placeholder']")
    private WebElement searchTextBox1;

    @FindBy(xpath = "(//*[@alt='Icon search'])[1]")
    private WebElement searchButton;

    @FindBy(xpath = "(//*[@class='card-list__body'])[1]")
    private WebElement firstContractList;

    @FindBy(xpath = "//*[@class='card-list__body']")
    private List<WebElement> tenantList;

    @FindBy(xpath = "//*[@class='title']")
    private WebElement tenantNameLabel;

    @FindBy(xpath = "//*[text()='Email']/parent::div/following-sibling::div/a")
    private WebElement tenantEmailLabel;

    @FindBy(xpath = "//*[@class='mamikos-icon icon-arrow-right-long']")
    private WebElement backIcon;

    /**
     * Click on search text box
     * @throws InterruptedException
     */
    public void clickOnSearchBox() throws InterruptedException {
        selenium.waitTillElementIsClickable(searchTextBox1);
        selenium.hardWait(7);
        selenium.clickOn(searchTextBox1);
    }

    /**
     *
     * Type on search text box and click search icon
     * @param keyword is text want to search
     * @throws InterruptedException
     */
    public void setTextOnSearchTextBox(String keyword) throws InterruptedException {
        selenium.enterText(searchTextBox2, keyword, true);
        selenium.clickOn(searchButton);
    }

    /**
     * Get number of list tenant if tenant list not blank
     * @throws InterruptedException
     */
    public int getNumberOfList() throws InterruptedException {
        selenium.hardWait(2);
        int numberOfElements = 0;
        if(selenium.isElementPresent(By.xpath("//*[@class='card-list__body']"))){
            numberOfElements = tenantList.size();
        }else{
            selenium.waitTillElementIsVisible(tenantListIsBlankLabel, 5);
        }
        return numberOfElements;
    }

    /**
     * Click on specific tenant by index
     * @param index tenant on tenant list
     * @throws InterruptedException
     */
    public void clickOnTenantList(int index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementPresent(By.xpath("(//*[@class='card-list__body'])[" + index + "]"), 5);
        selenium.clickOn(By.xpath("(//*[@class='card-list__body'])[" + index + "]"));
    }

    /**
     * Get tenant name from tenant details
     */
    public String getTenantName() {
        return selenium.getText(tenantNameLabel);
    }

    /**
     * Get tenant email from tenant details
     */
    public String getTenantEmail() {
        return selenium.getText(tenantEmailLabel);
    }

    /**
     * Click on back icon
     */
    public void clickOnBackIcon() throws InterruptedException {
        selenium.clickOn(backIcon);
    }

    /**
     * Verify page tenant list is blank display on screen
     */
    public void tenantNotFoundIsPresent() {
        selenium.waitTillElementIsVisible(tenantListIsBlankLabel, 5);
    }
}
