package pageobjects.consultanttools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class PropertyDetailPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public PropertyDetailPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */



    //
    @FindBy(xpath = "//*[@class='text-muted']")
    private WebElement propertyIDLabel;

    @FindBy(xpath = "//*[@class='title']")
    private WebElement propertyTitleLabel;

    @FindBy(xpath = "//*[text()='Tipe Kos']/parent::div/following-sibling::div/p")
    private WebElement propertyTypeLabel;

    @FindBy(xpath = "//*[text()='Total Kamar']/parent::div/following-sibling::div/p")
    private WebElement totalRoomLabel;

    @FindBy(xpath = "//*[text()='Kamar Terisi']/parent::div/following-sibling::div/p")
    private WebElement roomFilledLabel;

    @FindBy(xpath = "//*[text()='Kamar Kosong']/parent::div/following-sibling::div/p")
    private WebElement emptyRoomLabel;

    @FindBy(xpath = "//*[text()='Nama Pemilik']/parent::div/following-sibling::div/p")
    private WebElement ownerNameLabel;

    @FindBy(xpath = "//*[text()='Nomor Pemilik']/parent::div/following-sibling::div")
    private WebElement ownerNumberLabel;

    @FindBy(xpath = "//*[text()='Nama Pengelola']/parent::div/following-sibling::div/p")
    private WebElement managerNameLabel;

    @FindBy(xpath = "//*[text()='Nomor Pengelola']/parent::div/following-sibling::div/p")
    private WebElement managerNumberLabel;

    @FindBy(xpath = "//*[text()='Area']/parent::div/following-sibling::div/p")
    private WebElement areaLabel;

    @FindBy(xpath = "//*[text()='Alamat']/parent::div/following-sibling::div/p")
    private WebElement addressLabel;

    @FindBy(xpath = "//*[text()='Harga Bulanan']/parent::div/following-sibling::div/p")
    private WebElement monthlyPriceLabel;

    @FindBy(xpath = "//*[text()='Harga Harian']/parent::div/following-sibling::div/p")
    private WebElement dailyPriceLabel;

    @FindBy(xpath = "//*[text()='Harga Mingguan']/parent::div/following-sibling::div/p")
    private WebElement weeklyPriceLabel;

    @FindBy(xpath = "//*[text()='Harga 3 Bulanan']/parent::div/following-sibling::div/p")
    private WebElement threeMonthlyPriceLabel;

    @FindBy(xpath = "//*[text()='Harga 6 Bulanan']/parent::div/following-sibling::div/p")
    private WebElement sixMonthlyPriceLabel;

    @FindBy(xpath = "//*[text()='Harga Tahunan']/parent::div/following-sibling::div/p")
    private WebElement annualPriceLabel;

    @FindBy(xpath = "//*[@class='detail-card-header']/div/a")
    private WebElement propertyHyperlink;

    @FindBy(xpath = "//button[contains(text(),'Ubah Detail Kos')]")
    private WebElement updateKosDetailButton;

    @FindBy(xpath = "//button[contains(.,'Update Ketersediaan Kamar')]")
    private WebElement updateRoom;

    /**
     * Get property ID
     * @return property ID
     */
    public String getPropertyID() {
        return selenium.getText(propertyIDLabel);
    }

    /**
     * Get property name
     * @return property name
     */
    public String getPropertyTitle() {
        return selenium.getText(propertyTitleLabel);
    }

    /**
     * Get property type
     * @return property type
     */
    public String getPropertyType() {
        return selenium.getText(propertyTypeLabel).replace("\n", " ");
    }

    /**
     * Get total room
     * @return total room
     */
    public String getTotalRoom() {
        return selenium.getText(totalRoomLabel);
    }

    /**
     * Get room filled
     * @return room filled
     */
    public String getRoomFilled() {
        return selenium.getText(roomFilledLabel);
    }

    /**
     * Get empty room
     * @return empty room
     */
    public String getEmptyRoom() {
        return selenium.getText(emptyRoomLabel);
    }

    /**
     * Get owner name
     * @return owner name
     */
    public String getOwnerName() {
        return selenium.getText(ownerNameLabel);
    }

    /**
     * Get owner number
     * @return owner number
     */
    public String getOwnerNumber() {
        return selenium.getText(ownerNumberLabel);
    }

    /**
     * Get manager name
     * @return manager name
     */
    public String getManagerName() {
        return selenium.getText(managerNameLabel);
    }

    /**
     * Get manager number
     * @return manager number
     */
    public String getManagerNumber() {
        return selenium.getText(managerNumberLabel);
    }

    /**
     * Get property area
     * @return property area
     */
    public String getArea() {
        return selenium.getText(areaLabel);
    }

    /**
     * Get property address
     * @return property address
     */
    public String getAddress() {
        return selenium.getText(addressLabel);
    }

    /**
     * Get monthly price
     * @return monthly price
     */
    public String getMonthlyPrice() {
        return selenium.getText(monthlyPriceLabel);
    }

    /**
     * Get daily price
     * @return daily price
     */
    public String getDailyPrice() {
        return selenium.getText(dailyPriceLabel);
    }

    /**
     * Get weekly price
     * @return weekly price
     */
    public String getWeeklyPrice() {
        return selenium.getText(weeklyPriceLabel);
    }

    /**
     * Get 3 monthly price
     * @return 3 monthly price
     */
    public String getThreeMonthlyPrice() {
        return selenium.getText(threeMonthlyPriceLabel);
    }

    /**
     * Get 6 monthly price
     * @return 6 monthly price
     */
    public String getSixMonthlyPrice() {
        return selenium.getText(sixMonthlyPriceLabel);
    }

    /**
     * Get annual price
     * @return string annual price
     */
    public String getAnnualPrice() {
        return selenium.getText(annualPriceLabel);
    }

    /**
     * Click on hyperlink property
     * @throws InterruptedException
     */
    public void clickOnPropertyHyperlink() throws InterruptedException {
        selenium.clickOn(propertyHyperlink);
    }

    /**
     * Click on update kos detail button
     * @throws InterruptedException
     */
    public void clickOnUpdateKosDetail() throws InterruptedException {
        selenium.clickOn(updateKosDetailButton);
    }

    /**
     * Get detail contract hyperlink
     * @throws InterruptedException
     */
    public String getDetailPropertyHyperlink() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.hardWait(4);
        String url = selenium.getURL();
        selenium.closeTabWindowBrowser();
        return url;
    }

    public void clickOnUpdateRoom() throws InterruptedException {
        selenium.clickOn(updateRoom);
    }
}
