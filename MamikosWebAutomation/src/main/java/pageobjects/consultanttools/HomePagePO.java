package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class HomePagePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public HomePagePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(xpath = "//*[@class='navbar-title']")
    private WebElement titleLabel;

    @FindBy(xpath = "//*[@class='mamikos-icon icon-burger-menu']")
    private WebElement burgerMenuButton;

    @FindBy(xpath = "//button[text() = 'Nanti Saja']")
    private WebElement laterButton;

    /**
     * Get label from Home Page Consultant
     * @return text
     */
    public String getLabel() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(titleLabel);
    }

    /**
     * Click burger menu
     * @throws InterruptedException
     */
    public void clickOnBurgerMenu() throws InterruptedException {
        selenium.waitTillElementIsVisible(burgerMenuButton);
        selenium.hardWait(2);
        selenium.clickOn(burgerMenuButton);
    }

    /**
     * Click list menu
     * @param menu want to click
     * @throws InterruptedException
     */
    public void clickOnMenu(String menu) throws InterruptedException {
        selenium.clickOn(By.xpath("//span[contains(text(),'" + menu + "')]"));
    }

    /**
     * Verify menus is present
     * @param index specific index menu
     */
    public String menuIsPresent(int index) {
        return selenium.getText(By.xpath("//li[" + index + "]/div/span[@class='menu__title']"));
    }

    /**
     * Verify sub menu is present
     * @param menu is parent or menu
     * @param index specific index menu
     */
    public String subMenuIsPresent(String menu, int index) {
        return selenium.getText(By.xpath("(//span[contains(text(),'" + menu + "')]/parent::div/following-sibling::ul//span)[" + index + "]"));
    }


    /**
     * Click on later button
     * @throws InterruptedException
     */
    public void clickOnLaterButton() throws InterruptedException {
        if (selenium.isElementDisplayed(laterButton)){
            selenium.clickOn(laterButton);
        }
    }
}
