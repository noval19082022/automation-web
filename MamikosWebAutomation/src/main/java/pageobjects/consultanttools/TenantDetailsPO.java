package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

public class TenantDetailsPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    JavaHelpers java;

    public TenantDetailsPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(xpath = "//*[@class='btn btn-primary btn-block']")
    private WebElement updateTenantDetailsButton;

    @FindBy(xpath = "//input[@placeholder='Email Penyewa']")
    private WebElement emailEditText;

    @FindBy(xpath = "//input[@placeholder='Nama Lengkap']")
    private WebElement nameEditText;

    @FindBy(xpath = "//div[text()='Okupasi']/parent::div//div[@class='form-control dropdown-toggle']")
    private WebElement occupationDropdownList;

    @FindBy(xpath = "//div[text()='Hitungan Sewa']/parent::div//div[@class='form-control dropdown-toggle']")
    private WebElement rentalPeriodDropdownList;

    @FindBy(id = "Harga Sewa")
    private WebElement rentalPriceEditText;

    @FindBy(xpath = "//input[@placeholder='Pilih tanggal jatuh tempo']")
    private WebElement duDateEditText;

    @FindBy(xpath = "//div[text()='Catatan']/following-sibling::div")
    private WebElement notesDropdownList;

    @FindBy(xpath = "//label[contains(text(),'Penjelasan Catatan')]/parent::div/div")
    private WebElement notesTextField;

    @FindBy(xpath = "(//textarea[@placeholder='Tulis catatan di sini'])[1]")
    private WebElement notesEditText;

    @FindBy(xpath = "//button[contains(text(),'Simpan')]")
    private WebElement saveNotesButton;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-block'][contains(text(),'Simpan')]")
    private WebElement saveButton;

    @FindBy(xpath = "//span[text()='Nama']/parent::div/following-sibling::div/p")
    private WebElement nameValueLabel;

    @FindBy(xpath = "//span[text()='Status']/parent::div/following-sibling::div/p")
    private WebElement statusValueLabel;

    @FindBy(xpath = "//span[text()='Kontrak']/parent::div/following-sibling::div/p")
    private WebElement contractValueLabel;

    @FindBy(xpath = "//span[text()='Kos']/parent::div/following-sibling::div/p")
    private WebElement kosNameValueLabel;

    @FindBy(xpath = "//span[text()='Hitungan Sewa']/parent::div/following-sibling::div/p")
    private WebElement rentalPeriodValueLabel;

    @FindBy(xpath = "//span[text()='Harga']/parent::div/following-sibling::div/p")
    private WebElement priceValueLabel;

    @FindBy(xpath = "//span[text()='Jatuh Tempo']/parent::div/following-sibling::div/p")
    private WebElement dueDateValueLabel;

    @FindBy(xpath = "//span[text()='Nama Pemilik']/parent::div/following-sibling::div/p")
    private WebElement ownersNameValueLabel;

    @FindBy(xpath = "//span[text()='Nomor HP Pemilik']/parent::div/following-sibling::div/a")
    private WebElement ownersPhoneNumberValueLabel;

    @FindBy(xpath = "//span[text()='Jenis Kelamin']/parent::div/following-sibling::div/p")
    private WebElement tenantGenderValueLabel;

    @FindBy(xpath = "//span[text()='Nomor HP Penyewa']/parent::div/following-sibling::div/a")
    private WebElement tenantNumberPhoneValueLabel;

    @FindBy(xpath = "//span[text()='Email']/parent::div/following-sibling::div/a")
    private WebElement tenantEmailValueLabel;

    @FindBy(xpath = "//span[text()='Okupasi']/parent::div/following-sibling::div/p")
    private WebElement tenantOccupationValueLabel;

    @FindBy(xpath = "//span[text()='Catatan']/parent::div/following-sibling::div/p")
    private WebElement notesOptionValueLabel;

    @FindBy(xpath = "//span[text()='Catatan']/parent::div/following-sibling::div/p")
    private WebElement notesDetailValueLabel;

    @FindBy(xpath = "//input[@placeholder='Cari Kos']")
    private WebElement kosNameEditText;

    @FindBy(xpath = "//input[@placeholder='Cari property']")
    private WebElement searchKosNameEditText;

    @FindBy(xpath = "//img[@alt='Icon search']")
    private WebElement searchIcon;

    @FindBy(xpath = "(//div[@class='consultant-card-list'])[1]")
    private WebElement firstKosList;

    /**
     * Click on update tenant details button
     */
    public void clickOnUpdateTenantDetailsButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
        selenium.clickOn(updateTenantDetailsButton);
    }

    /**
     * Enter tenant email
     * @param email is tenant email
     */
    public void enterEmail(String email) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(2);
        selenium.enterText(emailEditText, email, true);
    }

    /**
     * Enter tenant name
     * @param name is tenant name
     */
    public void enterName(String name) {
        selenium.enterText(nameEditText, name, true);
    }

    /**
     * Select tenant gender
     * @param gender is tenant gender
     */
    public void selectGender(String gender) throws InterruptedException {
        selenium.clickOn(By.xpath("//div[contains(text(),'Jenis Kelamin')]/parent::div//span[contains(text(),'" + gender + "')]"));
    }

    /**
     * Select tenant occupation
     * @param occupation is tenant occupation
     */
    public void selectOccupation(String occupation) throws InterruptedException {
        selenium.clickOn(occupationDropdownList);
        selenium.hardWait(1);
        WebElement element = driver.findElement(By.xpath("//div[text()='Okupasi']/parent::div//a/span[text()='" + occupation + "']"));
        selenium.JSScrollAndClickOn(element);
    }

    /**
     * Select tenant occupation
     * @param rentalPeriod is tenant rental period
     */
    public void selectRentalPeriod(String rentalPeriod) throws InterruptedException {
        selenium.clickOn(rentalPeriodDropdownList);
        selenium.hardWait(1);
        WebElement element = driver.findElement(By.xpath("//div[text()='Hitungan Sewa']/parent::div//a/span[text()='" + rentalPeriod +"']"));
        selenium.JSScrollAndClickOn(element);
    }

    /**
     * Select tenant occupation
     * @param rentalPrice is tenant occupation
     */
    public void enterRentalPrice(String rentalPrice) {
        selenium.enterText(rentalPriceEditText, rentalPrice, true);
    }

    /**
     * Select due date
     * @param tomorrow is due date
     */
    public void selectDate(String tomorrow) throws InterruptedException {
        selenium.clickOn(duDateEditText);
        selenium.clickOn(By.xpath("//span[text()='" + tomorrow + "']"));
    }

    /**
     * Select notes
     * @param notes is notes option
     */
    public void selectNotes(String notes) throws InterruptedException {
        selenium.clickOn(notesDropdownList);
        selenium.waitForJavascriptToLoad();
        By element = By.xpath("//span[@class='radio-label'][contains(text(),'" + notes + "')]");
        selenium.pageScrollInView(element);
        selenium.clickOn(element);
    }

    /**
     * Enter notes
     * @param notes is notes
     */
    public void enterAndSaveNotes(String notes) throws InterruptedException {
        selenium.clickOn(notesTextField);
        selenium.enterText(notesEditText, notes, true);
        selenium.clickOn(saveNotesButton);
    }

    /**
     * Click on save button
     * @throws InterruptedException
     */
    public void clickSaveButton() throws InterruptedException {
        selenium.pageScrollInView(saveButton);
        selenium.clickOn(saveButton);
    }

    /**
     * Get tenant name
     * @return name
     */
    public String getName() {
        return selenium.getText(nameValueLabel);
    }

    /**
     * Get tenant status
     * @return status
     */
    public String getStatus() {
        return selenium.getText(statusValueLabel);
    }

    /**
     * Get contract status
     * @return contract
     */
    public String getContract() {
        return selenium.getText(contractValueLabel);
    }

    /**
     * Get kos name
     * @return kosName
     */
    public String getKosName() {
        return selenium.getText(kosNameValueLabel);
    }

    /**
     * Get rental period
     * @return rentalPeriod
     */
    public String getRentalPeriod() {
        return selenium.getText(rentalPeriodValueLabel);
    }

    /**
     * Get rental price
     * @return price
     */
    public int getPrice() {
        String price = selenium.getText(priceValueLabel);
        //java.extractNumber(price);
        return java.extractNumber(price);
    }

    /**
     * Get due date
     * @return due date
     */
    public String getDueDate() {
        return selenium.getText(dueDateValueLabel);
    }

    /**
     * Get owner name
     * @return ownerName
     */
    public String ownerName() {
        return selenium.getText(ownersNameValueLabel);
    }

    /**
     * Get owners phone number
     * @return ownersPhoneNumber
     */
    public String getOwnersNumberPhone() {
        return selenium.getText(ownersPhoneNumberValueLabel);
    }

    /**
     * Get tenant gender
     * @return gender
     */
    public String getGender() {
        return selenium.getText(tenantGenderValueLabel);
    }

    /**
     * Get tenant phone number
     * @return tenantPhoneNumber
     */
    public String getTenantsPhoneNUmber() {
        return selenium.getText(tenantNumberPhoneValueLabel);
    }

    /**
     * Get tenant email
     * @return email
     */
    public String getEmail() {
        return selenium.getText(tenantEmailValueLabel);
    }

    /**
     * Get tenant occupation
     * @return occupation
     */
    public String getOccupation() {
        return selenium.getText(tenantOccupationValueLabel);
    }

    /**
     * Get notes
     * @return notes
     */
    public String getNotes() {
        return selenium.getText(notesOptionValueLabel);
    }

    /**
     * Get detail notes
     * @return detailNotes
     */
    public String getNotesDetails() {
        return selenium.getText(notesDetailValueLabel);
    }

    /**
     * Select kos
     * @param kosName is kos name
     * @throws InterruptedException
     */
    public void selectKos(String searchKos, String kosName) throws InterruptedException {
        selenium.clickOn(kosNameEditText);
        selenium.enterText(searchKosNameEditText, searchKos, true);
        selenium.clickOn(searchIcon);
        selenium.hardWait(3);
        selenium.clickOn(By.xpath("//div[@class='consultant-card-list']//p[text()='" + kosName + "']"));
    }
}
