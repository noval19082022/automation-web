package pageobjects.consultanttools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class SalesMotionPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SalesMotionPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='input']")
    private WebElement searchBox;

    @FindBy(xpath = "//*[@class='ct-card__wrapper']")
    private WebElement salesMotionCard;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-block']")
    private WebElement addReport;

    @FindBy(xpath = "//*[@class='navbar-title']")
    private WebElement titlePage;

    @FindBy(xpath = "//*[@placeholder='Masukkan detail data']")
    private WebElement dataAssociateField;

    @FindBy(xpath = "//*[@placeholder='']")
    private WebElement searchData;

    @FindBy(xpath = "//*[@class='ct-card__body space-between']")
    private WebElement dataAssociate;

    @FindBy(xpath = "//*[@class='__radio-label' and contains(text(),'DBET')]")
    private WebElement dbetType;

    @FindBy(xpath = "//*[@class='__radio-label' and contains(text(),'Kontrak')]")
    private WebElement contractType;

    @FindBy(xpath = "//*[@class='ct-radio radio default'][1]")
    private WebElement notInterestedButton;

    @FindBy(xpath = "//*[@class='ct-radio radio default'][2]")
    private WebElement followUpButton;

    @FindBy(xpath = "//*[@class='ct-radio radio default'][3]")
    private WebElement interestedButton;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-block']")
    private WebElement submitReport;

    /**
     * Search sales motion as keyword
     * @param keyword that want to search
     * @throws InterruptedException
     */
    public void searchSalesMotion(String keyword) throws InterruptedException {
        selenium.click(searchBox);
        selenium.enterText(searchBox, keyword, false);
        selenium.sendKeyEnter(searchBox);
        selenium.hardWait(2);
    }

    /**
     * Click on Add Report button
     * @throws InterruptedException
     */
    public void clickAddReport() throws InterruptedException {
        selenium.javascriptClickOn(salesMotionCard);
        selenium.hardWait(3);
        selenium.click(addReport);
        selenium.waitTillElementIsVisible(titlePage);
    }

    /**
     * Click on DBET data type
     * @throws InterruptedException
     */
    public void setDbetType() throws InterruptedException {
        selenium.hardWait(2);
        selenium.click(dbetType);
    }

    /**
     * Click on Contract data type
     * @throws InterruptedException
     */
    public void setContractType() throws InterruptedException {
        selenium.hardWait(2);
        selenium.click(contractType);
    }

    /**
     * Search data associate report as keyword
     * @param keyword that want to search
     * @throws InterruptedException
     */
    public void searchDataAssociate(String keyword) throws InterruptedException {
        selenium.javascriptClickOn(dataAssociateField);
        selenium.javascriptClickOn(searchData);
        selenium.enterText(searchData, keyword, false);
        selenium.sendKeyEnter(searchData);
        selenium.hardWait(2);
        selenium.click(dataAssociate);
    }

    /**
     * Click on submit report of not interested report
     * @throws InterruptedException
     */
    public void submitNotInterestedReport() throws InterruptedException {
        selenium.click(notInterestedButton);
        selenium.pageScrollInView(submitReport);
        selenium.waitTillElementIsVisible(submitReport);
        selenium.javascriptClickOn(submitReport);
        selenium.waitTillElementIsVisible(titlePage);
    }

    /**
     * Click on submit report of follow up report
     * @throws InterruptedException
     */
    public void submitFollowUpReport() throws InterruptedException {
        selenium.click(followUpButton);
        selenium.pageScrollInView(submitReport);
        selenium.waitTillElementIsVisible(submitReport);
        selenium.javascriptClickOn(submitReport);
        selenium.waitTillElementIsVisible(titlePage);
    }

    /**
     * Click on submit report of interested report
     * @throws InterruptedException
     */
    public void submitInterestedReport() throws InterruptedException {
        selenium.click(interestedButton);
        selenium.pageScrollInView(submitReport);
        selenium.waitTillElementIsVisible(submitReport);
        selenium.javascriptClickOn(submitReport);
        selenium.waitTillElementIsVisible(titlePage);
    }

    /**
     * Get title page
     * @return text
     */
    public String getTitlePage() throws InterruptedException {
        return selenium.getText(titlePage);
    }
}
