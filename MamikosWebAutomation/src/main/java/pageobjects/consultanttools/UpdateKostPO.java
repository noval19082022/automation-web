package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class UpdateKostPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public UpdateKostPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "propertyAddress")
    private WebElement fullAddressTextBox;

    @FindBy(id = "propertyName")
    private WebElement propertyNameTextBox;

    @FindBy(xpath = "//*[@class='alert alert-danger']")
    private WebElement dangerAlertMessage;

    @FindBy(xpath = "//*[@class='btn btn-kost  btn-mamiorange'][contains(text(),'Lanjutkan')]")
    private WebElement nextButton;

    @FindBy(xpath = "//*[@class='btn btn-kost  btn-mamiorange'][contains(text(),'Batal')]")
    private WebElement cancelButton;

    @FindBy(xpath = "//*[@placeholder='_X_']")
    private WebElement roomSizeTextBox;

    @FindBy(css = "[placeholder='Jumlah kamar']")
    private WebElement totalRoomTextBox;

    @FindBy(id = "propertyRoomAvailable")
    private WebElement emptyRoomTextBox;

    @FindBy(id = "inputPriceMonthly")
    private WebElement monthlyPriceTextBox;

    @FindBy(id = "inputPriceDaily")
    private WebElement dailyPriceTextBox;

    @FindBy(id = "inputPriceWeekly")
    private WebElement weeklyPriceTextBox;

    @FindBy(id = "inputPriceThreeMonthly")
    private WebElement quarterlyPriceTextBox;

    @FindBy(id = "inputPriceSixMonthly")
    private WebElement sixMonthlyPriceTextBox;

    @FindBy(id = "inputPriceYearly")
    private WebElement annuallyPriceTextBox;

    /**
     * Enter full address
     * @param fullAddress that value want to input
     * @throws InterruptedException
     */
    public void enterFullAddress(String fullAddress) throws InterruptedException {
        selenium.pageScrollInView(fullAddressTextBox);
        selenium.clickOn(fullAddressTextBox);
        if (fullAddress.equals("")){
            selenium.enterText(fullAddressTextBox, fullAddress, true);
            selenium.enterTextUsingJavascriptExecute(fullAddressTextBox, fullAddress, true);
        }else{
            selenium.enterTextUsingJavascriptExecute(fullAddressTextBox, "", true);
            selenium.enterText(fullAddressTextBox, fullAddress, true);
        }
    }

    /**
     * Send keys BackSpace to fullAddressTextBox
     */
    public void sendKeysBackSpace() {
        selenium.sendKeyBackSpace(fullAddressTextBox);
    }

    /**
     * Enter full address
     * @param propertyName that value want to input
     * @throws InterruptedException
     */
    public void enterPropertyName(String propertyName) throws InterruptedException {
        selenium.pageScrollInView(propertyNameTextBox);
        selenium.clickOn(propertyNameTextBox);
        if (propertyName.equals("")){
            selenium.enterText(propertyNameTextBox, propertyName, true);
            selenium.enterTextUsingJavascriptExecute(propertyNameTextBox, propertyName, true);
        }else{
            selenium.enterTextUsingJavascriptExecute(propertyNameTextBox, "", true);
            selenium.enterText(propertyNameTextBox, propertyName, true);
        }
    }

    /**
     * Get error message blank field
     * @return string
     */
    public String getErrorMessage() {
        selenium.pageScrollInView(dangerAlertMessage);
        return selenium.getText(dangerAlertMessage);
    }

    /**
     * Verify button is disable
     */
    public Boolean isNextButtonDisable() {
        return selenium.isElementAtrributePresent(nextButton, "disabled");
    }

    /**
     * Click on cancel button
     * @throws InterruptedException
     */
    public void clickOnCancelButton() throws InterruptedException {
        selenium.pageScrollInView(cancelButton);
        selenium.clickOn(cancelButton);
    }

    /**
     * Click on next button
     */
    public void clickOnNextButton() {
        selenium.javascriptClickOn(nextButton);
    }

    /**
     * Verify message error is present
     * @param dangerAlertMessage is test message
     * @throws InterruptedException
     */
    public Boolean errorMessageIsPresent(String dangerAlertMessage) throws InterruptedException {
        return selenium.isElementPresent(By.xpath("//*[contains(text(),'" + dangerAlertMessage + "')]"));
    }

    /**
     * Enter room size
     * @param roomSize is room size
     */
    public void enterRoomSize(String roomSize) {
        selenium.enterTextUsingJavascriptExecute(roomSizeTextBox, "", true);
        selenium.enterText(roomSizeTextBox, roomSize, true);
    }

    /**
     * Select room size
     * @param roomSize is room size
     * @throws InterruptedException
     */
    public void selectRoomSize(String roomSize) throws InterruptedException {
        By element = By.xpath("//label[text()='" + roomSize + "']");
        selenium.waitTillElementIsClickable(element);
        selenium.pageScrollInView(element);
        selenium.hardWait(2);
        selenium.clickOn(element);
    }

    /**
     * Enter total room
     * @param totalRoom is total room
     */
    public void enterTotalRoom(String totalRoom) {
        selenium.pageScrollInView(totalRoomTextBox);
        selenium.enterTextUsingJavascriptExecute(totalRoomTextBox, "", true);
        selenium.enterText(totalRoomTextBox, totalRoom, true);
    }

    /**
     * Enter number of empty room
     * @param emptyRoom number of empty roon
     * @throws InterruptedException
     */
    public void enterEmptyRoom(String emptyRoom) throws InterruptedException {
        selenium.pageScrollInView(emptyRoomTextBox);
        selenium.enterTextUsingJavascriptExecute(emptyRoomTextBox, "", true);
        selenium.enterTextCharacterByCharacter(emptyRoomTextBox, emptyRoom, true);
    }

    /**
     * Enter monthly price
     * @param monthlyPrice is monthly price
     */
    public void enterMonthlyPrice(String monthlyPrice) {
        selenium.pageScrollInView(monthlyPriceTextBox);
        selenium.enterTextUsingJavascriptExecute(monthlyPriceTextBox, "", true);
        selenium.enterText(monthlyPriceTextBox, monthlyPrice, true);
    }

    /**
     * Enter daily price
     * @param dailyPrice is monthly price
     */
    public void enterDailyPrice(String dailyPrice) {
        selenium.pageScrollInView(dailyPriceTextBox);
        selenium.enterTextUsingJavascriptExecute(dailyPriceTextBox, "", true);
        selenium.enterText(dailyPriceTextBox, dailyPrice, true);
    }

    /**
     * Enter weekly price
     * @param weeklyPrice is monthly price
     */
    public void enterWeeklyPrice(String weeklyPrice) {
        selenium.pageScrollInView(weeklyPriceTextBox);
        selenium.enterTextUsingJavascriptExecute(weeklyPriceTextBox, "", true);
        selenium.enterText(weeklyPriceTextBox, weeklyPrice, true);
    }

    /**
     * Enter quarterly price
     * @param quarterlyPrice is monthly price
     */
    public void enterQuarterlyPrice(String quarterlyPrice) {
        selenium.pageScrollInView(quarterlyPriceTextBox);
        selenium.enterTextUsingJavascriptExecute(quarterlyPriceTextBox, "", true);
        selenium.enterText(quarterlyPriceTextBox, quarterlyPrice, true);
    }

    /**
     * Enter six monthly price
     * @param sixMonthlyPrice is monthly price
     */
    public void enterSixMonthlyPrice(String sixMonthlyPrice) {
        selenium.pageScrollInView(sixMonthlyPriceTextBox);
        selenium.enterTextUsingJavascriptExecute(sixMonthlyPriceTextBox, "", true);
        selenium.enterText(sixMonthlyPriceTextBox, sixMonthlyPrice, true);
    }

    /**
     * Enter annually price
     * @param annuallyPrice is monthly price
     */
    public void enterAnnuallyPrice(String annuallyPrice) {
        selenium.pageScrollInView(annuallyPriceTextBox);
        selenium.enterTextUsingJavascriptExecute(annuallyPriceTextBox, "", true);
        selenium.enterText(annuallyPriceTextBox, annuallyPrice, true);
    }
}
