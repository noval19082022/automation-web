package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ConsultantLoginPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ConsultantLoginPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@type='email']")
    private WebElement emailTextBox;

    @FindBy(xpath = "//*[@type='password']")
    private WebElement passwordTextBox;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-block']")
    private WebElement loginButton;

    @FindBy(xpath = "//*[@class='error-message-login el-center flex-col align-center']/p")
    private WebElement errorMessage;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement tryAgainButton;

    @FindBy(xpath = "//*[@class='phpdebugbar-close-btn']")
    private WebElement closePHPDebuggerIcon;

    /**
     * Enter username and password consultant
     *@param email is email consultant account
     *@param password is password consultant account
     */
    public void enterEmailPassword(String email, String password) {
        selenium.enterText(emailTextBox, email, false);
        selenium.enterText(passwordTextBox, password, false);
    }

    /**
     * Click on button login
     * @throws InterruptedException
     */
    public void clickOnLoginButton() throws InterruptedException {
        selenium.clickOn(loginButton);
    }

    /**
     * Get error message
     */
    public String getErrorMessage() {
        return selenium.getText(errorMessage);
    }

    /**
     * Verify try again button show on screen
     */
    public boolean tryAgainButtonIsPresent() {
        return selenium.isElementDisplayed(tryAgainButton);
    }

    /**
     * Click on try again button
     */
    public void clickOnTryAgainButton() throws InterruptedException {
        selenium.clickOn(tryAgainButton);
    }

    /**
     * Get email from email text box
     */
    public String getEmailTextBox() {
        return selenium.getText(emailTextBox);
    }

    /**
     * Get password from password text box
     */
    public String getPasswordTextBox() {
        return selenium.getText(passwordTextBox);
    }

    /**
     * Verify php debugger is present
     */
    public boolean phpDebuggerPresent() throws InterruptedException {
        return selenium.isElementPresent(By.xpath("//*[@class='phpdebugbar-close-btn']"));
    }

    /**
     * Click on close php debugger icon
     */
    public void clickOnClosePHPDebugger() throws InterruptedException {
        selenium.clickOn(closePHPDebuggerIcon);
    }
}
