package pageobjects.consultanttools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.List;

public class ActiveTenantListPO {
    WebDriver driver;
    JavaHelpers java;
    SeleniumHelpers selenium;

    public ActiveTenantListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//img[@alt='Icon filter']")
    private WebElement filterIcon;

    @FindBy(xpath = "//li[contains(text(),'Penyewa terbaru')]")
    private WebElement newestTenantShort;

    @FindBy(xpath = "//div/img[@alt='Icon close']")
    private WebElement closeShortOptionButton;

    @FindBys(@FindBy(xpath = "//div[@class='card-list__content']/div[@class='card-title']"))
    private List<WebElement> tenantNameList;

    @FindBys(@FindBy(xpath = "//div[@class='card-list__content']/p"))
    private List<WebElement> kosNameList;

    @FindBys(@FindBy(xpath = "//div[@class='card-list__content']//span"))
    private List<WebElement> dueDateList;

    /**
     * Click filter icon
     * @throws InterruptedException
     */
    public void clickOnFilterIcon() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(2);
        selenium.clickOn(filterIcon);
    }

    /**
     * Short newest tenant
     * @throws InterruptedException
     */
    public void clickOnShortNewestTenant() throws InterruptedException {
        selenium.clickOn(newestTenantShort);
    }

    /**
     * Close short option
     * @throws InterruptedException
     */
    public void closeShortOption() throws InterruptedException {
        selenium.clickOn(closeShortOptionButton);
    }

    /**
     * Get tenant name
     * @param index tenant will take from list
     */
    public String getTenantName(int index) {
        return selenium.getText(tenantNameList.get(index)).trim();
    }

    /**
     * Get kos name
     * @param index tenant will take from list
     */
    public String getKosName(int index) {
        return selenium.getText(kosNameList.get(index)).trim();
    }

    /**
     * Get kos name
     * @param index tenant will take from list
     */
    public String getDueDate(int index) {
        return selenium.getText(dueDateList.get(index)).trim();
    }
}
