package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class FilterContractPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public FilterContractPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='contract-filter-button']")
    private WebElement submitFilterButton;

    @FindBy(xpath = "//*[@class='contract-filter-button']")
    private WebElement startDateTextBox;

    @FindBy(xpath = "//*[@class='vdp-datepicker datepicker-right'")
    private WebElement endDateTextBox;

    /**
     * Click on status kontrak cancel
     * @param filterBy string value want to filter
     * @throws InterruptedException
     */
    public void clickOnFilterOption(String filterBy) throws InterruptedException {
        By element;
        if(filterBy.equals("Dibayar")){
            element = By.xpath("(//*[@class='custom-checkbox']//label[contains(text(),'" + filterBy + "')])[2]");
            selenium.waitTillElementIsClickable(element);
            selenium.hardWait(1);
            selenium.clickOn(element);
        }else{
            selenium.hardWait(1);
            selenium.clickOn(By.xpath("//*[@class='custom-checkbox']//label[contains(text(),'" + filterBy + "')]"));
        }
    }

    /**
     * Click on submit filter button
     * @throws InterruptedException
     */
    public void clickOnSubmitFilter() throws InterruptedException {
        selenium.pageScrollInView(submitFilterButton);
        selenium.hardWait(2);
        selenium.clickOn(submitFilterButton);
    }

    /**
     * Select start date filter contract
     * @param date is start date
     * @throws InterruptedException
     */
    public void selectStartDate(String date) throws InterruptedException {
        selenium.pageScrollInView(startDateTextBox);
        selenium.hardWait(10);
        selenium.clickOn(startDateTextBox);
        selenium.clickOn(By.xpath("//div[@class='vdp-datepicker datepicker-right']//span[text()='" + date + "']"));
    }
}