package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ManageContractPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ManageContractPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@alt='Icon filter']")
    private WebElement burgerFilterButton;

    @FindBy(xpath = "//*[@alt='Plus icon']")
    private WebElement addButton;

    @FindBy(xpath = "//*[@class='add-contract-button']//*[@class='btn btn-primary btn-block']")
    private WebElement addContractButton;

    @FindBy(xpath = "//div[@class='contract-list-page']")
    private WebElement listOfContract;

    @FindBy(xpath = "//*[@class='contract-empty']/h3")
    private WebElement propertyIsBlankLabel;

    @FindBy(xpath = "//*[@class='input__indicator']")
    private WebElement searchTextBox;

    @FindBy(xpath = "(//*[@class='contract-list-item'])[1]")
    private WebElement firstContractList;

    @FindBy(xpath = "//*[@class='contract-list-item']")
    private List <WebElement> contractList;

    @FindBy(xpath = "//div[@class='item-container contract-card-container']")
    private WebElement contractListItem;

    @FindBy(xpath = "(//section[@class='contract-action']//div[@class='consultant-btn -large -outline -block']//button[@class='btn btn-warning btn-block'])[1]")
    private WebElement batalkanKontrakButton;

    @FindBy(xpath = "(//div[@class='consultant-btn -large -outline -block']//button[@class='btn btn-warning btn-block'])[2]")
    private WebElement akhiriKontrakButton;

    @FindBy(xpath = "(//button[contains(text(),'Akhiri Kontrak')])[1]")
    private WebElement akhiriKontrakButtonInList;

    @FindBy(xpath = "(//div[@class='consultant-btn -large -block']//button[@class='btn btn-primary btn-block'])[4]")
    private WebElement confirmTerminateContractInList;

    @FindBy(xpath = "(//div[@class='consultant-btn -large -block']//button[@class='btn btn-primary btn-block'])[3]")
    private WebElement confirmCancelContractButton;

    @FindBy(xpath = "//i[@class='mamikos-icon icon-arrow-right-long']")
    private WebElement backArrowButton;

    @FindBy(xpath = "(//div[@class='disabled payment-status']//div)[1]")
    private WebElement firstDibatalkanContractStatus;

    @FindBy(xpath = "(//div[@class='warning payment-status']//div)[1]")
    private WebElement firstBelumDibayarContractStatus;

    @FindBy(xpath = "//div[@class='item-container contract-card-container']//div//div")
    private WebElement firstContractStatus;

    @FindBy(xpath = "(//div[@class='consultant-btn -outline -block']//button[@class='btn btn-warning btn-block'])[1]")
    private WebElement batalkanKontrakListButton;

    @FindBy(xpath = "//*[@class='contract-empty']//h3")
    private WebElement emptyDataLabel;

    @FindBy(xpath = "//*[@class='spinner']")
    private WebElement loadingAnimation;

    /**
     * click on Button Tambah Kontrak Baru
     * @throws InterruptedException
     */
    public void clickOnAddContractButton() throws InterruptedException {
        selenium.waitTillElementIsNotVisible(loadingAnimation);
        selenium.clickOn(addContractButton);
    }

    /**
     * Click on burger button filter
     * @throws InterruptedException
     */
    public void clickOnBurgerFilter() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitInCaseElementPresent(By.xpath("//*[@alt='Icon filter']"), 50);
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(burgerFilterButton);
    }

    /**
     * Click on property list
     * @param index property
     * @throws InterruptedException
     */
    public void clickOnPropertyList(int index) throws InterruptedException {
        By element = By.xpath("(//*[@class='contract-list-item'][" + index + "]//*[@class='item-container contract-card-container'])");
        selenium.waitInCaseElementPresent(By.xpath("(//*[@class='contract-list-item'][" + index + "]//*[@class='item-container contract-card-container'])"), 50);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(2);
        selenium.javascriptClickOn(element);
        selenium.hardWait(5);
    }

    /**
     * Get contract status
     * @throws InterruptedException
     * @return contractStatus
     */
    public String getLeftStatus(int index) throws InterruptedException {
        if (index == 1){
            selenium.hardWait(3);
        }
        By element = By.xpath("//*[@class='contract-list-item'][1]//*[@class='item-container contract-card-container']/div[1]/div[1]");
        selenium.waitInCaseElementPresent(element, 15);
        selenium.pageScrollInView(element);
        selenium.hardWait(2);
        return selenium.getText(element).trim();
    }

    /**
     * Get status cancel
     * @throws InterruptedException
     * @return contractStatus
     */
    public String getRightStatus(int index) throws InterruptedException {
        if (index == 1){
            selenium.hardWait(2);
        }
        By element = By.xpath("(//*[@class='item-container'])[" + index + "]/div[1]/div/span");
        selenium.waitTillElementIsVisible(element, 20);
        return selenium.getText(element).trim();
    }

    /**
     * Verify element right status is present
     * @return WebElement
     */
    public Boolean rightStatusIsPresent(int index) throws InterruptedException {
        By element = By.xpath("(//*[@class='item-container'])[" + index + "]/div[1]/div/span");

        return selenium.isElementPresent(element);
    }

    /**
     * Get list of contract with status cancel
     * @throws InterruptedException
     * @return numberOfElements is number of contract list
     */
    public int getNumberOfList() throws InterruptedException {
        selenium.waitInCaseElementVisible(contractListItem, 10);
        selenium.hardWait(20);
        int numberOfElements = 0;
        if(selenium.waitInCaseElementVisible(firstContractList, 5) != null){
            numberOfElements = contractList.size();
        }else{
            selenium.waitTillElementIsVisible(propertyIsBlankLabel, 5);
        }
        return numberOfElements;
    }

    /**
     * Click on search box filter
     * @throws InterruptedException
     */
    public void clickOnSearchBox() throws InterruptedException {
        selenium.waitTillElementIsVisible(searchTextBox, 30);
        selenium.hardWait(15);
        selenium.clickOn(searchTextBox);
    }

    /**
     * Get property name
     * @return property name
     */
    public String getPropertyNameLabel(int index) throws InterruptedException {
        if (index == 1){
            selenium.hardWait(7);
        }
        By element = By.xpath("(//div[@class='item-container contract-card-container']//div[@class='kost-address'])[" + index + "]");
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    public String getTenantNameLabel(int index) throws InterruptedException {
        if (index == 1){
            selenium.hardWait(5);
        }
        By element = By.xpath("(//div[@class='item-container contract-card-container']//div[@class='tenant-name'])[" + index + "]");
        selenium.waitTillElementIsVisible(element, 10);
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Get created property
     * @return createdBy
     */
    public String getCreatedBy(int index) throws InterruptedException {
        if (index == 1){
            selenium.hardWait(5);
        }
        By element = By.xpath("(//*[@class='created-by']/div/strong)[" + index + "]");
        selenium.waitInCaseElementVisible(contractListItem, 15);
        selenium.waitTillElementIsVisible(element, 35);
        selenium.pageScrollInView(element);
        selenium.hardWait(5);
        return selenium.getText(element);
    }

    /**
     * Verify terminate contract button is present
     * @return createdBy
     */
    public boolean terminateContractButtonIsPresent(int index) throws InterruptedException {
        By element = By.xpath("(//*[@class='contract-list-item'])[" + index + "]//button[contains(text(),'Akhiri Kontrak')]");
        return selenium.isElementPresent(element);
    }

    /**
     * cancel contract in Detail page
     * @throws InterruptedException
     */
    public void cancelContractInDetails() throws InterruptedException {
        selenium.waitTillElementIsVisible(batalkanKontrakButton);
        selenium.pageScrollInView(batalkanKontrakButton);
        selenium.javascriptClickOn(batalkanKontrakButton);
        selenium.waitTillElementIsVisible(confirmCancelContractButton);
        selenium.clickOn(confirmCancelContractButton);
        selenium.hardWait(3);
    }

    /**
     * Cancel contract in list contract
     * @throws InterruptedException
     */
    public void cancelContractInList() throws InterruptedException {
        selenium.waitTillElementIsVisible(batalkanKontrakListButton);
        selenium.pageScrollInView(batalkanKontrakListButton);
        selenium.javascriptClickOn(batalkanKontrakListButton);
        selenium.waitTillElementIsVisible(confirmCancelContractButton);
        selenium.clickOn(confirmCancelContractButton);
        selenium.hardWait(3);
    }

    /**
     * check Batalkan Kontrak button exist or not
     * @return true if Batalkan Kontrak button exist, false if Batalkan Kontrak button not exist
     */
    public boolean isBatalkanKontrakButtonExist() {
        return selenium.isElementDisplayed(batalkanKontrakButton);
    }

    /**
     * check Akhiri Kontrak button exist or not
     * @return true if Akhiri Kontrak button exist, false if Akhiri Kontrak button not exist
     */
    public boolean isAkhiriKontrakButtonExist() {
        return selenium.isElementDisplayed(akhiriKontrakButton);
    }

    /**
     * Click back arrow in detail kontrak
     * @throws InterruptedException
     */
    public void backFromDetailKontrak() throws InterruptedException {
        selenium.javascriptClickOn(backArrowButton);
    }

    /**
     * Get first contract status dibatalkan
     * @return String status kontrak
     */
    public String getFirstDibatalkanContractStatus() {
        selenium.waitTillElementIsVisible(firstDibatalkanContractStatus);
        return selenium.getText(firstDibatalkanContractStatus);
    }

    /**
     * Get first contract status in list contract
     * @return
     */
    public String getFirstContractStatus() throws InterruptedException {
        selenium.hardWait(3);
        if(selenium.isElementDisplayed(firstContractStatus)){
            return selenium.getText(firstContractStatus);
        } else {
            return "no contract in list";
        }
    }

    /**
     * terminate contract in detail contract
     * @throws InterruptedException
     */
    public void terminateContractInDetail() throws InterruptedException {
        selenium.pageScrollInView(akhiriKontrakButton);
        selenium.javascriptClickOn(akhiriKontrakButton);
        selenium.clickOn(confirmCancelContractButton);
        selenium.hardWait(3);
    }

    /**
     * get message in no kontrak found page
     * @return string Belum Ada Kontrak
     */
    public String getEmptyDataLabel() {
        return selenium.getText(emptyDataLabel);
    }

    /**
     * click akhiri kontrak button in contract list
     * @throws InterruptedException
     */
    public void terminateContractInList() throws InterruptedException {
        selenium.waitTillElementIsClickable(akhiriKontrakButtonInList);
        selenium.clickOn(akhiriKontrakButtonInList);
        selenium.clickOn(confirmTerminateContractInList);
        selenium.hardWait(5);
    }

    /**
     * Wait until loading animation disappear
     */
    public void waitLoadingComplete() {
        selenium.waitTillElementIsNotVisible(loadingAnimation);
    }

    /**
     * Verify list of contract is appeared
     */
    public void listOfContractIsAppeared() {
        selenium.isElementDisplayed(listOfContract);
    }
}