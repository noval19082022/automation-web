package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ManageBookingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ManageBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(css = "[placeholder='Cari data booking']")
    private WebElement searchBookingTextBox;

    @FindBys(@FindBy(xpath = "//div[@class='ct-card__body']"))
    private List<WebElement> bookingList;

    @FindBy(xpath = "//span[contains(.,'Butuh Konfirmasi')]")
    private WebElement butuhKonfirmasiTab;

    @FindBy(xpath = "//button[contains(.,'Lihat Detail')][1]")
    private WebElement seeMoreDetailsButton;

    @FindBy(xpath = "//div[@class='inner-container']//button[@class='btn btn-primary btn-block']")
    private WebElement acceptButton;

    @FindBy(xpath = "//div[@class='consultant-btn -large -outline -block']//button[@class='btn btn-primary btn-block']")
    private WebElement seeContractButton;

    @FindBy(xpath = "//div[@class='consultant-snackbar__content container-for-mobile']//button[@class='btn btn-primary btn-block']")
    private WebElement confirmationTerimaButton;

    @FindBy(xpath = "//div[@class='navbar-title']")
    private WebElement titleTerimaBooking;

    @FindBy(xpath = "//section[@class='accept-date section-divider small-padding']//input[@class='input']")
    private WebElement pilihKamarButton;

    @FindBy(xpath = "//div[@class='consultant-menu menu--is-active']//div[@class='tile__title']")
    private WebElement pilihDitempat;

    @FindBy(xpath = "(//*[@class='list__tile'][2]//*[@class='tile__title'])[1]")
    private WebElement firstRoomNumber;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-block']")
    private WebElement simpanKontrakButton;

    @FindBy(css = ".message__item")
    private WebElement fullErrorMessage;

    @FindBy(xpath = "//button[contains(.,'Tolak')]")
    private WebElement tolakButton;

    @FindBy(xpath = "//div[@class='consultant-snackbar__content container-for-mobile']//button[@class='btn btn-secondary btn-block']")
    private WebElement tolakConfirmationButton;

    @FindBy(css = ".btn")
    private WebElement tolakReasonButton;

    @FindBy(xpath = "//div[@class='radio-group__content']/div[1]//span[@class='icon dropdown-toggle bg']")
    private WebElement reason1;

    @FindBy(css = ".chip__content")
    private WebElement bookingStatus;

    @FindBy(css = ".chip-warning")
    private WebElement reasonMessage;

    @FindBy(xpath = "//i[@class='mamikos-icon icon-arrow-right-long']")
    private WebElement backArrowButton;

    @FindBy(xpath = "(//*[@class='detail-info'])[2]")
    private WebElement roomName;

    @FindBy(css = ".textarea__input")
    private WebElement customReasonTextField;

    @FindBy(xpath = "//*[@class='navbar-title']")
    private WebElement pageTitle;

    @FindBy(xpath = "//div[5]//input[@class='input']")
    private WebElement depositTextField;

    @FindBy(xpath = "//div[6]//input[@class='input']")
    private WebElement dendaTextField;

    @FindBy(xpath = "//div[@class='row']/div[2]//input[@class='input']")
    private WebElement feeAmount;

    @FindBy(xpath = "//div[@class='row']/div[3]//input[@class='input']")
    private WebElement feeType;

    @FindBy(xpath = "(//*[@class='menu__content list-container']//*[@class='list__tile']//*[@class='tile__title'])[4]")
    private WebElement hariType;

    @FindBy(xpath = "(//*[@class='menu__content list-container']//*[@class='list__tile']//*[@class='tile__title'])[5]")
    private WebElement mingguType;

    @FindBy(xpath = "(//*[@class='menu__content list-container']//*[@class='list__tile']//*[@class='tile__title'])[6]")
    private WebElement bulanType;

    @FindBy(xpath = "(//*[@class='menu__content list-container']//*[@class='list__tile']//*[@class='tile__title'])[7]")
    private WebElement tahunType;

    @FindBy(xpath = "//div[@class='consultant-btn -outline']/button[@class='btn btn-primary']")
    private WebElement tambahBiayaLainButton;

    @FindBy(xpath = "//form[1]/div[1]//input[@class='input']")
    private WebElement namaBiayaLainTextField;

    @FindBy(xpath = "//form[1]/div[2]//input[@class='input']")
    private WebElement valueBiayaLainTextField;

    @FindBy(xpath = "//form[1]//button[@class='btn btn-primary btn-block']")
    private WebElement tambahBiayaLainPopUpButton;

    @FindBy(xpath = "//div[@class='v-switch-core']")
    private WebElement dpSwitchButton;

    @FindBy(xpath = "//div[@class='ct-input-text ct-input--outlined ct-input--readonly']//input[@class='input']")
    private WebElement dpDropdownButton;

    @FindBy(xpath = "(//*[@class='menu__content list-container']//*[@class='tile__title'])[20]")
    private WebElement sepuluhPersenDP;

    @FindBy(xpath = "(//*[@class='menu__content list-container']//*[@class='tile__title'])[21]")
    private WebElement duapuluhPersenDP;

    @FindBy(xpath = "(//*[@class='menu__content list-container']//*[@class='tile__title'])[22]")
    private WebElement tigapuluhPersenDP;

    @FindBy(xpath = "(//*[@class='fake-table soft']//li)[1]//div[2]")
    private WebElement tagihanDeposit;

    @FindBy(xpath = "(//*[@class='fake-table soft']//li)[2]//div[2]")
    private WebElement tagihanDP;

    @FindBy(xpath = "(//*[@class='fake-table soft']//li)[3]//div[2]")
    private WebElement tagihanBiayaLain;

    @FindBy(xpath = "//button[text()='Terapkan']")
    private WebElement applyButton;

    @FindBy(xpath = "//button[@class='filter-activator']")
    private WebElement filterIcon;

    @FindBys(@FindBy(xpath = "//i[@class='mamikos-icon icon-product']/parent::div/following-sibling::div"))
    private List<WebElement> propertyTypeLabelList;

    /**
     * search booking data
     * @param keyword as tenant no hp or you can change to any search keyword available
     */
    public void searchBookingData(String keyword) throws InterruptedException {
        selenium.waitTillElementIsVisible(searchBookingTextBox);
        selenium.waitForJavascriptToLoad();
        selenium.enterText(searchBookingTextBox,keyword,true);
        selenium.sendKeyEnter(searchBookingTextBox);
    }

    /**
     * to switch booking tab
     * @param tab is the tab name user want to click ("Butuh Konfirmasi","Tunggu Pembayaran", etc)
     * @throws InterruptedException
     */
    public void clickBookingTab(String tab) throws InterruptedException {
        selenium.waitTillElementIsVisible(butuhKonfirmasiTab);
        selenium.pageScrollInView(By.xpath("//div[@class='ct-swiper']//span[contains(.,'"+tab+"')]"));
        selenium.clickOn(By.xpath("//div[@class='ct-swiper']//span[contains(.,'"+tab+"')]"));
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click on Lihat Detail Button
     * @throws InterruptedException
     */
    public void clickLihatDetailButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(seeMoreDetailsButton);
        selenium.pageScrollInView(seeMoreDetailsButton);
        selenium.clickOn(seeMoreDetailsButton);
    }

    /**
     * Accept Booking in Booking Details
     * @throws InterruptedException
     */
    public void acceptBooking() throws InterruptedException {
        selenium.waitTillElementIsVisible(acceptButton);
        selenium.pageScrollInView(acceptButton);
        selenium.hardWait(1);
        selenium.clickOn(acceptButton);
        selenium.clickOn(confirmationTerimaButton);
    }

    /**
     * select Pilih ditempat in room number
     * @throws InterruptedException
     */
    public void selectRoomPilihDitempat() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(titleTerimaBooking);
        selenium.javascriptClickOn(pilihKamarButton);
        selenium.clickOn(pilihDitempat);
    }

    /**
     * select room x in room number
     * @param room is the room that user want to pick
     * @throws InterruptedException
     */
    public void selectRoomNumber(String room) throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(titleTerimaBooking);
        selenium.javascriptClickOn(pilihKamarButton);
        selenium.clickOn(By.xpath("//div[@class='consultant-menu menu--is-active']//div[contains(.,"+room+")]"));
    }

    /**
     * click Simpan Kontrak Button in Booking details
     * @throws InterruptedException
     */
    public void clickSimpanKontrakButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(simpanKontrakButton);
        selenium.pageScrollInView(simpanKontrakButton);
        selenium.javascriptClickOn(simpanKontrakButton);
    }

    /**
     * function to get full error message
     * @return full error message
     */
    public String getRoomFullErrorMessage() {
        return selenium.getText(fullErrorMessage);
    }

    /**
     * click on tolak button and confirm tolak booking
     * @throws InterruptedException
     */
    public void tolakBooking() throws InterruptedException {
        selenium.waitTillElementIsVisible(tolakButton);
        selenium.clickOn(tolakButton);
        selenium.clickOn(tolakConfirmationButton);
    }

    /**
     * Choose radio button based on reason choosen by user
     * @param reason is reject reason user want to pick
     * @throws InterruptedException
     */
    public void chooseReason(String reason) throws InterruptedException {
        selenium.waitTillElementIsVisible(tolakReasonButton);
        if (reason.equalsIgnoreCase("Kos penuh")){
            selenium.clickOn(By.xpath("//div[@class='radio-group__content']/div[1]//span[@class='icon dropdown-toggle bg']"));
            selenium.clickOn(tolakReasonButton);
        }else if (reason.equalsIgnoreCase("Kamar ini sudah dipesan")){
            selenium.clickOn(By.xpath("//div[@class='radio-group__content']/div[2]//span[@class='icon dropdown-toggle bg']"));
            selenium.clickOn(tolakReasonButton);
        }else if (reason.equalsIgnoreCase("Kos sedang renovasi")){
            selenium.clickOn(By.xpath("//div[@class='radio-group__content']/div[3]//span[@class='icon dropdown-toggle bg']"));
            selenium.clickOn(tolakReasonButton);
        }else if (reason.equalsIgnoreCase("Lainnya")){
            selenium.clickOn(By.xpath("//div[@class='radio-group__content']/div[4]//span[@class='icon dropdown-toggle bg']"));
        }
    }

    /**
     * Get text in Booking status
     * @return text in Booking status
     */
    public String getBookingStatus() {
        selenium.waitTillElementIsVisible(bookingStatus);
        return selenium.getText(bookingStatus);
    }

    /**
     * Get text in Booking Message
     * @return text in booking message
     */
    public String getRejectBookingReasonMessage() {
        selenium.waitTillElementIsVisible(reasonMessage);
        return selenium.getText(reasonMessage);
    }

    /**
     * Get room name
     * @return String room name
     */
    public String getRoomName() {
        selenium.waitTillElementIsVisible(roomName);
        return selenium.getText(roomName);
    }

    /**
     * Click back arrow button
     * @throws InterruptedException
     */
    public void clickBackArrow() throws InterruptedException {
        selenium.waitTillElementIsVisible(backArrowButton);
        selenium.clickOn(backArrowButton);
    }
    /**
     * Write reason message in custom reason field
     * @param customReason is the reason why user reject booking
     * @throws InterruptedException
     */
    public void setCustomReason(String customReason) throws InterruptedException {
        selenium.waitTillElementIsVisible(customReasonTextField);
        selenium.enterText(customReasonTextField,customReason,true);
        selenium.clickOn(tolakReasonButton);
    }

    /**
     * Check if simpan kontrak button is enable
     */
    public void isSimpanButtonActive() {
        selenium.isElementDisplayed(simpanKontrakButton);
    }

    /**
     * Get page title only without subtitle
     * @return String "Detail Booking"
     */
    public String getPageTitle() {
        return selenium.getText(pageTitle).substring(0,14);
    }

    /**
     * set deposit in Booking process
     * @param deposit amount deposit
     */
    public void addDeposit(String deposit) {
        selenium.enterText(depositTextField,deposit,true);
    }

    /**
     * set denda in booking process
     * @param fee fee amout
     * @param value (1-30 depends on fee type)
     * @param type (hari,minggu,bulan,tahun)
     * @throws InterruptedException
     */
    public void addDenda(String fee, String value, String type) throws InterruptedException {
        selenium.enterText(dendaTextField,fee,true);
        selenium.javascriptClickOn(feeType);
        switch (type){
            case "Hari":
                selenium.clickOn(hariType);
                break;
            case "Minggu":
                selenium.clickOn(mingguType);
                break;
            case "Bulan":
                selenium.clickOn(bulanType);
                break;
            case "Tahun":
                selenium.clickOn(tahunType);
                break;
        }
        selenium.javascriptClickOn(feeAmount);
        selenium.clickOn(By.xpath("//div[@class='consultant-menu menu--is-active']//div[contains(.,"+value+")]"));
    }

    /**
     * add 1 biaya lain in booking process
     * @param name biaya lain name
     * @param value biaya lain value
     * @throws InterruptedException
     */
    public void addBiayaLain(String name, String value) throws InterruptedException {
        selenium.clickOn(tambahBiayaLainButton);
        selenium.enterText(namaBiayaLainTextField,name,true);
        selenium.enterText(valueBiayaLainTextField,value,true);
        selenium.clickOn(tambahBiayaLainPopUpButton);
    }

    /**
     * set DP
     * @param value (10%,20%,30%)
     * @throws InterruptedException
     */
    public void addDP(String value) throws InterruptedException {
        selenium.clickOn(dpSwitchButton);
        selenium.javascriptClickOn(dpDropdownButton);
        switch (value){
            case "10%":
                selenium.clickOn(sepuluhPersenDP);
                break;
            case "20%":
                selenium.clickOn(duapuluhPersenDP);
                break;
            case "30%":
                selenium.clickOn(tigapuluhPersenDP);
                break;
        }
    }

    /**
     * get deposit amount in tagihan
     * @return string text deposit in tagihan
     */
    public String getDepositInTagihan() {
        return selenium.getText(tagihanDeposit);
    }

    /**
     * get DP amount in tagihan
     * @return string text DP in tagihan
     */
    public String getDPInTagihan() {
        return selenium.getText(tagihanDP);
    }

    /**
     * get biaya lain amount in tagihan
     * @return string text biaya lain in tagihan
     */
    public String getBiayaLainInTagihan() {
        return selenium.getText(tagihanBiayaLain);
    }

    /**
     * Click Lihat Kontrak button in detail booking
     * @throws InterruptedException
     */
    public void viewContract() throws InterruptedException {
        selenium.waitTillElementIsVisible(seeContractButton);
        selenium.pageScrollInView(seeContractButton);
        selenium.clickOn(seeContractButton);
    }

    /**
     * Select property type for filter
     * @throws InterruptedException
     */
    public void selectFilterPropertyType(String propertyType) throws InterruptedException {
        selenium.clickOn(By.xpath("(//div[@class='col-xs-12 mb-2']//label[contains(text(),'" + propertyType + "')])[1]"));
    }

    /**
     * Count booking list
     */
    public int countBookingList() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(2);
        return bookingList.size();
    }

    /**
     * Click on filter icon
     * @throws InterruptedException
     */
    public void clickOnFilterIcon() throws InterruptedException {
        selenium.clickOn(filterIcon);
    }

    /**
     * Click on apply button
     * @throws InterruptedException
     */
    public void clickOnApplyButton() throws InterruptedException {
        selenium.clickOn(applyButton);
    }

    /**
     * Get label property type
     * @param index is property type will take from list
     */
    public String getPropertyType(int index) {
        selenium.pageScrollInView(propertyTypeLabelList.get(index));
        return selenium.getText(propertyTypeLabelList.get(index)).trim();
    }
}
