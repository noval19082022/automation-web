package pageobjects.consultanttools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class TaskHistoryPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public TaskHistoryPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='content mini-block-blue']")
    private WebElement statusActiveTask;

    /**
     * Get latest status task on history
     * @return data type funnel
     */
    public String getStatusTask() throws InterruptedException {
        selenium.waitTillElementIsVisible(statusActiveTask);
        return selenium.getText(statusActiveTask);
    }
}
