package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class AddNewContractPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AddNewContractPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='spinner']")
    private WebElement loadingAnimation;

    @FindBy(xpath = "//button[@class='navbar-btn-action arrow-right']")
    private WebElement backButtonArrow;

    @FindBy(xpath = "//button[contains(@class,'btn btn-primary btn-block')]")
    private WebElement searchPropertyButton;

    @FindBy(xpath = "//*[@class='ct-input__field']//*[@class='input']")
    private WebElement fieldKosName;

    @FindBy(xpath = "//div[@class='consultant-empty-state']]")
    private WebElement propertyNotFound;

    @FindBy(xpath = "//div[@class='search-property']")
    private WebElement formSearchProperty;

    @FindBy(xpath = "(//*[@class='radio-group__content']//*[@class='radio-label'])[1]")
    private WebElement firstKosList;

    @FindBy(xpath = "//*[@class='ct-radio radio default']")
    private List<WebElement> kosList;

    @FindBy(xpath = "//*[@name='Nomor Handphone']")
    private WebElement fieldNoHp;

    @FindBy(xpath = "//button[contains(@class,'btn btn-primary btn-block')]")
    private WebElement nextButton;

    @FindBy(xpath = "//*[@class='icon dropdown-toggle bg']")
    private By radioList;

    @FindBy(xpath="//*[@class='navbar-title']")
    private WebElement pageTitle;

    @FindBy(xpath="//*[@name='Nama']")
    private WebElement fieldName;

    @FindBy(xpath="//*[@name='Nomor HP']")
    private WebElement fieldHp;

    @FindBy(xpath="//*[@name='Email']")
    private WebElement fieldEmail;

    @FindBy(xpath="(//*[@class='btn-content'])[1]")
    private WebElement fieldGender;

    @FindBy(xpath="(//*[@class='btn-content'])[2]")
    private WebElement fieldStatus;

    @FindBy(xpath="(//*[@class='btn-content'])[3]")
    private WebElement fieldOccupation;

    @FindBy(xpath="(//*[@class='btn-group'])[3]")
    private WebElement expandOccupation;

    @FindBy(xpath="(//*[@id='Mahasiswa'])")
    private WebElement occupationMahasiswa;

    @FindBy(xpath="(//*[@class='image__preview']//img)[1]")
    private WebElement fieldImage1;

    @FindBy(xpath="(//*[@class='upload-activator'])[1]")
    private WebElement image1;

    @FindBy(xpath="(//*[@class='image__preview']//img)[2]")
    private WebElement fieldImage2;

    @FindBy(xpath="(//*[@class='upload-activator'])[2]")
    private WebElement image2;

    @FindBy(xpath="(//*[@class='text-danger'])[1]")
    private WebElement nameError;

    @FindBy(xpath="(//*[@class='text-danger'])[2]")
    private WebElement NohpError;

    @FindBy(xpath="(//*[@class='text-danger'])[3]")
    private WebElement emailError;

    @FindBy(xpath="(//*[@class='text-danger'])[4]")
    private WebElement penanggungJawabError;

    @FindBy(xpath="(//*[@class='text-danger'])[5]")
    private WebElement kontakPenanggungJawabError;

    @FindBy(xpath="(//*[@class='form-control dropdown-toggle']//*[@class='close'])[1]")
    private WebElement genderXButton;

    @FindBy(xpath="(//*[@class='form-control dropdown-toggle']//*[@class='close'])[2]")
    private WebElement statusXButton;

    @FindBy(xpath="(//*[@class='form-control dropdown-toggle']//*[@class='close'])[3]")
    private WebElement okupasiXButton;

    @FindBy(xpath="//*[@name='Nama Penanggung Jawab']")
    private WebElement fieldNamaPenanggungJawab;

    @FindBy(xpath="//*[@name='Nomor Kontak Penanggung Jawab']")
    private WebElement fieldKontakPenanggungJawab;

    @FindBy(xpath="(//*[@class='action__item']//*[@alt='Icon Delete'])[2]")
    private WebElement hapusFotoButton;

    @FindBy(xpath="(//*[@class='btn btn-primary btn-block btn-disabled'])[3]")
    private WebElement selanjutnyaButton;

    @FindBy(xpath="//*[@class='consultant-empty-state']")
    private WebElement emptyKosPage;

    @FindBy(xpath = "//button[contains(text(),'Selanjutnya')]")
    private WebElement nextPersonalDataButton;

    @FindBy(xpath = "//input[@name='Nomor Kamar']")
    private WebElement nomorKamarField;

    @FindBy(xpath = "(//span[@class='icon dropdown-toggle bg'])[1]")
    private WebElement firstRoomNumberAvailableRadioButton;

    @FindBy(xpath = "//button[contains(text(),'Terapkan')]")
    private WebElement terapkanRoomNumberButton;

    @FindBy(xpath = "//input[@placeholder='Pilih tanggal masuk']")
    private WebElement tanggalMasukField;

    @FindBy(xpath = "//span[@class='cell day today weekend sat']")
    private WebElement todaySatPick;

    @FindBy(xpath = "//span[@class='cell day today weekend sun']")
    private WebElement todaySunPick;

    @FindBy(xpath = "//span[@class='cell day today']")
    private WebElement todayWeekdaysPick;

    @FindBy(xpath = "//div[text()='Durasi Sewa']/following-sibling::div")
    private WebElement durasiSewaField;

    @FindBy(xpath = "//select[@class='secret']//option[@value='1']")
    private WebElement oneMonthOption;

    @FindBy(xpath = "//span[contains(text(),'Tambah Biaya Tetap')]")
    private WebElement tambahBiayaTetapToggle;

    @FindBy(xpath = "(//input[@name='Nama Biaya'])[1]")
    private WebElement namaBiayaTetapField;

    @FindBy(xpath = "(//input[@id='Jumlah'])[1]")
    private WebElement jumlahBiayaTetapField;

    @FindBy(xpath = "(//span[contains(text(),'Simpan')])[1]")
    private WebElement simpanBiayaTetapButton;

    @FindBy(xpath = "//span[contains(text(),'Tambah Biaya Lain')]")
    private WebElement tambahBiayaLainToggle;

    @FindBy(xpath = "(//input[@name='Nama Biaya'])[2]")
    private WebElement namaBiayaLainField;

    @FindBy(xpath = "(//input[@id='Jumlah'])[2]")
    private WebElement jumlahBiayaLainField;

    @FindBy(xpath = "(//span[contains(text(),'Simpan')])[2]")
    private WebElement simpanBiayaLainButton;

    @FindBy(xpath = "(//*[@class='other-costs__list']//li//div//h5)[1]")
    private WebElement fixedCostName;

    @FindBy(xpath = "(//*[@class='other-costs__list']//li//div//h5)[2]")
    private WebElement additionalCostName;

    @FindBy(xpath = "(//*[@class='other-costs__list']//li//div//p)[1]")
    private WebElement fixedCostAmount;

    @FindBy(xpath = "(//*[@class='other-costs__list']//li//div//p)[2]")
    private WebElement additionalCostAmount;

    @FindBy(xpath = "(//*[@class='secret'])[2]")
    private WebElement DurasiSewaOption;

    @FindBy(xpath = "(//span[@class='btn-content'])[2]")
    private WebElement kolomDurasi;

    @FindBy(xpath = "//div[text()='Hitungan Sewa']/following-sibling::div//select")
    private WebElement durationTextField;

    @FindBy(xpath = "//div[text()='Durasi Sewa']/following-sibling::div//select")
    private WebElement chooseDurasi;

    @FindBy(xpath = "//*[@class='v-switch-core']")
    private WebElement dendaSwitchButton;

    @FindBy(xpath = "//*[@id='Biaya Denda']")
    private WebElement penaltyAmountField;

    @FindBy(xpath = "//*[@name='Durasi']")
    private WebElement durasiDendaField;

    @FindBy(xpath = "(//span[@class='btn-content'])[3]")
    private WebElement unitDurasiDendaSelect;

    @FindBy(xpath = "//button[contains(@class, 'btn btn-primary btn-block')]")
    private WebElement saveContractButton;

    @FindBy(xpath = "//button[contains(text(),'Tidak, terimakasih')]")
    private WebElement tidakTerimakasihButton;

    @FindBy(xpath = "(//a[contains(text(),'Lihat Detail')])[1]")
    private WebElement lihatDetailKontrakButton;

    @FindBy(xpath = "(//*[@class='change-price']//div)[2]")
    private WebElement ubahBiayaLainButton;

    @FindBy(xpath = "(//*[@class='list__content']//h5)[1]")
    private WebElement fixNameText;

    @FindBy(xpath = "(//*[@class='list__content']//p)[1]")
    private WebElement fixAmountText;

    @FindBy(xpath = "(//*[@class='list__content']//h5)[2]")
    private WebElement additionalNameText;

    @FindBy(xpath = "(//*[@class='list__content']//p)[2]")
    private WebElement additionalAmountText;

    @FindBy(xpath = "(//*[@class='other-cost__label'])[1]")
    private WebElement tambahanBiayaTetapTitle;

    @FindBy(xpath = "(//*[@class='other-cost__label'])[2]")
    private WebElement tambahanBiayaLainTitle;

    @FindBy(xpath = "//*[@class='penalty-cost__form overflow-visible']")
    private WebElement formDenda;

    @FindBy(xpath = "(//*[@class='text-danger'])[4]")
    private WebElement errMessageDurasiDenda;

    @FindBy(xpath = "//div[@class='create-contract-page']")
    private WebElement createContractPage;

    @FindBy(xpath = "//div[@class='consultant-success-page is-page--open']")
    private WebElement contractSuccessfullyCreatedPage;

    @FindBy(id = "Biaya Denda")
    private WebElement penaltyCostEditText;

    @FindBy(xpath = "//input[@data-vv-name='Durasi']")
    private WebElement rangeDurationEditText;

    @FindBy(xpath = "//div[@class='consultant-select select__regular']")
    private WebElement durationDropdown;

    @FindBy(xpath = "//div[@class='form-group has-error']/following-sibling::div/p")
    private WebElement durationErrorMessage;

    /**
     * Fill Kos Name on the field
     * @param kosName Nama kos
     * @throws InterruptedException
     */
    public void setTextOnFieldKosName(String kosName) {
        selenium.waitTillElementIsVisible(fieldKosName);
        selenium.clearTextField(fieldKosName);
        selenium.enterText(fieldKosName, kosName, true);
    }

    /**
     * Click on Button Cari after inputing kos Name
     * @throws InterruptedException
     */
    public void clickOnSearchPropertyButton() throws InterruptedException {
        selenium.hardWait(1);
        selenium.clickOn(searchPropertyButton);
    }

    /**
     * Get how many result found after searching Kos name
     * @throws InterruptedException
     */
    public int getNumberOfList() throws InterruptedException {
        selenium.waitTillElementIsVisible(firstKosList);
        int numberOfElements = 0;
        if(selenium.waitInCaseElementVisible(firstKosList, 2) != null){
            numberOfElements = kosList.size();
        }
        return numberOfElements;
    }

    /**
     * Get Kos Name on the search result
     * @param index index of element
     */
    public String getKosNameLabel(int index) {
        By element = By.xpath("(//*[@class='radio-group__content']//*[@class='radio-label'])[" + index + "]");
        return selenium.getText(element);
    }

    /**
     * Click on 1 kos in the list
     * @param kosan kos name that user want to choose
     * @throws InterruptedException
     */
    public void clickOnKostList(String kosan) throws InterruptedException {
        selenium.waitTillElementIsVisible(firstKosList);
        for (int i=1;i<=getNumberOfList();i++) {
            By list = By.xpath("(//*[@class='radio-group__content']//*[@class='radio-label'])[" + i + "]");
            if (selenium.getText(list).contains(kosan)) {
                selenium.clickOn(By.xpath("(//*[@class='icon dropdown-toggle bg'])[" + i + "]")); //click on radio button based on kosan
                break;
            }
        }
    }

    /**
     * Click on button Lanjutkan
     * @throws InterruptedException
     */
    public void clickOnLanjutkanButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.click(nextButton);
    }

    /**
     * Fill Kos Name on the field
     * @param numberPhone No HP tenant
     * @throws InterruptedException
     */
    public void setTextOnFieldNoHp(String numberPhone) throws InterruptedException {
        selenium.hardWait(1);
        selenium.enterText(fieldNoHp, numberPhone, true);
    }

    /**
     * Get Title of current page
     */
    public String getPageTitle() {
        return selenium.getText(pageTitle);
    }

    /**
     * Get Tenant Nama on the Create new contract form
     */
    public String getTenantName() { return selenium.getElementAttributeValue(fieldName, "value"); }

    /**
     * Get Tenant Nomor Handphone on the Create new contract form
     */
    public String getTenantHp() {
        return selenium.getElementAttributeValue(fieldHp, "value");
    }

    /**
     * Get Tenant Email on the Create new contract form
     */
    public String getTenantEmail() {
        return selenium.getElementAttributeValue(fieldEmail, "value");
    }

    /**
     * Get Tenant Jenis Kelamin on the Create new contract form
     */
    public String getTenantJK() { return selenium.getText(fieldGender); }

    /**
     * Get Tenant Status on the Create new contract form
     */
    public String getTenantStatus() {
        return selenium.getText(fieldStatus);
    }

    /**
     * Get Tenant Okupasi on the Create new contract form
     */
    public String getTenantOkupasi() {
        return selenium.getText(fieldOccupation);
    }

    /**
     * Get image src path from preview image no 1
     */
    public String getTenantImg1() { return selenium.getElementAttributeValue(fieldImage1,"src");
    }

    /**
     * Get image src path from preview image no 2
     */
    public String getTenantImg2() {return selenium.getElementAttributeValue(fieldImage2,"src");
    }

    /**
     * Get validation error message on field Name
     */
    public String getNameError() {
        selenium.waitTillElementIsVisible(nameError);
        return selenium.getText(nameError);
    }

    /**
     * Get validation error message on field No HP
     */
    public String getTenantHpError() {
        selenium.waitTillElementIsVisible(NohpError);
        return selenium.getText(NohpError);
    }

    /**
     * Get validation error message on Email
     */
    public String getEmailError() {
        selenium.waitTillElementIsVisible(emailError);
        return selenium.getText(emailError);
    }

    /**
     * Identify fieldImg1 is not exist
     */
    public Boolean getEmptyImg1(){ return selenium.isElementDisplayed(fieldImage1);
    }

    /**
     * Identify fieldImg2 is not exist
     */
    public Boolean getEmptyImg2() { return selenium.isElementDisplayed(fieldImage2);
    }

    /**
     * Get validation error message on Penanggung Jawab
     */
    public String getPenanggungJawabError() {
        selenium.waitTillElementIsVisible(penanggungJawabError);
        return selenium.getText(penanggungJawabError);
    }

    /**
     * Get validation error message on Kontak Penanggung Jawab
     */
    public String getKontakPenanggungJawabError() {
        selenium.waitTillElementIsVisible(kontakPenanggungJawabError);
        return selenium.getText(kontakPenanggungJawabError);
    }

    /**
     * Set the field to empty
     * @param field fields on Tenant data form
     * @throws InterruptedException
     */
    public void setEmpty(String field) throws InterruptedException {
        switch (field){
            case "fieldName":
                selenium.enterText(fieldName,"",true);
                break;
            case "fieldHP":
                selenium.enterText(fieldHp,"",true);
                break;
            case "fieldEmail":
                selenium.enterText(fieldEmail,"",true);
                break;
            case "fieldGender":
                selenium.click(genderXButton);
                break;
            case "fieldStatus":
                selenium.click(statusXButton);
                break;
            case "fieldOccupation":
                selenium.click(okupasiXButton);
                break;
            case "fieldNamaPenanggungJawab":
                selenium.pageScrollInView(fieldOccupation);
                selenium.clickOn(expandOccupation);
                selenium.clickOn(occupationMahasiswa);
                selenium.waitTillElementIsVisible(fieldNamaPenanggungJawab);
                selenium.clickOn(fieldNamaPenanggungJawab);
                selenium.clickOn(fieldKontakPenanggungJawab);
                break;
            case "fieldKontakPenanggungJawab":
                selenium.pageScrollInView(fieldOccupation);
                selenium.clickOn(expandOccupation);
                selenium.clickOn(occupationMahasiswa);
                selenium.pageScrollInView(fieldKontakPenanggungJawab);
                selenium.clickOn(fieldKontakPenanggungJawab);
                selenium.clickOn(fieldNamaPenanggungJawab);
                break;
            default:
                System.out.println("No Field Match");
        }
    }

    /**
     * Identify button selanjutnya is not active
     * @Return true/false
     */
    public boolean isSelanjutnyaButtonDisabled() {
        return selenium.isElementAtrributePresent(selanjutnyaButton, "disabled");
    }

    /**
     * Identify empty kos page in Consultant tools is appear
     * @Return true/false
     */
    public boolean getKosEmptyPage() throws InterruptedException {
        selenium.hardWait(1);
        selenium.waitForJavascriptToLoad();
        return selenium.isElementDisplayed(emptyKosPage);
    }

    /**
     * Click selanjutnya button in personal data contract
     * @throws InterruptedException
     */
    public void clickOnNextPersonalDataButton() throws InterruptedException {
        selenium.pageScrollInView(nextPersonalDataButton);
        selenium.waitTillElementIsClickable(nextPersonalDataButton);
        selenium.clickOn(nextPersonalDataButton);
    }

    /**
     * Set room number with first available room
     */
    public void setRoomNumberInContract() throws InterruptedException {
        selenium.javascriptClickOn(nomorKamarField);
        selenium.hardWait(5);
        if(selenium.isElementDisplayed(firstRoomNumberAvailableRadioButton)){
            selenium.clickOn(firstRoomNumberAvailableRadioButton);
            selenium.pageScrollInView(terapkanRoomNumberButton);
            selenium.waitTillElementIsClickable(terapkanRoomNumberButton);
            selenium.clickOn(terapkanRoomNumberButton);
        }
    }

    /**
     * Choose tanggal masuk today
     * @throws InterruptedException
     */
    public void setCheckInDateToday() throws InterruptedException {
        selenium.javascriptClickOn(tanggalMasukField);
        if(selenium.isElementDisplayed(todayWeekdaysPick)){
            selenium.clickOn(todayWeekdaysPick);
        } else if(selenium.isElementDisplayed(todaySatPick)){
            selenium.clickOn(todaySatPick);
        } else if (selenium.isElementDisplayed(todaySunPick)){
            selenium.clickOn(todaySunPick);
        }
    }

    /**
     * Set rent duration
     * @param duration string duration user want to choose
     * @throws InterruptedException
     */
    public void setRentDurationOneMonth(String duration) throws InterruptedException {
        selenium.clickOn(durasiSewaField);
        selenium.hardWait(1);
        WebElement element = driver.findElement(By.xpath("//div[text()='Durasi Sewa']/following-sibling::div//a/span[text()='" + duration +"']"));
        selenium.JSScrollAndClickOn(element);
    }

    /**
     * Add Biaya tetap
     * @param nama nama biaya tetap
     * @param jumlah jumlah biaya tetap
     * @throws InterruptedException
     */
    public void addFixedCost(String nama, String jumlah) throws InterruptedException {
        selenium.javascriptClickOn(tambahBiayaTetapToggle);
        selenium.enterText(namaBiayaTetapField,nama,true);
        selenium.click(jumlahBiayaTetapField);
        selenium.enterTextCharacterByCharacter(jumlahBiayaTetapField,jumlah,true);
        selenium.click(namaBiayaTetapField);
        selenium.clickOn(simpanBiayaTetapButton);
    }

    /**
     * Get nama biaya tetap
     * @return string nama biaya tetap
     */
    public String getBiayaTetapName() {
        selenium.waitTillElementIsVisible(fixedCostName);
        return selenium.getText(fixedCostName);
    }

    /**
     * Get jumlah biaya tetap
     * @return string jumlah biaya tetap
     */
    public String getBiayaTetapAmount() {
        selenium.waitTillElementIsVisible(fixedCostAmount);
        return selenium.getText(fixedCostAmount);
    }

    /**
     * Add biaya lain
     * @param name nama biaya lain
     * @param amount jumlah biaya lain
     * @throws InterruptedException
     */
    public void addAdditionalCost(String name, String amount) throws InterruptedException {
        selenium.javascriptClickOn(tambahBiayaLainToggle);
        selenium.enterText(namaBiayaLainField,name,true);
        selenium.click(jumlahBiayaLainField);
        selenium.enterTextCharacterByCharacter(jumlahBiayaLainField,amount,true);
        selenium.click(namaBiayaLainField);
        selenium.clickOn(simpanBiayaLainButton);
    }

    /**
     * Get nama biaya lain
     * @return string nama biaya lain
     */
    public String getBiayaLainName() {
        selenium.waitTillElementIsVisible(additionalCostName);
        return selenium.getText(additionalCostName);
    }

    /**
     * Get jumlah biaya lain
     * @return string jumlah biaya lain
     */
    public String getBiayaLainAmount() {
        selenium.waitTillElementIsVisible(additionalCostAmount);
        return selenium.getText(additionalCostAmount);
    }
    

    /**
     * Enter penalty cost
     * @param amount jumlah denda
     * @param durasi toleransi keterlambatan
     * @param unitDurasi satuan durasi (Hari, Minggu, Bulan)
     */
    public void addPenaltyCost(String amount, String durasi, String unitDurasi) throws InterruptedException {
        selenium.pageScrollInView(dendaSwitchButton);
        selenium.clickOn(dendaSwitchButton);
        selenium.enterTextCharacterByCharacter(penaltyAmountField,amount,true);
        selenium.enterTextCharacterByCharacter(durasiDendaField,durasi,true);
        selenium.clickOn(unitDurasiDendaSelect);
        WebElement unit = driver.findElement(By.xpath("//div[@class='col-xs-6']/following-sibling::div//a/span[text()='"+unitDurasi+"']"));
        selenium.JSScrollAndClickOn(unit);
    }

    /**
     * Click on penalty cost toggle button add denda
     */
    public void activatePenaltyCost() throws InterruptedException {
        selenium.pageScrollInView(dendaSwitchButton);
        selenium.clickOn(dendaSwitchButton);
    }

    /**
     * Input amount penalty cost
     * @param amount jumlah denda
     */
    public void enterAmountPenaltyCost(String amount) throws InterruptedException {
        selenium.enterTextCharacterByCharacter(penaltyAmountField,amount,true);
    }

    /**
     * Input duration
     * @param duration toleransi keterlambatan
     */
    public void enterRangePenaltyCost(String duration) throws InterruptedException {
        selenium.enterTextCharacterByCharacter(durasiDendaField,duration,true);
    }

    /**
     * Enter renge and duration penalty cost
     * @param durasi toleransi keterlambatan
     * @param unitDurasi satuan durasi (Hari, Minggu, Bulan)
     */
    public void enterRangeAndPeriodPenaltyCost(String durasi, String unitDurasi) throws InterruptedException {
        selenium.enterTextCharacterByCharacter(durasiDendaField,durasi,true);
        selenium.clickOn(unitDurasiDendaSelect);
        WebElement unit = driver.findElement(By.xpath("//div[@class='col-xs-6']/following-sibling::div//a/span[text()='"+unitDurasi+"']"));
        selenium.JSScrollAndClickOn(unit);
    }

    /**
     * Get message error duration not valid
     */
    public String getDurationErrorMessage(){
        return selenium.getText(durationErrorMessage);
    }


    /**
     * Click on Simpan button to save contract
     * @throws InterruptedException
     */
    public void clickOnSaveContractButton() throws InterruptedException {
        selenium.pageScrollInView(saveContractButton);
        selenium.clickOn(saveContractButton);
    }

    /**
     * Verify save contract button is disable
     * @throws InterruptedException
     */
    public boolean saveContractButtonIsDisable() {
        selenium.pageScrollInView(saveContractButton);
        return selenium.isElementAtrributePresent(saveContractButton, "disabled");
    }

    /**
     * Click on Tidak terimakasih button after save contract
     * @throws InterruptedException
     */
    public void clickOnTidakTerimakasih() throws InterruptedException {
        selenium.waitTillElementIsVisible(tidakTerimakasihButton);
        selenium.clickOn(tidakTerimakasihButton);
    }

    /**
     * Click on lihat detail kontrak button of first list
     * @throws InterruptedException
     */
    public void clickOnLihatDetailKontrak() throws InterruptedException {
        selenium.waitTillElementIsNotVisible(loadingAnimation);
        selenium.waitTillElementIsVisible(lihatDetailKontrakButton);
        selenium.pageScrollInView(lihatDetailKontrakButton);
        selenium.javascriptClickOn(lihatDetailKontrakButton);
    }

    /**
     * Click on update biaya lain button
     * @throws InterruptedException
     */
    public void clickOnUbahBiayaLain() throws InterruptedException {
        selenium.waitTillElementIsVisible(ubahBiayaLainButton);
        selenium.clickOn(ubahBiayaLainButton);
    }

    /**
     * Get nama biaya tetap in detail contract page
     * @return string nama biaya tetap
     */
    public String getFixCostName() {
        return selenium.getText(fixNameText);
    }

    /**
     * Get amount biaya tetap in detail contract page
     * @return string amount biaya tetap
     */
    public String getFixCostAmount() {
        return selenium.getText(fixAmountText);
    }

    /**
     * Get nama biaya lain in detail contract page
     * @return string nama biaya lain
     */
    public String getAdditionalName() {
        return selenium.getText(additionalNameText);
    }

    /**
     * Get amount biaya lain in detail contract page
     * @return string amount biaya lain
     */
    public String getAdditionalAmount() {
        return selenium.getText(additionalAmountText);
    }

    /**
     * Check if tambahan biaya tetap is exist
     * @return true if exist, false if not exist
     */
    public boolean isTambahanBiayaTetapAppear() {
        return selenium.isElementDisplayed(tambahanBiayaTetapTitle);
    }

    /**
     * Check if tambahan biaya lain is exist
     * @return true if exist, false if not exist
     */
    public boolean isTambahanBiayaLainAppear() {
        return selenium.isElementDisplayed(tambahanBiayaLainTitle);
    }


    /**
     * Verify create contract page is appeared
     * @return Boolean element is appeared
     */
    public boolean isFormCreateContractPageAppear() {
        return selenium.isElementDisplayed(createContractPage);
    }

    /**
     * Verify back button arrow is appeared
     */
    public void backButtonArrowIsAppeared() {
        selenium.isElementDisplayed(backButtonArrow);
    }

    /**
     * Click on back button arrow
     * @throws InterruptedException
     */
    public void clickOnBackButtonArrow() throws InterruptedException {
        selenium.clickOn(backButtonArrow);
    }

    /**
     * Verify message if property not found
     */
    public void propertyNotFound() {
        selenium.isElementDisplayed(propertyNotFound);
    }

    /**
     * Verify form search property is appeared
     */
    public void formSearchPropertyIsAppeared() {
        selenium.isElementDisplayed(formSearchProperty);
    }

    /**
     * Verify search property button is disable
     */
    public void formSearchPropertyIsDisable() {
        selenium.isElementAtrributePresent(searchPropertyButton, "disabled");
    }

    /**
     * Verify next button is disable
     */
    public void nextButtonIsDisable() {
        selenium.isElementAtrributePresent(nextButton, "disabled");
    }

    /**
     * Verify page contract successfully created is appeared
     */
    public void contractSuccessfullyCreatedIsAppeared() {
        selenium.isElementDisplayed(contractSuccessfullyCreatedPage);
    }

    /**
     * Verify element  penalty cost not interact able on page create new contract
     */
    public boolean penaltyCostIsNotInteractAble(int duration) {
        return selenium.isClickable(penaltyCostEditText, duration);
    }

    /**
     * Verify element  penalty range not interact able on page create new contract
     */
    public boolean penaltyRangeIsNotInteractAble(int duration) {
        return selenium.isClickable(penaltyCostEditText, duration);
    }

    /**
     * Verify element  penalty duration is not interact able on page create new contract
     */
    public boolean penaltyDurationIsNotInteractAble(int duration) {
        return selenium.isClickable(penaltyCostEditText, duration);
    }
}
