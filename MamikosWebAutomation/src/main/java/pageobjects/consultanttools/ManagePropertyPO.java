package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ManagePropertyPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ManagePropertyPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(xpath = "//*[@class='input']")
    private WebElement searchTextBox;

    @FindBy(xpath = "//*[@class='consultant-card-list'][1]//*[@class='card-list__body']")
    private WebElement firstProperty;

    @FindBy(xpath = "//*[@class='card-list__body']")
    private List <WebElement> propertyList;

    @FindBy(xpath = "//*[@class='el-center flex-col']/h3")
    private WebElement propertyIsBlankLabel;

    @FindBy(xpath = "//button[contains(.,'Update Ketersediaan Kamar')]")
    private WebElement updateKetersediaanKamarButton;

    @FindBy(xpath = "//section[@class='search-filter']//span[@class='chip__content']/span[contains(.,'Kosong')]")
    private WebElement kosongTab;

    @FindBy(xpath = "//section[@class='search-filter']//span[@class='chip__content']/span[contains(.,'Terisi')]")
    private WebElement terisiTab;

    @FindBy(xpath = "//div[@class='card-list']/div[1]//div[@class='v-switch-core']")
    private WebElement sudahBerpenghuniButton;

    @FindBy(xpath = "//button[contains(.,'Ya')]")
    private WebElement yaConfirmationButton;

    @FindBy(xpath = "//h3[.='Kamar Tidak Ditemukan']")
    private WebElement kamarTidakDitemukan;

    @FindBy(xpath = "//button[.='Update Kamar']")
    private WebElement updateKamarButton;

    @FindBy(xpath = "(//*[@class='ma-0'])[4]")
    private WebElement kamarTerisiDetailPropertyLabel;

    @FindBy(xpath = "//i[@class='mamikos-icon icon-adjustment']")
    private WebElement filterButton;

    /**
     * Enter text and search property
     * @throws InterruptedException
     */
    public void enterAndSearchKost(String keyword) throws InterruptedException {
        selenium.hardWait(3);
        selenium.enterText(searchTextBox, keyword, true);
        selenium.sendKeyEnter(searchTextBox);
    }

    /**
     * Get list property
     * @return numberOfElements is number of property
     * @throws InterruptedException
     */
    public int getNumberOfList() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(2);
        selenium.isElementDisplayed(firstProperty);
        int numberOfElements = 0;
        if(selenium.waitTillElementIsVisible(firstProperty, 2) != null){
            numberOfElements = propertyList.size();
        }else{
            selenium.waitTillElementIsVisible(propertyIsBlankLabel, 5);
        }
        return numberOfElements;
    }

    /**
     * Get property name
     * @param index property list
     * @return property name
     */
    public String getPropertyName(int index) {
        By propertyName = By.xpath("//*[@class='consultant-card-list']["+index+"]//*[@class='card-title']");
        return selenium.getText(propertyName).trim();
    }

    /**
     * Get property status
     * @param index property list
     * @return property status
     */
    public String getVerificationStatus(int index) {
        By element = By.xpath("(//*[@class='chip__content'])['" + index + "']");
        selenium.waitInCaseElementVisible(element, 10);
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Click on property list
     * @param index property list
     */
    public void clickOnProperty(int index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        By propertyName = By.xpath("//*[@class='consultant-card-list'][" + index + "]");
        selenium.hardWait(5);
        selenium.pageScrollInView(propertyName);
        selenium.hardWait(1);
        selenium.clickOn(propertyName);
    }

    /**
     * Click button Update Ketersediaan Kamar
     * @throws InterruptedException
     */
    public void clickUpdateKetersediaanKamar() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(updateKetersediaanKamarButton);
        selenium.clickOn(updateKetersediaanKamarButton);
    }

    /**
     * Click tab kosong to view kamar kosong
     * @throws InterruptedException
     */
    public void clickTabKosong() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(kosongTab);
        selenium.clickOn(kosongTab);
    }

    /**
     * click tab terisi to view kamar terisi
     * @throws InterruptedException
     */
    public void clickTabTerisi() throws InterruptedException {
        selenium.waitTillElementIsVisible(terisiTab);
        selenium.clickOn(terisiTab);
    }

    /**
     * set all room to sudah berpenghuni
     * @throws InterruptedException
     */
    public void setAllRoomSudahBerpenghuni() throws InterruptedException {
        if(selenium.isElementDisplayed(sudahBerpenghuniButton)){
            int n = 1;
            while(!selenium.isElementDisplayed(kamarTidakDitemukan)){
                selenium.pageScrollInView(By.xpath("//div[@class='card-list']/div["+n+"]//div[@class='v-switch-core']"));
                selenium.clickOn(By.xpath("//div[@class='card-list']/div["+n+"]//div[@class='v-switch-core']"));
                selenium.clickOn(yaConfirmationButton);
                n++;
            }
            selenium.pageScrollInView(updateKamarButton);
            selenium.clickOn(updateKamarButton);
        }
    }

    /**
     * set N number room to available
     * @param counter as n number that want to set to available
     */
    public void setNRoomToAvailable(Integer counter) throws InterruptedException {
        for(int i=1;i<=counter;i++){
            selenium.clickOn(By.xpath("//div[@class='card-list']/div["+i+"]//div[@class='v-switch-core']"));
            selenium.clickOn(yaConfirmationButton);
        }
        selenium.pageScrollInView(updateKamarButton);
        selenium.clickOn(updateKamarButton);
    }

    /**
     * Get total room occupied
     * @return total room
     */
    public Integer getTotalRoomTerisi() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
        return Integer.parseInt(selenium.getText(kamarTerisiDetailPropertyLabel));
    }

    /**
     * Verify element room not appear
     * @return boolean
     */
    public boolean isKamarTidakDitemukanExist() {
        return selenium.isElementDisplayed(kamarTidakDitemukan);
    }

    /**
     * Click on burger filter button
     * @throws InterruptedException
     */
    public void clickOnFilterButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(filterButton);
        selenium.clickOn(filterButton);
    }

    /**
     * Get kost level of property
     * @return kost level
     */
    public String getKostLevel(int index) throws InterruptedException {
        By element = By.xpath("//div[@class='property-list']/div["+index+"]//div[@class='kos-level']");
        selenium.waitTillElementIsClickable(element);
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }
}
