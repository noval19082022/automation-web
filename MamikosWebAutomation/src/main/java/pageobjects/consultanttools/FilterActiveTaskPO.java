package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class FilterActiveTaskPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public FilterActiveTaskPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='btn btn-primary btn-block']")
    private WebElement submitFilterButton;

    /**
     * filter active task by data type
     * @param dataType that want to filter
     * @throws InterruptedException
     */
    public void clickFilterBy(String dataType) throws InterruptedException {
        selenium.clickOn(By.xpath("//*[@class='custom-checkbox']//label[contains(text(),'" + dataType + "')]"));
    }

    /**
     * Click on submit filter button
     * @throws InterruptedException
     */
    public void clickSubmitFilter() throws InterruptedException {
        selenium.waitTillElementIsClickable(submitFilterButton);
        selenium.clickOn(submitFilterButton);
    }
}
