package pageobjects.consultanttools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class PropertyPricePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public PropertyPricePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//button[contains(text(),'Update Kamar & Harga')]")
    private WebElement updateKamarDanHargaButton;

    @FindBy(id = "Perhari")
    private WebElement dailyPriceTextField;

    @FindBy(id = "Perminggu")
    private WebElement weeklyPriceTextField;

    @FindBy(id = "Perbulan")
    private WebElement monthlyPriceTextField;

    @FindBy(id = "Per 3 bulan")
    private WebElement threeMonthlyPriceTextField;

    @FindBy(id = "Per 6 bulan")
    private WebElement sixMonthlyPriceTextField;

    @FindBy(id = "Pertahun")
    private WebElement annualPriceTextField;

    @FindBy(xpath = "//*[@class='col-xs-12 mt-3']//*[@class='btn btn-primary btn-block']")
    private WebElement updateButton;

    @FindBy(xpath = "//button[contains(text(),'Ya')]")
    private WebElement yaConfirmationButton;

    @FindBy(xpath = "//button[contains(text(),'Oke')]")
    private WebElement okePopUpButton;

    /**
     * Click on Update Kamar & Harga Button
     * @throws InterruptedException
     */
    public void clickOnUpdateKamarDanHarga() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(updateKamarDanHargaButton);
        selenium.clickOn(updateKamarDanHargaButton);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
    }

    /**
     * Get daily price
     * @return daily price
     */
    public String getDailyPrice() throws InterruptedException {
        selenium.hardWait(1);
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(dailyPriceTextField,"value");
    }

    /**
     * Get weekly price
     * @return weekly price
     */
    public String getWeeklyPrice() throws InterruptedException {
        selenium.hardWait(1);
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(weeklyPriceTextField,"value");
    }

    /**
     * Get monthly price
     * @return monthly price
     */
    public String getMonthlyPrice() throws InterruptedException {
        selenium.hardWait(1);
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(monthlyPriceTextField,"value");
    }

    /**
     * Get 3 monthly price
     * @return 3 monthly price
     */
    public String getThreeMonthlyPrice() throws InterruptedException {
        selenium.hardWait(1);
        selenium.pageScrollInView(threeMonthlyPriceTextField);
        return selenium.getElementAttributeValue(threeMonthlyPriceTextField,"value");
    }

    /**
     * Get 6 monthly price
     * @return 6 monthly price
     */
    public String getSixMonthlyPrice() throws InterruptedException {
        selenium.hardWait(1);
        selenium.pageScrollInView(sixMonthlyPriceTextField);
        return selenium.getElementAttributeValue(sixMonthlyPriceTextField,"value");
    }

    /**
     * Get annual price
     * @return string annual price
     */
    public String getAnnualPrice() throws InterruptedException {
        selenium.hardWait(1);
        selenium.pageScrollInView(annualPriceTextField);
        return selenium.getElementAttributeValue(annualPriceTextField,"value");
    }

    /**
     * Set empty daily price
     * @throws InterruptedException
     */
    public void setEmptyToPriceField(WebElement element, WebElement element2) throws InterruptedException {
        selenium.doubleClickOnElement(element);
        selenium.hardWait(1);
        selenium.sendKeyBackSpace(element);
        selenium.click(element2);
        selenium.hardWait(1);
    }

    /**
     * Set empty daily price
     * @throws InterruptedException
     */
    public void emptyDailyPrice() throws InterruptedException {
        setEmptyToPriceField(dailyPriceTextField, weeklyPriceTextField);
    }

    /**
     * Set empty weekly price
     * @throws InterruptedException
     */
    public void emptyWeeklyPrice() throws InterruptedException {
        setEmptyToPriceField(weeklyPriceTextField, dailyPriceTextField);
    }

    /**
     * Set empty monthly price
     * @throws InterruptedException
     */
    public void emptyMonthlyPrice() throws InterruptedException {
        setEmptyToPriceField(monthlyPriceTextField, weeklyPriceTextField);
    }

    /**
     * Set empty quarterly price
     * @throws InterruptedException
     */
    public void emptyThreeMonthlyPrice() throws InterruptedException {
        setEmptyToPriceField(threeMonthlyPriceTextField, sixMonthlyPriceTextField);
    }

    /**
     * Set empty six monthly price
     * @throws InterruptedException
     */
    public void emptySixMonthlyPrice() throws InterruptedException {
        selenium.pageScrollInView(sixMonthlyPriceTextField);
        setEmptyToPriceField(sixMonthlyPriceTextField, annualPriceTextField);
    }

    /**
     * Set empty annual price
     * @throws InterruptedException
     */
    public void emptyAnnualPrice() throws InterruptedException {
        selenium.pageScrollInView(annualPriceTextField);
        setEmptyToPriceField(annualPriceTextField, sixMonthlyPriceTextField);
    }

    /**
     * Set alphabet to all field
     * @throws InterruptedException
     */
    public void setPriceToAlphabet(String rentType) throws InterruptedException {
        switch (rentType){
            case "Perhari":
                selenium.doubleClickOnElement(dailyPriceTextField);
                selenium.sendKeyBackSpace(dailyPriceTextField);
                selenium.enterTextCharacterByCharacter(dailyPriceTextField,"abcd",true);
                selenium.click(weeklyPriceTextField);
                break;
            case "Perminggu":
                selenium.doubleClickOnElement(weeklyPriceTextField);
                selenium.sendKeyBackSpace(weeklyPriceTextField);
                selenium.enterTextCharacterByCharacter(weeklyPriceTextField,"abcd",true);
                selenium.click(monthlyPriceTextField);
                break;
            case "Perbulan":
                selenium.doubleClickOnElement(monthlyPriceTextField);
                selenium.sendKeyBackSpace(monthlyPriceTextField);
                selenium.enterTextCharacterByCharacter(monthlyPriceTextField,"abcd",true);
                selenium.click(threeMonthlyPriceTextField);
                break;
            case "Per 3 bulan":
                selenium.pageScrollInView(threeMonthlyPriceTextField);
                selenium.doubleClickOnElement(threeMonthlyPriceTextField);
                selenium.sendKeyBackSpace(threeMonthlyPriceTextField);
                selenium.enterTextCharacterByCharacter(threeMonthlyPriceTextField,"abcd",true);
                selenium.click(sixMonthlyPriceTextField);
                break;
            case "Per 6 bulan":
                selenium.pageScrollInView(sixMonthlyPriceTextField);
                selenium.doubleClickOnElement(sixMonthlyPriceTextField);
                selenium.sendKeyBackSpace(sixMonthlyPriceTextField);
                selenium.enterTextCharacterByCharacter(sixMonthlyPriceTextField,"abcd",true);
                selenium.click(annualPriceTextField);
                break;
            case "Pertahun":
                selenium.pageScrollInView(annualPriceTextField);
                selenium.doubleClickOnElement(annualPriceTextField);
                selenium.sendKeyBackSpace(annualPriceTextField);
                selenium.enterTextCharacterByCharacter(annualPriceTextField,"abcd",true);
                selenium.click(sixMonthlyPriceTextField);
                break;
        }
    }

    /**
     * Save property updated
     * @throws InterruptedException
     */
    public void savePropertySet() throws InterruptedException {
        selenium.pageScrollInView(updateButton);
        selenium.clickOn(updateButton);
        selenium.clickOn(yaConfirmationButton);
        selenium.clickOn(okePopUpButton);
    }

    /**
     * Set daily price
     * @throws InterruptedException
     */
    public void setDailyPrice(String price) throws InterruptedException {
        selenium.doubleClickOnElement(dailyPriceTextField);
        selenium.sendKeyBackSpace(dailyPriceTextField);
        selenium.enterText(dailyPriceTextField, price,true);
        selenium.click(weeklyPriceTextField);
        savePropertySet();
    }

    /**
     * Set weekly price
     * @param price
     * @throws InterruptedException
     */
    public void setWeeklyPrice(String price) throws InterruptedException {
        selenium.doubleClickOnElement(weeklyPriceTextField);
        selenium.sendKeyBackSpace(weeklyPriceTextField);
        selenium.enterText(weeklyPriceTextField, price,true);
        selenium.click(monthlyPriceTextField);
        savePropertySet();
    }

    /**
     * Set monthly price
     * @param price
     * @throws InterruptedException
     */
    public void setMonthlyPrice(String price) throws InterruptedException {
        selenium.doubleClickOnElement(monthlyPriceTextField);
        selenium.sendKeyBackSpace(monthlyPriceTextField);
        selenium.enterText(monthlyPriceTextField, price,true);
        selenium.click(threeMonthlyPriceTextField);
        savePropertySet();
    }

    /**
     * Set quarterly price
     * @param price
     * @throws InterruptedException
     */
    public void setThreeMonthlyPrice(String price) throws InterruptedException {
        selenium.doubleClickOnElement(threeMonthlyPriceTextField);
        selenium.sendKeyBackSpace(threeMonthlyPriceTextField);
        selenium.enterText(threeMonthlyPriceTextField, price,true);
        selenium.click(sixMonthlyPriceTextField);
        savePropertySet();
    }

    /**
     * Set six monthly price
     * @param price
     * @throws InterruptedException
     */
    public void setSixMonthlyPrice(String price) throws InterruptedException {
        selenium.doubleClickOnElement(sixMonthlyPriceTextField);
        selenium.sendKeyBackSpace(sixMonthlyPriceTextField);
        selenium.enterText(sixMonthlyPriceTextField, price,true);
        selenium.click(annualPriceTextField);
        savePropertySet();
    }

    /**
     * Set annual price
     * @param price
     * @throws InterruptedException
     */
    public void setAnnualPrice(String price) throws InterruptedException {
        selenium.doubleClickOnElement(annualPriceTextField);
        selenium.sendKeyBackSpace(annualPriceTextField);
        selenium.enterText(annualPriceTextField, price,true);
        selenium.click(sixMonthlyPriceTextField);
        savePropertySet();
    }
}
