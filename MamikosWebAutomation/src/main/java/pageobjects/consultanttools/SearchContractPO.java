package pageobjects.consultanttools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class SearchContractPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SearchContractPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='input-control']")
    private WebElement searchTextBox;

    @FindBy(xpath = "(//*[@alt='Icon search'])[1]")
    private WebElement searchButton;

    /**
     * Search property name as keyword
     * @param keyword that want to search
     * @throws InterruptedException
     */
    public void setTextOnSearchTextBox(String keyword) throws InterruptedException {
        selenium.enterText(searchTextBox, keyword, false);
        selenium.clickOn(searchButton);
    }

}
