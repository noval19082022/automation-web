package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class SearchActiveTaskPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SearchActiveTaskPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='input']")
    private WebElement searchActiveTaskField;

    @FindBy(xpath = "//*[@class='activity-user-name']")
    private WebElement task;

    @FindBy(xpath = "//*[@class='content__wrapper']")
    private List<WebElement> searchResult;

    @FindBy(xpath = "//*[@class='alert__content']")
    private WebElement errorMessage;

    @FindBy(xpath = "//*[@class='activity__content']")
    private WebElement taskOnSearchResult;

    @FindBy(xpath = "//*[@class='text-muted text-center displat-4']")
    private WebElement noResultSearch;

    /**
     * Search data task as keyword
     * @param keyword that want to search
     * @throws InterruptedException
     */
    public void searchActiveTaskField(String keyword) throws InterruptedException {
        selenium.waitTillElementIsClickable(searchActiveTaskField);
        selenium.clickOn(searchActiveTaskField);
        selenium.enterText(searchActiveTaskField, keyword, false);
        selenium.waitTillElementIsVisible(searchActiveTaskField);
        selenium.hardWait(2);
    }

    /**
     * Click on data tenant search button
     * @throws InterruptedException
     */
    public void clickDataTenant() throws InterruptedException {
        By element = By.xpath("//*[@class='chip__content' and contains(text(),'Data Penyewa')]");
        selenium.javascriptClickOn(element);
    }

    /**
     * Click on data property search button
     * @throws InterruptedException
     */
    public void clickDataProperty() {
        By element = By.xpath("(//span[@class='chip__content'][contains(text(),'Data Properti')])[1]");
        String text = selenium.getText(element);
        if (text.contains("Data Properti Potensial")){
            element = By.xpath("(//span[@class='chip__content'][contains(text(),'Data Properti')])[2]");
        }
        selenium.javascriptClickOn(element);
    }

    /**
     * Click on data contract search button
     */
    public void clickDataContract() {
        By element = By.xpath("//*[@class='chip__content' and contains(text(),'Data Kontrak')]");
        selenium.javascriptClickOn(element);
    }

    /**
     * Click on data potential owner search button
     */
    public void clickDataPotentialOwner() {
        By element = By.xpath("//*[@class='chip__content' and contains(text(),'Data Owner Potensial')]");
        selenium.javascriptClickOn(element);
    }

    /**
     * Click on data potential property search button
     */
    public void clickDataPotentialProperty() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        By element = By.xpath("//*[@class='chip__content' and contains(text(),'Data Properti Potensial')]");
        selenium.javascriptClickOn(element);
    }

    /**
     * Get task name
     * @param index specific task want to get the name
     */
    public String getTaskName(int index)  {
        By element = By.xpath("(//*[@class='activity-user-name'])[" + index + "]");
        selenium.waitInCaseElementVisible(element, 10);
        return selenium.getText(task);
    }

    /**
     * Click one list of active task
     * @throws InterruptedException
     */
    public void clickTask() throws InterruptedException {
        selenium.waitTillElementIsClickable(task);
        selenium.clickOn(task);
        selenium.hardWait(1);
    }

    /**
     * Get number list of task
     */
    public int getNumberOfList() {
        int numberOfElements = 0;
        numberOfElements = searchResult.size();
        return numberOfElements;
    }

    /**
     * Get error message
     * @return content of error message
     */
    public String getErrorMessage()  {
        selenium.waitTillElementIsVisible(errorMessage);
        return selenium.getText(errorMessage);
    }

    /**
     * Search data task as keyword without select data type
     * @param keyword that want to search
     * @throws InterruptedException
     */
    public void searchActiveTaskWithoutSelectDataType(String keyword) throws InterruptedException {
        selenium.waitTillElementIsClickable(searchActiveTaskField);
        selenium.clickOn(searchActiveTaskField);
        selenium.enterText(searchActiveTaskField, keyword, false);
        selenium.waitTillElementIsVisible(searchActiveTaskField);
        selenium.hardWait(2);
        searchActiveTaskField.sendKeys(Keys.ENTER);
    }

    /**
     * Click task on search result
     * @throws InterruptedException
     */
    public void clickTaskOnSearchResult() throws InterruptedException {
        selenium.waitTillElementIsClickable(taskOnSearchResult);
        selenium.clickOn(taskOnSearchResult);
        selenium.hardWait(1);
    }

    /**
     * Get empty state
     * @return empty state no search result
     */
    public String getEmptyState()  {
        selenium.waitTillElementIsVisible(noResultSearch);
        return selenium.getText(noResultSearch);
    }
}
