package pageobjects.consultanttools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class RoomAllotmentPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public RoomAllotmentPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@placeholder='Masukkan Nama / Nomor Kamar']")
    private WebElement searchRoom;

    @FindBy(xpath = "(//div[contains(text(),'Nomor / Nama Kamar')]/following-sibling::div//input)[1]")
    private WebElement roomEditText;

    @FindBy(xpath = "//button[contains(.,'Update Kamar')]")
    private WebElement updateButton;

    @FindBy(xpath = "//div[@class='ct-input-text ct-input--dirty ct-input--invalid']//div[@class='message-text']")
    private WebElement warningMessage;

    @FindBy(xpath = "//div[@class='ct-input-text ct-input--invalid']//div[@class='message-text']")
    private WebElement emptyRoomNameMessage;

    @FindBy(xpath = "//i[@class='mamikos-icon icon-arrow-right-long']")
    private WebElement backButton;

    @FindBy(xpath = "//button[@class='btn btn-secondary btn-block']")
    private WebElement disagreeButton;

    @FindBy(xpath = "//button[contains(.,'Ya, Pindahkan')]")
    private WebElement agreeButton;

    @FindBy(xpath = "//div[@class='navbar-title']")
    private WebElement titlePage;

    @FindBy(xpath = "//button[contains(.,'Tambah Kamar Baru')]")
    private WebElement addRoomButton;

    @FindBy(xpath = "//div[@class='col-xs-6'][2]")
    private WebElement agreeAddRoomButton;

    @FindBy(xpath = "//div[@class='room-allotment-card']//i[@class='mamikos-icon icon-delete']")
    private WebElement deleteRoomButton;

    @FindBy(xpath = "//button[contains(.,'Hapus')]")
    private WebElement agreeDeleteButton;

    @FindBy(xpath = "//section[@class='search-filter']//span[@class='chip__content']/span[contains(.,'Kosong 1')]")
    private WebElement emptyRoomAmount;

    @FindBy(xpath = "//h3[.='Kamar Tidak Ditemukan']")
    private WebElement emptyRoom;

    @FindBy(xpath = "//div[@class='room-allotment-card']//div[@class='v-switch-core']")
    private WebElement occupiedRoomButton;

    @FindBy(xpath = "//button[contains(.,'Ya')]")
    private WebElement confirmOccupiedRoomButton;

    @FindBy(xpath = "//li[@class='message__item']")
    private WebElement updateSuccessMessage;

    @FindBy(xpath = "(//div[contains(text(),'Lantai')])[1]/following::input[1]")
    private WebElement floorEditText;

    @FindBy(xpath = "(//*[@alt='Icon close'])[1]")
    private WebElement xCloseMessageButton;

    /**
     * Enter text and search room name
     */
    public void enterAndSearchRoomName(String roomName) throws InterruptedException {
        selenium.waitTillElementIsVisible(roomEditText);
        selenium.pageScrollInView(roomEditText);
        selenium.hardWait(2);
        selenium.enterText(searchRoom,roomName,true);
        selenium.hardWait(5);
    }

    /**
     * Clear search room name
     */
    public void clearSearchRoomName() {
        selenium.waitTillElementIsVisible(searchRoom);
        selenium.pageScrollInView(searchRoom);
        selenium.clearTextField(searchRoom);
    }

    /**
     * Delete room name existing
     * @throws InterruptedException
     */
    public void setRoomNameBlank() throws InterruptedException {
        selenium.waitTillElementIsVisible(roomEditText);
        selenium.hardWait(2);
        selenium.clearTextField(roomEditText);
        selenium.sendKeyEnter(roomEditText);
    }

    /**
     * Delete floor name existing
     * @throws InterruptedException
     */
    public void setFloorNameBlank() throws InterruptedException {
        selenium.waitTillElementIsVisible(floorEditText);
        selenium.hardWait(2);
        selenium.clearTextField(floorEditText);
        selenium.sendKeyEnter(floorEditText);
    }

    /**
     * Enter text room name
     * @throws InterruptedException
     */
    public void setRoomName(String roomNumber) throws InterruptedException {
        selenium.waitTillElementIsVisible(roomEditText);
        selenium.pageScrollInView(roomEditText);
        selenium.hardWait(5);
        selenium.enterText(roomEditText, roomNumber, true);
        selenium.sendKeyEnter(roomEditText);
    }

    /**
     * Enter text floor name
     * @throws InterruptedException
     */
    public void setFloorName(String floorName) throws InterruptedException {
        selenium.waitTillElementIsVisible(floorEditText);
        selenium.pageScrollInView(floorEditText);
        selenium.enterText(floorEditText, floorName, true);
        selenium.sendKeyEnter(floorEditText);
    }

    /**
     * Click update button
     * @throws InterruptedException
     */
    public void clickOnUpdateButton() throws InterruptedException {
        selenium.pageScrollInView(updateButton);
        selenium.waitTillElementIsClickable(updateButton);
        selenium.hardWait(5);
        selenium.clickOn(updateButton);
        selenium.waitTillElementIsVisible(xCloseMessageButton);
    }

    /**
     * Get room name
     * @return room name
     */
    public String getRoomName() throws InterruptedException {
        selenium.pageScrollInView(searchRoom);
        selenium.pageScrollInView(roomEditText);
        return selenium.getElementAttributeValue(roomEditText, "value");
    }

    /**
     * Get floor name
     * @return floor name
     */
    public String getFloorName() throws InterruptedException {
        selenium.pageScrollInView(floorEditText);
        selenium.hardWait(5);
        return selenium.getElementAttributeValue(floorEditText, "value");
    }

    /**
     * Get warning message
     * @return warning message
     */
    public String getErrorMessage() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.getText(warningMessage);
    }

    public String getEmptyRoomErrorMessage() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.getText(emptyRoomNameMessage);
    }

    /**
     * Click back button
     * @throws InterruptedException
     */
    public void clickOnBackButton() throws InterruptedException {
        selenium.pageScrollInView(backButton);
        selenium.clickOn(backButton);
    }

    /**
     * Click agree button
     * @throws InterruptedException
     */
    public void clickOnAgreeButton() throws InterruptedException {
        selenium.clickOn(agreeButton);
    }

    /**
     * Click disagree button
     * @throws InterruptedException
     */
    public void clickOnDisagreeButton() throws InterruptedException {
        selenium.clickOn(disagreeButton);
    }

    /**
     * Get title page
     * @return title page
     */
    public String getTitlePage() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(titlePage);
    }

    /**
     * Click add new room button
     * @throws InterruptedException
     */
    public void clickOnAddRoomButton() throws InterruptedException {
        selenium.hardWait(7);
        selenium.clickOn(addRoomButton);
    }

    /**
     * Click agree add new room button
     * @throws InterruptedException
     */
    public void clickOnAgreeAddRoomButton() throws InterruptedException {
        selenium.clickOn(agreeAddRoomButton);
    }

    /**
     * Click delete room button
     * @throws InterruptedException
     */
    public void clickOnDeleteRoomButton() throws InterruptedException {
        selenium.hardWait(7);
        selenium.clickOn(deleteRoomButton);
    }

    /**
     * Click agree agree delete room button
     * @throws InterruptedException
     */
    public void clickOnAgreeDeleteButton() throws InterruptedException {
        selenium.clickOn(agreeDeleteButton);
    }

    /**
     * Get empty state no room found
     * @return empty state
     */
    public String getEmptyState() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(emptyRoom);
    }

    /**
     * Click occupied room button
     * @throws InterruptedException
     */
    public void clickOnOccupiedRoomButton() throws InterruptedException {
        selenium.clickOn(occupiedRoomButton);
    }

    /**
     * Click agree change occupied room button
     * @throws InterruptedException
     */
    public void clickOnConfirmOccupiedButton() throws InterruptedException {
        selenium.clickOn(confirmOccupiedRoomButton);
    }

    /**
     * Get success message update room
     * @return success message
     */
    public String getSuccessMessage() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(updateSuccessMessage);
    }
}
