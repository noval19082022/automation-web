package pageobjects.consultanttools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ListPotentialTenantPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ListPotentialTenantPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(xpath = "//*[@class='input__indicator']")
    private WebElement searchIndicatorTextBox;

    @FindBy(xpath = "//input[@class='input-control']")
    private WebElement searchTextBox;

    @FindBy(xpath = "(//*[@alt='Icon search'])[1]")
    private WebElement searchButton;

    @FindBy(xpath = "//button[contains(text(),'Buat Kontrak')]")
    private WebElement createContractButton;

    @FindBy(xpath = "//div[@class='consultant-card-list']")
    private WebElement potentialTenantCard;

    @FindBy(xpath = "//button[contains(text(),'Tambah Data Penyewa')]")
    private WebElement addPotentialTenant;

    @FindBy(xpath = "(//div[@class='consultant-card-list'])[1]")
    private WebElement firstListPotentialTenant;

    /**
     * Search data potential tenant
     * @param searchKeyword that want to search
     * @throws InterruptedException
     */
    public void searchTenant(String searchKeyword) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(searchIndicatorTextBox);
        selenium.enterText(searchTextBox, searchKeyword, true);
        selenium.waitTillElementIsVisible(searchButton);
        selenium.hardWait(3);
        selenium.clickOn(searchButton);
    }

    /**
     * Click on create contract button
     * @throws InterruptedException
     */
    public void clickOnCreateContractButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(createContractButton);
    }

    /**
     * Verify create contract button is disabled
     */
    public void createContractButtonIsDisabled() {
        selenium.isElementAtrributePresent(createContractButton, "disabled");
    }

    /**
     * Search potential tenant
     * @throws InterruptedException
     */
    public void searchTenantPotential(String tenant) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsClickable(searchIndicatorTextBox);
        selenium.hardWait(5);
        selenium.clickOn(searchIndicatorTextBox);
        selenium.enterText(searchTextBox, tenant, true);
        selenium.waitTillElementIsVisible(searchButton);
        selenium.hardWait(3);
        selenium.clickOn(searchButton);
    }

    /**
     * Select tenant potential
     * @throws InterruptedException
     */
    public void selectPotentialTenant() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
        selenium.clickOn(potentialTenantCard);
    }

    /**
     * Add new tenant potential
     * @throws InterruptedException
     */
    public void clickOnAddPotentialTenant() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
        selenium.clickOn(addPotentialTenant);
    }

    /**
     * Click on first list tenant potential
     * @throws InterruptedException
     */
    public void clickOnFirstListPotentialTenant() throws InterruptedException {
        selenium.clickOn(firstListPotentialTenant);
    }
}
