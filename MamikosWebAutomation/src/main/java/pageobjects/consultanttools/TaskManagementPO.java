package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class TaskManagementPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public TaskManagementPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='navbar-title']")
    private WebElement pageTitle;

    @FindBy(xpath = "//*[@class='link']")
    private WebElement seeAllFunnelLabel;

    @FindBy(xpath = "//*[@class='title mb-2']")
    private WebElement listActiveTaskLabel;

    /**
     * Get page title
     * @return page title
     */
    public String getPageTitle() {
        return selenium.getText(pageTitle);
    }

    /**
     * Verify element search text box is present
     * @return boolean
     */
    public boolean fieldSearchIsPresent() throws InterruptedException {
        return selenium.isElementPresent(By.xpath("//*[@class='ct-input-text search-activator ct-input--flat ct-input--readonly']"));
    }

    /**
     * Verify element consultant name is present
     * @return boolean
     */
    public boolean consultantNameIsPresent() throws InterruptedException {
        return selenium.isElementPresent(By.xpath("//*[@class='activity-list-funnels']/h3"));
    }

    /**
     * Verify element consultant roles is present
     * @return boolean
     */
    public boolean consultantRolesIsPresent() throws InterruptedException {
        return  selenium.isElementPresent(By.xpath("//*[@class='activity-list-funnels']/p"));
    }

    /**
     * Verify element funnel card is present
     * @return boolean
     */
    public boolean funnelCardIsPresent() throws InterruptedException {
        return selenium.isElementPresent(By.xpath("//*[@class='funnel']"));
    }

    /**
     * Get label see all funnel
     * @return String
     */
    public String getLabelSeeAllFunnel() {
        return selenium.getText(seeAllFunnelLabel);
    }

    /**
     * Get label list active task
     * @return String
     */
    public String getLabelListActiveTask() {
        return selenium.getText(listActiveTaskLabel);
    }

    /**
     * Verify element data type filter is present
     * @return boolean
     */
    public boolean dataTypeFilterIsPresent() throws InterruptedException {
        return selenium.isElementPresent(By.xpath("//*[@class='ct-swiper__item']"));
    }

    /**
     * Verify element list active task is present
     * @return boolean
     */
    public boolean listActiveTaskIsPresent() throws InterruptedException {
        return selenium.isElementPresent(By.xpath("//*[@class='ct-activity-item']"));
    }

    /**
     * Verify element pagination is present
     * @return boolean
     */
    public boolean paginationIsPresent() throws InterruptedException {
        return selenium.isElementPresent(By.xpath("//*[@class='consultant-pagination ct-pagination--green green-version']"));
    }
}
