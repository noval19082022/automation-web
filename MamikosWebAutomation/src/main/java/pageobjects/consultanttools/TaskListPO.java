package pageobjects.consultanttools;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class TaskListPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public TaskListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    //*[@class='activity-user-name']

    @FindBy(xpath = "//*[@class='btn btn-primary btn-block']")
    private WebElement seeAllTaskButton;

    @FindBy(xpath = "//*[@class='input']")
    private WebElement searchField;

    @FindBy(xpath = "//*[@class='activity-user-name']")
    private WebElement taskName;

    /**
     * Click see all task list button
     * @throws InterruptedException
     */
    public void clickSeeAllTaskButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.pageScrollInView(seeAllTaskButton);
        selenium.clickOn(seeAllTaskButton);
    }

    /**
     * Search task name to activate on list task
     * @param task that want to search
     * @throws InterruptedException
     */
    public void searchTaskList(String task) throws InterruptedException {
        selenium.waitTillElementIsClickable(searchField);
        selenium.enterText(searchField, task, false);
        searchField.sendKeys(Keys.ENTER);
        selenium.hardWait(7);
        if (selenium.isElementDisplayed(taskName)){
            selenium.click(taskName);
        }
    }

}
