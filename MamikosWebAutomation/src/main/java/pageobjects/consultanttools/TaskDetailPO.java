package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class TaskDetailPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public TaskDetailPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='consultant-btn -large -block']")
    private WebElement activateTaskButton;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-block'][contains(text(),'Selanjutnya')]")
    private WebElement nextStageButton;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-block'][contains(text(),'Edit Data')]")
    private WebElement updateDataButton;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-block'][text()='Batal']")
    private WebElement cancelButton;

    @FindBy(xpath = "//*[@class='bg-white detail-card'][1]")
    private WebElement taskDetail;

    @FindBy(xpath = "//*[@class='stage-title']")
    private WebElement stageTitle;

    @FindBy(xpath = "//a[@class='detail-contract-link']")
    private WebElement hyperlinkContract;

    @FindBy(xpath = "//a[@class='shortcut-link']")
    private WebElement hyperlinkProperty;

    @FindBy(xpath = "//*[@class='navbar-btn-action arrow-right']")
    private WebElement previousButton;

    @FindBy(xpath = "//*[@class='radio-label' and contains(text(),'Daftar Tugas')]")
    private WebElement taskListOptionActivity;

    @FindBy(xpath = "//*[@class='radio-label' and contains(text(),'Tugas Aktif')]")
    private WebElement activeTaskOptionActivity;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-block' and contains(text(),'Pindahkan')]")
    private WebElement moveButton;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-block' and contains(text(),'Simpan')]")
    private WebElement saveButton;

    @FindBy(xpath = "//*[@class='btn btn-warning btn-block']")
    private WebElement warningButton;

    @FindBy(xpath = "//*[@class='stage-action']")
    private WebElement historyButton;

    @FindBy(xpath = "//div[@class='radio-group__content']//span[contains(text(),'Daftar Tugas')]")
    private WebElement TaskListOptionStage;

    @FindBy(xpath = "(//div[@class='radio-group__content']/div[@class='ct-radio radio default disabled ct-radio--disabled']/following-sibling::div)[2]")
    private WebElement jumpStageActivityOption;

    @FindBy(xpath = "//textarea[@placeholder='Catatan']")
    private WebElement notesEditText;

    @FindBy(xpath = "//textarea[@placeholder='Alasan tidak setuju?']")
    private WebElement reasonNotAgree;

    @FindBy(xpath = "//div[@class='consultant-datepicker']")
    private WebElement monthPaymentEditText;

    @FindBy(xpath = "(//div[@class='consultant-datepicker'])[1]")
    private WebElement contactDateEditText;

    @FindBy(xpath = "(//div[@class='consultant-datepicker'])[1]")
    private WebElement testDateEditText;

    @FindBy(xpath = "(//div[@class='consultant-datepicker'])[2]")
    private WebElement originalDateEditText;

    @FindBy(xpath = "//input[@data-mask='99:99']")
    private WebElement callingTimeEditText;

    @FindBy(xpath = "//input[@placeholder='masukkan nomor HP']")
    private WebElement phoneNumberEditText;

    @FindBy(xpath = "//textarea[@placeholder='Alasan tidak di validasi?']")
    private WebElement reasonDataValidateEditText;

    @FindBy(xpath = "//input[@placeholder='Nama']")
    private WebElement nameEditText;

    @FindBy(xpath = "//input[@placeholder='Email']")
    private WebElement emailEditText;

    @FindBy(xpath = "//input[@placeholder='Harga Sewa']")
    private WebElement rentalPrice;

    @FindBy(xpath = "//input[@placeholder='Durasi Sewa']")
    private WebElement rentalDuration;

    @FindBy(xpath = "(//div[@class='consultant-datepicker'])[1]")
    private WebElement dueDateEditText;

    @FindBy(xpath = "//input[@placeholder='Test']")
    private WebElement testTextEditText;

    @FindBy(xpath = "//input[@data-mask='99:99']")
    private WebElement testTimeEditText;

    @FindBy(xpath = "//textarea[@placeholder='Input']")
    private WebElement inputTextEditText;

    @FindBy(xpath = "//input[@placeholder='Alamat']")
    private WebElement addressEditText;

    @FindBy(xpath = "//*[@class='activity-staging-form']")
    private WebElement taskForm;

    @FindBy(xpath = "//*[@class='spinner']")
    private WebElement loadingImage;

    @FindBy(xpath = "//textarea[@placeholder='Isi']")
    private WebElement contentsEditText;

    @FindBy(xpath = "//input[@placeholder='Nomor']")
    private WebElement numberEditText;

    @FindBy(xpath = "//*[@class='bg-white detail-card'][1]")
    private WebElement funnelContainer;

    @FindBy(xpath = "//ul[@class='report-list']")
    private WebElement reportList;

    @FindBys(@FindBy(xpath="//ul[@class='report-list']//h4"))
    private List<WebElement> listReportFieldLabel;

    /**
     * Click activate task button
     * @throws InterruptedException
     */
    public void clickActivateTaskButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(activateTaskButton,5);
        selenium.pageScrollInView(activateTaskButton);
        selenium.waitTillElementIsClickable(activateTaskButton);
        selenium.clickOn(activateTaskButton);
    }

    /**
     * Get stage title of task
     * @return data type funnel
     */
    public String getStageTitle() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(stageTitle);
        selenium.hardWait(7);
        return selenium.getText(stageTitle);
    }

    /**
     * Get task name on detail task
     * @return data type funnel
     */
    public String getTaskDetail() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(taskDetail);
        return selenium.getText(taskDetail);
    }

    /**
     * Click next button to move task
     * @throws InterruptedException
     */
    public void clickNextStageButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementVisible(nextStageButton,5);
        selenium.hardWait(2);
        selenium.waitTillElementIsClickable(nextStageButton);
        selenium.clickOn(nextStageButton);
    }

    /**
     * Click update data button to move task
     * @throws InterruptedException
     */
    public void clickOnUpdateDataButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementVisible(updateDataButton,5);
        selenium.hardWait(2);
        selenium.waitTillElementIsClickable(updateDataButton);
        selenium.clickOn(updateDataButton);
    }

    /**
     * Click Daftar Tugas radio button
     * @throws InterruptedException
     */
    public void clickOnTaskListOption() throws InterruptedException {
        selenium.waitTillElementIsClickable(taskListOptionActivity);
        selenium.clickOn(taskListOptionActivity);
    }

    /**
     * Click option active task
     * @throws InterruptedException
     */
    public void clickOptionActiveTask() throws InterruptedException {
        selenium.waitTillElementIsClickable(activeTaskOptionActivity);
        selenium.clickOn(activeTaskOptionActivity);
    }

    /**
     * Click submit button to move task
     * @throws InterruptedException
     */
    public void clickMoveButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(moveButton);
        selenium.clickOn(moveButton);
    }

    /**
     * Click warning button to verify move task
     * @throws InterruptedException
     */
    public void clickWarningButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(warningButton);
        selenium.clickOn(warningButton);
    }

    /**
     * Click previous button until back to manage task page
     * @throws InterruptedException
     */
    public void backToManageTaskPage() throws InterruptedException {
        while (!selenium.isElementPresent(By.xpath("//*[@class='funnels-container']"))){
            selenium.waitForJavascriptToLoad();
            selenium.hardWait(2);
            selenium.clickOn(previousButton);
        }
    }

    /**
     * Click hyperlink button to contract on detail task
     */
    public void clickContractHyperlink() {
        selenium.javascriptClickOn(hyperlinkContract);
    }

    /**
     * Click hyperlink button to property on detail task
     */
    public void clickPropertyHyperlink() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(funnelContainer);
        selenium.javascriptClickOn(hyperlinkProperty);
    }

    /**
     * Click see history task button
     */
    public void clickHistoryTask() {
        selenium.javascriptClickOn(historyButton);
    }

    /**
     * Click task stage active plus 2 (jump stage)
     */
    public void selectJumpStage() throws InterruptedException {
        selenium.clickOn(jumpStageActivityOption);
    }

    /**
     * Select option radio button
     */
    public void selectOptionActivityForm(String option) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsNotVisible(loadingImage);
        selenium.javascriptClickOn(By.xpath("//div[@class='ct-radio radio default']//input[@value='" + option + "']"));
    }

    /**
     * Fill field notes one activity form
     */
    public void enterNotes(String notes) {
        selenium.enterText(notesEditText, notes, true);
    }

    /**
     * Fill field reason for not agreeing
     */
    public void enterReasonNotAgree(String notes) {
        selenium.enterText(reasonNotAgree, notes, true);
    }

    /**
     * Click on save button
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Click on cancel button
     */
    public void clickOnCancelButton() throws InterruptedException {
        selenium.clickOn(cancelButton);
    }

    /**
     * Select monthly payment
     */
    public void selectDateMonthlyPayment(String date) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(monthPaymentEditText);
        selenium.clickOn(By.xpath("//span[contains(@class, 'cell day')][text()='" + date + "']"));
    }

    /**
     * Select contact date
     */
    public void selectContactDate(String date) throws InterruptedException {
        selenium.clickOn(contactDateEditText);
        selenium.clickOn(By.xpath("//span[contains(@class, 'cell day')][text()='" + date + "']"));
    }

    /**
     * Select original date
     */
    public void selectOriginalDate(String date) throws InterruptedException {
        selenium.pageScrollInView(originalDateEditText);
        selenium.clickOn(originalDateEditText);
        selenium.clickOn(By.xpath("(//span[contains(@class, 'cell day')][text()='" + date + "'])[2]"));
    }

    /**
     * Select original date
     */
    public void enterCallingTime(String time) throws InterruptedException {
        selenium.clearTextField(callingTimeEditText);
        selenium.enterTextCharacterByCharacter(callingTimeEditText, time, true);
    }

    /**
     * Enter phone number
     */
    public void enterPhoneNumber(String phoneNumber) {
        selenium.enterText(phoneNumberEditText, phoneNumber, true);
    }

    /**
     * Enter reason data not valid
     */
    public void enterReasonsNotValidated(String reason) {
        selenium.enterText(reasonDataValidateEditText, reason, true);
    }

    /**
     * Enter name
     */
    public void enterName(String name) {
        selenium.enterText(nameEditText, name, true);
    }

    /**
     * Enter email
     */
    public void enterEmail(String email) {
        selenium.enterText(emailEditText, email, true);
    }

    /**
     * Enter rental price
     */
    public void enterRentalPrice(String price) {
        selenium.enterText(rentalPrice, price, true);
    }

    /**
     * Enter rental duration
     */
    public void enterRentalDuration(String duration) {
        selenium.enterText(rentalDuration, duration, true);
    }

    /**
     * Select due date
     */
    public void selectDueDate(String date) throws InterruptedException {
        selenium.pageScrollInView(dueDateEditText);
        selenium.clickOn(dueDateEditText);
        selenium.clickOn(By.xpath("//span[contains(@class, 'cell day')][text()='" + date + "']"));
    }


    /**
     * Enter text for test form
     */
    public void enterTestNotes(String text) {
        selenium.enterText(testTextEditText, text, true);
    }

    /**
     * Enter time for testing time format
     */
    public void enterTestTime(String time) {
     selenium.enterText(testTimeEditText, time, true);
    }

    /**
     * Enter time for testing time format
     */
    public void enterInputTextField(String text) {
        selenium.enterText(inputTextEditText, text, true);
    }

    /**
     * Select test date
     */
    public void selectTestDate(String date) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(testDateEditText);
        selenium.clickOn(By.xpath("//span[contains(@class, 'cell day')][text()='" + date + "']"));
    }

    /**
     * Enter address property
     */
    public void enterAddress(String address) throws InterruptedException {
        selenium.enterText(addressEditText, address, true);
    }

    /**
     * Enter content
     */
    public void fillFieldContent(String contents) {
        selenium.enterText(contentsEditText, contents, true);
    }

    /**
     * Enter owner number
     * @param number is owner phone number
     */
    public void enterOwnerNumber(String number) {
        selenium.enterText(numberEditText, number, true);
    }

    /**
     * Enter owner number
     * @param index is indexing desired field
     * @throws InterruptedException
     */
    public String getValueReportField(int index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
        selenium.pageScrollUsingCoordinate(0, 1000);
        selenium.pageScrollInView(listReportFieldLabel.get(index));
        return selenium.getText(listReportFieldLabel.get(index));
    }
}
