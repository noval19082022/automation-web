package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class AddPotentialTenantPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AddPotentialTenantPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@placeholder='Tulis nomor handphone di sini']")
    private WebElement phoneNumberEditText;

    @FindBy(xpath = "//button[contains(text(),'Lanjutkan')]")
    private WebElement nextButton;

    @FindBy(xpath = "//input[@placeholder='Email Penyewa']")
    private WebElement emailEditText;

    @FindBy(xpath = "//input[@placeholder='Nama Lengkap']")
    private WebElement nameEditText;

    @FindBy(xpath = "//div[text()='Okupasi']/parent::div//div[@class='form-control dropdown-toggle']")
    private WebElement occupationDropdownList;

    @FindBy(xpath = "//input[@placeholder='Cari Kos']")
    private WebElement kosNameEditText;

    @FindBy(xpath = "//input[@placeholder='Cari property']")
    private WebElement searchKosNameEditText;

    @FindBy(xpath = "//img[@alt='Icon search']")
    private WebElement searchIcon;

    @FindBy(xpath = "(//div[@class='consultant-card-list'])[1]")
    private WebElement firstKosList;

    @FindBy(xpath = "//div[text()='Hitungan Sewa']/parent::div//div[@class='form-control dropdown-toggle']")
    private WebElement rentalPeriodDropdownList;

    @FindBy(id = "Harga Sewa")
    private WebElement rentalPriceEditText;

    @FindBy(xpath = "//input[@placeholder='Pilih tanggal jatuh tempo']")
    private WebElement duDateEditText;

    @FindBy(xpath = "//div[text()='Catatan']/following-sibling::div")
    private WebElement notesDropdownList;

    @FindBy(xpath = "//label[contains(text(),'Penjelasan Catatan')]/parent::div/div")
    private WebElement notesTextField;

    @FindBy(xpath = "(//textarea[@placeholder='Tulis catatan di sini'])[1]")
    private WebElement notesEditText;

    @FindBy(xpath = "//button[contains(text(),'Simpan')]")
    private WebElement saveNotesButton;

    @FindBy(xpath = "//button[contains(text(),'Daftarkan Penyewa')]")
    private WebElement registerTenantButton;

    /**
     * Click on create contract button
     * @param phoneNumber is phone number new potential tenant
     */
    public void enterPhoneNumber(String phoneNumber) {
        selenium.enterText(phoneNumberEditText, phoneNumber, true);
    }

    /**
     * Click on create contract button
     * @throws InterruptedException
     */
    public void clickOnNextButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
        selenium.clickOn(nextButton);
    }

    /**
     * Enter tenant email
     * @param email is tenant email
     */
    public void enterEmail(String email) {
        selenium.enterText(emailEditText, email, true);
    }

    /**
     * Enter tenant name
     * @param name is tenant name
     */
    public void enterName(String name) {
        selenium.enterText(nameEditText, name, true);
    }

    /**
     * Select tenant gender
     * @param gender is tenant gender
     */
    public void selectGender(String gender) throws InterruptedException {
        selenium.clickOn(By.xpath("//div[contains(text(),'Jenis Kelamin')]/parent::div//span[contains(text(),'" + gender + "')]"));
    }

    /**
     * Select tenant occupation
     * @param occupation is tenant occupation
     */
    public void selectOccupation(String occupation) throws InterruptedException {
        selenium.clickOn(occupationDropdownList);
        selenium.hardWait(1);
        WebElement element = driver.findElement(By.xpath("//div[text()='Okupasi']/parent::div//a/span[text()='" + occupation + "']"));
        selenium.JSScrollAndClickOn(element);
    }

    /**
     * Select kos
     * @param kosName is kos name
     * @throws InterruptedException
     */
    public void selectKos(String kosName) throws InterruptedException {
        selenium.clickOn(kosNameEditText);
        selenium.enterText(searchKosNameEditText, kosName, true);
        selenium.clickOn(searchIcon);
        selenium.clickOn(firstKosList);
    }

    /**
     * Select tenant occupation
     * @param rentalPeriod is tenant rental period
     * @throws InterruptedException
     */
    public void selectRentalPeriod(String rentalPeriod) throws InterruptedException {
        selenium.clickOn(rentalPeriodDropdownList);
        selenium.hardWait(1);
        WebElement element = driver.findElement(By.xpath("//div[text()='Hitungan Sewa']/parent::div//a/span[text()='" + rentalPeriod +"']"));
        selenium.JSScrollAndClickOn(element);
    }

    /**
     * Select tenant occupation
     * @param rentalPrice is tenant occupation
     */
    public void enterRentalPrice(String rentalPrice) {
        selenium.enterText(rentalPriceEditText, rentalPrice, true);
    }

    /**
     * Select due date
     * @param tomorrow is due date
     * @throws InterruptedException
     */
    public void selectDate(String tomorrow) throws InterruptedException {
        selenium.clickOn(duDateEditText);
        selenium.clickOn(By.xpath("//span[text()='" + tomorrow + "']"));
    }

    /**
     * Select notes
     * @param notes is notes option
     * @throws InterruptedException
     */
    public void selectNotes(String notes) throws InterruptedException {
        selenium.clickOn(notesDropdownList);
        selenium.waitForJavascriptToLoad();
        By element = By.xpath("//span[@class='radio-label'][contains(text(),'" + notes + "')]");
        selenium.pageScrollInView(element);
        selenium.clickOn(element);
    }

    /**
     * Enter notes
     * @param notes is notes
     */
    public void enterAndSaveNotes(String notes) throws InterruptedException {
        selenium.clickOn(notesTextField);
        selenium.enterText(notesEditText, notes, true);
        selenium.clickOn(saveNotesButton);
    }

    /**
     * Click on register tenant for save data tenant
     * @throws InterruptedException
     */
    public void clickOnAddTenantButton() throws InterruptedException {
        selenium.clickOn(registerTenantButton);
    }
}
