package pageobjects.consultanttools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ActiveTenantDetailPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ActiveTenantDetailPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='card-list'][2]//a[@class='link']")
    private WebElement tenantPhoneNumber;

    @FindBy(xpath = "//button[@class='navbar-btn-action arrow-right']")
    private WebElement backToPrevPageButton;


    public String getTenantPhoneNumber() {
        return selenium.getText(tenantPhoneNumber);
    }

    public void clickOnBackToPrevPageButton() throws InterruptedException {
        selenium.clickOn(backToPrevPageButton);
        selenium.hardWait(5);
    }
}
