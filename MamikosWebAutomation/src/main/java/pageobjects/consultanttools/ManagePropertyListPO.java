package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ManagePropertyListPO {
    private JavaHelpers java = new JavaHelpers();
    WebDriver driver;
    SeleniumHelpers selenium;

    public ManagePropertyListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//button[@class='filter-activator']")
    private WebElement filterAndShortIcon;

    @FindBys(@FindBy(xpath = "//div[@class='card-list__content']"))
    private List<WebElement> propertyList;

    @FindBy(xpath = "//button[text()='Terapkan']")
    private WebElement applyButton;

    @FindBys(@FindBy(xpath = "//div[@class='card-list__content']//div[@class='property-status']/span"))
    private List<WebElement> lastUpdateList;

//    @FindBys(@FindBy(xpath = "(//div[@class='property-list-body']//div[@class='col-xs-6 card-descriptions']/p)[1]"))
//    private List<WebElement> emptyRoomLabelList;

    /**
     * Click on icon filter and short property
     * @throws InterruptedException
     */
    public void clickOnFilterAndShortIcon() throws InterruptedException {
        selenium.clickOn(filterAndShortIcon);
    }

    /**
     * Count list of propert
     * @throws InterruptedException
     */
    public int countPropertyList() throws InterruptedException {
        selenium.hardWait(3);
        return propertyList.size();
    }

    /**
     * Click on filter and short option
     * @throws InterruptedException
     */
    public void clickOnShortRoomAvailability(String shortOption) throws InterruptedException {
        selenium.clickOn(By.xpath("//div[@class='radio-group__content']//span[contains(text(),'" + shortOption + "')]"));
    }

    /**
     * Click on apply button
     * @throws InterruptedException
     */
    public void clickOnApplyButton() throws InterruptedException {
        selenium.clickOn(applyButton);
    }

    /**
     * Get last update property
     * @param index property last update will take from list
     */
    public Date getLastUpdate(int index) throws ParseException {
        String lastUpdate = selenium.getText(lastUpdateList.get(index));

        Locale id = new Locale("in", "ID");
        SimpleDateFormat pattern = new SimpleDateFormat("dd MMM yyyy", id);

        return pattern.parse(lastUpdate);
    }

    /**
     * Get empty room
     * @param index property last update will take from list
     */
    public int getEmptyRoom(int index) {
        return java.extractNumber(selenium.getText(By.xpath("((//div[@class='property-list-body'])[" + index + "]//div[@class='col-xs-6 card-descriptions']/p)[1]")));
    }
}
