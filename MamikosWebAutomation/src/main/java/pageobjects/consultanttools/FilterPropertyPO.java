package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class FilterPropertyPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public FilterPropertyPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//button[@class='btn btn-primary btn-block']")
    private WebElement applyButton;

    /**
     * filter property by kost level
     * @param goldplus that want to filter
     * @throws InterruptedException
     */
    public void clickOnFilterParameter(String goldplus) throws InterruptedException {
        selenium.clickOn(By.xpath("//label[contains(text(),'" + goldplus + "')]"));
    }

    /**
     * Click on apply filter button
     * @throws InterruptedException
     */
    public void clickOnApplyButton() throws InterruptedException {
        selenium.clickOn(applyButton);
    }

}
