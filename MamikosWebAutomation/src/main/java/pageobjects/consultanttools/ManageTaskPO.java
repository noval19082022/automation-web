package pageobjects.consultanttools;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ManageTaskPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ManageTaskPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='funnel']")
    private WebElement funnelButton;

    @FindBy(xpath = "//*[@class='ct-input-text search-activator ct-input--flat ct-input--readonly']")
    private WebElement searchFieldActiveTask;

    @FindBy(xpath = "//*[@class='search-container bg-white']")
    private WebElement searchScreen;

    @FindBy(xpath = "//*[@class='ct-swiper__item']")
    private WebElement filterButton;

    @FindBy(xpath = "//*[@class='ct-list__content']")
    private List<WebElement> activeTaskList;

    @FindBy(xpath = "//*[@class='funnel']")
    private List<WebElement> listFunnel;

    @FindBy(xpath = "//*[@class='task-title' and contains(text(),'Daftar Tugas')]")
    private WebElement stageLabel;

    @FindBy(xpath = "//*[@class='link']")
    private WebElement seeAllFunnel;

    /**
     * Click on see all funnel
     * @throws InterruptedException
     */
    public void clickSeeAllFunnel() throws InterruptedException {
        selenium.waitInCaseElementVisible(seeAllFunnel,5);
        selenium.pageScrollInView(seeAllFunnel);
        selenium.clickOn(seeAllFunnel);
        selenium.waitTillElementIsVisible(funnelButton,5);
    }

    /**
     * Get funnel name
     * @throws InterruptedException
     * @return funnel name
     */
    public String getFunnel(int index) throws InterruptedException {
        By element = By.xpath("(//*[@class='funnel']["+index+"]//span)[1]");
        selenium.waitInCaseElementVisible(element,5);
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Get list of all funnel
     * @throws InterruptedException
     * @return numberOfElements is number of active task list
     */
    public int getNumberOfFunnel() {
        int numberOfElements = 0;
        numberOfElements = listFunnel.size();
        return numberOfElements;
    }

    /**
     * Click on funnel card
     * @throws InterruptedException
     */
    public void clickOnFunnelCard(String funnel) throws InterruptedException {
        selenium.waitInCaseElementVisible(By.xpath("//span[.='"+funnel+"']"),5);
        selenium.pageScrollInView(By.xpath("//span[.='"+funnel+"']"));
        selenium.clickOn(By.xpath("//span[.='"+funnel+"']"));
        selenium.waitTillElementIsVisible(stageLabel,5);
    }

    /**
     * Click on search field
     * @throws InterruptedException
     */
    public void clickSearchFieldActiveTask() throws InterruptedException {
        selenium.waitInCaseElementVisible(searchFieldActiveTask,5);
        selenium.clickOn(searchFieldActiveTask);
        selenium.waitTillElementIsVisible(searchScreen);
    }

    /**
     * Click on filter button
     * @throws InterruptedException
     */
    public void clickFilterButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(filterButton,5);
        selenium.pageScrollInView(filterButton);
        selenium.clickOn(filterButton);
    }

    /**
     * Get list of active task
     * @throws InterruptedException
     * @return numberOfElements is number of active task list
     */
    public int getNumberOfList() throws InterruptedException {
        int numberOfElements = 0;
        numberOfElements = activeTaskList.size();
        return numberOfElements;
    }

    /**
     * Get data type task
     * @throws InterruptedException
     * @return data type task
     */
    public String getDataTypeTask(int index) throws InterruptedException {
        By element = By.xpath("(//*[@class='task-label'])[" + index + "]");
        selenium.waitTillElementIsClickable(element);
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }
}
