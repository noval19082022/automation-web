package pageobjects.mamikos.mamiads.visibility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class VisibilityPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public VisibilityPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//span[@class='bg-c-text bg-c-text--body-4 ']")
    private WebElement visibilityText;

    @FindBy(css = ".bg-c-text--title-2")
    private WebElement titleVisibletext;

    /**
     * user verify message on mamiads visibility
     * @return message mamiads visibiliy
     */
    public String getVisibilityMessage() {
        return selenium.getText(visibilityText);
    }

    /**
     * user verify title in bottom message on mamiads visibility
     * @return title mamiads visibiliy
     */
    public String getTitleVisibilityText() {
        return selenium.getText(titleVisibletext);
    }
}