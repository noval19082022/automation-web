package pageobjects.mamikos.mamiads.rekomendasilisting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class RekomendasiListingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public RekomendasiListingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(css = ".premium-recom-title")
    WebElement rekomendasiTitle;

    @FindBy(xpath = "//button[@class='btn btn-danger btn-xs']")
    WebElement hapusHistoryButton;

    @FindBy(css = ".bg-c-button--primary")
    WebElement cariAndSewaButton;

    @FindBy(css = ".user-kost-empty__unique-code-entry")
    WebElement kodePemilikButton;

    @FindBy(xpath = "//div[@class='premium-recom-slider-item']")
    WebElement paginationNumberAct;

    @FindBy(xpath = "//*[@class=‘premium-recom-slide’]//div[@class=‘track-list-booking-kost’]")
    WebElement rekomendasiListingActual;

    /**
     * Get message in of empty state on favorite page
     * @param emptyMessage for specific message of empty state want to get
     * @return empty state
     */
    public String getMessageEmptyState(String emptyMessage) {
        String emptyStateLocator = "//p[.='"+ emptyMessage +"']";
        selenium.waitInCaseElementVisible(By.xpath(emptyStateLocator), 3);
        return selenium.getText(By.xpath(emptyStateLocator));
    }

    /**
     * Click tab on favorite page
     * @param tabTitle for spesific tab want to click
     * @throws InterruptedException
     */
    public void clickOnTab(String tabTitle) throws InterruptedException {
        String tabTitleLocator = "//*[contains(text(),'"+ tabTitle +"')]";
        selenium.waitInCaseElementVisible(By.xpath(tabTitleLocator),3 );
        selenium.clickOn(By.xpath(tabTitleLocator));
    }

    /**
     * Get properti name on pernah dilihat tab
     * @param propertyName for specific menu want to get
     * @return properti name
     */
    public String getLastSeenDetailProperti(String propertyName) {
        String propertyNameLocator = "//*[contains(text(),'"+ propertyName +"')]";
        selenium.waitInCaseElementVisible(By.xpath(propertyNameLocator), 3);
        return selenium.getText(By.xpath(propertyNameLocator));
    }

    /**
     * is rekomendasi section is present
     *
     * @return false
     */
    public boolean isRekomendasiSectionVisible(){
        selenium.waitInCaseElementVisible(rekomendasiTitle, 3);
        return selenium.isElementDisplayed(rekomendasiTitle);
    }

    /**
     * is hapus histori button present
     *
     * @return true
     */
    public boolean isHapusHistoryVisible() {
        return selenium.isElementDisplayed(hapusHistoryButton);
    }

    /**
     * verify menu user tenant
     * @param menuUser for spesific menu user want to get
     * @return menuUser
     */
    public String getMenuUserTenant(String menuUser) {
        String menuUserLocator = "//li[contains(.,'"+ menuUser +"')]";
        selenium.waitInCaseElementVisible(By.xpath(menuUserLocator), 3);
        return selenium.getText(By.xpath(menuUserLocator));
    }

    /**
     * is cari and sewa button display
     * 
     * @return true
     */
    public boolean isMulaiCariDanSewaKosBtnDisplay() {
        return selenium.isElementDisplayed(cariAndSewaButton);
    }

    /**
     * is kode dari pemilik button display
     *
     * @return true
     */
    public boolean isMasukkanKodeDariPemilikBtnDisplay() {
        return selenium.isElementDisplayed(kodePemilikButton);
    }

    /**
     * Pagination number get as integer
     * @return integer data type
     */
    public int getPaginationActual() {
        try {
            return Integer.parseInt(selenium.getText(paginationNumberAct));
        }
        catch (Exception e) {
            return 0;
        }
    }

    /**
     * Rekomendasi listing number get as integer
     * @return integer data type
     */
    public int getRekomendasiActual() {
        try {
            return Integer.parseInt(selenium.getText(rekomendasiListingActual));
        }
        catch (Exception e) {
            return 0;
        }
    }
}
