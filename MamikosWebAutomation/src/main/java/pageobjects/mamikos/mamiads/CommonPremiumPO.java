package pageobjects.mamikos.mamiads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class CommonPremiumPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public CommonPremiumPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='bg-c-modal__action-closable']")
    WebElement closeOnBoardingMamiAdsPopUp;

    @FindBy(className = "bg-c-empty-state__title")
    WebElement titleText;

    @FindBy(className = "bg-c-empty-state__description")
    WebElement messageText;

    @FindBy(className = "goldplus-header__back-button")
    WebElement paduanMamiadsBackButton;

    @FindBy(css = ".gp-swiper__navigation-button-next")
    WebElement caraMenggunakanMamiadsNextButton;

    private By btnPilihSaldo = By.xpath("//button[.='Pilih Saldo']");

    /**
     * Click on Pop Up Keuntungan MamiAds
     *
     * @throws InterruptedException
     */
    public void clickOnCloseOnBoardingMamiAdsPopUp() throws InterruptedException {
        selenium.hardWait(5);
        if ( selenium.waitInCaseElementVisible(closeOnBoardingMamiAdsPopUp,15) != null) {
            selenium.clickOn(closeOnBoardingMamiAdsPopUp);
        }
    }

    /**
     * Verify Booking Langsung pop up is appear or not
     *
     * @return pop up is appear
     */
    public boolean isOnBoardingMamiAdsPopUpAppear() {
        return selenium.waitInCaseElementVisible(closeOnBoardingMamiAdsPopUp, 3) != null;
    }

    /**
     * Click on Text
     *
     * @throws InterruptedException
     */
    public void clickOnText(String menu) throws InterruptedException {
        selenium.hardWait(7);
        String element = "//*[contains(text(),'"+menu+"')]";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 3);
        selenium.clickOn(By.xpath(element));
        selenium.hardWait(1);
    }

    /**
     * Get Text title
     *
     * @return title
     */
    public String getTitleText(){
        selenium.waitInCaseElementVisible(titleText, 5);
        return selenium.getText(titleText);
    }

    /**
     * Get Text message
     *
     * @return message
     */
    public String getMessageText(){
        selenium.waitInCaseElementVisible(messageText, 5);
        return selenium.getText(messageText);
    }

    /**
     * Get element contain text
     *
     * @return text
     */
    public String containText(String text){
        return selenium.getText(By.xpath("//*[contains(text(),'"+text+"')]"));
    }

    /**
     * Click on Panduan MamiAds Back Button
     *
     * @throws InterruptedException
     */
    public void clickOnPanduanMamiAdsBackButton() throws InterruptedException {
        selenium.clickOn(paduanMamiadsBackButton);
    }

    /**
     * Click on Cara Menggunakan Mamiads Next Button
     *
     * @throws InterruptedException
     */
    public void clickOncaraMenggunakanMamiadsNextButton() throws InterruptedException {
        selenium.clickOn(caraMenggunakanMamiadsNextButton);
    }

    /**
     * Wait until button Pilih Saldo is perfectly loaded
     */
    public void waitTillPilihSaldoButtonAreLoaded() {
        selenium.waitTillElementsCountIsMoreThan(btnPilihSaldo, 1);
    }

}
