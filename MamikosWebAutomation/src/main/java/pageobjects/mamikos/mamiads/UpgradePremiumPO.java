package pageobjects.mamikos.mamiads;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UpgradePremiumPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public UpgradePremiumPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='btn btn-mamiorange track-owner-upgrade']")
    private WebElement upgradeButton;

    @FindBy(id = "swal2-content")
    private WebElement premiumWarning;

    @FindBy(xpath = "//a[contains(.,'Konfirmasi')]")
    private WebElement konfirmasiButton;

    @FindBy(xpath = "//a[.='Ganti Paket']")
    private WebElement gantiPaketButton;

    @FindBy(id = "balanceSubmitButton")
    private WebElement okButton;

    @FindBy(xpath = "//span[@class='text-green']")
    private WebElement nominalSaldoText;

    @FindBy(css = ".manage-ads__balance-allocation .check.is-green-brand")
    private WebElement switchOnButton;

    @FindBy(xpath = "//b[.='Yang Dialokasikan di Data Iklan Ini']")
    private WebElement editAlokasiPopUpMessage;

    @FindBy(xpath = "//form[2]//button[@class='btn btn-mamigreen-inverse']")
    private WebElement batalPopUpButton;

    @FindBy(xpath = "//*[@id=\"ownerHeader\"]/div[3]/div/div[3]/a")
    private WebElement tambahSaldoButton;

    @FindBy(css = ".separate-left")
    private WebElement batalSaldoButton;

    @FindBy(xpath = "//form[2]//button[@class='btn btn-mamigreen']")
    private WebElement okAllocatedButton;

    @FindBy(css = ".pull-left")
    private WebElement backtolistiklanButton;

    @FindBy(css = ".swal2-buttonswrapper")
    private WebElement IsDisplayLoading;

    @FindBy(xpath = "//p[.='Kos']")
    private WebElement kosMenu;

    @FindBy(css = ".btn.btn-mamiorange.track-owner-upgrade")
    private WebElement tambahSaldoButtonIfBuyPremium;

    @FindBy(css = ".swal2-confirm")
    private WebElement okButtonPopUp;

    @FindBy(xpath = "//b[.='YA']")
    private WebElement tingkatkanKlikMessageYa;

    @FindBy(xpath = "//b[.='TIDAK']")
    private WebElement tingkatkanKlikMessageTidak;

    @FindBy(css = ".swal2-content")
    private WebElement textOffTingkatkanKlikToggle;

    @FindBy(css = ".track-owner-upgrade")
    private WebElement upgradePremiumButton;

    @FindBy(css = ".track-purchase-confirm")
    private WebElement konfirmasiPembayaranButton;

    @FindBy(css = ".item-chosen .change-purchase")
    private WebElement ubahButton;

    @FindBy(css = "#pkg-option .pkg-title")
    private WebElement packagelistSection;

    @FindBy(css = ".detail-info .nominal:nth-child(2)")
    private WebElement nominalPremiumText;

    @FindBy(xpath = "//div[@class='kos-card__expandable']")
    private WebElement lihatselengkapnyaButton;

    @FindBy(xpath = "//div[@class='kos-card__status-name--kos-waiting']")
    private WebElement noFoundLihatselengkapnyaButton;

    @FindBy(xpath = "//*[@class='btn btn-mamigreen-inverse detail-kos__manage-premium-link']")
    private WebElement aturPremiumButton;

    @FindBy(xpath = "//*[@class='input premium-balance__input']")
    private WebElement allocationField;

    @FindBy(css = ".c-mk-button.premium-balance__submit")
    private WebElement konfirmasiAllocationButton;

    @FindBy(xpath = "//span[contains(text(),'Kelola Alokasi Saldo')]")
    private WebElement kelolaAlokasiSaldoButton;

    @FindBy(css = "#__layout  .premium-balance__error")
    private WebElement minimumAlokasiSaldoRp5000Text;

    @FindBy(css = "#__layout span.icon.premium-balance__back-icon")
    private WebElement backtoAlokasiSaldoButton;

    @FindBy(css = ".premium-card .premium-card__buy-balance")
    private WebElement konfirmasiSaldoButton;

    @FindBy(css = ".top-ads-modal .cancel-button")
    private WebElement batalNonAktifIklanTeratasButton;

    @FindBy(css = ".top-ads-modal .b-checkbox")
    private WebElement checkboxNonAktifIklanTeratas;

    @FindBy(id = "premiumActionButton")
    private WebElement aturPremiumButtonDashboard;

    @FindBy(css = ".wrapper__alert .media-content")
    private WebElement messageWarningPopUp;

    @FindBy(css = ".wrapper__alert .delete")
    private WebElement closeWarningPopUpButton;

    @FindBy(css = ".modal-dialog .owner-intercept-booking-modal__close-button")
    private WebElement closeBookingLangsungPopUp;

    @FindBy(css = ".owner-intercept-booking-modal .modal-dialog .modal-content")
    private WebElement bookingLangsungPopUp;

    @FindBy(xpath = "//*[@id=\"__layout\"]/div/div[1]/aside/div/div[2]/div[1]/p")
    private WebElement propertySaya;

    @FindBy(xpath = "//p[.='Statistik']")
    private WebElement statistikMenu;

    @FindBy(css = ".mk-action-card__main-content-title.bg-c-text.bg-c-text--title-5")
    private WebElement goldplusStatistik;

    @FindBy(xpath = "//a[.='Lihat detail statistik']")
    private WebElement lihatdetailStatistik;

    @FindBy(xpath = "//h2[.='Statistik Kunjungan Iklan']")
    private WebElement statistikKunjungan;

    @FindBy(xpath = "//h2[.='Statistik Chat Masuk']")
    private WebElement statistikChat;

    @FindBy(xpath = "//h2[.='Statistik Peminat Kos']")
    private WebElement statistikPeminat;

    @FindBy(css = ".bg-c-empty-state__title:nth-child(2)")
    private WebElement statistikText;

    @FindBy(css = "div.statistic-menu .bg-c-empty-state__description")
    private WebElement emptyDescStatistik;

    @FindBy ( css = ".c-mk-card.premium-page__card > div > div.manage-ads__promo > a")
    private WebElement kelolaPromoButton;

    @FindBy (css = ".title .input")
    private WebElement enterPromoTextField;

    @FindBy (css = ".description .textarea")
    private WebElement enterDetailPromoTextField;

    @FindBy (xpath = "//button[@class='c-mk-button c-mk-button--green-brand c-mk-button--block']")
    private WebElement pasangPromoButton;

    @FindBy(css = ".period1 .input")
    private WebElement startDateButton;

    @FindBy(css = ".period2 .input")
    private WebElement endDateButton;

    @FindBy(xpath = "//span[@class='total-data status-waiting']")
    private WebElement statusPromo;

    private By checkIsOffClass = By.className("is-off");

    private By VerifyAllocatedSaldoButton = By.xpath("//span[@class='promoted-active-label']");

    private By bayarButton = By.cssSelector(".purchase-detail .purchase-btn");

    @FindBy(css = ".custom-loading .owner-tips-loading .tips-loading")
    private WebElement loadingAnimation;

    @FindBy(css = ".bg-l-sidebar > div:nth-of-type(2) > .bg-l-sidebar__multi-dropdown")
    private WebElement propertySayaDropdownMenu;

    /**
     * Click on Upgrade Premium button
     *
     * @throws InterruptedException
     */
    public void clickOnUpgradeBtn() throws InterruptedException {
        selenium.clickOn(upgradeButton);
    }

    /**
     * Get Warning Text from Upgrade Premium Pop up
     *
     * @return Premium Warning
     */
    public String getPremiumWarning() {
        return selenium.getText(premiumWarning);
    }

    /**
     * Click on Confirmation button
     *
     * @throws InterruptedException
     */

    public void clickOnConfirmationBtn() throws InterruptedException {
        selenium.clickOn(konfirmasiButton);
    }

    /**
     * Click on Ganti Paket button
     *
     * @throws InterruptedException
     */

    public void clickOnGantiPaketBtn() throws InterruptedException {
        selenium.pageScrollInView(gantiPaketButton);
        selenium.clickOn(gantiPaketButton);
    }

    /**
     * Click on balance amount will be buy owner
     *
     * @throws InterruptedException
     */

    public void clickBalance(String text, String balancePrice) throws InterruptedException {
        String xpathLocator = "//div[.='Saldo " + text + "Rp " + balancePrice + "']";
        selenium.clickOn(By.xpath(xpathLocator));
    }

    /**
     * Click on OK button after select balance amount
     *
     * @throws InterruptedException
     */

    public void clickOnOKBtn() throws InterruptedException {
        selenium.pageScrollInView(okButton);
        selenium.clickOn(okButton);
    }

    /**
     * Verify a nominal is a match between nominal and selected balance amount
     *
     * @return nominalSaldoText
     */

    public boolean verifyBuySaldo(String text) {
        return selenium.getText(nominalSaldoText).contains(text);
    }


    public void clickOnSwitchBtn() throws InterruptedException {
        selenium.pageScrollInView(switchOnButton);
        selenium.clickOn(switchOnButton);
    }

    /**
     * Click on Alokasi button
     *
     * @throws InterruptedException
     */
    public String getEditAlokasiPopUpMessage() throws InterruptedException {
        return selenium.getText(editAlokasiPopUpMessage);
    }

    /**
     * Click on Batal button
     *
     * @throws InterruptedException
     */
    public void clickOnBatalBtn() throws InterruptedException {
        selenium.clickOn(batalPopUpButton);

    }

    /**
     * Click on Tambah Saldo button
     *
     * @throws InterruptedException
     */
    public void clickOnTambahSaldoBtn() throws InterruptedException {
        try {
            if (tambahSaldoButton.isDisplayed()) {
                selenium.clickOn(tambahSaldoButton);
            }
        } catch (Exception e) {
            selenium.clickOn(tambahSaldoButtonIfBuyPremium);
        }
    }

    /**
     * Click on Batal Saldo button
     *
     * @throws InterruptedException
     */
    public void clickOnBatalSaldoBtn() throws InterruptedException {
        selenium.pageScrollInView(batalSaldoButton);
        selenium.clickOn(batalSaldoButton);
    }

    /**
     * Get Tambah Saldo text from Tambah Saldo button
     *
     * @throws InterruptedException
     */
    public String getTambahSaldoText() {
        return selenium.getText(tambahSaldoButton);
    }

    /**
     * Click on ok Allocated button
     *
     * @throws InterruptedException
     */
    public void clickOnOkAllocatedBtn() throws InterruptedException {
        selenium.clickOn(okAllocatedButton);

    }

    /**
     * Click on Back to list iklan button
     *
     * @throws InterruptedException
     */
    public void clickOnBacktolistiklanBtn() throws InterruptedException {
        selenium.clickOn(backtolistiklanButton);
    }

    /**
     * Click on Verify Allocated Saldo button
     *
     * @throws InterruptedException
     */
    public String getVerifyAllocatedSaldo() {
        return selenium.getText(VerifyAllocatedSaldoButton);
    }

    /**
     * Is display loading
     *
     * @throws InterruptedException
     */
    public String getDisplayLoading() {
        return selenium.getText(IsDisplayLoading);
    }

    /**
     * Click on menu kos
     *
     * @throws InterruptedException
     */
    public void clickOnKosMenu() throws InterruptedException {
        selenium.javascriptClickOn(kosMenu);
        int i = 0;
        while (isLoadingAnimationPresent()) {
            selenium.hardWait(2);
            if (!isLoadingAnimationPresent()) {
                break;
            }
            else if (i == 10){
                break;
            }
            i++;
        }
    }

    /**
     * Get Premium Warning tambah saldo/buy premium before confirmed previous buy process
     *
     * @throws InterruptedException
     */
    public String getPremiumWarningTambahSaldo() {
        return selenium.getText(premiumWarning);
    }

    /**
     * Click OK button on Pop up warning
     *
     * @throws InterruptedException
     */
    public void clickOnOKButtonPopUp() throws InterruptedException {
        selenium.clickOn(okButtonPopUp);
    }

    /**
     * Click on Premium button
     *
     * @throws InterruptedException
     */
    public void clickOnPremiumButton() throws InterruptedException {
        selenium.clickOn(upgradePremiumButton);
    }

    public void clickOnLihatSelengkapnyaButton() throws InterruptedException {
        selenium.clickOn(lihatselengkapnyaButton);
    }
    /**
     * no found lihat selengkapnya button
     *
     * @throws InterruptedException
     */
    public boolean NoFoundLihatSelengkapnyaButton() throws InterruptedException {
        return selenium.waitInCaseElementVisible(noFoundLihatselengkapnyaButton, 5) != null;
    }

    public void clickOnAturPremiumButton() throws InterruptedException {
        selenium.pageScrollInView(aturPremiumButton);
        selenium.clickOn(aturPremiumButton);
        selenium.switchToWindow(2);
    }


    public void clickOnAllocationField(String saldo) throws InterruptedException {
        selenium.clickOn(allocationField);
        allocationField.sendKeys(Keys.CONTROL, "a");
        selenium.enterText(allocationField, saldo, false);
    }

    /**
     * Get Tidak Text on tingkatkan klik toggle
     *
     * @return tingkatkanKlikMessageTidak
     */
    public String getTextTingkatkanKlikTidak() {
        return selenium.getText(tingkatkanKlikMessageTidak);
    }

    public void clickOnAllocationField() throws InterruptedException {
        selenium.clickOn(allocationField);
        selenium.enterText(allocationField, "5000", false);

    }

    public void clickOnkonfirmasiAllocationButton() throws InterruptedException {
        selenium.clickOn(konfirmasiAllocationButton);
    }

    public String getTextKelolaAlokasiSaldo() {
        selenium.pageScrollInView(kelolaAlokasiSaldoButton);
        return selenium.getText(kelolaAlokasiSaldoButton);
    }

    public void clickOnkelolaAlokasiSaldoButton() throws InterruptedException {
        selenium.pageScrollInView(kelolaAlokasiSaldoButton);
        selenium.clickOn(kelolaAlokasiSaldoButton);
    }

    public String getTextminimumAlokasiSaldoRp5000() {
        return selenium.getText(minimumAlokasiSaldoRp5000Text);

    }

    public void clickOnbacktoAlokasiSaldoButton() throws InterruptedException {
        selenium.clickOn(backtoAlokasiSaldoButton);
    }

    /**
     * Click on Konfirmasi Pembayaran button
     *
     * @throws InterruptedException
     */
    public void clickOnKonfirmasiPembayaranButton() throws InterruptedException {
        selenium.clickOn(konfirmasiPembayaranButton);
    }

    /**
     * Verify bayar button is appear
     *
     * @return VerifyBayarButton
     */
    public boolean isBayarButtonVisible() throws InterruptedException {
        return selenium.isElementPresent(bayarButton);
    }

    /**
     * Click on Ubah button
     *
     * @throws InterruptedException
     */
    public void clickOnUbahButton() throws InterruptedException {
        selenium.clickOn(ubahButton);
    }

    /**
     * Click Beli button on package number x
     *
     * @throws InterruptedException
     */
    public void clickBeliButton(String text) throws InterruptedException {
        selenium.pageScrollInView(packagelistSection);
        WebElement xpathLocator = driver.findElement
                (By.cssSelector(".package-option-item:nth-child(" + text + ") #buyPremiumButton"));
        selenium.clickOn(xpathLocator);
    }

    /**
     * Click on balance amount will be buy owner
     *
     * @return nominalPremiumText
     */
    public boolean verifyBuyPremium(String text) {
        return selenium.getText(nominalPremiumText).contains(text);
    }

    /**
     * Click on konfirmasi saldo
     *
     * @throws InterruptedException
     */
    public void clickOnKonfirmasi() throws InterruptedException {
        selenium.clickOn(konfirmasiSaldoButton);
    }

    /**
     * Select on Balance Package
     *
     * @throws InterruptedException
     */
    public void clickOnBalancePackage(String balanceAmount, String balancePrice) throws InterruptedException {
        String xpathLocator = "//div[.='Saldo " + balanceAmount + "Rp " + balancePrice + "']";
        selenium.clickOn(By.xpath(xpathLocator));
    }

    /**
     * Click on Batal button on pop up verifikasi OFF iklan teratas
     *
     * @throws InterruptedException
     */
    public void clickOnBatalNonAktifIklanTeratas() throws InterruptedException {
        selenium.clickOn(batalNonAktifIklanTeratasButton);
    }

    /**
     * Click on checkbox Jangan tampilkan ini kembali
     *
     * @throws InterruptedException
     */
    public void clickOnCheckbox() throws InterruptedException {
        selenium.clickOn(checkboxNonAktifIklanTeratas);
    }

    /**
     * Click on Atur Premium button on dashboard
     *
     * @throws InterruptedException
     */
    public void clickAturPremiumDashboard() throws InterruptedException {
        selenium.clickOn(aturPremiumButtonDashboard);
    }

    /**
     * Verify message warning pop up
     *
     * @return messafe warning
     */
    public String getTextWarningMessagePopUp() {
        return selenium.getText(messageWarningPopUp);
    }

    /**
     * Click on X warning pop up
     *
     * @throws InterruptedException
     */
    public void clickOnXWarningPopUp() throws InterruptedException {
        selenium.clickOn(closeWarningPopUpButton);
    }

    /**
     * Verify Booking Langsung pop up is appear or not
     *
     * @return pop up is appear
     */
    public boolean isBookingLangsungPopUpAppear() {
        return selenium.waitInCaseElementVisible(bookingLangsungPopUp, 3) != null;
    }

    /**
     * Click on close X booking langsung pop up
     *
     * @throws InterruptedException
     */
    public void clickOnCloseBookingLangsungPopUp() throws InterruptedException {
        selenium.clickOn(closeBookingLangsungPopUp);
        selenium.hardWait(5);
    }

    /**
     * Click on Properti Saya menu
     *
     * @throws InterruptedException
     */
    public void clickOnpropertySayaMenu() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(propertySaya);
    }

    /**
     * Get Statistik
     *
     * @return Statistik
     * <p>
     * <p>
     * /**
     * Click on Statistik Menu
     * @throws InterruptedException
     */
    public void clickOnStatistikMenu() throws InterruptedException {
        selenium.clickOn(statistikMenu);
    }

    /**
     * Get GoldPlus Statistik Text
     *
     * @return GoldPlus Statistik Text
     */
    public void clickOngoldplusStatistik() throws InterruptedException {
        selenium.clickOn(goldplusStatistik);
    }

    /**
     * Get Lihat Detail Statistik
     *
     * @return Lihat Detail Statistik
     */
    public void clickOnlihatdetailStatistik() throws InterruptedException {
        selenium.clickOn(lihatdetailStatistik);
    }

    /**
     * Get Statistik Kunjungan kos Text
     *
     * @return Statistik Kunjungan kos Text
     */
    public void clickOnstatistikKunjungan() throws InterruptedException {
        selenium.clickOn(statistikKunjungan);
    }

    /**
     * Get Statistik Chat Masuk Text
     *
     * @return Statistik Chat Masuk Text
     */
    public void clickOnstatistikChat() throws InterruptedException {
        selenium.pageScrollInView(statistikChat);
        selenium.clickOn(statistikChat);
    }

    /**
     * Get Statistik Chat Masuk Text
     *
     * @return Statistik Chat Masuk Text
     */
    public void clickOnstatistikPeminat() throws InterruptedException {
        selenium.pageScrollInView(statistikPeminat);
        selenium.clickOn(statistikPeminat);
    }
    /**
     * Get  Statistik Text
     *
     * @return Statistik Text
     */
    public String getStatistikText() {
        return selenium.getText(statistikText);
    }
    /**
     * Get Description Statistik Text
     *
     * @return Description Statistik Text
     */
    public String getDesStatistikText() {
        return selenium.getText(emptyDescStatistik);
    }

    /* Click On Kelola Promo Button
     *
             * @throws InterruptedException
     */
    public void clickOnaKelolaPromoBtn() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.pageScrollInView(kelolaPromoButton);
        selenium.clickOn(kelolaPromoButton);
    }

    /**
     * Enter Promo Text
     *
     * @throws InterruptedException
     */
    public void enterJudulPromo(String judulPromo) throws InterruptedException {
        selenium.clickOn(enterPromoTextField);
        selenium.enterText(enterPromoTextField, judulPromo, false);

    }
    /**
     * Enter Detail Promo
     *
     * @throws InterruptedException
     */
    public void enterDetailPromo(String detailPromo) throws InterruptedException {
        selenium.clickOn(enterDetailPromoTextField);
        selenium.enterText(enterDetailPromoTextField, detailPromo, false);

    }
    /**
     * Click Pasang Promo
     *
     * @throws InterruptedException
     */
    public void clickPasangPromoBtn() throws InterruptedException {
        selenium.pageScrollInView(pasangPromoButton);
        selenium.clickOn(pasangPromoButton);
    }
    public void clickOnstartDateBtn() throws InterruptedException {
        selenium.clickOn(startDateButton);

    }
    public void clickOnendDateBtn() throws InterruptedException {
        selenium.clickOn(endDateButton);

    }
    public String getStatusText() {
        selenium.pageScrollInView(statusPromo);
        return selenium.getText(statusPromo);

    }
    public void chooseNextDay() throws InterruptedException {
        DateFormat dateFormat = new SimpleDateFormat("dd");
        Date date = new Date();
        String date1 = dateFormat.format(date);
        String xpathLocator = "//div[@class='dropdown is-active is-mobile-modal']//a[contains(.,'" + Integer.sum(Integer.parseInt(date1),1) + "')]";
        selenium.clickOn(By.xpath(xpathLocator));
    }

    /**
     * Check if loading animation present
     * @return true if loading present. Otherwise false
     */
    private boolean isLoadingAnimationPresent() {
        return selenium.waitInCaseElementVisible(loadingAnimation, 2) != null;
    }
    /**
     * Check  dropdown property saya
     * @return true if dropdown menu showing. Otherwise false
     */
    public boolean isPropertyMenuDropdownShowing(){
        return selenium.waitInCaseElementVisible(propertySayaDropdownMenu, 3) != null;
    }

}



