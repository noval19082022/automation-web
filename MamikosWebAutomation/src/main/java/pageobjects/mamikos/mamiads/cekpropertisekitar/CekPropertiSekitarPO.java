package pageobjects.mamikos.mamiads.cekpropertisekitar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class CekPropertiSekitarPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public CekPropertiSekitarPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//p[.='Cek Properti Sekitar']")
    WebElement cekPropertiSekitarMenu;

    /**
     * Click 'Naikkan Iklan' button on mamiads page
     *
     * @throws InterruptedException
     */
    public void clickOnCekPropertiSekitarMenu() throws InterruptedException {
        selenium.waitInCaseElementVisible(cekPropertiSekitarMenu, 5);
        selenium.clickOn(cekPropertiSekitarMenu);
    }

}
