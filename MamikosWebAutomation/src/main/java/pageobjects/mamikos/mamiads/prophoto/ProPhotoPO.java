package pageobjects.mamikos.mamiads.prophoto;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ProPhotoPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ProPhotoPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//h1[@class='pkg-title']")
    WebElement PilihProPhotoTitle;


    public String getTextTitlePilihProPhoto() {
        return selenium.getText(PilihProPhotoTitle);
    }

    public void clickOnBeliProPhoto(String proPhotoPackageName) throws InterruptedException {
        String xpathLocator = "(//*[.='"+ proPhotoPackageName +"']/parent::*/following-sibling::*//a)[1]";
        selenium.waitInCaseElementVisible(By.xpath(xpathLocator), 5);
        selenium.clickOn(By.xpath(xpathLocator));
    }

}
