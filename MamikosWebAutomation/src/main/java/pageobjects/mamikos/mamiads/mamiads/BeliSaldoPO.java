package pageobjects.mamikos.mamiads.mamiads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class BeliSaldoPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private String rincianPembayaran;
    private static String saldo, totalPembayaran;

    public BeliSaldoPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[contains(text(),'Bayar Sekarang')]")
    WebElement bayarSekarangButton;

    @FindBy(xpath = "//*[ text() = 'Detail Tagihan']")
    private WebElement detailTagihanTitle;

    @FindBy(className = "back-btn")
    private WebElement backButton;

    @FindBy(xpath = "//a[.='back back']")
    private WebElement backButtonOnBalanceList;

    @FindBy(xpath = "//div[@id='invoiceVoucherInput']//p[@class='bg-c-text bg-c-text--body-2 ']")
    private WebElement voucherCodeApplied;

    @FindBy(css = ".bg-c-text--heading-1")
    private WebElement invoiceTitle;

    @FindBy(xpath = "//*[@class='invoice-detail-wrapper']/following::p")
    private List<WebElement> rincianPembayaranInvoice;

    @FindBy(id = "invoiceDetailPayment")
    WebElement rincianPembayaranText;

    @FindBy(xpath = "//*[contains(text(),'Selesai')]")
    WebElement selesaiButton;

    /**
     * user choose saldo
     * @param saldo type saldo
     * @throws InterruptedException
     */
    public void chooseSaldo(String saldo) throws InterruptedException {
        setSaldo(saldo);
        selenium.hardWait(15);
        By element = By.xpath("//*[contains(text(),'"+saldo+"')]/following-sibling::button");
        selenium.clickOn(element);
    }

    /**
     * user get saldo
     * @return 6.000
     */
    public String getSaldo() {
        return BeliSaldoPO.saldo.replaceAll("Rp","");
    }

    /**
     * user set saldo from input
     * @return Rp6.000
     */
    public void setSaldo(String saldo) {
        BeliSaldoPO.saldo = saldo;
    }

    /**
     * click button back
     *
     * @throws InterruptedException
     */
    public void clickOnBackButton() throws InterruptedException {
            selenium.waitInCaseElementVisible(backButton, 3);
            selenium.clickOn(backButton);
    }

    /**
     * click on bayar button
     *
     * @throws InterruptedException
     */
    public void clickOnBayarButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(bayarSekarangButton,3);
        selenium.clickOn(bayarSekarangButton);
        selenium.hardWait(3);
    }

    /**
     * get text detail tagihan
     *
     */
    public String getDetailTagihanText() {
        return selenium.getText(detailTagihanTitle);
    }

    /**
     * Check if favorit saldo is displayed
     * @return true if element present otherwise false
     */
    public boolean favoriteSaldo (String saldo){
        By element = By.xpath("//*[contains(text(),'"+saldo+"')]/following-sibling::div");
        return selenium.isElementDisplayed(driver.findElement(element));
    }

    /**
     * Get list saldo price, price in rupiah, discount, discount price
     * @param index input with listSaldo
     * @return String saldo price, price in rupiah, discount, discount price
     */
    public String listSaldo (String listSaldo, int index){
        String element = "";
        switch (listSaldo){
            case "price"     : element = "balance-list-item__name"; break;
            case "priceInRp" : element = "balance-list-item__price"; break;
            case "disc"      : element = "percentage"; break;
            case "discPrice" : element = "amount"; break;
        }
        return selenium.getText(driver.findElements(By.className(element)).get(index));
    }

    /**
     * Get detail tagihan
     * @param validasi
     *  <p> 1 = Saldo Pilihan Anda
     *  <p> 2 = Saldo Rincian Pembayaran
     *  <p> 3 = Nominal Rincian Pembayara
     *  <p> 4 = Total Pembayaran
     *  <p> 5 = Button Bayar Sekarang
     * @return String
     */
    public String detailTagihan(int validasi){
        String element = "";
        switch (validasi){
            case 1 : element = "//*[@class='chosen-subtitle']"; break;
            case 2 : element = "//*[@class='detail-subtitle']"; break;
            case 3 : element = "//*[@class='detail-info'] //*[@class='nominal']"; break;
            case 4 : element = "//*[@class='total-purchase'] //*[@class='nominal']"; break;
            case 5 : element = "//*[@class='purchase-detail__footer'] "; break;
        }
        return selenium.getText(By.xpath(element));
    }

    /**
     * Get pembayaran
     *  <p> - No. Invoice
     *  <p> - Jenis Pembayaran
     *  <p> - Metode Pembayaran
     * @return String
     */
    public String getPembayaranText(String text) throws InterruptedException {
        selenium.hardWait(3);
        if(text.contains("Status Transaksi")) {
            return selenium.getText(By.cssSelector(".bg-c-label"));
        } else if (text.contains("Total Pembayaran")) {
            return selenium.getText(By.cssSelector(".invoice-total-amount"));
        } else {
            return selenium.getText(By.xpath("//*[contains(text(),'"+text+"')]/../../following-sibling::div"));
        }
    }

    public String getTotalPembayaran() {
        return totalPembayaran;
    }

    public void setTotalPembayaran(String totalPembayaran) throws InterruptedException {
        selenium.hardWait(3);
        BeliSaldoPO.totalPembayaran = totalPembayaran;
    }

    /**
     * save all text in rincian pembayaran
     * @return all text in rincian pembayaran can be get with getAllRincianPembayaran
     */
    public String setAllRincianPembayaran() {
        try{
            return this.rincianPembayaran = selenium.getText(rincianPembayaranText);
        } catch (org.openqa.selenium.TimeoutException e){
            return this.rincianPembayaran = selenium.getText(driver.findElements(By.id("invoiceDetailPayment")).get(1));
        }
    }

    /**
     * get rincian pembayaran
     * @return rincian pembayaran
     */
    public String getAllRincianPembayaran() {
        return rincianPembayaran;
    }

    /**
     * Verify selesai button available
     * @return true if available
     */
    public boolean isSelesaiButtonAvailable() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.waitInCaseElementVisible(selesaiButton,3) != null;
    }

    /**
     * Get voucher applied
     * @param text
     *
     */
    public String getVoucherApplied(String text){
        if (text.contains("Voucher")){
            return selenium.getText(voucherCodeApplied);
        } else {
            return selenium.getText(By.cssSelector(".invoice-total-amount"));
        }
    }

    /**
     * Validate rincian pembayaran
     * @param index
     *
     */
    public String getRincianPembayaran(int index) {
        return selenium.getText(rincianPembayaranInvoice.get(index));
    }

    /**
     * Get metode pembayaran
     * @param metodePembayaran
     *
     */
    public String getMetodePembayaran(String metodePembayaran) {
        return selenium.getText(By.cssSelector("div:nth-of-type(5) .bg-c-text--body-2"));
    }
}