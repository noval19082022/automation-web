package pageobjects.mamikos.mamiads.mamiads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BeliSaldoAdminPagePO {
    WebDriver driver;
    SeleniumHelpers selenium;
    public String invoiceNumberMamiads = "";


    public BeliSaldoAdminPagePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[contains(text(),'Beli Saldo')]")
    WebElement beliSaldoButton;

    @FindBy(xpath = "//*[contains(text(),'Bayar Sekarang')]")
    WebElement bayarSekarangButton;

    @FindBy(name = "search_value")
    WebElement invoiceNumberTextBox;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-md']")
    WebElement cariInvoiceButton;

    @FindBy(xpath = "//a[.='Reset']")
    WebElement resetButton;

    @FindBy(xpath = "//*[contains(text(),'Premium Invoice')]")
    private WebElement premiumMenu;

    @FindBy(xpath = "//*[@href='/backoffice/invoice/premium/package']")
    private WebElement packageInvoiceListMenu;

    @FindBy(xpath = "//*[ text() = 'Detail Tagihan']")
    private WebElement detailTagihanTitle;

    @FindBy(className = "back-btn")
    private WebElement backButton;

    @FindBy(xpath = "//a[.='back back']")
    private WebElement backButtonOnBalanceList;

    @FindBy(xpath = "//*[@class='box-footer']")
    private WebElement emptyTable;

    @FindBy(xpath = "(//td)[2]")
    private WebElement invoiceNumberField;

    @FindBy(xpath = "//*[@class='label   label-default  ']")
    private WebElement invoiceStatusField;

    @FindBy(xpath = "//input[@name='schedule_date_from']")
    private WebElement inputDateForm;

    @FindBy(xpath = "//input[@name='schedule_date_to']")
    private WebElement inputDateTo;


    /**
     * click and search by
     * @param searchBy type of search by
     * @throws InterruptedException
     */
    public void chooseSearchBy(String searchBy) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//option[.='"+searchBy+"']"));
        selenium.clickOn(element);
    }

    /**
     * user input phone number
     *
     * @throws InterruptedException
     */
    public void inputPhoneNumber(String _ownerNumber_payment) throws InterruptedException {
        selenium.hardWait(2);
        selenium.enterText(invoiceNumberTextBox,_ownerNumber_payment,true);
    }

    /**
     * field package invoice list
     *
     * @throws InterruptedException
     */
    public void verifyListShortByPhoneNumber(String _ownerNumber_payment) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("(//*[contains(text(),'"+_ownerNumber_payment+"')])[1]"));
        selenium.isElementDisplayed(element);
    }

    /**
     * user input invalid phone number
     *
     * @throws InterruptedException
     */
    public void inputInvalidPhoneNumber(String _ownerNumber_payment) throws InterruptedException {
        selenium.hardWait(2);
        selenium.enterText(invoiceNumberTextBox,_ownerNumber_payment+"1",true);
    }

    /**
     * field package invoice list
     *
     * @throws InterruptedException
     */
    public void verifyListShortByInvalidPhoneNumber() throws InterruptedException {
        selenium.isElementDisplayed(emptyTable);
    }

    /**
     * click and filter by status
     * @param status type of satus
     * @throws InterruptedException
     */
    public void chooseByStatus(String status) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//option[.='"+status+"']"));
        selenium.clickOn(element);
    }

    /**
     * table package invoice list after choose filter by status unpaid
     *
     * @throws InterruptedException
     */
    public void verifyListFilterByStatusUnpaid(String status) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//span[text()='"+status+"']"));
        selenium.isElementDisplayed(element);
    }



    /**
     * search invoice premium package
     *
     * @throws InterruptedException
     */
    public void searchInvoice() throws InterruptedException {
        selenium.enterText(invoiceNumberTextBox,invoiceNumberMamiads,true);
        selenium.clickOn(cariInvoiceButton);
        selenium.hardWait(3);
    }

    /**
     * click button search invoice premium package
     *
     * @throws InterruptedException
     */
    public void clickOnSearchInvoiceButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.javascriptClickOn(cariInvoiceButton);
    }

    /**
     * search invoice premium package
     *
     * @throws InterruptedException
     */
    public void verifyStatus(String status) throws InterruptedException {
        By element = By.xpath("//*[text()='"+status+"']");
        selenium.waitInCaseElementVisible(element,2);
    }

    /**
     * Click on Package Invoice List Menu From Left Bar
     * @throws InterruptedException
     */
    public void clickOnPackageInvoiceListMenu() throws InterruptedException {
        selenium.clickOn(premiumMenu);
        selenium.clickOn(packageInvoiceListMenu);
        selenium.hardWait(3);
    }

    /**
     * get invoice number after search invoice number
     * @throws InterruptedException
     */
    public String getInvoiceNumber() throws InterruptedException{
        selenium.hardWait(2);
        return selenium.getText(invoiceNumberField);
    }

    /**
     * user input valid invoice number
     *
     * @throws InterruptedException
     */
    public void inputInvoiceNumber(String invoiceNumber) throws InterruptedException {
        selenium.hardWait(2);
        selenium.enterText(invoiceNumberTextBox,invoiceNumber,true);
    }

    /**
     * user input invalid invoice number
     *
     * @throws InterruptedException
     */
    public void inputInvalidInvoiceNumber(String invoiceNumber) throws InterruptedException {
        selenium.hardWait(2);
        selenium.enterText(invoiceNumberTextBox,invoiceNumber+"1",true);
    }

    /**
     * click button reset on premium package page
     *
     * @throws InterruptedException
     */
    public void clickOnResetButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(resetButton);
    }

    /**
     * user input gp invoice number
     *
     * @throws InterruptedException
     */
    public void inputGpInvoiceNumber(String gpInvoiceNumber) throws InterruptedException {
        selenium.hardWait(2);
        selenium.enterText(invoiceNumberTextBox,gpInvoiceNumber,true);
    }

    /**
     * get invoice status expired invoice number
     * @throws InterruptedException
     */
    public String getStatus() throws InterruptedException{
        selenium.hardWait(2);
        return selenium.getText(invoiceStatusField);
    }

    /**
     * user input due date from and to
     * @throws InterruptedException
     */

    public void userInputDueDate (String dueDatefrom, String dueDateTo) throws  InterruptedException {
        selenium.enterText(inputDateForm, dueDatefrom, true);
        selenium.enterText(inputDateTo, dueDateTo, true);
    }

    public void  systemDisplayInvoiceBasedOnExpiryDate (String expiryDate) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//td[.='"+expiryDate+"']"));
        selenium.waitTillElementIsVisible(element);
    }
}
