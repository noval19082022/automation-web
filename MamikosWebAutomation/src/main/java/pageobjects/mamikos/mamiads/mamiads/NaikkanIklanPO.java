package pageobjects.mamikos.mamiads.mamiads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class NaikkanIklanPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public NaikkanIklanPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * xpath as attributes.
     */

    @FindBy(css = ".bg-c-modal__body-title")
    WebElement titlePopUp;

    @FindBy(css = ".bg-c-modal__body-description")
    WebElement messagePopUp;

    @FindBy(css = ".bg-c-switch")
    WebElement posisiIklanToggle;

    @FindBy(xpath = "//*[@class='bg-c-toast__content']")
    WebElement messageOnOffToast;

    @FindBy(xpath = "//span[@class='hidden-xs']")
    WebElement saldoMamiAdsText;

    @FindBy(xpath = "//a[.='Naikkan Iklan']")
    WebElement naikkanIklanButton;

    @FindBy(xpath = "//h1[.='MamiAds']")
    WebElement mamiAdsText;

    @FindBy(xpath = " (//*[@class='btn btn-mamiorange track-owner-upgrade'])")
    WebElement naikanIklanPropertySayaButton;

    @FindBy(id = "filter-status-dropdown")
    WebElement defaultFilterMamiAds;

    @FindBy(xpath = "//h3[@class='bg-c-modal__body-title']")
    WebElement titlePopUpConfirmation;

    @FindBy(css = ".description.bg-c-text.bg-c-text--body-2")
    WebElement descPopUpConfirmation;

    @FindBy(css = ".bg-c-modal__action-closable > .bg-c-icon")
    private WebElement iconClose;

    @FindBy(css = ".amount")
    private WebElement saldoMamiAdsValue;

    @FindBy(id = "description-modal-toggle")
    private WebElement messagePopUpSaldoInsufficient;

    @FindBy(xpath = "//a[contains(.,'back')]")
    private WebElement backArrowButton;

    @FindBy(xpath = "//p[@class='mami-ads-f-a-q__head-title bg-c-text bg-c-text--heading-1 ']")
    WebElement titleText;

    @FindBy(xpath = "//p[@class='mami-ads-f-a-q__head-description bg-c-text bg-c-text--body-landing ']")
    WebElement subTitleText;

    /**
     * Get text title in Toast Message
     *
     * @return text title in Toast Message
     */
    public String getTextTitlePopUp() {
        selenium.waitInCaseElementVisible(titlePopUp, 5);
        return selenium.getText(titlePopUp);
    }

    /**
     * Get text message in Toast Message
     *
     * @return text message in Toast Message
     */
    public String getTextMessagePopUp(String text) {
        if (text.contains("menaikkan posisi iklan")) {
            return selenium.getText(messagePopUpSaldoInsufficient);
        } else {
            return selenium.getText(messagePopUp);
        }
    }

    /**
     * Click 'Naikkan Iklan' button on mamiads page
     *
     * @throws InterruptedException
     */
    public void clickPosisiIklanToggle() throws InterruptedException {
        selenium.waitInCaseElementVisible(posisiIklanToggle, 5);
        selenium.clickOn(posisiIklanToggle);
    }

    /**
     * Get posisi iklan (naik / tidak naik)
     *
     * @return posisi iklan
     * @params adsName, posisiIklan
     */
    public String getPosisiIklan(String adsName, String posisiIklan) throws InterruptedException {
        selenium.hardWait(3);
        String posisiIklanLocator = "//*[.='" + adsName + "']/parent::*/following-sibling::*//div[@id='ads-position-" + posisiIklan + "']";
        selenium.waitInCaseElementVisible(By.xpath(posisiIklanLocator), 5);
        return selenium.getText(By.xpath(posisiIklanLocator));
    }

    /**
     * Get message on toast success on off the iklan
     *
     * @return toast message
     */
    public String getTextMessageToastOnOffIklan() {
        selenium.waitInCaseElementVisible(messageOnOffToast, 5);
        return selenium.getText(messageOnOffToast);
    }

    public String getMessageTextPosisIklan(String propertyName) {
        String xpathLocator = "//*[.='" + propertyName + "']/parent::*/following-sibling::*//div[@class='ads-status__desc']";
        selenium.waitInCaseElementVisible(By.xpath(xpathLocator), 5);
        return selenium.getText(By.xpath(xpathLocator));
    }

    public String getTextSaldoMamiAds() {
        return selenium.getText(saldoMamiAdsText);
    }

    public String getTextMamiAdsTitle() {
        return selenium.getText(mamiAdsText);
    }

    /**
     * Click 'Naikkan Iklan' button on Properi Saya
     *
     * @throws InterruptedException
     */
    public void clickOnNaikkanIklanPropertiSayaButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(naikanIklanPropertySayaButton, 5);
        selenium.clickOn(naikanIklanPropertySayaButton);
    }

    /**
     * user can see default filter is "Semua Iklan"
     *
     * @throws InterruptedException
     */
    public String getTeksDefaultFilter() {
        return selenium.getText(defaultFilterMamiAds);
    }

    /**
     * Verify the status toggle iklan
     *
     * @return toggleStatus
     * @params adsName, toggleStatus
     */
    public boolean getToggleStatus(String adsName, String toggleStatus) {
        String toggleLocator = "//*[.='" + adsName + "']/parent::*/following-sibling::*//input[@id='room-toggle-switch-" + toggleStatus + "']";
        selenium.pageScrollInView(By.xpath(toggleLocator));
        return selenium.isElementDisplayed(driver.findElement(By.xpath(toggleLocator)));
    }

    /**
     * Get message of ads status
     *
     * @return adsStatus message
     * @params adsName
     */
    public String getAdsStatusDesc(String adsName) {
        String adsStatusLocator = "//*[.='" + adsName + "']/parent::*/following-sibling::*//div[@id='ads-status-description']";
        selenium.waitInCaseElementVisible(By.xpath(adsStatusLocator), 5);
        return selenium.getText(By.xpath(adsStatusLocator));
    }

    /**
     * Get message of anggaran description
     *
     * @return anggaranDesc message
     * @params adsName
     */
    public String getTextAnggaranDesc(String adsName) throws InterruptedException {
        selenium.hardWait(3);
        String anggaranDescLocator = "//*[.='" + adsName + "']/parent::*/following-sibling::*//div[@id='ads-allocation-description']";
        selenium.waitInCaseElementVisible(By.xpath(anggaranDescLocator), 5);
        return selenium.getText(By.xpath(anggaranDescLocator));
    }

    /**
     * Get message on pop up confirmation
     *
     * @return message on pop up confirmation
     * @params action
     */
    public String getTextSwitchTogglePopUp(String action) {
        if (action.equals("off")) {
            selenium.waitInCaseElementVisible(titlePopUpConfirmation, 3);
            return selenium.getText(titlePopUpConfirmation);
        } else {
            selenium.waitInCaseElementVisible(descPopUpConfirmation, 3);
            return selenium.getText(descPopUpConfirmation);
        }
    }

    /**
     * Click toggle ads
     *
     * @throws InterruptedException
     * @params toggleStatus, adsName
     */
    public void clickToggleTheAds(String toggleStatus, String adsName) throws InterruptedException {
        String switchToggleLocator;
        if (toggleStatus.equals("off")) {
            switchToggleLocator = "//*[.='" + adsName + "']/parent::*/following-sibling::*//input[@id='room-toggle-switch-off']";
        } else {
            switchToggleLocator = "//*[.='" + adsName + "']/parent::*/following-sibling::*//input[@id='room-toggle-switch-on']";
        }
        selenium.javascriptClickOn(driver.findElement(By.xpath(switchToggleLocator)));
    }

    /**
     * Click Aktifkan button
     *
     * @throws InterruptedException
     * @params actionButton
     */
    public void clickActionButtonInPopUp(String actionButton) throws InterruptedException {
        String actionButtonLocator = "//button[contains(text(), '" + actionButton + "')]";
        selenium.waitInCaseElementVisible(By.xpath(actionButtonLocator), 3);
        selenium.clickOn(By.xpath(actionButtonLocator));
    }

    /**
     * Verify the description full occupancy active ads
     *
     * @return message full occupancy
     * @params adsName
     */
    public String isFullOcuppancyActiveAds(String adsName) {
        String adsFullOccupancyActiveAds = "//*[.='" + adsName + "']/parent::*/following-sibling::*//div[@class='ads-status__kamar-penuh']";
        selenium.waitInCaseElementVisible(By.xpath(adsFullOccupancyActiveAds), 5);
        return selenium.getText(By.xpath(adsFullOccupancyActiveAds));
    }

    /**
     * Get list Ads on mamiAds dashboard page
     *
     * @return AdsName, posisiIklan, currentToggle, AvailableRoom, currentStatusDesc
     * @params listAds, index
     */
    public String listAds(String listAds, int index) {
        By element = null;
        switch (listAds) {
            case "adsName":
                element = By.cssSelector(".name");
                break;
            case "posisiIklan":
                element = By.xpath("//*[contains(@class, 'rainbow')]");
                break;
            case "currentToggleOn":
                element = By.xpath("//*[contains(@class, 'switch--on')]");
                break;
            case "currentToggleOff":
                element = By.xpath("//*[contains(@class, 'switch--off')]");
                break;
            case "availRoom":
                element = By.cssSelector(".ads-status__kamar-penuh");
                break;
            case "currentStatusDesc":
                element = By.id("ads-status-description");
                break;
        }
        return selenium.getText(driver.findElements(element).get(index));
    }

    /**
     * Verify the toggle on list ads
     *
     * @return status toggle (on/ off)
     * @params toggle, index
     */
    public boolean listAdsToggle(String toggle, int index) {
        return selenium.isElementDisplayed(driver.findElements(By.xpath("//*[contains(@class, 'switch--" + toggle + "')]")).get(index));
    }

    /**
     * Verify the description full occupancy
     *
     * @return message full occupancy
     * @params adsName
     */
    public String isFullOcuppancy(String adsName) throws InterruptedException {
        selenium.hardWait(3);
        String adsFullOccupancy = "//*[.='" + adsName + "']/parent::*/following-sibling::*//div[@class='full-room bg-c-label bg-c-label--rainbow bg-c-label--rainbow-grey']";
        selenium.waitInCaseElementVisible(By.xpath(adsFullOccupancy), 5);
        return selenium.getText(By.xpath(adsFullOccupancy));
    }

    /**
     * Click icon close
     *
     * @throws InterruptedException
     */
    public void clickIconClose() throws InterruptedException {
        selenium.clickOn(iconClose);
    }

    /**
     * get saldo mamiads text
     * convert from String (Rp1.000) to int (1000)
     *
     * @return int saldo mamiads
     */
    public int getSaldoMaText() {
        String saldo = selenium.getText(saldoMamiAdsValue).replaceAll("[^0-9]", "");
        return Integer.valueOf(saldo);
    }

    /**
     * click on back button arrow
     *
     * @throws InterruptedException
     */
    public void clickOnBackArrawButton() throws InterruptedException {
        selenium.clickOn(backArrowButton);
    }

    /**
     * Verify the toggle on current list saldo
     *
     * @return status current saldo
     * @params currentStatusSaldo
     */
    public String listCurrentStatusSaldo(String adsName) {
        String currentStatusSaldo = "//*[.='" + adsName + "']/parent::*/following-sibling::*//div[@id='ads-allocation-description']";
        WebElement myElement = driver.findElement(By.xpath(currentStatusSaldo));
        selenium.waitTillElementIsVisible(myElement);
        return selenium.getText(myElement);
    }

    /**
     * waiting for loading screen finished
     *
     * @throws InterruptedException
     */
    public void isLoadingFinished() throws InterruptedException {
        selenium.hardWait(10);
    }

    /**
     * Get Text title
     *
     * @return title
     */
    public String getTitleText() {
        selenium.pageScrollInView(titleText);
        return selenium.getText(titleText);
    }

    /**
     * Get Text sub title
     *
     * @return subtitle
     */
    public String getSubTitleText(){
        selenium.waitInCaseElementVisible(subTitleText, 5);
        return selenium.getText(subTitleText);
    }

    /**
     * Click On Text Qustion
     *
     * @return
     * @param
     */
    public void clickOnQustionText(String questionText) throws InterruptedException {
        selenium.pageScrollUsingCoordinate(5,5);
        String qustionTextLocator = "//p[contains(.,'" + questionText + "')]";
        WebElement myElement = driver.findElement(By.xpath(qustionTextLocator));
        selenium.waitTillElementIsVisible(myElement);
        selenium.clickOn(By.xpath(qustionTextLocator));
    }

    /**
     * Get Text Answer
     *
     * @return answerText
     * @param answerText
     */
    public String getAnswerText(String answerText) {
        String answerTextLocator = "//p[contains(.,'" + answerText + "')]";
        return selenium.getText(By.xpath(answerTextLocator));
    }

    /**
     * Get Title Pop up while appear beli saldo pop up
     *
     * @return title pop up confirmation
     */
    public String getTitleBeliSaldoPopUp() {
        return selenium.getText(titlePopUpConfirmation);
    }
}