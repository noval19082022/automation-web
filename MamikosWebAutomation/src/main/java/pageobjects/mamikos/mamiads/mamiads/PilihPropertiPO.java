package pageobjects.mamikos.mamiads.mamiads;

import org.openqa.selenium.By;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class PilihPropertiPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public PilihPropertiPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(tagName = "input")
    WebElement searchPilihPropertiTextBox;

    @FindBy(xpath = "//*[@class='property-list'] //*[contains(@class,'title')]")
    WebElement titleLabel;

    @FindBy(xpath = "//*[@class='property-list']//p")
    WebElement messageLabel;

    @FindBy(xpath = "//*[contains(@class,'unavailable')]")
    WebElement unavailableProperty;

    @FindBy(xpath = "//*[contains(@class,'property-list-item__card')]")
    WebElement listProperty;

    @FindBy(className = "information")
    WebElement propertyNameLabel;

    @FindBy(xpath = "//*[contains(@class,'back-icon')]")
    WebElement backButton;

    @FindBy(className = "properties")
    WebElement listAdvertisedProperties;

    @FindBy(xpath = "//u[.='Batalkan']")
    WebElement batalkanText;

    /**
     * Search property name with a keyword
     * @param keyword that want to search
     * @throws InterruptedException
     */
    public void enterTextToSearchProperti(String keyword) throws InterruptedException {
        selenium.hardWait(3);
        try{
            if (searchPilihPropertiTextBox.isDisplayed()) {
                Assert.assertEquals(getSearchPilihPropertiPlaceHolder(),"Nama properti yang dicari");
                selenium.enterText(searchPilihPropertiTextBox, keyword, true);
                selenium.clickOn(By.cssSelector(".bg-c-input__icon"));
                selenium.waitTillElementIsNotVisible(By.xpath("//*[contains(@class,'bg-c-skeleton')]"),30);
            }
        } catch (org.openqa.selenium.NoSuchElementException E){
            System.out.println("no property list available");
        }
    }

    /**
     * Get Search Properti PlaceHolder text
     * @return String
     */
    public String getSearchPilihPropertiPlaceHolder() {
        return selenium.getElementAttributeValue(searchPilihPropertiTextBox, "Placeholder");
    }

    /**
     * Get text title in Filter's result
     * @return text title in Filter's result
     */
    public String getTextTitle(){
        selenium.waitTillElementIsVisible(titleLabel,5);
        return selenium.getText(titleLabel);
    }

    /**
     * Get text message in Filter's result
     * @return text message in Filter's result
     */
    public String getTextMessage(){
        selenium.waitTillElementIsVisible(messageLabel,5);
        return selenium.getText(messageLabel);
    }

    /**
     * Check Unavailable Property is displayed
     * @return true / false
     */
    public boolean isPropertyListUnavailableVisible() {
        return selenium.waitInCaseElementVisible(unavailableProperty, 5) != null;
    }

    /**
     * Get text in List Property Unavailable
     * @return text in Property Unavailable
     */
    public String getTextPropertyUnavailable(){
        selenium.waitTillElementIsVisible(unavailableProperty,5);
        return selenium.getText(unavailableProperty);
    }

    /**
     * Get background color in List Property
     * @return background color in List Property
     */
    public String getBackgroundColorProperty(){
        return Color.fromString(listProperty.getCssValue("background-color")).asHex();
    }

    /**
     * Get text label in Label Property Name
     * @return text in label Property Name
     */
    public String getPropertyNameLabel() {
        selenium.waitTillElementIsVisible(propertyNameLabel,5);
        return selenium.getText(propertyNameLabel);
    }

    /**
     * Click on Back Button <-
     *
     * @throws InterruptedException
     */
    public void clickOnBackButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(backButton,5);
        selenium.clickOn(backButton);
    }
    /**
     * Get list of Advertised Properties
     * @return text Advertised Properties
     */

    public String getListAdvertisedProperties(){
        selenium.waitTillElementIsVisible(listAdvertisedProperties,5);
        return selenium.getText(listAdvertisedProperties);
    }

    public void clickOnBatalkanText() throws InterruptedException {
        selenium.waitInCaseElementClickable(batalkanText,5);
        selenium.clickOn(batalkanText);
    }

    /**
     * Check Unavailable Property is displayed
     * @return true / false
     */
    public boolean isBatalkanTextVisible() {
        return selenium.waitInCaseElementVisible(batalkanText, 5) != null;
    }
}
