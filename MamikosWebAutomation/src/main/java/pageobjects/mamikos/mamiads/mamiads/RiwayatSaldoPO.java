package pageobjects.mamikos.mamiads.mamiads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class RiwayatSaldoPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public RiwayatSaldoPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='transaction-done'] //h4")
    WebElement getTitleSelesaiTransaksi;

    @FindBy(xpath = "//*[@class='transaction-done'] //p")
    WebElement getMessageSelesaiTransaksi;

    @FindBy(xpath = "//*[@class='right-side-payment-status-paid']")
    WebElement lunastransactionText;

    @FindBy(css = "[href='#basic-back']")
    WebElement backRiwayatButton;

    @FindBy(css = ".history-icon__counter")
    WebElement countHistoryIcn;

    /**
     * Get Text title selesai transaksi
     *
     * @return title
     */
    public String getTextTitleSelesaiTransaksi(){
        return selenium.getText(getTitleSelesaiTransaksi);
    }

    /**
     * Get Text message selesai transaksi
     *
     * @return message
     */
    public String getTextMessageSelesaiTransaksi(){
        return selenium.getText(getMessageSelesaiTransaksi);
    }

    /**
     * Get detail tagihan
     * @param validasi
     *  <p> 1 = Nominal Saldo
     *  <p> 2 = Total Pembayaran
     *  <p> 3 = Status Transaksi
     * @return String
     */
    public String gettransactionList(int validasi){
        String element = "";
        switch (validasi){
            case 1 : element = "//*[@class='transaction-done'] //*[@class='left-side-saldo-status']"; break;
            case 2 : element = "//*[@class='transaction-done'] //*[@class='right-side-saldo-status']"; break;
            case 3 : element = "//*[@class='right-side-payment-status-paid']"; break;
        }
        selenium.waitTillElementIsVisible(driver.findElement(By.xpath(element)));
        return selenium.getText(By.xpath(element));
    }

    /**
     * user verify status transaction mamiads is "Lunas"
     * @return transaction Status Text
     */
    public String getTransactionStatusText() {
        return selenium.getText(lunastransactionText);
    }

    /**
     * Click on Back Riwayat Button
     * @throws InterruptedException
     */
    public void clickOnBackRiwaytButton() throws InterruptedException {
        selenium.clickOn(backRiwayatButton);
    }

    /**
     * Get count riwayat beli saldo
     * @return int countHistoryIcn
     */
    public Integer getCountRiwayatBeliSaldo() {
        return Integer.parseInt(selenium.getText(countHistoryIcn));
    }

    /**
     * Click on Transaction
     * @throws InterruptedException
     */
    public void clickOnTransaction(String number) throws InterruptedException {
        selenium.hardWait(3);
        String transactionElement = "//div[@class='transaction-on-process']//div["+number+"]/a";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(transactionElement)), 3);
        selenium.clickOn(By.xpath(transactionElement));
        selenium.hardWait(2);
    }

    /**
     * Verify text on new tab
     * @return element
     */
    public String getTextNewTab() {
        String element = "//p[@class='bg-c-text bg-c-text--heading-1 ']";
        return selenium.getText(By.xpath(element));
    }
}
