package pageobjects.mamikos.mamiads.mamiads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class QuickAllocatePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public QuickAllocatePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    /**
     * user verify the alokasi ads title
     * @return alokasi title
     * @param alokasiTitleText
     */
    public String getAlokasiTitleText(String alokasiTitleText) {
        String alokasiTitleElement = "//div[@class='kos-card'][1]//div[contains(@class,'alokasi-ads')]//div[contains(text(), '"+ alokasiTitleText+ "')]";
        selenium.waitTillElementIsVisible(By.xpath(alokasiTitleElement),2);
        return selenium.getText(driver.findElement(By.xpath(alokasiTitleElement)));
    }

    /**
     * user verify the alokasi ads title
     * @return alokasi title
     * @param toggleAdsStatus
     */
    public boolean getToggleAdsStatus(String toggleAdsStatus) {
        String toggleAdsElement = "//div[@class='kos-card'][1]//div[contains(@class,'alokasi-ads')]//div[@class='bg-c-switch alokasi-ads__toggle bg-c-switch--"+ toggleAdsStatus +"']";
        selenium.waitTillElementIsVisible(By.xpath(toggleAdsElement), 2);
        return selenium.isElementDisplayed(driver.findElement(By.xpath(toggleAdsElement)));
    }

    /**
     * user verify the alokasi ads title
     * @return alokasi title
     * @param adsDescText
     */
    public String getAdsDescText(String adsDescText) {
        String adsDescElement = "//div[@class='kos-card'][1]//div[contains(@class,'ads__des')]//div[contains(text(), '"+ adsDescText +"')]";
        selenium.waitInCaseElementVisible(By.xpath(adsDescElement), 2);
        return selenium.getText(driver.findElement(By.xpath(adsDescElement)));
    }
}
