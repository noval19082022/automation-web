package pageobjects.mamikos.mamiads.mamiads;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class UbahAnggaranPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public UbahAnggaranPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@class='bg-c-input__field']")
    WebElement anggaranTextBox;

    @FindBy(xpath = "//*[contains(@class,'mamiads-set')] //*[contains(text(),'Rp')]")
    WebElement anggaranTextBoxMessage;

    @FindBy(xpath = "//*[contains(@class, 'modal__content')]")
    WebElement formAanggaranPopUp;

    @FindBy (css = ".change-budget-validation-modal__action-confirm")
    WebElement yaGantiButton;

    @FindBy (xpath = "//div[@class='bg-c-radio']//span[@class='bg-c-radio__icon']/span[1]")
    WebElement metodeAnggaranButton;

    /**
     * Set Anggaran harian
     */
    public void setAnggaranHarianForm(String anggaran) {
        selenium.enterText(anggaranTextBox,anggaran,true);
    }

    /**
     * Get anggaran error messages text
     * @return string
     */
    public String getAnggaranTextBoxMessage(){
        return selenium.getText(anggaranTextBoxMessage);
    }

    /**
     * Verify the form anggaran is displayed
     * @return string
     */
    public boolean isFormAnggaranDisplayed() {
        return selenium.isElementDisplayed(formAanggaranPopUp);
    }

    /**
     * Ubah property
     * @return adsName
     */
    public void clickOnUbahAdsButton(String adsName) throws InterruptedException {
        String getUbahAdsName = "//div[@class='mami-ads-widget'] /div[contains(.,'"+adsName+"')] //a";
        selenium.waitInCaseElementVisible(By.xpath(getUbahAdsName), 3);
        selenium.clickOn(By.xpath(getUbahAdsName));
    }

    /**
     * Click on Ya,Ganti Button
     ** @throws InterruptedException
     */
    public void clickOnYaGantiButton() throws InterruptedException {
        selenium.clickOn(yaGantiButton);
    }

    /**
     * Click on Metode Anggaran, Saldo Maksimal and Dibatasi Harian
     ** @throws InterruptedException
     */
    public void clickOnMetodeAnggaranButton() throws InterruptedException {
        selenium.clickOn(metodeAnggaranButton);
    }
}
