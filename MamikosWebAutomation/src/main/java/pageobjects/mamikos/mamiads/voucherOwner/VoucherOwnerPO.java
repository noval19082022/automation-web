package pageobjects.mamikos.mamiads.voucherOwner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class VoucherOwnerPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public VoucherOwnerPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }
    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "[data-testid='codeVoucher_txt']")
    private WebElement voucherCodeField;

    @FindBy(css = "[data-testid='pakaiVoucher_btn']")
    private WebElement pakaiVoucherButton;

    @FindBy(xpath = "//div[@data-testid='warning_txt']")
    private WebElement warningMessageVoucher;

    @FindBy(xpath = "//*[@class='bg-c-icon bg-c-icon--md']")
    private WebElement icnButton;

    /**
     * Verify voucher pop up present
     * @return true if present
     */
    public boolean isVoucherPopUpPresent(String popUpString) {
        String popUpElement = "//div[.='" + popUpString + "']";
        return selenium.waitInCaseElementPresent(By.xpath(popUpElement), 3) != null;
    }

    /**
     * click on Masukkan button on pop up Voucher Anda
     * @param masukkanVoucher
     * @throws InterruptedException
     */
    public void clickOnMasukkanVoucher(String masukkanVoucher) throws InterruptedException {
        String masukkanVoucherElement = "//div[@class='c-title']/div[contains(.,'" + masukkanVoucher + "')]";
        selenium.clickOn(By.xpath(masukkanVoucherElement));
    }
    /**
     * Input voucher code
     * @param voucherCode
     *
     */
    public void inputVoucherCode(String voucherCode) {
        selenium.enterText(voucherCodeField, voucherCode, true);
    }

    /**
     * Click pakai voucher button
     * @throws InterruptedException
     *
     */
    public void clickOnPakaiVoucherButton() throws InterruptedException {
        selenium.clickOn(pakaiVoucherButton);
    }

    /**
     * Get message warning invalid input voucher
     * @return warning message
     *
     */
    public String getMessageWarningVoucher() {
        return selenium.getText(warningMessageVoucher);
    }

    /**
     * Clear voucher code text field
     *
     *
     */
    public void clearVoucherCode() {
        selenium.clearTextField(voucherCodeField);
    }

    /**
     * Click close icon on pop up
     * @throws InterruptedException
     *
     */
    public void clickOnIconClose() throws InterruptedException {
        selenium.clickOn(icnButton);
    }
}
