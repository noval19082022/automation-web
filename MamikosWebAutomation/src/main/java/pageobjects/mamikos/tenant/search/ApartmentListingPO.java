package pageobjects.mamikos.tenant.search;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.Collection;

public class ApartmentListingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ApartmentListingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "vacancyCriteria")
    private WebElement searchEditText;

    @FindBy(xpath = "//*[@class='input-group-btn']")
    private WebElement searchIcon;

    /**
     * Input text for search apartment
     * @param text keyword will input to element search
     * @throws InterruptedException
     */
    public void searchApartment(String text) throws InterruptedException {
        selenium.enterText(searchEditText, text, true);
        selenium.clickOn(searchIcon);
        selenium.hardWait(2);
    }

    /**
     * Get apartment details
     * @param indexData is the index of the data to retrieve the index value from the list you want to get the value
     * @throws InterruptedException
     */
    public String getApartmentDetails(int indexData) {
        String valueText = null;
        By element = null;
        element = By.xpath("(//*[@class='rc-info'])[" + indexData + "]");
        selenium.waitInCaseElementVisible(element, 10);
        selenium.pageScrollInView(element);
        WebElement addressDetail = selenium.waitInCaseElementVisible(element, 10);
        valueText = selenium.getText(addressDetail);
        return valueText;
    }
}
