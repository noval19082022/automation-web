package pageobjects.mamikos.tenant.search;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.Select;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.ArrayList;
import java.util.List;

public class SearchPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SearchPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(text(), 'Area')]/parent::li")
    private WebElement area;

    @FindBy(xpath = "//a[contains(text(), 'Stasiun & Halte')]/parent::li")
    private WebElement station;

    @FindBy(xpath = "//a[contains(text(), 'Kampus')]/parent::li")
    private WebElement campus;

    //Pop up I understand

    @FindBy(xpath = "//input[@class='form-control']")
    private WebElement searchTextBar;

    @FindBy(xpath = "//a[contains(text(),'Kos Yogyakarta')]")
    private WebElement kosJogjaButton;

    @FindBy(id= "landingConnector")
    private WebElement searchAnotherCategoryDropdown;

    @FindBy(id= "filterType")
    private WebElement kostTypeDropdown;

    @FindBy(id= "filterTime")
    private WebElement timePeriodDropdown;

    @FindBy(id= "filterSort")
    private WebElement sortingDropdown;

    @FindBy(css = "#suggestionsBox span")
    private WebElement exceptionText;

    @FindBy(css = ".field-reset")
    private WebElement resetIcon;

    @FindBy(css = ".search-box-modal__results:nth-child(3) .results-list:nth-child(2) .results-title")
    private WebElement firstResultNameText;

    @FindBy(xpath = "//*[@class = 'search-box-modal__results'][1]/*[@class = 'results-list track_search'][1]")
    private WebElement firstResultAreaText;

    @FindBy(xpath = "//[@class='bg-c-tag bg-c-tag--active bg-c-tag--lg dropdown-button--gender']")
    private WebElement kosTypeFilter;

    @FindBy(xpath = "(//div[@id='baseMainFilter'])[1]")
    private WebElement kosTypeFilter2;

    @FindBy(xpath = "//span[@role='text'][normalize-space()='Bulanan']")
    private WebElement timePeriodFilter;

    @FindBy(xpath = "(//div[@class='bg-c-dropdown'])[2]")
    private WebElement timePeriodFilter2;

    @FindBy(css = "#landingKostContainer .title-container h1")
    private WebElement titleKostList;

    @FindBy(css = "div #mamiMap")
    private WebElement mamiMap;

    @FindBy(css = "div #nominatimMap")
    private WebElement nomintaimMap;


    /**
     * Click on 'Search' bar
     * @throws InterruptedException
     */
    public void clickSearchBar() throws InterruptedException {
        selenium.clickOn(area);
    }

    /**
     * Click on 'Station & Stop' Tab
     * @throws InterruptedException
     */
    public void clickStationTab() throws InterruptedException {
        selenium.waitInCaseElementVisible(station, 10);
        selenium.clickOn(station);
    }

    // tap city name by dynamic xpath
    public void userTapCity(String city) throws InterruptedException {
        String xpathLocator = "//div/button[contains(.,'" + city + "')]";
        WebElement element = driver.findElement(By.xpath(xpathLocator));
        selenium.clickOn(element);
    }
    // Check element appear or not by text
    public boolean checkElementbyText(String city) {
        String xpathLocator = "//a[contains(text(), '" + city + "')]";
        WebElement element = driver.findElement(By.xpath(xpathLocator));
        return selenium.waitInCaseElementVisible(element, 3) != null;
    }

    /** List all popular search city
     * @return  popular city
     */
    public List<String> listPopularCity() {
        String xpathLocator = "//div[@class='pills']/button";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        List<String> popularCity = new ArrayList<>();
        for (WebElement s : elements) {
            popularCity.add((selenium.getText(s)).replaceAll("\\s", ""));
        }
        return popularCity;
    }

    /** Click one of city in popular search
     * @param city is name of city
     */
    public void clickPopularCity(String city) throws InterruptedException {
        String xpathLocator = "//div[@class='pills']//button[contains(text(), '" + city +"')]";
        selenium.clickOn(By.xpath(xpathLocator));
    }

    /** List all listing address in search result
     * @return list of address (String)
     */
    public List<String> listKostAddress() throws InterruptedException {
        List<String> addressList = new ArrayList<>();
        if (selenium.waitInCaseElementVisible(mamiMap, 3) != null) {
            String xpathLocator = "//span[contains(@class,'rc-info__name bg-c-text bg-c-text--title-4')]";
            selenium.hardWait(5);
            List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
            for (WebElement a : elements) {
                addressList.add((selenium.getText(a).toLowerCase()));
            }
        } else if(selenium.waitInCaseElementVisible(nomintaimMap, 3) != null){
            String xpathLocator = "//*[@class='rc-info']/child::span[1]";
            selenium.hardWait(5);
            List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
            for (WebElement a : elements) {
                addressList.add((a.getText().trim().toLowerCase()));
            }
        }
        return addressList;
    }

    /** List all listing facilities in search result
     * @return list of facilities (String)
     */
    public List<String> listKostFacilities() {
        String cssLocator = "div[class='rc-facilities rc-facilities--horizontal']";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.cssSelector(cssLocator));
        List<String> facilitiesList = new ArrayList<>();
        for (WebElement a : elements) {
            facilitiesList.add((selenium.getText(a)));
        }
        return facilitiesList;
    }

    /**
     * Get List of Popular Station Name is present
     * @param station Station Name
     * @return
     */
    public Boolean isPopularStationPresent(String station){
        return selenium.waitInCaseElementVisible(By.xpath("//button[contains(text(), '"+ station +"')]"), 5) != null;
    }

    /**
     * Get List of Popular Station Name Text
     * @param station Station Name
     * @return
     */
    public String getPopularStation(String station){
        return selenium.getText(By.xpath("//button[contains(text(), '"+ station +"')]"));
    }

    /**
     * Scroll down to 'City Name' and click 'City Name'
     * @param city City Name
     * @throws InterruptedException
     */
    public void clickOnCities(String city) throws InterruptedException {
        String cities = "//div/button[contains(.,'" + city + "')]";
        if (selenium.waitInCaseElementVisible(By.xpath(cities), 10) == null){
            selenium.pageScrollInView(By.xpath(cities));
        }
        selenium.clickOn(By.xpath(cities));
    }

    /**
     * Get List of each Station Name on Cities is Present
     * @param stationName Station Name
     * @return
     */
    public Boolean isEachStationFromCitiesPresent(String stationName){
        return selenium.waitInCaseElementVisible(By.xpath("//a[contains(text(), '" + stationName + "')]"), 5) != null;
    }

    /**
     * Get List of each Station Name on Cities Text
     * @param stationName Station Name
     * @return
     */
    public String getEachStationFromCities(String stationName){
        return selenium.getText(By.xpath("//a[contains(text(), '" + stationName + "')]"));
    }

    /**
     * Enter Text in search bar and select result
     * @param searchText is text we want to search
     */
    public void enterTextToSearchAndSelectResult(String searchText) {
        selenium.enterText(searchTextBar, searchText, false);
        String xpathLocator = "//label[contains(.,'" + searchText + "')]";
        By resultLocator = By.xpath(xpathLocator);
        selenium.waitTillElementsCountIsEqualTo(resultLocator, 1);
        selenium.waitTillAllElementsAreLocated(resultLocator).get(0).click();
    }
    
    /**
     * Click on 'Campus' Tab
     * @throws InterruptedException
     */
    public void clickOnCampusTab() throws InterruptedException {
        selenium.clickOn(campus);
    }

    /**
     * Get Lists of each Popular Campus Name
     * @param campus Campus Name
     * @return
     */
    public Boolean isPopularCampusPresent(String campus) {
        return selenium.waitInCaseElementVisible(By.xpath("//button[contains(text(), '"+ campus +"')]"), 5) != null;
    }

    /**
     * Get Lists of each Popular Campus Name Text
     * @param campus Campus Name
     * @return
     */
    public String getPopularCampus(String campus){
        return selenium.getText(By.xpath("//button[contains(text(), '"+ campus +"')]"));
    }

    /**
     * Get List of each Campus Name on Cities is Present
     * @param campus Station Name
     * @return
     */
    public Boolean isEachCampusFromCitiesPresent(String campus){
        return selenium.waitInCaseElementVisible(By.xpath("//a[contains(text(), '" + campus + "')]"), 5) != null;
    }

    /**
     * Get List of each Campus Name on Cities Text
     * @param campus Station Name
     * @return
     */
    public String getEachCampusFromCities(String campus){
        return selenium.getText(By.xpath("//a[contains(text(), '" + campus + "')]"));
    }

    /**
     * Click on 'Kos Jogja' button
     * @throws InterruptedException
     */
    public void clickOnKosJogjaButton() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0,2750);
        selenium.hardWait(5);
        selenium.clickOn(kosJogjaButton);
        selenium.switchToWindow(0);
    }

    /**
     * Click on 'Search Another Category' Dropdown
     * @throws InterruptedException
     */
    public void clickSearchAnotherCategoryDropdown() throws InterruptedException {
        selenium.clickOn(searchAnotherCategoryDropdown);
    }

    /** List all search another category
     * @return search another category
     */
    public List<String> searchAnotherCategory() {
        Select dropdown = new Select(driver.findElement(By.id("landingConnector")));
        List<WebElement> listCategory = dropdown.getOptions();
        List<String> anotherCategory = new ArrayList<>();
        for (WebElement s : listCategory) {
            anotherCategory.add((selenium.getText(s)).trim());
        }
        return anotherCategory;
    }

    /**
     * Click on Kost Type Dropdown
     * @throws InterruptedException
     */
    public void clickKostTypeDropdown() throws InterruptedException {
        selenium.clickOn(kostTypeDropdown);
    }

    /** List all kost type
     * @return kost type
     */
    public List<String> dropdownKostType() {
        Select dropdown = new Select(driver.findElement(By.id("filterType")));
        List<WebElement> listCategory = dropdown.getOptions();
        List<String> allKostType = new ArrayList<>();
        for (WebElement s : listCategory) {
            allKostType.add((selenium.getText(s)).trim());
        }
        return allKostType;
    }


    /**
     * Enter Text in search bar and select result
     * @param searchText is text we want to search
     */
    public void enterTextToSearchAndSelectResultCity(String searchText) {
        selenium.enterText(searchTextBar, searchText, false);
        String xpathLocator = "//label[contains(.,'" + searchText + "')]";
        By resultLocator = By.xpath(xpathLocator);
        selenium.waitTillAllElementsAreLocated(resultLocator).get(0).click();
    }

    /**
     * Click on Time Period Dropdown
     * @throws InterruptedException
     */
    public void clickOnTimePeriodDropdown() throws InterruptedException {
        selenium.clickOn(timePeriodDropdown);
    }

    /** List all time period
     * @return time period
     */
    public List<String> dropdownTimePeriod() {
        Select dropdown = new Select(driver.findElement(By.id("filterTime")));
        List<WebElement> listCategory = dropdown.getOptions();
        List<String> timePeriod = new ArrayList<>();
        for (WebElement s : listCategory) {
            timePeriod.add((selenium.getText(s)).trim());
        }
        return timePeriod;
    }

    /**
     * Click on Sorting Dropdown
     * @throws InterruptedException
     */
    public void clickSortingDropdown() throws InterruptedException {
        selenium.clickOn(sortingDropdown);
    }

    /** List all Sorting
     * @return Sorting
     */
    public List<String> sortingDropdown() {
        Select dropdown = new Select(driver.findElement(By.id("filterSort")));
        List<WebElement> listCategory = dropdown.getOptions();
        List<String> sorting = new ArrayList<>();
        for (WebElement s : listCategory) {
            sorting.add((selenium.getText(s)).trim());
        }
        return sorting;
    }

    /**
     * Get text from search bar
     * @return text inputed
     */
    public String getTextSearchBar() {
        return selenium.getText(searchTextBar);
    }

    /**
     * Get text result
     * @return text inputed
     */
    public String getSearchResult() {
        selenium.waitInCaseElementVisible(firstResultAreaText, 5);
        return selenium.getText(firstResultAreaText);
    }

    /** Click one of city
     * @param city is name of city
     *@throws InterruptedException
     */
    public void clickCity(String city) throws InterruptedException {
        String xpathLocator = "//div/button[contains(.,'" + city + "')]";
        selenium.clickOn(By.xpath(xpathLocator));
    }

    /** Click one of city in area search
     * @param campus is name of city
     *@throws InterruptedException
     */
    public void clickAreaBelow(String campus) throws InterruptedException {
        String xpathLocator = "//div[@class='accordion-wrapper']//a[contains(text(), '" + campus +"')]";
        selenium.clickOn(By.xpath(xpathLocator));
    }

    /** List all listing result
     * @return list of result (String)
     */
    public List<String> listResultKeyword() {
        String cssLocator = ".results-list label:nth-child(1)";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.cssSelector(cssLocator));
        List<String> addressList = new ArrayList<>();
        for (WebElement a : elements) {
            addressList.add((selenium.getText(a)));
        }
        return addressList;
    }

    /**
     * Get exception text
     * @return exception text
     */
    public String getExceptionText() {
        selenium.waitInCaseElementVisible(exceptionText, 3);
        return selenium.getText(exceptionText);
    }

    /**
     * Enter Text in search bar
     * @param word is text we want to search
     */
    public void enterRandomChar(String word) throws InterruptedException {
        selenium.enterText(searchTextBar, word, false);
        selenium.hardWait(2);
    }

    /**
     * Click the reset button
     * @throws InterruptedException
     */
    public void clickXIcon() throws InterruptedException {
        selenium.waitInCaseElementVisible(resetIcon, 3);
        selenium.clickOn(resetIcon);
    }

    /**
     * Check is the search bar is empty
     * @return true 
     */
    public boolean isSearchbarEmpty() {
        return selenium.getText(searchTextBar) == null;
    }

    /** List all listing result in name section
     * @return list of result (String)
     */
    public List<String> listResultNameKeyword() {
        String xpathLocator = "//div[@id='suggestionsBox']//div[3]/a[1]";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        List<String> nameList = new ArrayList<>();
        for (WebElement p : elements) {
            nameList.add((selenium.getText(p)));
        }
        return nameList;
    }

    /**
     * Click the first result based on name
     * @throws InterruptedException
     */
    public void clickTheFirstResultBasedOnName() throws InterruptedException {
        selenium.waitInCaseElementVisible(firstResultNameText,3);
        selenium.clickOn(firstResultNameText);
    }

    /** List all listing result in area section
     * @return list of result (String)
     */
    public List<String> listResultAreaKeyword() {
        String xpathLocator = "//*[@class = 'search-box-modal__results'][1]//*[@class='results-title']";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        List<String> addressList = new ArrayList<>();
        for (WebElement a : elements) {
            addressList.add((selenium.getText(a)));
        }
        return addressList;
    }

    /**
     * Click the first result based on area
     * @throws InterruptedException
     */
    public void clickTheFirstResultBasedOnArea() throws InterruptedException {
        selenium.waitInCaseElementVisible(firstResultAreaText,3);
        selenium.clickOn(firstResultAreaText);
    }

    /**
     * Verify object is present
     */
    public boolean filterIsPresent() throws InterruptedException {
        selenium.waitTillElementIsVisible(By.xpath("//*[@id='filterOptionsWrapper']"),30);
        return selenium.waitInCaseElementVisible(By.xpath("//*[@id='filterOptionsWrapper']"), 30) != null;
    }

    /**
     * List kost jogja
     */
    public boolean listKostJoga() throws InterruptedException {
        return selenium.waitInCaseElementVisible(By.cssSelector(".content-wrapper .list-title"), 30) != null;
    }
    /**
     * Click on kos type filter
     * @throws InterruptedException
     */
    public void clickOnKosTypeFilter() throws InterruptedException {
        selenium.hardWait(3);
       // selenium.javascriptClickOn(kosTypeFilter);
        if (selenium.isElementNotDisplayed(kosTypeFilter)) {
            selenium.clickOn(kosTypeFilter);  // click this if appears element
        }else {
            selenium.clickOn(kosTypeFilter2);  //click this if not appears element
        }
    }

    /**
     * Verify object is present
     * @throws InterruptedException
     */
    public boolean kostTypeFilterIsPresent(String text) throws InterruptedException {
        selenium.hardWait(5);
        selenium.pageScrollUsingCoordinate(0,10);
        return selenium.isElementPresent(By.xpath("//*[@id='filterOptionsWrapper']//span[text()='" + text +"']"));
    }

    /**
     * Click on kos time period filter
     * @throws InterruptedException
     */
    public void clickOnTimePeriodFilter() throws InterruptedException {
       // selenium.clickOn(timePeriodFilter);
        selenium.hardWait(5);
        if (selenium.isElementNotDisplayed(timePeriodFilter)) {
            selenium.clickOn(timePeriodFilter);  // click this if appears element
        }else {
            selenium.clickOn(timePeriodFilter2);  //click this if not appears element
        }
    }

    /**
     * Verify object is present
     * @throws InterruptedException
     */
    public boolean kostTimePeriodFilterIsPresent(String text) throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0,10);
        return selenium.isElementPresent(By.xpath("//*[@class='filter-input__label']//span[2][text()='" + text + "']"));
    }


    /**
     * Get title kost landing text
     * @return text
     */
    public String getTitleKostLanding() {
        String[] text = selenium.getText(titleKostList).split("Kost ", 0);
        String city = text[text.length-1];
        city = city.replace(" Murah","");
        return city;
    }

    /**
     * Click on selected area
     * @throws InterruptedException
     * @param area is area name choosen
     */
    public void clickAreaFromkeywordBelow(String area) throws InterruptedException {
        WebElement selectedArea = driver.findElement(By.xpath("//div[@class='results-box']/label[@title = '"+ area +"']"));
        selenium.clickOn(selectedArea);
        selenium.hardWait(2);
    }

    /**
     * Click on selected subarea
     * @throws InterruptedException
     * @param subarea is area name choosen
     */
    public void clickSubAreaBasedOnKeyword(String subarea) throws InterruptedException {
        WebElement selectedSubArea = driver.findElement(By.xpath("//label[contains(@title, '"+ subarea +"')]"));
        selenium.clickOn(selectedSubArea);
    }
}

