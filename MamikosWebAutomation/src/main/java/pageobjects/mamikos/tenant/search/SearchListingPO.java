package pageobjects.mamikos.tenant.search;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.ArrayList;
import java.util.List;

public class SearchListingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public SearchListingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='row']/div[1]")
    private WebElement firstKost;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement inputSearchBoxListing;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement inputSearchBox;

    private By kostApartemenAutoCompleteResult = By.xpath("//div[text()='Nama kost']/following-sibling::a");

    @FindBy(className = "results-list")
    private List<WebElement> resultLists;

    @FindBy(className = "results-subtitle")
    private List<WebElement> resultSubtitle;

    @FindBy(className = "track-list-booking-kost")
    private List<WebElement> roomCardContainer;

    @FindBy(css = "#landingKostContainer .title-container h1")
    private WebElement landingTitleHeading;

    @FindBy(className = "btn-popper")
    private WebElement instantBookingPopUp;

    private By genderButton = By.xpath("//span[contains(text(),'Semua Tipe Kos')]");

    private By flashSaleButton = By.xpath("//span[contains(text(),'Promo Ngebut')]");

    private By goldPlusButton = By.xpath("//span[contains(text(),'Kos Andalan')]");

    private By mamiroomsButton = By.xpath("//div[@class='singgahsini-filter']");

    private By popUpConfirmationbutton = By.cssSelector("button[data-path='btn_popperAction']");

    private By FTUETitleText = By.xpath("//h2[@data-path='lbl_popperTitle']");

    private By eliteButton = By.xpath("//span[contains(text(),'Kos Pilihan')]");

    @FindBy(xpath = "//h2[@class = 'list__title']")
    private WebElement titleListingResultText;

    @FindBy(xpath = "//h1[contains(@class, 'area__title')]")
    private WebElement areaTitleText;

    //----------------------- Main Filter Button -------------------------

    @FindBy(xpath = "(//div[@id = 'baseMainFilter'])[1]//button[contains(text() , 'Simpan')]")
    private WebElement saveGenderFilterButton;

    @FindBy(css = "#baseFilterPrice > div > div.bg-c-dropdown__trigger > span")
    private WebElement priceFilter;

    @FindBy(xpath = "(//input[@class = 'price-input'])[1]")
    private WebElement minPriceFilter;

    @FindBy(css = "#baseFilterPrice .input-price-container .input-price-content:nth-child(3) .price-input")
    private WebElement maxPriceFilter;

    @FindBy(xpath = "(//div[@class = 'dropdown-menu__action-save']/button)[3]")
    private WebElement savePriceFilterButton;

    @FindBy(css = ".filter-kost-type__sort #baseMainFilter")
    private WebElement sortingButton;

    @FindBy(xpath = "//span[@role = 'button' and contains(text() , 'Fasilitas')]")
    private WebElement facilityFilterButton;

    @FindBy(xpath = "(//div[@id = 'modalSearchFacilities'])//button[contains(text() , 'Simpan')]")
    private WebElement saveFacilityFilterButton;

    private By kosRuleFilterButton = By.xpath("//span[contains(text(),'Aturan Kos')]");

    @FindBy(xpath = "//div[@class='bg-c-dropdown__menu bg-c-dropdown__menu--open bg-c-dropdown__menu--fit-to-content bg-c-dropdown__menu--text-lg']//button[@class='bg-c-button dropdown-menu__action-base bg-c-button--primary-naked bg-c-button--md']")
    private WebElement saveKosRule;

    //--------------------------- Desc Filter -------------------------

    private By GPText = By.xpath("//span[@data-path='lbl_goldplus']");

    private By mamiRoomsToggle = By.xpath("//span[@data-path='lbl_mamirooms']/following-sibling::div//input");

    private By eliteText = By.xpath("//span[@data-path='lbl_elite']");

    private By AndalanToggle = By.cssSelector("[data-popper-placement='bottom-start'] .bg-c-switch");

    private By pilihanToogle = By.cssSelector("[data-testid='filter-kost-elite'] .bg-c-switch");

    private By promoNgebutText = By.xpath("//span[@data-path='lbl_flash_sale']");

    //--------- Map Section ----------

    @FindBy(css = "#app div.container-fluid.map-container.map-container--tall.default-content-map.default-content-map--hide > button")
    private WebElement mapLegendButton;

    @FindBy(css = "div[style='display: none;']")
    private WebElement mapLegendClosedStatus;

    @FindBy(css = ".nominatim-list__grid > div:nth-child(3) #nominatimMap")
    private WebElement nominatimMap;

    @FindBy(xpath = "//button[contains(@class, 'nominatim-list__map-button')]")
    private WebElement searchByMapButton;

    @FindBy(css = ".nominatim-list__grid .bg-c-grid__item:nth-child(2) #nominatimMap")
    private WebElement nominatimMapOnCenter;

    //--------- Recommendation Kos Section ----------

    @FindBy(css="#app > div > h1")
    private WebElement recommendationListTitle;

    //---------------- Listing --------------------

    @FindBy(xpath = "//button[contains(@class , 'nominatim-list__see-more')]")
    private WebElement seeMoreListingButton;

    @FindBy(xpath = "//button[contains(@class , 'nominatim-list__go-top')]")
    private WebElement backToTopButton;

    @FindBy(css = ".nominatim-rooms-grid:nth-child(36)")
    private WebElement morePropertyListing;

    @FindBy(css = ".nominatim-rooms-grid .kost-rc")
    private WebElement oneKosListing;

    @FindBy(xpath = "(//div[@class = 'kost-rc'])[2]")
    private WebElement secondKosListing;

    @FindBy(xpath = "//button[contains(text() , 'Reset filter')]")
    private WebElement resetFilterButton;

    @FindBy(xpath = "//span[contains(@class , 'nominatim-list__info-text')]")
    private WebElement infoText;

//    private By firstPriceListing = By.cssSelector(".nominiatim-list__title + .nominatim-list__grid .nominatim-rooms-grid:first-of-type:nth-child(1) .rc-price__text");
    private By firstPriceListing = By.xpath("//div[@class='nominatim-list']/div[2]/div[1]//span[@class='rc-price__text bg-c-text bg-c-text--body-1 ']");

//    private By lastPriceListing = By.cssSelector(".nominatim-rooms-grid:last-of-type .rc-price__text");
    private By lastPriceListing = By.xpath("//div[@class='nominatim-list']/div[@class='nominatim-list__grid bg-c-grid bg-c-grid--vtop bg-c-grid--left ']/div[3]//span[@class='rc-price__text bg-c-text bg-c-text--body-1 ']");

    private By firstPriceListingProp = By.xpath("(//span[contains(@class, 'rc-price__text')])[1]");

    private By lastPriceListingProp = By.xpath("(//span[contains(@class, 'rc-price__text')])[13]");

    @FindBy (xpath = "//div[@class='rc-overview__label rc-overview__label--goldplus bg-c-label bg-c-label--rainbow bg-c-label--rainbow-white track-list-booking-kost track-list-promo-kost']")
    private WebElement labelAndalan;

    private By eliteLabel = By.xpath("//div[@class='rc-photo__controlled-room rc-photo__controlled-room--elite track-list-booking-kost']");

    @FindBy (xpath = "//h3[.='Kos Tidak Ditemukan']")
    private WebElement textKostNotFound;

    @FindBy (xpath = "//span[.='Silahkan ubah filter untuk meningkatkan hasil pencarian kos.']")
    private WebElement textFilterSuggest;

    //---------------- Apartement --------------------
    @FindBy (xpath = "//i[@class='open-indicator']")
    private WebElement areaCityDropdown;

    @FindBy (xpath = "//p[.='Apartemen tidak ditemukan.']")
    private WebElement apartementNotFoundText;

    @FindBy (xpath = "//div[@class='dropdown-toggle clearfix']/input[@class='form-control']")
    private WebElement searchAreaDropdown;

    @FindBy (id = "vacancyCriteria")
    private WebElement searchAreaByKeyword;

    @FindBy (xpath = "//div[@class='row']/div[1]//div[@class='rc-overview__label bg-c-label bg-c-label--rainbow bg-c-label--rainbow-white']")
    private WebElement badgeApartement;

    /**
     * Click on first listing that appear
     * @throws InterruptedException
     */
    public void clickFirstKost() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0,0);
        selenium.clickOn(firstKost);
        selenium.switchToWindow(0);
    }

    /**
     * Set text to search input box on search listing
     * @param propertyName set your target kost or apartment
     * @throws InterruptedException
     */
    public void setTextInInputSearchListing(String propertyName) throws InterruptedException {
        selenium.waitTillElementIsVisible(inputSearchBoxListing);
        selenium.enterText(inputSearchBoxListing, propertyName, true);
    }

    /**
     * Select result autocomplete base on input in search field
     * @param propertyName with your property name example "Kost Gani Putri"
     * @param clearText clear text before input
     * @throws InterruptedException
     */
    public void searchPropertyAndSelectResult(String propertyName, Boolean clearText) throws InterruptedException {
        selenium.waitTillElementIsVisible(inputSearchBoxListing);
        selenium.enterText(inputSearchBox, propertyName, true);
        selenium.hardWait(5);
     //   selenium.waitTillElementsCountIsMoreThan(kostApartemenAutoCompleteResult, 0);
        selenium.waitTillAllElementsAreLocated(kostApartemenAutoCompleteResult);
        selenium.javascriptClickOn(kostApartemenAutoCompleteResult);
    }

    /**
     * Click filter by gender
     * @throws InterruptedException
     */
    public void clickFilterByGender(String gender) throws InterruptedException {
        selenium.clickOn(genderButton);
        selenium.javascriptClickOn(driver.findElement(By.xpath("(//div[@id = 'baseMainFilter'])[1]//span[text() = '"+ gender +"']//parent::label/input")));
        selenium.clickOn(saveGenderFilterButton);
        selenium.hardWait(3);
    }

    /**
     * Click on result lists based on result address
     * @param result Search Result
     * @throws InterruptedException
     */
    public void clickResultLists(String result) throws InterruptedException {
        for(int i = 0; i < resultLists.size(); i++){
            if (result.equals(resultSubtitle.get(i).getText())){
                selenium.hardWait(3);
                selenium.clickOn(resultLists.get(i));
            }
        }
    }

    /**
     * @return true if FTUE present, otherwise false.
     */
    public boolean isFTUE_screenPresent() {
        return selenium.waitInCaseElementPresent(FTUETitleText, 5) != null
                || selenium.waitInCaseElementPresent(popUpConfirmationbutton, 5) != null;
    }

    /**
     * Will check First Time User Experience screen first. And then will click on close button on FTUE if present in the screen.
     * Dismiss FTUE first time user experience by click on close button if present.
     * @throws InterruptedException
     */
    public void clickFTUEKosListingPopUp() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        while (isFTUE_screenPresent()) {
            if (selenium.waitInCaseElementVisible(popUpConfirmationbutton, 3) != null) {
                selenium.clickOn(popUpConfirmationbutton);
            } else {
                break;
            }
        }
    }

    /**
     * Click on 'Kost' with Book Now Tag
     * @throws InterruptedException
     */
    public void clickOnKostWithBookNowTag() throws InterruptedException {
        selenium.clickOn(roomCardContainer.get(0));
        selenium.switchToWindow(2);
    }

    /**
     * Get landing kost filter
     * @return string
     */
    public String getLandingKostFilter(){
        String words = selenium.getText(landingTitleHeading);
        String city = words.substring(5);
        city = city.replace(" Murah","");
        return city.toLowerCase();
    }

    /**
     * Click on Instant Booking Pop Up
     * @throws InterruptedException
     */
    public void clickInstantBookingPopUp() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(instantBookingPopUp, 3) != null){
            selenium.clickOn(instantBookingPopUp);
        }
    }

    /**
     * Click on filter flash sale button
     * @throws InterruptedException
     */
    public void clickFilterFSButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(flashSaleButton);
        selenium.clickOn(flashSaleButton);
    }

    /**
     * Click on filter Gold Plus button
     * @throws InterruptedException
     */
    public void clickFilterGPButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(goldPlusButton);
    }

    /**
     * Click on filter Mamirooms button
     * @throws InterruptedException
     */
    public void clickFilterMamiroomsButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(mamiroomsButton);
    }

    /**
     * Get description filter Gold Plus
     * @return string
     */
    public String getFilterGPText(){
        selenium.waitInCaseElementVisible(GPText, 3);
        return selenium.getText(GPText).toLowerCase();
    }

    /**
     * Get description filter Kos Pilihan
     * @return string
     */
    public String getFilterEliteText(){
        selenium.waitInCaseElementVisible(eliteText, 3);
        return selenium.getText(eliteText).toLowerCase();
    }


    /**
     * Get List of legend wording is present
     * @param legend Legend Name
     * @return text legend Name present / not
     */
    public Boolean isLegendPresent(String legend){
        return selenium.waitInCaseElementVisible(By.xpath("//*[@class='map-style__legend-icon bg-c-grid__item bg-is-col-4']//div[contains(text(), '" + legend + "')]"), 30) != null;
    }

    /**
     * Get List of legend wording is present
     * @param legend Legend Name
     * @return text legend name
     */
    public String getLegendDesc(String legend){
        return selenium.getText(By.xpath("//*[@class='map-style__legend-icon bg-c-grid__item bg-is-col-4']//div[contains(text(), '" + legend + "')]"));
    }

    /**
     * Get List of legend wording description is present
     * @param decs Legend description
     * @return text legend description present / not
     */
    public Boolean isLegendDescPresent(String decs){
        return selenium.waitInCaseElementVisible(By.xpath("//*[@class='map-style__legend-description bg-c-grid__item bg-is-col-8']//span[contains(text(), '" + decs + "')]"), 10) != null;
    }

    /**
     * Get List of legend wording description is present
     * @param decs Legend description
     * @return text legend description
     */
    public String getLegendDescText(String decs){
        return selenium.getText(By.xpath("//*[@class='map-style__legend-description bg-c-grid__item bg-is-col-8']//span[contains(text(), '" + decs + "')]"));
    }

    /**
     * Get List of legend wording information is present
     * @param information Legend information
     * @return text legend information present / not
     */
    public Boolean isLegendInformationPresent(String information){
        return selenium.waitInCaseElementVisible(By.xpath("//*[@class='map-style__legend-description bg-c-grid__item bg-is-col-8'][contains(text(), '" + information + "')]"), 10) != null;
    }

    /**
     * Get List of legend wording information is present
     * @param information Legend information
     * @return text legend information
     */
    public String getLegendInformationText(String information){
        return selenium.getText(By.xpath("//*[@class='map-style__legend-description bg-c-grid__item bg-is-col-8'][contains(text(), '"+ information +"')]"));
    }

    /**
     * Click on map legend information button
     * @throws InterruptedException
     */
    public void clickMapLegendButton() throws InterruptedException {
        selenium.clickOn(mapLegendButton);
    }

    /**
     * Get status map legend pop up is appears
     * @return map legend pop up present / not
     */
    public boolean isMapLegendPresent() {
        return selenium.waitInCaseElementVisible(mapLegendClosedStatus, 3) != null;
    }

    /**
     * Get arround recommendation kos list Desc Text
     * @return recomendation title in listing
     */
    public String getRecommendationKosList() {
        selenium.switchToWindow(0);
        String desc = selenium.getText(recommendationListTitle);
        return desc;
    }

    /**
     * Get town title text
     * @return string town title text
     */
    public String getTownTitleText() {
        selenium.waitInCaseElementVisible(landingTitleHeading, 3);
        return selenium.getText(landingTitleHeading);
    }

    /**
     * Get title listing result of keyword
     * @return string listing result text
     */
    public String getTitleListingResult() {
        selenium.waitInCaseElementVisible(titleListingResultText,5);
        return selenium.getText(titleListingResultText);
    }

    /**
     * Get title area text based on keyword
     * @return string title area text
     */
    public String getTitleAreaText() {
        selenium.waitInCaseElementVisible(areaTitleText, 3);
        return selenium.getText(areaTitleText);
    }

    /**
     * Check element of nominatim map in right side of page present
     * @return element displayed true / false
     */
    public boolean isNominatimMapPresent() {
        return selenium.isElementDisplayed(nominatimMap);
    }

    /**
     * Check element of property number 16 (last property) in below of page is present
     * @return element displayed true / false
     */
    public boolean isBottomKostListingPresent(int listing) {
        WebElement bottomKostList = driver.findElement(By.cssSelector("div.nominatim-rooms-grid:nth-child("+ listing +")"));
        selenium.pageScrollInView(bottomKostList);
        return selenium.isElementDisplayed(bottomKostList);
    }

    /**
     * Scroll down to find element of 'Lihat lebih banyak' button
     * Check element of button 'Lihat lebih banyak' in below of page is present
     * @return element visible true / false
     */
    public boolean isSeeMorePropertyPresent() {
        selenium.pageScrollInView(seeMoreListingButton);
        return selenium.waitInCaseElementVisible(seeMoreListingButton, 3) != null;
    }

    /**
     * Check element of back to top button is present
     * @return element displayed true / false
     */
    public boolean isBacktoTopPresent() {
        return selenium.isElementDisplayed(backToTopButton);
    }

    /**
     * Click on 'Lihat lebih banyak' button
     * wait until page load
     * @throws InterruptedException
     */
    public void clickSeeMorePropertyButton() throws InterruptedException {
        selenium.clickOn(seeMoreListingButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Check element of last listing property after click 'Lihat lebih banyak' button is present
     * @return element displayed true / false
     */
    public boolean isMoreKostListingPresent() {
        selenium.pageScrollInView(morePropertyListing);
        return selenium.isElementDisplayed(morePropertyListing);
    }

    /**
     * Click on back to top (arrow top) button
     * @throws InterruptedException
     */
    public void clickBackToTopButton() throws InterruptedException {
        selenium.javascriptClickOn(backToTopButton);
    }

    /**
     * Check element of 'Cari berdasarkan peta' button is present
     * @return element visible true / false
     */
    public boolean isSearchByMapButtonPresent() {
        return selenium.waitTillElementIsVisible(searchByMapButton, 3) != null;
    }

    /**
     * Click on filter price
     */
    public void clickFilterPrice() {
        selenium.waitInCaseElementVisible(priceFilter,3);
        selenium.javascriptClickOn(priceFilter);
    }

    /**
     * Input minimum price according to text in feature file
     * @throws InterruptedException
     * @param price1 is price min inputed
     */
    public void inputMinPrice(String price1) throws InterruptedException {
        selenium.waitTillElementIsVisible(minPriceFilter, 3);
        selenium.enterText(minPriceFilter, price1, true);
        selenium.hardWait(3);
    }

    /**
     * Input maximum price according to text in feature file
     * @param price2 is price max inputed
     */
    public void inputMaxPrice(String price2) {
        selenium.waitTillElementIsVisible(maxPriceFilter, 3);
        selenium.clearTextField(maxPriceFilter);
        selenium.enterText(maxPriceFilter, price2, true);
    }

    /**
     * Click save button on popup price filter
     * @throws InterruptedException
     */
    public void clickSavePriceFilter() throws InterruptedException {
        selenium.clickOn(savePriceFilterButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Check element of property is just one displayed
     * @return element displayed true / false
     * @throws InterruptedException
     */
    public boolean isOneKosListingPresent() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementDisplayed(oneKosListing);
    }

    /**
     * Check element of second property displayed
     * @return element displayed true / false
     */
    public boolean isSecondKosListingPresent() throws InterruptedException {
        return selenium.isElementDisplayed(secondKosListing);
    }

    /**
     * Check element of map nominatim is present on center of page
     * @return element true / false
     */
    public boolean isNominatimMapPresentOncenter() {
        return selenium.waitInCaseElementVisible(nominatimMapOnCenter, 3) != null;
    }

    /**
     * Check the alert message present or not
     * @return string alert text
     * @param alert is text warning
     */
    public boolean isAlertMessageNominatimPresent(String alert) {
        WebElement elementText = driver.findElement(By.xpath("//span[contains(text() , '"+ alert +"')]"));
        return selenium.waitInCaseElementVisible(elementText, 3) != null;
    }

    /**
     * Check reset button displayed
     * @return element displayed true / false
     */
    public boolean isResetFilterButtonPresent() {
        return selenium.isElementDisplayed(resetFilterButton);
    }

    /**
     * Click reset filter button
     * @throws InterruptedException
     */
    public void clickResetFilterButton() throws InterruptedException {
        selenium.clickOn(resetFilterButton);
        selenium.hardWait(3);
    }

    /**
     * Check information empty state in area boundaries landing page is present
     * @return element displayed true / false
     */
    public boolean isEmptyStateTextAreaBoundariesLandingPagePresent(String emptyStateText) throws InterruptedException {
        selenium.hardWait(2);
        WebElement infoText = driver.findElement(By.xpath("//h4[text() = '"+ emptyStateText +"']"));
        return selenium.waitInCaseElementVisible(infoText, 3) != null;
    }

    /**
     * Click mamiroom toggle button
     */
    public void clickMamiroomsActivatedToggle() {
        selenium.javascriptClickOn(mamiRoomsToggle);
    }

    /**
     * Click search by maps in maps landing page area boundaries
     * @throws InterruptedException
     */
    public void clickSearchByMapsButton() throws InterruptedException {
        selenium.javascriptClickOn(searchByMapButton);
    }

    /**
     * Click the sorting option based on param value
     * @param  sorting sorting option
     * @throws InterruptedException
     */
    public void selectsSortingOption(String sorting) throws InterruptedException {
        selenium.clickOn(sortingButton);
        selenium.javascriptClickOn(driver.findElement(By.xpath("//div[@class = 'filter-kost-type__sort']//label/span[contains(text(), '"+ sorting +"')]")));
    }

    /**
     * Get first property price in landing boundaries page
     * @return int price
     */
    public int getFirstPricePropertyListing() {
        selenium.waitInCaseElementVisible(firstPriceListing, 3);
        String text = ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;",driver.findElement(firstPriceListing)).toString();
        String first = text.replaceAll("[Rp.\\t\\n\\r]","");
        return Integer.parseInt(first);
    }

    /**
     * Get last property price in landing boundaries page
     * @return int price
     */
    public int getLastPricePropertyListing() {
        selenium.pageScrollInView(lastPriceListing);
        String text = ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;",driver.findElement(lastPriceListing)).toString();
        String last = text.replaceAll("[Rp.\\t\\n\\r]","");
        return Integer.parseInt(last);
    }

    /**
     * Get first property price in listing property page
     * @return int price
     */
    public int getFirstPricePropertyPageListing() {
        selenium.waitInCaseElementVisible(firstPriceListingProp, 3);
        String first = selenium.getText(firstPriceListingProp).replaceAll("[Rp.]", "");
        return Integer.parseInt(first);
    }

    /**
     * Get last property price in listing property page
     * @return int price
     */
    public int getLastPricePropertyPageListing() {
        selenium.pageScrollInView(lastPriceListingProp);
        String last = selenium.getText(lastPriceListingProp).replaceAll("[Rp.]", "");
        return Integer.parseInt(last);
    }

    /**
     * Get the gender label in listing
     * @return list of string gender
     * @param gender is gender option (putra, putri, campur)
     */
    public List<String> getGenderInListing(String gender) {
        String xpathLocator = "//div[@data-testid = 'kostRoomCard']//div[contains(text() , '"+ gender +"')]";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        List<String> genderFilter = new ArrayList<>();
        for (WebElement a : elements) {
            genderFilter.add((selenium.getText(a)));
        }
        return genderFilter;
    }

    /**
     * Click filter Facility, and choose facility
     * @param fac is chosen facility
     * @throws InterruptedException
     */
    public void clickFilterByFacility(String fac) throws InterruptedException {
        selenium.clickOn(facilityFilterButton);
        selenium.hardWait(2);
        WebElement facility = driver.findElement(By.xpath("//div[@class = 'modal-body__facilities']//text()[contains(., '"+ fac +"')]/parent::label"));
        selenium.pageScrollInView(facility);
        selenium.javascriptClickOn(facility);
        selenium.clickOn(saveFacilityFilterButton);
    }

    /**
     * Get top facility on kos Listing
     * @param fac is chosen facility
     * @return list of string facility
     */
    public List<String> getFacilityInKostListing(String fac) throws InterruptedException {
        String xpathLocator = "//span[@data-testid = 'roomCardFacilities-facility']//span[text() = '"+ fac +"']";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        List<String> facFilter = new ArrayList<>();
        selenium.hardWait(5);
        for (WebElement a : elements) {
            selenium.pageScrollInView(a);
            facFilter.add((selenium.getText(a)));
        }
        return facFilter;
    }

    /**
     * Click on first listing that appear
     * @throws InterruptedException
     */
    public void clickFilterElite() throws InterruptedException {
        selenium.waitTillElementIsClickable(eliteButton);
        selenium.clickOn(eliteButton);
        selenium.hardWait(3);
    }

    /**
     * Click Filter Kos Andalan toggle button
     */
    public void clickAndalanActivatedToggle() throws InterruptedException {
        selenium.waitInCaseElementVisible(AndalanToggle, 5);
        selenium.clickOn(AndalanToggle);
    }

    /**
     * Check if label Kos Andalan is present
     * @return displayed true, otherwise false
     */
    public boolean isAndalanShow() throws InterruptedException {
        return selenium.waitInCaseElementVisible(labelAndalan, 5) != null;
    }

    /**
     * Acvtivate Elite Kos Filter
     */
    public void activateEliteFilter() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(eliteButton);
        selenium.hardWait(2);
        selenium.clickOn(pilihanToogle);
    }

    /**
     * Check if Elite Label is present
     *
     * @return displayed true, otherwise false
     */
    public boolean isEliteLabelPresent() {
        return selenium.waitInCaseElementPresent(eliteLabel, 5) != null;
    }

    /**
     * Get text kos not found
     * @return String kos not found
     */
    public String getTextKostNotFound() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(textKostNotFound);
    }

    /**
     * Get text filter suggest
     * @return String filter suggest
     */
    public String getTextFilterSuggest() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(textFilterSuggest);
    }

    /**
     * Click on filter Gold Plus button and activate
     * @throws InterruptedException
     */
    public void clickActivatePilihan() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(goldPlusButton);
        selenium.hardWait(2);
        selenium.clickOn(eliteButton);
        selenium.hardWait(2);
        selenium.clickOn(pilihanToogle);
    }

    /**
     * Click filter kost rule, and choose kos rule
     * @param kosRule is chosen kos rule
     * @throws InterruptedException
     */
    public void clickFilterByKosRule(String kosRule) throws InterruptedException {
        selenium.clickOn(kosRuleFilterButton);
        selenium.hardWait(3);
        selenium.javascriptClickOn(driver.findElement(By.xpath("(//div[@id = 'filterOptionsWrapper'])/div[8]//span[text() = '"+ kosRule +"']//parent::label/input")));
        selenium.hardWait(5);
        selenium.clickOn(saveKosRule);
    }

    /**
     * Enter Text in search bar
     * @param areaCity is text we want to search
     */
    public void enterAreaCity(String areaCity) {
        selenium.enterText(searchAreaDropdown, areaCity, false);
        selenium.sendKeyEnter(searchAreaDropdown);
    }

    /**
     * Get text apartement not found
     * @return String apartement not found
     */
    public String getTextApartementNotFound() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(apartementNotFoundText);
    }

    /**
     * Enter Text in search bar
     * @param keywordArea is text we want to search
     */
    public void enterKeywordArea(String keywordArea) {
        selenium.enterText(searchAreaByKeyword, keywordArea, false);
        selenium.sendKeyEnter(searchAreaByKeyword);
    }

    /**
     * Check if Apartement Label is present
     *
     * @return displayed true, otherwise false
     */
    public boolean isBadgeApartementPresent() {
        return selenium.waitInCaseElementVisible(badgeApartement, 5) != null;
    }

    /**
     * Get the Kos Andalan label in listing
     * @return list of string KOs Andalan
     * @param filter is filter option (Kos Andalan)
     */
    public List<String> getFilterLabelInListing(String filter) {
        String xpathLocator = "//div[@data-testid = 'kostRoomCard']//div[contains(text() , '"+ filter +"')]";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        List<String> andalanFilter = new ArrayList<>();
        for (WebElement a : elements) {
            andalanFilter.add((selenium.getText(a)));
        }
        return andalanFilter;
    }

    /**
     * Get description filter Promo Ngebut
     * @return string
     */
    public String getFilterPromoNgebutText(){
        selenium.waitInCaseElementVisible(promoNgebutText, 3);
        return selenium.getText(promoNgebutText).toLowerCase();
    }
}

