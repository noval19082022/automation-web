package pageobjects.mamikos.tenant.search;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.ArrayList;
import java.util.List;

public class FilterListingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

        public FilterListingPO(WebDriver driver) {
            this.driver = driver;
            selenium = new SeleniumHelpers(driver);

            // This initElements method will create all WebElements
            PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
        }

        /*
         * All WebElements are identified by @FindBy annotation
         *
         * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
         * className, xpath as attributes.
         */

    @FindBy(xpath = "//*[@class='form-control track-pilih-tipe-unit']")
    private WebElement unitTypeDropdownList;

    @FindBy(xpath = "//*[@class='form-control track-pilih-tipe-prabotan']")
    private WebElement furnitureDropdownList;

    @FindBy(xpath = "//*[@class='form-control track-pilih-urutkan']")
    private WebElement priceSortingDropdownList;

    @FindBy(xpath = "//*[@class='form-control track-pilih-rent-type']")
    private WebElement timePeriodDropdownList;

    @FindBy(id = "filterPriceMin")
    private WebElement minimumPriceTextBox;

    @FindBy(id = "filterPriceMax")
    private WebElement maximumPriceTextBox;

    @FindBy(xpath = "(//*[@id='filterPriceMax']/following-sibling::button)[1]")
    private WebElement setButton;

    @FindBy(xpath = "//*[@class='navbar-brand']")
    private WebElement mamikosLogo;

    @FindBy(css = "#app .nav-brand")
    private WebElement mamikosLogo2;

    @FindBy(css = ".c-mk-header__logo a")
    private WebElement mamikosOwnerLogo;

    @FindBy(xpath = "//*[@class='landing-unit-list-container']")
    private WebElement noApartmentLabel;

    @FindBy(xpath = "//input[@placeholder='Search']")
    private WebElement cityAreaDropdownList;

    @FindBy(xpath = "//a[normalize-space()='Bandung']")
    private WebElement clickCity;

    @FindBy(xpath = "//span[@class='bg-c-select__trigger-text']")
    private WebElement cityAndAreaText;

    @FindBy(xpath = "//*[@class='rc-info__location bg-c-text bg-c-text--body-3 ']")
    private List<WebElement> cityAndAreaValidationList;


    /**
     * Filter list apartment
     * @throws InterruptedException
     * @param filterBy for specific filter apartment list
     * @param filterValue value for dropdown list
     */
    public void filterApartment(String filterBy, String filterValue) throws InterruptedException {
        switch (filterBy) {
            case "unit type":
                selenium.selectDropdownValueByText(unitTypeDropdownList, filterValue);
                break;
            case "furniture":
                selenium.selectDropdownValueByText(furnitureDropdownList, filterValue);
                break;
            case "price":
                selenium.selectDropdownValueByText(priceSortingDropdownList, filterValue);
                break;
            case "time period":
                selenium.selectDropdownValueByText(timePeriodDropdownList, filterValue);
                break;
        }
    }

    /**
     * Filter list apartment
     * @throws InterruptedException
     * @param filterBy for specific filter apartment list
     * @param minPrice value for minimum price apartment
     * @param maxPrice value for maximum price apartment
     */
    public void filterApartment(String filterBy, String minPrice, String maxPrice) throws InterruptedException {
        if (filterBy.equals("price range")){
            selenium.enterText(minimumPriceTextBox, minPrice, false);
            selenium.fillFieldAfterDoubleClick(maximumPriceTextBox, maxPrice);
        }
    }

    /**
     * Get numberOfElements
     * @param filterBy for specific filter apartment list
     */
    public int getNumberOfElements(String filterBy) throws InterruptedException {
        int numberOfElements = 0;
        List<WebElement> list = new ArrayList<>();
        if (selenium.waitInCaseElementVisible(noApartmentLabel, 5) != null){
            switch (filterBy) {
                case "unit type":
                    list = driver.findElements(By.xpath("//*[@class='kost-detail-two apart-detail']/descendant::div[1]"));
                    break;
                case "furniture":
                    list = driver.findElements(By.xpath("//*[@class='kost-detail-two apart-detail']/descendant::div[2]"));
                    break;
                case "price":
                case "price range":
                    list = driver.findElements(By.xpath("//*[@class='detail-price-kost']/span"));
                    break;
                case "time period":
                    list = driver.findElements(By.xpath("//*[@class='detail-price-kost']/span/span[1]"));
                    break;
            }
        }
        numberOfElements = list.size();
        return numberOfElements;
    }

    /**
     * Get apartment details
     * @param indexData is the index of the data to retrieve the index value from the list you want to get the value
     * @param filterBy for specific filter apartment list
     */
    public String getApartmentDetails(int indexData, String filterBy) throws InterruptedException {
        String valueText = null;
        By element = null;
        if (indexData == 1){
            selenium.hardWait(3);
        }
        switch (filterBy) {
            case "unit type":
                element = By.xpath("(//*[@class='kost-detail-two apart-detail']/descendant::div[1])[" + indexData + "]");
                selenium.waitInCaseElementVisible(element, 10);
                selenium.pageScrollInView(element);
                valueText = selenium.getText(element);
                break;
            case "furniture":
                element = By.xpath("(//*[@class='kost-detail-two apart-detail']/descendant::div[2])[" + indexData + "]");
                selenium.waitInCaseElementVisible(element, 10);
                selenium.pageScrollInView(element);
                valueText = selenium.getText(element);
                break;
            case "price":
            case "price range":
                element = By.xpath("(//*[@class='real-price'])[" + indexData + "]");
                selenium.waitInCaseElementVisible(element, 10);
                selenium.pageScrollInView(element);
                WebElement price = selenium.waitInCaseElementVisible(element, 10);
                valueText = selenium.getText(price).replaceAll("[A-Z a-z . / ,]","").trim();
                break;
            case "time period":
                element = By.xpath("(//*[@class='real-price-unit'])[" + indexData + "]");
                selenium.waitInCaseElementVisible(element, 10);
                selenium.pageScrollInView(element);
                WebElement timePeriod = selenium.waitInCaseElementVisible(element, 10);
                String actualText = selenium.getText(timePeriod);
                valueText = actualText.substring((actualText.indexOf('/') + 2)).trim();
                break;
        }
        return valueText;
    }

    /**
     * Click set button
     * @throws InterruptedException
     */
    public void clickOnSetButton() throws InterruptedException {
        selenium.clickOn(setButton);
    }

    /**
     * Click mamikos.com logo
     * @throws InterruptedException
     */
    public void clickOnMamikosLogo() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(mamikosLogo, 1) != null) {
            selenium.clickOn(mamikosLogo);
        }
        else if (selenium.waitInCaseElementVisible(mamikosLogo2, 1) != null){
            selenium.clickOn(mamikosLogo2);
        }
        else {
            selenium.clickOn(mamikosOwnerLogo);
        }
    }

    /**
     * Input/enter City
     * @param city
     */
    public void enterCityOnFilter(String city) throws InterruptedException {
        selenium.enterText(cityAreaDropdownList, city, true);
        selenium.clickOn(clickCity);
    }

    /**
     * Click city and area
     * @throws InterruptedException
     */
    public void clickCityText() throws InterruptedException {
        selenium.clickOn(cityAndAreaText);
        selenium.hardWait(3);
    }

    /**
     * get city and area validation on list
     * @return string area
     */
    public String getCityAndAreaValidationOnList(Integer index) {
        return selenium.getText(cityAndAreaValidationList.get(index));
    }
}

