package pageobjects.mamikos.tenant.ApartmentDetails;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ApartmentDetailsPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ApartmentDetailsPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }
    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(xpath = "//button[@data-testid='btn-love']")
    private WebElement favoriteButton;

    @FindBy(xpath = "//div[@class='row']/div[1]//div[@class='apartment-rc']")
    private WebElement detailsApartmentButton;

    @FindBy(css = ".card-status__room")
    private WebElement apartmentUnitText;

    @FindBy(xpath = "//button[contains(.,'Hubungi Pengelola')]")
    private WebElement contactApartmentButton;

    @FindBy(css = ".bg-c-modal__body-title")
    private WebElement contactApartmentText;

    @FindBy(xpath = "//div[@class='kost-action__love']/button[@class='bg-c-button bg-c-button--tertiary bg-c-button--md']")
    private WebElement savedFavNumberLabel;

    @FindBy(css = ".btn-love--red-icon")
    private WebElement savedUnFavNumberLabel;

    @FindBy(css = ".btn-love--red-icon")
    private WebElement redLoveIcon;

    @FindBy(xpath = "(//div[@id='detailTitle'])[1]")
    private WebElement apartmentTitle;

    @FindBy(css = "div.nav-main-link")
    private WebElement searchAdsDropdown;

    @FindBy(css = "[href='/cari']")
    private WebElement searchListDropdownNameKos;

    @FindBy(css = "#priceCard > div > div.card-footer > button")
    private WebElement chatAptButton;



    /**
     * Click on first apartment list
     * @throws InterruptedException
     */
    public void clickDetailsApt() throws InterruptedException {
        selenium.waitTillElementIsVisible(detailsApartmentButton, 50);
        selenium.clickOn(detailsApartmentButton);
        selenium.switchToWindow(0);
        selenium.waitInCaseElementVisible(apartmentTitle, 30);
    }

    /**
     * Click on love button
     * @throws InterruptedException
     */
    public void clickLoveBtn() throws InterruptedException {
        selenium.pageScrollInView(apartmentTitle);
        selenium.clickOn(favoriteButton);
    }

    /**
     * Click on Hubungi Pengelola
     */
    public void clickContactApt() throws InterruptedException {
        selenium.pageScrollInView(apartmentUnitText);
        selenium.hardWait(2);
        selenium.clickOn(contactApartmentButton);
    }

    /**
     * Verify Hubungi Apartemen
     */
    public boolean verifyContactApt(String correctMessage) {
        return (selenium.getText(contactApartmentText).equals(correctMessage));
    }

    /**
     * Get current amount of favourite of apartment detail
     * @return Integer number of saved favourite
     */
    public Integer getApartmentFavouriteNumber() {
        selenium.pageScrollInView(savedFavNumberLabel);
        selenium.waitInCaseElementVisible(savedFavNumberLabel, 50);
        String favouriteNumber = selenium.getText(savedFavNumberLabel);
        if(favouriteNumber.contains("glyph")){
            favouriteNumber = favouriteNumber.substring(30,31);
        }
        else
        {
            favouriteNumber = favouriteNumber.substring(18,19);

        }
        return Integer.parseInt(favouriteNumber);
    }

    /**
     * click favorite button
     * love button in favorite is red
     */

    public void clickOnFavoriteButton() throws InterruptedException {
        selenium.pageScrollInView(savedFavNumberLabel);
        selenium.hardWait(2);
        selenium.javascriptClickOn(savedFavNumberLabel);
    }

    /**
     * click unfavorite button
     * love button in favorite is grey
     */

    public void clickOnUnFavoriteButton(){
        selenium.pageScrollInView(savedUnFavNumberLabel);
        selenium.waitInCaseElementVisible(savedUnFavNumberLabel, 10);
        selenium.javascriptClickOn(savedUnFavNumberLabel);
    }

    /**
     * Verify love button in favorite is red
     * @return true if love icon is red
     */
    public boolean isLoveRed() {
        return selenium.waitInCaseElementVisible(redLoveIcon, 3) != null;
    }

    /**
     * Get apartment title
     * @return String title of the apartment
     */
    public String getApartmentTitle() {
        return selenium.getText(apartmentTitle);
    }

    /**
     * Wait for apartment title appear
     */
    public void waitTitleApartmentAppear() {
        selenium.waitInCaseElementVisible(apartmentTitle, 11);
    }

    /**
     * Click on Search Ads Dropdown
     */
    public void clickSearchAds() throws InterruptedException {
        selenium.clickOn(searchAdsDropdown);

    }

    /**
     * Get list dropdown name kos
     */
    public String searchListDropdownNameKos() {
        return selenium.getText(searchListDropdownNameKos);
    }

    /**
     * Click on "Chat" in Apartment detail
     * @throws InterruptedException
     */
    public void clickChatApt() throws InterruptedException {
//        selenium.waitInCaseElementVisible(chatAptButton,2);
        selenium.javascriptClickOn(chatAptButton);
    }
}
