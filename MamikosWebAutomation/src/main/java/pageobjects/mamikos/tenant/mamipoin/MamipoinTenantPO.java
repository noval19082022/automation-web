package pageobjects.mamikos.tenant.mamipoin;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.List;

public class MamipoinTenantPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public MamipoinTenantPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    private By mamipoinTenantEntryPointButton = By.xpath("//*[@class='title-link'][contains(.,'Poin Saya')]");

    private By titleMamipoinTenantLandingPage = By.xpath("//h1[text()='MamiPoin']");

    private By informasiPoinButton = By.xpath("//a[@href='/user/mamipoin/expired']");

    private By titleInformasiPoinPage = By.xpath("//h1[@class='title-content'][contains(.,'Tanggal Kedaluwarsa')]");

    private By subtitleInformasiPoinPage = By.xpath("//p[@class='subtitle-content']");

    private By tableTitleTanggalKedaluwarsa = By.xpath("//h1[@class='table-title'][text()='Tanggal Kedaluwarsa']");

    private By tableTitleJumlahMamipoin = By.xpath("//h1[@class='table-title-right'][text()='Jumlah MamiPoin']");

    private By titleTidakAdaPoinYangTersedia = By.xpath("//h1[@class='title'][text()='Tidak Ada Poin yang Tersedia']");

    private By subtitleTidakAdaPoinYangTersedia = By.xpath("//p[@class='subtitle__content'][contains(.,'Yuk mulai kumpulkan poin Kamu.')]");

    private By lihatCaranyaButon = By.xpath("//*[@href='/user/mamipoin/guideline'][contains(.,'Lihat Caranya')]");

    private By riwayatPoinButton = By.cssSelector(".card__footer-long");

    private By titleRiwayatPoinPage = By.xpath("//h1[@class='title'][text()='Riwayat Poin']");

    private By filterSemuaRiwayatPoinPage = By.xpath("//div[@class='b-tab__box-button b-tab__text b-tab__box-active '][contains(.,'Semua')]");

    private By filterPoinDiterimaRiwayatPoinPage = By.xpath("//div[@class='b-tab__box-button b-tab__text'][contains(.,'Poin Diterima')]");

    private By filterPoinDitukarRiwayatPoinPage = By.xpath("//div[@class='b-tab__box-button b-tab__text'][contains(.,'Poin Ditukar')]");

    private By filterPoinKedaluwarsaRiwayatPoinPage = By.xpath("//div[@class='b-tab__box-button b-tab__text'][contains(.,'Poin Kedaluwarsa')]");

    private By titleRiwayatMasihKosong = By.xpath("//div[@class='title'][contains(.,'Riwayat Masih Kosong')]");

    private By subtitleRiwayatMasihKosong = By.xpath("//div[@class='footer__text'][contains(.,'Penerimaan dan penukaran poin Kamu akan tercatat di halaman ini.')]");

    private By dapatkanPoinButton = By.xpath("//p[@class='card__footer-sort']");

    private By titleDapatkanPoinPage = By.xpath("//div[@class='title'][text()='Dapatkan Poin']");

    private By tabPetunjukDapatkanPoinPage = By.xpath("//div[@class='tab__content tab__active'][contains(.,'Petunjuk')]");

    private By tabSyaratDanKetentuanDapatkanPoinPage = By.xpath("//div[@class='tab__content'][contains(.,'Syarat dan Ketentuan')]");

    private By linkPusatBantuanDapatkanPoinPage = By.xpath("//*[@class='footer__link']");

    private By dapatkanPoinHeadline = By.xpath("//div[contains(text(), 'Cara Mudah Mendapatkan')]");

    private By dapatkanPoinSubtitle = By.xpath("//div[contains(text(), 'Kamu bisa mengumpulkan')]");

    private By dapatkanPoinPageFooter = By.className("footer");

    private By expiredPoinInfo = By.cssSelector(".card__info-poin");

    private By settingsMenu = By.xpath("//a[@href='/user/pengaturan']");

    private By poinNotAvailable = By.xpath("//p[contains(.,'Poin Anda masih 0. Lakukan aktivitas di Mamikos untuk mendapat poin.')]");

    @FindBy(className = "guideline")
    public List<WebElement> content;

    @FindBy(className = "list__date")
    public List<WebElement> expDateList;

    @FindBy(className = "b-list")
    private List<WebElement> pointHistoryList;

    @FindBy(xpath = "//*[@class='bg-c-switch__input']")
    private WebElement mamiPoinToggle;

    @FindBy(css = ".discount-text")
    private WebElement totalDiscountText;

    @FindBy(xpath = "//div[@class='nav-section nav-fixed']")
    private WebElement navHeader;

    @FindBy(className = "tab__content")
    private List<WebElement> tabContent;

    @FindBy(css = "#invoicePoint .bg-c-text--body-2")
    private WebElement noHaveMamipoinText;

    @FindBy(xpath = "//p[@class='content__poin-expired']")
    private WebElement pointUsedText;

    /**
     * Verify mamipoin tenant entry point is not displayed
     * @return boolean
     */
    public Boolean isMamipoinTenantEntryPointNotDisplayed() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.isElementPresent(mamipoinTenantEntryPointButton);
    }

    /**
     * Verify the amount of poin owned by the tenant
     * @param poin
     * @return amount of poin
     */
    public String verifyAmountOfPoinOwnedByTenant(String poin) {
        return selenium.getText(By.xpath("//*[@href='/user/mamipoin']//span[contains(text(),'" + poin + "')]"));
    }

    /**
     * Verify the amount of poin saya
     * @param poin
     * @return amount of poin
     */
    public String verifyAmountOfPoinSaya(String poin) {
        return selenium.getText(By.xpath("//p[contains(text(),'" + poin + "')]"));
    }

    /**
     * Click on mamipoin tenant entry point button
     * @throws InterruptedException
     */
    public void clickOnMamipoinTenantEntryPointButton() throws InterruptedException {
        selenium.clickOn(mamipoinTenantEntryPointButton);
    }

    /**
     * Verify title in the mamipoin tenant landing page is displayed
     * @return boolean
     */
    public Boolean isTitleInTheMamipoinTenantLandingPageDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementVisible(titleMamipoinTenantLandingPage, 10)!=null;
    }

    /**
     * Verify informasi poin button is displayed
     * @return boolean
     */
    public Boolean isInformasiPoinButtonDisplayed() throws InterruptedException {
        return selenium.isElementPresent(informasiPoinButton);
    }

    /**
     * Verify title in the informasi poin page is displayed
     * @return boolean
     */
    public Boolean isTitleInTheInformasiPoinPageDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementVisible(titleInformasiPoinPage, 10)!=null;
    }

    /**
     * Verify subtitle in the informasi poin page is displayed
     * @return boolean
     */
    public Boolean isSubtitleInTheInformasiPoinPageDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementVisible(subtitleInformasiPoinPage, 10)!=null;
    }

    /**
     * Verify table title tanggal kedaluwarsa is displayed
     * @return boolean
     */
    public Boolean isTableTitleTanggalKedaluwarsaDisplayed() throws InterruptedException {
        return selenium.isElementPresent(tableTitleTanggalKedaluwarsa);
    }

    /**
     * Verify table title jumlah mamipoin is displayed
     * @return boolean
     */
    public Boolean isTableTitleJumlahMamipoinDisplayed() throws InterruptedException {
        return selenium.isElementPresent(tableTitleJumlahMamipoin);
    }

    /**
     * Verify title tidak ada poin yang tersedia is displayed
     * @return boolean
     */
    public Boolean isTitleTidakAdaPoinYangTersediaDisplayed() throws InterruptedException {
        return selenium.isElementPresent(titleTidakAdaPoinYangTersedia);
    }

    /**
     * Verify subtitle tidak ada poin yang tersedia is displayed
     * @return boolean
     */
    public Boolean isSubtitleTidakAdaPoinYangTersediaDisplayed() throws InterruptedException {
        return selenium.isElementPresent(subtitleTidakAdaPoinYangTersedia);
    }

    /**
     * Verify button lihat caranya is displayed
     * @return boolean
     */
    public Boolean isButtonLihatCaranyaDisplayed() throws InterruptedException {
        return selenium.isElementPresent(lihatCaranyaButon);
    }

    /**
     * Click on lihat caranya button
     * @throws InterruptedException
     */
    public void clickOnLihatCaranyaButton() throws InterruptedException {
        selenium.clickOn(lihatCaranyaButon);
    }

    /**
     * Click on mamipoin tenant informasi poin button
     * @throws InterruptedException
     */
    public void clickOnInformasiPoinButton() throws InterruptedException {
        selenium.clickOn(informasiPoinButton);
    }

    /**
     * Verify riwayat poin button is displayed
     * @return boolean
     */
    public Boolean isRiwayatPoinButtonDisplayed() throws InterruptedException {
        return selenium.isElementPresent(riwayatPoinButton);
    }

    /**
     * Click on mamipoin tenant riwayat poin button
     * @throws InterruptedException
     */
    public void clickOnRiwayatPoinButton() throws InterruptedException {
        selenium.clickOn(riwayatPoinButton);
    }

    /**
     * Verify title in the riwayat poin page is displayed
     * @return boolean
     */
    public Boolean isTitleInTheRiwayatPoinPageDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementVisible(titleRiwayatPoinPage, 10) != null;
    }

    /**
     * Verify filter semua in the riwayat poin page is displayed
     * @return boolean
     */
    public Boolean isFilterSemuaInTheRiwayatPoinPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(filterSemuaRiwayatPoinPage);
    }

    /**
     * Verify filter poin diterima in the riwayat poin page is displayed
     * @return boolean
     */
    public Boolean isFilterPoinDiterimaInTheRiwayatPoinPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(filterPoinDiterimaRiwayatPoinPage);
    }

    /**
     * Verify filter poin ditukar in the riwayat poin page is displayed
     * @return boolean
     */
    public Boolean isFilterPoinDitukarInTheRiwayatPoinPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(filterPoinDitukarRiwayatPoinPage);
    }

    /**
     * Verify filter poin kedaluwarsa in the riwayat poin page is displayed
     * @return boolean
     */
    public Boolean isFilterPoinKedaluwarsaInTheRiwayatPoinPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(filterPoinKedaluwarsaRiwayatPoinPage);
    }

    /**
     * Verify title riwayat masih kosong in the riwayat poin page is displayed
     * @return boolean
     */
    public Boolean isTitleRiwayatMasihKosongDisplayed() throws InterruptedException {
        return selenium.isElementPresent(titleRiwayatMasihKosong);
    }

    /**
     * Verify subtitle riwayat masih kosong in the riwayat poin page is displayed
     * @return boolean
     */
    public Boolean isSubtitleRiwayatMasihKosongDisplayed() throws InterruptedException {
        return selenium.isElementPresent(subtitleRiwayatMasihKosong);
    }

    /**
     * Verify dapatkan poin button is displayed
     * @return boolean
     */
    public Boolean isDapatkanPoinButtonDisplayed() throws InterruptedException {
        return selenium.isElementPresent(dapatkanPoinButton);
    }

    /**
     * Click on mamipoin tenant dapatkan poin button
     * @throws InterruptedException
     */
    public void clickOnDapatkanPoinButton() throws InterruptedException {
        selenium.clickOn(dapatkanPoinButton);
    }

    /**
     * Verify title in the dapatkan poin page is displayed
     * @return boolean
     */
    public Boolean isTitleInTheDapatkanPoinPageDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementVisible(titleDapatkanPoinPage, 10) != null;
    }

    /**
     * Verify tab petunjuk in the dapatkan poin page is displayed
     * @return boolean
     */
    public Boolean isTabPetunjukInTheDapatkanPoinPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(tabPetunjukDapatkanPoinPage);
    }

    /**
     * Verify tab syarat dan ketentuan in the dapatkan poin page is displayed
     * @return boolean
     */
    public Boolean isTabSyaratDanKetentuanInTheDapatkanPoinPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(tabSyaratDanKetentuanDapatkanPoinPage);
    }

    /**
     * Verify link pusat bantuan in the dapatkan poin page is displayed
     * @return boolean
     */
    public Boolean isLinkPusatBantuanInTheDapatkanPoinPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(linkPusatBantuanDapatkanPoinPage);
    }

    /**
     * Click link on pusat bantuan
     * @throws InterruptedException
     */
    public void clickLinkOnPusatBantuan() throws InterruptedException {
        selenium.pageScrollInView(linkPusatBantuanDapatkanPoinPage);
        selenium.clickOn(linkPusatBantuanDapatkanPoinPage);
    }

    /**
     * Get Dapatkan Point Headline Text
     * @return text
     */
    public String getDapatkanPointHeadline() {
        return selenium.getText(dapatkanPoinHeadline);
    }

    /**
     * Get Dapatkan Point Subtitle Text
     * @return text
     */
    public String getDapatkanPoinSubtitle() {
        return selenium.getText(dapatkanPoinSubtitle);
    }

    /**
     * Are All Contents On Dapatkan Poin Page Appear?
     * @param element input string that define Web Element value
     * @return true or false
     */
    public Boolean isContentOnDapatkanPoinAppear(WebElement element) {
        return selenium.waitInCaseElementVisible(element, 5) != null;
    }

    /**
     * Is Footer On Dapatkan Poin Page Appear?
     * @return true or false
     */
    public Boolean isFooterOnDapatkanPoinAppear() {
        return selenium.waitInCaseElementVisible(dapatkanPoinPageFooter, 5) != null;
    }

    /**
     * Click Link on Petunjuk
     * @param link
     */
    public void clickLinkOnPetunjuk(String link) {
        selenium.pageScrollInView(By.xpath("//a[contains(., '" + link + "')]"));
        selenium.javascriptClickOn(By.xpath("//a[contains(., '" + link + "')]"));
        selenium.switchToWindow(2);
    }

    /**
     * Get Expired Point Information on Mamipoin Landing Page
     * @return text
     */
    public String getExpiredPointInfoOnLandingPage() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.getText(expiredPoinInfo);
    }

    /**
     * Get Expired Point Date on Information Point Page
     * @param element input string that define Web Element value
     * @return date (string)
     */
    public String getExpiredDate(WebElement element) {
        return selenium.getText(element);
    }

    /**
     * Is Expired Date List Appear?
     * @param element input string that define Web Element value
     * @return true or false
     */
    public Boolean isExpiredDateListAppear(WebElement element) {
        return selenium.waitInCaseElementVisible(element, 5) != null;
    }

    /**
     * Is Last Item on Point History List Appear
     * @return true or false
     */
    public Boolean isLastItemOnPointHistoryListAppear() {
        return selenium.waitInCaseElementVisible(pointHistoryList.get(pointHistoryList.size() - 1), 5) != null;
    }

    /**
     * Scroll Down To Last Item on Point History List
     * @param title input string that define title value
     * @throws InterruptedException
     */
    public void scrollToLastItemOnPointHistoryList(String title) throws InterruptedException {
        do {
            selenium.pageScrollInView(pointHistoryList.get(pointHistoryList.size() - 1));
            selenium.waitForJavascriptToLoad();
        } while (selenium.waitInCaseElementPresent(By.xpath("//p[contains(., '" + title + "')]"), 3) == null);
    }

    /**
     * Get element attribute from filter
     * @param filter input string that define filter value
     * @return
     */
    public String getFilterElementAttribute(String filter) {
        return selenium.getElementAttributeValue(By.xpath("//div[contains(text(), '" + filter + "')]"), "class");
    }

    /**
     *  Get selected filter text
     * @param filter input string that define filter value
     * @return
     */
    public String getFilterText(String filter) {
        return selenium.getText(By.xpath("//div[contains(text(), '" + filter + "')]"));
    }

    /**
     * Click on Selected Filters
     * @param filter input string that define filter value
     * @throws InterruptedException
     */
    public void clickOnFilters(String filter) throws InterruptedException {
        selenium.clickOn(By.xpath("//div[contains(text(), '" + filter + "')]"));
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click mamipoin toggle
     * @throws InterruptedException
     */
    public void clickOnMamiPoinToggle() throws InterruptedException {
        selenium.hardWait(5);
        selenium.javascriptPageScrollHeightToDown();
        selenium.pageScrollInView(mamiPoinToggle);
        selenium.javascriptClickOn(mamiPoinToggle);
    }

    /**
     * Get text total potongan mamipoin
     * @return String total potongan mamipoin
     */
    public String getTotalDiscountMamipoin() {
        int number = JavaHelpers.extractNumber(selenium.getText(totalDiscountText));
        return String.valueOf(number);
    }

    /**
     * Get point history date text
     * @param date input string that define date value
     * @return string
     */
    public String getPointHistoryDateText(String date) {
        String e = "//h1[contains(text(), '" + date + "')]";
        return selenium.getText(By.xpath(e));
    }

    /**
     * Is point history date appear?
     * @param date input string that define date value
     * @return string
     * @throws InterruptedException
     */
    public Boolean isPointHistoryDateAppear(String date) throws InterruptedException {
        String e = "//h1[contains(text(), '" + date + "')]";
        do{
            selenium.pageScrollInView(pointHistoryList.get(pointHistoryList.size() - 1));
            selenium.waitForJavascriptToLoad();
        }while(selenium.waitInCaseElementVisible(By.xpath(e), 3) == null);
        return selenium.waitInCaseElementVisible(By.xpath(e), 3) != null;
    }

    /**
     * Is Link Pusat Bantuan Appear?
     * @return true or false
     */
    public Boolean isLinkPusatBantuanAppear(){
        selenium.pageScrollInView(linkPusatBantuanDapatkanPoinPage);
        return selenium.waitInCaseElementVisible(linkPusatBantuanDapatkanPoinPage, 3) != null;
    }

    /**
     * Get Header Element Attribute
     * @return string
     */
    public String getHeaderElementAttribute(){
        if(selenium.waitInCaseElementVisible(settingsMenu, 5) != null)
            selenium.pageScrollInView(settingsMenu);
        return selenium.getElementAttributeValue(navHeader, "class");
    }

    /**
     * Click on Syarat dan Ketentuan Tab
     * @throws InterruptedException
     */
    public void clickOnSyaratDanKetentuanTab() throws InterruptedException {
        selenium.pageScrollInView(tabSyaratDanKetentuanDapatkanPoinPage);
        selenium.clickOn(tabSyaratDanKetentuanDapatkanPoinPage);
    }

    /**
     * Get Syarat dan Ketentuan Tab Text
     * @return string
     */
    public String getSyaratDanKetentuanTabText(){
        return selenium.getText(tabContent.get(1));
    }

    /**
     * Get Syarat dan Ketentuan Attribute
     * @return string
     * @throws InterruptedException
     */
    public String getSyaratDanKetentuanAttribute() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(tabContent.get(1), "class");
    }

    /**
     * Get No Point Available Title Text
     * @return string
     */
    public String getNoPointAvailableTitleText(){
        return selenium.getText(titleTidakAdaPoinYangTersedia);
    }

    /**
     * Get Text No Have Mamipoin
     * @return string
     * @throws InterruptedException
     */
    public String getTextNoHaveMamipoin() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(noHaveMamipoinText);
    }

    /**
     * Verify informasi poin hat have been used is displayed
     * @return boolean
     */
    public Boolean isInformasiPoinUsedDisplayed() throws InterruptedException {
        return selenium.isElementDisplayed(pointUsedText);
    }

    /**
     * Get Text doesnt have mamipoin at page payment
     * @return text
     */
    public String getTextDoesntHaveMamipoinPayment() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.getText(poinNotAvailable);
    }


    /**
     * Check when owner blacklist mamipoin
     * @return false if appear
     */
    public boolean isMamipoinToggleAppear() {
        return selenium.waitInCaseElementVisible(mamiPoinToggle, 3) != null;
    }

    /**
     * Check when toggle mamipoin Off
     * @return false if appear
     */
    public boolean isDiscountMamipoinAppear() {
        return selenium.waitInCaseElementVisible(totalDiscountText, 3) != null;
    }

}