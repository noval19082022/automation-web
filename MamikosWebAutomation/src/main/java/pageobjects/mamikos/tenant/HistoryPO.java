package pageobjects.mamikos.tenant;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class HistoryPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public HistoryPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "#app div:nth-child(1) a.router-link-exact-active.router-link-active div.tabs-label-caption")
    private WebElement seenTab;

    @FindBy(css = "div[class=button-container-delete] > button")
    private WebElement clearHistoryButton;

    @FindBy(css = "#app div:nth-child(1) a:nth-child(2) div.tabs-label-caption:last-child")
    private WebElement favouriteTab;

    @FindBy(css = "#app div div div:nth-child(1) a:last-of-type div:nth-last-child(2)")
    private WebElement chatTab;

    @FindBy(css = ".bg-c-label")
    private WebElement firstApartmentTitle;

    @FindBy(xpath = "//div[@class='rc-overview__label bg-c-label bg-c-label--rainbow bg-c-label--rainbow-white']")
    private WebElement apartmentName;

    //Jangan lupa di dynamiskan
    @FindBy(css = "#app div:nth-child(2) > div > div.row > div:nth-child(1)")
    private WebElement propertyList;

    private By allPropertyList = By.cssSelector("#app .kost-rc__inner");

    private By kosName = By.cssSelector("#app > div > div.container.history-container > div > div > div.row > div:nth-child(1) > div > div.kost-rc > div > div.kost-rc__content > div.kost-rc__info > div > span.rc-info__name.bg-c-text.bg-c-text--body-4");

    private By kosName2 = By.cssSelector("#app > div > div.container.history-container > div > div > div.row > div:nth-child(1)");

    /**
     * Click on Favourite tab on History page
     * @throws InterruptedException
     */
    public void clickOnFavouriteTab() throws InterruptedException {
        selenium.clickOn(favouriteTab);
    }

    /**
     * Get kos name from History
     * @param listNumber input with number list start from 1
     * @return kos name
     */
    public String getKosName(int listNumber) {
        String kosNameTarget = kosName.toString().replace("By.cssSelector: ", "");
        kosNameTarget = String.format(kosNameTarget, String.valueOf(listNumber));
        By kosName = By.cssSelector(kosNameTarget);
        return selenium.getText(kosName);
    }

    /**
     * Get kos name from History
     * @param listNumber input with number list start from 1
     * @return kos name
     */
    public String getKosName2(int listNumber) {
        String kosNameTarget = kosName2.toString().replace("By.cssSelector: ", "");
        kosNameTarget = String.format(kosNameTarget, String.valueOf(listNumber));
        By kosName = By.cssSelector(kosNameTarget);
        return selenium.getText(kosName);
    }

    /** verify system display tab using the right text on history page
     * @param data index data
     */
    public String getTabTextByIndex(int data) {
        return selenium.getText(By.xpath("(//*[@class='tabs-label-caption'])[" + (data + 1) + "]"));
    }

    /**
     * Wait for property list to be loaded in Favourite tab
     */
    public void waitTiilPropertyListLoaded() {
        selenium.waitTillElementsCountIsMoreThan(allPropertyList, 0);
    }

    /**
     * Get first apartment name from favorite
     * @return apartment name
     */
    public String getFirstApartmentName() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(apartmentName,10);
        return selenium.getText(apartmentName);
    }

    /**
     * Click on first apartment on list
     * @throws InterruptedException
     */
    public void clickDetailsApt() throws InterruptedException {
        selenium.clickOn(firstApartmentTitle);
        selenium.switchToWindow(0);
    }

    /**
     * Check list of apartment/kos in favourite empty/not
     * @return true if empty
     */
    public boolean isFavouriteListEmpty() {
        return selenium.waitInCaseElementVisible(firstApartmentTitle, 3) == null;
    }

    /**
     * Wait for favourite tab toappear
     */
    public void waitTillFavoriteTabAppear() {
        selenium.waitInCaseElementVisible(favouriteTab, 11);
    }
}

