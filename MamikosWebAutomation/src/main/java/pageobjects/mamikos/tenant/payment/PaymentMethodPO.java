package pageobjects.mamikos.tenant.payment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.ArrayList;

public class PaymentMethodPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public PaymentMethodPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//button[contains(text(), 'Bayar Sekarang')]")
    private WebElement payNowButton;

    @FindBy(xpath = "//*[text()='No. Virtual Account']/parent::div/following-sibling::div/span")
    private WebElement virtualAccountNumberLabel;

    @FindBy(xpath = "//*[text()='Kode Pembayaran']/parent::div/following-sibling::div/span")
    private WebElement virtualAccountNumberLabelPermata;

    @FindBy(xpath = "//*[text()='Kode Perusahaan']/parent::div/following-sibling::div/span")
    private WebElement kodePerusahaanLabel;

    @FindBy (xpath = "//*[@class='switch has-text-primary is-rounded']/parent::div")
    private WebElement voucherSwitch;

    @FindBy (xpath = "//*[@placeholder='Masukan kode voucher Anda']")
    private WebElement voucherCodeTextBox;

    @FindBy (xpath = "//*[@class='button --is-custom-button-voucher is-danger btn-voucher']")
    private WebElement useVoucherButton;

    @FindBy (xpath = "//*[@class='bg-c-input__field']")
    private WebElement noOvoTextBox;

    @FindBy (xpath = "//*[@placeholder='0000 0000 0000 0000']")
    private WebElement cardNumTextBox;

    @FindBy (xpath = "//*[@placeholder='MM']")
    private WebElement monthTextBox;

    @FindBy (xpath = "//*[@placeholder='YY']")
    private WebElement yearTextBox;

    @FindBy (xpath = "//*[@placeholder='000']")
    private WebElement cvvTextBox;

    @FindBy (xpath = "//*[@class='invoice-select-method']")
    private WebElement pilihButton;

    @FindBy (xpath = "//div[@class='bg-c-toast__content']")
    private WebElement toastMessage;

    @FindBy (xpath = "//button[@class='bg-c-button bg-c-button--tertiary-naked-inversed bg-c-button--md']")
    private WebElement toastDeleteButton;

    @FindBy(xpath = "//*[contains(text(), 'Harga Sewa')]/parent::*/following-sibling::*")
    private WebElement perDurationPriceText;

    @FindBy(xpath = "//*[contains(text(), 'Admin Fee')]/following-sibling::*")
    private WebElement adminFeeText;

    @FindBy(xpath = "//*[contains(text(), 'Total Pembayaran')]/following-sibling::*")
    private WebElement totalPaymentText;

    @FindBy(xpath = "//button[contains(text(), 'Saya Sudah Bayar')]")
    private WebElement btnIAlreadyPaid;

    @FindBy(xpath = "//button[.='Sudah Bayar']")
    private WebElement btnPupAlreadyPaid;

    @FindBy(css = ".universal-invoice__page-title p")
    private WebElement textUniversalPageTitle;

    @FindBy(xpath = "//button[normalize-space()='Ubah Metode Pembayaran']")
    private WebElement changeMethodPayment;

    @FindBy(xpath = "//button[normalize-space()='Ubah']")
    private WebElement ubahButton;



    //Payment
    private String PAYMENT="src/test/resources/testdata/mamikos/payment.properties";
    private String noHP = JavaHelpers.getPropertyValue(PAYMENT,"noHPOvo");
    private String cardNum = JavaHelpers.getPropertyValue(PAYMENT,"cardNum");
    private String month = JavaHelpers.getPropertyValue(PAYMENT, "month");
    private String year = JavaHelpers.getPropertyValue(PAYMENT, "year");
    private  String cvv = JavaHelpers.getPropertyValue(PAYMENT, "cvv");

    /**
     * Select payment method
     * @param paymentMethod is option for payment method
     * @throws InterruptedException
     */
    public void selectPaymentMethod(String paymentMethod) throws InterruptedException {
        ArrayList<String> tabs = new ArrayList<>(selenium.getWindowHandles());
        selenium.switchToWindow(tabs.size());
        selenium.hardWait(10);
        selenium.waitInCaseElementClickable(pilihButton,60);
        if (selenium.isElementNotDisplayed(pilihButton)) {
            selenium.clickOn(pilihButton);
        }else {
            selenium.waitInCaseElementClickable(changeMethodPayment,60);
            selenium.clickOn(changeMethodPayment);
            selenium.clickOn(ubahButton);
        }
        selenium.hardWait(5);
        By element = By.xpath("//span[contains(text(),'" + paymentMethod + "')]");
        selenium.waitTillElementIsClickable(element);
        switch (paymentMethod){
            case "Kartu Kredit":
                selenium.clickOn(element);
                selenium.hardWait(3);
                selenium.enterText(cardNumTextBox, "4000000000001091", true);
                selenium.enterText(monthTextBox, month, true);
                selenium.enterText(yearTextBox, year, true);
                selenium.enterText(cvvTextBox, cvv, true);
                break;
            case "OVO":
                selenium.pageScrollInView(element);
                selenium.hardWait(3);
                selenium.clickOn(element);
                selenium.hardWait(3);
                selenium.enterText(noOvoTextBox, noHP, true);
                selenium.hardWait(2);
                break;
            default:
                selenium.pageScrollInView(element);
                selenium.hardWait(3);
                selenium.clickOn(element);
                break;
        }
    }

    /**
     * Click pay now button
     * @throws InterruptedException
     */
    public void clickOnPayNowButton() throws InterruptedException {
        selenium.pageScrollInView(payNowButton);
        selenium.hardWait(2);
        selenium.javascriptClickOn(payNowButton);
        selenium.hardWait(10);
    }

    /**
     * Get virtual account number
     * @return virtual account number
     * * @throws InterruptedException
     */
    public String getVirtualAccountNumber() throws InterruptedException {
        selenium.waitTillElementIsClickable(virtualAccountNumberLabel);
        selenium.hardWait(5);
        return selenium.getText(virtualAccountNumberLabel);
    }

    /**
     * Get virtual account number
     * @return virtual account number Permata
     * * @throws InterruptedException
     */
    public String getVirtualAccountNumberPermata() throws InterruptedException {
        selenium.waitTillElementIsClickable(virtualAccountNumberLabelPermata);
        selenium.hardWait(2);
        return selenium.getText(virtualAccountNumberLabelPermata);
    }

    /**
     * Get kode perusahaan (Bank)
     * @return kode perusahaan
     * * @throws InterruptedException
     */
    public String getKodePerusahaan() throws InterruptedException {
        selenium.waitTillElementIsClickable(kodePerusahaanLabel);
        selenium.hardWait(2);
        return selenium.getText(kodePerusahaanLabel);
    }

    /**
     * Click VoucherSwitch
     * Enter voucherCode
     */
    public void inputVoucher(String voucherCode) throws InterruptedException {
        selenium.clickOn(voucherSwitch);
        selenium.enterText(voucherCodeTextBox, voucherCode, true);
        selenium.clickOn(useVoucherButton);
    }

    /**
     * Get toast message
     * @return message
     */
    public String getToastMessage() {
        return selenium.getText(toastMessage).replace("\n", " ");
    }

    /**
     * Click on delete on toast message
     * @return message
     */
    public void clickOnDeleteOnToastMessage() throws InterruptedException {
        selenium.clickOn(toastDeleteButton);
    }

    /**
     * Get per period / or basic amount price
     * @return per period price / or basic amount price as integer
     */
    public int getBasicPrice() {
        return JavaHelpers.extractNumber(selenium.getText(perDurationPriceText));
    }

    /**
     * Get admin fee
     * @return integer data type of admin fee
     */
    public int getAdminPrice(){
        return JavaHelpers.extractNumber(selenium.getText(adminFeeText));
    }

    /**
     * Get total payment
     * @return integer data type of total payment
     */
    public int getSubTotal() {
        return JavaHelpers.extractNumber(selenium.getText(totalPaymentText));
    }

    /**
     * Check if add ons with target name is visible
     * @param addOnsName add ons name
     * @return boolean data type
     */
    public boolean isAddOnsWithName(String addOnsName) {
        String elementName = "//*[contains(text(), '"+ addOnsName +"')]";
        return selenium.waitTillElementIsVisible(driver.findElement(By.xpath(elementName)), 20) != null;
    }


    /**
     * Check if add ons paid with target name is visible
     * @param addOnsName add ons name
     * @return boolean data type
     */
    public boolean isAddOnsPaidWithName(String addOnsName) {
        String elementName = "(//*[contains(text(), '"+ addOnsName +"')])[2]";
        return selenium.waitInCaseElementVisible(By.xpath(elementName), 20) != null;
    }

    /**
     * Click on I already paid button
     * @throws InterruptedException
     */
    public void clickOnAlreadyPaidButton() throws InterruptedException {
        selenium.clickOn(btnIAlreadyPaid);
        selenium.waitTillElementIsClickable(btnPupAlreadyPaid);
        selenium.hardWait(2);
        selenium.clickOn(btnPupAlreadyPaid);
    }

    /**
     * Get page universal title
     * @return string data type
     */
    public String getSuccesPaidText() {
        return selenium.getText(textUniversalPageTitle);
    }

    /**
     * Get price number as string
     * @param addOnsName add ons name on the price list
     * @return string data type
     */
    public String getPriceNumberWithName(String addOnsName) {
        String priceNumberElement = "//*[contains(text(), '"+addOnsName+"')]/parent::*/following-sibling::*";
        return String.valueOf(JavaHelpers.extractNumber(selenium.getText(By.xpath(priceNumberElement))));
    }

    /**
     * Get additional price number as string
     * @param additionalPriceName
     * @return string data type
     */
    public String getAdditionalPriceNumberText(String additionalPriceName) {
        String additionalPriceElement = "//*[contains(text(), '"+additionalPriceName+"')]/following-sibling::*";
        return String.valueOf(JavaHelpers.extractNumber(selenium.getText(By.xpath(additionalPriceElement))));
    }
}
