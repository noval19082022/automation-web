package pageobjects.mamikos.tenant.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.io.IOException;

public class HomePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public HomePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(css = "#home > div.home-top-container > div > div > div")
    private WebElement searchBar;

    @FindBy(xpath = "//*[@class='bg-c-dropdown__trigger']")
    private WebElement adsSearchOptionList;

    /**
     * Click on 'Search' bar
     * @throws InterruptedException
     */
    public void clickSearchBar() throws InterruptedException {
        selenium.waitInCaseElementVisible(searchBar,3);
        selenium.hardWait(2);
        selenium.javascriptClickOn(searchBar);
    }

    /**
     * Click on option Ads Search
     * @param option select value option
     */
    public void selectOptionAdsSearch(String option) throws InterruptedException {
        selenium.javascriptClickOn(adsSearchOptionList);
        selenium.clickOn(By.xpath("//a[@href='/" + option.toLowerCase() + "']"));
    }
}
