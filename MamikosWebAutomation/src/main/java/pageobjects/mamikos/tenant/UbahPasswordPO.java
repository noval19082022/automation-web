package pageobjects.mamikos.tenant;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class UbahPasswordPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public UbahPasswordPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

        @FindBy(xpath = "//span[normalize-space()='Pengaturan']")
        private WebElement settingMenu;

        @FindBy(xpath = "//a[contains(text(),'Ubah')]")
        private WebElement clickUbahButton;

        @FindBy(css = "[name='Password lama']")
        private WebElement userFillsPasswordLama;

        @FindBy(css = "[name='Password baru']")
        private WebElement userFillsPasswordBaru;

        @FindBy(css = "[name='Password konfirmasi']")
        private WebElement userFillsPasswordConfirm;

        @FindBy(css = ".mami-button")
        private WebElement clickOnSimpanButton;

        @FindBy(xpath = "//span[.='Password berhasil diubah']")
        private WebElement messageSuccessUbahPassword;

        @FindBy(xpath = "//h1[@class='home-top-search-title']")
        private WebElement autoLogout;

        @FindBy(xpath = "//p[contains(text(),'Password maksimal 25 karakter')]")
        private WebElement messageErrorMoreThan;

        @FindBy(xpath = "//p[contains(text(),'Gunakan kombinasi huruf dan angka')]")
        private WebElement messageErrorSpecialCharacter;

        @FindBy(css = "button[data-path='btn_submitPasswordChange']")
        private WebElement changePasswordSubmitButton;

        /**
         * Click on Pengaturan
         * @throws InterruptedException
         */
        public void clickPengaturanButton () throws InterruptedException {
            selenium.hardWait(5);
            selenium.pageScrollInView(settingMenu);
            selenium.javascriptClickOn(settingMenu);
        }
        /**
         * Click on Ubah Button
         * @throws InterruptedException
         */
        public void clickUbahButton () throws InterruptedException {
            selenium.javascriptClickOn(clickUbahButton);
        }
         /**
          * Click on Simpan Button
          * @throws InterruptedException
          */
         public void clickOnSimpanButton () throws InterruptedException {
             selenium.clickOn(clickOnSimpanButton);
        }
        /**
         * Fills Password Lama
         */
         public void userFillsPasswordLama (String password){
             selenium.enterText(userFillsPasswordLama, password, false);
         }
         /**
         * Fills Password Baru
         */
         public void userFillsPasswordBaru (String newPassword) throws InterruptedException {
         selenium.enterText(userFillsPasswordBaru, newPassword, false);
         }
         /**
          * Fills Password Confirm
          */
        public void userFillsPasswordConfirm (String confirmPassword) throws InterruptedException {
        selenium.enterText(userFillsPasswordConfirm, confirmPassword, false);
        selenium.hardWait(3);
        }
        /**
          * Appears Message Success Change Password
          *
          */
        public String messageSuccessUbahPassword() throws InterruptedException {
        selenium.waitInCaseElementVisible(messageSuccessUbahPassword, 5);
        selenium.hardWait(3);
        return selenium.getText(messageSuccessUbahPassword);
        }
       /**
        * Auto logout and redirect to homepage
        *
        */
        public String autoLogout() throws InterruptedException {
        return selenium.getText(autoLogout);
    }
        /**
          * Appears Message Error More Than 25 Character
          *
          */
        public String messageErrorMoreThan(){
        selenium.waitInCaseElementVisible(messageErrorMoreThan, 5);
        return selenium.getText(messageErrorMoreThan);
    }
        /**
          * Appears Message Error Special Character
          *
          */
        public String messageErrorSpecialCharacter(){
        selenium.waitInCaseElementVisible(messageErrorSpecialCharacter, 5);
        return selenium.getText(messageErrorSpecialCharacter);
    }

    public Boolean isChangePasswordSubmitButtonDisabled(){
        return selenium.isElementAtrributePresent(changePasswordSubmitButton, "disabled");
    }

}

