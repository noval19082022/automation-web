package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.Arrays;
import java.util.List;

public class SectionBookingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SectionBookingPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(css = "#detailPriceCardDesktop > div > div.remaining-room > label")
    private WebElement remainingRoom;

    private static By priceType = By.cssSelector("#detailPriceCardDesktop div.card-price > div:nth-child(%s) span.price-type");

    private By priceTag = By.cssSelector("#detailPriceCardDesktop div.card-price > div:nth-child(%s) span.price-tag");

    @FindBy(css = ".content-container .owner-information")
    private WebElement ownerInformation;

    @FindBy(xpath = "//*[@class='owner-information__profile-data']/span[1]")
    private WebElement pemilikKost;

    @FindBy(css= "#priceCard .booking-card__info-price-amount")
    private WebElement defaultPrice;

    @FindBy(xpath= "//*[@class='booking-card__booking']/*[not(@disabled='disabled')]")
    private WebElement rentSubmitButton;

    /**
     * Get Remaining Room
     * @return String data type e.g "Sisa 4 Kamar"
     */
    public String getRemainingRoom(){
        return selenium.getText(remainingRoom);
    }

    /**
     * Get return remaining number only
     * @return int data type e.g 1,2,3,4
     */
    public int getRemainingRoomNumber() {
        List<String> roomNumber = Arrays.asList(getRemainingRoom().split(" "));
        return Integer.parseInt(roomNumber.get(1));
    }

    /**
     * Check if rent submit button is available
     * @return true or false
     */
    public boolean isRentSubmitButtonAvailable() {
        return selenium.waitTillElementIsVisible(rentSubmitButton, 5) != null;
    }

    /**
     * Get Room Default Price
     * @return string data type
     */
    public  String getDefaultPrice(){
        return selenium.getText(defaultPrice);
    }

    /**
     * Price Type and Price Tag Formatter
     * @return
     */
    public By cssElementFormatBy(By targetSelect, int listNumber) {
        String targetFormatCss = targetSelect.toString().replace("By.cssSelector: ", "");
        targetFormatCss = String.format(targetFormatCss, String.valueOf(listNumber));
        return By.cssSelector(targetFormatCss);
    }

    /**
     * Get Pemilik Kost
     * @return
     */
    public  String getOwnerKost() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 3000);
        selenium.waitInCaseElementVisible(pemilikKost, 15);
        return selenium.getText(pemilikKost);
    }
}
