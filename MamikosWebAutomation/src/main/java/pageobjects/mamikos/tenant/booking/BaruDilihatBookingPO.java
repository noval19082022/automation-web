package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BaruDilihatBookingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public BaruDilihatBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }
    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(., 'Baru Dilihat')]")
    private WebElement baruDilihatSection;

    @FindBy(css = ".empty-text")
    private WebElement getTextBaruDilihat;

    @FindBy(css = ".draft-booking-last-seen .btn-primary")
    private WebElement clickCariKostButton;

    @FindBy(css = "div.info-booking > div.info-detail > span")
    private WebElement clickLihatDetail;

    @FindBy(css = "div.draft-card-info > div.info-title > span")
    private WebElement kostNameBaruDilihat;

    @FindBy(css = "div.info-item > span.price-number.text-capitalize")
    private WebElement perBulanPrice;

    @FindBy(css = "div.info-booking > div:nth-child(1) > span:nth-child(2)")
    private WebElement tanggalMasuk;

    @FindBy(css = " div.info-booking > div:nth-child(2) > span:nth-child(2)")
    private WebElement durasi;

    @FindBy(css = "div > div > div > div > div.btn-cta-label")
    private WebElement searchBar;

    private By kosDetailBaruDilihat = By.id("detailKostContainer");

    @FindBy(css = ".detail-title.--kost-title")
    private WebElement kostNameOnKostDetail;

    @FindBy(css ="div.draft-last-seen-list > div:nth-child(1) > div.draft-footer > button.btn.btn-primary")
    private WebElement bookingLangsungButton;

    private By bookingFormpage = By.id("bookingContainer");

    @FindBy(css = " div.booking-breadcrumb__back-nav > span")
    private WebElement backButtonOnBookingForm;

    @FindBy(css = "div.modal-footer.confirmation-button-group > button.btn.btn-success")
    private WebElement simpanDraft;

    @FindBy(css = "#userBookingSection > div > ul > li:nth-child(2) > a")
    private WebElement draftBookingSection;

    private By ftueCancelBooking = By.cssSelector("#bookingModalCancel > div > div");

    @FindBy(css = "button.mami-text-counter__counter-plus:not(.--disabled)")
    private WebElement rentDurationButton;

    @FindBy(css = ".form-date-checkin .booking-input-checkin__input")
    private WebElement datePickerBox;

    @FindBy(css = "div#bookingActionButtons button")
    private WebElement selanjutnyaButton;

    @FindBy(css = "div.info-date--checkin > span.info-date--date")
    private WebElement getCheckinDate;

    @FindBy(css = "div.info-date--checkout > span.info-date--date")
    private WebElement getCheckoutDate;

    @FindBy(css = "div.info-date--rent-duration > span.info-date--date")
    private WebElement getDuration;

    @FindBy(css = "div.draft-footer > button.btn.btn-default")
    private WebElement deleteButtonKost;

    @FindBy(css = "#draftBookingModalDelete > div > div")
    private WebElement deleteFTUEPresent;

    @FindBy(css = "div.tenant-data-form__info-basic > div:nth-child(2) > input")
    private WebElement tenantPhoneNumber;

    @FindBy(css = "div.modal-actions > button.btn.btn-success")
    private WebElement cancelDeleteBaruDilihat;

    @FindBy(css = "div.modal-actions > button.btn.btn-primary")
    private WebElement confirmDeleteBaruDilihat;

    @FindBy( css = "span.text-disabled")
    private WebElement getAvailableRoom;

    @FindBy(css= "div.draft-footer > button.btn.btn-disabled")
    private WebElement fullRoomButton;

    @FindBy(css = ".mami-radio.selected")
    private WebElement perBulanselected;

    private By bookingFormPage = By.id("bookingContainer");

    @FindBy(css = ".draft-card-container .btn.btn-default:first-child")
    private WebElement deleteButtonFirstList;

    @FindBy(css=".fade.in .modal-container button:last-child")
    private WebElement btnConfirmDelete;

    @FindBy(xpath = "//div[@class='popper-ftue']")
    private WebElement ftuePromoNgebut;

    @FindBy(xpath = "//div[@class='popper-ftue__content']")
    private WebElement ftueBookingLangsung;

    @FindBy(xpath = "//button[contains(., 'Saya mengerti')]")
    private WebElement btnSayaMengertiOnCari;

     /**
     * Click On Baru dilihat menu
     * @throws InterruptedException
     */
    public void baruDilihatSection() {
              selenium.javascriptClickOn(baruDilihatSection);  }

    /**
     * return blank baru dilihat text example ""
     * @return rent period of selected text
     */
    public String getTextBaruDilihat() {
        return selenium.getText(getTextBaruDilihat);
    }

    /**
     * Click On Cari Kost Button
     * @throws InterruptedException
     */
    public void clickCariKostButton () {
        selenium.javascriptClickOn(clickCariKostButton);
    }

    /**
     *  get Kost name examples "Kost Garden Abepura"
     * @return text kost name on baru dilihat
     */
    public String getKostName() {
        return selenium.getText(kostNameBaruDilihat);
    }

    /**
     * Verify kost name on baru dilihat page
     */
    public Boolean isKostNameVisible(){
        return selenium.waitInCaseElementVisible(kostNameBaruDilihat,3)!= null;
    }

    /**
     * get Kost price for Bulanan examples "Rp500.000 Per Bulan"
     * @return kost price for Bulanan
     */
    public String getPriceBaruDilihat(){
        return selenium.getText(perBulanPrice);
    }

    /**
     *  get Tanggal Masuk examples "Belum Terisi"
     * @return tanggal masuk will be filled with belum terisi
     */
    public String getTanggalMasuk(){
        return  selenium.getText(tanggalMasuk);
    }

    /**
     * get durasi
     * @return durasi will be filled with belum terisi
     */
    public String getDurasi(){
        return  selenium.getText(durasi);
    }

    /**
     * Click On Search button
     * @throws InterruptedException
     */
    public void clickOnSearchBar(){
        selenium.javascriptClickOn(searchBar);
    }

    /**
     * get kost name on baru dilihat
     * @return True if element present false if element not present
     * @throws InterruptedException
     */
    public Boolean isInKosDetailBaruDilihat() throws InterruptedException {
        return selenium.isElementPresent(kosDetailBaruDilihat);
    }

    /**
     * get kost name on kost detail
     * return get Kost name examples "Kost Garden Abepura"
     * @return
     */
    public String getKostNameOnKosDetail() {
        return selenium.getText(kostNameOnKostDetail);
    }

    /**
     * Click On Lihat detail on kost list
     * @throws InterruptedException
     */
    public void clickLihatDetailButton () {
        selenium.javascriptClickOn(clickLihatDetail); }

    /**
     * wait and get url kost  detail
     * return get url Kost Detail
     * @return
     */
    public String getUrlcurrentwindows() {
        selenium.switchToWindow(2);
        return selenium.getURL();
    }

    /**
     * Click On Booking Langsung on baru dilihat page
     * @throws InterruptedException
     */
    public void clickOnBookingLangsung() throws InterruptedException{
        selenium.hardWait(2);
        selenium.javascriptClickOn(bookingLangsungButton);
    }

    /**
     * verify  booking form will present or not present
     * @return True if element present false if element not present
     * @throws InterruptedException
     */
    public Boolean isInBookingFormPage() throws InterruptedException {
        return selenium.isElementPresent(bookingFormpage);
    }

    /**
     * Click On Back button  on booking form
     * @throws InterruptedException
     */
    public void clickBackButtonOnBookingForm(){
        selenium.javascriptClickOn(backButtonOnBookingForm);
    }

    /**
     * Click On Simpan Draft on popup save draft booking
     * @throws InterruptedException
     */
    public void clickOnSimpanDraft(){
        selenium.javascriptClickOn(simpanDraft);
    }

    /**
     * verify  popup cancel booking
     * @return True if element present false if element not present
     * @throws InterruptedException
     */
    public Boolean isFTUECancelPresent(){
        return selenium.waitInCaseElementVisible(ftueCancelBooking, 3)!= null;
    }

    /**
     * Click Draft Booking
     * @throws InterruptedException
     */
    public void clickDraftBookingSection(){
        selenium.javascriptClickOn(draftBookingSection);
    }

    /**
     * Check if plus icon button is enable for 5 seconds
     * @return true if plus icon button enable otherwise false
     */
    public boolean isPlusIconButton() {
        return selenium.waitInCaseElementVisible(rentDurationButton, 5) != null;
    }

    /**
     * Increase Rent Duration
     * @throws InterruptedException
     */
    public void increaseRateDurationBaruDilihat() throws InterruptedException {
        if(isPlusIconButton()) {
            selenium.waitTillElementIsClickable(rentDurationButton);
            selenium.hardWait(2);
            selenium.clickOn(rentDurationButton);
        }
    }

    /**
     * Click On Selanjutnya button
     * @throws InterruptedException
     */
    public void selanjutnyaButtonOnForm() throws InterruptedException {
        selenium.waitInCaseElementVisible(selanjutnyaButton,2);
        selenium.hardWait(2);
        selenium.clickOn(selanjutnyaButton);
    }

    /**
     *  get Checkin Date on form
     * @return checkin date
     */
    public String getCheckinDate() throws InterruptedException{
        return selenium.getText(getCheckinDate);
    }

    /**
     * get checkout date on booking form
     * @return  Checkout Date
     */
    public String getCheckoutDate() throws InterruptedException{
        return selenium.getText(getCheckoutDate);
    }

    /**
     * return get Duration
     * @return duration on preview
     */
    public String getDurationOnPreview() throws InterruptedException{
        return selenium.getText(getDuration);
    }

    /**
     * Input tenant Phone number
     * @return tenant phone number
     */
    public void inputTenantPhoneNumber(String phoneNumber) throws InterruptedException{
        selenium.waitInCaseElementVisible(tenantPhoneNumber, 2);
        selenium.clickOn(tenantPhoneNumber);
        selenium.hardWait(2);
        selenium.enterText(tenantPhoneNumber, phoneNumber, false);
    }

    /**
     * Click On Trash Icon on baru dilihat page
     * @throws InterruptedException
     */
    public void clickOnTrashIcon() throws InterruptedException {
        if(selenium.waitInCaseElementVisible(deleteButtonKost, 3) != null) {
            selenium.clickOn(deleteButtonKost);
        }
    }

    /**
     * verify delete ftue
     * @return True if element present false if element not present
     * @throws InterruptedException
     */
    public Boolean isFTUEDeletePresent() {
        return selenium.waitInCaseElementVisible(deleteFTUEPresent, 3)!= null;
    }

    /**
     * Click Batal Button delete kost
     * @throws InterruptedException
     */
    public void clickOnCancelDeleteButton() throws InterruptedException {
        if(selenium.waitInCaseElementVisible(cancelDeleteBaruDilihat, 3) != null) {
            selenium.clickOn(cancelDeleteBaruDilihat);
        }
    }

    /**
     * Click On Confirm Delete button on confirmation popup
     * @throws InterruptedException
     */
    public void clickOnConfirmDeleteBooking() throws InterruptedException {
        if(selenium.waitInCaseElementVisible(confirmDeleteBaruDilihat, 3) != null) {
            selenium.clickOn(confirmDeleteBaruDilihat);
        }
    }

    /**
     * get full room on baru dilihat section
     * @return Text Room Available on baru dilihat list example "Tersedia 5 Kamar"
     * @throws InterruptedException
     */
    public String getFullRoom() throws InterruptedException {
        return selenium.getText(getAvailableRoom);
    }

    /**
     * get status text on button
     * @return Text Room available on button will full or not full
     * @throws InterruptedException
     */
    public String getBookingStatus()  {
        return selenium.getText(fullRoomButton);
    }


    /**
     * verify booking form
     * @return True if element present false if element not present
     * @throws InterruptedException
     */
    public Boolean isInKostDetail() throws InterruptedException {
        return selenium.isElementPresent(bookingFormPage);

    }

    /**
     * Get kost name on newly seen based on their order 1 is the lates
     * @param index 1-5
     * @return String data type e.g "Kost Wild Rift, Naglik, Sleman
     */
    public String getKostNameIndex(int index) {
        String elementKostName = ".draft-card-container:nth-child("+ index +") .info-title";
        By element = By.cssSelector(elementKostName);
        return selenium.getText(element);
    }

    /**
     * Click on direct booking based on index set
     * @param index 1-5
     * @throws InterruptedException
     */
    public void clicksOnDirectBookingIndex(Integer index) throws InterruptedException {
        String elementDirectBooking = ".draft-card-container:nth-child("+ index +") button:last-child";
        By element = By.cssSelector(elementDirectBooking);
        selenium.waitInCaseElementVisible(element, 2);
        selenium.clickOn(element);
        selenium.switchToWindow(1);
    }

    /**
     * Check if delete/trash button visible in screen
     * @return true if visible otherwise false
     */
    private boolean isNewlySeenDeleteButtonVisible() {

        return     selenium.waitInCaseElementVisible(deleteButtonFirstList, 2) != null;

    }

    /**
     * Delete all newly seen, max iteration is 10
     * @throws InterruptedException
     */
    public void deleteAllNewlySeen() throws InterruptedException {
        int i = 0;
        while (isNewlySeenDeleteButtonVisible()) {
            selenium.clickOn(deleteButtonFirstList);
            selenium.clickOn(btnConfirmDelete);
            selenium.hardWait(2);
            if(i==10) {
                break;
            }
            else if(!isNewlySeenDeleteButtonVisible()) {
                break;
            }
            i++;
        }
    }

    /**
     * check if ftue promo ngebut is showing
     */
    public void checkAndClickFtuePromoNgebut() throws InterruptedException {
        if(selenium.waitInCaseElementVisible(ftuePromoNgebut, 2) != null){
            selenium.clickOn(btnSayaMengertiOnCari);
        }
    }

    /**
     * check if ftue booking langsung is showing
     */
    public void checkAndClickFtueBookingLangsung() throws InterruptedException {
        if(selenium.waitInCaseElementVisible(ftueBookingLangsung, 2) != null){
            selenium.clickOn(btnSayaMengertiOnCari);
        }
    }
}
