package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class BookingListPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    // continue button in pop up
    @FindBy(xpath = "(//*[@id='bookingListCard'])[1]")
    private WebElement firstBooking;

    @FindBy(id = "bookingListCard")
    private List<WebElement> bookingList;

    @FindBy(xpath = "//*[@class='icon-empty']")
    private WebElement bookingListIsBlankLabel;

    @FindBy(xpath = "(//button[contains(text(),'Check-in Kos')])[2]")
    private WebElement checkInButton;

    @FindBy(xpath = "(//button[@type='button'][normalize-space()='Check-in'])[1]")
    private WebElement finishCheckInButton;

    @FindBy(xpath = "//button[contains(text(),'Bayar Pelunasan Sekarang')]")
    private WebElement repaymentButton;

    @FindBy(xpath = "(//*[@class='booking-card'])[1]//*[@class='footer-buttons']/button[contains(text(),'Kost Saya')]")
    private WebElement firstMyKostButton;

    /**
     * Get number of booking list
     * @throws InterruptedException
     * @return number of elements
     */
    public int getNumberListOfBooking() throws InterruptedException {
        selenium.hardWait(2);
        int numberOfElements = 0;
        if(selenium.waitInCaseElementVisible(firstBooking, 5) != null){
            numberOfElements = bookingList.size();
        }else{
            selenium.waitTillElementIsVisible(bookingListIsBlankLabel, 5);
        }
        return numberOfElements;
    }

    /**
     * Get one data booking status
     * @param index is number for specific data want to get
     * @return booking status
     */
    public String getBookingStatus(int index) {
        By element = By.xpath("(//*[@class='card-header']/label)[" + index + "]");
        return selenium.getText(element);
    }

    /**
     * Get one data property name
     * @param index is number for specific data want to get
     * @return property name
     */
    public String getPropertyName(int index) {
        By element = By.xpath("(//*[@id='bookingListCard'])[" + index + "]/div/div[2]//label[@class='kost-title']");
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Get one data rental period
     * @param index is number for specific data want to get
     * @return rental period
     */
    public String getRentalPeriod(int index) {
        By element = By.xpath("(//label[text()='Durasi Sewa']/parent::div/following-sibling::label)[ " + index + "]");
        selenium.waitTillElementIsVisible(element, 10);
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Click pay now button on specific data
     * @param index is number for specific data want to get
     * @throws InterruptedException
     */
    public void clickOnPayNowButton(int index) throws InterruptedException {
        By element = By.xpath("(//*[@class='footer-buttons']/a)[" + index + "]");
        selenium.pageScrollInView(element);
        selenium.javascriptClickOn(element);
    }

    /**
     * Click check in button on specific data
     * @param index is number for specific data want to get
     * @throws InterruptedException
     */
    public void clickOnCheckIn(int index) throws InterruptedException {
        By element = By.xpath("(//*[@id='bookingListCard'])[" + index + "]//*[@class='footer-buttons']/child::button[@class='btn btn-primary']");
        selenium.pageScrollInView(element);
        selenium.javascriptClickOn(element);
        selenium.hardWait(5 );
        selenium.javascriptClickOn(checkInButton);
        selenium.javascriptClickOn(finishCheckInButton);
    }

    /**
     * Click check in button on specific data
     * Click repayment button
     * @param index is number for specific data want to get
     * @throws InterruptedException
     */
    public void clickOnCheckInAfterRepayment(int index) throws InterruptedException {
        By element = By.xpath("(//*[@id='bookingListCard'])[" + index + "]//*[@class='footer-buttons']/child::button[@class='btn btn-primary']");
        selenium.pageScrollInView(element);
        selenium.javascriptClickOn(element);
        selenium.clickOn(repaymentButton);
        if (selenium.waitInCaseElementPresent(element, 3) != null) {
            selenium.pageScrollInView(element);
            selenium.javascriptClickOn(element);
        }
    }

    public void clickOnFirstMyKostButton() throws InterruptedException {
        selenium.clickOn(firstMyKostButton);
    }
}
