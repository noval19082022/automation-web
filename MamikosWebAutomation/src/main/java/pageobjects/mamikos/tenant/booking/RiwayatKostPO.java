package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class RiwayatKostPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public RiwayatKostPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//a[contains(., 'Riwayat Kos')]")
    private WebElement riwayatKosMenu;

    @FindBy(xpath = "//h1[@class='title'][contains(., 'Riwayat Kos')]")
    private WebElement riwayatKosPage;

    @FindBy(xpath = "(//div[@class='user-review-card history-kost-card__review'])[1]")
    private WebElement reviewKostCardEmpty;

    @FindBy(xpath = "//span[contains(., 'Tulis review:')]")
    private WebElement containOfReviewKost;

    @FindBy(xpath = "(//a[@class='btn-close-filter has-text'])[1]")
    private WebElement closeButtonReviewKost;

    @FindBy(xpath = "(//b[contains(., 'Lihat Detail')])[1]")
    private WebElement lihatDetailButton;

    @FindBy(xpath = "//b[contains(., 'Lihat Fasilitas')]")
    private WebElement lihatFasilitasButton;

    @FindBy(xpath = "//a[contains(., 'Lihat Riwayat Transaksi')]")
    private WebElement lihatRiwayatTransaksiButton;

    @FindBy(xpath = "//div[@id='app']")
    private WebElement scrollPage;

    @FindBy(xpath = "//a[@class='link-back'][contains(., 'Kembali ke Booking')]")
    private WebElement kembaliKeBookingButton;

    @FindBy(xpath = "//button[@type='button'][contains(., 'Chat Pemilik')]")
    private WebElement chatPemilikButton;

    private By backButtonChatRoom = By.cssSelector("body > div div.mc-chat-room__header > button:nth-child(1)");

    @FindBy(xpath = "//div[@class='mc-channel-list__header']/child::button")
    private WebElement closeChatRoom;

    @FindBy(xpath = "//a[@class='btn btn-primary'][contains(., 'Booking Ulang')]")
    private WebElement bookingUlangButton;

    @FindBy(xpath = "//div[@class='booking-title-form']")
    private WebElement bookingFormTitle;

    @FindBy(xpath = "//div[@class='user-review-card']")
    private WebElement reviewKostOnRiwayatKosDetail;

    @FindBy(xpath = "//h4[@class='empty-title'][contains(., 'Belum Ada Kos')]")
    private WebElement emptyStateTitleRiwayatKos;

    @FindBy(xpath = "//*[@class='empty-desc']//following-sibling::span")
    private WebElement emptyStateSubtitleRiwayatKos;

    @FindBy(css = ".user-review-card--flexbox" )
    private WebElement titleRiwayatKosReview;

    private By kostReviewEntryPoint = By.xpath("//*[@class='user-review-card--empty bg-c-card bg-c-card--md bg-c-card--light']");

    @FindBy(className = "add-comment__title")
    private WebElement titleReview;

    /**
     * Click Riwayat Kos menu
     */
    public void clickRiwayatKosMenu() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(riwayatKosMenu);
    }

    /**
     * Get title Riwayat Kos page
     */
    public String getTitleRiwayatKos(){
        return selenium.getText(riwayatKosPage);
    }

    /**
     * Click review kost card when user not review kos yet
     */
    public void clickReviewKos() throws InterruptedException {
        selenium.clickOn(reviewKostCardEmpty);
    }

    /**
     * Click review kost card when user not review kos yet on riwayat kost detail
     */
    public void clickReviewKostOnRiwayatKostDetail() throws InterruptedException {
        selenium.clickOn(reviewKostOnRiwayatKosDetail);
    }

    /**
     * Get one of contains of review kost page
     */
    public String getContainOfReviewKost(){
        return selenium.getText(containOfReviewKost);
    }

    /**
     * This method also use on :
     * - Click close button on review kost page
     * - Click close button on Lihat Fasilitas page
     */
    public void clickCloseRiwayatKost() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(closeButtonReviewKost);
    }

    /**
     * Click on Lihat Detail button on riwayat kost
     */
    public void clickLihatDetail() throws InterruptedException {
        selenium.clickOn(lihatDetailButton);
        selenium.hardWait(2);
    }

    /**
     * Click Lihat Fasilitas on Riwayat Kost detail
     */
    public void clickLihatFasilitas() throws InterruptedException {
        selenium.clickOn(lihatFasilitasButton);
    }

    /**
     * Click Lihat Riwayat Transaksi on Riwayat Kost detail
     */
    public void clickLihatRiwayatTransaksi() throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//div[@class='detail-sections']"));
        selenium.pageScrollInView(element);
        selenium.hardWait(2);
        selenium.clickOn(lihatRiwayatTransaksiButton);
    }

    /**
     * Click Kembali ke Booking
     */
    public void clickKembaliKeBooking() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(kembaliKeBookingButton);
        selenium.hardWait(2);
    }

    /**
     * Click Chat Pemilik on riwayat kost detail
     */
    public void clickChatPemilik() throws InterruptedException {
        selenium.pageScrollInView(chatPemilikButton);
        selenium.hardWait(2);
        selenium.clickOn(chatPemilikButton);
    }

    /**
     * click Back button on chat room
     */
    public void clickBackAndClickCloseChatRoom() throws InterruptedException{
        if (selenium.isElementPresent(backButtonChatRoom)) {
            selenium.clickOn(backButtonChatRoom);
        } else {
            selenium.clickOn(closeChatRoom);
        }
    }

    /**
     * Click booking ulang on riwayat kost detail
     */
    public void clickBookingUlang() throws InterruptedException {
        selenium.clickOn(bookingUlangButton);
        selenium.switchToWindow(2);
    }

    /**
     * Get title Booking form
     */
    public String getTitleBookingForm(){
        return selenium.getText(bookingFormTitle);
    }

    /**
     * Get empty state title at Riwayat kos
     */
    public String getEmptyStateTitle(){
        return selenium.getText(emptyStateTitleRiwayatKos);
    }

    /**
     * Get empty state subtitle at Riwayat kos
     */
    public String getEmptyStateSubtitle(){
        return selenium.getText(emptyStateSubtitleRiwayatKos);
    }

    /**
     * Get Riwayat Kos Review title Text
     *
     * @return string "Bagaimana pengalaman ngekosmu?"
     */
    public String getTitleRiwayatKosReviewText(){
        return selenium.getText(titleRiwayatKosReview);
    }

    /**
     * Verify Kost Review entry point is not displayed
     * @return boolean
     */
    public Boolean isKostReviewEntryPointNotDisplayed() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.isElementPresent(kostReviewEntryPoint);
    }

    /**
     * Click title riwayat kos review text
     * @throws InterruptedException
     */
    public void clickOnRiwayatKosReviewText() throws InterruptedException {
        selenium.javascriptClickOn(titleRiwayatKosReview);
    }

    /**
     * Get Review title Text
     *
     * @return string "Tulis review kamu lebih lanjut"
     */
    public String getTitleReviewText(){
        return selenium.getText(titleReview);
    }
}