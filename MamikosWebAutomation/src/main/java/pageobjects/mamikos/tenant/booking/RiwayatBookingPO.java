package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class RiwayatBookingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public RiwayatBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    //---------------Check Butuh Pembayaran status---------------//
    @FindBy(xpath = "//ul[@data-testid='userMenu-list']/child::*[2]")
    private WebElement riwayatNdrafBooking;

    @FindBy(xpath = "//div[@class='filter-button-mobile']/child::*[1]")
    private WebElement filterBooking;


    //--------------Click Lihat selengkapnya button-------------//
    @FindBy(xpath = "//*[@class='card-toggle hidden-xs']")
    private WebElement lihatSelengkapnyaButton;

    @FindBy(xpath="//*[contains(text(), 'Apakah uang saya bisa dikembalikan?')]")
    private WebElement refundText;


    //---------------Check Butuh Pembayaran status---------------//
    /**
     * click on Riwayat dan Draft Booking menu
     * @throws InterruptedException
     */
    public void clickRiwayatBooking() throws InterruptedException {
        selenium.waitTillElementIsClickable(riwayatNdrafBooking);
        selenium.clickOn(riwayatNdrafBooking);
    }

    /**
     * click on Filter at Riwayat tab
     * @throws InterruptedException
     */
    public void clickFilterBooking() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(filterBooking);
    }

    /**
     * choose booking status
     * e.g Tunggu Konfirmasi, Terbayar
     * @param statusBooking
     * @throws InterruptedException
     */
    public void chooseBookingStatus(String statusBooking) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//div[@class='pick-a-boo-content']//button[@type='button'][contains(., '" +statusBooking + "')]"));
        selenium.clickOn(element);
        selenium.hardWait(2);
    }

    /**
     * get booking status on booking list riwayat tab
     * @param statusBooking
     */
    public String getBookingStatus(String statusBooking){
        WebElement element = driver.findElement(By.xpath("//label[@class='--waiting'][contains(., '" + statusBooking + "')]"));
        return selenium.getText(element);
    }

    /**
     * click on Lihat seengkapnya
     * @throws InterruptedException
     */
    public void clickSelengkapnyaButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.click(lihatSelengkapnyaButton);
    }

    /**
     * click on Apakah uang saya bisa dikembalikan link
     * @throws InterruptedException
     */
    public void clickOnRefundLink() throws InterruptedException {
        selenium.pageScrollInView(refundText);
        selenium.hardWait(2);
        selenium.javascriptClickOn(refundText);
    }
}
