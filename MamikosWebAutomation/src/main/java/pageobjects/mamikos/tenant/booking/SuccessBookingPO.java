package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class SuccessBookingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SuccessBookingPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);
    }

    private By chatPemilikKosAfterBooking = By.cssSelector("#bookingContainer > div.booking-success button");

    private By closeChatBoxAfterBookingSuccess = By.xpath("//*[@class='btn ic-close']");

    private By closeAllChatBoxAfterBookingSuccess = By.xpath("//*[@class='btn ic-minimize']");

    @FindBy(xpath = "(//*[@class='footer-buttons']//button[contains(text(), 'Ya, Batalkan')])[2]")
    private WebElement yesCancelButton;

    @FindBy(xpath = "//*[@class='booking-list-card'][1]//*[contains(text(), 'Lihat selengkapnya')]")
    private WebElement firstViewMore;

    @FindBy(xpath = "//*[@class='detail-booking']")
    private WebElement firstDetailTenant;

    @FindBy(xpath = "(//button[contains(text(),'Batalkan Booking')])[2]")
    private WebElement cancelBookingButton;

    @FindBy(xpath = "//div[@class='col-xs-12']/div[1]//label[@class='--failed']")
    private WebElement bookingStatusCancel;

    /**
     * click chat pemilik kos after success booking
     * @throws InterruptedException
     */
    public void clickOnChatPemilikKos() throws InterruptedException{
        selenium.hardWait(20);
        selenium.clickOn(chatPemilikKosAfterBooking);
        selenium.hardWait(5);
    }

    /**
     * close chat box after booking
     * @throws InterruptedException
     */
    public void clickCloseOnChatBox() throws InterruptedException{
        selenium.clickOn(closeChatBoxAfterBookingSuccess);
        selenium.clickOn(closeAllChatBoxAfterBookingSuccess);
    }
}