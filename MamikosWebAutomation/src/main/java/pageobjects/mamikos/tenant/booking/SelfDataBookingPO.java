package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class SelfDataBookingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public SelfDataBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='--is-active'][contains(text(), 'Data Penyewa')]")
    private WebElement activeSelectedSelfData;

    @FindBy(id = "tenantGender")
    private WebElement genderRadioButton;

    @FindBy(xpath = "//*[@role='radiobutton']//*[.='%s']")
    private WebElement radioButtonGender;

    private By radioButtonGenderBy = By.xpath("//*[@role='radiobutton']//*[.='%s']");

    @FindBy(css = "#tenantGender .selected .mami-radio-label")
    private WebElement selectedGenderRadio;

    /**
     * Check and wait if self data page is active by checking class @class='--is-active'
     * @return true if element present otherwise false
     */
    public boolean isSelfDataIsActivePresent() {
        return selenium.waitInCaseElementVisible(activeSelectedSelfData, 3) != null;
    }

    /**
     * Check and wait if radio button to select gender is present
     * @return true if element present otherwise false
     */
    public boolean isGenderRadioButtonPresent() {
        return selenium.waitInCaseElementVisible(genderRadioButton, 3) != null;
    }

    /**
     * Format element radio button to male or female
     * @param toFormat fill in with "Laki-laki" or "Perempuan"
     * @return By element after format
     */
    private By elementFormat(String toFormat) {
        String element = radioButtonGenderBy.toString().replace("By.xpath: ", "");
        element = String.format(element, String.valueOf(toFormat));
        return By.xpath(element);
    }

    /**
     * Click on radio button based on parameter
     * @param gender fill in with "Laki-laki" or "Perempuan"
     * @throws InterruptedException
     */
    public void clickOnRadioButton(String gender) throws InterruptedException {
        selenium.clickOn(elementFormat(gender));
    }

    /**
     * Get selected radio button text
     * @return String data type return "Laki-laki" or "Perempuan"
     */
    public String getSelectedRadioButtonText() {
        return selenium.getText(selectedGenderRadio);
    }
}
