package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class KosDetailPO {

    WebDriver driver;
    SeleniumHelpers selenium;


    public KosDetailPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    // continue button in pop up
    @FindBy(css = "button.btn-next")
    private WebElement continueBtn;

    @FindBy(xpath = "//*[@class='btn-primary']")
    private WebElement mauDongBtn;

    @FindBy(xpath = "//*[@class='btn-secondary']")
    private WebElement promoNgebutBtn;

    private By ftueScreenPopUpBody = By.cssSelector(".onboarding-ftue.highlighted");

    @FindBy(css = "#detailKostContainer .owner-information button.owner-information__chat")
    private WebElement chatKosButton;

    @FindBy(css = "button.btn-primary.swiper-button-disabled")
    private WebElement iUnderstand;

    @FindBy(css = "#detailKostOverviewFavShare > div.detail-kost-overview-widget__favorite-button > button")
    private WebElement favouriteSaveButton;

    @FindBy(css = "button[class='bg-c-button detail-kost-additional-widget__outer bg-c-button--tertiary bg-c-button--md track-favorite']")
    private WebElement unFavourite;

    @FindBy(xpath = "//a[@class='btn-secondary'][contains(text(), 'Saya Mengerti')]")
    private WebElement iUnderstandButton;

    private By iUnderstandButtonBy = By.xpath("//button[contains(text(),'Saya Mengerti')]");

    private By kosDetailPage = By.id("detailKostContainer");

    @FindBy(id = "photoContainer")
    private By imageBoxSlider;

    @FindBy(id = "priceCard")
    private By kosPriceBox;

    @FindBy(id = "swal2-title")
    private WebElement favouriteCompleteBox;

    private By favouriteCompleteBoxBy = By.id("swal2-container");

    @FindBy(css = "button.swal2-confirm.swal2-styled")
    private WebElement OK_button;

    @FindBy(css = ".booking-card__booking-action")
    private WebElement bookingButton;

    @FindBy(css = ".navbar-back[title='Back Arrow']")
    private WebElement bookingBackBtn;

    @FindBy(xpath = "//*[@id='bookingModalGenderMismatch' and @style]//*[@title='Tutup']")
    private WebElement closePopupDifferentGenderBtn;

    private By heartIconRed = By.xpath("//button[contains(text(),'Hapus')]");

    private By heartIconGrey = By.xpath("//button[contains(text(),'Simpan')]");

    private By loadingAnimation = By.cssSelector("body div.swal2-loading:last-child");

    private By textDifferentGender = By.cssSelector(".modal-gender-caption > label");

    private By textKostTitle = By.cssSelector("#detailTitle .detail-title__room-name");

    @FindBy(xpath = "//b[.='Ketentuan pengajuan sewa']")
    private WebElement viewFacilityButton;

    @FindBy(xpath = "//h6[contains(text(),'Review')]")
    private WebElement reviewText;

    private By loginPopup = By.xpath("//label[contains(.,'Nomor Handphone')]");

    private By loginGmailButton = By.xpath("//button[@class='btn btn-action btn-google']");

    private By loginFBButton = By.xpath("//button[@class='btn btn-action btn-facebook']");

    private By loginOwnerText = By.cssSelector(".login-owner-home");

    private By loginTenantText = By.cssSelector(".login-user-home");

    private By mapsPointer = By.xpath("//span[@class='kost-location__title']");

    private By dailyPrice = By.cssSelector(".--daily-price");

    private By perBulanDuration = By.xpath("//*[.='/ bulan']");

    private By dateTextBox = By.xpath("//input[@class='booking-input-checkin__input']");

    private By tomorrowDateLabel = By.xpath("//span[@class='cell day']");

    private By saturdayDateLabel = By.xpath("//span[contains(text(),'Sab')]");

    private By sundayDateLabel = By.xpath("//span[contains(text(),'Min')]");

    private By rentTypeBox = By.cssSelector(".booking-rent-type");

    @FindBy(xpath = "(//label[contains(text(),'Lihat selengkapnya')])[1]")
    private WebElement firstViewMore;

    @FindBy(xpath = "(//button[contains(text(),'Batalkan Booking')])[2]")
    private WebElement cancelBookingButton;

    @FindBy(xpath = "//a[@class='nav-brand brand-short']")
    private WebElement mamikosHomepageBtn;

    @FindBy(xpath = "(//button[contains(text(),'Ya, Batalkan')])[2]")
    private WebElement yesCancelButton;

    @FindBy(css=".fade.in .booking-cancel-container button.btn-primary")
    private WebElement btnOkAfterCancel;

    @FindBy(xpath = "(//span[contains(text(),'Lihat lebih sedikit')])[1]")
    private WebElement firstSeeLessButton;

    @FindBy(xpath = "//*[@class='detail-booking']")
    private WebElement firstDetailTenant;

    @FindBy(css = "img[alt='Mamikos Promo Ngebut']")
    private WebElement flashSaleFTUE;

    @FindBy(css = ".popper-ftue__content-title")
    private WebElement promoNgebutFTUE;

    @FindBy(css = ".onboarding-ftue-content .btn-secondary")
    private WebElement closeFlashSaleFTUEButton;

    @FindBy(css = ".popper-ftue__content-button")
    private WebElement sayaMengertiButton;

    @FindBy(css = ".checkin-date-section input:not([autocomplete*=off])")
    private WebElement datePicker;

    @FindBy(css = ".is-absolute .day__month_btn + .next")
    private WebElement nextMonthButton;

    @FindBy(xpath = "//*[@class='detail-kost-overview__gender']")
    private WebElement kostType;

    @FindBy(xpath = "//*[.='LIHAT']")
    private WebElement seeButton;

    @FindBy(css = ".bg-c-button.auth-step-card__nav-action.bg-c-button--tertiary.bg-c-button--md bg-c-button--icon-only-md")
    private WebElement loginPopupBackButton;

    //---------- FTUE Booking Benefit ---------

    @FindBy(xpath = "//*[@class='onboarding-ftue-content']//img[contains(@alt,'pertama')]")
    private WebElement FTUEImg1;

    @FindBy(xpath = "//*[@class='onboarding-ftue-content']//img[contains(@alt,'kedua')]")
    private WebElement FTUEImg2;

    @FindBy(xpath = "//*[@class='onboarding-ftue-content']//img[contains(@alt,'ketiga')]")
    private WebElement FTUEImg3;

    @FindBy(xpath = "//*[@class='onboarding-ftue-content']//img[contains(@alt,'keempat')]")
    private WebElement FTUEImg4;

    @FindBy(xpath = "//*[@class='onboarding-ftue-content']//img[contains(@alt,'kelima')]")
    private WebElement FTUEImg5;

    @FindBy(css = ".cta-wrapper .btn-secondary")
    private WebElement learnMoreButton;

    //--------- share container -----------

    @FindBy(xpath = "//button[contains(text(),'Bagikan')]")
    private WebElement shareButton;

    @FindBy(xpath = "//*[@id='mamiShareContainer']/a[@class='share-network-facebook']")
    private WebElement shareFacebookButton;

    // ------------- Benefit --------------

    @FindBy(css = "#detailKostContainer .detail-container__kost-benefit")
    private WebElement kosBenefit;

    @FindBy(css = "#detailKostContainer .detail-kost-benefit-content span")
    private WebElement benefitTitle;

    @FindBy(css = "#detailKostContainer .detail-kost-benefit-content p")
    private WebElement benefitDesc;

    // ------------- Facility -------------

    @FindBy(css = "#detailKostContainer .detail-container__facilities .detail-container__facilities-title")
    private WebElement facilityTitleText;

    @FindBy(css = ".detail-container__facilities .price-card-container .card-price")
    private WebElement facilityPriceInfo;

    @FindBy(css = ".kost-fac-container .fac-detail .text-normalize")
    private WebElement facilityRoomSize;

    @FindBy(css = ".kost-fac-container .fac-detail-header:nth-child(1) .text-normalize")
    private WebElement topFacility;

    @FindBy(xpath = "(//div[@class='fac-item']//div[@class='fac-item__title']/following-sibling::div/div/h3)[1]")
    private WebElement topFacility2;

    @FindBy(xpath = "//div[@class = 'fac-top__title'][2]")
    private WebElement topFacilityTitle;

    @FindBy(xpath = "//div[@class = 'fac-top__button']/button")
    private WebElement facilitySeeAllButton;

    @FindBy(css = "#detailKostContainer .kost-rules .kost-rules__divider")
    private WebElement kosRuleDevider;

    @FindBy(css = ".detail-kost-room-facilities")
    private WebElement facilityRoomTitle;

    @FindBy(xpath = "//*[@class='fac-item']//h2[contains(text(), 'Fasilitas kamar mandi')]")
    private WebElement facilityBathRoomTitle;

    @FindBy(xpath = "//*[@class='fac-item']//h2[contains(text(), 'Fasilitas parkir')]")
    private WebElement facilityParkingTitle;

    @FindBy(css = ".detail-kost-room-facilities")
    private WebElement facilityRoomSection;


    // ------------ kos rule -------------

    @FindBy(css = ".fac-top .fac-top__button .btn-fac-more")
    private WebElement seeMoreFacButton;

    @FindBy(css = " .detail-kost-rules__title")
    private WebElement kosRuleTitle;

    @FindBy(css = " .detail-kost-rules__content")
    private WebElement kosRuleSectiom;


    @FindBy(css = ".kost-rules .kost-rules-list .kost-rules-list__item:nth-child(1)")
    private WebElement kosRuleElement;

    @FindBy(css = ".kost-rules-gallery > div:nth-of-type(1)")
    private WebElement kosRuleImageElement;

    @FindBy(css = ".kost-gallery-modal-content__gallery-next.swiper-arrow")
    private WebElement iconArrowNextButton;

    @FindBy(css = ".kost-gallery-modal-content__gallery-prev.swiper-arrow")
    private WebElement iconArrowPrevButton;

    @FindBy(css = ".modal-content .kost-gallery-modal-content")
    private WebElement kosRulePopUpImageElement;

    @FindBy(xpath = "//div[@class='kost-rules__content']")
    private WebElement kosRuleContent;

    @FindBy(css = ".detail-kost-rules__see-all")
    private WebElement seeAllKosRuleButton;


    // ------------ Map section -----------

    @FindBy(id = "detailMap")
    private WebElement kostMapContainer;

    @FindBy(css = ".kost-location .kost-location__address")
    private WebElement kosLocationAddressText;

    @FindBy(css = ".kost-location .kost-location__map-button")
    private WebElement viewMapsButton;

    @FindBy(xpath = "//button[contains(text(),'Tanya alamat lengkap')]")
    private WebElement askAddressButton;

    @FindBy(css = ".kost-landmark-list__tabs")
    private WebElement tabPOILandmark;

    // ------------ Info Tambahan --------------

    @FindBy (css = "#kostDescription .kost-desc__title")
    private WebElement descKosText;

    @FindBy(css = "#kostDescription .kost-desc__see-all")
    private WebElement infoTambahanButton;

    // ------------ Owner Lower Section -----------

    @FindBy(css = ".owner-information .bg-c-divider")
    private WebElement OwnerDeviderLine;

    @FindBy(css = "#detailKostContainer  .owner-information .owner-information__profile .owner-information__name")
    private WebElement ownerNameText;

    @FindBy(css = "#detailKostContainer .owner-information__profile")
    private WebElement ownerImageProfile;

    @FindBy(css = " #detailKostContainer  .owner-information__type")
    private WebElement ownerStatus;

    @FindBy(css = "#detailKostContainer .owner-kost-information__label")
    private WebElement successfulTansaction;

    @FindBy(css = "#detailKostContainer .owner-rate-information__info:nth-child(1)")
    private WebElement bookingProcessed;

    @FindBy(css = "#detailKostContainer .owner-rate-information__info:nth-child(2)")
    private WebElement bookingChance;

    @FindBy(css="button.owner-information__chat")
    private WebElement askKostOwnerButton;

    @FindBy(css = ".owner-information__profile-img")
    private WebElement ImageOwner;

    @FindBy(xpath = "//h3[.='Pemilik kos terverifikasi']")
    private WebElement verifiedOwner;

    @FindBy(css = ".detail-kost-owner-section__kost-keeper")
    private WebElement ownerStatement;

    // ------------ Kos Report Section -----------

    @FindBy(css = ".kost-report-container .kost-report")
    private WebElement kosReportContainer;

    @FindBy(css = ".kost-report-container .text-primary")
    private WebElement kosReportButton;

    @FindBy(css = ".modal-body .modal-promo__title")
    private WebElement popUpReportKos;

    @FindBy(id = "reportDescription")
    private WebElement reportTextBox;

    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--destructive bg-c-button--md bg-c-button--block']")
    private WebElement sendReportButton;

    @FindBy(xpath = "(//div[@class='modal-report__body']//label)[1]")
    private WebElement checkBoxKosReport;

    //--------------------------------------

    @FindBy(id = "swal2-content")
    private WebElement reportConfirmationPopUp;

    @FindBy(css = "#detailKostContainer button.owner-information__rates-link")
    private WebElement aboutStatisticsButton;

    @FindBy(css = "#modalDetailRate .modal-dialog .modal-content")
    private WebElement statisticModal;

    @FindBy(css = "#modalDetailRate .owner-rate-modal-header .owner-rate-modal-header__close")
    private WebElement closeStatisticsModalBtn;

    @FindBy (css =".today")
    private WebElement datePickToday;

    @FindBy(xpath = "//span[@class='next']")
    private WebElement nextMonthArrow;

    @FindBy(xpath = "//*[contains(@class, 'today')]/following::*[1]")
    private WebElement firstDate;

    @FindBy(xpath = "//div[@class='col-xs-12']/div[1]//label[@class='--failed']")
    private WebElement bookingStatusCancel;

    @FindBy(xpath = "//*[@class='radio success']/label[contains(., 'Lainnya')]")
    private WebElement reasonLainnya;

    @FindBy(xpath = "//*[@class='form-text']")
    private WebElement textAreaLainnya;

    @FindBy(css  = "#detailTitle > div > p")
    private WebElement kostName;

    //------------------Gallery Photo-----------------

    @FindBy(xpath = "//div[@class = 'detail-photo']/button")
    private WebElement seeAllPhotoButton;

    @FindBy(xpath = "//span[@class = 'detail-category-header__close']")
    private WebElement closeButtonGallery;

    @FindBy(xpath = " //span[contains(text(), 'Foto Kamar Mandi')]")
    private WebElement fotoBangunanText;

    @FindBy(xpath = "//span[text()= 'Foto Kamar']")
    private WebElement fotoKamarText;

    @FindBy(xpath = "//span[contains(text(), 'Foto Kamar Mandi')]")
    private WebElement fotoKamarMandiText;

    @FindBy(xpath = "//span[contains(text(), 'Foto Lainnya')]")
    private WebElement fotoLainnya;

    @FindBy(css="#detailPhotoContainer .detail-category-content__img-wrapper.detail-category-content__img-wrapper")
    private WebElement detailPhotoButton;

    private By arrowPhotoNextButton = By.xpath("//div[@class='detail-carousel-content__nav-next swiper-arrow']");

    //------------------Kos Recommendation-----------------

    @FindBy(id = "relatedCard")
    private WebElement relatedCard;

    @FindBy(xpath = "//*[@id='relatedCard']//label[contains(.,'Kamu mungkin menyukainya')]")
    private WebElement recommendationKosText;

    @FindBy(css = "#relatedCard > div > div.related-box__header > div > a")
    private WebElement seeAllRecomendationButton;

    @FindBy(css = "#relatedCard div.slider-nav__next.swiper-kos-next")
    private WebElement arrowRecommendationNextButton;

    @FindBy(css = ".slider-nav__prev.swiper-kos-prev")
    private WebElement arrowRecommendationPrevButton;

    @FindBy(css="#relatedCard .related-box__header .slider-nav__next.swiper-kos-next > svg")
    private WebElement photoRecommendation;

    @FindBy(xpath = "//div[2]//div[@class='rc-overview']/child::*[contains(text(), 'Campur')]")
    private WebElement mixGenderFilter;

    @FindBy(css="#baseFilterPrice > div > div.bg-c-dropdown__trigger > span")
    private WebElement filterPrice;

    @FindBy(xpath="(//h3[@data-path='lbl_roomTitle'])[4]")
    private WebElement lastRecommendation;

    @FindBy(xpath="(//h3[@data-path='lbl_roomTitle'])[5]")
    private WebElement nextRecommendation;

    @FindBy(xpath = "(//h3[@data-path='lbl_roomTitle'])[1]")
    private WebElement firstKostCard;

    @FindBy(css="input.booking-input-checkin__input")
    private WebElement inpDateBox;

    @FindBy(css="//span[@class='cell day today']/parent::div/following-sibling::div")
    private WebElement tomorrowDate;

    @FindBy(css=".mami-radio[data-path='rdb_monthlyBookingOption']")
    private WebElement radPerMonth;

    @FindBy(css = "input.booking-rent-type__input")
    private WebElement inpRentDurationBox;

    @FindBy(css=".is-open .booking-input-checkin-content__header p")
    private WebElement txtMaxMonthDisclaimer;

    @FindBy(css=".is-open .bg-c-alert__content p")
    private WebElement txtCheckinDateDisclaimer;

    @FindBy(css = "#detailTitle > div > p")
    private WebElement txtKostName;

    @FindBy(xpath = "//a[contains(.,'Uang muka (DP)')]")
    private WebElement uangMukaLabel;

    @FindBy(xpath = "//span[contains(.,'Uang muka (DP)')]")
    private WebElement uangMukaLabelOnPopup;

    @FindBy(xpath = "(//span[contains(.,'Pelunasan')])[1]")
    private WebElement pelunasanLabelOnPopup;

    @FindBy(xpath = "//*[@class='detail-kost-price-item__content']/following-sibling::p")
    private List<WebElement> uangMukaValueOnPopup;

    @FindBy(xpath = "//span[contains(.,'Biaya layanan Mamikos')]")
    private List<WebElement> biayaLayananLabelOnPopup;

    @FindBy(xpath = "//*[@class='detail-kost-price-item__label bg-c-text bg-c-text--body-2 ']/following-sibling::p")
    private List<WebElement> biayaLayananValueOnPopup;

    @FindBy(xpath = "//a[contains(.,'Pelunasan')]")
    private WebElement pelunasanLabel;

    @FindBy(xpath = "//a[contains(.,'Total Pembayaran Pertama')]")
    private WebElement totalBayarPertamaLabel;

    @FindBy(xpath = "//span[text()=('Total Pembayaran Pertama (DP)')]")
    private WebElement totalBayarPertamaLabelOnPopup;

    @FindBy(xpath = "//span[text()=('Total Pembayaran Pertama (Pelunasan)')]")
    private WebElement totalBayarPertamaPelunasanLabelOnPopup;

    @FindBy(xpath = "//*[@class='detail-kost-price-item__label bg-c-text bg-c-text--body-2 ']/following-sibling::p")
    private List<WebElement> totalBayarPertamaValueOnPopup;

    @FindBy(xpath = "//*[@class='detail-kost-price-item__label bg-c-text bg-c-text--heading-6 ']/following-sibling::p")
    private List<WebElement> totalBayarValueOnPopup;

    @FindBy(xpath = "//p[contains(.,'Wajib bayar DP')]")
    private WebElement wajibDPWarning;

    @FindBy(xpath = "//*[@class='bg-c-modal__action-closable']//*[@class='bg-c-icon bg-c-icon--md']")
    private WebElement icnCloseButton;

    @FindBy(xpath = "//span[contains(.,'Harga Sewa Per Minggu')]")
    private WebElement hargaSewaLabelOnPopup;

    @FindBy(xpath = "//span[text()=('Total Pembayaran Selanjutnya')]")
    private WebElement totalBayarSelanjutnyaLabelOnPopup;

    @FindBy(xpath = "//*[@class='booking-form-radio__label']/p")
    private List<WebElement> uangMukaLabelOnPengajuanSection;

    @FindBy(xpath = "//*[@class='booking-form-radio__right']/p")
    private List<WebElement> totalDPOnPengajuanSection;

    @FindBy(xpath = "//span[text()=('Total Pelunasan')]")
    private WebElement totalBayarPertamaPelunasanLabelOnPageSection;

    @FindBy(xpath = "//*[@class='booking-input-checkin-content__info']//p[contains(.,'Waktu mulai ngekos ')]")
    private List<WebElement> txtMaxMonthAndCheckinDateDisclaimer;

    //--------------------- Right Panel ----------------------

    @FindBy(css = "#priceCard .rc-price__real")
    private WebElement totalPriceText;

    @FindBy(xpath = "//input[@class = 'booking-input-checkin__input']")
    private WebElement bookingDateForm;

    @FindBy(xpath = "//div[@class = 'vdp-datepicker__calendar inline']")
    private WebElement bookingDate;

    @FindBy(xpath = "//input[@class = 'booking-rent-type__input']")
    private WebElement bookingDurationForm;

    @FindBy(xpath = "//span[@class = 'booking-card__info-price-amount-unit']")
    private WebElement totalPriceDurationText;

    @FindBy(xpath = "//h5[@class = 'booking-card__info-price-amount']")
    private WebElement mainPricePropertyText;

    //--------------------- Breadcrumbs ----------------------

    @FindBy(xpath = "(//a[@class = 'breadcrumb-trail'])[1]")
    private WebElement homeBreadcrumbsText;

    @FindBy(xpath = "(//a[@class = 'breadcrumb-trail'])[2]")
    private WebElement townBreadcrumbsText;

    @FindBy(xpath = "(//span[@class = 'breadcrumb-trail active'])[1]")
    private WebElement propNameBreadcrumbsText;

    //--------------------- Down Payment ----------------------
    @FindBy(xpath = "//p[contains(.,'Jika kamu bayar pakai DP:')]")
    private WebElement jikaPakiDPLabel;

    @FindBy(xpath = "//p[contains(.,'Jika kamu pakai pembayaran penuh:')]")
    private WebElement jikaBayarPenuhLabel;

    @FindBy(xpath = "//p[contains(.,'Bisa bayar pakai DP')]")
    private WebElement bisaPakaiDPWarning;

    @FindBy(xpath = "//a[contains(.,'Pembayaran Penuh')]")
    private WebElement pembayaranPenuhLabel;

    @FindBy(xpath = "//span[contains(.,'Pembayaran Penuh')]")
    private WebElement pembayaranPenuhLabelPopUp;

    @FindBy(xpath = "(//span[contains(.,'Pembayaran Penuh')])[1]")
    private WebElement pembayaranpenuhLabelOnPopup;

    @FindBy(xpath = "//span[text()=('Total Pembayaran Pertama (Penuh)')]")
    private WebElement totalBayarPenuhLabelOnPopup;

    //------------ownerBadges-----------

    @FindBy(xpath = "//span[contains(.,'booking berhasil')]")
    private WebElement badgesBookingBerhasil;

    @FindBy(xpath ="//p[contains(.,'Jumlah transaksi')]")
    private WebElement badgesBookingBerhasilSubTitle;

    //------------------Badge Kos-----------------
    private By eliteBadge = By.xpath("//img[@alt='Kos Pilihan logo']");
    private By apikBadge   = By.xpath("//img[@alt='Apik logo']") ;
    private By singgahsiniBadge = By.xpath("//img[@alt='Singgahsini logo']");

    //---------------- Pop up message cancel booking success ------------------------------
    @FindBy(xpath = "//*[contains(@class, 'fade in')]//div[@class='modal-cancel-caption'] ")
    private WebElement messageSuccessCancelBooking;

    @FindBy(xpath = "//*[contains(@class, 'fade in')]//div[@class='modal-cancel-button']")
    private WebElement successCancelBookingButton;

    //------------------disclaimer ketentuan waktu kos-------------------
    @FindBy(xpath ="//*[@class='booking-input-checkin-content__info']//following-sibling::*//p[contains(.,'setelah pengajuan sewa')]")
    private WebElement disclaimerCalenderText;

    private By homeTextButton = By.xpath("//div[@class='breadcrumb-container']//a[.='Home']");

    private By titlePageBookingRequest = By.cssSelector(".bg-c-text--heading-2");

    //------------------PLM carousel Section -------------------
    private By carouselSection = By.cssSelector(".detail-kost-types-carousel");
    private By ajukanSewaButton = By.xpath("//div[@class='swiper-slide detail-kost-types-carousel__swiper-slide swiper-slide-active']//button[@class='bg-c-button room-type-act__button bg-c-button--primary bg-c-button--sm']");
    private By seeDetailButton = By.xpath("//div[@class='swiper-slide detail-kost-types-carousel__swiper-slide swiper-slide-active']//button[@class='bg-c-button room-type-act__button bg-c-button--secondary bg-c-button--sm']");
    private By carouselKosPhoto = By.xpath("//div[@class='bg-c-image room-type-cover__image bg-c-image--2-1']");
    private By carouselGender = By.xpath("//div[@class='room-type-cover__gender bg-c-label bg-c-label--rainbow bg-c-label--rainbow-white']");
    private By carouselRoomType = By.xpath("//p[@class='room-type-overview__title bg-c-text bg-c-text--title-4 ']");
    private By carouselRoomFacility = By.xpath("//span[@class='room-type-fac__text bg-c-text bg-c-text--body-2 ']");
    private By carouselRoomPrice = By.xpath("//div[@class='room-type-price__cost-wrapper']");
    private By seeOtherTypeButton = By.xpath("//button[@class='bg-c-button bg-c-button--tertiary bg-c-button--md']");
    private By swipeButtonPLM = By.cssSelector(".detail-kost-types-carousel__swiper-nav--next");
    private By otherTypeKosPhoto = By.xpath("(//div[@class='room-type-cover'])[1]");
    private By otherTypeGender = By.xpath("(//div[@class='room-type-cover'])[1]");
    private By otherTypeRoomType = By.xpath("//div[@class='type-list-modal__content']//p[@class='room-type-overview__title bg-c-text bg-c-text--title-4 ']");
    private By otherTypeRoomFacility = By.xpath("(//*[@data-testid='room-type-list'])[1]/descendant::div[9]");
    private By otherTypeRoomPrice = By.xpath("(//*[@data-testid='room-type-list'])[1]/descendant::div[@data-testid='roomPrice']");
    private By otherTypeajukanSewaButton = By.xpath("(//*[@data-testid='room-type-list'])[1]/descendant::button");

    @FindBy(xpath = "//div[@class='type-list-modal__content']/div[1]//button[@class='bg-c-button room-type-act__button bg-c-button--primary bg-c-button--md']")
    private WebElement ajukanSewaFirstButton;

    @FindBy(xpath = "(//*[@data-testid='room-type-list'])[1]")
    private WebElement kosCardOtherType;

    //------------------Facilty Share Section-----------------
    private By facShareSection = By.cssSelector(".detail-kost-public-facilities");
    private By facShareSeeAllButton = By.cssSelector(".detail-kost-facility-category__see-more-button-label");
    private By facilitySharedTitle = By.cssSelector(".detail-kost-facilities-modal__title");
    private By facDescription = By.cssSelector(".detail-kost-facilities-modal__description");
    private By facPopup = By.cssSelector(".detail-kost-facilities-modal__body");

    //------------------Facilty Parking Section-----------------
    private By facParkingSection = By.cssSelector(".detail-kost-parking-facilities__content");
    private By facParkirTitle = By.xpath("//p[contains(.,'Fasilitas parkir')]");

    //------------------Promo Owner Section-----------------
    private By promoOwnerSection = By.cssSelector(".detail-container__owner-promo--desktop-card");
    private By promoOwnerButton = By.cssSelector(".detail-kost-owner-promo__see-detail");
    private By promoOwnerTitle = By.xpath("//p[@class='bg-c-text bg-c-text--heading-4 ']");
    private By promoOwnerClickText = By.xpath("//a[contains(.,'tanya pemilik kos terlebih dahulu.')]");
    private By chatKostPopUp = By.cssSelector(".modal-chat__body");

    //------------------Facilty Room Section-----------------
    private By facRoomSeeAllButton = By.xpath("(//button[@class='bg-c-button detail-kost-facility-category__see-more-button bg-c-button--tertiary bg-c-button--md'])[1]");
    private By facilityRoomIcon = By.xpath("//div[@class='detail-kost-facilities-modal__body']//div[@class='detail-kost-facility-item__icon']");
    private By facRoomName = By.xpath("//div[@class='detail-kost-facilities-modal__body']//div[@class='bg-c-list-item__description']");

    //------------------Facilty Bath Section-----------------
    private By facBathSection = By.cssSelector(".detail-kost-bathroom-facilities");
    private By facBathIcon = By.xpath("//div[@class='detail-kost-bathroom-facilities']//div[@class='detail-kost-facility-item__icon']");
    private By facBathName = By.xpath("//div[@class='detail-kost-bathroom-facilities']//div[@class='bg-c-list-item__description']");

    //------------------Facility Notes Section-----------------
    private By facNotesSection = By.xpath("//div[@class='detail-kost-facility-notes']");
    private By facNotesDesc = By.xpath("//div[@id='kost-facility-note-description']");
    private By expandFacNotesBtn = By.xpath("//div[@class='detail-kost-facility-notes']//button");

    //------------------Owner Story Section-----------------
    private By ownerStorySection = By.xpath("//div[@class='detail-kost-owner-story']");
    private By ownerStoryDesc = By.xpath("//div[@id='kost-owner-story-content']");
    private By expandOwnerStoryBtn = By.xpath("//div[@class='detail-kost-owner-story']//button");

    //-----------------Refund Policy----------------
    private By refundPolicyTitle = By.xpath("//*[@class='detail-kost-refund__content']//p[@class='detail-kost-refund__title bg-c-text bg-c-text--title-5 ']");
    private By refundDescription = By.xpath("//*[@class='detail-kost-refund__content']//p[@class='detail-kost-refund__description bg-c-text bg-c-text--body-4 ']");
    private By tnCRefundTitle = By.xpath("//p[contains(.,'Syarat dan Ketentuan Refund')]");

    private By refundSSContentOne = By.xpath("//*[@class='detail-kost-refund-policy-list-item__content']//p[contains(.,'Refund sebelum check-in')]");
    private By refundSSContentTwo = By.xpath("//*[@class='detail-kost-refund-policy-list-item__content']//p[contains(.,'DP tidak dapat')]");
    private By refundSSContentThree = By.xpath("//*[@class='detail-kost-refund-policy-list-item__content']//p[contains(.,'Bayar langsung lunas')]");
    private By timeConditionText = By.xpath("//a[contains(.,'ketentuan waktu berikut')]");

    //------------------Overview Section-----------------
    private By propertyGender = By.xpath("//span[@class='detail-kost-overview__gender-box']");
    private By propertyLocation = By.xpath("//div[@class='detail-kost-overview__area']");
    private By roomAvailability = By.xpath("//div[@class='detail-kost-overview__availability']");

    @FindBy(xpath = "//p[contains(.,'10% dari harga sewa')]")
    private WebElement totalDiscountLabel;

    // ------------ Owner Upper Section -----------
    @FindBy(xpath = "//div[contains(text(), 'Kos disewakan oleh')]")
    private WebElement ownerBadgesTitle;

    @FindBy(xpath = "//div[@class='detail-kost-owner-section__last-online']")
    private WebElement ownerLastSeenDetail;

    @FindBy(xpath = "//div[@id='bookingDetailCard']//div[@class='detail-warning']")
    private WebElement warningBookingDetail;

    private By discountLabelKostDetail = By.xpath("//div[@class='rc-price__additional-data']");

    /**
     * Click on continue button in pop up
     *
     * @throws InterruptedException
     */
    public void clickContinueBtn() throws InterruptedException {
        selenium.clickOn(continueBtn);
    }

    /**
     * Click on I understand button in pop up
     *
     * @throws InterruptedException
     */
    public void clickUnderstand() throws InterruptedException {
        selenium.clickOn(iUnderstand);
    }

    /**
     * Check whether pop up appear/not
     */
    public boolean checkContinuePopUp() {
        return selenium.waitTillElementIsVisible(continueBtn, 2) != null;
    }

    /**
     * Click on "Chat" in kos Detail
     * @throws InterruptedException
     */
    public void clickChatKos() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0,3500);
        selenium.pageScrollUsingCoordinate(0,4800);
        selenium.waitInCaseElementVisible(chatKosButton,2);
        selenium.javascriptClickOn(chatKosButton);
    }

    /**
     * @return true if TFUE present, otherwise false.
     * @throws InterruptedException
     */
    public Boolean isFTUE_screenPresent() {
        return selenium.waitInCaseElementPresent(ftueScreenPopUpBody, 5) != null
                || selenium.waitInCaseElementPresent(iUnderstandButtonBy, 5) != null;
    }

    /**
     * Will check First Time User Experience screen first. And then will click on continue button or I understand button on FTUE if present in the screen.
     * Dismiss FTUE firt time user experience by click on next button if present.
     *
     * @throws InterruptedException
     */
    public void dismissFTUEScreen() throws InterruptedException {
        selenium.pageScrollInView(shareButton);
        selenium.waitForJavascriptToLoad();
        int i = 0;
        if (selenium.waitInCaseElementVisible(continueBtn, 3) != null) {
            do {
                selenium.javascriptClickOn(continueBtn);
                    i++;
            } while (i < 5);
        selenium.clickOn(iUnderstandButtonBy);
        selenium.hardWait(2);
        }else if(selenium.waitInCaseElementVisible(mauDongBtn,3) != null){
            selenium.javascriptClickOn(promoNgebutBtn);
        }
    }

    /**
     * @return True if red love icon is present otherwise false
     * @throws InterruptedException
     */
    public Boolean isKosFavourite() throws InterruptedException {
        return selenium.isElementPresent(heartIconRed);
    }

    /**
     * @return True if grey love icon is present otherwise false
     * @throws InterruptedException
     */
    public Boolean isKosNotFavourite() throws InterruptedException {
        return selenium.isElementPresent(heartIconGrey);
    }


    /**
     * Click on favourite button mark with love icon
     *
     * @throws InterruptedException
     */
    public void clickOnFavouriteButton() throws InterruptedException {
        selenium.javascriptClickOn(favouriteSaveButton);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(2);
    }
    /**
     * Click on favourite button mark with love icon
     *
     * @throws InterruptedException
     */
    public boolean unFavouriteButton() throws InterruptedException {
        return selenium.isElementDisplayed(unFavourite);
    }

    /**
     * Check detail kos page reached
     *
     * @return Boolean
     * @throws InterruptedException
     */
    public Boolean isInKosDetail() throws InterruptedException {
        return selenium.isElementPresent(kosDetailPage);
    }

    /**
     * Get current amount of favourite of kos detail
     *
     * @return Integer
     */
    public Integer getCurrentFavouriteNumber() {
        String favouriteNumber = selenium.getText(favouriteSaveButton).replace(" Disimpan", "");
        return Integer.parseInt(favouriteNumber);
    }

    /**
     * Check is success favourite a kos screen appear after click on favourite button
     *
     * @return true if element present, otherwise false
     * @throws InterruptedException
     */
    public Boolean isFavouriteSuccessBoxPresent() throws InterruptedException {
        return selenium.isElementPresent(favouriteCompleteBoxBy);
    }

    /**
     * Click OK button on success favourite pop up
     *
     * @throws InterruptedException
     */
    public void clickOnOkButtonSuccessFavourite() throws InterruptedException {
        selenium.clickOn(OK_button);
    }

    /**
     * Click on Booking Button
     *
     * @throws InterruptedException
     */
    public void clickOnBookingButton() throws InterruptedException {
        selenium.pageScrollInView(bookingButton);
        selenium.waitTillElementIsClickable(bookingButton);
        selenium.javascriptClickOn(bookingButton);
    }

    /**
     * Select Starting Date of Boarding if exist
     * @param date date e.g. 15,20 etc
     */
    public void selectDateForStartBoarding(String date) throws InterruptedException {
        selenium.hardWait(2);
        if(selenium.waitInCaseElementPresent(dateTextBox,5)!=null){
            selenium.javascriptClickOn(dateTextBox);
            if(date.equalsIgnoreCase("today")){
                selenium.waitInCaseElementVisible(datePickToday,10);
                selenium.javascriptClickOn(datePickToday);
            }else  {
//                int tomorrowDate = Integer.parseInt(getTomorrowDate());
//                if (tomorrowDate == 1){
//                    selenium.pageScrollInView(nextMonthArrow);
//                    selenium.javascriptClickOn(nextMonthArrow);
//                    selenium.waitTillElementIsClickable(firstDate);
//                    selenium.clickOn(firstDate);
//                }
                if (selenium.waitInCaseElementVisible(tomorrowDateLabel, 3) != null) {
                    selenium.javascriptClickOn(tomorrowDateLabel);
                }
                else if (selenium.waitInCaseElementVisible(saturdayDateLabel, 3) != null) {
                    selenium.javascriptClickOn(saturdayDateLabel);
                }
                else {
                    selenium.javascriptClickOn(sundayDateLabel);
                }
            }
        }
    }

    /**
     * Get tomorrow date
     */
    public String getTomorrowDate() {
       return selenium.getText(tomorrowDateLabel);
    }

    /**
     * Select date booking today
     * @throws InterruptedException
     */
    public void selectBookingDateToday() throws InterruptedException {
        selenium.waitTillElementIsVisible(dateTextBox,5);
        selenium.javascriptClickOn(dateTextBox);
        selenium.clickOn(datePickToday);
    }

    /**
     * Select date booking tomorrow
     * @throws InterruptedException
     */
    public void selectBookingDateTomorrow(String tomorrow) throws InterruptedException {
        selenium.waitTillElementIsVisible(dateTextBox,4);
        selenium.javascriptClickOn(dateTextBox);
        selenium.javascriptClickOn(By.xpath("(//div[@class='date-wrapper']//span[@class='cell day'][contains(text(),'" + tomorrow +"')])[1]"));
    }

    /**
     * Select Rent Type of Booking
     * @param type type of rent
     */
    public void selectRentType(String type) throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(By.xpath("//div[@class='booking-rent-type__options']/div[@class='booking-rent-type__options-item']/div[contains(.,'"+type+"')]"));
    }

    /**
     * Wait for loading animation to be disappear
     *
     */
    public void waitForLoadingAnimationDisapper() {
        selenium.waitTillElementIsNotVisible(loadingAnimation);
    }

    /**
     * get text popup different gender
     *
     * @return String
     */
    public String getPopupDifferentGenderText() {
        return selenium.getText(textDifferentGender);
    }

    /**
     * get text kost title
     *
     * @return String
     */
    public String getTextKostTitle() {
        selenium.switchToWindow(0);
        return selenium.getText(textKostTitle);
    }

    /**
     * Click on Close button on popup different gender
     *
     * @throws InterruptedException
     */
    public void clickOnCloseBtn() throws InterruptedException {
        selenium.click(closePopupDifferentGenderBtn);
    }

    /**
     * Scroll to View Maps
     *
     * @throws InterruptedException
     */
    public void scrollViewMaps() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 2500);
    }

    /**
     * Click View Maps
     *
     * @throws InterruptedException
     */
    public void clickViewMaps() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0,2500);
        selenium.hardWait(5);
        selenium.clickOn(viewMapsButton);
    }

    /**
     * Verify Popup Login
     * @throws InterruptedException
     */
    public Boolean loginPopupPresent(){
        return selenium.waitInCaseElementPresent(loginPopup, 5) != null;
    }

    /**
     * Verify Maps Pointer
     * @throws InterruptedException
     */
    public Boolean pointerMapsPresent() {
        return selenium.waitInCaseElementPresent(mapsPointer, 5) != null;
    }

    /**
     * Verify daily rent price is present
     * @return true if daily rent price is present otherwise false
     * @throws InterruptedException
     */
    public boolean isDailyRentPricePresent() throws InterruptedException {
        return selenium.isElementPresent(perBulanDuration);
    }

    /**
     * Cancel booking if tenant have
     * @throws InterruptedException
     */
    public void cancelBooking() throws InterruptedException {
        selenium.waitTillElementIsVisible(firstViewMore);
        selenium.javascriptClickOn(firstViewMore);
        selenium.pageScrollInView(firstDetailTenant);
        if (selenium.isElementDisplayed(cancelBookingButton)){
            selenium.hardWait(2);
            selenium.javascriptClickOn(cancelBookingButton);
            selenium.hardWait(2);
            WebElement reason = driver.findElement(By.xpath("//*[@class='radio success']/label[contains(.,'Merasa tidak cocok/tidak sesuai kriteria')]"));
            selenium.waitTillElementIsVisible(reason);
            selenium.hardWait(2);
            selenium.clickOn(reason);
            selenium.hardWait(2);
            selenium.clickOn(yesCancelButton);
            selenium.hardWait(3);
        }
    }

    /**
     * Click close on mamikos promo pop up if appear
     * @throws InterruptedException
     */
    public void clickClosePromoPopUp() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(flashSaleFTUE, 3) != null) {
            selenium.clickOn(closeFlashSaleFTUEButton);
        }
    }

    /**
     * Click close on mamikos promo ngebut pop up if appear
     * @throws InterruptedException
     */
    public void clickClosePromoNgebutPopUp() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(promoNgebutFTUE, 3) != null) {
            selenium.clickOn(sayaMengertiButton);
        }
    }

    /**
     * Click Ask Address
     *
     * @throws InterruptedException
     */
    public void clickAskAddress() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(714, -407);
        selenium.waitInCaseElementVisible(kostMapContainer, 5);
        selenium.javascriptClickOn(askAddressButton);
    }

    /**
     * Check if date picker is present in page
     * @return boolean data type
     */
    public boolean isDatePickerPresent() {
        return selenium.waitTillElementIsVisible(datePicker, 5) != null;
    }

    /**
     * Check if date picker is clickable
     * @return true if date booking picker is clickable
     */
    public boolean isDatePickerClickable() {
        return selenium.waitInCaseElementClickable(datePicker, 5) != null;
    }

    /**
     *
     * @param date input with date e.g "1" or "30"
     * @throws InterruptedException
     */
    public void selectBookingDate(String date) throws InterruptedException {
        if (isDatePickerPresent() && isDatePickerClickable()){
            if (date.equalsIgnoreCase("1")) {
                selenium.clickOn(nextMonthButton);
            }
            selenium.clickOn(By.xpath("//*[contains(@class, 'is-absolute')]//*[contains(@class, 'cell day')][.='"+ date +"']"));
            selenium.hardWait(3);
            clickOnBookingButton();
        }
    }

    /**
     * Get kost type gender Putra, Putri, Campur
     * @return String data type
     */
    public String getKostTypeText() {
        return selenium.getText(kostType);
    }

    /**
     * Click on LIHAT button when its appear
     * @throws InterruptedException
     */
    public void clickOnSeeButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(seeButton);
        selenium.hardWait(1);
        selenium.clickOn(seeButton);
    }

    /**
     * Check FTUE Image 1 is present
     * @return true / false
     */
    public boolean isFTUEImage1Present() {
        return selenium.waitInCaseElementVisible(FTUEImg1,3) != null;
    }

    /**
     * Check FTUE Image 2 is present
     * @return true / false
     */
    public boolean isFTUEImage2Present() {
        return selenium.waitInCaseElementVisible(FTUEImg2,3) != null;
    }

    /**
     * Check FTUE Image 3 is present
     * @return true / false
     */
    public boolean isFTUEImage3Present() {
        return selenium.waitInCaseElementVisible(FTUEImg3,3) != null;
    }

    /**
     * Check FTUE Image 4 is present
     * @return true / false
     */
    public boolean isFTUEImage4Present() {
        return selenium.waitInCaseElementVisible(FTUEImg4,3) != null;
    }

    /**
     * Check FTUE Image 5 is present
     * @return true / false
     */
    public boolean isFTUEImage5Present() {
        return selenium.waitInCaseElementVisible(FTUEImg5,3) != null;
    }

    /**
     * Click pelajari lebih lanjut FTUE button
     * @throws InterruptedException
     */
    public void clickPelajariLebihLanjutButton() throws InterruptedException {
        selenium.clickOn(learnMoreButton);
        selenium.hardWait(3);
        selenium.switchToWindow(2);
    }

    /**
     * Click share button
     * @throws InterruptedException
     */
    public void clickShareButton() throws InterruptedException {
        selenium.pageScrollInView(textKostTitle);
        selenium.javascriptClickOn(shareButton);
    }

    /**
     * Click share facebook button
     * @throws InterruptedException
     */
    public void clickShareFacebookButton() throws InterruptedException {
        selenium.javascriptClickOn(shareFacebookButton);
    }

    /**
     * Check facebook window is present
     * @return facebook url
     */
    public String isFacebookWindowPresent() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.hardWait(3);
        return selenium.getURL();
    }

    /**
     * Check kos rule is present
     * @return true / false
     */
    public boolean isKosRulePresent() throws InterruptedException {
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        selenium.pageScrollUsingCoordinate(0,4800);
        selenium.hardWait(3);
        selenium.pageScrollInView(kosRuleSectiom);
        return selenium.waitInCaseElementVisible(kosRuleTitle, 3) != null;
    }

    /**
     * Get map button text
     * @return map button text
     * @param text
     */
    public String getMapButtonText(String text) throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 3500);
//        selenium.pageScrollInView(kostMapContainer);
        selenium.hardWait(3);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        return selenium.getText(By.xpath("//*[@class='kost-location__map']//button[contains(text(),'"+ text +"')]"));
    }


    /**
     * Click see map button
     * @throws InterruptedException
     */
    public void clickOnSeeMapButton() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(714, -407);
        selenium.waitInCaseElementVisible(kostMapContainer,3);
        selenium.javascriptClickOn(viewMapsButton);
    }

    /**
     * Check kos rule image is present
     * @return true / false
     */
    public boolean isKosRuleImagePresent() {
        selenium.pageScrollInView(kosRuleSectiom);
        selenium.pageScrollUsingCoordinate(230, -1000);
        selenium.waitTillElementIsClickable(kosRuleImageElement);
        return selenium.waitInCaseElementVisible(kosRuleImageElement, 3) != null;
    }

    /**
     * Click on image kos rule
     * @throws InterruptedException
     */
    public void clickOnKosRuleImage() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(242,2500);
        selenium.clickOn(kosRuleImageElement);
        selenium.waitTillElementIsClickable(kosRuleImageElement);
    }

    /**
     * swipe arrow next image kos rule
     * @throws InterruptedException
     */
    public void clickOnArrowNextButton() throws InterruptedException {
        selenium.clickOn(iconArrowNextButton);
    }

    /**
     * swipe arrow previous image kos rule
     * @throws InterruptedException
     */
    public void clickOnArrowPrevButton() throws InterruptedException {
        selenium.hardWait(1);
        selenium.clickOn(iconArrowPrevButton);
    }

    /**
     * Check pop up image kos rule is present
     * @return true / false
     */
    public boolean isPopUpImgKosRulePresent() {
        return selenium.waitInCaseElementVisible(kosRulePopUpImageElement, 3) != null;
    }

    /**
     * Check owner section is present
     * @return true / false
     */
    public boolean isOwnerSectionPresent(){
        selenium.pageScrollInView(relatedCard);
        selenium.pageScrollInView(OwnerDeviderLine);
        return selenium.waitInCaseElementVisible(aboutStatisticsButton, 5) != null;
    }

    /**
     * Check owner name is displayed
     *
     * @return status true / false
     */
    public boolean isOwnerNameDisplayed() {
        return selenium.waitInCaseElementVisible(ownerNameText, 5)!= null;
    }

    /**
     * Check owner picture  is displayed
     *
     * @return status true / false
     */
    public boolean isOwnerPictureDisplayed(){
        return selenium.waitInCaseElementVisible(ownerImageProfile, 5)!=null;
    }

    /**
     * Check owner status is displayed
     *
     * @return status true / false
     */
    public boolean isOwnerStatusDisplayed(){
        return selenium.waitInCaseElementVisible(ownerStatus, 5)!=null;
    }

    /**
     * Check number of transaction is displayed
     *
     * @return status true / false
     */
    public boolean isNumberTransactionDisplayed(){
        return selenium.waitInCaseElementVisible(successfulTansaction, 5)!=null;
    }

    /**
     * Check booking processed is displayed
     *
     * @return status true / false
     */
    public boolean isBookingProcessedDisplayed(){
        return selenium.waitInCaseElementVisible(bookingProcessed, 5)!=null;
    }

    /**
     * Check booking chance is displayed
     *
     * @return status true / false
     */
    public boolean isBookingChanceDisplayed(){
        return selenium.waitInCaseElementVisible(bookingChance, 5)!=null;
    }

    /**
     * Click on about statistics button
     *
     * @throws InterruptedException
     */
    public void clickStatisticsDetailButton() throws InterruptedException {
        selenium.clickOn(aboutStatisticsButton);
    }

    /**
     * Check statistics modal is displayed
     *
     * @return status true / false
     */
    public boolean isStatisticsModalDisplayed(){
        return selenium.waitInCaseElementVisible(statisticModal, 5)!=null;
    }

    /**
     * Click on close statistics modal button
     *
     * @throws InterruptedException
     */
    public void closeStatisticsModal() throws InterruptedException {
        selenium.clickOn(closeStatisticsModalBtn);
    }

    /**
     * Check image kos report is present
     * @return true / false
     */
    public boolean isKosReportPresent() {
        selenium.pageScrollInView(kosReportContainer);
        return selenium.waitInCaseElementVisible(kosReportContainer, 3) != null;
    }

    /**
     * Click on kos report button
     * @throws InterruptedException
     */
    public void clickOnKosReportButton() throws InterruptedException {
        selenium.javascriptClickOn(kosReportButton);
    }

    /**
     * Check pop up image kos report is present
     * @return true / false
     */
    public boolean isPopUpKosReportPresent() {
        return selenium.waitInCaseElementVisible(popUpReportKos, 3) != null;
    }

    /**
     * Enter text to textbox
     *@param textReport is text we want to enter
     */
    public void insertReportText(String textReport) {
        selenium.enterText(reportTextBox, textReport, false);
        reportTextBox.sendKeys(Keys.ENTER);
    }

    /**
     * Click on check box kos report
     * @throws InterruptedException
     */
    public void clickOnCheckBox() throws InterruptedException{
        selenium.click(checkBoxKosReport);
    }

    /**
     * Click on send report button
     * @throws InterruptedException
     */
    public void clickOnSendReportButton() throws InterruptedException {
        selenium.click(sendReportButton);
    }

    /**
     * Check kos report pop up confirmation is present
     * @return true / false
     */
    public boolean isReportConfirmationPresent() {
        return selenium.waitInCaseElementVisible(reportConfirmationPopUp, 3) != null;
    }

    /**
     * Check description kost is present
     * @return true / false
     */
    public boolean isInfoTambahanPresent() {
        return selenium.waitInCaseElementVisible(descKosText, 2) != null;
    }

    /**
     * Check button selengkapnya is present
     * @return true / false
     */
    public boolean isSelengkapnyaButtonPresent() {
        return selenium.waitInCaseElementVisible(infoTambahanButton, 2) != null;
    }

    /**
     * Check button info tambahan is present
     * @return true / false
     */
    public void clickOnInfoTambahanButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(infoTambahanButton,2);
        selenium.javascriptClickOn(infoTambahanButton);
    }

    /**
     * Get Info tambahan Desc Text
     *
     * @return variable
     */
    public String getInfoTambahanLabel() {
        String desc = selenium.getText(infoTambahanButton);
        return desc;
    }

    /**
     * click lihat selengkapnya data riwayat booking
     *
     * @throws InterruptedException
     */
    public void clickOnLihatSelengkapnyaRiwayatBooking() throws InterruptedException {
        selenium.waitInCaseElementVisible(firstDetailTenant, 3);
        selenium.clickOn(firstViewMore);
    }

    /**
     * click Lihat Selengkapnya
     * scroll down to click Batalkan Booking button
     * select reason cancellation and click Yes, Batalkan button
     * @param reason
     * @throws InterruptedException
     */
    public void cancelBookingWithReason(String reason) throws InterruptedException {
        if (selenium.isElementDisplayed(warningBookingDetail)){
            selenium.pageScrollInView(firstDetailTenant);
            selenium.waitTillElementIsClickable(cancelBookingButton);
            selenium.hardWait(2);
            selenium.javascriptClickOn(cancelBookingButton);
            selenium.hardWait(3);
        } else {
        selenium.clickOn(firstViewMore);
        selenium.pageScrollInView(firstDetailTenant);
            if (selenium.isElementDisplayed(cancelBookingButton)) {
                selenium.waitTillElementIsClickable(cancelBookingButton);
                selenium.hardWait(2);
                selenium.javascriptClickOn(cancelBookingButton);
                selenium.hardWait(3);
            }
        }

        WebElement element = driver.findElement(By.xpath("//*[@class='radio success']/label[contains(.,'"+ reason +"')]"));
        selenium.waitTillElementIsVisible(element);
        selenium.hardWait(5);
        selenium.clickOn(element);
        selenium.hardWait(5);
        selenium.clickOn(yesCancelButton);
    }

    /**
     * check booking status after booking cancelled
     * @return
     */
    public String getBookingStatusCancel(){
        return selenium.getText(bookingStatusCancel);
    }

    /**
     * click Lihat Selengkapnya
     * check reason cancel according reason selection
     * @param reason
     * @return
     * @throws InterruptedException
     */
    public String getReasonCancellation(String reason) throws InterruptedException{
        selenium.clickOn(firstViewMore);
        selenium.pageScrollInView(firstDetailTenant);
        WebElement element = driver.findElement(By.xpath("//*[@id='bookingDetailCard']//span[contains(.,'"+ reason +"')]"));
        return selenium.getText(element);
    }

    /**
     * click Lihat Selengkapnya
     * scroll down to click Batalkan Booking button
     * select reason Lainnya
     * input reason text in textarea lainnya and click Yes, Batalkan
     * @param lainnya
     * @throws InterruptedException
     */
    public void cancelBookingWithLainnya(String lainnya) throws InterruptedException{
        selenium.clickOn(firstViewMore);
        selenium.pageScrollInView(firstDetailTenant);
        if(selenium.isElementDisplayed(cancelBookingButton)){
            selenium.waitTillElementIsClickable(cancelBookingButton);
            selenium.hardWait(2);
            selenium.javascriptClickOn(cancelBookingButton);
            selenium.hardWait(2);
        }
        selenium.waitTillElementIsVisible(reasonLainnya);
        selenium.hardWait(3);
        selenium.clickOn(reasonLainnya);
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(textAreaLainnya);
        selenium.clickOn(textAreaLainnya);
        selenium.hardWait(2);
        WebElement element = driver.findElement(By.xpath("//*[@class='form-text']/textarea[contains(., '')]"));
        selenium.enterText(element, lainnya, false);
        selenium.hardWait(5);
        element.sendKeys(Keys.ENTER);
        selenium.hardWait(2);
        selenium.clickOn(yesCancelButton);
    }

    /**
     * click Lihat Selengkapnya
     * check reason cancel according reason inputted at Lainnya reason
     * @param reasonLainnya
     * @return
     * @throws InterruptedException
     */
    public String getReasonCancellationLainnya(String reasonLainnya) throws InterruptedException{
        selenium.clickOn(firstViewMore);
        selenium.pageScrollInView(firstDetailTenant);
        WebElement element = driver.findElement(By.xpath("//*[@id='bookingDetailCard']//span[contains(.,'"+ reasonLainnya +"')]"));
        return selenium.getText(element);
    }

    /**
     * Check facility title displayed or not
     * @return true / false
     */
    public boolean isFacilitiyTitleDisplayed() throws InterruptedException {
        selenium.pageScrollInView(kosBenefit);
        return selenium.waitInCaseElementVisible(facilityTitleText, 3) != null;
    }

    /**
     * Check facility price info displayed or not
     * @return true / false
     */
    public boolean isFacilitiyPriceInfoDisplayed() throws InterruptedException {
        selenium.pageScrollInView(facilityPriceInfo);
        return selenium.waitInCaseElementVisible(facilityPriceInfo, 3) != null;
    }

    /**
     * Check facility room size displayed or not
     * @return true / false
     */
    public boolean isFacilitiyRoomSizeDisplayed() throws InterruptedException {
        selenium.pageScrollInView(facilityRoomSize);
        return selenium.waitInCaseElementVisible(facilityRoomSize, 3) != null;
    }

    /**
     * Check top facility displayed or not
     * @return true / false
     */
    public boolean isFacilitiyRoomDisplayed() {
        return selenium.waitInCaseElementVisible(topFacility, 3) != null;
    }

    /**
     * Check top facility displayed or not
     * @return true / false
     */
    public boolean isFacilitiyRoomOpenedDisplayed() {
        return selenium.waitInCaseElementVisible(topFacility2, 3) != null;
    }

    /**
     * Check facility see all button displayed or not
     * @return true / false
     */
    public boolean isFacilitiySeeAllButtonDisplayed() {
        return selenium.waitInCaseElementVisible(facilitySeeAllButton, 3) != null;
    }

    /**
     * Click facility see all button with scroll down
     * @throws InterruptedException
     */
    public void clickFacilitySeeAllButton() throws InterruptedException {
        selenium.pageScrollInView(topFacilityTitle);
        selenium.clickOn(facilitySeeAllButton);
    }

    /**
     * Check facility room title displayed or not
     * @return true / false
     */
    public boolean isFacilitiyRoomTitleDisplayed() {
        return selenium.waitInCaseElementVisible(facilityRoomTitle, 3) != null;
    }

    /**
     * Check shared facility title displayed or not
     * @return true / false
     */
    public boolean isSharedFacilitiyTitleDisplayed() {
        return selenium.waitInCaseElementVisible(facilitySharedTitle, 3) != null;
    }

    /**
     * Check parking facility title displayed or not
     * @return true / false
     */
    public boolean isParkingFacilitiyTitleDisplayed() {
        return selenium.waitInCaseElementVisible(facilityParkingTitle, 3) != null;
    }

    /**
     * Scroll to kost name element
     */
    public void scrollToKostName() throws InterruptedException {
        selenium.hardWait(3);
        selenium.pageScrollInView(kostName);
    }

    /**
     * Click on ask owner button
     * @throws InterruptedException
     */
    public void clickOnAskOwnerButton() throws InterruptedException {
        scrollToAskKostOwnerButton();
        selenium.javascriptClickOn(askKostOwnerButton);
    }

    /**
     * Check if owner kost chat is visible
     * @return visible true, otherwise false
     */
    private boolean isAskKostOwnerButtonPresent() {
        return selenium.waitInCaseElementVisible(askKostOwnerButton, 5) != null;
    }

    /**
     * Scroll to ask kost owner chat button
     * @throws InterruptedException
     */
    private void scrollToAskKostOwnerButton() throws InterruptedException {
        for(int i = 0; i <= 30; i++) {
            selenium.pageScrollUsingCoordinate(0, 3000);
            selenium.hardWait(10);
            if(isAskKostOwnerButtonPresent()) {
                break;
            }
        }
    }

    /**
     * Check button see all photo is present
     * @return true / false
     */
    public boolean isSeeAllPhotoButtonPresent() {
        return selenium.waitInCaseElementVisible(seeAllPhotoButton, 3) != null;
    }

    /**
     * Click on see all button
     * @throws InterruptedException
     */
    public void clickOnSeeAllButton() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(10,10);
        selenium.pageScrollInView(homeTextButton);
        selenium.javascriptClickOn(seeAllPhotoButton);
    }

    /**
     * Check button close photo is present
     * @return true / false
     */
    public boolean isCloseButtonPresent() {
        return selenium.waitInCaseElementVisible(closeButtonGallery,1) != null;
    }

    /**
     * Check text Foto Bangunan is present
     * @return true / false
     */
    public boolean isBuildingPhotosPresent() {
        return selenium.waitInCaseElementVisible(fotoBangunanText,1) != null;
    }

    /**
     * Check text foto kamar is present
     * @return true / false
     */
    public boolean isRoomPhotosPresent() {
        selenium.pageScrollUsingCoordinate(0, 2000);
        return selenium.waitInCaseElementVisible(fotoKamarText,1) != null;
    }

    /**
     * Check text foto kamar mandi is present
     * @return true / false
     */
    public boolean isBathroomPhotosPresent() {
        return selenium.waitInCaseElementVisible(fotoKamarMandiText,1) != null;
    }

    /**
     * Check text foto lainnya is present
     * @return true / false
     */
    public boolean isOthersPhotosPresent() {
        return selenium.waitInCaseElementVisible(fotoLainnya,1) != null;
    }

    /**
     * Click on detail photo gallery button
     * @throws InterruptedException
     */
    public void clickOnDetailPhotoButton() throws InterruptedException {
        selenium.clickOn(detailPhotoButton);
    }

    /**
     * swipe on photo gallery
     * @throws InterruptedException
     */
    public void clickOnArrowPhotoGalleryNextButton() {
        selenium.waitInCaseElementVisible(arrowPhotoNextButton, 5);
        selenium.javascriptClickOn(arrowPhotoNextButton);
    }

    /**
     * Check button see all is present
     * @return true / false
     */
    public boolean isLihatSemuaKosButtonPresent() {
        selenium.pageScrollInView(relatedCard);
        selenium.pageScrollInView(recommendationKosText);
        return selenium.waitInCaseElementVisible(seeAllRecomendationButton,4) != null;
    }

    /**
     * Check button arrow next is present
     * @return true / false
     */
    public boolean isArrowRecommendationButtonPresent() {
        return selenium.waitInCaseElementVisible(arrowRecommendationNextButton,5) != null;
    }

    /**
     * Check photo kos recommendation is present
     * @return true / false
     */
    public boolean isListPhotoRecommendationKosPresent() {
        return selenium.waitInCaseElementVisible(photoRecommendation,1) != null;
    }

    /**
     * Get recommendation kos Desc Text
     *
     * @return recommendation label
     */
    public String getRecommendationKosLabel() {
        String desc = selenium.getText(recommendationKosText);
        return  desc;
    }

    /**
     * swipe on next kos recommendation
     * @throws InterruptedException
     */
    public void clickOnArrowRecommendationNextButton() throws InterruptedException{
        selenium.clickOn(arrowRecommendationNextButton);
    }

    /**
     * swipe on previous kos recommendation
     * @throws InterruptedException
     */
    public void clickOnArrowRecommendationPreviousButton() throws InterruptedException {
        selenium.clickOn(arrowRecommendationPrevButton);
    }

    /**
     * click on lihat semua kos recommendation
     * @throws InterruptedException
     */
    public void clickOnSeeAllRecommendation() throws InterruptedException {
        selenium.clickOn(seeAllRecomendationButton);
    }

    /**
     * Get price filter type
     *
     * @return price kos
     */
    public String getPriceType() {
        String desc = selenium.getText(filterPrice);
        return desc;
    }

    /**
     * Get last kos recommendation
     *
     * @return text last kost card
     */
    public String getLastKostCardRecommendation() {
        return  selenium.getText(lastRecommendation);
    }

    /**
     * Is next recommendation kos present
     *
     * @return true or false
     */
    public boolean isNextRecommendationElementPresent() {
        return selenium.waitInCaseElementVisible(nextRecommendation, 3) != null;
    }

    /**
     * Is first kos recommendation present
     *
     * @return true or false
     */
    public boolean isFirstKostCardRecommendationPresent() {
        return selenium.waitInCaseElementVisible(firstKostCard, 3) != null;
    }

    /**
     * Check if per month radio button is visible
     * @return visible true, otherwise false
     */
    private boolean isPerMonthRadioButtonVisible() {
        return selenium.waitInCaseElementVisible(radPerMonth, 5) != null;
    }

    /**
     * Choose tomorrow date on kost detail
     */
    public void chooseTomorrowDateOnKostDetail() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(inpDateBox);
        WebElement element = driver.findElement(By.xpath("//span[@class='cell day today']/parent::div/following-sibling::div"));
        selenium.clickOn(element);
    }

    /**
     * Click of per month radio
     */
    public void choosePerMonthRadio() {
        if (!isPerMonthRadioButtonVisible()) {
            selenium.javascriptClickOn(inpDateBox);
        }
        selenium.javascriptClickOn(radPerMonth);
    }

    /**
     * Max month disclaimer text
     * @return string data type
     */
    public String getMaxMonthAndCheckinDateDisclaimerText(Integer number) {
        return selenium.getText(txtMaxMonthAndCheckinDateDisclaimer.get(number));
    }

    /**
     * Checkin date disclaimer text
     * @return string data type
     */
    public String getCheckinDateDisclaimerText() {
        return selenium.getText(txtCheckinDateDisclaimer);
    }


    /**
     * Get kost name on kost detail
     * @return string data type "Ancient Fuelweaver Web Automation"
     */
    public String getKostName() {
        return selenium.getText(txtKostName);
    }

    /**
     * check uang muka (DP) label on detail kost
     * @return uang muka (dp) label is present
     */
    public boolean uangMukaLabelPresent() {
        return selenium.waitInCaseElementVisible(uangMukaLabel, 5) != null;
    }

    /**
     * check uang muka (DP) label on pop up uang muka
     * @return uang muka (dp) label is present
     */
    public boolean uangMukaLabelOnPopupPresent() {
        return selenium.waitInCaseElementVisible(uangMukaLabelOnPopup, 5) != null;
    }

    /**
     * get uang muka (DP) label on pop up uang muka
     * @return value uang muka (dp)
     */
    public String getTotalUangMukaOnPopup(Integer angka) throws InterruptedException{
        return selenium.getText(uangMukaValueOnPopup.get(angka));
    }

    /**
     * click uang muka (DP) label
     */
    public void clickOnUangMukaLabel() throws InterruptedException{
        selenium.clickOn(uangMukaLabel);
    }

    /**
     * check biaya layanan mamikos label on pop up uang muka
     * @return biaya layanan mamikos label is present
     */
    public boolean biayaLayananLabelOnPopupPresent(Integer angka) {
        return selenium.waitInCaseElementVisible(biayaLayananLabelOnPopup.get(angka), 5) != null;
    }

    /**
     * get biaya layanan mamikos label on pop up uang muka
     * @return value biaya layanan mamikos
     */
    public String getTotalBiayaLayananOnPopup(Integer angka) {
        return selenium.getText(biayaLayananValueOnPopup.get(angka));
    }

    /**
     * check pelunasan label
     * @return pelunasan label is present
     */
    public boolean pelunasanLabelPresent() {
        return selenium.waitInCaseElementVisible(pelunasanLabel, 2) != null;
    }

    /**
     * check pelunasan label
     */
    public void clickOnPelunasanLabel() throws InterruptedException{
        selenium.clickOn(pelunasanLabel);
    }

    /**
     * check pembayaran penuh label
     */
    public void clickOnPembayaranPenuhLabel() throws InterruptedException{
        selenium.hardWait(3);
        selenium.javascriptClickOn(pembayaranPenuhLabel);
    }

    /**
     * check uang muka (DP) label on pop up uang muka
     * @return uang muka (dp) label is present
     */
    public boolean pelunasanLabelOnPopupPresent() {
        return selenium.waitInCaseElementVisible(pelunasanLabelOnPopup, 5) != null;
    }

    /**
     * check Total pembayaran pertama label
     * @return Total pembayaran pertama label is present
     */
    public boolean totalBayarPertamaLabelPresent() {
        return selenium.waitInCaseElementVisible(totalBayarPertamaLabel, 2) != null;
    }

    /**
     * click on Total pembayaran pertama label
     */
    public void clickOnTotalBayarPertamaLabel() throws InterruptedException{
        selenium.clickOn(totalBayarPertamaLabel);
    }

    /**
     * check Total pembayaran pertama label on popup uang muka
     * @return Total pembayaran pertama label is present
     */
    public boolean totalBayarPertamaLabelOnPopupPresent() {
        return selenium.waitInCaseElementVisible(totalBayarPertamaLabelOnPopup, 2) != null;
    }

    /**
     * check Total pembayaran pertama label on popup uang muka
     * @return Total pembayaran pertama label is present
     */
    public boolean totalBayarPertamaPelunasanLabelOnPopupPresent() {
        return selenium.waitInCaseElementVisible(totalBayarPertamaPelunasanLabelOnPopup, 2) != null;
    }

    /**
     * get Total pembayaran pertama label on popup uang muka
     * @return value Total pembayaran pertama
     */
    public String getTotalBayarPertamaOnPopup(Integer angka) {
        return selenium.getText(totalBayarPertamaValueOnPopup.get(angka));
    }

    /**
     * check wajib bayar dp warning
     * @return wajib bayar dp warning is present
     */
    public boolean wajibBayarWarningPresent() {
        return selenium.waitInCaseElementVisible(wajibDPWarning, 2) != null;
    }

    /**
     * click button close popup
     */
    public void clickOnCloseButton() throws InterruptedException{
        selenium.clickOn(icnCloseButton);
    }

    /**
     * check harga sewa per minggu label on pop up
     * @return harga sewa perminggu label is present
     */
    public boolean hargasewaLabelOnPopupPresent() {
        return selenium.waitInCaseElementVisible(hargaSewaLabelOnPopup, 5) != null;
    }

    /**
     * check Total pembayaran selanjutnya label on popup
     * @return Total pembayaran pertama label is present
     */
    public boolean totalBayarSelanjutnyaLabelOnPopupPresent() {
        return selenium.waitInCaseElementVisible(totalBayarSelanjutnyaLabelOnPopup, 2) != null;
    }

    /**
     * get Total pembayaran label on popup
     * @return value Total pembayaran
     */
    public String getTotalBayarOnPopup(Integer angka) {
        selenium.pageScrollInView(totalBayarValueOnPopup.get(3));
        return selenium.getText(totalBayarValueOnPopup.get(angka));
    }

    /**
     * get Total pembayaran label on popup
     * @return value Total pembayaran
     */
    public String getTotalBayar(Integer angka) {
        return selenium.getText(totalBayarValueOnPopup.get(angka));
    }

    /**
     * get uang muka (DP) label
     * * @return uang muka label
     */
    public String getUangMukaLabelOnPengajuanSection(Integer angka) throws InterruptedException{
        selenium.hardWait(5);
        selenium.waitInCaseElementVisible(uangMukaLabelOnPengajuanSection.get(0),10);
        return selenium.getText(uangMukaLabelOnPengajuanSection.get(angka));
    }

    /**
     * get info total DP label
     * * @return info dp label
     */
    public String getInfoDPLabelOnPengajuanSection(Integer angka) throws InterruptedException{
        return selenium.getText(uangMukaLabelOnPengajuanSection.get(angka));
    }

    /**
     * get total total DP
     * * @return totaal dp
     */
    public String getTotalDPOnPengajuanSection(Integer angka) throws InterruptedException{
        selenium.hardWait(5);
        return selenium.getText(totalDPOnPengajuanSection.get(angka));
    }

    /**
     * check Total pembayaran pertama label on pengajuan sewa section
     * @return Total pembayaran pertama label is present
     */
    public boolean totalBayarPertamaPelunasanLabelOnPengajuanSectionPresent() {
        return selenium.waitInCaseElementVisible(totalBayarPertamaPelunasanLabelOnPageSection, 2) != null;
    }

    /**
     * Check if total price is present
     * @return visible true, otherwise false
     */
    public boolean isTotalPricePresent() {
        return selenium.waitInCaseElementVisible(totalPriceText, 3) != null;
    }

    /**
     * Check if form booking date is present
     * @return visible true, otherwise false
     */
    public boolean isFormBookingDatePresent() {
        selenium.pageScrollUsingCoordinate(0,500);
        return selenium.waitInCaseElementVisible(bookingDateForm, 3) != null;
    }

    /**
     * Click on booking date form
     * @throws InterruptedException
     */
    public void clickOnBookingDate() throws InterruptedException {
        selenium.pageScrollInView(seeAllPhotoButton);
        selenium.waitInCaseElementVisible(bookingDateForm, 5);
        selenium.javascriptClickOn(bookingDateForm);
    }

    /**
     * Get booking date description inside booking date
     * @return string data type
     */
    public String getDescBookingDateText(String desc) {
        WebElement description = driver.findElement(By.xpath("//section[@class = 'booking-input-checkin-content__info']//p[contains(text(), '"+ desc +"')]"));
        selenium.pageScrollInView(description);
        return selenium.getText(description).toLowerCase();
    }

    /**
     * Check alert is present present / not
     * @return true / false
     */
    public boolean isAlertBookingDateTextPresent(String alert) {
        return selenium.isElementDisplayed(driver.findElement(By.xpath("//section[@class = 'booking-input-checkin-content__info']//p[contains(text(), '"+ alert +"')]")));
    }

    /**
     * Check if booking date is present
     * @return visible true, otherwise false
     */
    public boolean isDateBookingPresent() {
        return selenium.waitInCaseElementVisible(bookingDate, 3) != null;
    }

    /**
     * Check if booking duration form is present
     * @return visible true, otherwise false
     */
    public boolean isFormBookingDurationPresent() {
        return selenium.waitInCaseElementVisible(bookingDurationForm, 3) != null;
    }

    /**
     * Get booking duration
     * @return string duration (exm : month, day, week)
     */
    public String getBookingDurationText() {
        return selenium.getElementAttributeValue(bookingDurationForm, "placeholder").replace("Per ", "");
    }

    /**
     * Get total price duration text
     * @return string duration (exm : month, day, week)
     */
    public String getTotalPriceDurationText() {
        return selenium.getText(totalPriceDurationText);
    }

    /**
     * Check if Main price is present
     * @return displayed true, otherwise false
     */
    public boolean isMainPricePresent() {
        return selenium.isElementDisplayed(mainPricePropertyText);
    }

    /**
     * Check if booking duration is present
     * @return displayed true, otherwise false
     */
    public boolean isBookingButtonPresent() {
        return selenium.isElementDisplayed(bookingButton);
    }

    /**
     * Get Home breadcrumbs text
     * @return string Home text
     */
    public String getHomeBreadcrumbsText() {
        return selenium.getText(homeBreadcrumbsText);
    }

    /**
     * Get Town breadcrumbs text
     * @return string Town text
     */
    public String getTownBreadcrumbsText() {
        return selenium.getText(townBreadcrumbsText);
    }

    /**
     * Get property name breadcrumbs text
     * @return string property name text
     */
    public String getPropNameBreadcrumbsText() {
        return selenium.getText(propNameBreadcrumbsText);
    }

    /**
     * Click on breadcrumb town level
     * @throws InterruptedException
     */
    public void clickTownBreadcrumbs() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(7000, 0);
        selenium.clickOn(townBreadcrumbsText);
        selenium.hardWait(3);
    }

    /**
     * Click on breadcrumb home level
     * @throws InterruptedException
     */
    public void clickHomeBreadcrumbs() throws InterruptedException {
        selenium.clickOn(homeBreadcrumbsText);
        selenium.hardWait(3);
    }

    /**
     * check jika bayar pakai dp label on kost detail
     * @return jika bayar pakai dp label is present
     */
    public boolean jikaPakaiDPLabelOnKostDetailPresent() {
        return selenium.waitInCaseElementVisible(jikaPakiDPLabel, 5) != null;
    }

    /**
     * check jika bayar penuh label on kost detail
     * @return jika bayar penuh label is present
     */
    public boolean jikaBayarPenuhLabelOnKostDetailPresent() {
        return selenium.waitInCaseElementVisible(jikaBayarPenuhLabel, 3) != null;
    }

    /**
     * check bisa pakai dp warning
     * @return bisa pakai dp warning is present
     */
    public boolean bisaPakaiDPWarningPresent() {
        return selenium.waitInCaseElementVisible(bisaPakaiDPWarning, 2) != null;
    }

    /**
     * check pembayaran penuh label on pop up pembayaran penuh
     * @return pembayaran penuh label is present
     */
    public boolean pembayaranPenuhLabelOnPopupPresent() {
        return selenium.waitInCaseElementVisible(pembayaranPenuhLabelPopUp, 3) != null;
    }

    /**
     * check Total pembayaran penuh label on popup pembayaran penuh
     * @return Total pembayaran penuh label is present
     */
    public boolean totalBayarPenuhLabelOnPopupPresent() {
        return selenium.waitInCaseElementVisible(totalBayarPenuhLabelOnPopup, 2) != null;
    }

     /**
     * Click Back on Login Popup
     * @throws InterruptedException
     */
    public void clickBackOnLoginPopup() throws InterruptedException {
        selenium.clickOn(loginPopupBackButton);
        selenium.hardWait(1);
    }

    /**
     * Click see all facility without scroll to facility
     * @throws InterruptedException
     */
    public void clickSeeAllFacility() throws InterruptedException {
        selenium.javascriptClickOn(facilitySeeAllButton);
    }

    /**
     * check Top Facility Share in kos detail present
     * @return true / false
     * @param facilityShare is top facility we selected on filter
     */
    public boolean isTopFacilitySharePresent(String facilityShare) throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0,4800);
        selenium.hardWait(3);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        return selenium.isElementDisplayed(driver.findElement(By.xpath("//div[@class='detail-kost-facility-category detail-kost-public-facilities__content']//div[@class='bg-c-list-item__description']//p[contains(.,'"+ facilityShare +"')]")));
    }

    /**
     * Check Mix gender is displayed
     *
     * @return status true / false
     */
    public boolean isMixGenderDisplay(){
        return selenium.waitInCaseElementVisible(mixGenderFilter, 5)!= null;
    }

    /**
     * check owner badges on kost detail
     * @return owner badges label is present
     *  @throws InterruptedException
     */
    public boolean ownerBadgesAsPresent() throws InterruptedException {
        return selenium.waitInCaseElementVisible(ownerBadgesTitle, 5) != null;
    }

    /**
     * Get owner badges subtitle text
     * @return string Jumlah transaksi booking yang berhasil dilakukan text
     *  @throws InterruptedException
     */
    public String getOwnerBadesSubTitle() throws InterruptedException {
        selenium.hardWait(3);
        selenium.pageScrollInView(badgesBookingBerhasilSubTitle);
        return selenium.getText(badgesBookingBerhasilSubTitle);
    }

    /**
     * Check if Elite badge is present
     * @return displayed true, otherwise false
     */
    public boolean isEliteBadgePresent() {
        selenium.pageScrollInView(eliteBadge);
        return selenium.waitInCaseElementPresent(eliteBadge,5)!=null;
    }

    /**
     * Get success message cancel booking pop up
     * @return message success cancel booking
     */
    public String getMessageCancelBooking() {
        selenium.waitInCaseElementVisible(messageSuccessCancelBooking, 3);
        return selenium.getText(messageSuccessCancelBooking);
    }

    /**
     * Get text on button cancel booking pop up
     * @return button cancel booking
     */
    public String getBtnCancelBooking() {
        selenium.waitInCaseElementVisible(successCancelBookingButton, 3);
        return selenium.getText(successCancelBookingButton);
    }

    /**
     * Click on pop success cancel booking
     * @throws InterruptedException
     */
    public void clickOnPopUpCancelBooking() throws InterruptedException {
        selenium.waitTillElementIsVisible(successCancelBookingButton);
        selenium.clickOn(successCancelBookingButton);
    }

    /**
     * Check if Apik badge is present
     * @return displayed true, otherwise false
     */
    public boolean isApikBadgePresent() {
        selenium.pageScrollInView(apikBadge);
        return selenium.waitInCaseElementPresent(apikBadge,5)!=null;
    }

    /**
     * Scroll to Owner Badge Section
     */
    public void scrolltOWnerSection(){
        selenium.pageScrollUsingCoordinate(0,7000);
        selenium.pageScrollInView(ImageOwner);
        selenium.waitInCaseElementVisible(ImageOwner, 5);
    }

    /**
     * check owner badges section on kost detail
     * @return owner badges label is present
     *  @throws InterruptedException
     */
    public boolean ownerBadgesSectionAsPresent() throws InterruptedException {
        return selenium.waitInCaseElementVisible(ownerNameText, 5) != null;
    }

    /**
     * Check if Owner Verified is present
     * @return displayed true, otherwise false
     */
    public boolean isOwnerVerified() throws InterruptedException{
        selenium.pageScrollInView(kostName);
        return selenium.waitInCaseElementVisible(verifiedOwner,5) != null;
    }

    /**
     * Check if Owner Statement is present
     * @return displayed true, otherwise false
     */
    public boolean isOwnerStatement() throws InterruptedException{
        selenium.pageScrollInView(kostName);
        return selenium.waitInCaseElementVisible(ownerStatement,5) != null;
    }

    /**
     * check kosRule in kos detail present
     * @return true / false
     * @param kosRule is top kosRule we selected on filter
     */
    public boolean isTopKosRulePresent(String kosRule) throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0,4800);
        selenium.hardWait(3);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        return selenium.isElementDisplayed(driver.findElement(By.xpath("//div[@class='bg-c-list-item detail-kost-rule-item detail-kost-rules__item']//p[contains(.,'"+ kosRule +"')]")));
    }

    /**
     * Check if Singgahsini badge is present
     * @return displayed true, otherwise false
     */
    public boolean isSinggahsiniBadgePresent() {
        selenium.pageScrollInView(singgahsiniBadge);
        return selenium.waitInCaseElementPresent(singgahsiniBadge,5)!=null;
    }

    /**
     * Scroll to Kos Benefit Section
     * @throws InterruptedException
     */
    public void scrollToBenefitSection() throws InterruptedException {
        selenium.pageScrollInView(kosBenefit);
    }

    /**
     *  Check if Kos Benefit Title is present
     */
    public boolean isBenefitTitlePresent(){
        return selenium.waitInCaseElementVisible(benefitTitle,5)!=null;
    }

    /**
     *  Check if Kos Benefit Description is present
     */
    public boolean isBenefitDescPresent(){
        return selenium.waitInCaseElementVisible(benefitDesc, 5)!=null;
    }

    /**
     * Click Checkin date
     * @throws InterruptedException
     */
    public void clickOnChecinDate() throws InterruptedException {
        selenium.pageScrollInView(dateTextBox);
        selenium.javascriptClickOn(dateTextBox);
    }

    /**
     * Get text ketentuan kos on calender popup
     * @return text waktu booking kos terdekat
     */
    public String getKetentuanCheckinDateText() throws InterruptedException{
        return selenium.getText(disclaimerCalenderText);
    }

    /**
     * Scroll to Carousel Section
     * @throws InterruptedException
     */
    public void scrollToCarouselSection() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        selenium.waitInCaseElementVisible(carouselSection,3);
        selenium.pageScrollInView(carouselSection);
    }

    /**
     * Click on Ajukan Sewa Button
     * @throws InterruptedException
     */
    public void clickOnAjukanSewaCarousel() throws InterruptedException {
        selenium.pageScrollInView(ajukanSewaButton);
        selenium.javascriptClickOn(ajukanSewaButton);
        selenium.switchToWindow();

    }

    /**
     * Check Carousel Room Photo is displayed
     *
     * @return status true / false
     */
    public boolean isCarouselPhotoDisplayed() {
        return selenium.waitInCaseElementVisible(carouselKosPhoto, 3)!= null;
    }

    /**
     * Check Carousel Room Gender is displayed
     *
     * @return status true / false
     */
    public boolean isCarouselRoomGenderDisplayed() {
        return selenium.waitInCaseElementVisible(carouselGender, 3)!= null;
    }

    /**
     * Check Carousel Room Type is displayed
     *
     * @return status true / false
     */
    public boolean isCarouselRoomTypeDisplayed() {
        return selenium.waitInCaseElementVisible(carouselRoomType, 3)!= null;
    }

    /**
     * Check Carousel Room Facility is displayed
     *
     * @return status true / false
     */
    public boolean isCarouselRoomFacilityDisplayed() {
        return selenium.waitInCaseElementVisible(carouselRoomFacility, 3)!= null;
    }

    /**
     * Check Carousel Room Price is displayed
     *
     * @return status true / false
     */
    public boolean isCarouselRoomPriceDisplayed() {
        return selenium.waitInCaseElementVisible(carouselRoomPrice, 3)!= null;
    }

    /**
     * Check See Detail button is displayed
     *
     * @return status true / false
     */
    public boolean isCarouselSeeDetailButtonDisplayed() {
        return selenium.waitInCaseElementVisible(seeDetailButton, 3)!= null;
    }

    /**
     * Check Booking button is displayed
     *
     * @return status true / false
     */
    public boolean isCarouselBookingButtonDisplayed() {
        return selenium.waitInCaseElementVisible(ajukanSewaButton, 3)!= null;
    }

    /**
     * Check page "Pengajuan Sewa" is displayed
     *
     * @return status true / false
     */
    public boolean isBookingRequestPageShow() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.waitInCaseElementVisible(titlePageBookingRequest, 3)!= null;

    }

    /**
     * Click button swipe on PLM Carousel
     *
     * @throws InterruptedException
     */
    public void clickSwipePLM() throws InterruptedException {

        selenium.waitForJavascriptToLoad();
        int i = 0;
        if (selenium.waitInCaseElementVisible(swipeButtonPLM, 3) != null) {
            do {
                selenium.javascriptClickOn(swipeButtonPLM);
                i++;
            } while (i < 6);
        }
    }

    /**
     * Click on see another type button carousel
     * @throws InterruptedException
     */
    public void clickOnSeeAnotherType() throws InterruptedException {
        selenium.javascriptClickOn(seeOtherTypeButton);

    }
    /**
     * Click on Ajukan Sewa Button at page another type
     * @throws InterruptedException
     */
    public void clickOnFirstAJukanSewa() throws InterruptedException {
        selenium.javascriptClickOn(ajukanSewaFirstButton);
        selenium.switchToWindow();

    }

    /**
     * check Top Facility Room in kos detail present
     * @return true / false
     * @param facilityRoom is top facility we selected on filter
     */
    public boolean isTopFacilityRooomPresent(String facilityRoom) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        selenium.pageScrollInView(facilityRoomTitle);
        return selenium.isElementDisplayed(driver.findElement(By.xpath("//div[@class='detail-kost-facility-category detail-kost-room-facilities__content']//div[@class='bg-c-list-item detail-kost-facility-item detail-kost-facility-category__item']//p[contains(.,'"+ facilityRoom +"')]")));

    }

    /**
     * check Top Facility Bathroom in kos detail present
     * @return true / false
     * @param facilityBathRoom is top facility we selected on filter
     */
    public boolean isTopFacilityBathRooomPresent(String facilityBathRoom) throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 1500);
        selenium.hardWait(3);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        return selenium.isElementDisplayed(driver.findElement(By.xpath("//div[@class='detail-kost-facility-category detail-kost-bathroom-facilities__content']//div[@class='detail-kost-facility-category__item-grid bg-c-grid__item bg-is-col-6 detail-kost-facility-category__item-grid--two-columns']/div[contains(.,'"+ facilityBathRoom +"')]")));

    }

    /**
     * Click on See Detail Button
     * @throws InterruptedException
     */
    public void clickOnSeeDetailCarousel() throws InterruptedException {
        selenium.javascriptClickOn(seeDetailButton);
        selenium.switchToWindow();

    }

    /**
     * Click on Kos Card at page another type
     * @throws InterruptedException
     */
    public void clickOnKosCardOtherType() throws InterruptedException {
        selenium.javascriptClickOn(kosCardOtherType);
        selenium.switchToWindow();

    }

    /**
     * Check Other Type Room Photo is displayed
     *
     * @return status true / false
     */
    public boolean isOtherTypePhotoDisplayed() {
        return selenium.waitInCaseElementVisible(otherTypeKosPhoto, 3)!= null;
    }

    /**
     * Check Other Type Room Gender is displayed
     *
     * @return status true / false
     */
    public boolean isOtherTypeRoomGenderDisplayed() {
        return selenium.waitInCaseElementVisible(otherTypeGender, 3)!= null;
    }

    /**
     * Check Other Type Room Type is displayed
     *
     * @return status true / false
     */
    public boolean isOtherTypeRoomTypeDisplayed() {
        return selenium.waitInCaseElementVisible(otherTypeRoomType, 3)!= null;
    }

    /**
     * Check Other Type Room Facility is displayed
     *
     * @return status true / false
     */
    public boolean isOtherTypeRoomFacilityDisplayed() {
        return selenium.waitInCaseElementVisible(otherTypeRoomFacility, 3)!= null;
    }

    /**
     * Check Other Type Room Price is displayed
     *
     * @return status true / false
     */
    public boolean isOtherTypeRoomPriceDisplayed() {
        return selenium.waitInCaseElementVisible(otherTypeRoomPrice, 3)!= null;
    }

    /**
     * Check Other Type Booking button is displayed
     *
     * @return status true / false
     */
    public boolean isOtherTypeBookingButtonDisplayed() {
        return selenium.waitInCaseElementVisible(otherTypeajukanSewaButton, 3)!= null;
    }

    /**
     * Check see all button kos rule
     *
     * @return status true / false
     */
    public boolean isKosRuleButtonShow() {
        return selenium.waitInCaseElementVisible(seeAllKosRuleButton, 3)!= null;
    }

    /**
     * Check tab POI of Landmark
     *
     * @return status true / false
     */
    public boolean isPOILandmarkShow() {
        return selenium.waitInCaseElementVisible(tabPOILandmark, 3)!= null;
    }

    /**
     * Check section Facility Share
     *
     * @return status true / false
     */
    public boolean isFacShareShow() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        return selenium.waitInCaseElementVisible(facShareSection, 3)!= null;
    }

    /**
     * Click on see all button Facility Share
     * @throws InterruptedException
     */
    public void clickOnButtonFacShare() throws InterruptedException {
        selenium.javascriptClickOn(facShareSeeAllButton);

    }

    /**
     * Check shared facility description displayed or not
     * @return true / false
     */
    public boolean isSharedFacilitiyDescDisplayed() {
        return selenium.waitInCaseElementVisible(facDescription, 3) != null;
    }

    /**
     * Check shared facility pop up displayed or not
     * @return true / false
     */
    public boolean isSharedFacilitiyPopUpDisplayed() {
        return selenium.waitInCaseElementVisible(facPopup, 3) != null;
    }

    /**
     * Check facility parking section displayed or not
     * @return true / false
     */
    public boolean isFacParkingDisplayed() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        return selenium.waitInCaseElementVisible(facParkingSection, 3) != null;
    }

    /**
     * Check facility parking title displayed or not
     * @return true / false
     */
    public boolean isFacParkingTitleDisplayed() {
        return selenium.waitInCaseElementVisible(facParkirTitle, 3) != null;
    }

    /**
     * Check owner section displayed or not
     * @return true / false
     */
    public boolean isPromoOwnerSectionDisplayed() {
        return selenium.waitInCaseElementVisible(promoOwnerSection, 3) != null;
    }

    /**
     * Check owner title pop up displayed or not
     * @return true / false
     */
    public boolean isTitlePopUpPromoOwnerDisplayed() {
        return selenium.waitInCaseElementVisible(promoOwnerTitle, 3) != null;
    }

    /**
     * Click on Lihat Selengkapya at Promo Owner Section
     * @throws InterruptedException
     */
    public void clickOnButtonPromoOwner() throws InterruptedException {
        selenium.javascriptClickOn(promoOwnerButton);

    }

    /**
     * Click on text pop up promo owner
     * @throws InterruptedException
     */
    public void clickOnTextPopUpPromoOwner() throws InterruptedException {
        selenium.javascriptClickOn(promoOwnerClickText);

    }

    /**
     * Check pop up Hubungi Kost displayed or not
     * @return true / false
     */
    public boolean isChatKostPopUpDisplayed() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(chatKostPopUp, 3) != null;
    }

    /**
     * Check section Facility Room
     *
     * @return status true / false
     */
    public boolean isFacRoomShow() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        return selenium.waitInCaseElementVisible(facilityRoomSection, 3)!= null;
    }

    /**
     * Click on see all button Facility Room
     * @throws InterruptedException
     */
    public void clickOnSeeAllButtonFacRoom() throws InterruptedException {
        selenium.javascriptClickOn(facRoomSeeAllButton);

    }

    /**
     * Check room facility icon displayed or not
     * @return true / false
     */
    public boolean isRoomFacilitiyIconDisplayed() {
        return selenium.waitInCaseElementVisible(facilityRoomIcon, 3) != null;
    }

    /**
     * Check room facility name displayed or not
     * @return true / false
     */
    public boolean isRoomFacilitiyNameDisplayed() {
        return selenium.waitInCaseElementVisible(facRoomName, 3) != null;
    }

    /**
     * Check room facility pop up displayed or not
     * @return true / false
     */
    public boolean isRoomFacilitiyPopUpDisplayed() {
        return selenium.waitInCaseElementVisible(facPopup, 3) != null;
    }

    /**
     * Check section Facility Room
     *
     * @return status true / false
     */
    public boolean isFacBathShow() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
        return selenium.waitInCaseElementVisible(facBathSection, 3)!= null;
    }

    /**
     * Check bath facility icon displayed or not
     * @return true / false
     */
    public boolean isBathFacilitiyIconDisplayed() {
        return selenium.waitInCaseElementVisible(facBathIcon, 3) != null;
    }

    /**
     * Check bath facility name displayed or not
     * @return true / false
     */
    public boolean isBathFacilitiyNameDisplayed() {
        return selenium.waitInCaseElementVisible(facBathName, 3) != null;
    }

    /**
     * Check facility notes section displayed or not
     * @return true / false
     */
    public boolean isFacilityNotesSectionDisplayed() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(facNotesSection, 3) != null;
    }

    /**
     * Check kost facility notes displayed or not
     * @return true / false
     */
    public boolean isFacilityNotesDescDisplayed() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(facNotesDesc, 3) != null;
    }

    /**
     * Check expand button facility notes displayed or not
     * @return true / false
     */
    public boolean isExpandFacNotesDisplayed() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(expandFacNotesBtn, 3) != null;
    }

    /**
     * Click on expand facility notes
     * @throws InterruptedException
     */
    public void clickOnExpandFacNotes() throws InterruptedException {
        selenium.javascriptClickOn(expandFacNotesBtn);

    }

    /**
     * Check owner story section displayed or not
     * @return true / false
     */
    public boolean isOwnerStorySectionDisplayed() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(ownerStorySection, 3) != null;
    }

    /**
     * Check kost owner story displayed or not
     * @return true / false
     */
    public boolean isOwnerStoryDescDisplayed() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(ownerStoryDesc, 3) != null;
    }

    /**
     * Check expand button facility notes displayed or not
     * @return true / false
     */
    public boolean isExpandOwnerStoryDisplayed() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(expandOwnerStoryBtn, 3) != null;
    }

    /**
     * Click on expand facility notes
     * @throws InterruptedException
     */
    public void clickOnExpandOwnerStory() throws InterruptedException {
        selenium.javascriptClickOn(expandOwnerStoryBtn);

    }

    /**
     * Check if refund policy kos  is visible
     * @return visible true, otherwise false
     */
    private boolean isRefundPolicyAsPresent() {
        return selenium.waitInCaseElementVisible(refundPolicyTitle, 5) != null;
    }

    /**
     * Scroll to refund policy text
     * @throws InterruptedException
     */
    private void scrollToPeraturanKosText() throws InterruptedException {
        for(int i = 0; i <= 30; i++) {
            selenium.pageScrollUsingCoordinate(0, 800);
            selenium.hardWait(5);
            if(isRefundPolicyAsPresent()){
                break;
            }
        }
    }

    /**
     * Get refund title text on kost detail
     * @return e.g "Bisa Refund"
     *  @throws InterruptedException
     */
    public String refundPolicyPresent() throws InterruptedException {
        scrollToPeraturanKosText();
        return selenium.getText(refundPolicyTitle);
    }

    /**
     * Click bagaimana ketentuannya button
     *  @throws InterruptedException
     */
    public void clickRefundLink() throws InterruptedException {
        selenium.getText(refundDescription);
        WebElement element = driver.findElement(By.xpath("//*[@class='detail-kost-refund__content']//a[@class='bg-c-link bg-c-link--high']"));
        selenium.clickOn(element);
    }

    /**
     * Click on Kebijakan refund mamikos button
     *  @throws InterruptedException
     */
    public void getAndClickTnCRefund() throws InterruptedException {
        selenium.getText(tnCRefundTitle);
        selenium.hardWait(2);
        WebElement linkButton = driver.findElement(By.xpath("//p[contains(.,'Kebijakan refund Mamikos')]"));
        selenium.pageScrollInView(linkButton);
        selenium.javascriptClickOn(linkButton);
    }

    /**
     * Check property gender displayed or not
     * @return true / false
     */
    public boolean isPropertyGenderDisplayed(){
        selenium.refreshPage();
        return selenium.isElementPresent(propertyGender);
    }

    /**
     * Check property location displayed or not
     * @return true / false
     */
    public boolean isPropertyLocationDisplayed() {
        return selenium.isElementPresent(propertyLocation);
    }

    /**
     * Check property room availability displayed or not
     * @return true / false
     */
    public boolean isRoomAvailabilityDisplayed() {
        return selenium.isElementPresent(roomAvailability);
    }

    /**
     * get info total discount DP label
     * * @return info dp label
     */
    public String getInfoDiscountDPLabelOnPengajuanSection() {
        return selenium.getText(totalDiscountLabel);
    }

    /**
     * get info discount label on kost detail
     * * @return info discount label
     */
    public String getDiscountLabelOnKostDetail() {
        selenium.pageScrollInView(discountLabelKostDetail);
        return selenium.getText(discountLabelKostDetail);
    }

    /**
     * check discount label is not present on kost detail
     * @return
     * @throws InterruptedException
     */
    public boolean isDiscountLabelAbsence() throws InterruptedException {
        return selenium.isElementPresent(discountLabelKostDetail) != null;
    }

    /**
     * get refund before checkin
     * * @return refund before checkin text
     */
    public String getSSRefundBeforeCheckin() {
        return selenium.getText(refundSSContentOne);
    }

    /**
     * get refund DP
     * * @return refund dp text
     */
    public String getSSRefundDP() {
        return selenium.getText(refundSSContentTwo);
    }

    /**
     * get refund pay directly
     * * @return refund pay directly text
     */
    public String getAndClickSSRefundPayDirectly() throws InterruptedException {
        return selenium.getText(refundSSContentThree);
    }

    /**
     * Click on Ketentuan waktu berikut
     *  @throws InterruptedException
     */
    public void clickOnTimeCondition() throws InterruptedException{
        selenium.clickOn(timeConditionText);
    }

    /**
     * Check owner last online on detail page is displayed
     *
     * @return status true / false
     */
    public boolean isOwnerLastSeeDetailDisplayed() {
        return selenium.waitInCaseElementVisible(ownerLastSeenDetail, 3)!= null;
    }
}