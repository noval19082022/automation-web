package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BookingConfirmationPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingConfirmationPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(xpath = "//*[@class='info-kost__title-box--name text-large']")
    private WebElement roomName;

    @FindBy(css = ".info-price .detail-kost-price-item[discount] p:last-child")
    private WebElement roomPrice;

    @FindBy(id = "tenantName")
    private WebElement tenantName;

    @FindBy(css = "input[type='tel']")
    private WebElement tenantMobile;

    @FindBy(xpath =  "(//span[@class='bg-c-checkbox__icon'])[3]")
    private WebElement termsAndConditionCheckBox;

    @FindBy(xpath =  "//button[normalize-space()='Kirim pengajuan ke pemilik']")
    private WebElement submitToOwner;

    @FindBy(xpath = "//button[@class='bg-c-button booking-request-form__submit-btn hidden-xs bg-c-button--primary bg-c-button--lg bg-c-button--block']")
    private WebElement submitButton;

    private By duration = By.id("inputDuration");

    @FindBy(css = ".booking-success__button-chat")
    private WebElement chatKostOwner;

    @FindBy(className = "booking-success__button-chat")
    private WebElement chatPemilikKosButton;

    @FindBy(className = "caption-title")
    private WebElement belumBisaBookingModal;

    @FindBy(xpath = "//*[@class='modal-booking-button']//*[@class='btn btn-primary']")
    private WebElement lihatHistoryBookingButton;

    @FindBy(xpath = "//*[@class='card-toggle hidden-xs']")
    private WebElement lihatSelengkapnyaButton;

    @FindBy(xpath = "//div[@class='detail-cancel']/button[@class='btn btn-success']")
    private WebElement batalkanBookingButton;

    @FindBy(xpath = "//div[@class='modal fade in']//button[@class='btn btn-primary btn-cancel-yes']")
    private WebElement yaBatalkanButton;

    @FindBy(xpath = "//*[@title='Mamikos']")
    private WebElement mamikosButton;

    @FindBy(css = "img.booking-navbar__brand")
    private WebElement mamikosLogoInBooking;

    @FindBy(xpath = "//*[@id='bookingActionButtons']//button")
    private WebElement selanjutnyaButton;

    @FindBy(xpath = "//div[@class='booking-contract-modal-cancel modal fade in']//div[@class='modal-body']//button[@class='btn btn-primary']")
    private WebElement okBatalkanBookingPopupButton;

    private By seeSubmissionStatusButton = By.cssSelector("button.bg-c-button:nth-child(1)");

    private By closeBookingSucces = By.cssSelector("#bookingSuccess span.close-btn");

    private By exitBookingButton = By.cssSelector("button.hidden-xs:nth-child(1");

    private By continueBooking = By.cssSelector("#bookingModalCancel button.btn.btn-primary");

    private By saveDraftBooking = By.cssSelector("#bookingModalCancel button.btn.btn-success");

    private By handleLoadingForDraft = By.cssSelector("div.swal2-buttonswrapper.swal2-loading");

    private By confirmationTextTitleDraft = By.cssSelector("#bookingModalCancel div.confirmation-text > h4");

    private By confirmationTextBodyDraft = By.cssSelector("#bookingModalCancel div.confirmation-text > p");

    @FindBy(css = ".booking-success__button-chat")
    private WebElement chatKostOwnerButton;

    @FindBy(css = "#bookingSummary  div.info-kost__title-box > span")
    private WebElement kostNameNew;

    @FindBy(css = "#bookingSummary div.detail-kost-prices > div:nth-child(1) > div:nth-child(1)")
    private WebElement roomPriceNew;

    @FindBy(css = "#tenantName")
    private WebElement tenantNameNew;

    @FindBy(xpath = "//*[@class='booking-content']//*[@class='kost-price-item__amount is-highlighted']")
    private WebElement downPaymentPrice;

    @FindBy(xpath = "//p[contains(.,'Uang muka (DP)')]")
    private WebElement downPaymentText;

    @FindBy(css = "p.hidden-xs")
    private WebElement bookingFormPageTitle;

    @FindBy(css = ".booking-terms-content__dp-info-title")
    private WebElement kostWajibDPText1;

    @FindBy(css = ".booking-terms-content__dp-info-description")
    private WebElement kostWajibDPText2;

    @FindBy(css = ".booking-card__booking-action")
    private WebElement bookingButton;

    @FindBy(xpath="//*[@class='booking-form-tenant-info-item booking-form-tenant-info__item']//*[@class='booking-form-tenant-info-item__error bg-c-text bg-c-text--body-4 ']")
    private WebElement alertJobsText;

    @FindBy(xpath="//*[@class='bg-c-alert__content']//p[contains(.,'Masukkan pekerjaan')]")
    private WebElement alertTextAfterClick;

    /**
     * Get Room Name
     * @return
     */
    public String getRoomName(){
        return selenium.getText(roomName);
    }

    /**
     * Get Room Price
     * @return String
     */
    public  String getRoomPrice(){
        return selenium.getText(roomPrice);
    }

    /**
     * Get Tenant Name
     * @return
     */
    public String getTenantName(){
        return selenium.getText(tenantName);
    }

    /**
     * Get Tenant Mobile Number
     * @return
     */
    public String getTenantMobile(){
        return selenium.getText(tenantMobile);
    }

    /**
     * Check Tenant Gender is selected
     * @param gender
     * @return Tenet Gender checkBox is selected
     */
    public boolean isTenantGenderSelected(String gender) throws InterruptedException {
        String xpath = "//*[text()='" + gender + "']//parent::div[contains(@class, 'selected')]";
        WebElement element = driver.findElement(By.xpath("//label[text()='"+gender+"']/parent::div"));
        selenium.hardWait(3);
        return selenium.waitInCaseElementPresent(By.xpath(xpath), 5) != null;
    }

    /**
     * Click On Terms And Condition Check Box
     * @throws InterruptedException
     */
    public void clickOnTermsAndConditionCheckBox() throws InterruptedException {
        selenium.hardWait(5);
        selenium.javascriptClickOn(termsAndConditionCheckBox);
    }

    /**
     * If the TnC check box is visible
     * @return true means the TnC checkbox is visible, false is otherwise
     */
    public boolean isTnCcheckBoxVisible() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.waitInCaseElementClickable(termsAndConditionCheckBox, 3) != null;
    }

    /**
     * Click On submit to owner
     * @throws InterruptedException
     */
    public void clickOnSubmitToOwner() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(submitToOwner);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click on Submit Button of Booking page
     * @throws InterruptedException
     */
    public void clickOnSubmitButton() throws InterruptedException {
        selenium.pageScrollInView(submitButton);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
        selenium.javascriptClickOn(submitButton);
    }

    /**
     * If the Submit button (Ajukan sewa button in form booking) is visible
     * @return true means submit button is visible, false means otherwise
     */
    public boolean isSubmitButtonVisible() throws InterruptedException {
        selenium.pageScrollInView(submitButton);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
        return selenium.waitInCaseElementClickable(submitButton, 5) != null;
    }

    /**
     * get duration text from booking confirmation
     * @return String exp 1 Tahun etc
     */
    public String getDurationText() {
        return selenium.getText(duration);
    }

    /**
     * Wait until button chat success
     */
    public void waitUntilButtonChatSuccessBookingAppear() {
        selenium.waitInCaseElementVisible(chatKostOwner, 10);
    }

    /**
     * Click on close "x" button success booking confirmation
     * @throws InterruptedException
     */
    public void clickOnBookingSuccessCloseButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(closeBookingSucces);
        selenium.hardWait(2);
        selenium.clickOn(closeBookingSucces);
    }

    /**
     * Check if button see submission status in DOM
     * @return true if the submission button present otherwise false
     * @throws InterruptedException
     */
    public boolean isSubmissionStatusButtonPresent() {
        return selenium.waitInCaseElementPresent(seeSubmissionStatusButton, 5) != null;
    }

    /**
     * Verify element is present
     * throws InterruptedException
     */
    public void bookingSuccessfullyIsPresent() throws InterruptedException {
        selenium.isElementPresent(By.xpath("//div[@class='booking-success__container']"));
    }

    /**
     * exit pop up draft booking
     */
    public void clickOnEXitBookingButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(exitBookingButton);
        selenium.clickOn(exitBookingButton);
    }

    /**
     * to handling loading for pop up draft
     */
    public boolean isHandleLoadingForPopUpDraft(){
        return selenium.waitInCaseElementVisible(handleLoadingForDraft, 10) != null;
    }

    /**
     * confirmation pop up draft text "Yakin batalkan Booking?"
     */
    public String getTitleConfirmationDraftText(){
        return selenium.getText(confirmationTextTitleDraft);
    }

    /**
     * confirmation pop up draft text "Jika kamu batalkan, kamu tetap bisa melihat Kos ini di Draft..."
     */
    public String getBodyConfirmationDraftText(){
        return selenium.getText(confirmationTextBodyDraft);
    }

    /**
     * continue booking after pop up draft show up
     */
    public void continueBookingFromPopUpDraft() throws InterruptedException {
        selenium.clickOn(continueBooking);
    }

    /**
     * save booking to draft
     */
    public void saveDraftBookingFromPopUpDraft() throws InterruptedException {
        selenium.clickOn(saveDraftBooking);
    }

    /**
<<<<<<< HEAD
     * click Chat Pemilik Kos Button in screen after booking
     * @throws InterruptedException
     */
    public void clickOnChatPemilikKosBtn() throws InterruptedException {
        selenium.waitTillElementIsVisible(chatPemilikKosButton);
        selenium.clickOn(chatPemilikKosButton);
    }

    /**
     * check if user have incomplete booking
     * @return true if belum bisa booking pop up present on the screen
     */
    public boolean isBelumBisaBookingPresent() {
        return selenium.waitInCaseElementVisible(belumBisaBookingModal,10) != null;
    }

    /**
     * click lihat history booking button in belum bisa booking pop up
     * @throws InterruptedException
     */
    public void clickOnLihatHistoryBookingButton() throws InterruptedException {
        selenium.clickOn(lihatHistoryBookingButton);
    }

    /**
     * Click Lihat Selengkapnya button in Booking list
     * @throws InterruptedException
     */
    public void clickOnLihatSelengkapnyaButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(lihatSelengkapnyaButton);
        selenium.clickOn(lihatSelengkapnyaButton);
    }

    /**
     * Click on Batalkan Booking button in Booking details
     * @throws InterruptedException
     */
    public void clickOnBatalkanBooking() throws InterruptedException {
        selenium.waitTillElementIsVisible(batalkanBookingButton);
        selenium.hardWait(3);
        selenium.pageScrollInView(batalkanBookingButton);
        selenium.waitTillElementIsClickable(batalkanBookingButton);
        selenium.clickOn(batalkanBookingButton);
    }

    /**
     * Click Ya Batalkan button in confirmation batalakan booking
     * @throws InterruptedException
     */
    public void confirmBatalkanBooking() throws InterruptedException {
        selenium.hardWait(2);
        selenium.waitTillElementIsVisible(yaBatalkanButton);
        selenium.clickOn(yaBatalkanButton);
        selenium.hardWait(3);
    }

    /**
     * Click Ok to close pop up after batalkan booking
     * @throws InterruptedException
     */
    public void closeBatalkanBookingPopUp() throws InterruptedException {
        selenium.waitTillElementIsVisible(okBatalkanBookingPopupButton);
        selenium.clickOn(okBatalkanBookingPopupButton);
    }

    /**
     * click on Mamikos in header
     * @throws InterruptedException
     */
    public void clickOnMamikos() throws InterruptedException {
        selenium.waitTillElementIsVisible(mamikosButton);
        selenium.clickOn(mamikosButton);
    }

    /**
     * Click on chat kost owner / chat pemilik kost on success booking page
     * @throws InterruptedException
     */
    public void clickOnChatKostOwnerButton() throws InterruptedException {
        selenium.waitTillElementIsNotVisible(handleLoadingForDraft);
        selenium.javascriptClickOn(chatKostOwnerButton);
    }

    /**
     * Get Room Name New (after revamp booking form)
     * @return
     */
    public String getRoomNameNew(){
        return selenium.getText(kostNameNew);
    }

    /**
     * Get Room Price New (after revamp booking form)
     * @return
     */
    public String getRoomPriceNew(){
        return selenium.getText(roomPriceNew);
    }

    /**
     * Get Tenant Name New (after revamp booking form)
     * @return
     */
    public String getTenantNameNew(){
        return selenium.getElementAttributeValue(tenantNameNew, "value");
    }

    /**
     * Click mamikos logo in booking page
     * @throws InterruptedException
     */
    public void clickOnMamikosLogo() throws InterruptedException {
        selenium.waitTillElementIsVisible(mamikosLogoInBooking);
        selenium.javascriptClickOn(mamikosLogoInBooking);
    }

    /**
     * Get down payment active state
     * @return string data type "Uang Muka (DP)"
     */
    public String getDownPaymentText() {
        return selenium.getText(downPaymentText);
    }

    /**
     * Get down payment active price text
     * @return string data type e.g "Rp101.100"
     */
    public String getDownPaymentPriceText() {
        return selenium.getText(downPaymentPrice);
    }

    /**
     * Get booking page title
     * @return data string type "Pengajuan Sewa"
     */
    public String getBookingFormPageTitle() {
        return selenium.getText(bookingFormPageTitle);
    }

    /**
     * Get text active down payment
     * @return string data type
     */
    public String getText1KostDownPayment() throws InterruptedException{
        selenium.hardWait(5);
        selenium.pageScrollInView(kostWajibDPText1);
        selenium.pageScrollInView(kostWajibDPText1);
        return selenium.getText(kostWajibDPText1);
    }

    /**
     * Get text active down payment
     * @return string data type
     */
    public String getText2KostDownPayment() {
        return selenium.getText(kostWajibDPText2);
    }

    /**
     * get error alert on pekerjaan
     * @return String Pekerjaan wajib diisi
     */
    public String getAlertJobsText() throws InterruptedException{
        return selenium.getText(alertJobsText);
    }

    /**
     * get error alert on jobs after click
     * @return String Masukkan pekerjaan untuk memproses pengajuan sewa.
     */
    public String getAlertJobsTextAfterClick() throws InterruptedException{
        selenium.hardWait(2);
        return selenium.getText(alertTextAfterClick);
    }
}
