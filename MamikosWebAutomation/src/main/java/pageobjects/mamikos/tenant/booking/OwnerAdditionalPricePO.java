package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class OwnerAdditionalPricePO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public OwnerAdditionalPricePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//*[@class='update-price__caption']//h1")
    private WebElement captionAdditionalPriceTitle;

    @FindBy(xpath = "//*[@class='update-price__caption']//p")
    private WebElement captionAdditionalPriceSubtitle;

    @FindBy(xpath = "//*[@class='c-mk-button c-mk-button--green-brand']")
    private WebElement aturSekarangButton;

    @FindBy(xpath = "(//*[@class='additional-price-item'])[3]//*[@class='check is-green-brand']")
    private WebElement depositToggle;

    @FindBy(xpath = "//*[@class='media-content']")
    private WebElement informationDeposit;

    @FindBy(xpath = "//*[@class='price-item']//input")
    private WebElement fieldAmountDeposit;

    @FindBy(xpath = "//button[contains(.,'Simpan')]")
    private WebElement saveButton;

    @FindBy(xpath = "//*[@class='toast is-dark is-bottom']//div[contains(.,'Biaya berhasil diubah')]")
    private WebElement toastInputOrEditSuccess;

//    @FindBy(xpath = "//*[.='Deposit']/parent::*//following-sibling::*//button[1]")
    @FindBy(xpath = "(//button[contains(.,'Ubah')])[1]")
    private WebElement ubahButtonDeposit;

    @FindBy(xpath = "//*[@class='additional-price-item__info-price']")
    private WebElement depositPrice;

//    @FindBy(xpath = "//*[.='Deposit']/parent::*//following-sibling::*//button[2]")
    @FindBy(xpath="(//button[contains(.,'Hapus')])[1]")
    private WebElement hapusButtonDeposit;

    @FindBy(xpath = "//*[@class='c-mk-card__title card-title --left']")
    private WebElement headerDeleteConfirmation;

    @FindBy(xpath = "//*[@class='c-mk-card__body']//button[contains(.,'Kembali')]")
    private WebElement kembaliConfirmation;

    @FindBy(xpath = "//*[@class='c-mk-card__body']//button[contains(.,'Ya, Hapus')]")
    private WebElement hapusConfirmation;

    @FindBy(xpath = "//*[@class='toast is-dark is-bottom']//div[contains(.,'Biaya berhasil dihapus')]")
    private WebElement toastSuccessDeleted;

    @FindBy(css=".c-field-select__label + div .c-field-select__select")
    private WebElement selectedDPPercentage;

    @FindBy(xpath="//*[@class='additional-price-item'][4]//*[@class='check is-green-brand']")
    private WebElement downPaymentToggle;

    @FindBy(css="#__nuxt .additional-price-item__info-title")
    private WebElement downPaymentActivePercentage;

    @FindBy(css="#__nuxt .additional-price-item__info-price")
    private WebElement downPaymentActivePriceRange;

    @FindBy(xpath = "//*[contains(text(), 'DP')]/parent::*//following-sibling::*//button[2]")
    private WebElement downPaymentDeleteButton;

    @FindBy(xpath = "(//*[@class='additional-price-item'])[2]//*[@class='check is-green-brand']")
    private WebElement dendaToggle;

    @FindBy(xpath = "//h3[contains(.,'Biaya Denda')]")
    private WebElement infomationDenda;

    @FindBy(css = ".input.field-amount")
    private WebElement totalFinePrice;

    @FindBy(xpath ="//*[@class='c-field-select__select']")
    private WebElement fineTime;

    @FindBy(xpath ="//*[@class='c-field-select__select']/option[2]")
    private WebElement fineTimeValue;

    @FindBy(xpath ="//*[@class='c-field-input__input bordered']")
    private WebElement totalFineTime;

    @FindBy(css = ".is-active .c-field-select__select")
    private WebElement dropPenaltyDuration;

    @FindBy(xpath = "//*[@class='additional-price-item__info-price']")
    private WebElement dendaPrice;

//    @FindBy(xpath ="//*[.='Denda']/parent::*//following-sibling::*//button[1]")
    @FindBy(xpath = "(//button[contains(.,'Ubah')])[1]")
    private WebElement ubahDendaButton;

//    @FindBy(xpath = "//*[.='Denda']/parent::*//following-sibling::*//button[2]")
    @FindBy(xpath = "(//button[contains(.,'Hapus')])[1]")
    private WebElement deleteDenda;

    private By otherPriceToggle = By.cssSelector(".additional-price-item:nth-child(1) span.is-green-brand");

    @FindBy(css = "input.c-field-input__input")
    private WebElement inputOtherPriceName;

    @FindBy(xpath = "//*[@class='c-mk-card add-price-card']")
    private WebElement dpPopUp;

    @FindBy(xpath = "//*[@class='bg-c-radio bg-c-radio--checked']")
    private WebElement defaultDPChoice;


//    @FindBy(css = ".price-value input")
//    private WebElement inputOtherPriceAmount;

    //private By inputOtherPriceAmount = By.cssSelector(".price-value input");
    @FindBy(xpath = "//input[@class='input']")
    private WebElement inputOtherPriceAmount;

    @FindBy(css = ".form-action-button button")
    private WebElement btnSaveOtherPrice;

    @FindBy(css=".additional-price-item:nth-child(1) .additional-price-item__info-title")
    private WebElement textOtherPriceActiveName;

    @FindBy(css=".additional-price-item:nth-child(1) .additional-price-item__info-price")
    private WebElement textOtherPriceActiveNumber;

    @FindBy(css=".property-room__update-price-button button")
    private WebElement btnUpdatePrice;

    @FindBy(css=".additional-price-item .additional-price-item__action button:last-child")
    private WebElement btnDeleteActiveOtherPrice;

    @FindBy(xpath = "//*[@class='form-price-dp__dp-options']/child::p")
    private WebElement informationDp;

    @FindBy(xpath = "//*[@class='bg-c-alert__content-description bg-c-text bg-c-text--body-4 ']")
    private WebElement tidakWajibInformationDp;

    @FindBy(xpath = "//*[@class='bg-c-text bg-c-text--body-4 ']")
    private WebElement wajibInformationDp;

    /**
     * get title on additional price pop up at dashboard
     */
    public String getCaptionAdditionalPrice() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(captionAdditionalPriceTitle);
    }

    /**
     * get subtitle on additional price pop up at dashboard
     */
    public String getCaptionAdditionalPriceSubtitle(){
        return selenium.getText(captionAdditionalPriceSubtitle);
    }

    /**
     * click atur sekarang on pop up additional price at dashboard
     */
    public void clickAturSekarang() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(aturSekarangButton);
    }

    /**
     * @param kost
     */
    public void clickKostName(String kost) throws InterruptedException {
        String kostList = "(//*[@class='property-list__item-name']//span[contains(.,'"+ kost +"')])[1]";
        selenium.clickOn(By.xpath(kostList));
    }

    /**
     * click toggle on deposit
     */
    public void clickDepositToggle() throws InterruptedException {
        selenium.clickOn(depositToggle);
    }

    /**
     * get information about additional deposit
     */
    public String getInformationDeposit(){
        return selenium.getText(informationDeposit);
    }

    /**
     * input amount deposit
     * @param depositPrice is text we want to input deposit
     */
    public void inputAmountDeposit(String depositPrice) throws InterruptedException {
        selenium.clickOn(fieldAmountDeposit);
        selenium.hardWait(2);
        selenium.enterText(fieldAmountDeposit, depositPrice, false);
        selenium.hardWait(2);
        selenium.clickOn(saveButton);
    }

//    Need solution for this step
//    /**
//     * get toast when input or edit additional price is success
//     */
//    public String getToastInputOrEditSuccess(String toastText){
//        String toast = "//*[@class='toast is-dark is-bottom']//div[contains(.,'"+ toastText +"')]";
//        selenium.waitTillElementIsVisible(driver.findElement(By.xpath(toast)), 60);
//        return selenium.getText(driver.findElement(By.xpath(toast)));
//    }

    /**
     * get deposit list when deposit is activated
     */
    public String getDepositsPriceTest() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(depositPrice);
    }

    /**
     * click ubah button on deposit
     * and select all value
     * then delete value
     */
    public void clickUbahDepositButton() throws InterruptedException {
        selenium.clickOn(ubahButtonDeposit);
        int depositText = getDepositInputAmount().length();
        selenium.clickOn(fieldAmountDeposit);
        for(int i = 0; i < depositText; i++) {
            driver.findElement(By.xpath("//*[@class='input field-amount']")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
            driver.findElement(By.xpath("//*[@class='input field-amount']")).sendKeys(Keys.BACK_SPACE);
        }
    }

    /**
     * and input the new deposit on the field
     * then click Simpan
     * @param depositPrice
     */
    public void inputNewDeposit(String depositPrice) throws InterruptedException {
        selenium.enterText(fieldAmountDeposit, depositPrice, false);
        selenium.hardWait(2);
        selenium.clickOn(saveButton);
        selenium.hardWait(2);
    }

    /**
     * delete deposit list
     */
    public void deleteDeposit() throws InterruptedException {
        selenium.clickOn(hapusButtonDeposit);
    }

    /**
     * get header on delete confirmation pop up deposit
     */
    public String getHeaderDeleteConfirmationDeposit(){
        return selenium.getText(headerDeleteConfirmation);
    }

    /**
     * click kembali on delete confirmation pop up deposit
     */
    public void clickKembaliOnDeleteConfirmation() throws InterruptedException {
        selenium.clickOn(kembaliConfirmation);
    }

    /**
     * click delete on delete confirmation pop up deposit
     */
    public void clickHapusOnDeleteConfirmation() throws InterruptedException {
        selenium.clickOn(hapusConfirmation);
        selenium.hardWait(3);
    }

    /**
     * get toast delete deposit is success
     */
    public String getToastDeletedDeposit(){
        return selenium.getText(toastSuccessDeleted);
    }

    /**
     * check deposit list not appears
     * @return false if not appears
     */
    public boolean isDepositListAppears(){
        return selenium.waitInCaseElementVisible(depositPrice, 3) != null;
    }

    /**
     * Get field deposit current text price
     * must trigger deposit's change pop-up before use this method
     * @return string text e.g "Rp100.000"
     */
    private String getDepositInputAmount() {
        return selenium.getElementAttributeValue(fieldAmountDeposit, "value");
    }

    /**
     * Get selected percentage text
     * @return string data type
     */
    public String getDownPaymentCurrentPercentage() throws InterruptedException{
        selenium.hardWait(5);
       return selenium.getSelectedDropdownValue(selectedDPPercentage);
    }

    /**
     * Get price list on down payment percentage select
     * @param index input with int 1-6
     * @return String text e.g "DP Sewa Per Bulan"
     */
    public String getDPPriceList(int index) {
        String priceList = ".form-price-dp__price-list:nth-child("+ index +") span:nth-child(1)";
        WebElement e = driver.findElement(By.cssSelector(priceList));
        return selenium.getText(e);
    }

    /**
     * Get price number list on down payment percentage select
     * @param index input with int 1-6
     * @return String text e.g "Rp100.000"
     */
    public String getDPPriceListNumber(int index) {
        String priceList = ".form-price-dp__price-list:nth-child("+ index +") span:nth-child(2)";
        WebElement e = driver.findElement(By.cssSelector(priceList));
        return selenium.getText(e);
    }

    /**
     * Down payment toggle click
     * @throws InterruptedException
     */
    public void clickDownPaymentToggle() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(downPaymentToggle);
    }

    /**
     * Click on save button, this general method for all save button that contains "Simpan" word. With html tag is button.
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Get string text from active percentage downpayment
     * @return string text e.g DP10%
     */
    public String getDPPricePercentage() {
        return selenium.getText(downPaymentActivePercentage);
    }


    /**
     * Get string text from active down payment percentage range
     * @return string text e.g Rp100.000-Rp200.000
     */
    public String getDPPriceRange() {
        return selenium.getText(downPaymentActivePriceRange);
    }

    /**
     * click on delete/hapus down payment button
     * @throws InterruptedException
     */
    public void clickOnDeleteDownPayment() throws InterruptedException {
        selenium.clickOn(downPaymentDeleteButton);
    }

    /**
     * click  denda toggle  button
     * @throws InterruptedException
     */
    public void clickOnDendaToggle() throws InterruptedException {
        selenium.clickOn(dendaToggle);
    }

    /**
     * Get string text from active denda
     * @return string text e.g Rp50.000
     */
    public String getDendaInformation() {
        return selenium.getText(infomationDenda);
    }

    /**
     * input denda price
     * @param dendaPrice
     */
    public void inputDendaPrice(String dendaPrice) throws InterruptedException {
        selenium.clickOn(fieldAmountDeposit);
        selenium.enterText(fieldAmountDeposit, dendaPrice, false);
        selenium.hardWait(2);
    }

    /**
     * click on fine time
     * @throws InterruptedException
     */
    public void clickOnSelectFineTime() throws InterruptedException{
        selenium.clickOn(fineTime);
        selenium.hardWait(5);
        selenium.clickOn(fineTimeValue);
    }

    /**
     * input denda time
     * @param dendaTime
     */
    public void inputDendaTime(String dendaTime) throws InterruptedException {
        selenium.clickOn(totalFineTime);
        selenium.enterText(totalFineTime, dendaTime, false);
        selenium.hardWait(2);
        selenium.clickOn(saveButton);
    }

    /**
     * check deposit list not appears
     * @return false if not appears
     */
    public boolean isDendaListAppears(){
        return selenium.waitInCaseElementVisible(dendaPrice, 4) != null;
    }

    /**
     * get denda list when denda is activated
     */
    public String getDendaPriceText() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.getText(dendaPrice);
    }

    /**
     * get denda time on form
     */
    public String getDendaTime() {
        return selenium.getElementAttributeValue(fineTime,"value");
    }

    /**
     * click ubah button on denda
     * and select all value
     * then delete denda price and denda time
     */
    public void clickUbahDendaButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(ubahDendaButton);
        int dendaText = getDepositInputAmount().length();
        selenium.clickOn(fieldAmountDeposit);
        for (int i = 0; i < dendaText; i++) {
            driver.findElement(By.xpath("//*[@class='input field-amount']")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
            driver.findElement(By.xpath("//*[@class='input field-amount']")).sendKeys(Keys.BACK_SPACE);
        }
        int dendaTime = getDendaTime().length();
        for (int i = 0; i < dendaTime; i++) {
            driver.findElement(By.xpath("//*[@class='c-field-input__input bordered']")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
            driver.findElement(By.xpath("//*[@class='c-field-input__input bordered']")).sendKeys(Keys.BACK_SPACE);

        }
    }

    /**
     * click on delete denda button
     * @throws InterruptedException
     */
    public void clickDeleteDendaButton() throws InterruptedException {
        selenium.clickOn(deleteDenda);
    }

    /**
     * Click on other price toggle
     * Other price's toggle is first index from class .additional-price-item
     */
    public void clickOnOtherPriceToggle() {
        selenium.waitTillElementIsVisible(otherPriceToggle, 30);
        selenium.javascriptClickOn(otherPriceToggle);
    }

    /**
     * Enter name and price on other price pop up form
     * trigger clickOnOtherPriceToggle() before perform this method
     * @param name other price name
     * @param price string data type e.g 100000
     */
    public void inputOtherPrice(String name, String price) throws InterruptedException {
        selenium.enterText(inputOtherPriceName, name, false);
        selenium.hardWait(2);
        selenium.waitTillElementIsVisible(inputOtherPriceAmount, 30);
        selenium.clickOn(inputOtherPriceAmount);
        selenium.enterText(inputOtherPriceAmount, price, false);
        selenium.hardWait(2);
        selenium.clickOn(btnSaveOtherPrice);
    }

    /**
     * Get other price active name
     * @return String data type e.g "Biaya Parkir"
     */
    public String getActiveOtherPricesName() {
        return selenium.getText(textOtherPriceActiveName);
    }

    /**
     * Get other price active number
     * @return String data type e.g "Rp10.000"
     */
    public String getActiveOtherPriceNumber() {
        return selenium.getText(textOtherPriceActiveNumber);
    }

    /**
     * Click on update price button
     * @throws InterruptedException
     */
    public void clickOnUpdatePriceButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(btnUpdatePrice);
    }

    /**
     * Click on update price button
     * @throws InterruptedException
     */
    public void deleteActiveAdditionalPrice() throws InterruptedException {
        if(selenium.waitInCaseElementVisible(btnDeleteActiveOtherPrice, 1) != null){
            selenium.clickOn(btnDeleteActiveOtherPrice);
            clickHapusOnDeleteConfirmation();
        }
    }

    /**
     * check if other price name on index number 1 is visible
     * @return true if other price visible, otherwise false
     */
    public boolean isOtherPriceNamePresent() {
        return selenium.waitInCaseElementVisible(textOtherPriceActiveName, 5) != null;
    }

    /**
     * check if other price number on index number 1 is visible
     * @return true if number price visible, otherwise false
     */
    public boolean isOtherPriceNumberPresent() {
        return selenium.waitInCaseElementVisible(textOtherPriceActiveNumber, 5) != null;
    }

    /**
     * Choose penaly duration
     * @param duration input with Hari, Minggu, Bulan
     */
    public void choosePenaltyDuration(String duration) throws InterruptedException {
        selenium.selectDropdownValueByText(dropPenaltyDuration, duration);
        selenium.hardWait(3);
    }

    /**
     * Get total penalty time input attribute value
     * @param attribute desired attributed e.g "class"
     * @return string data type e.g "1" and "31"
     */
    public String getPenaltyDurationAmountInputAttributeValue(String attribute) {
        return selenium.getElementAttributeValue(totalFineTime, attribute);
    }

    /**
     * check dp pop up is present after click toggle active dp
     */
    public void dpPopUpPresent() {
        selenium.waitInCaseElementVisible(dpPopUp,3);
    }

    /**
     * get information about dp
     */
    public String getInformationDP() throws InterruptedException{
        selenium.hardWait(3);
        selenium.waitInCaseElementVisible(informationDp,5);
        return selenium.getText(informationDp);
    }

    /**
     * check default options wajib dp untuk seluruh calon penyewa
     */
    public void tidakWajibChoicePresent() {
        selenium.waitInCaseElementVisible(defaultDPChoice,2);
    }

    /**
     * get information about tidak wajib dp
     */
    public String getInformationTidakWajibDP(){
        return selenium.getText(tidakWajibInformationDp);
    }

    /**
     * click on Ya or Tidak wajib DP
     * @throws InterruptedException
     */
    public void clickOnOptionYaorTidak(String choice) throws InterruptedException{
        selenium.hardWait(2);
        WebElement element = driver.findElement(By.xpath("//p[contains(text(),'"+choice+"')]"));
        selenium.clickOn(element);
    }

    /**
     * get information about DP Wajib
     */
    public String getDPInformation(){
        return selenium.getText(wajibInformationDp);
    }
}