package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BookingFormFillPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingFormFillPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "#inputDuration + button")
    private WebElement rentDurationIncreaseButton;

    @FindBy(css = ".form-date-checkin .booking-input-checkin__input")
    private WebElement dateTextBox;

    @FindBy(css = "div#bookingActionButtons button")
    private WebElement nextButton;

    @FindBy(css = ".is-open .day__month_btn + .next")
    private WebElement nextMonthIcon;

    @FindBy(css = "#bookingInputPriceDate .booking-input-price__title h2")
    private WebElement textDefinedPriceAndRentDuration;

    @FindBy(css = "#bookingInputPriceDate .booking-input-price__title span")
    private WebElement textChooseBasedOnYourStayDuration;

    @FindBy(css= ".info-date--rent-duration .info-date--date")
    private WebElement rentDuration;

    @FindBy(css = "#bookingSummary .info-date--rent-duration .info-date--date")
    private WebElement previewRentDuration;

    @FindBy(xpath = "//*[@class='navbar-back' and @title='Back Arrow']")
    private WebElement backButton;

    @FindBy(css = ".swal2-buttonswrapper.swal2-loading [type*='button']:first-child")
    private WebElement loadingAnimation;

    @FindBy(xpath = "//*[.=\"Selanjutnya\"]")
    private WebElement continueButton;

    @FindBy(xpath = "//p[contains(@class,'bg-c-text bg-c-text--body-1')][normalize-space()='Ubah']")
    private List<WebElement> ubahButton;


    @FindBy(xpath = "//button[normalize-space()='Simpan']")
    private WebElement simpanButton;

    @FindBy(xpath = "(//p[contains(@class,'bg-c-text bg-c-text--body-1')][normalize-space()='Ubah'])[3]")
    private WebElement ubahPaymentMethodButton;

    @FindBy (css =".today")
    private WebElement datePickToday;

    @FindBy(xpath = "//p[contains(@class,'booking-success__title')]")
    private WebElement bookingSuccessMessage;

    @FindBy(xpath = "//button[contains(text(),'Lihat status pengajuan')]")
    private WebElement lihatStatusPengajuanButton;

    private By bookingTitle = RelativeLocator.with(By.xpath("//*[.='Booking']")).above(By.xpath("//*[@class='user-booking-tabs user-booking__tabs']"));

    private By bookingBackButtonBy = By.cssSelector("#bookingContainer .booking-breadcrumb__back-nav span");

    private By rentPeriodListSelected = By.cssSelector(".mami-radio-icon--selected + .rent-type .rent-type-desc--label");

//    private By rentDurationIndex = By.cssSelector("#bookingRadioDuration .rent-type-group:nth-child(%s) .rent-type-desc--label");

    private By rentDurationIndex = By.xpath("//*[@id='bookingRadioDuration']//*[@class='rent-type-group']//*[@class='rent-type-desc--label']");

//    private By rentDurationIndex = By.xpath("(//*[@id='bookingRadioDuration']//*[@class='rent-type-group']//*[@class='rent-type-desc--label'])["+ index +"]");

    private By arrowBackBookingForm = By.cssSelector("#bookingNavbar img.navbar-back");

    private By bookingCancelBookingPopUpBody = By.cssSelector("#bookingModalCancel div.modal-body");

    private By bookingConfirmationCancelTitleText = By.cssSelector("#bookingModalCancel div.confirmation-text > h4");

    private By stillSubmitButtonConfirmationCancel = By.cssSelector("#bookingModalCancel div.modal-footer.confirmation-button-group > button:first-child");

    private By durationFormSection = By.cssSelector("#bookingInputPriceDate .booking-input-price__radio");

    private By bookingSummary = By.cssSelector(".booking-summary-container #bookingSummary");

    private By saveDraftButton = By.cssSelector("#bookingModalCancel div.modal-footer.confirmation-button-group > button:last-child");

    private By rentDurationList = By.cssSelector("#bookingRadioDuration .rent-type-group");

    private By rentPeriodAdditionalInformationSelected = By.cssSelector("#bookingRadioDuration span.rent-type-desc--selected");

    private By selectedPrice = By.cssSelector(".mami-radio-icon--selected + .rent-type strong");

    private By infoKostPrice = By.xpath("//*[@id='bookingSummary']//*[contains(text(), 'Harga Sewa Per')]/following-sibling::*[1]");

    private By bookingSummaryPerPeriodPrice = By.xpath("//*[@id='bookingSummary']//*[contains(text(), 'Harga Sewa Per')]//following::p");

    private By perPeriodRentText = By.xpath("//*[@id='bookingSummary']//*[contains(text(), 'Harga Sewa Per')]");

    @FindBy(css = ".booking-steps-container + .booking-summary-container")
    private WebElement bookingForm;

    @FindBy(css= "#bookingBreadcrumb")
    private WebElement bookingBreadCrumb;

    //Booking page 2

    @FindBy(css = "#tenantGender .selected .mami-radio-label")
    private WebElement selectedRadioGender;

    @FindBy(xpath = "//*[@id=\"bookingTenantData\"]//*[@role=\"alert\"]//*[@class=\"mami-info__description\"]")
    private WebElement alertInformationRestrictedGender;

    @FindBy(xpath = "(//span[@class='bg-c-radio__icon'])[2]")
    private WebElement paymentWithFullPayment;

    @FindBy(xpath = "//p[@class='booking-confirmation__content-title bg-c-text bg-c-text--heading-6 ']")
    private WebElement informationPopup;

    @FindBy(css ="a.bg-c-link--medium:nth-child(1)")
    private WebElement tncBookingLink;

    @FindBy(xpath ="//h1[@data-testid='bookingTncModal-title']")
    private WebElement tncContentText;

    @FindBy(css="button.bg-c-button--primary:nth-child(1)")
    private WebElement okeButtonPopup;

    @FindBy(css=".booking-confirmation__tnc > p:nth-child(2) > span:nth-child(2) > a")
    private WebElement tncSinggahSiniLink;


    /**
     * Check if plus icon button is enable for 5 seconds
     * @return true if plus icon button enable otherwise false
     */
    public boolean isPlusIconButtonEnable() {
        return selenium.waitInCaseElementVisible(rentDurationIncreaseButton, 5) != null;
    }

    /**
     * Increase Rent Duration
     * @throws InterruptedException
     */
    public void increaseRateDuration() throws InterruptedException {
        if(isPlusIconButtonEnable()) {
            selenium.waitTillElementIsClickable(rentDurationIncreaseButton);
            selenium.hardWait(2);
            selenium.javascriptClickOn(rentDurationIncreaseButton);
        }
    }

    /**
     * Select Date
     * @param selectedDate date e.g. 15,20 etc
     * @throws InterruptedException
     */
    public void selectDate(String selectedDate) throws InterruptedException {
        selenium.javascriptClickOn(dateTextBox);
        if (selectedDate.equals("1")){
            selenium.clickOn(nextMonthIcon);
        }
        selenium.clickOn(By.xpath("//*[@class='booking-input-checkin-content booking-input-checkin__date']//*[contains(@class, 'selected')]/following-sibling::*[1]"));
        selenium.hardWait(3);
    }

    /**
     * Select Date
     * @param date date e.g. 15,20 etc
     * @throws InterruptedException
     */
    public void selectDateForToday(String date) throws InterruptedException {
        selenium.javascriptClickOn(dateTextBox);
        selenium.clickOn(By.xpath("//*[@class='date-wrapper']/span["+date+"]"));
    }

    /**
     * Click On Next Button
     * @throws InterruptedException
     */
    public void clickOnNextButton() throws InterruptedException {
        selenium.pageScrollInView(nextButton);
        selenium.javascriptClickOn(nextButton);
    }

    /**
     * Select payment period
     * @param paymentPeriod is text want to fill to text box
     * @throws InterruptedException
     */
    public void selectPaymentPeriod(String paymentPeriod) throws InterruptedException {
        selenium.pageScrollInView(ubahButton.get(1));
        selenium.javascriptClickOn(ubahButton.get(1));
        By selectDurationSewa = By.xpath("//p[normalize-space()='" + paymentPeriod + "']");
        selenium.javascriptClickOn(selectDurationSewa);
        selenium.javascriptClickOn(simpanButton);
    }

    public void chooseFullPayment () throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(paymentWithFullPayment);
    }

    /**
     * return selected rent period example "Per Bulan"
     * @return rent period of selected text
     */
    public String getSelectedPeriodText() {
        return selenium.getText(rentPeriodListSelected);
    }

    /**
     * Convert %s to index 1, 2, 3, of rentDurationIndex
     * @param index fill with integer data type 1-6
     * @return By.xpath after reformat
     */
    private By elementFormatRentDurationIndex(int index) {
        By element = By.xpath("(//*[@id='bookingRadioDuration']//*[@class='rent-type-group']//*[@class='rent-type-desc--label'])["+ index +"]");
        String rentDurationTarget = element.toString().replace("By.xpath: ", "");
        rentDurationTarget = String.format(rentDurationTarget, String.valueOf(index));
        return By.xpath(rentDurationTarget);
    }

    /**
     * Check if daily rent duration is present
     * @param index fill with integer data type 1-4
     * @return true if daily rent duration is present otherwise false
     */
    public boolean isDailyPresent(int index) {
        String rentDurationText = selenium.getText(elementFormatRentDurationIndex(index));
        return rentDurationText.equals("Per Hari");
    }

    /**
     * Click on arrow back button left side mamikos.com icon
     * @throws InterruptedException
     */
    public void clickOnArrowBackButton() throws InterruptedException{
        selenium.hardWait(1);
        selenium.clickOn(arrowBackBookingForm);
    }

    /**
     * @return True if element present false if element not present
     * @throws InterruptedException
     */
    public boolean isBookingConfirmationPopUpPresent() throws InterruptedException {
        selenium.waitTillElementIsVisible(bookingCancelBookingPopUpBody, 5);
        return selenium.isElementPresent(bookingCancelBookingPopUpBody);
    }

    /**
     * @return return text of booking cancel confirmation pop up "Yakin kamu ingin membatalkan pengajuan sewa?"
     */
    public String getTitleBookingCancelConfirmationText() {
        return selenium.getText(bookingConfirmationCancelTitleText);
    }

    /**
     * Click on Still Submit button or Tetap Ajukan button
     * @throws InterruptedException
     */
    public void clickOnStillSubmitButton() throws InterruptedException {
        selenium.hardWait(1);
        selenium.clickOn(stillSubmitButtonConfirmationCancel);
    }

    /**
     * get text of decide price and rent duration
     * @return text "Tentukan harga dan masa sewa yang Anda butuhkan"
     */
    public String getDecidePriceAndRentDurationText() {
        return selenium.getText(textDefinedPriceAndRentDuration);
    }

    /**
     *get text of choose base on rent duration
     * @return text "Pilih sesuai kebutuhan dan lamanya Anda akan menetap"
     */
    public String getChooseBaseOnRentDurationText() {
        return selenium.getText(textChooseBasedOnYourStayDuration);
    }

    /**
     * @return true if duration form present otherwise false
     * @throws InterruptedException
     */
    public boolean isDurationFormPresent() throws InterruptedException {
        return selenium.isElementPresent(durationFormSection);
    }

    /**
     * @return true if booking summary present otherwise false
     * @throws InterruptedException
     */
    public boolean isBookingSummaryPresent() throws InterruptedException {
        return selenium.isElementPresent(bookingSummary);
    }

    /**
     * Click on Yes terminate button or Ya, Batalkan button
     * @throws InterruptedException
     */
    public void clickOnSaveDraftButton() throws InterruptedException {
        selenium.clickOn(saveDraftButton);
    }

    /**
     * return number of rent duration list
     * @return
     */
    public int getListRentDurationNumber() {
        selenium.waitTillElementsCountIsMoreThan(rentDurationList, 0);
        int numberOfElements;
        List<WebElement> list = new ArrayList<>();
        list = driver.findElements(rentDurationList);
        numberOfElements = list.size();
        return numberOfElements;
    }

    /**
     * Get list price duration index text Per Bulan, Per Minggu, Per 6 Bulan, Per Tahun
     * @param index input with int value max number is 4
     * @return String Per Bulan, Per Minggu, Per 6 Bulan, Per Tahun
     */
    public String getListPriceRentDurationIndexText(int index) {
        By element = By.xpath("(//*[@id='bookingRadioDuration']//*[@class='rent-type-group']//*[@class='rent-type-desc--label'])["+ index +"]");
        return selenium.getText(element);
    }

    /**
     * Click on list Per Bulan Per Minggu etc
     * @param index input with int value
     * @throws InterruptedException
     */
    public void clickOnRentPriceListIndex(int index) throws InterruptedException{
        selenium.pageScrollInView(bookingBreadCrumb);
        selenium.clickOn(elementFormatRentDurationIndex(index));
    }

    /**
     * Get selected additional info text of selected rent duration
     * @return String Dibayar seminggu sekali Dibayar sebulan sekali, Dibayar 6 bulan sekali, Dibayr setahun sekali
     */
    public String getSelectedRentDurationAdditionalInformationText() {
        By element = By.cssSelector("#bookingRadioDuration span.rent-type-desc--selected");
        return selenium.getText(element);
    }

    /**
     * Get Rent Duration
     * @return String
     */
    public String getRentDuration(){
        return selenium.getText(rentDuration);
    }

    /**
     * Get preview rent duration example 1 Bulan
     * @return String
     */
    public String getPreviewRentDurationText() {
        return selenium.getText(previewRentDuration);
    }

    /**
     * Get full text price on booking confirmation page
     * @param elementTarget input with target element
     * @return String
     */
    public String getFullPriceText(String elementTarget) {
        String toReturn = null;
        switch (elementTarget) {
            case "selectedPriceText":
                toReturn = selenium.getText(selectedPrice);
                break;
            case "infoPriceText":
                toReturn = selenium.getText(infoKostPrice);
                break;
            case "infoPriceTextEach":
                toReturn = selenium.getText(bookingSummaryPerPeriodPrice);
                break;
            default:
                throw new IllegalArgumentException(
                        "Please input parameter with \"selectedPrice\", \"infoPriceText\", \"infoPriceTextEach\"");
        }
        return toReturn;
    }

    /**
     * Get booking preview rent duration text example Harga Sewa Per Bulan
     * Each is based on element attribute class
     * @return
     */
    public String getBookingPreviewRentDurationEachText() {
        return selenium.getText(perPeriodRentText);
    }

    /**
     * Return true if price duration contain word from selected rent count duration otherwise false
     * @param containWords input with Per Minggu, Per Bulan, Per 6 Bulan, Per Tahun etc
     * @return true or false
     */
    public boolean isDurationContainPreviewBookingPresent(String containWords) {
        return getBookingPreviewRentDurationEachText().contains(containWords) && getSelectedPeriodText().contains(containWords);
    }

    /**
     * Check element price index list is present
     * @param index input with int value max number is 4
     * @return true if element present otherwise false
     * @throws InterruptedException
     */
    public boolean isListPriceRentDurationIndexPresent(int index) throws InterruptedException {
        return selenium.isElementPresent(elementFormatRentDurationIndex(index));
    }

    /**
     * Get property hyperlink
     * @throws InterruptedException
     */
    public String getPropertyHyperlink() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.hardWait(3);
        String url = selenium.getURL();
        selenium.closeTabWindowBrowser();
        return url;
    }

    /**
     * Check if loading animation is present in booking form fill PO
     * @return true if loading animation appear
     * @throws InterruptedException
     */
    public boolean isLoadingAnimationAppear() {
        return  selenium.waitInCaseElementVisible(loadingAnimation, 5) != null;
    }

    /**
     * Click on Back button from booking page
     *
     * @throws InterruptedException
     */
    public void clickOnBackButtonOfBookingPage() throws InterruptedException {
        selenium.waitTillElementIsClickable(bookingBackButtonBy);
        selenium.clickOn(bookingBackButtonBy);
    }

    /**
     * Click on continue button / Selanjutnya button
     * @throws InterruptedException
     */
    public void clickOnContinueButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(continueButton);
        selenium.hardWait(1);
        selenium.clickOn(continueButton);
    }

    /**
     * Get selected gender text
     * @return String data type e.g "Laki-Laki"
     */
    public String getSelectedGenderText() {
        return selenium.getText(selectedRadioGender);
    }

    /**
     * Get alert information selected gender text
     * @return String data type kost name + "hanya boleh disewa" + gender or "bisa pasutri"
     */
    public String getAlertInformationRestrictedGenderText() {
        return selenium.getText(alertInformationRestrictedGender);
    }

    /**
     * Check if booking form element present
     * @return boolean data type
     * @throws InterruptedException
     */
    public boolean isInBookingForm() throws InterruptedException {
        return selenium.waitTillElementIsVisible(bookingForm) != null;
    }

    /**
     * Get per period price number with currency
     * @return string data type e.g Rp900.000
     */
    private String getBookingSummaryPerPeriodText() {
        return selenium.getText(bookingSummaryPerPeriodPrice);
    }

    /**
     * Get price number only without currency. From booking summary
     * @return integer data type e.g 6000000
     */
    public int getBookingSummaryPerPeriodPrice() {
        return Integer.parseInt(getBookingSummaryPerPeriodText().replace("Rp", "").replace(".", ""));
    }

    /**
     * Get selected per period price text
     * @return string data type e.g Rp 900.000
     */
    private String getSelectedPerPeriodPriceText() {
        return selenium.getText(selectedPrice);
    }

    public int getSelectedPerPeriodPrice() {
        return Integer.parseInt(getSelectedPerPeriodPriceText().replace("Rp ", "").replace(".", ""));
    }

    /**
     * Get date string on selected date box
     * @return string data type e.g "5 June 2021"
     */
    public String getSelectedDate() throws ParseException {
        String date = selenium.getElementAttributeValue(dateTextBox, "value");
        String pattern = "d-MMM-yyyy";
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }

    /**
     * Select type pembayaran pertama
     * @param typePembayaran is text want to fill to text box
     * @throws InterruptedException
     */
    public void selectTypePembayaranPertama(String typePembayaran) throws InterruptedException {
        By element = By.xpath("//div[@class='booking-form-radio__label']/p[.='"+typePembayaran+"']");
        selenium.hardWait(5);
        selenium.waitTillElementIsVisible(By.xpath("//div[@class='booking-form-radio__label']/p[.='"+typePembayaran+"']"), 15);
        selenium.pageScrollInView(element);
        selenium.hardWait(2);
        selenium.clickOn(element);
    }

    /**
     * Select payment method DP or Full Payment
     * @param paymentMethod
     * @throws InterruptedException
     */
    public void selectPaymentMethod(String paymentMethod) throws InterruptedException {
        selenium.pageScrollInView(ubahPaymentMethodButton);
        selenium.javascriptClickOn(ubahPaymentMethodButton);
        By selectPaymentMethod = By.xpath("//*[@class='booking-form-radio__label']//*[contains(text(), '"+paymentMethod+"')]");
        selenium.javascriptClickOn(selectPaymentMethod);
        selenium.hardWait(5);
        selenium.clickOn(simpanButton);
    }

    /**
     * Select checkin date on Ubah button on Pengajuan sewa for kost not implement DP
     * @throws InterruptedException
     */
    public void selectCheckinTime() throws InterruptedException {
        selenium.pageScrollInView(ubahPaymentMethodButton);
        selenium.javascriptClickOn(ubahPaymentMethodButton);
        selenium.clickOn(datePickToday);
        selenium.clickOn(simpanButton);
    }

    /**
     * Verify the Pengajuan sewa berhasil dikirim page
     * @return booking success message "Pengajuan Sewa Berhasil Dikirim"
     */
    public String getBookingSuccessMessage() {
        selenium.waitInCaseElementVisible(bookingSuccessMessage, 3);
        return selenium.getText(bookingSuccessMessage);
    }

    /**
     * Click on Lihat Status Pengajuan button
     * @throws InterruptedException
     */
    public void clickOnLihatStatusPengajuan() throws InterruptedException {
        selenium.waitTillElementIsClickable(lihatStatusPengajuanButton);
        selenium.clickOn(lihatStatusPengajuanButton);
        selenium.hardWait(3);
    }

    /**
     * Click On Ubah Metode Pembayaran Button
     * @throws InterruptedException
     */
    public void clickOnChangeMethodPayButton() throws InterruptedException {
        selenium.pageScrollInView(ubahButton.get(2));
        selenium.waitInCaseElementClickable(ubahButton.get(2),5);
        selenium.javascriptClickOn(ubahButton.get(2));
    }

    /**
     * Verify Booking title is displayed
     * @return boolean
     */
    public Boolean isBookingTitleDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementVisible(bookingTitle, 10)!=null;
    }

    /**
     * Verify information popup before submit booking
     * @return boolean
     */
    public Boolean isInformationBookingDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementVisible(informationPopup, 2)!=null;
    }

    /**
     * Click On tnc Booking on pengajuan booking page
     * @throws InterruptedException
     */
    public void clickOnTnCBookingReguler() throws InterruptedException {
        selenium.waitInCaseElementClickable(tncBookingLink,3);
        selenium.clickOn(tncBookingLink);
    }

    /**
     * Verify information on content
     *  @return String
     */
    public String tncContentText() {
        selenium.waitInCaseElementVisible(tncContentText, 7);
        return selenium.getText(tncContentText);
    }

    /**
     * Verify information on content
     *  @return String
     */
    public String tncBookingTextReguler() throws InterruptedException {
        selenium.waitInCaseElementVisible(tncBookingLink, 3);
        return selenium.getText(tncBookingLink);
    }

    /**
     * Click On OKPaham button on popup
     * @throws InterruptedException
     */
    public void clickOnOkPahamButton() throws InterruptedException {
        selenium.waitInCaseElementClickable(okeButtonPopup,3);
        selenium.clickOn(okeButtonPopup);
    }

    /**
     * Verify information on content
     *  @return String
     */
    public String tncBookingTextSinggahSini() throws InterruptedException {
        selenium.waitInCaseElementVisible(tncSinggahSiniLink, 3);
        return selenium.getText(tncSinggahSiniLink);
    }

    /**
     * Click On tnc Booking kost singgah sini on pengajuan booking page
     * @throws InterruptedException
     */
    public void clickOnTnCBookingSinggahSini() throws InterruptedException {
        selenium.waitInCaseElementClickable(tncSinggahSiniLink,2);
        selenium.clickOn(tncSinggahSiniLink);
    }

}
