package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BookingFormPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingFormPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    //booking form
    @FindBy(css = ".booking-request-form")
    private WebElement bookingForm;

    /**
     * Verify page have booking form
     * return boolean true if element exist
     */
    public boolean isBookingFormPresent() {
       return selenium.waitInCaseElementVisible(bookingForm, 3) != null;
    }

    /**
     * Verify page have booking form
     * return boolean true if element exist
     */
    public boolean isBookingFormFromChatPresent() {
        selenium.switchToWindow(2);
        return selenium.waitInCaseElementVisible(bookingForm, 3) != null;
    }
}
