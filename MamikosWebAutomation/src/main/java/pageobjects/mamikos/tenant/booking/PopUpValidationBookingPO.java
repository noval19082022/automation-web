package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PopUpValidationBookingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public PopUpValidationBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    // ---------- Job, Gender, karyawan, mahasiswa Pop Up Validation -----------

    @FindBy(xpath = "//h3[@class='bg-c-modal__body-title']")
    private WebElement titleValidation;

    @FindBy(xpath = "//p[@class='bg-c-modal__body-description']")
    private WebElement subtitleValidation;

    @FindBy(xpath = "//button[@class='bg-c-modal__action-closable']")
    private WebElement closeButtonValidation;

    @FindBy(xpath = "//p[@class='hidden-xs bg-c-text bg-c-text--heading-2 ']")
    private WebElement bookingFormTitle;

    /**
     * Get title on job, gender, karyawan, mahasiswa pop up validation
     */
    public String getTitleValidation(){
        return selenium.getText(titleValidation);
    }

    /**
     * Get subtitle on job, gender, karyawan, mahasiswa pop up validation
     */
    public String getSubtitleValidation(){
        return selenium.getText(subtitleValidation);
    }

    /**
     * Click close on job, gender, karyawan, mahasiswa pop up validation
     */
    public void clickClosePopUpValidation() throws InterruptedException {
        selenium.clickOn(closeButtonValidation);
    }

    /**
     * Get title on booking form page
     */
    public String getTitleBookingForm(){
        return selenium.getText(bookingFormTitle);
    }
}