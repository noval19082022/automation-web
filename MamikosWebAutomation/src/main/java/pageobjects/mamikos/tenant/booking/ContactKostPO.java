package pageobjects.mamikos.tenant.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ContactKostPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ContactKostPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".modal-chat .modal-chat__caption")
    private WebElement captionText;

    @FindBy(xpath = "//h3[@class='bg-c-modal__body-title']")
    private WebElement title;

    @FindBy(css = "div.question-option:nth-child(1) > label:nth-child(1) > p")
    private WebElement defaultSelectedRadio;

    @FindBy(css = "button.btn-modal-chat")
    private WebElement rentSubmitButton;

    /**
     * Get contact kost pop up title
     * @return string data type "Hubungi Kost"
     */
    public String getContactKostPopUpTitle() throws InterruptedException{
        selenium.hardWait(3);
        return selenium.getText(title);
    }


    /**
     * Get contact kost pop up caption
     * @return string data type "Anda akan terhubung dengan pemilik langsung melalui chatroom mamikos"
     */
    public String getContactKostPopUpCaption() {
        return selenium.getText(captionText);
    }

    /**
     * Get default selected questioner radio button text
     * @return string data type e.g "Saya butuh cepat nih. Bisa booking sekarang?"
     */
    public String getDefaultSelectedRadioQuestioner() {
        return selenium.getText(defaultSelectedRadio);
    }

    /**
     * Click on rent submit button/ Ajukan Sewa button
     * @throws InterruptedException
     */
    public void clickOnRentSubmitButton() throws InterruptedException {
        selenium.clickOn(rentSubmitButton);
    }
}