package pageobjects.mamikos.tenant.profile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class VerifikasiAkunPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public VerifikasiAkunPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//span[contains(.,'Verifikasi Akun')]")
    private WebElement verifikasiAkunMenu;

    @FindBy(xpath = "//button[contains(.,'Verifikasi')]")
    private WebElement verificationButton;

    @FindBy(xpath = "//input[@name='Email']")
    private WebElement inputEmailTextBox;

    @FindBy(xpath = "//p[@class='failed-alert']")
    private WebElement errorMessageEmail;

    @FindBy(xpath = "//button[.='Verifikasi']")
    private WebElement verificationButton2;

    @FindBy(id = "swal2-content")
    private WebElement errorMessageEmailRegistered;

    @FindBy(className = "swal2-confirm")
    private WebElement okErrorMessageButton;

    @FindBy(xpath = "//*[@data-testid='verificationPhoneNumber']")
    private WebElement ubahPhoneNumberButton;

    @FindBy(xpath = "//input[@name='Nomor Handphone']")
    private WebElement phoneNumberField;

    @FindBy(xpath = "//p[@class='failed-alert']")
    private WebElement errorMessagePhoneNumber;

    @FindBy(xpath = "//span[.='Ubah']")
    private WebElement ubahPhoneNumberButton2;

    @FindBy(css = ".notif-phone-verification")
    private WebElement otpVerificationMessageText;

    @FindBy(xpath = "//*[@data-testid='photoID']")
    private WebElement uploadIDCard;

    @FindBy(xpath = "//*[@data-testid='continueButton']")
    private WebElement continueButton;

    @FindBy(xpath = "//*[@alt='ic_camera_shoot.svg']")
    private WebElement cameraButton;

    @FindBy(xpath = "//button[contains(text(), \"Simpan\")]")
    private WebElement saveButton;

    @FindBy(xpath = "//span[contains(text(), \"Ambil ulang foto\")]")
    private WebElement retakePhotoText;

    @FindBy(className = "--photo")
    private WebElement uploadedPhoto;

    @FindBy(className = "photo-box")
    private WebElement photoBox;

    @FindBy(className = "send-back-otp")
    private WebElement sendBackOTPText;

    @FindBy(xpath = "(//span[@class='countdown'])[1]")
    private WebElement countdownCounterText;

    @FindBy(xpath = "//h1[normalize-space()='Verifikasi Identitas']")
    private WebElement popupConfirmationClosed;


    /**
     * Click on Verifikasi Akun Menu From Tenant Profile page
     *
     */
    public void clickVerifikasiAkunMenu() throws InterruptedException {
        selenium.pageScrollInView(verifikasiAkunMenu);
        selenium.hardWait(3);
        selenium.clickOn(verifikasiAkunMenu);
    }

    /**
     * Click on Verifikasi Button in Verifikasi Akun page
     *
     */
    public void clickVerifikasiButton() throws InterruptedException {
        selenium.waitInCaseElementClickable(verificationButton, 20);
        selenium.javascriptClickOn(verificationButton);
        selenium.hardWait(3);
    }

    /**
     * Fill out email field with value from feature
     *
     */
    public void fillOutEmailField(String email) throws InterruptedException {
        selenium.hardWait(5);
        selenium.clearTextField(inputEmailTextBox);
        selenium.enterText(inputEmailTextBox, email, true);
    }

    /**
     * get error message wrong email format
     *
     * @return
     */
    public String getErrorMessageEmail(){
        selenium.waitInCaseElementVisible(errorMessageEmail, 5);
        return selenium.getText(errorMessageEmail);
    }

    /**
     * Click on Verifikasi Button after fill email registered
     *
     */
    public void clickVerifikasiButton2() throws InterruptedException {
        selenium.hardWait(3);
        selenium.javascriptClickOn(verificationButton2);
        selenium.hardWait(3);
    }

    /**
     * get error message email registered
     *
     * @return
     */
    public String getErrorMessageEmailRegistered() {
        selenium.waitInCaseElementVisible(errorMessageEmailRegistered, 5);
        return selenium.getText(errorMessageEmailRegistered);
    }

    /**
     * Click at OK button on Pop Up Error Message
     *
     */
    public void clickOKErrorMessageButton() throws InterruptedException {
        selenium.clickOn(okErrorMessageButton);
    }

    /**
     * Click at Ubah Button in section phone number (before user edit field phone number)
     *
     */
    public void clickUbahPhoneNumberButton() throws InterruptedException {
        selenium.waitInCaseElementClickable(ubahPhoneNumberButton,5);
        selenium.javascriptClickOn(ubahPhoneNumberButton);
    }

    /**
     * Clear Phone Number Field
     *
     */
    public void emptyPhoneNumberField() {
        selenium.clearTextField(phoneNumberField);
    }

    /**
     * Get Error Message in Phone Number Section
     *
     */
    public String getErrorMessagePhoneNumber(){
        selenium.waitInCaseElementVisible(errorMessagePhoneNumber, 5);
        return selenium.getText(errorMessagePhoneNumber);
    }

    /**
     * Fill out phone number field with value from feature
     *
     */
    public void fillOutPhoneNumberField(String phoneNumber) throws InterruptedException {
        selenium.clearTextField(phoneNumberField);
        selenium.enterText(phoneNumberField, phoneNumber, true);
    }

    /**
     * Click at Ubah Button in section phone number (after user edit field phone number)
     *
     */
    public void clickUbahPhoneNumberButton2() throws InterruptedException {
        selenium.hardWait(3);
        selenium.javascriptClickOn(ubahPhoneNumberButton2);
    }

    /**
     * Get OTP Verification Message Text
     * @return string
     */
    public String getOTPVerificationMessage() {
        selenium.waitInCaseElementVisible(otpVerificationMessageText,10);
        return selenium.getText(otpVerificationMessageText);
    }

    /**
     * Click on Upload Identity Card
     * @throws InterruptedException
     */
    public void clickOnUploadIDCard() throws InterruptedException {
        selenium.pageScrollInView(uploadIDCard);
        selenium.clickOn(uploadIDCard);
    }

    /**
     * Click Continue on Pop up
     * @throws InterruptedException
     */
    public void clickContinueOnPopUp() throws InterruptedException {
        selenium.clickOn(continueButton);
        selenium.hardWait(5);
    }

    /**
     * Click on Camera Button
     * @throws InterruptedException
     */
    public void clickOnCameraButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(cameraButton, 5);
        selenium.clickOn(continueButton);
        selenium.clickOn(cameraButton);
    }

    /**
     * Click on Save Photo Button
     * @throws InterruptedException
     */
    public void clickOnSavePhotoButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(saveButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click on uploaded photo field
     * @throws InterruptedException
     */
    public void clickOnUploadedPhoto() throws InterruptedException {
        selenium.waitInCaseElementVisible(uploadedPhoto, 5);
        selenium.hardWait(5);
        selenium.clickOn(uploadedPhoto);
    }

    /**
     * Is photo box appeared?
     * @return true or false
     */
    public Boolean isPhotoBoxAppear(){
        return selenium.waitInCaseElementVisible(photoBox, 5) != null;
    }

    /**
     * Click on Send Back OTP Text
     * @throws InterruptedException
     */
    public void clickOnSendBackOTPText() throws InterruptedException {
        selenium.waitInCaseElementVisible(sendBackOTPText, 200);
        selenium.clickOn(sendBackOTPText);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click on Send Back OTP Text
     * @throws InterruptedException
     */
    public void clickOnIdentityType(String type) throws InterruptedException {
            WebElement element = driver.findElement(By.xpath("//div[@role='radiobutton'][normalize-space()='"+type+"']"));
            selenium.hardWait(5);
            selenium.clickOn(element);
    }

    /**
     * Click on Ya button
     * @throws InterruptedException
     */
    public void clickOnYesButton(String Yes) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//*[contains(text(), '"+Yes+"')]"));
        selenium.clickOn(element);
    }

    /**
     * Popup closed
     * @return true or false
     */
    public Boolean popupConfirmationClosed(){
        return selenium.waitInCaseElementVisible(popupConfirmationClosed, 5) != null;
    }

    /**
     * Is Countdown Counter Text Appear
     * @return true or false
     */
    public Boolean isCountdownCounterTextAppear(){
        return selenium.waitInCaseElementVisible(countdownCounterText, 5) != null;
    }

    /**
     * Get Countdown Counter Text
     * @return string
     */
    public String getCountdownCounterText() throws InterruptedException {
        selenium.waitInCaseElementVisible(countdownCounterText, 50);
        return selenium.getText(countdownCounterText);
    }

    /**
     * Clear Email Input Field
     */
    public void emptyEmailField(){
        selenium.clearTextField(inputEmailTextBox);
    }
}
