package pageobjects.mamikos.tenant.profile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class DraftBookingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public DraftBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    private By draftBookingSectionId = By.id("userBookingSection");

    private By kostNameAndLocation = By.cssSelector("#draftBookingWrapper .draft-booking-list .draft-main .info-title > span");

    @FindBy(css = "#draftBookingWrapper .draft-booking-list .draft-main .info-title > span")
    private WebElement absenceKostNameAndLocation;

    @FindBy(css = "#detailKostContainer")
    private WebElement redirectKostDetail;

    @FindBy(xpath = "//*[.='LIHAT']")
    private WebElement seeButton;

    @FindBy(css = ".toasted.booking-toasted-default.default")
    private WebElement toastMessageSaveDraft;

    @FindBy(css = "#draftBookingWrapper button.btn.btn-primary")
    private WebElement continueBookingDraftPage;

    @FindBy(css = "#bookingInputPriceDate .booking-input-price__title h2")
    private WebElement FormBookingHargaDanMasaSewa;

    @FindBy(css = "#draftBookingWrapper div.info-detail > span")
    private WebElement seeDetailKostButton;

    @FindBy(css = "#detailKostContainer")
    private WebElement kosDetailNewTab;

    @FindBy(css = "#draftBookingWrapper div.draft-footer > button.btn.btn-default")
    private WebElement deleteButtonDraft;

    @FindBy(css = "#draftBookingModalDelete div.modal-title > span")
    private WebElement modalTitleDeleteDraftText;

    @FindBy(css = "#draftBookingModalDelete div.modal-caption")
    private WebElement modalCaptionDeleteDraftText;

    @FindBy(css = "#draftBookingModalDelete div.modal-button-close > a > span")
    private WebElement closeButtonModalDeleteDraft;

    @FindBy(css = "#draftBookingModalDelete button.btn.btn-success")
    private WebElement batalkanButtonModalDeleteDraft;

    @FindBy(css = "#draftBookingModalDelete div.modal-actions > button.btn.btn-primary")
    private WebElement hapusDraftButtonModalDeleteDraft;

    @FindBy(xpath="//a[@class='tab-item-link'][contains(., 'Draft')]")
    private WebElement draftMenuButton;

    /**
     * Check if in draft booking section
     * @return true if is in draft booking section otherwise false
     * @throws InterruptedException
     */
    public boolean isInBookingSection() throws InterruptedException {
        return selenium.isElementPresent(draftBookingSectionId);
    }

    public String getKostNameAndLocation() {
        return selenium.getText(kostNameAndLocation);
    }

    /**
     * check booking is not present on draft by kostName and Location, after click delete draft
     * @return
     * @throws InterruptedException
     */
    public boolean isKostNameAndLocationAbsence() throws InterruptedException {
        return selenium.isElementPresent(kostNameAndLocation) != null;
    }

    /**
     * redirect to kost detail after save draft
     * @return
     */
    public Boolean isSeeKostDetail(){
        return selenium.waitTillElementIsVisible(redirectKostDetail, 5) != null;
    }

    /**
     * toast draft is clickable
     */
    public Boolean isSeeButtonClickable(){
        return selenium.waitTillElementIsClickable(seeButton) != null;
    }

    /**
     * get text toast draft
     */
    public String getSeeButtonText() throws InterruptedException {
        selenium.waitTillElementIsVisible(toastMessageSaveDraft, 5);
        return selenium.getText(toastMessageSaveDraft);
    }

    /**
     * click continue booking on draft page
     */
    public void continueBookingDraftPage() throws InterruptedException {
        selenium.clickOn(continueBookingDraftPage);
    }

    /**
     * get form booking harga dan masa sewa
     */
    public String getFormBookingMasaSewa(){
        return selenium.getText(FormBookingHargaDanMasaSewa);
    }

    /**
     * click see detail kost from draft booking page
     */
    public void seeDetailKostButtonDraftBooking() throws InterruptedException {
        selenium.clickOn(seeDetailKostButton);
    }

    /**
     * see detail kost from draft booking page and auto open new tab
     * @return
     */
    public boolean seeDetailKostOpenNewTab() throws InterruptedException {
        selenium.switchToWindow(2);
        return selenium.waitTillElementIsVisible(kosDetailNewTab) != null;
    }

    /**
     * click on delete button draft booking page and close new tab then switch to tab browser 1
     */
    public void deleteButtonDraftBooking() throws InterruptedException {
        selenium.closeTabWindowBrowser();
        selenium.switchToWindow(1);
        selenium.clickOn(deleteButtonDraft);
    }

    /**
     * get title text on delete pop up draft booking
     */
    public String getTitleModalDeletDraft(){
        return selenium.getText(modalTitleDeleteDraftText);
    }

    /**
     * get caption text on delete pop up draft booking
     */
    public String getCaptionModalDeleteDraft(){
        return selenium.getText(modalCaptionDeleteDraftText);
    }

    /**
     * click close (X) on delete pop up draft booking
     */
    public void closeModalDeleteDraft() throws InterruptedException {
        selenium.clickOn(closeButtonModalDeleteDraft);
    }

    /**
     * click delete button on draft booking on tab browser one
     */
    public void deleteButtonDraft() throws InterruptedException {
        selenium.clickOn(deleteButtonDraft);
    }

    /**
     * click batalkan on pop up delete draft booking
     */
    public void batalkanModalDeleteDraft() throws InterruptedException {
        selenium.clickOn(batalkanButtonModalDeleteDraft);
    }

    /**
     * click delete draft booking
     */
    public void hapusDraftModalDeleteDraft() throws InterruptedException {
        selenium.clickOn(hapusDraftButtonModalDeleteDraft);
    }

    /**
     * click draft menu
     * @throws InterruptedException
     */
    public void clickDraftMenuButton() throws InterruptedException {
        selenium.clickOn(draftMenuButton);
    }

    /**
     * delete all drafts if any
     * @throws InterruptedException
     */
    public void deleteAllDraftsIfAny() throws InterruptedException {
        selenium.hardWait(2);
        int i;
        for (i=0; i<=10; i++){
            selenium.hardWait(2);
            if (isDeleteDraftBtnVisible()){
                if (!isLanjutBookingDraftBtnVisible()){
                    break;
                }
                if (isLanjutBookingDraftBtnVisible()){
                    selenium.hardWait(2);
                    selenium.clickOn(deleteButtonDraft);
                    selenium.hardWait(2);
                    selenium.clickOn(hapusDraftButtonModalDeleteDraft);
                    selenium.refreshPage();
                }
            } else {
                break;
            }
        }
    }

    /**
     * if delete draft button is available
     * @return visible true otherwise false
     */
    private boolean isDeleteDraftBtnVisible(){
        return selenium.waitInCaseElementVisible(deleteButtonDraft, 3) != null;
    }

    /**
     * check if the Lanjut booking button in draft is visble
     * @return true means it's visible, false means it isn't visible
     */
    private boolean isLanjutBookingDraftBtnVisible(){
        return selenium.waitInCaseElementVisible(continueBookingDraftPage, 3) != null;
    }
}