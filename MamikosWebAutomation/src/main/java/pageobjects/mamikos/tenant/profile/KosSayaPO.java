package pageobjects.mamikos.tenant.profile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.ArrayList;
import java.util.List;

public class KosSayaPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public KosSayaPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//*[contains(@class, 'menu-profil')]//*[contains(text(), 'Kos Saya')]")
    private WebElement btnKosSaya;

    @FindBy(xpath = "//*[contains(text(), 'Bayar')][1]")
    private WebElement btnPayFirstBills;

    @FindBy(xpath = "//*[.='Tagihan kos']")
    private WebElement btnBills;

    // Mau coba dong homepage
    @FindBy(css = "#home > section.booking-shortcut-main.container > div")
    private WebElement mauCobaDong;

    @FindBy(xpath = "//ul[@class='nav-main-ul']/child::*[6]")
    private WebElement profilHome;

    @FindBy(xpath = "//a[@href='/user']")
    private WebElement profil;

    @FindBy(xpath = "//div[@class='user-kost-main']/child::h2")
    private WebElement kostSayaPage;

    @FindBy(xpath = "//div[@class='profil col-xs-6']/child::h1")
    private WebElement tenantName;

    @FindBy(xpath = "//a[@href='/user/kost-saya/kost-info']")
    private WebElement lihatInformasiKos;

    @FindBy(xpath = "//div[@class='user-kost-page-header']/child::p")
    private WebElement informasiKosTitle;

    @FindBy(xpath = "//div[@class='user-kost-page-header']/child::button")
    private WebElement backButton;

    @FindBy(xpath = "//div[@class='user-review-card']")
    private WebElement reviewCard;

    @FindBy(xpath = "//div[@class='user-kost-activities']/child::p")
    private WebElement aktivitasKosSaya;

    @FindBy(xpath = "//div[@class='user-kost-activities__menu-wrapper']/child::*[1]")
    private WebElement tagihanKos;

    @FindBy(xpath = "//div[@class='user-kost-activities__menu-wrapper']/child::*[2]")
    private WebElement kontrak;

    @FindBy(xpath = "//div[@class='user-kost-activities__menu-wrapper']/child::*[3]")
    private WebElement chatPemilik;

    @FindBy(xpath = "//div[@class='user-kost-activities__menu-wrapper']/child::*[4]")
    private WebElement bantuan;

    @FindBy(xpath = "//div[@class='user-kost-activities__menu-wrapper']/child::*[5]")
    private WebElement forum;

    //-----------Kost saya section on Homepage---------------//
    @FindBy(xpath = "//p[contains(.,'Kamu belum menyewa kos')]")
    private WebElement emptyKosSayaSection;

    @FindBy(xpath = "//p[contains(.,'Pengajuan sewa lagi dicek pemilik')]")
    private WebElement waitingConfirmationTitle;

    @FindBy(xpath = "//p[contains(.,'Batas akhir konfirmasi dari pemilik kos')]")
    private WebElement confirmationTimeTitle;

    @FindBy(css = ".bg-c-button")
    private WebElement chatPemilikButton;

    //-------------Lihat Informasi Kos----------------//
    @FindBy(xpath = "//div[@class='user-kost-info']/child::*[2]")
    private WebElement kostTypeInfoKos;

    @FindBy(xpath = "//div[@class='user-kost-info']/child::*[3]")
    private WebElement kostNameInfoKos;

    @FindBy(xpath = "//div[@class='user-kost-info']/child::*[5]")
    private WebElement kostAddressTitle;

    @FindBy(xpath = "//div[@class='user-kost-info']/child::*[6]")
    private WebElement kostAddressInfoKos;

    @FindBy(xpath = "//div[@class='user-kost-info']/child::*[8]")
    private WebElement peraturanUmum;

    @FindBy(xpath = "//div[@class='user-kost-facilities']/child::*[7]")
    private WebElement lihatSemuaFasilitasBtn;

    @FindBy(xpath = "//div[@class='user-kost-facilities']/child::*[5]")
    private WebElement fasilitasKamar;

    @FindBy(xpath = "//div[@class='user-kost-facilities']/child::*[6]")
    private WebElement fasilitasKmrMandi;

    @FindBy(xpath = "//div[@class='user-kost-facilities']/child::*[7]")
    private WebElement fasilitasUmum;

    @FindBy(xpath = "//div[@class='user-kost-facilities']/child::*[8]")
    private WebElement fasilitasParkir;

    //------------Kontrak section-------------//
    @FindBy(xpath = "//div[@class='user-kost-page-header']/child::p")
    private WebElement kontrakTitle;

    @FindBy(xpath = "//div[@class='user-kost-brief-info__info-wrapper']/child::*[1]")
    private WebElement kostType;

    @FindBy(xpath = "//div[@class='user-kost-brief-info__info-wrapper']/child::*[2]")
    private WebElement kostName;

    @FindBy(xpath = "//div[@class='user-kost-brief-info__info-wrapper']/child::*[3]")
    private WebElement kostAddress;

    @FindBy(xpath = "//div[@class='user-kost-brief-info__info-wrapper']/child::*[4]")
    private WebElement roomNumber;

    @FindBy(xpath = "//div[@class='user-kost-contract']/child::*[4]")
    private WebElement totalBiaya;

    @FindBy(xpath = "//div[@class='user-kost-contract']/child::*[5]")
    private WebElement totalBiayaPrice;

    @FindBy(xpath = "//div[@class='user-kost-contract']/child::*[6]")
    private WebElement rincianBiaya;

    @FindBy(xpath = "//div[@class='bg-c-list-item user-kost-contract__list-item']/child::*[1][contains(., 'Harga sewa')]")
    private WebElement hargaSewaWrd;

    @FindBy(xpath = "(//div[@class='bg-c-list-item user-kost-contract__list-item']/child::*[2])[1]")
    private WebElement hargaSewaPrice;

    @FindBy(xpath = "//div[@class='bg-c-list-item user-kost-contract__list-item']/child::*[1][contains(., 'listrik')]")
    private WebElement biayaLainListrik;

    @FindBy(xpath = "//div[@class='bg-c-list-item user-kost-contract__list-item']/child::*[2][contains(., '10.000')]")
    private WebElement listrikPrice;

    @FindBy(xpath = "//div[@class='bg-c-list-item user-kost-contract__list-item']/child::*[1][contains(., 'sampah')]")
    private WebElement biayaLainSampah;

    @FindBy(xpath = "//div[@class='bg-c-list-item user-kost-contract__list-item']/child::*[2][contains(., '5.000')]")
    private WebElement sampahPrice;

    @FindBy(xpath = "//div[@class='user-kost-contract']/child::*[8]")
    private WebElement tglPenagihantitle;

    @FindBy(xpath = "//div[@class='user-kost-contract']/child::*[9]")
    private WebElement tglPenagihanSubTitle;

    @FindBy(xpath = "(//div[@class='user-kost-contract__list'])[2]/child::*[1]/child::*[1]")
    private WebElement mulaiSewa;

    @FindBy(xpath = "(//div[@class='user-kost-contract__list'])[2]/child::*[1]/child::*[2]")
    private WebElement durasiSewa;

    @FindBy(xpath = "(//div[@class='user-kost-contract__list'])[2]/child::*[2]")
    private WebElement sewaBerakhir;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--title-4 '][contains(., 'Aturan Denda')]")
    private WebElement aturanDendaTitle;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--body-2 '][contains(., 'Denda diberlakukan')]")
    private WebElement aturanDendaPrice;

    @FindBy(css = ".bg-c-card--clickable")
    private WebElement ajukanHentiBtn;

    //--------------kost saya homepage for draft-------//
    @FindBy(css ="#home > section.booking-shortcut-main.container > div > p")
    private WebElement draftTitle;

    @FindBy(css = "div.bg-c-list-item:nth-child(1) > div:nth-child(1) > div")
    private WebElement checkinDateTitle;

    @FindBy(css ="div.bg-c-list-item:nth-child(2) > div:nth-child(1) > div")
    private WebElement durationTitle;

    @FindBy(css = ".semua-promo-slide")
    private WebElement allPromobutton;

    @FindBy(css = "p.booking-info-rejected__space")
    private WebElement rejectReasonTitle;

    @FindBy(xpath = "(//div[@class='stop-rent__section'])[1]/child::*[1]")
    private WebElement hentiSewaPage;

    @FindBy(xpath = "(//button[@class='close'])[1]")
    private WebElement closeBtn;

    @FindBy(xpath = "//div[@class='booking-shortcut__flash-sale']/child::div")
    private WebElement flashSaleLabel;

    //----------------kost saya rejected----------------//
    @FindBy(css = "a.bg-c-link:nth-child(2)")
    private WebElement lihatPengajuanLainButton;

    //-------------Tenant Input Unique Code--------------//
    @FindBy(xpath = "//button[@type='button'][contains(., 'Masukkan kode dari pemilik')]")
    private WebElement inputUniqueCodeBtn;

    @FindBy(xpath = "//p[contains(., 'Masukkan kode unik dari pemilik')]")
    private WebElement titleInputUniqueCode;

    @FindBy(xpath = "//div[@class='input-unique-code__wrapper']/child::p[2]")
    private WebElement subtitleInputUniqueCode;

    @FindBy(xpath = "//div[@class='unique-code-input__wrapper']")
    private WebElement fieldUniqueCode;

    @FindBy(xpath = "//input[@type='text']")
    private WebElement uniqueCodeTxt;

    @FindBy(xpath = "//button[@type='button'][contains(., 'Kirim kode unik')]")
    private WebElement krmKodeBtn;

    @FindBy(xpath = "//p[contains(., 'Verifikasi nomor HP')]")
    private WebElement titleVerification;

    @FindBy(xpath = "(//span[@class='bg-c-text bg-c-text--body-3 '])[1]")
    private WebElement tenantPhoneNum1;

    @FindBy(xpath = "(//span[@class='bg-c-text bg-c-text--body-3 '])[2]")
    private WebElement tenantPhoneNum2;

    @FindBy(xpath = "//div[@class='bg-c-alert__content']/p")
    private WebElement alertVerification;

    @FindBy(xpath = "//button[@type='button'][contains(., 'Kirim OTP ke 08********320')]")
    private WebElement sendOtpBtn;

    @FindBy(xpath = "//p[contains(., 'Pilih Metode Verifikasi')]")
    private WebElement titleVerifMethod;

    @FindBy(xpath = "//div[@class='otp-method-selector']/p[2]")
    private WebElement subtitleVerifMethod;

    @FindBy(xpath = "(//p[@class='bg-c-text bg-c-text--body-4 '])[1]")
    private WebElement viaSmsTxt;

    @FindBy(xpath = "//div[@class='otp-method-selector']/div[1]")
    private WebElement viaSmsBtn;

    @FindBy(xpath = "//p[contains(., 'Verifikasi nomor HP')]")
    private WebElement titleOtpSms;

    private By backBtn = By.xpath("(//button[@type='button'])[1]");

    @FindBy(xpath = "//h3[contains(., 'Yakin batalkan proses verifikasi?')]")
    private WebElement titlePopUpBtlVerif;

    @FindBy(xpath = "//button[contains(., 'Ya, batalkan')]")
    private WebElement yaBtn;

    //--------forum--------//
    @FindBy(xpath = "//p[contains(., 'Forum')]")
    private WebElement forumButton;

    @FindBy(xpath = "//h3[contains(., 'Fitur ini sedang kami kembangkan')]")
    private WebElement popupForumTitle;

    @FindBy(xpath ="//p[contains(., 'Saya tertarik.')]")
    private WebElement informationNotTickTitle;

    @FindBy(xpath ="//span[@class='bg-c-checkbox__icon']")
    private WebElement checkboxPopUpForum;

    @FindBy(xpath="//p[contains(., 'Kami akan memberitahu Anda saat fitur ini sudah tersedia.')]")
    private WebElement informationAfterTickCheckboxForum;

    @FindBy(xpath= "//button[contains(., 'Oke')]")
    private WebElement okForumButton;

    //-----------check full payment------------//
    @FindBy(xpath = "//button[contains(., 'Check-in di sini')]")
    private WebElement checkinBtnKosSaya;

    @FindBy(xpath = "//button[contains(., 'Check-in Kos')]")
    private WebElement checkinBtnKosDetail;

    @FindBy(xpath = "//div[@class='bg-c-modal__footer']//button")
    private WebElement popUpCheckin;

    @FindBy(xpath = "(//button[contains(., 'Selesai & ke Kos Saya')])[1]")
    private WebElement selesaiCheckinBtn;

    @FindBy(xpath = "(//div[@class='bg-c-list-item user-kost-contract__list-item']/child::*[1])[2]")
    private WebElement biayaLainTxt1;

    @FindBy(xpath = "(//div[@class='bg-c-list-item user-kost-contract__list-item']/child::*[2])[2]")
    private WebElement biayaLainPrice1;

    //-----------kost saya homepage confirm booking------//
    @FindBy(xpath = "//p[contains(., 'Batas akhir pembayaran')]")
    private WebElement paymentDeadlineText;

    //-----------Chat list Title------------//
    @FindBy(css ="p.bg-c-text--heading-4")
    private WebElement chatListTitle;

    //------------bantuan--------//
    @FindBy(xpath ="//p[contains(., 'Check-in')]")
    private WebElement checkinText;

    @FindBy(xpath ="//p[contains(., 'Pembayaran dan tagihan')]")
    private WebElement paidAndBillText;

    @FindBy(xpath ="//p[contains(., 'Kontrak sewa')]")
    private WebElement leaseContractText;

    @FindBy(xpath="//a[contains(., 'Mengapa saya harus check-in')]")
    private WebElement whyCheckin;

    @FindBy(xpath="//a[contains(., 'Mengapa saya harus check-in')]")
    private WebElement question1Title;

    @FindBy(xpath = "//a[contains(., 'Bagaimana cara membatalkan check-in')]")
    private WebElement question2Title;

    @FindBy(xpath = "//a[contains(., 'Apakah saya harus membayar uang sewa')]")
    private WebElement question3Title;

    @FindBy(xpath="//a[contains(., 'Bagaimana cara melihat tagihan')]")
    private WebElement question4Title;

    @FindBy(xpath ="//a[contains(., 'Bagaimana cara check-out')]")
    private WebElement question5Title;

    @FindBy(xpath="//a[contains(., 'Kontrak saya akan segera berakhir')]")
    private WebElement question6Title;

    @FindBy(xpath ="//p[contains(., 'Masih butuh bantuan')]")
    private WebElement stillNeedHelpTitle;

    @FindBy(xpath ="//a[contains(., 'Hubungi CS')]")
    private WebElement contactCs;

    @FindBy(xpath = "//button[contains(., 'Bayar di sini')]")
    private WebElement btnBayarDiSini;

    @FindBy(xpath = "//button[normalize-space()='Sudah Dibayar']")
    private WebElement btnAlreadyPaid;

    @FindBy(xpath = "//button[normalize-space()='Belum Dibayar']")
    private WebElement btnBeforePaid;

    /**
     * click on kost saya tenant profile page
     * entry to kost saya to manage bills from tenant side
     * @throws InterruptedException
     */
    public void clickOnKosSayaButton() throws InterruptedException {
        selenium.clickOn(btnKosSaya);
    }

    /**
     * Click on pay button on first list bills
     * @throws InterruptedException
     */
    public void clickOnPayButtonFirstBills() throws InterruptedException {
        selenium.hardWait(5);
        if (selenium.isElementNotDisplayed(btnPayFirstBills)) {
            selenium.clickOn(btnPayFirstBills);
        }else {
            selenium.clickOn(btnAlreadyPaid);
            selenium.clickOn(btnBeforePaid);
            selenium.clickOn(btnPayFirstBills);
        }
        selenium.hardWait(5);
        ArrayList<String> tabs = new ArrayList<>(selenium.getWindowHandles());
        selenium.switchToWindow(tabs.size());
    }

    /**
     * click on bills tab
     * @throws InterruptedException
     */
    public void clickOnBillsTab() throws InterruptedException {
        selenium.javascriptClickOn(btnBills);
    }

    /**
     * click on Mau Coba Dong section at homepage
     * @throws InterruptedException
     */
    public void clickMauCobaDong() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.waitInCaseElementVisible(mauCobaDong, 4);
        selenium.refreshPage();
        selenium.hardWait(2);
        //this element for click Mau Coba Dong section
        WebElement element = driver.findElement(By.xpath("//div[@class='booking-shortcut-empty__button-text']"));
        selenium.waitInCaseElementClickable(element, 5);
        selenium.javascriptClickOn(element);
        selenium.hardWait(4);
    }

    /**
     * click on Profil
     * @throws InterruptedException
     */
    public void clickProfil() throws InterruptedException {
        selenium.clickOn(profilHome);
        selenium.clickOn(profil);
    }

    /**
     * get title kos saya page
     * @return title kos saya
     */
    public String getTitleKosSaya(){
        selenium.pageScrollUsingCoordinate(0, -250);
        return selenium.getText(kostSayaPage);
    }

    /**
     * get welcome title
     * @return welcome title
     */
    public String getWelcomeTitle(){
        String name = selenium.getText(tenantName);
        WebElement element = driver.findElement(By.xpath("//h1[@class='bg-c-text bg-c-text--heading-4 '][contains(., 'Hai, " + name + "!')]"));
        return selenium.getText(element);
    }

    /**
     * click hyperlink Lihat informasi kos
     * @throws InterruptedException
     */
    public void clickLihatInformasiKos() throws InterruptedException {
        selenium.clickOn(lihatInformasiKos);
    }

    /**
     * get title of informasi kos page
     * @return title of informasi kos
     */
    public String getInformasiKosTitle(){
        selenium.pageScrollUsingCoordinate(0, -250);
        return selenium.getText(informasiKosTitle);
    }

    /**
     * click back on informasi kos page
     */
    public void clickBackInformasiKos() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, -250);
        selenium.clickOn(backButton);
    }

    /**
     * get review card section
     * @return review card
     */
    public String getReviewCard(){
        return selenium.getText(reviewCard);
    }

    /**
     * get aktivitas kos saya section
     * @return aktivitas kos saya
     */
    public String getAktivitasKosSaya(){
        return selenium.getText(aktivitasKosSaya);
    }

    /**
     * get tagihan kos
     * @return tagihan kos
     */
    public String getTagihanKos(){
        return selenium.getText(tagihanKos);
    }

    /**
     * get kontrak
     * @return kontrak
     */
    public String getKontrak(){
        return selenium.getText(kontrak);
    }

    /**
     * get chat pemilik
     * @return chat pemilik
     */
    public String getChatPemilik(){
        return selenium.getText(chatPemilik);
    }

    /**
     * get bantuan
     * @return bantuan
     */
    public String getBantuan(){
        return selenium.getText(bantuan);
    }

    /**
     * get forum
     * @return forum
     */
    public String getForum(){
        return selenium.getText(forum);
    }

    /**
     * get empty kost saya
     * @return ex Kamu belum menyewa kos
     */
    public String getEmptyState() {
        return selenium.getText(emptyKosSayaSection);
    }

    /**
     * click on Chat pemilik kos at homepage
     * @throws InterruptedException
     */
    public void checkWaitingSection() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.waitInCaseElementVisible(waitingConfirmationTitle, 4);
        selenium.getText(waitingConfirmationTitle);
        selenium.waitTillElementIsVisible(confirmationTimeTitle, 4);
        selenium.getText(confirmationTimeTitle);
        //this element for click Lanjutkan button
        WebElement element = driver.findElement(By.cssSelector(".bg-c-button"));
        selenium.javascriptClickOn(element);
        selenium.hardWait(4);
    }

    /**
     * get title booking waiting confirmation
     * @return ex Pengajuan sewa lagi dicek pemilik
     */
    public String getKostSayaHomepageTitle() throws InterruptedException{
        selenium.refreshPage();
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.hardWait(5);
        return selenium.getText(waitingConfirmationTitle);
    }

    /**
     * get kost type at lihat informasi kos page
     * @return kost type at lihat informasi kos page
     */
    public String getKostTypeInfoKos() throws InterruptedException{
        selenium.hardWait(2);
        return selenium.getText(kostTypeInfoKos);
    }

    /**
     * get kost name at lihat informasi kos page
     * @return kost name at lihat informasi kos page
     */
    public String getKostNameInfoKos(){
        return selenium.getText(kostNameInfoKos);
    }
    /**
     * get title draft booking
     * @return ex Mau lanjut ajukan sewa di kos ini?
     */
    public String getDraftBookingTitle() throws InterruptedException {
        selenium.refreshPage();
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.pageScrollInView(draftTitle);
        selenium.waitInCaseElementVisible(draftTitle, 10);
        return selenium.getText(draftTitle);
    }

    /**
     * click on Ajukan sewa button
     * @throws InterruptedException
     */
    public void clickAjukanSewa() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.waitInCaseElementVisible(checkinDateTitle, 4);
        selenium.getText(checkinDateTitle);
        selenium.waitTillElementIsVisible(durationTitle, 4);
        selenium.getText(durationTitle);
        //this element for click Chat Pemilik section
        WebElement element = driver.findElement(By.cssSelector(".bg-c-button"));
        selenium.javascriptClickOn(element);
        selenium.hardWait(4);
    }

    /**
     * get kost address title at lihat informasi kos page
     * @return kost address title at lihat informasi kos page
     */
    public String getKostAddressTitle(){
        return selenium.getText(kostAddressTitle);
    }

    /**
     * get kost address at lihat informasi kos page
     * @return kost address at lihat informasi kos page
     */
    public String getKostAddressInfoKos(){
        return selenium.getText(kostAddressInfoKos);
    }

    /**
     * get Peraturan umum at lihat informasi kos page
     * @return Peraturan umum at lihat informasi kos page
     */
    public String getPeraturanUmum(){
        return selenium.getText(peraturanUmum);
    }

    /**
     * click on Lihat semua fasilitas button at Lihat informasi kos page
     * @throws InterruptedException
     */
    public void clickLihatSemuaFas() throws InterruptedException {
//        selenium.clickOn(lihatSemuaFasilitasBtn);
        selenium.javascriptClickOn(lihatSemuaFasilitasBtn);
    }

    /**
     * get Fasilitas kamar at lihat informasi kos page
     * @return Fasilitas kamar at lihat informasi kos page
     */
    public String getFasilitasKamar(){
        return selenium.getText(fasilitasKamar);
    }

    /**
     * get Fasilitas kamar mandi at lihat informasi kos page
     * @return Fasilitas kamar mandi at lihat informasi kos page
     */
    public String getFasilitasKmrMandi(){
        return selenium.getText(fasilitasKmrMandi);
    }


    /**
     * click kontrak section
     * @throws InterruptedException
     */
    public void clickKontrak() throws InterruptedException {
//        selenium.clickOn(kontrak);
        selenium.javascriptClickOn(kontrak);
    }

    /**
     * get title kontrak
     * @return title kontrak
     */
    public String getTitleKontrak(){
        return selenium.getText(kontrakTitle);
    }

    /**
     * get kost type
     * @return kost type
     */
    public String getKostType(){
        return selenium.getText(kostType);
    }

    /**
     * get kost name
     * @return kost name
     */
    public String getKostName(){
        return selenium.getText(kostName);
    }

    /**
     * get kost address
     * @return kost address
     */
    public String getKostAddress(){
        return selenium.getText(kostAddress);
    }

    /**
     * get room number
     * @return room number
     */
    public String getRoomNumber(){
        return selenium.getText(roomNumber);
    }

    /**
     * get total biaya
     * @return total biaya
     */
    public String getTotalBiaya(){
        return selenium.getText(totalBiaya);
    }

    /**
     * get total biaya price
     * @return total biaya price
     */
    public String getTotalBiayaPrice(){
        return selenium.getText(totalBiayaPrice);
    }

    /**
     * get rincian biaya
     * @return rincian biaya
     */
    public String getRincianBiaya(){
        return selenium.getText(rincianBiaya);
    }

    /**
     * get harga sewa wording
     * @return harga sewa wording
     */
    public String getHargaSewa(){
        return selenium.getText(hargaSewaWrd);
    }

    /**
     * get harga sewa price
     * @return harga sewa price
     */
    public String getHargaSewaPrice(){
        return selenium.getText(hargaSewaPrice);
    }

    /**
     * get biaya lain wording ke 1
     * @return biaya lain wording ke 1
     */
    public String getBiayaLainListrik(){
        return selenium.getText(biayaLainListrik);
    }

    /**
     * get biaya lain listrik price
     * @return biaya lain listrik price
     */
    public String getListrikPrice(){
        return selenium.getText(listrikPrice);
    }

    /**
     * get biaya lain wording ke 2
     * @return biaya lain wording ke 2
     */
    public String getBiayaLainSampah(){
        return selenium.getText(biayaLainSampah);
    }

    /**
     * get biaya lain sampah price
     * @return biaya lain sampah price
     */
    public String getSampahPrice(){
        return selenium.getText(sampahPrice);
    }

    /**
     * get tanggal penagihan title
     * @return tanggal penagihan title
     */
    public String getTglPenagihanTitle(){
        return selenium.getText(tglPenagihantitle);
    }

    /**
     * get tanggal penagihan subtitle
     * @return tanggal penagihan subtitle
     */
    public String getTglPenagihanSubTitle(){
        return selenium.getText(tglPenagihanSubTitle);
    }

    /**
     * get mulai sewa
     * @return mulai sewa
     */
    public String getMulaiSewa(){
        return selenium.getText(mulaiSewa);
    }

    /**
     * get durasi sewa
     * @return durasi sewa
     */
    public String getDurasiSewa(){
        return selenium.getText(durasiSewa);
    }

    /**
     * get sewa berakhir
     * @return sewa berakhir
     */
    public String getSewaBerakhir(){
        return selenium.getText(sewaBerakhir);
    }

    /**
     * get aturan denda title
     * @return aturan denda title
     */
    public String getAturanDendaTitle(){
        return selenium.getText(aturanDendaTitle);
    }

    /**
     * get aturan denda price
     * @return aturan denda price
     */
    public String getAturanDendaPrice(){
        return selenium.getText(aturanDendaPrice);
    }

    /**
     * click ajukan berhenti sewa button
     * @throws InterruptedException
     */
    public void clickAjukanHentiBtn() throws InterruptedException {
        selenium.javascriptClickOn(ajukanHentiBtn);
        selenium.hardWait(2);
    }

    /**
     * get ajukan berhenti sewa page
     * @return ajukan berhenti sewa page
     */
    public String getAjukanHentiPage(){
        return selenium.getText(hentiSewaPage);
    }

    /**
     * click close button at ajukan berhenti sewa page
     * @throws InterruptedException
     */
    public void clickCloseBtn() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(closeBtn);
    }
    /**
     * get Fasilitas umum at lihat informasi kos page
     * @return Fasilitas umum at lihat informasi kos page
     */
    public String getFasilitasUmum(){
        return selenium.getText(fasilitasUmum);
    }

    /**
     * get Fasilitas parkir at lihat informasi kos page
     * @return Fasilitas parkir at lihat informasi kos page
     */
    public String getFasilitasParkir(){
        return selenium.getText(fasilitasParkir);
    }

    /**
     * get Flash sale label
     * @return Promo ngebut at draft booking section
     */
    public String getFlashSaleLabel() throws InterruptedException{
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.pageScrollInView(draftTitle);
        selenium.waitInCaseElementVisible(flashSaleLabel, 4);
        return selenium.getText(flashSaleLabel);
    }

    /**
     * get reject reason title on homepage
     * @return Alasan pengajuan sewa ditolak
     */
    public String getRejectReasonTitle() {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.waitTillElementIsVisible(rejectReasonTitle, 6);
        return selenium.getText(rejectReasonTitle);
    }

    /**
     * click Cari kos lain button
     * @throws InterruptedException
     */
    public void clickCariKosLainButton() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 500);
        WebElement element = driver.findElement(By.cssSelector(".bg-c-button"));
        selenium.javascriptClickOn(element);
    }

    /**
     * get Lihat pengajuan sewa lain text
     * @return Lihat pengajuan sewa lainnya
     */
    public String getSeeOtherBookingText() {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.waitTillElementIsVisible(lihatPengajuanLainButton, 4);
        return selenium.getText(lihatPengajuanLainButton);
    }

    /**
     * click Lihat pengajuan sewa button
     * @throws InterruptedException
     */
    public void clickLihatPengajuanSewaButton() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.javascriptClickOn(lihatPengajuanLainButton);
    }

    //-------------Tenant Input Unique Code--------------//
    /**
     * click Masukkan kode dari pemilik kos button
     */
    public void clickInputCode() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 1500);
        selenium.waitTillElementIsClickable(inputUniqueCodeBtn);
        selenium.clickOn(inputUniqueCodeBtn);
    }

    /**
     * Get title Masukkan kode unik dari pemilik
     * @return Masukkan kode unik dari pemilik
     */
    public String getTitleInputUniqueCode(){
        return selenium.getText(titleInputUniqueCode);
    }

    /**
     * Get subtitle Masukkan kode unik dari pemilik
     * @return subtitle Masukkan kode unik dari pemilik
     */
    public String getSubtitleInputUniqueCode(){
        return selenium.getText(subtitleInputUniqueCode);
    }

    /**
     * user input valid unique code at unique code field
     * one by one using split character
     * @throws InterruptedException
     */
    public void fieldUniqueCode(String uniqueCode) throws InterruptedException {
        selenium.waitInCaseElementVisible(fieldUniqueCode, 2);
        selenium.enterText(uniqueCodeTxt, uniqueCode, true);
        selenium.waitTillElementIsClickable(krmKodeBtn);
        selenium.clickOn(krmKodeBtn);
    }

    /**
     * Get title Verification Phone number page
     * @return title Verification Phone number page
     */
    public String getTitleVerification(){
        return selenium.getText(titleVerification);
    }

    /**
     * Get tenant's phone number at owner
     * @return tenant's phone number at owner
     */
    public String getTenantPhoneNum1(){
        return selenium.getText(tenantPhoneNum1);
    }

    /**
     * Get tenant's phone number at kos saya
     * @return tenant's phone number at kos saya
     */
    public String getTenantPhoneNum2(){
        return selenium.getText(tenantPhoneNum2);
    }

    /**
     * Get verification alert
     * @return verification alert
     */
    public String getAlertVerif(){
        return selenium.getText(alertVerification);
    }

    /**
     * Get wording of OTP button
     * @return wording of OTP button
     */
    public String getOtpWording(){
        return selenium.getText(sendOtpBtn);
    }

    /**
     * Click OTP button
     * @throws InterruptedException
     */
    public void clickOtpBtn() throws InterruptedException {
        selenium.clickOn(sendOtpBtn);
    }

    /**
     * get title Verification method
     * @return title Verification method
     */
    public String getTitleVerifMethod(){
        return selenium.getText(titleVerifMethod);
    }

    /**
     * get subtitle Verification method
     * @return subtitle Verification method
     */
    public String getSubtitleVerifMethod(){
        return selenium.getText(subtitleVerifMethod);
    }

    /**
     * get copy of verification via SMS
     * @return copy of verification via SMS
     */
    public String getViaSms(){
        return selenium.getText(viaSmsTxt);
    }

    /**
     * click verification via SMS
     * @throws InterruptedException
     */
    public void clickViaSms() throws InterruptedException {
        selenium.clickOn(viaSmsBtn);
    }

    /**
     * get title OTP via SMS
     * @return title OTP via SMS
     */
    public String getTitleOtpSms(){
        return selenium.getText(titleOtpSms);
    }

    /**
     * click back at OTP SMS page
     * @throws InterruptedException
     */
    public void clickBack() throws InterruptedException {
        selenium.clickOn(backBtn);
    }

    /**
     * get title of pop up cancel verification
     * @return title of pop up cancel verification
     */
    public String getTitlePopUpBtlVerif(){
        selenium.waitInCaseElementVisible(titlePopUpBtlVerif, 2);
        return selenium.getText(titlePopUpBtlVerif);
    }

    /**
     * click Ya, batalkan at pop up cancel verification
     * @throws InterruptedException
     */
    public void clickYaBtl() throws InterruptedException {
        selenium.clickOn(yaBtn);
    }

    /**
     * Click back button until first page is present
     * @throws InterruptedException
     */
    public void clickBackKosSaya() throws InterruptedException {
        int i;
        for (i=0; i<3; i++){
            selenium.pageScrollUsingCoordinate(0, -1000);
            selenium.waitTillElementIsClickable(backBtn);
            selenium.clickOn(backBtn);
        }
    }

    /**
     * click Forum button
     * @throws InterruptedException
     */
    public void clickForumButton() throws InterruptedException {
        selenium.javascriptClickOn(forumButton);
    }

    /**
     * get title of pop up feature developed
     * @return title of pop up feature developed
     */
    public String getPopupForumTitle() throws InterruptedException {
        return selenium.getText(popupForumTitle);
    }

    /**
     * get subtitle of pop up feature developed not tick
     * @return subtitle of pop up feature developed not tick
     */
    public String getInformationNotTickTitle() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(informationNotTickTitle);
    }

    /**
     * click checkbox on feature developed popup
     * @throws InterruptedException
     */
    public void clickCheckboxForum() throws InterruptedException {
        selenium.clickOn(checkboxPopUpForum);
    }

    /**
     * get title of pop up feature developed
     * @return title of pop up cancel verification
     */
    public String getInformationAfterTickCheckboxForum() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(informationAfterTickCheckboxForum);
    }

    /**
     * click oke button
     * @throws InterruptedException
     */
    public void clickOKButtonOnForum() throws InterruptedException {
        selenium.clickOn(okForumButton);
    }

    //-----------check full payment------------//
    /**
     * click check in button at kos saya page
     * @throws InterruptedException
     */
    public void clickCheckinKosSaya() throws InterruptedException {
        selenium.javascriptClickOn(checkinBtnKosSaya);
        selenium.javascriptClickOn(checkinBtnKosDetail);
        selenium.waitTillElementIsClickable(popUpCheckin);
        selenium.clickOn(popUpCheckin);
        selenium.waitTillElementIsClickable(selesaiCheckinBtn);
        selenium.clickOn(selesaiCheckinBtn);
    }

    /**
     * get biaya lain wording ke 1
     * @return biaya lain wording ke 1
     */
    public String getBiayaLain1(){
        return selenium.getText(biayaLainTxt1);
    }

    /**
     * get biaya lain 1 listrik price
     * @return biaya lain listrik price
     */
    public String getBiayaLainPrice1(){
        return selenium.getText(biayaLainPrice1);
    }

    //---------------- butuh pembayaran homepage----------//
    /**
     * click Bayar disini
     * @throws InterruptedException
     */
    public void clickBayarDisini() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.waitTillElementIsVisible(draftTitle, 4);
        selenium.getText(draftTitle);
        //this element for click Bayar disini
        WebElement element = driver.findElement(By.cssSelector(".bg-c-button"));
        selenium.javascriptClickOn(element);
        selenium.hardWait(4);
    }

   //--------------Chat Pemilik---------//
    /**
     * click chat pemilik menu
     * @throws InterruptedException
     */
    public void clickChatPemilikMenu() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(chatPemilik);
    }

    /**
     * get Chat list Title
     * @return Chat
     */
    public String getChatListTitle() throws InterruptedException{
        selenium.hardWait(4);
        return selenium.getText(chatListTitle);
    }

    //--------bantuan-----//
    /**
     * click Bantuan menu
     * @throws InterruptedException
     */
    public void clickBantuanMenu() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(bantuan);
    }

    /**
     * get subtitle on bantuan page
     * @return checkin
     */
    public String getCheckinSubtitle() {
        return selenium.getText(checkinText);
    }

    /**
     * get subtitle on bantuan page
     * @return Pembayaran and Tagihan
     */
    public String getPaidAndBillingSubtitle() {
        return selenium.getText(paidAndBillText);
    }

    /**
     * get subtitle on bantuan page
     * @return Kontrak sewa
     */
    public String getContractSubtitle() throws InterruptedException{
        selenium.hardWait(2);
        return selenium.getText(leaseContractText);
    }

    /**
     * click question1
     * @throws InterruptedException
     */
    public void clickOnQuestion1() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(question1Title);
    }

    /**
     * click question2
     * @throws InterruptedException
     */
    public void clickOnQuestion2() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(question2Title);
    }

    /**
     * click question3
     * @throws InterruptedException
     */
    public void clickOnQuestion3() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(question3Title);
        selenium.hardWait(5);
    }

    /**
     * click question4
     * @throws InterruptedException
     */
    public void clickOnQuestion4() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(question4Title);
    }

    /**
     * click question5
     * @throws InterruptedException
     */
    public void clickOnQuestion5() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(question5Title);
    }

    /**
     * click question6
     * @throws InterruptedException
     */
    public void clickOnQuestion6() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(question6Title);
    }

    /**
     * click Contact Cs
     * @throws InterruptedException
     */
    public void clickContactCsButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(stillNeedHelpTitle,3);
        selenium.waitTillElementIsVisible(contactCs, 2);
        selenium.javascriptClickOn(contactCs);
    }

    /**
     * Clicks button Bayar Di Sini
     * will appear on kos saya for finishing DP payment
     * @throws InterruptedException
     */
    public void clickOnButtonBayarDiSini() throws InterruptedException {
        selenium.click(btnBayarDiSini);
    }

    /**
     * if the waiting confirmation on homepage is visible
     * @return true means waiting confirmation is visible, false is otherwise
     */
    public boolean isWaitingConfirmationHomepagePresent() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.hardWait(5);
        return selenium.waitInCaseElementVisible(waitingConfirmationTitle, 3) != null;
    }

    /**
     * if the draft booking on homepage is visible
     * @return true means waiting confirmation is visible, false is otherwise
     */
    public boolean isDraftBookingHomepagePresent() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.hardWait(5);
        return selenium.waitInCaseElementVisible(draftTitle, 3) != null;
    }

    /**
     * Click Bayar button on Kos Saya
     * @throws InterruptedException
     */
    public void clickBayarButtonOnKosSaya() throws InterruptedException {
        selenium.waitInCaseElementVisible(btnPayFirstBills,3);
        selenium.click(btnPayFirstBills);
    }
}
