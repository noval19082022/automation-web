package pageobjects.mamikos.tenant.profile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import static java.awt.SystemColor.text;

public class TenantProfilePO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public TenantProfilePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(., 'Riwayat dan Draft Booking')]")
    private WebElement bookingMenu;

    @FindBy(xpath = "//*[@data-testid='editProfileButton']")
    private WebElement editProfileButton;

    @FindBy(xpath = "//*[@data-testid='myKostButton']")
    private WebElement myKostMenu;

    @FindBy(css = "a[href='/user/pengaturan']")
    private WebElement settingMenu;

    @FindBy(xpath = "//div[@class='user-profile-dropdown__trigger']")
    private WebElement tenantProfilePicture;

    @FindBy(css = "div .user-profile-dropdown__item:first-child")
    private WebElement tenantProfile;

    /**
     * Click on Booking Menu From Tenant Profile page
     *
     * @throws InterruptedException
     */
    public void clickOnBookingMenu() throws InterruptedException {
        selenium.clickOn(tenantProfilePicture);
        selenium.clickOn(tenantProfile);
        selenium.hardWait(4);
        selenium.waitInCaseElementVisible(bookingMenu,5);
        selenium.clickOn(bookingMenu);
    }

    /**
     * Click on Edit Profil Button
     * @throws InterruptedException
     */
    public void clickEditProfilButton() throws InterruptedException {
        selenium.waitInCaseElementClickable(editProfileButton,10);
        selenium.hardWait(3);
        selenium.clickOn(editProfileButton);
    }

    /**
     * Click on Kos Saya From Tenant Profile page
     *
     */
    public void clickOnMyKostMenu() {
        selenium.javascriptClickOn(myKostMenu);
    }

    /**
     * Is Setting Menu Appear
     * @return true or false
     */
    public Boolean isSettingMenuAppear(){
        return selenium.waitInCaseElementVisible(settingMenu, 5) != null;
    }
}
