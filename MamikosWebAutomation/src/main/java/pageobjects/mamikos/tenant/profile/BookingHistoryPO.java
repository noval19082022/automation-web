package pageobjects.mamikos.tenant.profile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.ArrayList;

public class BookingHistoryPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingHistoryPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".card-header label")
    private WebElement status;

    @FindBy(className = "kost-title")
    private WebElement roomName;

    @FindBy(xpath = "//*[text()='Durasi Sewa']/parent::div/following-sibling::label")
    private WebElement duration;

    @FindBy(xpath = "//div[@class='card-footer --need-payment']//span[text()='Sisa waktu pembayaran']")
    private WebElement paymentExpiryTime;

    @FindBy(id = "userBookingSection")
    private WebElement userBookingSection;

    @FindBy(xpath = "//*[@class='--waiting' and contains(text(), 'Tunggu Konfirmasi')]")
    private WebElement needConfirmationText;

    @FindBy(xpath = "//*[contains(text(), 'Tunggu Konfirmasi')]/parent::*/following-sibling::*[contains(@class, 'hidden-xs')]")
    private WebElement seeCompleteWithNeedConfirmationText;

    @FindBy(xpath= "//*[@class='detail-cancel']//*[@type='button']")
    private WebElement cancelBookingButton;

    @FindBy(xpath= "//*[@id='bookingModalCancel' and @style]//*[contains(text(), 'Ya, Batalkan')]")
    private WebElement yesCancelBookingButton;

    @FindBy(xpath = "//*[@id='bookingContractModalCancel' and @style]//*[contains(text(), 'berhasil')]")
    private WebElement cancelActiveBookingSuccessPopup;

    @FindBy(xpath = "//*[@id='bookingContractModalCancel' and @style]//*[contains(text(), 'Ok')]")
    private WebElement OkButtonBookingSuccessPopup;

    @FindBy(xpath = "//*[@href]//*[contains(text(), 'Booking')]")
    private WebElement bookingSidebarButton;

    @FindBy(css = ".booking-list-card:nth-child(1) .card-header label")
    private WebElement textBookingStatusFirstList;

    @FindBy(css = ".booking-list-card:nth-child(1) .card-toggle.hidden-xs")
    private WebElement toggleSeeCompleteFirstList;

    @FindBy(css = ".booking-list-card:nth-child(1) .--is-detail .header-reject-reason span")
    private WebElement textRejectReasonFirstList;

    @FindBy(xpath = "(//*[.='Tunggu Konfirmasi']/preceding::*/following-sibling::div//*[@class='kost-title'])[1]")
    private WebElement needConfirmationKostName;

    @FindBy(xpath = "(//*[.='Tunggu Konfirmasi']/preceding::*/following-sibling::div//*[@class='--room-lighter'])[1]")
    private WebElement needConfirmationRoomStatus;

    @FindBy(xpath = "(//*[.='Tunggu Konfirmasi']/preceding::*/following-sibling::div//*[@class='date-value'])[1]")
    private WebElement needConfirmationCheckinDate;

    @FindBy(xpath = "(//*[.='Tunggu Konfirmasi']/preceding::*/following-sibling::div//*[@class='date-item'][2]//*[@class='date-value'])[1]")
    private WebElement needConfirmationRentDuration;

    @FindBy(xpath = "(//*[contains(text(), 'Tunggu Konfirmasi')]/parent::*[@class='card-header']/following-sibling::*[@class='card-footer']//button)[1]")
    private WebElement needConfirmationOwnerChatButton;

    @FindBy(xpath = "(//*[contains(text(), 'Tunggu Konfirmasi')]/parent::*[@class='card-header']/following-sibling::div/*[@class='text-primary'])[1]")
    private WebElement needConfirmationSeeCompleteButton;

    @FindBy(css = ".--is-detail .detail-price-wrapper span")
    private WebElement txtExpandedBookingPriceDuration;

    @FindBy(css = ".--is-detail .detail-price-wrapper span b")
    private WebElement txtExpandedBookingPriceNumber;

    @FindBy(xpath = "//*[contains(@class, '--is-detail')]//*[.='Lihat Fasilitas']//b")
    private WebElement btnExpandedSeeFacilities;

    @FindBy(css = ".modal.fade.in#bookingModalFacilities .modal-body")
    private WebElement boxFacilities;

    @FindBy(css = ".modal.fade.in#bookingModalFacilities .btn-close-filter span")
    private WebElement buttonXClosePopUp;

    @FindBy(xpath = "//*[contains(@class, '--is-detail')]//*[contains(text(), 'lihat informasi terbaru')]")
    private WebElement btnExpandedSeeNewestInfo;

    @FindBy(css = ".--is-detail .detail-collapse span")
    private WebElement btnSeeLess;

    @FindBy(css = "div.--is-detail")
    private WebElement boxExpandedBookingDetails;

    @FindBy(css = ".--is-detail .tenant-item .--name")
    private WebElement txtExpandedRenterName;

    @FindBy(css = ".--is-detail .tenant-item:last-child b")
    private WebElement txtExpandedPhoneNumber;

    @FindBy(css = "div.chat-board")
    private WebElement boxChat;

    @FindBy(css=".btn.ic-close")
    private WebElement btnBackArrowChatBox;

    @FindBy(css = ".chat-section .product-link-name")
    private WebElement txtKostNameChatBox;

    @FindBy(xpath = "//label[contains(., 'Tunggu Konfirmasi')]")
    private WebElement tungguKnfrmasiStatus;

    @FindBy(css =".filter-item-mobile:first-child span")
    private WebElement buttonFilter;

    @FindBy(css = "li:nth-child(2) button")
    private WebElement listNeedConfirmation;

    @FindBy(css = ".booking-list-card:nth-child(1) .hidden-xs .text-primary")
    private WebElement buttonSeeCompleteIndexOne;

    @FindBy(css = ".--is-detail .detail-cancel button")
    private WebElement buttonCancelBookingIsDetail;

    @FindBy(css = ".fade.in .booking-cancel-container button.btn-primary")
    private WebElement buttonOkAfterCancel;

    @FindBy(css = "button [href='#basic-close']")
    private WebElement buttonCloseBookingCancel;

    @FindBy(css = ".fade.in .form-options")
    private WebElement boxReasonOptions;

    @FindBy(css=".notif-header div")
    private WebElement txtNotifNumber;

    @FindBy(css = ".notif-header")
    private WebElement buttonNotification;

    @FindBy(xpath = "(//div[@class='notif-text'])[1]/child::*[2]")
    private WebElement txtFirstTitleNotif;

    @FindBy(xpath = "//div[@class='col-xs-12 notif-center-head']/child::*[2]")
    private WebElement buttonXCloseNotification;

    @FindBy(xpath = "(//*[contains(text(), 'Bayar Sekarang')])[2]")
    private WebElement buttonNeedPayment;

    @FindBy(css = ".fade.in button.btn.--is-custom-btn")
    private WebElement btnCustom;

    @FindBy(css = ".checkin-confirmation-modal__wrapper button:last-child")
    private WebElement modalBtnCustom;

    @FindBy(css = "button[type='button'].bg-c-button--primary:nth-child(1)")
    private WebElement modalBtnCustomHidden;

    @FindBy(xpath = "//*[@href='#basic-close']")
    private WebElement btnClosePopupCancelBooking;

    @FindBy(xpath = "(//div[@class='booking-cancel-container'])[2]")
    private WebElement popUpCancelBooking;

    @FindBy(xpath = "(//a[@href='/'])[1]")
    private WebElement mamikosBtn;

    @FindBy(xpath = "//button[contains(., 'Lihat Rekomendasi Kos')]")
    private WebElement lihatRekomenBtn;

    @FindBy(xpath = "//button[@class='bg-c-modal__action-closable']")
    private WebElement closeLhtRekomenBtn;

    /**
     * Get booking Status
     *
     * @return Status Value
     */
    public String getStatusValue() {
        return selenium.getText(status);
    }

    /**
     * Get Room Name from Booking History
     *
     * @return Room Name
     */
    public String getRoomName() {
        return selenium.getText(roomName);
    }

    /**
     * Get Rent Duration from Booking History
     *
     * @return Duration Value
     */
    public String getDuration() {
        return selenium.getText(duration);
    }

    /**
     * GEt Payment Expiry Time from Booking History
     *
     * @return Payment  Expiry Text
     */
    public String getPaymentExpiryTimeText() {
        return selenium.getText(paymentExpiryTime);
    }

    /**
     * Check if history booking section id is visible in the viewport
     * @return true if booking section id is visible
     */
    public boolean isInHistoryBookingSection() {
        return selenium.waitInCaseElementVisible(userBookingSection, 2) != null;
    }

    /**
     * Wait until section booking page is visible
     * @throws InterruptedException
     */
    public void waitUntilHistoryBookingSectionVisible() throws InterruptedException {
        do {
            selenium.hardWait(1);
        }
        while (!isInHistoryBookingSection());
    }

    /**
     * Check if need confirmation text is present
     * @return true if need confirmation status present
     */
    public boolean isNeedConfirmationStatusPresent() {
        return selenium.waitInCaseElementVisible(needConfirmationText, 3) != null;
    }

    /**
     * Click on see complete/Lihat selengkapnya that contain need confirmation status and proceed to cancel active booking
     * @throws InterruptedException
     */
    public void cancelActiveBooking() throws InterruptedException {
        if(isNeedConfirmationStatusPresent()) {
            selenium.clickOn(seeCompleteWithNeedConfirmationText);
            selenium.waitTillElementIsClickable(cancelBookingButton);
            selenium.hardWait(2);
            selenium.javascriptClickOn(cancelBookingButton);
            selenium.waitTillElementIsClickable(yesCancelBookingButton);
            selenium.hardWait(2);
            selenium.clickOn(yesCancelBookingButton);
            selenium.waitInCaseElementVisible(cancelActiveBookingSuccessPopup, 60);
            selenium.hardWait(2);
            selenium.clickOn(OkButtonBookingSuccessPopup);
        }
    }

    /**
     * Click on sidebar booking to navigate to History Booking section
     * @throws InterruptedException
     */
    public void clickOnBookingSidebarButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(bookingSidebarButton);
        selenium.hardWait(1);
        selenium.clickOn(bookingSidebarButton);
    }

    /**
     * Get booking status on first list
     * @return string data type e.g "Pemilik Menolak"
     */
    public String getBookingStatusHeaderFirstKostList() {
        return selenium.getText(textBookingStatusFirstList);
    }

    /**
     * Get reject reason on first list kost
     * @return string data type e.g "Kamar tidak tersedia untuk penyewa"
     * @throws InterruptedException
     */
    public String getRejectReasonOnDetailsFirstKostList() throws InterruptedException {
        selenium.clickOn(toggleSeeCompleteFirstList);
        return selenium.getText(textRejectReasonFirstList);
    }

    /**
     * Get need confirmation kost name
     * @return String data type e.g "Ancient Fuelweaver Automation"
     */
    public String getRoomNameNeedConfirmation() {
        return selenium.getText(needConfirmationKostName);
    }

    /**
     * Check if button see complete is visible
     * @return true if visible otherwise false
     */
    private boolean isSeeCompleteListIndexNumberOnePresent() {
        return selenium.waitInCaseElementVisible(buttonSeeCompleteIndexOne, 5) != null;
    }

    /**
     * Is wait for confirmation button is visible
     * @return visible true otherwise false
     */
    private boolean isWaitForConfirmationBookingVisible() {
        return selenium.waitInCaseElementVisible(tungguKnfrmasiStatus, 3) != null;
    }

    /**
     * Cancel all need confirmation button. Max is 10 iteration for running purpose
     * @throws InterruptedException
     */
    public void cancelAllBookingWithDefaultReason() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(2);
        for(int i = 0; i <= 10; i++) {
            selenium.waitInCaseElementClickable(buttonFilter, 10);
            selenium.javascriptClickOn(buttonFilter);
            selenium.hardWait(2);
            selenium.javascriptClickOn(listNeedConfirmation);
            selenium.hardWait(2);
            if (isWaitForConfirmationBookingVisible()) {
                if(!isSeeCompleteListIndexNumberOnePresent()){
                    break;
                }
                if (isSeeCompleteListIndexNumberOnePresent()) {
                    selenium.hardWait(2);
                    selenium.javascriptClickOn(buttonSeeCompleteIndexOne);
                    selenium.hardWait(2);
                    selenium.javascriptClickOn(buttonCancelBookingIsDetail);
                    selenium.hardWait(5);
                    selenium.waitTillElementIsVisible(boxReasonOptions, 20);
                    selenium.hardWait(5);
                    selenium.javascriptClickOn(yesCancelBookingButton);
                    if (isLihatRekomendasiIsVisible()){
                        selenium.hardWait(3);
                        selenium.javascriptClickOn(closeLhtRekomenBtn);
                        selenium.refreshPage();
                        break;
                    } else if (!isLihatRekomendasiIsVisible()){
                        selenium.waitTillElementIsVisible(popUpCancelBooking, 25);
                        selenium.refreshPage();
                    }
                }
            } else {
                break;
            }
        }
    }

    /**
     * if the lihat rekomendasi button after cancel booking is visible
     * @return true means the lihat rekomendasi button is visible, false means otherwise
     */
    public boolean isLihatRekomendasiIsVisible() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.waitInCaseElementVisible(lihatRekomenBtn, 25) != null;
    }

    /**
     * Text notif number get as integer
     * @return integer data type
     */
    public int getNotificationNumber() {
        try {
            return Integer.parseInt(selenium.getText(txtNotifNumber));
        }
        catch (Exception e) {
            return 0;
        }
    }

    /**
     * click notification center
     * @throws
     */
    public void clickNotifCenter() {
        selenium.javascriptClickOn(buttonNotification);
    }

    /**
     * Get notification title text in first list of notification
     * @return text string data type
     */
    public String getFirstNotificationText() {
        return selenium.getText(txtFirstTitleNotif);
    }

    /**
     * Close notification
     */
    public void closeNotification() {
        selenium.javascriptClickOn(buttonXCloseNotification);
    }

    /**
     * Get room status in first index
     * @return string data type e.g "Kamar Belum Dikonfirmasi"
     */
    public String getRoomStatusNeedConfirmation() {
        return selenium.getText(needConfirmationRoomStatus);
    }

    /**
     * Get checkin date in first index need confirmation
     * @return string data type e.g "17 Jun 2021"
     */
    public String getCheckinDateNeedConfirmation() {
        return selenium.getText(needConfirmationCheckinDate);
    }

    /**
     * Get rent duration in first index need confirmation
     * @return string data type e.g "1 Bulan"
     */
    public String getRentDurationNeedConfirmation() {
        return selenium.getText(needConfirmationRentDuration);
    }

    /**
     * Clicks on need confirmation button chat owner
     */
    public void clicksOnNeedConfirmationChatButton() {
        selenium.javascriptClickOn(needConfirmationOwnerChatButton);
    }

    /**
     * Get kost name on chat box
     * @return string data type e.g "Kost Wild Rift"
     */
    public String getKostNameOnChatBox() {
        return selenium.getText(txtKostNameChatBox);
    }

    /**
     * Check if box chat is visible
     * @return visible true, otherwise false
     */
    public boolean isBoxChat() {
        return selenium.waitInCaseElementVisible(boxChat, 20) != null;
    }

    /**
     * Click on button back arrow on chat box
     * @throws InterruptedException
     */
    public void dismissChatBox() throws InterruptedException {
        if (isBoxChat()) {
            selenium.clickOn(btnBackArrowChatBox);
        }
    }

    /**
     * Click on see complete button on need confirmation booking list number 1
     */
    public void clickOnSeeCompleteButtonNeedConfirmationBooking() {
        selenium.javascriptClickOn(needConfirmationSeeCompleteButton);
    }

    /**
     * Get expanded booking price in booking details
     * @return string data type e.g Rp1.000.000 /bulan
     */
    public String getExpandedBookingPrice() {
        return selenium.getText(txtExpandedBookingPriceDuration);
    }

    /**
     * Check if see facilities button is visible after expanded from see complete on booking details
     * @return visible is true, otherwise false
     */
    public boolean isSeeFacilitiesButton() {
        return selenium.waitInCaseElementVisible(btnExpandedSeeFacilities, 20) != null;
    }

    /**
     * Chekc if see newest information is visible after expanded from see complete on booking details
     * @return true if visible, otherwise false
     */
    public boolean isSeeNewInformationButton() {
        return selenium.waitInCaseElementVisible(btnExpandedSeeNewestInfo, 20) != null;
    }

    /**
     * Get renter name after expanded a booking
     * @return string data type e.g "Tenant Otomed Baru Dilihat"
     */
    public String getExpandedRenterName() {
        return selenium.getText(txtExpandedRenterName);
    }

    /**
     * Get reneter phone number on expanded booking list
     * @return string data type e.g "087708777612"
     */
    public String getExpandedPhoneNumber() {
        return selenium.getText(txtExpandedPhoneNumber);
    }

    /**
     * Get text of detail booking element with index chosen
     * @param index 1 Booking ID
     *              2 Checking Date
     *              3 Checkout Date
     *              4 Duration
     *              5 Rent Count
     *              Input it with above number
     * @return String data type of selected index
     */
    public String getExpandedBookingDetailElement(String index) {
        String element = ".--is-detail .booking-item:nth-of-type("+index+") b";
        return selenium.getText(driver.findElement(By.cssSelector(element)));
    }

    /**
     * Click on see facilities button after expanded it with click on "Lihat Selengkapnya"
     * @throws InterruptedException
     */
    public void clicksOnExpandedFacilities() throws InterruptedException {
        selenium.clickOn(btnExpandedSeeFacilities);
    }

    /**
     * Check if facilities box is visible after click on see facilities button
     * clicksOnExpandedFacilities() trigger it with this method
     * @return visible true otherwise false
     */
    public boolean isFacilitiesBox() {
        return selenium.waitInCaseElementVisible(boxFacilities, 20) != null;
    }

    public void clicksOnPopUpXButton() throws InterruptedException {
        selenium.clickOn(buttonXCloseNotification);
    }

    /**
     * Click on see newest facilities to go to kost details, swich to window number 2 after
     * @throws InterruptedException
     */
    public void clicksSeeNewestInformationExpandedBooking() throws InterruptedException {
        selenium.clickOn(btnExpandedSeeNewestInfo);
        selenium.hardWait(2);
        selenium.switchToWindow(2);
    }

    /**
     * Clicks on See Less button on expanded booking history list
     */
    public void clicksOnSeeLessButton() {
        selenium.javascriptClickOn(btnSeeLess);
    }

    /**
     * Check if there is a expanded booking list on history booking
     * @return visible is true, otherwise false
     */
    public boolean isBookingDetailExpanded() {
        return selenium.waitInCaseElementVisible(boxExpandedBookingDetails, 2) != null;
    }

    /**
     * Click on need payment button.
     * Button will available after owner accept booking
     * @throws InterruptedException
     */
    public void clickOnNeedPaymentButton() throws InterruptedException {
        selenium.clickOn(buttonNeedPayment);
    }

    /**
     * Click on custom button
     * This method is for settlement invoice, pop up will appear for this scenario
     * @throws InterruptedException
     */
    public void clickOnPayRepaymentButton() throws InterruptedException {
        selenium.clickOn(btnCustom);
        selenium.hardWait(3);
    }

    /**
     * Click on check in button with desired kost name
     * @param kostName input with kost name targe
     * @throws InterruptedException
     */
    public void clickOnCheckinButtonKost(String kostName) throws InterruptedException {
        String kostListEl = "(//*[contains(text(), '"+ kostName +"')]/ancestor::*[@id='bookingListCard']//*[@type='button' and contains(text(), 'Check-in Kos')])[2]";
        selenium.waitTillElementIsVisible(By.xpath(kostListEl), 30);
        selenium.hardWait(2);
        selenium.clickOn(driver.findElement(By.xpath(kostListEl)));
        selenium.waitTillElementIsVisible(modalBtnCustom);
        selenium.hardWait(2);
        selenium.clickOn(modalBtnCustom);
        selenium.waitInCaseElementVisible(modalBtnCustomHidden, 20);
        selenium.hardWait(2);
        selenium.javascriptClickOn(modalBtnCustomHidden);
    }
}
