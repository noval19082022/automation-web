package pageobjects.mamikos.tenant.profile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class EditProfilePO {

    WebDriver driver;
    SeleniumHelpers selenium;
    JavaHelpers java;

    public EditProfilePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//div[@class='bg-c-select']")
    private WebElement universityDropdown;

    @FindBy(xpath = "(//div[@class = 'bg-c-input bg-c-input--md']//input[@class='bg-c-input__field'])[2]")
    private WebElement inputSearchUniversity;

    @FindBy(xpath = "//input[@class = 'form-control custom-input other-input']")
    private WebElement inputAnotherUniversity;

    @FindBy(id = "swal2-title")
    private WebElement titleMessagePopUp;

    @FindBy(id = "swal2-content")
    private WebElement contentMessagePopUp;

    private By successIconPopUp = By.cssSelector(".swal2-success-ring");

    @FindBy(xpath = "//*[@data-testid='profileButton']")
    private WebElement userClickProfileDropdownMenu;

    @FindBy(xpath = "//*[@class='bg-c-select'][1]")
    private WebElement professionDropdown;

    @FindBy(css = ".custom-wrapper > div:nth-child(1)")
    private WebElement userClickDropdownInstansi;

    @FindBy(xpath = "//div[@class='open btn-group']//input[@class='form-control']")
    private WebElement clickSearch;

    @FindBy(xpath = "(//button[@class='btn btn-danger btn-lg hidden-xs distance'])")
    private WebElement batalButton;

    @FindBy(xpath = "//*[@id='profileProfession']/div[1]/div")
    private WebElement radioMahasiswaButton;

    @FindBy(css = "#profileProfession > div:nth-child(2) > div:nth-child(1) > label:nth-child(1) > span:nth-child(2)")
    private WebElement radioKaryawanButton;

    @FindBy(css = "[data-popper-placement='top-start'] [placeholder='Search']")
    private WebElement inputText;

    @FindBy(xpath = "//a[contains(.,'Lainnya')]")
    private WebElement clickLainnya;

    @FindBy(xpath = "//input[@class='form-control custom-input other-input']")
    private WebElement fillsNameUniversitas;

    @FindBy(xpath = "//input[@class='form-control custom-input other-input']")
    private WebElement inputTextNamaUniversitas;

    @FindBy(css = "[data-popper-placement='top-start'] > ul")
    private WebElement instansiWarning;

    @FindBy(xpath = "//p[contains(.,'No. Handphone Darurat minimal mengandung 8 karakter.')]")
    private WebElement validationMessageNoHpDaruratLessThan8;

    @FindBy(xpath = "//p[contains(.,'No. Handphone Darurat tidak boleh lebih dari 14 karakter.')]")
    private WebElement validationMessageNoHpDaruratMoreThan14;

    @FindBy(xpath = "//input[@name='No. Handphone Darurat']")
    private WebElement userInputPhoneNumberLessThan8;

    @FindBy(xpath = "//input[@name='No. Handphone Darurat']")
    private WebElement userInputPhoneNumberMoreThan14;

    @FindBy(xpath = "//input[@name='No. Handphone Darurat']")
    private WebElement clickNomorDarurat;

    @FindBy(css = "[for='profileGenderMale'] > .bg-c-radio__icon")
    private WebElement userClickRadioButtonPria;

    @FindBy(xpath = "//button[@class='btn btn-primary btn-lg hidden-xs']")
    private WebElement saveButton;

    @FindBy(xpath = "//h2[@class='swal2-title']")
    private WebElement popupMessageProfileDisimpan;

    @FindBy(css = "select#profileLastEducation > option:nth-of-type(7)")
    private WebElement userSelectS1;

    @FindBy(xpath = "//select[@id='profileLastEducation']")
    private WebElement userClickLastEducationTenant;

    @FindBy(xpath = "//input[@id='profileUserName']")
    private WebElement userClickFieldFullName;

    @FindBy(xpath = "//input[@id='profileUserName']")
    private WebElement userDeletePreviousFullName;

    @FindBy(xpath = "//input[@id='profileUserName']")
    private WebElement userFullNameWithNumber;

    @FindBy(xpath = "//p[contains(.,'Nama Lengkap harus diisi.')]")
    private WebElement messageErrorFullName;

    @FindBy(css = ".select-custom .bg-c-select__trigger")
    private WebElement userClickKotaAsalDropdown;

    @FindBy(css = ".select-custom li:nth-of-type(2) > .bg-c-dropdown__menu-item")
    private WebElement userSelectCity;

    @FindBy(css = ".select-custom [placeholder='Search']")
    private WebElement userSearchCity;

    @FindBy(css = "[data-popper-placement='bottom-start'] li:nth-of-type(3) > .bg-c-dropdown__menu-item")
    private WebElement userSeeResultCityTangerang;

    @FindBy(css = "[for='lainnya'] > .bg-c-radio__icon")
    private WebElement userChooseProfessionLainnya;

    @FindBy(xpath = "//input[@id='profileDescription']")
    private WebElement userInputProfession;

    @FindBy(xpath = "//a[contains(.,'Universitas Indonesia')]")
    private WebElement userChooseUniversitasIndonesia;

    @FindBy(css = ".hidden-xs.btn-primary")
    private WebElement userSeeButtonSimpanEditProfileDisable;

    @FindBy(xpath = "//a[contains(.,'Indonesia')]")
    private List<WebElement> dropdownSearchResults;

    @FindBy(xpath = "//select[@id='profileStatus']")
    private WebElement maritalStatusDropdown;

    @FindBy(css = "select#profileStatus > option:nth-of-type(2)")
    private WebElement defaultStatusText;

    @FindBy(xpath = "//input[@class='form-control custom-input other-input']")
    private WebElement inputTextNamaInstansi;

    @FindBy(xpath = "//input[@placeholder='Masukkan Tanggal Lahir']")
    private WebElement iconCalendar;

    @FindBy(xpath = "//span[@class='cell day selected']")
    private WebElement dateOfBirth;

    @FindBy(xpath="//*[@class='swal2-buttonswrapper']//button[@class='swal2-confirm swal2-styled']")
    private WebElement okButtonOnPopupMessage;

    @FindBy(xpath="//span[contains(.,'There is no data')]")
    private WebElement noDataText;

    @FindBy(xpath="//*[@data-testid='jobDescription-errorMessage']")
    private WebElement errorMessageJobs;


    /**
     * Select University Name on Edit Profile Page
     *
     * @param name
     * @throws InterruptedException
     */
    public void selectUniversityOrOfficeName(String name) throws InterruptedException {
        selenium.pageScrollInView(batalButton);
        selenium.clickOn(universityDropdown);
        selenium.clickOn(driver.findElement(By.xpath("//a[contains(.,'"+ name +"')]")));
        selenium.clickOn(saveButton);
    }

    /**
     * Input University Name on Edit Profile Page
     *
     * @param name
     * @throws InterruptedException
     */
    public void inputUniversityOrOfficeName(String name) throws InterruptedException {
        selenium.pageScrollInView(batalButton);
        selenium.clickOn(universityDropdown);
        selenium.clickOn(driver.findElement(By.xpath("//a[contains(.,'Lainnya')]")));
        selenium.pageScrollInView(batalButton);
        selenium.enterText(inputAnotherUniversity, name, true);
        selenium.clickOn(saveButton);
    }

    /**
     * Check Success Icon Pop Up is Present in Edit Profile Page
     *
     * @return boolean
     */
    public boolean checkSuccessIconPopUp() {
        return selenium.waitInCaseElementPresent(successIconPopUp, 5) != null;
    }


    /**
     * Get Text of Title Message Pop Up in Edit Profile Page
     *
     * @return Text of Title Messsage Pop Up
     */
    public String getTitleMessagePopUp() {
        return selenium.getText(titleMessagePopUp);
    }

    /**
     * Get Text of Content Message Pop Up in Edit Profile Page
     *
     * @return Text of Content Message Pop Up
     */
    public String getContentMessagePopUp() {
        return selenium.getText(contentMessagePopUp);
    }


    /**
     * user click profile dropdown menu
     *
     * @throws InterruptedException
     */
    public void userClickProfileDropdownButton() throws InterruptedException {
        selenium.clickOn(userClickProfileDropdownMenu);
    }


    /**
     * user click radio button karyawan
     *
     * @throws InterruptedException
     */
    public void userChooseProfession(String Profession) throws InterruptedException {
        selenium.pageScrollInView(batalButton);
        selenium.hardWait(4);
        WebElement profession = driver.findElement(By.xpath("//label[@for='"+Profession+"']"));
        selenium.clickOn(profession);
    }

    /**
     * user click dropdown universitas
     *
     * @throws InterruptedException
     */
    public void userClickDropdownUniversitas() throws InterruptedException {
        selenium.waitInCaseElementVisible(professionDropdown, 5);
        selenium.clickOn(professionDropdown);
    }

    /**
     * user click dropdown instansi
     *
     * @throws InterruptedException
     */
    public void userClickDropdownInstansi() throws InterruptedException {
        selenium.pageScrollInView(batalButton);
        selenium.hardWait(10);
        selenium.clickOn(userClickDropdownInstansi);
    }

    /**
     * user fills not matches on list
     *
     * @throws InterruptedException
     */
    public void userFillsNotMatches(String text) {
        selenium.enterText(inputText, text, true);
    }

    /**
     * user fills matches on list
     *
     * @throws InterruptedException
     */
    public void userFillsMatched(String indonesia) throws InterruptedException {
        selenium.pageScrollInView(batalButton);
        selenium.enterText(inputText, indonesia, true);
        selenium.hardWait(3);
    }

    /**
     * user fills lainnya on fields search
     *
     * @throws InterruptedException
     */
    public void userFillsCharacters(String lainnya) throws InterruptedException {
        selenium.enterText(inputText, lainnya, true);
        selenium.hardWait(3);
    }

    /**
     * user click lainnya on list universitas
     *
     * @throws InterruptedException
     */
    public void userClickLainnya() throws InterruptedException {
        selenium.clickOn(clickLainnya);
    }

    /**
     * user fills one character on list fields universitas
     *
     * @throws InterruptedException
     */
    public void userFillsSingleCharacter(String text) throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(fillsNameUniversitas);
        selenium.enterText(inputTextNamaUniversitas, text, true);
    }

    /**
     * appears dropdown list universitas
     *
     * @throws InterruptedException
     */
    public boolean dropdownWillDisplayed() {
        return selenium.waitInCaseElementVisible(instansiWarning, 5) != null;
    }

    /**
     * appears error message less than 8
     *
     * @throws InterruptedException
     */
    public String validationMessageNoHpDaruratLesThan8() {
        return selenium.getText(validationMessageNoHpDaruratLessThan8);
    }

    /**
     * appears error message more than 14
     *
     * @throws InterruptedException
     */
    public String validationMessageNoHpDaruratMoreThan14() throws InterruptedException {
        return selenium.getText(validationMessageNoHpDaruratMoreThan14);
    }

    /**
     * user input phone number less than 8
     *
     * @throws InterruptedException
     */
    public void userInputPhoneNumberLessThan8(String phone) throws InterruptedException {
        selenium.pageScrollInView(batalButton);
        selenium.clickOn(clickNomorDarurat);
        selenium.enterText(userInputPhoneNumberLessThan8, phone, false);
        selenium.hardWait(3);
    }

    /**
     * user input phone number more than 14
     *
     * @throws InterruptedException
     */
    public void userInputPhoneNumberMoreThan14(String phone) throws InterruptedException {
        selenium.waitInCaseElementClickable(batalButton,10);
        selenium.pageScrollInView(batalButton);
        selenium.clickOn(clickNomorDarurat);
        selenium.enterText(userInputPhoneNumberMoreThan14, phone, false);
        selenium.hardWait(3);
    }

    /**
     * user click radio button
     *
     * @throws InterruptedException
     */
    public void userClickRadioButtonPria() throws InterruptedException {
        selenium.clickOn(userClickRadioButtonPria);
    }

    /**
     * user click simpan button
     *
     * @throws InterruptedException
     */
    public void userClickSimpanButton() throws InterruptedException {
        selenium.pageScrollInView(batalButton);
        selenium.hardWait(5);
        selenium.waitInCaseElementClickable(saveButton,20);
        selenium.clickOn(saveButton);
    }

    /**
     * appears popup message profil disimpan
     */
    public String popupMessageProfileDisimpan() {
        return selenium.getText(popupMessageProfileDisimpan);
    }

    /**
     * user click last education tenant
     */
    public void userClickLastEducationTenant() throws InterruptedException {
        selenium.waitInCaseElementClickable(batalButton,10);
        selenium.pageScrollInView(batalButton);
        selenium.clickOn(userClickLastEducationTenant);
        selenium.hardWait(3);

    }

    /**
     * user select S1
     */
    public void userSelectS1() throws InterruptedException {
        selenium.clickOn(userSelectS1);

    }

    /**
     * user click field nama lengkap
     */
    public void userClickFieldFullName() throws InterruptedException {
        selenium.clickOn(userClickFieldFullName);

    }

    /**
     * user delete previous fullname
     */
    public void userDeletePreviousFullName() throws InterruptedException {
        selenium.hardWait(2);
        this.userDeletePreviousFullName.clear();
    }

    /**
     * appears message error fullname
     */
    public String messageErrorFullName() {
        selenium.waitInCaseElementVisible(messageErrorFullName, 5);
        return selenium.getText(messageErrorFullName);
    }

    /**
     * user fills fullname with number
     */
    public void userFullNameWithNumber(String noval1908) throws InterruptedException {
        selenium.clearTextField(userFullNameWithNumber);
        selenium.enterText(userFullNameWithNumber, noval1908, false);
        selenium.hardWait(3);
    }

    /**
     * user click kota asal
     */
    public void userClickKotaAsalDropdown() throws InterruptedException {
        selenium.clickOn(userClickKotaAsalDropdown);
    }

    /**
     * user select city
     */
    public void userSelectCity() throws InterruptedException {
        selenium.clickOn(userSelectCity);
    }

    /**
     * user search city
     */
    public void userSearchCity(String Tangerang) {
        selenium.pageScrollInView(batalButton);
        selenium.enterText(userSearchCity, Tangerang, false);
    }

    /**
     * appears popup message profil disimpan
     */
    public String userSeeResultCityTangerang() {

        return selenium.getText(userSeeResultCityTangerang);
    }


    /**
     * user fills profession
     */
    public void userInputProfession(String Wirsawasta) throws InterruptedException {
        selenium.pageScrollInView(batalButton);
        selenium.enterText(userInputProfession, Wirsawasta, true);
        selenium.hardWait(3);
    }

    /**
     * user choose universitas indonesia
     */
    public void userChooseUniversitasIndonesia() throws InterruptedException {
        selenium.clickOn(userChooseUniversitasIndonesia);
    }
    /**
     * user no choose profession
     */
    public void userNoChooseProfession() throws InterruptedException {
        selenium.pageScrollInView(batalButton);
        selenium.javascriptClickOn(radioKaryawanButton);
    }

    /**
     * disable button simpan
     */
    public String userSeeButtonSimpanEditProfileDisable() {
        return selenium.getText(userSeeButtonSimpanEditProfileDisable);
    }

    /**
     * Get Dropdown Result Not Found Text
     * @param message
     * @return string
     */
    public String getDropdownResultNotFoundText(String message) throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(By.xpath("//span[contains(., '"+message+"')]"));
    }

    /**
     * Is Dropdown Result List Contains Inputted Text
     * @param text
     * @return true or false
     */
    public Boolean isDropdownResultsListContains(String text){
        List<String> resultList = new ArrayList<>();
        for (WebElement dropdownSearchResult : dropdownSearchResults) {
            resultList.add(dropdownSearchResult.getText());
        }
        return resultList.get(0).contains(text);
    }

    public Boolean isButtonSimpanDisabled(String attribute){
        return selenium.isElementAtrributePresent(saveButton, attribute);
    }

    /**
     * appears dropdown list marital status
     *
     * @throws InterruptedException
     */
    public void clickOnMaritalStatusDropdown() throws InterruptedException {
        selenium.clickOn(maritalStatusDropdown);
    }

    /**
     * click icon calendar
     *
     * @throws InterruptedException
     */
    public void clickIconCalendar() throws InterruptedException {
        selenium.clickOn(iconCalendar);
    }

    /**
     * choose date of birth
     *
     * @throws InterruptedException
     */
    public void chooseDateOfBirth() throws InterruptedException {
        selenium.clickOn(dateOfBirth);
    }



    /**
     * see default marital status is belum kawin
     */
    public String seeDefaultStatusBelumKawin() {
        return selenium.getText(defaultStatusText);
    }

    /**
     * user choose marital status is kawin
     */
    public void selectMaritalStatus(String marital) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//option[@value='"+marital+"']"));
        selenium.clickOn(element);
    }

    /**
     * user fills character on list fields pekerjaan
     *
     * @throws InterruptedException
     */
    public void fillPekerjaanSingleCharacter(String text) throws InterruptedException {
        selenium.hardWait(3);
        selenium.enterText(inputTextNamaInstansi, text, true);
    }

    /**
     * user choose instansi name
     */
    public void selectOffice(String instansi) throws InterruptedException {
        WebElement intansiName = driver.findElement(By.xpath("//a[normalize-space()='"+instansi+"']"));
        selenium.clickOn(intansiName);
    }

    /**
     * click on OK button
     * @throws InterruptedException
     */
    public void clickOnOkButtonOnPopupMessage() throws InterruptedException {
        selenium.clickOn(okButtonOnPopupMessage);
        selenium.hardWait(2);
    }

    /**
     * appears error message there is no data
     */
    public String getNoDataText(){
        return selenium.getText(noDataText);
    }

    /**
     * appears error message on jobs
     */
    public String getErrroMessageOnJobs(){
        return selenium.getText(errorMessageJobs);
    }
}
