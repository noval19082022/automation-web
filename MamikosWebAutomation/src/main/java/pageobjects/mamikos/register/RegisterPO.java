package pageobjects.mamikos.register;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class RegisterPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public RegisterPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@data-testid='fullnameTextbox']")
    private WebElement nameInputText;

    @FindBy(xpath = "//*[@data-testid='phoneNumberTextbox']")
    private WebElement phoneInputText;

    @FindBy(xpath = "//*[@data-testid='emailTextbox']")
    private WebElement emailInputText;

    @FindBy(xpath = "//*[@data-testid='passwordTextbox']")
    private WebElement passwordInputText;

    @FindBy(xpath = "//*[@data-testid='repeatPasswordTextbox']")
    private WebElement passwordConfirmationInputText;

    @FindBy(xpath = "//*[@data-testid='daftarButton']")
    private WebElement registerButton;

    @FindBy(xpath = "//*[@id=\"recaptcha-anchor-label\"]")
    private WebElement captchaBox;

    @FindBy(className = "captcha-warning")
    private WebElement captchaError;

    @FindBy(className = "bg-c-input__icon--clickable")
    private WebElement passwordEyeButton;

    @FindBy(css = "div[class='swal2-content']")
    private WebElement popUpErrorMessages;

    @FindBy(xpath = "//label[contains(.,'Email (Opsional)')]")
    private WebElement emailTitle;

    @FindBy(css = ".registration-form__submit-button")
    private WebElement buttonRegisterDisable;


    /**
     * Fill register form and click register
     * @param name User Full Name
     * @param phone User Phone Number
     * @param email User email
     * @param password User password
     * @throws InterruptedException
     */
    public void fillOutRegisterForm(String name, String phone, String email, String password) throws InterruptedException {
        selenium.enterText(nameInputText, name, false);
        selenium.enterText(phoneInputText, phone, false);
        selenium.enterText(emailInputText, email, false);
        selenium.enterText(passwordInputText, password,false);
        selenium.enterText(passwordConfirmationInputText, password, false);
        selenium.clickOn(registerButton);
    }

    /**
     *
     * @param name User Full Name
     * @param phone User Phone Number
     * @param email User Email
     * @param password User Password
     * @throws InterruptedException
     */
    public void fillOutRegistrationFormWithoutClickRegister(String name, String phone, String email, String password) throws InterruptedException {
        selenium.enterText(nameInputText, name, true);
        selenium.enterText(phoneInputText, phone, true);
        selenium.hardWait(3);
        selenium.enterText(emailInputText, email, true);
        selenium.hardWait(3);
        selenium.enterText(passwordInputText, password,true);
        selenium.enterText(passwordConfirmationInputText, password, true);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click on Password Eye Button
     * @throws InterruptedException
     */
    public void clickPasswordEyeButton() throws InterruptedException {
        selenium.clickOn(passwordEyeButton);
    }

    /**
     * Get Password Attribute: Type = text
     * @return password attribute: type = text
     */
    public Boolean isPasswordAttributePresent(){
        return selenium.getElementAttributeValue(passwordInputText, "type=text") != null;
    }

    /**
     * Click on Register Button
     * @throws InterruptedException
     */
    public void clickOnRegisterButtonForm() throws InterruptedException {
        selenium.clickOn(registerButton);
    }
    /**
     * verify button register owner is disable
     * @throws InterruptedException
     */

    public Boolean disableButtonRegister() {
        return !selenium.isClickable(buttonRegisterDisable,5);
    }

    /**
     * Get error message on form
     * @param error error messages
     * @return error message
     */
    public String getErrorMessages(String error){
        return selenium.getText(By.xpath("//div[normalize-space()='"+error+"']"));
    }

    /**
     * is Captcha Error Present?
     * @return captchaError captcha error message
     */
    public Boolean isCaptchaErrorPresent(){
        return selenium.waitInCaseElementVisible(captchaError, 5) != null;
    }

    /**
     * is Captcha Present?
     * @return captcha container
     */
    public Boolean isCaptchaPresent(){
        selenium.pageScrollInView(captchaBox);
        return selenium.waitInCaseElementVisible(captchaBox, 5) != null;
    }

    /**
     * Get Pop Up Error Message
     * @return error message
     */
    public String getPopUpErrorMessage(){
        return selenium.getText(popUpErrorMessages);
    }

    /**
     * Get Name Input Text
     * @return string
     */
    public String getNameInputText(){
        return selenium.getInputText(nameInputText);
    }

    /**
     * Get Password Input Text
     * @return string
     */
    public String getPasswordInputText(){
        return selenium.getInputText(passwordInputText);
    }

    /**
     * Get Email Input Text
     * @return string
     */
    public String getEmailInputText(){
        return selenium.getInputText(emailInputText);
    }

    /**
     * check email title is available
     * @return true if the screen is present otherwise false (
     */
    public boolean isEmailTitleAvailable(){
        return selenium.waitInCaseElementVisible(emailTitle, 5) != null;
    }
}
