package pageobjects.mamikos.promo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class PromoDetailPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public PromoDetailPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".promo-title")
    private WebElement promoTitleLabel;

    @FindBy(xpath = "//*[@class=\"link-button klik-cta-promo\"]")
    private WebElement bookingNowButton;

    /**
     * Get promo title
     * @return String promo title
     */
    public String getPromoTitle() {
        return selenium.getText(promoTitleLabel);
    }

    /**
     * Check button use now exist
     * @return boolean true if button exist
     */
    public boolean bookingNowButtonDisplayed() {
        return selenium.waitInCaseElementVisible(bookingNowButton, 3) != null;
    }

}
