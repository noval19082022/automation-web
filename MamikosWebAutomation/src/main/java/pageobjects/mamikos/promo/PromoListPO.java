package pageobjects.mamikos.promo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.awt.*;
import java.awt.datatransfer.*;
import java.io.*;

public class PromoListPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public PromoListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "(//button[contains(text(),'SALIN')])[1]")
    private WebElement firstCopyButton;

    @FindBy(xpath = "//*[@class='tooltip-inner']")
    private WebElement firstTooltipLabel;

    @FindBy(xpath = "(//*[text()='Kode Promo']/following-sibling::p)[1]")
    private WebElement firstPromoCode;

    @FindBy(xpath = "//article[1]//button[@class='show-detail-btn-click']")
    private WebElement firstSeeDetailButton;

    @FindBy(xpath = "//article[1]//*[@class='promo-meta-title']/h2/a")
    private WebElement firstPromoTitleLabel;

    @FindBy(xpath = "//a[.='>']")
    private WebElement nextPageButton;

    @FindBy(xpath = "//a[.='<']")
    private WebElement previousPageButton;

    @FindBy(xpath = "//li[@class='page-item active']/a")
    private WebElement currentPageIndexButton;

    @FindBy(css = ".detail-kos__manage-premium-link")
    private WebElement setPromoButton;

    @FindBy(xpath = "//*[@class='premium-page__header']")
    private WebElement pagePromoTitle;

    @FindBy(xpath = "//*[@class='total-data status-']")
    private WebElement statusPromoText;

    @FindBy(xpath = "//h1[contains(text(),'Mamikos')]")
    private WebElement clipboardTextNotFound;


    /**
     * Click On first copy promo button
     * @throws InterruptedException
     */
    public void clickOnFirstCopyPromo() throws InterruptedException {
        selenium.clickOn(firstCopyButton);
    }

    /**
     * Verify first tooltip message present
     * @return true if present, false if not
     */
    public boolean isTooltipPresent() {
        return selenium.waitInCaseElementVisible(firstTooltipLabel, 3) != null;
    }

    /**
     * Get copied text in clipboard
     * @return String copied text in clipboard
     */
    public String getClipboardText() throws IOException, UnsupportedFlavorException, InterruptedException {
         Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
            return (String) clip.getContents(null).getTransferData(DataFlavor.stringFlavor);
    }
    /**
     * promo code not found
     * @return
     */
    public boolean isGetClipboardText() {
        return selenium.waitInCaseElementVisible(firstPromoCode, 5) != null;
    }

    /**
     * Get promo code
     * @return String promo code
     */
    public String getFirstPromoCode() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.getText(firstPromoCode);
    }

    /**
     * Get promo code
     * @return String promo code
     */
    public String getClipboardText2() throws InterruptedException {
        return selenium.getText(firstPromoCode);
    }

    /**
     * Click On first see detail promo button
     * @throws InterruptedException
     */
    public void clickFirstSeeDetail() throws InterruptedException {
        selenium.clickOn(firstSeeDetailButton);
    }

    /**
     * Get first promo title
     * @return String promo title
     */
    public String getFirstPromoTitle() {
        return selenium.getText(firstPromoTitleLabel);
    }

    /**
     * Click On next page
     * @throws InterruptedException
     */
    public void clickNextPage() throws InterruptedException {
        if (selenium.isElementDisplayed(nextPageButton)) {
            selenium.clickOn(nextPageButton);
        }
    }

    /**
     * Click On previous page
     * @throws InterruptedException
     */
    public void clickPrevPage() throws InterruptedException {
        if (selenium.isElementDisplayed(previousPageButton)) {
            selenium.clickOn(previousPageButton);
        }
    }

    /**
     * Get current pagination index
     * @return String page index
     */
    public String getPageIndex() {
        String currentPage;
        if (selenium.isElementDisplayed(nextPageButton)) {
            currentPage = selenium.getText(currentPageIndexButton);
        }else
            currentPage = null;
        return currentPage;
    }

    /**
     * Click On certain page
     * @throws InterruptedException
     */
    public void clickPageIndex(String index) throws InterruptedException {
        String pageIndex = "//a[text()='" + index + "']";
        if (selenium.isElementPresent(By.xpath(pageIndex))) {
            selenium.clickOn(By.xpath(pageIndex));
        }

    }

    /**
     * Click On button atur promo
     * @throws InterruptedException
     */
    public void clickOnSetPromoButton() throws InterruptedException {
        selenium.pageScrollInView(setPromoButton);
        selenium.clickOn(setPromoButton);
    }

    /**
     * Get title Kelola Promo
     * @return String kelola promo title
     */
    public String getPagePromoTitle() {
        selenium.switchToWindow(0);
        selenium.waitTillElementIsVisible(pagePromoTitle);
        return selenium.getText(pagePromoTitle);
    }

    /**
     * Get Status Promo
     * @return String status promo
     */
    public String getStatusPromo() {
        return selenium.getText(statusPromoText);
    }

}
