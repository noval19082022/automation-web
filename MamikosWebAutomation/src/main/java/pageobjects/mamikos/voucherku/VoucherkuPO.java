package pageobjects.mamikos.voucherku;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class VoucherkuPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public VoucherkuPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */
    @FindBy(css = ".user-profil-dropdown")
    private WebElement profilDropdown;

    @FindBy(xpath = "//a[@href='/user/voucherku']")
    private WebElement voucherkuMenu;

    @FindBy(xpath = "//div[@id='active']/div[1]/div[1]")
    private WebElement tersediaVoucherCard;

    @FindBy(xpath = "//span[contains(.,'Terpakai')]")
    private WebElement terpakaiTab;

    @FindBy(xpath = "//div[@class='b-pill']/span[contains(.,'Kedaluwarsa')]")
    private WebElement kedaluwarsaTab;

    @FindBy(className = "top-wrapper__right-text")
    private WebElement promoLainnyaButton;

    @FindBy(xpath = "//div[@id='used']/div[1]/div[1]")
    private WebElement terpakaiVoucherCard;

    @FindBy(xpath = "//div[@id='expired']/div[1]/div[1]")
    private WebElement kedaluwarsaVoucherCard;

    @FindBy(className = "voucher-code__button")
    private WebElement voucherListSalinButtonActive;

    @FindBy(css = ".bg-c-button")
    private WebElement voucherDetailSalinButtonActive;

    @FindBy(xpath = "//*[@id='active'] //*[contains(@class, 'voucher-card')]")
    private List<WebElement> availableListVoucherCard;

    @FindBy(xpath = "//*[@id='used'] //*[contains(@class, 'voucher-card')]")
    private List<WebElement> usedListVoucherCard;

    @FindBy(xpath = "//*[@id='expired'] //*[contains(@class, 'voucher-card')]")
    private List<WebElement> expiredListVoucherCard;

    @FindBy(className = "bg-c-button")
    private WebElement voucherDetailCode;

    @FindBy(css = ".bg-c-button[@disabled='disabled']")
    private WebElement voucherDetailSalinButtonDisabled;

    @FindBy(css = ".voucher-detail-term__label")
    private WebElement voucherTnC;

    @FindBy(xpath = "//h4[.='Partner']")
    private WebElement partnerTab;

    @FindBy(xpath = "//div[@id='default']/div[1]/div[1]//span[@class='text__partner '][contains(text(),'S&K Berlaku')]")
    private WebElement checkSnKBerlakuText;

    @FindBy(xpath = "//div[@id='default']/div[1]/div[1]")
    private WebElement voucherPartnerCard;

    @FindBy(className = "voucher-detail__image")
    private WebElement voucherDetailImageBanner;

    @FindBy(className = "voucher-detail-title__text")
    private WebElement voucherDetailCampaignTitle;

    @FindBy(xpath = "//span[@class='voucher-valid__container--active text--active']")
    private WebElement voucherDetailExpiredDate;

    @FindBy(className = "voucher-detail-term__label")
    private WebElement voucherDetailSyaratDanKetentuanLabel;

    @FindBy(className = "voucher-detail-term__text")
    private WebElement voucherDetailSyaratDanKetentuanDescription;

    @FindBy(css = ".copy-code-text__label")
    private WebElement voucherDetailKodeVoucherLabel;

    @FindBy(css = ".copy-code-text__code")
    private WebElement voucherDetailVoucherCode;

    @FindBy(className = "copy-code-text__icon")
    private WebElement voucherDetailTicketIcon;

    @FindBy(className = "copy-code-card__button button-active")
    private WebElement voucherDetailSalinButton;

    @FindBy(className = "voucher-detail__image")
    private WebElement voucherPartnerDetailImageBanner;

    @FindBy(className = "voucher-detail-title__text")
    private WebElement voucherPartnerDetailCampaignTitle;

    @FindBy(xpath = "//span[@class='voucher-valid__container--active text--active']")
    private WebElement voucherPartnerDetailExpiredDate;

    @FindBy(className = "voucher-detail-term__label")
    private WebElement voucherPartnerDetailSyaratDanKetentuanLabel;

    @FindBy(className = "voucher-detail-term__text")
    private WebElement voucherPartnerDetailSyaratDanKetentuanDescription;

    @FindBy(css = ".copy-code-text__label")
    private WebElement voucherPartnerDetailKodeVoucherLabel;

    @FindBy(css = ".copy-code-text__code")
    private WebElement voucherPartnerDetailVoucherCode;

    @FindBy(className = "copy-code-text__icon")
    private WebElement voucherPartnerDetailTicketIcon;

    @FindBy(css = ".bg-c-button")
    private WebElement voucherPartnerDetailSalinButton;

    @FindBy(className = "copy-code-card__tooltip")
    private WebElement voucherPartnerDetailCopyToast;

    private By tersediaEmptyState = By.xpath("//p[.='Voucher yang dapat kamu gunakan akan tersedia di sini']");

    private By terpakaiEmptyState = By.xpath("//p[.='Voucher yang telah kamu gunakan akan tampil di sini']");

    private By kedaluwarsaEmptyState = By.xpath("//p[.='Voucher yang habis masa berlakunya akan tampil di sini']");

    private By redVoucherCounter = By.className("count-badge");

    private By voucherListCopyToast = By.className("voucher-code__tooltip");

    private By voucherDetailCopyToast = By.className("copy-code-card__tooltip");

    private By voucherListSalinButtonDisabled = By.className("voucher-code__button disable");

    private By voucherListHeader = By.xpath("//h1[@class='title']");

    private By checkVoucherTersediaTab = By.xpath("//a[.='Tersedia']");

    private By checkVoucherTerpakaiTab = By.xpath("//a[.='Terpakai']");

    private By checkVoucherKedaluwarsaTab = By.xpath("//a[.='Kedaluwarsa']");

    private By checkVoucherPromoLainnya = By.className("other-promo__text");

    private By checkVoucherPromoLihatButton = By.className("other-promo__button");

    private By checkVoucherCard = By.className("voucher-card");

    private By checkVoucherListImage = By.className("voucher-image");

    private By checkVoucherListExpiredDate = By.className("voucher-valid");

    private By checkVoucherListKodeVoucherLabel = By.className("voucher-code__text");

    private By checkVoucherListVoucherCode = By.className("voucher-code__code");

    private By checkVoucherListSalinButton = By.className("voucher-code__button");

    private By checkVoucherListTerpakaiLabel = By.xpath("//div[@id='used']/div[1]/div[1]//span[@class='voucher-valid__container--disable text--disable']");

    private By checkVoucherListKedaluwarsaLabel = By.xpath("//div[@id='expired']/div[1]/div[1]//span[@class='voucher-valid__container--disable text--disable']");

    private By checkVoucherListDisabledKodeVoucherLabel = By.className("voucher-code__text disable");

    private By checkVoucherListDisabledVoucherCode = By.className("voucher-code__code disable");

    private By voucherDetailTerpakaiLabel = By.xpath("//span[@class='voucher-valid__container--disable text--disable detail'][text()='Tersedia']");

    private By voucherDetailKedaluwarsaLabel = By.xpath("//span[@class='voucher-valid__container--disable text--disable detail'][text()='Kedaluwarsa']");

    private By checkVoucherDetailDisabledKodeVoucherLabel = By.className("vcopy-code-text__label text-disable");

    private By checkVoucherDetailDisabledVoucherCode = By.className("copy-code-text__code text-disable");

    private By verifikasiAkunButton = By.xpath("//span[contains(.,'Verifikasi Akun')]");

    /**
     * Click on Profil Dropdown in homepage
     *
     * @throws InterruptedException
     */
    public void clickProfilDropdown() throws InterruptedException {
        selenium.click(profilDropdown);
        selenium.hardWait(3);
    }

    /**
     * Click on Voucherku menu/button
     *
     * @throws InterruptedException
     */
    public void clickVoucherkuMenu() throws InterruptedException {
        selenium.hardWait(6);
        selenium.pageScrollInView(verifikasiAkunButton);
        selenium.hardWait(3);
        selenium.clickOn(voucherkuMenu);
    }

    /**
     * Click on Voucher Card in voucher list
     *
     * @throws InterruptedException
     */
    public void clickVoucherCard() throws InterruptedException {
        selenium.click(tersediaVoucherCard);
        selenium.hardWait(3);
    }

    /**
     * Click on Terpakai tab inside voucherku
     *
     * @throws InterruptedException
     */
    public void clickTerpakaiTab() throws InterruptedException {
        selenium.hardWait(5);
        selenium.click(terpakaiTab);
        selenium.hardWait(3);
    }

    /**
     * Click on Kedaluwarsa tab inside voucherku
     *
     * @throws InterruptedException
     */
    public void clickKedaluwarsaTab() throws InterruptedException {
        selenium.hardWait(3);
        selenium.click(kedaluwarsaTab);
        selenium.hardWait(3);
    }

    /**
     * Get Tersedia Empty state text
     *
     * @throws InterruptedException
     */
    public boolean isTersediaEmptyStateVisible() throws InterruptedException {
        return selenium.isElementPresent((By) tersediaEmptyState);
    }

    /**
     * Get Terpakai Empty state text
     *
     * @throws InterruptedException
     */
    public boolean isTerpakaiEmptyStateVisible() throws InterruptedException {
        return selenium.isElementPresent((By) terpakaiEmptyState);
    }

    /**
     * Get Kedaluwarsa Empty state text
     *
     * @throws InterruptedException
     */
    public boolean isKedaluwarsaEmptyStateVisible() throws InterruptedException {
        return selenium.isElementPresent((By) kedaluwarsaEmptyState);
    }

    /**
     * Check if Image Banner element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailImageBannerVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherDetailImageBanner,3) != null;
    }

    /**
     * Check if Campaign Title element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailCampaignTitleVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherDetailCampaignTitle,3) != null;
    }

    /**
     * Check if Expired Date element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailExpiredDateVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherDetailExpiredDate,3) != null;
    }

    /**
     * Check if Syarat dan Ketentuan label element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailSyaratDanKetentuanLabelVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherDetailSyaratDanKetentuanLabel,3) != null;
    }

    /**
     * Check if Syarat dan Ketentuan description/text element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailSyaratDanKetentuanDescriptionVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherDetailSyaratDanKetentuanDescription,3) != null;
    }

    /**
     * Check if Kode Voucher label element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailKodeVoucherLabelVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherDetailKodeVoucherLabel,3) != null;
    }

    /**
     * Check if Voucher Code element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailVoucherCodeVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherDetailVoucherCode,3) != null;
    }

    /**
     * Check if Ticket Icon element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailTicketIconVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherDetailTicketIcon,3) != null;
    }

    /**
     * Check if Salin button element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailSalinButtonVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherDetailSalinButtonActive,3) != null;
    }

    /**
     * Click Lihat button on Promo Lainnya
     *
     * @throws InterruptedException
     */
    public void clickPromoLainnyaButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(terpakaiTab,10);
        selenium.pageScrollUsingCoordinate(0, -3000);
        selenium.hardWait(3);
        selenium.clickOn(promoLainnyaButton);
    }

    /**
     * Get current url
     *
     * @return URL
     */
    public String getUrlcurrentwindows() {
        selenium.switchToWindow(2);
        return selenium.getURL();
    }

    /**
     * Click on Voucher Card in terpakai tab
     *
     * @throws InterruptedException
     */
    public void clickTerpakaiVoucherCard() throws InterruptedException {
        selenium.clickOn(terpakaiVoucherCard);
    }

    /**
     * Click on Voucher Card in kedaluwarsa tab
     *
     * @throws InterruptedException
     */
    public void clickKedaluwarsaVoucherCard() throws InterruptedException {
        selenium.clickOn(kedaluwarsaVoucherCard);
    }

    /**
     * Click Salin button on voucher list
     *
     * @throws InterruptedException
     */
    public void clickVoucherListSalinButton() throws InterruptedException {
        selenium.clickOn(voucherListSalinButtonActive);
    }

    /**
     * Click Salin button on voucher detail
     *
     * @throws InterruptedException
     */
    public void clickVoucherDetailSalinButton() throws InterruptedException {
        selenium.clickOn(voucherDetailSalinButtonActive);
    }

    /**
     * Check if Voucher counter displayed in Voucher button
     *
     * @throws InterruptedException
     */
    public boolean isRedVoucherCounterVisible() throws InterruptedException {
        return selenium.isElementPresent((By) redVoucherCounter);
    }

    /**
     * Check if Voucher list toast appear after click Salin button in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListCopyToastVisible() throws InterruptedException {
        return selenium.isElementPresent((By) voucherListCopyToast);
    }

    /**
     * Check if Voucher list toast appear after click Salin button in voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailCopyToastVisible() throws InterruptedException {
        return selenium.isElementPresent((By) voucherDetailCopyToast);
    }

    /**
     * Check if Salin button is disabled in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListSalinButtonDisabledVisible() throws InterruptedException {
        return selenium.isElementPresent((By) voucherListSalinButtonDisabled);
    }

    /**
     * Check if Salin button is disabled in voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailSalinButtonDisabledVisible() throws InterruptedException {
        return selenium.isElementDisplayed(voucherDetailSalinButtonDisabled);
    }

    /**
     * Check Voucher Header in voucherku page
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListHeaderVisible() throws InterruptedException {
        return selenium.isElementPresent((By) voucherListHeader);
    }

    /**
     * Check Tersedia Tab in voucherku
     *
     * @throws InterruptedException
     */
    public boolean isVoucherTersediaTabVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherTersediaTab);
    }

    /**
     * Check Terpakai Tab in voucherku
     *
     * @throws InterruptedException
     */
    public boolean isVoucherTerpakaiTabVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherTerpakaiTab);
    }

    /**
     * Check Kedaluwarsa Tab in voucherku
     *
     * @throws InterruptedException
     */
    public boolean isVoucherKedaluwarsaTabVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherKedaluwarsaTab);
    }

    /**
     * Check Promo Lainnya text
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPromoLainnyaVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherPromoLainnya);
    }

    /**
     * Check Promo Lainnya Lihat Button
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPromoLihatButtonVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherPromoLihatButton);
    }

    /**
     * Check Voucher Card
     *
     * @throws InterruptedException
     */
    public boolean isVoucherCardVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherCard);
    }

    /**
     * Check Voucher Image in voucher List
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListImageVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherListImage);
    }

    /**
     * Check Voucher Expired Date in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListExpiredDateVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherListExpiredDate);
    }

    /**
     * Check Kode Voucher text in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListKodeVoucherLabelVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherListKodeVoucherLabel);
    }

    /**
     * Check voucher code in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListVoucherCodeVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherListVoucherCode);
    }

    /**
     * Check Salin button in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListSalinButtonVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherListSalinButton);
    }

    /**
     * Check Terpakai label in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListTerpakaiLabelVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherListTerpakaiLabel);
    }

    /**
     * Check Kedaluwarsa label in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListKedaluwarsaLabelVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherListKedaluwarsaLabel);
    }

    /**
     * Check disabled Kode Voucher text in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListDisabledKodeVoucherLabelVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherListDisabledKodeVoucherLabel);
    }

    /**
     * Check disabled Voucher Code in voucher list
     *
     * @throws InterruptedException
     */
    public boolean isVoucherListDisabledVoucherCodeVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherListDisabledVoucherCode);
    }


    /**
     * Check if Terpakai Label element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailTerpakaiLabelVisible() throws InterruptedException {
        return selenium.isElementPresent((By) voucherDetailTerpakaiLabel);
    }

    /**
     * Check if Kedaluwarsa Label element is present inside voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailKedaluwarsaLabelVisible() throws InterruptedException {
        return selenium.isElementPresent((By) voucherDetailKedaluwarsaLabel);
    }

    /**
     * Check disabled Kode Voucher in voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailDisabledKodeVoucherLabelVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherDetailDisabledKodeVoucherLabel);
    }

    /**
     * Check disabled Voucher Code in voucher detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherDetailDisabledVoucherCodeVisible() throws InterruptedException {
        return selenium.isElementPresent((By) checkVoucherDetailDisabledVoucherCode);
    }

    /**
     * Scroll Down Available Voucher List
     *
     */
    public void scrollDownAvailableVoucherLists() {
        if(Constants.ENV.equals("prod")){
            selenium.waitInCaseElementVisible(availableListVoucherCard.get(availableListVoucherCard.size()-2),10);
            selenium.pageScrollInView(availableListVoucherCard.get(availableListVoucherCard.size()-2));
        }
        else {
            selenium.waitInCaseElementVisible(availableListVoucherCard.get(0),5);
            selenium.pageScrollInView(availableListVoucherCard.get(availableListVoucherCard.size()-1));
        }
    }

    /**
     * Scroll Up Available Voucher List
     *
     */
    public void scrollUpAvailableVoucherLists() {
        selenium.waitInCaseElementVisible(availableListVoucherCard.get(0),10);
        selenium.pageScrollInView(availableListVoucherCard.get(0));
    }

    /**
     * Scroll Down Used Voucher List
     *
     */
    public void scrollDownUsedVoucherLists() {
        selenium.waitInCaseElementVisible(usedListVoucherCard.get(0),5);
        selenium.pageScrollInView(usedListVoucherCard.get(usedListVoucherCard.size()-1));
    }

    /**
     * Scroll Up Used Voucher List
     *
     */
    public void scrollUpUsedVoucherLists() {
        selenium.waitInCaseElementVisible(usedListVoucherCard.get(0),10);
        selenium.pageScrollInView(usedListVoucherCard.get(0));
    }

    /**
     * Scroll Down Expired Voucher List
     *
     */
    public void scrollDownExpiredVoucherLists() {
        selenium.waitInCaseElementVisible(expiredListVoucherCard.get(0),10);
        selenium.pageScrollInView(expiredListVoucherCard.get(expiredListVoucherCard.size()-1));
    }

    /**
     * Scroll Up Expired Voucher List
     *
     */
    public void scrollUpExpiredVoucherLists() {
        selenium.waitInCaseElementVisible(expiredListVoucherCard.get(0),10);
        selenium.pageScrollInView(expiredListVoucherCard.get(0));
    }

    /**
     * Scroll Down Voucher Detail
     *
     * @throws InterruptedException
     */
    public void scrollDownVoucherDetail() throws InterruptedException {
        selenium.pageScrollInView(voucherTnC);
        selenium.hardWait(2);
    }

    /**
     * Scroll Up Voucher Detail
     *
     * @throws InterruptedException
     */
    public void scrollUpVoucherDetail() throws InterruptedException {
        By element = By.xpath("//*[@class='voucher-detail-term__text']//ol//li");
        List<WebElement>listTermAndCondition = driver.findElements(element);
        selenium.pageScrollInView(listTermAndCondition.get(0));
        selenium.hardWait(2);
    }

    /**
     * Get Voucher Limited Period
     *
     */
    public String getVoucherExpiredDate(){
        return selenium.getText(By.xpath("//span[@class='voucher-valid__container--active text--active']"));
    }

    /**
     * Check voucher code is present
     * @return true / false
     * @param voucherCode is name of voucher
     */
    public boolean isVoucherTargetedPresent(String voucherCode) {
        return selenium.waitInCaseElementVisible(By.xpath("//*[contains(@class, 'voucher__list')] //span[contains(text(), '"+voucherCode+"')]"), 2) != null;
    }

    /**
     * scroll down the page to find voucher in case voucher position is on bottom, will keep scrolling until voucher is displayed
     * @param voucherCode is code voucher
     * @throws InterruptedException
     */
    public void scrollToFindTargetVoucher(String voucherCode) throws InterruptedException {
        selenium.hardWait(3);
        do {scrollDownAvailableVoucherLists();}
        while (!isVoucherTargetedPresent(voucherCode));
    }

    /**
     * Click Partner Tab Button
     *
     * @throws InterruptedException
     */
    public void clickPartnerTabButton() throws InterruptedException {
        selenium.clickOn(partnerTab);
    }

    /**
     * Check Voucher Partner S&K text in voucher List
     *
     * @throws InterruptedException
     */
    public boolean isSnKBerlakuTextVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(checkSnKBerlakuText,3) != null;
    }

    /**
     * Click on Voucher Partner card
     *
     * @throws InterruptedException
     */
    public void clickOnVoucherPartnerCard() throws InterruptedException {
        selenium.clickOn(voucherPartnerCard);
    }

    /**
     * Check if Image Banner element is present inside voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailImageBannerVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailImageBanner,3) != null;
    }

    /**
     * Check if Campaign Title element is present inside voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailCampaignTitleVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailCampaignTitle,3) != null;
    }

    /**
     * Check if Expired Date element is present inside voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailExpiredDateVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailExpiredDate,3) != null;
    }

    /**
     * Check if Syarat dan Ketentuan label element is present inside voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailSyaratDanKetentuanLabelVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailSyaratDanKetentuanLabel,3) != null;
    }

    /**
     * Check if Syarat dan Ketentuan description/text element is present inside voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailSyaratDanKetentuanDescriptionVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailSyaratDanKetentuanDescription,3) != null;
    }

    /**
     * Check if Kode Voucher label element is present inside voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailKodeVoucherLabelVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailKodeVoucherLabel,3) != null;
    }

    /**
     * Check if Voucher Code element is present inside voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailVoucherCodeVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailVoucherCode,3) != null;
    }

    /**
     * Check if Ticket Icon element is present inside voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailTicketIconVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailTicketIcon,3) != null;
    }

    /**
     * Check if Salin button element is present inside voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailSalinButtonVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailSalinButton,3) != null;
    }

    /**
     * Click Salin button on voucher partner detail
     *
     * @throws InterruptedException
     */
    public void clickVoucherPartnerDetailSalinButton() throws InterruptedException {
        selenium.clickOn(voucherPartnerDetailSalinButton);
    }

    /**
     * Check if Voucher list toast appear after click Salin button in voucher partner detail
     *
     * @throws InterruptedException
     */
    public boolean isVoucherPartnerDetailCopyToastVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(voucherPartnerDetailCopyToast,3) != null;
    }

    /**
     * Click on Voucher Name
     */
    public void clickVoucherName(String voucherName) throws InterruptedException {
        selenium.clickOn(By.xpath("//img[@alt='"+ voucherName +"'][@class='voucher-image__image']"));
    }


}