package pageobjects.mamikos.voucherku;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class TenantInvoicePO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public TenantInvoicePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//p[.='Total Pembayaran']/following-sibling::p")
    private WebElement remainingPayment;

    @FindBy(xpath = "//*[@data-testid='masukkan_link']")
    private WebElement inputVoucherCodeButton;

    @FindBy(xpath = "//*[@data-testid='codeVoucher_txt']")
    private WebElement voucherCodeTextBox;

    @FindBy(css= ".c-action__button")
    private WebElement useButton;

    @FindBy(xpath = "//div[@class='card-footer --need-payment']//a[contains(.,'Bayar Sekarang')]")
    private WebElement bayarSekarangButton;

    @FindBy(xpath = "//button[contains(.,'Bayar Pelunasan Sekarang')]")
    private WebElement bayarPelunasanSekarangButton;

    @FindBy(css = ".c-warning")
    private WebElement voucherAlertMessage;

    @FindBy(xpath = "//*[@data-testid='masukkan_link']")
    private WebElement inputVoucherCodeButtonOnVoucherList;

    @FindBy(css = ".invoice-voucher-switch")
    private WebElement removeButton;

    @FindBy(xpath = "//*[@class=\"icon-color bg-c-icon bg-c-icon--md icon-color__success\"]")
    private WebElement successIcon;

    @FindBy(xpath = "//div[@class='bg-c-toast__content'][contains(text(),'Voucher Dipakai')]")
    private WebElement usedVoucherToast;

    @FindBy(xpath = "//div[@class='bg-c-toast__content'][contains(text(),'Voucher Dihapus')]")
    private WebElement removedVoucherToast;

    @FindBy(xpath = "//div[@class='bg-c-switch invoice-point-switch bg-c-switch--off']")
    private WebElement mamipoinToggleButtonOn;

    @FindBy(xpath = "//div[@class='bg-c-switch invoice-point-switch bg-c-switch--on']")
    private WebElement mamipoinToggleButtonOff;

    @FindBy(xpath = "//*[.='Admin Fee']/following-sibling::*")
    private WebElement txtAdminFee;

    @FindBy(css = ".invoice-detail-row-section:first-of-type div + p")
    private WebElement txtRentCostPerPeriod;

    @FindBy(css = ".details-collapsible + .invoice-detail-row-section p:last-child")
    private WebElement txtTotalCost;

    @FindBy(xpath = "//*[@href='#basic-error-round-glyph']")
    private WebElement invalidVoucherIcon;

    private By tenantPointEstimate = By.xpath("//*[@class='mamipoin-estimated-text --is-vertical-align-sub']");

    @FindBy(xpath= "//a[.='Hapus']")
    private WebElement hapusButton;

    @FindBy(css = "#voucher-scroll")
    private WebElement voucherDetailsPopUp;

    @FindBy(css = "use[href='#basic-success-round-glyph']")
    private WebElement tickIconVoucherUsage;

    @FindBy(css = ".c-empty")
    private WebElement voucherSuggestionEmptyState;

    @FindBy(xpath = "//*[contains(text(), 'Anda memiliki') and contains(text(), 'voucher')]")
    private WebElement textVoucherEligible;

    @FindBy(css = ".bg-c-toast__content")
    private WebElement toastInvoice;

    @FindBy(xpath = "//*[@class='bg-c-toast__content']/following-sibling::*//button[.='Hapus']")
    private WebElement btnToastInvoiceDelete;

    @FindBy(xpath = "//*[@data-testid='pakaiVoucher_btn']")
    private WebElement pakaiButtonOnVoucherDetail;


    /**
     * Get Remaining Payment
     * @return remaining payment
     */
    public String getRemainingPayment() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.hardWait(6);
        return selenium.getText(remainingPayment);
    }

    /**
     * Click on input button for display edit text input voucher
     * @throws InterruptedException
     */
    public void accessVoucherForm() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.clickOn(inputVoucherCodeButton);
        selenium.clickOn(inputVoucherCodeButtonOnVoucherList);
    }

    /**
     * Enter text to edit text input voucher code
     * @param voucherCode is voucher code
     */
    public void enterVoucherCode(String voucherCode) {
        selenium.enterText(voucherCodeTextBox, voucherCode, true);
    }

    /**
     * Click button use for use voucher
     * @throws InterruptedException
     */
    public void clickOnUseButton() throws InterruptedException {
        selenium.clickOn(useButton);
    }

    /**
     * Get voucher alert message after click Gunakan Button
     */
    public String getVoucherAllertMessage() {
        return selenium.getText(voucherAlertMessage);
    }

    /**
     * Click Bayar Sekarang button
     * @throws InterruptedException
     */
    public void clickBayarSekarangButton() throws InterruptedException {
        selenium.clickOn(bayarSekarangButton);
    }

    /**
     * Click Bayar Pelunasan Sekarang button to open settlement invoice
     * @throws InterruptedException
     */
    public void clickBayarPelunasanSekarangButton() throws InterruptedException {
        selenium.clickOn(bayarPelunasanSekarangButton);
    }

    /**
     * Click on Remove button to Remove Voucher from Invoice
     * @throws InterruptedException
     */
    public void clickOnRemoveVoucherButton() throws InterruptedException {
        if (selenium.isElementDisplayed(removeButton)) {
            selenium.clickOn(removeButton);
        }
    }

    /**
     * Verify Voucher Applied Successfully
     * @return boolean
     */
    public Boolean verifyVoucherAppliedSuccessfully() {
        return selenium.waitInCaseElementVisible(successIcon, 10)!=null;
    }

    /**
     * Verify Voucher Removed Toast
     * @return boolean
     */
    public boolean verifyVoucherRemovedSuccesfully() {
        return selenium.waitInCaseElementVisible(removedVoucherToast,10)!=null;
    }

    /**
     * Click MamiPoin Toggle Button to On
     * @throws InterruptedException
     */
    public void clickMamipoinToggleButtonToOn() throws InterruptedException {
        selenium.clickOn(mamipoinToggleButtonOn);
        selenium.hardWait(6);
    }

    /**
     * Click MamiPoin Toggle Button to Off
     * @throws InterruptedException
     */
    public void clickMamipoinToggleButtonToOff() throws InterruptedException {
        selenium.clickOn(mamipoinToggleButtonOff);
        selenium.hardWait(6);
    }

    /**
     * Get per period kost price
     * @return string data type of kost price
     */
    public String getPerPeriodCost() {
        return selenium.getText(txtRentCostPerPeriod);
    }

    /**
     * Get admin fee cost
     * @return string data type admin fee price
     */
    public String getAdminFeeCost() {
        selenium.waitTillElementIsVisible(txtAdminFee, 60);
        return txtAdminFee.getText().trim();
    }

    /**
     * Get total cost price
     * @return string data type total cost
     */
    public String getTotalCost() {
        return selenium.getText(txtTotalCost);
    }

    /**
     * Get invalid voucher icon status
     * @return string data type total cost
     */
    public boolean voucherInvalidIconAppeared() {
        return selenium.isElementDisplayed(invalidVoucherIcon);
    }

    /**
     * Check if Point Estimate is not visible on invoice
     *
     * @throws InterruptedException
     */
    public Boolean isPointEstimateTenantVisible() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.isElementPresent(tenantPointEstimate);
    }

    /**
     * Click button delete voucher
     * @throws InterruptedException
     */
    public void clickOnDeleteVoucherButton() throws InterruptedException {
        selenium.clickOn(hapusButton);
    }

    /**
     * Clicks on Lihat detail button
     * @param index button index
     * @throws InterruptedException
     */
    public void clicksOnLihatDetailIndex(String index) throws InterruptedException {
        By e = By.xpath("//button[contains(.,'Lihat Detail')]["+index+"]");
        selenium.clickOn(e);
    }

    /**
     * Clicks on masukkan voucher button
     * @throws InterruptedException
     */
    public void clicksOnMasukkanButton() throws InterruptedException {
        selenium.clickOn(inputVoucherCodeButton);
    }

    /**
     * Check if voucher detail visible
     * @return visible true otherwise false
     */
    public boolean isVoucherDetailsVisible() {
        return selenium.waitInCaseElementVisible(voucherDetailsPopUp, 30) != null;
    }

    /**
     * Click on pakai button on voucher pop up
     * @param index pakai button index
     * @throws InterruptedException
     */
    public void clicksOnPakaiButtonIndex(String index) throws InterruptedException {
        By e = By.xpath("(//*[.='Pakai'])["+index+"]");
        selenium.clickOn(e);
    }

    /**
     * Check if tick green icon is visible
     * @return visible true otherwise false
     */
    public boolean isVoucherUsageTickVisible() {
        return selenium.waitInCaseElementVisible(tickIconVoucherUsage, 60) != null;
    }

    /**
     * Check if voucher suggestion empty state is visible
     * @return visible true otherwise false
     */
    public boolean isVoucherSuggestionEmptyStateVisible() {
        return selenium.waitInCaseElementVisible(voucherSuggestionEmptyState, 60) != null;
    }

    /**
     * Get voucher eligible text
     * @return string data type
     */
    public String getVoucherEligibleText() {
        return selenium.getText(textVoucherEligible);
    }

    /**
     * Click on input button for display edit text input voucher
     * @throws InterruptedException
     */
    public void accessVoucherFormForRepayment() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.clickOn(inputVoucherCodeButton);
        selenium.clickOn(inputVoucherCodeButtonOnVoucherList);
    }

    /**
     * return toast content text
     * @return string data type
     */
    public String getActiveToastContentText() {
        selenium.waitTillElementIsVisible(By.xpath("//div[contains(text(), 'Kode voucher tidak bisa')]"), 10);
        return selenium.getText(toastInvoice);
    }

    /**
     * Check if hapus button on toast is visible
     * @return visible true other wise false
     */
    public boolean isToastHapusButtonVisible() {
        return selenium.waitInCaseElementVisible(btnToastInvoiceDelete, 10) != null;
    }

    /**
     * Click on Pakai button on Voucher Detail
     * @throws InterruptedException
     */
    public void clickOnPakaiButtonOnVoucherDetail() throws InterruptedException {
        selenium.clickOn(pakaiButtonOnVoucherDetail);
    }
}
