package pageobjects.mamikos.mamipoinowner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class MamiPoinOwnerPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public MamiPoinOwnerPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".mamipoin > .bg-c-card")
    private WebElement mamipoinWidget;

    private By verifyMamipoinWidget = By.cssSelector(".c-wrapper");

    private By pelajariText = By.xpath("//div[@class='title-running'][contains(text(),'Pelajari')]");

    private By zeroPoint = By.xpath("//div[@class='fadeInDown'][contains(text(),'0')]");

    private By tukarPoinText = By.xpath("//div[@class='title-running'][contains(text(),'Tukar Poin')]");

    private By point = By.xpath("//div[@class='fadeInDown'][contains(text(),'100.010')]");

    private By pointOwner = By.xpath("//span[@class='info-count']");

    private By nextButton = By.xpath("//button[@type='button'][contains(.,'Selanjutnya')]");

    private By mamipointLandingPageOnboardingTooltipText = By.xpath("//p[@class='message'][contains(text(),'Ini adalah jumlah poin Anda saat ini yang dapat Anda tukarkan dengan berbagai hadiah.')]");

    private By rewardHistoryOnBoarding = By.xpath("//p[@class='message'][contains(text(),'Cek seluruh hadiah yang telah Anda tukarkan dengan poin Anda di sini.')]");

    private By pointHistoryOnBoarding = By.xpath("//p[@class='message'][contains(text(),'Semua poin yang didapat dan aktivitas yang telah dilakukan tercatat di sini.')]");

    private By termAndConditionOnBoarding = By.xpath("//p[@class='message'][contains(text(),'Pelajari cara-cara untuk mendapatkan poin di bagian ini.')]");

    private By redeemPointOnBoarding = By.xpath("//p[@class='message'][contains(text(),'Anda dapat menukar poin Anda dengan hadiah di bagian ini.')]");

    @FindBy(xpath = "//button[@type='button'][contains(.,'Oke')]")
    private WebElement finishButton;

    @FindBy(xpath = "//button[@type='button'][contains(.,'Selesai')]")
    private WebElement selesaiButton;

    private By iconBackMamipoinOwner = By.cssSelector(".bg-c-icon[data-v-c503c40e]");

    private By titleMamipoinOwnerLandingPage = By.xpath("//div[@class='c-navigation__title'][contains(.,'MamiPoin')]");

    private By tukarPoinButton = By.xpath("//div[@class='c-mk-card exchange-card__card']");

    private By riwayatHadiahButton = By.xpath("//*[@href='javascript:void(0)'][contains(.,'Riwayat Hadiah')]");

    private By riwayatPoinButton = By.xpath("//*[@href='javascript:void(0)'][contains(.,'Riwayat Poin')]");

    private By syaratDanKetentuanButton = By.xpath("//*[@href='javascript:void(0)'][contains(.,'Syarat dan Ketentuan')]");

    private By poinAndaOnboarding = By.xpath("//p[@class='message'][contains(text(),'Pastikan poin Anda cukup untuk ditukarkan dengan hadiah yang Anda inginkan.')]");

    private By hadiahBisaDitukarOnboarding = By.xpath("//p[@class='message'][contains(text(),'Anda dapat menukar poin Anda sesuai dengan jumlah yang dibutuhkan hadiah terkait.')]");

    private By bantuanOnboarding = By.xpath("//p[@class='message'][contains(text(),'Tekan tombol Bantuan untuk kembali mempelajari cara penukaran poin.')]");

    private By titleTukarPoinPage = By.xpath("//strong[@class='link-text'][contains(.,'Menu MamiPoin')]");

    private By logoTukarPoinPage = By.xpath("//img[@class='mamipoin-image']");

    private By bantuanButton = By.xpath("//button[@type='button'][@class='button faq-button is-rounded --desktop']");

    private By tabRiwayatPoin = By.xpath("//a[.='Riwayat Poin']");

    private By historyDeduction = By.cssSelector(".--deduction");

    private By historyAddition = By.cssSelector(".item-poin");

    
    /**
     * Click on MamiPoin Widget in owner dashboard
     *
     * @throws InterruptedException
     */
    public void clickOnMamiPoinWidget() throws InterruptedException {
        selenium.clickOn(mamipoinWidget);
        selenium.hardWait(3);
    }

    /**
     * Verify MamiPoin Widget displayed on Owner Dashboard
     *
     * @return boolean
     */
    public boolean verifyMamiPoinWidgetDisplayed() {
        return selenium.waitInCaseElementPresent(verifyMamipoinWidget, 5) != null;
    }

    /**
     * Verify Pelajari Text displayed on MamiPoin Widget
     *
     * @return boolean
     */
    public boolean verifyPelajariText() {
        return selenium.waitInCaseElementPresent(pelajariText, 5) != null;
    }

    /**
     * Verify MamiPoin is = 0 on MamiPoin Widget
     *
     * @return boolean
     */
    public boolean verifyPointIs0() {
        return selenium.waitInCaseElementPresent(zeroPoint, 5) != null;
    }

    /**
     * Verify Tukar Poin Text displayed on MamiPoin Widget
     *
     * @return boolean
     */
    public boolean verifyTukarPoinText() throws InterruptedException {
        return selenium.waitInCaseElementPresent(tukarPoinText, 5) != null;

    }

    /**
     * Verify MamiPoin is >= 1 on MamiPoin Widget
     *
     * @return boolean
     */
    public boolean verifyPointIsNot0() {
        return selenium.waitInCaseElementPresent(point, 5) != null;
    }

    /**
     * Get MamiPoin Landing Page Onboarding Text
     *
     * @return text of mamipoin landing page onboarding
     */
    public String getMamiPoinLandingPageOnboardingText(){
        return selenium.getText(mamipointLandingPageOnboardingTooltipText);
    }

    /**
     * Get MamiPoin Reward History Onboarding Text
     *
     * @return text of mamipoin landing page onboarding
     */
    public String getRewardHistoryOnboardingText(){
        return selenium.getText(rewardHistoryOnBoarding);
    }

    /**
     * Get MamiPoin Point History Onboarding Text
     *
     * @return text of mamipoin landing page onboarding
     */
    public String getPointHistoryOnboardingText(){
        return selenium.getText(pointHistoryOnBoarding);
    }

    /**
     * Get MamiPoin Term and Condition Onboarding Text
     *
     * @return text of mamipoin landing page onboarding
     */
    public String getTermAndConditionOnboardingText(){
        return selenium.getText(termAndConditionOnBoarding);
    }

    /**
     * Get MamiPoin Redeem Point Onboarding Text
     *
     * @return text of mamipoin landing page onboarding
     */
    public String getRedeemPointOnboardingText(){
        return selenium.getText(redeemPointOnBoarding);
    }

    /**
     * Click on Next Button on MamiPoin landing Page Onboarding
     *
     * @throws InterruptedException
     */
    public void clickOnNextButton() throws InterruptedException {
        selenium.clickOn(nextButton);
        selenium.hardWait(3);
    }

    /**
     * Click on Finish Button on MamiPoin landing Page Onboarding
     *
     * @throws InterruptedException
     */
    public void clickOnFinishButton() throws InterruptedException {
        selenium.clickOn(finishButton);
        selenium.hardWait(3);
    }

    /**
     * Verify title in the mamipoin owner landing page is displayed
     * @return boolean
     */
    public Boolean isTitleInTheMamipoinOwnerLandingPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(titleMamipoinOwnerLandingPage);
    }

    /**
     * Verify tukar poin button is displayed
     * @return boolean
     */
    public Boolean isTukarPoinButtonDisplayed() throws InterruptedException {
        return selenium.isElementPresent(tukarPoinButton);
    }

    /**
     * Click on tukar poin button
     *
     * @throws InterruptedException
     */
    public void clickOnTukarPoinButton() throws InterruptedException {
        selenium.clickOn(tukarPoinButton);
    }

    /**
     * Verify riwayat hadiah button is displayed
     * @return boolean
     */
    public Boolean isRiwayatHadiahButtonDisplayed() throws InterruptedException {
        return selenium.isElementPresent(riwayatHadiahButton);
    }

    /**
     * Verify riwayat poin button is displayed
     * @return boolean
     */
    public Boolean isRiwayatPoinButtonDisplayed() throws InterruptedException {
        return selenium.isElementPresent(riwayatPoinButton);
    }

    /**
     * Verify riwayat poin button is displayed
     * @return boolean
     */
    public Boolean isSyaratDanKetentuanButtonDisplayed() throws InterruptedException {
        return selenium.isElementPresent(syaratDanKetentuanButton);
    }

    /**
     * Get Poin Anda Onboarding Text
     *
     * @return text of poin anda onboarding
     */
    public String getPoinAndaOnboardingText(){
        return selenium.getText(poinAndaOnboarding);
    }

    /**
     * Get Hadiah Bisa Ditukar Onboarding Text
     *
     * @return text of hadiah bisa ditukar onboarding
     */
    public String getHadiahBisaDitukarOnboardingText(){
        return selenium.getText(hadiahBisaDitukarOnboarding);
    }

    /**
     * Get Bantuan Onboarding Text
     *
     * @return text of bantuan onboarding
     */
    public String getBantuanOnboardingText(){
        return selenium.getText(bantuanOnboarding);
    }

    /**
     * Click on Selesai Button on Tukar Poin Page Onboarding
     *
     * @throws InterruptedException
     */
    public void clickOnSelesaiButton() throws InterruptedException {
        selenium.clickOn(selesaiButton);
        selenium.hardWait(3);
    }

    /**
     * Verify title in the tukar poin page is displayed
     * @return boolean
     */
    public Boolean isTitleInTheTukarPoinPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(titleTukarPoinPage);
    }

    /**
     * Verify logo in the tukar poin page is displayed
     * @return boolean
     */
    public Boolean isLogoInTheTukarPoinPageDisplayed() throws InterruptedException {
        return selenium.isElementPresent(logoTukarPoinPage);
    }

    /**
     * Verify the amount of Mamipoin Anda
     * @return amount of poin
     * @param poin
     */
    public String verifyAmountOfMamipoinAnda(String poin) {
        return selenium.getText(By.xpath("//strong[@class='info-total'][contains(text(),'"+ poin +"')]"));
    }

    /**
     * Verify bantuan button is displayed
     * @return boolean
     */
    public Boolean isBantuanButtonDisplayed() throws InterruptedException {
        return selenium.isElementPresent(bantuanButton);
    }

    /**
     * Click on Tab Riwayat Poin
     *
     * @throws InterruptedException
     */
    public void clickOnTabRiwayatPoin() throws InterruptedException {
        selenium.clickOn(tabRiwayatPoin);
        selenium.hardWait(3);
    }


    /**
     * Verify history deduction is displayed
     * @return boolean
     */
    public Boolean isHistoryDeductionDisplayed() throws InterruptedException {
        return selenium.isElementPresent(historyDeduction);
    }

    /**
     * Verify history deduction is displayed
     * @return boolean
     */
    public Boolean isHistoryAdditionDisplayed() throws InterruptedException {
        return selenium.isElementPresent(historyAddition);
    }

    /**
     * Get value from mamipoin owner
     *
     * @return integer mamipoin owner
     */
    public int getMamipoinOwnerText(){
        String  mamipoin = selenium.getText(pointOwner).substring(0,7).replaceAll("[.]","");
        return Integer.parseInt(mamipoin);
    }

    /**
     * Click icon back at mamipon owner dashboard
     *
     * @throws InterruptedException
     */
    public void clicOnIconBackMamipoin() throws InterruptedException {
        selenium.clickOn(iconBackMamipoinOwner);

    }

    /**
     * Get value from mamipoin owner < 100000
     *
     * @return integer mamipoin owner
     */
    public int getMamipoinOwnerLessThanText(){
        String  mamipoin = selenium.getText(pointOwner).substring(0,6).replaceAll("[. ]","");
        return Integer.parseInt(mamipoin);
    }

}