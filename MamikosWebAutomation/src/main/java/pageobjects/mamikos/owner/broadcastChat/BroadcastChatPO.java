package pageobjects.mamikos.owner.broadcastChat;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BroadcastChatPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public BroadcastChatPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(css = ".bg-c-button--md.bg-c-button--primary")
    private WebElement ajukanGantiPaketBtn;

    @FindBy(css = ".bg-c-button--tertiary")
    private WebElement lihatDetailPaketBtn;

    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--md bg-c-button--block']")
    private WebElement beliPaketBtn;

    @FindBy(xpath = "//div[@class='broadcast-chat-list__action']")
    private WebElement tambahBroadcastChatBtn;

    @FindBy(xpath = "//b[.='Bantuan & Tips']")
    private WebElement bantuantipsBtn;

    @FindBy(xpath = "//input[@class='bg-c-input__field']")
    private WebElement searchKostInputBC;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--title-3 ']")
    private WebElement invalidSearchKosBC;

    @FindBy(xpath = "//button[@class='bg-c-button broadcast-chat-list-item__detail-button bg-c-button--secondary bg-c-button--md']")
    private WebElement lihatRincianBtn;

    @FindBy(xpath = "//div[@class='broadcast-detail__info__send-message bg-c-card bg-c-card--lined bg-c-card--md bg-c-card--light']")
    private WebElement labelPesanTerkirim;

    @FindBy(css = ".bg-c-toast__content")
    private WebElement labelKosBelumMemilikiCalonPenerima;

    @FindBy(xpath = "//*[@data-testid='broadcastChat-selectKosButton']")
    private WebElement pilihKosBtn;

    @FindBy(css = ".bg-c-link--medium-naked")
    private WebElement masukanPesanBtn;

    @FindBy(css = ".broadcast-chat-select-message > div:nth-of-type(2) .broadcast-chat-message-card__message")
    private WebElement firstOptionMsg;

    @FindBy(css = ".broadcast-chat-select-message > div:nth-of-type(3) .broadcast-chat-message-card__message")
    private WebElement secondOptionMsg;

    @FindBy(css = ".bg-c-button[data-v-9f41e758]")
    private WebElement pilihPesanBtn;

    @FindBy(xpath = "//textarea[@class='bg-c-textarea__field bg-c-textarea__field--lg']")
    private WebElement broadcastMsgInput;

    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--md']")
    private WebElement previewPesanBtn;

    @FindBy(css = "b:nth-of-type(2)")
    private WebElement previewBCMessage;

    @FindBy(css = ".bg-c-field__message")
    private WebElement errorInputBCTemplateMsg;

    @FindBy(xpath = "//div[@class='broadcast-chat-detail__select-message__inner']/a[.='Ubah']")
    private WebElement ubahBCTemplateMsgBtn;

    @FindBy(xpath = "//button[contains(text(),'Tidak Jadi')]")
    private WebElement tidakJadiButton;

    @FindBy(xpath = "//button[contains(text(),'Keluar')]")
    private WebElement keluarButton;

    @FindBy(css = ".bg-c-icon--xl")
    private WebElement backButtonBC;

    /**
     * click on button ajukan ganti paket
     * @throws InterruptedException
     */
    public void isAjukanGantiPaketBtnDisplay() throws InterruptedException {
        selenium.isElementDisplayed(ajukanGantiPaketBtn);
    }

    /**
     * click on button lihat detail paket
     * @throws InterruptedException
     */
    public void isLihatDetailPaketDisplay() throws InterruptedException {
        selenium.isElementDisplayed(lihatDetailPaketBtn);
    }

    /**
     * click on button lihat detail paket
     * @throws InterruptedException
     */
    public void clickonlihatDetailPaket() throws InterruptedException {
        selenium.clickOn(lihatDetailPaketBtn);
    }

    /**
     * verify button lihat detail paket is present
     * @throws InterruptedException
     */
    public void isLihatDetailPaketDisplayed() throws InterruptedException {
        selenium.isElementDisplayed(lihatDetailPaketBtn);
    }

    /**
     * verify button beli paket is present
     * @throws InterruptedException
     */
    public void isBeliPaketDisplayed() throws InterruptedException {
        selenium.isElementDisplayed(beliPaketBtn);
    }

    /**
     * click on button tambah broadcast chat
     * @throws InterruptedException
     */
    public void clickontambahbroadcastchat() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(tambahBroadcastChatBtn);
    }

    /**
     * click on button bantuan & tips
     * @throws InterruptedException
     */
    public void clickonbantuantips() throws InterruptedException {
        selenium.clickOn(bantuantipsBtn);
        selenium.hardWait(3);
        selenium.switchToWindow(2);
    }

    /**
     * Wait till element Input Search Kost BC visible
     * Insert text to search kost BroadcastChat
     */
    public void searchKostBC(String text) {
        selenium.waitTillElementIsVisible(searchKostInputBC);
        selenium.enterText(searchKostInputBC, text, true);
        searchKostInputBC.sendKeys(Keys.ENTER);
    }

    /**
     * Get message invalid search kost
     * @return error message invalid search kost
     */
    public String getInvalidSearchKostMessage() {
        return selenium.getText(invalidSearchKosBC);
    }

    /**
     * click on button lihat rincian broadcast chat
     * @throws InterruptedException
     */
    public void clickonLihatRincianButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(lihatRincianBtn);
        selenium.clickOn(lihatRincianBtn);
    }

    /**
     * verify label lihat detail paket is present
     * @throws InterruptedException
     */
    public void isLabelPesanTerkirimDisplayed() throws InterruptedException {
        selenium.isElementDisplayed(labelPesanTerkirim);
    }

    /**
     * Get label Kos Belum Memiliki Calon Penerima
     * @return Kos belum memiliki calon penerima
     */
    public String getlabelKosBelumMemilikiCalonPenerimaLabel(){
        return selenium.getText(labelKosBelumMemilikiCalonPenerima);
    }

    /**
     * click on button pilih kos
     * @throws InterruptedException
     */
    public void clickonPilihKosButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(pilihKosBtn);
        selenium.clickOn(pilihKosBtn);
    }

    /**
     * click on button masukan pesan broadcast chat
     * @throws InterruptedException
     */
    public void clickOnMasukanPesanButton() throws InterruptedException {
        selenium.waitTillElementIsClickable(masukanPesanBtn);
        selenium.clickOn(masukanPesanBtn);
    }

    /**
     * click on button masukan pesan broadcast chat
     * @throws InterruptedException
     */
    public void selectFirstOptionBC() throws InterruptedException {
        selenium.waitTillElementIsClickable(firstOptionMsg);
        selenium.clickOn(firstOptionMsg);
    }

    /**
     * click on button masukan pesan broadcast chat
     * @throws InterruptedException
     */
    public void clickOnpilihPesanBtn() throws InterruptedException {
        selenium.waitTillElementIsClickable(pilihPesanBtn);
        selenium.clickOn(pilihPesanBtn);
    }

    /**
     * Wait till element Input Search Kost BC visible
     * Insert text to search kost BroadcastChat
     */
    public void inputBCmessage(String text) throws InterruptedException {
        selenium.waitTillElementIsVisible(broadcastMsgInput);
        selenium.enterText(broadcastMsgInput, text, true);
        broadcastMsgInput.sendKeys(Keys.ENTER);
        selenium.hardWait(4);
    }

    /**
     * click on button masukan pesan broadcast chat
     * @throws InterruptedException
     */
    public void clickOnPreviewPesanBtn() throws InterruptedException {
        selenium.waitTillElementIsClickable(previewPesanBtn);
        selenium.clickOn(previewPesanBtn);
    }

    /**
     * Get Preview Message Broadcast Chat
     * @return Preview Message Broadcast Chat
     */
    public String getPreviewMessageBC() {
        return selenium.getText(previewBCMessage);
    }

    /**
     * Get error Input Template Broadcast Chat Template Message
     * @return error Input Template Broadcast Chat Template Message
     */
    public String getErrorInputBC() {
        return selenium.getText(errorInputBCTemplateMsg);
    }

    /**
     * click on button ubah template message button
     * @throws InterruptedException
     */
    public void clickOnUbahTemplateMsg() throws InterruptedException {
        selenium.waitTillElementIsVisible(ubahBCTemplateMsgBtn);
        selenium.clickOn(ubahBCTemplateMsgBtn);
    }

    /**
     * click on second template message BC
     * @throws InterruptedException
     */
    public void clickOnSecondOptionMessage() throws InterruptedException {
        selenium.waitTillElementIsVisible(secondOptionMsg);
        selenium.clickOn(secondOptionMsg);
    }

    /**
     * verify textfield broadcast chat message is not displayed
     * @throws InterruptedException
     */
    public void isTextFieldBCMessageDisplayed() throws InterruptedException {
        selenium.isElementDisplayed(broadcastMsgInput);
    }

    /**
     * verify kost card is disable
     *
     * @throws InterruptedException
     */
    public boolean isKostCardDisabled(String menu) throws InterruptedException {
        selenium.hardWait(2);
        WebElement kostCard = driver.findElement(By.xpath("//*[contains(text(),'"+menu+"')]"));
        return kostCard.isDisplayed() && kostCard.isEnabled();
    }

    /**
     * click on Tidak Jadi Button BC
     * @throws InterruptedException
     */
    public void clickOnTidakJadiBtn() throws InterruptedException {
        selenium.waitTillElementIsVisible(tidakJadiButton);
        selenium.clickOn(tidakJadiButton);
    }

    /**
     * click on Tidak Jadi Button BC
     * @throws InterruptedException
     */
    public void clickOnKeluarButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(keluarButton);
        selenium.clickOn(keluarButton);
        selenium.hardWait(5);
    }

    /**
     * click on back Button BC
     * @throws InterruptedException
     */
    public void clickOnBackButtonBC() throws InterruptedException {
        selenium.waitTillElementIsVisible(backButtonBC);
        selenium.clickOn(backButtonBC);
    }
}
