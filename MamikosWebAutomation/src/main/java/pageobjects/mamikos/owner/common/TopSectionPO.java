package pageobjects.mamikos.owner.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import utilities.Constants;
import utilities.SeleniumHelpers;

public class TopSectionPO {

	WebDriver driver;
	SeleniumHelpers selenium;

	public TopSectionPO(WebDriver driver) {
		this.driver = driver;
		selenium = new SeleniumHelpers(driver);

		// This initElements method will create all WebElements
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
	}

	/*
	 * All WebElements are identified by @FindBy annotation
	 * 
	 * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
	 * className, xpath as attributes.
	 */

	@FindBy(css = "a.btn-mamigreen")
	private WebElement addDataButton;

	@FindBy(id = "ownerAddBtnLg")
	private WebElement addButton;

	
	/**
	 * Click on 'Add Data' button
	 * 
	 * @throws InterruptedException
	 */
	public void clickOnAddDataButton() throws InterruptedException {
		selenium.waitInCaseElementClickable(addDataButton,20);
		selenium.clickOn(addDataButton);
	}

	/**
	 * Click on 'Add' button
	 * 
	 * @throws InterruptedException
	 */
	public void clickOnAddButton() throws InterruptedException {
		selenium.clickOn(addButton);
	}

}
