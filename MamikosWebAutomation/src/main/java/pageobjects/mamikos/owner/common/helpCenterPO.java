package pageobjects.mamikos.owner.common;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.ArrayList;
import java.util.List;

public class helpCenterPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public helpCenterPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(xpath = "//*[text()='Pusat Bantuan']")
    private WebElement helpCenter;

    /**
     * Click help center
     *
     * @throws InterruptedException
     */
    public void clickhelpCenter() throws InterruptedException {
        selenium.clickOn(helpCenter);
        selenium.hardWait(5);
    }
}



