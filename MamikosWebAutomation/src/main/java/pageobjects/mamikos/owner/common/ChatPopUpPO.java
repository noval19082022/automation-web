package pageobjects.mamikos.owner.common;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ChatPopUpPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ChatPopUpPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    //send button in question chat pop up
    @FindBy(css = ".btn-modal-chat")
    private WebElement sendQuestion;

    //latest chat message
    @FindBy(xpath = "(//div[@class='mc-balloon-chat__content']/div)[last()]")
    private WebElement latestChat;

    //chat textbox to write chat
    @FindBy(css = "div[contenteditable='true']")
    private WebElement chatTextBox;

    //send button in chat textbox
    @FindBy(xpath = "//div[@class='mc-typing-bar']//button")
    private WebElement sendChat;

    //chat button in owner
    @FindBy(css = ".widget-ic")
    private WebElement ownerChatBtn;

    //back button in chat room
    @FindBy(xpath = "(//div[@class = 'mc-chat-room__header']//button)[1]")
    private WebElement backBtn;

    //close button in chat room
    @FindBy(xpath = "//div[@class = 'mc-channel-list__header']//button")
    private WebElement closeBtn;

    //whole chat list page
    @FindBy(xpath = "//div[@class='wrapper-question']")
    private WebElement chatChannel;

    @FindBy(xpath = "//button[normalize-space()='Hubungi Pengelola']")
    private WebElement chatChannel2;

    //room card in the top of detail room chat
    @FindBy(css = ".mc-product-link-card .mc-product-link-card__content")
    private WebElement roomCard;

    @FindBy(css = "body .chat-sheet .mc-chat-room__header .mc-chat-room__header-content .bg-c-tooltip__target p")
    private WebElement roomCardTitleText;

    @FindBy(xpath = "//a[contains(.,'Lihat Iklan')]")
    private WebElement seeAdsButton;

    @FindBy(className = "product-link-to-booking")
    private WebElement bookingButton;

    @FindBy(css = "[alt='chat kosong']")
    private WebElement emptyChatImage;

    @FindBy(css = ".bg-c-empty-state__title")
    private WebElement emptyChatTitle;

    @FindBy(css = ".bg-c-empty-state__description")
    private WebElement emptyChatDesc;

    @FindBy(css = ".mk-chat__unread")
    private WebElement emptyChatIndicator;

    @FindBy(xpath = "//div[@class = 'mc-product-link-card__CTA']/button[contains(text(),'Ajukan Sewa')]")
    private WebElement roomCardBookingButton;

    // Chat CS (Contact us)
    @FindBy(id = "webWidget")
    private WebElement contactUsPopUp;

    @FindBy(xpath = "//button[@class='bg-c-button track_request_booking bg-c-button--primary bg-c-button--sm'][@disabled]")
    private WebElement disabledRoomCardBookingButton;

    @FindBy(xpath = "(//*[@class='bg-c-button__icon bg-c-icon bg-c-icon--md'])[1]")
    private WebElement broadcastChatBtn;

    @FindBy(css = "input[data-testid='hiddenInputImage']")
    private WebElement inpImage;

    @FindBy(css = ".mc-image-uploader-modal__typing-bar button.mc-typing-bar__send-button svg")
    private WebElement btnSendImage;

    By imgSent = By.cssSelector("img[alt='lampiran gambar']");

    @FindBy(css = "div .mc-activity-indicator__loading")
    private WebElement imgLoading;

    @FindBy(css = "select")
    private WebElement dropDownWaktuSurvei;

    @FindBy(xpath = "(//div[@class='mc-balloon-chat__content']/div)[last()-1]")
    private WebElement surveyKostSubmited;

    @FindBy(xpath ="//*[contains(@class, 'form-survey')] //button[contains(text(), 'Ubah Jadwal')]" )
    private WebElement btnUbahJadwal;

    //---------------chat label----------------//
    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--body-2 '][contains(., 'Chat')]")
    private WebElement openChatOnHeader;

    private By chatRoom = By.xpath("//div[@class='mc-channel-list']");

    @FindBy(xpath = "//div[@class='mc-channel-list__channels']/child::*[1]")
    private WebElement chatList;

    private By chatDetailRoom = By.xpath("//div[@class='mc-chat-room__header']");

    @FindBy(xpath = "//div[@data-testid='booking-status-label'][contains(., 'Butuh respon pengajuan sewa')]")
    private WebElement chatLabel;

    @FindBy(xpath = "//button[contains(text(), 'Ubah Jadwal')]")
    private WebElement buttonUbahJadwalOnHeader;

    @FindBy(xpath = "//button[contains(text(), 'Batalkan Survei')]")
    private WebElement buttonBatalkanSurvei;

    @FindBy(xpath = "//button[contains(text(), 'Survei Kos')]")
    private WebElement buttonSurveiKos;

    @FindBy(xpath = "//div[@class='membership-card__title']")
    private WebElement nonGPLabel;

    @FindBy(xpath = "//p[@class='mc-chat-room__status bg-c-text bg-c-text--label-4 ']")
    private WebElement ownerLastSeenChatroom;

    @FindBy(xpath = "(//p[@class='mc-balloon-chat__sender-name bg-c-text bg-c-text--body-3 '])[last()]")
    private WebElement lastChatHeader;

    //---------------FTUE Continue Send Chat----------------//
    @FindBy(xpath = "//button[@class='bg-c-modal__action-closable']")
    private WebElement closeBtnFTUESendChat;

    @FindBy(xpath = "//p[@class='bg-c-modal__body-description']")
    private WebElement FTUESendChatDesc;

    @FindBy(xpath = "//button[contains(text(),'Kembali')]")
    private WebElement backBtnFTUESendChat;

    @FindBy(xpath = "//button[contains(text(),'Lanjut kirim chat')]")
    private WebElement continueBtnFTUESendChat;

    @FindBy(xpath = "(//h6[contains(., 'Raney Arora')])")
    private WebElement noChatRoom;


    /**
     * List all questions in pop up
     * @return list questions
     */
    public List<String> listQuestions() {
        String xpathLocator = "//*[contains(@class, 'question-option')]/label/p";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        List<String> questionsList = new ArrayList<>();
        for (WebElement s : elements) {
            questionsList.add((selenium.getText(s)).replaceAll("\\s", ""));
        }
        return questionsList;
    }

    /**
     * Click one of question in radio button
     * @param text is position from top
     * @throws InterruptedException
     */
    public void clickQuestion(String text) throws InterruptedException {
        String xpathLocator = "//p[contains(.,'" + text + "')]";
        selenium.javascriptClickOn(By.xpath(xpathLocator));
    }

    /**
     * Click send in question pop up
     * @throws InterruptedException
     */
    public void clickSend() {
        selenium.javascriptClickOn(sendQuestion);
    }

    /**
     * Get latest chat
     * @return String latest chat (most bottom chat)
     */
    public String getLatestChatText() throws InterruptedException {
        selenium.waitInCaseElementVisible(latestChat, 5);
        selenium.pageScrollInView(latestChat);
        selenium.hardWait(7);
        return selenium.getText(latestChat);

    }

    /**
     * Verify button label in question pop up is correct
     * @param expectedLabel is expected text in the button
     * @return boolean
     */
    public boolean verifySendLabel(String expectedLabel) {
        return selenium.getText(sendQuestion).replaceAll("\\s", "").equals(expectedLabel);
    }

    /**
     * Enter text to textbox
     * @param message is text we want to enter
     * Hit send after enter message
     */
    public void insertChatText(String message) throws InterruptedException {
        selenium.waitInCaseElementVisible(chatTextBox, 9);
        selenium.hardWait(3);
        selenium.enterText(chatTextBox, message, false);
        chatTextBox.sendKeys(Keys.ENTER);
    }

    /**
     * Click chat room in chat list
     * @throws InterruptedException
     * @param text is chat room title
     */
    public void clickChatRoomRoom(String text) throws InterruptedException {
        selenium.hardWait(2);
        String xpathLocator = "(//h6[contains(., '" + text + "')])[1]";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        selenium.clickOn(elements.get(0));
    }

    /**
     * Click chat room in chat list
     * @throws InterruptedException
     * @param text is chat room title
     */
    public void clickChatRoomRoom2(String text) throws InterruptedException {
        String xpathLocator = "//div[@id='channel-list-wrapper']/div[1]//h6[contains(.,'Raney Arora')]";
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(By.xpath(xpathLocator));
        selenium.clickOn(elements.get(0));
    }

    public boolean clickNoChatRoomRoom() {
        return selenium.isElementNotDisplayed(noChatRoom);
    }

    /**
     * Click back in chatroom
     * @throws InterruptedException
     */
    public void clickBack() throws InterruptedException {
        selenium.javascriptClickOn(backBtn);
    }

    /**
     * Click close in chatroom list
     * @throws InterruptedException
     */
    public void clickClose() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(closeBtn, 3) != null) {
            selenium.javascriptClickOn(closeBtn);
            selenium.hardWait(3);
        }
    }

    /**
     * Check whether chat list appear/not
     * @return true if chatlist present
     * @throws InterruptedException
     */
    public boolean isChatListPresent() {
        return selenium.waitInCaseElementVisible(chatChannel, 5) != null;
    }

    /**
     * Check whether chat list appear/not
     * @return true if chatlist present
     * @throws InterruptedException
     */
    public boolean isChatListPresent2() {
        return selenium.waitInCaseElementVisible(chatChannel2, 5) != null;
    }

    /**
     * Check whether room card appear/not
     * @return true if room card present
     */
    public boolean isRoomCardPresent() {
        return selenium.waitInCaseElementVisible(roomCard, 3) != null;
    }

    /**
     * Get the room card title property
     * @return room card title text
     */
    public String getRoomCardPropName() {
        return selenium.getText(roomCardTitleText);
    }

    /**
     * Click Lihat Iklan button
     * @throws InterruptedException
     */
    public void clickLihatIklanButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(seeAdsButton, 3);
        selenium.javascriptClickOn(seeAdsButton);
    }

    /**
     * Click Booking button
     * @throws InterruptedException
     */
    public void clickBookingButton() throws InterruptedException {
        selenium.javascriptClickOn(roomCardBookingButton);
    }

    /**
     * Check booking button disable is present
     * @return boolean
     */
    public boolean isBookingButtonDisablePresent() {
        return selenium.waitInCaseElementVisible(disabledRoomCardBookingButton, 3) != null;
    }

    /**
     * Check contact us pop up is present
     * @return true if appear
     */
    public boolean isContactUsPresent() {
        return selenium.waitInCaseElementVisible(contactUsPopUp, 3) != null;
    }

    /**
     * Check empty chat image is present
     * @return true if appear
     */
    public boolean isEmptyChatImagePresent() {
        return selenium.waitInCaseElementVisible(emptyChatImage, 3) != null;
    }

    /**
     * Get empty chat title
     * @return empty chat title
     */
    public String getEmptyChatTitle() {
        return selenium.getText(emptyChatTitle);
    }

    /**
     * Get empty chat description
     * @return empty chat description
     */
    public String getEmptyChatDescription() {
        return selenium.getText(emptyChatDesc);
    }

    /**
     * Get empty chat indicator
     * @return empty chat indicator text
     */
    public String getEmptyChatIndicator() {
        return selenium.getText(emptyChatIndicator);
    }

    /**
     * Click Broadcast Chat Entry Point from Chat Page
     * @throws InterruptedException
     */
    public void clickOnBCChatPage() throws InterruptedException {
        selenium.clickOn(broadcastChatBtn);
    }
    /**
     * Click chatroom
     * @throws InterruptedException
     */
    public void clickChatIndexNumber(String index) throws InterruptedException {
        String chatList = "#channel-list-wrapper > div:nth-of-type("+index+")";
        selenium.waitTillElementIsClickable(By.cssSelector(chatList));
        selenium.clickOn(By.cssSelector(chatList));
        selenium.hardWait(3);
    }

    /**
     * Check quota
     * @throws InterruptedException
     */
    public String getTextLastQuota() throws InterruptedException {
        String quota = ".mc-chat-room-quota-info__message-subtitle > .bg-c-text";
        return selenium.getText(By.cssSelector(quota));
    }

    /**
     * Free quota
     * @throws InterruptedException
     */
    public String getTextFreeQuota() throws InterruptedException {
        String quota = "//p[@class='bg-c-text bg-c-text--label-1 ']";
        return selenium.getText(By.xpath(quota));
    }

    public void uploadImageChat() throws InterruptedException {
        String projectPath = System.getProperty("user.dir");
        String imagePath = "/src/main/resources/images/anime.jpeg";
        File file = new File(projectPath + imagePath);
        inpImage.sendKeys(file.getAbsolutePath());
        selenium.hardWait(30);
    }

    /**
     * Click on send image button, on send image pop up confirmation
     */
    public void clicksOnSendImageButton() throws InterruptedException {
        selenium.clickOn(btnSendImage);
    }

    /**
     * Get integer from image sent element size.
     * @return integer data type
     */
    public int getImageSentSize() {
        return selenium.waitTillAllElementsAreLocated(imgSent).size();
    }

    /**
     * wait till image loading dissapear
     */
    public void waitTillImageLoadingDissapear() throws InterruptedException {
        do {
            selenium.hardWait(1);
        }
        while (selenium.waitInCaseElementClickable(imgLoading, 10) != null);
    }

    /**
     * Change Survey Time
     * @param time
     * @throws InterruptedException
     */
    public void inputSurveyTime(String time) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
        selenium.waitInCaseElementVisible(dropDownWaktuSurvei,10);
        selenium.selectDropdownValueByText(dropDownWaktuSurvei, time);
    }

    /**
     * Click button ubah jadwal on survey form
     * @throws InterruptedException
     */
    public void clickBtnUbahJadwal() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(btnUbahJadwal);
        selenium.waitTillElementIsNotPresent(btnUbahJadwal,5);
    }

    /**
     * Get submitted form chat
     * @return String latest chat (most bottom chat)
     */
    public String getFormSurveyInputed() throws InterruptedException {
        selenium.hardWait(7);
        selenium.waitInCaseElementVisible(surveyKostSubmited, 5);
        return selenium.getText(surveyKostSubmited);
    }

    /**
     * Check if question list displayed
     * @return true if appear
     */
    public boolean isQuestionDisplayed(String question) {
        String xpathLocator = "//p[contains(.,'" + question + "')]";
        return selenium.waitInCaseElementVisible(By.xpath(xpathLocator), 1) != null;
    }

    /**
     * click chat on header dashboard
     */
    public void clickChatOnHeader() throws InterruptedException {
        selenium.clickOn(openChatOnHeader);
    }

    /**
     * click very first chat on chat list
     */
    public void clickChatList() throws InterruptedException {
        selenium.waitInCaseElementPresent(chatRoom, 2);
        selenium.clickOn(chatList);
    }

    /**
     * check chat label
     * e.g butuh respon pengajuan sewa
     */
    public String getChatLabel(String label) throws InterruptedException {
        selenium.waitInCaseElementPresent(chatDetailRoom, 2);
        String element = "//div[@data-testid='booking-status-label'][contains(., '" + label + "')]";
        return selenium.getText(By.xpath(element));
    }

    /**
     * Click on batalkan survey if the survey already submitted
     * @throws InterruptedException
     */
    public void clickOnBatalkanSurvey() throws InterruptedException {
        selenium.hardWait(7);
        if (selenium.isElementDisplayed(buttonUbahJadwalOnHeader)) {
            selenium.clickOn(buttonUbahJadwalOnHeader);
            selenium.clickOn(buttonBatalkanSurvei);
        }
    }

    /**
     * Click on Survei Kos button
     * @throws InterruptedException
     */
    public void clickOnSurveiKosButton() throws InterruptedException {
        selenium.hardWait(7);
        selenium.clickOn(buttonSurveiKos);
    }
    /**
     * owner as nonGP
     * @throws InterruptedException
     */
    public String getNonGP() throws InterruptedException {
        return selenium.getText(nonGPLabel);
    }

    /**
     * Check owner last online on chatroom is displayed
     *
     * @return status true / false
     */
    public boolean isOwnerLastSeenChatroomDisplayed() {
        return selenium.waitInCaseElementVisible(ownerLastSeenChatroom, 3)!= null;
    }

    /**
     * Get header chat text
     * @return String header chat text (most bottom chat)
     */
    public String getHeaderChatText() throws InterruptedException {
        selenium.waitInCaseElementVisible(lastChatHeader, 5);
        selenium.pageScrollInView(lastChatHeader);
        selenium.hardWait(7);
        return selenium.getText(lastChatHeader);
    }

    /**
     * Verify is close button in FTUE Send Chat present
     * return boolean true or false
     */
    public boolean isCloseBtnFTUESendChatPresent() {
        return selenium.waitInCaseElementVisible(closeBtnFTUESendChat, 3) != null;
    }

    /**
     * Verify is FTUE Send Chat Description present
     * return boolean true or false
     */
    public boolean isDescFTUESendChatPresent() {
        return selenium.waitInCaseElementVisible(FTUESendChatDesc, 3) != null;
    }

    /**
     * Verify is back button in FTUE Send Chat present
     * return boolean true or false
     */
    public boolean isBackBtnFTUESendChatPresent() {
        return selenium.waitInCaseElementVisible(backBtnFTUESendChat, 3) != null;
    }

    /**
     * Verify is Continue button in FTUE Send Chat present
     * return boolean true or false
     */
    public boolean isContinueBtnFTUESendChatPresent() {
        return selenium.waitInCaseElementVisible(continueBtnFTUESendChat, 3) != null;
    }

    /**
     * Click back in FTUE Continue Send Chat
     * @throws InterruptedException
     */
    public void clickBackFTUESendChat() {
        selenium.javascriptClickOn(backBtnFTUESendChat);
    }

    /**
     * Click send in question pop up
     * @throws InterruptedException
     */
    public void clickSendChat() {
        selenium.javascriptClickOn(sendChat);
    }
}
