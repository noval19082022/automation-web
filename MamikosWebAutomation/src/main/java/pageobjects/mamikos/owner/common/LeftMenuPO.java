package pageobjects.mamikos.owner.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class LeftMenuPO {

	WebDriver driver;
	SeleniumHelpers selenium;

	public LeftMenuPO(WebDriver driver) {
		this.driver = driver;
		selenium = new SeleniumHelpers(driver);

		// This initElements method will create all WebElements
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
	}

	/*
	 * All WebElements are identified by @FindBy annotation
	 * 
	 * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
	 * className, xpath as attributes.
	 */

	@FindBy(xpath = "//p[.='Apartemen']")
	private WebElement ownerApartmentMenu;

	@FindBy(xpath = "//*[text()='BARU']")
	private WebElement baruText;

	@FindBy(className = "user-menu")
	private WebElement userMenu;

	@FindBy(xpath = "//p[text()='Pengajuan Booking']")
	private WebElement bookingRequestMenu;

	@FindBy(xpath = "//p[.='Pengajuan Booking']")
	private WebElement bookingAndBillingMenu;

	@FindBy(xpath = "//*[@class='link-text-menu' and contains(text(),'Status Booking Langsung')]")
	private WebElement statusBookingLangsungMenu;

	@FindBy(xpath = "//p[.='Kos']")
	private WebElement ownerKosMenuButton;

	@FindBy(className = "c-mk-header-user__setting")
	private WebElement userSettings;

	@FindBy(xpath = "//a[.='Permintaan Booking']\n")
	private WebElement permintaanBookingMenu;

	@FindBy(xpath = "//p[text()='Manajemen Kos']")
	private WebElement manajemenKosMenu;

	private By tooltipNew = By.cssSelector("#__layout div.highlight-info.highlight-info.--top div.badge-new");

	@FindBy(xpath = "//p[contains(.,'Home')]")
	private WebElement homeButtonOwnerMenu;

	@FindBy(xpath = "//*[.='Kelola Tagihan']")
	private WebElement btnManageBill;

	@FindBy(xpath = "//p[.='Properti Saya']")
	private WebElement propertiSayaMenu;

	@FindBy(xpath ="//p[contains(.,'Laporan Keuangan')]")
	private WebElement financialReportMenu;

	/**
	 * Click on Owner Apartment Menu
	 * 
	 * @throws InterruptedException
	 */
	public void clickOnOwnerApartmentMenu() throws InterruptedException {
		selenium.hardWait(3);
		selenium.clickOn(ownerApartmentMenu);
	}

	/**
	 * Click on 'Baru' text
	 * 
	 * @throws InterruptedException
	 */
	public void clickOnBaruText() throws InterruptedException {
		selenium.clickOn(baruText);
		selenium.switchToWindow(1);
	}

	/**
	 * Click on Booking Request Menu from Left Bar
	 * @throws InterruptedException
	 */
	public void clickOnBookingRequestButton() throws InterruptedException {
		selenium.javascriptClickOn(bookingRequestMenu);
	}

	/**
	 * Click on Booking and Billing menu
	 * @throws InterruptedException
	 */
	public void clickOnBookingAndBillingMenu() throws InterruptedException {
		selenium.waitTillElementIsClickable(bookingAndBillingMenu);
		selenium.hardWait(5);
		selenium.javascriptClickOn(bookingAndBillingMenu);
		selenium.waitForJavascriptToLoad();
	}

	/**
	 * Click on Kos button
	 * @throws InterruptedException
	 */
	public void clickOnKosButton() throws InterruptedException {
		selenium.hardWait(3);
		selenium.clickOn(ownerKosMenuButton);
	}

	/**
	 * Click on "BARU" tooltip
	 * @throws InterruptedException
	 */
	public void clickOnNewTooltip() throws InterruptedException {
		selenium.clickOn(tooltipNew);
	}

	/**
	 * Click on Status Booking Langsung
	 * @throws InterruptedException
	 */
	public void clickOnStatusBookingLangsungMenu() throws InterruptedException {
		selenium.waitTillElementIsClickable(statusBookingLangsungMenu);
		selenium.hardWait(5);
		selenium.javascriptClickOn(statusBookingLangsungMenu);
	}

	/**
	 * Click on Permintaan Booking
	 * @throws InterruptedException
	 */
	public void clickOnPermintaanBookingMenu() throws InterruptedException {
		selenium.waitTillElementIsClickable(permintaanBookingMenu);
		selenium.hardWait(5);
		selenium.javascriptClickOn(permintaanBookingMenu);
	}

	/**
	 * Click on Kost Management Menu
	 *
	 * @throws InterruptedException
	 */
    public void clicOnKostManagementMenu() throws InterruptedException {
    	selenium.hardWait(2);
		selenium.javascriptClickOn(manajemenKosMenu);
    }

	/**
	 * Check if home button from owner side menu is present
	 * @return true if present otherwise false
	 */
	private boolean isHomeButtonOwnerMenuPresent() {
		return selenium.waitInCaseElementVisible(homeButtonOwnerMenu, 3) != null;
	}

	/**
	 * Click on home button owner menu on the left bar
	 * @throws InterruptedException
	 */
	public void clickOnHomeOwnerLeftMenu() throws InterruptedException {
		int i = 0;
		while (isHomeButtonVisible()) {
			selenium.hardWait(2);
			if(i == 15) {
				break;
			}
			i++;
		}
		selenium.clickOn(homeButtonOwnerMenu);
	}

	/**
	 * Wait and check if element home is visible
	 * @return home visible is true, otherwise false
	 */
	private boolean isHomeButtonVisible() {
		return selenium.waitInCaseElementVisible(homeButtonOwnerMenu, 3) != null;
	}

	/**
	 * Click on manage bill button to go to manage bill page
	 * @throws InterruptedException
	 */
	public void clickOnManageBillLeftMenu() throws InterruptedException {
		selenium.hardWait(2);
		selenium.clickOn(btnManageBill);
	}

	/**
	 * Click on Properti Saya Menu
	 * @throws InterruptedException
	 */
	public void clickOnPropertiSayaMenu() throws InterruptedException {
		selenium.waitInCaseElementVisible(propertiSayaMenu,3);
		selenium.clickOn(propertiSayaMenu);
	}

	/**
	 * Click on Laporan Keuangan menu
	 * @throws InterruptedException
	 */
	public void clickOnfinancialReportMenu() throws InterruptedException{
		selenium.clickOn(financialReportMenu);
	}
}
