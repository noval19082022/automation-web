package pageobjects.mamikos.owner.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class AddPopUpPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AddPopUpPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "label[for='addListingApartment']")
    private WebElement appartmentRadioButton;

    @FindBy(xpath = "//button[contains(text(), 'Tambahkan Data')]")
    private WebElement addButton;

//    @FindBy(css = ".all-properties")
//    private WebElement updateAllKos;

    private By updateAllKos = By.cssSelector(".all-properties");

    @FindBy(xpath = "//*[contains(text(), 'Instan Booking')]")
    private WebElement instantBookingPopUp;

    @FindBy(css = ".premium-trial-modal__ok-button")
    private WebElement popUpFreePremiumOKButton;

    @FindBy(css = ".highlight-opens")
    private WebElement closeInstanBookingPopUpInOwnerHome;

    @FindBy(css = ".highlight-info__daily-allocation")
    private WebElement alokasiIklanHarianLabel;

    @FindBy(css = ".highlight-button")
    private WebElement okeAlokasiIklanHarianButton;

    @FindBy(css = ".daily-balance-allocation-body")
    private WebElement dailyBalanceAllocation;

//    @FindBy(xpath = "//*[@class='update-price__action-button']//*[contains(text(), 'Saya Mengerti')]")
//    private WebElement iUnderstandButtonAdditionalPrice;

    private By iUnderstandButtonAdditionalPrice = By.xpath("//*[@class='update-price__action-button']//*[contains(text(), 'Saya Mengerti')]");

    @FindBy(css = ".bg-c-button--md.bg-c-button--primary")
    private WebElement doneButtonEditKosPopUp;

    @FindBy(css = ".bg-c-button--md.photo-submission-tips-footer__button-next")
    private WebElement nextBtnPhotoSubmissionPopUp;

//    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--tertiary bg-c-button--md']")
//    private WebElement editDataLainButton;

    private By editDataLainButton = By.xpath("//button[@class='bg-c-button bg-c-button--tertiary bg-c-button--md']");

    @FindBy(css = "c-mk-card__body changes-interception")
    private WebElement popUpAttentionInAddKos;

    @FindBy(xpath= "//button[@class='bg-c-button changes-interception__button bg-c-button--primary bg-c-button--md']")
    private WebElement continueInputDataButton;

    @FindBy(xpath= "//button[normalize-space()='Edit Data Lain']")
    private WebElement editOtherDataPopUp;

    @FindBy(xpath = "//button[@class='bg-c-button changes-interception__button bg-c-button--tertiary bg-c-button--md']")
    private WebElement movePageButton;

//    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--tertiary bg-c-button--md gp-intercept__button--close']")
//    private WebElement GpPopup;

    private By GpPopup = By.xpath("//button[@class='bg-c-button bg-c-button--tertiary bg-c-button--md gp-intercept__button--close']");

    @FindBy(className = "owner-intercept-booking-modal__action-button")
    private WebElement learnInstantBookingButton;

    @FindBy(className = "owner-intercept-booking-modal__close-button")
    private WebElement closeLearnInstantBookingButton;

    private final By loadingAddKosPage = By.className("loading-step-desktop");


    /**
     * Select apartment option and click on add button
     *
     * @throws InterruptedException
     */
    public void selectAppartmentOptionAndClickOnAddButton() throws InterruptedException {
        selenium.clickOn(appartmentRadioButton);
        selenium.clickOn(addButton);
    }

    /**
     * Click on Update All Kos from update Popup
     *
     * @throws InterruptedException
     */
    public void clickOnUpdateAllKosButton() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(updateAllKos, 3) != null) {
            selenium.clickOn(updateAllKos);
            selenium.waitInCaseElementVisible(updateAllKos, 9);
            selenium.hardWait(3);
        }
    }

    /**
     * Close Instant booking pop up
     *
     * @throws InterruptedException
     */
    public void clickInstantPopUp() throws InterruptedException {
        if (selenium.waitInCaseElementClickable(instantBookingPopUp, 2) != null) {
            if (selenium.waitInCaseElementClickable(closeInstanBookingPopUpInOwnerHome, 3) != null) {
                selenium.clickOn(closeInstanBookingPopUpInOwnerHome);
            } else {
                selenium.clickOn(instantBookingPopUp);
                selenium.switchToWindow(1);
            }
        }
        else if (selenium.waitInCaseElementClickable(learnInstantBookingButton, 3) != null) {
            selenium.clickOn(closeLearnInstantBookingButton);
        }
    }

    /**
     * Click OK in free premium pop up
     *
     * @throws InterruptedException
     */
    public void clickOKinFreePremiumPopUp() throws InterruptedException {
        selenium.clickOn(popUpFreePremiumOKButton);
    }

    /**
     * Verify free premium pop up appear
     *
     * @return boolean status
     */
    public boolean isFreePremiumPopUpAppear() {
        return selenium.waitInCaseElementVisible(popUpFreePremiumOKButton, 3) != null;
    }

    /**
     * Click to close alokasi iklan harian pop up if exist
     * @throws InterruptedException
     */
    public void clickCloseAlokasiIklanHarianPopUp() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(alokasiIklanHarianLabel, 3) != null) {
            selenium.clickOn(okeAlokasiIklanHarianButton);
        }
    }

    /**
     * Click 'Tidak' to close daily balance allocation pop up
     * @throws InterruptedException
     */
    public void clickNoOnDailyBalanceAllocationPopUp() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(dailyBalanceAllocation, 3) != null) {
            selenium.clickOn(By.cssSelector(".c-mk-button--default"));
        }
    }

    /**
     * Click 'ok' to continue user login
     * @throws InterruptedException
     */
    public void clickOnConfirmContinueLogin() throws InterruptedException {
        if (selenium.isElementPresent(By.xpath("//button[@name='__CONFIRM__']"))) {
            selenium.clickOn(By.xpath("//button[@name='__CONFIRM__']"));
        }
    }

    /**
     * Click 'manti saja' to close popup GP
     * @throws InterruptedException
     */

    public void clickNantiSajaOnGpPopUp () throws InterruptedException {
       if (selenium.waitInCaseElementVisible(GpPopup, 3) != null) {
           selenium.hardWait(4);
           selenium.javascriptClickOn(GpPopup);
       }
    }

    /**
     * Click 'Saya Mengerti' to close set additional costs pop up
     * @throws InterruptedException
     */
    public void clickOnIUnderstandOnSetAdditionalCostsPopUp() throws InterruptedException {
        if (isAdditionalPricePopUpPresent()) {
            selenium.hardWait(2);
            selenium.clickOn(iUnderstandButtonAdditionalPrice);
        }
    }

    /**
     * Check additional price pop us is present in screen
     * @return true or false
     */
    private boolean isAdditionalPricePopUpPresent() {
        return selenium.waitInCaseElementVisible(iUnderstandButtonAdditionalPrice, 9) != null;
    }

    /**
     * Click 'Kembali' in attention pop up
     * @throws InterruptedException
     */
    public void clickBackAttentionPopUp() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(editDataLainButton, 15) != null) {
            selenium.clickOn(editDataLainButton);
            selenium.hardWait(5);
        }
    }

    /**
     * Click 'Lanjut Isi Data' in attention pop up
     * @throws InterruptedException
     */
    public void clickContinueInputDataPopUp() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(continueInputDataButton, 15) != null) {
            selenium.hardWait(1);
            selenium.clickOn(continueInputDataButton);
        }
    }
    /**
     * Click edit Other Data PopUp
     * @throws InterruptedException
     */
    public void editOtherDataPopUp() throws InterruptedException {
        selenium.clickOn(editOtherDataPopUp);
    }
    /**
     * Click 'Done' in success edit kos pop up
     * @throws InterruptedException
     */
    public void clickDoneEditKosPopUp() throws InterruptedException {
        selenium.clickOn(doneButtonEditKosPopUp);
    }

    /**
     * Click 'Next' in photo submission pop up
     * @throws InterruptedException
     */
    public void clickNextPhotoSubmitPopUp() throws InterruptedException {
        selenium.clickOn(nextBtnPhotoSubmissionPopUp);
    }

    /**
     * Verify if next button appear in photo submission
     * @return true if appear
     */
    public boolean isPhotoSubmissionNextButtonAppear() {
        return selenium.waitInCaseElementVisible(nextBtnPhotoSubmissionPopUp, 5) != null;
    }

    /**
     * Verify attention pop up appear
     * @return true if appear
     */
    public boolean isAttentionPopUpAppear() {
        return selenium.waitInCaseElementVisible(popUpAttentionInAddKos, 3) != null;
    }

    /**
     * Click 'Pindah Halaman' in attention pop up
     * @throws InterruptedException
     */
    public void clickMovePageInPopUp() throws InterruptedException {
        selenium.clickOn(movePageButton);
    }

    /**
     * Wait until loading is finished
     */
    public void waitLoadingFinish() {
        selenium.fluentWaitTillElementCountIsEqualTo(loadingAddKosPage, 0, 30, 3);
    }
}
