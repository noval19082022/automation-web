package pageobjects.mamikos.owner.apartment;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import utilities.Constants;
import utilities.SeleniumHelpers;

public class AddPO {

	WebDriver driver;
	SeleniumHelpers selenium;

	public AddPO(WebDriver driver) {
		this.driver = driver;
		selenium = new SeleniumHelpers(driver);

		// This initElements method will create all WebElements
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
	}

	/*
	 * All WebElements are identified by @FindBy annotation
	 * 
	 * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
	 * className, xpath as attributes.
	 */

	@FindBy(id = "propertyName")
	private WebElement appartmentName;

	@FindBy(id = "unitName")
	private WebElement unitName;

	@FindBy(xpath = "(//a[@class='clickable-link edit-data-link'])[1]")
	private WebElement editApartmentDataButton;

	@FindBy(xpath = "//button[@class='btn btn-kost  btn-mamiorange']")
	private WebElement submitButton;

	@FindBy(xpath = "//a[contains(.,'SELESAI')]")
	private WebElement doneButton;

	@FindBy(css = ".col-xs-12.stretch > div:nth-of-type(1) .room-title")
	private WebElement firstApartNameLabel;

	@FindBy(css = ".col-xs-12.stretch > div:nth-of-type(1) .status")
	private WebElement firstApartStatusLabel;

	@FindBy(css = "[name='Nomor unit']")
	private WebElement unitNumberInput;

	@FindBy(css = "#unitType")
	private WebElement unitTypeDropdown;

	@FindBy(css = "#propertyFloor")
	private WebElement floorInput;

	@FindBy(css = "#unitSize")
	private WebElement sizeInput;

	@FindBy(css = "#propertyDescription")
	private WebElement descriptionTextarea;

	@FindBy(css =".body-container" )
	private WebElement apartemenList;

	/**
	 * Fill out add apartment form
	 * @param appartmentNameValue apartment name
	 * @param unitNameValue       unit name
	 */
	public void fillAddApartmentForm(String appartmentNameValue, String unitNameValue) {
		selenium.enterText(appartmentName, appartmentNameValue, false);
		selenium.enterText(unitName, unitNameValue, false);
	}

	/**
	 * Click first edit apartment data
	 * @throws InterruptedException
	 */
	public void clickEditApartData() throws InterruptedException {
		selenium.clickOn(editApartmentDataButton);
	}

	/**
	 * Click Submit button
	 * @throws InterruptedException
	 */
	public void clickSubmit() throws InterruptedException {
		selenium.javascriptClickOn(submitButton);
		selenium.hardWait(5);
	}

	/**
	 * Click Done button
	 * @throws InterruptedException
	 */
	public void clickDone() throws InterruptedException {
		selenium.javascriptClickOn(doneButton);
		selenium.hardWait(8);
	}

	/**
	 * Get first apartment name
	 * @return String apartment name
	 */
	public String getFirstApartName() {
		return selenium.getText(firstApartNameLabel);
	}

	/**
	 * Get first apartment status
	 * @return String apartment name
	 */
	public String getFirstApartStatus() {
		return selenium.getText(firstApartStatusLabel);
	}

	/**
	 * Enter text to unit number field
	 * @param number is unit Number
	 */
	public void fillUnitNumber(String number) {
		selenium.enterText(unitNumberInput, number, true);
	}

	/**
	 * Select unit type from dropdown
	 * @param unitType is unit type
	 */
	public void selectUnitType(String unitType) {
		selenium.selectDropdownValueByText(unitTypeDropdown, unitType);
	}

	/**
	 * Enter text to floor field
	 * @param floor is unit floor
	 */
	public void fillFloor(String floor) {
		selenium.enterText(floorInput, floor, true);
	}

	/**
	 * Enter text to size field
	 * @param size is unit size
	 */
	public void fillSize(String size) {
		selenium.enterText(sizeInput, size, true);
	}

	/**
	 * Enter text to description field
	 * @param desc is unit description
	 */
	public void fillDesc(String desc) {
		selenium.enterText(descriptionTextarea, desc, true);
	}

	/**
	 * Scroll page apartemen list
	 * @throws InterruptedException
	 */
	public void scrollApartemenList() throws InterruptedException {
		selenium.hardWait(3);
		selenium.pageScrollInView(apartemenList);
	}
}
