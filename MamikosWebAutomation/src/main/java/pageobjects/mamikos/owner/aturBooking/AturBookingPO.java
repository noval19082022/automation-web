package pageobjects.mamikos.owner.aturBooking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class AturBookingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public AturBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(className = "bg-c-modal__body-title")
    private WebElement aturBookingTitle;

    @FindBy( css = " button.bg-c-button.bg-c-button--primary.bg-c-button--lg")
    private WebElement aturBookingBtn;

    @FindBy( css = "#BookingSettingDesktop > p.bg-c-text.bg-c-text--body-2")
    private WebElement aturBookingPageContent;

    @FindBy(xpath = "(//*[@class='widget-card bg-c-card bg-c-card--lined bg-c-card--md bg-c-card--light'])[1]//following::a[3][contains(.,'Ubah Peraturan Masuk Kos')]")
    private WebElement ubahPeraturanMasukKosSection;

    @FindBy(xpath = "//p[@class='booking-setting__title bg-c-text bg-c-text--heading-3 ']")
    private WebElement titlePeraturanMasukKosPage;

    @FindBy(xpath = "//p[.='Pengajuan Booking']")
    private WebElement pengajuanBookingBtn;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--button-md ']")
    private WebElement ubahAturanbtn;

    @FindBy(xpath = "//div[@class='kos-input-select']")
    private WebElement selectKostDropDown;

    @FindBy(xpath = "//button[@type='button'][contains(.,'Pilih nama kos')]")
    private WebElement pilihNamaKosbtn;

    @FindBy(xpath = "//button[@type='button'][contains(.,'Simpan')]")
    private WebElement simpanPrtrnMskKosBtn;

    @FindBy(xpath = "//div[@class='bg-c-toast__content']")
    private WebElement toastSimpanKosGP;

    @FindBy(xpath = "//span[contains(.,'Kriteria calon penyewa')]")
    private WebElement kriteriaCalonPenyewaText;

    @FindBy By kosKhususKriteriaToogle = By.id("switch-job_status");

    @FindBy By selectKosMahasiswa = By.id("option-job_status-1");

    @FindBy(xpath ="//*[@class='detail-kost-rules__content']//*[@class='detail-kost-rules__title bg-c-text bg-c-text--heading-4 ']")
    private WebElement peraturanKosText;

    @FindBy(xpath ="//*[@class='booking-terms-content booking-terms__content']//*[@class='booking-terms-content__title bg-c-text bg-c-text--heading-4 ']")
    private WebElement ketentuanKosText;

    @FindBy(xpath="//*[@class='detail-kost-rules__content']//p[contains(.,'Khusus karyawan')]")
    private WebElement employeesOnlyText;

    @FindBy(xpath ="//div[@class='bg-c-toast__content']")
    private WebElement simpanKosNoGPToast;

    @FindBy By setPasutriToogle = By.id("switch-is_married");

    @FindBy(xpath = "//input[@id='option-capacity-0']")
    private WebElement roomForTenantOnly;

    @FindBy(xpath = "//*[@class='detail-kost-rules__content']//p[contains(.,'Boleh pasutri')]")
    private WebElement pasutriText;

    @FindBy By setBawaAnakToogle = By.id("switch-is_bring_child");

    @FindBy(xpath = "//span[contains(.,'Boleh bawa anak')]")
    private WebElement bawaAnakText;

    @FindBy By setKTPToogle = By.id("switch-required_identity_booking");

    @FindBy By setBukuNikahToogle = By.id("switch-required_marriage_book");

    @FindBy By setKKToogle = By.id("switch-required_family_card");

    @FindBy(xpath ="//input[@id='option-capacity-1']")
    private WebElement maksTwoTenant;

    @FindBy(xpath="//*[@class='detail-kost-rules__content']//p[contains(.,'Khusus Mahasiswa')]")
    private WebElement mahasiswaText;

    @FindBy(xpath="//span[contains(.,'Waktu mulai masuk kos')]")
    private WebElement checkinRule;

    @FindBy(xpath = "//p[contains(.,'Ubah waktu')]")
    private WebElement updateTime;

    @FindBy(css ="div.checkin-setting-modal__d-day-checkin > div > input")
    private WebElement h0Toogle;

    private By minCheckinAmmount = By.id("min-checkin-amount");

    @FindBy(xpath= "//*[@class='booking-checkin-select-option-modal__action']//*[contains(text(),'Simpan')]")
    private WebElement saveButtonOnCheckinAmmount;

    private By minCheckinTimeUnit = By.id("min-checkin-time-unit");

    @FindBy(css="div.bg-c-modal__footer > div > button")
    private WebElement saveButtonOnUnit;

    @FindBy(css="div > div > div.bg-c-modal__footer > button")
    private WebElement saveButtonOnPopup;

    @FindBy(xpath = "//*[@data-testid='booking-terms-list-item']//p[contains(.,'Waktu mulai ngekos terdekat')]")
    private WebElement nearestTime;

    @FindBy(xpath = "//*[@data-testid='booking-terms-list-item']//p[contains(.,'Waktu mulai ngekos terjauh')]")
    private WebElement furthestTime;

    /**
     * Get title popup
     * @return Atur booking content
     */
    public String getTitleText() {
        return selenium.getText(aturBookingTitle);
    }

    /**
     * Click on Atur booking button
     * @throws InterruptedException
     */
    public void clickManageBookingBtn() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(aturBookingBtn);
    }

    /**
     * Get title on pengaturan booking page
     * @return e.g "Kini Anda bisa mengatur durasi sewa"
     */
    public String getManageBookingTitile() {
        return selenium.getText(aturBookingPageContent);
    }

    /**
     * Get title on Ubah Peraturan Masuk Kos at dashboard
     * @return title ubah peraturan masuk kos section
     */
    public String getTitleUbahPeraturanKos(){
        return selenium.getText(ubahPeraturanMasukKosSection);
    }

    /**
     * Click on Ubah Peraturan Masuk Kos at dashboard
     * @throws InterruptedException
     */
    public void clickUbahPeraturanKosDashboard() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(ubahPeraturanMasukKosSection);
    }

    /**
     * Get title Peraturan Masuk Kos page
     * @return title peraturan masuk kos page
     */
    public String getTitlePeraturanMasukKosPage(){
        return selenium.getText(titlePeraturanMasukKosPage);
    }

    /**
     * click Pengajuan Booking at sidebar
     * @throws InterruptedException
     */
    public void clickPengajuanBooking() throws InterruptedException {
        selenium.clickOn(pengajuanBookingBtn);
        selenium.hardWait(2);
    }

    /**
     * get wording Ubah aturan
     * @return Ubah aturan text
     */
    public String getAturBookingText() throws InterruptedException{
        selenium.hardWait(6);
        selenium.waitTillElementIsVisible(ubahAturanbtn);
        return selenium.getText(ubahAturanbtn);
    }

    /**
     * click Ubah aturan at pengajuan booking page
     * @throws InterruptedException
     */
    public void clickUbahAturan() throws InterruptedException {
        selenium.hardWait(5);
        selenium.javascriptClickOn(ubahAturanbtn);
    }

    /**
     * select kost GP at Peraturan Masuk Kost page
     * @param kostGp
     * @throws InterruptedException
     */
    public void selectKostDropdown(String kostGp) throws InterruptedException {
        selenium.clickOn(selectKostDropDown);
        selenium.hardWait(3);
        WebElement element = driver.findElement(By.xpath("//input[@id='kos-87241215']//following::p[contains(.,'"+ kostGp +"')]"));
        selenium.waitInCaseElementVisible(element,2);
        selenium.clickOn(element);
        selenium.hardWait(2);
        selenium.clickOn(pilihNamaKosbtn);
    }

    /**
     * click Simpan at Peraturan masuk kos
     * @throws InterruptedException
     */
    public void clickSimpanPeratrnMskKos() throws InterruptedException {
        selenium.clickOn(simpanPrtrnMskKosBtn);
    }

    /**
     * get wording toast when click Simpan on kost GP
     * @return toast simpan kost gp
     */
    public String getToastSimpanKosGp(){
        return selenium.getText(toastSimpanKosGP);
    }

    /**
     * Click on Kos name
     * @throws InterruptedException
     */
    public void clickKosName() throws InterruptedException {
        selenium.hardWait(3);
    }

    /**
     * Click on selected kos name
     * @throws InterruptedException
     */
    public void clickKosNameSelected(String kostNotGp) throws InterruptedException {
        selenium.clickOn(selectKostDropDown);
        WebElement element = driver.findElement(By.xpath("//input[@id='kos-58518226']//following::p[contains(.,'"+ kostNotGp +"')]"));
        selenium.clickOn(element);
        selenium.hardWait(2);
        selenium.clickOn(pilihNamaKosbtn);
    }

    /**
     * Click on kriteria calon penyewa
     * @throws InterruptedException
     */
    public void clickKriteriaPenyewa() throws InterruptedException {
        selenium.clickOn(kriteriaCalonPenyewaText);
        selenium.hardWait(2);
    }

    /**
     * Click on activated kos khusus
     * @throws InterruptedException
     */
    public void actiavtedOnKosKhusus() throws InterruptedException {
        selenium.hardWait(2);
        selenium.javascriptClickOn(kosKhususKriteriaToogle);
        selenium.hardWait(2);
    }

    /**
     * Get title on pengaturan kos on kost detail page
     * @return e.g "Petaruran Kos"
     * @throws InterruptedException
     */
    public String kostRuleAsPresent() throws InterruptedException{
        selenium.pageScrollInView(peraturanKosText);
        return selenium.getText(peraturanKosText);
    }

    /**
     * Check if peraturan kos  is visible
     * @return visible true, otherwise false
     */
    private boolean isPeraturanKosAsPresent() {
        return selenium.waitInCaseElementVisible(peraturanKosText, 5) != null;
    }

    /**
     * Check if ketentuan kos  is visible
     * @return visible true, otherwise false
     */
    private boolean isKetentuanKosAsPresent() {
        return selenium.waitInCaseElementVisible(ketentuanKosText, 5) != null;
    }

    /**
     * Scroll to peraturan kost text
     * @throws InterruptedException
     */
    private void scrollToKetentuanKosText() throws InterruptedException {
        for(int i = 0; i <= 30; i++) {
            selenium.pageScrollUsingCoordinate(0, 2500);
            selenium.hardWait(5);
            if(isKetentuanKosAsPresent()) {
                break;
            }
        }
    }

    /**
     * Scroll to peraturan kost text
     * @throws InterruptedException
     */
    private void scrollToPeraturanKosText() throws InterruptedException {
        for(int i = 0; i <= 30; i++) {
            selenium.pageScrollUsingCoordinate(0, 1900);
            selenium.hardWait(5);
            if(isPeraturanKosAsPresent()) {
                break;
            }
        }
    }

    /**
     * Get title special kos on kost detail
     * @return e.g "Khusus karyawan"
     *  @throws InterruptedException
     */
    public String kostEmployeesAsPresent() throws InterruptedException {
        scrollToPeraturanKosText();
        return selenium.getText(employeesOnlyText);
    }

    /**
     * Get toast success save text
     * @return e.g "Peraturan terbaru berhasil disimpan"
     * @throws InterruptedException
     */
    public String getToastSimpanNotGp() throws InterruptedException {
        return selenium.getText(simpanKosNoGPToast);
    }

    /**
     * Click on activated bisa pasutri
     * @throws InterruptedException
     */
    public void activatedSetPasutri() throws InterruptedException {
        selenium.javascriptClickOn(setPasutriToogle);
        selenium.hardWait(3);
    }

    /**
     * verify button Kamar khusus penyewa is disable
     * @throws InterruptedException
     */
    public Boolean disableRoomForTenantOnly() {
        return !selenium.isClickable(roomForTenantOnly,5);
    }

    /**
     * Get Boleh pasutri text
     * @return e.g "Boleh pasutri"
     * @throws InterruptedException
     */
    public String getPasutriText() throws InterruptedException {
        scrollToPeraturanKosText();
        return selenium.getText(pasutriText);
    }

    /**
     * Click on activated bawa anak
     * @throws InterruptedException
     */
    public void activatedSetBawaAnak() throws InterruptedException {
        selenium.javascriptClickOn(setBawaAnakToogle);
        selenium.hardWait(3);
    }

    /**
     * Get Boleh bawa text
     * @return e.g "Boleh pasutri"
     * @throws InterruptedException
     */
    public String getBawaAnakText() throws InterruptedException {
        scrollToPeraturanKosText();
        return selenium.getText(bawaAnakText);
    }

    /**
     * Click on selected kos name
     * @throws InterruptedException
     */
    public void clickKosName(String kostNotGp1) throws InterruptedException {
        selenium.clickOn(selectKostDropDown);
        selenium.hardWait(7);
        WebElement element = driver.findElement(By.xpath("//input[@id='kos-53963443']//following::p[contains(.,'"+ kostNotGp1 +"')]"));
        selenium.clickOn(element);
        selenium.pageScrollInView(element);
        selenium.hardWait(2);
        selenium.clickOn(pilihNamaKosbtn);
    }

    /**
     * Click on activated KTP
     * @throws InterruptedException
     */
    public void activatedKtp() throws InterruptedException {
        selenium.javascriptClickOn(setKTPToogle);
        selenium.hardWait(3);
    }

    /**
     * Click on activated Buku Nikah
     * @throws InterruptedException
     */
    public void activatedSetBukuNikah() throws InterruptedException {
        selenium.pageScrollInView(setBukuNikahToogle);
        selenium.javascriptClickOn(setBukuNikahToogle);
        selenium.hardWait(3);
    }

    /**
     * Click on activated KK
     * @throws InterruptedException
     */
    public void activatedSetKK() throws InterruptedException {
        selenium.javascriptClickOn(setKKToogle);
        selenium.hardWait(3);
    }

    /**
     * verify button maks 2 orang is disable
     * @throws InterruptedException
     */
    public Boolean disableTwoTenant() {
        return !selenium.isClickable(maksTwoTenant,5);
    }

    /**
     * Click on kost Mahasiswa
     * @throws InterruptedException
     */
    public void selectKosMahasiswa() throws InterruptedException {
        selenium.javascriptClickOn(selectKosMahasiswa);
        selenium.hardWait(3);
    }

    /**
     * Get Khusus Mahasiswa text
     * @return e.g "Khusus Mahasiswa"
     * @throws InterruptedException
     */
    public String getMahasiswaText() throws InterruptedException {
        scrollToPeraturanKosText();
        return selenium.getText(mahasiswaText);
    }

    /**
     * Check if boleh bawa anak is activated
     * @return true if boleh bawa anak is activated
     */
    public boolean isBawaAnakToogleActive() {
        return selenium.waitInCaseElementVisible(setBawaAnakToogle, 5) != null;
    }

    /**
     * Check if boleh bawa pasutri is activated
     * @return true if boleh bawa pasutri is activated
     */
    public boolean isPasutriToogleActive() {
        return selenium.waitInCaseElementVisible(setPasutriToogle, 5) != null;
    }

    /**
     * Click on waktu mulai kost
     * @throws InterruptedException
     */
    public void clickWaktuMulaiKos() throws InterruptedException {
        selenium.javascriptClickOn(checkinRule);
        selenium.hardWait(3);
    }

    /**
     * Click on ubah waktu
     * @throws InterruptedException
     */
    public void clickOnUpdateTime() throws InterruptedException {
        selenium.javascriptClickOn(updateTime);
        selenium.hardWait(3);
    }

    /**
     * Click on hari H (0 hari) setelah booking
     * @throws InterruptedException
     */
    public void clickOnH0AfterBooking() throws InterruptedException {
        selenium.javascriptClickOn(h0Toogle);
        selenium.hardWait(3);
    }

    /**
     * Click on checkinDate
     * @throws InterruptedException
     */
    public void selectMinusCheckinDate(String checkinDate) throws InterruptedException {
        selenium.javascriptClickOn(minCheckinAmmount);
        WebElement element = driver.findElement(By.xpath("//p[contains(.,'"+ checkinDate +"')]"));
        selenium.clickOn(element);
        selenium.hardWait(2);
        selenium.javascriptClickOn(saveButtonOnCheckinAmmount);
    }

    /**
     * Click on checkin Time
     * @throws InterruptedException
     */
    public void selectMinusCheckinTime(String checkinTime) throws InterruptedException {
        selenium.javascriptClickOn(minCheckinTimeUnit);
        WebElement element = driver.findElement(By.xpath("//p[contains(.,'"+ checkinTime +"')]"));
        selenium.clickOn(element);
        selenium.hardWait(2);
        selenium.javascriptClickOn(saveButtonOnUnit);
    }

    /**
     * Click on simpan button
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.javascriptClickOn(saveButtonOnPopup);
        selenium.hardWait(3);
    }

    public String nearestCheckinRuleAsPresent() throws InterruptedException{
        scrollToKetentuanKosText();
        return selenium.getText(nearestTime);
    }

    public String furthestCheckinRuleAsPresent() throws InterruptedException{
        return selenium.getText(furthestTime);
    }

}
