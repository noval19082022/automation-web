package pageobjects.mamikos.owner.settings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class SettingsPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SettingsPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//div[@id='ownerPersonalInfoWrapper']/div[1]//p[@class='bg-c-text bg-c-text--body-1 ']")
    private WebElement changeNameButton;

    @FindBy(xpath = "//div[@id='ownerPersonalInfoWrapper']/div[2]//a[.='Ubah']")
    private WebElement changeNomorHandphoneButton;

    @FindBy(css = "[name='name']")
    private WebElement inputNameTextbox;

    @FindBy(css = "[name='phone_number']")
    private WebElement inputNomorHandphoneTextbox;

    @FindBy(css = ".bg-c-button")
    private WebElement submitButton;

    @FindBy(id = "swal2-content")
    private WebElement errorPopUpText;

    @FindBy(xpath = "//div[@id='collapseNameSetting']//button[@class='btn btn-mamiorange pull-right']")
    private WebElement submitChangeNameButton;

    @FindBy(css = "[for='alarm_email']")
    private WebElement emailRecommendationCheckbox;

    @FindBy(css = "[for='app_chat']")
    private WebElement chatNotificationCheckbox;

    @FindBy(css = "[for='update_kost']")
    private WebElement smsUpdateKosCheckbox;

    @FindBy(css = ".bg-c-field__label > [for]")
    private WebElement userSeeButtonSimpanDisable;

    @FindBy(css = ".swal2-content")
    private WebElement userSeeMessageErrorValidation;

    @FindBy(css = ".bg-c-pin")
    private WebElement otpPopUp;

    @FindBy(css = ".owner-phone-number-otp-modal__resend-button")
    private WebElement resendOTPButton;

    @FindBy(css = ".swal2-confirm")
    private WebElement okButtonOnPopUP;

    @FindBy(className = "bg-c-toast__content")
    private WebElement toastMessage;

    @FindBy(xpath = "//i[@class='mdi mdi-account-circle mdi-48px']")
    private WebElement profilePictureNull;

    @FindBy(className = "c-mk-header__avatar")
    private WebElement profilePictureNotNull;

    @FindBy(xpath = "//*[contains(@class, 'membership-card__section mamipoin')]")
    private WebElement mamipoinAvailable;

    /**
     * Click on Change Name Button
     * @throws InterruptedException
     */
    public void clickOnChangeNameButton() throws InterruptedException {
        selenium.clickOn(changeNameButton);
    }

    /**
     * Get Error Message Phone Number Text
     */
    public String userSeeMessageErrorValidation() {
        return selenium.getText(userSeeMessageErrorValidation);
    }

    /**
     * Fills phone number owner
     * @throws InterruptedException
     */
    public void inputPhoneNumberOwnerRegisteredNewFlow(String phone) throws InterruptedException {
        selenium.enterText(inputNomorHandphoneTextbox, phone, true);
    }

    /**
     * Get Phone Number PlaceHolder text
     * @return String
     */
    public String getPhoneNumberPlaceHolder() {
        this.inputNomorHandphoneTextbox.clear();
        return selenium.getElementAttributeValue(inputNomorHandphoneTextbox, "Placeholder");
    }

    /**
     * Click on Change Phone Number Button
     * @throws InterruptedException
     */
    public void clickChangePhoneNumberButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(changeNomorHandphoneButton);
    }

    /**
     * Input Name then click Submit button
     * @param name username or name
     * @throws InterruptedException
     */
    public void inputFormNameAndClickSubmitButton(String name) throws InterruptedException {
        selenium.enterText(inputNameTextbox, name, true);
        selenium.clickOn(submitButton);
    }

    /**
     * Get Username Text
     * @return username e.g. tiara
     */
    public String getUsernameText(String username){
        return selenium.getText(By.xpath("//p[contains(text(), '"+username+"')]"));
    }

    /**
     * Get Error Pop Up Text
     * @return error pop pup message
     */
    public String getErrorPopupText(){
        return selenium.getText(errorPopUpText);
    }

    /**
     * Is Error Pop Up Present?
     * @return error pop up
     */
    public Boolean isErrorPopUpPresent(){
        return selenium.waitInCaseElementVisible(errorPopUpText, 3) != null;
    }

    /**
     * Click on submit button after change owner's name
     * @throws InterruptedException
     */
    public void clickonSubmitChangeName() throws InterruptedException {
        selenium.clickOn(submitChangeNameButton);
    }

    /**
     * Click on simpan button after change nomor handphone owner
     * @throws InterruptedException
     */
    public void clickonSimpanChangeNoHp() throws InterruptedException {
        selenium.clickOn(submitButton);
    }

    /**
     * Click on recommendation via email checkbox
     * @throws InterruptedException
     */
    public void clickRecommendationViaEmail() throws InterruptedException {
        selenium.waitTillElementIsNotPresent(toastMessage, 3);
        selenium.clickOn(emailRecommendationCheckbox);
    }

    /**
     * Click on chat notification checkbox
     * @throws InterruptedException
     */
    public void clickChatNotification() throws InterruptedException {
        selenium.waitTillElementIsNotPresent(toastMessage, 3);
        selenium.clickOn(chatNotificationCheckbox);
    }

    /**
     * Click on SMS update kos checkbox
     * @throws InterruptedException
     */
    public void clickSMSUpdateKos() throws InterruptedException {
        selenium.waitTillElementIsNotPresent(toastMessage, 3);
        selenium.clickOn(smsUpdateKosCheckbox);
    }

    /**
     * Get Inputed Text from Input Name field
     * @return inserted text
     */
    public String getInputNameText(){
        return inputNameTextbox.getAttribute("value");
    }

    /**
     * Is OTP Pop Up Appear
     * @return true or false
     */
    public Boolean isOTPPopUpAppear(){
        return selenium.waitInCaseElementVisible(otpPopUp, 3) != null;
    }

    /**
     * Wait For 60 Sec and Click on Resend OTP Button
     * @throws InterruptedException
     */
    public void clickOnResendOTPButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(resendOTPButton, 62);
        selenium.clickOn(resendOTPButton);
    }

    /**
     * Wait For 5 Sec and Click on Ok Button
     * @throws InterruptedException
     */
    public void clickonOkButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(okButtonOnPopUP, 5);
        selenium.clickOn(okButtonOnPopUP);
    }

    /**
     * Get text toast
     * @return String toast message
     */
    public String getToast() throws InterruptedException {
        selenium.pageScrollInView(toastMessage);
        selenium.hardWait(1);
        return selenium.getText(toastMessage);
    }
    /**
     * Get Profile Picture Is Null
     */
    public Boolean isProfilePictureNull(){
        return profilePictureNull.isDisplayed();
    }

    /**
     * Get Profile Picture Is Show
     */
    public Boolean isProfilePictureNotNull(){
        return profilePictureNotNull.isDisplayed();
    }

    /**
     * Get Mamipoin is Available
     * @return true if displayed for Mamipoin
     */
    public Boolean isMamipoinAvailable(){
        return mamipoinAvailable.isDisplayed();
    }
}
