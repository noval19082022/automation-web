package pageobjects.mamikos.owner.kostmanagement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.Collections;
import java.util.List;

public class ManageBillsPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ManageBillsPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".bm-filter .bm-filter__kost-text")
    private WebElement txtSelectedKost;

    private final By listRenterResult = By.cssSelector("div.billing-management__data");

    @FindBy(css = "div.empty-state-list__empty-data h1")
    private WebElement txtTitleAllPaidStatus;

    private By inputMonthFilter = By.xpath("//*[@class='billing-management-input-trigger bg-c-dropdown'][1]");

    @FindBy(css = ".bm-filter__rent input")
    private WebElement inputRentCount;

    @FindBy(xpath = "//*[@class='month__year_btn up']")
    private WebElement dropYearFilter;

    @FindBy(css = ".mamipoin-tabs__list li:nth-child(2)")
    private WebElement tabInMamikos;

    @FindBy(css="a.billing-management__button__due-date")
    private WebElement sortingKostResult;

    @FindBy(css = ".bm-filter__kost-title + div")
    private WebElement boxBillsInKost;

    @FindBy(css = "input[placeholder='Cari Nama Penyewa']")
    private WebElement inpRenterName;

    @FindBy(css = "input[placeholder='Cari Nama Penyewa'] + button")
    private WebElement btnSearchRenter;

    @FindBy(css = ".is-active .close-icon")
    private WebElement icXClose;

    @FindBy(xpath = "//*[contains(text(), 'Ditransfer')]")
    private WebElement btnTransferred;

    @FindBy(xpath = "//a[contains(., 'Sudah bayar')]")
    private WebElement sudahByrTab;

    @FindBy(xpath = "//div[@class='billing-management__tenant-info']")
    private WebElement billingInvoice;

    @FindBy(xpath = "//p[contains(., 'Kapan uang masuk ke rekening saya?')]")
    private WebElement disbursementLink;

    @FindBy(xpath = "//h1[@class='mh-article-page__title bg-c-text bg-c-text--heading-2 ']")
    private WebElement disbursementLinkPage;

    @FindBy(xpath="//*[@class='bm-filter__kost-label']//*[@class='bm-filter__kost-text']")
    private WebElement filterKosName;

    @FindBy(css="div.bm-filter__rent-option.column.is-half.is-full-mobile > div:nth-child(1) > div.bg-c-dropdown__trigger > div > input")
    private WebElement monthFilter;

    /**
     * Get selected kost text on active filter by kost
     * @return string data type e.g "Kost Mamitest"
     */
    public String getSelectedKostText() {
        return selenium.getText(txtSelectedKost);
    }

    /**
     * Get list of renter on not paid section manage bills page
     * @return List of web element
     */
    public List<WebElement> getResultOnNotPaidYet() {
        try {
            return selenium.waitTillAllElementsAreLocated(listRenterResult);
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    /**
     * Get title text all paid status
     * @return string data type "Tidak Ada yang Belum Bayar"
     */
    public String getAllPaidTitle() {
        try {
            return selenium.getText(txtTitleAllPaidStatus);
        }catch (Exception e) {
            return "";
        }

    }

    /**
     * Select month filter by month number
     * @param monthNumber 1 = January
     * @throws InterruptedException
     */
    public void selectManageBillsMonthFilter(String monthNumber) throws InterruptedException {
        selenium.clickOn(inputMonthFilter);
        selenium.hardWait(1);
        selenium.clickOn(By.xpath("//*[@class='date-wrapper']//*[@class='cell month']["+ monthNumber +"]"));
    }

    /**
     * Select years filter manage bills
     * @param year input with year number as string e.g "2021"
     * @throws InterruptedException
     */
    public void selectYearsFilter(String year) throws InterruptedException {
        selenium.clickOn(inputMonthFilter);
        selenium.selectDropdownValueByText(dropYearFilter, year);
    }

    /**
     * Select rent count by it text
     * @param rentCount rent count text e.g "Per Hari"
     * @throws InterruptedException
     */
    public void setRentCount(String rentCount) throws InterruptedException {
        String e = "//*[contains(text(), '"+ rentCount +"')]";
        selenium.clickOn(inputRentCount);
        selenium.hardWait(2);
        selenium.clickOn(By.xpath(e));
    }

    /**
     * Click on renter result with our choice of index
     * @param index element index e.g "1". Input with string
     * @throws InterruptedException
     */
    public void clickOnRenterResult(String index) throws InterruptedException {
        selenium.clickOn(By.xpath("//*[@class='billing-management-table__row']["+ index +"]"));
    }

    /**
     * Click on tab in mamikos
     * @throws InterruptedException
     */
    public void clicksOnInMamikosTab() throws InterruptedException {
        selenium.clickOn(tabInMamikos);
    }

    /**
     * Select kost result sorting by it text
     * @param sortingType input with "Tanggal Jatuh Tempo", "Kamar", "Nama Penyewa"
     * @throws InterruptedException
     */
    public void sortingResultBy(String sortingType) throws InterruptedException {
        String e = "//*[contains(@class, 'is-active')]//*[contains(text(), '"+sortingType+"')]";
        selenium.clickOn(sortingKostResult);
        selenium.clickOn(driver.findElement(By.xpath(e)));
    }

    /**
     * Get room number by parsing text number to Integer
     * @param row kost result in row
     * @return Integer data type of kost room number
     */
    public Integer getRoomNumberKostResult(Integer row) {
        String textRoomNumber = "div.billing-management__data:nth-child("+ row +") .billing-management__tenant-info__room";
        return Integer.parseInt(selenium.getText(driver.findElement(By.cssSelector(textRoomNumber))));
    }

    /**
     * Get renter name text on kost result
     * @param row renter result row
     * @return String data type renter name
     */
    public String getRenterName(Integer row) {
        String renterNameEl = "div.billing-management__data:nth-child("+ row +") .billing-management__tenant-info__name";
        return selenium.getText(driver.findElement(By.cssSelector(renterNameEl)));
    }

    /**
     * Check if renter is available
     * @param row renter result row
     * @return true if renter present, otherwise false
     */
    public boolean isRenterResultVisible(Integer row) {
        String renterNameEl = "div.billing-management__data:nth-child("+ row +") .billing-management__tenant-info__name";
        return selenium.waitInCaseElementVisible(driver.findElement(By.cssSelector(renterNameEl)), 5) != null;
    }

    /**
     * Get payment due date text
     * @param row renter result row
     * @return String data type
     */
    public String getPaymentDueDate(Integer row) {
        String dueDateEl = "div.billing-management__data:nth-child("+row+") .billing-management__row.simple-text";
        return selenium.getText(driver.findElement(By.cssSelector(dueDateEl)));
    }

    /**
     * Click on box bills in filter
     * @param kostName input with desired kost namem, example: "Kos Booking Semarang"
     * @throws InterruptedException
     */
    public void setKostBills(String kostName) throws InterruptedException {
        String kostBillEl = "//*[@class='field']//*[contains(text(), '"+kostName+"')]";
        selenium.waitTillElementIsVisible(boxBillsInKost, 10);
        selenium.hardWait(5);
        selenium.javascriptClickOn(boxBillsInKost);
        selenium.javascriptClickOn(driver.findElement(By.xpath(kostBillEl)));
        selenium.clickOn(icXClose);
    }

    /**
     * Click on box bills in filter and choose kost to filter for recurring
     * @param kostName input with desired kost namem, example: "Kos Booking Semarang"
     * @throws InterruptedException
     */
    public void setKostBillsRecurring(String kostName) throws InterruptedException {
        String kostBillEl = "//*[@class='field']//*[contains(text(), '"+kostName+"')]";
        selenium.waitTillElementIsVisible(boxBillsInKost, 10);
        selenium.hardWait(5);
        selenium.javascriptClickOn(boxBillsInKost);
        selenium.javascriptClickOn(driver.findElement(By.xpath(kostBillEl)));
    }

    /**
     * Set renter name filter
     * @param renterName desired renter name example "Jhon Lennon"
     * @throws InterruptedException
     */
    public void setRenterName(String renterName) throws InterruptedException {
        By renterNameEl = By.xpath("//*[@class='billing-management__tenant-info__name' and contains(text(), '"+renterName+"')]");
        selenium.enterText(inpRenterName, renterName, false);
        selenium.waitTillElementIsClickable(btnSearchRenter);
        selenium.hardWait(1);
        selenium.javascriptClickOn(btnSearchRenter);
        selenium.waitTillElementsCountIsMoreThan(renterNameEl, 0);
        selenium.hardWait(2);
    }

    /**
     * Clicks on transferred button
     * @throws InterruptedException
     */
    public void clicksOnTransferredTab() throws InterruptedException {
        selenium.clickOn(btnTransferred);
    }

    /**
     * clicks on Sudah bayar tab
     */
    public void clicksOnSudahBayarTab() throws InterruptedException {
        selenium.clickOn(sudahByrTab);
    }

    /**
     * open the billing invoice
     */
    public void openTheInvoice() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(billingInvoice);
    }

    /**
     * clicks on disbursement link
     * @throws InterruptedException
     */
    public void clicksDisbursementLink() throws InterruptedException{
        selenium.hardWait(3);
        selenium.clickOn(disbursementLink);
    }

    /**
     * get disbursement link page
     */
    public String getDisbursementLinkPage(){
        return selenium.getText(disbursementLinkPage);
    }

    /**
     * click back on the browser
     */
    public void clickBackOnBrowser(){
        selenium.back();
    }

    /**
     * filter kost name
     * @throws InterruptedException
     */
    public void clickKostNameSelected(String kostName)throws InterruptedException{
        selenium.hardWait(4);
        selenium.clickOn(filterKosName);
        WebElement element = driver.findElement(By.xpath("//*[@class='control-label']//*[contains(text(), '"+kostName+"')]"));
        selenium.clickOn(element);
        selenium.hardWait(2);
    }

    /**
     * filter bills month
     * @throws InterruptedException
     */
    public void clickMonthSelected(String monthName)throws InterruptedException{
        selenium.clickOn(monthFilter);
        WebElement element = driver.findElement(By.xpath("//*[@class='date-wrapper']//span[contains(., '"+monthName+"')]"));
        selenium.clickOn(element);
        selenium.hardWait(2);
    }
}
