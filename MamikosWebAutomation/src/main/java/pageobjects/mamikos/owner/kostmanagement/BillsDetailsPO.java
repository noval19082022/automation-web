package pageobjects.mamikos.owner.kostmanagement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BillsDetailsPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public BillsDetailsPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "label.checkbox input")
    private WebElement cBoxPayOutsideMamipay;

    @FindBy(css = "div.is-active")
    private WebElement popUpActive;

    @FindBy(css = "div.is-active .title")
    private WebElement popUpTitle;

    @FindBy(css = "div.is-active h1 + p")
    private WebElement popUpCaption;

    @FindBy(xpath = "//button[.='Batal']")
    private WebElement btnCancelPopUp;

    @FindBy(xpath = "//button[.='Ya, Tandai']")
    private WebElement btnYesMarkPopUp;

    @FindBy(css=".detail-bill-invoice-unpaid div[class*='status']")
    private WebElement txtPaymentStatus;

    @FindBy(xpath = "//*[.='Biaya Tambahan']/parent::*//following-sibling::*[.='Ubah Biaya']")
    private WebElement buttonEditAdditionalPrice;

    @FindBy(xpath = "//*[.='Biaya Sewa']/parent::*//following-sibling::*[.='Ubah Biaya']")
    private WebElement buttonEditKostPrice;

    @FindBy(css = ".is-active .title-header")
    private WebElement txtHeaderTitle;

    @FindBy(css = ".is-active .mdi-close")
    private WebElement btnClosePopUp;

    @FindBy(xpath = "//*[.='Ditransfer ke:']/following-sibling::div//a")
    private WebElement btnEditBankTransfer;

    @FindBy(css = "div.is-active button")
    private WebElement txtButtonActive;

    @FindBy(css=".item-section.is-mobile.cursor-pointer")
    private WebElement boxTenantInfo;

    @FindBy(xpath="//*[.='Total Pembayaran']/parent::*/following-sibling::*[contains(@class, 'has-text')]/*")
    private WebElement txtTotalCost;

    @FindBy(xpath = "(//*[.='Biaya Tambahan']/parent::*/parent::*/following-sibling::*//*[contains(@class, 'has-text-right')]/span)[1]")
    private WebElement txtAdditionalPrice;

    @FindBy(xpath = "(//*[.='Biaya Sewa']/parent::*/parent::*/following-sibling::*//*[contains(@class, 'has-text-right')]/span)[1]")
    private WebElement txtPerPeriodPrice;

    /**
     * Click on pay outside mamipay checkbox
     * @throws InterruptedException
     */
    public void clickOnPayOutsideMamipayCheckBox() {
        selenium.javascriptClickOn(cBoxPayOutsideMamipay);
    }

    /**
     * check if pop up active
     * @return visible is true otherwise false
     */
    public boolean isPopUpActive() {
        return selenium.waitInCaseElementVisible(popUpActive, 5) != null;
    }

    /**
     * Get pop-up title as string
     * @return string data type
     */
    public String getPayOutsideMamipayTitle() {
        return selenium.getText(popUpTitle);
    }

    /**
     * Get pop-up caption as string
     * @return string data type
     */
    public String getPayOutsideMamipayCaption() {
        return selenium.getText(popUpCaption);
    }

    /**
     * Check for both cancel and yes mark / ya, tandai button is visible
     * @return both btn must visible for true condition. Other than that false.
     */
    public boolean isCancelAndAcceptButton() {
        return selenium.waitInCaseElementVisible(btnCancelPopUp, 10) != null
                && selenium.waitTillElementIsVisible(btnYesMarkPopUp, 10) != null;
    }

    /**
     * Clicks on yes mark button
     * @throws InterruptedException
     */
    public void clicksOnYesMark() throws InterruptedException {
        selenium.clickOn(btnYesMarkPopUp);
    }

    /**
     * Get payment status text
     * @return string data type e.g "Telah Diterima di luar MamiPay"
     */
    public String getPaymentStatus() {
        return selenium.getText(txtPaymentStatus);
    }

    /**
     * Wait until pop up is not visible
     * @throws InterruptedException
     */
    public void waitTillPopUpPayOutsideMamipayNotVisible() throws InterruptedException {
        while (isPopUpActive()) {
            selenium.hardWait(1);
        }
    }

    /**
     * Click on edit additional price
     * @throws InterruptedException
     */
    public void clicksOnEditAdditionalPrice() throws InterruptedException {
        selenium.clickOn(buttonEditAdditionalPrice);
    }

    /**
     * Get active pop up title header
     * @return string data type e.g "Biaya Tambahan"
     */
    public String getPopUpHeaderTitle() {
        return selenium.getText(txtHeaderTitle);
    }

    /**
     * dismiss pop up by clicks on x button
     * @throws InterruptedException
     */
    public void dismissPopUp() throws InterruptedException {
        selenium.clickOn(btnClosePopUp);
    }

    /**
     * Clicks on button edit bank transfer
     * @throws InterruptedException
     */
    public void clicksOnBankTransferEdit() throws InterruptedException {
        selenium.clickOn(btnEditBankTransfer);
    }

    /**
     * Get button text, button must be the only one in the active pop - up
     * @return string data type e.g "Saya Mengerti"
     */
    public String getActivPopUpButtonText() {
        return selenium.getText(txtButtonActive);
    }

    /**
     * Click on box tenant info to go to tenant details
     * @throws InterruptedException
     */
    public void clickOnTenantInfo() throws InterruptedException {
        selenium.clickOn(boxTenantInfo);
    }

    /**
     * Click on edit kost price
     */
    public void clickOnEditKostPrice() throws InterruptedException {
        selenium.clickOn(buttonEditKostPrice);
    }

    /**
     * Get total cost
     * @return string data type total cost
     */
    public String getTotalCostText() {
        return selenium.getText(txtTotalCost);
    }

    /**
     * Get additional price text
     * @return string data type additional price first list
     */
    public String getAdditionalPriceText() {
        return selenium.getText(txtAdditionalPrice);
    }

    /**
     * Get price cost per period
     * @return string data type of per period text
     */
    public String getPerPeriodText() {
        return selenium.getText(txtPerPeriodPrice);
    }

    /**
     * check if price target is present on the price details
     * @param priceName price name target
     * @return visible/present true otherwise false
     */
    public boolean isPricePresent(String priceName) {
        String priceEl = "//*[.='"+priceName+"']";
        return selenium.waitInCaseElementVisible(By.xpath(priceEl), 30) != null;
    }
}
