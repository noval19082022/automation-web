package pageobjects.mamikos.owner.kostmanagement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class TenantDetailsPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public TenantDetailsPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(css= "div#billingManagementProfileMenu")
    private WebElement boxTenantProfile;

    public boolean isTenantProfileVisible() {
        return selenium.waitInCaseElementVisible(boxTenantProfile, 20) != null;
    }
}
