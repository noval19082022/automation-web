package pageobjects.mamikos.owner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class DashboardOwnerPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public DashboardOwnerPO (WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//p[contains(text(), 'Atur Harga')]/ancestor::a")
    private WebElement updatePriceButton;

    @FindBy(xpath = "//p[contains(text(), 'Atur Ketersediaan Kamar')]/ancestor::a")
    private WebElement updateRoomButton;

    @FindBy(xpath = "//button[contains(.,'Chat CS')]")
    private WebElement chatCSButton;

    @FindBy(xpath = "//p[contains(text(),'Tambah Properti')]")
    private WebElement addKostButton;

    @FindBy(css = ".mamiads-card")
    private WebElement saldoMamiAdsButton;

    @FindBy(css = ".mamipoin-card__highlight")
    private WebElement tooltip;

    @FindBy(xpath = "//p[contains(.,'Tambah Penyewa')]")
    private WebElement addTenantContractButton;

    @FindBy(xpath = "//*[contains(text(), 'Pekerjaan')]")
    private WebElement jobMenuButton;

    @FindBy(xpath = "//div[contains(text(), 'MamiPoin')]")
    private WebElement mamipoinButton;

    @FindBy(css = ".c-notification__count")
    private WebElement notificationCounterLabel;

    @FindBy(css = ".c-notification__see-more")
    private WebElement seeAllNotifButton;

    @FindBy(xpath = "//p[.='Kelola Tagihan']")
    private WebElement billsMenuButton;

    @FindBy(xpath = "//div[@class='owner-kos-list col-xs-12']/div[1]//div[@class='kos-card__expandable']")
    private WebElement seeMoreButton;

    @FindBy(xpath = "//div[@class='membership-card__label bg-c-label bg-c-label--rainbow bg-c-label--rainbow-black']")
    private WebElement gplevel;

    @FindBy(xpath = "//div[@class='membership-card__expired-date-gp']")
    private WebElement gpExpiredDate;

    @FindBy(xpath = "//div[@class='membership-card__section goldplus bg-c-grid__item bg-is-col-5']//div[@class='bg-c-card bg-c-card--lined bg-c-card--sm bg-c-card--light']")
    private WebElement gpCard;

    @FindBy(css = ".goldplus-register-benefit__button")
    private WebElement daftarGPButton;

    @FindBy(css= ".bg-c-red-dot")
    private WebElement redDot;

    @FindBy(css= ".c-mk-card__body .update-price__action-button")
    private WebElement bodyManageAdditionalPrice;

    @FindBy(css ="//*[., '']")
    private WebElement manageAllAdditionalPriceButton;

    @FindBy(xpath = "//p[.='Manajemen Kos']")
    private WebElement kosManagementButton;

    @FindBy(xpath = "//button[@class='bg-c-button auto-bbk-info-bar__button bg-c-button--tertiary-inversed bg-c-button--md bg-c-button--block']")
    private WebElement lengkapiDataDiriButton;

    @FindBy(xpath = "//*[text()='Lengkapi data diri Anda']")
    private WebElement lengkapiDataDiriAndaMenu;

    @FindBy(css =" .dashboard-desktop__body .widget-card__content > a:nth-of-type(1) > .bg-c-card--light")
    private WebElement widgetRoomALlotment;

    @FindBy(css =".wrapper__body" )
    private WebElement scrollWrapper;

    @FindBy(css =".property-list" )
    private WebElement scrollPropertyList;

    private By buttonBackRoom = By.cssSelector(".mdi-arrow-left");

    private By roomAllotmentPage = By.cssSelector("div.room-page__header");

    @FindBy(css ="[placeholder='Masukkan nama atau nomor kamar']" )
    private WebElement searchRoomAllotment;

    @FindBy(xpath ="//button[@class='availability-option__button']" )
    private WebElement filterRoomAllotment;

    @FindBy(xpath ="//th[2]/div[@class='th-wrap is-centered']" )
    private WebElement seeTablenamorRoom;

    @FindBy(xpath ="//th[3]/div[@class='th-wrap is-centered']" )
    private WebElement seeFloor;

    @FindBy(css =".c-mk-button" )
    private WebElement buttonAddRoom;

    @FindBy(css =".mdi-close-circle-outline" )
    private WebElement buttonCloseAddEdit;

    @FindBy(xpath ="//tbody[1]/tr[1]//i[@class='mdi mdi-square-edit-outline mdi-24px']" )
    private WebElement buttonEditRoom;

    @FindBy(css="tbody > tr:nth-of-type(1) .c-mk-icon__svg" )
    private WebElement buttonDeleteRoom;

    @FindBy(css =".c-mk-button--green-inverse" )
    private WebElement buttonDissmiss;

    @FindBy(css = ".rating-card__details")
    private WebElement ratingCardDetails;

    @FindBy(css = ".rating-card")
    private List<WebElement> reviewLists;

    @FindBy(css = "div:nth-of-type(8)")
    private List<WebElement> detailReviewLists;

    @FindBy(css = " div:nth-child(1) > div > p.widget-card__title.bg-c-text.bg-c-text--heading-4")
    private WebElement waktuMengelolaTitle;

    @FindBy(xpath="//p[contains(.,'Atur Harga')]")
    private WebElement setPrice;

    @FindBy(css = ".room-page__title")
    private WebElement roomPricePage;

    @FindBy(xpath="//p[contains(.,' Daftar ke Booking Langsung')]")
    private WebElement directBooking;

    @FindBy(xpath="//p[contains(.,' Daftar kontrak penyewa kos')]")
    private WebElement tenantButton;

    @FindBy(css = ".booking-register-wrapper")
    private WebElement bookingBenefit;

    @FindBy(xpath="//p[contains(.,' Pusat Bantuan')]")
    private WebElement helpCenterWidget;

    @FindBy(css = ".property-reviews-card__link")
    private WebElement seeAllKostReview;

    @FindBy(xpath = "//p[contains(.,'Pemilik')]")
    private WebElement helpCenter;

    @FindBy(css = ".bg-c-empty-state.billing-management-empty-state")
    private WebElement manageBillBlankPage;

    @FindBy(xpath = "//*[@class='suggestion-modal__title']")
    private WebElement totalNotBookingPopup;

    @FindBy(xpath = "//*[@class='mdi mdi-close mdi-24px']")
    private WebElement closeIconOnNotBookingPopup;

    @FindBy(xpath ="//p[.='Kos Anda Belum Bisa Di-Booking']")
    private WebElement manageBookingBlankPage;

    @FindBy(css = ".rating-card__wrapper")
    private List<WebElement> ratingCardWrapperLists;

    @FindBy(xpath = "//p[contains(., \"Waktunya Mengelola Kos\")]")
    private WebElement waktunyaMengelolaKosText;

    @FindBy(css = "a[href='/ownerpage/add']")
    private WebElement addNewProperty;

    @FindBy(id = "ownerAddBtnLg")
    private WebElement addNewPropertyAds;

    @FindBy(xpath = "//button[contains(.,\"Tambahkan Data\")]")
    private WebElement addDataButton;

    @FindBy(css = ".choose-property-type__property > button:nth-of-type(1)")
    private WebElement kosButton;

    @FindBy(css = ".choose-property-type__create-button")
    private WebElement createKosButton;

    @FindBy(css = ".greeting-section__name")
    private WebElement greetingUserLabel;

    @FindBy(css = ".bg-c-link:nth-child(4) > div")
    private WebElement penyewaSection;

    @FindBy(xpath = "//p[contains(text(), 'Mengelola data kamar kos')]")
    private WebElement updateRoomLabel;

    @FindBy(xpath = "//p[contains(text(), 'Update harga sewa di iklan kos')]")
    private WebElement updatePriceLabel;

    @FindBy(xpath = "//p[contains(.,'Penyewa di')]")
    private WebElement penyewaPage;

    @FindBy(css = "button:nth-of-type(2)")
    private WebElement apartmentMenu;

    @FindBy(xpath = "//*[contains(@class, 'action-card--light')] //*[contains(@class, 'content-title')]")
    private List<WebElement> widgetTitleWaktunyaMengelolaProperti;

    @FindBy(xpath = "//*[contains(@class, 'action-card--light')] //*[contains(@class, 'content-subtitle')]")
    private List<WebElement> widgetSubtitleWaktunyaMengelolaProperti;

    @FindBy(xpath = "//p[contains(.,'Ubah Peraturan Masuk Kos')]")
    private WebElement peraturanMasukKosMenu;

    @FindBy(xpath = "//h3[@class='bg-c-modal__body-title']")
    private WebElement warningBroadcast;

    @FindBy(xpath = "//p[contains(.,'Pendapatan Total')]")
    private WebElement pendapatanText;

    @FindBy(xpath = "//span[contains(.,'Lihat laporan keuangan')]")
    private WebElement pendapatanButton;

    @FindBy(xpath ="//p[contains(.,'Buka Laporan')]")
    private WebElement financialReportTitleText;

    @FindBy(xpath ="//p[contains(.,'Untuk saat ini')]")
    private WebElement financialReportSubTitleText;

    @FindBy(xpath = "//*[contains(@class, 'mk-action-card--dark')] //p[contains(.,'Berapa lama')]")
    private WebElement disburseText;

    //------------------pengajuan sewa section---------
    @FindBy(xpath= "//*[@class='booking-confirmation-section__content']")
    private WebElement pengajuanSewaSection;

    @FindBy(xpath= "//*[@class='bg-c-button bg-c-button--tertiary bg-c-button--lg']")
    private WebElement rejectButtonOnDashboard;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    private WebElement confirmButtonOnDashboard;

    /**
     * Click on update price button
     * @throws InterruptedException
     */
    public void clickUpdatePrice() throws InterruptedException {
        selenium.clickOn(updatePriceButton);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
    }

    /**
     * Click on update room button
     * @throws InterruptedException
     */
    public void clickUpdateRoom() throws InterruptedException {
        selenium.clickOn(updateRoomButton);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
    }

    /**
     * Click on "Chat CS"
     * @throws InterruptedException
     */
    public void clickOnChatCS() throws InterruptedException {
        selenium.clickOn(chatCSButton);
    }

    /**
     * Click on "Add Kost Button"
     * @throws InterruptedException
     */
    public void clickAddKostButton() throws InterruptedException {
        selenium.clickOn(addKostButton);
    }

    /**
     * Click on kos button in choose property pop up
     * @throws InterruptedException
     */
    public void clickKosInPopUp() throws InterruptedException {
        selenium.clickOn(kosButton);
    }

    /**
     * Click on create kos button in choose property pop up
     * @throws InterruptedException
     */
    public void clickCreateKosInPopUp() throws InterruptedException {
        selenium.clickOn(createKosButton);
    }

    /**
     * Click on "Saldo MamiAdsButton"
     * @throws InterruptedException
     */
    public void clickSaldoMamiAdsButton() throws InterruptedException {
        selenium.clickOn(saldoMamiAdsButton);
    }

    /**
     * Check and wait for element tooltip is available
     * @return
     */
    public boolean isTooltipAppear() {
        return selenium.waitInCaseElementVisible(tooltip, 5) != null;
    }

    /**
     * Check if tooltip appear and click on it
     * @throws InterruptedException
     */
    public void dismissTooltip() throws InterruptedException {
        if(isTooltipAppear()) {
            do {
                selenium.click(tooltip);
            }
            while(isTooltipAppear());
        }
    }

    /**
     * Click on Add Tenant Contract Button
     * @throws InterruptedException
     */
    public void clickAddTenantContractButton() throws InterruptedException {
        selenium.pageScrollInView(addTenantContractButton);
        selenium.clickOn(addTenantContractButton);
    }

    /**
     * Click on Job Menu Button
     * @throws InterruptedException
     */
    public void clickJobMenuButton() throws InterruptedException {
        selenium.clickOn(jobMenuButton);
    }

    /**
     * Click on Mamipoin Button
     * @throws InterruptedException
     */
    public void clickMamipoinButton() throws InterruptedException {
        selenium.clickOn(mamipoinButton);
    }

    /**
     * Verify link see all notification appear
     * @return true if appear
     */
    public boolean isSeeAllNotifAppear() {
        return selenium.waitInCaseElementVisible(seeAllNotifButton, 3) != null;
    }

    /**
     * Verify notification counter appear and not null
     * @return true if appear and not null
     */
    public boolean  isNotifCounterAppear() {
        return selenium.waitInCaseElementVisible(notificationCounterLabel, 3) != null
                && selenium.getText(notificationCounterLabel) != null;
    }

    /**
     * Count Notification list
     * @return integer number of notification
     */
    public int countNotificationList() {
        return selenium.waitTillAllElementsAreLocated(By.cssSelector(".c-notification__item")).size();
    }

    /**
     * Click on bills menu button
     * @throws InterruptedException
     */
    public void clickBillsMenu() throws InterruptedException {
        selenium.clickOn(billsMenuButton);
    }

    /**
     * Get text on gp label
     * @return gpLevel
     */
    public String getGpLabel() {
        selenium.waitTillElementIsVisible(gplevel, 30);
        return selenium.getText(gplevel);
    }

    /**
     * Get text on gp Expired Date
     * @return gpExpiredDate
     */
    public String getGpExpiredDate() {
        selenium.waitTillElementIsVisible(gpExpiredDate, 30);
        return selenium.getText(gpExpiredDate);
    }

    /**
     * Validate red dot appear
     *
     */
    public void redDotAppear(){
        selenium.waitTillElementIsNotVisible(redDot);
    }

    /**
     * Click gp label
     *
     */
    public void clickGPLabel() throws InterruptedException{
        selenium.clickOn(gplevel);
    }

    /**
     * Click gp Card
     *
     */
    public void clickGPCard() throws InterruptedException{
        selenium.clickOn(gpCard);
    }

    /**
     * check if manage additional price pop up present in the screen
     * @return true if the screen is present otherwise false
     */
    public boolean isManageAdditionalPricePopUpPresent() {
        return selenium.waitInCaseElementVisible(bodyManageAdditionalPrice, 5) != null;
    }

    /**
     * Click on manage all price button on manage additional price pop-up
     * @throws InterruptedException
     */
    public void clickOnManageAllKostButton() throws InterruptedException {
        if (isManageAdditionalPricePopUpPresent()) {
            selenium.clickOn(manageAllAdditionalPriceButton);
        }
    }

    /**
     * Click kos management button in left menu
     * @throws InterruptedException
     */
    public void clickKosManagement() throws InterruptedException {
        selenium.clickOn(kosManagementButton);
    }

    /**
     * Click lengkapi data diri on dashboard owner
     * @throws InterruptedException
     */
    public void lengkapiDataDiri() throws InterruptedException {
        selenium.clickOn(lengkapiDataDiriButton);
    }
    /**
     * Verify lengkapi data diri anda
     */
    public boolean lengkapiDataDiriAnda(){
        return selenium.waitInCaseElementVisible(lengkapiDataDiriAndaMenu, 3) != null;
    }
    /**
     * Click on widget room allotment
     * @throws InterruptedException
     */
    public void clickWidgetRoomAllotment() throws InterruptedException {
        selenium.javascriptClickOn(widgetRoomALlotment);
        selenium.hardWait(3);
    }

    /**
     * Scroll down page dashboard
     * @throws InterruptedException
     */
    public void scrollBottomPage() throws InterruptedException {
        selenium.hardWait(3);
        selenium.pageScrollInView(scrollWrapper);
    }

    /**
     * Scroll page property list
     * @throws InterruptedException
     */
    public void scrollPropertyList() throws InterruptedException {
        selenium.hardWait(3);
        selenium.pageScrollInView(scrollPropertyList);
    }

    /**
     * check button back is present
     * @return true if the button back is present otherwise false
     */
    public boolean isButtonBackRoomShow() {
      return selenium.waitInCaseElementVisible(buttonBackRoom, 3) != null;
    }

    /**
     * check owner already at screen room page
     * @return true if the screen is present otherwise false
     */
    public boolean isRoomAllotmentPageShow() {
        return selenium.waitInCaseElementVisible(roomAllotmentPage, 3) != null;
    }

    /**
     * click on field search room
     * @throws InterruptedException
     */
    public void clickOnSearchRoomAllotment() throws InterruptedException {
        selenium.clickOn(searchRoomAllotment);
    }

    /**
     * click on filter search room
     * @throws InterruptedException
     */
    public void clickOnFilterRoomAllotment() throws InterruptedException {
        selenium.doubleClickOnElement(filterRoomAllotment);
    }

    /**
     * Verify Table column Name/Room Number appear
     * @return true if appear
     */
    public boolean isNameOrRoomAppear() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.waitInCaseElementVisible(seeTablenamorRoom,5) != null;
    }

    /**
     * click on floor
     * @throws InterruptedException
     */
    public void clickOnFloor() throws InterruptedException {
        selenium.clickOn(seeFloor);
    }

    /**
     * click on added room
     * @throws InterruptedException
     */
    public void clickOnAddedRoom() throws InterruptedException {
        selenium.clickOn(buttonAddRoom);
        selenium.clickOn(buttonCloseAddEdit);
    }

    /**
     * click on edit room
     * @throws InterruptedException
     */
    public void clickOnEditRoom() throws InterruptedException {
        selenium.clickOn(buttonEditRoom);
        selenium.clickOn(buttonCloseAddEdit);
    }

    /**
     * click on delete room
     * @throws InterruptedException
     */
    public void clickOnDeleteRoom() throws InterruptedException {
        selenium.clickOn(buttonDeleteRoom);
        selenium.clickOn(buttonDissmiss);
    }

    /**
     * Scroll to Rating Card Details and Click Rating Card Details
     * @throws InterruptedException
     */
    public void clickOnRatingCardDetails() throws InterruptedException {
        selenium.pageScrollInView(ratingCardDetails);
        selenium.clickOn(ratingCardDetails);
    }

    /**
     * Get No Review List Text
     * @param text
     * @return string
     */
    public String getNoReviewListText(String text){
        selenium.waitInCaseElementVisible(By.xpath("//p[contains(., '"+text+"')]"), 5);
        return selenium.getText(By.xpath("//p[contains(., '"+text+"')]"));
    }

    /**
     * Get Non Premium Text
     * @param text
     * @return string
     */
    public String getNonPremiumText(String text){
        selenium.waitInCaseElementVisible(By.xpath("//p[contains(., '"+text+"')]"), 5);
        return selenium.getText(By.xpath("//p[contains(., '"+text+"')]"));
    }

    /**
     * Get Review Lists Card
     * @return integer
     */
    public Integer getReviewListsCard(){
        selenium.pageScrollInView(reviewLists.get(0));
        return reviewLists.size();
    }

    /**
     * Click on Kos Review Listing
     * @throws InterruptedException
     */
    public void clickOnKosReviewListing() throws InterruptedException {
        selenium.clickOn(reviewLists.get(1));
    }

    /**
     * Is Detailed Review Lists Appear?
     * @return true or false
     */
    public Boolean isDetailedReviewListsAppear(){
        return selenium.waitInCaseElementVisible(detailReviewLists.get(0), 3) != null;
    }

    /**
     * Click on See All Kost Review
     * @throws InterruptedException
     */
    public void clickOnSeeAllKostReview() throws InterruptedException {
        selenium.clickOn(seeAllKostReview);
    }

    /**
     * Get See All Kost Review Text
     * @return string
     */
    public String getSeeAllKostReviewText(){
        return selenium.getText(seeAllKostReview);
    }

    /**
     * Is See All Kost Review Text Appear?
     * @return true or false
     */
    public Boolean isSeeAllKostReviewTextAppear(){
        return selenium.waitInCaseElementVisible(seeAllKostReview, 3) != null;
    }

    /**
     * Get string text waktu mengelola section
     * @return string text e.g Waktu Mengelola Kos
     */
    public String getWaktuMengelolaTitle() {
        return selenium.getText(waktuMengelolaTitle);
    }

        /**
     * click on Atur Harga widget
     * @throws InterruptedException
     */
    public void clickOnSetPrice() throws InterruptedException{
        scrollToSetPriceText();
        selenium.javascriptClickOn(setPrice);
        selenium.hardWait(2);
    }

    /**
     * check booking langsung widget will present
     * @return true if the screen is present otherwise false
     */
    public boolean isDirectBookingPresent() {
        return selenium.waitInCaseElementVisible(directBooking,3) !=null;

    }

    /**
     * click on Daftar Booking Langsung button on widget
     * @throws InterruptedException
     */
    public void clickOnDirectBookingWidget() throws InterruptedException{
        selenium.pageScrollInView(directBooking);
        selenium.hardWait(2);
        selenium.javascriptClickOn(directBooking);
    }

    /**
     * click on Penyewa button on widget
     * @throws InterruptedException
     */
    public void clickOnTenantWidget() throws InterruptedException{
        selenium.pageScrollInView(tenantButton);
        selenium.hardWait(2);
        selenium.javascriptClickOn(tenantButton);

    }

    /**
     * check owner already at booking benefit page
     * @return true if the screen is present otherwise false
     */
    public boolean isBookingBenefitShow() {
        return selenium.waitInCaseElementVisible(bookingBenefit,3) !=null;

    }

    /**
     * click on Pusat Bantuan button on widget
     * @throws InterruptedException
     */
    public void clickOnHelpCenter() throws InterruptedException{
        selenium.javascriptClickOn(helpCenterWidget);
    }

    /**
     * check owner already at help center page
     * @return true if the screen is present otherwise false
     */
    public boolean isHelpCenterShow() {
        return selenium.waitInCaseElementVisible(helpCenter,3) !=null;
    }

    /**
     * check owner already at manage bill blank page
     * @return true if the screen is present otherwise false
     */
    public boolean isManageBillBlankPageShow() {
        return selenium.waitInCaseElementVisible(manageBillBlankPage,3) !=null;
    }
    /**
     * check total kost not booking popup
     * @return true if the screen is present otherwise false
     */
    public boolean isTotalNotBookingPopupPresent() throws InterruptedException{
        return selenium.waitInCaseElementVisible(totalNotBookingPopup,5) !=null;
    }

    /**
     * click on close icon on popup total kost not booking
     * @throws InterruptedException
     */
    public void clickOnCloseOnPopupTotalNotBooking() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(closeIconOnNotBookingPopup);
        selenium.hardWait(2);
    }

    public boolean isManageBookingBlankPagePresent() throws InterruptedException {
        selenium.pageScrollInView(manageBookingBlankPage);
        return selenium.waitTillElementIsVisible(manageBookingBlankPage,4) !=null;
    }

    /**
     * Get Rating Card Wrapper List Size
     * @return int
     */
    public Integer getRatingCardWrapperSize(){
        selenium.waitInCaseElementVisible(ratingCardWrapperLists.get(0), 3);
        return ratingCardWrapperLists.size();
    }

    /**
     * Is Waktu Mengelola Kos Text Appear?
     * @return true or false
     */
    public Boolean isWaktunyaMengelolaKosTextAppear(){
        return selenium.waitInCaseElementVisible(waktuMengelolaTitle, 3) != null;
    }

    /**
     * Click on Add New Prooerty Ads
     * @throws InterruptedException
     */
    public void clickOnAddNewPropertyAds() throws InterruptedException {
        selenium.clickOn(addNewProperty);
        if(selenium.waitInCaseElementVisible(addNewPropertyAds, 5) != null)
            selenium.clickOn(addNewPropertyAds);
    }

    /**
     * Click Add Data Button on Pop Up
     * @throws InterruptedException
     */
    public void clickOnAddDataButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(addDataButton, 3);
        selenium.clickOn(addDataButton);
    }

    /**
     * Get username greeting in owner dashboard
     * @return String "Halo, <username>"
     */
    public String getUserGreeting() {
        return selenium.getText(greetingUserLabel);
    }

    /**
     * Click on username greeting in owner dashboard
     * @throws InterruptedException
     */
    public void clickUserGreeting() throws InterruptedException {
        selenium.clickOn(greetingUserLabel);
    }

    /**
     * Get booking langsung greeting in owner dashboard
     * @return String "Daftar Booking Langsung"
     */
    public String getDirectBooking() {
        return selenium.getText(directBooking);
    }


    /**
     * Click on Penyewa section on owner dashboard
     */
    public void clickPenyewaSection() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(penyewaSection);
    }
    /**
     * Get update room  greeting in owner dashboard
     * @return String "Atur Ketersediaan Kamar"
     */
    public String getUpdateRoom() {
        selenium.pageScrollInView(updateRoomButton);
        return selenium.getText(updateRoomButton);
    }

    /**
     * Get update room  greeting in owner dashboard
     * @return String "Atur Harga"
     */
    public String getUpdatePrice() {
        return selenium.getText(updatePriceButton);
    }

    /**
     * Get Penyewa  greeting in owner dashboard
     * @return String "Penyewa"
     */
    public String getPenyewa() {
        return selenium.getText(tenantButton);
    }

    /**
     * Get Add Tenant greeting in owner dashboard
     * @return String "Tambah Penyewa"
     */
    public String getTambahPenyewa() {
        return selenium.getText(addTenantContractButton);
    }

    /**
     * Get Add Tenant greeting in owner dashboard
     * @return String "Tambah Penyewa"
     */
    public String getPusatBantuan() {
        return selenium.getText(helpCenterWidget);
    }

    /**
     * Get Penyewa title in penyewa page
     * @return String "Penyewa di"
     */
    public String getPenyewaTitlePage() {
        return selenium.getText(penyewaPage);
    }

    /**
     * Choose Apartment on page Pilih Jenis Properti
     * @throws InterruptedException
     */
    public void chooseApartmentPropertyType() throws InterruptedException {
        selenium.clickOn(apartmentMenu);
    }

    /**
     * check if daftar member goldplus is appear
     * @return true if the screen is present otherwise false
     */
    public boolean isDaftarGoldplusAppear() {
        return selenium.waitInCaseElementVisible(daftarGPButton,5) !=null;
    }

    /** Get text guide how to use
     * @param widgetList is to get text inside waktunya mengelola properti (title and content)
     * @param index to get widget list element
     * @return String text widget waktunya mengelola properti
     */
    public String widgetWaktunyaMengelolaProperti (String widgetList, int index){
        List<WebElement> element = null;
        if(widgetList.equals("title")){
            element = widgetTitleWaktunyaMengelolaProperti;
        } else if (widgetList.equals("subtitle")){
            element = widgetSubtitleWaktunyaMengelolaProperti;
        }
        return selenium.getText(element.get(index));
    }

    /**
     * Get Ubah peraturan masuk kos menu
     * @return String "Ubah peraturan masuk kos"
     */
    public String getPeraturanMenu() {
        return selenium.getText(peraturanMasukKosMenu);
    }


    /**
     * Click on ubah peraturan masuk kos button
     * @throws InterruptedException
     */
    public void clickPeraturanMasukKos() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(peraturanMasukKosMenu);
        selenium.hardWait(2);
    }

    /**
     * Get warning broadcast
     * @return Text of broadcast
     */

    public String getWarningBroadcast() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(warningBroadcast);
    }

    /**
     * Get pendapatan title
     * @return Text of broadcast
     */
    public String getPendapatanTitle(){
        return selenium.getText(pendapatanText);
    }

    /**
     * Click on Lihat laporan keuangan button
     * @throws InterruptedException
     */
    public void clickPendapatanButton() throws InterruptedException{
        selenium.clickOn(pendapatanButton);
    }

    /**
     * Get laporan keuangan title
     * @return Text of laporan keuangan
     */
    public String getFinancialreportTitle(){
        return selenium.getText(financialReportTitleText);
    }

    /**
     * Get laporan keuangan Subtitle
     * @return Text of laporan keuangan subtitle
     */
    public String getFinancialreportnsubTitle(){
        return selenium.getText(financialReportSubTitleText);
    }

    /**
     * click Disbursement
     * @throws InterruptedException
     */
    public void clickDisbursementButton()throws InterruptedException{
        selenium.hardWait(3);
        selenium.clickOn(disburseText);
    }

    /**
     * check if set price is appear
     * @return true if appears Ubah Harga
     */
    public boolean setPriceAsPresent(){
        return selenium.waitInCaseElementVisible(setPrice,4) !=null;
    }

    /**
     * Scroll to Atur harga
     * @throws InterruptedException
     */
    private void scrollToSetPriceText() throws InterruptedException {
        for(int i = 0; i <= 30; i++) {
            selenium.pageScrollUsingCoordinate(0, 1500);
            selenium.hardWait(5);
            if(setPriceAsPresent()) {
                break;
            }
        }
    }

    /**
     * Click on Text Info Untuk Anda
     *
     * @throws InterruptedException
     */
    public void clickOnText(String textInfoAnda) throws InterruptedException {
        selenium.hardWait(7);
        String element = "//*[contains(text(),'"+textInfoAnda+"')]";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 3);
        selenium.clickOn(By.xpath(element));
        selenium.hardWait(1);
    }

    /**
     * check if pengajuan section dashboard is present
     * @return true if appears pengajuan sewa section
     */
    public boolean isPengajuanSewaSectionPresent() throws InterruptedException {
        return selenium.waitInCaseElementVisible(pengajuanSewaSection,7) != null;
    }

    /**
     * Click on Reject button on dashboard
     * @throws InterruptedException
     */
    public void clickOnRejectButtonOnDashboard() {
        selenium.pageScrollInView(rejectButtonOnDashboard);
        selenium.javascriptClickOn(rejectButtonOnDashboard);
    }

    /**
     * Click on confirm button on dashboard
     * @throws InterruptedException
     */
    public void clickOnConfirmButtonOnDashboard() {
        selenium.pageScrollInView(confirmButtonOnDashboard);
        selenium.javascriptClickOn(confirmButtonOnDashboard);
    }
}
