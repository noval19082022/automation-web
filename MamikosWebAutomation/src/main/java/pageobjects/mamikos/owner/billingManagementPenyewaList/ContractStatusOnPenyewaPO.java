package pageobjects.mamikos.owner.billingManagementPenyewaList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;
import pageobjects.mamikos.tenant.profile.KosSayaPO;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.Collections;
import java.util.List;

public class ContractStatusOnPenyewaPO {

    WebDriver driver;
    SeleniumHelpers selenium;
//    private KosSayaPO kosSaya = new KosSayaPO(driver);

    public ContractStatusOnPenyewaPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(css = "div.wrapper__body > div > nav > p")
    private WebElement penyewaPageTitle;

    @FindBy(xpath = "//*[@class='kos-filter__kos-option bg-c-select']//*[@class='bg-c-select__trigger-text']")
    private WebElement kosSearchPenyewa;

    @FindBy(css = ".status-filter > div")
    private WebElement filterBoxPenyewa;

    @FindBy(xpath = "//button[@type='button'][contains(., 'Download biodata penyewa')]")
    private WebElement downloadBiodataPenyewaButton;

    @FindBy(xpath = "//p[contains(., 'Fitur ini sedang kami kembangkan')]")
    private WebElement popUpDownloadBiodataPenyewa;

    @FindBy(xpath = "//span[@class='bg-c-checkbox__icon']")
    private WebElement checkboxPopUpDownloadBiodata;

    @FindBy(xpath = "//p[contains(., 'Kami akan memberitahu Anda saat fitur ini sudah tersedia.')]")
    private WebElement informationAfterTickCheckbox;

    @FindBy(xpath = "//button[@type='button'][contains(., 'Kembali')]")
    private WebElement kembaliButtonOnPopUp;

    @FindBy(xpath = "(//a[@class='bg-c-link bg-c-link--medium-naked'])[2]")
    private WebElement selengkapnyaButton;

    @FindBy(xpath = "(//div[@class='tenant-list__card'])[1]")
    private WebElement contractNumber1;

    @FindBy(xpath = "//div[@class='tenant-card-item__info']")
    private List<WebElement> contractList;

    @FindBy(xpath = "//div[@class='bg-c-empty-state']")
    private WebElement contractPageEmpty;

    @FindBy(xpath = "//a[@href='#contract']")
    private WebElement kontrakSewaTab;

    @FindBy(xpath = "//div[@class='tab-contract__cta']/child::*[1]")
    private WebElement ubahKontrakPenyewaBtn;

    @FindBy(css = ".bg-c-text--heading-4")
    private WebElement totalBiayaPrice;

    @FindBy(xpath = "(//div[@class='bg-c-list-item__description'])[1]/child::*[2]")
    private WebElement hargaSewaPrice;

    @FindBy(xpath = "//div[@class='additional-costs-section__list']/child::*[1]/child::*[1]")
    private WebElement biayaLain1;

    @FindBy(xpath = "//div[@class='additional-costs-section__list']/child::*[1]/child::*[2]")
    private WebElement biayaLainPrice1;

    @FindBy(xpath = "//div[@class='additional-costs-section__list']/child::*[2]/child::*[1]")
    private WebElement sampahName;

    @FindBy(xpath = "//div[@class='additional-costs-section__list']/child::*[2]/child::*[2]")
    private WebElement sampahPrice;

    //---------------------warning of tenant who don't have kos saya-----------------------//
    @FindBy(xpath = "(//div[@class='bg-c-alert__content']/child::*[1])[1]")
    private WebElement warningLine1;

    @FindBy(xpath = "(//div[@class='bg-c-alert__content']/child::*[2])[1]")
    private WebElement warningLine2;

    //---------------------waiting terminated confirmation-----------------------//
    @FindBy(xpath = "//div[@class='bg-c-alert__content']/child::p")
    private WebElement alertTermination;

    @FindBy(xpath = "//b[contains(., 'Selengkapnya')]")
    private WebElement selengkapnyaBtn;

    @FindBy(xpath = "//div[@class='terminate-confirmation__actions']/child::*[1]")
    private WebElement tolakBtn;

    @FindBy(xpath = "//div[@class='terminate-confirmation__actions']/child::*[2]")
    private WebElement konfirmasiBtn;

    //---------------------change owner's phone number-----------------------//
    @FindBy(xpath = "//div[@class='bg-c-alert__content']/child::*[2]")
    private WebElement krmUlangKodeBtn;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--heading-4 ']")
    private WebElement krmKodeUnikPage;

    @FindBy(xpath = "//input[@class='bg-c-input__field']")
    private WebElement phoneNumberField;

    @FindBy(xpath = "//button[contains(., 'Gunakan')]")
    private WebElement gunakanBtn;

    @FindBy(xpath = "(//*[@class='bg-c-select__trigger-text'])[1]")
    private WebElement dropDownKostBtn;

    @FindBy(xpath = "//*[@class='bg-c-input__field']")
    private WebElement searchKostField;

    @FindBy(xpath = "//h4[@class='bg-c-empty-state__title']")
    private WebElement emptyPenyewaLabel;

    @FindBy(xpath = "//h3[@class='bg-c-modal__body-title']")
    private WebElement noHPPenyewaPopUp;

    /**
     * Get penyewa page title text
     * @return string data type
     */
    public String getPenyewaTitle(){
        return selenium.getText(penyewaPageTitle);
    }

    /**
     * Click on search kost
     */
    public void clickSearchKost() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(kosSearchPenyewa);
        selenium.hardWait(2);
    }

    /**
     * Search kost on search bar
     * then click on result of search kost (example : search kost reykjavik then click on result kost)
     * @param searchKost
     */
    public void searchKostOnPenyewa(String searchKost) throws InterruptedException{
        selenium.hardWait(5);
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementVisible(emptyPenyewaLabel,3);
        selenium.clickOn(dropDownKostBtn);
        selenium.hardWait(2);
        selenium.enterText(searchKostField, searchKost, true);
        selenium.hardWait(5);
        WebElement kostNameDropdown = driver.findElement(By.cssSelector("[data-popper-placement='bottom-start'] .bg-c-dropdown__menu-item"));
        selenium.waitInCaseElementVisible(kostNameDropdown,6);
        selenium.clickOn(kostNameDropdown);
        selenium.hardWait(5);
    }

    /**
     * Click filter on penyewa
     * And select filter
     * @param filter
     */
    public void clickFilterOnPenyewa(String filter) throws InterruptedException {
        selenium.clickOn(filterBoxPenyewa);
        selenium.javascriptClickOn(By.xpath("//a[@class='bg-c-dropdown__menu-item bg-u-radius--md'][contains(., '"+filter+"')]"));
        selenium.hardWait(2);
    }

    /**
     * Get contract status Kontrak aktif on penyewa list
     * @param contractStatus
     */
    public String getContractStatus(String contractStatus) throws InterruptedException{
        selenium.hardWait(4);
        WebElement element = driver.findElement(By.xpath("//div[@class='tenant-card-item__content']//*[contains(., '"+contractStatus+"')]"));
        return selenium.getText(element);
    }

    /**
     * Click on download biodata penyewa button on penyewa page
     */
    public void clickDownloadBiodataPenyewa() throws InterruptedException {
        selenium.clickOn(downloadBiodataPenyewaButton);
    }

    /**
     * Get wording pop up download biodata penyewa
     */
    public String getPopUpDownloadBiodataPenyewa(){
        return selenium.getText(popUpDownloadBiodataPenyewa);
    }

    /**
     * Tick on checkbox pop up download biodata penyewa
     */
    public void tickOnCheckBoxPopUpDownloadBiodata() throws InterruptedException {
        selenium.clickOn(checkboxPopUpDownloadBiodata);
    }

    /**
     * Get information on pop up download biodata penyewa after tick checkbox
     */
    public String getInformationAfterTickCheckbox(){
        return selenium.getText(informationAfterTickCheckbox);
    }

    /**
     * Click on kembali button after tick checkbox on pop up download biodata penyewa
     */
    public void clickKembaliOnPopUpAfterTickCheckbox() throws InterruptedException {
        selenium.clickOn(kembaliButtonOnPopUp);
    }

    /**
     * Click selengkapnya button based on contract name (tenant's contract)
     * @throws InterruptedException
     */
    public void clickSelengkapnyaNew(int index) throws InterruptedException {
        By element = By.xpath("(//b[contains(., 'Selengkapnya')])[" + index + "]");
        selenium.pageScrollInView(element);
        selenium.javascriptClickOn(element);
    }

    /**
     * Get number of booking list
     * @throws InterruptedException
     * @return number of elements
     */
    public int getNumberListOfContract() throws InterruptedException {
        selenium.hardWait(2);
        int numberOfElements = 0;
        if (selenium.waitInCaseElementVisible(contractNumber1, 5) != null){
                numberOfElements = contractList.size();
        } else {
            selenium.waitInCaseElementVisible(contractPageEmpty, 5);
        }
        return numberOfElements;
    }

    /**
     * Get one data booking status
     * @param index is number for specific data want to get
     * @return booking status
     */
    public String getContractName(int index) {
        By element = By.xpath("(//div[@class='tenant-card-item__content']/p)[" + index + "]");
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * click kontrak sewa tab
     * @throws InterruptedException
     */
    public void clickKontrakSewa() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(kontrakSewaTab);
    }

    /**
     * click ubah kontrak penyewa
     * @throws InterruptedException
     */
    public void clickUbahKontrakPenyewa() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 500);
        selenium.clickOn(ubahKontrakPenyewaBtn);
    }

    /**
     * get total biaya price
     * @return total biaya price
     */
    public String getTotalBiayaPrice(){
        return selenium.getText(totalBiayaPrice);
    }

    /**
     * get harga sewa price
     * @return harga sewa price
     */
    public String getHargaSewaPrice(){
        return selenium.getText(hargaSewaPrice);
    }

    /**
     * get listrik name - biaya lain ke 1
     * @return listrik name - biaya lain ke 1
     */
    public String getBiayaLain1(){
        return selenium.getText(biayaLain1);
    }

    /**
     * get listrik price - biaya lain ke 1
     * @return listrik price - biaya lain ke 1
     */
    public String getBiayaLainPrice1(){
        return selenium.getText(biayaLainPrice1);
    }

    /**
     * get sampah name - biaya lain ke 2
     * @return sampah name - biaya lain ke 2
     */
    public String getSampahName(){
        return selenium.getText(sampahName);
    }

    /**
     * get sampah price - biaya lain ke 2
     * @return sampah price - biaya lain ke 2
     */
    public String getSampahPrice(){
        return selenium.getText(sampahPrice);
    }

    //----------------tenant who don't have kos saya------------------//
    /**
     * get wording of warning tenant who don't have kos saya at Semua filter
     * @return wording of warning at Semua filter
     */
    public String getWarningAtSemuaFltr(){
        return selenium.getText(warningLine1);
    }

    /**
     * click hyperlink at warning on semua filter
     * @throws InterruptedException
     */
    public void clickHyperlinkSemuaFltr() throws InterruptedException {
        selenium.clickOn(warningLine2);
    }

    /**
     * get wording of warning tenant who don't have kos saya at Sedang menyewa filter
     * at line 1 -> title
     * @return wording of warning at Sedang menyewa filter
     */
    public String getWarningAtSdgMenyewaFltr(){
        return selenium.getText(warningLine1);
    }

    /**
     * get wording of warning tenant who don't have kos saya at Sedang menyewa filter
     * at line 2 -> subtitle
     * @return wording of warning at Sedang menyewa filter
     */
    public String getWarningLine2AtSdgMenyewaFltr(){
        return selenium.getText(warningLine2);
    }

    //---------------------waiting terminated confirmation-----------------------//
    /**
     * get alert termination at waiting terminate confirmation status
     * @return
     */
    public String getAlertTermination(){
        return selenium.getText(alertTermination);
    }

    /**
     * click Selengkapnya button at Penyewa list
     * @throws InterruptedException
     */
    public void clickSelengkapnya() throws InterruptedException {
        selenium.waitTillElementIsClickable(selengkapnyaBtn);
        selenium.clickOn(selengkapnyaBtn);
    }

    /**
     * get Tolak button at contract waiting terminate confirmation page
     * @return
     */
    public String getTolakButton(){
        return selenium.getText(tolakBtn);
    }

    /**
     * get Konfirmasi button at contract waiting terminate confirmation page
     * @return
     */
    public String getKonfirmasiButton(){
        return selenium.getText(konfirmasiBtn);
    }

    //---------------------change owner's phone number-----------------------//
    /**
     * click Kirim ulang kode hyperlink
     * @throws InterruptedException
     */
    public void clickKirimUlangKode() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(krmUlangKodeBtn);
    }

    /**
     * get Kirim kode unik ke penyewa page
     * @return Kirim Kode unik title page
     */
    public String getKrmKodeUnikPage(){
        return selenium.getText(krmKodeUnikPage);
    }

    /**
     * get old owner's phone number
     * @return phone number
     */
    public String getPhoneNumberPenyewa(){
        WebElement element = driver.findElement(By.xpath("//p[@class='bg-c-text bg-c-text--title-3 ']"));
        return selenium.getText(element);
    }

    /**
     * click Ubah nomor HP hyperlink
     * @throws InterruptedException
     */
    public void clickUbahNmrHp() throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//a[@class='bg-c-link bg-c-link--high']"));
        selenium.click(element);
    }

    /**
     * click phone number field
     * and change into the new number
     * @param
     * @throws InterruptedException
     */
    public void clickPhoneNmbField(String ubhPhoneNumber) throws InterruptedException {
        selenium.clickOn(phoneNumberField);
        selenium.enterText(phoneNumberField, ubhPhoneNumber, true);
        selenium.hardWait(2);
        selenium.clickOn(gunakanBtn);
        selenium.waitTillElementIsNotVisible(noHPPenyewaPopUp,3);
    }

    /**
     * click phone number field
     * and change into the old number (change into first condition)
     * @param ubhPhoneNumber
     * @throws InterruptedException
     */
    public void changeToFirstCondition(String ubhPhoneNumber) throws InterruptedException {
        selenium.clickOn(phoneNumberField);
        selenium.sendKeyBackSpace(phoneNumberField);
        selenium.enterText(phoneNumberField, ubhPhoneNumber, true);
        selenium.hardWait(1);
        selenium.clickOn(gunakanBtn);
        selenium.hardWait(2);
    }
}