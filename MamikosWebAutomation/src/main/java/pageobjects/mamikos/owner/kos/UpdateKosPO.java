package pageobjects.mamikos.owner.kos;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

public class UpdateKosPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public UpdateKosPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".room-price-collapse-wrapper__toggle-button")
    private WebElement seeOtherPriceButton;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[2]//input[@class='input property-room__price-item-input-currency satu']")
    private WebElement dailyPriceTextBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[3]//input[@class='input property-room__price-item-input-currency satu']")
    private WebElement weeklyPriceTextBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[1]//input[@class='input property-room__price-item-input-currency satu']")
    private WebElement monthlyPriceTextBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[4]//input[@class='input property-room__price-item-input-currency satu']")
    private WebElement threeMonthlyPriceTextBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[5]//input[@class='input property-room__price-item-input-currency satu']")
    private WebElement sixMonthlyPriceTextBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[6]//input[@class='input property-room__price-item-input-currency satu']")
    private WebElement yearlyPriceTextBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[2]//span[@class='check is-primary']")
    private WebElement dailyPriceCheckBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[3]//span[@class='check is-primary']")
    private WebElement weeklyPriceCheckBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[1]//span[@class='check is-primary']")
    private WebElement monthlyPriceCheckBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[4]//span[@class='check is-primary']")
    private WebElement threeMonthlyPriceCheckBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[5]//span[@class='check is-primary']")
    private WebElement sixMonthlyPriceCheckBox;

    @FindBy(xpath = "//div[@class='room-price-collapse-wrapper__container']/div[6]//span[@class='check is-primary']")
    private WebElement yearlyPriceCheckBox;

    @FindBy(css = ".c-mk-button--green-brand")
    private WebElement updatePriceButton;

    @FindBy(xpath = "//p[contains(.,'Atur Harga')]")
    private WebElement setPriceButton;

    @FindBy(css = "div.room-price-collapse-wrapper__container > div:nth-of-type(2) .media-content")
    private WebElement warningDailyPrice;

    @FindBy(css = "div.room-price-collapse-wrapper__container > div:nth-of-type(3) .media-content")
    private WebElement warningWeeklyPrice;

    @FindBy(css = "div.room-price-collapse-wrapper__container > div:nth-of-type(1) .media-content")
    private WebElement warningMonthlyPrice;

    @FindBy(css = "div.room-price-collapse-wrapper__container > div:nth-of-type(4) .media-content")
    private WebElement warningThreeMonthlyPrice;

    @FindBy(css = "div.room-price-collapse-wrapper__container > div:nth-of-type(5) .media-content")
    private WebElement warningSixMonthlyPrice;

    @FindBy(css = "div.room-price-collapse-wrapper__container > div:nth-of-type(6) .media-content")
    private WebElement warningYearlyPrice;

    @FindBy(css = ".room-page__title")
    private WebElement textUpdateHarga;

    @FindBy(xpath = "//div[@class='room-additional-price']/div[@class='additional-price-item']//div[contains(.,'Biaya Lainnya Per Bulan')]")
    private WebElement textOtherPrice;

    @FindBy(xpath = "//div[@class='room-additional-price']/div[@class='additional-price-item']//div[contains(.,'Biaya Denda')]")
    private WebElement textFineFee;

    @FindBy(xpath = "//div[@class='room-additional-price']/div[@class='additional-price-item']//div[contains(.,'Biaya Deposit')]")
    private WebElement textDepositFee;

    @FindBy(xpath = "//div[@class='room-additional-price']/div[@class='additional-price-item']//div[contains(.,'Biaya Uang Muka (DP)')]")
    private WebElement textDPFee;

    @FindBy(xpath = "//article[contains(@class, 'flash-sale-notif') and not(@style='display: none;')]//div[@class='media-content']")
    private WebElement promoNgebutLabel;

    @FindBy(css = ".delete")
    private WebElement closeInfobarButton;


    /**
     * Enter Text in daily price text box
     * @param dailyPrice is text we want to search
     * @throws InterruptedException
     */
    public void inputDailyPriceKos(String dailyPrice) throws InterruptedException {
        if (selenium.waitInCaseElementVisible(dailyPriceTextBox, 2) == null) {
            selenium.clickOn(dailyPriceCheckBox);
        }
        selenium.hardWait(2);
        selenium.clickOn(dailyPriceTextBox);
        selenium.clearTextField(dailyPriceTextBox);
        dailyPriceTextBox.sendKeys(dailyPrice);
        selenium.hardWait(2);
    }

    /**
     * Enter Text in weekly price text box
     * @param weeklyPrice is text we want to search
     * @throws InterruptedException
     */
    public void inputWeeklyPrice(String weeklyPrice) throws InterruptedException {
        if (selenium.waitInCaseElementVisible(weeklyPriceTextBox, 2) == null) {
            selenium.clickOn(weeklyPriceCheckBox);
        }
        selenium.clickOn(weeklyPriceTextBox);
        selenium.clearTextField(weeklyPriceTextBox);
        weeklyPriceTextBox.sendKeys(weeklyPrice);
        selenium.hardWait(2);
    }


    /**
     * Enter Text in monthly price text box
     * @param monthlyPrice is text we want to search
     * @throws InterruptedException
     */
    public void inputMonthlyPrice(String monthlyPrice) throws InterruptedException {
        if (selenium.waitInCaseElementVisible(monthlyPriceTextBox, 2) == null) {
            selenium.clickOn(monthlyPriceCheckBox);
        }
        selenium.clickOn(monthlyPriceTextBox);
        selenium.clearTextField(monthlyPriceTextBox);
        monthlyPriceTextBox.sendKeys(monthlyPrice);
        selenium.hardWait(2);
    }

    /**
     * Enter Text in three monthly price text box
     * @param threeMonthlyPrice is text we want to search
     * @throws InterruptedException
     */
    public void inputThreeMonthlyPrice(String threeMonthlyPrice) throws InterruptedException {
        if (selenium.waitInCaseElementVisible(threeMonthlyPriceTextBox, 2) == null) {
            selenium.clickOn(threeMonthlyPriceCheckBox);
        }
        selenium.clickOn(threeMonthlyPriceTextBox);
        selenium.clearTextField(threeMonthlyPriceTextBox);
        threeMonthlyPriceTextBox.sendKeys(threeMonthlyPrice);
        selenium.hardWait(2);
    }

    /**
     * Enter Text in six monthly price text box
     * @param sixMonthlyPrice is text we want to search
     * @throws InterruptedException
     */
    public void inputSixMonthlyPrice(String sixMonthlyPrice) throws InterruptedException {
        if (selenium.waitInCaseElementVisible(sixMonthlyPriceTextBox, 2) == null) {
            selenium.clickOn(sixMonthlyPriceCheckBox);
        }
        selenium.clickOn(sixMonthlyPriceTextBox);
        selenium.clearTextField(sixMonthlyPriceTextBox);
        sixMonthlyPriceTextBox.sendKeys(sixMonthlyPrice);
        selenium.hardWait(2);
    }

    /**
     * Enter Text yearly price text box
     * @param yearlyPrice is text we want to search
     * @throws InterruptedException
     */
    public void inputYearlyPrice(String yearlyPrice) throws InterruptedException {
        if (selenium.waitInCaseElementVisible(yearlyPriceTextBox, 2) == null) {
            selenium.clickOn(yearlyPriceCheckBox);
        }
        selenium.clickOn(yearlyPriceTextBox);
        selenium.clearTextField(yearlyPriceTextBox);
        yearlyPriceTextBox.sendKeys(yearlyPrice);
        selenium.hardWait(2);
    }

    /**
     * Click on update button
     * @throws InterruptedException
     */
    public void clickButtonUpdate() throws InterruptedException {
        selenium.pageScrollInView(updatePriceButton);
        selenium.clickOn(updatePriceButton);
        selenium.hardWait(4);
    }

    /**
     * Get text price daily
     * @return String daily price
     */
    public String getDailyPrice() {
        selenium.pageScrollInView(dailyPriceTextBox);
        int number = JavaHelpers.extractNumber(selenium.getElementAttributeValue(dailyPriceTextBox, "value"));
        return String.valueOf(number);
    }

    /**
     * Get text price weekly
     * @return String weekly price
     */
    public String getWeeklyPrice() {
        int number = JavaHelpers.extractNumber(selenium.getElementAttributeValue(weeklyPriceTextBox, "value"));
        return String.valueOf(number);
    }

    /**
     * Get text price monthly
     * @return String monthly price
     */
    public String getMonthlyPrice() {
        int number = JavaHelpers.extractNumber(selenium.getElementAttributeValue(monthlyPriceTextBox, "value"));
        return String.valueOf(number);
    }

    /**
     * Get text price three monthly
     * @return String three monthly price
     */
    public String getThreeMonthlyPrice() {
        int number = JavaHelpers.extractNumber(selenium.getElementAttributeValue(threeMonthlyPriceTextBox, "value"));
        return String.valueOf(number);
    }

    /**
     * Get text price six monthly
     * @return String six monthly price
     */
    public String getSixMonthlyPrice() {
        int number = JavaHelpers.extractNumber(selenium.getElementAttributeValue(sixMonthlyPriceTextBox, "value"));
        return String.valueOf(number);
    }

    /**
     * Get text price yearly
     * @return String yearly price
     */
    public String getYearlyPrice() {
        int number = JavaHelpers.extractNumber(selenium.getElementAttributeValue(yearlyPriceTextBox, "value"));
        return String.valueOf(number);
    }

    /**
     * Click on see other price button
     * @throws InterruptedException
     */
    public void clickSeeOtherPrices() throws InterruptedException {
        selenium.pageScrollInView(seeOtherPriceButton);
        selenium.clickOn(seeOtherPriceButton);
        selenium.hardWait(5);
    }


    /**
     * Delete Text in name or number room
     * @throws InterruptedException
     */
    public void clickSetPriceButton() throws InterruptedException {
        selenium.pageScrollInView(setPriceButton);
        selenium.clickOn(setPriceButton);
        selenium.hardWait(5);
    }

    /**
     * Get text warning price daily
     * @return String warning daily price
     */
    public String getWarningDailyPrice() throws InterruptedException {
        selenium.pageScrollInView(warningDailyPrice);
        selenium.hardWait(2);
        return selenium.getText(warningDailyPrice);
    }

    /**
     * Check if warning price daily appear
     * @return true if appear
     */
    public boolean isWarningDailyPriceAppear() {
        return selenium.waitInCaseElementVisible(warningDailyPrice, 3) != null;
    }

    /**
     * Get text warning price weekly
     * @return String warning weekly price
     */
    public String getWarningWeeklyPrice() throws InterruptedException {
        selenium.pageScrollInView(warningWeeklyPrice);
        selenium.hardWait(2);
        return selenium.getText(warningWeeklyPrice);
    }

    /**
     * Check if warning price weekly appear
     * @return true if appear
     */
    public boolean isWarningWeeklyPriceAppear() {
        return selenium.waitInCaseElementVisible(warningWeeklyPrice, 3) != null;
    }

    /**
     * Get text warning price monthly
     * @return String warning monthly price
     */
    public String getWarningMonthlyPrice() throws InterruptedException {
        selenium.pageScrollInView(warningMonthlyPrice);
        selenium.hardWait(2);
        return selenium.getText(warningMonthlyPrice);
    }

    /**
     * Check if warning price monthly appear
     * @return true if appear
     */
    public boolean isWarningMonthlyPriceAppear() {
        return selenium.waitInCaseElementVisible(warningMonthlyPrice, 3) != null;
    }

    /**
     * Get text warning price three monthly
     * @return String warning three monthly price
     */
    public String getWarningThreeMonthlyPrice() throws InterruptedException {
        selenium.pageScrollInView(warningThreeMonthlyPrice);
        selenium.hardWait(2);
        return selenium.getText(warningThreeMonthlyPrice);
    }

    /**
     * Check if warning price three monthly appear
     * @return true if appear
     */
    public boolean isWarningThreeMonthlyPriceAppear() {
        return selenium.waitInCaseElementVisible(warningThreeMonthlyPrice, 3) != null;
    }

    /**
     * Get text warning price six monthly
     * @return String warning six monthly price
     */
    public String getWarningSixMonthlyPrice() throws InterruptedException {
        selenium.pageScrollInView(warningSixMonthlyPrice);
        selenium.hardWait(2);
        return selenium.getText(warningSixMonthlyPrice);
    }

    /**
     * Check if warning price six monthly appear
     * @return true if appear
     */
    public boolean isWarningSixMonthlyPriceAppear() {
        return selenium.waitInCaseElementVisible(warningSixMonthlyPrice, 3) != null;
    }

    /**
     * Get text warning price yearly price
     * @return String warning yearly price
     */
    public String getWarningYearlyPrice() throws InterruptedException {
        selenium.pageScrollInView(warningYearlyPrice);
        selenium.hardWait(2);
        return selenium.getText(warningYearlyPrice);
    }

    /**
     * Check if warning price yearly appear
     * @return true if appear
     */
    public boolean isWarningYearlyPriceAppear() {
        return selenium.waitInCaseElementVisible(warningYearlyPrice, 3) != null;
    }

    /**
     * Check if button update price is disable
     * @return true if disable
     */
    public boolean isButtonUpdatePriceDisable() throws InterruptedException {
        selenium.pageScrollInView(updatePriceButton);
        selenium.hardWait(2);
        return updatePriceButton.isDisplayed();
    }

    /**
     * Get text update harga
     * @return String update harga
     */
    public String getTextUpdateHarga() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(textUpdateHarga);
    }

    /**
     * Check if textbox monthly price is displayed
     * @return true if displayed
     */
    public boolean isTextboxMonthlyPriceChecked() {
        return monthlyPriceTextBox.isDisplayed();
    }

    /**
     * Check if textlink other prices is displayed
     * @return true if displayed
     */
    public boolean isTextlinkOtherPricesChecked() {
        return seeOtherPriceButton.isDisplayed();
    }

    /**
     * Check if textbox daily price is displayed
     * @return true if displayed
     */
    public boolean isTextboxDailyPriceChecked() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(dailyPriceTextBox, 2) == null) {
            selenium.clickOn(dailyPriceCheckBox);
        }
        selenium.hardWait(1);
        return dailyPriceTextBox.isDisplayed();
    }

    /**
     * Check if textbox weekly price is displayed
     * @return true if displayed
     */
    public boolean isTextboxWeeklyPriceChecked() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(weeklyPriceTextBox, 2) == null) {
            selenium.clickOn(weeklyPriceCheckBox);
        }
        selenium.hardWait(1);
        return weeklyPriceTextBox.isDisplayed();
    }

    /**
     * Check if textbox three monthly price is displayed
     * @return true if displayed
     */
    public boolean isTextboxThreeMonthlyPriceChecked() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(threeMonthlyPriceTextBox, 2) == null) {
            selenium.clickOn(threeMonthlyPriceCheckBox);
        }
        selenium.hardWait(1);
        return threeMonthlyPriceTextBox.isDisplayed();
    }

    /**
     * Check if textbox six monthly price is displayed
     * @return true if displayed
     */
    public boolean isTextboxSixMonthlyPriceChecked() throws InterruptedException {
        selenium.pageScrollInView(sixMonthlyPriceCheckBox);
        selenium.hardWait(3);
        if (selenium.waitInCaseElementVisible(sixMonthlyPriceTextBox, 2) == null) {
            selenium.clickOn(sixMonthlyPriceCheckBox);
        }
        selenium.hardWait(1);
        return sixMonthlyPriceTextBox.isDisplayed();
    }

    /**
     * Check if textbox yearly price is displayed
     * @return true if displayed
     */
    public boolean isTextboxYearlyPriceChecked() throws InterruptedException {
        selenium.pageScrollInView(yearlyPriceCheckBox);
        selenium.hardWait(3);
        if (selenium.waitInCaseElementVisible(yearlyPriceTextBox, 2) == null) {
            selenium.clickOn(yearlyPriceCheckBox);
        }
        selenium.hardWait(1);
        return yearlyPriceTextBox.isDisplayed();
    }

    /**
     * Check if textbox other price is displayed
     * @return true if displayed
     */
    public boolean isTextOtherPriceChecked() throws InterruptedException {
        selenium.pageScrollInView(textOtherPrice);
        selenium.hardWait(2);
        return textOtherPrice.isDisplayed();
    }

    /**
     * Check if textbox fine fee is displayed
     * @return true if displayed
     */
    public boolean isTextFineFeeChecked() throws InterruptedException {
        selenium.pageScrollInView(textFineFee);
        selenium.hardWait(2);
        return textFineFee.isDisplayed();
    }

    /**
     * Check if textbox deposit fee is displayed
     * @return true if displayed
     */
    public boolean isTextDepositFeeChecked() throws InterruptedException {
        selenium.pageScrollInView(textDepositFee);
        selenium.hardWait(2);
        return textDepositFee.isDisplayed();
    }

    /**
     * Check if textbox DP fee is displayed
     * @return true if displayed
     */
    public boolean isTextDPFeeChecked() throws InterruptedException {
        selenium.pageScrollInView(textDPFee);
        selenium.hardWait(2);
        return textDPFee.isDisplayed();
    }

    /**
     * Click on kos name in update price
     * @throws InterruptedException
     * @param kosName is kos name
     */
    public void clickKosName(String kosName) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//span[.='"+ kosName +"']"));
        selenium.clickOn(element);
        selenium.waitInCaseElementVisible(updatePriceButton, 11);
    }

    /**
     * Search room name in update room
     * @throws InterruptedException
     * @param search is kos name
     */
    public void clickSearchInUpdateRoom(String search) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//input[@placeholder='Masukkan nama atau nomor kamar']"));
        selenium.clickOn(element);
        selenium.fillFieldAfterDoubleClick(element,search);
        driver.findElement(By.xpath("//input[@placeholder='Masukkan nama atau nomor kamar']")).sendKeys(Keys.ENTER);
    }

    /**
     * Get text in promo ngebut infobar
     * @return String promo ngebut info
     */
    public String getPromoNgebutInfo() {
        return selenium.getText(promoNgebutLabel);
    }

    /**
     * Click monthly price checkbox
     * @throws InterruptedException
     */
    public void clickMonthlyPriceCheckbox() throws InterruptedException {
        selenium.clickOn(monthlyPriceCheckBox);
    }

    /**
     * Click yearly price checkbox
     * @throws InterruptedException
     */
    public void clickYearlyPriceCheckbox() throws InterruptedException {
        selenium.clickOn(yearlyPriceCheckBox);
    }

    /**
     * Verify if monthly price field is enable
     * @return true if enable
     */
    public boolean isMonthlyPriceFieldEnable() {
        return selenium.waitInCaseElementVisible(monthlyPriceTextBox, 3) != null;
    }

    /**
     * Verify if yearly price field is enable
     * @return true if enable
     */
    public boolean isYearlyPriceFieldEnable() {
        return selenium.waitInCaseElementVisible(yearlyPriceTextBox, 3) != null;
    }

    /**
     * Click close infobar button
     * @throws InterruptedException
     */
    public void clickCloseInfobar() throws InterruptedException {
        selenium.clickOn(closeInfobarButton);
    }
}

