package pageobjects.mamikos.owner.kos;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.time.Duration;
import java.util.LinkedList;
import java.util.List;

public class UpdateRoomPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public UpdateRoomPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//div[@class='detail-kos__data-desc']/div[contains(.,'Update Kamar')]")
    private WebElement updateKamarButton;

    @FindBy(css = ".toast.is-dark.is-bottom")
    private WebElement toastElementText;

    @FindBy(css = "input + .check.is-green-brand")
    private WebElement cboxRenterOccupied;

    @FindBy(css = "[placeholder='Masukkan nama atau nomor kamar']")
    private WebElement searchInput;

    @FindBy(xpath = "//table[@class='table is-striped']")
    private WebElement roomNameTable;

    @FindBy(css = ".is-empty")
    private WebElement emptyTable;

    @FindBy(xpath = "//td[6]//span[@class='icon']")
    private WebElement firstEditButton;

    @FindBy(xpath = "//div[@class='control is-clearfix']/input[@class='input']")
    private WebElement floorFieldInput;

    @FindBy(xpath = "//label[contains(text(), 'Nama Kamar')]/parent::div/following::*[1]//div[@class='media-content']")
    private WebElement errorMessageRoomName;

    @FindBy(xpath = "//label[contains(text(), 'Lantai')]/parent::div/following::*[1]//div[@class='media-content']")
    private WebElement errorMessageFloor;

    @FindBy(xpath = "//div[@class='control modal-input__with-room-label is-clearfix short']/input[@class='input' and @disabled='disabled']")
    private WebElement roomNameFieldInputDisabled;

    @FindBy(xpath = "//div[@class='control is-clearfix']/input[@class='input' and @disabled='disabled']")
    private WebElement floorFieldInputDisabled;

    @FindBy(css = ".add-modal__action:disabled")
    private WebElement updateRoomButtonDisabled;

    @FindBy(css = ".add-modal__action")
    private WebElement updateRoomButton;

    @FindBy(css = ".add-modal__close")
    private WebElement updateRoomCloseButton;

    @FindBy(css = ".is-active .modal-add-tenant-intercept__body-title")
    private WebElement txtAddRenterPopUpTitle;

    @FindBy(css = ".is-active .modal-add-tenant-intercept__body-description")
    private WebElement txtAddRenterPopUpDescription;

    @FindBy(css = ".is-active button")
    private WebElement btnActivePopup;

    @FindBy(css = ".modal-input__with-room-label > .input")
    private WebElement roomNameInput;

    @FindBy(css = ".room-table__total-room-label")
    private WebElement totalRoomLabel;

    @FindBy(xpath = "//span[@class='room-table__total-room-label']")
    private WebElement textTotalRoom;

    @FindBy(css = ".check")
    private WebElement alreadyInhabitedCheckbox;

    @FindBy(css = ".availability-option__button")
    private WebElement roomFilterDropdown;

    @FindBy(css = ".availability-option__button > span:nth-of-type(1)")
    private WebElement roomFilterLabel;

    @FindBy(css = ".c-mk-button")
    private WebElement addRoomButton;

    @FindBy(xpath = "//button[@class='c-mk-button column delete-modal__action c-mk-button--green-brand']")
    private WebElement deleteButtonInPopUp;

    @FindBy(css = ".c-mk-button--green-inverse")
    private WebElement cancelButtonInPopUp;

    @FindBy(css = "tbody > tr:nth-of-type(1) > td:nth-of-type(3)")
    private WebElement firstRoomFloorLabel;

    @FindBy(css = "tbody > tr:nth-of-type(1) > td:nth-of-type(2)")
    private WebElement firstRoomNameLabel;

    @FindBy(css = ".add-modal__action")
    private WebElement tambahKmrBtn;

    @FindBy(xpath = "//div[contains(text(),'Update Kamar')]")
    private WebElement updateListKamarButton;

    @FindBy(xpath = "//div[@class='c-mk-card__body']/button[contains(.,'Update Kamar')]")
    private WebElement updateRoomBtn;

    @FindBy(xpath = "//div[@class='c-mk-card__body']//button[contains(.,'Update Kamar')]")
    private WebElement updateButton;

    @FindBy(xpath = "//tbody[1]/tr[1]/td[contains(.,'Kosong')]")
    private WebElement elementTextNull;

    @FindBy(xpath = "//td[contains(.,'Terisi')]")
    private WebElement elementTextFilled;

    @FindBy(xpath = "//div[@class='detail-kos__data-desc']/div[contains(.,'Update Kamar')]")
    private WebElement updateRoom;

    /**
     * Click on edit button on selected room list number
     *
     * @param listNumber input with number e.g 1
     * @throws InterruptedException
     */
    public void clickOnEditButtonRoomList(String listNumber) throws InterruptedException {
        String editButtonEl = "tr:nth-child(" + listNumber + ") .mdi.mdi-square-edit-outline";
        selenium.javascriptClickOn(driver.findElement(By.cssSelector(editButtonEl)));
    }

    /**
     * check if update room button is visible
     *
     * @return visible true, otherwise false
     */
    private boolean isUpdateRoomButtonVisible() {
        return selenium.waitInCaseElementVisible(updateKamarButton, 1) != null;
    }

    /**
     * Click on update room button
     */
    public void clickOnUpdateRoomButton() throws InterruptedException {
        int i = 0;
        selenium.clickOn(updateRoomButton);
        while (isUpdateRoomButtonVisible()) {
            selenium.hardWait(2);
            if (i == 20) {
                break;
            }
            i++;
        }
        selenium.hardWait(1);
    }

    /**
     * check if update room still visible
     *
     * @return true if visible otherwise false
     */
    private boolean isUpdateRoomPopUpVisible() {
        try {
            selenium.waitTillElementIsVisible(updateKamarButton, 1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Wait until update room button is disappear
     */
    public void waitTillUpdateRoomButtonDisappear() throws InterruptedException {
        int i = 1;
        while (isUpdateRoomPopUpVisible()) {
            selenium.hardWait(1);
            if (!isUpdateRoomPopUpVisible()) {
                break;
            } else if (i == 10) {
                break;
            }
            i++;
        }
    }

    /**
     * Get text null
     *
     * @return return toast text e.g Anda berhasil update kamar
     */
    public String getNullText() {
        selenium.waitInCaseElementVisible(elementTextNull, 10);
        return selenium.getText(elementTextNull);
    }

    /**
     * Get text fill
     */
    public String getFilledText() {
        selenium.waitInCaseElementVisible(elementTextFilled, 10);
        return selenium.getText(elementTextFilled);
    }

    /**
     * Get toast message
     *
     * @return return toast text e.g Anda berhasil update kamar
     */
    public boolean getText() {
        return selenium.waitInCaseElementVisible(elementTextNull, 5) != null;
    }


    /**
     * Check is list present
     *
     * @param listNumber input with number e.g 1
     * @return true if list present, otherwise false
     */
    private boolean isListPresent(String listNumber) {
        String editButtonEl = "tr:nth-child(" + listNumber + ") .mdi.mdi-square-edit-outline";
        try {
            selenium.waitInCaseElementVisible(driver.findElement(By.cssSelector(editButtonEl)), 2);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Wait till loading disappear by checking room availability
     *
     * @throws InterruptedException
     */
    public void waitTillLoadingDisappear() throws InterruptedException {
        int i = 0;
        while (!isListPresent("1")) {
            selenium.hardWait(2);
            if (isListPresent("1") || i == 10) {
                break;
            }
            i++;
        }
    }

    /**
     * Check if a room is in filled condition
     *
     * @param roomNumber room number 1, 2, 3, 4
     * @return filled room true, otherwise false
     * @throws InterruptedException
     */
    public boolean isRoomIsAvailable(String roomNumber) throws InterruptedException {
        String roomEl = "tbody tr:nth-child(" + roomNumber + ") td:nth-child(4)";
        return selenium.isElementPresent(By.cssSelector(roomEl));
    }

    /**
     * Get text of room status
     *
     * @param roomNumber room number
     * @return string data type e.g Terisi
     */
    public String getRoomStatusText(String roomNumber) {
        String roomEl = "tbody tr:nth-child(" + roomNumber + ") td:nth-child(4)";
        return selenium.getText(driver.findElement(By.cssSelector(roomEl)));
    }

    /**
     * Tik untik on checkbox renter occupied
     */
    public void clickOnCheckBoxRenter() throws InterruptedException {
        selenium.hardWait(10);
        selenium.clickOn(cboxRenterOccupied);
    }

    /**
     * Insert text to search bar in room allotment and hit enter
     *
     * @param text is text we want to insert
     */
    public void searchNameOrRoomNo(String text) {
        selenium.enterText(searchInput, text, true);
        searchInput.sendKeys(Keys.ENTER);
    }

    /**
     * Get all room names/numbers in table
     *
     * @return list of room names/number
     */
    public List<String> getAllRoomNameInTable() {
        List<String> roomNames = new LinkedList<>();
        List<WebElement> tableRows = new WebDriverWait(driver, Duration.ofSeconds(15)).until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(roomNameTable, By.tagName("tr")));
        for (int i = 1; i <= tableRows.size(); i++) {
            roomNames.add(selenium.getText(driver.findElement(By.xpath("//tbody/tr[ '" + i + "']/td[2]"))));
        }
        return roomNames;
    }

    /**
     * Verify if table is empty
     *
     * @return true if empty
     */
    public boolean isTableEmpty() {
        return selenium.waitInCaseElementVisible(emptyTable, 3) != null;
    }

    /**
     * Click on first Edit Button in room name/number table
     *
     * @throws InterruptedException
     */
    public void clickFirstEditButton() throws InterruptedException {
        selenium.clickOn(firstEditButton);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Fill floor field with text
     *
     * @param floor is text for floor
     */
    public void insertTextFloor(String floor) {
        selenium.enterText(floorFieldInput, floor, true);
    }

    /**
     * Get error message below room name field
     *
     * @return error message below room name field
     */
    public String getErrorRoomName() {
        return selenium.getText(errorMessageRoomName);
    }

    /**
     * Get error message below floor field
     *
     * @return error message below floor field
     */
    public String getErrorFloor() {
        return selenium.getText(errorMessageFloor);
    }

    /**
     * Verify if room name field is disabled
     *
     * @return true if disabled
     */
    public boolean isRoomNameFieldDisabled() {
        return selenium.waitInCaseElementVisible(roomNameFieldInputDisabled, 3) != null;
    }

    /**
     * Verify if floor field is disabled
     *
     * @return true if disabled
     */
    public boolean isFloorFieldDisabled() {
        return selenium.waitInCaseElementVisible(floorFieldInputDisabled, 3) != null;
    }

    /**
     * Verify if update room button is disabled
     *
     * @return true if disabled
     */
    public boolean isUpdateRoomDisabled() {
        return selenium.waitInCaseElementVisible(updateRoomButtonDisabled, 3) != null;
    }

    /**
     * Click on close button in update room pop up
     *
     * @throws InterruptedException
     */
    public void closeUpdateRoomPopUp() throws InterruptedException {
        selenium.clickOn(updateRoomCloseButton);
    }

    /**
     * Click on first empty room on loop max loop is 10 times
     *
     * @param roomList input with room index 1-10;
     * @throws InterruptedException
     */
    public void clickOnUpdateRoomButtonOnFirstFoundEmptyRoom(int roomList, String roomStatus) throws InterruptedException {
        selenium.hardWait(5);
        String roomNumber = String.valueOf(roomList);
        while (isRoomIsAvailable(roomNumber)) {
            if (getRoomStatusText(roomNumber).equalsIgnoreCase(roomStatus)) {
                System.out.println(getRoomStatusText(roomNumber));
                clickOnEditButtonRoomList(roomNumber);
                break;
            } else if (roomList == 10) {
                break;
            }
            roomList++;
        }
    }

    /**
     * Get pop up title of add renter
     *
     * @return String data type e.g "Anda belum tambah data penyewa"
     */
    public String getPopupAddRenterTitle() {
        return selenium.getText(txtAddRenterPopUpTitle);
    }

    /**
     * Get pop up description of add renter
     *
     * @return String data type "Sebelum menandai kamar..."
     */
    public String getPopupAddRenterDescription() {
        return selenium.getText(txtAddRenterPopUpDescription);
    }

    /**
     * Get active button text
     *
     * @return string data type e.g "Tambah Penyewa"
     */
    public String getAddRenterText() {
        return selenium.getText(btnActivePopup);
    }

    /**
     * click on btn active pop up. On update room must be trigger from GP kost.
     *
     * @throws InterruptedException
     */
    public void clickOnAddRenterButton() throws InterruptedException {
        selenium.click(btnActivePopup);
        selenium.hardWait(5);
    }

    /**
     * Fill room name field with text
     *
     * @param room is text for room name/number
     */
    public void insertTextRoomName(String room) {
        selenium.enterText(roomNameInput, room, true);
    }

    /**
     * Click on kos name in update room
     *
     * @param kosName is kos name
     * @throws InterruptedException
     */
    public void clickKosName(String kosName) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//span[.='" + kosName + "']"));
        selenium.clickOn(element);
        selenium.waitInCaseElementVisible(firstEditButton, 11);
    }

    /**
     * Get goldplus label beside room Name/number
     *
     * @param roomNo is room name/number
     * @return text goldplus
     */
    public String getGoldPlusLabel(String roomNo) {
        WebElement element = driver.findElement(By.xpath("//td[normalize-space()='" + roomNo + "']"));
        return selenium.getText(element);
    }

    /**
     * Get number of total room
     *
     * @return total room number in String
     */
    public String getTotalRoom() {
        selenium.waitTillElementIsNotPresent(cancelButtonInPopUp, 5);
        selenium.waitInCaseElementVisible(firstEditButton, 5);
        String totalRoomText = selenium.getText(totalRoomLabel);
        return String.valueOf(JavaHelpers.extractNumber(totalRoomText));
    }

    /**
     * Get Text total room
     */
    public boolean isTotalRoom() {
        return selenium.waitInCaseElementVisible(totalRoomLabel, 5) != null;
    }

    /**
     * Get number of total room
     */
    public String getTextTotalRoom() throws InterruptedException {
        return selenium.getText(textTotalRoom);
    }

    /**
     * Click on already inhabited checkbox
     *
     * @throws InterruptedException
     */
    public void clickAlreadyInhabitedCheckbox() throws InterruptedException {
        selenium.clickOn(alreadyInhabitedCheckbox);
    }

    /**
     * Click on update room button in pop up
     *
     * @throws InterruptedException
     */
    public void clickUpdateRoomButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(updateRoomButton);
        selenium.hardWait(2);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Filter room table with selected text in param
     *
     * @param filter is room filter text
     * @throws InterruptedException
     */
    public void filterRoomTable(String filter) throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(roomFilterDropdown);
        selenium.hardWait(2);
        WebElement element = driver.findElement(By.xpath("//a[contains(.,'" + filter + "')]"));
        selenium.waitInCaseElementVisible(element, 3);
        selenium.clickOn(element);
    }

    /**
     * Click on add room button in room allotment
     *
     * @throws InterruptedException
     */
    public void clickAddRoomButton() throws InterruptedException {
        selenium.clickOn(addRoomButton);
    }

    /**
     * Click delete beside room Name/number
     *
     * @param roomNo is room name/number
     * @throws InterruptedException
     */
    public void deleteRoom(String roomNo) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//tbody[1]//td[2][normalize-space(text())='" + roomNo + "']/following-sibling::td[4]/div/div"));
        selenium.clickOn(element);
        selenium.waitInCaseElementVisible(deleteButtonInPopUp, 3);
        selenium.clickOn(deleteButtonInPopUp);
        selenium.hardWait(3);
    }

    /**
     * Get room filter
     *
     * @return String room filter that applied
     */
    public String getRoomFilter() {
        return selenium.getText(roomFilterLabel);
    }

    /**
     * Get floor name/number from field
     *
     * @return floor name/number text
     */
    public String getFirstFloorNumber() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(firstRoomFloorLabel);
    }


    /**
     * Get room name/number from field
     * @return room name/number text
     */
    public String getFirstRoomName() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(firstRoomNameLabel);
    }

    /**
     * is room number
     */
    public boolean isFirstRoomName() {
        return selenium.waitInCaseElementVisible(firstRoomNameLabel, 5) != null;
    }
    /**
     * Tick on checkbox renter
     * @throws InterruptedException
     */
    public void tickOnCheckBoxRenter() throws InterruptedException {
        selenium.hardWait(10);
        selenium.clickOn(cboxRenterOccupied);
    }

    /**
     * click on Tambah kamar
     * @throws InterruptedException
     */
    public void clickTambahKmr() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(tambahKmrBtn);
    }

    /**
     * check untick condition
     * @return untick room
     */
    public String getUntickRoom() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(cboxRenterOccupied);
    }

    /**
     * click on Update List Kamar Button
     * @throws InterruptedException
     */
    public void clickOnUpdateListKamarButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(updateListKamarButton);
    }

    /**
     * click on Update Room Button on Kos List
     * @throws InterruptedException
     */
    public void clickUpdateRoomBtn() throws InterruptedException {
        selenium.javascriptClickOn(updateRoomBtn);
        selenium.hardWait(3);
    }

    /**
     * click on Update Room Button on popup
     * @throws InterruptedException
     */
    public void clickUpdateBtn() throws InterruptedException {
        selenium.javascriptClickOn(updateRoom);
        selenium.hardWait(3);
    }
}

