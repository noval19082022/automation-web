package pageobjects.mamikos.owner.kos;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.ArrayList;
import java.util.List;

public class AddKosPO {
    private final By loadingDeleteKos = By.cssSelector(".swal2-modal.swal2-show.swal2-loading");
    private final By loadingKosList = By.cssSelector(".owner-tips-loading");
    private final By kosRulePhotos = By.xpath("//div[@class='image-container']");

    /*
     * All WebElements are identified by @FindBy annotation
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * xpath as attributes.
     */
    private final By seeKosRulePhoto = By.xpath("//div[@class='image-uploader']/div[1]//div[@class='preview__menu']/div[contains(.,'Lihat Foto')]");
    private final By changeKosRulePhoto = By.xpath("//div[@class='image-uploader']/div[1]//div[@class='preview__menu']/div[contains(.,'Ubah Foto')]");
    private final By deleteKosRulePhoto = By.xpath("(//div[@class='preview__menu-item'][contains(text(),'Hapus')])[1]");
    WebDriver driver;
    SeleniumHelpers selenium;
    @FindBy(xpath = "(//div[@class='preview__menu-item'][contains(text(),'Hapus')])[1]")
    private WebElement deletePhoto;
    @FindBy(css = "[for='addListingKos']")
    private WebElement addKosRadioButton;
    @FindBy(xpath = "//button[contains(text(), 'Tambahkan Data')]")
    private WebElement addButton;
    @FindBy(xpath = "//input[@class='input target-autocomplete']")
    private WebElement locationTextBox;
    @FindBy(xpath = "//div[@class='pac-container mamikos-provider target-autocomplete']/div[1]")
    private WebElement locationAutoComplete;
    @FindBy(xpath = "//button[@class='btn btn-kost  btn-mamiorange'][@disabled='disabled']")
    private WebElement disabledNextButtonLocationData;
    @FindBy(css = ".bg-c-input__field")
    private WebElement kosNameTextBox;
    @FindBy(xpath = "//div[@class='content']/div[1]//div[@class='bg-c-field bg-c-field--error']/div[2]")
    private WebElement kosNameIsInvalidValueLabel;
    @FindBy(css = "[alt='type-kost-girl']")
    private WebElement kostTypeIconGirl;
    @FindBy(css = "[alt='type-kost-boy']")
    private WebElement kostTypeIconBoy;
    @FindBy(css = "[alt='type-kost-mix']")
    private WebElement kostTypeIconMix;
    @FindBy(xpath = "//img[@alt='User Photo']")
    private WebElement userPhotoButton;
    @FindBy(xpath = "//a[contains(.,'Halaman Pemilik')]")
    private WebElement ownerPageButton;
    @FindBy(xpath = "//p[.='Properti Saya']")
    private WebElement leftMenuPropertiSaya;
    @FindBy(xpath = "//p[.='Manajemen Kos']")
    private WebElement managementKosMenuButton;
    @FindBy(xpath = "//p[.='Kos']")
    private WebElement leftMenuKos;
    @FindBy(xpath = "//a[contains(.,'Edit Data Kos')]")
    private WebElement buttonEditDataKost;
    @FindBy(xpath = "//input[@id='propertyAddress']")
    private WebElement fieldAddressComplete;
    @FindBy(xpath = "//p[@class='text-danger']")
    private WebElement textValidationAddress;
    @FindBy(xpath = "//div[@class='property-sidebar__steps steps']/div[1]/span[@class='steps__edit-item']")
    private WebElement editDataKosButton;
    @FindBy(xpath = "//div[@class='property-sidebar__steps steps']/div[2]/span[@class='steps__edit-item']")
    private WebElement editKosAddressButton;
    @FindBy(xpath = "//div[@class='property-sidebar__steps steps']/div[3]/span[@class='steps__edit-item']")
    private WebElement editKosPhotoButton;
    @FindBy(xpath = "//div[@class='property-sidebar__steps steps']/div[4]/span[@class='steps__edit-item']")
    private WebElement editRoomPhotoButton;
    @FindBy(xpath = "//div[@class='property-sidebar__steps steps']/div[5]/span[@class='steps__edit-item']")
    private WebElement editFacilitiesButton;
    @FindBy(xpath = "//div[@class='property-sidebar__steps steps']/div[6]/span[@class='steps__edit-item']")
    private WebElement editRoomAvailabilityButton;
    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg' and @disabled='disabled']")
    private WebElement editFinishedButtonDisabled;
    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    private WebElement editFinishedButton;
    @FindBy(xpath = "//*[@id=\"__layout\"]/div/div[2]/div/div/div[2]/div/div")
    private WebElement clickUploadKosRulesPhoto;
    @FindBy(xpath = "//label[@class='step-one__input-label step-one__input-label--has-checkbox']//span[@class='bg-c-checkbox__icon']")
    private WebElement checkTypeRoom;
    @FindBy(id = "hasTypeName")
    private WebElement roomTypeCheckbox;
    @FindBy(css = ".bg-c-button--primary")
    private WebElement nextButtonCreateKostDisable;
    @FindBy(css = ".bg-c-input__field")
    private WebElement fieldInputKostName;
    @FindBy(css = ".content div:nth-of-type(2) .bg-c-input__field")
    private WebElement fieldInputRoomType;
    @FindBy(css = "label[for='hasDataOwner'] span[class='bg-c-checkbox__icon']")
    private WebElement checkBoxAdminKos;
    @FindBy(css = "label[class='step-one__input-label step-one__input-label--has-checkbox small'] div[class='bg-c-checkbox']")
    private WebElement uncheckBoxAdminKos;
    @FindBy(xpath = "//div[8]//input[@class='bg-c-input__field']")
    private WebElement fieldInputAdminKost;
    @FindBy(xpath = "//div[9]//input[@class='bg-c-input__field']")
    private WebElement fieldInputAdminPhone;
    @FindBy(css = ".step-zero__add-new")
    private WebElement addNewKosButton;
    @FindBy(xpath = "//div[@class='content']//div[@class='bg-c-field']//textarea[contains(@class, 'bg-c-textarea__field bg-c-textarea__field--lg')]")
    private WebElement kosDescriptionField;
    @FindBy(css = ".bg-c-dropdown__trigger")
    private WebElement yearBuiltDropdown;
    @FindBy(css = "select")
    private WebElement yearBuiltDropdownBar;
    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    private WebElement nextButton;
    @FindBy(xpath = "//*[contains(text(), 'Lanjutkan') and not(@disabled)]")
    private WebElement lanjutkanButton;
    @FindBy(xpath = "//input[@class='is-hidden']")
    private WebElement uploadKosRulesPhoto;
    @FindBy(xpath = "//div[@class='step-three']/div[2]//input[@class='is-hidden']")
    private WebElement uploadFrontBuildingPhoto;
    @FindBy(xpath = "//div[@class='step-three']/div[3]//input[@class='is-hidden']")
    private WebElement uploadInsideBuildingPhoto;
    @FindBy(xpath = "//div[@class='step-three']/div[4]//input[@class='is-hidden']")
    private WebElement uploadStreetViewPhoto;
    @FindBy(xpath = "//div[@class='step-four']/div[2]//input[@class='is-hidden']")
    private WebElement uploadFrontRoomPhoto;
    @FindBy(xpath = "//div[@class='step-four']/div[3]//input[@class='is-hidden']")
    private WebElement uploadInsideRoomPhoto;
    @FindBy(xpath = "//div[@class='step-four']/div[4]//input[@class='is-hidden']")
    private WebElement uploadBathroomPhoto;
    @FindBy(xpath = "//div[@class='step-four']/div[5]//input[@class='is-hidden']")
    private WebElement uploadAdditionalPhoto;
    @FindBy(id = "anotherRentPrice")
    private WebElement otherMonthlyPriceCheckbox;
    @FindBy(id = "minimumDurationDaily")
    private WebElement dailyPriceCheckbox;
    @FindBy(id = "minimumDurationWeekly")
    private WebElement weeklyPriceCheckbox;
    @FindBy(id = "minimumDurationQuarterly")
    private WebElement threeMonthlyPriceCheckbox;
    @FindBy(id = "minimumDurationSemiannually")
    private WebElement sixMonthlyPriceCheckbox;
    @FindBy(id = "minimumDurationAnnually")
    private WebElement yearlyPriceCheckbox;
    @FindBy(id = "additionalCost")
    private WebElement additionalCostCheckbox;
    @FindBy(id = "deposito")
    private WebElement depositCheckbox;
    @FindBy(xpath = "//label[@for='lateCharge']//span[@class='bg-c-checkbox__icon']")
    private WebElement fineCheckbox;
    @FindBy(css = ".step-seven__content > .step-seven__field > div:nth-of-type(1) > .bg-c-field > .input")
    private WebElement dailyPriceInput;
    @FindBy(xpath = "//div[@class='step-seven__content']/div[1]/div[1]/input[@class='input step-seven__input']")
    private WebElement monthlyPriceInput;
    @FindBy(xpath = "//div[@class='step-seven__content']/div[@class='step-seven__field']/div[2]//input[@class='input step-seven__input']")
    private WebElement weeklyPriceInput;
    @FindBy(xpath = "//div[@class='step-seven__content']/div[@class='step-seven__field']/div[3]//input[@class='input step-seven__input']")
    private WebElement threeMonthlyPriceInput;
    @FindBy(xpath = "//div[@class='step-seven__content']/div[@class='step-seven__field']/div[4]//input[@class='input step-seven__input']")
    private WebElement sixMonthlyPriceInput;
    @FindBy(xpath = "//div[@class='step-seven__content']/div[@class='step-seven__field']/div[5]//input[@class='input step-seven__input']")
    private WebElement yearlyPriceInput;
    @FindBy(css = ".step-seven .step-seven__field.bg-c-field .input")
    private WebElement depositInput;
    @FindBy(css = ".step-seven__field .bg-c-field .input")
    private WebElement fineInput;
    @FindBy(xpath = "//button[@class='btn btn-mamigreen-inverse']")
    private WebElement deleteKosButton;
    @FindBy(css = ".swal2-confirm")
    private WebElement confirmDelete;
    @FindBy(css = ".bg-c-button--md")
    private WebElement popUpNextButton;
    @FindBy(css = "[placeholder='Jumlah kamar yang kosong']")
    private WebElement availableRoomField;
    @FindBy(id = "minimumDurationRent")
    private WebElement minDurationRentCheckbox;
    @FindBy(css = ".bg-c-dropdown__trigger")
    private WebElement minDurationRentDropdown;
    @FindBy(css = ".bg-c-input__field")
    private WebElement additionalCostNameField;
    @FindBy(xpath = "//input[@class='input additional-cost__input']")
    private WebElement totalAdditionalCostField;
    @FindBy(id = "downPayment")
    private WebElement downPaymentCheckbox;
    @FindBy(xpath = "//div[@class='step-seven__content']/div[@class='step-seven__field']/div[@class='step-seven__field']//div[@class='bg-c-dropdown__trigger']")
    private WebElement downPaymentDropdown;
    @FindBy(xpath = "//div[@class='step-seven__late-charge-wrapper']//div[1]//div[1]//div[1]//div[1]")
    private WebElement timeLimitNumberDropdown;
    @FindBy(xpath = "//div[@class='step-seven__late-charge-wrapper']/div[2]//div[@class='bg-c-dropdown__trigger']")
    private WebElement timeLimitDropdown;
    @FindBy(xpath = "//button[normalize-space()='Selesai']")
    private WebElement doneSuccessButton;
    @FindBy(css = ".bg-c-button--tertiary")
    private WebElement skipAskMamikosButton;
    @FindBy(xpath = "//div[10]//textarea[@class='bg-c-textarea__field bg-c-textarea__field--lg']")
    private WebElement otherNotesInput;
    @FindBy(xpath = "//h4[.='Catatan Alamat']/following-sibling::div//textarea")
    private WebElement addressNotesInput;
    @FindBy(css = ".bg-c-button--md")
    private WebElement setRuleButton;
    @FindBy(xpath = "//div[@class='bg-c-field__message']")
    private WebElement errorMsgRoomType;
    @FindBy(css = ".bg-c-input__field")
    private WebElement roomTypeInputInPopUp;
    @FindBy(css = ".bg-c-modal__body-title")
    private WebElement titleSuccessEditPopUpText;
    @FindBy(xpath = "//div[@class='list-room']/div[1]//div[contains(@class, 'room-input --name bg-c-field')]//input[@class='bg-c-input__field']")
    private WebElement firstRoomNameInput;
    @FindBy(xpath = "//div[@class='list-room']/div[1]//div[@class='room-input bg-c-field']//input[@class='bg-c-input__field']")
    private WebElement firstRoomFloorInput;
    @FindBy(xpath = "//div[@class='room-input bg-c-field bg-c-field--error']/div[@class='bg-c-field__message']")
    private WebElement errorMessageRoomFloor;
    @FindBy(css = ".content > div:nth-of-type(1) .bg-c-field__message")
    private WebElement errorMessageKosName;
    @FindBy(css = ".content > div:nth-of-type(2) .bg-c-field__message")
    private WebElement errorMessageKosType;
    @FindBy(css = ".content > div:nth-of-type(4) .bg-c-field__message")
    private WebElement errorMessageKosDesc;
    @FindBy(css = "div:nth-of-type(8) .bg-c-field__message")
    private WebElement errorMessageKosManagerName;
    @FindBy(css = "div:nth-of-type(9) .bg-c-field__message")
    private WebElement errorMessageKosManagerPhone;
    @FindBy(xpath = "//div[@class='empty-state__label']")
    private WebElement addKosRulePhotoButton;
    @FindBy(xpath = "//div[@class='image-uploader']/div[1]//img")
    private WebElement lastImageSource;
    @FindBy(css = ".list-room > div:nth-of-type(1) [type='checkbox']")
    private WebElement alreadyInhabitedCheck;
    @FindBy(css = ".list-room > div:nth-of-type(1) .check")
    private WebElement alreadyInhabitedCheckbox;
    @FindBy(css = ".c-mk-button")
    private WebElement doneRoomAvailabilityButton;
    @FindBy(xpath = "//div[@class='bg-c-field__message']")
    private WebElement errorMessageCity;
    @FindBy(xpath = "//div[@class='bg-c-field__message']")
    private WebElement errorMessageSubdistrict;
    @FindBy(css = ".step-six__other-input--error-message")
    private WebElement errorMessageRoomSize;
    @FindBy(css = ".step-six__other-size > div:nth-of-type(1) [placeholder='0']")
    private WebElement roomSizeLengthInput;
    @FindBy(css = ".step-six__other-size > div:nth-of-type(2) [placeholder='0']")
    private WebElement roomSizeWidthInput;
    @FindBy(xpath = "//a[contains(.,'Edit Data Pribadi')]")
    private WebElement editPersonalDataButton;
    @FindBy(css = ".step-six__content > div:nth-of-type(2) .bg-c-field__message")
    private WebElement errorMessageTotalRoom;
    @FindBy(css = ".step-six__content > div:nth-of-type(3) .bg-c-field__message")
    private WebElement errorMessageTotalAvailableRoom;
    @FindBy(css = "[href='#basic-close']")
    private WebElement icnClose;
    @FindBy(xpath = "//*[@class='bg-c-text bg-c-text--title-2 ']")
    private WebElement pageChoosePropertyTitle;
    @FindBy(xpath = "//*[@class='images__error bg-c-alert bg-c-alert--error']")
    private List<WebElement> uploadInvalidKosPhotoText;
    @FindBy(xpath = ".bg-c-alert__content-title")
    private List<WebElement> needToCompletePhotoText;
    @FindBy(css = ".loading-step-desktop")
    private WebElement loadingEditForm;
    @FindBy(xpath = "//*[contains(text(), 'Tambah tipe kamar lain dari kos dibawah ini')]")
    private WebElement tambahTipeKamarText;
    @FindBy(css = "button:nth-of-type(2)")
    private WebElement editSelesai;
    @FindBy(css = ".owner-kos-list > div:nth-of-type(1) .kos-card__title > .text")
    private WebElement firstKosNameLabel;
    @FindBy(xpath = "//input[contains (@name, 'email')]")
    private WebElement emailTextBox;
    @FindBy(xpath = "//input[contains (@name, 'password')]")
    private WebElement passWordTextBox;
    @FindBy(xpath = "//button[normalize-space()='Sign me in']")
    private WebElement loginButton;
    @FindBy(xpath = "//a[contains(.,'Kost Owner')]")
    private WebElement kostOwnerMenu;
    @FindBy(xpath = "//input[@name='q']")
    private WebElement kosNameSearch;
    @FindBy(xpath = "//a[@title='Verify']//i[@class='fa fa-check']")
    private WebElement firstVerifyButton;
    @FindBy(xpath = "//button[@id='buttonSearch']")
    private WebElement searchButton;
    @FindBy(xpath = "//div[@id='verifyModal4' and @style='display: block;']//form/button[@class='btn btn-sm btn-success']")
    private WebElement verifyKosPopUpButton;
    @FindBy(xpath = "//*[@class='bg-c-modal__action-closable']//*[@class='bg-c-icon bg-c-icon--md']")
    private WebElement icnCloseButton;
    @FindBy(xpath = "(//div[@class='bg-c-select__trigger bg-c-select__trigger--lg'])[2]")
    private WebElement kabupatenButton;
    @FindBy(xpath = "//div[@class='bg-c-select__trigger bg-c-select__trigger--error bg-c-select__trigger--lg']")
    private WebElement kabupatenButton2;
    @FindBy(xpath = "//div[@class='bg-c-select__trigger bg-c-select__trigger--error bg-c-select__trigger--lg']")
    private WebElement kecamatanButton;
    @FindBy(xpath = "//div[@class='bg-c-select__trigger bg-c-select__trigger--disabled bg-c-select__trigger--lg']")
    private WebElement kecamatanButton2;
    @FindBy(css = ".c-mk-header__username")
    private WebElement ownerUserName;
    @FindBy(xpath = "//*[@class='c-mk-header-user__logout']")
    private WebElement logoutOwnerPageButton;
    @FindBy(xpath = "//*[@data-testid='entryButton']")
    private WebElement enterButton;
    @FindBy(xpath = "//*[@class='btn btn-primary btn-mamigreen login-button track-login-owner']")
    private WebElement loginOwnerButton;
    @FindBy(xpath = "//*[@data-testid='phoneNumberTextbox']")
    private WebElement phoneTextbox;
    @FindBy(xpath = "//*[@data-testid='pemilikKosButton']")
    private WebElement kosOwnerButton;
    @FindBy(xpath = "//*[@data-testid='passwordTextbox']")
    private WebElement passwordTextbox;
    @FindBy(xpath = "//*[@class='bs-searchbox']/input")
    private WebElement inputKosTextBox;
    @FindBy(xpath = "//li[@class='bs-searchbox']/following-sibling::li[1]")
    private WebElement clickKosName;
    @FindBy(xpath = "//span[@class='btn-content']")
    private WebElement searchKosDropdownList;
    @FindBy(xpath = "//div[@class='kos-card__expandable']")
    private WebElement lihatselengkapnyaButton;


    /**
     * Webdriver selenium helper
     */
    public AddKosPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * Click on radio button "Kos"
     *
     * @throws InterruptedException
     */
    public void clickOnKosRadioButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(addKosRadioButton, 5);
        selenium.clickOn(addKosRadioButton);
    }

    /**
     * Click on add data button
     *
     * @throws InterruptedException
     */
    public void clickOnAddDataButton() throws InterruptedException {
        selenium.clickOn(addButton);
    }

    /**
     * Input kost location in create kost page
     *
     * @throws InterruptedException
     */
    public void insertKosLocation(String locationName) throws InterruptedException {
        Thread.sleep(5);
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            System.out.println("Alert data: " + alertText);
            alert.accept();
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
        }
        selenium.hardWait(5);
        selenium.clickOn(locationTextBox);
        locationTextBox.clear();
        selenium.enterText(locationTextBox, locationName, true);
        selenium.waitForJavascriptToLoad();
          selenium.hardWait(5);
    }

    /**
     * Click on the first autocomplete result
     *
     * @throws InterruptedException
     */
    public void clickOnFirstResult(String kabupaten, String kecamatan) throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitInCaseElementVisible(locationAutoComplete, 5);
        selenium.clickOn(locationAutoComplete);
        if (selenium.isElementNotDisplayed(kabupatenButton)) {
            selenium.pageScrollInView(kabupatenButton);
            selenium.clickOn(kabupatenButton);
        }else {
            selenium.pageScrollInView(kabupatenButton2);
            selenium.clickOn(kabupatenButton2);
        }
        WebElement elementA = driver.findElement(By.xpath("//div[normalize-space()='"+kabupaten+"']"));
        selenium.clickOn(elementA);
        selenium.hardWait(3);
        if (selenium.isElementNotDisplayed(kecamatanButton)) {
            selenium.pageScrollInView(kecamatanButton);
            selenium.clickOn(kecamatanButton);
        }else {
            selenium.pageScrollInView(kecamatanButton2);
            selenium.clickOn(kecamatanButton2);
        }
        WebElement elementB = driver.findElement(By.xpath("//div[normalize-space()='"+kecamatan+"']"));
        selenium.hardWait(3);
        selenium.clickOn(elementB);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
    }

    /**
     * Click on the first autocomplete result
     *
     * @throws InterruptedException
     */
    public void checkResult() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitInCaseElementVisible(locationAutoComplete, 5);
        selenium.clickOn(locationAutoComplete);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
    }


    /**
     * Check on disabled next button on location data when create kos
     *
     * @return boolean
     */
    public boolean checkDisabledNextButtonLocationData() {
        return selenium.waitInCaseElementVisible(disabledNextButtonLocationData, 3) != null;
    }

    /**
     * Input kost name in create kost page
     *
     * @throws InterruptedException
     */
    public void insertKosName(String kosName) throws InterruptedException {
        selenium.enterText(kosNameTextBox, kosName, true);
        selenium.hardWait(1);
    }

    /**
     * Check if kost name field is enable
     *
     * @return true if enable
     */
    public boolean isKosNameFieldEnable() {
        return kosNameTextBox.isEnabled();
    }

    /**
     * Check if kost room type checkbox is enable
     *
     * @return true if enable
     */
    public boolean isRoomTypeCheckboxEnable() {
        return roomTypeCheckbox.isEnabled();
    }

    /**
     * Message error when input invalid kost name : existing kost name in create kost page
     *
     * @return error message
     */
    public String getErrorMessageExistingKostName() {
        return selenium.getText(kosNameIsInvalidValueLabel);
    }

    /**
     * Click on gender
     *
     * @throws InterruptedException
     */
    public void clickOnIconGender(String gender) throws InterruptedException {
        if (gender.equals("male") || gender.equals("boy")) {
            selenium.hardWait(3);
            selenium.pageScrollInView(kostTypeIconBoy);
            selenium.clickOn(kostTypeIconBoy);
        } else if (gender.equals("female") || gender.equals("girl")) {
            selenium.waitInCaseElementClickable(kostTypeIconGirl,10);
            selenium.pageScrollInView(kostTypeIconGirl);
            selenium.clickOn(kostTypeIconGirl);
        } else {
            selenium.hardWait(3);
            selenium.pageScrollInView(kostTypeIconMix);
            selenium.clickOn(kostTypeIconMix);
        }
    }

    /**
     * Click on kos room size
     *
     * @param size is room size
     * @throws InterruptedException
     */
    public void clickOnIconRoomSize(String size) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//span[.='" + size + "']"));
        selenium.hardWait(5);
        selenium.clickOn(element);
    }

    /**
     * Click at menu owner dashboard
     *
     * @throws InterruptedException
     */
    public void ownerDashboardMenu() throws InterruptedException {
        selenium.clickOn(userPhotoButton);
        selenium.hardWait(3);
        selenium.clickOn(ownerPageButton);
        selenium.hardWait(8);
    }

    /**
     * Click at menu properti saya
     *
     * @throws InterruptedException
     */
    public void clickOnMenuPropertySaya() throws InterruptedException {
        selenium.clickOn(leftMenuPropertiSaya);
    }

    /**
     * Verify Management kos menu
     */
    public boolean managementKosMenu() {
        return selenium.waitInCaseElementVisible(managementKosMenuButton, 3) != null;
    }

    /**
     * Click at menu kost
     *
     * @throws InterruptedException
     */
    public void clickOnMenuKost() throws InterruptedException {
        selenium.clickOn(leftMenuKos);
        selenium.hardWait(3);
    }

    /**
     * click button edit kost
     *
     * @throws InterruptedException
     */
    public void buttonEditKost() throws InterruptedException {
        selenium.clickOn(buttonEditDataKost);
        selenium.hardWait(5);
    }
    /**
     * return boolean edit button
     *
     * @throws InterruptedException
     */
    public boolean disableButtonEditKost() throws InterruptedException {
        return selenium.waitInCaseElementVisible(buttonEditDataKost, 5) != null;
    }


    /**
     * Message error when edit kost address is emphty
     *
     * @return error message
     */
    public String EditCompleteKostAddress() {
        return selenium.getText(textValidationAddress);
    }

    /**
     * Click button edit Data kos
     *
     * @throws InterruptedException
     */
    public void clickEditKosData() throws InterruptedException {
        selenium.hardWait(10);
        selenium.waitInCaseElementClickable(editDataKosButton, 10);
        selenium.clickOn(editDataKosButton);
    }

    /**
     * Click button edit kos address
     *
     * @throws InterruptedException
     */
    public void clickEditKosAddress() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(editKosAddressButton);
        selenium.hardWait(5);
        // selenium.fluentWaitElementToBeClickable(By.xpath("//div[@class='property-sidebar__steps steps']/div[2]/span[@class='steps__edit-item']"),15,2);
        //    selenium.waitTillElementIsNotPresent(loadingEditForm,10);
    }

    /**
     * Click button edit kos photos
     *
     * @throws InterruptedException
     */
    public void clickEditKosPhotos() throws InterruptedException {
        selenium.waitTillElementIsNotPresent(loadingEditForm, 3);
        selenium.clickOn(editKosPhotoButton);
    }

    /**
     * Click button edit room photos
     *
     * @throws InterruptedException
     */
    public void clickEditRoomPhotos() throws InterruptedException {
        selenium.waitTillElementIsNotPresent(loadingEditForm, 3);
        selenium.clickOn(editRoomPhotoButton);
    }

    /**
     * Click button edit facilities
     *
     * @throws InterruptedException
     */
    public void clickEditFacilities() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(editFacilitiesButton);
    }

    /**
     * Click button edit facilities
     *
     * @throws InterruptedException
     */
    public void clickEditRoomAvailability() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(editRoomAvailabilityButton);
    }

    /**
     * Click kos rules checkbox
     *
     * @param kosRule is kos rules
     * @throws InterruptedException
     */
    public void clickKosRulesCheckbox(String kosRule) throws InterruptedException {
        selenium.hardWait(5);
        WebElement element = driver.findElement(By.xpath("//span[contains(., 'Peraturan kos')]/following-sibling::div//label[contains(text(), '" + kosRule + "')]//input"));
        selenium.javascriptClickOn(element);
    }

    /**
     * Click facilities checkbox
     *
     * @param section  is facility section, example "Fasilitas Umum"
     * @param facility is facility name
     * @throws InterruptedException
     */
    public void clickFacilitiesCheckbox(String section, String facility) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        WebElement elementA = driver.findElement(By.xpath("//h4[contains(., '" + section + "')]/following-sibling::div//span[contains(text(), '" + facility + "')]/preceding-sibling::input"));
        if (selenium.isElementNotDisplayed(elementA)) {
            selenium.clickOn(elementA);
        } else if (selenium.isElementNotDisplayed(elementA)) {
            selenium.javascriptClickOn(elementA);
        } else if (selenium.isElementNotDisplayed(elementA)) {
            WebElement elementB = driver.findElement(By.xpath("//span[contains(text(), '" + facility + "')]/preceding-sibling::input"));
            selenium.clickOn(elementB);
        } else {
                WebElement elementB = driver.findElement(By.xpath("//span[contains(text(), '" + facility + "')]/preceding-sibling::input"));
                selenium.javascriptClickOn(elementB);
            }
        }

    /**
     * Verify button edit finish is disabled
     *
     * @return true if disabled
     */
    public String isEditFinishedButtonDisabled() {
     //   return selenium.waitInCaseElementVisible(editFinishedButtonDisabled, 3) != null;
        return selenium.getText(editFinishedButtonDisabled);
    }

    /**
     * Get warning title in certain facility
     *
     * @param facility is facility section
     * @return error message title
     */
    public String getWarningTitleFacility(String facility) {
        WebElement element = driver.findElement(By.xpath("//h4[contains(text(), '" + facility + "')]/following-sibling::div[1]//p[1]"));
        return selenium.getText(element);
    }

    /**
     * Get warning description in certain facility
     *
     * @param facility is facility section
     * @return error message description
     */
    public String getWarningDescFacility(String facility) {
        WebElement element = driver.findElement(By.xpath("//h4[contains(text(), '" + facility + "')]/following-sibling::div[1]//p[2]"));
        return selenium.getText(element);
    }

    /**
     * Click on cheklist room type
     *
     * @throws InterruptedException
     */
    public void clickOnChecklistRoomType() throws InterruptedException {
        selenium.pageScrollInView(checkTypeRoom);
        selenium.clickOn(checkTypeRoom);
    }

    /**
     * Check on disabled next button when create kos
     *
     * @return boolean
     */
    public boolean checkDisabledNextButtonCreateKost() {
        return selenium.waitInCaseElementVisible(nextButtonCreateKostDisable, 3) != null;
    }

    /**
     * Input kost name at section Data Kost
     */
    public void insertKosNameDataKost(String kosNameDataKost) {
        selenium.enterText(fieldInputKostName, kosNameDataKost, true);
    }

    /**
     * Input room type at section Data Kost
     *
     * @param roomType is String room type
     */
    public void insertRoomTypeDataKost(String roomType) {
        selenium.enterText(fieldInputRoomType, roomType, true);
    }

    /**
     * Input room type at section Add Data Kost pop up
     *
     * @param roomType is String room type
     */
    public void insertRoomTypePopUp(String roomType) {
        selenium.enterText(roomTypeInputInPopUp, roomType, true);
    }

    /**
     * Click on cheklist admin kost
     *
     * @throws InterruptedException
     */
    public void clickOnChecklistAdminKost() throws InterruptedException {
        selenium.javascriptPageScrollHeightToDown();
        selenium.waitInCaseElementClickable(checkBoxAdminKos,10);
        selenium.clickOn(checkBoxAdminKos);
    }

    /**
     * return boolean checkbox
     *
     * @throws InterruptedException
     */
    public boolean unhecklistAdminKost() throws InterruptedException {
        return selenium.waitInCaseElementVisible(uncheckBoxAdminKos, 5) != null;
    }

    /**
     * Input name admin kost at section Data Kost
     *
     * @param adminKos is admin name
     */
    public void insertNameAdminKos(String adminKos) {
        selenium.enterText(fieldInputAdminKost, adminKos, true);
    }

    /**
     * Input name admin kost at section Data Kost
     *
     * @param phone is admin phone number
     */
    public void insertAdminPhone(String phone) {
        selenium.enterText(fieldInputAdminPhone, phone, true);
    }

    /**
     * Click on add new kos button
     *
     * @throws InterruptedException
     */
    public void clickAddNewKos() throws InterruptedException {
        selenium.clickOn(addNewKosButton);
    }

    /**
     * Verify add new kos button appear
     *
     * @return true if appear
     */
    public boolean isAddNewKosBtnAppear() {
        return selenium.waitInCaseElementVisible(addNewKosButton, 3) != null;
    }

    /**
     * Input kost description in create kost page
     *
     * @param desc is kos description
     */
    public void insertKosDescription(String desc) throws InterruptedException {
        selenium.enterText(kosDescriptionField, desc, true);
    }

    /**
     * Select kos year built in dropdown
     *
     * @param year is kos year built
     * @throws InterruptedException
     */
    public void selectKosYearBuilt(String year) throws InterruptedException {
        selenium.pageScrollInView(yearBuiltDropdown);
        selenium.clickOn(yearBuiltDropdown);
        selenium.hardWait(3);
        WebElement element = driver.findElement(By.xpath("//a[contains(.,'" + year + "')]"));
        selenium.clickOn(element);
    }

    /**
     * "]
     * Click on next button
     *
     * @throws InterruptedException
     */
    public void clickNext() throws InterruptedException {
        //   selenium.waitTillElementIsClickable(lanjutkanButton);
        selenium.hardWait(5);
        selenium.pageScrollInView(lanjutkanButton);
        selenium.clickOn(lanjutkanButton);
        selenium.hardWait(5);
    }

    /**
     * "]
     * Click on lanjutkan button
     *
     * @throws InterruptedException
     */
    public void clickLanjutkan() throws InterruptedException {
        selenium.hardWait(1);
        //  selenium.pageScrollInView(lanjutkanButton);
        selenium.pageScrollUsingCoordinate(0, 1000);
        selenium.clickOn(lanjutkanButton);
        selenium.hardWait(5);
    }

    /**
     * "]
     * Click on edit selesai button
     */
    public void clickEditSelesai() throws InterruptedException {
        selenium.javascriptClickOn(editSelesai);
    }

    /**
     * Upload image to certain photo section
     *
     * @param section is photo section
     */
    public void uploadPhoto(String section, String photo) throws InterruptedException {
        String projectpath = System.getProperty("user.dir");
        WebElement element;
        switch (section) {
            case "Foto bangunan tampak depan":
                element = uploadFrontBuildingPhoto;
                break;
            case "Foto tampilan dalam bangunan":
                element = uploadInsideBuildingPhoto;
                break;
            case "Foto tampak dari jalan":
                element = uploadStreetViewPhoto;
                break;
            case "Foto depan kamar":
                element = uploadFrontRoomPhoto;
                break;
            case "Foto dalam kamar":
                element = uploadInsideRoomPhoto;
                break;
            case "Foto kamar mandi":
                element = uploadBathroomPhoto;
                break;
            case "Tambahan foto lain? boleh dong":
                element = uploadAdditionalPhoto;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + section);
        }
        selenium.hardWait(3);
        selenium.isElementDisplayed(element);
        if (photo.equals("valid")) {
            element.sendKeys(projectpath + "/src/main/resources/images/anime.jpeg");
        } else {
            element.sendKeys(projectpath + "/src/main/resources/images/toothwithcrown.svg");
        }
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click on checkbox other than monthly price
     */
    public void clickOtherThanMonthlyPrice() {
        selenium.javascriptClickOn(otherMonthlyPriceCheckbox);
    }

    /**
     * Click on daily price checkbo]"?"
     */
    public void clickDailyPriceCheckbox() {
        if (selenium.waitInCaseElementClickable(dailyPriceInput, 2) == null) {
            selenium.javascriptClickOn(dailyPriceCheckbox);
        }
    }

    /**
     * Click on weekly price checkbox
     */
    public void clickWeeklyPriceCheckbox() {
        selenium.javascriptClickOn(weeklyPriceCheckbox);
    }

    /**
     * Click on three monthly price checkbox
     */
    public void click3MonthlyPriceCheckbox() {
        selenium.javascriptClickOn(threeMonthlyPriceCheckbox);
    }

    /**
     * Click on six monthly price checkbox
     */
    public void click6MonthlyPriceCheckbox() {
        selenium.javascriptClickOn(sixMonthlyPriceCheckbox);
    }

    /**
     * Click on yearly price checkbox
     */
    public void clickYearlyPriceCheckbox() {
        selenium.javascriptClickOn(yearlyPriceCheckbox);
    }

    /**
     * Click on additional cost checkbox
     */
    public void clickAdditionalCostCheckbox() {
        selenium.javascriptClickOn(additionalCostCheckbox);
    }

    /**
     * Click on deposit checkbox
     */
    public void clickDepositCheckbox() {
        selenium.javascriptClickOn(depositCheckbox);
    }

    /**
     * Click on fine checkbox
     */
    public void clickFineCheckbox() {
        selenium.javascriptClickOn(fineCheckbox);
    }

    /**
     * Click on set rule button if appear
     */
    public void clickSetRuleButton() {
        if (selenium.waitInCaseElementVisible(setRuleButton, 5) != null) {
            selenium.javascriptClickOn(setRuleButton);
        }
    }

    /**
     * Fill daily price field
     * if field disabled then click the checkbox
     *
     * @param price is kos daily price
     */
    public void insertDailyPrice(String price) throws InterruptedException {
        if (selenium.waitInCaseElementClickable(dailyPriceInput, 2) == null) {
            this.clickDailyPriceCheckbox();
        }
        selenium.pageScrollInView(dailyPriceInput);
        selenium.clickOn(dailyPriceInput);
        selenium.enterTextCharacterByCharacter(dailyPriceInput, price, false);
    }

    /**
     * Input monthly price
     *
     * @param price is kos monthly price
     */
    public void insertMonthlyPrice(String price) throws InterruptedException {
        selenium.hardWait(5);
        selenium.waitTillElementIsVisible(monthlyPriceInput);
        selenium.waitTillElementIsClickable(monthlyPriceInput);
        selenium.clickOn(monthlyPriceInput);
        selenium.enterTextCharacterByCharacter(monthlyPriceInput, price, false);
    }

    /**
     * Fill weekly price field
     * if field disabled then click the checkbox
     *
     * @param price is kos weekly price
     */
    public void insertWeeklyPrice(String price) throws InterruptedException {
        if (selenium.waitInCaseElementClickable(weeklyPriceInput, 2) == null) {
            this.clickWeeklyPriceCheckbox();
        }
        selenium.pageScrollInView(weeklyPriceInput);
        selenium.clickOn(weeklyPriceInput);
        selenium.enterTextCharacterByCharacter(weeklyPriceInput, price, false);
    }

    /**
     * Fill three monthly price field
     * if field disabled then click the checkbox
     *
     * @param price is kos 3 monthly price
     */
    public void insert3MonthlyPrice(String price) throws InterruptedException {
        if (selenium.waitInCaseElementClickable(threeMonthlyPriceInput, 2) == null) {
            this.click3MonthlyPriceCheckbox();
        }
        selenium.pageScrollInView(yearlyPriceInput);
        selenium.clickOn(threeMonthlyPriceInput);
        selenium.enterTextCharacterByCharacter(threeMonthlyPriceInput, price, false);
    }

    /**
     * Fill six monthly price field
     * if field disabled then click the checkbox
     *
     * @param price is kos 6 monthly price
     */
    public void insert6MonthlyPrice(String price) throws InterruptedException {
        if (selenium.waitInCaseElementClickable(sixMonthlyPriceInput, 2) == null) {
            this.click6MonthlyPriceCheckbox();
        }
        selenium.clickOn(sixMonthlyPriceInput);
        selenium.enterTextCharacterByCharacter(sixMonthlyPriceInput, price, false);
    }

    /**
     * Fill yearly price field
     * if field disabled then click the checkbox
     *
     * @param price is kos yearly price
     */
    public void insertYearlyPrice(String price) throws InterruptedException {
        if (selenium.waitInCaseElementClickable(yearlyPriceInput, 2) == null) {
            this.clickYearlyPriceCheckbox();
        }
        selenium.clickOn(yearlyPriceInput);
        selenium.enterTextCharacterByCharacter(yearlyPriceInput, price, false);
    }

    /**
     * Input deposit
     *
     * @param deposit is deposit value
     */
    public void insertDeposit(String deposit) throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 1600);
        selenium.clickOn(depositInput);
        // selenium.enterTextUsingJavascriptExecute(depositInput, deposit, true);
        selenium.fillFieldAfterDoubleClick(depositInput, deposit);
    }

    /**
     * Input fine
     *
     * @param fine is deposit value
     */
    public void insertFine(String fine) throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 2000);
        // selenium.clickOn(fineInput);
        selenium.enterTextUsingJavascriptExecute(fineInput, fine, true);
        //  selenium.fillFieldAfterDoubleClick(fineInput, fine);

    }

    /**
     * Get error message in all field of add kos price page
     *
     * @param section is field section
     * @return error message
     */
    public String getErrorMessagePrice(String section) {
        WebElement element = driver.findElement(By.xpath("//h4[contains(., '" + section + "')]/following-sibling::div//div[@class='bg-c-field__message']"));
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Delete first kos on the list
     *
     * @throws InterruptedException
     */
    public void deleteFirstKos() throws InterruptedException {
        selenium.waitInCaseElementVisible(deleteKosButton, 5);
        selenium.clickOn(deleteKosButton);
        selenium.clickOn(confirmDelete);
        selenium.hardWait(1);
        selenium.waitTillElementsCountIsEqualTo(loadingDeleteKos, 0);
        selenium.hardWait(2);
        selenium.waitTillElementsCountIsEqualTo(loadingKosList, 0);
        selenium.hardWait(1);
    }

    /**
     * Click kos name under add other type from kos
     *
     * @param kosName is kos name
     * @throws InterruptedException
     */
    public void clickKosName(String kosName) throws InterruptedException {
        selenium.fluentWaitElementToVisible(tambahTipeKamarText);
        WebElement element = driver.findElement(By.xpath("//div[.='" + kosName + "']"));
        selenium.clickOn(element);
    }

    /**
     * Click copy from radio button
     *
     * @param type is kos type
     * @throws InterruptedException
     */
    public void clickCopyKosType(String type) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//p[.='" + type + "']"));
        selenium.clickOn(element);
    }

    /**
     * Click next button in copy from kos type pop up
     */
    public void clickNextInPopUp() throws InterruptedException {
        selenium.javascriptClickOn(popUpNextButton);
        selenium.hardWait(10);
    }

    /**
     * Enter total available room
     *
     * @param totalRoom is total available room
     */
    public void enterTotalAvailableRoom(String totalRoom) {
        selenium.pageScrollInView(availableRoomField);
        selenium.enterText(availableRoomField, totalRoom, true);
    }

    /**
     * Click on minimum rent duration checkbox
     */
    public void clickMinDurationRentCheckbox() {
        selenium.javascriptClickOn(minDurationRentCheckbox);
    }

    /**
     * Select minimum duration rent from dropdown
     *
     * @param minDuration is minimum rent duration
     * @throws InterruptedException
     */
    public void selectMinDurationRent(String minDuration) throws InterruptedException {
        selenium.waitTillElementIsVisible(minDurationRentDropdown);
        selenium.waitTillElementIsNotPresent(loadingEditForm, 5);
        selenium.clickOn(minDurationRentDropdown);
        WebElement element = driver.findElement(By.xpath("//li[contains(.,'" + minDuration + "')]"));
        selenium.clickOn(element);
    }

    /**
     * Enter additional cost's name
     *
     * @param name is additional cost's name
     */
    public void enterAdditionalCostName(String name) {
        selenium.enterText(additionalCostNameField, name, false);
    }

    /**
     * Enter total additional cost
     *
     * @param total is total additional cost
     */
    public void enterTotalAdditionalCost(String total) throws InterruptedException {
        selenium.enterTextUsingJavascriptExecute(totalAdditionalCostField, total, true);
    }

    /**
     * Click on down payment checkbox
     */
    public void clickDownPaymentCheckbox() {
        selenium.javascriptClickOn(downPaymentCheckbox);
    }

    /**
     * Select down payment from dropdown
     *
     * @param downPayment is down payment percentage
     * @throws InterruptedException
     */
    public void selectDownPayment(String downPayment) throws InterruptedException {
        selenium.pageScrollInView(downPaymentDropdown);
        selenium.clickOn(downPaymentDropdown);
        WebElement element = driver.findElement(By.xpath("//a[contains(.,'" + downPayment + "')]"));
        selenium.clickOn(element);
    }

    /**
     * Input amount denda
     *
     * @throws InterruptedException
     */
    public void inputAmountDenda(String denda) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("(//input[@type='text'])[10]"));
        selenium.clickOn(element);
        selenium.enterText(element, denda, false);
    }

    /**
     * Select time limit from dropdown
     *
     * @param number is number in left dropdown
     * @param time   is time in right dropdown
     * @throws InterruptedException
     */
    public void selectTimeLimit(String number, String time) throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 3000);
        selenium.clickOn(timeLimitNumberDropdown);
        WebElement element = driver.findElement(By.xpath("//div[@class='bg-c-dropdown__menu bg-c-dropdown__menu--open bg-c-dropdown__menu--scrollable bg-c-dropdown__menu--fit-to-trigger bg-c-dropdown__menu--text-lg']//li[" + number + "]/a"));
        selenium.clickOn(element);
        selenium.clickOn(timeLimitDropdown);
        WebElement element2 = driver.findElement(By.xpath("//a[contains(.,'" + time + "')]"));
        selenium.clickOn(element2);
    }

    /**
     * Click on done in success page
     *
     * @throws InterruptedException
     */
    public void clickDoneSuccessButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(doneSuccessButton);
    }

    /**
     * Click on skip in ask mamikos page if appear
     *
     * @throws InterruptedException
     */
    public void clickSkipAskMamikosButton() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(skipAskMamikosButton, 3) != null) {
            selenium.clickOn(skipAskMamikosButton);
        }
    }

    /**
     * Click on upload rule photo
     *
     * @throws InterruptedException
     */
    public void clickUploadKosRulesPhoto() throws InterruptedException {
        selenium.clickOn(clickUploadKosRulesPhoto);
    }

    /**
     * Click on edit done in add kos form page
     *
     * @throws InterruptedException
     */
    public void clickEditDoneButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(editFinishedButton);
        selenium.hardWait(5);
    }

    /**
     * Upload image to kos rules section
     */
    public void uploadKosRulesPhoto() {
        String projectpath = System.getProperty("user.dir");
        uploadKosRulesPhoto.sendKeys(projectpath + "/src/main/resources/images/anime.jpeg");
    }

    /**
     * Upload invalid image to kos rules section
     */
    public void uploadInvalidKosRulesPhoto() {
        String projectpath = System.getProperty("user.dir");
        uploadKosRulesPhoto.sendKeys(projectpath + "/src/main/resources/images/toothwithcrown.svg");
    }

    /**
     * Get error message in upload invalid photo
     *
     * @return String <p>Upload Gagal</p>
     * Format foto tidak didukung
     */
    public List<String> getUploadInvalidKosPhotoText() {
        List<String> invalidImageText = new ArrayList<>();
        for (WebElement s : uploadInvalidKosPhotoText) {
            invalidImageText.add((selenium.getText(s).replaceAll("\n", " ")));
        }
        return invalidImageText;
    }

    /**
     * Enter other notes
     *
     * @param notes is text notes
     */
    public void enterOtherNotes(String notes) {
        selenium.enterText(otherNotesInput, notes, false);
    }

    /**
     * Enter address notes
     *
     * @param notes is address notes
     */
    public void enterAddressNotes(String notes) {
        selenium.pageScrollInView(addressNotesInput);
        selenium.enterText(addressNotesInput, notes, true);
    }

    /**
     * Message error when input invalid room type (empty or already exist)
     *
     * @return String error message
     */
    public String getErrorMessageRoomType() {
        return selenium.getText(errorMsgRoomType);
    }

    /**
     * Get title for success edit pop up
     *
     * @return String pop up title
     */
    public String getTitlePopUpSuccessEditKos() throws InterruptedException {
        selenium.hardWait(5);
      //  selenium.waitForJavascriptToLoad();
        return selenium.getText(titleSuccessEditPopUpText);
    }

    /**
     * Enter room name / number in room allotment page
     *
     * @param roomNo is room name or number
     */
    public void enterRoomName(String roomNo) throws InterruptedException {
        selenium.hardWait(5);
        selenium.waitForJavascriptToLoad();
        selenium.enterText(firstRoomNameInput, roomNo, true);
    }

    /**
     * Get room name/number from field
     *
     * @return room name/number text
     */
    public boolean getFirstRoomName(String room) {
//        selenium.getInputText(firstRoomNameInput);
//        return false;
        return selenium.waitInCaseElementVisible(firstRoomNameInput, 3) != null;
    }

    /**
     * Enter floor number in room allotment page
     *
     * @param floorNo is floor number
     */
    public void enterFloorNumber(String floorNo) {
        selenium.enterText(firstRoomFloorInput, floorNo, true);
    }

    /**
     * Get floor name/number from field
     *
     * @return floor name/number text
     */
    public boolean getFirstFloorNumber(String floor) {
        return selenium.waitInCaseElementVisible(firstRoomNameInput, 3) != null;
    }

    /**
     * Get error message floor field in room allotment
     *
     * @return String error message
     */
    public String getErrorFloorField() {
        return selenium.getText(errorMessageRoomFloor);
    }

    /**
     * Get error message kos name field in data kos
     *
     * @return String error message
     */
    public String getErrorKosNameField() {
        return selenium.getText(errorMessageKosName);
    }

    /**
     * Get error message kos type field in data kos
     *
     * @return String error message
     */
    public String getErrorKosTypeField() {
        return selenium.getText(errorMessageKosType);
    }

    /**
     * Get error message kos description field in data kos
     *
     * @return String error message
     */
    public String getErrorKosDescField() {
        return selenium.getText(errorMessageKosDesc);
    }

    /**
     * Get error message kos manager name field in data kos
     *
     * @return String error message
     */
    public String getErrorKosManagerNameField() {
        return selenium.getText(errorMessageKosManagerName);
    }

    /**
     * Get error message kos manager phone field in data kos
     *
     * @return String error message
     */
    public String getErrorKosManagerPhoneField() {
        return selenium.getText(errorMessageKosManagerPhone);
    }

    /**
     * Check if add photo button in kos rules is appear
     *
     * @return true if appear
     */
    public boolean isKosRuleAddPhotoAppear() {
        return selenium.waitInCaseElementVisible(addKosRulePhotoButton, 3) != null;
    }

    /**
     * Check if see photo button in kos rules photo is appear
     *
     * @return true if appear
     */
    public boolean isSeeKosRulePhotoAppear() {
        return selenium.waitInCaseElementPresent(seeKosRulePhoto, 3) != null;
    }

    /**
     * Check if change photo button in kos rules photo is appear
     *
     * @return true if appear
     */
    public boolean isChangeKosRulePhotoAppear() {
        return selenium.waitInCaseElementPresent(changeKosRulePhoto, 3) != null;
    }

    /**
     * Check if delete photo button in kos rules photo is appear
     *
     * @return true if appear
     */
    public boolean isDeleteKosRulePhotoAppear() {
        return selenium.waitInCaseElementPresent(deleteKosRulePhoto, 3) != null;
    }

    /**
     * Click delete photo button in last kos rules photo
     */
    public void clickDeleteKosRulePhoto() {
        Actions builder = new Actions(driver);
//        selenium.pageScrollInView(lastImageSource);
//        builder.moveToElement(lastImageSource).perform();
        driver.findElement(deleteKosRulePhoto).click();
    }

    /**
     * Get image source in kos rule photo
     *
     * @return String image src
     */
    public String getKosRulePhotoImageSrc() {
        return selenium.getElementAttributeValue(lastImageSource, "src");
    }

    /**
     * Upload different image to kos rules section
     */
    public void uploadOtherKosRulesPhoto() throws InterruptedException {
        String projectpath = System.getProperty("user.dir");
        uploadKosRulesPhoto.sendKeys(projectpath + "/src/main/resources/images/stf.jpeg");
        selenium.hardWait(3);
    }

    /**
     * Get all image in kos rule photo and count it
     *
     * @return int number of image
     */
    public int countKosRulePhotos() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.waitTillAllElementsAreLocated(kosRulePhotos).size();
    }

    /**
     * Click checkbox already inhabited in Room Availability
     *
     * @throws InterruptedException
     */
    public void clickAlreadyInhabitedCheckbox() throws InterruptedException {
        selenium.clickOn(alreadyInhabitedCheckbox);
    }

    /**
     * Verify if already inhabited checkbox is checked
     *
     * @return true if checked
     */
    public boolean isAlreadyInhabitedChecked() {
        return alreadyInhabitedCheck.isSelected();
    }

    /**
     * Click done in room availability page
     *
     * @throws InterruptedException
     */
    public void clickDoneRoomAvail() throws InterruptedException {
        selenium.clickOn(doneRoomAvailabilityButton);
    }

    /**
     * Get error message kabupaten/city field in kos address
     *
     * @return String error message
     */
    public String getErrorCityField() {
        selenium.pageScrollInView(errorMessageCity);
        return selenium.getText(errorMessageCity);
    }

    /**
     * Get error message kecamatan field in kos address
     *
     * @return String error message
     */
    public String getErrorSubdistrictField() {
        return selenium.getText(errorMessageSubdistrict);
    }

    /**
     * Get facility index in create kos
     *
     * @param section is facility section
     * @return Int index
     */
    private int getIndexByFacilitySection(String section) {
        int number;
        if (section.equals("Fasilitas Umum")) {
            number = 1;
        } else if (section.equals("Fasilitas Kamar")) {
            number = 2;
        } else {
            number = 3;
        }
        return number;
    }

    /**
     * Get error message facility title in create kos
     *
     * @param section is facility section
     * @return String error message
     */
    public String getErrorFacilityTitle(String section) {
        int number = this.getIndexByFacilitySection(section);
        WebElement element = driver.findElement(By.xpath("//div[@class='step-five']/div['" + number + "']//p[1]"));
        return selenium.getText(element);
    }

    /**
     * Get error message facility description in create kos
     *
     * @param section is facility section
     * @return String error message
     */
    public String getErrorFacilityDesc(String section) {
        int number = this.getIndexByFacilitySection(section);
        WebElement element = driver.findElement(By.xpath("//div[@class='step-five']/div['" + number + "']//p[2]"));
        return selenium.getText(element);
    }

    /**
     * Get error message room size in room availability
     *
     * @return String error message
     */
    public String getErrorRoomSize() {
        return selenium.getText(errorMessageRoomSize);
    }

    /**
     * Enter room size length and width
     *
     * @param length is room length (left field)
     */
    public void enterRoomSizeLength(String length) {
        selenium.enterText(roomSizeLengthInput, length, true);
    }

    /**
     * Enter room size length and width
     *
     * @param width is room length (left field)
     */
    public void enterRoomSizeWidth(String width) {
        selenium.enterText(roomSizeWidthInput, width, true);
    }

    /**
     * Click on edit personal data button
     *
     * @throws InterruptedException
     */
    public void clickOnEditPersonalDataButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(editPersonalDataButton, 5);
        selenium.clickOn(editPersonalDataButton);
    }

    /**
     * Click icon close on page Pilih Jenis Properti
     *
     * @throws InterruptedException
     */
    public void clickOnIconClose() throws InterruptedException {
        selenium.clickOn(icnClose);
    }

    /**
     * Get title on page choose propery type
     *
     * @return String choose property type title
     */
    public String getPageChoosePropertyTypeTitle() {
        selenium.waitTillElementIsVisible(pageChoosePropertyTitle);
        return selenium.getText(pageChoosePropertyTitle);
    }

    /**
     * Check if kos description is enable
     *
     * @return true if enable
     */
    public boolean isKosDescEnable() {
        return kosDescriptionField.isEnabled();
    }

    /**
     * Check if kos year built is enable
     *
     * @return true if enable
     */
    public boolean isKosYearBuildEnable() {
        return yearBuiltDropdownBar.isEnabled();
    }

    /**
     * Get error message in total room
     *
     * @return String error message
     */
    public String getErrorMessageTotalRoom() {
        return selenium.getText(errorMessageTotalRoom);
    }

    /**
     * Get error message in total available room
     *
     * @return String error message
     */
    public String getErrorMessageTotalAvailableRoom() {
        selenium.pageScrollInView(errorMessageTotalAvailableRoom);
        return selenium.getText(errorMessageTotalAvailableRoom);
    }

    /**
     * Delete all photo
     */
    public void clickHapusFoto() throws InterruptedException {
        List<WebElement> photos = driver.findElements(By.xpath("(//div[@class='image-container'])[1]"));
        Actions builder = new Actions(driver);
        for (WebElement photo : photos) {
            selenium.pageScrollInView(photo);
            builder.moveToElement(photo).perform();
            driver.findElement(By.xpath("//*[contains(text(), 'Hapus')]")).click();
        }
    }

    /**
     * Get error message when remove photo
     *
     * @return String <p>Anda harus melengkapi foto ini</p>
     */
    public List<String> getNeedtoCompletePhotoText() {
        List<String> needToCompletePhotoTexts = new ArrayList<>();
        for (WebElement s : needToCompletePhotoText) {
            needToCompletePhotoTexts.add((selenium.getText(s).replaceAll("\n", " ")));
        }
        return needToCompletePhotoTexts;
    }

    public boolean getFirstKosName() {
        return selenium.waitInCaseElementVisible(firstKosNameLabel, 5) != null;
    }
}
