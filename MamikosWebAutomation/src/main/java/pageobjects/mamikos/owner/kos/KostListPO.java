package pageobjects.mamikos.owner.kos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class KostListPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public KostListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".btn-group .form-control.dropdown-toggle .btn-content")
    private WebElement searchYourKostHereDropDown;

    @FindBy(css = "input.form-control")
    private WebElement searchYourKostHereInput;

    @FindBy(css = ".dropdown-menu li:nth-child(2)")
    private WebElement searchYourKostResult;

    @FindBy(xpath = "//div[@class='kos-card__detail-wrapper detail-kos']//div[@class='detail-kos__data-desc']/div[contains(.,'Update Harga')]")
    private WebElement updatePriceButton;

    @FindBy(xpath = "//*[contains(text(), 'Update Kamar')]/preceding-sibling::*")
    private WebElement txtRoomAvailableAmount;

    @FindBy(css = ".owner-kos-list > div:nth-of-type(1) .kos-card__title > .text")
    private WebElement firstKosNameLabel;

    @FindBy(xpath = "(//div[@class='kos-card__address']//div[@class='text'])[1]")
    private WebElement firstKosNameLabelLocation;

    @FindBy(css = ".owner-kos-list > div:nth-of-type(1) .kos-card__address > .text")
    private WebElement firstKosAddressLabel;

    //@FindBy(css = ".kos-card__status [class*='kos-card__status']")
    private By kosStatusLabel = By.cssSelector(".kos-card__status [class*='kos-card__status']");

    @FindBy(css = ".owner-kos-list > div:nth-of-type(1) > .kos-card__text > div > span")
    private WebElement firstKosTypeLabel;

    @FindBy(css = ".fade.in .owner-intercept-booking-modal__close-button")
    private WebElement buttonClosePopUp;

    @FindBy(xpath = "//*[@class='owner-kos-list col-xs-12']/div[1]/img")
    private WebElement firstKosImage;

    @FindBy(xpath = "(//div[@class='kos-card__rejected-message']/div[@class='text'])[1]")
    private WebElement firstRejectedInfoText;

    @FindBy(css = "div.mami-loading-inline .spinner")
    private WebElement aniLoading;

    @FindBy(xpath = "//div[normalize-space()='Update Harga']")
    private WebElement updateKosButton;

    @FindBy(css = ".bg-c-button--tertiary")
    private WebElement laterBtnInPopUpNewPolicy;

    @FindBy(xpath = "//a[contains(.,'Lengkapi Data Kos')]")
    private WebElement completeKosDataButton;

    @FindBy(xpath = "//span[@class='btn-content']")
    private WebElement searchKosDropdownList;

    @FindBy(xpath = "//*[@class='bs-searchbox']/input")
    private WebElement inputKosTextBox;

    @FindBy(xpath = "//li[@class='bs-searchbox']/following-sibling::li[1]")
    private WebElement clickKosName;

    @FindBy(xpath = "(//a[@class='kos-card__show'])[1]")
    private WebElement firstSeeKosButton;

    @FindBy(css = ".statistic__choice")
    private WebElement statisticChoiceSelection;

    @FindBy(xpath = "//div[@class='detail-kos__data-box' and contains(text(), 'Chat')]")
    private WebElement chatButton;

    @FindBy(xpath = "//div[@class='detail-kos__data-box' and contains(text(), 'Review')]")
    private WebElement reviewButton;

    @FindBy(css = ".owner-kos-list > div:nth-of-type(1) button")
    private WebElement deleteKosButton;

    @FindBy(xpath = "//button[@class='swal2-cancel swal2-styled']")
    private WebElement cancelDeleteKosButton;



    /**
     * Check is specific kost list available
     * @param listNumber input with number 1-3
     * @return true is kost present, otherwise false
     */
    private boolean isKostListAvailable(String listNumber) {
        String kostListEl = ".kos-card:nth-child("+ listNumber +")";
        return selenium.waitInCaseElementVisible(driver.findElement(By.cssSelector(kostListEl)), 2) != null;
    }

    /**
     * Click on search your kost here / cari kos anda disini
     * @throws InterruptedException
     */
    private void clickOnSearchYourKostHere() throws InterruptedException {
        selenium.clickOn(searchYourKostHereDropDown);
    }

    /**
     * Set kost name to be selected
     * @param kostName input with kost name e.g "Kost Mama Kamu"
     */
    private void setSearchKostName(String kostName) {
        selenium.enterText(searchYourKostHereInput, kostName, false);
    }

    /**
     * Search kost on search your kost here and select matching result
     * @param kostName input with kost name e.g "Kost Mama Kamu"
     * @throws InterruptedException
     */
    private void choseKostFromSearchKost(String kostName) throws InterruptedException {
        clickOnSearchYourKostHere();
        setSearchKostName(kostName);
        try {
            selenium.clickOn(searchYourKostResult);
        }
        catch (Exception e) {
            selenium.clickOn(searchYourKostResult);
        }
        int i = 0;
        while(!isKostListAvailable("1")) {
            selenium.hardWait(2);
            if(isKostListAvailable("1")) {
                break;
            }
            else if(i == 10) {
                break;
            }
            i++;
        }
    }

    /**
     * Click on see complete / lihat selengkapnya base on kost name
     * @param listNumber input with list number 1-3
     * @throws InterruptedException
     */
    private void clickOnSeeCompleteOnKostList(String listNumber) throws InterruptedException {
        String seeComplete = ".kos-card:nth-child("+ listNumber +") .kos-card__detail";
        WebElement e = driver.findElement(By.cssSelector(seeComplete));
        selenium.clickOn(e);
    }

    /**
     * Click on update room / update kamar based on kost name
     * @param listNumber input with list number 1-3
     */
    public void clickUpdateRoomKostList(String listNumber) throws InterruptedException {
        String updateRoom = ".detail-kos__data-box:nth-child("+ listNumber +") .detail-kos__update-price";
        selenium.clickOn(driver.findElement(By.cssSelector(updateRoom)));
        selenium.hardWait(3);
    }

    /**
     * Navigate to update room page
     * @param kostName input with your kost name e.g "Kost Andalan"
     * @throws InterruptedException
     */
    public void navigateToUpdateRoomPage(String kostName, String listNumber) throws InterruptedException {
        choseKostFromSearchKost(kostName);
        clickOnSeeCompleteOnKostList(listNumber);
        clickUpdateRoomKostList(listNumber);
    }

    /**
     * Click on update price in kos detail
     * @throws InterruptedException
     */
    public void clickUpdatePrice() throws InterruptedException {
        selenium.clickOn(updatePriceButton);
        selenium.hardWait(3);
    }

    /**
     * Navigate to update room page
     * @param kostName input with your kost name e.g "Kost Andalan"
     * @throws InterruptedException
     */
    public void openSelectedKostListDetail(String kostName, String listNumber) throws InterruptedException {
        choseKostFromSearchKost(kostName);
        selenium.hardWait(5);
        clickOnSeeCompleteOnKostList(listNumber);
    }

    /**
     * Get available room amount
     * @return int data type 0 to max number of room
     */
    public int getAvailableRoomNumberText() {
        return Integer.parseInt(selenium.getText(txtRoomAvailableAmount));
    }

    /**
     * Get first kos name in kos list
     * @return string kos name
     */
    public String getFirstKosName() throws InterruptedException {
        selenium.waitTillElementIsVisible(firstKosNameLabel);
        selenium.hardWait(5);
        return selenium.getText(firstKosNameLabel);
    }
    /**
     * Get first location kos name in kos list
     * @return string kos name
     */
    public String getFirstKosNameLocation() throws InterruptedException {
        selenium.waitTillElementIsVisible(firstKosNameLabelLocation);
        selenium.hardWait(5);
        return selenium.getText(firstKosNameLabelLocation);
    }

    /**
     * Get first kos status in kos list
     * @return string kos status
     */
    public String getFirstKosStatus() {
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(kosStatusLabel);
        return selenium.getText(elements.get(0));
    }

    /**
     *  kos status
     */
    public boolean getFirstKosStatus2() {
        return selenium.waitInCaseElementVisible(kosStatusLabel, 5) != null;
    }



    /**
     * Get first kos address in kos list
     * @return string kos address
     */
    public String getFirstKosAddress() {
        return selenium.getText(firstKosAddressLabel);
    }

    /**
     * Get first kos type in kos list
     * @return string kos type
     */
    public String getFirstKosType() {
        return selenium.getText(firstKosTypeLabel);
    }
    /**
     *  kos status kos type
     */
    public boolean getFirstKosType2() {
        return selenium.waitInCaseElementVisible(firstKosTypeLabel, 5) != null;
    }
    /**
     * Wait for 5 second to check pop up is visible or not
     * @return true if pop up visible, otherwise false
     */
    private boolean isPopUpVisible() {
        try {
            selenium.waitTillElementIsVisible(buttonClosePopUp, 5);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Close pop up if available
     * @throws InterruptedException
     */
    public void dismissActivePopUp() throws InterruptedException {
        int i = 0;
        while (isPopUpVisible()) {
            selenium.clickOn(buttonClosePopUp);
            if(i == 3) {
                break;
            }
            i++;
        }
    }

    /**
     * Get first kos image src
     * @return string image link src
     */
    public String getFirstKosImageSrc() {
        return selenium.getElementAttributeValue(firstKosImage, "src");
    }

    /**
     * Get first kos rejected message
     * @return string rejected message
     */
    public String getFirstKosRejectMsg() {
        return selenium.getText(firstRejectedInfoText);
    }

    /**
     * Click update kos button in kos list
     * @throws InterruptedException
     */
    public void clickUpdateKosButton() throws InterruptedException {
        selenium.clickOn(updateKosButton);
    }

    /**
     * Click later button in new mamikos policy pop up if appear
     * @throws InterruptedException
     */
    public void clickLaterInPopUp() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(laterBtnInPopUpNewPolicy, 3) != null) {
            selenium.clickOn(laterBtnInPopUpNewPolicy);
        }
    }

    /**
     * Click on complete kos data button in kos list
     * @throws InterruptedException
     */
    public void clickCompleteKosData() throws InterruptedException {
        selenium.clickOn(completeKosDataButton);
        selenium.hardWait(3);
    }

    /**
     * Click on "Cari Kos Anda Disini"
     * @throws InterruptedException
     */
    public void clickOnSearchKosTextBox() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(searchKosDropdownList);
        selenium.hardWait(2);
    }

    /**
     * Enter Text in search bar
     * @param kosName is text we want to search
     */
    public void inputKosName(String kosName) throws InterruptedException {
        selenium.enterText(inputKosTextBox, kosName, false);
    }

    /**
     * Click on kos name
     * @throws InterruptedException
     */
    public void clickKosName() throws InterruptedException {
        selenium.clickOn(clickKosName);
        selenium.hardWait(5);
    }

    /**
     * Click on see kos button in first kos list
     * @throws InterruptedException
     */
    public void clickFirstSeeKos() throws InterruptedException {
        selenium.clickOn(firstSeeKosButton);
    }

    /**
     * Get selected statistic
     * @return String selected statistic
     */
    public String getSelectedStatistic() {
        return selenium.getSelectedDropdownValue(statisticChoiceSelection);
    }

    /**
     * Get all statistic options
     * @return List of Strings statistic options
     */
    public List<String> getAllStatisticOptions() {
        return selenium.getAllDropdownValues(statisticChoiceSelection);
    }

    /**
     * Click on chat button in kos list
     * @throws InterruptedException
     */
    public void clickChat() throws InterruptedException {
        selenium.clickOn(chatButton);
    }

    /**
     * Click on review button in kos list
     * @throws InterruptedException
     */
    public void clickReview() throws InterruptedException {
        selenium.clickOn(reviewButton);
    }
    /**
     * Click on delete button in kos list
     * @throws InterruptedException
     */
    public void clickDeleteKos() throws InterruptedException {
        selenium.clickOn(deleteKosButton);
    }

    /**
     * Click on cancel delete button in kos list
     * @throws InterruptedException
     */
    public void clickCancelDeleteKost() throws InterruptedException{
        selenium.hardWait(3);
        selenium.clickOn(cancelDeleteKosButton);
    }

}

