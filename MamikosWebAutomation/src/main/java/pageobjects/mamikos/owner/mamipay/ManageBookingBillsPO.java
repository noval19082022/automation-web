package pageobjects.mamikos.owner.mamipay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ManageBookingBillsPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ManageBookingBillsPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "a[href='/ownerpage/add']")
    private WebElement buttonAddProp;

    @FindBy(xpath = "//button[@class='swal2-confirm swal2-styled']")
    private WebElement errorPopupOkButton;

    @FindBy(className = "button-active-booking")
    private WebElement buttonActivateBooking;

    @FindBy(xpath = "//button[contains(text(), 'Daftar Sekarang')]")
    private WebElement buttonRequestActivateBooking;

    @FindBy(className = "is-flex-button")
    private WebElement buttonAddOccupantData;


    /**
     * Is Title on Slider One Exist?
     * @return true
     */
    public Boolean isTitleSliderOneExist(String title){
        return selenium.waitInCaseElementVisible(By.xpath("//h1[contains(text(), '"+title+"')]"), 5) != null;
    }

    /**
     * Is Content on Slider One Exist?
     * @return true
     */
    public Boolean isContentSliderOneExist(String content){
        return selenium.waitInCaseElementVisible(By.xpath("//p[contains(text(), '"+content+"')]"), 5) != null;
    }

    public String getContentSliderOne(String content){
        return selenium.getText(By.xpath("//p[contains(text(), '"+content+"')]"));
    }

    /**
     * Get Title on Slider One Text
     * @return Title Slider One Text
     */
    public String getTitleSliderOne(String title){
        return selenium.getText(By.xpath("//h1[contains(text(), '"+title+"')]"));
    }

    /**
     * Is Add Property Button Exist?
     * @return true
     */
    public Boolean isAddPropetyButtonExist(){
        selenium.pageScrollInView(buttonAddProp); //scroll to button Add Property
        return selenium.waitInCaseElementVisible(buttonAddProp, 3) != null;
    }

    /**
     * Get Add Property Button
     */
    public Boolean getAddPropertyButton(){
        return selenium.waitInCaseElementVisible(buttonAddProp, 5) != null;
    }

    /**
     * Is Button Activate Booking Exist?
     * @return true
     */
    public Boolean isButtonActivateBookingExist() throws InterruptedException {
        selenium.hardWait(3);
        selenium.pageScrollInView(buttonActivateBooking); //scroll to button activate booking
        return selenium.waitInCaseElementVisible(buttonActivateBooking, 3) != null;
    }

    /**
     * Get Button Activate Booking Text
     * @return Button Activate Booking Text
     */
    public String getButtonActivateBooking(){
        selenium.pageScrollInView(buttonActivateBooking); //scroll to button activate booking
        return selenium.getText(buttonActivateBooking);
    }

    /**
     * Is Button Request Activate Booking Exist?
     * @return true
     */
    public Boolean isButtonRequestActivateBookingExist(){
        selenium.pageScrollInView(buttonRequestActivateBooking);
        return selenium.waitInCaseElementVisible(buttonRequestActivateBooking, 3) != null;
    }

    /**
     * Get Button Request Active Booking Text
     * @return Button Request Active Booking Text
     */
    public String getButtonRequestActivateBookingExist(){
        selenium.pageScrollInView(buttonRequestActivateBooking);
        return selenium.getText(buttonRequestActivateBooking);
    }

    /**
     * Click on Ok button on Popup error
     * @throws InterruptedException
     */
    public void clickOnPopupErrorOkButton() throws InterruptedException {
        selenium.clickOn(errorPopupOkButton);
    }

    /**
     * Is Button Add Occupant Data Exist?
     * @return true
     */
    public Boolean isAddOccupantDataButtonExist(){
        return selenium.waitInCaseElementVisible(buttonAddOccupantData, 3) != null;
    }

    /**
     * Get Button Add Occupant Data Text
     * @return Button Add Occupant Data Text
     */
    public String getAddOccupantDataButton(){
        return selenium.getText(buttonAddOccupantData);
    }
}
