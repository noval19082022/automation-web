package pageobjects.mamikos.owner.mamipay;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import utilities.Constants;
import utilities.SeleniumHelpers;

public class ActivateMamipayPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ActivateMamipayPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(className = "btn-trial-close")
    private WebElement freePremiumPopup;

    @FindBy(className = "next-link")
    private WebElement nextButton;

    @FindBy(className = "skip-link")
    private WebElement skipButton;

    @FindBy(className = "swiper-slide")
    private List<WebElement> swiperSlide;

    @FindBy(css = "[placeholder='Masukkan nama lengkap']")
    private WebElement inputName;

    @FindBy(css = "[placeholder='Masukkan nomor rekening Anda']")
    private WebElement inputBankAccountNumber;

    @FindBy(css = "[placeholder='Masukkan nama bank']")
    private WebElement inputBankName;

    @FindBy(css = "[placeholder='Masukkan nama pemilik rekening']")
    private WebElement inputBankOwnerName;

    @FindBy(className = "mami-checkbox--toggle-icon")
    private WebElement checkBox;

    @FindBy(className = "btn-primary")
    private WebElement btnNext;

    @FindBy(xpath = "//*[contains(text(), 'Lanjutkan')]/parent::button")
    private WebElement btnNextBBK;

    @FindBy(css = ".personal-data__content > div:nth-of-type(1) .bg-c-field__message")
    private WebElement errorMessageFullname;

    @FindBy(xpath = "//div[@class='notices is-bottom']")
    private WebElement toastMessage;

    @FindBy(xpath = "//button[@class='button personal-data__submit is-primary is-fullwidth']")
    private WebElement buttonNextMamipay;

    @FindBy(xpath = "//div[@class='control is-clearfix']/input[@class='input']")
    private WebElement bankNameMamipay;

    @FindBy(xpath = "//a[contains(.,'Bank Mandiri')]")
    private WebElement listBankNameMandiri;

    @FindBy(xpath = "//button[contains(@class, 'button personal-data__') and not(@disabled='disabled')]")
    private WebElement submitDataMamipay;

    @FindBy(css = ".personal-data__content > div:nth-of-type(2) .bg-c-field__message")
    private WebElement errorMessageBankAccount;

    @FindBy(css = ".personal-data__content > div:nth-of-type(4) .bg-c-field__message")
    private WebElement errorMessageBankAccountName;

    @FindBy(css = ".media-content")
    private WebElement infoMamipayForm;

    @FindBy(css = ".control-label")
    private WebElement agreementText;

    @FindBy(css = ".check")
    private WebElement termAndConsCheckbox;

    @FindBy(linkText = "Syarat dan Ketentuan" )
    private WebElement termAndConsMamipay;

    @FindBy(css = ".bg-c-button")
    private WebElement backButtonActivationSent;


    /**
     * Click on Free Premium Popup
     * @throws InterruptedException
     */
    public void clickOnPremiumPopup() throws InterruptedException {
        selenium.clickOn(freePremiumPopup);
    }

    /**
     * Click on next button 6 times based on Activate Mamipay Screen
     * @throws InterruptedException
     */
    public void clickOnNextButton() throws InterruptedException {
        selenium.pageScrollInView(nextButton);
        selenium.waitInCaseElementClickable(nextButton, 5);
        for (int i = 0; i < swiperSlide.size(); i++) {
            selenium.clickOn(nextButton);
        }
    }

    /**
     * Click on 'Skip Button'
     * @throws InterruptedException
     */
    public void clickOnSkipButton() throws InterruptedException {
        selenium.clickOn(skipButton);
    }

    /**
     * Fill out Activate Mamipay Form
     * @param bankAccountNumber bank account number
     * @param bankOwnerName     bank owner name
     * @throws InterruptedException
     */
    public void fillActivateMamipayForm(String bankAccountNumber, String bankOwnerName) throws InterruptedException {
        selenium.enterText(inputBankAccountNumber, bankAccountNumber, false);
        selenium.clickOn(inputBankName);
        selenium.enterText(inputBankOwnerName, bankOwnerName, false);
        selenium.pageScrollInView(checkBox);
        selenium.clickOn(checkBox);
        selenium.click(btnNext);
    }

    /**
     * Fill out Bank Account Number Form
     * @param bankAccountNumber bank account number
     * @throws InterruptedException
     */
    public void fillBankAccountNumberForm(String bankAccountNumber) throws InterruptedException {
        selenium.clickOn(inputBankAccountNumber);
        selenium.enterText(inputBankAccountNumber, bankAccountNumber, true);

    }

    /**
     * Fill out Bank Account Name Form
     * @param bankAccountName bank account name
     * @throws InterruptedException
     */
    public void fillBankAccountNameForm(String bankAccountName) throws InterruptedException {
        selenium.pageScrollInView(inputBankOwnerName);
        selenium.clickOn(inputBankOwnerName);
        selenium.enterText(inputBankOwnerName, bankAccountName, true);
    }

    /**
     * Is Activate Mamipay Form Present?
     * @return true or false
     */
    public boolean isActivateMamipayFormPresent() {
        return selenium.waitInCaseElementVisible(inputBankAccountNumber, 3) != null && selenium.waitInCaseElementVisible(inputBankName, 3) != null && selenium.waitInCaseElementVisible(inputBankAccountNumber, 3) != null;
    }

    /**
     * Get Form Error Messages
     * @param message message
     * @return message e.g Wajib diisi
     */
    public String getFormErrorMessages(String message) {
        return selenium.getText(By.xpath("//p[contains(text(), '" + message + "')]"));
    }

    /**
     * Fill out Full Name
     * @param fullName
     */
    public void fillInputNameForm(String fullName) {
        selenium.enterText(inputName, fullName, true);
    }


    /**
     * Click on Button Next BBK
     * @throws InterruptedException
     */
    public void clickOnbuttonBBK() throws InterruptedException {
        selenium.waitInCaseElementVisible(btnNextBBK, 10);
        selenium.hardWait(10);
        selenium.clickOn(btnNextBBK);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(3);
        selenium.waitInCaseElementClickable(inputBankOwnerName, 5);
    }

    /**
     * Click on the bank account
     * @throws InterruptedException
     */
    public void clickOnBankAccount() throws InterruptedException {
        selenium.clickOn(inputBankAccountNumber);
    }

    /**
     * Message error when input invalid name at activate mamipay
     * @return error message invalid input name
     */
    public String getErrorMessageInputName() throws InterruptedException {
        selenium.waitInCaseElementVisible(errorMessageFullname, 5);
        return selenium.getText(errorMessageFullname);
    }

    /**
     * Clear text Full Name
     * @throws InterruptedException
     */
    public void clearTextfullName() throws InterruptedException {
        selenium.clickOn(inputName);
        inputName.clear();
    }

    /**
     * Toast message error when input invalid name at activate mamipay
     * @return toast error message invalid input name
     */
    public String geToastMessageInputName() throws InterruptedException {
        selenium.waitInCaseElementVisible(toastMessage, 5);
        return selenium.getText(toastMessage);
    }


    /**
     * Click on 'Lanjutkan'
     * @throws InterruptedException
     */
    public void clickOnNextButtonMamipay() throws InterruptedException {
        selenium.clickOn(buttonNextMamipay);
    }

    /**
     * Fill out Bank Name
     * @param bankName
     * @throws InterruptedException
     */
    public void fillInputBankName(String bankName) throws InterruptedException {
        selenium.clickOn(bankNameMamipay);
        selenium.enterText(bankNameMamipay, bankName, true);
        WebElement element = driver.findElement(By.xpath("//a[contains(.,'" + bankName + "')]"));
        selenium.clickOn(element);
    }

    /**
     * Is Activate button submit active?
     * @return true or false
     */
    public boolean isActivateMamipayButtonSubmitPresent() {
        return selenium.waitInCaseElementVisible(submitDataMamipay, 3) != null;
    }

    /**
     * Message error when input invalid Bank Account at activate mamipay
     * @return error message invalid Bank Account
     */
    public String getErrorMessageInputBankAccount() throws InterruptedException {
        selenium.waitInCaseElementVisible(errorMessageBankAccount, 5);
        return selenium.getText(errorMessageBankAccount);
    }

    /**
     * Message error when input invalid Bank Account Nameat activate mamipay
     * @return error message invalid Bank Account Name
     */
    public String getErrorMessageInputBankAccountName() throws InterruptedException {
        selenium.waitInCaseElementVisible(errorMessageBankAccountName, 5);
        return selenium.getText(errorMessageBankAccountName);
    }

    /**
     * Get Info text in Mamipay Form
     * @return message info text
     */
    public String getInfoMamipayForm() {
        return selenium.getText(infoMamipayForm);
    }

    /**
     * Get Agreement text in Mamipay Form
     * @return agreement text without newline
     */
    public String getAgreementText() {
        return selenium.getText(agreementText).replace("\n", "");
    }

    /**
     * Click on the term and conditions checkbox
     * @throws InterruptedException
     */
    public void clickTermsAndConsCheckbox() throws InterruptedException {
        selenium.waitInCaseElementVisible(termAndConsCheckbox, 7);
        selenium.clickOn(termAndConsCheckbox);
    }

    /**
     * Click on 'Syarat dan Ketentuan' Link in mamipay form
     * @throws InterruptedException
     */
    public void clickOnTermAndConditionsMamipay() throws InterruptedException {
        selenium.clickOn(termAndConsMamipay);
        selenium.switchToWindow(0);
    }

    /**
     * Click on Submit mamipay datd
     * @throws InterruptedException
     */
    public void clickSubmitButtonMamipay() throws InterruptedException {
        selenium.clickOn(submitDataMamipay);
    }

    /**
     * Get Full Name inputted text in Mamipay Form
     * @return String Full Name inputted text
     */
    public String getInputTextFullName() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getInputText(inputName);
    }

    /**
     * Get Bank account number inputted text in Mamipay Form
     * @return String Bank account number inputted text
     */
    public String getInputTextBankAcc() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getInputText(inputBankAccountNumber);
    }

    /**
     * Get Bank owner name inputted text in Mamipay Form
     * @return String Bank owner name inputted text
     */
    public String getInputTextBankOwnerName() {
        return selenium.getInputText(inputBankOwnerName);
    }

    /**
     * Get Bank name inputted text in Mamipay Form
     * @return String Bank name inputted text
     */
    public String getInputTextBankName() {
        return selenium.getInputText(inputBankName);
    }

    /**
     * Click on Back button in Activation request sent
     * @throws InterruptedException
     */
    public void clickBackButtonActivationRequest() throws InterruptedException {
        selenium.clickOn(backButtonActivationSent);
    }
}
