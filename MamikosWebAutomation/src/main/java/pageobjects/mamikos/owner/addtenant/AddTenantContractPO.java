package pageobjects.mamikos.owner.addtenant;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class AddTenantContractPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public AddTenantContractPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */
    @FindBy(xpath = "//*[@type='tel']")
    private WebElement phoneNumber;

    @FindBy(xpath = "//*[@class='mami-field-dropdown room-allotment-select field-item']/input")
    private WebElement selectRoomNumberDropdown;

    @FindBy(css = ".room-option:first-child input")
    private WebElement radioFirstRoom;

    @FindBy(xpath = "//button[contains(.,'Terapkan')]")
    private WebElement applyButton;

    @FindBy(xpath = "//button[contains(.,'Tambah Penyewa')]")
    private WebElement addTenantButton;

    @FindBy(xpath = "//div[@id='addTenantInputName']//input")
    private WebElement tenantNameField;

    @FindBy(xpath = "//button[contains(.,'Selanjutnya')]")
    private WebElement nextButton;

    @FindBy(css = "div.add-tenant-select-options .btn-content")
    private WebElement rentCountDropdown;

    @FindBy(xpath = "//input[@class='form-control']")
    private WebElement rentPriceField;

    @FindBy(xpath = "//span[.='Pilih durasi sewa']")
    private WebElement rentDurationDropdown;

    @FindBy(xpath = "//input[@class='form-control add-tenant-due-date__input']")
    private WebElement dueDateField;

    @FindBy(xpath = "//button[text()='Simpan']")
    private WebElement saveButton;

    @FindBy(xpath = "//button[contains(.,'Ya, Simpan Data')]")
    private WebElement saveDataButton;

    @FindBy(xpath = "//button[contains(text(),\"Lanjut\")]")
    private WebElement continueButton;

    @FindBy(css = ".bg-c-button--primary")
    private WebElement startButton;

    @FindBy(xpath = "//span[contains(.,'Saya yang menambah kontrak')]")
    private WebElement addContractButton;

    private By seeDetailTenantButton = By.xpath("//button[contains(.,'Lihat Detail Penyewa')]");

    private By loadingAnimation = By.cssSelector(".swal2-buttonswrapper.swal2-loading");

    private By addTenantTiltle = By.xpath("//h2[contains(.,'Berhasil Menyimpan Data!')]");

    private By addTenantDescription = By.xpath("//p[contains(.,'Setelah ini, kami akan melakukan pengecekan data kepada penyewa.')]");

    private By addOtherTenantButton = By.xpath("//button[contains(.,'Tambah Penyewa Lain')]");

    @FindBy(css = ".profile-tenant-title .visible-lg")
    private WebElement txtFormTitle;

    @FindBy(css = ".add-tenant-step .field-item:nth-child(1) input[readonly]")
    private WebElement txtKostNameValue;

    @FindBy(css = ".add-tenant-step .field-item:nth-child(3) input[readonly]")
    private WebElement txtFullRoomName;

    @FindBy(css = ".add-tenant-kost-full-modal.fade.in ")
    private WebElement popUpFullRoomRestriction;

    @FindBy(css = ".add-tenant-kost-full-modal.fade.in .add-tenant-modal__title")
    private WebElement popUpFullRoomTitle;

    @FindBy(css = ".fade.in .add-tenant-modal__title")
    private WebElement popUpAddTenantTitleText;

    @FindBy(css = ".fade.in .add-tenant-modal__description")
    private WebElement popUpAddTenantDescriptionText;

    @FindBy(css = ".add-tenant-kost-full-modal.fade.in .add-tenant-modal__description")
    private WebElement popUpFullRoomDescription;

    @FindBy(css = ".add-tenant-kost-full-modal.fade.in .add-tenant-modal__footer button.btn-primary")
    private WebElement popUpFullRoomResctrictionChangeRoomsDataButton;

    @FindBy(css = ".fade.in img[alt='image gender error']")
    private WebElement imgPopUpGenderRestriction;

    @FindBy(xpath = "(//*[@class=('mami-radio')])[1]")
    private WebElement selectGenderRadioButton;

    @FindBy(xpath = "//b[contains(.,'kost flores')]")
    private WebElement scrollKostList;

    @FindBy(xpath = "//b[contains(.,'Kost Zahra Papua')]")
    private WebElement scrollKostListFemale;

    /**
     * check kost true or false
     */
    private boolean isKostListVisible(){
        return selenium.waitInCaseElementVisible(scrollKostList, 2) != null;
    }

    /**
     * check kost true or false for kost female
     */
    private boolean isKostListVisibleFemale(){
        return selenium.waitInCaseElementVisible(scrollKostListFemale, 2) != null;
    }

    /**
     * scroll on kost list page until get kost name list
     */
    public void scrollToKostListTillVisible() throws InterruptedException {
        for (int i=0; i<=10; i++){
            selenium.pageScrollUsingCoordinate(0, 2000);
            selenium.hardWait(5);
            if (isKostListVisible()){
                break;
            }
            else if (isKostListVisibleFemale()){
                break;
            }
        }
    }

    /**
     * Select kost name
     * @param kostName
     * @throws InterruptedException
     */
    public void selectKost(String kostName) throws InterruptedException {
        selenium.hardWait(8);
        scrollToKostListTillVisible();
        WebElement element = driver.findElement(By.xpath("//b[contains(.,'"+kostName+"')]"));
        selenium.javascriptClickOn(element);
        if (selenium.waitInCaseElementVisible(loadingAnimation, 4) != null) {
            selenium.waitTillElementIsNotVisible(loadingAnimation);
        }
    }

    /**
     * Set tenant phone number
     * @param number
     */
    public void setTenantPhoneNumber(String number){
        selenium.enterText(phoneNumber,number,true);
    }

    public void selectRoomNumber(String roomNumber) throws InterruptedException {
        selenium.clickOn(selectRoomNumberDropdown);
        selenium.pageScrollInView(By.xpath("//span[.='"+roomNumber+"']"));
        selenium.hardWait(1);
        selenium.clickOn(By.xpath("//span[.='"+roomNumber+"']"));
        selenium.hardWait(1);
        selenium.clickOn(applyButton);
    }

    /**
     * Click on Add Tenant Button
     * @throws InterruptedException
     */
    public void clickAddTenantButton() throws InterruptedException {
        selenium.javascriptClickOn(addTenantButton);
    }

    /**
     * Set tenant name
     * @param tenantName
     */
    public void setTenantName(String tenantName){
        selenium.enterText(tenantNameField,tenantName,true);
    }

    /**
     * Choose tenant gender
     * @param gender
     * @throws InterruptedException
     */
    public void chooseTenantGender(String gender) throws InterruptedException {
        selenium.clickOn(By.xpath("//div[@class='gender-options']/div[contains(.,'"+gender+"')]"));
    }

    /**
     * Click on next button
     * @throws InterruptedException
     */
    public void clickONextButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(nextButton);
        selenium.waitTillElementIsNotVisible(loadingAnimation);
    }

    /**
     * Select rent count
     * @param rentCount
     * @throws InterruptedException
     */
    public void selectRentCount(String rentCount) throws InterruptedException {
        selenium.clickOn(rentCountDropdown);
        selenium.clickOn(By.xpath("//span[contains(.,'"+rentCount+"')]"));
        selenium.hardWait(2);
    }

    /**
     * Set rent price
     * @param rentPrice
     */
    public void setRentPrice(String rentPrice) {
        selenium.enterTextUsingJavascriptExecute(rentPriceField,rentPrice,true);
    }

    /**
     * Select rent duration
     * @param rentDuration
     * @throws InterruptedException
     */
    public void selectRentDuration(String rentDuration) throws InterruptedException {
        selenium.waitInCaseElementVisible(rentDurationDropdown,5);
        selenium.clickOn(rentDurationDropdown);
        selenium.clickOn(By.xpath("//span[contains(.,'"+rentDuration+"')]"));
    }

    /**
     * Select due date is today
     * @param dueDate
     * @throws InterruptedException
     */
    public void selectDueDate(String dueDate) throws InterruptedException {
        selenium.clickOn(dueDateField);
        selenium.clickOn(By.xpath("//span[text()='"+dueDate+"']"));
    }

    /**
     * Click on save button
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.clickOn(saveButton);
        selenium.clickOn(saveDataButton);
        selenium.waitTillElementIsNotVisible(loadingAnimation);
    }

    /**
     * Verify Pop Up Title Add Tenant is Present
     *
     * @return  boolean
     */
    public boolean verifyPopUpTitleAddTenant() {
        return selenium.waitInCaseElementPresent(addTenantTiltle,2)!=null;
    }

    /**
     * Verify Pop Up Description Add Tenant is Present
     *
     * @return boolean
     */
    public boolean verifyPopUpDescriptionAddTenant(){
        return selenium.waitInCaseElementPresent(addTenantDescription, 2)!=null;
    }

    /**
     * Verify See Detail Tenant Button is Present
     *
     * @return boolean
     */
    public boolean verifySeeTenantDetailButton(){
        return selenium.waitInCaseElementPresent(seeDetailTenantButton, 2)!=null;
    }

    /**
     * Verify Add Other Tenant Button is Present
     *
     * @return boolean
     */
    public boolean verifyAddOtherTenantButton(){
        return selenium.waitInCaseElementPresent(addOtherTenantButton, 2)!=null;
    }

    /**
     * Click on continue button
     *
     * @throws InterruptedException
     */
    public void clickOnContinueButton() throws InterruptedException {
        selenium.clickOn(continueButton);
    }

    /**
     * Click on start button
     *
     * @throws InterruptedException
     */
    public void clickOnStartButton() throws InterruptedException {
        selenium.clickOn(startButton);
    }

    /**
     * Click on I Add Contract Button
     *
     * @throws InterruptedException
     */
    public void clickOnAddContractButton() throws InterruptedException {
        selenium.clickOn(addContractButton);
    }

    /**
     * Get current title of add tenant form
     * @return String data type e.g "Masukkan Informasi Penyewa"
     */
    public String getFormTitle() throws InterruptedException{
        selenium.hardWait(7);
        return selenium.getText(txtFormTitle);
    }

    /**
     * Get input text by take in the value of kost name input element
     * @return string data type e.g "kost upras lah"
     */
    public String getSelectedKostName() {
        return selenium.getElementAttributeValue(txtKostNameValue, "value");
    }

    /**
     * Get full room name in input box
     * @return string data type e.g "Kamar 2 Lantai 3"
     */
    public String getFullRoomName() {
        return selenium.getElementAttributeValue(txtFullRoomName, "value");
    }

    /**
     * Check if full room restriction pop up visible
     * @return visible true, otherwise false
     */
    public boolean isFullRoomPopUp() {
        return selenium.waitInCaseElementVisible(popUpFullRoomRestriction, 20) != null;
    }

    /**
     * Get pop up full room title text
     * @return string data "Seluruh Kamar Kos Sudah Terisi"
     */
    public String getFullRoomRestrictionTitle() {
        return selenium.getText(popUpFullRoomTitle);
    }

    /**
     * Get pop up full room description text
     * @return string data type "Saat ini sistem kami mencatat seluruh kamar Kos Anda sudah penuh. Anda bisa mengubahnya di sini."
     */
    public String getFullRoomRestrictionDescription() {
        return selenium.getText(popUpFullRoomDescription);
    }

    /**
     * Click on change room data button on full room pop up restriction
     */
    public void clicsOnChangeRoomDataButton() {
        selenium.javascriptClickOn(popUpFullRoomResctrictionChangeRoomsDataButton);
    }

    /**
     * Select first available room
     * @throws InterruptedException
     */
    public void selectFirstAvailableRoom() throws InterruptedException {
        selenium.clickOn(selectRoomNumberDropdown);
        selenium.javascriptClickOn(radioFirstRoom);
        selenium.hardWait(1);
        selenium.javascriptClickOn(applyButton);
    }

    /**
     * Check if image pop up gender restriction is visible
     * @return visible true, otherwise false
     */
    public boolean isDiferrentGenderRestrictedPopUp() {
        return selenium.waitInCaseElementVisible(imgPopUpGenderRestriction, 20) != null;
    }

    /**
     * Get pop up title text
     * @return string data type
     */
    public String getDifferentGenderPopUpTitleText() {
        return selenium.getText(popUpAddTenantTitleText);
    }

    /**
     * Get pop up description text
     * @return string data type
     */
    public String getDifferentGenderPopUpDescriptionText() {
        return selenium.getText(popUpAddTenantDescriptionText);
    }

    /**
     * Select gender male
     * @throws InterruptedException
     */
    public void selectGenderMale() throws InterruptedException {
        selenium.clickOn(selectGenderRadioButton);
        selenium.hardWait(2);
    }
}
