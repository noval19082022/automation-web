package pageobjects.mamikos.owner.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BookingConfirmContractPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingConfirmContractPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "inputCostDeposit")
    private WebElement depositFeeTextBox;

    @FindBy(xpath = "//*[@id='inputCostFine']")
    private WebElement penaltyFeeTextBox;

    @FindBy(xpath = "//*[@id='inputDurationFine']")
    private WebElement penaltyFeeDurationTextBox;

    @FindBy(css = "#inputStatus.form-control.input-select-custom:not([disabled])")
    private WebElement penaltyFeeDurationType;

    @FindBy(xpath = "//button[contains(text(),'Simpan')]")
    private WebElement saveButton;

    @FindBy(className = "swal2-confirm")
    private WebElement confirmationPopupOkButton;

    @FindBy(xpath = "(//*[@id='inputTenantName'])[1]")
    private WebElement firstAdditionalCostLabel;

    @FindBy(xpath = "(//*[@id='filterPriceMin'])[3]")
    private WebElement firstAdditionalCostValue;

    @FindBy(xpath = "//*[@class='v-switch-core']")
    private WebElement downPaymentToggleButton;

    @FindBy(xpath = "//*[@id='filterPriceMin'][@class='form-control input-custom input-error']")
    private WebElement downPaymentTextBox;

    @FindBy(xpath = "//label[text()='Batas Pelunasan']")
    private WebElement paymentDeadlineLabel;

    //label[text()='Batas Pelunasan']

    private By loadingAnimation = By.cssSelector(".swal2-buttonswrapper.swal2-loading");

    @FindBy(css = " button.swal2-confirm.swal2-styled")
    private WebElement okButtonConfirmationPopup;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--heading-2 '][contains(., 'Pengajuan Booking')]")
    private WebElement pengajuanBookingPage;

    private By OK_buttonSavedContract = By.xpath("//button[@class='swal2-confirm swal2-styled'][contains(text(), 'OK')]");

    private By iUnderstandButton = By.cssSelector(".additiona-price-modal__primary-btn");

    @FindBy(xpath = "//*[@class= 'additiona-price-modal__content']")
    private WebElement additionalPriceAlertPopUp;

    @FindBy(xpath = "//*[@class= 'additiona-price-modal__content']//*[@type= 'button']")
    private WebElement additionalPriceIUnderstandButton;

    @FindBy(css = "#inputTenantName")
    private WebElement otherPriceName;

    @FindBy(xpath = "//*[.='Biaya 1']/parent::div//*[@id='filterPriceMin']")
    private WebElement otherPriceNumber;

    @FindBy(xpath = "//*[@class='btn btn-primary additiona-price-modal__primary-btn']")
    private WebElement sayaMengertiButton;

    /**
     *  Enter Payment Details from Owner Side
     * @param depositeFeeText Deposite Fee
     * @param panaltyFeeText Panalty Fee
     * @param penaltyFeeDurationText Penalty Fee Duration
     * @param penaltyFeeDurationTypeText Penalty Duration Type
     * @throws InterruptedException
     */
    public void enterDepositePanaltyInfo(String depositeFeeText, String panaltyFeeText, String penaltyFeeDurationText, String penaltyFeeDurationTypeText) throws InterruptedException {
        selenium.enterText(depositFeeTextBox, depositeFeeText, false);
        selenium.pageScrollInView(depositFeeTextBox);
        selenium.clickOn(penaltyFeeTextBox);
        if(isTotalPriceChangeAlert()) {
            clickOnIUnderstandButton();
        }
        selenium.enterText(penaltyFeeTextBox, panaltyFeeText, false);
        selenium.enterText(penaltyFeeDurationTextBox, penaltyFeeDurationText, false);
        selenium.enterText(penaltyFeeDurationTextBox, penaltyFeeDurationText, true);
        selenium.hardWait(5);
        selenium.pageScrollInView(depositFeeTextBox);
        selenium.hardWait(5);
        selenium.selectDropdownValueByText(penaltyFeeDurationType, penaltyFeeDurationTypeText);
    }

    /**
     * Click on Save button
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.javascriptClickOn(saveButton);
        selenium.waitTillElementIsNotVisible(loadingAnimation, 60);
    }

    /**
     * Click on OK button on success confirmation popup
     * @throws InterruptedException
     */
    public void clickOnOKButton() throws InterruptedException {
      selenium.clickOn(okButtonConfirmationPopup);
      selenium.hardWait(2);
    }

    /**
     * get title at Pengajuan Booking page after confirm booking successfully
     * @return
     */
    public String getPengajuanBookingPage(){
        return selenium.getText(pengajuanBookingPage);
    }

    /**
     * Click on Saya Mengerti button
     * @throws InterruptedException
     */
    public void clickOnConfirmDownPayment() throws InterruptedException {
        selenium.javascriptClickOn(sayaMengertiButton);
    }

    /**
     * Wait OK button to be appeared and click on it
     * @throws InterruptedException
     */
    public void clickOnOkButtonRenterAddedPopUp() throws InterruptedException {
        selenium.waitInCaseElementVisible(OK_buttonSavedContract, 10);
        selenium.hardWait(2);
        selenium.clickOn(OK_buttonSavedContract);
    }

    /**
     * Enter text to deposit fee text box
     * @param  depositFee is value deposit fee
     * @throws InterruptedException
     */
    public void enterDepositFee(String depositFee) throws InterruptedException {
        selenium.hardWait(2);
        selenium.pageScrollInView(depositFeeTextBox);
        selenium.javascriptClickOn(depositFeeTextBox);
        selenium.enterText(depositFeeTextBox, depositFee, true);
    }

    /**
     * Enter text to penalty fee text box
     * @param  penaltyFee is value penalty fee
     */
    public void enterPenaltyFee(String penaltyFee) {
        selenium.pageScrollInView(penaltyFeeTextBox);
        selenium.javascriptClickOn(penaltyFeeTextBox);
        selenium.enterText(penaltyFeeTextBox, penaltyFee, true);
        selenium.pageScrollInView(penaltyFeeDurationTextBox);
        selenium.javascriptClickOn(penaltyFeeDurationTextBox);
        selenium.enterText(penaltyFeeDurationTextBox, penaltyFee, true);
    }

    /**
     * Create additional cost and enter value additional cost
     * @param  additionalCostsLabel is label additional cost
     * * @param  additionalCosts is value additional cost
     */
    public void enterAdditionalCost(String additionalCostsLabel, String additionalCosts) {
        selenium.pageScrollInView(firstAdditionalCostLabel);
        selenium.javascriptClickOn(firstAdditionalCostLabel);
        selenium.enterText(firstAdditionalCostLabel, additionalCostsLabel, true);
        selenium.javascriptClickOn(firstAdditionalCostValue);
        selenium.enterText(firstAdditionalCostValue, additionalCosts, true);
    }

    /**
     * Switch down payment toggle button and enter value down payment
     * @param  downPayment is value down payment
     */

    public void enterDownPayment(String downPayment) throws InterruptedException {
        selenium.javascriptClickOn(downPaymentToggleButton);
        selenium.clickOn(iUnderstandButton);
        selenium.enterText(downPaymentTextBox, downPayment, true);
    }

    /**
     * Check if alert pop-up present
     * @return
     */
    private boolean isTotalPriceChangeAlert() {
        return selenium.waitInCaseElementClickable(additionalPriceAlertPopUp, 2) != null;
    }

    /**
     * Click on I understand button on total price change alert pop-up
     * @throws InterruptedException
     */
    private void clickOnIUnderstandButton() throws InterruptedException {
        selenium.clickOn(additionalPriceIUnderstandButton);
    }

    /**
     * Get deposit input value
     * @return string data type e.g "Rp 250.000"
     */
    public String getDepositPrice() {
        return selenium.getElementAttributeValue(depositFeeTextBox, "value");
    }

    /**
     * Get lateness penalty price
     * @return string data type e.g "Rp 250.000"
     */
    public String getLatenessFinePrice() {
        return selenium.getElementAttributeValue(penaltyFeeTextBox, "value");
    }

    /**
     * Get lateness penalty duration and duration type
     * @return string data type
     */
    public String getLatenessFineDuration() {
        String duration = selenium.getElementAttributeValue(penaltyFeeDurationTextBox, "value");
        String durationType = selenium.getElementAttributeValue(penaltyFeeDurationType, "value");
        return duration + " " + durationType;
    }

    /**
     * Get first list of other price name
     * Only work when there is only one other price available
     * @return string data type e.g "Biaya bersihkan jendela"
     */
    public String getOtherPriceName() {
        return selenium.getElementAttributeValue(otherPriceName, "value");
    }

    /**
     * Get other price number as text
     * @return string data type
     */
    public String getOtherPricePrice() {
        return selenium.getElementAttributeValue(otherPriceNumber, "value");
    }
}
