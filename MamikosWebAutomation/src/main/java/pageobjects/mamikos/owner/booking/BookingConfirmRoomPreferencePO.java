package pageobjects.mamikos.owner.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BookingConfirmRoomPreferencePO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingConfirmRoomPreferencePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    private By secondRoomNumberDropdownList = By.xpath("//input[@name='input-room-number']");

    @FindBy(xpath = "//input[@name='input-room-number']")
    private WebElement roomNumberDropdownList;

    @FindBy(xpath = "(//*[@class='mami-radio'])[2]")
    private WebElement firstRoomAvailableRadioButton;

    @FindBy(xpath = "(//*[@class='mami-radio'])[3]")
    private WebElement secondRoomAvailableRadioButton;

    @FindBy(xpath = "//div[@class='modal-dialog modal-sm']//a[@title='Tutup']")
    private WebElement closeFilter;

    @FindBy(xpath = "//div[@class='room-left']//span[contains(text(),'Sisa 13 kamar')]")
    private WebElement firstRoomNoAvailable;

    @FindBy(xpath = "//button[contains(text(),'Lanjutkan')]")
    private WebElement continueButton;

    @FindBy(xpath = "//button[contains(.,'Terapkan')]")
    private WebElement applyButton;

    /**
     * Select first room number available
     */
    public void selectFirstRoomAvailable() throws InterruptedException {
        selenium.hardWait(20);
        if (selenium.isElementNotDisplayed(roomNumberDropdownList)) {
            selenium.javascriptClickOn(roomNumberDropdownList);
        }else {
            selenium.clickOn(secondRoomAvailableRadioButton);
        }
        selenium.hardWait(10);
        selenium.javascriptClickOn(firstRoomAvailableRadioButton);
        selenium.hardWait(10);
        selenium.isElementNotDisplayed(firstRoomAvailableRadioButton);
    }

    /**
     * Select second room number available
     */
    public void selectSecondRoomAvailable() throws InterruptedException {
        selenium.clickOn(closeFilter);
        selenium.clickOn(roomNumberDropdownList);
        selenium.clickOn(secondRoomAvailableRadioButton);
    }

    /**
     * Select first room number not available
     */
    public boolean selectFirstRoomNoAvailable() throws InterruptedException {
      return selenium.isElementDisplayed(firstRoomNoAvailable);
    }

    /**
     * Click on Next button
     * @throws InterruptedException
     */
    public void clickOnContinueButton() throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//button[contains(.,'Terapkan')]"));
        selenium.javascriptClickOn(element);
        selenium.hardWait(2);
        selenium.pageScrollInView(continueButton);
        selenium.javascriptClickOn(continueButton);
    }

    /**
     * Select room number
     * @throws InterruptedException
     */
    public void selectRoomNumber(String roomNumber) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
        selenium.waitInCaseElementVisible(roomNumberDropdownList, 5);
        selenium.javascriptClickOn(roomNumberDropdownList);
        selenium.hardWait(5);
        selenium.pageScrollInView(By.xpath("//span[.='"+roomNumber+"']"));
        selenium.hardWait(5);
        selenium.javascriptClickOn(By.xpath("//span[.='"+roomNumber+"']"));
        selenium.hardWait(5);
        selenium.clickOn(applyButton);
    }
}
