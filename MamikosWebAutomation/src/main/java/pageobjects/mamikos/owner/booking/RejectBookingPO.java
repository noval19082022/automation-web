package pageobjects.mamikos.owner.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class RejectBookingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public RejectBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//*[contains(text(), 'tidak tersedia')]/parent::*/parent::*")
    private WebElement radioRoomNotAvailable;

    @FindBy(xpath = "//*[contains(text(), 'reject test')]/parent::*/parent::*")
    private WebElement radioRejectTest;

    @FindBy(xpath = "//*[@class='reject-modal__reason-option-label']//*[contains(text(), 'Lainnya')]")
    private WebElement radioOtherReason;

    @FindBy(xpath = "//*[@class='bg-c-modal__wrapper']//*[contains(text(), 'Saya Mengerti')]")
    private WebElement btnIUnderstand;

    @FindBy(css = " div.reject-modal__tnc > div > label > span")
    private WebElement checkBoxTAndC;

    @FindBy(css = ".bg-c-link--medium")
    private WebElement buttonTAndC;

    @FindBy(css = ".reject-modal__tnc-check")
    private WebElement tAndCStatus;

    @FindBy(xpath = "//*[@class='reject-modal__actions']//button[contains(text(), 'Pilih')]")
    private WebElement btnYesReject;

    @FindBy(xpath = "//button[contains(text(), 'Selesai')]")
    private WebElement btnDone;

    @FindBy(id="bookingRequestRejectModal-other")
    private WebElement inputOtherReason;

    @FindBy(css = "#reject-reason-8")
    private WebElement bssReasonRadioButton;

    @FindBy(css="[for='terminateReasonOther'] > .bg-c-radio__icon")
    private WebElement radioPaidOtherReason;

    @FindBy(css="#otherReasonDesc")
    private WebElement inputPaidOtherReason;

    @FindBy(css = "#checkoutDateValue")
    private WebElement dateBoxOtherReason;

    @FindBy (xpath ="//span[@class='cell day']")
    private WebElement datePickToday;

    @FindBy (xpath = "//button[.='Pilih']")
    private WebElement pilihButton;

    @FindBy (css = ".bg-c-button--primary")
    private WebElement konfirmasiButton;

    @FindBy (xpath="//*[@class='bg-c-modal__action-closable']//*[@class='bg-c-icon bg-c-icon--md']")
    private WebElement closeButtonRejectPopup;

    @FindBy(xpath = "//button[contains(.,'Pilih')]")
    private WebElement pilihRejctButton;

    @FindBy(id = "bookingRejectTnC")
    private WebElement tAndCCheckBox;

    @FindBy(xpath ="//*[@class='bg-c-modal__body-title']")
    private WebElement popupConfirmReject;

    @FindBy(xpath ="//*[@class='bg-c-modal__footer-CTA']//button[contains(.,'Tolak')]")
    private WebElement rejectOnCta;

    /**
     * Reject booking with full room reason
     * @throws InterruptedException
     */
    public void rejectBookingFullRoom() throws InterruptedException {
        selenium.hardWait(5);
        selenium.javascriptClickOn(radioRoomNotAvailable);
        selenium.hardWait(2);
        selenium.javascriptClickOn(btnIUnderstand);
        selenium.hardWait(2);
        selenium.javascriptClickOn(tAndCStatus);
        selenium.hardWait(2);
        selenium.javascriptClickOn(btnYesReject);
        selenium.hardWait(2);
        selenium.javascriptClickOn(btnDone);
        selenium.hardWait(2);
    }

    /**
     * Reject booking with test reject
     * @throws InterruptedException
     */
    public void rejectBookingWithRejectTest() throws InterruptedException {
        selenium.clickOn(radioRejectTest);
        selenium.javascriptClickOn(checkBoxTAndC);
        selenium.clickOn(btnYesReject);
    }

    /**
     * Choose lainnya as reject reason and input reason for it
     * @param reasonText reject reason for rejecting the booking e.g "Saya sudah tidak buka kosan lagi"
     * @throws InterruptedException
     */
    public void rejectBookingOtherReason(String reasonText) throws InterruptedException {
        selenium.hardWait(5);
        selenium.javascriptClickOn(radioOtherReason);
        selenium.hardWait(3);
        selenium.enterText(inputOtherReason, reasonText, true);
        selenium.hardWait(3);
        if(getTAndCAttributeValue("class").contains("reject-modal__tnc-check bg-c-checkbox")) {
            selenium.javascriptClickOn(tAndCCheckBox);
        }
        selenium.hardWait(3);
        selenium.javascriptClickOn(pilihRejctButton);
        selenium.hardWait(5);
    }

    /**
     * Click on syarat dan ketentuan / Term And Condition
     * @throws InterruptedException
     */
    public void clicksOnTermAndConditionLink() {
        selenium.pageScrollInView(buttonTAndC);
        selenium.javascriptClickOn(buttonTAndC);
    }

    /**
     * Get attribute value of term and condition check box
     * QA use thi for tick or not-ticked status
     * @param attName desired attribute e.g "class"
     * @return attribute value as string
     */
    public String getTAndCAttributeValue(String attName) {
        return selenium.getElementAttributeValue(tAndCStatus, attName);
    }

    /**
     * Input other reason without reject the booking
     * @param reasonText input with desired reject reason
     * @throws InterruptedException
     */
    public void inputOtherReasonWithoutReject(String reasonText) throws InterruptedException {
        selenium.hardWait(5);
        selenium.javascriptClickOn(radioOtherReason);
        selenium.hardWait(2);
        selenium.enterText(inputOtherReason, reasonText, true);
        selenium.hardWait(2);
        selenium.javascriptClickOn(tAndCStatus);
        selenium.hardWait(2);
    }

    /**
     * Get yes button attribute value
     * @param attName desired attribute
     * @return attribute value as string
     */
    public String getYesRejectButtonAttributeValue(String attName) {
        return selenium.getElementAttributeValue(pilihRejctButton, attName);
    }

    /**
     * Check if yes cancel booking button is active
     * @param attName should input with attribute like "disabled"
     * @return contains attribute true, otherwise false
     */
    public boolean isYesCancelButtonActive(String attName) {
        return selenium.isElementAtrributePresent(btnYesReject, attName);
    }

    /**
     * click TnC Reject booking
     * @throws InterruptedException
     */
    public void clickOnTAndCCheckBox() {
        selenium.javascriptClickOn(tAndCCheckBox);
    }

    /**
     * Reject booking with full room reason
     * @throws InterruptedException
     */
    public void rejectBookingBssReason() throws InterruptedException {
        selenium.hardWait(2);
        selenium.pageScrollInView(bssReasonRadioButton);
        selenium.hardWait(2);
        selenium.javascriptClickOn(bssReasonRadioButton);
        selenium.hardWait(2);
        selenium.javascriptClickOn(checkBoxTAndC);
        selenium.hardWait(2);
        selenium.javascriptClickOn(btnYesReject);
        selenium.hardWait(2);
    }

    /**
     * Choose lainnya as reject reason and input reason for it
     * @param reasonsText reject reason for rejecting the booking e.g "Saya sudah tidak buka kosan lagi"
     * @throws InterruptedException
     */
    public void rejectBookingPaidOtherReason(String reasonsText) throws InterruptedException {
        selenium.hardWait(3);
        selenium.javascriptClickOn(radioPaidOtherReason);
        selenium.enterText(inputPaidOtherReason, reasonsText, true);
        selenium.hardWait(5);
        selenium.javascriptClickOn(dateBoxOtherReason);
        selenium.clickOn(datePickToday);
        selenium.clickOn(pilihButton);
        selenium.clickOn(konfirmasiButton);
    }

    /**
     * click close icon
     * @throws InterruptedException
     */
    public void clickCloseButtonRejectPopup() throws InterruptedException {
        selenium.clickOn(closeButtonRejectPopup);
    }

    /**
     * check if cta reject as present
     * @return true if appears cta reject
     */
    public boolean isPopupConfirmationPresent() throws InterruptedException {
        return selenium.waitInCaseElementVisible(popupConfirmReject,5) != null;
    }

    /**
     * click Reject button on cta
     * @return
     */
    public void clickRejectOnCta() throws InterruptedException{
        selenium.hardWait(5);
        selenium.javascriptClickOn(rejectOnCta);
    }
}
