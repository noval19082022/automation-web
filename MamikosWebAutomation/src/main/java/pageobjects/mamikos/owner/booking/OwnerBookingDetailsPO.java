package pageobjects.mamikos.owner.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class OwnerBookingDetailsPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public OwnerBookingDetailsPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(className = "booking-id")
    private WebElement bookingID;

    @FindBy(css = " div.mc-product-link-card__content > div:nth-child(2) > p")
    private WebElement roomTitle;

    @FindBy(css = "div.mc-product-link-card__content > div:nth-child(2) > div > p")
    private WebElement roomPrice;

    @FindBy(css = "#manageOwnerBooking td.rent-duration")
    private WebElement rentDuration;

    @FindBy(css =  "div.mc-chat-room__header-title.bg-c-tooltip.bg-c-tooltip--light > div.bg-c-tooltip__target")
    private WebElement tenantName;

    @FindBy(css = "#manageOwnerBooking .tenant-phone")
    private WebElement tenantNumber;

    @FindBy(css = ".bg-c-text[data-v-7efa4c21]")
    private WebElement bookingStatus;

    @FindBy(xpath = "//*[@type='button'][contains(text(),'Terima')]")
    private WebElement acceptButton;

    @FindBy(xpath = "//*[@type='button'][contains(text(),'Ya, Terima')]")
    private WebElement confirmationPopUPYesAcceptButton;

    @FindBy(css = ".swal2-loading[style='display: block;']")
    private WebElement loadingAnimation;

    @FindBy(xpath = "//button[contains(.,'Lihat Detail')]")
    private WebElement lihatDetail;

    @FindBy(xpath = "//*[@class='btn btn-action-secondary' and contains(.,'Tolak')]")
    private WebElement tolakButton;

    @FindBy(xpath = "//*[@class='btn btn-primary' and contains(.,'Ya, Tolak')]")
    private WebElement yaTolakButton;

    @FindBy(xpath = "//*[@class='reject-option' and contains(.,'Lainnya')]")
    private WebElement lainnyaReason;

    @FindBy(xpath = "//textarea[@placeholder='Masukkan alasan lainnya di sini']")
    private WebElement textArea;

    @FindBy(xpath = "//input[@id='mamiCheckboxCommon']")
    private WebElement termConditionTolak;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-submit' and contains(.,'Ya, Tolak')]")
    private WebElement yaTolakButtonOnRejectPage;

    @FindBy(xpath = "(//*[@class='category-swiper-item swiper-slide'])[7]")
    private WebElement andaTolakFilter;

    @FindBy(xpath = "//button[contains(., 'Lihat Detail')]")
    private WebElement lihatDetailAndaTolak;

    @FindBy(xpath = "//*[@class='tenant-description']")
    private WebElement reasonDescription;

    @FindBy(xpath = "//*[@class='status-info__title bg-c-text bg-c-text--heading-6 ']")
    private WebElement waktuPemesananTitle;

    @FindBy(xpath = "(//*[@class='status-info__detail-label bg-c-text bg-c-text--body-2 '])[1]")
    private WebElement mulaiSewaTitle;

    @FindBy(xpath = "(//*[@class='status-info__detail-label bg-c-text bg-c-text--body-2 '])[3]")
    private WebElement durasiSewaTitle;

    @FindBy(xpath = "//*[@class='bg-c-text bg-c-text--body-2 ']")
    private List<WebElement> detailPembayaranTitle;

    @FindBy(xpath = "//*[@class='status-info__detail-value bg-c-text bg-c-text--body-1 ']")
    private List<WebElement> detailPembayaranValue;

    @FindBy(xpath = "//button[.='Terima']")
    private WebElement acceptButtonFromChat;

    @FindBy(css = ".mc-chat-room__header-content > .bg-c-label")
    private WebElement bookingStatusOnChatRoom;

    @FindBy(css = ".bg-c-text--button-md")
    private WebElement detailButton;

    @FindBy(css = "div.mc-product-link-card__content > div:nth-child(2) > div > p")
    private WebElement sisaKamarLabel;

    @FindBy(css = ".bg-c-link")
    private WebElement ubahKamarButton;

    @FindBy(xpath = "//p[contains(.,'Uang Muka (DP)')]")
    private WebElement downPaymentLabel;


    /**
     * Wait for the loading animation on the page disappear
     * Max wait is 15+-
     * @throws InterruptedException
     */
    public void waitTillLoadingAnimationDisappear() throws InterruptedException {
        int count = 1;
        selenium.waitInCaseElementClickable(loadingAnimation, 5);
        do {
            count++;
            if (count == 15)
                break;
            selenium.hardWait(1);
        }
        while (selenium.waitInCaseElementClickable(loadingAnimation, 1) != null);
    }

    /**
     * Get Booking ID from Booking Details Page
     * @return Booking ID
     */
    public String getBookingID(){
        return selenium.getText(bookingID);
    }

    /**
     * Get Room Title from Booking Details Page
     * @return Room Title
     */
    public String getRoomTitle(){
        return selenium.getText(roomTitle);
    }

    /**
     * Get Room Price from Booking Details Page
     * @return Room Price
     */
    public  String getRoomPrice(){
        return selenium.getText(roomPrice);
    }

    /**
     * Get Tenant Name from Booking Details Page
     * @return Tenant Name
     */
    public  String getTenantName(){
        selenium.pageScrollInView(roomTitle);
        return selenium.getText(tenantName);
    }

    /**
     * Get Rent Duration from Booking Details Page
     * @return Rent Duration
     */
    public String getRentDuration(){
        return selenium.getText(rentDuration);
    }

    /**
     * Get Tenant Number from Booking Details Page
     * @return Tenant Number
     */
    public String getTenantNumber(){
        return selenium.getText(tenantNumber);
    }

    /**
     * Get Booking Status from Booking Details Page
     * @return Booking Status
     */
    public String getBookingStatus(){
        selenium.pageScrollInView(bookingStatus);
        return selenium.getText(bookingStatus);
    }

    /**
     * Click On Accept Button from Booking Details Page
     */
    public void clickOnAcceptButton() {
        selenium.waitTillElementIsClickable(acceptButton);
        selenium.javascriptClickOn(acceptButton);
        selenium.waitTillElementIsClickable(confirmationPopUPYesAcceptButton);
        selenium.javascriptClickOn(confirmationPopUPYesAcceptButton);
    }

    /**
     * Click on Lihat Detail
     * Click Tolak button from booking detail page
     */
    public void clickOnTolakButton() throws InterruptedException {
        selenium.clickOn(lihatDetail);
        selenium.hardWait(2);
        selenium.clickOn(tolakButton);
    }

    /**
     * Click on Ya, Tolak on tolak pop up confirmation
     */
    public void clickOnYaTolakButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(yaTolakButton);
        selenium.hardWait(2);
    }

    /**
     * Choose Lainnya reason on reject page
     * Enter reason with text
     * @param lainnya
     */
    public void clickOnLainnya(String lainnya) throws InterruptedException {
        selenium.clickOn(lainnyaReason);
        selenium.enterText(textArea, lainnya, false);
        selenium.hardWait(2);
    }

    /**
     * Scroll down tolak page
     * And click term and condition on tolak booking page
     */
    public void clickTermAndConditionTolak() throws InterruptedException {
        selenium.pageScrollInView(termConditionTolak);
        selenium.waitForJavascriptToLoad();
        driver.findElement(By.xpath("//input[@id='mamiCheckboxCommon']")).click();
    }

    /**
     * Click Ya, Tolak button on tolak page
     */
    public void clickTolakBookingOnTolakPage() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(yaTolakButtonOnRejectPage);
        selenium.hardWait(5);
    }

    /**
     * Filter by Anda Tolak
     */
    public void clickAndaTolakFilter() throws InterruptedException {
        selenium.hardWait(2);
        driver.findElement(By.xpath("(//*[@class='category-swiper-item swiper-slide'])[7]")).click();
        selenium.hardWait(2);
    }

    /**
     * Click Lihat Detail button on Anda Tolak filter
     */
    public void clickLihatDetailOnAndaTolak() throws InterruptedException {
        selenium.clickOn(lihatDetailAndaTolak);
    }

    /**
     * Check rejection reason according to the reason entered by the owner
     * @param rejectLainnya
     * @return rejectionReason
     */
    public String getRejectionReason(String rejectLainnya){
        selenium.pageScrollInView(reasonDescription);
        WebElement element = driver.findElement(By.xpath("//*[@class='tenant-description']//i[contains(., '"+ rejectLainnya +"')]"));
        return selenium.getText(element);
    }

    /**
     * Get title waktu pemesanan
     * @return title waktu pemesanan
     */
    public String getTitleWaktuPemesanan(){
        return selenium.getText(waktuPemesananTitle);
    }

    /**
     * Get title mulai sewa
     * @return title mulai sewa
     */
    public String getTitleMulaiSewa(){
        return selenium.getText(mulaiSewaTitle);
    }

    /**
     * Get title durasi sewa
     * @return title durasi sewa
     */
    public String getTitleDurasiSewa(){
        return selenium.getText(durasiSewaTitle);
    }

    /**
     * Get title durasi sewa
     * @return title durasi sewa
     */
    public String getTitleDetailPembayaran(Integer angka){
        return selenium.getText(detailPembayaranTitle.get(angka));
    }

    /**
     * get total detail pembayaraan
     * @return detail pembayaran
     */
    public String getTotalDetail(Integer angka) throws InterruptedException{
        return selenium.getText(detailPembayaranValue.get(angka));
    }

    /**
     * Verify down payment is not displayed
     * @return boolean
     */
    public Boolean isDownPaymentNotDisplayed(String text) throws InterruptedException {
        By element = By.xpath("//*[text()=('"+text+"')]");
        selenium.hardWait(2);
        return selenium.isElementPresent(element);
    }

    /**
     * Click On Accept Button from Chat Room
     */
    public void clickOnAcceptButtonFromChatRoom() {
        selenium.waitTillElementIsClickable(acceptButtonFromChat);
        selenium.javascriptClickOn(acceptButtonFromChat);
        selenium.waitTillElementIsClickable(confirmationPopUPYesAcceptButton);
        selenium.javascriptClickOn(confirmationPopUPYesAcceptButton);
    }

    /**
     * Get Booking status
     * @return title Booking status
     */
    public String getTitleBookingStatus() throws InterruptedException{
        selenium.hardWait(5);
        return selenium.getText(bookingStatusOnChatRoom);
    }

    /**
     * Click On detail Button from Chat Room
     */
    public void clickOnDEtailButtonFromChatRoom() throws InterruptedException{
        selenium.waitTillElementIsClickable(detailButton);
        selenium.clickOn(detailButton);
    }

    /**
     * Check if kost name is displayed
     * @return true if kost name otherwise false
     */
    public boolean isKostNameDisplayed() {
        return selenium.waitInCaseElementVisible(roomTitle, 5) != null;
    }

    /**
     * Check if price kost is displayed
     * @return true if price kost otherwise false
     */
    public boolean isPriceKostDisplayed() {
        return selenium.waitInCaseElementVisible(roomPrice, 2) != null;
    }

    /**
     * Check if sisa kamar is displayed
     * @return true if sisa kamar otherwise false
     */
    public boolean isSisaKamarDisplayed() {
        return selenium.waitInCaseElementVisible(sisaKamarLabel, 2) != null;
    }

    /**
     * Check if ubah kamar button is displayed
     * @return true if ubah kamar otherwise false
     */
    public boolean isUbahKamarButtonDisplayed() {
        return selenium.waitInCaseElementVisible(ubahKamarButton, 2) != null;
    }

    /**
     * Check if label down payment is displayed
     * @return true if down payment label otherwise false
     */
    public boolean isDownPaymentLabelDisplayed() {
        return selenium.waitInCaseElementVisible(downPaymentLabel, 2) != null;
    }
}