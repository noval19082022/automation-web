package pageobjects.mamikos.owner.booking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ManageBookingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ManageBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    private By instantBooking = By.xpath("//*[@class='btn btn-primary']/*[.='Atur']");

    @FindBy(xpath = "//span[normalize-space()='Butuh Konfirmasi']")
    private WebElement needConfirmationFilter;

    @FindBy(css = ".ib-modal-header .ib-modal-header-close-icon")
    private WebElement instantBookingPopupCloseButton;

    private By bookingRequestButton = By.cssSelector("[href*='/ownerpage/manage/all/booking']");

    @FindBy(xpath = "//*[text() = 'Permintaan Booking' and @href='/ownerpage/manage/all/booking']")
    private WebElement bookingRequestMenu;

    @FindBy(css = ".tenant-name-box span")
    private WebElement tenantName;

    @FindBy(css = "#manageBookingStatusHandle span")
    private WebElement bookingStatus;

    @FindBy(css = ".box-detail-kost span")
    private WebElement roomName;

    @FindBy(xpath = "//*[text()='Tanggal Masuk']/parent::span/following-sibling::span")
    private WebElement checkInDate;

    @FindBy(className = "box-content")
    private WebElement rentDuration;

    @FindBy(className = "show-detail-link")
    private WebElement bookingDetailsButton;

    @FindBy(xpath = "//*[@class='manage-booking-list-card__body'][1]")
    private WebElement firstBookingList;

    @FindBy(xpath = "//*[@class='manage-booking-list-card__body']")
    private List<WebElement> bookingList;

    @FindBy(xpath = "//*[@class='p-icon p-plain pretty p-toggle'][2]")
    private WebElement requiresConfirmationTab;

    @FindBy(xpath = "//*[@id='manageModalDialog'][2]//*[contains(text(),'Ya,')]")
    private WebElement confirmationPopUPYesAcceptButton;

    @FindBy(xpath = "//*[.='Pelajari']")
    private WebElement pelajariButton;

    @FindBy(xpath = "//div[@class='ib-modal-content']")
    private WebElement instantBookingBody;

    @FindBy(css = "input[placeholder='Pilih Kos']")
    private WebElement kostFilter;

    @FindBy(xpath = "//*[@alt='right pointing']")
    private WebElement rightPointingImg;

    @FindBy(xpath = "//*[@class='btn manage-booking-props-filter__button --active']")
    private WebElement activeTab;

    @FindBy(xpath = "//*[@class='manage-booking-props-card__button manage-bill-link']")
    private WebElement kelolaTagihan;

    @FindBy(xpath = "//span[@role='button'][normalize-space()='Terbayar']")
    private WebElement terbayarTab;

    @FindBy(xpath = "//a[contains(.,'Lihat Kontrak')]")
    private WebElement seeContractButton;

    @FindBy(css = ".swal2-loading[style='display: block;']")
    private WebElement loadingAnim;

    private By tenantNameBy = By.cssSelector("#manageBookingList > div > div:nth-child(%s) .tenant-name-box span");

    private By bookingStatusBy = By.cssSelector(".manage-booking-list-card:nth-child(%s) .manage-booking-status");

    private By roomNameBy = By.cssSelector(".manage-booking-list-card:nth-child(%s) .room-info__property-name");

    private By checkInDateBy = By.cssSelector(".manage-booking-list-card:nth-child(%s) .booking-info__checkin div:nth-child(2)");

    private By rentDurationBy = By.cssSelector(".manage-booking-list-card:nth-child(%s) .booking-info__checkout div:nth-child(2)");

    @FindBy(xpath = "(//*[@class='bg-c-text bg-c-text--body-3 '][contains(.,'Lihat Detail')])[1]")
    private WebElement bookingDetailsButtonBy;

    @FindBy(css = "#inputFilter")
    private WebElement kostBookingListInput;

    @FindBy(css = "[data-testid='bookingRequestList-statusFilter']")
    private WebElement ownerBookingProposal;

    @FindBy(xpath = "//button[contains(.,'Tolak')]")
    private WebElement btnRejectBooking;

    @FindBy(css = ".swiper-wrapper > div:nth-of-type(2) [type='checkbox']")
    private WebElement btnButuhKonfirmasi;

    @FindBy(xpath = "//*[@class='bg-c-modal__footer-CTA']//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    private WebElement btnYesReject;

    @FindBy (css = ".swiper-wrapper > div:nth-of-type(1) [type='checkbox']")
    private WebElement allFilterButton;

    @FindBy(xpath = "//*[@class='swiper-wrapper']/div[contains(.,'Anda Tolak')]")
    private WebElement filterYourReject;

    @FindBy(xpath = "(//*[@class='bg-c-text bg-c-text--body-3 '])[1]")
    private WebElement textBookingStatusFirstList;

    @FindBy(css = ".booking-request-list__booking-list > div:nth-of-type(1) .bg-c-text--title-3")
    private WebElement textTenantNameFirstList;

    @FindBy(css = ".fade.in .modal-button-close span")
    private WebElement btnClosePopUp;

    @FindBy(css = "i[data-v-c1f8caf0]")
    private WebElement txtRejectReason;

    @FindBy(css = ".bg-c-dropdown__menu--text-lg input")
    private WebElement inputSearchKost;

    @FindBy(xpath = "//p[@class='booking-empty__title bg-c-text bg-c-text--title-3 ']")
    private WebElement noRequestBooking;

    /**
     * Format %s into element index
     * @param targetElement fill with target element to format
     * @param targetIndex fill with int value of target index
     * @return
     */
    public By formatElementBy(By targetElement, int targetIndex) {
        String targetFormatElement = targetElement.toString().replace("By.cssSelector: ", "");
        targetFormatElement = String.format(targetFormatElement, String.valueOf(targetIndex));
        return By.cssSelector(targetFormatElement);
    }

    /**
     * Click on Booking Request Option from Owner Booking Conformation page
     *
     * @throws InterruptedException
     */
    public void clickOnBookingRequestOption() throws InterruptedException {
        selenium.waitTillElementIsClickable(bookingRequestMenu);
        selenium.clickOn(bookingRequestMenu);
    }

    /**
     * Click on filter booking need confirmation
     *
     * @throws InterruptedException
     */
    public void clickOnNeedConfirmationFilter() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementClickable(needConfirmationFilter,10);
        selenium.clickOn(needConfirmationFilter);
    }

    /**
     * click on no request booking
     */
    public boolean clickOnNoRequestBooking() {
        return selenium.isElementDisplayed(noRequestBooking);
    }

    /**
     * Get Booking Status from Booking Info page
     * @param targetIndex fill with int value of target index
     * @return
     */
    public String getBookingStatus(int targetIndex) {
        return selenium.getText(formatElementBy(bookingStatusBy, targetIndex));
    }

    /**
     * Get Room Name from Booking Info Page
     * @param targetIndex fill with int value of target index
     * @return
     */
    public String getRoomName(int targetIndex) {
        return selenium.getText(formatElementBy(roomNameBy, targetIndex));
    }

    /**
     * Get CheckIn Date info From Booking Info page
     * @param targetIndex fill with int value of target index
     * @return
     */
    public String getCheckInDate(int targetIndex) {
        return selenium.getText(formatElementBy(checkInDateBy, targetIndex));
    }

    /**
     * Get Rent Duration data from  Booking Info Page
     * @param targetIndex fill with int value of target index
     * @return
     */
    public String getRentDuration(int targetIndex) {
        return selenium.getText(formatElementBy(rentDurationBy, targetIndex));
    }

    /**
     * Click on Booking Deatils button from Booking Info Page
     * @throws InterruptedException
     */
    public void clickOnBookingDetailsButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(bookingDetailsButtonBy);
    }

    /**
     * Get number list of booking
     * @return numberOfElements is number of booking list
     */
    public int getNumberListOfBooking() throws InterruptedException {
        int numberOfElements = 0;
        selenium.hardWait(10);
        if(selenium.waitInCaseElementVisible(firstBookingList, 40) != null){
            numberOfElements = bookingList.size();
        }
        return numberOfElements;
    }

    /**
     * Get tenant name
     * @return tenant name
     */
    public String getTenantName(int index) {
        By element = By.xpath("(//*[@class='tenant-name text-ellipsis'])[" + index + "]");
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Click accept button
     * @param index is number for specific data want to get
     */
    public void clickOnAcceptButton(int index){
        By element = By.xpath("//*[@class='btn btn-primary btn-action-primary'and contains(text(),'Terima')]["+ index +"]");
        selenium.pageScrollInView(element);
        selenium.javascriptClickOn(element);
        selenium.javascriptClickOn(confirmationPopUPYesAcceptButton);
    }

    /**
     * Click requires confirmation tab
     * @throws InterruptedException
     */
    public void clickOnRequiresConfirmationTab() throws InterruptedException {
        selenium.waitTillElementIsClickable(requiresConfirmationTab);
        selenium.hardWait(8);
        selenium.javascriptClickOn(requiresConfirmationTab);
        selenium.hardWait(2);
        selenium.refreshPage();
        selenium.waitTillElementIsClickable(requiresConfirmationTab);
        selenium.hardWait(10);
        selenium.javascriptClickOn(requiresConfirmationTab);
    }

    /**
     * Select kost from filter
     * @throws InterruptedException
     */
    public void selectKostFromFilter(String kostName) throws InterruptedException {
        int count = 1;
        String kostNameEl = "//a[contains(.,'"+kostName+"')]";
        selenium.waitInCaseElementVisible(loadingAnim, 20);
        do {
            count++;
            if (count == 15)
                break;
            selenium.hardWait(3);
        }
        while (isLoadingAppear());
        selenium.waitTillElementIsClickable(kostFilter);
        selenium.hardWait(5);
        selenium.clickOn(kostFilter);
        selenium.hardWait(3);
        selenium.enterText(inputSearchKost, kostName, false);
        selenium.hardWait(5);
        selenium.waitTillElementIsClickable(By.xpath(kostNameEl));
        selenium.clickOn(By.xpath(kostNameEl));
    }

    /**
     * Wait for 5 seconds to check if right pointing img is present in viewport
     * @return true if right pointing img is visible otherwise false
     */
    public boolean isRightPointingImgPresent() {
        return selenium.waitInCaseElementVisible(rightPointingImg, 5) != null;
    }

    /**
     * Click on instant booking Pelajari button and dismiss the pop up
     * @throws InterruptedException
     */
    public void dismissInstantBookingPopUp() throws InterruptedException {
        if(isRightPointingImgPresent()) {
            selenium.waitInCaseElementVisible(pelajariButton, 3);
            selenium.hardWait(2);
            selenium.clickOn(pelajariButton);
            selenium.waitInCaseElementVisible(instantBookingBody, 3);
            selenium.hardWait(2);
            selenium.waitTillElementIsClickable(instantBookingPopupCloseButton);
            selenium.clickOn(instantBookingPopupCloseButton);
        }
    }

    /**
     * Click on active tab
     * @throws InterruptedException
     */
    public void clickOnActiveTab() throws InterruptedException {
        selenium.waitTillElementIsClickable(activeTab);
        selenium.hardWait(2);
        selenium.javascriptClickOn(activeTab);
        selenium.hardWait(3);
        selenium.javascriptClickOn(kelolaTagihan);
    }

    /**
     * Click on terbayar tab
     * @throws InterruptedException
     */
    public void clickOnTerbayarTab() throws InterruptedException {
        selenium.waitTillElementIsVisible(terbayarTab);
        selenium.clickOn(terbayarTab);
        selenium.hardWait(5);
        selenium.clickOn(seeContractButton);
    }

    /**
     * Check if loading animation visibility
     * @return true false
     */
    public boolean isLoadingAppear() {
        return selenium.waitInCaseElementVisible(loadingAnim, 1) != null;
    }

    /**
     * Handling loading that may appear
     * @throws InterruptedException
     */
    public void loadingHandler() throws InterruptedException {
        int maxDoLoop = 0;
        do {
            maxDoLoop++;
            if (maxDoLoop == 20)
                break;
            selenium.hardWait(1);
        }
        while (isLoadingAppear());
    }

    /**
     * Change kost on manage booking page
     * @param kostName String kostName
     * @throws InterruptedException
     */
    public void changeKost(String kostName) throws InterruptedException {
        String kostListTarget = "//li/span[.='"+ kostName + "']";
        selenium.clickOn(kostBookingListInput);
        selenium.hardWait(2);
        selenium.enterText(kostBookingListInput, kostName, false);
        selenium.clickOn(selenium.waitInCaseElementPresent(By.xpath(kostListTarget), 2));
    }

    /**
     * Check if manageBooking page is reached
     * @return true if page reached otherwise false
     */
    public boolean isInOwnerBookingProposal() {
        return selenium.waitInCaseElementVisible(ownerBookingProposal, 2) != null;
    }

    /**
     * Reject list number 1 booking request
     * @throws InterruptedException
     */
    public void clickOnRejectBooking() throws InterruptedException {
        selenium.hardWait(5);
        selenium.javascriptClickOn(btnRejectBooking);
        selenium.hardWait(3);
        selenium.javascriptClickOn(btnYesReject);
    }

    /**
     * Activated your reject booking status
     * @throws InterruptedException
     */
    public void activatedYourRejectedFilter() throws InterruptedException {
        selenium.hardWait(5);
        selenium.waitTillElementIsClickable(filterYourReject);
        selenium.clickOn(filterYourReject);
    }

    /**
     * click on all filter
     */
    public void clickOnAllFilter() {
        selenium.waitTillElementIsVisible(allFilterButton, 5);
        selenium.javascriptClickOn(allFilterButton);
    }

    /**
     * Get booking status from first list tenant card
     * @return string data type e.g "Anda Tolak"
     */
    public String getFirstListBookingStatus() {
        return selenium.getText(textBookingStatusFirstList);
    }

    /**
     * Get tenant name from first list tenant card
     * @return string data type e.g "Tenant Premium"
     */
    public String getFirstListTenantName() {
        return selenium.getText(textTenantNameFirstList);
    }

    /**
     * Check if button reject is still available
     * @return true if visible otherwise false
     */
    private boolean isButtonRejectVisible() {
        return selenium.waitInCaseElementVisible(btnRejectBooking, 30) != null;
    }

    /**
     * Dismiss reject pop up if it still available after reject booking
     */
    public void dismissRejectPopUp() {
        if(isButtonRejectVisible()) {
            selenium.javascriptClickOn(btnClosePopUp);
        }
    }

    /**
     * Get reject reason after booking is rejected from
     * @return
     */
    public String getRejectReason() {
        return selenium.getText(txtRejectReason);
    }
}
