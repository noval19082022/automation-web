package pageobjects.mamikos.landingpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.List;


public class GoldPlusPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public GoldPlusPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    //--------------- Banner Promo ------------------
    @FindBy(xpath = "//*[@class='goldplus-banner__learn-more']")
    private WebElement learnMoreButton;

    @FindBy(xpath = "//*[@class='c-mk-button goldplus-card__main-cta c-mk-button--green-inverse c-mk-button--block']")
    private WebElement manageGoldPlus;

    //--------------- Register Gold Plus ------------------
    @FindBy(xpath = "//button[contains(text(),'Daftar')]")
    private WebElement registerGPButton;

    @FindBy(css = ".membership-card__label")
    private WebElement waitPaymentGPButton;

    @FindBy(xpath = "//h2[contains(.,'Paket GoldPlus')]")
    private WebElement listGPPage;

    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--md']")
    private List<WebElement> chooseGoldPlusButton;

    @FindBy(css = ".goldplus-package-content__packages-wrapper > div:nth-of-type(2) .goldplus-package-card__modal-toggler")
    private WebElement lihatDetailLainnyaGP1Button;

    @FindBy(xpath = "//p[contains(text(),'Rincian Pembayaran')]")
    private WebElement detailTagihanGPPage;

    @FindBy(xpath = "//*[@class='goldplus-billing-detail__billing-price bg-c-text bg-c-text--body-1 ']")
    private WebElement goldPlusPriceLabel;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-button--tertiary-naked bg-c-button--md']")
    private WebElement changeGPButton;

    @FindBy(css = ".bg-c-checkbox__icon")
    private WebElement uncheckCheckBox;

    @FindBy(xpath = "//div[@class='bg-c-toast__content']")
    private WebElement warningToast;

    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg bg-c-button--block']")
    private WebElement payNowButtonDisable;

    @FindBy(css = ".gp-swiper__navigation-button-next")
    private WebElement swiperNavigateGpGuideButton;

    @FindBy(xpath = "//*[contains(@class, 'swiper-slide-active')]//h4")
    private WebElement titleOnHowtoUseGpGuideOnEcardText;

    @FindBy(xpath = "//*[contains(@class, 'swiper-slide-active')] //div[2]/p")
    private WebElement descOnHowtoUseGpGuideOnEcardText;

    @FindBy(xpath = "//*[contains(@class, 'light') and not(contains(@class,'dim'))] //*[contains(@class, 'body')]")
    private WebElement stepOnHowtoUseGpGuideOnEcardText;

    @FindBy(xpath = "//a[contains(.,'Pelajari caranya')]")
    private WebElement pelajariCaranyaText;

    @FindBy(xpath = "//*[@class='goldplus-package-card__modal-toggler']")
    private List<WebElement> checkOtherBenefitButton;

    @FindBy(xpath = "(//*[@class='bg-c-text bg-c-text--body-landing '])[1]")
    private WebElement headerText;

    @FindBy(xpath = "//*[@class='bg-c-button goldplus-package-modal__footer-button bg-c-button--primary bg-c-button--md bg-c-button--block']")
    private WebElement chooseGpButton;

    @FindBy(tagName = "h2")
    private WebElement manfaatGP1Popup;

    @FindBy(xpath = "//*[@class='goldplus-tnc-check']/span[1]")
    private WebElement syaratKetentuanText;

    @FindBy(xpath = "//*[contains(@class, 'goldplus-guide-card__content')]//h4")
    private List<WebElement> panduanGoldPlusTitleList;

    @FindBy(xpath = "//*[contains(@class, 'goldplus-guide-card__content')]//p")
    private List<WebElement> panduanGoldPlusContentList;

    @FindBy(xpath = "//*[@class='membership-card__title']")
    private WebElement mamikosGPLabel;

    @FindBy(xpath = "(//a[contains(.,'Lihat Selengkapnya')])[1]")
    private WebElement seeDetailButton;

    @FindBy(xpath = "//h1[@class='goldplus-header__title']/child::span")
    private WebElement pagePaketGoldplusTitle;

    @FindBy(xpath = "//*[@class='c-mk-card__header goldplus-room-card__header']")
    private WebElement statusPaketGoldplusText;

    @FindBy(xpath = "//*[@class='goldplus-room-list-content__footer']/button")
    private WebElement changePaketGPButton;

    @FindBy(xpath = "//*[@class='bg-c-modal__body-title']")
    private WebElement popUpChangePaketGoldplusText;

    @FindBy(xpath = "//div[@class='goldplus-rooms-empty-state__title']")
    private WebElement statusPaketGoldplusMenungguPembayaranText;

    @FindBy(xpath = "(//*[@class='wrap-helper'])[1]")
    private WebElement totalPropertyGoldplusText;

    @FindBy(css = ".bg-c-button--primary")
    private WebElement bayarSekarangButton;

    @FindBy(xpath = "(//*[@class='glyphicon glyphicon-cog'])[1]")
    private WebElement actionButton;

    @FindBy(css = ".swal2-confirm")
    private WebElement yesButton;

    @FindBy(xpath = "(//*[@class='actions terminate-contract'])[1]")
    private WebElement terminateContractChoice;

    @FindBy(xpath = "//*[@class='swal2-html-container']")
    private WebElement successTerminateGPText;


    @FindBy(xpath = "(//*[@class='goldplus-billing-detail__billing-title bg-c-text bg-c-text--body-2 '])[2]")
    private WebElement rincianMamiadsText;

    @FindBy(xpath = "(//*[@class='goldplus-billing-detail__billing-price bg-c-text bg-c-text--body-1 '])[2]")
    private WebElement saldoMamiadsText;

    @FindBy(xpath = "//*[contains(@class,'goldplus-mamiads-detail__item-name')]")
    private List<WebElement> saldoNameText;

    @FindBy(xpath = "//*[contains(@class,'goldplus-mamiads-detail__item-cashback')]")
    private List<WebElement> cashbackNameText;

    @FindBy(xpath = "//*[contains(@class,'goldplus-mamiads-detail__item-discount bg-c-text bg-c-text--body-4 ')]")
    private List<WebElement> discNameText;

    @FindBy(xpath = "//*[contains(@class,'goldplus-mamiads-detail__item-sale-price')]")
    private List<WebElement> saleNameText;

    @FindBy(xpath = "//*[contains(@class,'goldplus-mamiads-detail__item-discount-price')]")
    private List<WebElement> discPriceText;

    @FindBy(xpath = "//*[contains(@class,'goldplus-mamiads-detail__item-saving')]")
    private List<WebElement> savingNameText;

    @FindBy(css = ".goldplus-mamiads-detail__expand")
    private WebElement tutupListBalanceGP;

    @FindBy(css = ".goldplus-package-detail__item-title")
    private WebElement gpPackageText;

    @FindBy(css = ".goldplus-package-detail__item-description")
    private WebElement pilihanAndaText;

    @FindBy(css = ".goldplus-billing-detail__billing-price")
    private WebElement rincianPembayaranText;

    @FindBy(xpath = "//div[@class='goldplus-tnc-check']/span[1]\n")
    private WebElement syaratDanKetentuanGPText;

    @FindBy(xpath = " //div[5]//a[.='Lihat Selengkapnya']")
    private WebElement lihatSelengkapnyaRincianPembayaran;

    @FindBy(xpath = " //a[contains(.,'Selesai')]")
    private WebElement tabSelesaiRincianBayar;

    @FindBy(css = ".bg-c-tabs-content")
    private WebElement listTableRiwayatPembayaran;

    @FindBy(css = ".gold-plus-features__upgrade-to-gp2")
    private WebElement blackBoxUpgradeGP;

    @FindBy(css = ".bg-c-button")
    private WebElement buttonUpgradeGP2Black;

    @FindBy(css = ".bg-c-modal__body")
    private WebElement popUpUpgradeGP2;

    @FindBy(css = " .gold-plus-upgrade-modal-footer__button-next")
    private WebElement buttonUpgradeGP2PopUp;

    @FindBy(css = " .back-icon > .bg-c-icon")
    private WebElement backDetailTagihan;

    @FindBy(xpath = " //div[@class='gold-plus-features']/div[3]//a[.='Lihat Selengkapnya']")
    private WebElement lihatSelengkapnyaProperty;

    @FindBy(css = " .bg-c-icon[data-v-389fd05e]")
    private WebElement iconClosePopUpUpgradeGP;

    @FindBy(css = " .bg-c-icon[data-v-7e8d294e]")
    private WebElement iconBackGpPage;

    @FindBy(xpath = " //p[@class='goldplus-package-detail__item-title bg-c-text bg-c-text--body-1 ']")
    private WebElement packageGP;

    @FindBy(css = ".goldplus-package-detail__item-description")
    private WebElement descGpPage;

    @FindBy(xpath = "//a[contains(.,'Syarat dan Ketentuan Umum')]")
    private WebElement textSyarat;

    @FindBy(css = ".mdi-close")
    private WebElement iconClosePopUpSyarat;

    @FindBy(css = ".c-mk-button")
    private WebElement sayaMengertiButton;

    @FindBy(css = ".goldplus-submission-modal-wrapper")
    private WebElement syaratKetentuanPopup;







    /**
     * Click on the learn more button
     *
     * @throws InterruptedException
     */
    public void clickOnTheLearnMoreButton() throws InterruptedException {
        selenium.clickOn(learnMoreButton);
        selenium.switchToWindow(0);
    }

    /**
     * Verify is GoldPlus Banner Appear
     *
     * @return true if element appear
     */
    public boolean isGoldPlusBannerAppear() {
        return selenium.waitInCaseElementVisible(learnMoreButton, 5) != null;
    }

    /**
     * Click on daftar gold plus
     *
     * @throws InterruptedException
     */
    public void clickOnRegisterGoldPlus() throws InterruptedException {
        selenium.hardWait(3);
        selenium.javascriptClickOn(registerGPButton);
    }

    /**
     * Verify page GoldPlus appear
     *
     * @return true if page appear
     */
    public boolean pageListGoldPlusAppear() throws InterruptedException {
        selenium.pageScrollInView(listGPPage);
        selenium.hardWait(3);
        return selenium.waitInCaseElementVisible(listGPPage, 5) != null;

    }

    /**
     * Click on pilih paket gold plus 1
     *
     * @param number package gold plus
     * @throws InterruptedException
     */
    public void choosePaketGoldPlus(int number) throws InterruptedException {
        selenium.hardWait(3);
        selenium.javascriptClickOn(chooseGoldPlusButton.get(number));
    }

    /**
     * Click on lihat detail lainnya
     *
     * @throws InterruptedException
     */
    public void lihatDetailLainnyaGP1() throws InterruptedException {
        selenium.hardWait(3);
        selenium.click(lihatDetailLainnyaGP1Button);
    }

    /**
     * Verify page detail tagihan appear
     *
     * @return true if page appear
     * @throws InterruptedException
     */
    public boolean pageDetailTagihanGoldPlusAppear() throws InterruptedException {
        selenium.hardWait(3);
        selenium.pageScrollInView(detailTagihanGPPage);
        return selenium.waitInCaseElementVisible(detailTagihanGPPage, 5) != null;
    }

    /**
     * Get price gold plus 1
     *
     * @return String gold plus 1 price
     */
    public String getGoldplus1Price() {
        int number = JavaHelpers.extractNumber(selenium.getText(goldPlusPriceLabel));
        return String.valueOf(number);
    }

    /**
     * Click on ubah gold plus
     *
     * @throws InterruptedException
     */
    public void clickOnUbahGoldPlus() throws InterruptedException {
        selenium.clickOn(changeGPButton);
    }

    /**
     * Click on uncheck check box gold plus
     *
     * @throws InterruptedException
     */
    public void clickOnUncheckCheckBox() throws InterruptedException {
        selenium.clickOn(uncheckCheckBox);
    }

    /**
     * Verify warning toast appear
     *
     * @return true if toast appear
     */
    public boolean isWarningToastAppear() {
        return selenium.waitInCaseElementVisible(warningToast, 5) != null;
    }

    /**
     * Check on disabled pay now button
     *
     * @return boolean
     */
    public boolean isPayNowButtonDisable() {
        return !payNowButtonDisable.isEnabled();
    }

    /**
     * Get text toast
     *
     * @return text toast
     */
    public String getTextToast() {
        return selenium.getText(warningToast);
    }

    /**
     * Click on "Pelajari caranya" in GP registration page
     *
     * @throws InterruptedException
     */
    public void clickOnPelajariCaranya() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(pelajariCaranyaText);
    }

    /**
     * Click on @param in GP registration page
     *
     * @param panduan is used for which panduan user wants to click (Naikkan Iklan)
     * @throws InterruptedException
     */
    public void clickOnPanduanFitur(String panduan) throws InterruptedException {
        String element = "//*[contains(text(),'" + panduan + "')]";
        selenium.clickOn(By.xpath(element));
    }

    /**
     * Get text guide how to use
     *
     * @param howToUse is to get text insinde the e-card (title, desc and stepCard)
     * @return String text guide how to use
     */
    public String goldplusGuideDetailHowToUse(String howToUse) {
        WebElement element;
        switch (howToUse) {
            case "title":
                element = titleOnHowtoUseGpGuideOnEcardText;
                break;
            case "desc":
                element = descOnHowtoUseGpGuideOnEcardText;
                break;
            case "stepCard":
                element = stepOnHowtoUseGpGuideOnEcardText;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + howToUse);
        }
        return selenium.getText(element);
    }

    /**
     * Click on swiper navigate gp guide button
     *
     * @throws InterruptedException
     */
    public void clickSwiperButtonGuideGp() throws InterruptedException {
        if (swiperNavigateGpGuideButton.isEnabled()) {
            selenium.clickOn(swiperNavigateGpGuideButton);
        }
    }

    /**
     * Click on check other benefits
     *
     * @param number other benefits gp
     * @throws InterruptedException
     */
    public void clickOnCheckOtherBenefits(int number) throws InterruptedException {
        selenium.clickOn(checkOtherBenefitButton.get(number));
    }

    /**
     * Get text header on pop up other benefits
     *
     * @return text header popup
     */
    public String getTextHeader() {
        return selenium.getText(headerText);
    }

    /**
     * Verify choose gp button appear
     *
     * @return true if button appear
     */
    public boolean isChooseGpButtonAppear() {
        return selenium.waitInCaseElementVisible(chooseGpButton, 5) != null;
    }

    /**
     * Verify popup manfaat gp1 appear
     *
     * @return true if button appear
     */
    public boolean popupManfaatGP1() {
        return selenium.waitTillElementIsVisible(manfaatGP1Popup, 5) != null;
    }

    /**
     * Get text syarat dan ketentuan goldplus
     *
     * @return text syarat dan ketentuan
     */
    public String getTextSyaratDanKetentuan() {
        return selenium.getText(syaratKetentuanText);
    }

    /**
     * Check on enabled pay now button
     *
     * @return boolean
     */
    public boolean isPayNowButtonEnable() {
        return payNowButtonDisable.isEnabled();
    }

    /**
     * Get text guide how to use
     *
     * @param panduanFitur is to get text insinde panduan fitur goldplus (title and content)
     * @param index        to get panduan fitur element
     * @return String text list panduan fitur goldplus
     */
    public String listPanduanFiturGoldPlus(String panduanFitur, int index) {
        List<WebElement> element = null;
        if (panduanFitur.equals("title")) {
            element = panduanGoldPlusTitleList;
        } else if (panduanFitur.equals("content")) {
            element = panduanGoldPlusContentList;
        }
        return selenium.getText(element.get(index));
    }

    /**
     * Click on mamikos gold plus label
     *
     * @throws InterruptedException
     */
    public void clickOnMamikosGPLabel() throws InterruptedException {
        selenium.clickOn(mamikosGPLabel);
    }

    /**
     * Click on mamikos gold plus label
     *
     * @throws InterruptedException
     */
    public void clickOnSeeDetailGoldPlusAnda() throws InterruptedException {
        selenium.clickOn(seeDetailButton);
    }

    /**
     * Get title page Paket Goldplus Anda
     *
     * @return String Paket Goldplus Anda title
     */
    public String getPagePaketGoldPlusTitle() {
        selenium.waitTillElementIsVisible(pagePaketGoldplusTitle);
        return selenium.getText(pagePaketGoldplusTitle);
    }

    /**
     * Get status paket goldplus
     *
     * @return String status paket goldplus
     */
    public String getStatusPaketGoldPlus() {
        return selenium.getText(statusPaketGoldplusText).replaceAll("\n", " ");
    }

    /**
     * Click on ajukan ganti paket gp
     *
     * @throws InterruptedException
     */
    public void clickOnChangePaketGoldPlus() throws InterruptedException {
        selenium.clickOn(changePaketGPButton);
    }

    /**
     * Get text on popup ajukan ganti paket goldplus
     *
     * @return String text ganti paket goldplus
     */
    public String getTextChangePaketGoldPlus() {
        return selenium.getText(popUpChangePaketGoldplusText);
    }

    /**
     * Click on type filter paket gp anda
     *
     * @param typeFilter to click type filter paket gp
     * @throws InterruptedException
     */
    public void clickOnTypeFilter(String typeFilter) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//button[contains(.,'" + typeFilter + "')]"));
        selenium.clickOn(element);
    }

    /**
     * Get status paket goldplus on tab menunggu pembayaran and sedang diproses
     *
     * @return String status paket goldplus
     */
    public String getStatusMenungguPembayaranGP() {
        return selenium.getText(statusPaketGoldplusMenungguPembayaranText);
    }

    /**
     * Get text total property terdaftar goldplus
     *
     * @return String text total property
     * @throws InterruptedException
     */
    public String getTextTotalPropertyGoldPlus() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.getText(totalPropertyGoldplusText).replaceAll("\n", " ");
    }

    /**
     * Click on button bayar sekarang
     *
     * @throws InterruptedException
     */
    public void clickOnBayarSekarangButton() throws InterruptedException {
        selenium.clickOn(bayarSekarangButton);
        selenium.hardWait(3);
    }

    /**
     * Click button action on property package
     *
     * @throws InterruptedException
     */
    public void clickOnActionButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(actionButton);
    }

    /**
     * Click termminate contract on property package
     *
     * @throws InterruptedException
     */
    public void clickOnTerminateContract() throws InterruptedException {
        selenium.clickOn(terminateContractChoice);
    }

    /**
     * return boolean no button termminate
     *
     * @throws InterruptedException
     */
    public boolean isNoTerminateContract() throws InterruptedException {
        return selenium.waitInCaseElementVisible(terminateContractChoice, 5) != null;
    }

    /**
     * Click yes button to confirm terminate contract goldplus
     *
     * @throws InterruptedException
     */
    public void clickOnYesButton() throws InterruptedException {
        selenium.clickOn(yesButton);
        selenium.hardWait(6);
    }

    /**
     * Get text success terminate contract goldplus
     *
     * @return String text success terminate contract
     */
    public String getTextSuccessTerminateContractGP() {
        return selenium.getText(successTerminateGPText);
    }

    /**
     * user choose saldo
     *
     * @param saldo type saldo
     * @throws InterruptedException
     */
    public void chooseSaldo(String saldo) throws InterruptedException {
        selenium.hardWait(5);
        WebElement element = driver.findElement(By.xpath("//p[contains(.,'" + saldo + "')]"));
        selenium.clickOn(element);
    }

    /**
     * Get text rincian MamiAds
     *
     * @return String text rincian MamiAds
     */
    public String getTextRinicianMamiAds() {
        return selenium.getText(rincianMamiadsText);
    }

    /**
     * Get text saldo MamiAds
     *
     * @return String text saldo MamiAds
     */
    public String getTextSaldoMamiAds() {
        return selenium.getText(saldoMamiadsText);
    }

    /**
     * Get MamiAds saldo, cashback, disc, salePrice, discount price MamiAds and saving
     *
     * @param index input with mamiadsSaldo
     * @return String MAmiAds saldo, cashback, disc, salePrice, discount price MamiAds and saving
     */
    public String mamiadsSaldo(String mamiadsSaldo, int index) {
        List<WebElement> element = null;
        switch (mamiadsSaldo) {
            case "saldo":
                element = saldoNameText;
                break;
            case "cashback":
                element = cashbackNameText;
                break;
            case "disc":
                element = discNameText;
                break;
            case "salePrice":
                element = saleNameText;
                break;
            case "discPriceMamiAds":
                element = discPriceText;
                break;
            case "saving":
                element = savingNameText;
                break;
        }
        selenium.pageScrollInView(tutupListBalanceGP);
        return selenium.getText(element.get(index));
    }

    /**
     * Verify the mamiads package not displayed from rincian pembayaran
     *
     * @return false
     */
    public boolean isRincianNotVisible() {
        return selenium.isElementDisplayed(rincianMamiadsText);
    }

    /**
     * Verify the saldo mamiads not displayed from rincian pembayaran
     *
     * @return false
     */
    public boolean isSaldoNotVisible() {
        return selenium.isElementNotDisplayed(saldoMamiadsText);
    }

    /**
     * Verify the GP package selected
     *
     * @return GP package
     */
    public String getGpPackage() {
        return selenium.getText(gpPackageText);
    }

    /**
     * Verify text at section Pilihan Anda detail tagihan GP
     *
     * @return Pilihan Anda Text
     */
    public String getPilihanAndaTitle() {
        return selenium.getText(pilihanAndaText);
    }

    /**
     * Verify text at section Rincian Pembayaran detail tagihan GP
     *
     * @return Rincian Pembayaran Text
     */
    public String getRincianPembayaranText() {
        return selenium.getText(rincianPembayaranText);
    }

    /**
     * Verify text at section Syarat dan Ketetntuan detail tagihan GP
     *
     * @return syarat dan ketentuan Text
     */
    public String getSyaratKetentuanGPText() {
        return selenium.getText(syaratDanKetentuanGPText);
    }

    /**
     * Verify button Bayar Sekarang is present
     *
     * @return button Bayar Sekarang present
     */
    public boolean isBayarSekarangButtonAppear() {
        return selenium.waitInCaseElementVisible(bayarSekarangButton, 2) != null;
    }

    /**
     * Click link lihat selengkapnya at section rincian pembayaran goldplus
     *
     * @throws InterruptedException
     */
    public void clickLihatSelengkapnyaRincianPembayaran() throws InterruptedException {
        selenium.javascriptClickOn(lihatSelengkapnyaRincianPembayaran);
    }

    /**
     * Click on tab Selesai at page rincian pembayaran goldplus
     *
     * @throws InterruptedException
     */
    public void clickTabSelesai() throws InterruptedException {
        selenium.javascriptClickOn(tabSelesaiRincianBayar);
    }

    /**
     * Verify table rincian pembayaran goldplus is present
     *
     * @return button table rincian pembayaran present
     */
    public boolean isTablePembayaranPresent() {
        return selenium.waitInCaseElementVisible(tabSelesaiRincianBayar, 2) != null;
    }

    /**
     * Get text upgrade GP 2
     *
     * @return String text upgrade GP 2
     */
    public String getTextUpgradeGP2() {
        return selenium.getText(blackBoxUpgradeGP);
    }

    /**
     * Click on button upgrade GP 2 at box black
     *
     * @throws InterruptedException
     */
    public void clickUpgradeGp2Black() throws InterruptedException {
        selenium.javascriptClickOn(buttonUpgradeGP2Black);
    }

    /**
     * Verify pop up upgrade goldplus is present
     *
     * @return pop up upgrade GP present
     */
    public boolean isPopUpUpgradeGPPresent() {
        return selenium.waitInCaseElementVisible(popUpUpgradeGP2, 2) != null;
    }

    /**
     * Click on button upgrade GP 2 at pop up upgrade GP 2
     *
     * @throws InterruptedException
     */
    public void clickUpgradeGp2PopUP() throws InterruptedException {
        selenium.waitTillElementIsClickable(buttonUpgradeGP2PopUp);
        selenium.hardWait(5);
        selenium.clickOn(buttonUpgradeGP2PopUp);
    }

    /**
     * Click icon back at detail tagihan upgrade GP
     *
     * @throws InterruptedException
     */
    public void clickBackDetailTagihan() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(backDetailTagihan);
    }

    /**
     * Click lihat selengkapnya at section property terdaftar
     *
     * @throws InterruptedException
     */
    public void clickLihatSelengkapnyaProperty() throws InterruptedException {
        selenium.clickOn(lihatSelengkapnyaProperty);
    }

    /**
     * Click icon close at pop up upgrade GP 2
     *
     * @throws InterruptedException
     */
    public void clickIconCloseUpgradeGP() throws InterruptedException {
        selenium.clickOn(iconClosePopUpUpgradeGP);
    }

    /**
     * Click on widget gold plus "Menunggu Pembayaran"
     *
     * @throws InterruptedException
     */
    public void clickOnWaitPaymentGoldPlus() throws InterruptedException {
        selenium.hardWait(3);
        selenium.javascriptClickOn(waitPaymentGPButton);
    }

    /**
     * Navigate to URL
     */
    public void navigateToPage() {
        selenium.navigateToPage(Constants.MAMIKOS_URL);
    }

    /**
     * Click icon back at GP dashboard
     *
     * @throws InterruptedException
     */
    public void clickIconBackGP() throws InterruptedException {
        selenium.clickOn(iconBackGpPage);
    }

    /**
     * Get text goldplus package
     *
     * @return String text goldplus package
     */
    public String getTextGoldplusPackage() {
        return selenium.getText(packageGP);
    }

    /**
     * Get text goldplus package description
     *
     * @return String text goldplus package description
     */
    public String getTextGoldplusPackageDesc() {
        return selenium.getText(descGpPage);
    }

    /**
     * Verify button bayar sekarang is present
     *
     * @return button bayar selarang GP present
     */
    public boolean isButtonBayarSekarangPresent() {
        return selenium.waitInCaseElementVisible(bayarSekarangButton, 2) != null;
    }

    /**
     * Get pembayaran Goldplus
     *  <p> - No. Invoice
     *  <p> - Jenis Pembayaran
     *  <p> - Metode Pembayaran
     * @return String
     */
    public String getPembayaranTextGoldplus(String text) throws InterruptedException {
        selenium.hardWait(3);
        if(text.contains("Status Transaksi")) {
            return selenium.getText(By.cssSelector(".bg-c-label"));
        } else if (text.contains("Total Pembayaran")) {
            return selenium.getText(By.cssSelector(".invoice-total-amount"));
        } else {
            return selenium.getText(By.xpath("//*[contains(text(),'"+text+"')]/../../following-sibling::div"));
        }
    }

    /**
     * Click at text syarat dan ketentuan umum
     *
     * @throws InterruptedException
     */
    public void clickOnTextSyarat() throws InterruptedException {
        selenium.javascriptClickOn(textSyarat);
    }

    /**
     * Verify pop up syarat ketentuan is present
     *
     * @return pop up syarat ketentuan GP present
     */
    public boolean isPopUpSyaratPresent() {
        return selenium.waitInCaseElementVisible(syaratKetentuanPopup, 2) != null;
    }


    /**
     * Click at icon close pop up syarat dan ketentuan GP
     *
     * @throws InterruptedException
     */
    public void clickOnIconCLosePopUp() throws InterruptedException {
        selenium.javascriptClickOn(iconClosePopUpSyarat);
    }

    /**
     * Click at icon close pop up syarat dan ketentuan GP
     *
     * @throws InterruptedException
     */
    public void clickOnButtonSayaMengerti() throws InterruptedException {
        selenium.javascriptClickOn(sayaMengertiButton);
    }

}

