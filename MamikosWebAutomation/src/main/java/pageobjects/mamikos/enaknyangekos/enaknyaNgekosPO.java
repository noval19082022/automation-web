package pageobjects.mamikos.enaknyangekos;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.Objects;

public class enaknyaNgekosPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public enaknyaNgekosPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }


    @FindBy(css = ".enaknyangekos-header-content__title")
    private WebElement onLandingpageEnaknyangekos;

    @FindBy(css = ".enaknyangekos-benefits__title")
    private WebElement onLandingpage;

    @FindBy(css = ".enaknyangekos-navbar__container")
    private WebElement viewMulaiCariKosButton;

    @FindBy(css = "[href='/booking']")
    private WebElement bookingKosButton;

    @FindBy(xpath = "//strong[contains(.,'Mudah cari kostnya, Mudah bookingnya, Mudah bayarnya.')]")
    private WebElement directToMenuBooking;

    @FindBy(css = "nav[id='navbar'] li:nth-child(2)")
    private WebElement promoButton;

    @FindBy(css = ".enaknyangekos-voucher__list > div:nth-of-type(1)")
    private WebElement voucherButton;

    @FindBy(xpath = "//h3[normalize-space()='#EnaknyaNgekos di Kos Andalan']")
    private WebElement autoscrollToVoucher;

    @FindBy(css = ".text-no-accordion h2 > [data-v-f5047aca]")
    private WebElement directToMenuPromo;

    @FindBy(css = "[href='https://twitter.com/mamikosapp']")
    private WebElement twitterIcon;

    @FindBy(css = "[href='https://www.instagram.com/mamikosapp/']")
    private WebElement instagramIcon;

    @FindBy(css = "a[href='https://www.facebook.com/Mamikos-Cari-Kos-Gampang-1043376389026435/?fref=ts']")
    private WebElement facebookIcon;

    @FindBy(css = "div:nth-of-type(4) > h5")
    private WebElement hubungiKami;

    @FindBy(xpath = "//div[@class='enaknyangekos-header-video']")
    private WebElement videoThumbnail;

    @FindBy(xpath = "//div[@class='youtube-embed__play-btn']")
    private WebElement playVideo;

    @FindBy(className = "enaknyangekos-header-video__embed")
    private WebElement viewVideo;

    @FindBy(css = ".enaknyangekos-menu__list > li:nth-of-type(3) > .enaknyangekos-menu__link")
    private WebElement fiturUnggulanButton;

    @FindBy(className = "enaknyangekos-feature__title")
    private WebElement autoscrollToFiturUnggulan;

    @FindBy(xpath = "//a[contains(.,'Produk dan Layanan')]")
    private WebElement productDanLayananButton;

    @FindBy(css = ".enaknyangekos-benefits__title")
    private WebElement autoscrollToProductDanLayanan;

    @FindBy(css = "[alt='GET IT ON Google Play']")
    private WebElement googleplayButton;

    @FindBy(css = "[alt='Available on the App Store']")
    private WebElement playstoreButton;

    @FindBy(xpath = "//p[.='cs@mamikos.com']")
    private WebElement emailButton;

    @FindBy(xpath = "//button[normalize-space()='Mulai cari kos']")
    private WebElement mulaiCariKos;

    @FindBy(xpath = "//p[.='0813-2511-1171']")
    private WebElement whatsappButton;

    @FindBy(xpath = "//form[1]")
    private WebElement sendEmailPopup;

    @FindBy(xpath = "//h3[.='Form Bantuan']")
    private WebElement emailText;

    @FindBy(xpath = "//button[@class='bg-c-button enaknyangekos-tiering__button bg-c-button--secondary bg-c-button--lg']")
    private WebElement cariApikButton;

    /**
     * on landing page enaknyangekos
     * @return
     */

    public String onLandingpageEnaknyangekos() {
        return selenium.getText(onLandingpageEnaknyangekos);
    }

    /**
     * scroll page
     */

    public void scrollPageEnaknyangekos()  {
        selenium.pageScrollInView(onLandingpage);
    }

    /**
     * button Show mulai cari kos
     */

    public void viewMulaiCariKosButton() throws InterruptedException {
        selenium.clickOn(viewMulaiCariKosButton);
    }

    /**
     * click booking kos on header
     */

    public void clickBookingKosButton() throws InterruptedException {
        selenium.clickOn(bookingKosButton);
    }

    /**
     * click promo kos on header
     */

    public void clickPromoButton() throws InterruptedException {
        selenium.clickOn(promoButton);
        selenium.pageScrollInView(autoscrollToVoucher);
        selenium.hardWait(10);
    }

    /**
     * click voucher button
     */

    public void clickVoucherButton() throws InterruptedException {
        selenium.clickOn(voucherButton);
        selenium.navigateToPage("https://promo.mamikos.com/");
    }

    /**
     * click fitur unggulan button
     */

    public void clickFiturUnggulanButton() throws InterruptedException {
        selenium.clickOn(fiturUnggulanButton);
    }

    /**
     * click product dan layanan button
     */

    public void clickProductDanLayananButton() throws InterruptedException {
        selenium.clickOn(productDanLayananButton);
    }

    /**
     * click twitter icon
     */

    public void clickTwitterIcon() throws InterruptedException {
        selenium.pageScrollInView(hubungiKami);
        selenium.clickOn(twitterIcon);
        selenium.switchToWindow(2);
        selenium.hardWait(7);
    }

    /**
     * click instagram icon
     */

    public void clickInstagramIcon() throws InterruptedException {
        selenium.pageScrollInView(hubungiKami);
        selenium.clickOn(instagramIcon);
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * click facebook icon
     */

    public void clickFacebookIcon() throws InterruptedException {
        selenium.pageScrollInView(hubungiKami);
        selenium.clickOn(facebookIcon);
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * click booking kos on header
     * @return
     */

    public String directToMenuBooking(){
        return selenium.getText(directToMenuBooking);
    }

    /**
     * autoscroll to view voucher
     * @return
     */

    public String autoscrollToVoucher() {
        return selenium.getText(autoscrollToVoucher);
    }

    /**
     * autoscroll to view fitur unggulan
     * @return
     */

    public String autoscrollToFiturUnggulan() throws InterruptedException {
        selenium.pageScrollInView(autoscrollToFiturUnggulan);
        selenium.hardWait(6);
        return selenium.getText(autoscrollToFiturUnggulan);
    }

    /**
     * autoscroll to view product dan layanan
     * @return
     */

    public String autoscrollToProductDanLayanan() throws InterruptedException {
        selenium.pageScrollInView(autoscrollToProductDanLayanan);
        selenium.hardWait(6);
        return selenium.getText(autoscrollToProductDanLayanan);
    }

    /**
     * user direct to menu promo
     * @return
     */

    public String directToMenuPromo() {
        selenium.pageScrollInView(directToMenuPromo);
        return selenium.getText(directToMenuPromo);
    }

    /**
     * user direct to website twitter
     */

    public void directToWebsite() throws InterruptedException {
        selenium.hardWait(10);
    }

    /**
     * click video thumbnail
     */

    public void clickVideoThumbnail() throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//div[@class='enaknyangekos-header-video']"));
        selenium.waitTillElementIsClickable(element);
        selenium.hardWait(5);
        selenium.clickOn(element);
    }

    /**
     * user view video
     */
    public void viewVideo() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(playVideo);
        selenium.hardWait(30);
    }

    /**
     * user click footer
     */
    public void clickOnFooter(String button) throws InterruptedException {
        selenium.pageScrollInView(hubungiKami);
        if(button.equalsIgnoreCase("Google Play")) {
            selenium.clickOn(googleplayButton);
        }
        else if(button.equalsIgnoreCase("App Store")) {
            selenium.clickOn(playstoreButton);
        }
        else if(button.equalsIgnoreCase("Whatsapp Number")) {
            selenium.clickOn(whatsappButton);
        }
        else if(button.equalsIgnoreCase("email")) {
            selenium.clickOn(emailButton);
        }
        else if(button.equalsIgnoreCase("Mulai cari kos")) {
            selenium.hardWait(2);
            selenium.javascriptClickOn(mulaiCariKos);
            selenium.navigateToPage("https://jambu.kerupux.com/kos/andalan");}
        else{
            selenium.hardWait(5);
            selenium.clickOn(By.xpath("//h6[.='"+button+"']"));
        }
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * user view Email
     */
    public String viewSendEmail() throws InterruptedException {
        selenium.waitTillElementIsVisible(sendEmailPopup);
        return selenium.getText(emailText);
    }

    /**
     * user search APIK button
     */
    public void clickCariAPIK() throws InterruptedException {
        selenium.pageScrollInView(By.xpath("//div[@class='enaknyangekos-tiering__details bg-c-grid__item bg-is-col-6']"));
        selenium.hardWait(2);
        selenium.clickOn(cariApikButton);
        selenium.switchToWindow(2);
        selenium.hardWait(3);
    }
}

