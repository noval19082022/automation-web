package pageobjects.mamikos.homepage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class HomepagePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public HomepagePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    //--------------- Banner Promo ------------------
    @FindBy(css = ".swiper-button-prev.swiper-arrow")
    private WebElement previousBannerPromoButton;

    @FindBy(css = ".swiper-button-next.swiper-arrow")
    private WebElement nextBannerpromoButton;

    @FindBy(css = ".promo-banner__navigation-link")
    private WebElement seeAllPromoButton;

    @FindBy(xpath = "//div[@class='bg-c-swiper']//div[contains(@class, 'swiper-slide-active')]")
    private WebElement firstBanner;

    @FindBy(xpath = "//div[@class='bg-c-swiper']//div[contains(@class, 'swiper-slide-next')]")
    private WebElement leftBanner;

    @FindBy(xpath = "//div[@class='bg-c-swiper']//div[contains(@class, 'swiper-slide-active')]/following-sibling::div[1]")
    private WebElement lastBanner;

    @FindBy(xpath = "//div[@class='bg-c-swiper']//div[contains(@class, 'swiper-slide-active')]/following-sibling::div[2]")
    private WebElement nextBanner;

    //-------------- Testimonial Owner ----------------

    @FindBy (css = "#testimonial > div.testimonial-cta > h3 > a")
    private WebElement kosRegisterButton;

    @FindBy(css = "#testimonial > h2")
    private WebElement testimonialTitle;

    private By testimonial = By.cssSelector("#testimonial .swiper-wrapper");

    private By ownerImg = By.cssSelector("#testimonial .testimonial-item:nth-child(%s) .testimonial-identity .identity-image");

    private By ownerName = By.cssSelector("#testimonial .testimonial-item:nth-child(%s) .testimonial-identity div:nth-child(2) h3");

    private By kosName = By.cssSelector("#testimonial .testimonial-item:nth-child(%s) .testimonial-identity div:nth-child(2) p");

    private By testimonialDesc = By.cssSelector("#testimonial .testimonial-item:nth-child(%s) .testimonial-content");

    private By homeId = By.cssSelector("#app #home");


    @FindBy (css = "#testimonial .testimonial-wrapper .testimonial-swiper-button-next")
    private WebElement nextButton;

    @FindBy(className = "testimonial-swiper-button-prev")
    private WebElement prevButton;

    @FindBy(xpath = "//div[@class='testimonial-wrapper']//div[contains(@class, 'swiper-slide-active')]//div[2]")
    private WebElement firstTestimonial;

    @FindBy(xpath = "//div[@class='testimonial-wrapper']//div[contains(@class, 'swiper-slide-next')]//div[2]")
    private WebElement prevTestimonial;

    @FindBy(xpath = "//div[@class='testimonial-wrapper']//div[contains(@class, 'swiper-slide-active')]/following-sibling::div[2]//div[2]")
    private WebElement lastTestimonial;

    @FindBy(xpath = "//div[@class='testimonial-wrapper']//div[contains(@class, 'swiper-slide-next')]//div[2]")
    private WebElement nextTestimonial;

    //--------------- Searchbar ------------------
    @FindBy(css = ".home-top-container .home-top-search-title")
    private WebElement searchTitle;

    @FindBy(css = ".home-top-container .home-top-search-subtitle")
    private WebElement searchDesc;

    @FindBy(css = ".home-top-container .btn-cta-label")
    private WebElement searchLabel;

    @FindBy(css = ".home-top-container .btn-cta-search-label")
    private WebElement searchButton;

    //-------------- Recommendation section --------------
    @FindBy(className = "recommendation-header")
    private WebElement recommendationFilter;

    @FindBy(css = "#recommendation #userLocation")
    private WebElement filterlocation;

    @FindBy(css = "#recommendation #userLocation option:nth-child(1)")
    private WebElement activeLocation;

    @FindBy(css = "#recommendation .recommendation-wrapper > div > div")
    private WebElement roomCardRecommendation;

    @FindBy(xpath = "(//div[@class='recommendation-item__room']//span[contains(@class, 'rc-info__name')])[4]")
    private WebElement lastRoomCardRecommendation;

     @FindBy(css = "#recommendation .slider-nav__next")
    private WebElement nextButtonRecommendation;

    @FindBy(css = "#recommendation .slider-nav__prev")
    private WebElement prevButtonRecommendation;

    @FindBy(css = "#recommendation .slider-nav .slider-nav__link")
    private WebElement seeAllRecommenButton;

    private By popularCity = By.xpath("//*[@id='recommendation']//select/option");

    //-------------- Promotion section ---------------
    @FindBy(className = "promoted-header")
    private WebElement promotionFilter;

    @FindBy(css = "#promoted #userLocation")
    private WebElement filterLocationPromo;

    @FindBy(css = "#promoted .promoted-wrapper > div > div")
    private WebElement roomCardPromotion;

    @FindBy(css = "#promoted .slider-nav .slider-nav__link")
    private WebElement seeAllPromoKosButton;

    @FindBy(xpath = "//div[@class='promoted-item swiper-slide swiper-slide-active']/div[last()]//span[contains(@class, 'rc-info__name')]")
    private WebElement lastRoomTitlePromoKos;

    @FindBy(css = "#promoted .slider-nav__next")
    private WebElement nextButtonPromoted;

    @FindBy(css = "#promoted .slider-nav__prev")
    private WebElement prevButtonPromoted;

    private By promotionCity = By.xpath("//*[@id='promoted']//*[@id='userLocation']/option");

    @FindBy(css = "#promoted .promoted-wrapper .rc-info__name")
    private WebElement promoThumbnailTitleText;

    //------------- popular area section ---------------

    @FindBy(css = ".popular-area-cards .popular-area-card-item:nth-child(2)")
    private WebElement secondPopularCity;

    @FindBy(css = ".popular-area-cards .popular-area-card-item:nth-child(3)")
    private WebElement thirdPopularCity;

    @FindBy(css = ".kost-area-populer-home")
    private WebElement seeAllPopularAreaButton;

    private By popularArea = By.cssSelector(".popular-area .popular-area-cards > a");

    //------------- popular campus area section ---------------

    @FindBy(css = ".kost-kampus-home")
    private WebElement seeAllCampusAreaButton;

    private By kostCampusArea = By.cssSelector(".popular-campus .popular-campus-item > div > h3:first-child");

    private By kostCityCampusArea = By.cssSelector(".popular-campus .popular-campus-item > div > h3:last-child");

    //------------- Footer homepage section ---------------
    @FindBy(css = "#home > footer.mami-footer > div.container > div >div.product-identity > img")
    private WebElement logoMamikosFooter;

    @FindBy(css = "#home > footer > div.container > div > div.product-identity > div.product-download > a:nth-child(1) > img")
    private WebElement googlePlayStore;

    @FindBy(css = "#home > footer > div.container > div > div.product-identity > div.product-download > a:nth-child(2) > img")
    private WebElement appStore;

    @FindBy(css = "#home > footer > div.container > div > div:nth-child(2) > a:nth-child(2) > h6")
    private WebElement tentangKamiLink;

    @FindBy(css = "#home > footer > div.container > div > div:nth-child(2) > a:nth-child(3) > h6")
    private WebElement jobMamikosLink;

    @FindBy(css = "#home > footer > div.container > div > div:nth-child(2) > a:nth-child(4)")
    private WebElement promosikanIklanFooterLink;

    @FindBy(css = ".mami-footer .site-links a:nth-child(5)")
    private WebElement helpCenterLink;

    @FindBy(css = "#home > footer > div.container > div > div:nth-child(4) > a:nth-child(2) > h6")
    private WebElement kebijakanPrivasiLink;

    @FindBy(css = "#home > footer > div.container > div > div:nth-child(4) > a:nth-child(3) > h6")
    private WebElement syaratDanKetentuanLink;

    @FindBy(css = "#home > footer > div.container > div > div:nth-child(2) > h5")
    private WebElement mamikosBottomText;

    @FindBy(css = ".site-links-social a:nth-child(1)")
    private WebElement footerFBIcon;

    @FindBy(css = "#home > footer > div.footer.container > div.footer__social > a:nth-child(2)")
    private WebElement footerTwitterIcon;

    @FindBy(css = ".site-links-social a:nth-child(3)")
    private WebElement footerIGIcon;

    @FindBy(css = ".site-links-contact > a:nth-child(2)")
    private WebElement WA1;

    @FindBy(css = ".whatsapp-contact-list:nth-child(2) a:nth-child(3)")
    private WebElement WA2;

    @FindBy(css = ".whatsapp-contact-list:nth-child(3) a:nth-child(1)")
    private WebElement WA3;

    @FindBy(css = ".site-links-contact > a:nth-child(1)")
    private WebElement footerEmailIcon;

    @FindBy(css = "#home  .footer.container .footer__copyright")
    private WebElement copyrighttext;

    @FindBy(css = ".site-links-contact > a:nth-child(2)")
    private WebElement WANumberText;

    @FindBy(xpath = "//div[@class='promoted-item swiper-slide swiper-slide-active']//div[@class='promoted-item__room'][1]")
    private WebElement firstPromoKostThumbnail;


    /**
     * Check first banner is displayed
     * @return true / false
     */
    public boolean isFirstBannerPresent() {
        return selenium.waitInCaseElementVisible(firstBanner, 1) != null;
    }

    /**
     * Get first banner index
     *
     * @return index
     */
    public String getActiveBannerIndex() {
        return selenium.getElementAttributeValue(firstBanner, "data-swiper-slide-index");
    }

    /**
     * Click previous button
     *
     * @throws InterruptedException
     */
    public void clickOnPreviousButton() throws InterruptedException {
        selenium.clickOn(previousBannerPromoButton);
    }

    /**
     * Get left banner index
     *
     * @return index
     */
    public String getPreviousBannerIndex() {
        return selenium.getElementAttributeValue(leftBanner, "data-swiper-slide-index");
    }

    /**
     * Check third banner is displayed
     * @return true / false
     */
    public boolean isThirdBannerPresent() {
        return selenium.waitInCaseElementVisible(lastBanner, 1) != null;
    }

    /**
     * Get last banner index
     *
     * @return index
     * @throws InterruptedException
     */
    public String getLastBannerIndex() {
        return selenium.getElementAttributeValue(lastBanner, "data-swiper-slide-index");
    }

    /**
     * Click next button
     *
     * @throws InterruptedException
     */
    public void clickOnNextButton() throws InterruptedException {
        selenium.clickOn(nextBannerpromoButton);
    }

    /**
     * Get next banner index
     *
     * @return index
     */
    public String getNextBannerIndex() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.getElementAttributeValue(nextBanner, "data-swiper-slide-index");
    }

    /**
     * Click "Lihat Semua" promos button
     *
     * @throws InterruptedException
     */
    public void clickOnSeeAllButton() throws InterruptedException {
        selenium.javascriptClickOn(seeAllPromoButton);
    }

    /**
     * Get URL
     *
     * @return URL
     * @throws InterruptedException
     */
    public String getURL() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.getURL();
    }

    /**
     * Scroll down to Kos register button
     */
    public void scrollDownToTestimonial() {
        selenium.pageScrollUsingCoordinate(0, 1500);
//        selenium.waitInCaseElementVisible(testimonialTitle,10);
        selenium.pageScrollInView(testimonialTitle);
    }

    /**
     * Check the Title is present
     *
     * @return status true / false
     */
    public boolean isTestimonialTitlePresent() {
        return selenium.waitInCaseElementVisible(testimonialTitle, 3) != null;
    }

    /**
     * Convert %s to index 1, 2, 3, of testimonialIndex
     *
     * @param a     fill with element
     * @param index fill with integer data
     * @return By.cssSelector after reformat
     */
    public By testimonialIndex(By a, int index) {
        String testimonialTarget = a.toString().replace("By.cssSelector: ", "");
        testimonialTarget = String.format(testimonialTarget, Integer.toString(index));
        return By.cssSelector(testimonialTarget);
    }

    /**
     * Get total index of testimonial
     *
     * @return size (total index)
     */
    public int getTestimonialSize() {
        List<WebElement> element = driver.findElements(testimonial);
        int size = element.size();
        return size;
    }

    /**
     * Check the owner image is present
     *
     * @return status true / false
     * @throws InterruptedException
     */
    public boolean isOwnerImgPresent(int index) throws InterruptedException {
        return selenium.isElementPresent(testimonialIndex(ownerImg, index));
    }

    /**
     * Check the owner name is present
     *
     * @return status true / false
     * @throws InterruptedException
     */
    public boolean isOwnerNamePresent(int index) throws InterruptedException {
        return selenium.isElementPresent(testimonialIndex(ownerName, index));
    }

    /**
     * Check the owner kos name is present
     *
     * @return status true / false
     * @throws InterruptedException
     */
    public boolean isOwnerKosNamePresent(int index) throws InterruptedException {
        return selenium.isElementPresent(testimonialIndex(kosName, index));
    }

    /**
     * Check the testimonial desc is present
     *
     * @return status true / false
     * @throws InterruptedException
     */
    public boolean isTestimonialDescPresent(int index) throws InterruptedException {
        return selenium.isElementPresent(testimonialIndex(testimonialDesc, index));
    }

    /**
     * Click next button
     *
     * @throws InterruptedException
     */
    public void clickNextButton() throws InterruptedException {
        selenium.clickOn(nextButton);
    }

    /**
     * Click prev button
     *
     * @throws InterruptedException
     */
    public void clickPrevButton() throws InterruptedException {
        selenium.clickOn(prevButton);
    }

    /**
     * Get last testimonial element
     *
     * @return string
     */
    public String getLastTestimonialElement() {
        return selenium.getText(lastTestimonial);
    }

    /**
     * Get next testimonial element
     *
     * @return string
     */
    public String getNextTestimonialElement() {
        return selenium.getText(nextTestimonial);
    }

    /**
     * Get previous testimonial element
     *
     * @return string
     */
    public String getPrevTestimonialElement() {
        return selenium.getText(prevTestimonial);
    }

    /**
     * Get first testimonial element
     *
     * @return string
     */
    public String getFirstTestimonialElement() {
        return selenium.getText(firstTestimonial);
    }

    /**
     * Get Search Title Text
     *
     * @return variable
     */
    public String getSearchTitleText() {
        String title = selenium.getText(searchTitle);
        return title;
    }

    /**
     * Get Search Desc Text
     *
     * @return variable
     */
    public String getSearchDescText() {
        String desc = selenium.getText(searchDesc);
        return desc;
    }

    /**
     * Get Search Label Text
     *
     * @return variable
     */
    public String getSearchLabel() {
        String label = selenium.getText(searchLabel);
        return label;
    }

    /**
     * Check element Search Button is displayed
     *
     * @return status true / false
     */
    public boolean isSearchButtonDisplayed() {
        return searchButton.isDisplayed();
    }

    /**
     * Check redirection kos register button
     *
     * @return url is equal
     * @throws InterruptedException
     */
    public String getRegisterKosURL() throws InterruptedException {
        selenium.clickOn(kosRegisterButton);
        selenium.waitForJavascriptToLoad();
        return selenium.getURL();
    }

    //------------- recommendation section ---------------

    /**
     * Scroll down to Kos recommendation button
     */
    public void scrollDownToRecommendation() {
        selenium.pageScrollUsingCoordinate(0, 1500);
//        selenium.waitInCaseElementVisible(kosRegisterButton, 10);
        selenium.pageScrollInView(kosRegisterButton);
    }

    /**
     * Check element city filter is displayed
     *
     * @return status true / false
     */
    public boolean isCityFilterDisplayed() {
        return selenium.waitInCaseElementVisible(recommendationFilter, 5) != null;
    }

    /**
     * Check element room card recommendation is displayed
     *
     * @return status true / false
     */
    public boolean isRoomCardRecommendationDisplayed() {
        return selenium.waitInCaseElementVisible(roomCardRecommendation, 5) != null;
    }

    /**
     * Check element see all recommendation button is displayed
     *
     * @return status true / false
     */
    public boolean isSeeAllRecomendationDisplayed() {
        return selenium.waitInCaseElementVisible(seeAllRecommenButton, 5) != null;
    }

    /**
     * Click on Location button
     * @throws InterruptedException
     */
    public void clickOnLocationKosRecommendation() throws InterruptedException {
        this.scrollDownToRecommendation();
        selenium.clickOn(filterlocation);
    }

    /**
     * List all popular search city
     *
     * @return popular city
     */
    public List<String> listPopularCity() {
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(popularCity);
        List<String> popularCity = new ArrayList<>();
        for (WebElement s : elements) {
            popularCity.add((selenium.getText(s)));
        }
        return popularCity;
    }

    /**
     * Get last recommendation title element
     *
     * @return string
     */
    public String getLastRoomRecommendation() {
        return selenium.getText(lastRoomCardRecommendation);
    }

    /**
     * Click next button on recommendation section
     *
     * @throws InterruptedException
     */
    public void clickNextRecommendationButton() throws InterruptedException {
        selenium.clickOn(nextButtonRecommendation);
        selenium.hardWait(2);
    }

    /**
     * Click prev button on recommendation section
     *
     * @throws InterruptedException
     */
    public void clickPrevRecommendationButton() throws InterruptedException {
        selenium.clickOn(prevButtonRecommendation);
        selenium.hardWait(2);
    }

    /**
     * Click see all button on recommendation section
     *
     * @throws InterruptedException
     */
    public void clickSeeAllRecommenButton() throws InterruptedException {
        selenium.clickOn(seeAllRecommenButton);
    }

    /**
     * Check if you are in homepage
     *
     * @return true if home id present otherwise false
     * @throws InterruptedException
     */
    public boolean isHomepageId() throws InterruptedException {
        return selenium.isElementPresent(homeId);
    }
    //------------- promotion section ---------------

    /**
     * Scroll down to see all recommendation button
     */
    public void scrollDownToPromotion() {
        selenium.pageScrollUsingCoordinate(0, 2000);
//        selenium.waitInCaseElementVisible(lastRoomCardRecommendation, 10);
        selenium.pageScrollInView(lastRoomCardRecommendation);
    }

    /**
     * Check element city filter promotion is displayed
     *
     * @return status true / false
     */
    public boolean isCityFilterPromotionDisplayed() {
        return selenium.waitInCaseElementVisible(promotionFilter, 5) != null;
    }

    /**
     * Check element room card promotion is displayed
     *
     * @return status true / false
     */
    public boolean isRoomCardPromotionDisplayed() {
        return selenium.waitInCaseElementVisible(roomCardPromotion, 5) != null;
    }

    /**
     * Check element see all promotion button is displayed
     *
     * @return status true / false
     */
    public boolean isSeeAllPromotionDisplayed() {
        return selenium.waitInCaseElementVisible(seeAllPromoKosButton, 5) != null;
    }

    /**
     * Click on Location button promotion section
     * @throws InterruptedException
     */
    public void clickOnLocationKosPromotion() throws InterruptedException {
        this.scrollDownToRecommendation();
        selenium.clickOn(filterLocationPromo);
    }

    /**
     * List all promo kos city
     *
     * @return list of promo kos city
     */
    public List<String> listPromotionCity() {
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(promotionCity);
        List<String> popularCity = new ArrayList<>();
        for (WebElement s : elements) {
            popularCity.add((selenium.getText(s)));
        }
        return popularCity;
    }

    /**
     * Get last promo kos title element
     *
     * @return string
     */
    public String getLastRoomTitlePromo() {
        return selenium.getText(lastRoomTitlePromoKos);
    }

    /**
     * Click next button on promotion section
     *
     * @throws InterruptedException
     */
    public void clickNextPromoButton() throws InterruptedException {
        selenium.clickOn(nextButtonPromoted);
        selenium.hardWait(2);
    }

    /**
     * Click prev button on promotion section
     *
     * @throws InterruptedException
     */
    public void clickPrevPromoButton() throws InterruptedException {
        selenium.clickOn(prevButtonPromoted);
        selenium.hardWait(2);
    }

    /**
     * Click see all button on promotion section
     *
     * @throws InterruptedException
     */
    public void clickSeeAllPromoButton() throws InterruptedException {
        selenium.clickOn(seeAllPromoKosButton);
    }

    /**
     * Get text city recommendation
     *
     * @return string
     */
    public String getCityRecommendation() {
        return selenium.getText(activeLocation);
    }

    //------------- popular area section ---------------

    /**
     * Scroll down to see all promo button
     */
    public void scrollDownToPopularArea() {
        selenium.pageScrollUsingCoordinate(0, 3000);
        selenium.waitInCaseElementVisible(seeAllPromoKosButton, 10);
        selenium.pageScrollInView(seeAllPromoKosButton);
    }

    /**
     * List all popular area city
     *
     * @return popular city
     */
    public List<String> listPopularAreaKos() {
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(popularArea);
        List<String> popularCity = new ArrayList<>();
        for (WebElement s : elements) {
            popularCity.add((selenium.getText(s)));
        }
        return popularCity;
    }

    /**
     * Get text second popular city
     *
     * @return string
     */
    public String getSecondPopularCityText() {
        return selenium.getText(secondPopularCity);
    }

    /**
     * Click text second popular area city
     * @throws InterruptedException
     */
    public void clickSecondPopularAreaCityText() throws InterruptedException {
        selenium.clickOn(secondPopularCity);
    }

    /**
     * Get text third popular city
     *
     * @return string
     */
    public String getThirdPopularCityText() {
        return selenium.getText(thirdPopularCity);
    }

    /**
     * Click text third popular area city
     * @throws InterruptedException
     */
    public void clickThirdPopularAreaCityText() throws InterruptedException {
        selenium.clickOn(thirdPopularCity);
    }

    /**
     * Click see all button popular area
     * @throws InterruptedException
     */
    public void clickSeeAllPopularArea() throws InterruptedException {
        selenium.waitInCaseElementVisible(seeAllPopularAreaButton,3);
        selenium.javascriptClickOn(seeAllPopularAreaButton);
    }

    //------------- Kost Around Campus Section---------------

    /**
     * Scroll down to see all promo button
     */
    public void scrollDownToKostAroundCampus() throws InterruptedException {
        selenium.pageScrollUsingCoordinate(0, 3600);
//        selenium.waitInCaseElementVisible(seeAllCampusAreaButton, 10);
//        selenium.pageScrollInView(seeAllCampusAreaButton);
    }

    /**
     * List all popular campus area
     *
     * @return list of campus
     */
    public List<String> listPopularAreaCampusKos() {
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(kostCampusArea);
        List<String> campus = new ArrayList<>();
        for (WebElement s : elements) {
            campus.add((selenium.getText(s)));
        }
        return campus;
    }

    /**
     * List all popular area campus city
     *
     * @return list of city
     */
    public List<String> listPopularAreaCityCampusKos() {
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(kostCityCampusArea);
        List<String> city = new ArrayList<>();
        for (WebElement s : elements) {
            city.add((selenium.getText(s)));
        }
        return city;
    }

    /**
     * Click see all button campus area
     * @throws InterruptedException
     */
    public void clickSeeAllCampusArea() throws InterruptedException {
        selenium.clickOn(seeAllCampusAreaButton);
    }

    //------------- Footer homepage Section---------------

    /**
     * Check logo mamikos footer is displayed
     *
     * @return status true / false
     */
    public boolean isLogoMamikosDisplayed() {
        return selenium.waitInCaseElementVisible(logoMamikosFooter, 5)!= null;
    }

    /**
     * Check google play store is displayed
     *
     * @return status true / false
     */
    public boolean isGooglePlayStoreDisplayed() {
        return selenium.waitInCaseElementVisible(googlePlayStore, 5)!= null;
    }

    /**
     * Check app store is displayed
     *
     * @return status true / false
     */
    public boolean isAppStoreDisplayed() {
        return selenium.waitInCaseElementVisible(appStore, 5)!= null;
    }

    /**
     * Check tentang kami button is displayed
     *
     * @return status true / false
     */
    public boolean isTentangKamiDisplayed() {
        return selenium.waitInCaseElementVisible(tentangKamiLink, 5)!= null;
    }

    /**
     * Check job mamikos button is displayed
     *
     * @return status true / false
     */
    public boolean isJobMamikosDisplayed() {
        return selenium.waitInCaseElementVisible(jobMamikosLink, 5)!= null;
    }

    /**
     * Check promosi iklan anda footer button is displayed
     *
     * @return status true / false
     */
    public boolean isPromosiIklanAndaGratisDisplayed() {
        return selenium.waitInCaseElementVisible(promosikanIklanFooterLink, 5)!= null;
    }

    /**
     * Check pusat bantuan button is displayed
     *
     * @return status true / false
     */
    public boolean isPusatBantuanDisplayed() {
        return selenium.waitInCaseElementVisible(helpCenterLink, 5)!= null;
    }

    /**
     * Check kebijakan privasi button is displayed
     *
     * @return status true / false
     */
    public boolean isKebijakanPrivasiDisplayed() {
        return selenium.waitInCaseElementVisible(kebijakanPrivasiLink, 5)!= null;
    }

    /**
     * Check syarat dan ketentuan button is displayed
     *
     * @return status true / false
     */
    public boolean isSyaratDanKetentuanUmumDisplayed() {
        return selenium.waitInCaseElementVisible(syaratDanKetentuanLink, 5)!= null;
    }

    /**
     * Scroll down to bottom mamikos page
     */
    public void scrollDownToBottomMamikos() {
        selenium.waitInCaseElementVisible(mamikosBottomText, 10);
        selenium.pageScrollInView(mamikosBottomText);
    }

    /**
     * Click button google play store
     *
     * @throws InterruptedException
     */
    public void clickButtonGooglePlayStore() throws InterruptedException {
        selenium.clickOn(googlePlayStore);
    }

    /**
     * Click button app store
     *
     * @throws InterruptedException
     */
    public void clickButtonGAppStore() throws InterruptedException {
        selenium.clickOn(appStore);
        selenium.hardWait(2);
    }

    /**
     * Click button tentang kami
     *
     * @throws InterruptedException
     */
    public void clickLinkTentangKami() throws InterruptedException {
        selenium.clickOn(tentangKamiLink);
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * Click button job mamikos
     *
     * @throws InterruptedException
     */

    public void clickJobMamikosLink() throws InterruptedException {
        selenium.clickOn(jobMamikosLink);
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * Click button promosikan iklan gratis
     *
     * @throws InterruptedException
     */
    public void clickPromosikanIklanGratisLink() throws InterruptedException {
        selenium.clickOn(promosikanIklanFooterLink);
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * Click button kebijakan privacy
     *
     * @throws InterruptedException
     */
    public void clickKebijakanPrivasiLink() throws InterruptedException {
        selenium.clickOn(kebijakanPrivasiLink);
        selenium.switchToWindow(0);
    }
    /**
     * Check redirection help center link
     * @return url is equal
     * @throws InterruptedException
     */
    public String getHelpCenterLinkURL() throws InterruptedException {
        selenium.clickOn(helpCenterLink);
        selenium.switchToWindow(0);
        selenium.hardWait(5);
        return selenium.getURL();
    }
    /**
     * Check redirection help center link
     * @return url is equal
     * @throws InterruptedException
     */
    public String getSyaratKetentuanLinkURL() throws InterruptedException {
        selenium.clickOn(syaratDanKetentuanLink);
        selenium.switchToWindow(0);
        return selenium.getURL();
    }

    /**
     * Check redirection google play store link
     * @return url is equal
     * @throws InterruptedException
     */
    public String getGooglePlayStoreLinkURL() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.hardWait(5);
        return selenium.getURL();
    }

    /**
     * Check redirection app store link
     * @return url is equal
     * @throws InterruptedException
     */
    public String getAppStoreLinkURL() throws InterruptedException {
        selenium.switchToWindow(2);
        selenium.hardWait(5);
        return selenium.getURL();
    }

    /**
     * Click title thumbnail
     * Switch to other window
     * @throws InterruptedException
     */
    public void clickPromoThumbnail() throws InterruptedException {
        selenium.waitInCaseElementVisible(promoThumbnailTitleText, 5);
        selenium.clickOn(promoThumbnailTitleText);
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * Get title kos name
     * @return Kos name
     */
    public String getThumbnailTitle() {
        return selenium.getText(promoThumbnailTitleText);
    }

    /**
     * Click on facebook icon
     * Switch window
     * @throws InterruptedException
     */
    public void clickFacebookIcon() throws InterruptedException {
        selenium.waitInCaseElementVisible(footerFBIcon,3);
        selenium.clickOn(footerFBIcon);
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * Click on twitter icon
     * Switch window
     * @throws InterruptedException
     */
    public void clickTwitterIcon() throws InterruptedException {
        selenium.waitInCaseElementVisible(footerTwitterIcon,3);
        selenium.clickOn(footerTwitterIcon);
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * Click on instagram icon
     * Switch window
     * @throws InterruptedException
     */
    public void clickInstagramIcon() throws InterruptedException {
        selenium.waitInCaseElementVisible(footerIGIcon,3);
        selenium.clickOn(footerIGIcon);
        selenium.switchToWindow(2);
        selenium.hardWait(5);
    }

    /**
     * Get Whatsapp text number 1
     * @return wa text
     */
    public String getWhatsappNumber1Text() {
        selenium.waitInCaseElementVisible(WA1,3);
        return selenium.getText(WA1);
    }

    /**
     * Get Whatsapp text number 2
     * @return wa text
     */
    public String getWhatsappNumber2Text() {
        selenium.waitInCaseElementVisible(WA1,3);
        return selenium.getText(WA2);
    }

    /**
     * Get Whatsapp text number 3
     * @return wa text
     */
    public String getWhatsappNumber3Text() {
        selenium.waitInCaseElementVisible(WA1,3);
        return selenium.getText(WA3);
    }

    /**
     * Get email text
     * @return email text
     */
    public String getEmailText(){
        selenium.waitInCaseElementVisible(footerEmailIcon,3);
        return selenium.getText(footerEmailIcon);
    }

    /**
     * Get copyright text
     * @return email text
     */
    public String getCopyrightText(){
        selenium.waitInCaseElementVisible(copyrighttext,3);
        return selenium.getText(copyrighttext);
    }

    /**
     * Scroll down to Kos register button
     */
    public void scrollDownToKosRegister() {
        selenium.pageScrollUsingCoordinate(0, 1500);
    }

    /**
     * Click first promoted kost thumbnail
     * @throws InterruptedException
     */
    public void clickFirstPromoKostThumbnail() throws InterruptedException {
        selenium.clickOn(firstPromoKostThumbnail);
    }
}
