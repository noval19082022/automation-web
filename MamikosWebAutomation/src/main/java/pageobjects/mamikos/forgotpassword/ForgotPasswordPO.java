package pageobjects.mamikos.forgotpassword;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ForgotPasswordPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ForgotPasswordPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(className = "bg-c-input__field")
    private WebElement phoneNumberTextInput;

    @FindBy(xpath = "//*[contains(text(), 'Pilih metode verifikasi')]")
    private WebElement sendButton;

    @FindBy(xpath = "(//p[@class='auth-step-card__title bg-c-text bg-c-text--heading-4 '])[1]")
    private WebElement titlePage;

    @FindBy(xpath = "(//div[@class='otp-input-wrapper__footer'])[1]")
    private WebElement otpInput;

    @FindBy(xpath = "//button[contains(text(), 'Kirim ulang kode')]")
    private WebElement resendOTP;

    @FindBy(className = "otp-method-selector__item")
    private List<WebElement> otpMethod;

    @FindBy(className = "bg-c-field__message")
    private WebElement getErrorMessagePhone;

    @FindBy(xpath = "//input")
    private List<WebElement> otpInputVal;

    @FindBy(css = ".otp-input-wrapper__footer-content--invalid")
    private WebElement otpErrorMessage;

    @FindBy(className = ".otp-input-wrapper__footer-content--invalid:nth-child(1)")
    private WebElement errorMessageOTP;

    @FindBy(css = ".bg-c-button__icon")
    private WebElement otpBackButton;

    @FindBy(css = ".bg-c-modal__body-title")
    private WebElement popupBatalkanProcessText;

    @FindBy(xpath = "//*[contains(text(), 'Ya, batalkan')]")
    private WebElement batalkanButton;

    @FindBy(css = ".bg-c-toast__content")
    private WebElement toastMessage;

    /**
     * Fill out Registered Phone Number and Click Send
     * @param phone phone number
     * @throws InterruptedException
     */
    public void fillOutRegisteredPhoneNumberAndClickSendButton(String phone) throws InterruptedException {
        selenium.waitInCaseElementVisible(phoneNumberTextInput, 5);
        selenium.enterText(phoneNumberTextInput, phone, false);
        selenium.waitTillElementIsClickable(sendButton);
        selenium.clickOn(sendButton);
    }

    /**
     * Click until element is visible
     * @throws InterruptedException
     */
    public void waitAndClickResendOTP() throws InterruptedException {
        selenium.clickOn(resendOTP);
    }

    /**
     * Wait and Get Resend OTP Button Text
     * @return Resend OTP Button Text (string)
     */
    public String getResendOTPButton(){
        selenium.waitInCaseElementVisible(resendOTP, 80);
        return selenium.getText(resendOTP).substring(0,16);
    }

    /**
     * Get Verification Phone Number Title
     * @return Verification Phone Number Title
     */
    public String getTitlePage() throws InterruptedException {
        selenium.hardWait(3);
        String element = "//p[@class='auth-step-card__title bg-c-text bg-c-text--heading-4 ']";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 10);
        return selenium.getText(By.xpath(element));
    }

    /**
     * Get Verification Phone Number Title
     * @return Verification Phone Number Title
     */
    public String getTitlePage2() throws InterruptedException {
        selenium.hardWait(5);
        String element = ".auth-step-card__description";
        selenium.waitInCaseElementVisible(driver.findElement(By.cssSelector(element)), 10);
        return selenium.getText(By.cssSelector(element));
    }


    /**
     * Click send OTP via SMS
     * @throws InterruptedException
     */
    public void clicksendOTPviaSMS() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(otpMethod.get(0));
    }

    /**
     * Click send OTP via WA
     * @throws InterruptedException
     */
    public void clicksendOTPviaWA() throws InterruptedException {
        selenium.clickOn(otpMethod.get(1));
    }

    /**
     * Get error message in phone field
     * @return String error message
     */
    public String getErrorMessage() {
        selenium.waitInCaseElementVisible(getErrorMessagePhone, 5);
        return selenium.getText(getErrorMessagePhone);
    }

    /**
     * User fill out unregistered phone number
     * @param phone is phone number
     */
    public void fillOutUnregisteredPhoneNumber(String phone) {
        selenium.enterText(phoneNumberTextInput, phone, true);
    }

//    /**
//     * Verify OTP Page
//     * @return
//     */
//    public boolean isOTPInputAppear() {
//       return selenium.waitInCaseElementVisible(otpInput, 150) != null;
//    }

    /**
     * User fill out OTP value
     * @param otp1 is first otp
     * @param otp2 is second otp
     * @param otp3 is third otp
     * @param otp4 is fourth otp
     */
    public void fillOTP(String otp1, String otp2, String otp3, String otp4) throws InterruptedException {
        selenium.hardWait(5);
        selenium.enterText(otpInputVal.get(0), otp1, true);
        selenium.enterText(otpInputVal.get(1), otp2, true);
        selenium.enterText(otpInputVal.get(2), otp3, true);
        selenium.enterText(otpInputVal.get(3), otp4, true);
    }

    /**
     * Get OTP Error Message
     * @return string error message
     */
    public String getOTPErrorMessage() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.getText(otpErrorMessage);
    }

    /**
     * Verify OTP Page
     * @return true if otp input appear
     */
    public Boolean isSendButtonEnable(){
        return sendButton.isEnabled();
    }

    /**
     * input invalid OTP
     * @return true if otp input appear
     */
    public void fillInvalidOTP(String otp) throws InterruptedException {
        selenium.enterText(otpInputVal.get(0), otp, true);
    }

    /**
     * Click back button on popup OTP
     * @throws InterruptedException
     */
    public void clickBackbutton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(otpBackButton);
    }

    /**
     * Click batalkan process button
     * @throws InterruptedException
     */
    public String getPopupBatalkanProcess() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(popupBatalkanProcessText);
    }

    /**
     * Click batalkan button on popup OTP
     * @throws InterruptedException
     */
    public void clickBatalkanbutton() throws InterruptedException {
        selenium.clickOn(batalkanButton);
    }

    /**
     * Get toast message waiting case
     * @return string toast message
     */
    public String toastMessageText() throws InterruptedException {
        selenium.waitInCaseElementVisible(toastMessage, 3);
        return selenium.getText(toastMessage);
    }

    /**
     * Click customer service whatsapp
     * @throws InterruptedException
     */
    public void clickCsWaButton(String csWA) throws InterruptedException {
        selenium.clickOn(By.xpath("//a[contains(.,'"+csWA+"')]"));
    }

    /**
     * Get text message on whatsapp page
     * @return string text message
     */
    public String getpretextWa() throws InterruptedException {
        String element = "//p[@class='_9vd5']";
        selenium.switchToWindow(2);
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 5);
        return selenium.getText(By.xpath(element));
    }

    /**
     * Get OTP text message send to Wa or Sms
     * @return string text message
     */
    public String getOtptext() throws InterruptedException {
        selenium.hardWait(3);
        String element = ".otp-input-wrapper__message";
        selenium.waitInCaseElementVisible(driver.findElement(By.cssSelector(element)), 10);
        return selenium.getText(By.cssSelector(element));
    }
}


