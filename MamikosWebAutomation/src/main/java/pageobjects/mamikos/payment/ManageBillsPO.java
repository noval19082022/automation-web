package pageobjects.mamikos.payment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ManageBillsPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ManageBillsPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@placeholder='Cari kamar atau penyewa']")
    private WebElement searchTextBox;

    @FindBy(xpath = "//input[@placeholder='Cari kamar atau penyewa']/following-sibling::img")
    private WebElement searchIcon;

    @FindBy(xpath = "(//*[@class='vgt-table striped '])[2]//tr")
    private List<WebElement> billsList;

    @FindBy(xpath = "//a[text()='>']")
    private WebElement nextPageIcon;

    @FindBy(xpath = "//a[text()='<']")
    private WebElement prevPageIcon;

    @FindBy(xpath = "//*[@class='pagination-table']/li")
    private List<WebElement> pageList;

    @FindBy(xpath = "//li/a[@class='page-item disabled']/following-sibling::li[@class='page-item']")
    private WebElement lastPage;

    /**
     * Enter text and click search icon
     * @param text want to input to text box
     */
    public void enterAndSearch(String text) {
        selenium.enterText(searchTextBox, text, false);
        selenium.javascriptClickOn(searchIcon);
    }

    /**
     * Get number of bills list
     * @throws InterruptedException
     */
    public int getNumberListOfBills() throws InterruptedException {
        selenium.hardWait(4);
        return billsList.size()-1;
    }

    /**
     * Get value attribute from element
     * @param index is number of element from list
     */
    public String getAttributeValueContractStatus(int index) throws InterruptedException {
        Thread.sleep(300);
        WebElement element = driver.findElement(By.xpath("(//*[@class='vgt-table striped '])[2]//tr[" + index + "]/td[1]/span/div/span/span"));
        selenium.pageScrollInView(element);
        return selenium.getElementAttributeValue(element, "class");
    }

    /**
     * Click one data from list of bills
     * @param index is number of element from list
     * @throws InterruptedException
     */
    public void clickOnBillsData(int index) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("((//*[@class='vgt-table striped '])[2]//tr[@class='clickable'])[" + index +"]"));
        selenium.pageScrollInView(element);
        selenium.hardWait(2);
        selenium.doubleClickOnElement(element);
    }

    /**
     * Get tenant name
     * @param index is number of element from list
     */
    public String getTenantName(int index) {
        By element = By.xpath("(//*[@class='vgt-table striped '])[2]//tr[" + index + "]/td[1]/span/div/span");
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Get one bill status from list of bills
     * @param index is number of element from list
     */
    public String getBillStatus(int index) {
        By element = By.xpath("//tr[" + index + "]/td[3]/span/span");
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Get one contract status is waiting
     * @param index is number of element from list
     */
    public String waitingContractStatus(int index) {
        By element = By.xpath("(//*[@class='vgt-table striped '])[2]//tr[" + index + "]/td[1]//span[@class='--status-waiting']/span");
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Get one contract status is check-in
     * @param index is number of element from list
     */
    public String checkinContractStatus(int index) {
        By element = By.xpath("(//*[@class='vgt-table striped '])[2]//tr[" + index + "]/td[1]//span[@class='--status-checkin']/span");
        selenium.pageScrollInView(element);
        return selenium.getText(element);
    }

    /**
     * Click on next page icon
     * @throws InterruptedException
     */
    public void clickOnNextPageIcon() throws InterruptedException {
        selenium.pageScrollInView(nextPageIcon);
        Thread.sleep(500);
        selenium.javascriptClickOn(nextPageIcon);
    }

    /**
     * Click on prev page icon
     * @throws InterruptedException
     */
    public void clickOnPrevPageIcon() throws InterruptedException {
        selenium.pageScrollInView(prevPageIcon);
        Thread.sleep(500);
        selenium.javascriptClickOn(prevPageIcon);
    }

    /**
     * Click on last page
     * @throws InterruptedException
     */
    public void clickOnLastPage() throws InterruptedException {
        selenium.hardWait(4);
        int indexLastPage = pageList.size() - 1;
        System.out.println(indexLastPage);
        By element = By.xpath("(//*[@class='pagination-table']/li)[" + indexLastPage  + "]/a");
        selenium.pageScrollInView(element);
        selenium.hardWait(1);
        selenium.javascriptClickOn(element);
    }
}
