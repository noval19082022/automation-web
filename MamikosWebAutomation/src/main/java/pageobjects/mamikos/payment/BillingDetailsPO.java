package pageobjects.mamikos.payment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BillingDetailsPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public BillingDetailsPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */
    
    @FindBy(xpath = "//button[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    private WebElement confirmationButton;

    @FindBy(xpath = "//*[@class='btn btn-primary --custom-button-confirm-end-contract']")
    private WebElement confirmationTerminateContractButton;

    @FindBy(xpath = "//*[@data-testid='statusContractLabel-req_terminate_accepted']")
    private WebElement messageLabel;

    @FindBy(xpath = "//div[@class='tab-contract__cta']/child::*[2]")
    private WebElement terminateContractLink;

    @FindBy(xpath = "//*[@placeholder='Pilih tanggal']")
    private WebElement terminateDateTextBox;

    @FindBy(xpath = "//*[@class='cell day today']")
    private WebElement firstDateList;

    @FindBy(xpath = "//button[contains(text(),'Pilih')]")
    private WebElement pilihDateButton;

    @FindBy(xpath = "//button[contains(text(),'Konfirmasi Pemberhentian Sewa')]")
    private WebElement terminateControlButton;

    @FindBy(xpath = "//*[@placeholder='Masukkan alasan lainnya di sini']")
    private WebElement reasonTextBox;

    @FindBy(xpath = "//p[@class='bg-c-alert__content-description bg-c-text bg-c-text--body-4 ']")
    private WebElement disbursementAlert;

    @FindBy(xpath = "//p[contains(., 'Kapan uang masuk ke rekening saya?')]")
    private WebElement disbursementLink;

    @FindBy(xpath = "//h1[@class='mh-article-page__title bg-c-text bg-c-text--heading-2 ']")
    private WebElement disbursementLinkPage;

    /**
     * Click confirmation button
     * @throws  InterruptedException
     */
    public void clickOnConfirmationButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.switchToWindow(0);
        selenium.waitTillElementIsVisible(confirmationButton, 5);
        selenium.javascriptClickOn(confirmationButton);
    }

    /**
     * Click confirmation terminate contract button
     * @throws  InterruptedException
     */
    public void clickOnConfirmationTerminateContractButton() throws InterruptedException {
        selenium.clickOn(confirmationTerminateContractButton);
    }

    /**
     * Get warning message
     */
    public String getWarningMessage() throws InterruptedException{
        selenium.hardWait(5);
        selenium.waitInCaseElementVisible(messageLabel,60);
        return selenium.getText(messageLabel);
    }

    /**
     * Click on terminate contract link
     */
    public void clickOnTerminateContract() throws InterruptedException {
        selenium.switchToWindow(0);
        selenium.hardWait(3);
        selenium.pageScrollInView(terminateContractLink);
        selenium.waitInCaseElementVisible(terminateContractLink, 10);
        selenium.hardWait(8);
        selenium.clickOn(terminateContractLink);
    }

    /**
     * Click on terminate Reason
     * @param terminatedReason text want to terminate reason
     */
    public void selectTerminateReason(String terminatedReason) throws InterruptedException {
        selenium.clickOn(By.xpath("//input[@id='terminateReason0']/following-sibling::*[2][contains(., '" + terminatedReason + "')]"));
    }

    /**
     * Select date to terminate contract
     */
    public void selectDate() throws InterruptedException {
        selenium.clickOn(terminateDateTextBox);
        selenium.hardWait(3);
        selenium.clickOn(By.xpath("//*[@class='cell day']"));

    }

    /**
     * Click on terminate contract
     */
    public void clickOnTerminateContractButton() throws InterruptedException {
        selenium.clickOn(pilihDateButton);
        selenium.hardWait(2);
        selenium.clickOn(terminateControlButton);
    }

    /**
     * Input reason checkout
     */
    public void inputReasonReason(String terminatedReason) throws InterruptedException{
        selenium.hardWait(2);
        selenium.enterText(reasonTextBox, terminatedReason, true);
    }

    /**
     * get text disbursement alert
     */
    public String getDisbursementAlert() {
        return selenium.getText(disbursementAlert);
    }

    /**
     * clicks on disbursement link
     * @throws InterruptedException
     */
    public void clicksDisbursementLink() throws InterruptedException{
        selenium.clickOn(disbursementLink);
    }

    /**
     * get disbursement link page
     */
    public String getDisbursementLinkPage(){
        return selenium.getText(disbursementLinkPage);
    }
}