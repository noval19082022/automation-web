package pageobjects.mamikos.payment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import javax.xml.xpath.XPath;

public class ManualPayoutPO {
    WebDriver driver;
    SeleniumHelpers selenium;


    public ManualPayoutPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy (xpath = "//a[contains(.,'Manual Payout')]")
    private WebElement openMenuManualPayout;
    @FindBy(xpath = "//th[.='Transaction ID']")
    private WebElement transactionID;
    @FindBy(xpath = "//th[.='Invoice']")
    private WebElement invoicenumber;
    @FindBy(xpath = "//th[.='Account Number']")
    private WebElement accountnumber;
    @FindBy(xpath = "//th[.='Bank']")
    private WebElement bank;
    @FindBy(xpath = "//th[.='City']")
    private WebElement city;
    @FindBy(xpath = "//th[.='Account Name']")
    private WebElement accountname;
    @FindBy(xpath = "//th[.='Transfer Amount']")
    private WebElement transferamount;
    @FindBy(xpath = "//th[.='Reason']")
    private WebElement reason;
    @FindBy(xpath = "//th[.='Status']")
    private WebElement status;
    @FindBy(xpath = "//th[.='Type']")
    private WebElement type;
    @FindBy(xpath = "//th[.='Transferred At']")
    private WebElement transactiontype;
    @FindBy(xpath = "//th[.='Created At']")
    private WebElement createdAT;
    @FindBy(xpath = "//th[.='Created By']")
    private WebElement createdBY;
    @FindBy(name = "search_value")
    private WebElement columnInput;
    @FindBy(xpath = "//input[@class='btn btn-primary btn-md']")
    private WebElement clickSearch;
    @FindBy(xpath = "//td[.='79370282/2021/04/0037']")
    private WebElement invoiceVeriffied;
    @FindBy(xpath = "//td[.='tytyty']")
    private WebElement nameveriffed;
    @FindBy(name = "search_by")
    private WebElement columnSearchBy;
    @FindBy(name = "type")
    private WebElement typeRestarted;
    @FindBy(name = "status")
    private WebElement statusRestarted;
    @FindBy(name = "create_date_from")
    private WebElement createFromRestarted;
    @FindBy(name = "create_date_to")
    private WebElement createTorestarted;
    @FindBy(name = "create_date_from")
    private WebElement transfferedAT;
    @FindBy(name = "create_date_to")
    private WebElement transfferedDateTo;
    @FindBy(xpath = "//a[.='Create Payout']")
    private WebElement buttonCreatePayout;
    @FindBy(xpath = "//input[@name='destination_name']")
    private WebElement accountName;
    @FindBy(xpath = "//input[@name='destination_account']")
    private WebElement accNumber;
    @FindBy(xpath = "//input[@name='transfer_amount']")
    private WebElement inputAmount;
    @FindBy(xpath = "//textarea[@name='reason']")
    private WebElement inputReason;
    @FindBy(xpath = "//input[@name='invoice_number']")
    private WebElement inputInvoice;
    @FindBy(xpath = "//input[@class='btn btn-primary btn-md']")
    private WebElement confirmButton;
    @FindBy(xpath = "(//a[text()='Cancel'])[1]")
    private WebElement cancelPayout;
    @FindBy(xpath = "(//a[text()='Change Type'])[1]")
    private WebElement changeType;
    @FindBy(xpath = "(//a[text()='Change Invoice'])[1]")
    private WebElement changeInvoice;
    @FindBy(xpath = "(//a[text()='Transfer'])[1]")
    private WebElement buttonTransfer;
    @FindBy(xpath = "(//a[text()='Edit'])[1]")
    private WebElement ButtonEdit;
    @FindBy(xpath = "//a[.='Cancel']")
    private WebElement CancelFormPayout;





    /**
     * Open Manual Payout Menu on BackOffice
     * @throws  InterruptedException
     */

    public void openMenuManualPayout () throws  InterruptedException {
        selenium.clickOn(openMenuManualPayout);
    }
    /**
     * Owner will see detail on manual payout menu
     * @throws  InterruptedException
     */
    public void userSeeDetailManualPayout () throws InterruptedException {
        selenium.isElementDisplayed(transactionID);
        selenium.isElementDisplayed(invoicenumber);
        selenium.isElementDisplayed(accountnumber);
        selenium.isElementDisplayed(bank);
        selenium.isElementDisplayed(city);
        selenium.isElementDisplayed(accountname);
        selenium.isElementDisplayed(transferamount);
        selenium.isElementDisplayed(reason);
        selenium.isElementDisplayed(status);
        selenium.isElementDisplayed(type);
        selenium.isElementDisplayed(transactiontype);
        selenium.isElementDisplayed(createdAT);
        selenium.isElementDisplayed(createdBY);
    }
    /**
     * User choose one of selection on column search by
     * @throws  InterruptedException
     */
    public void userSelectDataManualPayout (String searchBy) throws  InterruptedException {
        WebElement element = driver.findElement(By.xpath("//option[.='"+searchBy+"']"));
        selenium.clickOn(element);
    }

    /**
     * User input Invoice Number
     * @throws  InterruptedException
     */
    public void inputInvoice (String invoice) throws InterruptedException {
        selenium.clickOn(columnInput);
        selenium.enterText(columnInput, invoice, true);
    }

    /**
     * User click button search
     * @throws  InterruptedException
     */
    public void clickSearch () throws InterruptedException {
        selenium.clickOn(clickSearch);
    }

    /**
     * User verify Data of invoice number that has been search before
     * @throws  InterruptedException
     */
    public void userWillVerifyInvoice () throws InterruptedException {
        selenium.isElementDisplayed(invoiceVeriffied);

    }

    /**
     * User restart column search
     * @throws  InterruptedException
     */

    public void columnSearchRestarted () throws  InterruptedException {
        selenium.isElementDisplayed(columnSearchBy);
        selenium.isElementDisplayed(columnInput);
        selenium.isElementDisplayed(typeRestarted);
        selenium.isElementDisplayed(statusRestarted);
        selenium.isElementDisplayed(createFromRestarted);
        selenium.isElementDisplayed(createTorestarted);
    }

    /**
     * User input account name
     * @throws  InterruptedException
     */

    public void inputAccountName (String name) throws  InterruptedException {
        selenium.clickOn(columnInput);
        selenium.enterText(columnInput,name,true);
    }

    /**
     * User verify account name that has been searched before
     * @throws  InterruptedException
     */

    public void userVerifyAccountName () throws InterruptedException {
        selenium.isElementDisplayed(nameveriffed);
    }

    /**
     * User choose transaction type
     * @throws  InterruptedException
     */
    public void userSelectTransactionType (String type) throws  InterruptedException {
        WebElement element =driver.findElement(By.xpath("//option[.='"+type+"']"));
        selenium.clickOn(element);
    }

    /**
     * User verify transaction type that user has been chooses before
     * @throws  InterruptedException
     */
    public void userVerifyTransactionType (String resultType) throws  InterruptedException {
        WebElement element =driver.findElement(By.xpath("//td[.='"+resultType+"']"));
        selenium.waitTillElementIsVisible(element);
    }

    /**
     * User choose status transaction
     * @throws  InterruptedException
     */

    public void userSelectTransactionSttaus (String status) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//option[.='"+status+"']"));
        selenium.clickOn(element);
    }

    /**
     * User verify status transaction that has been chooses before
     * @throws  InterruptedException
     */

    public void verifyResultStatus (String resultStatus) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//span[.='"+resultStatus+"']"));
        selenium.waitTillElementIsVisible(element);
    }

    /**
     * User input from and to date
     * @throws  InterruptedException
     */

    public void userInputCreateDate (String dateFrom, String dateTo) throws  InterruptedException {
        selenium.enterText(transfferedAT, dateFrom, true);
        selenium.enterText(transfferedDateTo, dateTo,true);
    }

    /**
     * User verify data transaction that has been searched by create date
     * @throws  InterruptedException
     */

    public void vefirytTransactionbyCreateDate (String createFrom, String createTo) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//td[.='"+createFrom+"']"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
        WebElement element2 = driver.findElement(By.xpath("//td[.='"+createTo+"']"));
        selenium.waitTillElementIsVisible(element2);
        selenium.isElementDisplayed(element2);
    }

    /**
     * User click button create payout
     * @throws  InterruptedException
     */

    public void clickButtonCreatePayout () throws InterruptedException {
        selenium.click(buttonCreatePayout);
    }

    /**
     * User choose payout transaction type
     * @throws  InterruptedException
     */

    public void choosePayoutType (String payoutType) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//option[.='"+payoutType+"']"));
        selenium.waitTillElementIsVisible(element1);
        selenium.clickOn(element1);
    }

    /**
     * User input bank account and name
     * @throws  InterruptedException
     */

    public void inputAccNumberAndName (String accname, String accnumber) throws  InterruptedException {
        selenium.enterText(accNumber, accnumber, true);
        selenium.hardWait(2);
        selenium.enterText(accountName, accname, true);

    }

    /**
     * User choose bank name
     * @throws  InterruptedException
     */

    public void chooseBankName (String mandiri) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//option[.='"+mandiri+"']"));
        selenium.waitTillElementIsVisible(element1);
        selenium.clickOn(element1);
    }

    /**
     * User input reason and invoice number
     * @throws  InterruptedException
     */

    public void  inputAmountReasonAndInvoice (String amount, String reason, String invoice) throws InterruptedException {
        selenium.enterText(inputAmount, amount, true);
        selenium.hardWait(2);
        selenium.enterText(inputReason, reason, true);
        selenium.hardWait(2);
        selenium.enterText(inputInvoice, invoice, true);
    }

    /**
     * User click button create payout
     * @throws  InterruptedException
     */

    public void createPayout () throws InterruptedException {
        selenium.clickOn(confirmButton);
    }

    /**
     * User verify payout that has been created before
     * @throws  InterruptedException
     */

    public void payoutCreated (String successCreated, String statusPending) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//div[contains(text(),'"+successCreated+"')]"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
        selenium.hardWait(2);
        WebElement element2 = driver.findElement(By.xpath("(//span[contains(text(),'"+statusPending+"')])[1]"));
        selenium.waitTillElementIsVisible(element2);
        selenium.isElementDisplayed(element2);

    }

    /**
     * User click button cancel payout
     * @throws  InterruptedException
     */

    public void cancelPayout () throws InterruptedException {
        selenium.clickOn(cancelPayout);
    }

    /**
     * User verify that payout transaction has been cancelled
     * @throws  InterruptedException
     */

    public void payoutTransactionCancelled (String successCancelled, String statusCancel) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//div[contains(text(),'"+successCancelled+"')]"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
        selenium.hardWait(2);
        WebElement element2 = driver.findElement(By.xpath("(//span[contains(text(),'"+statusCancel+"')])[1]"));
        selenium.waitTillElementIsVisible(element2);
        selenium.isElementDisplayed(element2);

    }

    /**
     * User click button change type
     * @throws  InterruptedException
     */


    public void changeType () throws InterruptedException {
        selenium.waitTillElementIsVisible(changeType);
        selenium.clickOn(changeType);
    }

    /**
     * User choose new type to changes current type
     * @throws  InterruptedException
     */

    public void chooseNewType (String payoutType) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//option[.='"+payoutType+"']"));
        selenium.waitTillElementIsVisible(element1);
        selenium.clickOn(element1);
    }

    /**
     * User click button submit changes
     * @throws  InterruptedException
     */

    public void submitChanges () throws InterruptedException {
        selenium.clickOn(confirmButton);
    }

    /**
     * User verify that type of payout transaction is updated
     * @throws  InterruptedException
     */

    public void payoutTypeUpdated (String messageSuccess, String typeUpdated) throws InterruptedException {
        WebElement element1 = driver. findElement(By.xpath("//div[contains(text(),'"+messageSuccess+"')]"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
        WebElement element2 = driver.findElement(By.xpath("(//td[.='"+typeUpdated+"'])[1]"));
        selenium.waitTillElementIsVisible(element2);
        selenium.isElementDisplayed(element2);
    }

    /**
     * User click button change invoice
     * @throws  InterruptedException
     */

    public void changeInvoice () throws InterruptedException {
        selenium.clickOn(changeInvoice);
    }

    /**
     * User input new invoice number to changes current invoice number
     * @throws  InterruptedException
     */

    public void inputNewInvoice (String newInvoiceNumber) throws InterruptedException {
        selenium.enterText(inputInvoice, newInvoiceNumber, true);
    }

    /**
     * User verify that invoice number is updated
     * @throws  InterruptedException
     */

    public void invoiceUpdated (String messageSuccess, String newInvoice) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//div[contains(text(),'"+messageSuccess+"')]"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
        selenium.hardWait(2);
        WebElement element2 = driver.findElement(By.xpath("(//td[.='"+newInvoice+"'])[1]"));
        selenium.waitTillElementIsVisible(element2);
        selenium.isElementDisplayed(element2);

    }

    /**
     * User click button transfer
     * @throws  InterruptedException
     */

    public void buttonTransfer () throws InterruptedException {
        selenium.javascriptClickOn(buttonTransfer);
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * User ensure that payout transaction has status processing
     * @throws  InterruptedException
     */

    public void transactionProcessed (String successProcess, String statusProcessing) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//div[contains(text(),'"+successProcess+"')]"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
        selenium.hardWait(2);
        WebElement element2 = driver.findElement(By.xpath("(//span[contains(text(),'"+statusProcessing+"')])[1]"));
        selenium.waitTillElementIsVisible(element2);
        selenium.isElementDisplayed(element2);
    }

    /**
     * User click button edit
     * @throws  InterruptedException
     */

    public void clickButtonEdit () throws InterruptedException {
        selenium.clickOn(ButtonEdit);
    }

    /**
     * User input new amount and new reason
     * @throws  InterruptedException
     */

    public void inputNewAmountAndReason (String changeAmount, String changeReason) throws  InterruptedException{
        selenium.enterText(inputAmount, changeAmount, true);
        selenium.hardWait(2);
        selenium.enterText(inputReason, changeReason, true);
    }

    /**
     * User verify that new data is shown eith corectly after edited several data
     * @throws  InterruptedException
     */

    public void shownNewData (String newBankAcc, String newBankName, String newAccName, String newAmount, String newReason) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("(//td[.='"+newBankAcc+"'])[1]"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
        selenium.hardWait(2);
        WebElement element2 = driver.findElement(By.xpath("(//td[.='"+newBankName+"'])[1]"));
        selenium.waitTillElementIsVisible(element2);
        selenium.isElementDisplayed(element2);
        selenium.hardWait(2);
        WebElement element3 = driver.findElement(By.xpath("(//td[.='"+newAccName+"'])[1]"));
        selenium.waitTillElementIsVisible(element3);
        selenium.isElementDisplayed(element3);
        selenium.hardWait(2);
        WebElement element4 = driver.findElement(By.xpath("(//td[.='"+newAmount+"'])[1]"));
        selenium.waitTillElementIsVisible(element4);
        selenium.isElementDisplayed(element4);
        selenium.hardWait(2);
        WebElement element5 = driver.findElement(By.xpath("(//td[.='"+newReason+"'])[1]"));
        selenium.waitTillElementIsVisible(element5);
        selenium.isElementDisplayed(element5);

    }

    /**
     * User ensure error message need allow transfer permission is appeared
     * @throws  InterruptedException
     */

    public void payoutNotCreatedAndGetErrorMessage (String messageNeedAllowTransfer) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//div[contains(text(),'"+messageNeedAllowTransfer+"')]"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
    }

    /**
     * User ensure error message minimum amount is appeared
     * @throws  InterruptedException
     */

    public void userGetMessageErrorMinAmount (String errorMinAmount)throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//li[contains(text(),'"+errorMinAmount+"')]"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
    }

    /**
     * User click button cancel on form input data payout
     * @throws  InterruptedException
     */

    public void CancelFormPayout () throws InterruptedException {
        selenium.clickOn(CancelFormPayout);
    }

    /**
     * User will see mandatory error message
     * @throws  InterruptedException
     */

    public void appearMessageToInputMandatoryData (String errorAmount, String errorReason) throws InterruptedException {
        WebElement element1 = driver.findElement(By.xpath("//li[contains(text(),'"+errorAmount+"')]"));
        selenium.waitTillElementIsVisible(element1);
        selenium.isElementDisplayed(element1);
        WebElement element2 = driver.findElement(By.xpath("//li[contains(text(),'"+errorReason+"')]"));
        selenium.waitTillElementIsVisible(element2);
        selenium.isElementDisplayed(element2);
    }



    

}


