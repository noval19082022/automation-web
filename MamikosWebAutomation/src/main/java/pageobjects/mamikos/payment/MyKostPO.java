package pageobjects.mamikos.payment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;
import java.util.List;

public class MyKostPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public MyKostPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[text()='Berhenti Sewa']")
    private WebElement terminateContractLink;

    @FindBy(xpath = "//*[@placeholder='Pilih tanggal']")
    private WebElement dateTerminateContractTextBox;

    @FindBy(xpath = "//*[@class='cell day today']")
    private WebElement firstDateList;

    @FindBy(xpath = "//button[.='Kirim']")
    private WebElement terminateContractButton;

    @FindBy(xpath = "//*[@placeholder='Ceritakan pengalamanmu di sini']")
    private WebElement kostReviewTextBox;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--title-5 ']")
    private WebElement billsTab;

    @FindBy(xpath = "//h3[@class='bg-c-modal__body-title']")
    private WebElement billsMessage;

    @FindBy(xpath = "//label[text()='Durasi Sewa']/following-sibling::span")
    private WebElement rentDurationLabel;

    @FindBy(xpath = "//button[.='Bayar di sini']")
    private WebElement payButton;

    @FindBy(css = ".today")
    private WebElement pilihDateButton;

    @FindBy(xpath = "(//p[@class='bg-c-text bg-c-text--label-2 '])[2]")
    private WebElement sectionToReviewKost;

    @FindBy(xpath = "//button[.='Selesai']")
    private WebElement selesaiButton;

    @FindBy(xpath = "//p[.='Ajukan Berhenti Sewa']")
    private WebElement terminateContractButton2;

    @FindBy(xpath = "//h1[.='Berhasil Mengajukan ke Pemilik']")
    private WebElement messageSuccessRequestTerminate;

    @FindBy (xpath ="//div[@class='bg-c-datepicker']")
    private WebElement datePicker;

    @FindBy (css =".stop-rent__review-card .user-review-card--empty")
    private WebElement reviewKost;

    @FindBy (css =".bg-c-modal__footer-CTA > .bg-c-button")
    private WebElement popupReview;

    @FindBy (xpath ="//button[@class='bg-c-button bg-c-button--primary bg-c-button--md bg-c-button--block']")
    private WebElement ajukanBerhentiSewa;

    @FindBy (css = ".bg-c-text--title-5")
    private WebElement ajukanBerhentiSewaText;

    @FindBy (xpath = "//*[@type='button' and @disabled='disabled']")
    private WebElement ajukanBerhentiSewaButton;

    @FindBy (xpath = "//p[.='Kontrak']")
    private WebElement kontrakMenu;

    @FindBy (xpath = "//*[contains(@data-path, 'lbl')]")
    private List<WebElement> reviewPage;

    @FindBy (xpath ="//p[@class='user-review-card--reviewed__text bg-c-text bg-c-text--label-2 ']")
    private WebElement titleKostReviewSubmitted;

    @FindBy(css = ".user-review-card--flexbox")
    private WebElement starsKostReviewSubmitted;

    @FindBy(xpath = "//*[@class='close']")
    private WebElement closeButton;

    @FindBy(css = ".bg-c-button")
    private WebElement bayarDiSiniButton;

    @FindBy(xpath = "//p[.='Forum']")
    private WebElement forumButton;

    @FindBy(xpath = "(//button[normalize-space()='Sudah Dibayar'])[1]")
    private WebElement sudahDibayar;

    @FindBy(xpath = "(//span[@class='status-card'])[1]")
    private WebElement oneTime;

    /**
     * Click link terminate contract
     */
    public void clickOnTerminateContract() {
        selenium.javascriptClickOn(terminateContractLink);
    }

    /**
     * Select reason to terminate contract
     * @param reason is text reason terminate contract
     * @throws InterruptedException
     */
    public void selectReasonTerminateContract(String reason) throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(By.xpath("//p[.='"+reason+"']"));
    }

    /**
     * Select date terminate contract
     * @throws InterruptedException
     */
    public void selectDate() throws InterruptedException {
        selenium.clickOn(dateTerminateContractTextBox);
        selenium.hardWait(3);
        //selenium.javascriptClickOn(By.xpath("//input[@class='bg-c-input__field']//span[text()='" + date + "']"));
        selenium.javascriptClickOn(datePicker);
        selenium.javascriptClickOn(pilihDateButton);
        String scrollPage = "//div[@class='user-review-card--empty bg-c-card bg-c-card--md bg-c-card--light']";
        selenium.pageScrollInView(By.xpath(scrollPage));
        selenium.clickOn(By.xpath(scrollPage));
    }

    /**
     * user clicks beri review untuk kost
     */
    public void clickOnReviewKost() throws InterruptedException {
        selenium.pageScrollInView(reviewKost);
        selenium.clickOn(reviewKost);
    }

    /**
     * user clicks ajukan berhenti sewa
     */
    public void clickOnAjukanBerhentiSewa() throws InterruptedException {
        selenium.clickOn(popupReview);
        selenium.pageScrollInView(ajukanBerhentiSewa);
        selenium.hardWait(5);
        selenium.clickOn(ajukanBerhentiSewa);
    }

    /**
     * Give rating for boarding house facility
     * @param point is number of scores review boarding house
     * @param  option of review criteria
     * @throws InterruptedException
     */
    public void selectRating(int point, String option) throws InterruptedException {
        selenium.hardWait(6);
        By element = By.xpath("//span[contains(text(),'"+option+"')]/parent::div/following-sibling::div/div/div/span["+point+"]");
        selenium.clickOn(element);
    }

    /**
     * Enter text for review boarding house
     * @param text is text want to input as boarding house review
     */
    public void enterKostReview(String text) {
        selenium.enterText(kostReviewTextBox, text, true);
    }

    /**
     * Click on terminate contract button
     * @throws InterruptedException
     */
    public void clickOnTerminateContractButton() throws InterruptedException {
        selenium.clickOn(terminateContractButton);
        selenium.hardWait(3);
    }

    /**
     * Click on bill tab
     * @throws InterruptedException
     */
    public void clickOnBillTab() throws InterruptedException {
        String element = "//div[@class='user-kost-activities__menu-wrapper']/div[contains(.,'Kontrak')]";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 10);
        selenium.clickOn(By.xpath(element));
        selenium.pageScrollInView(billsTab);
        selenium.hardWait(1);
        selenium.waitTillElementIsClickable(billsTab);
        selenium.clickOn(billsTab);
    }

    /**
     * Click on tagihan tab
     * @throws InterruptedException
     */
    public void clickOnTagihanTab() throws InterruptedException {
        String element = "//p[normalize-space()='Tagihan kos']";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 10);
        selenium.javascriptClickOn(By.xpath(element));
    }

    /**
     * Click on sudah dibayar
     * @throws InterruptedException
     */
    public void clickOnSudahDibayar() throws InterruptedException {
        selenium.clickOn(sudahDibayar);
    }

    /**
     * Click on one-time
     */
    public void clickOnOneTime() throws InterruptedException {
        selenium.clickOn(oneTime);
    }

    /**
     * Get warning message from bill tab
     */
    public String getMessage() {
        return selenium.getText(billsMessage);
    }

    /**
     * Get rent duration
     */
    public String getRentDuration() {
        return selenium.getText(rentDurationLabel);
    }

    /**
     * Verify system display terminate contract link
     */
    public void displayTerminateContract() {
        selenium.waitInCaseElementPresent(By.xpath("//*[text()='Hentikan Kontrak Sewa']"), 5);
    }

    /**
     * Click on pay button for next month
     */
    public void clickPayButtonForNextPayment() {
        selenium.javascriptClickOn(payButton);
    }

    /**
     * Click on pay button for next month
     * @throws InterruptedException
     */
    public void clickOnPayButton() throws InterruptedException {
        selenium.pageScrollInView(payButton);
        selenium.clickOn(payButton);
    }

    public void clickToReviewKost () throws  InterruptedException {
        selenium.clickOn(sectionToReviewKost);
    }

    public void clickSelesaiButton () throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(selesaiButton);
    }

    public void clickTerminateButton () throws InterruptedException {
        selenium.hardWait(5);
        selenium.pageScrollInView(terminateContractButton2);
        selenium.clickOn(terminateContractButton2);
    }

    public String messageRequestTerminate () {
        return selenium.getText(messageSuccessRequestTerminate);
    }

    /**
     * Click Ajukan Berhenti Sewa text
     * @throws InterruptedException
     */
    public void clickAjukanBerhentiSewaText() throws InterruptedException {
        selenium.pageScrollInView(ajukanBerhentiSewaText);
        selenium.javascriptClickOn(ajukanBerhentiSewaText);
    }

    /**
     * Verify button Ajukan Berhenti Sewa is disabled
     * @return true if disabled
     */
    public boolean isAjukanBerhentiSewaButtonDisabled() {
        return selenium.waitInCaseElementVisible(ajukanBerhentiSewaButton, 3) != null;
    }

    /**
     * Click Kontrak at kost saya page
     * @throws InterruptedException
     */
    public void clickKontrakMenu() throws InterruptedException {
        selenium.pageScrollInView(kontrakMenu);
        selenium.javascriptClickOn(kontrakMenu);
    }

    /**
     * Get Text of Review Title by index
     * @param index - index review title
     * @return text of Review Page
     */
    public String getAllReviewPage (int index){
        return selenium.getText(reviewPage.get(index));
    }

    /**
     * Get Kost Review Submitted Text
     *
     * @return string "Kamu memberikan nilai:"
     */
    public String getTitleKostReviewSubmittedText(){
        selenium.waitInCaseElementVisible(titleKostReviewSubmitted,3);
        return selenium.getText(titleKostReviewSubmitted);
    }

    /**
     * Get Kost Review Submitted Stars Amount
     *
     * @return string "3"
     */
    public String getStarsKostReviewSubmittedText(){
        return selenium.getText(starsKostReviewSubmitted);
    }

    /**
     * Click on Close Button
     *
     * @throws InterruptedException
     */
    public void clickOnCloseButton() throws InterruptedException {
        selenium.clickOn(closeButton);
    }

    /**
     * Click on Bayar di sini Button
     *
     * @throws InterruptedException
     */
    public void clickOnBayarDiSiniButton() throws InterruptedException {
        selenium.pageScrollInView(forumButton);
        selenium.waitInCaseElementClickable(bayarDiSiniButton,2);
        selenium.javascriptClickOn(bayarDiSiniButton);
    }

}
