package pageobjects.mamikos.payment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class AllInvoicePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AllInvoicePO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(partialLinkText = "All Invoice Li")
    private WebElement openAllInvoice;
    @FindBy(name = "search_by")
    private WebElement selectInvoice;
    @FindBy(name = "search_value")
    private WebElement imvoiceNumber;
    @FindBy(xpath = "//*[@class='btn btn-primary btn-md']")
    private WebElement clickCariInvoice;
    @FindBy(xpath = "//div[@class='box-footer']")
    private WebElement blankScreen;
    @FindBy(xpath = "//select[@name='search_by']/option[2]")
    private WebElement selectCode;
    @FindBy(xpath = "//a[.='Change Status']")
    private WebElement clickChangeStatus;
    @FindBy(xpath = "//select[@name='status']/option[2]")
    private WebElement changeToPaid;
    @FindBy(xpath = "//input[@name='paid_at']")
    private WebElement inputDateAndTime;
    @FindBy(xpath = "//input[@class='btn btn-primary btn-md']")
    private WebElement clickSubmitChange;
    @FindBy(xpath = "//select[@name='status']/option[1]")
    private WebElement changeToUnpaid;
    @FindBy(xpath = "//input[@id='notMamipay']")
    private WebElement checklistNotInMamipay;
    @FindBy(xpath = "//th[.='Shortlink']")
    private WebElement shortlink;
    @FindBy(xpath = "//th[.='Invoice Number']")
    private WebElement invoiceNumber;
    @FindBy(xpath = "//th[.='Invoice Name']")
    private WebElement invoiceName;
    @FindBy(xpath = "//th[.='Scheduled Date']")
    private WebElement scheduledDate;
    @FindBy(xpath = "//th[.='Order Type")
    private WebElement orderType;
    @FindBy(xpath = "//th[.='Payment Method']")
    private WebElement paymentMethod;
    @FindBy(xpath = "//th[.='Total Amount']")
    private WebElement totalamount;
    @FindBy(xpath = "//th[.='Owner/Renter']")
    private WebElement ownerrenter;
    @FindBy(xpath = "//th[.='Invoice Status']")
    private WebElement invoicestatus;
    @FindBy(xpath = "//div[@class='datepicker-days']//tr[1]/td[.='1']")
    private WebElement chooseFromDate;
    @FindBy(xpath = "//div[@class='datepicker-days']//tr[1]/td[.='2']")
    private WebElement chooseToDate;
    @FindBy(xpath = "//a[.='Reset']")
    private WebElement resetButton;
    @FindBy(xpath = "//input[@name='schedule_date_from']")
    private WebElement inputScheduleFrom;
    @FindBy(xpath = "//input[@name='schedule_date_to']")
    private WebElement inputScheduleTo;
    @FindBy(xpath = "//tr[1]/td[4]")
    private WebElement dataScheduledDate;
    @FindBy(xpath = "//input[@name='nominal_from']")
    private WebElement valueFrom;
    @FindBy(xpath = "//input[@name='nominal_to']")
    private WebElement valueTo;
    @FindBy(xpath = "//select[@name='status']")
    private WebElement sectionStatus;
    @FindBy(xpath = "//select[@name='order_type']")
    private WebElement sectionOrderType;

    /**
     * Open menu All invoice
     * @throws  InterruptedException
     */
    public void openAllInvoice () throws InterruptedException {
        selenium.clickOn(openAllInvoice);
    }
    /**
     * Choose invoice
     * @throws  InterruptedException
     */
    public void selectInvoice () throws  InterruptedException {
        selenium.clickOn(selectInvoice);
    }
    /**
     * Input Invoice Number
     * @throws  InterruptedException
     */
    public void inputInvoice (String number) throws  InterruptedException {
        selenium.enterText(imvoiceNumber, number, false);
    }
    /**
     * Click Button Cari Invoice
     * @throws  InterruptedException
     */
    public void clickCariInvoice () throws  InterruptedException {
        selenium.clickOn(clickCariInvoice);
    }
    /**
     * Click Button reset
     * @throws  InterruptedException
     */
    public void clickReset () throws  InterruptedException {
        selenium.clickOn(resetButton);
    }
    /**
     * Verify Data Transaction
     * @throws  InterruptedException
     */
    public void verifyDataTransaction () throws InterruptedException {
        selenium.isElementDisplayed(shortlink);
        selenium.isElementDisplayed(invoiceNumber);
        selenium.isElementDisplayed(invoiceName);
        selenium.isElementDisplayed(scheduledDate);
        selenium.isElementDisplayed(orderType);
        selenium.isElementDisplayed(paymentMethod);
        selenium.isElementDisplayed(totalamount);
        selenium.isElementDisplayed(ownerrenter);
        selenium.isElementDisplayed(invoicestatus);

    }
    /**
     * user get blank Screen
     * @throws  InterruptedException
     */

    public void getBlankScreen () throws InterruptedException {
        selenium.getText(blankScreen);
    }
    /**
     * user choose invoice code
     * @throws  InterruptedException
     */
    public void selectCode () throws  InterruptedException {
        selenium.clickOn(selectCode);
    }
    /**
     * user click status invoice
     * @throws  InterruptedException
     */
    public void clickChangeStatus () throws  InterruptedException {
        selenium.clickOn(clickChangeStatus);
    }
    /**
     * user change status invoice to paid
     * @throws  InterruptedException
     */
    public void changeToPaid () throws  InterruptedException {
        selenium.clickOn(changeToPaid);
    }
    /**
     * user input date and time
     * @throws  InterruptedException
     */
    public void inputDateAndTime (String date) throws  InterruptedException {
        selenium.enterText(inputDateAndTime, date, false);
    }
    /**
     * user submit change invoice
     * @throws  InterruptedException
     */
    public void clickSubmitChange () throws  InterruptedException {
        selenium.clickOn(clickSubmitChange);
    }
    /**
     * invoice ststus updated
     * @throws  InterruptedException
     */
    public void  showInvoiceAfterChange (String status) throws  InterruptedException {
        WebElement element = driver.findElement(By.xpath("//span[text()='"+status+"']"));
        selenium.waitTillElementIsVisible(element);

    }
    /**
     * user change status invoice to unpaid
     * @throws  InterruptedException
     */
    public void changeToUnpaid () throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(changeToUnpaid);
    }
    /**
     * user checklist section not in mamipay
     * @throws  InterruptedException
     */
    public void checklistNotInMamipay () throws InterruptedException {
        selenium.clickOn(checklistNotInMamipay);
    }

    /**
     * user choose pament method
     * @throws  InterruptedException
     */
    public void selectPayment (String method) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//option[.='"+method+"']"));
        selenium.clickOn(element);
    }

    /**
     * user verify data transaction with method has been selected earlier
     * @throws  InterruptedException
     */
    public void showResultData (String resultMethod) throws  InterruptedException {
        WebElement element = driver.findElement(By.xpath("(//td[contains(.,'"+resultMethod+"')])[1]"));
        selenium.waitTillElementIsVisible(element);
    }
    /**
     * user input schdule date from and to
     * @throws  InterruptedException
     */
    public void choosescheduleDate (String From, String To) throws InterruptedException {
        selenium.enterText(inputScheduleFrom, From,true);
        selenium.enterText(inputScheduleTo, To,true);
    }

    /**
     * verify data transaction that already selected
     * @throws  InterruptedException
     */
    public void showDataBaseOnSchduleDate () throws InterruptedException {
        selenium.isElementDisplayed(dataScheduledDate);
    }

    /**
     * input nominal from and to
     * @throws  InterruptedException
     */
    public void inputValueAmount (String nominalFrom, String nominalTo) throws InterruptedException {
        selenium.enterText(valueFrom, nominalFrom, true);
        selenium.enterText(valueTo, nominalTo,true);
    }

    /**
     * verify data based on nominal
     * @throws  InterruptedException
     */

    public void showRsultBasedOnNominal (String dataNominal) throws  InterruptedException {
        WebElement element = driver.findElement(By.xpath("(//td[.='"+dataNominal+"'])[1]"));
        selenium.waitTillElementIsVisible(element);
    }

    /**
     * choose status transaction
     * @throws  InterruptedException
     */
    public void selectDetailStatus (String statusTransaction) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//option[.='"+statusTransaction+"']"));
        selenium.clickOn(element);
    }

    /**
     * verify data transaction based on status
     */
    public void resultDataBasedOnStatus (String dataStatus){
        WebElement element = driver.findElement(By.xpath("(//span[.='"+dataStatus+"'])[1]"));
        selenium.waitTillElementIsVisible(element);
    }

    /**
     * select order type
     * @throws  InterruptedException
     */

    public void selectOrderType (String selectOrderType) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//option[.='"+selectOrderType+"']"));
        selenium.clickOn(element);
    }

    /**
     * verify data based on order type
     * @throws  InterruptedException
     */

    public void resultDataBasedOnOrderType (String resultType) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("(//td[contains(.,'"+resultType+"')])[1]"));
        selenium.waitTillElementIsVisible(element);
    }

    /**
     * verify data based other invoice booking
     * @throws  InterruptedException
     */

    public void getDataInvoice (String otherInvoiceBooking) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//td[.='"+otherInvoiceBooking+"']"));
        selenium.waitTillElementIsVisible(element);
    }
















}
