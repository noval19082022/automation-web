package pageobjects.mamikos.payment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;
import org.openqa.selenium.support.ui.Select;

import javax.xml.xpath.XPath;

public class GpInvoiceListPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public GpInvoiceListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);
        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//a[contains(.,'Goldplus Invoice List')]")
    private WebElement openMenuGpInvoice;
    @FindBy(xpath = "//input[@name='search_value']")
    private WebElement inputValue;
    @FindBy(xpath = "//input[@class='btn btn-primary btn-md']")
    private WebElement clickCariGpInvoice;
    @FindBy(xpath = "//th[.='Invoice Number']")
    private WebElement InvoiceNumber;
    @FindBy(xpath = "//th[.='Invoice Name']")
    private WebElement InvoiceName;
    @FindBy(xpath = "//th[.='Due Date']")
    private WebElement DueDate;
    @FindBy(xpath = "//th[.='Expiry Date']")
    private WebElement ExpiryDate;
    @FindBy(xpath = "//th[.='Package Type']")
    private WebElement PackageType;
    @FindBy(xpath = "//th[.='Billing Type']")
    private WebElement BillingType;
    @FindBy(xpath = "//th[.='Total Amount']")
    private WebElement TotalAmount;
    @FindBy(xpath = "//th[.='Owner/Renter']")
    private WebElement OwnerRenter;
    @FindBy(xpath = "//th[.='Invoice Status']")
    private WebElement InvoiceStatus;
    @FindBy(xpath = "//tbody/tr[1]")
    private WebElement valuesofgpdetails;
    @FindBy(xpath = "//div[@class='box-footer']")
    private WebElement getBlankData;
    @FindBy(xpath = "//option[.='Owner Phone Number']")
    private WebElement selectOwnerNumber;
    @FindBy(xpath = "//option[.='Invoice Code']")
    private WebElement selectInvoiceCode;
    @FindBy(xpath = "//a[.='Reset']")
    private WebElement clickButtonReset;
    @FindBy(xpath = "//option[.='Invoice Number']")
    private WebElement searchByBackToRestarted;
    @FindBy(xpath = "//span[text()='unpaid']")
    private WebElement willShownUnpaidInvoice;
    @FindBy(xpath = "//input[@name='schedule_date_from']")
    private WebElement scheduleForm;
    @FindBy(xpath = "//input[@name='schedule_date_to']")
    private WebElement scheduleTo;
    @FindBy(xpath = "//div[@class='col-xs-12']/div[1]//input[@name='phone_number']")
    private WebElement inputPhoneNumberReset;
    @FindBy(xpath = "//input[@value='Reset']")
    private WebElement buttonResetGP;
    @FindBy(xpath = "//div[@class='col-xs-12']/div[2]//input[@name='phone_number']")
    private WebElement inputPhoneNumberGPRecurring;
    @FindBy(xpath = "//input[@value='Create Recurring']")
    private WebElement buttonRecurringGP;
    @FindBy(xpath = "//div[@class='callout callout-success']")
    private WebElement succsessNotif;








    /**
     * Open menu GPInvoice List
     * @throws  InterruptedException
     */
    public void openMenuGpInvoice () throws InterruptedException{
        selenium.clickOn(openMenuGpInvoice);
    }

    /**
     * Input valid GP Invoice
     * @throws  InterruptedException
     */

    public void inputGPinvoice (String GPinvoice) throws  InterruptedException {
        selenium.clickOn(inputValue);
        selenium.enterText(inputValue, GPinvoice, true);
    }

    /**
     * Click cari invoice button
     * @throws  InterruptedException
     */

    public void clickCariGpInvoice () throws  InterruptedException {
        selenium.clickOn(clickCariGpInvoice);
    }

    /**
     * Verify GP invoice detail
     * @throws  InterruptedException
     */

    public void verifyGpInvoiceDetail () throws InterruptedException {
        selenium.isElementDisplayed(InvoiceNumber);
        selenium.isElementDisplayed(InvoiceName);
        selenium.isElementDisplayed(DueDate);
        selenium.isElementDisplayed(ExpiryDate);
        selenium.isElementDisplayed(PackageType);
        selenium.isElementDisplayed(BillingType);
        selenium.isElementDisplayed(TotalAmount);
        selenium.isElementDisplayed(OwnerRenter);
        selenium.isElementDisplayed(InvoiceStatus);
        selenium.isElementDisplayed(valuesofgpdetails);
    }

    /**
     * blank screen for admin that input invalid number,invoice, and invoice code
     * @throws  InterruptedException
     */

    public void getBlankData () {
        selenium.getText(getBlankData);

    }

    /**
     * Input owner number
     * @throws  InterruptedException
     */

    public void inputOwnerNumber (String OwnerNumber)  throws  InterruptedException{
        selenium.clickOn(inputValue);
        selenium.enterText(inputValue, OwnerNumber, true);
    }

    /**
     * Choose owner number
     * @throws  InterruptedException
     */

    public void selectOwnerNumber () throws  InterruptedException {
        selenium.clickOn(selectOwnerNumber);
    }

    /**
     * Choose Invoice code
     * @throws  InterruptedException
     */

    public void selectInvoiceCode () throws  InterruptedException{
        selenium.clickOn(selectInvoiceCode);
    }

    /**
     * Input invoice code
     * @throws  InterruptedException
     */

    public void inputInvoiceCode (String InvoiceCode) throws InterruptedException{
        selenium.clickOn(inputValue);
        selenium.enterText(inputValue, InvoiceCode,true);
    }

    /**
     * Click Button Reset
     * @throws  InterruptedException
     */

    public void clickButtonReset () throws  InterruptedException {
        selenium.clickOn(clickButtonReset);
    }

    /**
     * search by back to basic
     * @throws  InterruptedException
     */

    public void searchByBackToRestarted () throws  InterruptedException {
        selenium.isElementDisplayed(searchByBackToRestarted);
    }

    /**
     * choose one of GP status
     * @throws  InterruptedException
     */

    public void userSelectStatusGP (String GPstatus) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//option[.='"+GPstatus+"']"));
        selenium.clickOn(element);
    }

    /**
     * show detail GP invoice according GP status
     * @throws  InterruptedException
     */

    public void willShowAccordingGPStatus (String resultGPInvoice) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("(//span[.='"+resultGPInvoice+"'])[1]"));
        selenium.waitTillElementIsVisible(element);
    }

    /**
     * choose one of GP type
     * @throws  InterruptedException
     */

    public void selectGPtype (String gpType) throws  InterruptedException {
        By option = By.xpath("//select[@name='package_type']/option[@value = '"+gpType+"']");
        driver.findElement(option).click();
    }

    /**
     * show detail GP invoice according GP type
     * @throws  InterruptedException
     */


    public void shownGPdetail (String gpDetail) throws  InterruptedException {
        WebElement element = driver.findElement(By.xpath("//td[.='"+gpDetail+"']"));
        selenium.waitTillElementIsVisible(element);

    }

    /**
     * input due date detail
     * @throws  InterruptedException
     */

    public void chooseDueDateFrom (String date1, String date2) throws  InterruptedException {
        selenium.enterText(scheduleForm, date1, true);
        selenium.enterText(scheduleTo, date2, true);
    }

    /**
     * Show data transaction according due date detail
     * @throws  InterruptedException
     */


    public void showDetailTransactionAccordingDetailDate () throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//td[.='2021-03-08']"));
        selenium.waitTillElementIsVisible(element);
    }

    /**
     * Input phone number for reset GP
     * @throws  InterruptedException
     */

    public void inputPhoneNumberGP (String phoneNumberGP) throws InterruptedException{
        selenium.enterText(inputPhoneNumberReset, phoneNumberGP, true);
    }


    /**
     * Click on Button Reset GP
     * @throws  InterruptedException
     */

    public void clickButtonResetGP() throws InterruptedException{
        selenium.javascriptClickOn(buttonResetGP);
        selenium.hardWait(2);
    }

    /**
     * Input phone number for recurring GP
     * @throws  InterruptedException
     */

    public void inputPhoneNumberGPrecurring (String phoneNumberGPrecurring) throws InterruptedException{
        selenium.enterText(inputPhoneNumberGPRecurring, phoneNumberGPrecurring, true);
    }


    /**
     * Click on Button Recurring GP
     * @throws  InterruptedException
     */

    public void clickButtonRecurringGP() throws InterruptedException{
        selenium.javascriptClickOn(buttonRecurringGP);
        selenium.hardWait(2);
    }


    /**
     * Get Text notif succsess recurring and reset GP
     * @return text
     */
    public String getTexSuccsessNotif() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.getText(succsessNotif);
    }

}
