package pageobjects.mamikos.payment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;


public class invoiceSecurityPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public invoiceSecurityPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }


//    @FindBy(xpath = "//a[normalize-space()='"+ invoice+"']")
//    private WebElement linkInvoice;

    @FindBy(xpath = "//h1[contains(text(),'Invoice Kedaluwarsa')]")
    private WebElement shownInvoiceNotFound;

    @FindBy(xpath = "//p[contains(text(),'Pembayaran Berhasil')]")
    private WebElement shownInvoicePaid;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--heading-1 ']")
    private WebElement shownInvoiceUnpaid;

    @FindBy(xpath = "(//h1[normalize-space()='Invoice Kedaluwarsa'])[1]")
    private WebElement shownInvoiceUnpaidKedaluarsa;

    @FindBy(xpath = "//h1[normalize-space()='Invoice Tidak Ditemukan']")
    private WebElement invoiceTidakDitemukan;


    /**
     * Click link invoice
     *
     * @throws InterruptedException
     */
    public void clickLinkInvoice(String shortlink) throws InterruptedException {
        WebElement elementShortlink = driver.findElement(By.xpath("//a[normalize-space()='"+ shortlink +"']"));
        selenium.clickOn(elementShortlink);
    }

    /**
     * Click shortlink all invoice
     *
     * @throws InterruptedException
     */
    public void clickFirstShortlink() throws InterruptedException {
        WebElement elementShortlink = driver.findElement(By.cssSelector("tbody tr:nth-child(1) td:nth-child(1) a"));
        selenium.clickOn(elementShortlink);
    }

    /**
     * Click riwayat button
     *
     * @throws InterruptedException
     */
    public void clickRiwayatButton() throws InterruptedException {
        String element = "//div[@class='history-caption']";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 5);
        selenium.clickOn(By.xpath(element));
    }

    /**
     * Click selesai button
     *
     * @throws InterruptedException
     */
    public void clickSelesaiButton() throws InterruptedException {
        String element = "//h4[.='Selesai']";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 5);
        selenium.clickOn(By.xpath(element));
    }

    /**
     * Click dalam proses button
     *
     * @throws InterruptedException
     */
    public void dalamProsesButton() throws InterruptedException {
        String element = "//h4[.='Dalam Proses']";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 5);
        selenium.clickOn(By.xpath(element));
    }

    /**
     * user choose invoice paid
     *
     * @throws InterruptedException
     */
    public void chooseInvoicePaid() throws InterruptedException {
        String element = "(//div[contains(@class,'transaction-done__history-list bg-c-grid bg-c-grid--vtop bg-c-grid--left bg-c-grid--desktop')])[1]";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 20);
        selenium.clickOn(By.xpath(element));
    }

    /**
     * user choose invoice unpaid
     *
     * @throws InterruptedException
     */
    public void chooseInvoiceUnpaid() throws InterruptedException {
        String element = "(//div[contains(@class,'transaction-on-process__history-list bg-c-grid bg-c-grid--vtop bg-c-grid--left bg-c-grid--desktop')])[1]";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 5);
        selenium.clickOn(By.xpath(element));
    }

    /**
     * user choose goldplus invoice list
     *
     * @throws InterruptedException
     */
    public void openMenuGpInvoiceList() throws InterruptedException {
        String element = "//a[contains(.,'Goldplus Invoice List')]";
        selenium.waitInCaseElementClickable(driver.findElement(By.xpath(element)), 5);
        selenium.clickOn(By.xpath(element));
    }

    /**
     * show page Invoice Not Found
     *
     * @return
     */
    public String showInvoiceNotFound() throws InterruptedException {
        selenium.switchToWindow(0);
        selenium.hardWait(3);
        return selenium.getText(shownInvoiceNotFound);
    }

    /**
     * show page Invoice Paid
     *
     * @return
     */
    public String getInvoicePaid() {
        selenium.switchToWindow(0);
        selenium.waitInCaseElementVisible(shownInvoicePaid,50);
        return selenium.getText(shownInvoicePaid);
    }

    /**
     * show page Invoice not found
     *
     * @return
     */
    public String showInvoiceTidakDitemukan() {
        selenium.switchToWindow(0);
        selenium.waitInCaseElementVisible(invoiceTidakDitemukan,50);
        return selenium.getText(invoiceTidakDitemukan);
    }

    /**
     * show page Invoice not found false
     *
     * @return
     */
    public boolean isInvoiceTidakDitemukan(){
        selenium.switchToWindow(0);
        selenium.waitInCaseElementVisible(invoiceTidakDitemukan,50);
        return selenium.isElementDisplayed(invoiceTidakDitemukan);
    }
    /**
     * getInvoicePaid
     *
     * @return
     */
    public boolean isInvoicePaid(){
        return selenium.isElementDisplayed(shownInvoicePaid);
    }

    /**
     * show page Invoice unpaid
     *
     * @return
     */
    public String showInvoiceUnpaid() throws InterruptedException {
        selenium.switchToWindow(0);
        selenium.hardWait(3);
        return selenium.getText(shownInvoiceUnpaid);
    }

    /**
     * show page Invoice Kedaluarsa
     *
     * @return
     */
    public String showInvoiceUnpaidKedaluarsa() throws InterruptedException {
        selenium.switchToWindow(0);
        selenium.hardWait(3);
        return selenium.getText(shownInvoiceUnpaidKedaluarsa);
    }
    /**
     * show page Invoice Kedaluarsa false
     *
     * @return
     */
    public boolean isInvoiceKedaluarsa() throws InterruptedException {
        selenium.switchToWindow(0);
        selenium.hardWait(3);
        return selenium.isElementDisplayed(shownInvoiceUnpaidKedaluarsa);
    }
    /**
     * show page Invoice unpaid false
     *
     * @return
     */
    public boolean isShowInvoiceUnpaid() throws InterruptedException {
        selenium.switchToWindow(0);
        selenium.hardWait(3);
        return selenium.isElementDisplayed(shownInvoicePaid);
    }

}


