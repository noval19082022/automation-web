package pageobjects.mamikos.payment;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class AddOnsPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AddOnsPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//*[@href='/backoffice/add-on']")
    private WebElement addOnsListMenu;

    @FindBy(xpath = "//a[.='Create Add On']")
    private WebElement createAddOnButton;

    @FindBy(id = "addon-confirm-title")
    private WebElement mandatoryPopUp;

    @FindBy(xpath = "//*[@class='btn btn-default']")
    private WebElement closePopUpButton;

    @FindBy(name = "short_description")
    private WebElement addOnsDescriptionTexBox;

    @FindBy(id = "name-add-on")
    private WebElement addOnsNameTexBox;

    @FindBy(name = "initial_price")
    private WebElement addOnsPriceTexBox;

    @FindBy(xpath = "//th[.='Add Ons ID']")
    private WebElement addOnsIDField;

    @FindBy(xpath = "//th[.='Nama Add Ons']")
    private WebElement nameAddOnsField;

    @FindBy(xpath = "//th[.='Deskripsi']")
    private WebElement deskripsiField;

    @FindBy(xpath = "//th[.='Harga']")
    private WebElement hargaField;

    @FindBy(xpath = "//th[.='Last updated at']")
    private WebElement lastUpdateField;

    @FindBy(xpath = "//th[.='Note']")
    private WebElement noteField;

    @FindBy(xpath = "//th[.='Action']")
    private WebElement actionField;

    @FindBy(xpath = "(//*[@class='btn btn-sm btn-block btn-primary'])[4]")
    private WebElement editButton;

    @FindBy(xpath = "(//*[@class='btn btn-sm btn-block btn-danger'])[5]")
    private WebElement deleteButton;

    @FindBy(id = "id-add-on")
    private WebElement addOnsIDTextBox;

    @FindBy(name = "note")
    private WebElement noteTextBox;

    @FindBy(xpath = "//a[.='Cancel']")
    private WebElement cancelButtonOnFormEdit;

    @FindBy(xpath = "//a[.='Update Add On']")
    private WebElement updateAddOnButtun;

    @FindBy(id = "addon-confirmation-button")
    private WebElement yesUpdateAddOnButton;

    @FindBy(xpath = "//*[@class='btn btn-default']")
    private WebElement cancelUpdateAddOnButton;

    @FindBy(xpath = "(//*[@class='btn btn-default'])[2]")
    private WebElement deleteAddOnsPopup;

    @FindBy(xpath = "(//*[@class='btn btn-default'])[6]")
    private WebElement deleteButtonOnPopup;

    @FindBy(xpath = "//img[@alt='ic_close']")
    private WebElement closeUpdateAddOnButton;

    @FindBy(xpath = "//*[@class='callout callout-success']")
    private WebElement successAlert;

    @FindBy(xpath = "//textarea[@name='note']")
    private WebElement addNote;

    @FindBy(id = "addon-confirmation-button")
    private WebElement yesButton;

    /**
     * Click on Add Ons List Menu From Left Bar
     * @throws InterruptedException
     */
    public void clickOnAddOnsListMenu() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(addOnsListMenu);
        selenium.hardWait(3);
    }

    /**
     * Click on Create Add On Button
     * @throws InterruptedException
     */
    public void clickOnCreateAddOnsButton() throws InterruptedException {
        selenium.clickOn(createAddOnButton);
        selenium.hardWait(3);
    }

    /**
     * Get Message Pop Up Mandatory Fields
     * @throws InterruptedException
     */
    public String getMessagePopUpMandatoryField() throws InterruptedException {
        selenium.waitInCaseElementVisible(mandatoryPopUp,3);
        return selenium.getText(mandatoryPopUp);
    }

    /**
     * Click on Close Pop Up Button
     * @throws InterruptedException
     */
    public void clickOnClosePopUpButton() throws InterruptedException {
        selenium.clickOn(closePopUpButton);
        selenium.hardWait(3);
    }


    /**
     * Enter text description add ons
     * @throws InterruptedException
     */
    public void enterAddOnsDescription(String description) throws InterruptedException {
        selenium.enterText(addOnsDescriptionTexBox, description, true);
        selenium.hardWait(3);
    }

    /**
     * Get description add ons
     * @throws InterruptedException
     */
    public String getAddOnsDescription() throws InterruptedException {
        return selenium.getElementAttributeValue(addOnsDescriptionTexBox,"value");
    }

    /**
     * Enter text Price add ons
     * @throws InterruptedException
     */
    public void enterAddOnsPrice(String price) throws InterruptedException {
        selenium.enterText(addOnsPriceTexBox, price, true);
        selenium.hardWait(3);
    }

    /**
     * Enter text add ons name
     * @throws InterruptedException
     */
    public void enterAddOnsName(String name) throws InterruptedException {
        selenium.enterText(addOnsNameTexBox, name, true);
        selenium.hardWait(3);
    }

    /**
     * Get name add ons
     * @throws InterruptedException
     */
    public String getAddOnsName() throws InterruptedException {
        return selenium.getElementAttributeValue(addOnsNameTexBox,"value");
    }

    /**
     * Check fields on add ons list
     * @throws InterruptedException
     */
    public void checkFieldsOnAddOnsList() throws InterruptedException {
        selenium.waitInCaseElementVisible(addOnsIDField,1);
        selenium.waitTillElementIsVisible(nameAddOnsField);
        selenium.waitTillElementIsVisible(deskripsiField);
        selenium.waitTillElementIsVisible(hargaField);
        selenium.waitTillElementIsVisible(lastUpdateField);
        selenium.waitTillElementIsVisible(noteField);
        selenium.waitTillElementIsVisible(actionField);
    }

    /**
     * Click edit Button
     * @throws InterruptedException
     */
    public void clickEditAddOnsButton() throws InterruptedException {
        selenium.clickOn(editButton);
        selenium.hardWait(3);
    }

    /**
     * Click delete add ons
     * @throws InterruptedException
     */
    public void clickDeleteAddOnsButton() throws InterruptedException {
        selenium.clickOn(deleteButton);
        selenium.hardWait(3);
    }

    /**
     * Check fields on edit add ons form
     * @throws InterruptedException
     */
    public void checkFieldsEditOnAddOnsForm() throws InterruptedException {
        selenium.waitInCaseElementVisible(addOnsIDTextBox,1);
        selenium.waitTillElementIsVisible(addOnsNameTexBox);
        selenium.waitTillElementIsVisible(addOnsDescriptionTexBox);
        selenium.waitTillElementIsVisible(addOnsPriceTexBox);
        selenium.waitTillElementIsVisible(noteTextBox);
        selenium.waitTillElementIsVisible(cancelButtonOnFormEdit);
        selenium.waitTillElementIsVisible(updateAddOnButtun);
    }

    /**
     * Click update add ons Button
     * @throws InterruptedException
     */
    public void clickUpdateAddOnsButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(updateAddOnButtun);
    }

    /**
     * Click yes to update add ons
     * @throws InterruptedException
     */
    public void clickYesUpdateAddOnsButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(yesUpdateAddOnButton);
    }

    /**
     * Click cancel to update add ons
     * @throws InterruptedException
     */
    public void clickCancelUpdateAddOnsButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(cancelUpdateAddOnButton);
    }

    /**
     * Click close to update add ons
     * @throws InterruptedException
     */
    public void clickCloseUpdateAddOnsButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(closeUpdateAddOnButton);
    }


    /**
     * Click update add ons Button
     * @throws InterruptedException
     */
    public void verifyUpdateAddOns() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(successAlert);
    }

    /**
     * Enter text notes add ons
     * @throws InterruptedException
     */
    public void enterAddOnsNotes(String notes) throws InterruptedException {
        selenium.enterText(noteTextBox, notes, true);
        selenium.hardWait(3);
    }

    /**
     * Get description add ons
     * @throws InterruptedException
     */
    public String getAddOnsNotes() throws InterruptedException {
        return selenium.getElementAttributeValue(noteTextBox,"value");
    }

    /**
     * Click cancel add ons Button
     * @throws InterruptedException
     */
    public void clickCancelAddOnsButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(cancelButtonOnFormEdit);
    }

    /**
     * show pop-up delete add ons
     * @throws InterruptedException
     */
    public void showDeleteAddOnsPopup() throws InterruptedException {
        selenium.hardWait(3);
        selenium.isElementDisplayed(deleteAddOnsPopup);
    }

    /**
     * click button delete on pop up and show success
     * @throws InterruptedException
     */
    public void deleteAndSuccessDelete() throws InterruptedException {
        selenium.hardWait(3);
        selenium.javascriptClickOn(deleteButtonOnPopup);
        selenium.hardWait(2);
        selenium.waitTillElementIsVisible(successAlert);
    }
    /**
     * input note for add ons
     * @throws InterruptedException
     */

    public void userInputNote (String inputNote) throws InterruptedException {
        selenium.enterText(addNote, inputNote, true);
    }

    /**
     * to confirm on popup create addons
     * @throws InterruptedException
     */

    public void clickYesToCreate () throws InterruptedException {
        selenium.clickOn(yesButton);
    }

}
