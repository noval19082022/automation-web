package pageobjects.mamikos.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BannerPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public BannerPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.WEBDRIVER_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[@class='next-link']" )
    private WebElement nextLink;

    @FindBy(xpath = "//*[@alt='Icon close']" )
    private WebElement closeIcon;

    @FindBy(xpath = "//*[@class='btn btn-danger']" )
    private WebElement yesButton;

    @FindBy(xpath = "//button[@data-id='CLOSE']" )
    private WebElement closeButton;

    /**
     * Click on skip banner link
     * @throws InterruptedException
     */
    public void skipBanner() throws InterruptedException {
        selenium.hardWait(3);
        while (selenium.isElementDisplayed(nextLink)){
            selenium.clickOn(nextLink);
        }
        selenium.hardWait(3);
        if (selenium.isElementDisplayed(closeIcon)){
            selenium.clickOn(closeIcon);
            selenium.clickOn(yesButton);
        }
    }

    /**
     * Click on close banner pesta promo
     * @throws InterruptedException
     */
    public void skipBannerPromPromoParty() throws InterruptedException {
        if (selenium.isElementDisplayed(closeButton)){
            selenium.clickOn(closeButton);
        }
    }
}
