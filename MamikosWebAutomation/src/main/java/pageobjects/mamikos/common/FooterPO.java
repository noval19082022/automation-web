package pageobjects.mamikos.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class FooterPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public FooterPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.WEBDRIVER_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(linkText = "Syarat dan Ketentuan Umum" )
    private WebElement termandcons;

    /**
     * Click on 'Syarat dan Ketentuan' Link
     * @throws InterruptedException
     */
    public void clickOnTermAndConditions() throws InterruptedException {
        selenium.pageScrollInView(termandcons);
        selenium.clickOn(termandcons);
        selenium.switchToWindow(0);
        selenium.hardWait(2);
    }
}
