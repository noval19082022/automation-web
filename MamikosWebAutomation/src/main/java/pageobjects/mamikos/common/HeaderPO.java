package pageobjects.mamikos.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class HeaderPO {

    WebDriver driver;
    SeleniumHelpers selenium;
    @FindBy(xpath = "//*[@data-testid='entryButton']")
    private WebElement enterButton;

    // ---------- Before Login -------------
    @FindBy(xpath = "(//li[@class='user-nav user-login hidden-xs'])[1]")
    private WebElement theEnterButton;
    @FindBy(xpath = "//a[@href='/booking']")
    private WebElement bookingKosButton;
    @FindBy(css = ".nav-topbar-left .nav-topbar-url:first-child")
    private WebElement downloadAppButton;
    @FindBy(css = ".nav-topbar-right .nav-topbar-url")
    private WebElement promoAdsButton;
    @FindBy(xpath = "//div[@class = 'nav-main-link']")
    private WebElement searchAdsButton;
    @FindBy(css = "li:nth-child(1) .bg-c-dropdown__menu-item")
    private WebElement searchKosAdsDropdown;
    @FindBy(css = "li:nth-child(2) .bg-c-dropdown__menu-item")
    private WebElement searchApartemenAdsDropdown;
    @FindBy(css = "#globalNavbar .nav-main-li:nth-child(2) .nav-main-link")
    private WebElement helpCenterButton;

    //  Commented because the job vacancy page is temporarily hidden
//  @FindBy(css = "li:nth-child(3) .bg-c-dropdown__menu-item")
//  private WebElement searchJobAdsDropdown;
    @FindBy(css = "#globalNavbar .nav-main-li:nth-child(3) .nav-main-link")
    private WebElement termAndConditionButton;
    @FindBy(xpath = "//div[@class='user-profile-dropdown__trigger']")
    private WebElement tenantProfilePicture;

    //------------------- Login Tenant ----------------------
    @FindBy(xpath = "//*[@class='user-profil-img']")
    private WebElement tenantProfileNameDropdown;
    @FindBy(xpath = "//li/a[contains(.,'Favorit')]")
    private WebElement favoriteButton;
    @FindBy(xpath = "(//li[@class = 'nav-main-li'])[2]")
    private WebElement chatHeaderButton;
    @FindBy(xpath = "//div[@class = 'price-card-container']//div[@class = 'card-footer']/button")
    private WebElement chatButton;
    @FindBy(css = ".nav-main-li:nth-child(3) .nav-main-link")
    private WebElement chatInGreenHeaderButton;
    @FindBy(css = ".chat-sheet .mc-channel-list__header button")
    private WebElement chatRoomButton;
    @FindBy(css = "#notifCenter .nav-notification")
    private WebElement notificationButton;
    @FindBy(css = "#notificationContent .notif-wrapper")
    private WebElement notificationList;
    @FindBy(css = "#notificationContent .notif-content-empty .notif-empty-text-caption")
    private WebElement emptyNotificationList;
    @FindBy(xpath = "(//div/div[@class='nav-main-link'])[2]")
    private WebElement otherButton;
    @FindBy(css = "#globalNavbar .nav-others .dropdown-menu li:first-child a")
    private WebElement helpCenterDropdown;
    @FindBy(css = "#globalNavbar .nav-others .dropdown-menu li:last-child a")
    private WebElement termConditionDropdown;
    @FindBy(css = "div .user-profile-dropdown__item:first-child")
    private WebElement tenantProfile;
    @FindBy(xpath = "//*[@class='user-profile-dropdown__item'][contains(., 'Riwayat Transaksi')]")
    private WebElement transactionHistoryButton;
    @FindBy(xpath = "//*[@data-testid='exitButton']")
    private WebElement tenantlogoutButton;
    @FindBy(css = ".c-mk-header__username")
    private WebElement ownerUserName;

    //------------------- Login Owner ----------------------
    @FindBy(css = "[href='/auth/logout']")
    private WebElement ownerLogoutButton;
    @FindBy(xpath = "//a[contains(.,'Halaman Pemilik')]")
    private WebElement ownerPageButton;
    @FindBy(css = ".c-mk-header__menu .c-mk-header__menu-item:first-child a")
    private WebElement helpCenterOwnerButton;
    @FindBy(css = ".c-mk-header__menu .c-mk-header__menu-item:nth-child(3) a")
    private WebElement notificationOwnerButton;
    @FindBy(css = ".c-mk-header__menu .c-mk-header__menu-item:nth-child(3) a .bg-c-badge-counter--red")
    private WebElement newNotification;
    @FindBy(xpath = "//*[@class='c-mk-header-user__logout']")
    private WebElement logoutOwnerPageButton;
    @FindBy(css = ".c-mk-header-user__setting")
    private WebElement accountSettingsButton;
    @FindBy(xpath = "//a[@href='https://help.mamikos.com/pemilik']")
    private WebElement homeHelpCenterButton;
    @FindBy(css = ".navbar-icon[src='/assets/logo/svg/logo_mamikos_white.svg']")
    private WebElement homeLogoButton;
    @FindBy(css = ".c-mk-header-user__name")
    private WebElement ownerUserNameInPopUp;
    @FindBy(css = ".c-mk-header-user__email")
    private WebElement ownerPhoneInPopUp;
    @FindBy(xpath = "//p[.='Chat']")
    private WebElement ownerChatButton;
    @FindBy(xpath = "(//div[@class='notif-text']/child::*[2])[1]")
    private WebElement boxLastNotifContent;
    @FindBy(className = "c-notification__see-more")
    private WebElement seeMoreNotificationLabel;
    @FindBy(xpath = "//a[contains(., '×')]")
    private WebElement closeNotification;
    @FindBy(css = ".c-notification__item:first-child .c-notification__item-message")
    private WebElement txtNotificationContentFirstList;
    @FindBy(xpath = "//nav[@class='nav-main-navbar']")
    private WebElement navbarMenu;
    @FindBy(xpath = "//input[@class='bg-c-input__field']")
    private WebElement searchChat;
    @FindBy(xpath = "//input[@class='bg-c-input__field']")
    private WebElement noClickSearchChat;
    @FindBy(xpath = "//button[normalize-space()='Apa itu kuota chat room?']")
    private WebElement FTUE;
    @FindBy(xpath = "//*[@id=\"tooltipContent\"]/div[2]/div[2]/button[2]")
    private WebElement sayaMengerti;
    @FindBy(xpath = "//*[@id=\"tooltipContent\"]/div[2]/div/button")
    private WebElement closeTooltipBroadcast;

    public HeaderPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * Click on Enter button
     *
     * @throws InterruptedException
     */
    public void clickOnEnterButton() throws InterruptedException {
        selenium.hardWait(2);
        int i = 0;
        while (isNavbarMenuVisible()) {
            i++;
            if (i == 5) {
                break;
            } else if (isEnterBtnVisible()) {
                selenium.clickOn(enterButton);
                break;
            } else if (!isEnterBtnVisible()) {
                selenium.waitTillElementIsVisible(tenantProfilePicture);
                selenium.hardWait(5);
                selenium.clickOn(tenantProfilePicture);
                selenium.hardWait(5);
                selenium.clickOn(tenantlogoutButton);
                selenium.hardWait(5);
            }
        }
    }

    /**
     * if the enter (Masuk) button is visible
     *
     * @return true means the enter (Masuk) is visible, false means otherwise
     */
    public boolean isEnterBtnVisible() {
        return selenium.waitInCaseElementClickable(enterButton, 5) != null;
    }

    /**
     * if the navbar menu is visible
     *
     * @return true means the navbar menu is visible, false means otherwise
     */
    public boolean isNavbarMenuVisible() {
        return selenium.waitInCaseElementVisible(navbarMenu, 3) != null;
    }

    /**
     * Enter Button is  Displayed
     *
     * @return true / false
     */
    public boolean isEnterButtonDisplayed() {
        return selenium.waitInCaseElementVisible(enterButton, 5) != null;
    }

    /**
     * Tenant Profile Picture is  Displayed
     *
     * @return Tenant Profile Picture
     */
    public boolean isTenantProfilePictureDisplayed() {
        return selenium.waitInCaseElementVisible(tenantProfilePicture, 5) != null;
    }

    /**
     * Check element booking kos button header is displayed
     *
     * @return status true / false
     */
    public Boolean isBookingKosDisplayed() {
        return selenium.waitInCaseElementVisible(bookingKosButton, 5) != null;
    }

    /**
     * Check element download app button header is displayed
     *
     * @return status true / false
     */
    public Boolean isDownloadAppDisplayed() {
        return selenium.waitInCaseElementVisible(downloadAppButton, 5) != null;
    }

    /**
     * Check element promosi ads button header is displayed
     *
     * @return status true / false
     */
    public Boolean isPromosiAdsDisplayed() {
        return selenium.waitInCaseElementVisible(promoAdsButton, 5) != null;
    }

    /**
     * Check element search ads button header is displayed
     *
     * @return status true / false
     */
    public Boolean isSearchAdsDisplayed() {
        return selenium.waitInCaseElementVisible(searchAdsButton, 5) != null;
    }

    /**
     * Check element Search Kos Dropdown button header is displayed
     *
     * @return status true / false
     */
    public boolean isDropdownKosDiplayed() {
        return selenium.waitInCaseElementVisible(searchKosAdsDropdown, 5) != null;
    }

    /**
     * Check element Search Apartment Dropdown button header is displayed
     *
     * @return status true / false
     */
    public boolean isDropdownApartemenDiplayed() {
        return selenium.waitInCaseElementVisible(searchApartemenAdsDropdown, 5) != null;
    }

//    /**
//     * Check element Search Job Vac Dropdown button header is displayed
//     *
//     * @return status true / false
//     */
//    public boolean isDropdownJobDiplayed() {
//        return selenium.waitInCaseElementVisible(searchJobAdsDropdown, 5) != null;
//    }

    /**
     * Check element help center button header is displayed
     *
     * @return status true / false
     */
    public Boolean isHelpCenterDisplayed() {
        return selenium.waitInCaseElementVisible(helpCenterButton, 5) != null;
    }

    /**
     * Check element help center owner button header is displayed
     *
     * @return status true / false
     */
    public Boolean isHelpCenterOwnerDisplayed() {
        return selenium.waitInCaseElementVisible(helpCenterOwnerButton, 5) != null;
    }

    /**
     * Check element help center button header is displayed
     *
     * @return status true / false
     */
    public Boolean isTermConditionDisplayed() {
        return selenium.waitInCaseElementVisible(termAndConditionButton, 5) != null;
    }

    /**
     * Check Favorite button is displayed
     *
     * @return status true or false
     */
    public boolean isFavoriteDisplayed() {
        return selenium.waitInCaseElementVisible(favoriteButton, 5) != null;
    }

    /**
     * Check Chat button is displayed
     *
     * @return status true or false
     */
    public boolean isChatDisplayed() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.waitInCaseElementVisible(chatHeaderButton, 7) != null;
    }

    /**
     * Check element notification button header is displayed
     *
     * @return status true / false
     */
    public boolean isNotificationButtonDisplayed() {
        return selenium.waitInCaseElementVisible(notificationButton, 5) != null;
    }

    /**
     * Check element notification list is displayed
     *
     * @return status true / false
     */
    public boolean isNotificationListDisplayed() {
        if (selenium.waitInCaseElementVisible(notificationList, 5) != null) {
            return selenium.waitInCaseElementVisible(notificationList, 5) != null;
        } else {
            return selenium.waitInCaseElementVisible(emptyNotificationList, 5) != null;
        }
    }

    /**
     * Check element notification button owner header is displayed
     *
     * @return status true / false
     */
    public boolean isNotificationOwnerButtonDisplayed() {
        return selenium.waitInCaseElementVisible(notificationOwnerButton, 5) != null;
    }

    /**
     * Check element other dropdown header is displayed
     *
     * @return status true / false
     */
    public boolean isOtherButtonDisplayed() {
        return selenium.waitInCaseElementVisible(otherButton, 5) != null;
    }

    /**
     * Check element Help center Dropdown button header is displayed
     *
     * @return status true / false
     */
    public boolean isHelpCenterDropdownDisplayed() {
        return selenium.waitInCaseElementVisible(helpCenterDropdown, 5) != null;
    }

    /**
     * Check element term and condition Dropdown button header is displayed
     *
     * @return status true / false
     */
    public boolean isTermConditionDropdownDisplayed() {
        return selenium.waitInCaseElementVisible(termConditionDropdown, 5) != null;
    }

    /**
     * Check Chat button is displayed
     *
     * @return status true or false
     */
    public boolean chatRoomIsDisplayed() {

        return selenium.waitInCaseElementVisible(chatRoomButton, 5) != null;
    }

    /**
     * Check element owner page is displayed
     *
     * @return status true / false
     */
    public Boolean isOwnerPageDisplayed() {
        return selenium.waitInCaseElementVisible(ownerPageButton, 3) != null;
    }

    /**
     * Check element Exit button is displayed
     *
     * @return status true / false
     */
    public Boolean isExitButtonDisplayed() {
        return selenium.waitInCaseElementVisible(ownerLogoutButton, 3) != null;
    }

    /**
     * Click on tenant Notification on header
     *
     * @throws InterruptedException
     */
    public void clickOnNotification() throws InterruptedException {
        selenium.hardWait(5);
        selenium.waitInCaseElementClickable(notificationButton, 5);
        selenium.clickOn(notificationButton);
    }

    /**
     * Click search ads button
     *
     * @throws InterruptedException
     */
    public void clickOnSearchAds() throws InterruptedException {
        selenium.clickOn(searchAdsButton);
    }

    /**
     * Click search ads button
     *
     * @throws InterruptedException
     */
    public void clickOnOtherDropdown() throws InterruptedException {
        selenium.clickOn(otherButton);
    }

    /**
     * Click chat as tenant user
     *
     * @throws InterruptedException
     */
    public void clickChat() throws InterruptedException {
        //       if (selenium.waitInCaseElementVisible(chatButton, 5) == null) {
//            selenium.clickOn(chatInGreenHeaderButton);
//        } else {
        selenium.pageScrollInView(chatHeaderButton);
        selenium.hardWait(3);
        selenium.clickOn(chatHeaderButton);
    }
    //   }

    /**
     * Click on Favorite button after click on user name
     *
     * @throws InterruptedException
     */
    public void clickOnFavouriteButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(favoriteButton);
    }

    /**
     * Click on History button after click on user name
     *
     * @param option choose option from dropdown list profile
     * @throws InterruptedException
     */
    public void clickOnProfileOptionForTenant(String option) throws InterruptedException {
        selenium.clickOn(By.xpath("(//a[text()='" + option + "'])"));
    }

    /**
     * Click on tenant profile on header
     *
     * @throws InterruptedException
     */
    public void clickOnTenantProfile() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(tenantProfilePicture);
    }

    /**
     * Click on Owner Page
     *
     * @throws InterruptedException
     */
    public void clickOnOwnerPage() throws InterruptedException {
        selenium.waitTillElementIsClickable(ownerPageButton);
        selenium.hardWait(5);
        selenium.clickOn(ownerPageButton);
    }

    /**
     * Click on first notification
     *
     * @param notificationTitle is notification title
     * @throws InterruptedException
     */
    public void clickFirstNotification(String notificationTitle) throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(By.xpath("(//div[@class='notif-text']/p[text()='Booking Dikonfirmasi, Ayo Bayar Sekarang'])[1]"));
    }

    /**
     * Click Profile Icon and click change account
     *
     * @throws InterruptedException
     */
    public void clickSettings() throws InterruptedException {
        selenium.clickOn(ownerUserName);
        selenium.clickOn(accountSettingsButton);
    }

    /**
     * Click on Booking list on profile dropdown
     *
     * @throws InterruptedException
     */
    public void clickBookingProfileDropdown() throws InterruptedException {
        selenium.waitTillElementIsClickable(transactionHistoryButton);
        selenium.hardWait(2);
        selenium.clickOn(transactionHistoryButton);
    }

    /**
     * Click on the Profile Dropdown
     *
     * @throws InterruptedException
     */
    public void clickProfileDropdown() throws InterruptedException {
        selenium.waitInCaseElementVisible(tenantProfileNameDropdown, 50);
        selenium.clickOn(tenantProfileNameDropdown);
    }

    /**
     * Check redirection header
     *
     * @return url is equal
     */
    public String getHeaderURl() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitForJavascriptToLoad();
        return selenium.getURL();
    }

    /**
     * Check redirection download app button
     *
     * @return url is equal
     */
    public String getDownloadAppURl() throws InterruptedException {
        selenium.clickOn(downloadAppButton);
        return selenium.getURL();
    }

    /**
     * Check redirection promo ads button
     *
     * @return url is equal
     */
    public String getPromoAdsURl() throws InterruptedException {
        selenium.clickOn(promoAdsButton);
        return selenium.getURL();
    }

    /**
     * Check redirection help center button
     *
     * @return url is equal
     */
    public String getHelpCenterURl() throws InterruptedException {

        selenium.clickOn(homeHelpCenterButton);
        return selenium.getURL();
    }

    /**
     * Click search ads button
     *
     * @return url is equal
     */
    public String getDropdownKos() throws InterruptedException {
        selenium.clickOn(searchKosAdsDropdown);
        return selenium.getURL();
    }

    /**
     * Check redirection dropdown apartement
     *
     * @return url is equal
     */
    public String getDropdownApartementAds() throws InterruptedException {
        selenium.clickOn(searchApartemenAdsDropdown);
        return selenium.getURL();
    }

//    Commented because the job vacancy page is temporarily hidden
//    /**
//     * Check redirection dropdown job ads
//     *
//     * @return url is equal
//     */
//    public String getDropdownJobAds() throws InterruptedException {
//        selenium.clickOn(searchJobAdsDropdown);
//        return selenium.getURL();
//    }

    /**
     * Get option list display on tenant profile
     *
     * @param data index data
     */
    public String getProfileOptionForTenant(int data) throws InterruptedException {
        selenium.hardWait(2);
        String text = selenium.getText(By.xpath("(//*[@class='user-profile-dropdown__item']/a)[" + data + "]"));
        return text;
    }

    /**
     * Click on Logout Button
     *
     * @throws InterruptedException
     */
    public void logoutAsATenant() throws InterruptedException {
        selenium.waitTillElementIsVisible(tenantProfilePicture);
        selenium.hardWait(5);
        selenium.clickOn(tenantProfilePicture);
        selenium.hardWait(5);
        selenium.clickOn(tenantlogoutButton);
        selenium.hardWait(5);
    }

    /**
     * User Log out as a Owner
     *
     * @throws InterruptedException
     */
    public void logoutAsAOwner() throws InterruptedException {
        if (selenium.waitInCaseElementVisible(ownerUserName, 3) != null) {
            selenium.clickOn(ownerUserName);
            selenium.hardWait(3);
            selenium.clickOn(logoutOwnerPageButton);
        } else {
            selenium.clickOn(tenantProfilePicture);
            selenium.hardWait(3);
            selenium.clickOn(ownerLogoutButton);
        }
        selenium.hardWait(5);
    }

    /**
     * Navigate to Tenant Profile Page
     *
     * @throws InterruptedException
     */
    public void navigateToTenantProfile() throws InterruptedException {
        selenium.refreshPage();
        selenium.clickOn(tenantProfilePicture);
        selenium.clickOn(tenantProfile);

    }

    /**
     * Switch to first tab
     */
    public void switchToFirstTab() {
        selenium.switchToWindow(1);
    }

    /**
     * Get Owner name in pop up main menu
     *
     * @return text owner name
     */
    public String getOwnerNameInPopUp() {
        selenium.waitInCaseElementVisible(ownerUserNameInPopUp, 3);
        return selenium.getText(ownerUserNameInPopUp);
    }

    /**
     * Get Owner phone number in pop up main menu
     *
     * @return text owner phone
     */
    public String getOwnerPhoneInPopUp() {
        return selenium.getText(ownerPhoneInPopUp);
    }

    /**
     * Get Settings label in pop up main menu
     *
     * @return text settings label
     */
    public String getSettingsLabel() {
        return selenium.getText(accountSettingsButton);
    }

    /**
     * Get Logout label in pop up main menu
     *
     * @return text logout label
     */
    public String getLogoutLabel() {
        return selenium.getText(logoutOwnerPageButton).trim();
    }

    /**
     * Click on the Notification Button
     *
     * @throws InterruptedException
     */
    public void clickNotificationOwner() throws InterruptedException {
        selenium.clickOn(notificationOwnerButton);
    }

    /**
     * Click on Owner user name in top right
     *
     * @throws InterruptedException
     */
    public void clickOwnerUserName() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(ownerUserName);
    }

    /**
     * Click on owner chat button on header
     *
     * @throws InterruptedException
     */
    public void clickChatOwner() throws InterruptedException {
        selenium.waitInCaseElementVisible(ownerChatButton, 5);
        selenium.javascriptClickOn(ownerChatButton);
    }

    /**
     * Search Chat
     *
     * @throws InterruptedException
     */
    public void searchChat(String inputText) throws InterruptedException {
            selenium.clickOn(searchChat);
            selenium.enterText(searchChat, inputText, true);
            selenium.hardWait(3);
    }
    /**
     * no click search chat
     */
    public boolean noClickSearchChat() {
        return selenium.isElementDisplayed(noClickSearchChat);
    }
    /**
     * click FTUE mars
     */
    public void FTUE() throws InterruptedException {
        selenium.clickOn(FTUE);
        selenium.clickOn(sayaMengerti);
        selenium.clickOn(closeTooltipBroadcast);
    }

    /**
     * Get text of last tenant notification
     *
     * @return string data type
     */
    public String getNotificationText() {
        return selenium.getText(boxLastNotifContent);
    }

    /**
     * Click last notification
     *
     * @throws InterruptedException
     */
    public void clickOnRecentNotification() throws InterruptedException {
        selenium.clickOn(boxLastNotifContent);
    }

    /**
     * Click on See More Notification
     *
     * @throws InterruptedException
     */
    public void clickOnSeeMoreNotification() throws InterruptedException {
        selenium.clickOn(seeMoreNotificationLabel);
    }

    /**
     * Click on header menu
     *
     * @throws InterruptedException
     */
    public void clickHeaderMenu(String menu) throws InterruptedException {
        selenium.clickOn(By.xpath("//span[contains(., '" + menu + "')]"));
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click on header menu
     *
     * @throws InterruptedException
     */
    public void clickOnAdsDropdown(String ads) throws InterruptedException {
        selenium.javascriptClickOn(By.xpath("//li/a[contains(., '" + ads + "')]"));
        selenium.hardWait(3);
    }

    /**
     * Click on close notification (x)
     *
     * @throws InterruptedException
     */
    public void clickOnCloseNotification() throws InterruptedException {
        selenium.clickOn(closeNotification);
    }

    /**
     * Check redirection booking kos button
     *
     * @return url is equal
     */
    public String getBookingKosURL() throws InterruptedException {
        selenium.clickOn(bookingKosButton);
        return selenium.getURL();
    }

    /**
     * Check if new notification is visible
     *
     * @return visible true, otherwise false
     */
    public boolean isNewNotifPresent() {
        return selenium.waitInCaseElementVisible(newNotification, 20) != null;
    }

    /**
     * Get new notification number as int
     *
     * @return int data type e.g 2
     */
    public int getNotificationNumber() {
        return Integer.parseInt(selenium.getText(newNotification));
    }

    /**
     * Get text on first list notification
     *
     * @return string data type
     */
    public String getNotificationFirstListText() {
        return selenium.getText(txtNotificationContentFirstList);
    }

    /**
     * Click on first list child on notification owner
     */
    public void clickOnFirstListNotification() {
        selenium.javascriptClickOn(txtNotificationContentFirstList);
    }

    /**
     * Get owner username in top right
     *
     * @return String owner username
     */
    public String getOwnerUsername() {
        return selenium.getText(ownerUserName);
    }
}
