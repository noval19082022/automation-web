package pageobjects.mamikos.jobvacancy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class JobVacancyDetailPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public JobVacancyDetailPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.WEBDRIVER_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".nav-main-ul > div:nth-of-type(1) .nav-main-link")
    private WebElement searchAdsDD;


    @FindBy(linkText = "Lowongan Kerja")
    private WebElement jobVacancyItem;

    @FindBy(className = "name")
    private WebElement companyName;

    @FindBy(className = "address")
    private WebElement companyAddress;

    @FindBy(className = "vacancy-name")
    private WebElement vacancyName;

    @FindBy(className = "last_edu")
    private WebElement lastEducation;

    @FindBy(className = "track-apply-job")
    private WebElement applyJobButton;

    @FindBy(className = "btn-mamiorange")
    private WebElement nextButton;

    @FindBy(id = "lastEducation")
    private WebElement lastEducationInput;

    @FindBy(id = "propertyAddress")
    private WebElement propertyAddressInput;

    @FindBy(id = "skillsDesc")
    private WebElement skillDescInput;

    @FindBy(id = "experienceDesc")
    private WebElement experienceDescInput;

    @FindBy(id = "inputSalary")
    private List<WebElement> salaryInput;

    @FindBy(xpath = "//label[@for='jobsPlacement']")
    private WebElement jobPlacementCheck;

    @FindBy(className = "btn-mamigreen")
    private WebElement mamiGreenButton;

    @FindBy(className = "track-submit-apply-vacancy")
    private WebElement submitButton;

    @FindBy(className = "swal2-title")
    private WebElement successModalTitle;

    @FindBy(className = "swal2-content")
    private WebElement successModalContent;

    @FindBy(className = "swal2-confirm")
    private WebElement modalButtonConfirm;

    @FindBy(className = "inactive-subtitle")
    private WebElement jobInactive;

    /**
     * Click on 'Search Ads' Dropdown
     * @throws InterruptedException
     */
    public void clickOnSearchAdsDropDown() throws InterruptedException {
       selenium.hardWait(10);
        selenium.clickOn(searchAdsDD);
    }

    /**
     * Click on 'Job Vacancy' item list  on 'Search Ads' Dropdown
     * @throws InterruptedException
     */
    public void clickOnJobVacancyItemList() throws InterruptedException {
        selenium.clickOn(jobVacancyItem);
    }

    /**
     * Click one of 'Job Vacancy' lists
     * @param jobName Job Name e.g Back-end Engineer
     * @throws InterruptedException
     */
    public void clickOneOfJobVacancies(String jobName) throws InterruptedException {
        selenium.clickOn(By.xpath("//span[contains(text(), '" + jobName + "')]"));
        selenium.switchToWindow(2);
    }

    /**
     * Click on 'Apply Job' button
     * @throws InterruptedException
     */
    public void clickOnApplyJobButton() throws InterruptedException {
        selenium.clickOn(applyJobButton);
    }

    /**
     * Click 'Next' button on Job Application Form
     * @throws InterruptedException
     */
    public void clickOnNextButton() throws InterruptedException{
        selenium.clickOn(nextButton);
    }

    /**
     * Fill out 'Owner Address' form field
     * @param address Address e.g 'Sesame Street'
     */
    public void fillPropertyAddress(String address){
        selenium.enterText(propertyAddressInput, address, false);
    }

    /**
     * Fill out Job Application Form
     * @param lastEdu e.g 'Bachelor'
     * @param skillDesc e.g 'Web Automation ...'
     * @param experienceDesc e.g 'Internship on Google'
     * @param lastSal e.g '100000'
     * @param expectedSal e.g '999999'
     * @throws InterruptedException
     */
    public void fillSecondForm(String lastEdu, String skillDesc, String experienceDesc, String lastSal, String expectedSal) throws InterruptedException {
        selenium.enterText(lastEducationInput, lastEdu, false);
        selenium.enterText(skillDescInput, skillDesc, false);
        selenium.enterText(experienceDescInput, experienceDesc, false);
        selenium.pageScrollInView(salaryInput.get(0));
        selenium.enterText(salaryInput.get(0), lastSal, false);
        selenium.enterText(salaryInput.get(1), expectedSal, false);
        selenium.clickOn(jobPlacementCheck);
        selenium.clickOn(submitButton);
    }

    /**
     * Get Company Name Text
     * @return companyName
     */
    public String getCompanyName(){
        return selenium.getText(companyName);
    }

    /**
     * Get Company Address Text
     * @return companyAddress
     */
    public String getCompanyAddress(){
        return selenium.getText(companyAddress);
    }

    /**
     * Get Vacancy Name Text
     * @return vacancyName
     */
    public String getVacancyName(){
        return selenium.getText(vacancyName);
    }

    /**
     * Get Last Education Text
     * @return lastEducation
     */
    public String getLastEducation(){
        return selenium.getText(lastEducation);
    }

    /**
     * Get Success Modal Title Text
     * @return successModalTitle
     */
    public String getSuccessModalTitle(){
        return selenium.getText(successModalTitle);
    }

    /**
     * Get Success Modal Content Text
     * @return successModalContent
     */
    public String getSuccessModalContent(){
        return selenium.getText(successModalContent);
    }

    /**
     * Click 'Ok' button on modal confirmation dialog
     * @throws InterruptedException
     */
    public void clickOnModalButtonConfirm() throws InterruptedException {
        selenium.click(modalButtonConfirm);
    }

    /**
     * Get Apply Job button Text
     * @return applyJobButton
     */
    public String getApplyJobButtonText(){
        return selenium.getText(applyJobButton);
    }

    /**
     * Get Apply Job button attribute 'disabled'
     * @return true
     */
    public Boolean isGetApplyJobButtonDisabled(){
        return selenium.getElementAttributeValue(applyJobButton, "disabled") != null;
    }

    /**
     * Click on 'Show Location' button
     * @throws InterruptedException
     */
    public void clickOnShowLocationButton() throws InterruptedException {
        selenium.pageScrollInView(mamiGreenButton);
        selenium.clickOn(mamiGreenButton);
    }

    /**
     * Get Inactive Job Message Text
     * @return jobInactive
     */
    public String getInactiveMessage(){
        return selenium.getText(jobInactive);
    }
}
