package pageobjects.mamikos.jobvacancy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class LandingJobPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public LandingJobPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.WEBDRIVER_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "[title='SQA part time'] .exp-date")
    private WebElement expDateButton;

    @FindBy(css = "strong")
    private WebElement jobTitleText;

    @FindBy(xpath = "//button[@class='btn btn-default dropdown-toggle']")
    private WebElement searchAdsButton;

    @FindBy(css = "ul.dropdown-menu [href='/loker']")
    private WebElement anotherJobsButton;

    /**
     * Click on Exp date on the Job list
     * @throws InterruptedException
     */
    public void clickOnExpDateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(expDateButton,50);
        selenium.clickOn(expDateButton);
        selenium.switchToWindow(0);
    }
    /**
     * Click on Search ads
     * @throws InterruptedException
     */
    public void clickSearchAdsButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(searchAdsButton);
    }
    /**
     * Verify Jobs title
     * @return JOb details
     */
    public Boolean isJobTitleAppear() {
        return selenium.waitInCaseElementVisible(jobTitleText,7) != null;
    }
    /**
     * Click on Another Job
     * @throws InterruptedException
     */
    public void clickAnotherJobsButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(anotherJobsButton);
    }
}
