package pageobjects.mamikos.jobvacancy;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.ArrayList;
import java.util.List;

public class SearchJobPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SearchJobPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "vacancyCriteria")
    private WebElement searchBar;

    @FindBy(xpath = "//span[@class='vacancy-title track-open-detail']")
    private WebElement titleJobResult;

    @FindBy(xpath = "//div[@id='filterHidden']/div/select")
    private WebElement jobStatusBar;

    @FindBy(xpath = "//strong[text()='Status Pekerjaan']/ancestor::label/following-sibling::select")
    private WebElement jobStatusDropdownList;

    private By jobStatusOption = By.xpath("//div[@id='filterHidden']/div[1]/select/option");

    @FindBy(xpath = "//div[@id='filterHidden']/div[2]/select")
    private WebElement lastEducationBar;

    private By lastEducationOption = By.xpath("//div[@id='filterHidden']/div[2]/select/option");

    @FindBy(xpath = "//div[@id='filterHidden']/div[3]/select")
    private WebElement sortByBar;

    private By sortByOption = By.xpath("//div[@id='filterHidden']/div[3]/select/option");

    @FindBy(css = ".count-label")
    private WebElement amountJob;

    private By companyLogoImg = By.cssSelector(".vacancy-list:nth-child(%s) .box-vacancy .vacancy-header .company-logo");

    private By jobStatus = By.cssSelector(".vacancy-list:nth-child(%s) .box-vacancy .vacancy-header .badge-vacancy");

    private By companyName = By.cssSelector(".vacancy-list:nth-child(%s) .box-vacancy .vacancy-body .company-name .company-label");

    private By applyVia = By.cssSelector(".vacancy-list:nth-child(%s) .box-vacancy .vacancy-body .company-name .apply-via.from-mamikos span");

    private By companyLocation = By.cssSelector(".vacancy-list:nth-child(%s) .box-vacancy .vacancy-body .company-location span:nth-child(1)");

    private By salaryNonActive = By.cssSelector(".vacancy-list:nth-child(%s) .box-vacancy .vacancy-salary .salary");

    private By expiredJobDate = By.cssSelector(".vacancy-list:nth-child(%s) .box-vacancy .vacancy-date .exp-date");

    private By postJobDate = By.cssSelector(".vacancy-list:nth-child(%s) .box-vacancy .vacancy-date .post-date");

    //------- Pagination ---------

    private By paginationNext = By.cssSelector("#app div.pagination-section  li:last-child a");

    /**
     * Click on search bar
     */
    public void clickOnSearchBar() throws InterruptedException {
        selenium.clickOn(searchBar);
    }

    /**
     * enter keyword
     * Tap enter button
     */
    public void setTextOnSearchBar(String keyword) throws InterruptedException {
        selenium.enterText(searchBar, keyword, false);
        searchBar.sendKeys(Keys.ENTER);
        selenium.hardWait(10);
    }

    /**
     * Get search bar value that inputed
     * Get title result job
     * @return result keyword is contains inputed text
     */
    public boolean isSearchResultPresent() {
        String inputedText = selenium.getElementAttributeValue(searchBar, "value");
        String title = selenium.getText(titleJobResult).toLowerCase();
        return title.contains(inputedText);
    }

    /**
     * Click on job status filter
     */
    public void clickOnJobStatusFilter() throws InterruptedException {
            selenium.clickOn(jobStatusBar);
    }

    /** List all listing in Job status
     * @return list of job status (String)
     */
    public List<String> listJobStatus() {
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(jobStatusOption);
        List<String> jobStatus = new ArrayList<>();
        for (WebElement s : elements) {
            jobStatus.add((selenium.getText(s)));
        }
        return jobStatus;
    }

    /**
     * Click on last education filter
     */
    public void clickOnLastEducationFilter() throws InterruptedException {
            selenium.clickOn(lastEducationBar);
    }

    /** List all listing education in last education bar
     * @return list of education (String)
     */
    public List<String> listEducation() {
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(lastEducationOption);
        List<String> education = new ArrayList<>();
        for (WebElement s : elements) {
            education.add((selenium.getText(s)));
        }
        return education;
    }

    /**
     * Click on education filter
     */
    public void clickOnSortByFilter() throws InterruptedException {
            selenium.clickOn(sortByBar);
    }

    /** List all listing in sort by bar
     * @return list of sort by (String)
     */
    public List<String> listSortBy() {
        List<WebElement> elements = selenium.waitTillAllElementsAreLocated(sortByOption);
        List<String> sortBy = new ArrayList<>();
        for (WebElement s : elements) {
            sortBy.add((selenium.getText(s)));
        }
        return sortBy;
    }

    /**
     * Get the number of Joblist on the page
     */
    public int getJobListAmountInOnePage(){
       String textAmount = selenium.getText(amountJob);
       String[] parts = textAmount.split(" ", 4);
       String partAmountFirstPage = parts[2];
       return Integer.parseInt(partAmountFirstPage);
    }

    /**
     * Change the format dinamic element to be list number
     */
    public By getJobElementListBy(By a, int listNumber){
        String targetFormatBy = a.toString().replace("By.cssSelector: ", "");
        targetFormatBy = String.format(targetFormatBy, Integer.toString(listNumber));
        return By.cssSelector(targetFormatBy);
    }

    /**
     * @param listNumber
     * @return list number of companyLogoImg
     */
    public Boolean isCompanyLogoImgPresent(int listNumber) throws InterruptedException {
        return selenium.isElementPresent(getJobElementListBy(companyLogoImg, listNumber));
    }

    /**
     * @param listNumber
     * @return list number of jobStatus
     */
    public Boolean isJobStatusPresent(int listNumber) throws InterruptedException {
        return selenium.isElementPresent(getJobElementListBy(jobStatus, listNumber));
    }

    /**
     * @param listNumber
     * @return list number of companyName
     */
    public Boolean isCompanyNamePresent(int listNumber) throws InterruptedException {
        return selenium.isElementPresent(getJobElementListBy(companyName, listNumber));
    }

    /**
     * @param listNumber
     * @return list number of applyVia
     */
    public Boolean isApplyViaElementPresent(int listNumber) throws InterruptedException {
        return selenium.isElementPresent(getJobElementListBy(applyVia, listNumber));
    }

    /**
     * @param listNumber
     * @return list number of companyLocation
     */
    public Boolean isCompanyLocationPresent(int listNumber) throws InterruptedException {
        return selenium.isElementPresent(getJobElementListBy(companyLocation, listNumber));
    }

    /**
     * @param listNumber
     * @return list number of salaryNonActive
     */
    public Boolean isSalaryElementPresent(int listNumber) throws InterruptedException {
        return selenium.isElementPresent(getJobElementListBy(salaryNonActive, listNumber));
    }

    /**
     * @param listNumber
     * @return list number of expiredJobDate
     */
    public Boolean isExpiredJobElementPresent(int listNumber) throws InterruptedException {
        return selenium.isElementPresent(getJobElementListBy(expiredJobDate, listNumber));
    }

    /**
     * @param listNumber
     * @return list number of postJobDate
     */
    public Boolean isPostJobElementPresent(int listNumber) throws InterruptedException {
        return selenium.isElementPresent(getJobElementListBy(postJobDate, listNumber));
    }

    /**
     * Scroll down to pagination element
     * Clicks on '>' to go to the next page
     * @param page is number iteration of pagination
     */
    public void clickOnPagination(int page) throws InterruptedException {
        selenium.waitTillElementIsVisible(titleJobResult, 7);
        for(int i = 1; i <= page-1; i++) {
            List<WebElement> allVacancies = driver.findElements(By.cssSelector(".box-vacancy"));
            selenium.waitTillElementsCountIsEqualTo(paginationNext,1);
            selenium.waitInCaseElementVisible(By.cssSelector(".box-vacancy"),10);
            selenium.pageScrollInView(allVacancies.get(allVacancies.size() - 1));
            selenium.clickOn(paginationNext);
            selenium.waitInCaseElementVisible(amountJob, 10);
        }
    }

    /**
     * Get text amount job
     * Split text to get specific text needed
     * @return string amount number
     */
    public String getPageAmountNumber() {
        selenium.waitInCaseElementVisible(amountJob,5);
        String[] textAmount = selenium.getText(amountJob).split(" ");
        String partAmount = textAmount[0]+" "+textAmount[1]+" "+textAmount[2];
        return partAmount;
    }
}
