package pageobjects.mamikos.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import utilities.Constants;
import utilities.SeleniumHelpers;

public class LoginPO {

	WebDriver driver;
	SeleniumHelpers selenium;

	public LoginPO(WebDriver driver) {
		this.driver = driver;
		selenium = new SeleniumHelpers(driver);

		// This initElements method will create all WebElements
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
	}

	/*
	 * All WebElements are identified by @FindBy annotation
	 * 
	 * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
	 * className, xpath as attributes.
	 */

	@FindBy(xpath = "//*[@data-testid='pemilikKosButton']")
	private WebElement kosOwnerButton;

	@FindBy(xpath = "//*[@data-testid='phoneNumberTextbox']")
	private WebElement phoneTextbox;

	@FindBy(xpath = "//*[@data-testid='passwordTextbox']")
	private WebElement passwordTextbox;

	@FindBy(xpath = "//*[@class='btn btn-primary btn-mamigreen login-button track-login-owner']")
	private WebElement enterButton;

	@FindBy(css = "[class='greeting-section__name']")
	private WebElement greetingNameText;

	//You previously signed in to Mamikos with Facebook Pop Up
	@FindBy(id = "platformDialogForm")
	private WebElement previouslySignedInPopUpFB;

	//Ok button on previously signed in pop up
	@FindBy(name = "__CONFIRM__")
	private WebElement okButtonFB;

	@FindBy(xpath = "//*[@data-testid='loginButton']")
	private WebElement enterTenantButton;

	//login by FB from kos detail
	@FindBy(css = "#detailKostContainer form.form-login-facebook > button")
	private WebElement loginByFB;

	//login by Google from kos detail
	@FindBy(css = "#detailKostContainer form.form-login-google > button")
	private WebElement loginByGoogle;

	@FindBy(xpath = "//*[@data-testid='pencariKosButton']")
	private WebElement tenantButton;

	@FindBy(css = "#detailKostContainer .modal-login.modal.in .modal-body button.login-user-home")
	private WebElement tenantDetailKosButton;

	@FindBy(id = "email")
	private WebElement FbEmailTextbox;

	@FindBy(id = "identifierId")
	private WebElement gmailEmailTextbox;

	//login by FB from top header
	@FindBy(css = ".btn-facebook > .modal-action__content-text")
	private WebElement FbLoginTenantHeader;

	@FindBy(xpath = "//*[@data-testid='loginGoogleButton']")
	private WebElement gmailLoginTenantHeader;

	@FindBy(id = "pass")
	private WebElement FbPasswordTextbox;

	@FindBy(name = "password")
	private WebElement gmailPasswordTextbox;

	@FindBy(xpath = "//span[.='Berikutnya']")
	private WebElement gmailNextButton;

	@FindBy(css = "a[href='/register-pemilik?source=homepage']")
	private WebElement registerOwnerButton;

	@FindBy(xpath = "//*[@data-testid='registerButton']")
	private WebElement registerTenantButton;

	@FindBy(css = "a[data-path='btn_goToResetPassword']")
	private WebElement forgotPasswordButton;

	private By FBLoginButton = By.id("loginbutton");

	//login by FB from favorite page
    @FindBy(css = ".btn-facebook .facebook-logo")
    private WebElement FBLoginTenantFav;

    //login by Google from favorite page
    @FindBy(css = ".btn-google .google-logo")
    private WebElement GoogleLoginTenantFav;

    @FindBy(className = "user-profil-img")
    private WebElement userProfilePics;

    @FindBy(css = "c-loader wrapper__loader")
	private WebElement loadingScreen;

	@FindBy(xpath = "//button[@class='nav-login-button']")
	private WebElement masukButton;

	@FindBy(xpath = "//*[text()='Login Pemilik Kos']")
	private WebElement ownerLoginForm;

	@FindBy(css = ".bg-c-button__icon")
	private WebElement backButtonLoginOwner;

	@FindBy(css = ".bg-c-modal__body")
	private WebElement popUpLogin;

	@FindBy(xpath = "//*[@name='company_id']")
	private WebElement idTextbox;

	@FindBy(xpath = "//*[@name='email']")
	private WebElement emailTextbox;

	@FindBy(xpath = "//*[@name='password']")
	private WebElement passTextbox;

	@FindBy(xpath = "//button[@data-qaid='qa-submit-button']")
	private WebElement enterMasukButton;

	private By profileImage = RelativeLocator.with(By.tagName("img")).toRightOf(By.xpath("//*[contains(text(), 'Lainnya')]"));

	/**
	 * Click on Register Now button
	 * @throws InterruptedException
	 */
	public void clickOnRegisterOwnerButton() throws InterruptedException {
		selenium.clickOn(kosOwnerButton);
		selenium.clickOn(registerOwnerButton);
	}
	/**
	 * Login as Owner user to application
	 * 
	 * @param phone    phone
	 * @param password password
	 * @throws InterruptedException
	 */
	public void loginAsOwnerToApplication(String phone, String password) throws InterruptedException {
		try
		{
			selenium.waitInCaseElementVisible(kosOwnerButton, 5);
			selenium.clickOn(kosOwnerButton);
		} catch (org.openqa.selenium.TimeoutException ignored){

		}
		selenium.enterText(phoneTextbox, phone, true);
		selenium.enterText(passwordTextbox, password, true);
		selenium.clickOn(enterButton);
		selenium.waitForJavascriptToLoad();
		selenium.waitTillUrlContains("owner", 60);
		selenium.hardWait(2);
	}
	/**
	 * Login owner via phone number invalid password
	 * @param number phone
	 * @param password password
	 * @throws InterruptedException
	 */
	public void loginOwnerInvalidPassword(String number, String password) throws InterruptedException {
		try
		{
			selenium.waitInCaseElementVisible(kosOwnerButton, 5);
			selenium.clickOn(kosOwnerButton);
		} catch (org.openqa.selenium.TimeoutException ignored){

		}
		selenium.enterText(phoneTextbox, number, true);
		selenium.enterText(passwordTextbox, password, true);
		selenium.clickOn(enterButton);
	}

	/**
	 * Verify element present
	 * @throws InterruptedException
	 */
	public boolean checkLoginPopUp(){
		return selenium.waitInCaseElementVisible(loginByFB, 2)!=null
				&& selenium.waitInCaseElementVisible(loginByGoogle, 2)!=null;
	}

	/**
	 * Verify element present
	 */
	public boolean checkLoginPopUpFromFavoritePage(){
		return selenium.waitInCaseElementVisible(FBLoginTenantFav, 2)!=null
				&& selenium.waitInCaseElementVisible(GoogleLoginTenantFav, 2)!=null;
	}

	/**
	 * Login Tenant by Facebook
	 * @throws InterruptedException
	 */
	public void loginAsTenantByFacebook(String email, String password) throws InterruptedException
	{
		selenium.clickOn(tenantButton);
		selenium.clickOn(FbLoginTenantHeader);

		if (selenium.waitInCaseElementVisible(FbEmailTextbox, 3) != null) {
			selenium.enterText(FbEmailTextbox, email, false);
			selenium.enterText(FbPasswordTextbox, password, false);
			selenium.clickOn(FBLoginButton);
			selenium.waitTillElementIsNotVisible(FBLoginButton, 30);
		}

		if (selenium.waitInCaseElementVisible(previouslySignedInPopUpFB, 3) != null) {
			selenium.clickOn(okButtonFB);
		}
	}

	/**
	 * Login Tenant by Gmail
	 * @throws InterruptedException
	 */
	public void loginAsTenantByGmail(String email, String password) throws InterruptedException {
		selenium.clickOn(tenantButton);
		selenium.clickOn(gmailLoginTenantHeader);

		if (selenium.waitInCaseElementVisible(gmailEmailTextbox, 3) != null) {
			selenium.enterText(gmailEmailTextbox, email, false);
			selenium.clickOn(gmailNextButton);
			selenium.enterText(gmailPasswordTextbox, password, false);
			selenium.clickOn(gmailNextButton);
		}
	}


		/**
         * Login as Tenant user to application
         * @param number    phone
         * @param password password
         * @throws InterruptedException
         */
    public void loginAsTenantToApplication(String number, String password) throws InterruptedException {
        selenium.javascriptClickOn(tenantButton);
        selenium.waitInCaseElementVisible(phoneTextbox,10);
        selenium.enterText(phoneTextbox, number, false);
        selenium.enterText(passwordTextbox, password, false);
        selenium.pageScrollInView(enterTenantButton);
        selenium.clickOn(enterTenantButton);
		selenium.fluentWaitElementToBeClickable(profileImage, 60, 3);
		selenium.hardWait(5);
		selenium.waitForJavascriptToLoad();
    }

	/**
	 * Login as Tenant user to application
	 * @param number    phone
	 * @param password password
	 * @throws InterruptedException
	 */
	public void loginAsTenantInvalid(String number, String password) throws InterruptedException {
		selenium.javascriptClickOn(tenantButton);
		selenium.waitInCaseElementVisible(phoneTextbox,10);
		selenium.enterText(phoneTextbox, number, false);
		selenium.enterText(passwordTextbox, password, false);
		selenium.pageScrollInView(enterTenantButton);
		selenium.clickOn(enterTenantButton);
		selenium.hardWait(5);
		selenium.waitForJavascriptToLoad();
	}

	/**
	 * Click forgot password button
	 * @throws InterruptedException
	 */
	public void clickOnForgotPasswordButton() throws InterruptedException {
		selenium.clickOn(kosOwnerButton);
		selenium.clickOn(forgotPasswordButton);
	}

	/**
	 * Click tenant forgot password button
	 * @throws InterruptedException
	 */

	public void clickOnTenantForgotPasswordButton() throws InterruptedException {
		selenium.waitTillElementIsClickable(tenantButton);
		selenium.clickOn(tenantButton);
		selenium.clickOn(forgotPasswordButton);
	}

	/**
	 * Click on Register Now Button as Tenant
	 * @throws InterruptedException
	 */
	public void clickOnRegisterTenantButton() throws InterruptedException {
		selenium.clickOn(tenantButton);
		selenium.pageScrollInView(registerTenantButton);
		selenium.clickOn(registerTenantButton);
	}

	/**
	 * Login tenant via phone number invalid password
	 * @param number phone
	 * @param password password
	 * @throws InterruptedException
	 */
	public void loginTenantInvalidPassword(String number, String password) throws InterruptedException {
		selenium.clickOn(tenantButton);
		selenium.enterText(phoneTextbox, number, false);
		selenium.enterText(passwordTextbox, password, false);
		selenium.clickOn(passwordTextbox);
		selenium.clearTextField(passwordTextbox);
	}

	/**
	 * Get login error messages text
	 * @return string
	 */
	public String getLoginErrorMessagesText(String error){
		selenium.waitInCaseElementVisible(By.xpath("//p[contains(., '"+error+"')]"), 10);
		return selenium.getText(By.xpath("//p[contains(., '"+error+"')]"));
	}

	/**
	 * Get Login Title Pop Up Text
	 * @param text text
	 * @return string
	 */
	public String getLoginTitlePopUpText(String text){
		return selenium.getText(By.xpath("//p[contains(., '"+text+"')]"));
	}

	/**
	 * Get Login Subtitle Pop Up Text
	 * @param text text
	 * @return string
	 */
	public String getLoginSubtitleText(String text){
		return selenium.getText(By.xpath("//p[contains(., '"+text+"')]"));
	}

	/**
	 * Click Close on Pop up Login
	 * @throws InterruptedException
	 */
	public void clickCloseOnPopUpLogin() throws InterruptedException {
		selenium.clickOn(By.className("bg-c-modal__action-closable"));
	}

	/**
	 * Is Pop up title text appeared?
	 * @param text text
	 * @return true or false
	 */
	public Boolean isPopupTitleTextAppeared(String text){
		return selenium.waitInCaseElementVisible(By.xpath("//p[contains(., '"+text+"')]"), 3) != null;
	}

	/**
	 * Is Pop up subtitle text appeared?
	 * @param text text
	 * @return true or false
	 */
	public Boolean isPopupSubtitleTextAppeared(String text){
		return selenium.waitInCaseElementVisible(By.xpath("//p[contains(., '"+text+"')]"), 3) != null;
	}

	/**
	 * Click  Pop up Login Owner
	 * @throws InterruptedException
	 */

	public void popUpOwnerLogin() throws InterruptedException{
		selenium.clickOn(kosOwnerButton);
	}
	/**
	 * verify  pop up Login Owner
	 * @throws InterruptedException
	 */

	public void verifyOwnerLoginForm() throws InterruptedException{
		selenium.waitTillElementIsVisible(ownerLoginForm);
	}
	/**
	 * Click back Pop up Login Owner
	 * @throws InterruptedException
	 */
	public void clickBackOnPopUpLogin() throws InterruptedException{
		selenium.clickOn(backButtonLoginOwner);
	}
	public boolean isPopUpNotShowing(){
		return selenium.waitInCaseElementVisible(popUpLogin, 3) != null;
	}

	/**
	 * Enter ID , Email and Password then click on Login Button Bigflip
	 * @param ID enter ID
	 * @param passWord enter Password
	 * @param email enter Email
	 * @throws InterruptedException
	 */
	public void enterCredentialsAndClickOnLoginButtonBigflip(String ID, String email, String passWord) throws InterruptedException {
		selenium.enterText(idTextbox, ID, false);
		selenium.enterText(emailTextbox, email, false);
		selenium.enterText(passTextbox, passWord, false);
		selenium.clickOn(enterMasukButton);
		selenium.hardWait(5);
	}
}
