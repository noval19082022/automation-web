package pageobjects.mamikos.termsandcons;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class TermsAndConditionsPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public TermsAndConditionsPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.WEBDRIVER_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    /**
     * Click Terms And Conditions Type on Sidebar
     * @param terms Terms And Conditions Type e.g 'Owner'
     * @throws InterruptedException
     */
    public void clickOnTermsAndConditionsSidebar(String terms) throws InterruptedException{
        selenium.hardWait(1);
        selenium.clickOn(By.xpath("//p[contains(text(),'" +terms+"')]"));
    }

    /**
     *
     * @param terms Terms And Conditions Menu e.g 'Can Book'
     * @return childPath Return Terms And Conditions Menu on Sidebar
     */
    public Boolean verifyTermsAndConditionsChilds(String terms) {
        return selenium.waitInCaseElementVisible(By.xpath("//a[contains(text(), '" + terms + "')]"), 3) != null;
    }
}
