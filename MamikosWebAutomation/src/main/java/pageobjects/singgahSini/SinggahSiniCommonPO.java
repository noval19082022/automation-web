package pageobjects.singgahSini;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class SinggahSiniCommonPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public SinggahSiniCommonPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//button[contains(text(), 'Nanti Saja')]")
    private WebElement closePopUpNotifButton;


    /**
     * Check if Notif Pop Up appear and click close Button
     * @throws InterruptedException
     */
    public void closeMamikosNotifPopUp()throws InterruptedException{
        while (isNotifPopUpAppear()){
            if(selenium.waitInCaseElementClickable(closePopUpNotifButton, 3) != null){
                selenium.clickOn(closePopUpNotifButton);
            } else {
                break;
            }
        }
    }


    /**
     * is Notif Popup Appear?
     * @return true if appear
     */
    public boolean isNotifPopUpAppear()
    {
        return selenium.waitInCaseElementClickable(closePopUpNotifButton, 5) != null;
    }


}
