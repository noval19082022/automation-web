package pageobjects.singgahSini;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class LandingPagePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public LandingPagePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(className = "landing-singgahsini-warp")
    private WebElement benefitsSection;

    @FindBy(xpath = "//p[@class='landing-benefit__title bg-c-text bg-c-text--heading-1 ']")
    private WebElement advantagesTitle;

    @FindBy(xpath = "//section[@id='landingSinggahsiniAdvantages']//p")
    private WebElement advantagesDescription;

    @FindBy(xpath = "//section[@id='landingSinggahsiniAdvantages']//div[@class='advantage-slider']")
    private WebElement advantagesSlider;

    @FindBy(xpath = "//img[@class='landing-benefit__item-image']")
    private List<WebElement> advantageImages;

    @FindBy(xpath = "//div[@class='landing-benefit__item']//p[@class='bg-c-text bg-c-text--heading-6 ']")
    private List<WebElement> advantageSections;

    @FindBy(xpath = "//div[@class='landing-benefit__item']//p[@class='landing-benefit__item-subtitle bg-c-text bg-c-text--body-2 ']")
    private List<WebElement> advantageDetails;

    @FindBy(id = "landingSinggahsiniShortIntro")
    private WebElement aboutUsSection;

    @FindBy(id = "landingSinggahsiniTestimony")
    private WebElement testimonialSection;

    @FindBy(xpath = "(//div[@class='section__container-left']//p)[2]")
    private WebElement testimonialTitle;

    @FindBys(@FindBy(xpath = "//div[@class='section__content-wrapper']//div//p[@class='bg-c-text bg-c-text--heading-6 ']"))
    private List<WebElement> testimonialOwnerName;

    @FindBys(@FindBy(xpath = "//div[@class='section__content-wrapper']//div//p[@class='bg-c-text bg-c-text--label-2 bg-c-text--italic ']"))
    private List<WebElement> testimonialPropertyName;

    @FindBys(@FindBy(xpath = "//div[@class='section__content-wrapper']//p[@class='bg-c-text bg-c-text--body-landing ']"))
    private List<WebElement> testimonialText;

    @FindBy(xpath = "(//div[@class='section__navigation'])[2]")
    private WebElement testimonialSlider;

    @FindBy(id = "landingSinggahsiniShortIntro")
    private WebElement introductionSection;

    @FindBy(xpath = "//*[@id='landingSinggahsiniShortIntro']//*[@class='ss-title bg-c-text bg-c-text--heading-1 ']")
    private WebElement introductionTitle;

    @FindBy(xpath = "//*[@id='landingSinggahsiniShortIntro']//*[@class='ss-copy bg-c-text bg-c-text--body-landing ']")
    private WebElement introductionContent;

    @FindBy(xpath = "(//section[@id='landingSinggahsiniIntro']/div/div)[1]")
    private WebElement introductionImage;

    @FindBy(xpath = "(//section[@id='landingSinggahsiniIntro']/div/div)[2]")
    private WebElement introductionDescription;

    @FindBy(xpath = "//img[@alt='Gambar kamar yang dikelola oleh Singgahsini']")
    private List<WebElement> tentangKamiImage;

    @FindBy(xpath = "//p[@class='ss-title bg-c-text bg-c-text--heading-1 ']")
    private WebElement tentangKamiTilte;

    @FindBy(xpath = "//p[@class='ss-copy bg-c-text bg-c-text--body-2 ']")
    private WebElement tentangKamiSubTitle;

    @FindBy(xpath = "(//section[@class='section'])[4]")
    private WebElement howToJoinSection;

    @FindBy(xpath = "(//div[@class='section__content']//p)[1]")
    private WebElement howToJoinTitle;

    @FindBy(xpath = "(//div[@class='section__content']//p)[2]")
    private WebElement howToJoinDescription;

    @FindBy(xpath = "(//img[@class='section__cover-image'])")
    private WebElement howToJoinImage;

    @FindBys(@FindBy(xpath = "//div[@class='container join-step']//p"))
    private List <WebElement> descriptionOfStepsHowToJoin;

    @FindBy(id = "landingSinggahsiniGallery")
    private WebElement gallerySection;

    @FindBy(xpath = "//div[@id='landingSinggahsiniGallery']//h2")
    private WebElement galleryTitle;

    @FindBy(xpath = "//div[@id='landingSinggahsiniGallery']//h3")
    private WebElement gallerySubtitle;

    @FindBys(@FindBy(xpath = "//div[@id='landingSinggahsiniGallery']//div[@class='col-lg-4 landing-singgahsini-gallery__room-card']"))
    private List<WebElement> galleryRoomCard;

    @FindBys(@FindBy(xpath = "//div[@id='landingSinggahsiniGallery']//div[@class='landing-singgahsini-gallery__room-cover']"))
    private List<WebElement> galleryPropertyImage;

    @FindBys(@FindBy(xpath = "//div[@id='landingSinggahsiniGallery']//p[@class='landing-singgahsini-gallery__room-title']"))
    private List<WebElement> galleryPropertyName;

    @FindBys(@FindBy(xpath = "//div[@id='landingSinggahsiniGallery']//p[@class='landing-singgahsini-gallery__room-area']"))
    private List<WebElement> galleryPropertyArea;

    @FindBys(@FindBy(xpath = "//div[@id='landingSinggahsiniGallery']//p[@class='landing-singgahsini-gallery__room-price']"))
    private List<WebElement> galleryPropertyPrice;

    @FindBy(xpath = "//a[@class='btn btn-singgahsini-gallery']")
    private WebElement galleryViewMoreButton;

    @FindBy(id = "landingSinggahsiniFaq")
    private WebElement informationCenterSection;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--heading-1 ']")
    private WebElement faqTitle;

    @FindBys(@FindBy(xpath = "//section[@id='landingSinggahsiniFaq']//button"))
    private List<WebElement> questionsLabel;

    @FindBys(@FindBy(xpath = "//section[@id='landingSinggahsiniFaq']//button/following-sibling::div/div"))
    private List<WebElement> answerLabel;

    @FindBys(@FindBy(xpath = "//section[@id='landingSinggahsiniFaq']//button/following-sibling::div/div/p"))
    private List<WebElement> answerParagraph;

    @FindBys(@FindBy(xpath = "//div//ol//li"))
    private List<WebElement> answerList;

    @FindBy(xpath = "//button[@class='join-cta']")
    private WebElement joinNowButton;

    @FindBy(xpath = "//section[@id='landingSinggahsiniFooter']")
    private WebElement footerSection;

    @FindBys(@FindBy(xpath = "//div/div[@class='landing-singgahsini-social__list-wrapper']"))
    private List<WebElement> socialMedia;

    @FindBy(xpath = "//div[@class='section__link-texts']//p[@class='bg-c-text bg-c-text--body-2 ']")
    private List<WebElement> footerBtn;

    /**
     * Click on hyperlink
     * @throws InterruptedException
     */
    public void clickOnHyperlink(String hyperLink) throws InterruptedException {
        selenium.clickOn(By.xpath("//a[@class='landing-singgahsini-menu__link'][contains(text(),'" + hyperLink + "')]"));
    }

    /**
     * Check section about us present on landing page
     * @return aboutUsSection status
     */
    public boolean AboutUsSectionIsPresent() {
        return selenium.isElementDisplayed(aboutUsSection);
    }

    /**
     * Verify content of introduction section  is present
     * @return aboutUsSection status
     */
    public String howToJoinSectionContentIsPresent(String section) {
        if (section.equalsIgnoreCase("Title")) {
            return selenium.getText(howToJoinTitle);
        } else if (section.equalsIgnoreCase("Description")) {
            return selenium.getText(howToJoinDescription);
        }
        return section;
    }

    /**
     * Get steps description how to join singgahsini
     * @return steps description
     */
    public String getStepDescriptionHowToJoin(int index) {
        return selenium.getText(descriptionOfStepsHowToJoin.get(index));
    }

    /**
     * Check section how to join present on landing page
     * @return aboutUsSection status
     */
    public boolean howToJoinSectionIsPresent() {
        return selenium.isElementDisplayed(howToJoinSection);
    }

    /**
     * Check section gallery present on landing page
     * @return aboutUsSection status
     */
    public boolean gallerySectionIsPresent() {
        return selenium.isElementDisplayed(gallerySection);
    }

    /**
     * Verify system display join now button
     * @return aboutUsSection status
     */
    public boolean joinNiwButtonIsPresent() {
        return selenium.isElementDisplayed(joinNowButton);
    }

    /**
     * Scroll to footer
     * @throws InterruptedException
     */
    public void scrollToFooter(String footerMenu) {
        selenium.pageScrollInView(footerSection);
    }

    /**
     * Click on hyperlink on footer
     * @throws InterruptedException
     */
    public void clickHyperlinkOnFooter(String footerMenu) throws InterruptedException {
        switch (footerMenu){
            case "Manfaat":
                selenium.clickOn(footerBtn.get(0));
                break;
            case "Tentang Kami":
                selenium.clickOn(footerBtn.get(1));
                break;
            case "Tanya Jawab":
                selenium.clickOn(footerBtn.get(2));
                break;
            case "Facebook":
                selenium.clickOn(footerBtn.get(6));
                break;
            case "Twitter":
                selenium.clickOn(footerBtn.get(7));
                break;
            case "Instagram":
                selenium.clickOn(footerBtn.get(8));
                break;
            case "Youtube":
                selenium.clickOn(footerBtn.get(9));
                break;
        }
    }
    /**
     * Verify benefit section present on landing page
     * @return aboutUsSection status
     */
    public boolean benefitsSectionIsPresent() {
        return selenium.isElementDisplayed(benefitsSection);
    }

    /**
     * Verify information center section present on landing page
     * @return aboutUsSection status
     */
    public boolean informationCenterSectionIsPresent() {
        return selenium.isElementDisplayed(informationCenterSection);
    }

    /**
     * Go to faq section
     * @throws InterruptedException
     */
    public void scrollToFaqSection() {
        selenium.pageScrollInView(informationCenterSection);
    }

    /**
     * Go to how to join section
     * @throws InterruptedException
     */
    public void scrollToHowToJoinSection() {
        selenium.pageScrollInView(howToJoinSection);
    }

    /**
     * Click on social media
     * @throws InterruptedException
     */
    public void clickOnSocialMedia(int index) throws InterruptedException {
        selenium.pageScrollInView(socialMedia.get(index));
        selenium.hardWait(2);
        selenium.clickOn(socialMedia.get(index));
    }

    /**
     * Get social media url
     * @throws InterruptedException
     */
    public String getSocialMediaUrlOnNewTab() throws InterruptedException {
        selenium.hardWait(2);
        selenium.switchToWindow(2);
        selenium.waitForJavascriptToLoad();
        String url = selenium.getURL();
        selenium.hardWait(2);
        selenium.closeTabWindowBrowser();
        System.out.println(url);
        return url;
    }

    /**
     * Get advantages title
     * @return benefitsTitle
     */
    public String getAdvantagesTitle() {
        return selenium.getText(advantagesTitle);
    }

    /**
     * Get advantages description
     * @return benefitsDescription
     */
    public String getAdvantagesDescription() {
        return selenium.getText(advantagesDescription);
    }

    /**
     * Verify content of benefits section is present
     */
    public boolean advantagesSliderIsPresent() {
        return selenium.isElementDisplayed(advantagesSlider);
    }

    /**
     * Get advantages
     */
    public String getAdvantages(int i) {
        selenium.pageScrollInView(advantageSections.get(i));
        return selenium.getText(advantageSections.get(i));

    }

    /**
     * Scroll to introduction section
     */
    public void scrollToOwnerShouldJoinSinggahsiniSection() {
        selenium.pageScrollInView(introductionSection);
    }

    /**
     * Verify introduction section  is present
     */
    public boolean introductionSectionIsPresent() {
        return selenium.isElementDisplayed(introductionSection);
    }

    /**
     * Verify content of introduction section  is present
     */
    public boolean introductionSectionContentIsPresent() {
        return (selenium.isElementDisplayed(introductionTitle) &&
                selenium.isElementDisplayed(introductionContent) &&
                selenium.isElementDisplayed(tentangKamiImage.get(0)) &&
                selenium.isElementDisplayed(tentangKamiImage.get(1)) &&
                introductionSectionIsPresent());
    }

    /**
     * Verify testimonial section  is present
     */
    public void scrollToOwnerTestimonialSection() {
        selenium.pageScrollInView(testimonialSection);
    }

    /**
     * Verify content of testimonials section  is present
     */
    public boolean testimonialContentIsPresent(int i) {
        return (selenium.isElementDisplayed(testimonialTitle) &&
                selenium.isElementDisplayed(testimonialOwnerName.get(i)) &&
                selenium.isElementDisplayed(testimonialPropertyName.get(i)) &&
                selenium.isElementDisplayed(testimonialText.get(i))&&
                selenium.isElementDisplayed(testimonialSlider) &&
                selenium.isElementDisplayed(testimonialSection));
    }

    /**
     * Verify content of gallery section is present
     */
    public boolean galleryContentIsPresent() {
        return (selenium.isElementDisplayed(galleryTitle) &&
                selenium.isElementDisplayed(gallerySubtitle) &&
                selenium.isElementDisplayed(galleryRoomCard.get(0)) &&
                selenium.isElementDisplayed(galleryPropertyImage.get(0)) &&
                selenium.isElementDisplayed(galleryPropertyName.get(0)) &&
                selenium.isElementDisplayed(galleryPropertyArea.get(0)) &&
                selenium.isElementDisplayed(galleryPropertyPrice.get(0)) &&
                selenium.isElementDisplayed(galleryViewMoreButton) &&
                gallerySectionIsPresent());
    }

    /**
     * get title of faq section
     * @throws InterruptedException
     */
    public String getFAQTitle() {
        return selenium.getText(faqTitle);
    }

    /**
     * Get questions from faq section
     * @throws InterruptedException
     */
    public String getQuestions(int index) {
        return selenium.getText(questionsLabel.get(index));
    }

    public void expandFAQ(int index) throws InterruptedException {
        if(index > 2){
            selenium.pageScrollInView(questionsLabel.get(index-2));
        }
        selenium.hardWait(1);
        selenium.clickOn(questionsLabel.get(index));
    }

    /**
     * Get answer from faq section
     * @throws InterruptedException
     */
    public String getAnswer(int index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(answerLabel.get(index)).replace("\n", "");
    }
    /**
     * Get 2nd answer from faq section
     * @throws InterruptedException
     */
    public String getSecondAnswer(int index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(answerParagraph.get(index)).replace("\n", "");
    }
    /**
     * Get 3rd answer from faq section
     * @throws InterruptedException
     */
    public String getThirdAnswer(int index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(answerParagraph.get(index)).replace("\n", "");
    }
    /**
     * Get 4th answer from faq section
     * @throws InterruptedException
     */
    public String getFourthAnswer(int index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(answerList.get(index)).replace("\n", "");
    }
    /**
     * Get 5th answer from faq section
     * @throws InterruptedException
     */
    public String getFifthAnswer(int index) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(answerList.get(index)).replace("\n", "");
    }

    /**
     * Click view more button on gallery section
     * @throws InterruptedException
     */
    public void clickViewMorButtonOnGallerySection() throws InterruptedException {
        selenium.clickOn(galleryViewMoreButton);
    }

    /**
     * Get img src in Manfaat section
     * @return string src
     */
    public String getImgSrc(int index) {
        return selenium.getElementAttributeValue(advantageImages.get(index),"src");
    }

    /**
     * Get img src in Join section
     * @return string src
     */
    public String getJoinImgSrc() {
        return selenium.getElementAttributeValue(howToJoinImage,"src");
    }

    /**
     * get sub advantages in Manfaat section
     * @param i integer
     * @return String sub advantages
     */
    public String getSubAdvantages(int i) {
        selenium.pageScrollInView(advantageDetails.get(i));
        return selenium.getText(advantageDetails.get(i));
    }
}
