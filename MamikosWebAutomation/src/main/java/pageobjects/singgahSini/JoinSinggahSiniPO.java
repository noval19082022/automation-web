package pageobjects.singgahSini;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class JoinSinggahSiniPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public JoinSinggahSiniPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//button[contains(text(), 'Daftar Sekarang')]")
    private WebElement joinNowButton;

    @FindBy(name = "name")
    private WebElement fullNameEditText;

    @FindBy(name = "phone")
    private WebElement phoneNumberEditText;

    @FindBy(name = "kosName")
    private WebElement kostNameEditText;

    @FindBy(name = "city")
    private WebElement cityEditText;

    @FindBy(className = "bg-c-dropdown__menu-item")
    private List<WebElement> cityList;

    @FindBy(name = "kecamatan")
    private WebElement subdistrictDropdown;

    @FindBy(xpath = "//select[@name='kecamatan']/following-sibling::div//input")
    private WebElement subdistrictEditText;

    @FindBy(name = "village")
    private WebElement villageDropdown;

    @FindBy(xpath = "//select[@name='village']/following-sibling::div//input")
    private WebElement villageEditText;

    @FindBy(name = "fullAddress")
    private WebElement addressEditText;

    @FindBy(xpath = "//input[@name='name']/following::div[1]")
    private WebElement fullNameErrorText;

    @FindBy(xpath = "//input[@name='phone']/following::div[1]")
    private WebElement phoneErrorText;

    @FindBy(xpath = "//input[@name='kosName']/following::div[1]")
    private WebElement kostNameErrorText;

    @FindBy(xpath = "//div[@class='bg-c-dropdown']/following::div[1]")
    private WebElement cityErrorText;

    @FindBy(xpath = "//input[@placeholder='Cari']")
    private WebElement citySearchForm;

    @FindBy(xpath = "//input[@placeholder='Cari']/following::span")
    private WebElement citySearchErrorText;

    @FindBy(xpath = "//div[@data-vv-id='5']/following::div[1]")
    private WebElement fullAddressErrorText;

    @FindBy(className = "register-btn")
    private WebElement registerButton;

    @FindAll({
            @FindBy(className = "bg-c-link--high"),
            @FindBy(className = "bg-c-icon--md")
    })
    private WebElement backButton;

    @FindBy(className = "title")
    private WebElement pageTitle;

    @FindBy(className = "subtitle")
    private WebElement pageDescription;

    @FindBy(xpath = "//*[@class='landing-banner__title bg-c-text bg-c-text--heading-1 ']")
    private WebElement homePageTitle;

    @FindBy(className = "bg-c-modal__body-title")
    private WebElement modalTitle;

    @FindBy(className = "singgahsini-form-container__btn")
    private WebElement nextModalButton;

    @FindBy(className = "singgahsini-modal__btn")
    private WebElement okeButton;

    @FindBy(className = "singgahsini-form-container__btn-exit")
    private WebElement exitModalButton;



    /**
     * Click on Join Now Button
     *
     * @throws InterruptedException
     */
    public void clickOnJoinNowButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(joinNowButton);
        selenium.javascriptClickOn(joinNowButton);
    }

    /**
     * Fill out Full Name
     * @param name Owner Full Name
     */
    public void insertName(String name){
        selenium.waitTillElementIsVisible(fullNameEditText);
        selenium.enterText(fullNameEditText, name, false);
    }

    /**
     * Fill out Phone Number
     * @param phone Owner Phone Number
     */
    public void insertPhoneNumber(String phone) {
        selenium.enterText(phoneNumberEditText, phone, false);
    }

    /**
     * Fill out Kost Name
     * @param kostName Kost Name
     */
    public void insertKostName(String kostName) {
        selenium.enterText(kostNameEditText, kostName, false);
    }

    /**
     * Select subdistrict
     * @param subdistrict
     */
    public void selectSubdistrict(String subdistrict) throws InterruptedException{
        selenium.pageScrollInView(subdistrictDropdown);
        selenium.clickOn(subdistrictDropdown);
        selenium.waitTillElementIsVisible(subdistrictEditText);
        selenium.enterText(subdistrictEditText, subdistrict, true);
        selenium.clickOn(By.xpath("//ul/child::*[2][contains(., '" +subdistrict+ "')]"));
    }

    /**
     * Select selectVillage
     * @param village
     */
    public void selectVillage(String village) throws InterruptedException{
        selenium.pageScrollInView(villageDropdown);
        selenium.clickOn(villageDropdown);
        selenium.waitTillElementIsVisible(villageEditText);
        selenium.enterText(villageEditText, village, true);
        selenium.clickOn(By.xpath("//ul/child::*[2][contains(., '" +village+ "')]"));
    }

    /**
     * Fill out City on Search Form
     * @param city City
     */
    public void insertCity(String city)throws InterruptedException {
        selenium.pageScrollInView(cityEditText);
        selenium.waitTillElementIsVisible(cityEditText);
        selenium.clickOn(cityEditText);
        selenium.hardWait(5);
        selenium.enterText(citySearchForm, city, false);
        WebElement clickCity = driver.findElement(By.xpath("//ul/child::*[2][contains(., '" + city + "')]"));
        selenium.clickOn(clickCity);
    }

    /**
     * Fill out City on Search Form
     * @param city City
     */
    public void enterWrongCity(String city)throws InterruptedException {
        selenium.pageScrollInView(cityEditText);
        selenium.waitTillElementIsVisible(cityEditText);
        selenium.clickOn(cityEditText);
        selenium.hardWait(5);
        selenium.enterText(citySearchForm, city, false);
    }

    /**
     * Fill out Address
     * @param address Address
     */
    public void insertAddressNotes(String address) {
        selenium.enterText(addressEditText, address, false);
    }

    /**
     * Get error message on Full Name form
     *
     * @return error message
     */
    public String getFullNameErrorMessage() {
        return selenium.getText(fullNameErrorText);
    }

    /**
     * Get error message on Phone Number form
     *
     * @return error message
     */
    public String getPhoneErrorMessage() {
        return selenium.getText(phoneErrorText);
    }

    /**
     * Get error message on City form
     *
     * @return error message
     */

    public String getCityErrorMessage() {
        return selenium.getText(cityErrorText);
    }

    /**
     * Get error message on Kost Name form
     *
     * @return error message
     */
    public String getKostNameErrorMessage() {
        return selenium.getText(kostNameErrorText);
    }

    /**
     * Get error message on Kost Address form
     *
     * @return error message
     */
    public String getAddressErrorMessage() {
        return selenium.getText(fullAddressErrorText);
    }

    /**
     * Get page Title
     * @return page Title
     */
    public String getPageTitle() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(pageTitle);
        return selenium.getText(pageTitle);
    }


    /**
     * Get page description
     * @return page Title
     */
    public String getDescription() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(pageDescription);
    }

    /**
     * Get Homepage Title
     * @return Homepage Title
     */
    public String getHomePageTitle() {
        return selenium.getText(homePageTitle);
    }

    /**
     * Click on Button Kembali
     * @throws InterruptedException
     */
    public void clickOnBackButton() throws InterruptedException {
            selenium.waitTillElementIsVisible(backButton);
            selenium.clickOn(backButton);
    }

    /**
     * Click on Register Button
     * @throws InterruptedException
     */
    public void clickOnRegisterButton() throws InterruptedException {
        selenium.clickOn(registerButton);
    }

    /**
     * Get Modal Title
     * @return modal Title
     */
    public String getModalTitle() {
        return selenium.getText(modalTitle);
    }

    /**
     * Click on Button Keluar
     * @throws InterruptedException
     */
    public void clickOnExitButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(exitModalButton);
        selenium.clickOn(exitModalButton);
    }

    /**
     * Click on Button Lanjut Isi
     * @throws InterruptedException
     */
    public void clickOnNextButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(nextModalButton);
        selenium.clickOn(nextModalButton);
    }

    /**
     * Click on Button Oke
     * @throws InterruptedException
     */
    public void clckOnOkeButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(okeButton);
        selenium.clickOn(okeButton);
    }

    /**
     * Get City Search Error Message
     * @return error message
     */
    public String getCitySearchErrorMessage() {
        return selenium.getText(citySearchErrorText);
    }
}
