package pageobjects.mamipay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class CommonPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public CommonPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//*[@class='bg-c-icon bg-c-icon--md']")
    private WebElement closePopUpButton;

    @FindBy(xpath = "//div[@class='callout callout-success']")
    private WebElement successAlert;

    /**
     * Click on close popup button
     * @throws InterruptedException
     */
    public void ClickOnClosePopupButton() throws InterruptedException {
        selenium.clickOn(closePopUpButton);
        selenium.hardWait(2);
        if (selenium.isElementDisplayed(closePopUpButton)){
            selenium.clickOn(closePopUpButton);
        }
    }

    /**
     * Get alert message
     * @return Message alert
     */
    public String getMessageAlert() {
        return selenium.getText(successAlert);
    }

    /**
     * get URL
     * @return string URL
     * @throws InterruptedException
     */
    public String getURL() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getURL();
    }
}
