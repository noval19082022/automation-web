package pageobjects.mamipay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.List;

public class PartnerVoucherPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private JavaHelpers java = new JavaHelpers();

    public PartnerVoucherPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(name = "voucher_code")
    private WebElement voucherEditText;

    @FindBy(xpath = "//button[contains(text(),'Update')]")
    private WebElement updateVoucherButton;

    @FindBy(xpath = "//*[@class='callout callout-danger']/descendant::li")
    private WebElement errorMessageText;

    @FindBy(name = "public_campaign[title]")
    private WebElement campaignTitleEditText;

    @FindBy(name = "public_campaign[is_published]")
    private WebElement publishCheckBox;

    @FindBy(xpath = "//*[@class='callout callout-success']")
    private WebElement successUpdateVoucherMessage;

    @FindBy(name = "limit")
    private WebElement totalQuotaTextBox;

    @FindBy(xpath = "//*[@class='callout callout-danger']//li")
    private List<WebElement> fieldRequiredValidationMessage;

    @FindBy(xpath = "//*[.='Bulk Add Vouchers Partner']")
    private WebElement addVoucherPartnerButton;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    private WebElement addVoucherButton;


    /**
     * Enter voucher code
     * @param voucherCode
     */
    public void enterVoucherCode(String voucherCode) {
        selenium.enterText(voucherEditText, voucherCode, true);
    }

    /**
     * Click on update voucher button
     * @throws InterruptedException
     */
    public void clickOnUpdateVoucherButton() throws InterruptedException {
        selenium.clickOn(updateVoucherButton);
    }

    /**
     * Get message error
     * @return message error field is required
     */
    public String getMessageError() {
        return selenium.getText(errorMessageText);
    }

    /**
     * Enter Campaign Title
     * @param campaignTitle
     */
    public void enterCampaignTitle(String campaignTitle) {
        selenium.enterText(campaignTitleEditText, campaignTitle, true);
    }

    /**
     * Check publish is checked or not
     * @return
     */
    public boolean checkPublishIsCheckedOrNot() throws InterruptedException{
        selenium.pageScrollInView(publishCheckBox);
        return publishCheckBox.isSelected();
    }

    /**
     * Click on Publish Check Box
     * @throws InterruptedException
     */
    public void clickOnPublishCheckBox() throws InterruptedException {
        selenium.clickOn(publishCheckBox);
    }

    /**
     * Verify message success
     * @return boolean element message success
     */
    public boolean messageSuccessUpdateIsDisplayed() {
        return selenium.isElementDisplayed(successUpdateVoucherMessage);
    }

    /**
     * Enter Total Quota
     * @param totalQuota
     */
    public void enterTotalQuota(String totalQuota) {
        selenium.enterText(totalQuotaTextBox, totalQuota, true);
    }

    /**
     * get message validation field required
     * @return string message
     */
    public String getMessageValidationFieldRequired(Integer index) {
        return selenium.getText(fieldRequiredValidationMessage.get(index));
    }

    /**
     * click on add voucher partner button
     * @throws InterruptedException
     */
    public void clicksOnAddVoucherPartnerButton() throws InterruptedException {
        selenium.clickOn(addVoucherPartnerButton);
    }

    /**
     * click on add voucher button
     * @throws InterruptedException
     */
    public void clicksOnAddVoucherButton() throws InterruptedException {
        selenium.clickOn(addVoucherButton);
    }
}