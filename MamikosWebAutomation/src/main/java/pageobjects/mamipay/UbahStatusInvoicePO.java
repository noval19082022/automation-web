package pageobjects.mamipay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class UbahStatusInvoicePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public UbahStatusInvoicePO (WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }


    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */
    @FindBy(xpath = "//*[@data-testid='change-status-paid-time']")
    private WebElement timerText;

    @FindBy(xpath = "//*[@data-testid='change-status-save']")
    private WebElement simpanButton;

    @FindBy(xpath = "//tr")
    private List<WebElement> row;

    /**
     * set paid time in ubah status invoice modal
     * @param time time between 00:00 - 23:59, no need to put ':'
     * @throws InterruptedException
     */
    public void setPaidTime(String time) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.enterText(timerText,time,true);
    }

    /**
     * click Simpan Button in Ubah Status Invoice Modal
     * @throws InterruptedException
     */
    public void clickSimpanButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(simpanButton);
    }
}
