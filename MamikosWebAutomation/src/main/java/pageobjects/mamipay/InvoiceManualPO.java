package pageobjects.mamipay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class InvoiceManualPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public InvoiceManualPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='invoice-manual-header']/p")
    private WebElement pageInvoiceTitleText;

    @FindBy(xpath = "//button[contains(text(),'Buat Invoice')]")
    private WebElement buatInvoiceButton;

    @FindBy(xpath = "//input[@id='invoice-type-rent']")
    private WebElement biayaSewaRadioButton;

    @FindBy(xpath = "//input[@id='invoice-type-additional']")
    private WebElement biayaTambahanRadioButton;

    @FindBy(xpath = "(//button[@class='bg-c-button bg-c-pagination__item bg-c-button--tertiary bg-c-button--sm'])")
    private List<WebElement> pagination;

    //---change invoice type---//
    @FindBy(xpath = "//h3[@class='bg-c-modal__body-title']")
    private WebElement popUpTitleConfirm;

    @FindBy(xpath = "//p[@class='bg-c-modal__body-description']")
    private WebElement popUpSubtitleConfirm;

    @FindBy(xpath = "//button[contains(., 'Batal')]")
    private WebElement batalBtn;

    @FindBy(xpath = "//button[contains(., 'Lanjutkan')]")
    private WebElement lanjutkanBtn;

    @FindBy(xpath = "(//p[contains(., 'Biaya Sewa')])[2]")
    private WebElement biayaSewaTable;

    @FindBy(xpath = "(//p[contains(., 'Biaya Tambahan')])[2]")
    private WebElement biayaTambahanTable;

    @FindBy(xpath = "//td[@class='empty-data']")
    private WebElement emptyStateBiayaTable;

    By popUpConfirm = By.xpath("//div[@class='bg-c-modal__inner']");
    //---end of change invoice type---//

    //---buat dan kirim---//
    @FindBy(xpath = "//*[@class='global-toast bg-c-toast bg-c-toast--fixed']")
    private WebElement toastMessage;

    @FindBy(xpath = "//button[contains(., 'Buat dan Kirim')]")
    private List<WebElement> buatNKirimBtn;

    @FindBy(xpath = "//h3[contains(., 'Buat dan Kirim Invoice')]")
    private WebElement buatNKirimTitle;

    @FindBy(xpath = "//p[contains(., 'Mohon')]")
    private WebElement buatNKirimSubtitle;

    @FindBy(xpath = "//div[@class='mb-16']/child::*")
    private List<WebElement> buatNKirimDataPenyewa;

    @FindBy(xpath = "//p[contains(., 'Nomor Kamar')]/following-sibling::*")
    private WebElement buatNKirimNoKmr;

    @FindBy(xpath = "//p[contains(., 'Jenis Invoice')]/following-sibling::*")
    private WebElement buatNKirimJnsInv;

    @FindBy(xpath = "(//span/following-sibling::p)[3]")
    private WebElement tableTitle;

    @FindBy(xpath = "//button[@class='bg-c-modal__action-closable']")
    private WebElement closeBtn;

    @FindBy(xpath = "//button[contains(., 'Kembali')]")
    private WebElement kembaliBtn;

    @FindBy(xpath = "//div[@class='bg-c-toast__content']")
    private WebElement invManualToast;

    @FindBy(xpath = "//tr")
    private List<WebElement> row;

    @FindBy(xpath = "((//tr)[2]/td)[5]")
    private WebElement totalInvoice;

    @FindBy(xpath = "//div[@class='bg-c-tooltip__target']")
    private List<WebElement> jnsBiayaHover;
    //---end buat dan kirim---//

    //---invoice page on tenant side---//
    @FindBy(xpath = "//a[@class='bg-c-link bg-c-link--high']")
    private List<WebElement> invoiceNumber;

    @FindBy(xpath = "//div[@class='container-fluid universal-invoice__wrapper']")
    private WebElement invoicePage;

    @FindBy(xpath = "(//p[@data-testid='voucherCodeApplied_txt'])[2]")
    private WebElement jnsPmbayaran;

    @FindBy(xpath = "//div[@class='invoice-content-right-col grid-item']/p")
    private WebElement totalPmbayaran;

    @FindBy(xpath = "//div[@class='invoice-billing-room-snippet']/p")
    private WebElement listingNameInInv;

    @FindBy(xpath = "//div[@class='collapse-content']/p")
    private WebElement invTypInInv;

    @FindBy(xpath = "(//div[@class='invoice-detail-row-section']//p)[1]")
    private WebElement biayaInInv;

    @FindBy(xpath = "(//div[@class='invoice-detail-row-section']//p)[2]")
    private WebElement totalPrice;

    @FindBy(xpath = "(//div[@class='invoice-detail-row-section']//p)[4]")
    private WebElement totalPrice2;
    //---end of invoice page on tenant side---//

    //---delete biaya tambahan & biaya sewa---//
    @FindBy(xpath = "//div[@class='action-button']/button")
    private List<WebElement> delNEditActionBtn;

    @FindBy(xpath = "//div[@class='action-button']")
    private WebElement actionBtn;

    @FindBy(xpath = "//div[@class='bg-c-modal__body']/h3")
    private WebElement deleteBiayaTitle;

    @FindBy(xpath = "//div[@class='bg-c-modal__body']/p")
    private WebElement deleteBiayaSubtitle;

    @FindBy(xpath = "//div[@class='bg-c-modal__footer-CTA']/button")
    private List<WebElement> deleteBiayaActionBtn;

    @FindBy(xpath = "//*[@class='global-toast bg-c-toast bg-c-toast--fixed']")
    private WebElement deleteBiayaToast;
    //---end of delete biaya tambahan & biaya sewa---//

    //---Search and filter on invoice manual---//
    @FindBy(xpath = "//span[@class='bg-c-select__trigger-text']")
    private WebElement searchBy;

    @FindBy(xpath = "//input[@class='bg-c-input__field']")
    private WebElement searchValue;

    @FindBy(xpath = "//div[@class='invoice-manual-filter']/button")
    private List<WebElement> mainInvManualBtn;

    @FindBy(xpath = "(//td)[1]")
    private WebElement invNumberCol;

    @FindBy(xpath = "//p[@class='bg-c-text bg-c-text--body-4 ']")
    private WebElement resultNoFound;
    //---End of search and filter on invoice manual---//

    //Ubah Status Invoice
    @FindBy(xpath = "//div[@data-testid='invoice-manual-action-btn']/div[@role='button']")
    private List<WebElement> actionButton;

    @FindBy(xpath = "//a[@data-testid='invoice-manual-change-status']")
    private List<WebElement> ubahStatusButton;

    @FindBy(xpath = "//a[@data-testid='invoice-manual-change-status']//parent::*/following-sibling::*")
    private List<WebElement> lihatDetailButton;

    @FindBy(css = "button.bg-c-modal__action-closable")
    private WebElement ubahStatusInvoiceCloseButton;

    @FindBy(css = "button[data-testid='change-status-cancel']")
    private WebElement ubahStatusInvoiceKembaliButton;
    //End Ubah Status Invoice

    //---Filter Invoice Manual---//
    @FindBy(xpath = "//button[@data-testid='invoice-manual-filter-button-filter']")
    private WebElement filterInv;

    @FindBy(xpath = "//p[contains(., 'Filter')]")
    private WebElement filterPopUp;

    @FindBy(xpath = "//div[@class='invoice-manual-filter-modal-title']/p[2]")
    private WebElement subtitleFilter;

    @FindBy(xpath = "//div[@class='bg-c-grid bg-c-grid--vtop bg-c-grid--left ']//child::p")
    private List<WebElement> titlesOnFilter;

    @FindBy(xpath = "(//p[@class='invoice-manual-filter-modal-item-title bg-c-text bg-c-text--title-3 '])[3]")
    private WebElement invCreatedTitle;

    @FindBy(xpath = "//input[@placeholder='Pilih tanggal di sini']")
    private List<WebElement> tglPlaceholder;

    @FindBy(xpath = "//button[@class='bg-c-modal__action-closable']")
    private WebElement closeBtnFilter;

    @FindBy(xpath = "//div[@class='flex justify-end']/button")
    private List<WebElement> btnFilter;

    @FindBy(xpath = "//div[@class='mr-8 bg-c-badge-counter bg-c-badge-counter--black']")
    private WebElement filterCounter;
    //---End of Filter Invoice Manual---//

    /**
     * get URL
     * @return string URL
     * @throws InterruptedException
     */
    public String getURL() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getURL();
    }

    /**
     * get page title
     * @return string page title
     * @throws InterruptedException
     */
    public String getInvoicePageTitle() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementVisible(pageInvoiceTitleText, 10);
        return selenium.getText(pageInvoiceTitleText);
    }

    /**
     * click buat invoice button
     * @throws InterruptedException
     */
    public void clickBuatInvoice() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsClickable(buatInvoiceButton);
        selenium.clickOn(buatInvoiceButton);
    }

    /**
     * choose jenis invoice
     * @param jenisInvoice jenis invoice name
     * @throws InterruptedException
     */
    public void chooseJenisInvoice(String jenisInvoice) throws InterruptedException {
        WebElement button = null;
        selenium.waitForJavascriptToLoad();
        switch (jenisInvoice){
            case "Biaya Tambahan":
                button = biayaTambahanRadioButton;
                break;
            case "Biaya Sewa":
                button = biayaSewaRadioButton;
                break;
            default:
                System.out.println("Invalid jenis invoice");
        }
        selenium.javascriptClickOn(button);
    }

    //---change invoice type---//
    /**
     * Get Pop Up Confirmation Title
     * @return String Pop Up Confirmation Title
     */
    public String getPopUpTitleConfirm(){
        selenium.waitInCaseElementVisible(popUpTitleConfirm, 10);
        return selenium.getText(popUpTitleConfirm);
    }

    /**
     * Get Pop Up Confirmation Subtitle
     * @return String Pop Up Confirmation Title
     */
    public String getPopUpSubtitleConfirm(){
        return selenium.getText(popUpSubtitleConfirm);
    }

    /**
     * Click Batal button on the Pop Up Confirmation
     * @throws InterruptedException
     */
    public void clickBatalOnPopUp() throws InterruptedException {
        selenium.waitInCaseElementClickable(batalBtn, 10);
        selenium.clickOn(batalBtn);
    }

    /**
     * Click Lanjutkan button on the Pop Up Confirmation
     * @throws InterruptedException
     */
    public void clickLanjutkanOnPopUp() throws InterruptedException {
        selenium.waitInCaseElementClickable(lanjutkanBtn, 10);
        selenium.clickOn(lanjutkanBtn);
    }

    /**
     * Get Biaya Sewa Title on Table
     * @return String Biaya Sewa Title on Table
     */
    public String getBiayaSewaTable() {
        return selenium.getText(biayaSewaTable);
    }

    /**
     * Get Biaya Tambahan Title on Table
     * @return String Biaya Tambahan Title on Table
     */
    public String getBiayaTambahanTable() {
        return selenium.getText(biayaTambahanTable);
    }

    /**
     * Get empty state on Biaya Tambahan & Biaya Sewa
     * @return String empty state on Biaya Tambahan & Biaya Sewa
     */
    public String getEmptyState() {
        return selenium.getText(emptyStateBiayaTable);
    }

    /**
     * True = the pop up confirmation is not displayed
     * False = the pop up confirmation is displayed
     * @return
     */
    public Boolean isPopUpConfirmationDisplay() {
        return selenium.isElementPresent(popUpConfirm) == null;
    }
    //---end of change invoice type---//

    //---buat dan kirim---//

    /**
     * Click on Buat dan Kirim button
     * @throws InterruptedException
     */
    public void clickBuatDanKirim() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsNotVisible(toastMessage,5);
        selenium.clickOn(buatNKirimBtn.get(0));
    }

    /**
     * Get Buat dan Kirim title in Buat dan Kirim pop up
     * @return String Buat dan Kirim title
     */
    public String getBuatNKirimTitle() {
        return selenium.getText(buatNKirimTitle);
    }

    /**
     * Get Buat dan Kirim subtitle in Buat dan Kirim pop up
     * @return String Buat dan Kirim subtitle
     */
    public String getBuatNKirimSubtitle() {
        return selenium.getText(buatNKirimSubtitle);
    }

    /**
     * Get Nama Listing in Buat dan Kirim pop up
     * @return String Nama Listing
     */
    public String getNamaListing() {
        return selenium.getText(buatNKirimDataPenyewa.get(1));
    }

    /**
     * Get Nama Penyewa in Buat dan Kirim pop up
     * @return String Nama Penyewa
     */
    public String getNamaPenyewa() {
        return selenium.getText(buatNKirimDataPenyewa.get(3));
    }

    /**
     * Get Nomor HP Penyewa in Buat dan Kirim pop up
     * @return String Nomor HP Penyewa
     */
    public String getNoHPPenyewa() {
        return selenium.getText(buatNKirimDataPenyewa.get(5));
    }

    /**
     * Get Nomor Kamar Penyewa in Buat dan Kirim pop up
     * @return String Nomor Kamar Penyewa
     */
    public String getNoKamar() {
        return selenium.getText(buatNKirimNoKmr);
    }

    /**
     * Get Jenis Invoice in Buat dan Kirim pop up
     * @return String Jenis Invoice
     */
    public String getJenisInvoice() {
        return selenium.getText(buatNKirimJnsInv);
    }

    /**
     * Get table title in Buat dan Kirim pop up
     * @return String table title
     */
    public String getTableTitle() {
        return selenium.getText(tableTitle);
    }

    /**
     * Click close button in Buat dan Kirim pop up
     * @throws InterruptedException
     */
    public void clickCloseBtn() throws InterruptedException {
        selenium.clickOn(closeBtn);
    }

    /**
     * Click kembali button in Buat dan Kirim pop up
     * @throws InterruptedException
     */
    public void clickKembaliBtn() throws InterruptedException {
        selenium.waitInCaseElementClickable(kembaliBtn, 5);
        selenium.clickOn(kembaliBtn);
    }

    /**
     * Click Buat dan Kirim button in Buat dan Kirim pop up
     * @throws InterruptedException
     */
    public void clickBuatNKirimBtn() throws InterruptedException {
        selenium.clickOn(buatNKirimBtn.get(1));
    }

    /**
     * Get Invoice Manual toast
     * @return String Invoice Manual toast
     */
    public String getInvoiceManualToast() {
        return selenium.getText(invManualToast);
    }

    /**
     * Get Name in Detail Penyewa coloumn
     * Split the detail penyewa value
     * And get the nama penyewa only
     * @return String Name
     */
    public String getDetailPenyewaName() {
        String detailPenyewa = "((//tr)["+row.size()+"]/td)[2]";
        selenium.waitInCaseElementVisible(By.xpath(detailPenyewa), 5);
        String full = selenium.getText(By.xpath(detailPenyewa));
        String result = full.substring(0, 24);
        System.out.println(result);
        return result;
    }

    /**
     * Get No HP in Detail Penyewa coloumn
     * Split the detail penyewa value
     * And get the no hp penyewa only
     * @return String No HP
     */
    public String getDetailPenyewaNoHp() {
        String detailPenyewa = "((//tr)["+row.size()+"]/td)[2]";
        String full = selenium.getText(By.xpath(detailPenyewa));
        String result = full.substring(26, 39);
        System.out.println(result);
        return result;
    }

    /**
     * Get Nama Listing in Nama Listing coloumn
     * @return Nama Listing
     */
    public String getNamaListingTable() {
        String namaListing = "((//tr)["+row.size()+"]/td)[3]";
        return selenium.getText(By.xpath(namaListing));
    }

    /**
     * Get Jenis Biaya in Jenis Biaya coloumn
     * @return Jenis Biaya
     */
    public String getJenisBiaya() {
        String jenisBiaya = "((//tr)["+row.size()+"]/td)[4]";
        return selenium.getText(By.xpath(jenisBiaya));
    }

    /**
     * Get Total Invoice in Total Invoice coloumn
     * @return Total Invoice
     */
    public String getTotalInv() {
        String totalInvoice = "((//tr)["+row.size()+"]/td)[5]";
        return selenium.getText(By.xpath(totalInvoice));
    }

    /**
     * Get Status Invoice in Status Invoice coloumn
     * @return Status Invoice
     */
    public String getStatusInv() {
        String statusInvoice = "((//tr)["+row.size()+"]/td)[6]";
        return selenium.getText(By.xpath(statusInvoice));
    }

    /**
     * check if invoice pop up is diplayed or not
     * @return true if displayed, false if not displayed
     * @throws InterruptedException
     */
    public Boolean isInvoicePopClosed() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementDisplayed(buatNKirimTitle);
    }

    /**
     * perform mouse over the jenis biaya hover
     */
    public void hoverJnsBiaya() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(jnsBiayaHover.get((jnsBiayaHover.size()-1)));
        selenium.focusOnElement(jnsBiayaHover.get((jnsBiayaHover.size())-1));
    }

    /**
     * Get biaya detail title after hovering mouse over the jenis biaya
     * @return String biaya detail title
     */
    public String getBiayaDetailTitle() {
        String jenisBiayaHoverTitle = "((//div[@id='tooltipContent'])["+jnsBiayaHover.size()+"]//child::p)[1]";
        return selenium.getText(By.xpath(jenisBiayaHoverTitle));
    }

    /**
     * Get biaya detail nama biaya list after hovering mouse over the jenis biaya
     * @return String biaya detail nama biaya list
     */
    public String getBiayaDetailList(){
        String jenisBiayaHoverList = "((//div[@id='tooltipContent'])["+jnsBiayaHover.size()+"]//child::p)[2]";
        return selenium.getText(By.xpath(jenisBiayaHoverList));
    }

    /**
     * Get biaya detail nama biaya price after hovering mouse over the jenis biaya
     * @return String biaya detail nama biaya price
     */
    public String getBiayaDetailPrice() {
        String jenisBiayaHoverPrice = "((//div[@id='tooltipContent'])["+jnsBiayaHover.size()+"]//child::p)[3]";
        return selenium.getText(By.xpath(jenisBiayaHoverPrice));
    }
    //---end buat dan kirim---//

    //---disable buat dan kirim button---//
    /**
     * check if the buat dan kirim button is disable
     * @return true if enable, false if disable
     */
    public boolean isBuatNKirimDisabled() {
        return selenium.isClickable(buatNKirimBtn.get(0), 1);
    }

    /**
     * Get invoice number in 1st row
     * @return invoice number
     * @throws InterruptedException
     */
    public String getInvoiceNumber() throws InterruptedException {
        String noInvoice = "((//tr)["+row.size()+"]/td)[1]";
        selenium.waitForJavascriptToLoad();
        return selenium.getText(By.xpath(noInvoice));
    }

    /**
     * get total amount invoice manual
     * @return total amount without thousand separator .
     */
    public String getTotalAmount() {
        String totalInvoice = "((//tr)["+row.size()+"]/td)[5]";
        String amount = selenium.getText(By.xpath(totalInvoice));
        String amountOnly = amount.substring(2);
        String result = amountOnly.replace(".","");
        System.out.println(amountOnly);
        System.out.println(result);
        return result;
    }
    //---end of disable buat dan kirim button---//

    //---invoice page on tenant side---//

    /**
     * click Invoice Number that just created from Biaya tambahan & Biaya sewa
     * @throws InterruptedException
     */
    public void clickInvoiceNumber() throws InterruptedException {
        selenium.clickOn(invoiceNumber.get((invoiceNumber.size())-2));
    }

    /**
     * switch page from invoice manual (mamipay page) into invoice page
     * invoice manual page -> page/tab no 1
     * invoice page -> page/tab no 2
     */
    public void switchPage(){
        selenium.switchToWindow(2);
        selenium.waitInCaseElementVisible(invoicePage, 5);
    }

    /**
     * Get jenis pembayaran on invoice page
     * @return String jenis pembayaran
     */
    public String getJnsPmbayaran() {
        selenium.waitInCaseElementVisible(jnsPmbayaran, 3);
        return selenium.getText(jnsPmbayaran);
    }

    /**
     * Get total pembayaran on invoice page
     * @return String total pembayaran
     */
    public String getTotalPmbayaran() {
        return selenium.getText(totalPmbayaran);
    }

    /**
     * Get listing name on invoice page
     * @return String listing name
     */
    public String getListingNameInInvoice() {
        return selenium.getText(listingNameInInv);
    }

    /**
     * Get rincian pembayaran invoice type on invoice page
     * @return String rincian pembayaran invoice type
     */
    public String getRincianPmbayaranInvType() {
        return selenium.getText(invTypInInv);
    }

    /**
     * Get rincian pembayaran biaya on invoice page
     * @return String rincian pembayaran biaya
     */
    public String getRincianPmbayaranBiaya() {
        return selenium.getText(biayaInInv);
    }

    /**
     * Get rincian pembayaran total on invoice page
     * @return String rincian pembayaran total
     */
    public String getRincianPmbayaranTotal() {
        return selenium.getText(totalPrice);
    }

    /**
     * Get rincian pembayaran total (second) on invoice page
     * @return rincian pembayaran total
     */
    public String getRincianPmbayaranTotal2() {
        return selenium.getText(totalPrice2);
    }

    /**
     * click last page in pagination
     * @throws InterruptedException
     */
    public void goToLastPage() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(pagination.get((pagination.size()-1)));
        selenium.hardWait(5);
    }
    //---end of invoice page on tenant side---//

    //---delete biaya tambahan & biaya sewa---//

    /**
     * Click delete on biaya table
     * @throws InterruptedException
     */
    public void clickDeleteBiaya() throws InterruptedException {
        selenium.clickOn(delNEditActionBtn.get(1));
    }

    /**
     * Get delete title on delete confirmation pop up
     * @return String delete title on delete confirmation pop up
     */
    public String getDeleteBiayaTitle() {
        selenium.waitInCaseElementVisible(deleteBiayaTitle, 10);
        return selenium.getText(deleteBiayaTitle);
    }

    /**
     * Get delete subtitle on delete confirmation pop up
     * @return String delete subtitle on delete confirmation pop up
     */
    public String getDeleteBiayaSubtitle() {
        return selenium.getText(deleteBiayaSubtitle);
    }

    /**
     * Click Batal on delete confirmation pop up
     * @throws InterruptedException
     */
    public void clickBatal() throws InterruptedException {
        selenium.clickOn(deleteBiayaActionBtn.get(0));
    }

    /**
     * Click Hapus on delete confirmation pop up
     * @throws InterruptedException
     */
    public void clickHapus() throws InterruptedException {
        selenium.clickOn(deleteBiayaActionBtn.get(1));
    }

    /**
     * Check is delete confirmation pop up appears
     * @return String delte confirmation pop up
     * @throws InterruptedException
     */
    public boolean isDeletePopAppears() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementDisplayed(deleteBiayaTitle);
    }

    /**
     * Get succeed delete biaya toast
     * @return String succeed delete biaya toast
     */
    public String getDeleteBiayaToast() {
        return selenium.getText(deleteBiayaToast);
    }

    /**
     * Check if the delete confirmation pop is visible
     * @return String delete confirmation pop is visible
     */
    public boolean isDeleteBiayaVisible(){
        return selenium.waitInCaseElementVisible(actionBtn, 3) != null;
    }

    /**
     * Delete all biaya sewa/tambahan
     * Check if the delete button on biaya table appears
     * Then do delete biaya
     * If the delete button on biaya table not appears
     * Then stop delete and return empty state on biaya table
     * @throws InterruptedException
     */
    public void deleteAllBiaya() throws InterruptedException {
        int i;
        for (i=0; i<=5; i++){
            if (isDeleteBiayaVisible()){
                selenium.clickOn(delNEditActionBtn.get(1));
                selenium.waitInCaseElementVisible(deleteBiayaActionBtn.get(1), 2);
                selenium.clickOn(deleteBiayaActionBtn.get(1));
                selenium.hardWait(1);
            } else {
                break;
            }
        }
    }
    //---end of delete biaya tambahan & biaya sewa---//

    //---Search on invoice manual---//

    /**
     * Click Nomor Invoice Dropdown
     * And choose option in dropdown
     * @param search
     * @throws InterruptedException
     */
    public void chooseSearchBy(String search) throws InterruptedException {
        WebElement options = driver.findElement(By.xpath("//div[@class='bg-c-dropdown__menu-item-content'][contains(., '" +search+ "')]"));
        selenium.clickOn(searchBy);
        selenium.clickOn(options);
    }

    /**
     * Enter search value in Search bar
     * @param search
     * @throws InterruptedException
     */
    public void enterSearchValue(String search) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.enterText(searchValue, search, false);
    }

    /**
     * Click on Search button
     * @throws InterruptedException
     */
    public void clickSearch() throws InterruptedException {
        selenium.clickOn(mainInvManualBtn.get(2));
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Get Search result
     * @return String Search result
     */
    public String getSearchResult1() {
        return selenium.getText(invNumberCol);
    }

    /**
     * Get Resul No Found
     * @return String Resul No Found
     */
    public String getNoFound() {
        return selenium.getText(resultNoFound);
    }

    //---End of search on invoice manual---//

    //Ubah status invoice

    /**
     * Click Action button in last row
     * @throws InterruptedException
     */
    public void clickActionButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(actionButton.get(actionButton.size()-1));
        selenium.clickOn(actionButton.get(actionButton.size()-1));
    }

    /**
     * Click action button in last row
     * @param button Ubah Status or Lihat Detail
     * @throws InterruptedException
     */
    public void chooseAction(String button) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        if (button.equalsIgnoreCase("Ubah Status")){
            selenium.clickOn(ubahStatusButton.get((ubahStatusButton.size())-1));
        }else if (button.equalsIgnoreCase("Lihat Detail")){
            selenium.clickOn(lihatDetailButton.get((lihatDetailButton.size())-1));
        } else {
            System.out.println("Button not Available");
        }
    }

    /**
     * get Status Invoice in Invoice Manual table
     * @return String - Paid / Unpaid
     */
    public String getStatusInvoice() {
        String status = "(//*[@data-testid='invoice-manual-item-"+(row.size()-2)+"']/td)[6]/div";
        selenium.waitTillElementIsVisible(By.xpath(status),10);
        return selenium.getText(By.xpath(status));
    }

    /**
     * get paid date below Paid status in Invoice Manual table
     * @return String - at dd/MM/yyyy, time
     */
    public String getPaidDate() {
        String status = "(//*[@data-testid='invoice-manual-item-"+(row.size()-2)+"']/td)[6]";
        selenium.waitTillElementIsVisible(By.xpath(status),10);
        String full = selenium.getText(By.xpath(status));
        String paidDate = full.substring(5);
        return paidDate;
    }

    /**
     * Click X button to close modal Ubah Status Invoice
     * @throws InterruptedException
     */
    public void closeUbahStatusInvoiceModal() throws InterruptedException {
        selenium.clickOn(ubahStatusInvoiceCloseButton);
    }

    /**
     * Click Kembali button to close modal Ubah Status Invoice
     * @throws InterruptedException
     */
    public void clickKembaliUbahStatusInvoice() throws InterruptedException {
        selenium.clickOn(ubahStatusInvoiceKembaliButton);
    }
    //end ubah status invoice

    //---Filter Invoice Manual---//

    /**
     * Click Filter in Invoice Manual
     * @throws InterruptedException
     */
    public void clickFilterInv() throws InterruptedException {
        selenium.clickOn(filterInv);
    }

    /**
     * Check if the Filter pop up is appears
     * True = filter pop up appears
     * False = otherwise
     * @return Filter pop up
     * @throws InterruptedException
     */
    public boolean isFilterPopUpAppears() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementDisplayed(filterPopUp);
    }

    /**
     * Get Filter title
     * @return String Filter title
     */
    public String getFilterTitle() {
        return selenium.getText(filterPopUp);
    }

    /**
     * Get Filter subtitle
     * @return String Filter subtitle
     */
    public String getFilterSubtitle() {
        return selenium.getText(subtitleFilter);
    }

    /**
     * Get Status Invoice title
     * @return String Status Invoice title
     */
    public String getStatusInvTitle(){
        return selenium.getText(titlesOnFilter.get(0));
    }

    /**
     * Get Status Invoice Placeholder
     * @return String Status Invoice Placeholder
     */
    public String getStatusInvPlaceHolder() {
        return selenium.getText(titlesOnFilter.get(1));
    }

    /**
     * Get Jenis Biaya title
     * @return String Jenis Biaya title
     */
    public String getJenisBiayaTitle() {
        return selenium.getText(titlesOnFilter.get(5));
    }

    /**
     * Get Jenis Biaya Placeholder
     * @return String Jenis Biaya Placeholder
     */
    public String getJenisBiayaPlaceHolder() {
        return selenium.getText(titlesOnFilter.get(6));
    }

    /**
     * Get Tanggal Dibuat title
     * @return String Tanggal Dibuat title
     */
    public String getInvCreatedTitle() {
        return selenium.getText(invCreatedTitle);
    }

    /**
     * Get Tanggal Mulai title
     * @return String Tanggal Mulai title
     */
    public String getTglMulaiTitle() {
        return selenium.getText(titlesOnFilter.get(9));
    }

    /**
     * Get Tanggal Akhir title
     * @return String Tanggal Akhir title
     */
    public String getTglAkhirTitle() {
        return selenium.getText(titlesOnFilter.get(10));
    }

    /**
     * Click on Close button in Filter pop up
     * @throws InterruptedException
     */
    public void clickOnClose() throws InterruptedException {
        selenium.clickOn(closeBtnFilter);
    }

    /**
     * Click on Terapkan button in Filter pop up
     * @throws InterruptedException
     */
    public void clickOnTerapkan() throws InterruptedException {
        selenium.clickOn(btnFilter.get(1));
    }

    /**
     * Click on Reset button in Filter pop up
     * @throws InterruptedException
     */
    public void clickOnReset() throws InterruptedException {
        selenium.clickOn(btnFilter.get(0));
    }

    /**
     * Check Filter Counter in Filter button
     * True = Filter Counter appears
     * False = otherwise
     * @return Filter Counter
     */
    public boolean isFilterCounterDisplayed() {
        return selenium.isElementDisplayed(filterCounter);
    }

    /**
     * Click Status Invoice Dropdown
     * @throws InterruptedException
     */
    public void clickStatusInvDropdown() throws InterruptedException {
        selenium.clickOn(titlesOnFilter.get(1));
    }

    /**
     * Tick on Status Invoice Dropdown
     * @param tickDropDwn
     * @throws InterruptedException
     */
    public void tickOnInvStatus(String tickDropDwn) throws InterruptedException {
        WebElement invStatusDropdown = driver.findElement(By.xpath("//p[contains(., '" +tickDropDwn+ "')]"));
        selenium.clickOn(invStatusDropdown);
    }

    /**
     * Click on Main Reset in Invoice Manual page
     * @throws InterruptedException
     */
    public void clickOnMainReset() throws InterruptedException {
        selenium.clickOn(mainInvManualBtn.get(1));
    }

    /**
     * Get Invoice Status Paid in Detail Penyewa coloumn
     * Extract the characters
     * And get character Paid only
     * @return String Paid
     */
    public String getInvStatusPaid(){
        String statusInvoice = "((//tr)["+row.size()+"]/td)[6]";
        String full = selenium.getText(By.xpath(statusInvoice));
        String result = full.substring(0, 4);
        System.out.println(result);
        return result;
    }

    /**
     * Click Jenis Biaya Dropdown
     * @throws InterruptedException
     */
    public void clickJnsBiayaDropdown() throws InterruptedException {
        selenium.clickOn(titlesOnFilter.get(6));
    }

    /**
     * Get Date on Dibuat oleh in Detail Penyewa coloumn
     * Get all the characters on Dibuat oleh coloumn
     * And split the characters from word 'at'
     * Then get Date '19/09/2022' characters only
     * @return String Date
     */
    public String getCreatedBy() {
        String status = "(//*[@data-testid='invoice-manual-item-"+(row.size()-2)+"']/td)[7]";
        String full = selenium.getText(By.xpath(status));
        String split = full.split("at ")[1];
        String result = split.substring(0, 10);
        return result;
    }
    //---End of Filter Invoice Manual---//
}
