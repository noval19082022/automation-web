package pageobjects.mamipay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class AllInvoiceListPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AllInvoiceListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }


    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//h3[@class='box-title']")
    private WebElement pageTitleText;

    @FindBy(xpath = "//select[@name='order_type']")
    private WebElement orderTypeFilterOption;

    @FindBy(xpath = "//input[@class='btn btn-primary btn-md']")
    private WebElement cariInvoiceButton;

    @FindBy(xpath = "//table/tbody/tr")
    private List<WebElement> listInvoice;

    @FindBy(xpath = "//input[@name='search_value']")
    private WebElement searchInput;

    @FindBy(xpath = "//td[1]/a")
    private WebElement shortlink;

    @FindBy(xpath = "//td[2]")
    private WebElement invoiceNumberText;

    @FindBy(xpath = "//td[9]/span")
    private WebElement invoiceStatusText;

    @FindBy(xpath = "(//a[@class='btn btn-xs bg-navy btn-flat'])[1]")
    private WebElement changeStatusButton;

    @FindBy(xpath = "//a[@class='btn btn-xs bg-purple btn-flat']")
    private List<WebElement> viewLogButton;

    @FindBy(xpath = "//*[@name='status']")
    private WebElement statusInvoiceDropdown;

    @FindBy(xpath = "//input[@name='paid_at']")
    private WebElement paidAtInputText;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submitChangeButton;

    @FindBy(xpath = "(//table)[1]//td")
    private List<WebElement> dataInvoice;

    @FindBy(xpath = "(//table)[3]//td")
    private List<WebElement> paymentHistory;

    /**
     * get title page all invoice list
     * @return page title
     * @throws InterruptedException
     */
    public String getPageTitle() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(pageTitleText);
    }

    /**
     * set Order Type Filter
     * @param type
     */
    public void setOrderTypeFilter(String type) throws InterruptedException {
        selenium.waitTillElementIsVisible(orderTypeFilterOption);
        selenium.clickOn(orderTypeFilterOption);
        selenium.selectDropdownValueByText(orderTypeFilterOption,type);
    }

    /**
     * click cari invoice to apply filter
     */
    public void clickCariInvoice() throws InterruptedException {
        selenium.waitTillElementIsClickable(cariInvoiceButton);
        selenium.clickOn(cariInvoiceButton);
    }

    /**
     * count total invoice display
     * @return total invoice
     */
    public int countList() {
        return listInvoice.size();
    }

    /**
     * Compare order type ke-i with value Biaya sewa or biaya tambahan
     * @param i index or row
     * @return true if equal, false if not
     */
    public boolean isOrderTypeInvoiceManual(int i) {
        String xpath = "(//table/tbody/tr)["+i+"]/td[5]";
        String actual = selenium.getText(By.xpath(xpath));
        boolean result = false;
        if (actual.equalsIgnoreCase("Biaya Sewa") || actual.equalsIgnoreCase("Biaya Tambahan")){
            result = true;
        } else {
            System.out.println("Found incorrect order type in row-"+i+" => "+actual);
        }
        return result;
    }

    /**
     * set search value
     * @param invoiceNumber keyword invoice number
     * @throws InterruptedException
     */
    public void setSearchValue(String invoiceNumber) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.enterText(searchInput,invoiceNumber,true);
    }

    /**
     * get invoice number 1st row in table
     * @return invoice number
     * @throws InterruptedException
     */
    public String getInvoiceNumber() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(invoiceNumberText);
    }

    /**
     * get invoice status 1st row in table
     * @return invoice status
     * @throws InterruptedException
     */
    public String getInvoiceStatus() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(invoiceStatusText);
    }

    /**
     * click change status
     * @throws InterruptedException
     */
    public void clickChangeStatus() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(changeStatusButton);
    }

    /**
     * set invoice status to paid / unpaid
     * @param status paid, unpaid
     * @throws InterruptedException
     */
    public void setInvoiceStatus(String status) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(statusInvoiceDropdown);
        selenium.selectDropdownValueByText(statusInvoiceDropdown,status);
    }

    /**
     * set paid Date
     * @param date format yyyy-MM-dd HH:mm:ss
     * @throws InterruptedException
     */
    public void setPaidDate(String date) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        if (date.equalsIgnoreCase("today")){
            LocalDateTime today = LocalDateTime.now();
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String d = today.format(format);
            selenium.enterText(paidAtInputText,d,true);
        } else {
            selenium.enterText(paidAtInputText,date,true);
        }
    }

    /**
     * click submit change to save invoice change status
     * @throws InterruptedException
     */
    public void clickSubmitChange() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(submitChangeButton);
    }

    /**
     * click shortlink
     * @throws InterruptedException
     */
    public void clickShortlink() throws InterruptedException {
        selenium.clickOn(shortlink);
    }

    public void clickViewLog() throws InterruptedException {
        selenium.clickOn(viewLogButton.get(0));
    }

    /**
     * Get Data Invoice information
     * @param column column name that want to get
     * @return String value in targeted column name
     */
    public String getDataInvoice(String column) {
        String result = null;
        switch (column){
            case "Invoice Number":
                result = selenium.getText(dataInvoice.get(0));
                break;
            case "Status":
                result = selenium.getText(dataInvoice.get(2));
                break;
            case "Order Type":
                result = selenium.getText(dataInvoice.get(3));
                break;
            case "Amount":
                result = selenium.getText(dataInvoice.get(5));
                break;
            case "Paid Amount":
                result = selenium.getText(dataInvoice.get(6));
                break;
        }
        return result;
    }

    /**
     * Get Invoice Payment History
     * @param column column name
     * @return String value in targeted column name
     */
    public String getDataInvoicePaymentHistory(String column) {
        String result = null;
        switch (column){
            case "Payment Method":
                result = selenium.getText(paymentHistory.get(0));
                break;
            case "Amount":
                result = selenium.getText(paymentHistory.get(1));
                break;
            case "Status":
                result = selenium.getText(paymentHistory.get(5));
                break;
            case "Paid Amount":
                result = selenium.getText(paymentHistory.get(6));
                break;
        }
        return result;
    }

    /**
     * Get Data Revision History in column x and row y
     * @param column column name
     * @param row row
     * @return String value in targeted column name and row
     */
    public String getDataRevisionHistory(String column, String row) {
        String result = null;
        String locator = "";
        Integer r = Integer.parseInt(row);

        switch (column){
            case "Changed by":
                locator = "(((//table)[2]//tr)["+(r+1)+"]//td)[1]";
                result = selenium.getText(By.xpath(locator));
                break;
            case "Changer role":
                locator = "(((//table)[2]//tr)["+(r+1)+"]//td)[2]";
                result = selenium.getText(By.xpath(locator));
                break;
            case "What changed":
                locator = "(((//table)[2]//tr)["+(r+1)+"]//td)[3]";
                result = selenium.getText(By.xpath(locator));
                break;
            case "Old Value":
                locator = "(((//table)[2]//tr)["+(r+1)+"]//td)[4]";
                result = selenium.getText(By.xpath(locator));
                break;
            case "New Value":
                locator = "(((//table)[2]//tr)["+(r+1)+"]//td)[5]";
                result = selenium.getText(By.xpath(locator));
                break;
        }
        return result;
    }
}
