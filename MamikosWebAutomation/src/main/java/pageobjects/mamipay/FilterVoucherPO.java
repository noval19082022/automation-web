package pageobjects.mamipay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class FilterVoucherPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public FilterVoucherPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(css = "body")
    private WebElement voucherListTable;

    @FindBy(css = ".btn-md")
    private WebElement resetFilterButton;

    /**
     * Get all list filter value options
     * @param filter to define which filter we want to get also for element (Rules, Team, By, Status)
     * @return List of Strings statistic options
     */
    public List<String> getAllFilterOptions(String filter) {
        return selenium.getAllDropdownValues(driver.findElement(By.name(filter)));
    }

    /**
     * Click on dropdown filter voucher
     * @param filter to define which filter we want to get also for element (Rules, Team, By, Status, ID/Code/Campaign Name)
     * @value to choose which value we want to filter
     * @throws InterruptedException
     */
    public void chooseDropDownFilterVoucher(String filter, String value){
        if(filter.equals("campaign_voucher")){
            selenium.enterText(By.name(filter),value,true);
        } else {
            selenium.selectDropdownValueByText(driver.findElement(By.name(filter)),value);
        }
    }

    /**
     * Get Voucher List Table Text
     * @return string
     */
    public String getVoucherListTable() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
        return selenium.getText(voucherListTable);
    }

    /**
     * Click on reset filter button
     * @throws InterruptedException
     */
    public void clickResetFilterButton() throws InterruptedException {
        selenium.clickOn(resetFilterButton);
    }
}
