package pageobjects.mamipay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class CreateManualInvoicePO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public CreateManualInvoicePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='mb-24']/p")
    private WebElement buatInvoiceTitleText;

    @FindBy(xpath = "//input[@placeholder='Masukkan nama listing']")
    private WebElement searchListingInputText;

    @FindBy(xpath = "(//a[@role='button'])[1]")
    private WebElement listingSuggestionText;

    @FindBy(xpath = "//input[@placeholder='Masukkan nama penyewa']")
    private WebElement searchTenantInputText;

    @FindBy(xpath = "(//a[@role='button'])[1]")
    private WebElement tenantNameSuggestionText;

    @FindBy(xpath = "//input[@placeholder='No. HP penyewa']")
    private WebElement tenantPhoneNumberText;

    @FindBy(xpath = "//input[@placeholder='Masukkan no. kamar penyewa']")
    private WebElement tenantRoomNumberText;

    @FindBy(xpath = "//*[@class='icon-button']")
    private WebElement backButton;

    @FindBy(xpath = "//*[@class='bg-c-modal__inner']")
    private WebElement exitConfirmationPopUp;

    @FindBy(xpath = "//*[@class='bg-c-modal__body-title']")
    private WebElement exitConfirmationPopUpTitle;

    @FindBy(xpath = "//*[@class='bg-c-modal__body-description']")
    private WebElement exitConfirmatioPopUpDescription;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-button--primary bg-c-button--lg']")
    private WebElement tidakButton;

    @FindBy(xpath = "//*[@class='bg-c-button bg-c-button--tertiary bg-c-button--lg']")
    private WebElement yaButton;

    //---create biaya tambahan---//
    @FindBy(xpath = "//a[contains(., 'Invoice Manual')]")
    private WebElement invoiceManualMenu;

    @FindBy(xpath = "//button[contains(., 'Buat Invoice')]")
    private WebElement buatInvoiceBtn;

    @FindBy(xpath = "//input[@class='bg-c-input__field']")
    private List<WebElement> buatInvoiceFields;

    @FindBy(xpath = "//a[@class='bg-c-dropdown__menu-item bg-u-radius--md']")
    private List<WebElement> buatInvoiceFieldsDropdown;

    @FindBy(xpath = "//p[contains(., 'Biaya Tambahan')]")
    private WebElement biayaTambahan;

    @FindBy(xpath = "//button[contains(., 'Tambah')]")
    private List<WebElement> tambahBtn;

    @FindBy(xpath = "//button[@data-testid='add-cost-data']")
    private WebElement tambahBiayaButton;

    @FindBy(xpath = "//div[@data-testid='billing-modal-jenis-biaya']")
    private WebElement namaBiayaDropDown;

    @FindBy(xpath = "//span[@name='Nama biaya']//child::span")
    private WebElement namaBiayaText;

    @FindBy(xpath = "(//ul)[9]")
    private WebElement listDropdown;

    @FindBy(xpath = "//input[@placeholder='Pilih tanggal di sini']")
    private List<WebElement> periodeDate;

    @FindBy(xpath = "(//div[@class='vdp-datepicker__calendar'])[2]")
    private WebElement calendarView;

    @FindBy(xpath = "//textarea[@class='bg-c-textarea__field bg-c-textarea__field--lg']")
    private WebElement durasiBiayaTxt;

    @FindBy(xpath = "//input[@data-testid='billing-modal-jumlah-biaya']")
    private WebElement jumlahBiayaTxt;

    @FindBy(xpath = "//*[@class='global-toast bg-c-toast bg-c-toast--fixed']")
    private WebElement toastMessage;

    @FindBy(xpath = "//td")
    private List<WebElement> columnName;

    @FindBy(xpath = "//input[@data-testid='billing-modal-nama-biaya']")
    private WebElement lainnyaField;

    @FindBy(xpath = "//*[@class='bg-c-textarea__counter']")
    private WebElement durasiBiayaCounter;

    @FindBy(xpath = "//button[@class='bg-c-modal__action-closable']")
    private WebElement closeModalTambahBiayaButton;

    @FindBy(xpath = "//*[@class='bg-c-modal__inner']")
    private WebElement modalTambahBiaya;

    @FindBy(xpath = "//button[contains(.,'Kembali')]")
    private WebElement kembaliModalTambahBiayaButton;

    @FindBy(xpath = "//*[@class='bg-c-icon bg-c-icon--sm']")
    private List<WebElement> actionButton;
    //---end of create biaya tambahan---//

    //---required field biaya tambahan---//
    @FindBy(xpath = "(//span[contains(., 'Nama biaya tidak boleh kosong.')])[3]")
    private WebElement namaBiayaErrMsg;

    @FindBy(xpath = "(//span[contains(., 'Periode awal tidak boleh kosong.')])[3]")
    private WebElement periodeAwalErrMsg;

    @FindBy(xpath = "(//span[contains(., 'Periode akhir tidak boleh kosong.')])[3]")
    private WebElement periodeAkhirErrMsg;

    @FindBy(xpath = "(//span[contains(., 'Jumlah biaya tidak boleh kosong.')])[3]")
    private WebElement jumlahBiayaErrMsg;

    //---end of required field biaya tambahan---//

    /**
     * get buat invoice manual page
     * @return string buat invoice manual page
     * @throws InterruptedException
     */
    public String getBuatInvoicePageTitle() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(buatInvoiceTitleText);
        return selenium.getText(buatInvoiceTitleText);
    }

    /**
     * set listing name in create invoice manual
     * @param listingName listing name or keyword
     * @throws InterruptedException
     */
    public void setListingName(String listingName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsClickable(searchListingInputText);
        selenium.click(searchListingInputText);
        selenium.enterText(searchListingInputText,listingName,true);
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(listingSuggestionText);
    }

    /**
     * set tenant name in create invoice manual
     * @param tenantNamePMAN tenant name or keyword
     * @throws InterruptedException
     */
    public void setTenantName(String tenantNamePMAN) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsClickable(searchTenantInputText);
        selenium.click(searchTenantInputText);
        selenium.enterText(searchTenantInputText,tenantNamePMAN,true);
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(tenantNameSuggestionText);
    }

    /**
     * get Tenant phone number
     * @return String tenant phone number
     * @throws InterruptedException
     */
    public String getTenantNoHP() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(tenantPhoneNumberText,"value");
    }

    /**
     * check if no HP penyewa field disabled or not, by having attribute disabled
     * @return true if disabled, false if enable
     * @throws InterruptedException
     */
    public boolean isNoHPPenyewaDisabled() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementAtrributePresent(tenantPhoneNumberText,"disabled");
    }

    /**
     * get tenant room number
     * @return string tenant phone number
     * @throws InterruptedException
     */
    public String getTenantNoKamar() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(tenantRoomNumberText,"value");
    }

    /**
     * check if no kamar penyewa field disabled or not, by having attribute disabled
     * @return true if disabled, false if enable
     * @throws InterruptedException
     */
    public boolean isNoKamarPenyewaDisabled() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementAtrributePresent(tenantRoomNumberText,"disabled");
    }

    /**
     * click Back Button in Buat Invoice page
     * @throws InterruptedException
     */
    public void clickBackButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsClickable(backButton);
        selenium.clickOn(backButton);
    }

    /**
     * check if exit confirmation pop up appear
     * @return true if pop up appear, false if pop up not appear
     * @throws InterruptedException
     */
    public boolean isExitConfirmationPopUpAppear() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementDisplayed(exitConfirmationPopUp);
    }

    /**
     * get Title of Exit Confirmation Pop Up
     * @return String pop up title
     * @throws InterruptedException
     */
    public String getExitConfirmationPopUpTitile() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(exitConfirmationPopUpTitle);
    }

    /**
     * get Description of Exit Confirmation Pop Up
      * @return string pop up description
     * @throws InterruptedException
     */
    public String getExitConfirmationPopUpDesc() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(exitConfirmatioPopUpDescription);
    }

    /**
     * get Text in green button confirmation pop up
     * @return String text
     * @throws InterruptedException
     */
    public String getGreenButtonConfirmation() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(tidakButton);
    }

    /**
     * get Text in white button confirmation pop up
     * @return String text
     * @throws InterruptedException
     */
    public String getWhiteButtonConfirmation() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(yaButton);
    }

    /**
     * click action confirmation pop up
     * @param action Ya/Tidak
     * @throws InterruptedException
     */
    public void clickConfirmationPopUp(String action) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        if (action.equalsIgnoreCase("Ya")){
            selenium.clickOn(yaButton);
        } else if (action.equalsIgnoreCase("Tidak")){
            selenium.clickOn(tidakButton);
        } else {
            System.out.println("Button not exist");
        }
    }

    //---create biaya tambahan---//

    /**
     * click Tambah button on the Buat Invoice page
     * @throws InterruptedException
     */
    public void clickTambah() throws InterruptedException {
        selenium.clickOn(tambahBtn.get(0));
    }

    /**
     * click Nama Biaya dropdown and choose the Nama Biaya Tambahan
     * @param namaBiaya
     * @throws InterruptedException
     */
    public void chooseNamaBiaya(String namaBiaya) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(namaBiayaDropDown);
        selenium.waitInCaseElementVisible(listDropdown, 10);
        WebElement biaya = driver.findElement(By.xpath("//div[@class='bg-c-dropdown']//li[contains(., '" +namaBiaya+ "')]"));
        selenium.clickOn(biaya);
    }

    public void inputLainnyaTxt(String lainnya){
        selenium.enterText(lainnyaField, lainnya, false);
    }

    /**
     * Fill durasi biaya according parameter
     * @param durasiBiaya
     * @throws InterruptedException
     */
    public void fillDurasiBiaya(String durasiBiaya) throws InterruptedException {
        selenium.enterText(durasiBiayaTxt, durasiBiaya, true);
    }

    /**
     * clear durasi biaya field
     */
    public void emptyDurasiBiaya() {
        selenium.clearTextField(durasiBiayaTxt);
    }

    /**
     * Input jumlah biaya according parameter
     * @param jmlBiaya
     */
    public void inputJmlBiaya(String jmlBiaya){
        selenium.enterText(jumlahBiayaTxt, jmlBiaya, true);
    }

    /**
     * click Tambah button on the Biaya Tambahan pop up
     * @throws InterruptedException
     */
    public void clickTambahBiayaTmbhn() throws InterruptedException {
        selenium.clickOn(tambahBiayaButton);
    }

    /**
     *  Get toast success add biaya sewa / biaya tambahan
     * @return String biaya sewa toast
     */
    public String getToastBiaya() {
        return selenium.getText(toastMessage);
    }

    /**
     * Select date on Periode Awal as today
     * @param date
     * @throws InterruptedException
     */
    public void setTglPeriodeAwal(String date) throws InterruptedException {
        selenium.clickOn(periodeDate.get(0));
        selenium.waitInCaseElementVisible(calendarView, 5);
        try {
            WebElement selectDate = driver.findElement(By.xpath("//span[@class='cell day today'][contains(., '" +date+ "')]"));
            selenium.clickOn(selectDate);
        } catch (Exception e){
            WebElement selectDate = driver.findElement(By.xpath("//span[@class='cell day selected today']/parent::div/following-sibling::*[contains(., '" +date+ "')]"));
            selenium.clickOn(selectDate);
        }
    }

    /**
     * Select date on Periode Akhir as tomorrow
     * @param date
     * @throws InterruptedException
     */
    public void setTglPeriodeAkhir(String date) throws InterruptedException {
        selenium.clickOn(periodeDate.get(1));
        selenium.waitInCaseElementVisible(calendarView, 5);
        try {
            WebElement selectDate = driver.findElement(By.xpath("//span[@class='cell day today']/parent::div/following-sibling::*[contains(., '" +date+ "')]"));
            selenium.clickOn(selectDate);
        } catch (Exception e){
            WebElement selectDate = driver.findElement(By.xpath("//span[@class='cell day disabled today']/parent::div/following-sibling::*[contains(., '" +date+ "')]"));
            selenium.clickOn(selectDate);
        }
    }

    /**
     * Get Nama Biaya in the biaya tambahan table
     * @return String nama biaya table
     */
    public String getNamaBiayaTable() {
        return selenium.getText(columnName.get(0));
    }

    /**
     * Get nama biaya in specific row
     * @param row
     * @return nama biaya on row x
     * @throws InterruptedException
     */
    public String getNamaBiayaTable(int row) throws InterruptedException {
        int index = 5*(row-1);
        selenium.waitForJavascriptToLoad();
        return selenium.getText(columnName.get(index));
    }

    /**
     * Get nama tambahan in specific row
     * @param row
     * @return nama biaya on row x
     * @throws InterruptedException
     */
    public String getNamaBiayaTable2(int row) throws InterruptedException {
        int index = 6*(row-1);
        selenium.waitForJavascriptToLoad();
        return selenium.getText(columnName.get(index));
    }

    /**
     * Get Periode Awal in the biaya tambahan table
     * @return String Periode Awal table
     */
    public String getTglPeriodeAwal() {
        return selenium.getText(columnName.get(1));
    }

    /**
     * Get Periode Akhir in the biaya tambahan table
     * @return String Periode Akhir table
     */
    public String getTglPeriodeAkhir() {
        return selenium.getText(columnName.get(2));
    }

    /**
     * Get Jumlah Biaya in the biaya tambahan table
     * @return String Jumlah Biaya table
     */
    public String getJmlBiayaTable() {
        return selenium.getText(columnName.get(3));
    }

    /**
     * Get Disburse to Pemilik in the biaya tambahan table
     * @return String Disburse to Pemilik table
     */
    public String getDisburseToPemilikTable() {
        return selenium.getText(columnName.get(4));
    }

    /**
     * get Durasi biaya content in pop up tambah biaya
     * @return string value of durasi biaya
     * @throws InterruptedException
     */
    public String getDurasiBiaya() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(durasiBiayaTxt,"value");
    }

    /**
     * get counter durasi biaya
     * @return string counter in durasi biaya
     * @throws InterruptedException
     */
    public String getCounterDurasiBiaya() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(durasiBiayaCounter);
    }

     /** check periode awal field have attribute disabled
     * @return true if have disabled, false if not
     * @throws InterruptedException
     */
    public boolean isPeriodeAwalDisabled() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementAtrributePresent(periodeDate.get(0),"disabled");
    }

    /**
     * check periode akhir field have attribute disabled
     * @return true if have disabled, false if not
     * @throws InterruptedException
     */
    public boolean isPeriodeAkhirDisabled() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementAtrributePresent(periodeDate.get(1),"disabled");
    }

    /**
     * close modal tambah biaya
     * @throws InterruptedException
     */
    public void closeModalTambahBiaya() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(closeModalTambahBiayaButton);
    }

    /**
     * check if modal tambah biaya is diplayed or not
     * @return true if displayed, false if not displayed
     * @throws InterruptedException
     */
    public boolean isModalTambahBiayaClosed() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementDisplayed(modalTambahBiaya);
    }

    /**
     * get Nama Biaya Text in form tambah biaya
     * @return String nama biaya
     * @throws InterruptedException
     */
    public String getNamaBiayaText() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(namaBiayaText);
    }

    /**
     * get periode awal value
     * @return string periode awal value
     * @throws InterruptedException
     */
    public String getPeriodeAwalValue() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(periodeDate.get(0),"value");
    }

    /**
     * get durasi biaya value
     * @return string durasi biaya
     * @throws InterruptedException
     */
    public String getDurasiBiayaValue() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(durasiBiayaTxt,"value");
    }

    /**
     * get jumlah biaya value
     * @return jumlah biaya value
     * @throws InterruptedException
     */
    public String getJumlahBiayaValue() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(jumlahBiayaTxt,"value");
    }

    /**
     * close pop up tambah biaya using button kembali
     * @throws InterruptedException
     */
    public void kembaliModalTambahBiaya() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(kembaliModalTambahBiayaButton);
    }

    //---end of create biaya tambahan---//

    //---required field biaya tambahan---//

    /**
     * Get String Nama Biaya error message
     * @return String Nama Biaya error message
     */
    public String getNamaBiayaErrMsg() {
        return selenium.getText(namaBiayaErrMsg);
    }

    /**
     * Get String Periode Awal error message
     * @return String Periode Awal error message
     */
    public String getPeriodeAwalErrMsg() {
        return selenium.getText(periodeAwalErrMsg);
    }

    /**
     * Get String Periode Akhir error message
     * @return String Periode Akhir error message
     */
    public String getPeriodeAkhirErrMsg() {
        return selenium.getText(periodeAkhirErrMsg);
    }

    /**
     * Get String Jumlah Biaya error message
     * @return String Jumlah Biaya error message
     */
    public String getJumlahBiayaErrMsg() {
        return selenium.getText(jumlahBiayaErrMsg);
    }

    /**
     * Click Action Edit Button in 1st row
     * @throws InterruptedException
     */
    public void clickEditButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(actionButton.get(0));
    }
    //---end of required field biaya tambahan---//
}
