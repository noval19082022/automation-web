package pageobjects.ownersini;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class NavbarPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public NavbarPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@class='app-header__menu-item-link profile-menu']")
    private WebElement profileButton;

    @FindBy(xpath = "//*[@class='bg-c-link bg-c-link--high-naked']")
    private WebElement kembaliKeMamikosButton;

    /**
     * Get URL
     */
    public String getURL() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getURL();
    }

    /**
     * Click profile
     * @throws InterruptedException
     */
    public void clickProfile() throws InterruptedException {
        selenium.waitTillElementIsClickable(profileButton);
        selenium.clickOn(profileButton);
    }

    /**
     * Click button kembali ke mamikos in Profile
     * @throws InterruptedException
     */
    public void clickKembaliKeMamikos() throws InterruptedException {
        selenium.waitTillElementIsClickable(kembaliKeMamikosButton);
        selenium.clickOn(kembaliKeMamikosButton);
    }
}
