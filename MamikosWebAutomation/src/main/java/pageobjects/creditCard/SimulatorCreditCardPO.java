package pageobjects.creditCard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

public class SimulatorCreditCardPO {


    WebDriver driver;
    SeleniumHelpers selenium;

    public SimulatorCreditCardPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    private String PAYMENT="src/test/resources/testdata/mamikos/payment.properties";
    private String pass = JavaHelpers.getPropertyValue(PAYMENT,"password");


    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "input.input-field")
    private WebElement passwordTextBox;

    @FindBy(css = "#universalInvoiceContainer .iframe-style")
    private WebElement iframeAInvoiceContainer;

    @FindBy(css = "#Cardinal-CCA-IFrame")
    private WebElement iframeInputOtp;

    @FindBy(xpath = "//*[contains(text(),'Amount')]//following::label[1]")
    private WebElement paymentAmountLabel;

    @FindBy(xpath = "//*[@id=\"content\"]/div[2]/form[1]/input[2]")
    private WebElement paymentButton;

    @FindBy(css = ".bg-c-text--heading-1")
    private WebElement creditCardPaymentMessage;

    /**
     * Enter password credit card
     */
    public void enterPassword() throws InterruptedException{
        selenium.hardWait(5);
        System.out.println(pass);
        selenium.switchToIFrame(iframeAInvoiceContainer);
        selenium.hardWait(5);
        selenium.switchToIFrame(iframeInputOtp);
        selenium.enterText(passwordTextBox, pass, true);
        clickOnPaymentButtonCreditCard();
        driver.switchTo().defaultContent();
    }

    /**
     * Get total amount
     *@return  total amount
     */
    public String getTotalAmountLabel() throws InterruptedException{
        selenium.getWindowHandle();
        return selenium.getText(paymentAmountLabel);
    }

    /**
     * Click payment button
     *@throws InterruptedException
     */
    public void clickOnPaymentButtonCreditCard() throws InterruptedException {
        selenium.clickOn(paymentButton);
    }

    /**
     * Get message payment confirmation
     *@return message confirmation
     */
    public String getPaymentMessage() throws InterruptedException{
        selenium.hardWait(5);
        return selenium.getText(creditCardPaymentMessage);
    }
}
