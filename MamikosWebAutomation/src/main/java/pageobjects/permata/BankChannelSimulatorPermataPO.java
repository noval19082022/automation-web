package pageobjects.permata;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BankChannelSimulatorPermataPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public BankChannelSimulatorPermataPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "inputMerchantId")
    private WebElement billingNumberTextBox;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement searchVirtualAccountButtonPermata;

    @FindBy(xpath = "//*[text()='Total Transaksi']/following-sibling::div")
    private WebElement paymentAmountLabel;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement paymentButton;

    @FindBy(xpath = "//*[@class='alert alert-success']")
    private WebElement permataPaymentMessage;

    /**
     * Enter virtual account number bni
     *@param virtualAccountNumber is numeric generate by system
     */
    public void enterTextVirtualAccount(String virtualAccountNumber) {
        selenium.enterText(billingNumberTextBox, virtualAccountNumber, false);
    }

    /**
     * Click button for search data by virtual account number mandiri
     *@throws InterruptedException
     */
    public void clickOnSearchVirtualAccountPermata() throws InterruptedException {
        selenium.clickOn(searchVirtualAccountButtonPermata);
    }

    /**
     * Get total amount
     *@return  total amount
     */
    public String getTotalAmountLabel() {
        return selenium.getText(paymentAmountLabel);
    }

    /**
     * Click payment button
     *@throws InterruptedException
     */
    public void clickOnPaymentPermataButton() throws InterruptedException {
        selenium.clickOn(paymentButton);
    }

    /**
     * Get message payment confirmation
     *@return message confirmation
     */
    public String getPaymentMessage() {
        return selenium.getText(permataPaymentMessage);
    }
}
