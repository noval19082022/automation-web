package pageobjects.dana;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

public class SimulatorDanaPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SimulatorDanaPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    //PAYMENT
    private String PAYMENT="src/test/resources/testdata/mamikos/payment.properties";
    private String noHP = JavaHelpers.getPropertyValue(PAYMENT,"noHPDana");
    private String pin = JavaHelpers.getPropertyValue(PAYMENT,"pinDana");


    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//button[normalize-space()='Bayar langsung via DANA']")
    private WebElement bayarViaDanaButton;

    @FindBy(xpath = "//button[normalize-space()='Bayar langsung via LinkAja']")
    private WebElement bayarViaLinkAjaButton;

    @FindBy(xpath = "//button[@id='proceed-button']")
    private WebElement paymentProcess;

    @FindBy(xpath = "//input[1]")
    private WebElement phoneNumberTextBox;

    @FindBy(xpath = "//*[@class='password-focus']")
    private WebElement pinTextBox;

    @FindBy(xpath = "//*[@class='total-amount']")
    private WebElement totalAmountLabel;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement bayarButton;

    @FindBy(xpath = "//*[@class='btn-pay btn btn-primary']")
    private WebElement lanjutkanButton;

    @FindBy(xpath = "//*[@class='title is-5 has-text-centered']")
    private WebElement textBoxMessage;

    /**
     * click btn Bayar Via Dana
     */
    public void clickBayarViaDana() throws InterruptedException{
        selenium.hardWait(5);
        selenium.pageScrollInView(bayarViaDanaButton);
        selenium.clickOn(bayarViaDanaButton);
    }
    /**
     * click btn Bayar Via LinkAja
     */
    public void clickBayarViaLinkAja() throws InterruptedException{
        selenium.hardWait(5);
        selenium.pageScrollInView(bayarViaLinkAjaButton);
        selenium.clickOn(bayarViaLinkAjaButton);
    }
    /**
     * payment Xendit Dana
     */
    public void clickPaymentProcess() throws InterruptedException{
        selenium.switchToWindow(3);
        selenium.clickOn(paymentProcess);
        selenium.hardWait(5);
        selenium.refreshPage();
    }
    /**
     * Repayment Xendit Dana
     */
    public void clickRepaymentProcess() throws InterruptedException{
        selenium.switchToWindow(4);
        selenium.clickOn(paymentProcess);
        selenium.hardWait(10);
        selenium.refreshPage();
    }

    /**
     * input phone number dana and pin
     */
    public void inputPhoneNumberAndPin() throws InterruptedException{
        selenium.hardWait(5);
        selenium.switchToWindow(2);
        selenium.enterText(phoneNumberTextBox, noHP, true);
        selenium.clickOn(bayarButton);
        selenium.waitInCaseElementClickable(pinTextBox, 5);
        selenium.enterText(pinTextBox, pin, true);
    }

    /**
     * Get total amount
     *@return  total amount
     */
    public String getTotalAmountLabel() {
        return selenium.getText(totalAmountLabel);
    }

    /**
     * Click payment button
     *@throws InterruptedException
     */
    public void clickOnPaymentDanaButton() throws InterruptedException {
        selenium.clickOn(bayarButton);
        selenium.hardWait(3);
        selenium.clickOn(lanjutkanButton);
    }

    public void inputPhoneNumberAndPinRepayment() throws InterruptedException{
        selenium.hardWait(5);
        selenium.switchToWindow(3);
        selenium.enterText(phoneNumberTextBox, noHP, true);
        selenium.clickOn(bayarButton);
        selenium.hardWait(3);
        selenium.enterText(pinTextBox, pin, true);
    }

}
