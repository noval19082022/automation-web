package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;
import java.util.List;

public class SMSTemplatePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SMSTemplatePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(., 'Add Template')]")
    private WebElement addTemplateButton;

    @FindBy(name = "content")
    private WebElement smsTemplateContent;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement createButton;

    @FindBy(xpath = "//input[@value='Delete']")
    private List<WebElement> deleteButton;

    @FindBy(className = "btn-warning")
    private List<WebElement> editButton;

    @FindBy(className = "btn-primary")
    private WebElement saveButton;

    @FindBy(name = "period")
    private WebElement periodSelection;

    @FindBy(xpath = "//td[contains(., 'untuk automation')]")
    private WebElement smsTemplate;

    /**
     * Click on Add SMS Template Button
     * @throws InterruptedException
     */
    public void clickOnAddSMSTemplateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(addTemplateButton, 3);
        selenium.javascriptClickOn(addTemplateButton);
    }

    /**
     * Fill SMS Template Content
     * @param content input string that used to fill sms template content
     */
    public void fillSMSTemplateContent(String content){
        selenium.enterText(smsTemplateContent,content, true);
    }

    /**
     * Click on Create SMS Template Button
     * @throws InterruptedException
     */
    public void clickOnCreateSMSTemplateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(createButton, 3);
        selenium.clickOn(createButton);
    }

    /**
     * Click on Delete SMS Template Button
     * @throws InterruptedException
     */
    public void clickOnDeleteSMSTemplateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(deleteButton.get(2), 3);
        selenium.clickOn(deleteButton.get(2));
    }

    /**
     * Get Table Content Template
     * @param content input string that will search element text
     * @return string
     */
    public String getTableContentTemplate(String content){
        selenium.waitInCaseElementVisible(By.xpath("//td[contains(., '"+content+"')]"), 5);
        return selenium.getText(By.xpath("//td[contains(., '"+content+"')]"));
    }

    /**
     * Is Table Content Template Appeared?
     * @param content input string that will search element text
     * @return true or false
     */
    public Boolean isTableContentTemplateAppeared(String content){
        return selenium.waitInCaseElementVisible(By.xpath("//td[contains(., '"+content+"')]"), 3) != null;
    }

    /**
     * Click on Edit SMS Template Button
     * @throws InterruptedException
     */
    public void clickOnEditSMSTemplateButton() throws InterruptedException {
        selenium.click(editButton.get(2));
    }

    /**
     * Click Save Button on Edit SMS Template Section
     * @throws InterruptedException
     */
    public void clickOnSaveSMSTemplateButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Click on SMS Template Day Period Selection
     * @param period input string that will be used to match item selection
     * @throws InterruptedException
     */
    public void clickOnSMSTemplatePeriodSelection(String period) throws InterruptedException {
        selenium.clickOn(periodSelection);
        selenium.selectDropdownValueByText(periodSelection, period);
    }

    /**
     * Check SMS template Day -5 is present
     * @return true / false
     */
    public boolean isSMSTemplatePresent() {
        return selenium.waitInCaseElementVisible(smsTemplate, 10) != null;
    }

    /**
     * Set SMS Template
     * @throws InterruptedException
     */
    public void setSmsTemplate() throws InterruptedException {
        if (!isSMSTemplatePresent()) {
            selenium.clickOn(addTemplateButton);
            selenium.clickOn(periodSelection);
            selenium.selectDropdownValueByText(periodSelection, "-5");
            selenium.enterText(smsTemplateContent, "untuk automation", true);
            selenium.waitInCaseElementVisible(createButton, 5);
            selenium.clickOn(createButton);
        }
    }
}
