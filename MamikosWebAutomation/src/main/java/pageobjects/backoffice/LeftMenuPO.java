package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class LeftMenuPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public LeftMenuPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@href='/backoffice/contract/search']")
    private WebElement searchContractMenu;

    @FindBy(xpath = "//a[contains(.,'Invoice Admin Fee Discount')]")
    private WebElement invoiceAdminFeeDiscountMenu;

    @FindBy(css = "[href='/backoffice/owner']")
    private WebElement mamipayOwnerListMenu;

    @FindBy(css= "[href='/backoffice/invoice/search']")
    private WebElement searchInvoiceMenu;

    @FindBy(xpath = "//li[contains(., 'Billing Reminder Template')]")
    private WebElement billingReminderTemplateMenu;

    @FindBy(xpath = "//span[.='Paid Invoice List']")
    private WebElement paidInvoiceListMenu;

    @FindBy(xpath = "//li[contains(., 'Paid Invoice List')]//a[contains(., 'Disbursement')]")
    private WebElement disbursemenMenu;

    @FindBy(xpath = "//a[normalize-space()='Refund']")
    private WebElement refundMenu;

    @FindBy(xpath = "//h3[.='Daftar Invoice Refund']")
    private WebElement refundPage;

    @FindBy(css= "[href='/backoffice/transfer-deposit']")
    private WebElement transferDepositTenantMenu;

    @FindBy(xpath = "(//li[@class='menu-item '])[6]")
    private WebElement cpDisbursementMenu;

    @FindBy(xpath = "(//a[normalize-space()='Edit Deposit'])[1]")
    private WebElement editDepositButton;


    /**
     * Click on Admin Log Menu From Left Bar
     * @throws InterruptedException
     */
    public void clickOnSearchContractMenu() throws InterruptedException {
        selenium.clickOn(searchContractMenu);
        selenium.hardWait(3);
    }

    /**
     * Text edit deposit
     * @throws InterruptedException
     */
    public String userSeeEditDepositButton() throws InterruptedException {
        return selenium.getText(editDepositButton);
    }

    /**
     * Click on invoice admin discount fee From Left Bar
     * @throws InterruptedException
     */
    public void clickOnInvoiceAdminFeeDiscountMenu() throws InterruptedException {
        selenium.clickOn(invoiceAdminFeeDiscountMenu);
        selenium.hardWait(3);
    }

    /**
     * Click on Mamipay Owner List Menu From Left Bar
     * @throws InterruptedException
     */
    public void clickMamipayOwnerListMenu() throws InterruptedException {
        selenium.clickOn(mamipayOwnerListMenu);
    }

    /**
     * Click on search invoice menu on back office
     * @throws InterruptedException
     */
    public void clickOnSearchInvoiceMenu() throws InterruptedException {
        selenium.clickOn(searchInvoiceMenu);
    }

    /**
     * Click on sub menu of voucher discount menu
     * @throws InterruptedException
     */
    public void clickOnSubMenuOfVoucherDiscount(String subMenu) throws InterruptedException {
        String parentMenu = "/backoffice/voucher";

        selenium.clickOn(By.xpath("//a[@href='" + parentMenu + "']"));
        if (subMenu.equals("Mamikos Voucher")){
            selenium.clickOn(By.xpath("(//a[@href='" + parentMenu + "']/following-sibling::ul/li)[1]"));
        }else {
            selenium.clickOn(By.xpath("(//a[@href='" + parentMenu + "']/following-sibling::ul/li)[2]"));
        }
    }

    /**
     * Click on Billing Reminder Template Menu
     * @throws InterruptedException
     */
    public void clickOnBillingReminderTemplateMenu() throws InterruptedException {
        selenium.pageScrollInView(billingReminderTemplateMenu);
        selenium.waitInCaseElementVisible(billingReminderTemplateMenu, 3);
        selenium.clickOn(billingReminderTemplateMenu);
    }

    /**
     * Click on Sub Menu of Billing Reminder Template
     * @param menu input string for menu selection
     * @throws InterruptedException
     */
    public void clickOnSubMenuOfBillingReminderTemplateMenu(String menu) throws InterruptedException {
        selenium.clickOn(By.xpath("//ul[@class='treeview-menu']/li[contains(., '"+menu+"')]"));
    }

    /**
     * Go to disbursement page
     * @throws InterruptedException
     */
    public void goToDisbursementPage() throws InterruptedException {
        selenium.clickOn(paidInvoiceListMenu);
        selenium.clickOn(disbursemenMenu);
    }

    /**
     * Go to Paid Invoice List
     * @throws InterruptedException
     */
    public void goToPaidInvoiceList() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(paidInvoiceListMenu);
        selenium.clickOn(refundMenu);
    }

    /**
     * Go to Refund Page
     * @throws InterruptedException
     */
    public String goToRefundPage() throws InterruptedException {
        return selenium.getText(refundPage);
    }

    /**
     * Click on Transfer Deposit Tenant menu on back office
     * @throws InterruptedException
     */
    public void clickOnTransferDepositTenantMenu() throws InterruptedException {
        selenium.clickOn(transferDepositTenantMenu);
    }

    /**
     * Open CP Disbursement Menu
     */
    public void goToCPDisbursementPage() throws InterruptedException {
        selenium.waitInCaseElementClickable(cpDisbursementMenu, 5);
        selenium.clickOn(cpDisbursementMenu);
    }

    /**
     * click mamipay menu based on their name
     * @param menu menu name
     * @throws InterruptedException
     */
    public void clickMamipayMenu(String menu) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(By.xpath("//li[@class='menu-item ']/a[contains(text(),'"+menu+"')]"));
    }
}
