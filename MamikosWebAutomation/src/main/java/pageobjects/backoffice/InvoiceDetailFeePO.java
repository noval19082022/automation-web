package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.List;

public class InvoiceDetailFeePO {
    WebDriver driver;
    SeleniumHelpers selenium;
    JavaHelpers java;

    public InvoiceDetailFeePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);


        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".btn.btn-primary")
    private WebElement btnAddFee;

    @FindBy(css = "select#cost-type-select")
    private WebElement dropFeeType;

    @FindBy(css = "[name='cost_title']")
    private WebElement inpFeeName;

    @FindBy(css = "[name='cost_value']")
    private WebElement inpFeeAmount;

    @FindBy(css = "input.btn")
    private WebElement btnAddAdditionalFee;

    @FindBy(css = ".callout-success")
    private WebElement boxSuccesAddFee;

    @FindBy(css = ".callout-success")
    private WebElement boxSuccessAction;

    @FindBy(css=".box-title")
    private WebElement pageTitle;

    @FindBy(xpath = "//*[.='Biaya Tetap']/preceding-sibling::*")
    private WebElement txtOtherPrice;

    @FindBy(xpath = "//*[.='Biaya Tetap']/following-sibling::*[1]")
    private WebElement txtOtherPriceNumber;

    @FindBy(xpath = "//*[.='Basic Amount']/following-sibling::*//i")
    private WebElement editBasicAmount;

    @FindBy(xpath = "//*[.='Amount']/following-sibling::input")
    private WebElement inpBasicAmount;

    @FindBy(xpath = "//*[.='Basic Amount']/following-sibling::dd[1]")
    private WebElement txtBasicAmount;

    @FindBy(css = "select[name='cost_id']")
    private WebElement dropAddOnPrice;

    @FindBy(css = ".callout")
    private WebElement actionResult;

    private final By addOnsFeeTypeElement = By.xpath("//*[.='Add Ons']");

    /**
     * Click on button add fee on Invoice detail fee page
     * @throws InterruptedException
     */
    public void clickOnBtnAddFee() throws InterruptedException {
        selenium.clickOn(btnAddFee);
    }

    /**
     * Set dropdown value of fee type by text
     * @param feeType set with Biaya Lainnya, Biaya Tetap, Biaya Admin, Discount, Add On
     */
    public void setDropFeeType(String feeType) {
        selenium.selectDropdownValueByText(dropFeeType, feeType);
    }

    /**
     * Set additional fee name
     * @param feeName input with desired fee name example "Biaya Parkir"
     */
    public void setFeeName(String feeName) {
        selenium.enterText(inpFeeName, feeName, false);
    }

    /**
     * edit additional fee name
     * @param feeName input with desired fee name example "Biaya Parkir"
     */
    public void editFeeName(String feeName) {
        selenium.enterText(inpFeeName, feeName, true);
    }

    /**
     * Set additional fee input amount
     * @param feeAmount input with string number exp "100000"
     */
    public void setInpFeeAmount(String feeAmount) {
        selenium.enterText(inpFeeAmount, feeAmount, true);
    }

    /**
     * Click on button add fee on Invoice Addtional Fee Page
     * @throws InterruptedException
     */
    public void clickOnBtnAddAdditionalFee() throws InterruptedException {
        selenium.clickOn(btnAddAdditionalFee);
    }

    /**
     * Wait till box success add additional fee is visible, max wait is 20 second
     * @return boolean, visible true otherwise false
     */
    public boolean isSuccessBoxVisible() {
        return selenium.waitInCaseElementVisible(boxSuccesAddFee, 20) != null;
    }

    /**
     * Delete active other price
     * @param otherPriceName input with other price name
     * @throws InterruptedException
     */
    public void deleteAdditionalOtherPrice(String otherPriceName) throws InterruptedException {
        String otherPrice = "//*[.='"+otherPriceName+"']/following-sibling::*//*[@title='Delete Fee']";
        selenium.clickOn(driver.findElement(By.xpath(otherPrice)));
        selenium.waitTillElementIsVisible(boxSuccessAction);
    }

    /**
     * Get page title
     * @return string data type of page title
     */
    public String getPageTitle() {
        return selenium.getText(pageTitle);
    }

    /**
     * Check if invoice element is visible on invoice details page
     * @param invoiceEl input with element text on invoice details page. Example Invoice Number
     * @return visible true otherwise false
     */
    public boolean isInvoiceDetailsElementVisible(String invoiceEl) {
        String invoiceElelement = "//*[.='"+invoiceEl+"']/following-sibling::dd[1]";
        return selenium.waitInCaseElementVisible(By.xpath(invoiceElelement), 30) != null;
    }

    /**
     * Get other price name
     * @return string data type exampel "Parkir", "Listrik" etc
     */
    public String getOtherPriceName() {
        return selenium.getText(txtOtherPrice);
    }

    /**
     * Get invoice detail element value
     * @param invoiceEl input with element name that present on invoice detail
     * @return string data type
     */
    public String getInvoiceElementValue(String invoiceEl) {
        String invoiceElelement = "//*[.='"+invoiceEl+"']/following-sibling::dd[1]";
        return selenium.getText(driver.findElement(By.xpath(invoiceElelement)));
    }

    /**
     * Get other price's price number
     * @param invoiceEl input with other price Fee Type example "Admin", "Biaya Tetap" etc
     * @return Integer data type of other price's price number
     */
    public Integer getOtherPriceNumber(String invoiceEl) {
        String invoiceElelement = "//*[.='"+invoiceEl+"']/following-sibling::*[1]";
        return JavaHelpers.extractNumber(selenium.getText(driver.findElement(By.xpath(invoiceElelement))));
    }

    /**
     * Edit basic amount on admin invoice
     * @param newBasicAmountPrice input with desired price
     * @throws InterruptedException
     */
    public void editBasicAmount(Integer newBasicAmountPrice) throws InterruptedException {
        selenium.clickOn(editBasicAmount);
        selenium.hardWait(3);
        selenium.enterText(inpBasicAmount, newBasicAmountPrice.toString(), true);
        selenium.hardWait(60);
        selenium.javascriptClickOn(btnAddFee);
    }

    /**
     * Get basic amount as text
     * @return string data type
     */
    public String getBasicAmountText() {
        return selenium.getText(txtBasicAmount);
    }

    /**
     * Set drop add ons price by it index
     * @param index desirable index start with "1" input with number string data type
     */
    public void setDropAddOn(String index) {
        selenium.selectDropdownValueByIndex(dropAddOnPrice, Integer.parseInt(index));
    }

    /**
     * Set drop add ons price by it index
     * @param dropdownTextValue inner text of html target element
     */
    public void setDropAddOnByText(String dropdownTextValue) {
        selenium.selectDropdownValueByText(dropAddOnPrice, dropdownTextValue);
    }

    /**
     * Get complete add additional fee text
     * @return string data type
     */
    public String getCompleteAddAdditionalPriceText() {
        return selenium.getText(boxSuccesAddFee);
    }

    /**
     * Get list element of Adds Ons Fee type
     * @return List WebElement of add ons fee type
     */
    public List<WebElement> getAddsOnFeeTypeElementLenght() {
        return selenium.waitTillAllElementsAreLocated(addOnsFeeTypeElement);
    }

    /**
     * Add custom add ons to the invoice
     * @param addOnsName name of add ons
     * @param price price target
     * @throws InterruptedException
     */
    public void addCustomAddOns(String addOnsName, String price) throws InterruptedException {
        clickOnBtnAddFee();
        setDropFeeType("Add On");
        setFeeName(addOnsName);
        setInpFeeAmount(price);
        clickOnBtnAddAdditionalFee();
        selenium.waitInCaseElementVisible(actionResult, 30);
    }

    /**
     * Get action result message, success or fail message
     * @return string data type, succes or fail message
     */
    public String getActionResultMessage() {
        return selenium.getText(actionResult);
    }

    public void clickOnEditButton(String additionalPriceType) throws InterruptedException {
        String editElement = "//*[.='"+additionalPriceType+"']/following-sibling::td//i[@class='fa fa-pencil']";
        selenium.click(driver.findElement(By.xpath(editElement)));
    }

    /**
     * Wait for callout action to be appeared
     */
    public void waitCalloutActionResult() {
        selenium.waitInCaseElementVisible(actionResult, 30);
    }

    /**
     *
     * @param priceName
     * @return string data type
     */
    public boolean isPriceWithNameVisible(String priceName) {
        String priceEl = "//tr/td[.='"+priceName+"']";
        return selenium.waitInCaseElementVisible(By.xpath(priceEl), 20) != null;
    }

    /**
     * Get additional price number as text
     * @param priceName input with price name in the list
     * @return string data type of price number
     */
    public String getAdditionalPriceNumber(String priceName) {
        String priceEl = "//*[.='"+priceName+"']/following-sibling::td[2]";
        return selenium.getText(By.xpath(priceEl));
    }

    /**
     * Get additional price number as text
     * @param priceName input with price name in the list
     * @return string data type of price number
     */
    public String getAdditionalPriceNumberContractFlow(String priceName) {
        String priceEl = "//*[.='"+priceName+"']/parent::*/following-sibling::*";
        return selenium.getText(By.xpath(priceEl));
    }

    /**
     *
     * @param priceName
     * @return string data type
     */
    public boolean isPriceWithNameVisibleContractFlow(String priceName) {
        String priceEl = "//*[.='"+priceName+"']";
        return selenium.waitInCaseElementVisible(By.xpath(priceEl), 20) != null;
    }
}
