package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class PNTemplatePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public PNTemplatePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@value='Delete']")
    private List<WebElement> deleteButton;

    @FindBy(xpath = "//a[contains(., 'Add Template')]")
    private WebElement addTemplateButton;

    @FindBy(name = "period")
    private WebElement periodSelection;

    @FindBy(name = "title")
    private WebElement titleInputText;

    @FindBy(name = "content")
    private WebElement contentInputText;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement createButton;

    @FindBy(className = "btn-warning")
    private List<WebElement> editButton;

    @FindBy(className = "btn-primary")
    private WebElement saveButton;

    @FindBy(className = "callout-danger")
    private WebElement calloutError;

    @FindBy(xpath = "//td[contains(., 'untuk automation')]")
    private WebElement pnTemplate;

    /**
     * Click on Delete PN Template Button
     * @throws InterruptedException
     */
    public void clickOnDeletePNTemplateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(deleteButton.get(2), 5);
        selenium.clickOn(deleteButton.get(2));
    }

    /**
     * Click on Add PN Template Button
     * @throws InterruptedException
     */
    public void clickOnAddPNTemplateButton() throws InterruptedException {
        selenium.clickOn(addTemplateButton);
    }

    /**
     * Click on PN Template Day Period Selection and Select day by text
     * @param day input string that will be used to match item selection
     * @throws InterruptedException
     */
    public void clickOnPNTemplateDayPeriodSelection(String day) throws InterruptedException {
        selenium.clickOn(periodSelection);
        selenium.selectDropdownValueByText(periodSelection, day);
    }

    /**
     * Fill PN Template Title
     * @param title input string that will be used to fill PN Template Title
     */
    public void fillPNTemplateTitle(String title){
        selenium.enterText(titleInputText, title, true);
    }

    /**
     * Fill PN Template Content
     * @param content input string that will be used to fill PN Template Content
     */
    public void fillPNTemplateContent(String content){
        selenium.enterText(contentInputText, content, true);
    }

    /**
     * Click on Create PN Template Button
     * @throws InterruptedException
     */
    public void clickOnCreatePNTemplateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(createButton, 5);
        selenium.clickOn(createButton);
    }

    /**
     * Get Table Title Template
     * @param title input string that will search element text
     * @return string
     */
    public String getTableTitleTemplate(String title){
        selenium.waitInCaseElementVisible(By.xpath("//td[contains(., '"+title+"')]"), 3);
         return selenium.getText(By.xpath("//td[contains(., '"+title+"')]"));
    }

    /**
     * Get Table Content Template
     * @param content input string that will search element text
     * @return string
     */
    public String getTableContentTemplate(String content){
        selenium.waitInCaseElementVisible(By.xpath("//td[contains(., '"+content+"')]"), 3);
        return selenium.getText(By.xpath("//td[contains(., '"+content+"')]"));
    }

    /**
     * Click on Edit PN Template Button
     * @throws InterruptedException
     */
    public void clickOnEditPNTemplateButton() throws InterruptedException {
        selenium.clickOn(editButton.get(2));
    }

    /**
     * Click Save Button on Edit PN Template Section
     * @throws InterruptedException
     */
    public void clickOnSavePNTemplateButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Get Callout Error Text
     * @return String
     */
    public String getCalloutErrorText(){
        selenium.waitInCaseElementVisible(calloutError, 3);
        return selenium.getText(calloutError);
    }

    /**
     * Check PN template Day -1 is present
     * @return true / false
     */
    public boolean isPNTemplatePresent() {
        return selenium.waitInCaseElementVisible(pnTemplate, 10) != null;
    }

    /**
     * Set PN Template
     * @throws InterruptedException
     */
    public void setPnTemplate() throws InterruptedException {
        if (!isPNTemplatePresent()) {
            selenium.clickOn(addTemplateButton);
            selenium.clickOn(periodSelection);
            selenium.selectDropdownValueByText(periodSelection, "-1");
            selenium.enterText(titleInputText, "untuk automation", true);
            selenium.enterText(contentInputText, "untuk automation content", true);
            selenium.waitInCaseElementVisible(createButton, 5);
            selenium.clickOn(createButton);
        }
    }
}
