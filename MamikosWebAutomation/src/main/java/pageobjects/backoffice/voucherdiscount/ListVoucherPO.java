package pageobjects.backoffice.voucherdiscount;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ListVoucherPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ListVoucherPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//a[text()='Single Voucher']")
    private WebElement singleVoucherButton;

    @FindBy(xpath = "//a[text()='Mass Voucher']")
    private WebElement massVoucherButton;

    @FindBy(name = "campaign_voucher")
    private WebElement voucherEditText;

    @FindBy(xpath = "//button[text()='Search']")
    private WebElement searchButton;

    @FindBy(xpath = "//i[@class='fa fa-pencil']/parent::a")
    private WebElement updateIcon;

    @FindBy(xpath = "//span[@class='text-danger']")
    private WebElement voucherStatusIsActive;

    @FindBy(xpath = "//tbody//tr/td[5]")
    private WebElement rulesFirstRow;

    @FindBy(xpath = "//*[.='Add Single Voucher']")
    private WebElement btnAddSingleVoucher;

    @FindBy(css = "input[name='campaign_voucher']")
    private WebElement inpCampaignNamePrefix;

    @FindBy(xpath = "//*[.='Add Mass Voucher']")
    private WebElement btnAddMassVoucher;

    /**
     * Click on single voucher button
     * @throws InterruptedException
     */
    public void clickOnSingleVoucherButton() throws InterruptedException {
        selenium.clickOn(singleVoucherButton);
    }

    /**
     * Click on mass voucher button
     * @throws InterruptedException
     */
    public void clickOnMassVoucherButton() throws InterruptedException {
        selenium.clickOn(massVoucherButton);
    }

    /**
     * Enter voucher code
     * @param voucherCode
     */
    public void enterVoucherCode(String voucherCode) {
        selenium.enterText(voucherEditText, voucherCode, true);
    }

    /**
     * Click on search button
     * @throws InterruptedException
     */
    public void clickOnSearchButton() throws InterruptedException {
        selenium.javascriptClickOn(searchButton);
    }

    /**
     * Click on update icon
     * @throws InterruptedException
     */
    public void clickOnUpdateIcon() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(updateIcon);
        selenium.clickOn(updateIcon);
    }

    /**
     * Verify voucher status is active
     * @return boolean element status
     */
    public boolean statusIsNotActive() {
        return selenium.isElementDisplayed(voucherStatusIsActive);
    }

    /**
     * Get rules
     * @return String rules
     */
    public String getPaymentRules() {
        return selenium.getText(rulesFirstRow);
    }

    /**
     * Admin master/backoffice click on add single voucher button
     * @throws InterruptedException
     */
    public void clicksOnAddSingleVoucherButton() throws InterruptedException {
        selenium.clickOn(btnAddSingleVoucher);
    }

    /**
     * Input/enter prefix name or campaign name for filter
     * @param prefixCampaignName string data type of campaign/prefix target name
     */
    public void enterCampaignNameFilter(String prefixCampaignName) {
        selenium.enterText(inpCampaignNamePrefix, prefixCampaignName, false);
    }

    /**
     * Click update / pencil icon button based on it index
     * @param index from top to bottom start from 1
     * @throws InterruptedException
     */
    public void clickOnUpdateIconIndex(String index) throws InterruptedException {
        String updateEl = "(//i[@class='fa fa-pencil']/parent::a)["+index+"]";
        selenium.clickOn(By.xpath(updateEl));
    }

    /**
     * Click on voucher list on voucher prefix based on index
     * @param index number from top to bottom start with 1
     * @throws InterruptedException
     */
    public void clicksOnVoucherListIndex(String index) throws InterruptedException {
        String voucherListEl = "tr:nth-child("+index+") [data-original-title='Voucher List']";
        selenium.clickOn(By.cssSelector(voucherListEl));
    }

    /**
     * Get voucher list visible size
     * @return integer data type
     */
    public int getVoucherListVisible() {
        String voucherList = ".box tr .action";
        return selenium.waitTillAllElementsAreLocated(By.cssSelector(voucherList)).size();
    }

    /**
     * Get voucher status text
     * @param index index from top to bottom start with 1
     * @return strign data type
     */
    public String getVoucherListStatusIndex(String index) {
        String voucherStatusEl = ".box tr:nth-child("+index+") span";
        return selenium.getText(By.cssSelector(voucherStatusEl));
    }

    /**
     * Click on add mass voucher button
     * @throws InterruptedException
     */
    public void clicksOnAddMassVoucherButton() throws InterruptedException {
        selenium.clickOn(btnAddMassVoucher);
    }
}
