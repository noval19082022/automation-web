package pageobjects.backoffice.voucherdiscount;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.io.File;
import java.text.ParseException;
import java.util.*;

public class AddVoucherFormPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private JavaHelpers java = new JavaHelpers();

    public AddVoucherFormPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//input[@value='booking_with_dp']")
    private WebElement forFirstDPCheckBox;

    @FindBy(xpath = "//input[@value='pelunasan']")
    private WebElement forFirstSettlementCheckBox;

    @FindBy(xpath = "(//input[@value='booking'])[1]")
    private WebElement forFirstFullPaidCheckBox;

    @FindBy(xpath = "//input[@value='recurring']")
    private WebElement forRecurringCheckBox;

    @FindBy(id = "start-date")
    private WebElement startDateEditText;

    @FindBy(id = "end-date")
    private WebElement endDateEditText;

    @FindBy(name = "is_active")
    private WebElement activeCheckbox;

    @FindBy(xpath = "//button[contains(text(),'Single Voucher')]")
    private WebElement editSingleVoucherButton;

    @FindBy(xpath = "//button[contains(text(),'Mass Voucher')]")
    private WebElement editMassVoucherButton;

    @FindBy(name = "voucher_min_amount")
    private WebElement minimumTransactionEditText;

    @FindBy(xpath = "(//input[@placeholder='Search City...'])[1]")
    private WebElement applicableForCityEditText;

    @FindBys(@FindBy(xpath = "//span[@class='select2-selection__choice__remove']"))
    private List<WebElement> listCity;

    @FindBy(xpath = "(//textarea[@class='form-control input-email'])[1]")
    private WebElement emailApplicableForEditText;

    @FindBy(xpath = "(//textarea[@class='form-control input-email'])[2]")
    private WebElement emailNotApplicableForEditText;

    @FindBy(xpath = "(//select[@class='form-control select-profession'])[1]")
    private WebElement targetProfessionDropdown;

    @FindBy(xpath = "(//input[@placeholder='Search City...'])[2]")
    private WebElement NotApplicableForCityEditText;

    @FindBy(xpath = "//button[text()='Yes, Do It!']")
    private WebElement yesDoItButton;

    @FindBy(xpath = "//*[.='Cancel']/preceding-sibling::button[@type='submit']")
    private WebElement btnAddSingleVoucher;

    @FindBy(id = "campaignName")
    private WebElement inpCampaignName;

    @FindBy(css = "#campaignTeam")
    private WebElement selectCampaignTeam;

    @FindBy(css = "#campaignTeam [selected]")
    private WebElement selectedCampaignTeam;

    @FindBy(xpath = "//input[contains(@name, 'prefix[name]')]")
    private WebElement inpPrefixName;

    @FindBy(xpath = "//input[contains(@name, 'prefix[voucher_limit]')]")
    private WebElement inpTotalTarget;

    @FindBy(css = "#field_voucher_type")
    private WebElement dropSelectDiscountType;

    @FindBy(css = "#field_voucher_type [selected]")
    private WebElement selectedDiscountType;

    @FindBy(css = "input[name='voucher_amount']")
    private WebElement inpDiscountAmount;

    @FindBy(css = "input[name='voucher_min_amount']")
    private WebElement inpMinimumTransaction;

    @FindBy(css = "input[name='voucher_max_amount']")
    private WebElement inpMaximumDiscount;

    @FindBy(css = "input[name='limit']")
    private WebElement inpTotalEachQuota;

    @FindBy(css = "input[name='limit_daily']")
    private WebElement inpDailyEachQuota;

    @FindBy(css = "button.swal2-cancel")
    private WebElement buttonCancelUpdate;

    @FindBy(css = "textarea[name='applicable_group[email]']")
    private WebElement inpTargetEmail;

    @FindBy(css = "#inputCampaignImage")
    private WebElement inpCampignImage;

    @FindBy(xpath = "//img[contains(@src, 'small.jpg')]")
    private WebElement imageCampaignUploaded;

    @FindBy(css = "input[name='public_campaign[title]']")
    private WebElement inpCampaignTitle;

    @FindBy(css = ".note-editing-area p")
    private WebElement inpCampaignTermAndConditions;

    private By popUpTittle = By.xpath("//*[.='Are you sure?']");

    //mass voucher part
    @FindBy(css= "input[name='voucher_code']")
    private WebElement inpMassVoucherCode;

    @FindBy(css="input[name='applicable_group[email_csv]']")
    private WebElement inpCSVFile;

    //Applicable For
    @FindBy(xpath = "//*[.='Applicable For']/following-sibling::*//*[.='Room Type']/following-sibling::span//li[1]")
    private WebElement roomTypeSelectedTextFirstIndex;

    @FindBy(xpath = "//*[.='Applicable For']/following-sibling::*//*[.='Room Type']/following-sibling::span//ul")
    private WebElement roomTypeInputBox;

    @FindBy(xpath = "//*[.='Not Applicable For']/following-sibling::*//*[.='Room Type']/following-sibling::span//ul")
    private WebElement notApplicableForRoomType;

    By activeApplicableForRoomType = By.xpath("//*[.='Applicable For']/following-sibling::*//*[.='Room Type']/following-sibling::span//li[@title]");

    @FindBy(name = "min_contract_duration")
    private WebElement dropDownMinimumContractPeriod;

    @FindBy(xpath = "(//input[@placeholder='Search Kost by name...'])[1]")
    private WebElement applicableForKostEditText;

    @FindBy(xpath = "(//input[@placeholder='Search Kost by name...'])[2]")
    private WebElement NotApplicableForKostEditText;

    /**
     * Click on active checkbox
     * @throws InterruptedException
     */
    public void clickOnActiveCheckbox() throws InterruptedException {
        selenium.pageScrollInView(activeCheckbox);
        selenium.clickOn(activeCheckbox);
    }

    /**
     * Click on edit single voucher button
     * @throws InterruptedException
     */
    public void clickOnEditSingleVoucherButton() throws InterruptedException {
        selenium.clickOn(editSingleVoucherButton);
    }

    /**
     * Click on edit single voucher button
     * @throws InterruptedException
     */
    public void clickOnEditMassVoucherButton() throws InterruptedException {
        selenium.clickOn(editMassVoucherButton);
    }

    /**
     * Click on Yes, Do It! button
     * @throws InterruptedException
     */
    public void clickOnYesDoItButton() throws InterruptedException {
        selenium.clickOn(yesDoItButton);
    }

    /**
     * Enter text start date
     * @throws InterruptedException
     */
    public void enterStartDate() throws InterruptedException, ParseException {
        String day = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", -2, 0, 0, 0);
        String date = java.getTimeStamp("YYYY/MM");
        selenium.clickOn(startDateEditText);
        System.out.println(date + "/" + day + " 00:01");
        selenium.enterText(startDateEditText, date + "/" + day + " 00:01", true);
    }

    /**
     * Enter text end date
     * @throws InterruptedException
     */
    public void enterEndDate(String endDate) throws InterruptedException, ParseException {
        if (endDate.equals("")){
            selenium.enterText(endDateEditText, "", true);
        }else {
            String day = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", -1, 0, 0, 0);
            String date = java.getTimeStamp("YYYY/MM");
            System.out.println(date + "/" + day + " 00:01");
            selenium.enterText(endDateEditText, date + "/" + day + " 00:01", true);
        }
    }

    /**
     * Input voucher start date based on current date, tommorrow, or desired date number
     * @param startDate input with tomorrow, today, or dateNumber
     * @throws ParseException
     * @throws InterruptedException
     */
    public void chooseStartDate(String startDate) throws ParseException, InterruptedException {
        String day;
        if (startDate.equalsIgnoreCase("today")) {
            day = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 0, 0, 0, 0);
        }
        else if (startDate.equalsIgnoreCase("tomorrow")) {
            day = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        }
        else {
            day = startDate;
        }
        String dateElement = "(//*[contains(@style, 'display:')]//*[contains(@class, 'xdsoft_date')]//div[.='"+day+"'])[1]";
        String date = java.getTimeStamp("YYYY/MM");
        selenium.clickOn(startDateEditText);
        selenium.clickOn(By.xpath(dateElement));
        selenium.clickOn(endDateEditText);
    }

    /**
     * Enter minimum transaction
     * @throws InterruptedException
     */
    public void enterMinimumTransaction(String minimumTransaction) {
        selenium.enterText(minimumTransactionEditText, minimumTransaction, true);
    }

    /**
     * Select city for voucher is applicable
     * @throws InterruptedException
     */
    public void selectVoucherAplicableOnCity(String city) throws InterruptedException {
        selenium.pageScrollInView(applicableForCityEditText);
        selenium.hardWait(1);
        int count = listCity.size();
        if (count>0){
            for (int i=0 ; i<count; i++){
                selenium.clickOn(By.xpath("(//span[@class='select2-selection__choice__remove'])[" + i+1 + "]"));
            }
        }
        selenium.enterText(applicableForCityEditText, city, true);
        selenium.clickOn(By.xpath("//li[@class='select2-results__option select2-results__option--highlighted'][text()='" + city + "']"));
    }

    /**
     * Select city for voucher is not applicable
     * @throws InterruptedException
     */
    public void selectVoucherNotAplicableOnCity(String city) throws InterruptedException {
        selenium.hardWait(1);
        int count = listCity.size();
        if (count>0){
            for (int i=0 ; i<count; i++){
                selenium.clickOn(By.xpath("(//span[@class='select2-selection__choice__remove'])[" + i+1 + "]"));
            }
        }
        selenium.pageScrollInView(NotApplicableForCityEditText);
        selenium.enterText(NotApplicableForCityEditText, city, true);
        selenium.clickOn(By.xpath("//li[@class='select2-results__option select2-results__option--highlighted'][text()='" + city + "']"));
    }

    /**
     * Select checkbox for first settlement
     * @throws InterruptedException
     */
    public void selectForFirstSettlementPaymentRules() throws InterruptedException {
        selenium.pageScrollInView(forFirstSettlementCheckBox);
        selenium.hardWait(1);
        selenium.clickOn(forFirstSettlementCheckBox);
    }

    /**
     * Select checkbox for first DP
     * @throws InterruptedException
     */
    public void selectForFirstDPPaymentRules() throws InterruptedException {
        selenium.pageScrollInView(forFirstDPCheckBox);
        selenium.hardWait(1);
        selenium.clickOn(forFirstDPCheckBox);
    }

    /**
     * Select checkbox for first full Paid
     * @throws InterruptedException
     */
    public void selectForFullPaidPaymentRules() throws InterruptedException {
        selenium.pageScrollInView(forFirstFullPaidCheckBox);
        selenium.hardWait(1);
        selenium.clickOn(forFirstFullPaidCheckBox);
    }

    /**
     * Select checkbox for recurring
     * @throws InterruptedException
     */
    public void selectForRecurringPaymentRules() throws InterruptedException {
        selenium.pageScrollInView(forRecurringCheckBox);
        selenium.hardWait(1);
        selenium.clickOn(forRecurringCheckBox);
    }

    /**
     * Enter email tenant
     * @param email is email tenant
     */
    public void enterTenantEmailApplicableFor(String email) {
        selenium.pageScrollInView(emailApplicableForEditText);
        selenium.enterText(emailApplicableForEditText, email, true);
    }

    /**
     * Enter email tenant
     * @param email is email tenant
     */
    public void enterTenantEmailNotApplicableFor(String email) {
        selenium.pageScrollInView(emailNotApplicableForEditText);
        selenium.enterText(emailNotApplicableForEditText, email, true);
    }

    /**
     * Selectt target profession
     * @param profession is profession tenant
     */
    public void selectProfessionTarget(String profession) {
        selenium.pageScrollInView(targetProfessionDropdown);
        selenium.selectDropdownValueByText(targetProfessionDropdown, profession);
    }

    /**
     * Clicks on add single voucher button
     * @throws InterruptedException
     */
    public void clicksOnAddSingleVoucherButton() {
        selenium.javascriptClickOn(btnAddSingleVoucher);
    }

    /**
     * Input campaign name on voucher form
     * @param campaignName desired name for voucher campaign
     */
    public void inputCampaignName(String campaignName) {
        selenium.enterText(inpCampaignName, campaignName, false);
    }

    /**
     * Select dropdown campaign team
     * @param campaignTeam BUSINESS, HR, MARKETING, OTHER
     */
    public void selectCampaignTeam(String campaignTeam) throws InterruptedException {
        selenium.selectDropdownValueByText(selectCampaignTeam, campaignTeam);
        selenium.hardWait(2);
    }

    /**
     * Enter desired prefix name for single voucher
     * Must be in capital letter
     * @param prefix desired prefix name input with string data type
     */
    public void enterVoucherPrefix(String prefix) {
        selenium.enterText(inpPrefixName, prefix, false);
    }

    /**
     * Enter voucher total target
     * @param voucherTarget string data type, input with number
     */
    public void enterVoucherTotalTarget(String voucherTarget) {
        selenium.enterText(inpTotalTarget, voucherTarget, false);
    }

    /**
     * Tick on payment rules based on it value
     * @param value payment rules element value booking_with_dp, booking, pelunasan, tenant
     */
    public void tickOnPaymentRules(String value) throws InterruptedException {
        String paymentRulesEl = ".form-group-payment-rules input[value='"+value+"']";
        selenium.clickOn(By.cssSelector(paymentRulesEl));
        selenium.hardWait(1);
    }

    /**
     * Tick on payment rules based on it value
     * @param value payment rules element value booking_with_dp, booking, pelunasan, tenant
     */
    public void tickOnContractRules(String value) throws InterruptedException {
        String paymentRulesEl = "//*[.='Contract Rules*']/following-sibling::*//*[contains(@value, '"+value+"')]";
        selenium.clickOn(By.xpath(paymentRulesEl));
        selenium.hardWait(1);
    }

    /**
     * Select discount type dropdown
     * @param discountType input with inner text Fixed Amount, Percentage
     */
    public void selectDiscountType(String discountType) {
        selenium.selectDropdownValueByText(dropSelectDiscountType, discountType);
    }

    /**
     * Input discount amount
     * @param discountAmount discount amount percentage, or exact value
     */
    public void inputDiscountAmount(String discountAmount) {
        selenium.enterText(inpDiscountAmount, discountAmount, false);
    }

    /**
     * Input minimum transaction for voucher
     * @param minimumTransaction input with minimum transaction price
     */
    public void inputMinimumTransaction(String minimumTransaction) {
        selenium.enterText(inpMinimumTransaction, minimumTransaction, false);
    }


    /**
     * Tick on important rules checkbox
     * @param importantRule set with name html attribute value is_active, is_testing
     *                      inspect the element for more available name value
     * @throws InterruptedException
     */
    public void tickOnImportantRules(String importantRule) throws InterruptedException {
        String importantRulesEl = "input[name='"+importantRule+"']";
        selenium.clickOn(By.cssSelector(importantRulesEl));
        selenium.hardWait(2);
    }

    /**
     * input maximum discount for voucher usage
     * @param maximumDiscountVoucher input with number string data type
     */
    public void inputMaximumAmount(String maximumDiscountVoucher) {
        selenium.enterText(inpMaximumDiscount, maximumDiscountVoucher, false);
    }

    /**
     * Input total each quota
     * @param totalEachQuota limit of voucher quota per voucher
     */
    public void inputTotalEachQuota(String totalEachQuota) throws InterruptedException {
        selenium.enterText(inpTotalEachQuota, totalEachQuota, false);
        selenium.hardWait(2);
    }

    /**
     * Input daily each quaota,
     * @param dailyEachQuota input with number, daily quota pervoucher
     */
    public void inputDailyEachQuota(String dailyEachQuota) {
        selenium.enterText(inpDailyEachQuota, dailyEachQuota, true);
    }

    /**
     * Get campaign name from input campaign name value
     * @return string data type of campaign name
     */
    public String getCampaignNameText() {
        return selenium.getElementAttributeValue(inpCampaignName, "value");
    }

    /**
     * Get campaign start date only, no hour or minute
     * @return String data type
     */
    public String getCampaignStartDate() {
        return selenium.getElementAttributeValue(startDateEditText, "value");
    }

    /**
     * Get selected campaign text
     * @return string data type
     */
    public String getSelectedCampaignTeamText() {
        return selenium.getText(selectedCampaignTeam);
    }

    /**
     * Get input value of prefix voucher name
     * @return String data type
     */
    public String getVoucherPrefixText() {
        return selenium.getElementAttributeValue(inpPrefixName, "value");
    }

    /**
     * Get input value of voucher code name
     * @return string data type
     */
    public String getVoucherCode() {
        return selenium.getElementAttributeValue(inpMassVoucherCode, "value");
    }

    /**
     * Get input value of total target
     * @return String data type
     */
    public String getVoucerTotalTargetText() {
        return selenium.getElementAttributeValue(inpTotalTarget, "value");
    }

    /**
     * Check in case payment rule is ticked
     * @param paymentRule set with payment rule value
     * @return boolean, element visible is true otherwise false
     */
    public boolean isPaymentRulesTicked(String paymentRule) {
        String paymentRulesTicked = ".form-group-payment-rules input[value='"+paymentRule+"']";
        return driver.findElement(By.cssSelector(paymentRulesTicked)).isSelected();
    }

    /**
     * Check in case contract rule is ticked
     * @param contractRule set with contract rule value
     * @return boolean, element visible is true otherwise false
     */
    public boolean isContractRulesTicked(String contractRule) {
        String contractRuleTicked = "//*[.='Contract Rules*']/following-sibling::*//*[contains(@value, '"+contractRule+"')]";
        return driver.findElement(By.xpath(contractRuleTicked)).isSelected();
    }

    /**
     * Get selected discount type
     * @return string data type
     */
    public String getDiscountTypeText() {
        return selenium.getText(selectedDiscountType);
    }

    /**
     * Get discount amount input value
     * @return string data type
     */
    public String getDiscountAmountText() {
        return selenium.getElementAttributeValue(inpDiscountAmount, "value");
    }

    /**
     * Get total each quota input value
     * @return string data type
     */
    public String getEachQuotaText() {
        return selenium.getElementAttributeValue(inpTotalEachQuota, "value");
    }

    /**
     * Get daily each quota input value
     * @return string data type
     */
    public String getDailyEachQuota() {
        return selenium.getElementAttributeValue(inpDailyEachQuota, "value");
    }

    /**
     * Get maximum discount amount input value
     * @return string data type
     */
    public String getInputMaximumTransactionText() {
        return selenium.getElementAttributeValue(inpMaximumDiscount, "value");
    }

    /**
     * Get minimum transaction for discount input value
     * @return string data type
     */
    public String getInputMinimumTransactionText() {
        return selenium.getElementAttributeValue(minimumTransactionEditText, "value");
    }

    /**
     * Check if important rule is ticked
     * @param importantRule input with important rule value
     * @return ticked true otherwise false
     */
    public boolean isImportantRuleChecked(String importantRule) {
        String importantRuleEl = "//input[@name='"+importantRule+"']";
        return driver.findElement(By.xpath(importantRuleEl)).isSelected();
    }

    /**
     * Check if pop up title is present
     * @return visible true otherwise false
     */
    public boolean isVoucherUpdateConfirmation() {
        return selenium.waitInCaseElementVisible(popUpTittle, 5) != null;
    }

    /**
     * Click button cancel or no, go back button on voucher update confirmation pop-up
     * @throws InterruptedException
     */
    public void clicksOnNoGoBackButton() throws InterruptedException {
        selenium.clickOn(buttonCancelUpdate);
    }

    /**
     * Input voucher targeted email
     * @param email input with target email
     */
    public void inputVoucherTargetEmail(String email) {
        selenium.enterText(inpTargetEmail, email+",", false);
    }

    /**
     * Upload campaign image
     */
    public void uploadCampaignImage() throws InterruptedException {
        String projectpath = System.getProperty("user.dir");
        String imagePath = "/src/main/resources/images/anime.jpeg";
        inpCampignImage.sendKeys(projectpath+imagePath);
        selenium.waitTillElementIsVisible(imageCampaignUploaded, 20);
    }

    /**
     * Input campaign title single voucher form
     * @param campaignTitle desired campaign title input with string data type
     */
    public void inputCampaignTitle(String campaignTitle) {
        selenium.enterText(inpCampaignTitle, campaignTitle, false);
    }

    /**
     * Input campaign term and condition for single voucher form
     * @param termAndCondition desired campaign term and condition input with string data type
     */
    public void inputCampaignTermAndConditions(String termAndCondition) {
        selenium.enterText(inpCampaignTermAndConditions, termAndCondition, false);
    }

    /**
     * Input desired mass voucher code
     * @param massCode mass voucher code input with string data type
     */
    public void inputMassVoucherCode(String massCode) {
        selenium.enterText(inpMassVoucherCode, massCode, true);
    }

    /**
     * Upload csv file for mass voucher
     * @throws InterruptedException
     */
    public void uploadMassVoucherCSVFile() throws InterruptedException {
        String projectPath = System.getProperty("user.dir");
        String csvPath = "/src/main/resources/file/massVoucherFile.csv";
        File file = new File(projectPath + csvPath);
        inpCSVFile.sendKeys(file.getAbsolutePath());
        selenium.hardWait(3);
    }

    /**
     * Get room type selected first index only
     * @return string data type of applicable for room type text
     */
    public String getApplicableForSetRoomTypeValue() {
        return selenium.getText(roomTypeSelectedTextFirstIndex);
    }

    /**
     * Select room type based on room type name
     * @param roomType input with available room type
     * @throws InterruptedException
     */
    public void applicableForSetRoomType(String roomType) throws InterruptedException {
        By el = By.xpath("//span[@class='select2-results']//*[.='"+roomType+"']");
        selenium.clickOn(roomTypeInputBox);
        selenium.hardWait(3);
        selenium.clickOn(el);
        selenium.hardWait(5);
    }

    /**
     * remove applicable for room type based on index set
     * @param index : index number for right to left start with 1
     * @throws InterruptedException
     */
    public void removeApplicableForRoomTypeIndex(String index) throws InterruptedException {
        By el = By.xpath("//*[.='Applicable For']/following-sibling::*//*[.='Room Type']/following-sibling::span//li["+index+"]/span");
        selenium.clickOn(el);
        selenium.clickOn(notApplicableForRoomType);
    }

    /**
     * get active room type in applicable for rule
     * @return List WebElement of room type
     */
    public List<WebElement> getActiveRoomTypeApplicableFor() {
        return selenium.waitTillAllElementsAreLocated(activeApplicableForRoomType);
    }

    /**
     * Set drowpdown minimum contract period
     * @param period Weekly, Monthly other available period on admin master voucher form
     */
    public void setDropdownMinimumContractPeriod(String period) {
        selenium.selectDropdownValueByVisibleText(dropDownMinimumContractPeriod, period);
    }

    /**
     * Select kost for voucher is applicable
     * @throws InterruptedException
     */
    public void selectVoucherAplicableOnKost(String kost) throws InterruptedException {
        selenium.pageScrollInView(applicableForKostEditText);
        selenium.hardWait(1);
        int count = listCity.size();
        if (count>0){
            for (int i=0 ; i<count; i++){
                selenium.clickOn(By.xpath("(//span[@class='select2-selection__choice__remove'])[" + i+1 + "]"));
            }
        }
        selenium.enterText(applicableForKostEditText, kost, true);
        selenium.clickOn(By.xpath("//li[@class='select2-results__option select2-results__option--highlighted'][text()='" + kost + "']"));
    }

    /**
     * Select kost for voucher is not applicable
     * @throws InterruptedException
     */
    public void selectVoucherNotAplicableOnKost(String kost) throws InterruptedException {
        selenium.pageScrollInView(applicableForKostEditText);
        selenium.hardWait(1);
        int count = listCity.size();
        if (count>0){
            for (int i=0 ; i<count; i++){
                selenium.clickOn(By.xpath("(//span[@class='select2-selection__choice__remove'])[" + i+1 + "]"));
            }
        }
        selenium.enterText(NotApplicableForKostEditText, kost, true);
        selenium.clickOn(By.xpath("//li[@class='select2-results__option select2-results__option--highlighted'][text()='" + kost + "']"));
    }
}
