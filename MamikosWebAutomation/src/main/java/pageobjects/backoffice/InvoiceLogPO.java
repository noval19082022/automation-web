package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class InvoiceLogPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public InvoiceLogPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);


        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//tr[5]//parent::td")
    public List<WebElement> pushNotifTableData;

    @FindBy(xpath = "//div[7]/table/thead/tr/th")
    public List<WebElement> tableHeadData;

    @FindBy(xpath = "//tr[2]//parent::td")
    public List<WebElement> whatsAppTableData;

    /**
     * Get Reminder Type Table Text
     * @param text input string that define text value
     * @return string
     */
    public String getReminderTypeTableText(String text){
        String el = "//th[contains(text(), '"+text+"')]";
        selenium.pageScrollInView(By.xpath(el));
        return selenium.getText(By.xpath(el));
    }

    /**
     * Get Billing Reminder Type Table Text
     * @param text input string that define text value
     * @return string
     */
    public String getBiliingReminderTypeTableValue(String text){
        String el = "//td[contains(text(), '"+text+"')]";
        selenium.pageScrollInView(By.xpath(el));
        return selenium.getText(By.xpath(el));
    }

}
