package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

public class DisbursementPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public DisbursementPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[.='Allow Disburse']")
    private WebElement allowDisburseTab;

    @FindBy(xpath = "//*[.='Search By' and contains(@class, 'filter-option-inner-inner')]")
    private WebElement searchByButton;

    @FindBy(css = "input[name='search_value']")
    private WebElement inpSearchValue;

    @FindBy(css = "input[value='Search']")
    private WebElement btnSearch;

    @FindBy(css = "tr:nth-child(1) button.disbursement-invoice__button-success")
    private WebElement btnTransferFirstIndex;

    @FindBy(xpath = "(//*[.='Total Disburse']/following-sibling::*[@id])[1]")
    private WebElement textKostPrice;

    @FindBy(xpath = "(//*[.='Biaya Layanan Penyewa']/parent::div/following-sibling::p)[1]")
    private WebElement textAdminPrice;

    @FindBy(xpath = "(//*[.='Total Disburse']/following-sibling::label)[1]")
    private WebElement textTotalDisbursement;

    @FindBy(xpath = "//a/*[.='Processed']")
    private WebElement tabProcessed;

    @FindBy(xpath = "//button[.='Detail']")
    private WebElement btnDetail;

    @FindBy(xpath = "//*[.='Harga Kos']/following-sibling::p")
    private WebElement textProcessedKostPrice;

    @FindBy(xpath = "(//*[.='Total Disburse']/following-sibling::*)[1]")
    private WebElement textProcessedTotalDisburse;

    @FindBy(css = ".fade.in button.close")
    private WebElement closeButtonActivePopUp;

    @FindBy(xpath = "//a/*[.='Paid']")
    private WebElement tabPaid;

    @FindBy(xpath = "(//button[contains(text(), 'Tandai Sudah Transfer')])[1]")
    private WebElement btnMarkAlreadyPaidFirstIndex;

    @FindBy(css = ".fade.in input[placeholder='Transaction Date']")
    private WebElement inputTransferDate;

    @FindBy(css = "div[style] .xdsoft_calendar .xdsoft_today")
    private WebElement dateTodayDate;

    @FindBy(css = ".fade.in input[type='submit']")
    private WebElement inpMarkAsTransferred;
    
    @FindBy(css = "tr:first-of-type a[data-id]")
    private WebElement actionEyesFirstIndexedTransferredTab;

    @FindBy(css = ".fade.in input[value='Transfer Sekarang']")
    private WebElement btnTransferNow;

    /**
     * Click on the allow disburse tab to open disbursement menu
     * @throws InterruptedException
     */
    public void clickOnAllowDisburseTab() throws InterruptedException {
        selenium.clickOn(allowDisburseTab);
    }

    /**
     * Search by filter tenant
     * @param searchBy input with dropdown list
     * @param byValue value desired based on searchBy
     * @throws InterruptedException
     */
    public void searchByDisburse(String searchBy, String byValue) throws InterruptedException {
        String dropdownValue = "//*[@role='option']/*[.='"+searchBy+"']";
        selenium.clickOn(searchByButton);
        selenium.hardWait(3);
        selenium.clickOn(driver.findElement(By.xpath(dropdownValue)));
        selenium.enterText(inpSearchValue, byValue, false);
        selenium.clickOn(btnSearch);
    }

    /**
     * Transfer button first index
     * @throws InterruptedException
     */
    public void clickOnTransferIndexOne() throws InterruptedException {
        selenium.clickOn(btnTransferFirstIndex);
    }

    /**
     * Get kost price by it text
     * @return kost price number integer data type
     */
    public int getKostPrice() {
        return  JavaHelpers.extractNumber(selenium.getText(textKostPrice));
    }

    /**
     * Get add on price by it name
     * @param addOnName add on price's name, String data type
     * @return kost
     */
    public int getAddOnPrice(String addOnName) {
        String addOnEl = "//*[.='"+addOnName+"']/parent::div/following-sibling::p";
        return JavaHelpers.extractNumber(selenium.getText(driver.findElement(By.xpath(addOnEl))));
    }

    /**
     * Get admin price on disbursement
     * @return admin price integer data type.
     */
    public int getAdminPrice() {
        return JavaHelpers.extractNumber(selenium.getText(textAdminPrice));
    }

    /**
     * Get total disbursement price
     * @return total disbursement price, int data type
     */
    public int getTotalDisbursementPrice() {
        return JavaHelpers.extractNumber(selenium.getText(textTotalDisbursement));
    }

    /**
     * Click on processed tab
     * @throws InterruptedException
     */
    public void clickOnProcessedTab() throws InterruptedException {
        selenium.clickOn(tabProcessed);
    }

    /**
     * Click on detail button
     * @throws InterruptedException
     */
    public void clicksOnDetailButtonProcessedTab() throws InterruptedException {
        selenium.clickOn(btnDetail);
    }

    /**
     * Get kost price on processed disbursement
     * @return string data type
     */
    public String getProcessedKostPrice() {
        return selenium.getText(textProcessedKostPrice);
    }

    /**
     * Get total disburse on processed disbursement
     * @return string data type
     */
    public String getProcessedTotalDisburse() {
        return selenium.getText(textProcessedTotalDisburse);
    }

    /**
     * Click on active close button on active pop up
     * @throws InterruptedException
     */
    public void clickOnActivePopUpCloseButton() throws InterruptedException {
        selenium.clickOn(closeButtonActivePopUp);
    }

    /**
     * Click on paid tab
     * @throws InterruptedException
     */
    public void clicksOnPaidTab() throws InterruptedException {
        selenium.clickOn(tabPaid);
    }

    /**
     * Click on mark already transfer button
     * @throws InterruptedException
     */
    public void clicksOnMarkAlreadyTransferButton() throws InterruptedException {
        selenium.clickOn(btnMarkAlreadyPaidFirstIndex);
    }

    /**
     * Mark today as transfer data and mark as transferred
     * @throws InterruptedException
     */
    public void markTransferredTodayDate() throws InterruptedException {
        selenium.clickOn(inputTransferDate);
        selenium.hardWait(2);
        selenium.clickOn(dateTodayDate);
        selenium.clickOn(inpMarkAsTransferred);
        selenium.hardWait(10);
    }

    /**
     * Click on action button first index / first eyes image on transferred tab
     * @throws InterruptedException
     */
    public void clickOnActionButtonFirstIndex() throws InterruptedException {
        selenium.clickOn(actionEyesFirstIndexedTransferredTab);
    }

    /**
     * Check if price with priceName set is visible
     * @param priceName name of price name to check
     * @return visible true otherwise false
     */
    public boolean isTransferredPriceVisible(String priceName) {
        String priceEl = "//*[.='"+priceName+"']";
        return selenium.waitInCaseElementVisible(By.xpath(priceEl), 5) != null;
    }

    /**
     * Click on btn transfer now
     * @throws InterruptedException
     */
    public void transferNowButton() throws InterruptedException {
        selenium.clickOn(btnTransferNow);
    }

    /**
     * get processed disbursement price
     * @param disbursementPrice price as integer
     * @return integer data type
     */
    public int getProcessedDisbursementPrice(String disbursementPrice) {
        String el = "(//*[.='"+disbursementPrice+"']/following-sibling::*)[1]";
        return JavaHelpers.extractNumber(selenium.getText(By.xpath(el)));
    }
}
