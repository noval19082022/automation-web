package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.text.ParseException;

public class AddNewContractPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private JavaHelpers java = new JavaHelpers();

    public AddNewContractPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /**
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath="//*[@class='pull-right']//*[@class='btn btn-primary']")
    private WebElement addContractButton;

    @FindBy(xpath="//*[@name='inputPhoneNumberOwner']")
    private WebElement nomorHPOwnerTextField;

    @FindBy(css="[onclick='checkingOwnerPhone()']")
    private WebElement cekHpOwnerButton;

    @FindBy(xpath="//*[@name='inputOwnerKost']")
    private WebElement kosListRadioButton;

    @FindBy(xpath="//*[@class='buttonNextTab1']")
    private WebElement nomorHandphonePenyewaTab;

    @FindBy(xpath="//*[@name='phoneNumberRenter']")
    private WebElement tenantPhoneNumberTextField;

    @FindBy(xpath="(//*[@class='col-md-12']//*[@class='btn btn-primary'])[1]")
    private WebElement cekHpTenantButton;

    @FindBy(xpath="//*[@class='buttonNextTab2']")
    private WebElement dataDiriPenyewaTab;

    @FindBy(xpath="//*[@class='swal2-confirm swal2-styled']")
    private WebElement tenantOKButton;

    @FindBy(css=".select2")
    private WebElement selectRoomNumberButton;

    @FindBy(xpath="(//*[@aria-label='Kamar Tersedia']//*[@class='select2-results__option select2-results__option--selectable'])[1]")
    private WebElement availableRoomSelect;

    @FindBy(id="inputDateBill")
    private WebElement dateBillButton;

    @FindBy(id="inputRentCount")
    private WebElement rentCountButton;

    @FindBy(id="inputRentDuration")
    private WebElement rentDurationButton;

    @FindBy(xpath="//*[@class='buttonNextTab3']")
    private WebElement tambahanBiayaTab;

    @FindBy(xpath="//*[@class='btn btn-primary buttonNextTab4']")
    private WebElement konfirmasiTab;

    @FindBy(xpath="//*[@class='btn btn-primary buttonNextTab5']")
    private WebElement konfirmasiSeleseiButton;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-md']")
    private WebElement searchContractButton;

    @FindBy(xpath = "//*[@class='custom-label  custom-label__success ']")
    private WebElement activeContractLabel;

    @FindBy(id = "inputIdentity")
    private WebElement uploadIdentityPhoto;

    @FindBy(id = "inputRenterPhotoKtp")
    private WebElement uploadRenterPhotoKtp;

    @FindBy(id = "inputStatus")
    private WebElement inputStatusField;

    @FindBy(id = "inputWorking")
    private WebElement inputPekerjaanField;

    /**
     * Click add contract button on Mamipay admin
     * @throws InterruptedException
     */
    public void clickAddContractButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(addContractButton);
        selenium.clickOn(addContractButton);
    }

    /**
     * Set field no HP owner on Mamipay admin
     */
    public void setTextOnNoHpOwner() {
        selenium.waitTillElementIsVisible(nomorHPOwnerTextField);
        selenium.enterText(nomorHPOwnerTextField,"085947715987",true); // need to improve no hp owner
    }

    /**
     * Set field no HP owner on Mamipay admin
     */
    public void setTextOnNoHpOwner(String phoneNumber) {
        selenium.waitTillElementIsVisible(nomorHPOwnerTextField);
        selenium.enterText(nomorHPOwnerTextField, phoneNumber ,true); // need to improve no hp owner
    }


    /**
     * Click button cek to validate no hp owner on Mamipay admin
     * @throws InterruptedException
     */
    public void clickOnCekOwnerButton() throws InterruptedException {
        selenium.clickOn(cekHpOwnerButton);
    }

    /**
     * Choose kos Tobelo Asri Village on Mamipay admin
     * @throws InterruptedException
     */
    public void selectOwnerKos() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitTillElementIsClickable(kosListRadioButton);
        selenium.hardWait(5);
        selenium.click(kosListRadioButton);
    }

    /**
     * Click lanjut button to go to nomor hp penyewa tab on Mamipay admin
     * @throws InterruptedException
     */
    public void clickOnLanjutkanToTenantPhoneNumberButton() throws InterruptedException {
        selenium.clickOn(nomorHandphonePenyewaTab);
    }

    /**
     * Set field nomor hp tenant on Mamipay admin
     * @param HP tenant phone number
     */
    public void setTextOnNoHpTenant(String HP) {
        selenium.waitTillElementIsVisible(tenantPhoneNumberTextField);
        selenium.enterText(tenantPhoneNumberTextField,HP,true);
    }

    /**
     * Click button cek to validate tenant nomor hp on Mamipay admin
     * @throws InterruptedException
     */
    public void clickOnCekTenantButton() throws InterruptedException {
        selenium.clickOn(cekHpTenantButton);
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(tenantOKButton);
        selenium.clickOn(tenantOKButton);
    }

    /**
     * Click lanjut button to go to data diri dan detail harga sewa tab on Mamipay admin
     * @throws InterruptedException
     */
    public void clickOnLanjutkanToDataDiriButton() throws InterruptedException {
        selenium.clickOn(dataDiriPenyewaTab);
    }

    /**
     * Pick room number on form Data diri and detail harga sewa on Mamipay admin
     * @throws InterruptedException
     */
    public void chooseRoomNumber() throws InterruptedException {
        selenium.clickOn(selectRoomNumberButton);
        selenium.hardWait(2);
        selenium.clickOn(availableRoomSelect);
    }

    /**
     * Pick the date bill on form Data diri and detail harga sewa on Mamipay admin
     * @throws InterruptedException
     */
    public void fillDateBill() throws InterruptedException, ParseException {
        selenium.clickOn(dateBillButton);
        String today = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "yyyy-MM-dd", 0, 0, 0, 0);
        selenium.javascriptSetValue(dateBillButton,today); //masih butuh di improve pake today()
        selenium.sendKeyEscape();
    }

    /**
     * Pick the Rent count "Bulanan" on form Data diri and detail harga sewa on Mamipay admin
     * @throws InterruptedException
     */
    public void chooseRentCount() throws InterruptedException {
        selenium.selectDropdownValueByText(rentCountButton,"Bulanan");
        selenium.hardWait(3);
    }

    /**
     * Pick the Rent duration "1 Bulan" on form Data diri and detail harga sewa on Mamipay admin
     * @throws InterruptedException
     */
    public void chooseRentDuration() throws InterruptedException {
        selenium.selectDropdownValueByText(rentDurationButton,"1 Bulan");
    }

    /**
     * Upload image to Foto KTP/Identitas Lain
     */
    public void uploadIdentityPhoto() throws InterruptedException {
        String projectpath = System.getProperty("user.dir");
        uploadIdentityPhoto.sendKeys(projectpath + "/src/main/resources/images/anime.jpeg");
        selenium.hardWait(5);
    }

    /**
     * Upload image to Foto penyewa dengan KTP
     */
    public void uploadRenterPhotoKtp() throws InterruptedException {
        String projectpath = System.getProperty("user.dir");
        uploadRenterPhotoKtp.sendKeys(projectpath + "/src/main/resources/images/anime.jpeg");
        selenium.hardWait(5);
    }

    /**
     * Click lanjut button to go to tambahan biaya tab on Mamipay admin
     * @throws InterruptedException
     */
    public void clickOnLanjutkanToTambahanBiayaButton() throws InterruptedException {
        selenium.clickOn(tambahanBiayaTab);
    }

    /**
     * Click lanjut button to go to konfirmasi tab on Mamipay admin
     * @throws InterruptedException
     */
    public void clickOnLanjutkanToKonfirmasiButton() throws InterruptedException {
        selenium.clickOn(konfirmasiTab);
    }

    /**
     * Click button selesei to submit form on Mamipay admin
     * @throws InterruptedException
     */
    public void clickOnSelesaiButton() throws InterruptedException {
        selenium.pageScrollInView(konfirmasiSeleseiButton);
        selenium.clickOn(konfirmasiSeleseiButton);
        selenium.hardWait(5);
    }

    /**
     * To identify there is contract that have active label
     * @return
     */
    public String getTenantActiveLabel() {
        return selenium.getText(activeContractLabel);
    }

    /**
     * Select Status
     *
     */
    public void selectStatus() {
        selenium.selectDropdownValueByText(inputStatusField,"Kawin");
    }

    /**
     * Select Pekerjaan
     *
     */
    public void selectPekerjaan() {
        selenium.selectDropdownValueByText(inputPekerjaanField,"Karyawan");
    }
}