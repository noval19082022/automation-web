package pageobjects.backoffice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class AdminEventPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AdminEventPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//span[.='Event']")
    private WebElement accessMenuEvent;

    @FindBy(css = "[href='https://jambu.kerupux.com/admin/event/99/edit'] > .fa")
    private WebElement setBanner1ToOrder1;

    @FindBy(css = "[href='https://jambu.kerupux.com/admin/event/120/edit'] > .fa")
    private WebElement setBanner2ToOrder1;

    @FindBy(css = "[href='https://jambu.kerupux.com/admin/event/123/edit'] > .fa")
    private WebElement setBanner3ToOrder1;

    @FindBy(css = "[name='is_owner']")
    private WebElement filterOwnerDropdown;

    @FindBy(css = "[name='is_owner'] > [value='1']")
    private WebElement ownerOnly;

    @FindBy(css = "[name='is_published']")
    private WebElement published;

    @FindBy(css = "[name='is_published'] > [value='1']")
    private WebElement publishedOnly;

    @FindBy(id = "buttonSearch")
    private WebElement searchButton;

    @FindBy(css = "[name='ordering']")
    private WebElement ordering;

    @FindBy(css = "[name='ordering'] > [value='1']")
    private WebElement chooseOrdering1;

    @FindBy(css = "[name='ordering'] > [value='2']")
    private WebElement chooseOrdering2;

    @FindBy(css = "[value='3']")
    private WebElement chooseOrdering3;

    @FindBy(xpath = "//h4[.='Event Targeting (Khusus Owner)']")
    private WebElement eventTargeting;

    @FindBy(css = ".btn-primary")
    private WebElement updateButton;

    @FindBy(css = ".carousel-indicator")
    private WebElement goToEventBanner;

    @FindBy(xpath = "//p[normalize-space()='Dari Mamikos']")
    private WebElement banner;

    @FindBy(xpath = "//b[normalize-space()='Success!']")
    private WebElement seeUpdateBanner;

    @FindBy(css = "img[src='https://static.kerupux.com/uploads/cache/data/user/2019-04-04/FjNrkUXG-540x720.jpg']")
    private WebElement clickBanner;

    @FindBy(xpath = "//p[contains(.,'Dari Mamikos')]")
    private WebElement redirectionBanner;


    /**
     * click menu event
     */

    public void accessMenuEvent() throws InterruptedException {
        selenium.pageScrollInView(accessMenuEvent);
        selenium.clickOn(accessMenuEvent);
    }

    /**
     * set banner 1 to order 1
     */

    public void setBanner1ToOrder1() throws InterruptedException {
        selenium.clickOn(filterOwnerDropdown);
        selenium.clickOn(ownerOnly);
        selenium.clickOn(published);
        selenium.clickOn(publishedOnly);
        selenium.clickOn(searchButton);
        selenium.hardWait(5);
        selenium.clickOn(setBanner1ToOrder1);
        selenium.clickOn(ordering);
        selenium.pageScrollInView(eventTargeting);
        selenium.clickOn(updateButton);
    }

    /**
     * set banner 2 to order 1
     */
    public void setBanner2ToOrder1() throws InterruptedException {
        selenium.clickOn(filterOwnerDropdown);
        selenium.clickOn(ownerOnly);
        selenium.clickOn(published);
        selenium.clickOn(publishedOnly);
        selenium.clickOn(searchButton);
        selenium.hardWait(5);
        selenium.clickOn(setBanner2ToOrder1);
        selenium.clickOn(ordering);
        selenium.clickOn(chooseOrdering1);
        selenium.pageScrollInView(eventTargeting);
        selenium.clickOn(updateButton);
    }

    /**
     * set banner 2 to order 2
     */
    public void setBanner2ToOrder2() throws InterruptedException {
        selenium.clickOn(filterOwnerDropdown);
        selenium.clickOn(ownerOnly);
        selenium.clickOn(published);
        selenium.clickOn(publishedOnly);
        selenium.clickOn(searchButton);
        selenium.hardWait(5);
        selenium.clickOn(setBanner2ToOrder1);
        selenium.clickOn(ordering);
        selenium.clickOn(chooseOrdering2);
        selenium.pageScrollInView(eventTargeting);
        selenium.clickOn(updateButton);
    }

    /**
     * set banner 3 to order 1
     */
    public void setBanner3ToOrder1() throws InterruptedException {
        selenium.clickOn(filterOwnerDropdown);
        selenium.clickOn(ownerOnly);
        selenium.clickOn(published);
        selenium.clickOn(publishedOnly);
        selenium.clickOn(searchButton);
        selenium.hardWait(5);
        selenium.clickOn(setBanner3ToOrder1);
        selenium.clickOn(ordering);
        selenium.clickOn(chooseOrdering1);
        selenium.pageScrollInView(eventTargeting);
        selenium.clickOn(updateButton);
    }

    /**
     * set banner 3 to order 3
     */
    public void setBanner3ToOrder3() throws InterruptedException {
        selenium.clickOn(filterOwnerDropdown);
        selenium.clickOn(ownerOnly);
        selenium.clickOn(published);
        selenium.clickOn(publishedOnly);
        selenium.clickOn(searchButton);
        selenium.hardWait(5);
        selenium.clickOn(setBanner3ToOrder1);
        selenium.clickOn(ordering);
        selenium.clickOn(chooseOrdering3);
        selenium.pageScrollInView(eventTargeting);
        selenium.clickOn(updateButton);
    }

    /**
     * click banner
     */
    public void clickOnBanner() throws InterruptedException {
        selenium.clickOn(clickBanner);
    }

    /**
     * go To Event Banner
     */
     public void goToEventBanner()  {
         selenium.pageScrollInView(goToEventBanner);
     }

    /**
     * check update Banner
     * @return
     */
    public String banner() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.getText(banner);
    }

    /**
     * success update Banner
     * @return
     */
    public String userSeeUpdateBanner() throws InterruptedException {
        selenium.hardWait(5);
        return selenium.getText(seeUpdateBanner);
    }

}
