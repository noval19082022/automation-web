package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class EmailTemplatePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public EmailTemplatePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@value='Delete']")
    private List<WebElement> deleteButton;

    @FindBy(xpath = "//a[contains(., 'Add Template')]")
    private WebElement addTemplateButton;

    @FindBy(name = "period")
    private WebElement periodSelection;

    @FindBy(name = "subject")
    private WebElement emailTemplateSubject;

    @FindBy(name = "content")
    private WebElement emailTemplateContent;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement createButton;

    @FindBy(className = "btn-warning")
    private List<WebElement> editButton;

    @FindBy(className = "btn-primary")
    private WebElement saveButton;

    @FindBy(xpath = "//td[contains(., 'untuk automation')]")
    private WebElement emailTemplate;

    /**
     * Click on Delete Email Template Button
     * @throws InterruptedException
     */
    public void clickOnDeleteEmailTemplateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(deleteButton.get(2), 5);
        selenium.clickOn(deleteButton.get(2));
    }

    /**
     * Click on Add Email Template Button
     * @throws InterruptedException
     */
    public void clickOnAddEmailTemplateButton() throws InterruptedException {
        selenium.clickOn(addTemplateButton);
    }

    /**
     * Click on Email Template Day Period Selection and Select day by text
     * @param day input string that will be used to match item selection
     * @throws InterruptedException
     */
    public void clickOnEmailTemplateDayPeriodSelection(String day) throws InterruptedException {
        selenium.clickOn(periodSelection);
        selenium.selectDropdownValueByText(periodSelection, day);
    }

    /**
     * Fill Email Template Subject
     * @param subject input string that will be used to fill Email Template Subject
     */
    public void fillEmailTemplateSubject(String subject){
        selenium.enterText(emailTemplateSubject, subject, true);
    }

    /**
     * Fill Email Template Content
     * @param content input string that will be used to fill Email Template Content
     */
    public void fillEmailTemplateContent(String content){
        selenium.enterText(emailTemplateContent, content, true);
    }

    /**
     * Click on Create Email Template Button
     * @throws InterruptedException
     */
    public void clickOnCreateEmailTemplateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(createButton, 5);
        selenium.clickOn(createButton);
    }

    /**
     * Get Table Subject Template
     * @param subject input string that will search element text
     * @return string
     */
    public String getTableSubjectTemplate(String subject){
        selenium.waitInCaseElementVisible(By.xpath("//td[contains(., '"+subject+"')]"), 5);
        return selenium.getText(By.xpath("//td[contains(., '"+subject+"')]"));
    }

    /**
     * Click on Edit Email Template Button
     * @throws InterruptedException
     */
    public void clickOnEditEmailTemplateButton() throws InterruptedException {
        selenium.click(editButton.get(2));
    }

    /**
     * Click Save Button on Edit Email Template Section
     * @throws InterruptedException
     */
    public void clickOnSaveEmailTemplateButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Check Email template Day -1 is present
     * @return true / false
     */
    public boolean isEmailTemplatePresent() {
        return selenium.waitInCaseElementVisible(emailTemplate, 10) != null;
    }

    /**
     * Set Email Template
     * @throws InterruptedException
     */
    public void setEmailTemplate() throws InterruptedException {
        if (!isEmailTemplatePresent()) {
            selenium.clickOn(addTemplateButton);
            selenium.clickOn(periodSelection);
            selenium.selectDropdownValueByText(periodSelection, "-1");
            selenium.enterText(emailTemplateSubject, "untuk automation", true);
            selenium.enterText(emailTemplateContent, "untuk automation content", true);
            selenium.waitInCaseElementVisible(createButton, 5);
            selenium.clickOn(createButton);
        }
    }
}
