package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class WhatsAppTemplatePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public WhatsAppTemplatePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@value='Delete']")
    private List<WebElement> deleteButton;

    @FindBy(xpath = "//a[contains(., 'Add Template')]")
    private WebElement addTemplateButton;

    @FindBy(name = "notification_whatsapp_template_id")
    private WebElement whatsAppTemplateSelect;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement createButton;

    @FindBy(className = "btn-warning")
    private List<WebElement> editButton;

    @FindBy(className = "btn-primary")
    private WebElement saveButton;

    @FindBy(name = "period")
    private WebElement periodSelection;

    @FindBy(xpath = "//td[contains(., 'recurringbooking_voucher_d_plus_1_update')]")
    private WebElement whatsappTemplate;

    /**
     * Click on Delete WhatsApp Template Button
     * @throws InterruptedException
     */
    public void clickOnDeleteWhatsAppTemplateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(deleteButton.get(2),3);
        selenium.clickOn(deleteButton.get(2));
    }

    /**
     * Click on Add WhatsApp Template Button
     * @throws InterruptedException
     */
    public void clickOnAddWhatsAppTemplateButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(addTemplateButton, 3);
        selenium.clickOn(addTemplateButton);
    }

    /**
     * Click on WhatsApp Template Selection and Select item by text
     * @param item input string that will be used to match item selection
     * @throws InterruptedException
     */
    public void clickOnWhatsAppTemplateSelection(String item) throws InterruptedException {
        selenium.clickOn(whatsAppTemplateSelect);
        selenium.selectDropdownValueByText(whatsAppTemplateSelect, item);
    }

    /**
     * Click on Create WhatsApp Template Button
     * @throws InterruptedException
     */
    public void clickOnCreateWhatsAppTemplateButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(createButton, 3);
        selenium.clickOn(createButton);
    }


    /**
     * Click on Edit WhatsApp Template Button
     * @throws InterruptedException
     */
    public void clickOnEditWhatsAppTemplateButton() throws InterruptedException {
        selenium.clickOn(editButton.get(2));
    }

    /**
     * Click Save Button on Edit WhatsApp Template Section
     * @throws InterruptedException
     */
    public void clickOnSaveWhatsAppTemplateButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Click on WA Template Day Period Selection
     * @param period input string that will be used to match item selection
     * @throws InterruptedException
     */
    public void clickOnWATemplateDayPeriodSelection(String period) throws InterruptedException {
        selenium.clickOn(periodSelection);
        selenium.selectDropdownValueByText(periodSelection, period);
    }

    /**
     * Check Whatsapp template Day -5 is present
     * @return true / false
     */
    public boolean isWhatsappTemplatePresent() {
        return selenium.waitInCaseElementVisible(whatsappTemplate, 10) != null;
    }

    /**
     * Set Whatsapp Template
     * @throws InterruptedException
     */
    public void setWhatsappTemplate() throws InterruptedException {
        if (!isWhatsappTemplatePresent()) {
            selenium.clickOn(addTemplateButton);
            selenium.clickOn(periodSelection);
            selenium.selectDropdownValueByText(periodSelection, "-5");
            selenium.clickOn(whatsAppTemplateSelect);
            selenium.selectDropdownValueByText(whatsAppTemplateSelect, "recurringbooking_voucher_d_plus_1_update");
            selenium.waitInCaseElementVisible(createButton, 5);
            selenium.clickOn(createButton);
        }
    }


}
