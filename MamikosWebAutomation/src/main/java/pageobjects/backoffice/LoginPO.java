package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class LoginPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public LoginPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[contains (@name, 'email')]")
    private WebElement emailTextBox;

    @FindBy(xpath = "//input[contains (@name, 'password')]")
    private WebElement passWordTextBox;

    @FindBy(xpath = "//button[normalize-space()='Sign me in']")
    private WebElement loginButton;

    @FindBy(xpath = "//input[@value='Login']")
    private WebElement loginPadangButton;

    @FindBy(css = ".btn")
    private WebElement loginBangkrupuxButton;


    /**
     * Enter Email and Password then click on Login Button
     * @param email enter Email
     * @param passWord enter Password
     * @throws InterruptedException
     */
    public void enterCredentialsAndClickOnLoginButton(String email, String passWord) throws InterruptedException {
        selenium.enterText(emailTextBox, email, false);
        selenium.enterText(passWordTextBox, passWord, false);
        if (selenium.isElementNotDisplayed(loginPadangButton)) {
            selenium.clickOn(loginPadangButton);
        }else {
            selenium.clickOn(loginButton);
        }

    }

    /**
     * Enter Email and Password then click on Login Button Bangkrupux
     * @param email enter Email
     * @param passWord enter Password
     * @throws InterruptedException
     */
    public void enterCredentialsAndClickOnLoginButtonBangkrupux(String email, String passWord) throws InterruptedException {
        selenium.enterText(emailTextBox, email, false);
        selenium.enterText(passWordTextBox, passWord, false);
        selenium.clickOn(loginBangkrupuxButton);
    }


}
