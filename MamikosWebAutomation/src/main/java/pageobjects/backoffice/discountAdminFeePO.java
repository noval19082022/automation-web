package pageobjects.backoffice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class discountAdminFeePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public discountAdminFeePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(xpath = "//a[.='Add Discount Settings']")
    private WebElement clickOnAddDiscountSetting;

    @FindBy(xpath = "(//input[@value='Delete'])[7]")
    private WebElement clickOnDeleteButton;

    @FindBy(xpath = "//input[@name='name']")
    private WebElement inputDiscountName;

    @FindBy(xpath = "//input[@name='name']")
    private WebElement selectKostLevel;

    @FindBy(xpath = "//option[.='APIK']")
    private WebElement chooseApik;

    @FindBy(xpath = "//input[@name='amount']")
    private WebElement inputDiscountAmountText;

    @FindBy(xpath = "//input[@name='is_booking_transaction']")
    private WebElement clickIsBooking;

    @FindBy(xpath = "//input[@name='is_nonbooking_transaction']")
    private WebElement clickIsNonBooking;

    @FindBy(xpath = "//input[@class='btn btn-primary btn-md']")
    private WebElement clickOnSaveButton;

    @FindBy(xpath = "(//a[@class='btn btn-xs btn-warning'][normalize-space()='Edit'])[7]")
    private WebElement clickOnEditButton;

    @FindBy(xpath = "//h3[@class='box-title']")
    private WebElement invoiceDiscountPageHeaderText;

    @FindBy(xpath = "//div[@class='callout callout-success']")
    private WebElement messageSuccessText;

    @FindBy(xpath = "//span[normalize-space()='GP2 Staging']")
    private WebElement seeDicountAdminFeeInTagihan;

    @FindBy(xpath = "//td[normalize-space()='GP2 Staging']")
    private WebElement seeDicountAdminFeeInDetailFee;

    @FindBy(xpath = "(//a[@class='btn btn-xs bg-maroon btn-flat'][normalize-space()='Detail Fee'])[1]")
    private WebElement clickOnDetailFee;


    /**
     * click on add discount setting
     * * @throws InterruptedException
     */

    public void userClickOnAddDiscountSetting() throws InterruptedException {
        selenium.clickOn(clickOnAddDiscountSetting);
    }

    /**
     * click on delete button
     * * @throws InterruptedException
     */

    public void userClickOnDeleteButton() throws InterruptedException {
        selenium.waitInCaseElementClickable(clickOnDeleteButton,5);
        selenium.javascriptClickOn(clickOnDeleteButton);
        selenium.acceptAlert();
    }

    /**
     * click on select kost level
     * * @throws InterruptedException
     */

    public void userSelectKostLevel() throws InterruptedException {
        selenium.clickOn(selectKostLevel);
        selenium.clickOn(chooseApik);
    }

    /**
     * click on checxbox
     * * @throws InterruptedException
     */

    public void userClickOnChecbox() throws InterruptedException {
        selenium.clickOn(clickIsBooking);
        selenium.clickOn(clickIsNonBooking);
    }

    /**
     * click on save button
     * * @throws InterruptedException
     */

    public void userClickOnSaveButton() throws InterruptedException {
        selenium.clickOn(clickOnSaveButton);
    }

    /**
     * click on edit button
     * * @throws InterruptedException
     */

    public void userClickOnEditButton() throws InterruptedException {
        selenium.clickOn(clickOnEditButton);
    }

    /**
     * click on detail fee button
     * * @throws InterruptedException
     */

    public void userClickOnDetailFee() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(clickOnDetailFee);
    }

    /**
     * Get discount admin fee in tagihan
     * @return
     */
    public String userSeeDicountAdminFeeInTagihan(){
        return selenium.getText(seeDicountAdminFeeInTagihan);
    }

    /**
     * Get discount admin fee in detail fee
     * @return
     */
    public String userSeeDicountAdminFeeInDetailFee(){
        return selenium.getText(seeDicountAdminFeeInDetailFee);
    }

    /**
     * Get invoice discount Page Header
     * @return
     */
    public String getInvoiceDiscountPageHeader(){
        return selenium.getText(invoiceDiscountPageHeaderText);
    }

    /**
     * Get message success
     * @return
     */
    public String getMessageSuccess(){
        return selenium.getText(messageSuccessText);
    }

    /**
     * user input discount amount
     * * @throws InterruptedException
     */

    public void userInputDiscountAmount(String discountAmount) throws InterruptedException {
        selenium.clickOn(inputDiscountAmountText);
        selenium.enterText(inputDiscountAmountText,discountAmount, true );
    }

    /**
     * user input discount name
     * * @throws InterruptedException
     */

    public void userInputDiscountName(String discountName) throws InterruptedException {
        selenium.clickOn(inputDiscountName);
        selenium.enterText(inputDiscountName, discountName, false);
    }
}

