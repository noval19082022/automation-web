package pageobjects.backoffice.cpDisbursement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class TambahDataTransferPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public TambahDataTransferPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy (xpath = "//button[contains(text(),'Tambah Data Transfer')]")
    private WebElement tambahDataTransferBtn;

    @FindBy (xpath = "//*[@id='looking_for_kost-add-new']")
    private WebElement namaPropertyTxt;

    @FindBy (xpath = "//*[@id='looking_for_kost_message-add-new']")
    private WebElement errorMessageNamaPropertyLbl;

    @FindBy (xpath = "(//*[@class='eac-item'])[1]")
    private WebElement propertyNameSuggestionTxt;

    @FindBy (xpath = "//*[@id='kost-level-add-new']")
    private WebElement productTypeLbl;

    @FindBy (css = "select#cp-disbursement-level-bank-add-new")
    private WebElement bankNameTxt;

    @FindBy (xpath = "(//*[@name='destination_account'])[21]")
    private WebElement noRekeningTxt;

    @FindBy (xpath = "(//*[@name='destination_name'])[21]")
    private WebElement namaPemilikRekeningTxt;

    @FindBy (xpath = "(//*[@name='owner_phone_number'])[21]")
    private WebElement ownerPhoneNumberTxt;

    @FindBy (id = "transfer_amount-add-new")
    private WebElement totalPendapatanTxt;

    @FindBy (xpath = "//select[@id='cp-disbursement-level-multiple-add-new']")
    private WebElement tipeTransaksiDdl;

    @FindBy (xpath = "//*[@id='transfer_due_date-add-new']")
    private WebElement tanggalTransferDatePicker;

    @FindBy (xpath = "(//button[@class='close'])[21]")
    private WebElement closeTambahDataTransferBtn;

    @FindBy (xpath = "(//*[@name='remark'])[21]")
    private WebElement tipeTransaksiLainnyaTxt;

    @FindBy(xpath = "//*[@value='Tambahkan']")
    private WebElement tambahkanBtn;

    @FindBy(xpath = "(//td[contains(., '(Today)')])[1]")
    private WebElement tglTransferCol;

    @FindBy(xpath = "(//td[contains(., 'Kost Singgahsini Harapan Bunda Halmahera Utara')])[1]")
    private WebElement namaPropCol;

    @FindBy(xpath = "(//td[contains(., 'Commission')])[1]")
    private WebElement tipeTransaksiCol;

    @FindBy(xpath = "(//td)[4]")
    private WebElement totalPndptnCol;

    @FindBy(xpath = "(//td[contains(., 'Yudha FC')])[1]")
    private WebElement detailRekCol;

    /**
     * click Tambah Data Transfer Button
     * @throws InterruptedException
     */
    public void clickTambahDataTransfer() throws InterruptedException {
        selenium.clickOn(tambahDataTransferBtn);
    }

    /**
     * Write nama property in nama property field
     * @param keyword nama property
     * @throws InterruptedException
     */
    public void setNamaProperty(String keyword) throws InterruptedException {
        selenium.enterTextCharacterByCharacter(namaPropertyTxt, keyword, true);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * get Error Message in field nama property
     * @return String error message
     */
    public String getPropertyNameErrorMessage() {
        return selenium.getText(errorMessageNamaPropertyLbl);
    }

    /**
     * get first property suggestion
     * @return string property name
     * @throws InterruptedException
     */
    public String getPropertySuggestion() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(propertyNameSuggestionTxt);
    }

    /**
     * choose first property suggestion
     * @throws InterruptedException
     */
    public void clickFirstPropertySuggestion() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.click(propertyNameSuggestionTxt);
    }

    /**
     * get product type
     * @return String product type
     * @throws InterruptedException
     */
    public String getProductType() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(productTypeLbl);
    }

    /**
     * get bank name
     * @return string bank name
     */
    public String getBankName() {
        return selenium.getElementAttributeValue(bankNameTxt,"value");
    }

    /**
     * get no rekening
     * @return string no rekening
     */
    public String getNoRekening() {
        return selenium.getElementAttributeValue(noRekeningTxt,"value");
    }

    /**
     * get pemilik rekening
     * @return string nama pemilik rekening
     */
    public String getPemilikRekening() {
        return selenium.getElementAttributeValue(namaPemilikRekeningTxt,"value");
    }

    /**
     * get no telpon pemilik
     * @return string no telpon pemilik
     */
    public String getNoTelponPemilik() {
        return selenium.getElementAttributeValue(ownerPhoneNumberTxt,"value");
    }

    /**
     * set Total pendapatan
     * @param amount total pendapatan
     */
    public void setTotalPendapatan(String amount) {
        selenium.enterText(totalPendapatanTxt,amount,true);
    }

    /**
     * Set tipe transaksi
     * @param revModel tipe transaksi
     */
    public void setTipeTransaksi(String revModel) {
        selenium.selectDropdownValueByText(tipeTransaksiDdl,revModel);
    }

    /**
     * Set tanggal transfer
     * @param date tanggal transfer
     * @throws InterruptedException
     */
    public void setTanggalTransfer(String date) throws InterruptedException {
        selenium.clickOn(tanggalTransferDatePicker);
        selenium.javascriptClickOn(By.xpath("//td[@class='day'][contains(text(),'"+date+"')]"));
    }

    /**
     * close tambah data transfer pop up
     * @throws InterruptedException
     */
    public void closeTambahDataTransfer() throws InterruptedException {
        selenium.clickOn(closeTambahDataTransferBtn);
    }

    /**
     * get property name in tambah data transfer pop up
     * @return String property name
     */
    public String getPropertyName() {
        return selenium.getElementAttributeValue(namaPropertyTxt,"value");
    }

    /**
     * get total pendapatan in tambah data transfer pop up
     * @return String total pendapatan
     */
    public String getTotalPendapatan() {
        return selenium.getElementAttributeValue(totalPendapatanTxt,"value");
    }

    /**
     * get tipe transaksi in tambah data transfer pop up
     * @return String tipe transaksi
     */
    public String getTipeTransaksi() {
        return selenium.getElementAttributeValue(tipeTransaksiDdl,"value");
    }

    /**
     * get tanggal transfer in tambah data transfer pop up
     * @return String tanggal transfer
     */
    public String getTanggalTransfer() {
        return selenium.getElementAttributeValue(tanggalTransferDatePicker,"value");
    }

    public void setLainnyaTipeTransaksi(String char50plus) {
        selenium.clearTextField(tipeTransaksiLainnyaTxt);
        selenium.enterText(tipeTransaksiLainnyaTxt,char50plus,true);
    }

    public String getTipeTransaksiLainnya() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getElementAttributeValue(tipeTransaksiLainnyaTxt,"value");
    }

    /**
     * click Tambahkan button on Tambah Data Transfer
     * @throws InterruptedException
     */
    public void clickTambahkan() throws InterruptedException {
        selenium.clickOn(tambahkanBtn);
    }

    /**
     * get tanggal transfer ke pemilik coloumn
     * @return String today
     */
    public String getTglTransferCol(){
        return selenium.getText(tglTransferCol);
    }

    /**
     * get Nama Property coloumn
     * @return String nama property
     */
    public String getNamaPropCol(){
        return selenium.getText(namaPropCol);
    }

    /**
     * get Tipe Transaksi coloumn
     * @return String tipe transaksi
     */
    public String getTipeTransaksiCol(){
        return selenium.getText(tipeTransaksiCol);
    }

    /**
     * get Total Pendapatan coloumn
     * @return String total pendapatan
     */
    public String getTotalPndptnCol(){
        return selenium.getElementAttributeValue(totalPndptnCol, "value");
    }

    /**
     * get Detail Rekening coloumn
     * @return String detail rekening
     */
    public String getDetailRekCol(){
        return selenium.getText(detailRekCol);
    }
}
