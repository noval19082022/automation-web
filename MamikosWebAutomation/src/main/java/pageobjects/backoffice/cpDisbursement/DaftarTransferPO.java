package pageobjects.backoffice.cpDisbursement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class DaftarTransferPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public DaftarTransferPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }


    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy (xpath = "//button[@title='Search By']")
    private WebElement searchByBtn;

    @FindBy (xpath = "//a[@id='bs-select-1-0']")
    private WebElement searchByNamaPropertyDdl;

    @FindBy (xpath = "//a[@id='bs-select-1-1']")
    private WebElement searchByNamaPemilikRekeningDdl;

    @FindBy (xpath = "//a[@id='bs-select-1-2']")
    private WebElement searchByNomorRekeningDdl;

    @FindBy (xpath = "//input[@name='search_value']")
    private WebElement searchTxt;

    @FindBy (xpath = "//input[@class='btn pay-backoffice__btn-primary btn-md']")
    private WebElement searchBtn;

    @FindBy (xpath = "//*[@class='pay-backoffice__table-empty']")
    private WebElement emptyPage;

    @FindBy (xpath = "//div[@class='pay-backoffice__table-empty']/h3")
    private WebElement emptyPageMessage1;

    @FindBy (xpath = "//div[@class='pay-backoffice__table-empty']/p")
    private WebElement emptyPageMessage2;

    @FindBy (xpath = "//tbody/tr")
    private List<WebElement> listProperty;

    @FindBy (xpath = "//*[@aria-controls='confirmed']")
    private WebElement tabDaftarTransfer;

    @FindBy (xpath = "//*[@aria-controls='processing']")
    private WebElement tabTransferDiproses;

    @FindBy (xpath = "//*[@aria-controls='failed']")
    private WebElement tabTransferGagal;

    @FindBy (xpath = "//td[2]")
    private List<WebElement> propertyNameCol;

    /**
     * select search by method
     * @param searchBy name of search by you want to use
     * @throws InterruptedException
     */
    public void chooseSearchDaftarTransferBy(String searchBy) throws InterruptedException {
        selenium.clickOn(searchByBtn);
        switch (searchBy){
            case "Nama Property" :
                selenium.waitForJavascriptToLoad();
                selenium.javascriptClickOn(searchByNamaPropertyDdl);
                break;

            case "Nama Pemilik Rekening":
                selenium.waitForJavascriptToLoad();
                selenium.javascriptClickOn(searchByNamaPemilikRekeningDdl);
                break;

            case "Nomor Rekening":
                selenium.waitForJavascriptToLoad();
                selenium.javascriptClickOn(searchByNomorRekeningDdl);
                break;

            default:
                System.out.println("Search By Not Valid");
        }
    }

    /**
     * set keyword in search field daftar transfer
     * @param keyword keyword you want to search
     * @throws InterruptedException
     */
    public void searchDaftarTransfer(String keyword) throws InterruptedException {
        selenium.enterText(searchTxt,keyword,true);
        selenium.clickOn(searchBtn);
    }

    /**
     * verify if empty page appear
     * @return true if appear, false if not appear
     * @throws InterruptedException
     */
    public boolean isEmptyPageAppear() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.isElementDisplayed(emptyPage);
    }

    /**
     * get empty page primary message
     * @return String "Belum Ada Data Transfer"
     * @throws InterruptedException
     */
    public String getEmptyPageMessage1() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(emptyPageMessage1);
    }

    /**
     * get empty page secondary message
     * @return String "Silakan tambah data transfer terlebih dahulu."
     * @throws InterruptedException
     */
    public String getEmptyPageMessage2() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.getText(emptyPageMessage2);
    }

    /**
     * get total property in daftar transfer list
     * @return int total row
     */
    public int getTotalListCPDisbursement() {
        return listProperty.size();
    }

    /**
     * get Property Name in row x
     * @param x index for row
     * @return String property name, not include the property type
     * @throws InterruptedException
     */
    public String getPropertyName(int x) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        String full = selenium.getText(propertyNameCol.get(x));
        String[] splitFull = full.split("\\r?\\n");
        return splitFull[0];
    }

    /**
     * get Account Name
     * @param x increment index
     * @return String text account name index x
     * @throws InterruptedException
     */
    public String getAccountName(int x) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        String full = selenium.getText(By.xpath("(//tbody/tr/td)["+(5+(6*x))+"]"));
        String[] extract = full.split("\\r?\\n");
        return extract[0];
    }

    /**
     * get Bank Account Number
     * @param x increment index
     * @return String Bank Account Number
     * @throws InterruptedException
     */
    public String getAccountNumber(int x) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        String full = selenium.getText(By.xpath("(//tbody/tr/td)["+(5+(6*x))+"]"));
        String[] extract = full.split("\\r?\\n");
        return extract[1];
    }

    /**
     * choose CP Disbursement tab (Daftar Transfer / Transfer Diproses / Transfer Gagal)
     * @param tab tab name
     * @throws InterruptedException
     */
    public void clickTabCPDisbursement(String tab) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        switch (tab){
            case "Daftar Transfer":
                selenium.clickOn(tabDaftarTransfer);
                break;
            case "Transfer Diproses" :
                selenium.clickOn(tabTransferDiproses);
                break;
            case "Transfer Gagal" :
                selenium.clickOn(tabTransferGagal);
                break;
            default:
                System.out.println("Tab doesn't exist");
        }
    }
}
