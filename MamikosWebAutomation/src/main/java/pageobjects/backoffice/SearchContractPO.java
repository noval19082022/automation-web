package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

public class SearchContractPO {

    WebDriver driver;
    SeleniumHelpers selenium;
    JavaHelpers java;

    public SearchContractPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);
        java = new JavaHelpers();

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//h3[@class='tools-contract__title']")
    private WebElement searchContractHeader;

    @FindBy(css = "input[name='search_value']")
    private WebElement searchTextBox;

    @FindBy(css = "input[value='Search']")
    private WebElement searchContractButton;

    @FindBy(xpath = "//tr[1]/td[11]/span")
    private WebElement bookingStatus;

    @FindBy(xpath = "//*[.='Batalkan Kontrak']")
    private WebElement bookingCancelButton;

    @FindBy(xpath = "(//tbody/tr)[1]")
    private WebElement firstContractList;

    @FindBy(name = "search_by")
    private WebElement searchByDropdownlist;

    @FindBy(name = "search_value")
    private WebElement searchValueDropdownlist;

    @FindBy(xpath = "//*[@class='pull-right']//*[@class='btn btn-primary']")
    private WebElement addContractButton;

    @FindBy(xpath = "//*//*[.='Batalkan Kontrak']/parent::*/following-sibling::*[1]")
    private WebElement terminateContractButton;

    @FindBy(xpath = "//*[@class='label  label-success ']")
    private WebElement activeContractLabel;

    @FindBy(xpath = "(//tbody/tr)[1]//td[4]//ul//li/a")
    private WebElement invoiceUrl;

    @FindBy(xpath = "//*[.='Add On']")
    private WebElement btnActiveAddOn;

    @FindBy(xpath = "//*[.='Remove']")
    private WebElement btnRemoveAddOns;

    @FindBy(css = ".callout")
    private WebElement callout;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//textarea[@name='remark']")
    private WebElement inputTextDamageDetails;

    @FindBy(css = "[value='Search']")
    private WebElement searchButton;

    @FindBy(xpath = "(//a[contains(.,'Edit Deposit')])[2]")
    private WebElement editDepositButton;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//h4[@id='myModalLabel']")
    private WebElement akhiriKontrakSewaText;

    @FindBy(className = "select2-search__field")
    private WebElement kostLevelTextBox;

    @FindBy(xpath = "//li[@class='select2-results__option select2-results__option--selectable select2-results__option--highlighted']")
    private WebElement kostLevelOption;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//input[@name='destination_name']")
    private WebElement pemilikRekeningTextBox;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//input[@name='destination_account']")
    private WebElement nomorRekeningTextBox;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//select[@name='destination_bank']")
    private WebElement bankNameText;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//option[.='ANZ Indonesia']")
    private WebElement anzIndonesiaText;

    @FindBy(xpath = "//span[.='200']")
    private WebElement maximalLength;

    @FindBy(xpath = "//h4[.='Informasi Pemilik Sekarang']")
    private WebElement informasiPemilikText;

    @FindBy(xpath = "(//a[@class='btn btn-xs tools-contract__btn tools-contract__btn-green'][normalize-space()='Ganti Owner'])[1]")
    private WebElement gantiOwnerButton;

    @FindBy(css = "[name='phone_number']")
    private WebElement numberOwnerText;

    @FindBy(css = ".btn-primary")
    private WebElement cariAlternatifPemilik;

    @FindBy(xpath = "//input[@class='btn btn-primary btn-md']")
    private WebElement prosesGantiPemilik;

    @FindBy(xpath = "//a[.='Cancel']")
    private WebElement prosesCancelGantiPemilik;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//input[@name='fine_amount']")
    private WebElement biayaKerusakanText;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//p[.='Sisa Deposit']")
    private WebElement getSisaDepositText;

    @FindBy(xpath = "//h3[.='Data Contract']")
    private WebElement getDataContractText;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//p[.='Pastikan data rekening dan kerusakan sudah sesuai']")
    private WebElement konfirmasiSisaDepositButton;

    @FindBy(css = ".tools-contract__input .fa-caret-down")
    private WebElement btnDatePeriod;

    @FindBy(xpath = "(//a[@class='btn btn-xs tools-contract__btn tools-contract__btn-green'][normalize-space()='Extend Kontrak'])[1]")
    private WebElement extendContractButton;

    @FindBy(xpath = "//tbody[1]/tr[1]")
    private WebElement dataDetail;

    @FindBy(xpath = "(//span[@class='level-name '])[1]")
    private WebElement verifyKostDetail;

    @FindBy(xpath = "//div[@class='callout callout-success']")
    private WebElement messageDraft;

    @FindBy(xpath = "(//span[@class='custom-label  custom-label__danger '])[1]")
    private WebElement statusContract;

    @FindBy(xpath = "//input[@placeholder='Masukkan tanggal checkout']")
    private WebElement inputTanggalCheckoutTerminateDate;

    @FindBy(css = ".xdsoft_date.xdsoft_today")
    private WebElement todayTerminateDate;

    @FindBy(css = "input[value='Akhiri Kontrak']")
    private WebElement terminateContractPopUpButton;

    @FindBy(css = ".callout-success")
    private WebElement calloutSucessAction;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//input[@name='transfer_due_date']")
    private WebElement inputActiveTransferDate;

    @FindBy(xpath = "//td[@class='day']")
    private WebElement editDraftDate;

    @FindBy(xpath = "//td[@class='active day']")
    private WebElement selectDate;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//p[.='Tanggal Transfer']")
    private WebElement getDateTransfer;

    @FindBy(xpath = "//div[@class='tools-contract__modal modal fade in']//input[@class='btn btn-default tools-contract__btn tools-contract__btn-default']")
    private WebElement simpanDraftButton;

    @FindBy (css = ".fade.in .modal-header  h4")
    private  WebElement editDepositConfirmToFinanceApik;

    private By additionalDuration = RelativeLocator.with(By.xpath("//input[@name='additional_duration']")).below(By.xpath("//label[normalize-space()='Additional Duration']"));

    private By extendButton = RelativeLocator.with(By.xpath("//input[@value='Extend']")).below(By.xpath("//a[normalize-space()='+ Add fixed costs']"));

    private By tenantContractList = By.xpath("//tr[@contract-id]");


    /**
     * Get Search Contract Page Header
     *
     * @return
     */
    public String getSearchContractPageHeader() {
        return selenium.getText(searchContractHeader);
    }

    /**
     * Get Text sisa deposit
     *
     * @return
     */
    public String getSisaDeposit() {
        return selenium.getText(getSisaDepositText);
    }

    /**
     * Get Text data contract
     *
     * @return
     */
    public String getDataContract() {
        return selenium.getText(getDataContractText);
    }

    /**
     * Enter Search Text in to Search box
     *
     * @param searchText search text
     * @throws InterruptedException
     */
    public void enterTextToSearchTextbox(String searchText) throws InterruptedException {
        selenium.clickOn(searchTextBox);
        selenium.enterText(searchTextBox, searchText, true);
    }

    /**
     * Click On Search Contract Button
     *
     * @throws InterruptedException
     */
    public void clickOnSearchContractButton() throws InterruptedException {
        selenium.clickOn(searchContractButton);
    }

    /**
     * Get Status Value of Searched Booking
     *
     * @return Booking Status
     */
    public String getBookingStatus() {
        return selenium.getText(bookingStatus);
    }

    /**
     * Click On Booking Cancel Button
     *
     * @throws InterruptedException
     */
    public void clickOnBookingCancelButton() throws InterruptedException {
        String attributeValue = selenium.getElementAttributeValue(bookingCancelButton, "href");
        int cid = java.extractNumber(attributeValue);
        selenium.enterTextUsingJavascriptExecute("window.location.href = '/backoffice/contract/cancel/" + cid + "';");
        selenium.hardWait(5);
    }

    /**
     * Click On Terminate Button and accept alert
     */
    public void clickOnCancelContractButton() {
        if (selenium.isElementDisplayed(bookingCancelButton)) {
            selenium.waitTillElementIsClickable(bookingCancelButton);
            selenium.javascriptClickOn(bookingCancelButton);
            selenium.waitTillAlertPresent();
            selenium.acceptAlert();
        }
    }

    /**
     * Get ID contract
     *
     * @return ID contract
     */
    public String getIDContract() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getElementAttributeValue(firstContractList, "contract-id");
    }


    /**
     * Select Filter Search By
     *
     * @param filterText
     * @throws InterruptedException
     */
    public void selectFilterSearchBy(String filterText) throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(searchByDropdownlist);
        selenium.clickOn(By.xpath("//option[contains(.,'" + filterText + "')]"));
        selenium.hardWait(1);
    }

    /**
     * Search contract on the list on Mamipay admin
     *
     * @param HP tenant phone number
     * @throws InterruptedException
     */
    public void searchContractbyTenantPhoneNumber(String HP) throws InterruptedException {
        selenium.selectDropdownValueByText(searchByDropdownlist, "Renter Phone Number");
        selenium.javascriptSetValue(searchValueDropdownlist, HP);
        selenium.clickOn(searchContractButton);
        selenium.hardWait(3);
        selenium.waitTillElementsCountIsMoreThan(tenantContractList, 0);
    }

    /**
     * Click terminate contract button on Active kontrak
     */
    public void clickOnTerminateContractButton() {
        if (selenium.isElementDisplayed(terminateContractButton)) {
            selenium.javascriptClickOn(terminateContractButton);
        }
    }

    /**
     * to identify there is a button akhiri kontrak
     *
     * @return true/false
     */
    public boolean terminateContractButtonIsExist() {
        return selenium.isElementDisplayed(terminateContractButton);
    }

    /**
     * Check if tenant have active contract before creating a contract
     */
    public void checkTenantActiveContract() {
        if (selenium.isElementDisplayed(activeContractLabel)) {
            clickOnTerminateContractButton();
        }
    }

    /**
     * Click On invoice url of contract
     */
    public void clickOnInvoiceUrl() throws InterruptedException {
        selenium.clickOn(invoiceUrl);
    }

    /**
     * Button click on active add on
     *
     * @throws InterruptedException
     */
    public void clickOnAddOn() throws InterruptedException {
        selenium.clickOn(btnActiveAddOn);
    }

    /**
     * Get Contract ID Column Text
     *
     * @param contractID input string that used to define contract id value
     * @return string
     */
    public String getContractIDColumnText(String contractID) {
        return selenium.getText(By.xpath("//th[text()='" + contractID + "']"));
    }

    /**
     * Is Contract ID Column Appeared
     *
     * @param contractID input string that used to define contract id value
     * @return string
     */
    public Boolean isContractIDColumnAppeared(String contractID) {
        return selenium.waitInCaseElementVisible(By.xpath("//th[text()='" + contractID + "']"), 3) != null;
    }

    /**
     * Get Contract ID Value Text
     *
     * @param contractID input string that used to define contract id value
     * @return string
     */
    public String getContractIDValueText(String contractID) {
        return selenium.getText(By.xpath("//td[text()='" + contractID + "']"));
    }

    /**
     * Is Contract ID value appeared?
     *
     * @param contractID input string that used to define contract id value
     * @return string
     */
    public Boolean isContractIDValueAppeared(String contractID) {
        return selenium.waitInCaseElementVisible(By.xpath("//td[text()='" + contractID + "']"), 3) != null;
    }

    /**
     * Get Owner ID Value Text
     *
     * @param ownerID input string that used to define owner id value
     * @return string
     */
    public String getOwnerIDValueText(String ownerID) {
        return selenium.getElementAttributeValue(By.xpath("//tr[@owner-id='" + ownerID + "']"), "owner-id");
    }

    /**
     * Is Owner ID Value Text Appeared
     *
     * @param ownerID input string that used to define owner id value
     * @return string
     */
    public Boolean isOwnerIDValueTextAppeared(String ownerID) {
        return selenium.getElementAttributeValue(By.xpath("//tr[@owner-id='" + ownerID + "']"), "owner-id") != null;
    }

    /**
     * Click on add on button by it index
     *
     * @param index of add on button
     * @throws InterruptedException
     */
    public void clicksOnAddOnsIndex(String index) throws InterruptedException {
        String addOnEl = "//*[.='Add On'][" + index + "]";
        selenium.clickOn(By.xpath(addOnEl));
    }

    /**
     * click on invoice on contract first index
     *
     * @param index 1,2,3,4,5 etc
     * @throws InterruptedException
     */
    public void clicksOnInvoiceNumberOnFirstIndex(String index) throws InterruptedException {
        String invoiceEl = "(//tr[1]/following::ul/li/a[contains(text(), 'Pembayaran')])[" + index + "]";
        selenium.clickOn(By.xpath(invoiceEl));
        selenium.waitTillElementIsVisible(By.xpath("//td[1]/a"), 20);
        selenium.hardWait(5);
        selenium.javascriptClickOn(By.xpath("//td[1]/a"));
        selenium.switchToWindow(4);
    }

    /**
     * Delete add ons
     * please noticed that there should only one add ons appear otherwise method will fail
     *
     * @throws InterruptedException
     */
    public void deleteAddOns() throws InterruptedException {
        selenium.clickOn(btnRemoveAddOns);
        selenium.acceptAlert();
        selenium.waitTillElementIsVisible(callout);
        selenium.hardWait(2);
    }

    /**
     * true callout text
     *
     * @return boolean data type
     */
    public boolean calloutText() {
        return selenium.waitInCaseElementVisible(callout, 5) != null;
    }
    /**
     * Get callout text
     *
     * @return string data type
     */
    public String getCalloutText() {
        return selenium.getText(callout);
    }
    /**
     * input Text Damage Details
     */
    public void inputTextDamageDetails(String inputText) throws InterruptedException {
        selenium.clickOn(inputTextDamageDetails);
        selenium.enterText(inputTextDamageDetails, inputText, false);
    }

    /**
     * input text mamirooms
     *
     * @throws InterruptedException
     */
    public void fillsKostLevel(String kostLevel) throws InterruptedException {
        selenium.clickOn(kostLevelTextBox);
        selenium.enterText(kostLevelTextBox, kostLevel, false);
        selenium.clickOn(kostLevelOption);
    }

    /**
     * input text biaya kerusakan
     *
     * @throws InterruptedException
     */
    public void inputBiayaKerusakan(String biayaKerusakan) throws InterruptedException {
        selenium.clickOn(biayaKerusakanText);
        selenium.enterText(biayaKerusakanText, biayaKerusakan, true);
    }

    /**
     * input text name rekening
     *
     * @throws InterruptedException
     */
    public void fillsNamePemilikRekening(String inputNameRekening) throws InterruptedException {
        selenium.clickOn(pemilikRekeningTextBox);
        selenium.enterText(pemilikRekeningTextBox, inputNameRekening, true);
    }

    /**
     * input text nomor rekening
     *
     * @throws InterruptedException
     */
    public void fillsNomorRekening(String inputNomorRekening) throws InterruptedException {
        selenium.clickOn(nomorRekeningTextBox);
        selenium.enterText(nomorRekeningTextBox, inputNomorRekening, true);
    }

    /**
     * click search button
     *
     * @throws InterruptedException
     */
    public void clickSearchButton() throws InterruptedException {
        selenium.clickOn(searchButton);
    }

    /**
     * click Extend Contract Button
     *
     * @throws InterruptedException
     */
    public void clickExtendContractButton() throws InterruptedException {
        selenium.javascriptClickOn(extendContractButton);
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * click ganti owner button
     *
     * @throws InterruptedException
     */
    public void clickGantiOwnerButton() throws InterruptedException {
        // selenium.clickOn(By.xpath("//a[contains(.,'Ganti Owner')][1]"));
        selenium.hardWait(10);
        selenium.clickOn(gantiOwnerButton);
        selenium.acceptAlert();
    }

    /**
     * click see log button
     *
     * @throws InterruptedException
     */
    public void clickSeeLogButton() throws InterruptedException {
        selenium.clickOn(By.xpath("//a[contains(.,'See log')][1]"));
    }

    /**
     * fills Duration Month
     *
     * @throws InterruptedException
     */
    public void fillsDurationMonth(String month) throws InterruptedException {
        selenium.clickOn(additionalDuration);
        selenium.enterText(additionalDuration, month, false);
    }

    /**
     * click Extend Button
     *
     * @throws InterruptedException
     */
    public void clickExtendButton() throws InterruptedException {
        selenium.javascriptClickOn(extendButton);
        selenium.acceptAlert();
    }

    /**
     * click akhiri contract button
     *
     * @throws InterruptedException
     */
    public void clickAkhiriContractButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(By.xpath("(//button[@class='open_modal tools-contract__btn tools-contract__btn-danger'][normalize-space()='Akhiri Kontrak'])[1]"));
    }

    /**
     * input text phone number
     *
     * @throws InterruptedException
     */
    public void inputNumberOwnerText(String inputNumberOwner) throws InterruptedException {
        selenium.clickOn(numberOwnerText);
        selenium.enterText(numberOwnerText, inputNumberOwner, false);
    }

    /**
     * click cari alternatif pemilik button
     *
     * @throws InterruptedException
     */
    public void clickCariAlternatifPemilik() throws InterruptedException {
        selenium.clickOn(cariAlternatifPemilik);
    }

    /**
     * click proses ganti pemilik button
     *
     * @throws InterruptedException
     */
    public void clickProsesGantiPemilik() throws InterruptedException {
        selenium.clickOn(prosesGantiPemilik);
        selenium.acceptAlert();
    }

    /**
     * click cancel proses ganti pemilik button
     *
     * @throws InterruptedException
     */
    public void clickCancelProsesGantiPemilik() throws InterruptedException {
        selenium.clickOn(prosesCancelGantiPemilik);
    }

    /**
     * click dropdown bank name
     *
     * @throws InterruptedException
     */
    public void chooseBankName() throws InterruptedException {
        selenium.clickOn(bankNameText);
        selenium.clickOn(anzIndonesiaText);
    }

    /**
     * Maximal Length
     *
     * @return string data type
     * @throws InterruptedException
     */
    public String userSeeMaximalLength() throws InterruptedException {
        return selenium.getText(maximalLength);
    }

    /**
     * Get Name bank
     *
     * @return string data type
     * @throws InterruptedException
     */
    public String getNameBank() throws InterruptedException {
        return selenium.getText(anzIndonesiaText);
    }

    /**
     * Get informasi pemilik sekarang
     *
     * @return string data type
     * @throws InterruptedException
     */
    public String getInformasiPemilik() throws InterruptedException {
        return selenium.getText(informasiPemilikText);
    }

    /**
     * Get Edit Deposit for Confirm to Finance text Apik
     *
     * @return string data type
     * @throws InterruptedException
     */
    public String getEditDepositApik() throws InterruptedException {
        selenium.hardWait(3);
        return  selenium.getText(editDepositConfirmToFinanceApik);
    }

    /**
     * Get akhiri kontrak sewa text
     * @return string data type
     * @throws InterruptedException
     */
    public String getAkhiriKontrakSewa() throws InterruptedException {
        selenium.waitTillElementIsVisible(akhiriKontrakSewaText,10);
        return selenium.getText(akhiriKontrakSewaText);
    }

    /**
     * Get akhiri kontrak button disable
     * @return string data type
     * @throws InterruptedException
     */
    public String getAkhiriKontrakButtonDisable() throws InterruptedException {
        selenium.waitTillElementIsVisible(akhiriKontrakSewaText,10);
        return selenium.getText(akhiriKontrakSewaText);
    }

    /**
     * Get konfimasi edit deposit button
     * @return string data type
     * @throws InterruptedException
     */
    public String getKonfirmasiSisaDepositButton() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.getText(konfirmasiSisaDepositButton);
    }

    /**
     * redirect To Search Contract Menu Detail
     * @return string data type
     * @throws InterruptedException
     */
    public String redirectToSearchContractMenuDetail() throws InterruptedException {
        return selenium.getText(searchContractHeader);
    }

    /**
     * click edit deposit button
     */
    public void clickEditDepositButton() throws InterruptedException {
        selenium.waitInCaseElementClickable(editDepositButton,30);
        selenium.clickOn(editDepositButton);
    }

    /**
     * user Input Active Transfer Date
     */
    public void userInputActiveTransferDate() throws InterruptedException {
        selenium.clickOn(inputActiveTransferDate);
        selenium.javascriptClickOn(editDraftDate);
    }

    public boolean getDateTransfer() {
        return selenium.isElementDisplayed(getDateTransfer);
    }

    /**
     * user Input Day Transfer Date
     */
    public void userInputDayTransferDate() throws InterruptedException {
        selenium.clickOn(inputActiveTransferDate);
        selenium.javascriptClickOn(selectDate);
    }

    /**
     * user Click On Simpan Draft Button
     */
    public void userClickOnSimpanDraftButton() throws InterruptedException {
        selenium.clickOn(simpanDraftButton);
    }

    /**
     * click Contract Date Period
     * @throws InterruptedException
     */
    public void clickContractDatePeriod() throws InterruptedException {
        selenium.clickOn(btnDatePeriod);
    }
    /**
     * click choose today
     * @throws InterruptedException
     */
    public void chooseToday(String datetime) throws InterruptedException {
        selenium.clickOn(By.xpath("//li[.='"+datetime+"']"));
    }
    /**
     * check blank data
     * @throws InterruptedException
     */
    public boolean checkBlankData() {
        return selenium.waitInCaseElementVisible(dataDetail, 3) == null;
    }
    /**
     * check data detail
     * @throws InterruptedException
     */
    public void checkDataDetail() {
        selenium.waitTillElementIsVisible(dataDetail,3);
    }
    /**
     * get text detail
     * @throws InterruptedException
     */
    public String getTextDetail() {
        return selenium.getText(verifyKostDetail);
    }
    /**
     * get message draft
     * @throws InterruptedException
     */
    public String getMessageDraft() {
        return selenium.getText(messageDraft);
    }
    /**
     * get text status contract
     * @throws InterruptedException
     */
    public String getTextStatusContract() {
        return selenium.getText(statusContract);
    }

    /**
     * Terminate contract by today date
     * @throws InterruptedException
     */
    public void terminateContractByTodayDate() throws InterruptedException {
        String todayDateHour = "";
        String todayDate = java.getTimeStamp("YYYY-MM-dd");;
        String currentHour = java.getTimeStamp("HH:mm:ss");
        todayDateHour = todayDate + " " + currentHour;
        selenium.hardWait(2);
        if (selenium.waitInCaseElementVisible(terminateContractButton, 15) != null) {
             selenium.clickOn(terminateContractButton);
             if (selenium.isAlertPresent(5)) {
                 selenium.acceptAlert();
             }
             else {
                 selenium.enterText(inputTanggalCheckoutTerminateDate, todayDateHour, true);
                 selenium.hardWait(5);
                 selenium.javascriptClickOn(By.cssSelector(".xdsoft_time.xdsoft_today"));
                 selenium.clickOn(terminateContractPopUpButton);
             }
             selenium.waitTillElementIsVisible(calloutSucessAction, 20);
        }
    }

}
