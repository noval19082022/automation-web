package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class paymentBigflipPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public paymentBigflipPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = ".sc-j0rkfz-0")
    private WebElement switchButton;

    @FindBy(xpath = "//li[.='Test Mode']")
    private WebElement testModeButton;

    @FindBy(xpath = "//span[contains(.,'Anda sedang berada dalam Test Mode.')]")
    private WebElement testModeText;

    @FindBy(css = ".u-text-semi.fa-chevron-down")
    private WebElement riwayatTransaksiButton;

    @FindBy(xpath = "//a[.='Domestic']")
    private WebElement domesticButton;

    @FindBy(xpath = "//button[.='Force Success']")
    private WebElement forceSuccessButton;

    @FindBy(xpath = "//tr[1]//div[.='Berhasil ']")
    private WebElement successText;

    /**
     * Click on switch button to test mode
     * @throws InterruptedException
     */
    public void clickOnTestModeButton() throws InterruptedException {
        selenium.clickOn(switchButton);
        selenium.clickOn(testModeButton);
    }

    /**
     * Get test mode text
     * @return string data type
     */
    public String getTestModeText() {
        return selenium.getText(testModeText);
    }

    /**
     * Click on domestic button
     * @throws InterruptedException
     */
    public void clickOnDomesticButton() throws InterruptedException {
        selenium.clickOn(riwayatTransaksiButton);
        selenium.clickOn(domesticButton);
    }

    /**
     * Click on force success button
     * @throws InterruptedException
     */
    public void clickOnForceSuccessButton() throws InterruptedException {
        selenium.clickOn(forceSuccessButton);
    }

    /**
     * Get success text
     * @return string data type
     */
    public String getSuccessText() {
        return selenium.getText(successText);
    }

}
