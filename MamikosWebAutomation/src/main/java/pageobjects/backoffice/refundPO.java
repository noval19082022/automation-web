package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class refundPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public refundPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */


    @FindBy(xpath = "(//*[@class='table table-hover table-bordered refund-invoice__table-allowed']/descendant::button)[1]")
    private WebElement refundButton;

    @FindBy(xpath = "(//button[@type='button'][normalize-space()='Back'])")
    private WebElement backButtonPopupRefund;

    @FindBy(xpath = "(//select[@class='form-control refund-invoice__modal-select'])[1]")
    private WebElement dropdownChooseReason;

    @FindBy(xpath = "(//option[contains(text(),'Pemilik Membatalkan')])[1]")
    private WebElement chooseReason;

    @FindBy(xpath = "(//div[@class='refund-invoice__modal modal fade in']//child::input)[5]")
    private WebElement uncheckAdminFee;

    @FindBy(xpath = "(//input[@class='btn btn-success mark-the-transfer'])[1]")
    private WebElement refundAndTransferButton;

    @FindBy(xpath = "(//div[@class='refund-invoice__modal modal fade in']//child::input)[4]")
    private WebElement editAmount;

    @FindBy(css = ".col-xs-2:nth-child(1) .filter-option")
    private WebElement searchByDropdownlist;

    @FindBy(css = "input[name='search_value']")
    private WebElement searchTextBox;

    @FindBy(css = ".fa-download")
    private WebElement exportButton;

    @FindBy(css = ".modal-content .refund-invoice__button-default")
    private WebElement dateButton;

    @FindBy(xpath = "//div[@class='daterangepicker ltr show-ranges opensleft']//li[.='Today']")
    private WebElement dateTodayButton;

    @FindBy(css = ".refund-invoice__download .btn-success")
    private WebElement downloadButton;

    @FindBy(xpath = "(//input[@placeholder='Masukkan nama pemilik rekening'])")
    private WebElement inputAccountName;

    @FindBy(xpath = "(//input[@placeholder='Masukkan nomor rekening'])")
    private WebElement inputAccountNumber;

    @FindBy(css = "div[class='callout callout-danger'] ul li")
    private WebElement resultFoundText;

    @FindBy(xpath = "//div[@class='refund-invoice__modal modal fade in']//input[@name='refund_account_name']")
    private WebElement inputTextNameRekening;

    @FindBy(xpath = "//div[@class='refund-invoice__modal modal fade in']//input[@name='refund_account']")
    private WebElement inputTextNoRekening;

    @FindBy(xpath = "//tbody[1]/tr[1]/td[8]")
    private WebElement accountNumberText;

    @FindBy(xpath = "//tr[1] //a[.='Receipt']")
    private WebElement receiptButton;

    @FindBy(xpath = "//a[.='Receipt']")
    private WebElement receiptButton2;

    @FindBy(xpath = "//b[@role='presentation']")
    private WebElement dropdownBankName;

    @FindBy(xpath = "//input[@role='textbox']")
    private WebElement inputBankName;

    @FindBy(xpath = "//b[@role='presentation']")
    private WebElement popupListBankClosed;

    @FindBy(xpath = "//li[@role='treeitem']")
    private WebElement messageNotResultFound;

    /**
     * Click on refund button
     * @throws InterruptedException
     */
    public void clickOnRefundButton() throws InterruptedException {
        selenium.clickOn(refundButton);
    }
    /**
     * Click On back button popup refund
     * @throws InterruptedException
     */
    public void clickOnBackButtonPopupRefund() throws InterruptedException {
        selenium.clickOn(backButtonPopupRefund);
    }
    /**
     * Click on uncheck admin fee
     * @throws InterruptedException
     */
    public void clickOnUncheckAdminFee() throws InterruptedException {
        selenium.clickOn(uncheckAdminFee);
    }
    /**
     * Click on refund button
     * @throws InterruptedException
     */
    public void clickOnChooseReason() throws InterruptedException {
        selenium.clickOn(dropdownChooseReason);
        selenium.clickOn(chooseReason);
    }
    /**
     * Click on refund and transfer button
     * @throws InterruptedException
     */
    public void clickOnRefundAndTransferButton() throws InterruptedException {
        selenium.clickOn(refundAndTransferButton);
    }
    /**
     * Click on edit amount
     * @throws InterruptedException
     */
    public void clickOnEditAmount(String amount) throws InterruptedException {
        selenium.clickOn(editAmount);
        selenium.enterText(editAmount, amount, true);
    }
    /**
     * Select Filter Search By
     * @param filterText
     * @throws InterruptedException
     */
    public void selectFilterSearchBy(String filterText) throws InterruptedException {
        selenium.clickOn(searchByDropdownlist);
        selenium.clickOn(By.xpath("//span[normalize-space()='"+filterText+"']"));
    }
    /**
     * Enter Search Text in to Search box
     * @param searchText search text
     * @throws InterruptedException
     */
    public void enterTextToSearchTextbox(String searchText) throws InterruptedException {
        selenium.clickOn(searchTextBox);
        selenium.enterText(searchTextBox, searchText, true);
    }
    /**
     * Click on tab button refund menu
     * @throws InterruptedException
     */
    public void clickOnTabButton(String filterTab) throws InterruptedException {
        selenium.clickOn(By.xpath("//h4[.='"+filterTab+"']"));
    }
    /**
     * Verify on page refund failed tab
     * @throws InterruptedException
     */
    public void verifyFailedTab() throws InterruptedException {
        selenium.waitTillElementIsVisible(By.cssSelector(".refund-invoice__card .refund-invoice__filter"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Invoice Number']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Booking Code']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Tenant Detail']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Kost Detail']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Refund Failed Date']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Refund Amount']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Account Number']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Reason']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Action']"), 0);
    }
    /**
     * Click on export button
     * @throws InterruptedException
     */
    public void clickOnExportButton() throws InterruptedException {
        selenium.clickOn(exportButton);
    }
    /**
     * Choose date button
     * @throws InterruptedException
     */
    public void chooseDateTodayButton() throws InterruptedException {
        selenium.clickOn(dateButton);
        selenium.clickOn(dateTodayButton);
    }
    /**
     * Click on download button
     * @throws InterruptedException
     */
    public void clickOndownloadButton() throws InterruptedException {
        selenium.clickOn(downloadButton);
    }
    /**
     * Click on input account name
     * @throws InterruptedException
     */
    public void clickOnInputAccountName(String name) throws InterruptedException {
        selenium.clickOn(inputAccountName);
        selenium.enterText(inputAccountName, name, true);
    }
    /**
     * Click on input account number
     * @throws InterruptedException
     */
    public void clickOnInputAccountNumber(String number) throws InterruptedException {
        selenium.clickOn(inputAccountNumber);
        selenium.enterText(inputAccountNumber, number, true);
    }
    /**
     * Get result found text
     * @return string data type
     */
    public String getResultFoundText(String errorMessage) {
       return selenium.getText(By.xpath("//li[contains(text(),'" + errorMessage + "')]"));
        }
    /**
     * input field rekening
     * @throws InterruptedException
     */
    public void fillsFieldPemilikRekening(String inputNoRekening, String inputNameRekening) throws InterruptedException {
        selenium.clickOn(inputTextNoRekening);
        selenium.enterText(inputTextNoRekening, inputNoRekening, false);
        selenium.clickOn(inputTextNameRekening);
        selenium.enterText(inputTextNameRekening, inputNameRekening, false);
    }
    /**
     * Get account number text
     * @return string data type
     */
    public String getAccNumber() {
        selenium.waitTillElementIsVisible(receiptButton, 0);
        return selenium.getText(accountNumberText);
    }
    /**
     * Verify on page refund transferred tab
     * @throws InterruptedException
     */
    public void verifyTransferredTab() throws InterruptedException {
        selenium.waitTillElementIsVisible(By.cssSelector(".fa-download"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Invoice Number']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Booking Code']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Tenant Detail']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Kost Detail']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Method']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Transferred Date']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Refund Amount']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Account Number']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Reason']"), 0);
        selenium.waitTillElementIsVisible(By.xpath("//th[.='Action']"), 0);
        //a[.='Receipt']
    }
    /**
     * Verify on page refund transferred tab transaction flip
     * @throws InterruptedException
     */
    public void verifyTransactionFlip() throws InterruptedException {
        selenium.waitTillElementIsVisible(By.xpath("//a[.='Receipt']"), 0);
    }
    /**
     * Verify on page refund transferred tab transaction credit card
     * @throws InterruptedException
     */
    public void verifyTransactionCreditCard() throws InterruptedException {
        selenium.waitTillElementIsVisible(By.xpath("//span[.='( from Credit Card )']"), 15);
    }
    /**
     * Click on receipt button
     * @throws InterruptedException
     */
    public void clickReceiptButton() throws InterruptedException {
        selenium.clickOn(receiptButton2);
    }
    /**
     * User click on dropdown bank name
     * @throws  InterruptedException
     */
    public void clickOnDropdownBankName () throws InterruptedException {
        selenium.clickOn(dropdownBankName);
    }
    /**
     * User choose bank name
     * @throws  InterruptedException
     */
    public void chooseBankName (String bank) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//li[.='"+bank+"']"));
        selenium.clickOn(element);
    }
    /**
     * User input bank name
     * @throws  InterruptedException
     */
    public void inputBankName (String bankName) throws InterruptedException {
        selenium.clickOn(inputBankName);
        selenium.enterText(inputBankName, bankName, false);
    }
    /**
     * popup list bank closed and bank selected
     * @return
     */
    public boolean popupListBankClosed() {
        return selenium.waitInCaseElementVisible(popupListBankClosed, 5) != null;
    }
    /**
     * user will get message not result found
     * @return
     */
    public boolean messageNotResultFound() {
        return selenium.waitInCaseElementVisible(messageNotResultFound, 5) != null;
    }
}