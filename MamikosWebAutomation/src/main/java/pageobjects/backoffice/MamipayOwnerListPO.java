package pageobjects.backoffice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class MamipayOwnerListPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public MamipayOwnerListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(name = "search_value")
    private WebElement searchOwnerPhoneField;

    @FindBy(css = ".btn")
    private WebElement searchOwnerButton;

    @FindBy(xpath = "//table//tr[1]//a[@title='Remove']")
    private WebElement firstDeleteButton;


    /**
     * Insert owner phone number in search field
     * @param phone is owner phone number
     */
    public void enterTextOwnerPhoneNumber(String phone) {
        selenium.enterText(searchOwnerPhoneField, phone, true);
    }

    /**
     * Click on Search Owner button
     * @throws InterruptedException
     */
    public void clickSearchOwnerButton() throws InterruptedException {
        selenium.clickOn(searchOwnerButton);
        selenium.hardWait(3);
    }

    /**
     * Click on First delete button on Mamipay Owner List's table
     * @throws InterruptedException
     */
    public void clickFirstDeleteOwnerButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(firstDeleteButton, 5);
        selenium.javascriptClickOn(firstDeleteButton);
        selenium.hardWait(2);
    }

    /**
     * Check if there's delete button
     * @return true if delete button appear
     */
    public boolean isFirstDeleteOwnerButtonAppear() {
        return selenium.waitInCaseElementVisible(firstDeleteButton, 7) != null;
    }
}
