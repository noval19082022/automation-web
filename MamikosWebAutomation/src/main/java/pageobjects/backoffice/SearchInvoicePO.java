package pageobjects.backoffice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import java.util.List;

public class SearchInvoicePO {
    WebDriver driver;
    SeleniumHelpers selenium;
    JavaHelpers java;

    public SearchInvoicePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);


        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[.='Change Status']")
    private WebElement changeStatusButton;

    @FindBy(xpath = "//select[@name='status']")
    private WebElement paymentStatus;

    @FindBy(xpath = "//input[@name='transaction_date']")
    private WebElement paymentDate;

    @FindBy(xpath = "//input[@class='btn btn-primary btn-md']")
    private WebElement submitButton;

    @FindBy(css = "[name='search_by']")
    private WebElement dropSearchBy;

    @FindBy(css = "[name='search_value']")
    private WebElement inpSearchBySearchValue;

    @FindBy(css = "tr:first-of-type td a:first-of-type + a")
    private WebElement btnDetailFeeFirstIndex;

    private By btnDetailFeeSecondIndex = By.xpath("//tr[2]//td[@class='invoice-action']//*[@class='btn btn-xs bg-maroon btn-flat']");

    @FindBy(xpath = "(//*[contains(text(), 'Settlement')])[1]/following-sibling::*//*[.='Detail Fee']")
    private WebElement btnDetailFeeSettlementInvoiceFirstType;

    @FindBy(xpath = "(//*[contains(text(), 'DP')])[1]/following-sibling::*//*[.='Detail Fee']")
    private WebElement btnDetailDPInvoiceFirstIndex;

    @FindBy(css = "input[value='Cari Invoice']")
    private WebElement btnCariInvoice;

    @FindBy(xpath = "(//*[contains(text(), 'DP')]/following-sibling::*//*[.='Detail Fee'])[1]")
    private WebElement btnDetailFeeDPInvoiceFirstIndex;

    @FindBy(name = "from_auto_extend")
    private WebElement autoExtendSelection;

    /**
     * Click on change status invoice button
     * @throws InterruptedException
     */
    public void clickOnChangeStatusButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(changeStatusButton);
    }

    /**
     * Choose payment status paid
     * @throws InterruptedException
     */
    public void selectPaymentStatus() throws InterruptedException {
        selenium.clickOn(paymentStatus);
        selenium.selectDropdownValueByText(paymentStatus,"Paid");
    }

    /**
     * Input payment date
     * @throws InterruptedException
     */
    public void setPaymentDate(String date) throws InterruptedException {
        selenium.clickOn(paymentDate);
        selenium.enterTextCharacterByCharacter(paymentDate, date, true);
    }

    /**
     * Click on submit button to update payment status
     * @throws InterruptedException
     */
    public void clickOnSubmitButton() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(submitButton);
    }

    /**
     * Set dropdown search
     * @param searchBy input with Invoice Number, Invoice Code, Renter Phone Number, Renter Name, Owner Phone Number
     */
    public void setSearchBy(String searchBy) {
        try {
            selenium.selectDropdownValueByText(dropSearchBy, searchBy);
        }catch (Exception e) {
            throw new IllegalArgumentException("Input with Invoice Number, Invoice Code, Renter Phone Number, Renter Name, Owner Phone Number");
        }
    }

    /**
     * Set search by value
     * @param value input with desired value
     */
    public void setSearchByValue(String value) throws InterruptedException {
        selenium.enterText(inpSearchBySearchValue, value, true);
        selenium.clickOn(btnCariInvoice);
        selenium.hardWait(5);
    }

    /**
     * Click on invoice detail fee first index
     * @throws InterruptedException
     */
    public void clickOnInvoiceDetailFeeFirstIndex() throws InterruptedException {
        selenium.clickOn(btnDetailFeeFirstIndex);
    }

    /**
     * Click on invoice detail fee second index
     * @throws InterruptedException
     */
    public void clickOnInvoiceDetailFeeSecondIndex() throws InterruptedException {
        selenium.clickOn(btnDetailFeeSecondIndex);
    }

    /**
     * Click on down payment invoice detail fee for first type of element
     * @throws InterruptedException
     */
    public void clickOnInvoiceDPFirstType() throws InterruptedException {
        selenium.clickOn(btnDetailDPInvoiceFirstIndex);
    }

    /**
     * Click on settlement invoice detail fee for first type of element
     * @throws InterruptedException
     */
    public void clickOnInvoiceSettlementFirstType() throws InterruptedException {
        selenium.clickOn(btnDetailFeeSettlementInvoiceFirstType);
    }

    /**
     * Click on first index Detail Fee from DP invoice
     * @throws InterruptedException
     */
    public void clickOnInvoiceDPFirstIndex() throws InterruptedException {
        selenium.clickOn(btnDetailFeeDPInvoiceFirstIndex);
    }

    /**
     * Get total amount by it index
     * @param invoiceIndex index input with number. By element index.
     * @return string data type
     */
    public String getTotalAmount(int invoiceIndex) {
        String totalAmountEl = "tr:nth-child("+ invoiceIndex +") td:nth-child(5)";
        return selenium.getText(driver.findElement(By.cssSelector(totalAmountEl)));
    }

    /**
     * Select on Auto Extend Selection
     * @param item input string that will define item value
     */
    public void selectOnAutoExtendSelection(String item){
        selenium.selectDropdownValueByText(autoExtendSelection, item);
    }

    /**
     * Get Auto Extend Selection Text
     * @param item input string that will define item value
     * @return string
     */
    public String getAutoExtendSelectionText(String item){
        return selenium.getText(By.xpath("//option[text()='"+item+"']"));
    }

    /**
     * Click on Search Invoice Button on Admin Search Invoice Page
     * @throws InterruptedException
     */
    public void clickOnSearchInvoiceButton() throws InterruptedException {
        selenium.clickOn(btnCariInvoice);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Pass results as List Web Element
     * @param value input string that define value itself
     * @param status input string that define status value
     * @return Web Element
     */
    public List<WebElement> getResultsElement(String value, String status){
        List<WebElement> results = driver.findElements(By.xpath("//span[text()='"+value+"']//parent::span[@class='label label-"+status+"']"));
        return results;
    }

    /**
     * Is Search Invoice Results True Auto Extend Value
     * @param value
     * @param status
     * @param counter
     * @return Web Element
     */
    public Boolean isSearchInvoiceResultsHaveAutoExtendCorrectValue(String value, String status, Integer counter){
        List<WebElement> results = driver.findElements(By.xpath("//span[text()='"+value+"']//parent::span[@class='label label-"+status+"']"));
        return selenium.waitInCaseElementVisible(results.get(counter), 3) != null;
    }

    /**
     * Get Auto Extend Value of Search Invoice Results
     * @param value input string that define value itself
     * @param status input string that define status value
     * @param counter integer input that define counter value
     * @return String
     */
    public String getAutoExtendValueOfSearchInvoiceResults(String value, String status, Integer counter){
        List<WebElement> results = driver.findElements(By.xpath("//span[text()='"+value+"']//parent::span[@class='label label-"+status+"']"));
        return selenium.getText(results.get(counter));
    }

    /**
     * Click on See Log Button
     * @param link input string that define link value
     * @throws InterruptedException
     */
    public void clickOnSeeLogButton(String link) throws InterruptedException {
        String el = "//a[@href='" + Constants.BACKOFFICE_BASE_URL + "backoffice/log/invoice"+link+"']";
        selenium.clickOn(By.xpath(el));
        selenium.switchToWindow(2);
    }
}
