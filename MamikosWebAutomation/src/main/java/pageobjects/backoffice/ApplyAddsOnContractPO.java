package pageobjects.backoffice;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ApplyAddsOnContractPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ApplyAddsOnContractPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(id = "selectAddOn")
    private WebElement selectAddOn;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement btnSubmit;

    @FindBy(id = "selectAddOn")
    private WebElement dropAddOns;

    @FindBy(css = ".callout.callout-success")
    private WebElement calloutSuccess;
    /**
     * Select drop down value by index
     * @param index drop down index
     * @throws InterruptedException
     */
    public void setAddOnByIndex(int index) throws InterruptedException {
        selenium.selectDropdownValueByIndex(selectAddOn, index);
        selenium.hardWait(5);
        selenium.clickOn(btnSubmit);
        selenium.acceptAlert();
        selenium.hardWait(5);
    }

    /**
     * Select add ons dropdown by it index
     * @param index desired index 1,2,3,4 etc
     */
    public void setAddOnDropDownByIndex(String index) throws InterruptedException {
        selenium.selectDropdownValueByIndex(dropAddOns, Integer.parseInt(index));
        selenium.clickOn(btnSubmit);
        selenium.hardWait(2);
        selenium.acceptAlert();
        selenium.waitTillElementIsVisible(calloutSuccess);
    }

    /**
     * Select add ons dropdown by it index
     * @param text add ons text value
     */
    public void setAddOnDropDownByText(String text) throws InterruptedException {
        selenium.selectDropdownValueByVisibleText(dropAddOns, text);
        selenium.clickOn(btnSubmit);
        selenium.hardWait(2);
        selenium.acceptAlert();
        selenium.waitTillElementIsVisible(calloutSuccess);
    }
}
