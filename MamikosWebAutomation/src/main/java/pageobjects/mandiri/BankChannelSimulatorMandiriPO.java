package pageobjects.mandiri;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BankChannelSimulatorMandiriPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public BankChannelSimulatorMandiriPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "payment_code")
    private WebElement billKey;

    @FindBy(id = "biller_code")
    private WebElement billCode;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement searchVirtualAccountButtonMandiri;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement paymentButton;

    @FindBy(xpath = "//*[text()='Payment Amount']/following-sibling::div")
    private WebElement paymentAmountLabel;

    @FindBy(xpath = "//*[@class='alert alert-success']")
    private WebElement mandiriPaymentMessage;



    /**
     * Enter virtual account number mandiri
     *@param virtualAccountNumber is numeric generate by system
     */
    public void enterTextVirtualAccountMandiri(String virtualAccountNumber) {
        selenium.enterText(billKey, virtualAccountNumber, false);
    }

    /**
     * Enter biller code mandiri
     *@param billerCode is numeric generate by system
     */
    public void enterTextBillerCodeMandiri(String billerCode) {
        selenium.enterText(billCode, billerCode, false);
    }

    /**
     * Click button for search data by virtual account number mandiri
     *@throws InterruptedException
     */
    public void clickOnSearchVirtualAccountMandiri() throws InterruptedException {
        selenium.clickOn(searchVirtualAccountButtonMandiri);
    }

    /**
     * Click payment button
     *@throws InterruptedException
     */
    public void clickOnPaymentMandiriButton() throws InterruptedException {
        selenium.waitInCaseElementClickable(paymentButton,60);
        selenium.clickOn(paymentButton);
    }

    /**
     * Get total amount
     *@return  total amount
     */
    public String getTotalAmountLabel() {
        return selenium.getText(paymentAmountLabel);
    }

    /**
     * Get message payment confirmation
     *@return message confirmation
     */
    public String getPaymentMessage() {
        return selenium.getText(mandiriPaymentMessage);
    }
}
