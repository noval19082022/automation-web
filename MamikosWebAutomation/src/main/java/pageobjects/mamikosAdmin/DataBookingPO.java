package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class DataBookingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public DataBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */
    @FindBy(xpath = "//div[@class='toggle-filter-head']")
    private WebElement showHideFilterButton;

    @FindBy(xpath = "//div[@class='toggle-filter-head']")
    private WebElement filterShowHideToggleButton;

    @FindBy(xpath = "//input[@name='phone']")
    private WebElement tenantPhoneTextField;

    @FindBy(id = "buttonSearch")
    private WebElement searchFilterButton;

    @FindBy(xpath = "(//button[@class='btn btn-default btn-sm dropdown-toggle'])[1]")
    private WebElement actionButtonFirstList;

    @FindBy(xpath = "(//a[@title='Edit Note'])[1]")
    private WebElement editConsultantNotesButtonFirstList;

    @FindBy(xpath = "(//a[@title='Lihat Detail'])[1]")
    private WebElement seeDetailBookingButtonFirstList;

    @FindBy(xpath = "//select[@id='swal-select-editnote']")
    private WebElement remarksSelectOption;

    @FindBy(xpath = "//input[@id='swal-input-editnote']")
    private WebElement notesTextField;

    @FindBy(xpath = "//button[@class='swal2-confirm swal2-styled']")
    private WebElement saveRemarksNotesButton;

    @FindBy(xpath = "((((//table[@class='table table-custom-striped']//tbody//tr)[1]//td)[6])//div)[2]")
    private WebElement remarksField;

    @FindBy(xpath = "((((//table[@class='table table-custom-striped']//tbody//tr)[1]//td)[6])//div)[3]")
    private WebElement notesField;

    @FindBy(xpath = "((((//table[@class='table table-custom-striped']//tbody//tr)[1]//td)[6])//div)[4]")
    private WebElement updatedByField;

    @FindBy(xpath = "(//h3[@class='box-title'])[7]")
    private WebElement notesSectionInDetailBooking;

    @FindBy(xpath = "(((//table[@class='table table-striped'])[6]//tbody//tr)[1]//td)[1]")
    private WebElement notesHistoryFirstRow;

    @FindBy(xpath = "(((//table[@class='table table-striped'])[6]//tbody//tr)[1]//td)[2]")
    private WebElement remarksHistoryFirstRow;

    @FindBy(xpath = "(((//table[@class='table table-striped'])[6]//tbody//tr)[2]//td)[1]")
    private WebElement notesHistorySecondRow;

    @FindBy(xpath = "(((//table[@class='table table-striped'])[6]//tbody//tr)[2]//td)[2]")
    private WebElement remarksHistorySecondRow;

    @FindBy(xpath = "(//span[@class='label label-info'])[3]")
    private WebElement firstTagLabel;

    @FindBy(xpath = "(//span[@class='label label-info'])[4]")
    private WebElement secondTagLabel;

    @FindBy(xpath = "(//span[@class='label label-info'])[5]")
    private WebElement thirdTagLabel;

    @FindBy(xpath = "(//span[@class='label label-info'])[6]")
    private WebElement fourthTagLabel;

    @FindBy(xpath = "(//span[@class='label label-info'])[7]")
    private WebElement fifthTagLabel;

    @FindBy(xpath = "(//span[@class='label label-info'])[8]")
    private WebElement sixthTagLabel;

    @FindBy(xpath = "(//table[@class='table table-custom-striped']//th)[6]")
    private WebElement tagRemarksColumn;

    @FindBy(id = "select2-kost_type-container")
    private WebElement kostTypeOption;

    @FindBy(xpath = "(//*[@class='select2-search__field'])[3]")
    private WebElement kostTypeSearch;

    @FindBy(xpath = "//tr[1]//li[3]//*[contains(@href, 'admin/booking/users/wizard/accept')]")
    private WebElement actionConfirmButton;

    @FindBy(xpath = "//tr[1]//li[5]//*[contains(@title, 'Tolak Booking')]")
    private WebElement btnActionRejected;

    @FindBy(css = "[style='display: block;'] .wizard-title")
    private WebElement displayingConfirmBookingTitlePage;

    @FindBy(xpath = "//select[@id='dp-percentage']/*[@selected]")
    private WebElement selectedPercentage;

    @FindBy(xpath = "//*[@name='dp_amount']")
    private WebElement activeDPAmount;

//    @FindBy(xpath = "//*[contains(text(), 'Konfirmasi')]/following-sibling::*//*[.='Konfirmasi']")
//    private WebElement confirmationButton;

    private By confirmationButton = By.xpath("//*[contains(text(), 'Konfirmasi')]/following-sibling::*//*[.='Konfirmasi']");

    @FindBy(xpath = "(//a[@title='Set Transfer Permission'])[1]")
    private WebElement transferPermissionButtonFirstList;

    @FindBy(xpath = "//div[@class='modal modal-info fade transfer-permission-modal in']//select[@name='status']")
    private WebElement editTransferPermissionStatus;

    @FindBy(xpath = "//select[@name='refund_reason']")
    private WebElement refundReason;

    @FindBy(xpath = "//div[@class='modal modal-info fade transfer-permission-modal in']//button[@class='btn btn-outline']")
    private WebElement sendButton;

    @FindBy(xpath = "//*[@class='alert alert-success alert-dismissable']")
    private WebElement successMessage;

    @FindBy(xpath = "//table[@class='table table-custom-striped']//tr[1]/td[4]")
    private WebElement transferPermissionStatus;

    @FindBy(css = ".input-flex input[placeholder='Nama Biaya']")
    private WebElement textOtherPriceNameAdditionalPrice;

    @FindBy(css = ".input-flex input[placeholder='Nominal']")
    private WebElement textOtherPriceNumberAdditonalPrice;

    @FindBy(css= ".table-biaya-tetap td:first-child")
    private WebElement textOtherPriceNameConfirmation;

    @FindBy(css = ".table-biaya-tetap td:last-child")
    private WebElement textOtherPriceNumberConfirmation;

    @FindBy(css = ".fade.in .no-padding select.form-control.reason")
    private WebElement dropRejectReason;

    @FindBy(css = ".fade.in .no-padding .otherReason textarea")
    private WebElement inputOtherReason;

    @FindBy(css = ".fade.in .forClose + button")
    private WebElement btnSendRejectReason;

    @FindBy(css = ".alert")
    private WebElement boxActionSuccess;

    @FindBy(xpath = "//tr[1]//li[4]//*[contains(@title, 'Batalkan Booking')]")
    private WebElement btnActionCancel;

    @FindBy(css = ".fade.in .otherReason .form-control")
    private WebElement inputCancelOther;

    @FindBy(xpath = "//*[@class='btn-group open']//*[@title='Set Transfer Permission']")
    private WebElement dropItemTransferPermission;

    @FindBy(css = ".fade.transfer-permission-modal.in [type='submit']")
    private WebElement pUpAllowTransferPermission;

    @FindBy(css = ".alert.alert-success.alert-dismissable")
    private WebElement alertActionSuccess;

    @FindBy(xpath = "//*[@class='btn-group open']//*[@title='Lihat Detail']")
    private WebElement liEyeDetail;

    @FindBy(xpath = "//*[@class='btn btn-xs btn-warning']")
    private WebElement batalkanDPButton;

    @FindBy(xpath = "//*[@id=\"query-form\"]/div/div[10]")
    private WebElement dropdownButtonBookingStatus;

    private By bookingNowButton = RelativeLocator.with(By.xpath("//*[.='Booking Now']")).near(By.xpath("//*[.='Data Booking']"));

    private By clickSelectRoomField = By.xpath("//*[@class='select2-selection select2-selection--single']");

    private By inputKostName = By.xpath("//*[@class='select2-search__field']");

    private By selectKostName = RelativeLocator.with(By.xpath("//*[@class='select2-results__option select2-results__option--highlighted']")).near(By.xpath("//*[@type='button'][.='next']"));

    private By nextButton = By.xpath("//*[@class='btn btn-primary next-step button-step-1']");

    private By phoneNumberField = By.id("inputTenant");

    private By searchButton = By.id("searchTenant");

    private By nextButtonStep2 = By.xpath("//*[@class='btn btn-primary next-step duration-step button-step-2']");

    private By step3MilestoneActive = By.xpath("//*[@class='active']//*[@href='#step3']");

    private By searchTenantDropdown = By.id("searchTenantCategory");

    private By searchByName = By.xpath("//*[@id='searchTenantCategory']//*[@value='name']");

    private By bookingTypeDropdown = By.id("bookingType");

    private By dateField = By.id("inputCheckin");

    private By nextButtonStep3 = By.xpath("//*[@class='btn btn-default next-step last-step button-step-3']");

    private By submitButton = By.xpath("//*[@class='btn btn-primary btn-info-full next-step']");

    private By contractIdField = By.id("inputOldContractIdCheck");

    private By checkContractIdButton = By.id("checkOldContract");

    private By dataBookingMessage = By.xpath("//*[@class='alert alert-success alert-dismissable']");

    private By contractValid = By.id("contract-valid-text");

    /**
     * click Tampilkan/Sembunyikan button to show/hide filter data booking
     * @throws InterruptedException
     */
    public void showOrHideFilter() throws InterruptedException {
        selenium.waitTillElementIsVisible(showHideFilterButton);
        selenium.clickOn(showHideFilterButton);
    }

    /**
     * Input tenant HP in filter field
     * @param tenantHP Tenant no HP
     */
    public void setTenantPhoneField(String tenantHP) {
        selenium.enterText(tenantPhoneTextField,tenantHP,true);
    }

    /**
     * Click Search button in Filter
     * @throws InterruptedException
     */
    public void applyFilter() throws InterruptedException {
        selenium.clickOn(searchFilterButton);
        selenium.waitTillElementIsNotVisible(searchFilterButton);
    }

    /**
     * Click Action Button in first data appear in list
     * @throws InterruptedException
     */
    public void clickActionButton() throws InterruptedException {
        selenium.refreshPage();
        selenium.waitTillElementIsVisible(actionButtonFirstList);
        selenium.clickOn(actionButtonFirstList);
    }

    /**
     * Search booking status
     * @throws InterruptedException
     */
    public void userSearchBookingStatus(String booked) throws InterruptedException {
        selenium.waitInCaseElementClickable(dropdownButtonBookingStatus,20);
        selenium.clickOn(dropdownButtonBookingStatus);
        String element = "//li[.='"+booked+"']";
        selenium.clickOn(By.xpath(element));
    }

    /**
     * Click next button in data booking
     * @throws InterruptedException
     */
    public void clickNextButton() throws InterruptedException {
        String nextButton = "article[id='wizard-step-1'] a[class='btn btn-success']";
        selenium.waitInCaseElementVisible(By.cssSelector(nextButton), 3);
        selenium.pageScrollInView(By.cssSelector(nextButton));
        selenium.javascriptClickOn(By.cssSelector(nextButton));
        String nextButton2 = "article[id='wizard-step-2'] a[class='btn btn-success']";
        selenium.waitInCaseElementVisible(By.cssSelector(nextButton2), 3);
        selenium.javascriptClickOn(By.cssSelector(nextButton2));
        String nextButton3 = "article[id='wizard-step-3'] a[class='btn btn-success']";
        selenium.waitInCaseElementVisible(By.cssSelector(nextButton3), 3);
        selenium.javascriptClickOn(By.cssSelector(nextButton3));
        String confirmationButton = "//a[normalize-space()='Konfirmasi']";
        selenium.waitInCaseElementVisible(By.xpath(confirmationButton), 3);
        selenium.javascriptClickOn(By.xpath(confirmationButton));
    }

    /**
     * Click Edit Consultant Note button in Action menu
     */
    public void clickEditConsultantNote() {
        selenium.javascriptClickOn(editConsultantNotesButtonFirstList);
    }

    /**
     * select remarks option in edit consultant notes
     * @param remarks
     */
    public void chooseRemarks(String remarks) {
        selenium.selectDropdownValueByText(remarksSelectOption,remarks);
    }

    /**
     * input notes in edit consultant notes
     * @param notes
     */
    public void setRemarksNotes(String notes) {
        selenium.enterText(notesTextField,notes,true);
    }

    /**
     * Click Save button in edit consultant notes
     * @throws InterruptedException
     */
    public void clickSaveRemarksNotes() throws InterruptedException {
        selenium.clickOn(saveRemarksNotesButton);
        selenium.hardWait(8);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Get remarks in first data of the list
     * @return String remarks
     */
    public String getRemarks() {
        String fullRemarks = selenium.getText(remarksField);
        String remarks = fullRemarks.substring(10);
        return remarks;
    }

    /**
     * Get notes in first data of the list
     * @return String notes
     */
    public String getNotes() {
        String fullNotes = selenium.getText(notesField);
        String notes = fullNotes.substring(7);
        return notes;
    }

    /**
     * Click Detail button in Action menu
     */
    public void clickDetailButton() {
        selenium.waitTillElementIsVisible(seeDetailBookingButtonFirstList);
        selenium.javascriptClickOn(seeDetailBookingButtonFirstList);
    }

    /**
     * Scroll page to Notes in Detail booking page
     */
    public void navigateToNotesColumn() {
        selenium.waitTillElementIsVisible(notesSectionInDetailBooking);
        selenium.pageScrollInView(notesSectionInDetailBooking);
    }

    /**
     * Get notes in detail booking row "x"
     * @param row row of table you want to get the notes
     * @return String notes
     */
    public String getNotesInDetailBooking(String row) {
        selenium.waitTillElementIsVisible(notesHistoryFirstRow);
        return selenium.getText(By.xpath("(((//table[@class='table table-striped'])[6]//tbody//tr)["+row+"]//td)[1]"));
    }

    /**
     * Get remarks in detail booking row "x"
     * @param row row of table you want to get the remarks
     * @return String remarks
     */
    public String getRemarksInDetailBooking(String row) {
        selenium.waitTillElementIsVisible(remarksHistoryFirstRow);
        return selenium.getText(By.xpath("(((//table[@class='table table-striped'])[6]//tbody//tr)["+row+"]//td)[2]"));
    }

    /**
     * Get first tags in first booking list
     * @return String first tag
     * @throws InterruptedException
     */
    public String getFirstTag() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(1);
        return selenium.getText(firstTagLabel);
    }

    /**
     * Get second tags in first booking list
     * @return String second tag
     * @throws InterruptedException
     */
    public String getSecondTag() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(1);
        return selenium.getText(secondTagLabel);
    }

    /**
     * Get third tags in first booking list
     * @return String third tag
     * @throws InterruptedException
     */
    public String getThirdTag() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(1);
        return selenium.getText(thirdTagLabel);
    }

    /**
     * Get fourth tags in first booking list
     * @return String fourth tag
     * @throws InterruptedException
     */
    public String getFourthTag() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(1);
        return selenium.getText(fourthTagLabel);
    }

    /**
     * Get fifth tags in first booking list
     * @return String fifth tag
     * @throws InterruptedException
     */
    public String getFifthTag() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(1);
        return selenium.getText(fifthTagLabel);
    }

    /**
     * Get sixth tags in first booking list
     * @return String sixth tag
     * @throws InterruptedException
     */
    public String getSixthTag() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(1);
        return selenium.getText(sixthTagLabel);
    }

    /**
     * scroll to tag and remarks column
     */
    public void navigateToTagRemarksColumn() {
        selenium.waitTillElementIsVisible(tagRemarksColumn);
        selenium.pageScrollInView(tagRemarksColumn);
    }

    /**
     * choose kos type filter in data booking
     * @param option name of option like "All", "All Testing"
     * @throws InterruptedException
     */
    public void setKosTypeFilter(String option) throws InterruptedException {
        selenium.click(kostTypeOption);
            selenium.enterText(kostTypeSearch,option,true);
            selenium.sendKeyEnter(kostTypeSearch);
    }

    /**
     * get Updated By String
     * @return
     */
    public String getUpdatedBy() {
        String fullUpdatedBy = selenium.getText(updatedByField);
        String updateby = fullUpdatedBy.substring(13);
        return updateby;
    }

    /**
     * Click on confirm button on action dropdown
     * @throws InterruptedException
     */
    public void clikcActionConfirmButton() throws InterruptedException {
        selenium.waitInCaseElementClickable(actionConfirmButton, 5);
        selenium.clickOn(actionConfirmButton);
    }

    /**
     * Get displaying text of confirm booking page.
     * @return string data type e.g "Tambahan Biaya"
     */
    private String getConfirmBookingPageTitleText() {
        return selenium.getText(displayingConfirmBookingTitlePage);
    }

    /**
     * Click on Continue button on confirm booking on data booking admin section
     * @param pageTitle input with page title
     * @throws InterruptedException
     */
    public void navigateToConfirmPage(String pageTitle) throws InterruptedException {
        String pageTitleText;
        String element;
        while (!getConfirmBookingPageTitleText().equalsIgnoreCase(pageTitle)) {
            pageTitleText = getConfirmBookingPageTitleText();
            element = "//*[contains(text(), '"+ pageTitleText + "')]/following-sibling::*//*[.='Lanjutkan']";
            selenium.clickOn(driver.findElement(By.xpath(element)));
        }
    }

    /**
     * Get selected active dp percentage
     * @return string data type e.g 20% dari harga sewa
     */
    public String getDownPaymentActivePercentage() {
        return selenium.getText(selectedPercentage);
    }

    /**
     * Get active dp amount
     * @return String data type e.g 100000
     */
    public String getDownPaymentActivePrice() {
        return selenium.getElementAttributeValue(activeDPAmount, "value");
    }

    /**
     * Click on button confirmation accept booking. Navigate data booking > confirmation > page 4 to find this button
     * @throws InterruptedException
     */
    public void clickOnConfirmationButton() throws InterruptedException {
        selenium.javascriptClickOn(confirmationButton);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
    }

    /**
     * Click on transfer permission action
     * @throws InterruptedException
     */
    public void clickOnSetTransferPermissionButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(transferPermissionButtonFirstList);
        selenium.clickOn(transferPermissionButtonFirstList);
    }

    /**
     * Choose transfer permission status allow refund
     * @throws InterruptedException
     */
    public void setTransferPermissionStatus() throws InterruptedException {
        selenium.waitTillElementIsVisible(editTransferPermissionStatus);
        selenium.clickOn(editTransferPermissionStatus);
        selenium.selectDropdownValueByText(editTransferPermissionStatus,"Allow Refund");
    }

    /**
     * Choose refund reason
     * @throws InterruptedException
     */
    public void setRefundReason() throws InterruptedException {
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(refundReason);
        selenium.clickOn(refundReason);
        selenium.selectDropdownValueByText(refundReason,"Tidak jadi sewa kos");
        selenium.waitTillElementIsVisible(sendButton);
    }

    /**
     * Click on submit update transfer permission status
     * @throws InterruptedException
     */
    public void clickOnSendButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(sendButton);
        selenium.clickOn(sendButton);
    }

    /**
     * Get transfer permission status of data booking
     * @return String transfer permission status
     */
    public String getTransferPermissionStatus() {
        selenium.waitTillElementIsVisible(successMessage);
        return selenium.getText(transferPermissionStatus);
    }

    /**
     * Get additional price name text by taking value from the element
     * @return string data type e.g "Biaya cuci gudang"
     */
    public String getOtherPriceNameOnTambahanBiaya() {
        return selenium.getElementAttributeValue(textOtherPriceNameAdditionalPrice, "value");
    }

    /**
     * Get additional price number text by taking value from the element
     * @return string data type e.g "Rp50000"
     */
    public String getOtherPriceNumberOnTambahanBiaya() {
        return selenium.getElementAttributeValue(textOtherPriceNumberAdditonalPrice, "value");
    }

    /**
     * Get text other price name on confirmation screen
     * @return string data type "Biaya cuci mobil"
     */
    public String getOtherPriceNameOnConfirmation() {
        return selenium.getText(textOtherPriceNameConfirmation);
    }

    /**
     * Get text other price number on confirmation screen
     * @return string data type "50000"
     */
    public String getOtherPriceNumberOnConfirmation() {
        return selenium.getText(textOtherPriceNumberConfirmation);
    }

    /**
     * Click on rejected button on active action list
     * @throws InterruptedException
     */
    public void clickOnRejectedListButton() throws InterruptedException {
        selenium.clickOn(btnActionRejected);
    }

    /**
     * Choose reject reason on active pop-up reject reason
     * @param rejectReason input with reject reason "Kamar kos sudah ada yang booking"
     */
    public void chooseRejectReason(String rejectReason) {
        selenium.selectDropdownValueByVisibleText(dropRejectReason, rejectReason);
    }

    /**
     * Click on button send on active reject reason
     * @throws InterruptedException
     */
    public void clickOnSendButtonRejectBooking() throws InterruptedException {
        selenium.clickOn(btnSendRejectReason);
        selenium.hardWait(2);
        selenium.waitTillElementIsVisible(boxActionSuccess, 30);
    }

    /**
     * Input other reason , trigger other reason by selecting reject booking with "Lainnya"
     * @param otherReason
     */
    public void setOtherReasonText(String otherReason) {
        selenium.enterText(inputOtherReason, otherReason, false);
    }

    /**
     * Click on button action cancel
     */
    public void clickOnCancelListButton() {
        selenium.javascriptClickOn(btnActionCancel);
    }

    /**
     * Select cancel reason
     * @param cancelReason cancel reason by text e.g "Lainnya"
     */
    public void chooseCancelReason(String cancelReason) {
        selenium.selectDropdownValueByText(dropRejectReason, cancelReason);
    }

    /**
     * Input cancel other reason text
     * @param cancelReasonText input with other cancel reason e.g "Cancel dari admin"
     */
    public void setCancelOtherReasonText(String cancelReasonText) {
        selenium.enterText(inputCancelOther, cancelReasonText, false);
    }

    /**
     * Allow disbursement for selected phone
     * @param tenantPhone desired phone number
     * @throws InterruptedException
     */
    public void allowDisbursement(String tenantPhone) throws InterruptedException {
        String actionElement = "(//*[.='"+tenantPhone+"']/preceding::button[@data-toggle='dropdown'])[1]";
        selenium.clickOn(driver.findElement(By.xpath(actionElement)));
        selenium.waitTillElementIsVisible(dropItemTransferPermission);
        selenium.javascriptClickOn(dropItemTransferPermission);
        selenium.waitTillElementIsVisible(pUpAllowTransferPermission);
        selenium.hardWait(3);
        selenium.javascriptClickOn(pUpAllowTransferPermission);
        selenium.hardWait(3);
        selenium.waitTillElementIsVisible(alertActionSuccess);
    }

    /**
     * Go to data booking detail based on phone number index number 1
     * @param tenantPhone tenant phone number
     * @throws InterruptedException
     */
    public void goToDataBookingDetailIndex(String tenantPhone) throws InterruptedException {
        String actionElement = "(//*[.='"+tenantPhone+"']/preceding::button[@data-toggle='dropdown'])[1]";
        selenium.clickOn(By.xpath(actionElement));
        selenium.hardWait(3);
        selenium.clickOn(liEyeDetail);
    }

    /**
     * Go to invoice detail on contract log
     * @param index number 1,2,3,4
     * @throws InterruptedException
     */
    public void goToInvoiceDetail(String index) throws InterruptedException {
        String invoiceElement = "(//*[.='Contract Detail']//parent::*/following-sibling::div//td/a)["+index+"]";
        selenium.waitTillElementIsVisible(By.xpath(invoiceElement), 20);
        selenium.clickOn(By.xpath(invoiceElement));
        selenium.switchToWindow(5);
    }

    /**
     * click batalkan button in Down Payment steps Confirm Booking, to disable DP
     * @throws InterruptedException
     */
    public void cancelDownPayment() throws InterruptedException {
        selenium.waitTillElementIsClickable(batalkanDPButton);
        selenium.clickOn(batalkanDPButton);
    }

     /** Click Booking Now Button
     */
    public void clickBookingNowButton() throws InterruptedException {
        selenium.javascriptClickOn(bookingNowButton);
    }

    /**
     * Select Room by Nama Kost
     */
    public void selectRoomByNamaKost(String kostName) throws InterruptedException {
        selenium.clickOn(clickSelectRoomField);
        selenium.enterText(inputKostName, kostName, false);
        selenium.hardWait(5);
        selenium.clickOn(selectKostName);
        selenium.clickOn(nextButton);
    }

    /**
     * Fill Phone Number Field on Form Booking
     */
    public void fillPhoneNumberField(String phoneNumber) throws InterruptedException {
        selenium.enterText(phoneNumberField, phoneNumber, false);
    }

    /**
     * Click Search Button on Form Booking
     */
    public void clickSearchButton() throws InterruptedException {
        selenium.clickOn(searchButton);
    }

    /**
     * Click the Next button on form booking step 2
     */
    public void clickNextButtonStep2() throws InterruptedException {
        selenium.hardWait(5);
        selenium.clickOn(nextButtonStep2);
    }

    /**
     * Verify Step 3 is displayed
     * @return boolean
     */
    public Boolean isStep3Displayed() throws InterruptedException {
        return selenium.waitInCaseElementVisible(step3MilestoneActive, 10)!=null;
    }

    /**
     * Choose search tenant by name
     *
     * @throws InterruptedException
     */
    public void userChooseSearchTenantByName() throws InterruptedException {
        selenium.clickOn(searchTenantDropdown);
        selenium.clickOn(searchByName);
    }

    /**
     * verify pop up error on form booking
     */
    public void verifyPopUpError(String popUpMessage) throws InterruptedException {
        String message = driver.switchTo().alert().getText();
        Assert.assertEquals(popUpMessage, message);
    }

    /**
     * choose booking type on form booking
     *
     * @throws InterruptedException
     */
    public void userChooseBookingType(String bookingType) throws InterruptedException {
        selenium.clickOn(bookingTypeDropdown);
        selenium.clickOn(By.xpath("//*[@id='bookingType']//*[@value='" + bookingType + "']"));
    }

    /**
     * Fill Date Field on Form Booking
     */
    public void fillDateField(String date) throws InterruptedException {
        selenium.enterText(dateField, date, false);
    }

    /**
     * Click the Next button on form booking step 3
     */
    public void clickNextButtonStep3() throws InterruptedException {
        selenium.clickOn(nextButtonStep3);
    }

    /**
     * Click the Submit button on form booking
     */
    public void clickSubmitButton() throws InterruptedException {
        selenium.clickOn(submitButton);
    }

    /**
     * Fill Date Field on Form Booking
     */
    public void fillContractIdField(String contractId) throws InterruptedException {
        selenium.enterText(contractIdField, contractId, false);
        selenium.clickOn(checkContractIdButton);
    }

    /**
     * Verify success message on data booking is appear
     * @return boolean
     */
    public Boolean isSuccessMessageAppear() throws InterruptedException {
        return selenium.waitInCaseElementVisible(successMessage, 10)!=null;
    }

    /**
     * Verify Contract Valid message is displayed
     * @return boolean
     */
    public Boolean isContractValidMessageDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementVisible(contractValid, 10)!=null;
    }
}
