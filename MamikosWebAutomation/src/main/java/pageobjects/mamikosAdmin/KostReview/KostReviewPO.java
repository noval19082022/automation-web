package pageobjects.mamikosAdmin.KostReview;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class KostReviewPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public KostReviewPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(css = "a[href='https://jambu.kerupux.com/admin/review/external/create']")
    private WebElement createReviewButton;

    @FindBy(id = "inputAnonymous")
    private WebElement anonymousSelection;

    @FindBy(id = "selectReviewType")
    private WebElement reviewTypeSelection;

    @FindBy(id = "inputSelectTagType")
    private WebElement selectTagTypeSelection;

    @FindBy(name = "tenant_name")
    private WebElement tenantNameInput;

    @FindBy(name = "tenant_phone_number")
    private WebElement tenantPhoneNumberInput;

    @FindBy(id = "kost-name")
    private WebElement kostNameInput;

    @FindBy(css = ".search-kost")
    private WebElement searchKostButton;

    @FindBy(name = "contract_start_date")
    private WebElement startDateInput;

    @FindBy(name = "contract_end_date")
    private WebElement endDateInput;

    @FindBy(css = "a[data-handler='next']")
    private WebElement nextDateButton;

    @FindBy(name = "clean")
    private WebElement cleanessSelection;

    @FindBy(name = "happy")
    private WebElement happySelection;

    @FindBy(name = "room_facilities")
    private WebElement roomFacilitiesSelection;

    @FindBy(name = "safe")
    private WebElement safetySelection;

    @FindBy(name = "public_facilities")
    private WebElement publicFacilitesSelection;

    @FindBy(name = "pricing")
    private WebElement pricingSelection;

    @FindBy(name = "content")
    private WebElement contentInput;

    @FindBy(css = ".custom-button__save")
    private WebElement saveButton;

    @FindBy(xpath = "//div[@class='alert alert-success alert-dismissable']")
    private WebElement alert;

    @FindBy(xpath = "//a[@onclick=\"return confirm('Yakin mau hapus saya?')\"]")
    private List<WebElement> deleteReviewButton;

    @FindBy(css = ".custom-button__cancel")
    private WebElement cancelButton;

    @FindBy(id = "tenantContractField")
    private WebElement tenantContractIDInput;

    @FindBy(css = "button[onclick=\"getTenantContractInfo();\"]")
    private WebElement tenantContractInfoButton;

    @FindBy(xpath = "//a[@class='btn btn-xs btn-default']")
    private List<WebElement> editButton;

    @FindBy(id = "selectReviewStatus")
    private WebElement kostReviewStatusSelection;

    @FindBy(xpath = "//tr//td[5]")
    private List<WebElement> contentLists;

    @FindBy(xpath = "//a[@class='btn btn-xs btn-success']")
    private List<WebElement> liveButton;

    @FindBy(xpath = "//a[@onclick=\"return preventDoubleEvent(this);\"][@class=\"btn btn-xs btn-danger\"]")
    private List<WebElement> rejectButton;

    /**
     * Click on Create Review Button
     * @throws InterruptedException
     */
    public void clickOnCreateReviewButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(createReviewButton, 3);
        selenium.clickOn(createReviewButton);
    }

    /**
     * Fill First Create Review Data
     * @param anon input string that will match item selection for anonymous selection field
     * @param reviewType input string that will match item selection for review type selection field
     * @param ota input string that will match item selection for tag type selection field
     * @param tenantName input string that will be used to fill tenant name input
     * @param tenantPhone input string that will be used to fill tenant phone number input
     * @param kostName input string that will be used to fill kost name input
     * @param startDate input string that will be used to fill date time input
     * @param endDate input string that will be used to fill date time input
     * @throws InterruptedException
     */
    public void fillFirstCreateReviewWithoutContractData(String anon, String reviewType, String ota, String tenantName, String tenantPhone, String kostName, String startDate, String endDate) throws InterruptedException {
        selenium.selectDropdownValueByText(anonymousSelection, anon);
        if (!selenium.getElementAttributeValue(reviewTypeSelection, "type").equalsIgnoreCase("hidden"))
            selenium.selectDropdownValueByText(reviewTypeSelection, reviewType);
        selenium.selectDropdownValueByText(selectTagTypeSelection, ota);
        selenium.enterText(tenantNameInput, tenantName, true);
        selenium.enterText(tenantPhoneNumberInput, tenantPhone, true);
        selenium.enterText(kostNameInput, kostName, true);
        selenium.clickOn(searchKostButton);
        selenium.hardWait(3);
        selenium.clickOn(startDateInput);
        selenium.clickOn(By.xpath("//a[text()='"+startDate+"']"));
        selenium.clickOn(endDateInput);
        if (startDate.equalsIgnoreCase("30") || startDate.equalsIgnoreCase("31"))
            selenium.clickOn(nextDateButton);
        selenium.clickOn(By.xpath("//a[text()='"+endDate+"']"));
    }

    /**
     * Fill Second Create Review Data
     * @param clean input string that will match item selection for cleanness
     * @param happy input string that will match item selection for happiness
     * @param roomFac input string that will match item selection for room facilities
     * @param safety input string that will match item selection for safety
     * @param pubFac input string that will match item selection for public facilities
     * @param priceAdj input string that will match item selection for price adjustment
     * @param content input string that will be used to fill content value
     */
    public void fillSecondCreateReviewData(String clean, String happy, String roomFac, String safety, String pubFac, String priceAdj, String content) throws InterruptedException {
        selenium.selectDropdownValueByText(cleanessSelection, clean);
        selenium.selectDropdownValueByText(happySelection, happy);
        selenium.selectDropdownValueByText(roomFacilitiesSelection, roomFac);
        selenium.selectDropdownValueByText(safetySelection, safety);
        selenium.selectDropdownValueByText(publicFacilitesSelection, pubFac);
        selenium.selectDropdownValueByText(pricingSelection, priceAdj);
        selenium.enterText(contentInput, content, true);
    }

    /**
     * Is Success Alert Appear?
     * @return true or false
     */
    public Boolean isAlertAppear() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(alert, 40) != null;
    }

    /**
     * Get Success Alert Text
     * @return string
     */
    public String getAlertText(){
        return selenium.getText(alert);
    }

    /**
     * Click on Delete Review Button
     * @throws InterruptedException
     */
    public void clickOnDeleteReviewButton(String content) throws InterruptedException {
        for (int i = 0; i < deleteReviewButton.size(); i++) {
            if(contentLists.get(i).getText().equalsIgnoreCase(content)){
                selenium.waitInCaseElementClickable(deleteReviewButton.get(i), 10);
                selenium.javascriptClickOn(deleteReviewButton.get(i));
                selenium.acceptAlert();
            }
        }
    }

    /**
     * Click on Save Button
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Click on Cancel Button
     * @throws InterruptedException
     */
    public void clickOnCancelButton() throws InterruptedException {
        selenium.clickOn(cancelButton);
    }

    /**
     * Fill First Create Review With Contract Data
     * @param anon input string that will match item selection for anonymous
     * @param reviewType input string that will match item selection for review type selection field
     * @param contractID input string that will used to fill contract id value
     * @throws InterruptedException
     */
    public void fillFirstCreateReviewWithContractData(String anon, String reviewType, String contractID) throws InterruptedException {
        selenium.selectDropdownValueByText(anonymousSelection, anon);
        if (!selenium.getElementAttributeValue(reviewTypeSelection, "type").equalsIgnoreCase("hidden"))
            selenium.selectDropdownValueByText(reviewTypeSelection, reviewType);
        selenium.enterText(tenantContractIDInput, contractID, true);
        selenium.clickOn(tenantContractInfoButton);
    }

    /**
     * Click on Edit Kost Review Button
     * @throws InterruptedException
     */
    public void clickOnEditButton(String content) throws InterruptedException {
        for (int i = 0; i < editButton.size(); i++) {
            if(contentLists.get(i).getText().equalsIgnoreCase(content)){
                selenium.waitInCaseElementClickable(editButton.get(i), 10);
                selenium.clickOn(editButton.get(i));
            }
        }
    }

    /**
     * Change Kost Review Status
     * @param status
     */
    public void changeKostReviewStatus(String status){
        selenium.selectDropdownValueByText(kostReviewStatusSelection, status);
    }

    /**
     * Get Tenant Name Input Attribute
     * @return string
     */
    public String getTenantNameInputAttribute(){
        return selenium.getElementAttributeValue(tenantNameInput, "class");
    }

    /**
     * Get Tenant Phone Number Attribute
     * @return string
     */
    public String getTenantPhoneNumberAttribute(){
        return selenium.getElementAttributeValue(tenantPhoneNumberInput, "class");
    }

    /**
     * Get Kost Name Attribute
     * @return string
     */
    public String getKostNameAttribute(){
        return selenium.getElementAttributeValue(kostNameInput, "class");
    }

    /**
     * Get Start Date Input Attribute
     * @return string
     */
    public String getStartDateInputAttribute(){
        return selenium.getElementAttributeValue(startDateInput, "class");
    }

    /**
     * Get End Date Input Attribute
     * @return string
     */
    public String getEndDateInputAttribute(){
        return selenium.getElementAttributeValue(endDateInput, "class");
    }

    /**
     * Click on Live Button
     * @throws InterruptedException
     */
    public void clickOnLiveButton(String content) throws InterruptedException {
        for (int i = 0; i < liveButton.size(); i++) {
            if(contentLists.get(i).getText().equalsIgnoreCase(content)){
                selenium.waitInCaseElementClickable(liveButton.get(i), 10);
                selenium.clickOn(liveButton.get(i));
            }
        }
    }

    /**
     * Click on Reject Button
     * @throws InterruptedException
     */
    public void clickOnRejectButton(String content) throws InterruptedException {
        for (int i = 0; i < rejectButton.size(); i++) {
            if(contentLists.get(i).getText().equalsIgnoreCase(content)){
                selenium.waitInCaseElementClickable(rejectButton.get(i), 10);
                selenium.clickOn(rejectButton.get(i));
            }
        }
    }

}
