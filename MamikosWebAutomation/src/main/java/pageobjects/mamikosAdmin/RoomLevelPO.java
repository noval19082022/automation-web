package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;
import utilities.Constants;
import utilities.SeleniumHelpers;

import static org.testng.Assert.assertFalse;

public class RoomLevelPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public RoomLevelPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(.,'Add Room Level')]")
    private WebElement roomLevelButton;

    @FindBy(id = "level-name")
    private WebElement levelNameField;

    @FindBy(id = "level-notes")
    private WebElement levelNotesField;

    @FindBy(id = "level-charging-name")
    private WebElement chargingNameField;

    @FindBy(id = "level-count")
    private WebElement chargingFeeField;

    @FindBy(name = "charging_type")
    private WebElement chargingTypeField;

    @FindBy(name = "q")
    private WebElement keywordSearchField;

    @FindBy(id = "buttonSearch")
    private WebElement searchButton;

    @FindBy(xpath = "//i[@class='fa fa-pencil']")
    private WebElement editIcon;

    @FindBy(xpath = "//i[@class='fa fa-trash']")
    private WebElement deleteIcon;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    private WebElement saveButton;

    private By roomLevelName = By.xpath("//tbody[1]//td[2]");

    /**
     * Click on Add Room level Button
     *
     * @throws InterruptedException
     */
    public void clickOnRoomLevelButton() throws InterruptedException {
        selenium.clickOn(roomLevelButton);
    }

    /**
     * Set Room Level Name
     *
     * @param name room level
     */
    public void setNewRoomLevelName(String name){
        selenium.enterText(levelNameField, name, true);
    }

    /**
     * Set Room Level Notes
     *
     * @param notes room level
     */
    public void setNewRoomLevelNotes(String notes){
        selenium.enterText(levelNotesField, notes, true);
    }

    /**
     * Set Room Level Charging Name
     *
     * @param chargingName room level
     */
    public void setNewRoomLevelChargingName(String chargingName){
        selenium.enterText(chargingNameField, chargingName, true);
    }

    /**
     * Set Room Level Charging Fee
     *
     * @param chargingFee room level
     */
    public void setNewRoomLevelChargingFee(String chargingFee){
        selenium.enterText(chargingFeeField, chargingFee, true);
    }

    /**
     * Set Room Level Charging Type
     *
     * @param chargingType room level
     */
    public void setNewRoomLevelChargingType(String chargingType) throws InterruptedException {
        selenium.clickOn(chargingTypeField);
        selenium.clickOn(By.xpath("//option[.='"+chargingType+"']"));
    }

    /**
     * Set Keyword on Search Field
     *
     * @param keyword Keyword
     */
    public void setKeywordSearchField(String keyword){
        selenium.enterText(keywordSearchField, keyword, false);
    }

    /**
     * Click on Search Button
     *
     * @throws InterruptedException
     */
    public void clickOnSearchRoomLevelButton() throws InterruptedException {
        selenium.clickOn(searchButton);
    }

    /**
     * Get Text of Room Level Name in Room Level Page
     * @return Text of Room Level Name
     */
    public String getRoomLevelName() {
        return selenium.getText(roomLevelName);
    }

    /**
     * Check if Filter Result is not Present
     *
     * @return boolean
     */
    public boolean checkResultFilterIsNotPresent(){
        return selenium.waitInCaseElementPresent(roomLevelName,5)==null;
    }

    /**
     * Click on Edit Icon
     *
     */
    public void clickOnEditRoomLevel() {
        selenium.javascriptClickOn(editIcon);
    }

    /**
     * Click on Delete Icon
     *
     */
    public void clickOnDeleteRoomLevel() {
        selenium.javascriptClickOn(deleteIcon);
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * Click on Save Button
     *
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }
}