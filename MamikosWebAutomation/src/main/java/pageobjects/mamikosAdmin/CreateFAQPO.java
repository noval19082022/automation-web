package pageobjects.mamikosAdmin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class CreateFAQPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public CreateFAQPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "faq-question")
    private WebElement questionEditText;

    @FindBy(id = "faq-answer")
    private WebElement answerEditText;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    private WebElement saveButton;

    /**
     * Enter question
     * @param questions
     */
    public void enterQuestion(String questions) {
        selenium.enterText(questionEditText, questions, true);
    }

    /**
     * Enter answer
     * @param answer
     */
    public void enterAnswer(String answer) {
        selenium.enterText(answerEditText, answer, true);
    }

    /**
     * Click on save button
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }
}
