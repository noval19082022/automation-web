package pageobjects.mamikosAdmin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class CreateKostLevelPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public CreateKostLevelPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);

    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "level-name")
    private WebElement levelNameEditText;

    @FindBy(id = "level-key")
    private WebElement levelKeyEditText;

    @FindBy(id = "level-order")
    private WebElement orderDropdown;

    @FindBy(id = "level-partner")
    private WebElement isPartnerLevelDropdown;

    @FindBy(id = "has-value")
    private WebElement levelValuesDropdown;

    @FindBy(id = "level-hidden")
    private WebElement statusDropdown;

    @FindBy(id = "level-notes")
    private WebElement notesEditText;

    @FindBy(xpath = "//select[@name='charging_type']")
    private WebElement chargingTypeDropdown;

    @FindBy(id = "room-level")
    private WebElement roomLevelDropdown;

    @FindBy(id = "submit")
    private WebElement saveButton;

    @FindBy(xpath = "//button[text()='Yes, save it']")
    private WebElement yesButton;

    @FindBy(xpath = "//button[text()='OK']")
    private WebElement okButton;

    /**
     * Enter kost level name
     * @param levelName
     */
    public void enterLevelName(String levelName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.enterText(levelNameEditText, levelName, true);
    }

    /**
     * Enter kost level key
     * @param keyLevel
     */
    public void enterKey(String keyLevel) {
        selenium.pageScrollInView(levelKeyEditText);
        selenium.enterText(levelKeyEditText, keyLevel, true);
    }

    /**
     * Select order value
     * @param order
     */
    public void SelectOrder(String order) {
        selenium.selectDropdownValueByText(orderDropdown, order);
    }

    /**
     * Select is partner level
     * @param partnerLevel
     */
    public void selectIsPartnerLevel(String partnerLevel) {
        selenium.selectDropdownValueByText(isPartnerLevelDropdown, partnerLevel);
    }

    /**
     * Select set level values
     * @param setLevelValues
     */
    public void selectSetLevelValues(String setLevelValues) {
        selenium.selectDropdownValueByText(levelValuesDropdown, setLevelValues);
    }

    /**
     * Select status
     * @param status
     */
    public void selectStatus(String status) {
        selenium.selectDropdownValueByText(statusDropdown, status);
    }

    /**
     * Enter notes
     * @param notes
     */
    public void enterNotes(String notes) {
        selenium.enterText(notesEditText, notes,true);
    }

    /**
     * Select charging type
     * @param chargingType
     */
    public void selectChargingType(String chargingType) {
        selenium.selectDropdownValueByText(chargingTypeDropdown, chargingType);
    }

    /**
     * Enter room level
     * @param roomLevel
     */
    public void selectRoomLevel(String roomLevel) {
        selenium.pageScrollInView(roomLevelDropdown);
        selenium.selectDropdownValueByText(roomLevelDropdown, roomLevel);
    }

    /**
     * Click on yes button for save kost level
     * @throws
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.pageScrollInView(saveButton);
        selenium.clickOn(saveButton);
    }

    /**
     * Click on ok button on popup successfully added kost level
     * @throws
     */
    public void clickOnYesSaveKostLevelButton() throws InterruptedException {
        selenium.clickOn(yesButton);
    }

    /**
     * Click on ok button on popup successfully added kost level
     * @throws
     */
    public void clickOnOkLevelSuccessfullyAddedButton() throws InterruptedException {
        selenium.clickOn(okButton);
    }
}
