package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class FaqPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public FaqPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[(contains(@href,'create#kost-level'))]")
    private WebElement addLevelQAQButton;

    @FindBy(xpath = "//input[@placeholder='Search']")
    private WebElement searchEditText;

    @FindBy(id = "buttonSearch")
    private WebElement searchButton;

    @FindBy(xpath = "//table[@class='table table-striped']//th[contains(text(),'Question')]")
    private WebElement questionsColumn;

    @FindBy(xpath = "//table[@class='table table-striped']//th[contains(text(),'Answer')]")
    private WebElement answerColumn;

    @FindBy(xpath = "//table[@class='table table-striped']//th[contains(text(),'Action')]")
    private WebElement actionColumn;

    @FindBy(xpath = "//table[@class='table table-striped']//tbody/tr")
    private WebElement listLevelFaqNotEmpty;

    @FindBy(xpath = "//ul[@class='pagination']")
    private WebElement pagination;

    @FindBy(xpath = "//i[@class='fa fa-trash']")
    private WebElement firstTrashIcon;

    /**
     * Verify the content on the faq list page
     * @return Boolean element status
     */
    public boolean verifyContentListFAQ() {
        return (selenium.isElementDisplayed(addLevelQAQButton) &&
                selenium.isElementDisplayed(searchEditText) &&
                selenium.isElementDisplayed(searchButton) &&
                selenium.isElementDisplayed(questionsColumn) &&
                selenium.isElementDisplayed(answerColumn) &&
                selenium.isElementDisplayed(actionColumn) &&
                selenium.isElementDisplayed(pagination));
    }

    /**
     * Click on add level faq button
     * @throws InterruptedException
     */
    public void clickOnAddLevelFaqButton() throws InterruptedException {
        selenium.clickOn(addLevelQAQButton);
    }

    /**
     * Click on add level faq button
     * @return String value faq list column
     */
    public String getValueColumnFaq(int i) {
        return selenium.getText(By.xpath("//tbody//td[" + i + "]"));
    }

    /**
     * Search level faq
     * @return String value faq list column
     */
    public void searchLevelFaq(String question) throws InterruptedException {
        selenium.enterText(searchEditText, question, true);
        selenium.clickOn(searchButton);
    }

    /**
     * Delete level faq
     * @throws InterruptedException
     */
    public void deleteFirstListLevelFaq() throws InterruptedException {
        if (listLevelFaqIsAppeared()){
            selenium.doubleClickOnElement(firstTrashIcon);
            selenium.waitTillAlertPresent();
            selenium.acceptAlert();
            selenium.hardWait(5);
        }
    }

    /**
     * List level faq is appeared
     * @return boolean status list level faq
     */
    public boolean listLevelFaqIsAppeared() {
        return selenium.isElementDisplayed(listLevelFaqNotEmpty);
    }
}
