package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class KostLevelPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public KostLevelPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(.,'Add Kost Level')]")
    private WebElement kostLevelButton;

    @FindBy(id = "level-name")
    private WebElement levelNameField;

    @FindBy(id = "level-partner")
    private WebElement partnerLevelDropdown;

    @FindBy(id = "level-hidden")
    private WebElement levelStatusDropdown;

    @FindBy(id = "level-notes")
    private WebElement levelNotesField;

    @FindBy(name = "benefits[]")
    private WebElement levelBenefitsField;

    @FindBy(name = "criterias[]")
    private WebElement levelCriteriasField;

    @FindBy(id = "level-count")
    private WebElement levelChargingFeeField;

    @FindBy(xpath = "//div[@class='note-editable panel-body']")
    private WebElement levelTermsAndConditionsField;

    @FindBy(name = "q")
    private WebElement keywordSearchField;

    @FindBy(id = "buttonSearch")
    private WebElement searchButton;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    private WebElement saveButton;

    @FindBy(xpath = "//button[@class='swal2-confirm swal2-styled']")
    private WebElement saveConfirmationButton;

    @FindBy(xpath = "//button[@class='swal2-confirm swal2-styled']")
    private WebElement okButtonOnTheSuccessfulPopup;

    @FindBy(xpath = "//i[@class='fa fa-pencil']")
    private WebElement editIcon;

    @FindBy(xpath = "//i[@class='fa fa-trash']")
    private WebElement deleteIcon;

    @FindBy(css = "b")
    private WebElement titleMessageAlert;

    @FindBy(xpath = "//tbody[1]//td[2]")
    private WebElement kostLevelName;

    /**
     * Click on Add Kost level Button
     * @throws InterruptedException
     */
    public void clickOnKostLevelButton() throws InterruptedException {
        selenium.clickOn(kostLevelButton);
    }

    /**
     * Set Kost Level Name
     * @param name kost level
     */
    public void setNewKostLevelName(String name){
        selenium.enterText(levelNameField, name, true);
    }

    /**
     * Select Kost level Partner Level
     * @param partnerLevel kost level
     * @throws InterruptedException
     */
    public void setNewKostLevelPartnerLevel(String partnerLevel) throws InterruptedException {
        selenium.clickOn(partnerLevelDropdown);
        selenium.clickOn(By.xpath("//option[.='"+partnerLevel+"']"));
    }

    /**
     * Select Kost level Status
     * @param status kost level
     * @throws InterruptedException
     */
    public void setNewKostLevelStatus(String status) throws InterruptedException {
        selenium.clickOn(levelStatusDropdown);
        selenium.clickOn(By.xpath("//option[.='"+status+"']"));
    }

    /**
     * Set Kost Level Notes
     * @param notes kost level
     */
    public void setNewKostLevelNotes(String notes){
        selenium.enterText(levelNotesField, notes, true);
    }

    /**
     * Set Kost Level Benefits
     * @param benefits kost level
     */
    public void setNewKostLevelBenefits(String benefits){
        selenium.enterText(levelBenefitsField, benefits, true);
    }

    /**
     * Set Kost Level Criterias
     * @param criterias kost level
     */
    public void setNewKostLevelCriterias(String criterias){
        selenium.enterText(levelCriteriasField, criterias, true);
    }

    /**
     * Set Kost Level Charging Fee
     * @param chargingFee kost level
     */
    public void setNewKostLevelChargingFee(String chargingFee){
        selenium.enterText(levelChargingFeeField, chargingFee, true);
    }

    /**
     * Set Kost Level Terms and Conditions
     * @param termsAndConditions kost level
     */
    public void setNewKostLevelTermsAndConditions(String termsAndConditions){
        selenium.enterText(levelTermsAndConditionsField, termsAndConditions, true);
    }

    /**
     * Set Keyword on Search Field
     * @param keyword Keyword
     */
    public void setKeywordSearchField(String keyword){
        selenium.enterText(keywordSearchField, keyword, false);
    }

    /**
     * Click on Search Button
     * @throws InterruptedException
     */
    public void clickOnSearchKostLevelButton() throws InterruptedException {
        selenium.clickOn(searchButton);
    }

    /**
     * Click on Edit Icon
     *
     */
    public void clickOnEditKostLevel() {
        selenium.javascriptClickOn(editIcon);
    }

    /**
     * Click on Delete Icon
     *
     */
    public void clickOnDeleteKostLevel() {
        selenium.javascriptClickOn(deleteIcon);
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * Click on Save Confirmation Button
     *
     */
    public void clickOnSaveConfirmationButton() throws InterruptedException {
        selenium.clickOn(saveConfirmationButton);
    }

    /**
     * Click ok button on the successful popup
     *
     */
    public void clickOnOkButtonOnTheSuccessfulPopup() throws InterruptedException {
        selenium.waitTillElementIsClickable(okButtonOnTheSuccessfulPopup);
        selenium.clickOn(okButtonOnTheSuccessfulPopup);
    }

    /**
     * Click on Save Button
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.hardWait(1);
        selenium.clickOn(saveButton);
    }

    /**
     * Get Text of Title Message Alert in Kost Level Page
     * @return Text of Title Messsage Allert
     */
    public String getTitleMessageAlert() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(titleMessageAlert);
    }

    /**
     * Get Text of Content Message Alert in Kost Level Page
     * @return Text of Content Messsage Alert
     */
    public String getContentMessageAlert(){
        WebElement myElement = driver.findElement(By.xpath("//div[@class='alert alert-success alert-dismissable']"));
        return ((JavascriptExecutor)driver).executeScript("return arguments[0].lastChild.textContent;", myElement).toString();
    }

    /**
     * Get Text of Kost Level Name in Kost Level Page
     * @return Text of Kost Level Name
     */
    public String getKostLevelName() {
        return selenium.getText(kostLevelName);
    }
}