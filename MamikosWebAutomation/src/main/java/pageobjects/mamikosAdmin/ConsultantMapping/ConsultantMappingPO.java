package pageobjects.mamikosAdmin.ConsultantMapping;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ConsultantMappingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ConsultantMappingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(., 'Add Mapping')]")
    private WebElement addMappingButton;

    @FindBy(id = "province")
    private WebElement provinceSelection;

    @FindBy(id = "city")
    private WebElement citySelection;

    @FindBy(id = "subdistrict")
    private WebElement subdistrictSelection;

    @FindBy(id = "single")
    private WebElement consultantSelection;

    @FindBy(xpath = "//span[@class='select2-selection select2-selection--single']")
    private List<WebElement> singleInputSelection;

    @FindBy(xpath = "//span[@class='select2-selection select2-selection--multiple']")
    private WebElement multipleInputSelection;

    @FindBy(xpath = "//label[@for='is_mamirooms']")
    private WebElement mamiroomsCheckBox;

    @FindBy(xpath = "//div[@class='alert alert-success alert-dismissable']")
    private WebElement alert;

    @FindBy(id = "create-mapping-submit-button")
    private WebElement saveChangesButton;

    @FindBy(name = "name")
    private WebElement searchByNameInput;

    @FindBy(id = "buttonSearch")
    private WebElement searchButton;

    @FindBy(id = "updateMapping")
    private List<WebElement> updateMappingButton;

    @FindBy(className = "btn-delete-mapping")
    private WebElement deleteMappingButton;

    @FindBy(className = "edit-update-data")
    private WebElement editMappingButton;

    /**
     * Click on Add Mapping Button
     * @throws InterruptedException
     */
    public void clickOnAddMappingButton() throws InterruptedException {
        selenium.waitInCaseElementClickable(addMappingButton, 5);
        selenium.clickOn(addMappingButton);
    }

    /**
     * Get Add Mapping Button Text
     * @return string
     */
    public String getAddMappingButtonText(){
        return selenium.getText(addMappingButton);
    }

    /**
     * Fill Add Mapping Data
     * @param province input string that will match province value
     * @param city input string that will match city value
     * @param subdistrict input string that will match subdistrict value
     * @param consultant input string that will match consultant value
     * @throws InterruptedException
     */
    public void fillAddMappingData(String province, String city, String subdistrict, String consultant) throws InterruptedException {
        String provinceTemp = "//li[text()='"+province+"']";
        String cityTemp = "//li[text()='"+city+"']";
        String subdistrictTemp = "//li[text()='"+subdistrict+"']";
        String consultantTemp = "//li[text()='"+consultant+"']";
        selenium.click(singleInputSelection.get(0));
        selenium.clickOn(By.xpath(provinceTemp));
        selenium.hardWait(3);
        selenium.click(singleInputSelection.get(1));
        selenium.clickOn(By.xpath(cityTemp));
        selenium.hardWait(3);
        selenium.clickOn(multipleInputSelection);
        selenium.waitInCaseElementVisible(By.xpath(subdistrictTemp), 5);
        selenium.clickOn(By.xpath(subdistrictTemp));
        selenium.hardWait(3);
        if (selenium.waitInCaseElementVisible(By.xpath(subdistrictTemp), 5) != null)
            selenium.clickOn(multipleInputSelection);
        selenium.clickOn(singleInputSelection.get(2));
        selenium.clickOn(By.xpath(consultantTemp));
        selenium.clickOn(mamiroomsCheckBox);
    }

    /**
     * Is Success Alert Appear?
     * @return true or false
     */
    public Boolean isAlertAppear() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(alert, 40) != null;
    }

    /**
     * Get Success Alert Text
     * @return string
     */
    public String getAlertText(){
        return selenium.getText(alert);
    }

    /**
     * Click on Save Changes Button
     * @throws InterruptedException
     */
    public void clickOnSaveChangesButton() throws InterruptedException {
        selenium.clickOn(saveChangesButton);
    }

    /**
     * Fill Search Mapping By Name
     * @param name
     */
    public void fillSearchMappingByName(String name){
        selenium.enterText(searchByNameInput, name, false);
    }

    /**
     * Click on Search Button
     * @throws InterruptedException
     */
    public void clickOnSearchButton() throws InterruptedException {
        selenium.clickOn(searchButton);
    }

    /**
     * Click On Update Mapping Button
     * @throws InterruptedException
     */
    public void clickOnUpdateMappingButton() throws InterruptedException {
        selenium.clickOn(updateMappingButton.get(0));
    }

    /**
     * Click On Delete Mapping Button
     */
    public void clickOnDeleteMappingButton() throws InterruptedException {
        selenium.waitInCaseElementVisible(deleteMappingButton, 5);
        selenium.waitForJavascriptToLoad();
        selenium.javascriptClickOn(deleteMappingButton);
    }

    /**
     * Click on Edit Mapping Button
     * @throws InterruptedException
     */
    public void clickOnEditMappingButton() throws InterruptedException {
        selenium.clickOn(editMappingButton);
    }

    /**
     * Add Kecamatan on Edit Mapping
     * @param subdistrict input string that will match subdistrict value
     * @throws InterruptedException
     */
    public void addKecamatanOnEditMapping(String subdistrict) throws InterruptedException {
        String subdistrictTemp = "//li[text()='"+subdistrict+"']";
        String multipleSelectionTemp = "//*[@id=\"edit-mapping\"]/div[1]/div/div[3]/div/span/span[1]/span/ul";

        selenium.hardWait(5);
        selenium.clickOn(By.xpath(multipleSelectionTemp)); // didn't know why, when used the same element is not working
        selenium.clickOn(By.xpath(subdistrictTemp));
        selenium.clickOn(By.xpath(multipleSelectionTemp));
        selenium.waitInCaseElementVisible(By.id("edit-mapping-submit-button"), 5);
        selenium.clickOn(By.id("edit-mapping-submit-button"));
    }
}
