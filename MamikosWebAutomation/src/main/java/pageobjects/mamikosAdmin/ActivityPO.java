package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ActivityPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public ActivityPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(.,'Add Activty')]")
    private WebElement addActivityButton;

    @FindBy(id = "activity-key")
    private WebElement keyField;

    @FindBy(id = "activity-name")
    private WebElement nameField;

    @FindBy(id = "activity-target")
    private WebElement targetDropdown;

    @FindBy(id = "activity-title")
    private WebElement titleField;

    @FindBy(css = ".note-editable")
    private WebElement descriptionField;

    @FindBy(id = "activity-sequence")
    private WebElement sequenceField;

    @FindBy(id = "activity-status")
    private WebElement statusDropdown;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    private WebElement saveButton;

    @FindBy(name = "q")
    private WebElement keywordSearchField;

    @FindBy(name = "s")
    private WebElement filterStatus;

    @FindBy(className = "pagination")
    private WebElement pagination;

    @FindBy(xpath = "//div[@class='alert alert-danger alert-dismissable']//li")
    private List<WebElement> activityValidationMessage;

    @FindBy(xpath = "//i[@class='fa fa-pencil']")
    private WebElement editIcon;

    @FindBy(xpath = "//i[@class='fa fa-trash']")
    private WebElement deleteIcon;

    @FindBy(xpath = "//th[.='ID']")
    private WebElement idColumnHeader;

    @FindBy(xpath = "//th[.='Key']")
    private WebElement keyColumnHeader;

    @FindBy(xpath = "//th[.='Name']")
    private WebElement nameColumnHeader;

    @FindBy(xpath = "//th[.='Title']")
    private WebElement titleColumnHeader;

    @FindBy(xpath = "//th[.='Target']")
    private WebElement targetColumnHeader;

    @FindBy(xpath = "//th[.='Status']")
    private WebElement statusColumnHeader;

    @FindBy(xpath = "//th[.='Order']")
    private WebElement orderColumnHeader;

    @FindBy(xpath = "//th[.='Actions']")
    private WebElement actionsColumnHeader;

    @FindBy(xpath = "//input[@name='media_type']")
    private WebElement uploadImageButton;

    private By filterResult = By.cssSelector("tbody > tr");

    /**
     * Click on Add Activity Button
     *
     * @throws InterruptedException
     */
    public void clickOnAddActivityButton() throws InterruptedException {
        selenium.clickOn(addActivityButton);
    }

    /**
     * Verify add activity button is visible
     * @return true if add activity button is visible
     */
    public boolean verifyActivityButton() {
        return selenium.waitInCaseElementVisible(addActivityButton,3) != null;
    }

    /**
     * Verify keyword filter field is visible
     * @return true if keyword filter field is visible
     */
    public boolean verifyKeywordFilterField() {
        return selenium.waitInCaseElementVisible(keywordSearchField, 3) != null;
    }

    /**
     * Verify filter status is visible
     * @return true if filter status is visible
     */
    public boolean verifyFilterStatus() {
        return selenium.waitInCaseElementVisible(filterStatus, 3) != null;
    }

    /**
     * Verify edit button is visible
     * @return true if edit button is visible
     */
    public boolean verifyEditButton() {
        return selenium.waitInCaseElementVisible(editIcon, 3) != null;
    }

    /**
     * Verify delete button is visible
     * @return true if delete button is visible
     */
    public boolean verifyDeleteButton() {
        return selenium.waitInCaseElementVisible(deleteIcon, 3) != null;
    }

    /**
     * Verify pagination is visible
     * @return true if pagination is visible
     */
    public boolean verifyPagination() {
        return selenium.waitInCaseElementVisible(pagination, 3) != null;
    }

    /**
     * Verify id column is visible
     * @return true if id column is visible
     */
    public boolean verifyIdColumn() {
        return selenium.waitInCaseElementVisible(idColumnHeader, 3) != null;
    }

    /**
     * Verify key column is visible
     * @return true if key column is visible
     */
    public boolean verifyKeyColumn() {
        return selenium.waitInCaseElementVisible(keyColumnHeader, 3) != null;
    }

    /**
     * Verify name column is visible
     * @return true if name column is visible
     */
    public boolean verifyNameColumn() {
        return selenium.waitInCaseElementVisible(nameColumnHeader, 3) != null;
    }

    /**
     * Verify title column is visible
     * @return true if title column is visible
     */
    public boolean verifyTitleColumn() {
        return selenium.waitInCaseElementVisible(titleColumnHeader, 3) != null;
    }

    /**
     * Verify target column is visible
     * @return true if target column is visible
     */
    public boolean verifyTargetColumn() {
        return selenium.waitInCaseElementVisible(targetColumnHeader, 3) != null;
    }

    /**
     * Verify status column is visible
     * @return true if status column is visible
     */
    public boolean verifyStatusColumn() {
        return selenium.waitInCaseElementVisible(statusColumnHeader, 3) != null;
    }

    /**
     * Verify order column is visible
     * @return true if order column is visible
     */
    public boolean verifyOrderColumn() {
        return selenium.waitInCaseElementVisible(orderColumnHeader, 3) != null;
    }

    /**
     * Verify actions column is visible
     * @return true if actions column is visible
     */
    public boolean verifyActionsColumn() {
        return selenium.waitInCaseElementVisible(actionsColumnHeader, 3) != null;
    }

    /**
     * Set Activity Key
     *
     * @param key activity key
     */
    public void setActivityKeyField(String key){
        selenium.enterText(keyField, key, true);
    }

    /**
     * Set Activity Name
     *
     * @param name activity name
     */
    public void setActivityNameField(String name){
        selenium.enterText(nameField, name, true);
    }

    /**
     * Select Activity Target
     *
     * @param target activity target
     * @throws InterruptedException
     */
    public void selectActivityTarget(String target) throws InterruptedException {
        selenium.clickOn(targetDropdown);
        selenium.clickOn(By.xpath("//option[.='"+target+"']"));
    }

    /**
     * Set Activity Title
     *
     * @param title activity title
     */
    public void setActivityTitleField(String title){
        selenium.enterText(titleField, title, true);
    }

    /**
     * Set Activity Description
     *
     * @param description activity description
     */
    public void setActivityDescription(String description){
        selenium.enterText(descriptionField, description, true);
    }

    /**
     * Set Activity Sequence or Order
     *
     * @param sequence activity sequence or order
     */
    public void setActivitySequenceOrder(String sequence){
        selenium.enterText(sequenceField, sequence, true);
    }

    /**
     * Select Activity Status
     *
     * @param status activity status
     * @throws InterruptedException
     */
    public void selectActivityStatus(String status) throws InterruptedException {
        selenium.clickOn(statusDropdown);
        selenium.clickOn(By.xpath("//option[.='"+status+"']"));
//        selenium.hardWait(3);
    }

    /**
     * Click on Save Button
     *
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Set Keyword on Search Field
     *
     * @param keyword Keyword
     */
    public void setKeywordSearchField(String keyword){
        selenium.enterText(keywordSearchField, keyword, false);
    }

    /**
     * Check if New Activity Added is Present in Setting Page
     *
     * @param activityName activity name
     * @return boolean
     */
    public boolean checkNewActivityPresentInSettingPage(String activityName){
        return selenium.waitInCaseElementPresent(By.xpath("//a[contains(.,'"+activityName+"')]"),5)!=null;
    }

    /**
     * Get Text of Activity Validation
     *
     * @return text of validation message
     */
    public String getActivityValidationMessage(int index){
        return selenium.getText(activityValidationMessage.get(index));
    }

    /**
     * Get Validation Message Number
     *
     * @return number of validation message
     */
    public int getActivityValidationNumber(){
        return activityValidationMessage.size();
    }

    /**
     * Click on Edit Icon
     *
     */
    public void clickOnEditActivity() {
        selenium.javascriptClickOn(editIcon);
    }

    /**
     * Click on Delete Icon
     *
     */
    public void clickOnDeleteActivity() {
        selenium.javascriptClickOn(deleteIcon);
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * Check if Filter Result is not Present
     *
     * @return boolean
     */
    public boolean checkResultFilterIsNotPresent(){
        return selenium.waitInCaseElementPresent(filterResult,5)==null;
    }

    public void selectActivityImage(String newUploadImageActivity) throws InterruptedException {
        String projectpath = System.getProperty("user.dir");
        selenium.hardWait(5);
        selenium.javascriptSetAnAttribute(uploadImageButton, "type", "file");
        uploadImageButton.sendKeys(projectpath + "/src/main/resources/images/automation_image.png");
    }
}