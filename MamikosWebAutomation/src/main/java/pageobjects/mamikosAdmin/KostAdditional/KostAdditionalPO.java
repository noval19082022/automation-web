package pageobjects.mamikosAdmin.KostAdditional;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.List;

public class KostAdditionalPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private JavaHelpers java = new JavaHelpers();


    public KostAdditionalPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@name='q']")
    private WebElement searchTextField;

    @FindBy(xpath = "(//*[@id='buttonSearch'])[1]")
    private WebElement searchButton;

    @FindBy(xpath = "//a[contains(text(),'Atur Ketersediaan')]")
    private WebElement aturKetersediaanButton;

    @FindBy(xpath = "//*[@class='table dataTable no-footer']/tbody/tr")
    private List<WebElement> listOfRoom;

    @FindBy(xpath ="//*[@class='form-group'][3]/select")
    private WebElement roomStatusButton;

    @FindBy(xpath="//*[@class='alert alert-danger alert-dismissable']/li")
    private WebElement errorAlertInformationText;

    @FindBy(xpath="//*[@id='edit-form']/*[@class='form-group']/*[@id='floor']")
    private WebElement floorTextField;

    @FindBy(xpath="//*[@class='alert alert-success alert-dismissable']/b")
    private WebElement successAlertInfromationText;

    /**
     * search kos in Kos Additional menu
     * @param keyword kos name
     * @throws InterruptedException
     */
    public void searchKosAdditional(String keyword) throws InterruptedException {
        selenium.waitTillElementIsVisible(searchTextField);
        selenium.enterText(searchTextField,keyword,true);
        selenium.clickOn(searchButton);
    }

    /**
     * click Atur Ketersediaan Button
     * @throws InterruptedException
     */
    public void clickButtonAturKetersediaan() throws InterruptedException {
        selenium.waitTillElementIsClickable(aturKetersediaanButton);
        selenium.clickOn(aturKetersediaanButton);
    }

    /**
     * Get room unit status
     * @param room room name
     * @return String room status (Kosong, Terisi, Out Of Order)
     * @throws InterruptedException
     */
    public String getRoomUnitStatus(String room) throws InterruptedException {
        String roomStatus = null;
        selenium.waitForJavascriptToLoad();
        for (int i = 1; i <= listOfRoom.size(); i++) {
            String roomName = selenium.getText(By.xpath("(//*[@class='table dataTable no-footer']/tbody/tr/td)[" + (3+((i-1)*6))  + "]"));
            if (roomName.equals(room)) {
                roomStatus = selenium.getText(By.xpath("(//*[@class='table dataTable no-footer']/tbody/tr/td)[" + ((i*6)-1) + "]"));
                break;
            }
        }
        return roomStatus;
    }

    /**
     * click Edit Button
     * @throws InterruptedException
     */
    public void clickOnEditButton(String name) throws InterruptedException{
        selenium.clickOn(By.xpath("//*[@class='btn btn-sm btn-info'][@data-name=" +name+ "]"));
    }

    /**
     * select roomstatus and click update button
     * @throws InterruptedException
     */
    public void updateRoomStatusToEmpty() throws InterruptedException {
        selenium.click(roomStatusButton);
        selenium.hardWait(2);
        selenium.clickOn(By.xpath("//*[@class='form-group'][3]/select/option[2]"));
        selenium.clickOn(By.xpath("//*[@class='modal-footer']/*[contains(text(),'Update')]"));
    }

    /**
     * Get error alert after udpate
     * @throws String
     */
    public String getAlertText(String text){
        return selenium.getText(errorAlertInformationText);
    }

    /**
     * update floor text
     * @throws InterruptedException
     */
    public void updateFloorName(String keyword) throws InterruptedException {
        selenium.waitTillElementIsVisible(floorTextField);
        selenium.enterText(floorTextField,keyword,true);
        selenium.clickOn(By.xpath("//*[@class='modal-footer']/*[contains(text(),'Update')]"));
    }

    /**
     * Get success alert after udpate
     * @throws String
     */
    public String getSuccessAlertText(String text){
        return selenium.getText(successAlertInfromationText);
    }

}
