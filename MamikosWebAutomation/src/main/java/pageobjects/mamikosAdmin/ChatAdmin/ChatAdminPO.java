package pageobjects.mamikosAdmin.ChatAdmin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.Assert;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ChatAdminPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private static ThreadLocal<String> kosTitle = new ThreadLocal<>();

    public ChatAdminPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "(//b[contains(.,'CS Maya')]/following::b)[1]")
    private WebElement consultantButton;

    @FindBy(xpath = "(//*[@class='item-title'])[2]")
    private WebElement tenantChatList;

    @FindBy(xpath = "//*[@class='image-render']")
    private List<WebElement> imageOnChat;

    @FindBy(xpath = "//*[@class='image-view-render']")
    private WebElement imagePopUp;

    @FindBy(xpath = "//*[@class='chat-modal-cancel']")
    private WebElement closePopUpButton;

    @FindBy(xpath = "//*[@class='chat-title is-group']")
    private WebElement chatKosTitle;

    @FindBy(xpath = "//*[@class='breadcrumb-trail active']")
    private WebElement detailkosTitle;

    /**
     * Get Total Image
     *
     * @return number of total image message
     */
    public int getTotalImage(){
        return imageOnChat.size();
    }

    /**
     * Click on Consultant on field chat room
     *
     * @throws InterruptedException
     */
    public void clickOnConsultantButton() throws InterruptedException {
        selenium.pageScrollInView(consultantButton);
        selenium.clickOn(consultantButton);
    }

    /**
     * Click on one of chat from chat list
     *
     * @throws InterruptedException
     */
    public void clickOnChatList() throws InterruptedException {
        selenium.pageScrollInView(tenantChatList);
        selenium.clickOn(tenantChatList);
    }

    /**
     * Click image on chat
     *
     * @throws InterruptedException
     */
    public void clickOnImageChat(int number) throws InterruptedException {
        selenium.pageScrollInView(imageOnChat.get(number));
        selenium.clickOn(imageOnChat.get(number));
    }

    /**
     * check image on pop up is present or not
     *
     * @return image on pop up is present
     */
    public boolean imageOnPopupPresent() {
        return selenium.waitInCaseElementVisible(imagePopUp, 5) != null;
    }

    /**
     * Click on Close Pop Up Button
     * @throws InterruptedException
     */
    public void clickOnClosePopUpButton() throws InterruptedException {
        selenium.clickOn(closePopUpButton);
        selenium.hardWait(3);
    }

    /**
     * Click on one of chat from chat list
     *
     * @throws InterruptedException
     */
    public void clickOnTenantChat() throws InterruptedException {
        selenium.clickOn(tenantChatList);
    }

    /**
     * Setter Kos Title
     *
     */
    public static synchronized void setKosTitle(String titleKos){
        kosTitle.set(titleKos);
    }

    /**
     * Getter Kos Title
     *
     * @return String of Kos Title
     */
    public static synchronized String getKosTitle(){
        return kosTitle.get();
    }

    public String getChatKosTitle() {
        String getKosTitle = chatKosTitle.getText().substring(7);
        return getKosTitle;
    }

    /**
     * Verify element is displayed
     *
     * @return boolean
     */
    public void verifyKosTitle() {
        selenium.isElementDisplayed(chatKosTitle);
    }

    /**
     * Click on KosTitle
     * @throws InterruptedException
     */
    public void clickOnKosTitle() throws InterruptedException {
        selenium.clickOn(chatKosTitle);
        selenium.switchToLastWindow();
    }

    /**
     * Verify element is displayed
     * Assert Equals
     *
     * @return String
     */
    public String getKosDetailTitle(){
        selenium.isElementDisplayed(detailkosTitle);
        String getDetailKosTitle = detailkosTitle.getText();
        return getDetailKosTitle;
    }
}
