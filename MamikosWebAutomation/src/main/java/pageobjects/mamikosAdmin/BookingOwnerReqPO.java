package pageobjects.mamikosAdmin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BookingOwnerReqPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingOwnerReqPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);

    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//table[@class='table table-striped']//tr[1]//a[@class='btn btn-xs btn-danger']")
    private WebElement firstRejectButton;

    /**
     * Click on first Reject BBK button
     * @throws InterruptedException
     */
    public void clickOnFirstRejectButton() throws InterruptedException {
        selenium.javascriptClickOn(firstRejectButton);
        selenium.hardWait(2);
    }

}
