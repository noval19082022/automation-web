package pageobjects.mamikosAdmin.OwnerPointBlacklist;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class OwnerPointBlacklistPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public OwnerPointBlacklistPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//tr/td[2]")
    private List<WebElement> userType;

    @FindBy(xpath = "//td//span")
    private List<WebElement> blacklistStatus;

    @FindBy(xpath = "//a[@data-toggle='tooltip']")
    private List<WebElement> switchStatusButton;

    @FindBy(xpath = "//div[@class='alert alert-success alert-dismissable']")
    private WebElement successAlert;

    @FindBy(xpath = "//a[contains(@href, '/create?#point')]")
    private WebElement addUserTypeButton;

    @FindBy(name = "value")
    private WebElement userTypeSelection;

    @FindBy(xpath = "//label[@class='radio-inline']")
    private List<WebElement> statusRadioButton;

    @FindBy(xpath = "//button[text()='Save']")
    private WebElement saveButton;

    @FindBy(xpath = "//a[contains(@onclick, 'hapus?')]")
    private List<WebElement> deleteButton;

    @FindBy(xpath = "//thead//tr//th")
    private List<WebElement> columName;


    /**
     * Click on Switch Status Button
     * @param status input string that will match blacklist status value
     * @throws InterruptedException
     */
    public void clickOnSwitchStatusButton(String status) throws InterruptedException {
        for (int i = 0; i < blacklistStatus.size(); i++) {
            if(blacklistStatus.get(i).getText().equalsIgnoreCase(status)){
                selenium.clickOn(switchStatusButton.get(i));
                driver.switchTo().alert().accept();
            }
        }
    }

    /**
     * Click on Revert Switch Status Button
     * @param name inout string that will match user type value
     * @throws InterruptedException
     */
    public void clickOnRevertSwitchStatusButton(String name) throws InterruptedException {
        for (int i = 0; i < userType.size(); i++) {
            if (userType.get(i).getText().equalsIgnoreCase(name)){
                selenium.clickOn(switchStatusButton.get(i));
                driver.switchTo().alert().accept();
            }
        }
    }

    /**
     * Is Success Alert Appear?
     * @return true or false
     * @throws InterruptedException
     */
    public Boolean isSuccessAlertAppear() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        return selenium.waitInCaseElementVisible(successAlert, 3) != null;
    }

    public String getBlacklistStatus(String name){
        int temp = 0;
        for (int i = 0; i < userType.size(); i++) {
            if (userType.get(i).getText().equalsIgnoreCase(name)){
                temp = i;
            }
        }
        return selenium.getText(blacklistStatus.get(temp));
    }

    /**
     * Get Success Alert Text
     * @return String
     */
    public String getSuccessAlertText() {
        return selenium.getText(successAlert);
    }

    /**
     * Click on Add User Type Button
     * @throws InterruptedException
     */
    public void clickOnAddUserTypeButton() throws InterruptedException {
        selenium.clickOn(addUserTypeButton);
    }

    /**
     * Select User Type Value
     * @param value input string that will match user type selection value
     */
    public void selectUserTypeValue(String value) throws InterruptedException {
        selenium.selectDropdownValueByText(userTypeSelection, value);
    }

    /**
     * Select Status Radio Button
     * @param status input string that will match status on radio button value
     * @throws InterruptedException
     */
    public void selectStatusRadioButton(String status) throws InterruptedException {
        for (WebElement e : statusRadioButton) {
            if (e.getText().equalsIgnoreCase(status)) {
                selenium.clickOn(e);
            }
        }
    }

    /**
     * Click on Save Button
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Click on Delete Button
     * @param user input string that will match user type value
     * @throws InterruptedException
     */
    public void clickOnDeleteButton(String user) throws InterruptedException {
        for (int i = 0; i < userType.size(); i++) {
            if(userType.get(i).getText().equalsIgnoreCase(user)){
                selenium.clickOn(deleteButton.get(i));
                driver.switchTo().alert().accept();
            }
        }
    }

    /**
     * Get Text of Head Table Segment by index
     * @param index - index head table
     * @return text of Head Table
     */
    public String getColumnName (int index){
        return selenium.getText(columName.get(index));
    }
}
