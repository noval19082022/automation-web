package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class KostOwnerPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public KostOwnerPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@placeholder='No. Telp. Owner']")
    private WebElement ownerPhoneSearch;

    @FindBy(xpath = "//button[@id='buttonSearch']")
    private WebElement searchButton;

    @FindBy(xpath = "//select[@name='status']")
    private WebElement clickDropdownFilter;

    @FindBy(xpath = "//option[@value='add']")
    private WebElement clickFilterEditToVerify;

    @FindBy(xpath = "//*[@class='label label-danger'][contains(text(),'Hostile')]")
    private WebElement hostileLabel;

    @FindBy(xpath = "//input[@name='q']")
    private WebElement kosNameSearch;

    @FindBy(xpath = "//table[@class='table table-striped']//tr[1]//a[@title='Delete']")
    private WebElement firstDeleteButton;

    @FindBy(xpath = "//a[@title='Verify']//i[@class='fa fa-check']")
    private WebElement firstVerifyButton;

    private final By firstVerifyButtonBy = By.xpath("//table[@class='table table-striped']//tr[1]//a[@title='Verify']");

    @FindBy(xpath = "//table[@class='table table-striped']//tr[1]//button[text()='BBK Data']")
    private WebElement firstBBKDataButton;

    @FindBy(xpath = "//div[@id='verifyModal4' and @style='display: block;']//form/button[@class='btn btn-sm btn-success']")
    private WebElement verifyKosPopUpButton;

    @FindBy(xpath = "//table[@class='table table-striped']//tr[1]//a[@title='Alasan ditolak']")
    private WebElement firstRejectButton;

    @FindBy(xpath = "//div[@id='address']/label[1]/span")
    private WebElement firstCheckboxRejectReason;

    @FindBy(id = "submitButton")
    private WebElement rejectButton;

    @FindBy(css = ".swal2-confirm")
    private WebElement sendButton;

    @FindBy(xpath = "//table[@class='table table-striped']//tr[1]/td[8]/span")
    private WebElement firstKosStatusLabel;


    /**
     * Set owner phone number on search field
     *
     * @param ownerPhone of Hostile Owner
     */
    public void setOwnerPhone(String ownerPhone) {
        selenium.enterText(ownerPhoneSearch, ownerPhone,true);
    }

    /**
     * Click on search button
     * @throws InterruptedException
     */
    public void clickOnSearchButton() throws InterruptedException {
        selenium.javascriptClickOn(searchButton);
        selenium.hardWait(10);
    }

    /**
     * Get hostile label on coloumn Owner Id
     * @return Text of Hostile label
     */
    public String getHostileLabel() {
        selenium.waitTillElementIsVisible(hostileLabel);
        return selenium.getText(hostileLabel);
    }

    /**
     * Search kos name then hit enter
     * @param kosName of Kos Name
     */
    public void searchKosName(String kosName) throws InterruptedException {
        selenium.enterText(kosNameSearch, kosName, true);
        kosNameSearch.sendKeys(Keys.ENTER);
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementVisible(By.xpath("//table[@class='table table-striped']//tr[1]/td[3][contains(text(), '" + kosName + "'"), 5);
    }

    /**
     * Filter edited Ready To Verify
     */
    public void editedReadyToVerify () throws InterruptedException {
        selenium.clickOn(clickDropdownFilter);
        selenium.clickOn(clickFilterEditToVerify);
        selenium.clickOn(searchButton);
    }
    /**
     * Click on first delete button
     * @throws InterruptedException
     */
    public void clickOnFirstDeleteButton() throws InterruptedException {
        selenium.javascriptClickOn(firstDeleteButton);
        selenium.hardWait(2);
        selenium.waitTillAlertPresent();
    }

    /**
     * Click on first BBK Data button
     * @throws InterruptedException
     */
    public void clickOnFirstBBKDataButton() throws InterruptedException {
        selenium.javascriptClickOn(firstBBKDataButton);
        selenium.hardWait(2);
        selenium.switchToWindow(0);
    }

    /**
     * Click on first verify button
     * @throws InterruptedException
     */
    public void clickOnFirstVerifyButton() throws InterruptedException {
      //  selenium.javascriptClickOn(firstVerifyButton);
        if (selenium.isElementNotDisplayed(firstVerifyButton)) {
            selenium.clickOn(firstVerifyButton);
        }else {
            selenium.clickOn(searchButton);
        }
        selenium.hardWait(2);
        selenium.waitForJavascriptToLoad();
        if (selenium.waitInCaseElementVisible(verifyKosPopUpButton,3) != null) {
            selenium.hardWait(2);
            selenium.clickOn(verifyKosPopUpButton);
        }
     //   selenium.waitTillElementsCountIsEqualTo(firstVerifyButtonBy, 0);
        selenium.hardWait(5);
    }

    /**
     * Click on first Reject button in table
     * @throws InterruptedException
     */
    public void clickOnFirstRejectButton() throws InterruptedException {
        selenium.javascriptClickOn(firstRejectButton);
        selenium.hardWait(2);
    }

    /**
     * Click on Reject reason checkbox
     * @param section is reject reason section
     * @param reason is reject reason
     * @throws InterruptedException
     */
    public void clickOnRejectReasonCheckbox(String section, String reason) throws InterruptedException {
        selenium.waitInCaseElementVisible(firstCheckboxRejectReason, 5);
        WebElement element = driver.findElement(By.xpath("//h4[text()='"+ section +"']/following-sibling::div[1]/label[text()='"+ reason +"']"));
        selenium.clickOn(element);
    }

    /**
     * Click on Reject button in reject reason page
     * @throws InterruptedException
     */
    public void clickOnRejectButton() throws InterruptedException {
        selenium.clickOn(rejectButton);
    }

    /**
     * Click on Send button in reject reason pop up
     * @throws InterruptedException
     */
    public void clickOnSendButton() throws InterruptedException {
        selenium.clickOn(sendButton);
        selenium.hardWait(3);
        selenium.waitInCaseElementVisible(firstCheckboxRejectReason, 5);
    }

    /**
     * Get first kos status label on kos owner list
     * @return String kos status
     */
    public String getKosStatusLabel() {
        selenium.waitTillElementIsVisible(firstKosStatusLabel);
        return selenium.getText(firstKosStatusLabel);
    }
}
