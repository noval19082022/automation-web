package pageobjects.mamikosAdmin.RoomLevel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class ListRoomLevelPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ListRoomLevelPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(@href,'create#kost-level')]")
    private WebElement addRoomLevelButton;

    @FindBy(xpath = "//input[@placeholder='Level Name']")
    private WebElement searchLevelEditText;

    @FindBy(id = "buttonSearch")
    private WebElement searchLevelButton;

    @FindBy(xpath = "//a[contains(@href,'edit#kost-level')]")
    private WebElement firstUpdateIcon;

    /**
     * Add room level button is appeared
     * @return boolean element status
     */
    public boolean addRoomLevelButtonIsAppeared() {
        return selenium.isElementDisplayed(addRoomLevelButton);
    }

    /**
     * Search level edit text is appeared
     * @return boolean element status
     */
    public boolean searchLevelEditTextIsAppeared() {
        return selenium.isElementDisplayed(searchLevelEditText);
    }

    /**
     * Search level button is appeared
     * @return boolean element status
     */
    public boolean searchLevelButtonIsAppeared() {
        return selenium.isElementDisplayed(searchLevelButton);
    }

    /**
     * Get column name
     * @return String text
     */
    public String getColumnName(int i) {
        return selenium.getText(By.xpath("(//tr/th)[" + i + "]"));
    }

    /**
     * Click on page number button
     * @throws InterruptedException
     */
    public void clickOnPageNumber(String pageNumber) throws InterruptedException {
        selenium.clickOn(By.xpath("//a[@class='page-link'][text()='" + pageNumber + "']"));
    }

    /**
     * Verify page number button is active
     * @return String attribute value
     */
    public String pageNumberButtonIsActive(String pageNumber) {
        return selenium.getElementAttributeValue(By.xpath("//span[@class='page-link'][text()='" + pageNumber +"']/parent::li"), "class");
    }

    /**
     * Search room level
     * @param levelName
     */
    public void searchRoomLevel(String levelName) throws InterruptedException {
        selenium.enterText(searchLevelEditText, levelName, true);
        selenium.clickOn(searchLevelButton);
    }

    /**
     * Click on first icon update room level
     * @throws InterruptedException
     */
    public void clickOnFirstListIconUpdate() throws InterruptedException {
        selenium.clickOn(firstUpdateIcon);
    }
}
