package pageobjects.mamikosAdmin.RoomLevel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class AddRoomLevelPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public AddRoomLevelPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(id = "level-name")
    private WebElement levelNameEditText;

    @FindBy(xpath = "//button[text()='Save']")
    private WebElement saveButton;

    /**
     * Enter level name
     */
    public void enterLevelName(String levelName) {
        selenium.enterText(levelNameEditText, levelName, true);
    }

    /**
     * Click on save button
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.pageScrollInView(saveButton);
        selenium.clickOn(saveButton);
    }
}
