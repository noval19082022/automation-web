package pageobjects.mamikosAdmin.Survey;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class SurveyPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SurveyPO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[@title='Edit']")
    private List<WebElement> editButton;

    @FindBy(name = "status")
    private WebElement statusSelection;

    @FindBy(className = "btn-primary")
    private WebElement saveButton;

    @FindBy(xpath = "//tr//td[1]")
    public List<WebElement> tenantName;

    @FindBy(xpath = "//tr//td[2]")
    public List<WebElement> tenantPhoneNumber;

    @FindBy(xpath = "//tr//td[3]")
    public List<WebElement> propertyName;

    @FindBy(xpath = "//tr//td[6]")
    public List<WebElement> status;

    @FindBy(xpath = "//tr//td[7]")
    public List<WebElement> surveyTime;


    @FindBy(xpath = "//a[@rel='next']")
    private WebElement nextPage;

    @FindBy(name = "survey_date")
    private WebElement surveyDateInput;

    @FindBy(css = "a[data-handler='next']")
    private WebElement nextDateButton;

    @FindBy(name = "survey_time")
    private WebElement surveyTimeSelection;

    @FindBy(id = "inputTenantName")
    private WebElement tenantNameFilterInput;

    @FindBy(id = "inputStatus")
    private WebElement surveyStatusFilterSelection;

    @FindBy(id = "inputTenantPhone")
    private WebElement tenantPhoneNumberInput;

    @FindBy(id = "inputPropertyName")
    private WebElement propertyNameInput;


    private By filterTenantName = By.xpath("//*[@id='inputTenantName']");

    private By filterTenantPhoneNumber = By.xpath("//*[@id='inputTenantPhone']");

    private By filterPropertyName = By.xpath("//*[@id='inputPropertyName']");

    private By filterSurveyStatus = By.xpath("//*[@id='inputStatus']");

    private By filterSurveyDate = By.xpath("//*[@id='inputSurveyDate']");

    private By searchButton = By.xpath("//*[@id='buttonSearch']");

    private By resetFilterButton = By.xpath("//*[@id='buttonReset']");

    private By createSurveyButton = By.xpath("//*[@class='btn btn-success btn pull-left actions']");

    private By paginationButton = By.xpath("//*[@class='pagination']");

    /**
     * Click on Edit Button
     * @throws InterruptedException
     */
    public void clickOnEditButton(String tenant) throws InterruptedException {
        for (int i = 0; i < editButton.size(); i++) {
            if (tenantName.get(i).getText().equalsIgnoreCase(tenant)){
                selenium.clickOn(editButton.get(i));
            }else if(i == editButton.size()-1){
                selenium.clickOn(nextPage);
                i = 0;
            }
        }
    }

    /**
     * Change Survey Status
     * @param status
     */
    public void changeSurveyStatus(String status) throws InterruptedException {
        selenium.selectDropdownValueByText(statusSelection, status);
        selenium.clickOn(saveButton);
    }

    /**
     * Change Survey Date
     * @param date
     * @throws InterruptedException
     */
    public void changeSurveyDate(String date) throws InterruptedException {
        selenium.clickOn(surveyDateInput);
        if (date.equalsIgnoreCase("30") || date.equalsIgnoreCase("31"))
            selenium.clickOn(nextDateButton);
        selenium.clickOn(By.xpath("//a[text()='"+date+"']"));
        selenium.clickOn(saveButton);
    }

    /**
     * Change Survey Time
     * @param time
     * @throws InterruptedException
     */
    public void changeSurveyTime(String time) throws InterruptedException {
        selenium.selectDropdownValueByText(surveyTimeSelection, time);
        selenium.clickOn(saveButton);
    }

    /**
     * Verify filter Tenant Name in the Tenant Survey Page is displayed
     * @return boolean
     */
    public Boolean isFilterTenantNameDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementPresent(filterTenantName,5)!=null;
    }

    /**
     * Verify filter Tenant Phone Number in the Tenant Survey Page is displayed
     * @return boolean
     */
    public Boolean isFilterTenantPhoneNumberDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementPresent(filterTenantPhoneNumber,5)!=null;
    }

    /**
     * Verify filter Property Name in the Tenant Survey Page is displayed
     * @return boolean
     */
    public Boolean isFilterPropertyNameDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementPresent(filterPropertyName,5)!=null;
    }

    /**
     * Verify filter Survey Status in the Tenant Survey Page is displayed
     * @return boolean
     */
    public Boolean isFilterSurveyStatusDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementPresent(filterSurveyStatus,5)!=null;
    }

    /**
     * Verify filter Survey Date in the Tenant Survey Page is displayed
     * @return boolean
     */
    public Boolean isFilterSurveyDateDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementPresent(filterSurveyDate,5)!=null;
    }

    /**
     * Verify Search Button in the Tenant Survey Page is displayed
     * @return boolean
     */
    public Boolean isSearchButtonDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementPresent(searchButton,5)!=null;
    }

    /**
     * Verify Reset Filter Button in the Tenant Survey Page is displayed
     * @return boolean
     */
    public Boolean isResetFilterButtonDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementPresent(resetFilterButton,5)!=null;
    }

    /**
     * Verify Create Survey Button in the Tenant Survey Page is displayed
     * @return boolean
     */
    public Boolean isCreateSurveyButtonDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementPresent(createSurveyButton,5)!=null;
    }

    /**
     * Verify Pagination Button in the Tenant Survey Page is displayed
     * @return boolean
     */
    public Boolean isPaginationButtonDisplayed() throws InterruptedException {
        return selenium.waitInCaseElementPresent(paginationButton,5)!=null;
    }

    /**
     * Fill tenant name input filter with desired data
     * @param tenantName input string that will match input value
     */
    public void fillOnTenantNameFilter(String tenantName){
        selenium.waitInCaseElementVisible(tenantNameFilterInput, 5);
        selenium.enterText(tenantNameFilterInput, tenantName, true);
    }

    /**
     * Click on Search Filter Button
     * @throws InterruptedException
     */
    public void clickOnSearchFilterButton() throws InterruptedException {
        selenium.clickOn(searchButton);
    }

    /**
     * Get Tenant Name Table Result
     * @param index input integer that will match index element of table
     * @return string
     */
    public String getTenantNameTableResult(Integer index){
        return selenium.getText(tenantName.get(index));
    }

    /**
     * Select survey status filter
     * @param surveyStatus input string that will match selection value
     */
    public void selectSurveyStatusFilter(String surveyStatus){
        selenium.waitInCaseElementVisible(surveyStatusFilterSelection, 5);
        selenium.selectDropdownValueByText(surveyStatusFilterSelection, surveyStatus);
    }

    /**
     * Get Survey Status Table Result
     * @param index input integer that will match index element of table
     * @return string
     */
    public String getSurveyStatusTableResult(Integer index){
        return selenium.getText(status.get(index));
    }

    /**
     * Click on Survey Date Filter
     * @param date input string that will match date value with desired data
     * @throws InterruptedException
     */
    public void clickOnSurveyDateFilter(String date) throws InterruptedException {
        String tempDate = "//td[@data-handler='selectDay']//a[text()='"+date+"']";
        selenium.clickOn(surveyDateInput);
        selenium.waitInCaseElementVisible(By.xpath(tempDate), 5);
        selenium.clickOn(By.xpath(tempDate));
    }

    /**
     * Get Survey Time Table Result
     * @param index input integer that will match index of element
     * @return string
     */
    public String getSurveyTimeTableResult(Integer index){
        return selenium.getText(surveyTime.get(index));
    }

    /**
     * Fill on Tenant Phone Number Filter
     * @param phone input string that will match phone number value
     */
    public void fillOnTenantPhoneNumberFilter(String phone){
        selenium.waitInCaseElementVisible(tenantPhoneNumberInput, 5);
        selenium.enterText(tenantPhoneNumberInput, phone, true);
    }

    /**
     * Get Tenant Phone Number Table Result
     * @param index input integer that will match index element of table
     * @return string
     */
    public String getTenantPhoneNumberTableResult(Integer index){
        return selenium.getText(tenantPhoneNumber.get(index));
    }

    /**
     * Fill on Propery Name Filter
     * @param property input string that will match property name value
     */
    public void fillOnPropertyNameFilter(String property){
        selenium.waitInCaseElementVisible(propertyNameInput, 5);
        selenium.enterText(propertyNameInput, property, true);
    }

    /**
     * Get Property Name Table Result
     * @param index input integer that will match index element of table
     * @return string
     */
    public String getPropertyNameTableResult(Integer index){
        return selenium.getText(propertyName.get(index));
    }
}
