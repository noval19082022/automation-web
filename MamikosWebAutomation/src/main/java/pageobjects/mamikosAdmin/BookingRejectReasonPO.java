package pageobjects.mamikosAdmin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class BookingRejectReasonPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingRejectReasonPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);

    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//button[@class='btn btn-danger' and @value='single']")
    private WebElement rejectButton;

    @FindBy(xpath = "//div[@class='iradio_minimal']")
    private WebElement firstRejectReasonRadioButton;


    /**
     * Click on Reject BBK button
     * @throws InterruptedException
     */
    public void clickOnRejectButton() throws InterruptedException {
        selenium.clickOn(rejectButton);
        selenium.hardWait(2);
        selenium.waitForJavascriptToLoad();
    }

    /**
     * Click on first radio button
     * @throws InterruptedException
     */
    public void clickOnFirstRadioButton() throws InterruptedException {
        selenium.clickOn(firstRejectReasonRadioButton);
    }
}
