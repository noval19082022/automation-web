package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class HostileOwnerPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public HostileOwnerPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[@id='action-add']")
    private WebElement addButton;

    @FindBy(xpath = "//*[@class='swal2-input custom-input']")
    private WebElement inputField;

    @FindBy(xpath = "//*[@class='swal2-select']")
    private WebElement hostileReason;

    @FindBy(xpath = "//*[@class='swal2-confirm btn btn-primary']")
    private WebElement saveButton;

    @FindBy(xpath = "//*[@class='swal2-confirm swal2-styled']")
    private WebElement okButton;

    @FindBy(xpath = "//*[@class='form-control input-sm'][2]")
    private WebElement fieldOwnerPhone;

    @FindBy(xpath = "//*[@id='buttonSearch']")
    private WebElement searchButton;

    @FindBy(xpath = "//tbody[1]//td[2]")
    private WebElement ownerId;

    private By data = By.xpath("//tbody[1]//td[2]");

    @FindBy(xpath = "//tbody[1]//td[10]")
    private WebElement hostileReasonOnList;

    @FindBy(xpath = "//*[@class='btn btn-default btn-sm dropdown-toggle']")
    private WebElement actionButton;

    @FindBy(xpath = "//*[@data-action='reset']")
    private WebElement resetAction;

    @FindBy(xpath = "//*[@id='swal2-title'][contains(text(),'Masukkan ID owner')]")
    private WebElement setIDOwnerHostileTitle;

    @FindBy(xpath = "//*[@id='swal2-title'][contains(text(),'Set Hostility Score')]")
    private WebElement setHostileScoreTitle;

    @FindBy(xpath = "//*[@id='swal2-title'][contains(text(),'Tambah Hostile Reason')]")
    private WebElement setHostileReasonTitle;

    /**
     * Click on add button
     * @throws InterruptedException
     */
    public void clickOnAddButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(addButton);
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(addButton);
        selenium.waitTillElementIsVisible(inputField);
    }

    /**
     * Set data hostile owner ID
     *
     * @param ownerID of Hostile Owner
     */
    public void inputFieldID(String ownerID) throws InterruptedException {
        selenium.waitTillElementIsVisible(setIDOwnerHostileTitle);
        selenium.enterText(inputField, ownerID,true);
        selenium.hardWait(2);
    }

    /**
     * Set data hostile owner Score
     *
     * @param score of Hostile Owner
     */
    public void inputFieldScore(String score) throws InterruptedException {
        selenium.waitTillElementIsVisible(setHostileScoreTitle);
        selenium.enterText(inputField, score,true);
        selenium.hardWait(2);
    }

    /**
     * Set hostile reason of owner
     *
     * @param reason of Hostile Owner
     */
    public void selectHostileReason(String reason) throws InterruptedException {
        selenium.waitTillElementIsVisible(setHostileReasonTitle);
        selenium.waitTillElementIsVisible(hostileReason);
        if(reason.equals("Indikasi fraud")){
            selenium.selectDropdownValueByIndex(hostileReason,1);
            selenium.hardWait(1);
        }else if(reason.equals("Indikasi kompetitor")){
            selenium.selectDropdownValueByIndex(hostileReason,2);
            selenium.hardWait(1);
        } else if(reason.equals("Tidak bayar minimum charging GP")){
            selenium.selectDropdownValueByIndex(hostileReason,3);
            selenium.hardWait(1);
        } else if(reason.equals("Terbukti Fraud")){
            selenium.selectDropdownValueByIndex(hostileReason,4);
            selenium.hardWait(1);
        }
    }

    /**
     * Click on save button
     * @throws InterruptedException
     */
    public void clickSaveButton() throws InterruptedException {
        selenium.javascriptClickOn(saveButton);
        selenium.hardWait(2);
    }

    /**
     * Click on ok button
     * @throws InterruptedException
     */
    public void clickOkButton() throws InterruptedException {
        selenium.clickOn(okButton);
        selenium.hardWait(10);
    }

    /**
     * Set owner phone to search
     *
     * @param ownerPhone of Hostile Owner
     */
    public void inputOwnerPhone(String ownerPhone) throws InterruptedException {
        selenium.enterText(fieldOwnerPhone, ownerPhone,false);
    }

    /**
     * Click on search button
     * @throws InterruptedException
     */
    public void clickOnSearchButton() throws InterruptedException {
        selenium.clickOn(searchButton);
        selenium.hardWait(5);
    }

    /**
     * Check if list hostile owner is Present
     *
     * @return boolean
     */
    public boolean checkOwnerId() throws InterruptedException {
        return selenium.waitInCaseElementPresent(data,3)!=null;
    }

    /**
     * Get owner id
     * @return Text of Owner Id
     */
    public String getOwnerId() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(ownerId);
    }

    /**
     * Get hostile reason
     * @return Text of Hostile Reason
     */
    public String getHostileReason() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(hostileReasonOnList);
    }

    /**
     * Click on action button on list
     * @throws InterruptedException
     */
    public void clickOnActionButton() throws InterruptedException {
        selenium.javascriptClickOn(actionButton);
        selenium.hardWait(2);
    }

    /**
     * Click on action reset button
     * @throws InterruptedException
     */
    public void clickOnResetAction() throws InterruptedException {
        selenium.javascriptClickOn(resetAction);
        selenium.hardWait(2);
    }
}