package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class KostListPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public KostListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(@href,'upload#kost-level')]")
    private WebElement uploadCSVButton;

    @FindBy(name = "kost-name")
    private WebElement kostNameEditText;

    @FindBy(name = "owner-name")
    private WebElement ownerNameEditText;

    @FindBy(name = "owner-phone")
    private WebElement ownerPhoneNumberEditText;

    @FindBy(name = "level-id")
    private WebElement levelDropdown;

    @FindBy(id = "buttonSearch")
    private WebElement searchButton;

    @FindBys(@FindBy(xpath = "//tbody/tr"))
    private List<WebElement> row;

    @FindBy(xpath = "//a[contains(text(),'Edit Kost Level')]")
    private WebElement editKosLevelButton;

    @FindBy(xpath = "//a[contains(text(),'Room List')]")
    private WebElement roomListButton;

    @FindBy(xpath = "//a[contains(text(),'History')]")
    private WebElement historyButton;

    @FindBy(id = "level-id")
    private WebElement kosLevelDropdown;

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    private WebElement saveButton;

    @FindBy(xpath = "(//tbody/tr/td)[5]")
    private WebElement levelNameText;

    /**
     * Search kost by kost name
     * @param kostName
     * @throws InterruptedException
     */
    public void searchKostLevel(String kostName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.enterText(kostNameEditText, kostName, true);
        selenium.clickOn(searchButton);
    }

    /**
     * count number of list
     * @throws InterruptedException
     */
    public int getNumberOfList() {
        return row.size();
    }

    /**
     * Get kost name
     * @return String of kost name
     */
    public String getKostName(int index) {
        System.out.println(selenium.getText(By.xpath("(//tbody/tr)[" + index + "]/td[2]")));
        return selenium.getText(By.xpath("(//tbody/tr)[" + index + "]/td[2]"));
    }

    /**
     * Upload CSV button is appeared
     * @return boolean element status
     */
    public boolean uploadCSVButtonIsAppeared() {
        return selenium.isElementDisplayed(uploadCSVButton);
    }

    /**
     * Kost name edit text is appeared
     * @return boolean element status
     */
    public boolean kostNameEditTextIsAppeared() {
        return selenium.isElementDisplayed(kostNameEditText);
    }

    /**
     * Owner name edit text is appeared
     * @return boolean element status
     */
    public boolean ownerNameEditTextIsAppeared() {
        return selenium.isElementDisplayed(ownerNameEditText);
    }

    /**
     * Owner phone number edit text is appeared
     * @return boolean element status
     */
    public boolean ownerPhoneNumberEditTextIsAppeared() {
        return selenium.isElementDisplayed(ownerPhoneNumberEditText);
    }

    /**
     * All level dropdown is appeared
     * @return boolean element status
     */
    public boolean allLevelDropdownIsAppeared() {
        return selenium.isElementDisplayed(levelDropdown);
    }

    /**
     * Search button is appeared
     * @return boolean element status
     */
    public boolean searchButtonIsAppeared() {
        return selenium.isElementDisplayed(searchButton);
    }

    /**
     * Get column name
     * @return String text
     */
    public String getColumnName(int i) {
        return selenium.getText(By.xpath("(//tr/th)[" + i + "]"));
    }

    /**
     * Click Edit Kos Level in Action column
     */
    public void clickEditKosLevel() throws InterruptedException {
        selenium.clickOn(editKosLevelButton);
    }

    /**
     * select kos level
     * @param level - kos level name
     */
    public void selectKostLevel(String level) throws InterruptedException {
        selenium.selectDropdownValueByText(kosLevelDropdown,level);
    }

    /**
     * click save button
     * @throws InterruptedException
     */
    public void clickSave() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * dismiss alert to assign room level after change kos level
     */
    public void cancelPopUpAssignRoomLevel() throws InterruptedException {
        selenium.waitTillAlertPresent();
        selenium.dismissAlert();
    }

    /**
     * Get level name
     * @return String level name
     */
    public String getLevelName() {
        return selenium.getText(levelNameText);
    }

    /**
     * click room list button
     * @throws InterruptedException
     */
    public void clickRoomList() throws InterruptedException {
        selenium.clickOn(roomListButton);
    }

    /**
     * click history button
     * @throws InterruptedException
     */
    public void clickHistory() throws InterruptedException {
        selenium.clickOn(historyButton);
    }
}
