package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.List;

public class RewardManagementPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private JavaHelpers java = new JavaHelpers();

    public RewardManagementPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(.,'Add Reward Type')]")
    private WebElement addRewardTypeButton;

    @FindBy(id = "buttonSearch")
    private WebElement searchRewardTypeButton;

    @FindBy(id = "reward-type-key")
    private WebElement rewardKeyTextBox;

    @FindBy(id = "reward-type-name")
    private WebElement rewardNameTextBox;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement saveButton;

    @FindBy(xpath = "//*[@class='alert alert-success alert-dismissable']")
    private WebElement successAddRewardLabel;

    @FindBy(name = "q")
    private WebElement rewardKeySearchBox;

    @FindBy(xpath = "//i[@class='fa fa-pencil']")
    private WebElement editButton;

    @FindBy(xpath = "//*[@class='close']/following-sibling::li")
    private WebElement errorMessage;

    @FindBy(xpath = "//i[@class='fa fa-trash']")
    private WebElement deleteButton;

    @FindBy(xpath = "//h3[@class='box-title']")
    private WebElement rewardListHeader;

    @FindBy(xpath = "//input[@class='btn btn-primary btn-md']")
    private WebElement filterButton;

    @FindBy(xpath = "//a[contains(.,'Add Reward')]")
    private WebElement addRewardButton;

    @FindBy(xpath = "//th[.='ID']")
    private WebElement idField;

    @FindBy(xpath = "//th[.='Reward Name']")
    private WebElement rewardNameField;

    @FindBy(xpath = "//th[.='Start']")
    private WebElement startField;

    @FindBy(xpath = "//a[contains(.,'End')]")
    private WebElement endField;

    @FindBy(xpath = "//a[contains(.,'Redemption Point')]")
    private WebElement redemptionField;

    @FindBy(xpath = "//th[.='Status']")
    private WebElement statusField;

    @FindBy(xpath = "//th[.='Action']")
    private WebElement actionField;

    @FindBy(name = "keyword")
    private WebElement rewardNameFilter;

    @FindBy(xpath = "//i[@class='fa fa-pencil']")
    private WebElement updateButton;

    @FindBy(xpath = "//button[contains(.,'Update Reward')]")
    private WebElement updateRewardButton;

    @FindBy (css = "[for='is_active']")
    private WebElement activeCheckbox;

    @FindBy(xpath = "//button[@class='swal2-confirm swal2-styled']")
    private WebElement yesUpdateRewardButton;

    @FindBy(xpath = "//table[@class='table table-hover']//tr/th")
    private List<WebElement> fieldTableRewardList;

    @FindBy(xpath = "//input[@name='keyword']")
    private WebElement ownerUserPointNameFilter;

    @FindBy(xpath = "//button[@id='buttonSearch']")
    private WebElement searchButton;

    @FindBy(xpath = "//td[6]/a[contains(.,'Whitelist')]")
    private WebElement whitelistButton;

    @FindBy(xpath = "//button[@class='btn btn-default']")
    private WebElement yesDoItButton;

    @FindBy(xpath = "//div[@class='alert alert-success alert-dismissable']")
    private WebElement successAlert;

    @FindBy(xpath = "//td[6]/a[contains(.,'Blacklist')]")
    private WebElement blacklistButton;



    /**
     * Click on add reward type button
     * @throws InterruptedException
     */
    public void clickOnAddRewardType() throws InterruptedException {
        selenium.waitTillElementIsVisible(addRewardTypeButton);
        selenium.hardWait(2);
        selenium.clickOn(addRewardTypeButton);
    }

    /**
     * input reward type key
     * @param rewardKey
     */
    public void setRewardTypeKey(String rewardKey) throws InterruptedException{
        selenium.enterText(rewardKeyTextBox,rewardKey,true);
    }

    /**
     * input reward type name
     * @param rewardName
     */
    public void setRewardTypeName(String rewardName) throws InterruptedException{
        selenium.enterText(rewardNameTextBox,rewardName,true);
    }

    /**
     * Click save new reward type button
     * @throws InterruptedException
     */
    public void clickOnSave() throws InterruptedException {
        selenium.waitTillElementIsVisible(saveButton);
        selenium.hardWait(2);
        selenium.clickOn(saveButton);
    }

    /**
     * Check success add new reward type label is displayed
     *
     * @return status true or false
     */
    public boolean successAddRewardTypeIsDisplayed() throws InterruptedException{
        selenium.hardWait(4);
        return selenium.waitInCaseElementVisible(successAddRewardLabel, 5) != null;
    }

    /**
     * input reward type key to seacrh reward type
     * @param rewardKey
     */
    public void setOnSearchBoxRewardTypeKey(String rewardKey) throws InterruptedException{
        selenium.enterText(rewardKeySearchBox,rewardKey,true);
    }

    /**
     * Click edit reward type button
     * @throws InterruptedException
     */
    public void clickOnEditRewardType() throws InterruptedException {
        selenium.waitTillElementIsVisible(editButton);
        selenium.hardWait(4);
        selenium.clickOn(editButton);
    }

    /**
     * Click edit reward type button
     * @throws InterruptedException
     */
    public void clickOnSearchRewardType() throws InterruptedException {
        selenium.waitTillElementIsVisible(searchRewardTypeButton);
        selenium.hardWait(2);
        selenium.clickOn(searchRewardTypeButton);
    }

    /**
     * Get error text duplicate key and special character
     * @return error message
     * @throws InterruptedException
     */
    public String getErrorMesssage() throws InterruptedException {
        selenium.hardWait(3);
        return selenium.getText(errorMessage);
    }

    /**
     * Click delete reward type button
     * @throws InterruptedException
     */
    public void clickOnDeleteRewardType() throws InterruptedException {
        selenium.waitTillElementIsVisible(deleteButton);
        selenium.hardWait(2);
        selenium.clickOn(deleteButton);
    }

    /**
     * Check success add new reward type label is displayed
     *
     * @return status true or false
     */
    public boolean fieldOnTableIsDisplayed(Integer angka) throws InterruptedException{
        selenium.hardWait(4);
        return selenium.waitInCaseElementVisible(fieldTableRewardList.get(angka), 5) != null;
    }


    /**
     * Check success add new reward type label is displayed
     *
     * @return status true or false
     */
    public boolean rewardListHeaderIsDisplayed() throws InterruptedException{
        selenium.hardWait(4);
        return selenium.waitInCaseElementVisible(rewardListHeader, 5) != null;
    }

    /**
     * Check success add new reward type label is displayed
     *
     * @return status true or false
     */
    public boolean filterButtonIsDisplayed() throws InterruptedException{
        selenium.hardWait(4);
        return selenium.waitInCaseElementVisible(filterButton, 2) != null;
    }

    /**
     * Check success add new reward type label is displayed
     *
     * @return status true or false
     */
    public boolean addRewardButtonIsDisplayed() throws InterruptedException{
        selenium.hardWait(4);
        return selenium.waitInCaseElementVisible(addRewardButton, 5) != null;
    }

    /**
     * input reward type name on filter reward list
     * @param rewardName
     */
    public void setRewardTypeNameOnFilter(String rewardName) throws InterruptedException{
        selenium.hardWait(3);
        selenium.enterText(rewardNameFilter,rewardName,true);
    }

    /**
     * Click fliter button on reward list
     * @throws InterruptedException
     */
    public void clickOnFilterRewardList() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(filterButton);
    }

    /**
     * Click update button on reward list
     * @throws InterruptedException
     */
    public void clickOnUpdateRewardList() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(updateButton);
    }

    /**
     * set status reward list
     * @throws InterruptedException
     */
    public void setStatusRewardList() throws InterruptedException {
        selenium.hardWait(2);
        selenium.pageScrollInView(updateRewardButton);
        selenium.hardWait(5);
        selenium.javascriptClickOn(activeCheckbox);
        selenium.hardWait(5);
    }

    /**
     * Click update button on page detail reward
     * @throws InterruptedException
     */
    public void clickOnUpdateReward() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(updateRewardButton);
        selenium.waitInCaseElementVisible(yesUpdateRewardButton, 2);
        selenium.clickOn(yesUpdateRewardButton);
    }

    /**
     * input reward type name on filter user point
     * @param ownerUserPointName
     * @throws InterruptedException
     */
    public void setOwnerUserPointNameOnFilter(String ownerUserPointName) throws InterruptedException{
        selenium.hardWait(3);
        selenium.clickOn(ownerUserPointNameFilter);
        selenium.enterText(ownerUserPointNameFilter,ownerUserPointName,true);
        selenium.clickOn(searchButton);
    }
    /**
     * change whitelist to blacklist
     * @throws InterruptedException
     */
    public void changeWhitelistToBlacklist() throws InterruptedException{
        selenium.hardWait(3);
        selenium.clickOn(whitelistButton);
        selenium.hardWait(3);
        selenium.clickOn(yesDoItButton);
    }
    /**
     * see alert success
     * @throws InterruptedException
     */
    public boolean isAlertSuccessPresent() throws InterruptedException{
        selenium.hardWait(3);
        return selenium.waitTillElementIsVisible(successAlert,5)!=null;
    }
    /**
     * change blacklist to whitelist
     *
     */
    public void changeBlacklistToWhitelist() throws InterruptedException{
        selenium.hardWait(3);
        selenium.clickOn(blacklistButton);
        selenium.hardWait(3);
        selenium.clickOn(yesDoItButton);
    }

}
