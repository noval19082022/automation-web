package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class LeftMenuPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public LeftMenuPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[.='Segment']")
    private WebElement segmentMenu;

    @FindBy(xpath = "//a[contains(.,'Owner Point Setting')]")
    private WebElement ownerSettingMenu;

    @FindBy(xpath = "//span[.='Tenant Point Setting']")
    private WebElement tenantSettingMenu;

    @FindBy(xpath = "//a[contains(.,'User Point')]")
    private WebElement userPointMenu;

    @FindBy(xpath = "(//a[contains(.,'Activity')])[1]")
    private WebElement activityMenu;

    @FindBy(xpath = "//a[contains(.,'Owner Room Group')]")
    private WebElement ownerRoomGroupMenu;

    @FindBy(xpath = "//a[contains(.,'Kost Level')]")
    private WebElement kostLevelMenu;

    @FindBy(xpath = "//a[contains(.,'Room Level')]")
    private WebElement roomLevelMenu;

    @FindBy(xpath = "//a[contains(.,'Sales Motion')]")
    private WebElement salesMotionMenu;

    @FindBy(xpath = "//a[contains(.,'Property Package')]")
    private WebElement propertyPackageMenu;

    @FindBy(xpath = "//a[contains(.,'Hostile Owner')]")
    private WebElement hostileOwnerMenu;

    @FindBy(xpath = "//a[contains(.,'Kost Owner')]")
    private WebElement kostOwnerMenu;

    @FindBy(xpath = "//p[normalize-space()='Mamikos maintenance dulu, ya.']")
    private WebElement mamikosMaintenance;

    @FindBy(xpath = "//button[@class='maintenance__button']")
    private WebElement muatUlangButton;

    @FindBy(xpath = "//a[@id='room']")
    private WebElement kostMenu;

    @FindBy(xpath = "//a[@id='booking-users']")
    private WebElement dataBookingMenu;

    @FindBy(xpath = "//a[contains(.,'Reward Type')]")
    private WebElement rewardTypeMenu;

    @FindBy(xpath = "//a[contains(.,'Reward List')]")
    private WebElement rewardListMenu;

    @FindBy(xpath = "//a[contains(.,'User Point')]")
    private WebElement userPointMenuButton;

    @FindBy(id = "review")
    private WebElement kostReviewMenu;

    @FindBy(id = "tenant-survey")
    private WebElement tenantSurveyMenu;

    @FindBy(xpath = "//a[contains(@href, 'blacklist#')]")
    private WebElement ownerPointBlacklistMenu;

    @FindBy(xpath = "//a[contains(.,'Expiry')]")
    private WebElement expiryMenu;

    @FindBy(xpath = "//a[contains(.,'Chat Room')]")
    private WebElement chatRoomMenu;

    @FindBy(id = "consultant-mapping")
    private WebElement consultanMappingMenu;

    @FindBy(id = "room-additional")
    private WebElement kostAdditionalMenu;

    /**
     * Click on Segment Menu on Point Management Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnSegmentMenu() throws InterruptedException {
        selenium.pageScrollInView(segmentMenu);
        selenium.clickOn(segmentMenu);
    }

    /**
     * Click on Owner Setting Menu on Point Management Left Bar
     * @throws InterruptedException
     */
    public void clickOnOwnerSettingMenu() throws InterruptedException {
        selenium.pageScrollInView(ownerSettingMenu);
        selenium.clickOn(ownerSettingMenu);
    }

    /**
     * Click on Tenant Setting Menu on Point Management Left Bar
     * @throws InterruptedException
     */
    public void clickOnTenantSettingMenu() throws InterruptedException {
        selenium.pageScrollInView(tenantSettingMenu);
        selenium.clickOn(tenantSettingMenu);
    }

    /**
     * Click on User Point Menu on Point Management Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnUserPointMenu() throws InterruptedException {
        selenium.pageScrollInView(userPointMenu);
        selenium.clickOn(userPointMenu);
    }

    /**
     * Click on Activity on Point Management Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnActivityMenu() throws InterruptedException {
        selenium.pageScrollInView(activityMenu);
        selenium.clickOn(activityMenu);
    }

    /**
     * Click on Owner Room Group on Point Management Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnOwnerRoomGroupMenu() throws InterruptedException {
        selenium.pageScrollInView(ownerRoomGroupMenu);
        selenium.clickOn(ownerRoomGroupMenu);
    }

    /**
     * Click on Kost Level Menu on Level Management Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnKostLevelMenu() throws InterruptedException {
        selenium.pageScrollInView(kostLevelMenu);
        selenium.clickOn(kostLevelMenu);
    }

    /**
     * Click on Room Level Menu on Level Management Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnRoomLevelMenu() throws InterruptedException {
        selenium.pageScrollInView(roomLevelMenu);
        selenium.clickOn(roomLevelMenu);
    }

    /**
     * Click on Sales Motion Menu on Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnSalesMotionMenu() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(5);
        selenium.pageScrollInView(salesMotionMenu);
        selenium.clickOn(salesMotionMenu);
    }

    /**
     * Click on Sales Motion Menu on Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnPropertyPackageMenu() throws InterruptedException {
        selenium.hardWait(7);
        selenium.waitTillElementIsClickable(propertyPackageMenu);
        selenium.pageScrollInView(propertyPackageMenu);
        selenium.clickOn(propertyPackageMenu);
    }

    /**
     * Click on Hostile Owner Menu on Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnHostileOwnerMenu() throws InterruptedException {
        selenium.pageScrollInView(hostileOwnerMenu);
        selenium.clickOn(hostileOwnerMenu);
    }

    /**
     * Click on Hostile Owner Menu on Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnKostOwnerMenu() throws InterruptedException {
        selenium.pageScrollInView(kostOwnerMenu);
        selenium.clickOn(kostOwnerMenu);
    }
    /**
     * boolean for mamikos maintenance
     */
    public boolean isClickOnKostOwnerMenu() throws InterruptedException {
        return selenium.isElementDisplayed(mamikosMaintenance);
    }
    /**
     * if mamikos maintenance
     */
    public void clickMuatUlangButton() throws InterruptedException {
        selenium.clickOn(muatUlangButton);
    }
    /**
     * Scroll and Click Data Booking Menu on left bar
     * @throws InterruptedException
     */
    public void clickOnDataBookingMenu() throws InterruptedException {
        selenium.waitTillElementIsVisible(dataBookingMenu);
        selenium.pageScrollInView(dataBookingMenu);
        selenium.clickOn(dataBookingMenu);
    }

    /**
     * Click on sub menu of management level menu From Left Bar
     * @throws InterruptedException
     */
    public void clickOnSubMenuOfManagementLevel(String subMenu) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        if(Constants.ENV.equals("prod")){
            selenium.hardWait(5);
        }
        selenium.pageScrollInView(By.xpath("//a[@id='kost-level']/following-sibling::ul//span[text()='" + subMenu + "']"));
        selenium.clickOn(By.xpath("//a[@id='kost-level']/following-sibling::ul//span[text()='" + subMenu + "']"));
    }

    /**
     * Click on Kos Menu
     * @throws InterruptedException
     */
    public void clickOnKostMenu() throws InterruptedException {
        selenium.clickOn(kostMenu);
    }

    /**
     * Click on Reward Type Menu on Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnRewardTypeMenu() throws InterruptedException {
        selenium.hardWait(7);
        selenium.pageScrollInView(rewardTypeMenu);
        selenium.waitTillElementIsClickable(rewardTypeMenu);
        selenium.clickOn(rewardTypeMenu);
    }

    /**
     * Click on Reward List Menu on Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnRewardListMenu() throws InterruptedException {
        selenium.hardWait(7);
        selenium.pageScrollInView(rewardListMenu);
        selenium.waitTillElementIsClickable(rewardListMenu);
        selenium.clickOn(rewardListMenu);
    }
    /**
     * Click on User Point Menu on Left Bar
     *
     * @throws InterruptedException
     */
    public void userPointMenuButton() throws InterruptedException{
        selenium.hardWait(7);
        selenium.pageScrollInView(userPointMenuButton);
        selenium.waitTillElementIsClickable(userPointMenuButton);
        selenium.clickOn(userPointMenuButton);

    }

    /**
     * Click on Kost Review Menu
     * @throws InterruptedException
     */
    public void clickOnKostReviewMenu() throws InterruptedException {
        selenium.pageScrollInView(kostReviewMenu);
        selenium.clickOn(kostReviewMenu);
    }

    /**
     * Click on Tenant Survey Menu
     * @throws InterruptedException
     */
    public void clickOnTenantSurveyMenu() throws InterruptedException {
        selenium.pageScrollInView(tenantSurveyMenu);
        selenium.clickOn(tenantSurveyMenu);
    }

    /**
     * Click on Owner Point Blacklist Menu
     * @throws InterruptedException
     */
    public void clickOnOwnerPointBlacklistMenu() throws InterruptedException {
        selenium.pageScrollInView(ownerPointBlacklistMenu);
        selenium.clickOn(ownerPointBlacklistMenu);
    }

    /**
     * Click on Expiry Menu
     * @throws InterruptedException
     */
    public void clickOnExpiryMenu() throws InterruptedException {
        selenium.pageScrollInView(expiryMenu);
        selenium.clickOn(expiryMenu);
    }

    /**
     * Click on Segment Menu on Point Management Left Bar
     *
     * @throws InterruptedException
     */
    public void clickOnChatRoomMenu() throws InterruptedException {
        selenium.pageScrollInView(chatRoomMenu);
        selenium.clickOn(chatRoomMenu);
    }

    /**
     * Click on Consultant Mapping menu
     * @throws InterruptedException
     */
    public void clickOnConsultantMappingMenu() throws InterruptedException {
        selenium.pageScrollInView(consultanMappingMenu);
        selenium.clickOn(consultanMappingMenu);
    }

    /**
     * click Kost (Additional) Menu
     * @throws InterruptedException
     */
    public void clickKostAdditionalMenu() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsClickable(kostAdditionalMenu);
        selenium.clickOn(kostAdditionalMenu);
    }
}
