package pageobjects.mamikosAdmin;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class SegmentPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public SegmentPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(.,'Add Segment')]")
    private WebElement addSegmentButton;

    @FindBy(id = "segment-type")
    private WebElement segmentTypeField;

    @FindBy(id = "segment-title")
    private WebElement segmentTitleField;

    @FindBy(css = ".btn-primary")
    private WebElement saveButton;

    @FindBy(css = "b")
    private WebElement titleMessageAllert;

    @FindBy(xpath = "(//button[@class='close']/following-sibling::text())[2]")
    private WebElement contentMessageAllert;

    @FindBy(xpath = "(//tbody/tr/td[2])")
    private WebElement segmentList;

    @FindBy(css = "div.alert > li")
    private WebElement segmentValidation;

    @FindBy(id = "segment-kost-level-id")
    private WebElement kostLevelDropdown;

    @FindBy(id = "segment-room-level-id")
    private  WebElement roomLevelDropdown;

    @FindBy(id = "segment-target")
    private WebElement targetDropdown;

    @FindBy(xpath = "//th")
    private List<WebElement> segmentHeadTable;

    /**
     * Click on Add Segment Button
     * @throws InterruptedException
     */
    public void clickOnAddSegmentButton() throws InterruptedException {
        selenium.clickOn(addSegmentButton);
    }

    /**
     * Set new Segment Type
     * @param type Segment Type
     */
    public void setNewSegmentType(String type){
        selenium.enterText(segmentTypeField,type,true);
    }

    /**
     * Set new Segment Title
     * @param title Segment Title
     */
    public void setNewSegmentTitle(String title){
        selenium.enterText(segmentTitleField,title,true);
    }

    /**
     * Select Kost Level
     *
     * @param kostLevel kost level
     * @throws InterruptedException
     */
    public void selectKostLevelOnCreateSegmentPage(String kostLevel) throws InterruptedException {
        selenium.clickOn(kostLevelDropdown);
        selenium.clickOn(By.xpath("//*[@id='segment-kost-level-id']/option[.='"+kostLevel+"']"));
    }

    /**
     * Select Room Level
     *
     * @param roomLevel room level
     * @throws InterruptedException
     */
    public void selectRoomLevelOnCreateSegmentPage(String roomLevel) throws InterruptedException {
        selenium.clickOn(roomLevelDropdown);
        selenium.clickOn(By.xpath("//*[@id='segment-room-level-id']/option[.='"+roomLevel+"']"));
    }

    /**
     * Select Target
     * @param target target
     * @throws InterruptedException
     */
    public void selectTargetCreateSegmentPage(String target) throws InterruptedException {
        selenium.clickOn(targetDropdown);
        selenium.clickOn(By.xpath("//*[@id='segment-target']/option[.='"+target+"']"));
    }

    /**
     * Click on Save Button
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.clickOn(saveButton);
    }

    /**
     * Get Text of Title Message Allert in Segment Page
     * @return Text of Title Messsage Allert
     */
    public String getTitleMessageAllert() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(titleMessageAllert);
    }

    /**
     * Get Text of Content Message Allert in Segment Page
     *
     * @return Text of Content Messsage Allert
     */
    public String getContentMessageAllert(){
        WebElement myElement = driver.findElement(By.xpath("//div[@class='alert alert-success alert-dismissable']"));
        return ((JavascriptExecutor)driver).executeScript("return arguments[0].lastChild.textContent;", myElement).toString();
    }

    /**
     * Get Number of Segment List
     *
     * @return number of segment list
     */
    public int getAllSegmentNumber(){
        List<WebElement> segmentList = driver.findElements(By.xpath("(//tbody/tr/td[2])"));
        return segmentList.size();
    }

    /**
     * Get Text of Segment Name from Segment List
     *
     * @return text of Segment Name
     */
    public String getSegmentName(int index){
        List<WebElement> segmentList = driver.findElements(By.xpath("(//tbody/tr/td[2])"));
        return selenium.getText(segmentList.get(index));
    }


    /**
     * Check if New Segment Added is Present in Setting Page
     *
     * @param segmentName segment name
     * @return boolean
     */
    public boolean checkNewSegmentPresentInSettingPage(String segmentName){
        return selenium.waitInCaseElementPresent(By.xpath("//a[contains(.,'"+segmentName+"')]"),5)!=null;
    }

    /**
     * Click on Icon Edit Segment
     *
     * @param row segment
     * @throws InterruptedException
     */
    public void clickOnEditSegment(int row) throws InterruptedException {
        selenium.clickOn(By.xpath("//tbody[1]/tr["+row+"]//i[@class='fa fa-pencil']"));
    }

    /**
     * Click on Icon Delete Segment
     *
     * @param row segment
     * @throws InterruptedException
     */
    public void clickOnDeleteSegment(int row) throws InterruptedException {
        selenium.javascriptClickOn(By.xpath("//tbody[1]/tr["+row+"]//i[@class='fa fa-trash']"));
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * Check of Segment Is Present in Segment Page
     *
     * @param segmentName segment name
     * @return boolean
     */
    public boolean checkNewSegmentPresentInSegmentPage(String segmentName){
        return selenium.waitInCaseElementPresent(By.xpath("//td[contains(text(),'"+segmentName+"')]"),5)!=null;
    }

    /**
     * Get Text of Segment Validation
     *
     * @return text of validation message
     */
    public String getSegmentValidationMessage(){
        return selenium.getText(segmentValidation);
    }

    /**
     * Get Text of Head Table Segment by index
     * @param index - index head table
     * @return text of Head Table
     */
    public String getAllSegmentHeadTable (int index){
        System.out.println(selenium.getText(segmentHeadTable.get(index)));
        return selenium.getText(segmentHeadTable.get(index));
    }
}
