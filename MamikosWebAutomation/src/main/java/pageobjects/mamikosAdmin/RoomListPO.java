package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class RoomListPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public RoomListPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@name='room-name']")
    private WebElement roomNameSearchBox;

    @FindBy(xpath = "//*[@id='buttonSearch']")
    private WebElement searchRoomListButton;

    @FindBy(xpath = "//*[@title='Edit Room Level']")
    private WebElement editRoomLevelButton;

    @FindBy(xpath = "//*[@title='History']")
    private WebElement historyButton;

    @FindBy(xpath = "(//*[@class='btn btn-primary'])[1]")
    private WebElement assignAllButton;

    @FindBy(xpath = "(//*[@class='btn btn-primary'])[3]")
    private WebElement submitAssignButton;

    @FindBy(xpath = "//tbody/tr")
    private List<WebElement> roomListRow;

    /**
     * input text room name in room name search field
     * @param roomName
     */
    public void searchRoom(String roomName) throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.waitTillElementIsVisible(roomNameSearchBox);
        selenium.enterText(roomNameSearchBox,roomName,true);
    }

    /**
     * click button search
     * @throws InterruptedException
     */
    public void clickSearchRoomButton() throws InterruptedException {
        selenium.clickOn(searchRoomListButton);
    }

    /**
     * click button Edit Room Level
     * @throws InterruptedException
     */
    public void clickEditRoomLevel() throws InterruptedException {
        selenium.clickOn(editRoomLevelButton);
    }

    /**
     * click button history
     * @throws InterruptedException
     */
    public void clickHistory() throws InterruptedException {
        selenium.clickOn(historyButton);
    }

    /**
     * click assign all button
     * @throws InterruptedException
     */
    public void clickAssignAll() throws InterruptedException {
        selenium.clickOn(assignAllButton);
    }

    /**
     * assert actual room level is equal to expected level
     * @param level
     * @return boolen, true: if all room level is equal, false : if there is/are not equal room level
     */
    public boolean allRoomLevelNameEqual(String level) {
        boolean result = false;
        int x = 5;
        int y = 2;

        for (int i=0;i<roomListRow.size();i++){
            String actualLevel = selenium.getText(By.xpath("(//tbody/tr/td)["+x+"]"));
            String roomName = selenium.getText(By.xpath("(//tbody/tr/td)["+y+"]"));
            //compare actual and expected level name
            result = actualLevel.equalsIgnoreCase(level);
            //break if result is false, and give information about which room is not equals to expected level
            if (result == false){
                System.out.println("Room "+roomName+" Not Equals to "+level);
                System.out.println("Room Level in room "+roomName+" = "+actualLevel);
                break;
            }
            //adding for iterations
            x+=6;
            y+=6;
        }
        return result;
    }

    /**
     * click submit in assign all pop up
     * @throws InterruptedException
     */
    public void clickSubmitAssignPopUp() throws InterruptedException {
        selenium.clickOn(submitAssignButton);
    }
}
