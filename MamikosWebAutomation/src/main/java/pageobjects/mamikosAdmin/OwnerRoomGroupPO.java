package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class OwnerRoomGroupPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public OwnerRoomGroupPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[contains(.,'Add Owner Room Group')]")
    private WebElement addOwnerRoomGroupButton;

    @FindBy(id = "room-group-floor")
    private WebElement roomGroupFloorField;

    @FindBy(id = "room-group-ceil")
    private WebElement roomGroupFloorCeil;

    @FindBy(xpath = "(//tbody[1]//td[2])")
    private List<WebElement> roomGroupList;

    @FindBy(className = "pagination")
    private WebElement pagination;

    @FindBy(xpath = "//a[@title='Edit']")
    private List<WebElement> editButton;

    @FindBy(xpath = "//a[contains(@onclick, 'if')]")
    private List<WebElement> deleteButton;

    /**
     * Click on Add Owner Room Group Button
     *
     * @throws InterruptedException
     */
    public void clickOnAddOwnerRoomGroupButton() throws InterruptedException {
        selenium.clickOn(addOwnerRoomGroupButton);
    }

    /**
     * Enter Text to Room Group Floor Field
     *
     * @param floor room group floor
     */
    public void setRoomGroupFloor(String floor){
        selenium.enterText(roomGroupFloorField, floor , true );
    }

    /**
     * Enter Text to Room Group Floor Ceil
     *
     * @param ceil room group ceil
     */
    public void setRoomGroupCeil(String ceil){
        selenium.enterText(roomGroupFloorCeil, ceil, true);
    }

    /**
     * Check if Room Group is Present
     *
     * @param group room group
     * @return boolean
     */
    public boolean checkRoomGroupIsPresent(String group){
        return selenium.waitInCaseElementPresent(By.xpath("//td[contains(.,'"+group+"')]"), 5)!=null;
    }

    /**
     * Get Number of Owner Room Group List
     *
     * @return owner room group list number
     */
    public int getRoomGroupNumber(){
        return roomGroupList.size();
    }

    /**
     * Get Text of Owner Room Group
     *
     * @return owner room group text
     */
    public String getGroupText(int index){
        return selenium.getText(roomGroupList.get(index));
    }

    /**
     * Click on Icon Edit Room Group
     *
     * @param row group
     * @throws InterruptedException
     */
    public void clickOnEditRoomGroup(int row) throws InterruptedException {
        selenium.clickOn(By.xpath("//tbody[1]/tr["+row+"]//i[@class='fa fa-pencil']"));
    }

    /**
     * Click on Icon Delete Room Group
     *
     * @param row group
     */
    public void clickOnDeleteRoomGroup(int row) {
        selenium.javascriptClickOn(By.xpath("//tbody[1]/tr["+row+"]//i[@class='fa fa-trash']"));
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * Is Pagination Appear?
     * @return true or false
     */
    public Boolean isPaginationAppear(){
        return selenium.waitInCaseElementVisible(pagination, 3) != null;
    }

    /**
     * Get Table Title Index
     * @param index input integer that will match table head index
     * @return string
     */
    public String getTableTitleText(int index){
        return selenium.getText(By.xpath("//tr/th["+index+"]"));
    }

    /**
     * Get Add Owner Room Group Text
     * @return string
     */
    public String getAddOwnerRoomGroupText(){
        return selenium.getText(addOwnerRoomGroupButton);
    }

    /**
     * Is Edit Button Appeared?
     * @param index input integer that will match index of edit button
     * @return true or false
     */
    public Boolean isEditButtonAppeared(){
        return selenium.waitInCaseElementVisible(editButton.get(editButton.size()-1), 3) != null;
    }

    /**
     * Is Delete Button Appeared?
     * @param index input integer that will match index of delete button
     * @return true or false
     */
    public Boolean isDeleteButtonAppeared(){
        return selenium.waitInCaseElementVisible(deleteButton.get(editButton.size()-1), 3) != null;
    }

}
