package pageobjects.mamikosAdmin.SanjuniPero;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;
import java.util.Random;

public class SanjuniPeroPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SanjuniPeroPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    @FindBy(css = "#sanjunipero > span")
    private WebElement accessMenuSanjunipero;

    @FindBy(xpath = "//a[.='Parent']")
    private WebElement chooseParent;

    @FindBy(xpath = "//a[contains(.,'Add New Parent')]")
    private WebElement addNewParent;

    @FindBy(xpath = "//div[@id='biayaSewa_chosen']")
    private WebElement biayaSewa;

    @FindBy(xpath = "//li[normalize-space()='Daily']")
    private WebElement chooseBiayaSewaDaily;

    @FindBy(xpath = "//li[normalize-space()='Monthly']")
    private WebElement chooseBiayaSewaMonthly;

    @FindBy(xpath = "//li[normalize-space()='Weekly']")
    private WebElement chooseBiayaSewaWeekly;

    @FindBy(id = "slug")
    private WebElement slugInputText;

    @FindBy(css = "[value='Choose type']")
    private WebElement typeKostInputText;

    @FindBy(css = "[value='Choose biaya sewa']")
    private WebElement biayaSewaInputText;

    @FindBy(id = "titleTag")
    private WebElement tittleTagInputText;

    @FindBy(id = "titleHeader")
    private WebElement tittleHeaderInputText;

    @FindBy(id = "subtitle_header")
    private WebElement subtittleHeaderInputText;

    @FindBy(css = "[value='For facility filter']")
    private WebElement facilityTagInputText;

    @FindBy(xpath = "//li[.='All_goldplus']")
    private WebElement allGoldplus;

    @FindBy(css = "#biayaSewa_chosen [data-option-array-index='1']")
    private WebElement weekly;

    @FindBy(css = "li[data-option-array-index='51']")
    private WebElement aksesJam;

    @FindBy(css ="[for='active-record']")
    private WebElement active;

    @FindBy(css =".icheckbox_minimal")
    private WebElement checkboxActiveSanjunipero;

    @FindBy(css =".btn-primary")
    private WebElement saveButtonSanjunipero;

    @FindBy(css =".box-title")
    private WebElement backToMenuSanjunipero;

    @FindBy(name ="faq_question[]")
    private WebElement faqInputText;

    @FindBy(name ="faq_answer[]")
    private WebElement faqAnswerInputText;

    @FindBy(css =".alert > li:nth-of-type(1)")
    private WebElement checkSlugName;

    @FindBy(xpath ="(//i[@class='fa fa-external-link'])[1]")
    private WebElement actionPreview;


    @FindBy(id ="baseMainFilter")
    private WebElement semuaTipeKost;

    @FindBy(xpath ="//span[.='Tipe kos yang kamu cari berdasarkan gender.']")
    private WebElement tipeKost;

    @FindBy(xpath ="(//img[@class='rc-photo__cover'])[1]")
    private WebElement roomList;

    @FindBy(xpath ="//div[@id='biayaSewa_chosen']//ul[@class='chosen-choices']")
    private WebElement costRent;

    @FindBy(id ="sj-filter")
    private WebElement viewFilterDataKost;

    @FindBy(xpath ="//tbody[1]/tr[1]//a[2]")
    private WebElement deactiveAction;

    @FindBy(xpath ="//tbody/tr[1]")
    private WebElement deactiveTypePage;

    @FindBy(css =".activateParentSanjunipero")
    private WebElement enableAction;

    @FindBy(xpath ="(//tr[@class='active-sanjunipero-parent'])[1]")
    private WebElement enableTypePage;


    /**
     * search menu sanjunipero
     */

    public void accessMenuSanjunipero() {
        selenium.pageScrollInView(accessMenuSanjunipero);
    }

    /**
     * click menu parent
     */

    public void chooseParent() throws InterruptedException {
        selenium.clickOn(chooseParent);
    }

    /**
     * click add new parent
     */
    public void addNewParent() throws InterruptedException {
        selenium.clickOn(addNewParent);
    }
    /**
     * click biaya sewa
     */

    public void biayaSewa() throws InterruptedException {
        selenium.clickOn(biayaSewa);
    }
    /**
     * choose biaya sewa
     */

    public void chooseBiayaSewa(String Daily) throws InterruptedException {
        selenium.clickOn(chooseBiayaSewaDaily);
        selenium.clickOn(biayaSewa);
        selenium.clickOn(chooseBiayaSewaWeekly);
        selenium.clickOn(biayaSewa);
        selenium.clickOn(chooseBiayaSewaMonthly);
    }
    /**
     *
     * @param slug
     * @param typeKost
     * @param biayaSewa
     * @param tittleTag
     * @param tittleHeader
     * @param subtittleHeader
     * @param facilityTag
     * @throws InterruptedException
     */
    public void fillsMandetory(String slug, String typeKost, String biayaSewa, String tittleTag, String tittleHeader, String subtittleHeader, String facilityTag, String faq, String faqAnswer) throws InterruptedException {
        Random rand = new Random();
        // Generate random integers in range 0 to 999
        int rand_int1 = rand.nextInt(10000000);
            slug = slug + rand_int1;
        selenium.clickOn(slugInputText);
        selenium.enterText(slugInputText, slug, true);
        selenium.clickOn(typeKostInputText);
        selenium.enterText(typeKostInputText, typeKost, true);
        selenium.clickOn(allGoldplus);
        selenium.clickOn(biayaSewaInputText);
        selenium.enterText(biayaSewaInputText, biayaSewa, true);
        selenium.clickOn(weekly);
        selenium.clickOn(tittleTagInputText);
        selenium.enterText(tittleTagInputText, tittleTag,true);
        selenium.clickOn(tittleHeaderInputText);
        selenium.enterText(tittleHeaderInputText, tittleHeader, true);
        selenium.clickOn(subtittleHeaderInputText);
        selenium.enterText(subtittleHeaderInputText, subtittleHeader, true);
        selenium.clickOn(facilityTagInputText);
        selenium.enterText(facilityTagInputText, facilityTag, true);
        selenium.clickOn(aksesJam);
        selenium.clickOn(faqInputText);
        selenium.enterText(faqInputText, faq, true);
        selenium.clickOn(faqAnswerInputText);
        selenium.enterText(faqAnswerInputText, faqAnswer, true);
    }
    /**
     * click checkbox active sanjunipero
     */

    public void checkboxActiveSanjunipero() throws InterruptedException {
        selenium.pageScrollInView(active);
        selenium.clickOn(checkboxActiveSanjunipero);
        selenium.hardWait(5);
    }
    /**
     * click save button sanjunipero
     */

    public void saveButtonSanjunipero() throws InterruptedException {
        selenium.clickOn(saveButtonSanjunipero);
        selenium.hardWait(15);
    }
    /**
     * input slug name already exist
     */
    public void checkSlugNameAlreadyExist() throws InterruptedException {
        selenium.waitInCaseElementVisible(checkSlugName, 30);
        Random rand = new Random();
        int rand_int1 = rand.nextInt(100000);
        String slug = "Automation-slug";
        slug = slug + rand_int1;
        selenium.clickOn(slugInputText);
        selenium.enterText(slugInputText, slug, true);
        saveButtonSanjunipero();
        selenium.hardWait(5);
    }
    /**
     *
     * @param slug
     * @param typeKost
     * @param biayaSewa
     * @param tittleTag
     * @param tittleHeader
     * @param subtittleHeader
     * @param facilityTag
     * @throws InterruptedException
     */
    public void fillsMandatoryAlreadyExist(String slug, String typeKost, String biayaSewa, String tittleTag, String tittleHeader, String subtittleHeader, String facilityTag, String faq, String faqAnswer) throws InterruptedException {
        selenium.clickOn(slugInputText);
        selenium.enterText(slugInputText, slug, true);
        selenium.clickOn(typeKostInputText);
        selenium.enterText(typeKostInputText, typeKost, true);
        selenium.clickOn(allGoldplus);
        selenium.clickOn(biayaSewaInputText);
        selenium.enterText(biayaSewaInputText, biayaSewa, true);
        selenium.clickOn(weekly);
        selenium.clickOn(tittleTagInputText);
        selenium.enterText(tittleTagInputText, tittleTag,true);
        selenium.clickOn(tittleHeaderInputText);
        selenium.enterText(tittleHeaderInputText, tittleHeader, true);
        selenium.clickOn(subtittleHeaderInputText);
        selenium.enterText(subtittleHeaderInputText, subtittleHeader, true);
        selenium.clickOn(facilityTagInputText);
        selenium.enterText(facilityTagInputText, facilityTag, true);
        selenium.clickOn(aksesJam);
        selenium.clickOn(faqInputText);
        selenium.enterText(faqInputText, faq, true);
        selenium.clickOn(faqAnswerInputText);
        selenium.enterText(faqAnswerInputText, faqAnswer, true);
    }

    /**
     * click action preview
     */

    public void clickActionPreview() throws InterruptedException {
        selenium.clickOn(actionPreview);
    }
    /**
     * click semua tipe kost
     */

    public void clickSemuaTipeKost() throws InterruptedException {
        selenium.waitInCaseElementVisible(semuaTipeKost,5);
        selenium.clickOn(semuaTipeKost);
    }
    /**
     * click deactive button
     */

    public void clickDeactiveAction() throws InterruptedException {
        selenium.waitInCaseElementVisible(semuaTipeKost,5);
        selenium.javascriptClickOn(deactiveAction);
      //  selenium.acceptAlert();
    }
    /**
     * click deactive button
     */

    public void clickEnableAction() throws InterruptedException {
        selenium.waitInCaseElementVisible(semuaTipeKost,5);
        selenium.javascriptClickOn(enableAction);
      //  selenium.acceptAlert();
    }
    /**
     * tipe kost
     * @return
     */

    public boolean tipeKost() {
        return selenium.waitInCaseElementVisible(tipeKost, 5) != null;
    }

    /**
     * tipe kost
     * @return
     */

    public boolean roomList() {
        return selenium.waitInCaseElementVisible(roomList, 5) != null;
    }

    /**
     * cost rent
     * @return
     */

    public boolean costRent() {
        return selenium.waitInCaseElementVisible(costRent, 5) != null;
    }
    /**
     * view filter data kost
     * @return
     */

    public boolean viewFilterDataKost() {
        return selenium.waitInCaseElementVisible(viewFilterDataKost, 25) != null;
    }
    /**
     * view deactive type page
     * @return if deactive line type page is grey
     */

    public boolean deactiveParentSanjunipero(){
        return selenium.waitInCaseElementVisible(deactiveTypePage, 5) != null;
    }

    /**
     * view enable type page
     * @return if active line type page is green
     */

    public boolean enableParentSanjunipero(){
        return selenium.waitInCaseElementVisible(enableTypePage, 5) != null;
    }
}
