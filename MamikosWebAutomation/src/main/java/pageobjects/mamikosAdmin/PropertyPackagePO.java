package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

import java.util.List;

public class PropertyPackagePO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private JavaHelpers java = new JavaHelpers();

    public PropertyPackagePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//*[@name='filter-type']")
    private WebElement searchType;

    @FindBy(xpath = "//*[@id='contract-status']")
    private WebElement filterContractStatus;

    @FindBy(xpath = "//*[@id='filter-level']")
    private WebElement filterKostLevel;

    @FindBy(xpath = "//*[@class='search-button' and contains(text(),'Apply')]")
    private WebElement applyFilterButton;

    @FindBy(xpath = "//*[@id='search-input']")
    private WebElement searchPropertyPackageField;

    @FindBy(xpath = "//*[@class='search-button' and contains(text(),'Search')]")
    private WebElement searchPropertyPackage;

    @FindBy(xpath = "//tbody[1]//td[1]")
    private List <WebElement> propertyPackageList;

    @FindBy(xpath = "//tbody[1]//td[1]")
    private WebElement ownerNamePropertyPackage;

    @FindBy(xpath = "//tbody[1]//td[2]")
    private WebElement ownerPhonePropertyPackage;

    @FindBy(xpath = "//tbody[1]//td[3]")
    private WebElement levelPropertyPackage;

    @FindBy(xpath = "//tbody[1]//td[4]")
    private WebElement propertyName;

    @FindBy(xpath = "//*[@class='no-records-found']")
    private WebElement noRecord;

    @FindBy(xpath = "//*[@class='btn btn-default btn-sm dropdown-toggle']")
    private WebElement actionButton;

    @FindBy(xpath = "//*[@class='actions terminate-contract']")
    private WebElement terminateAction;

    @FindBy(xpath = "//*[@class='actions' and contains(text(),' See Package Detail')]")
    private WebElement seeDetailAction;

    @FindBy(xpath = "//*[@class='btn btn-primary']")
    private WebElement addNewPackageButton;

    @FindBy(id = "owner-phone")
    private WebElement ownerPhoneField;

    @FindBy(xpath = "//button[@id='search-owner-phone-button']")
    private WebElement searchButton;

    @FindBy(id = "property-name")
    private WebElement propertyNameField;

    @FindBy(id = "property-level")
    private WebElement propertyLevelField;

    @FindBy(id = "join-date")
    private WebElement joinDateField;

    @FindBy(xpath = "//*[@id='submit-form' and contains(text(),'Next')]")
    private WebElement nextButton;

    @FindBy(xpath = "//*[@class='btn btn-success']")
    private WebElement saveButton;

    @FindBy(xpath = "//*[@class='btn btn-default assign-all-rooms']")
    private WebElement assignButton;

    @FindBy(xpath = "//*[@class='swal2-confirm swal2-styled']")
    private WebElement yesButton;

    @FindBy(xpath = "//tbody[1]//td[5]")
    private WebElement kosLevel;

    @FindBy(xpath = "//*[@id='swal2-content']")
    private WebElement warningMessage;

    @FindBy(xpath = "//*[@class='actions' and contains(text(),'Update Contract')]")
    private WebElement updateAction;

    @FindBy(xpath = "//*[@class='checkbox-upgrade-gp2']")
    private WebElement upgradeButton;

    @FindBy(xpath = "//*[@id='submit-form']")
    private WebElement saveUpdateButton;

    @FindBy(xpath = "//*[@class='btn btn-default unassign-all-rooms']")
    private WebElement unassignButton;

    @FindBy(xpath = "//*[@class='actions add-new-property' and contains(text(),'Add New Property')]")
    private WebElement addNewPropertyAction;

    @FindBy(id = "property-list-dropdown")
    private WebElement propertyListPopUp;

    @FindBy(id = "go-to-assign")
    private WebElement savePropertyButton;

    @FindBy(id = "property-name-list")
    private WebElement propertyListOnDetail;

    @FindBy(xpath = "//*[@class='actions remove-property' and contains(text(),'Remove Property')]")
    private WebElement removePropertyAction;

    @FindBy(xpath = "//*[@id='select2-remove-property-list-container']")
    private WebElement removePropertyListPopUp;

    @FindBy(xpath = "//*[@class='btn btn-danger button-remove-property']")
    private WebElement removeButton;

    @FindBy(xpath = "//*[@class='actions delete-contract']")
    private WebElement deletePropertyContrctAction;

    @FindBy(xpath = "//*[@class='swal2-content']")
    private WebElement deleteSuccess;

    /**
     * Click on filter property package
     * @throws InterruptedException
     */
    public void clickOnFilter() throws InterruptedException {
        selenium.actionDropdownEnter(searchType, Keys.END);
    }

    /**
     * Set owner phone number that want to search
     *
     * @param ownerPropertyName of owner
     */
    public void searchOwner(String ownerPropertyName) throws InterruptedException {
        selenium.clearTextField(searchPropertyPackage);
        selenium.enterText(searchPropertyPackageField, ownerPropertyName,true);
    }

    /**
     * Click on search property package button
     * @throws InterruptedException
     */
    public void clickOnSearchPropertyPackage() throws InterruptedException {
        selenium.clickOn(searchPropertyPackage);
    }

    /**
     * Get list property package
     * @return numberOfElements is number of property package
     * @throws InterruptedException
     */
    public int getNumberOfList() throws InterruptedException {
        int numberOfElements = 0;
        if(selenium.waitInCaseElementVisible(ownerNamePropertyPackage, 5) != null){
            numberOfElements = propertyPackageList.size();
        }else{
            selenium.waitTillElementIsVisible(noRecord, 5);
        }
        return numberOfElements;
    }

    /**
     * Get status property package
     * @return Text of status property package
     */
    public String getStatusPropertyPackage(int index) throws InterruptedException {
        return selenium.getText(By.xpath("(//tbody[1]//td[8])[" + index + "]")); }

    /**
     * Click on action button
     * @throws InterruptedException
     */
    public void clickOnActionButton(int index) throws InterruptedException {
        selenium.hardWait(2);
        selenium.waitTillElementIsClickable(By.xpath("(//*[@class='btn btn-default btn-sm dropdown-toggle'])[" + index + "]"));
        selenium.clickOn(By.xpath("(//*[@class='btn btn-default btn-sm dropdown-toggle'])[" + index + "]"));
    }

    /**
     * Click on terminate option
     * @throws InterruptedException
     */
    public void clickOnTerminateContract() throws InterruptedException {
        selenium.waitTillElementIsVisible(terminateAction);
        selenium.hardWait(2);
        selenium.clickOn(terminateAction);
        selenium.clickOn(yesButton);
        selenium.hardWait(1);
        selenium.clickOn(yesButton);
    }

    /**
     * Click on detail package button
     * @throws InterruptedException
     */
    public void clickOnDetailPackage() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(actionButton);
        selenium.click(seeDetailAction);
        selenium.pageScrollInView(kosLevel);
    }

    /**
     * Click on add new package button
     * @throws InterruptedException
     */
    public void clickOnAddNewPackage() throws InterruptedException {
        selenium.waitTillElementIsVisible(addNewPackageButton);
        selenium.clickOn(addNewPackageButton);
    }

    /**
     * Set owner phone number that want to create package GP
     *
     * @param phone of owner
     */
    public void setOwnerPhoneField(String phone) throws InterruptedException {
        selenium.enterText(ownerPhoneField, phone, true);
    }

    /**
     * Click on search button to inquiry owner data
     * @throws InterruptedException
     */
    public void clickOnSearch() throws InterruptedException {
        selenium.clickOn(searchButton);
        selenium.hardWait(2);
    }

    /**
     * Click on property name
     */
    public void selectPropertyName(String propertyName) throws InterruptedException {
        selenium.waitTillElementIsVisible(propertyNameField);
        selenium.hardWait(2);
        selenium.selectDropdownValueByText(propertyNameField, propertyName);
    }

    /**
     * Click on property level GP 1
     * @throws InterruptedException
     */
    public void selectPropertyLevelGp1(String kostLevelGp1) throws InterruptedException {
        selenium.selectDropdownValueByText(propertyLevelField, kostLevelGp1);
    }

    /**
     * Click on property level GP 2
     * @throws InterruptedException
     */
    public void selectPropertyLevelGp2(String kostLevelGp2) throws InterruptedException {
        selenium.selectDropdownValueByText(propertyLevelField, kostLevelGp2);
    }

    /**
     * Click on property level GP 3
     * @throws InterruptedException
     */
    public void selectPropertyLevelGp3(String kostLevelGp3) {
        selenium.selectDropdownValueByText(propertyLevelField, kostLevelGp3);
    }

    /**
     * Click on join date
     * @throws InterruptedException
     */
    public void selectJoinDate() throws InterruptedException {
        selenium.clickOn(joinDateField);
        selenium.sendKeyEnter(joinDateField);
    }

    /**
     * Click on next button
     * @throws InterruptedException
     */
    public void clickOnNextButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.pageScrollInView(nextButton);
        selenium.clickOn(nextButton);
    }

    /**
     * Click on save button to saved mapping listing property
     * @throws InterruptedException
     */
    public void clickOnSaveButton() throws InterruptedException {
        selenium.waitTillElementIsVisible(saveButton);
        selenium.pageScrollInView(saveButton);
        selenium.clickOn(saveButton);
    }

    /**
     * Click on assign button to flag kost and room to GP
     * @throws InterruptedException
     */
    public void clickOnAssignButton() throws InterruptedException {
        selenium.hardWait(5);
        selenium.pageScrollInView(assignButton);
        selenium.clickOn(assignButton);
        selenium.hardWait(2);
        selenium.clickOn(yesButton);
    }

    /**
     * Get kost level
     * @return Text of Kost Level Name
     */
    public String getKosLevelLabel() throws InterruptedException {
        selenium.hardWait(10);
        return selenium.getText(kosLevel); }

    /**
     * Get warning message
     * @return Text of warning message
     */
    public String getWarningMessage(){ return selenium.getText(warningMessage); }

    /**
     * Click on update contract button
     * @throws InterruptedException
     */
    public void clickOnUpdateContract() throws InterruptedException {
        selenium.clickOn(updateAction);
        selenium.waitTillElementIsVisible(upgradeButton);
    }

    /**
     * Click on upgrade contract button
     * @throws InterruptedException
     */
    public void clickOnUpgradeContract() throws InterruptedException {
        selenium.clickOn(upgradeButton);
        selenium.pageScrollInView(saveUpdateButton);
        selenium.clickOn(saveUpdateButton);
        selenium.hardWait(2);
    }

    /**
     * Click on unassign button
     * @throws InterruptedException
     */
    public void clickOnUnassignButton() throws InterruptedException {
        selenium.pageScrollInView(unassignButton);
        selenium.clickOn(unassignButton);
        selenium.clickOn(yesButton);
        selenium.waitTillElementIsVisible(assignButton);
    }

    /**
     * Set owner name want to search
     *
     * @param ownerName of property package
     */
    public void searchOwnerByName(String ownerName) throws InterruptedException {
        selenium.clickOn(searchType);
        selenium.clickOn(By.xpath("//*[@name='filter-type']/option[2]"));
        selenium.clickOn(searchPropertyPackage);
        selenium.enterText(searchPropertyPackageField, ownerName,true);
    }

    /**
     * Set owner phone number want to search
     *
     * @param ownerPhone of property package
     */
    public void searchOwnerByPhone(String ownerPhone) throws InterruptedException {
        selenium.clickOn(searchType);
        selenium.clickOn(By.xpath("//*[@name='filter-type']/option[3]"));
        selenium.clickOn(searchPropertyPackage);
        selenium.enterText(searchPropertyPackageField, ownerPhone,true);
    }

    /**
     * Set property name want to search
     *
     * @param property of property package
     */
    public void searchByProperty(String property) throws InterruptedException {
        selenium.clickOn(searchType);
        selenium.clickOn(By.xpath("//*[@name='filter-type']/option[4]"));
        selenium.clickOn(searchPropertyPackage);
        selenium.enterText(searchPropertyPackageField, property,true);
    }

    /**
     * Get owner name
     * @return Text of owner name
     */
    public String getOwnerNamePropertyPackage() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(ownerNamePropertyPackage);
    }

    /**
     * Get owner phone
     * @return Text of owner phone
     */
    public String getOwnerPhonePropertyPackage() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(ownerPhonePropertyPackage);
    }

    /**
     * Get property name
     * @return Text of property name
     */
    public String getPropertyName() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(propertyName);
    }

    /**
     * Click on filter status
     * @throws InterruptedException
     */
    public void clickOnFilterStatus(String filterBy) throws InterruptedException {
        if(filterBy.equals("inactive")){
            selenium.selectDropdownValueByText(filterContractStatus, "Inactive");
            selenium.hardWait(1);
        }else if(filterBy.equals("active")){
            selenium.selectDropdownValueByText(filterContractStatus,"Active");
            selenium.hardWait(1);
        } else{
            selenium.selectDropdownValueByText(filterContractStatus,"Terminated");
            selenium.hardWait(1);
        }
        selenium.clickOn(applyFilterButton);
        selenium.hardWait(2);
    }

    /**
     * Click on filter kost level
     * @throws InterruptedException
     */
    public void clickOnFilterKostLevel(String filterBy) throws InterruptedException {
        if(filterBy.equals("Mamikos Goldplus 1 PROMO")){
            selenium.selectDropdownValueByText(filterKostLevel, "Mamikos Goldplus 1 PROMO");
            selenium.hardWait(1);
        }else if(filterBy.equals("Mamikos Goldplus 2 PROMO")){
            selenium.selectDropdownValueByText(filterKostLevel, "Mamikos Goldplus 2 PROMO");
            selenium.hardWait(1);
        } else{
            selenium.selectDropdownValueByText(filterKostLevel, "Mamikos Goldplus 3 PROMO");
            selenium.hardWait(1);
        }
        selenium.clickOn(applyFilterButton);
        selenium.hardWait(2);
    }

    /**
     * Get level property package
     * @return Text of level property package
     */
    public String getLevelPropertyPackage() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(levelPropertyPackage); }

    /**
     * Click on add new property action
     * @throws InterruptedException
     */
    public void clickOnAddNewProperty() throws InterruptedException {
        selenium.waitTillElementIsVisible(actionButton);
        selenium.hardWait(2);
        selenium.clickOn(addNewPropertyAction);
        selenium.waitTillElementIsVisible(propertyListPopUp);
    }

    /**
     * Click property list on detail contract
     * @throws InterruptedException
     */
    public void clickOnPropertyListOnDetail(String propertyNameAdded) throws InterruptedException {
        selenium.waitTillElementIsClickable(propertyListOnDetail);
        selenium.selectDropdownValueByText(propertyListOnDetail, propertyNameAdded);
    }

    /**
     * Click on remove property action
     * @throws InterruptedException
     */
    public void clickOnRemoveProperty() throws InterruptedException {
        selenium.hardWait(2);
        selenium.clickOn(removePropertyAction);
        selenium.waitTillElementIsVisible(removePropertyListPopUp);
    }

    /**
     * Click on property list want to remove
     * @throws InterruptedException
     */
    public void clickOnRemovePropertyButton() throws InterruptedException {
        selenium.clickOn(removeButton);
        selenium.waitTillElementIsVisible(yesButton);
        selenium.click(yesButton);
        selenium.hardWait(2);
    }

    /**
     * Click on delete contract action
     * @throws InterruptedException
     */
    public void clickOnDeleteContract() throws InterruptedException {
        selenium.clickOn(deletePropertyContrctAction);
        selenium.waitTillElementIsVisible(yesButton);
        selenium.hardWait(3);
        selenium.clickOn(yesButton);
    }

    /**
     * Get succes message
     * @return Text of succes message
     */
    public String getSuccessMessage() throws InterruptedException {
        selenium.hardWait(2);
        return selenium.getText(deleteSuccess); }


    /**
     * Select property name
     */
    public void selectProperty(String propertyName) {
        selenium.selectDropdownValueByText(propertyListPopUp, propertyName);
    }

    /**
     * Click on property list to add
     * @throws InterruptedException
     */
    public void clickOnSaveNewPropertyButton() throws InterruptedException {
        selenium.click(savePropertyButton);
        selenium.waitTillElementIsVisible(yesButton);
        selenium.hardWait(2);
        selenium.click(yesButton);
    }

    /**
     * Click on property to remove
     */
    public void clickOnPropertyToRemove(String propertyName) throws InterruptedException {
        selenium.clickOn(removePropertyListPopUp);
        selenium.hardWait(3);
        By element = By.xpath("//li[contains(.,'"+propertyName+"')]");
        selenium.clickOn(element);
    }
}
