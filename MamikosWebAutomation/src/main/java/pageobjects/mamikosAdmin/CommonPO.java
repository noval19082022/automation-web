package pageobjects.mamikosAdmin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.Collection;

public class CommonPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public CommonPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);

    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//div[@class='alert alert-success alert-dismissable']")
    private WebElement successAlert;

    @FindBy(xpath = "//div[@class='alert alert-danger alert-dismissable']/li")
    private WebElement dangerAlertMessage;

    @FindBy(id = "swal2-content")
    private WebElement popupMessage;

    @FindBy(xpath = "//button[text()='OK']")
    private WebElement okButton;

    /**
     * Get alert message
     * @return Message alert
     */
    public String getMessageAlert() {
        return selenium.getText(successAlert);
    }

    /**
     * Get danger alert message
     * @return Danger alert message
     */
    public String getMessageDangerAlert() {
        return selenium.getText(dangerAlertMessage);
    }

    /**
     * Get popup message
     * @throws InterruptedException
     * @return String Message
     */
    public String getPopupMessage() throws InterruptedException {
        String message = selenium.getText(popupMessage);
        selenium.clickOn(okButton);
        return message;
    }
}
