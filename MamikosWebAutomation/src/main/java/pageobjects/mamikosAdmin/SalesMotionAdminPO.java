package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;

public class SalesMotionAdminPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private JavaHelpers java = new JavaHelpers();

    public SalesMotionAdminPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[@class='btn btn-primary pull-right']")
    private WebElement createSalesMotion;

    @FindBy(id = "task-name")
    private WebElement taskNameField;

    @FindBy(id = "type")
    private WebElement typeTaskField;

    @FindBy(id = "division")
    private WebElement divisionField;

    @FindBy(id = "start-date")
    private WebElement startDateField;

    @FindBy(id = "due-date")
    private WebElement dueDateField;

    @FindBy(xpath = "//div[@class='datepicker-days']//th[@class='next']")
    private WebElement nextMonthIcon;

    @FindBy(id = "objective")
    private WebElement objectiveField;

    @FindBy(id = "tnc")
    private WebElement termConditionField;

    @FindBy(id = "requirement")
    private WebElement requirementField;

    @FindBy(id = "benefit")
    private WebElement benefitField;

    @FindBy(id = "participants")
    private WebElement participantField;

    @FindBy(id = "url-link")
    private WebElement urlLinkField;

    @FindBy(xpath = "//*[@class='btn btn-success pull-right']")
    private WebElement submitSalesMotion;

    @FindBy(xpath = "//*[@class='box-title']")
    private WebElement titlePage;

    @FindBy(id = "submit-assignee")
    private WebElement submitAssignee;

    @FindBy(xpath = "//*[@class='col-sm-6 text-center']")
    private WebElement detailSalesMotion;

    @FindBy(css = "input[placeholder='Search']")
    private WebElement searchField;

    @FindBy(css = "button[name='search']")
    private WebElement searchIcon;

    @FindBy(xpath = "//*[@class='btn btn-warning']")
    private WebElement editSalesMotion;

    @FindBy(xpath = "//ins[@class='iCheck-helper']")
    private WebElement checkBox;

    @FindBy(xpath = "//*[@class='btn btn-success pull-right']")
    private WebElement updateButton;

    @FindBy(id = "status")
    private WebElement statusOnDetail;

    @FindBy(css = "button[data-toggle-active='deactivate']")
    private WebElement deactiveButton;

    @FindBy(xpath = "//*[@class='swal2-confirm swal2-styled']")
    private WebElement confirmButton;

    @FindBy(xpath = "//tbody[1]//td[7]")
    private WebElement statusOnList;

    @FindBy(id = "due-date")
    private WebElement fieldDueDate;

    @FindBy(xpath = "//td[@class='day' and contains(text(),'28')]")
    private WebElement selectDueDate;

    @FindBy(id = "activate-button")
    private WebElement submitDueDate;

    @FindBy(css = "button[data-toggle-active='activate']")
    private WebElement activateButtonOnDetail;

    @FindBy(xpath = "//a[@class='btn btn-default']")
    private WebElement seeDetailButton;

    @FindBy(xpath = "//*[@class='box-title' and contains(text(),'Detail Assignee')]//a[@class='btn btn-default']")
    private WebElement editAssigneeButton;

    @FindBy(xpath = "//p[@id='updated-by']")
    private WebElement updatedByLabel;

    @FindBy(xpath = "//td//button[@class='btn btn-default']")
    private WebElement activateButtonOnList;

    @FindBy(xpath = "//*[@class='box-title' and contains(text(),'Sales Motion')]//a[@class='btn btn-default']")
    private WebElement editSalesMotionButton;

    /**
     * Click on create sales motion button
     * @throws InterruptedException
     */
    public void clickOnCreateSalesMotion() throws InterruptedException {
        selenium.waitTillElementIsVisible(createSalesMotion);
        selenium.clickOn(createSalesMotion);
    }

    /**
     * Set sales motion name
     *
     * @param task sales motion
     */
    public void setTaskName(String task){
        selenium.enterText(taskNameField, task, true);
    }

    /**
     * Click on type task of sales motion
     * @throws InterruptedException
     */
    public void clickOnTypeTask() throws InterruptedException {
        selenium.clickOn(typeTaskField);
        selenium.selectDropdownValueByIndex(typeTaskField,1);
    }

    /**
     * Click on division of sales motion
     * @throws InterruptedException
     */
    public void clickOnDivision() throws InterruptedException {
        selenium.clickOn(divisionField);
        selenium.selectDropdownValueByIndex(divisionField,1);
    }

    /**
     * Click on start date of sales motion
     * @param startDate sales motion
     */

    public void selectStartDateToday(String startDate) throws InterruptedException {
        selenium.clickOn(startDateField);
        selenium.hardWait(2);
        selenium.javascriptClickOn(By.xpath("//table[@class='table-condensed']//td[@class='day'][text()='" + startDate + "']"));
    }

    /**
     * Click on due date of sales motion
     * @param dueDate sales motion
     */
    public void selectDueDateTomorrow(String dueDate) throws InterruptedException {
        selenium.clickOn(dueDateField);
        selenium.hardWait(2);
        if (dueDate.equals("1")){
            selenium.clickOn(nextMonthIcon);
        }
        selenium.javascriptClickOn(By.xpath("//table[@class='table-condensed']//td[@class='day'][text()='" + dueDate + "']"));
    }

    /**
     * Set objective of sales motion
     *
     * @param objective sales motion
     */
    public void setObjective(String objective){
        selenium.enterText(objectiveField, objective, true);
    }

    /**
     * Set tnc of sales motion
     *
     * @param tnc sales motion
     */
    public void setTermCondition(String tnc){
        selenium.enterText(termConditionField, tnc, true);
    }

    /**
     * Set requirement of sales motion
     *
     * @param requirement sales motion
     */
    public void setRequirement(String requirement){
        selenium.enterText(requirementField, requirement, true);
    }

    /**
     * Set benefit of sales motion
     *
     * @param benefit sales motion
     */
    public void setBenefit(String benefit){
        selenium.enterText(benefitField, benefit, true);
    }

    /**
     * Set amount of participant sales motion
     *
     * @param participant sales motion
     */
    public void setParticipant(String participant){
        selenium.enterText(participantField, participant, true);
    }

    /**
     * Set url of sales motion
     *
     * @param url sales motion
     */
    public void setUrlLink(String url){
        selenium.enterText(urlLinkField, url, true);
    }

    /**
     * Click on submit sales motion
     * @throws InterruptedException
     */
    public void clickOnSubmitSalesMotion() throws InterruptedException {
        selenium.pageScrollInView(submitSalesMotion);
        selenium.clickOn(submitSalesMotion);
        selenium.waitTillElementIsVisible(titlePage);
    }

    /**
     * Click on consultant assignee of sales motion
     * @throws InterruptedException
     */
    public void clickOnConsultant() throws InterruptedException {
        By element;
        element = By.xpath("(//td[.='Admin Automation CRM'])");
        selenium.pageScrollInView(element);
        selenium.clickOn(element);
    }

    /**
     * Click on submit assignee sales motion
     * @throws InterruptedException
     */
    public void clickOnSubmitAssignee() throws InterruptedException {
        selenium.pageScrollInView(submitAssignee);
        selenium.clickOn(submitAssignee);
    }

    /**
     * Get detail page name of sales motion
     * @return Text of Kost Level Name
     */
    public String getDetailPage(){
        return selenium.getText(detailSalesMotion);
    }

    /**
     * Click on search button
     * @throws InterruptedException
     */
    public void searchSalesMotion(String task) throws InterruptedException {
        selenium.click(searchField);
        selenium.enterText(searchField,task, false);
        selenium.clickOn(searchIcon);
        selenium.hardWait(3);
    }

    /**
     * Click on edit sales motion button
     * @throws InterruptedException
     */
    public void clickOnEditSalesMotion() throws InterruptedException {
        selenium.clickOn(editSalesMotion);
        selenium.waitTillElementIsVisible(taskNameField);
    }

    /**
     * Click on update sales motion and update asignee
     * @throws InterruptedException
     */
    public void clickUpdateButton() throws InterruptedException {
        selenium.pageScrollInView(checkBox);
        selenium.javascriptClickOn(checkBox);
        selenium.hardWait(5);
        selenium.pageScrollInView(updateButton);
        selenium.clickOn(updateButton);
        selenium.hardWait(5);
        selenium.pageScrollInView(submitAssignee);
        selenium.clickOn(submitAssignee);
        selenium.hardWait(5);
    }

    /**
     * Get status on detail sales motion
     * @return Text of Status Sales motion
     */
    public String getStatusOnDetailSalesMotion() {
        selenium.pageScrollInView(statusOnDetail);
        return selenium.getText(statusOnDetail);
    }

    /**
     * Get status on list sales motion
     * @return Text of Status Sales motion
     */
    public String getStatusOnListSalesMotion() {
        return selenium.getText(statusOnList);
    }

    /**
     * Click on deactive button sales motion
     * @throws InterruptedException
     */
    public void clickOnDeactiveButton() throws InterruptedException {
        selenium.clickOn(deactiveButton);
        selenium.waitTillElementIsVisible(confirmButton);
        selenium.clickOn(confirmButton);
        selenium.click(confirmButton);
        selenium.hardWait(2);
    }

    /**
     * Click on activate button on list sales motion
     * @throws InterruptedException
     */
    public void clickOnSubmitDueDate() throws InterruptedException {
        selenium.clickOn(submitDueDate);
        selenium.clickOn(confirmButton);
        selenium.hardWait(2);
    }

    /**
     * Click on see detail sales motion button
     * @throws InterruptedException
     */
    public void clickOnSeeDetailButton() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.clickOn(seeDetailButton);
        selenium.waitTillElementIsVisible(editAssigneeButton);
    }

    /**
     * Click on assignee sales motion button
     * @throws InterruptedException
     */
    public void setAssigneeOtherConsultant() throws InterruptedException {
        selenium.javascriptClickOn(editAssigneeButton);
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(7);
        //assign/unassign consultant 1
        WebElement cons1 = driver.findElement(By.xpath("//td[text()='Admin Automation CRM']/parent::tr"));
        selenium.waitTillElementIsVisible(cons1);
        selenium.JSScrollAndClickOn(cons1);
        selenium.hardWait(3);
        //assign.unassign consultant 2
        WebElement cons2 = driver.findElement(By.xpath("//td[text()='Yudha']/parent::tr"));
        selenium.waitTillElementIsVisible(cons2);
        selenium.JSScrollAndClickOn(cons2);
        selenium.hardWait(3);
        selenium.clickOn(submitAssignee);
    }

    /**
     * Get consultant mapped sales motion
     * @return Text of consultant mapped Sales motion
     */
    public String getAssignee() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.hardWait(7);
        By element = By.xpath("//tbody/tr[1]");
        return selenium.getText(element);
    }

    /**
     * Get updated by sales motion
     * @return Text of consultant updated by Sales motion
     */
    public String getUpdatedBy() throws InterruptedException {
        return selenium.getText(updatedByLabel);
    }

    /**
     * Click on activate button on list sales motion
     * @throws InterruptedException
     */
    public void clickOnActivateButton() throws InterruptedException {
        selenium.clickOn(activateButtonOnList);
        selenium.waitTillElementIsVisible(dueDateField);
    }

    /**
     * Click on edit button on detail sales motion
     * @throws InterruptedException
     */
    public void clickOnEditSalesMotionDetail() throws InterruptedException {
        selenium.clickOn(editSalesMotionButton);
    }
}
