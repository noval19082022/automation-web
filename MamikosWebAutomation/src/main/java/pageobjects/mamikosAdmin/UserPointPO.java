package pageobjects.mamikosAdmin;

        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.FindBy;
        import org.openqa.selenium.support.PageFactory;
        import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
        import utilities.Constants;
        import utilities.SeleniumHelpers;
        import java.io.File;
        import java.util.List;

public class UserPointPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public UserPointPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@name='keyword']")
    private WebElement keywordSearchField;

    @FindBy(id = "buttonSearch")
    private WebElement searchButton;

    @FindBy(css = "tbody > tr")
    private List<WebElement> filterResultList;

    @FindBy(name = "user")
    private WebElement userDropdown;

    @FindBy(name = "status")
    private WebElement statusDropdown;

    @FindBy(xpath = "(//td[4])")
    private List<WebElement> userList;

    @FindBy(xpath = "(//td[6])")
    private List<WebElement> statusList;

    @FindBy(xpath = "//a[contains(.,'Total Point')]")
    private WebElement totalPointHeader;

    @FindBy(xpath = "(//td[5])")
    private List<WebElement> totalPointList;

    @FindBy(xpath = "//div[@class='modal-dialog']//h4[@class='modal-title']")
    private WebElement popUpConfirmationChangeStatusTitle;

    @FindBy(xpath = "//div[@class='modal-dialog']//div[@class='modal-body']//p")
    private WebElement popUpConfirmationChangeStatusBody;

    @FindBy(xpath = "//a[@title='Adjust Point']")
    private WebElement adjustPointIcon;

    @FindBy(name = "amount")
    private WebElement pointAmountField;

    @FindBy(name = "note")
    private WebElement noteField;

    @FindBy(xpath = "//div[@id='popup-adjust-point']//button[@class='btn btn-primary']")
    private WebElement submitButton;

    private By bulkAdjustPointButton = By.xpath("//a[contains(.,'Bulk Adjust Point')]");

    private By bulkUpdateBlacklistButton = By.xpath("//a[contains(.,'Bulk Update Blacklist')]");

    private By keywordFilter = By.xpath("//input[@name='keyword']");

    private By userFilter = By.xpath("//select[@name='user']");

    private By statusFilter = By.xpath("//select[@name='status']");

    private By searchButtonFilter = By.id("buttonSearch");

    private By nameListColumn = By.xpath("//th[.='Name']");

    private By emailListColumn = By.xpath("//th[.='Email']");

    private By phoneNumberListColumn = By.xpath("//th[.='Phone Number']");

    private By userListColumn = By.xpath("//th[.='User']");

    private By totalPointListColumn = By.xpath("//a[contains(.,'Total Point')]");

    private By statusListColumn = By.xpath("//th[.='Status']");

    private By adjustPointButton = By.cssSelector(".fa.fa-adjust");

    @FindBy (xpath = "//*[@title='History']")
    private WebElement historyButton;

    private By paginationList = By.cssSelector(".pagination");

    @FindBy(xpath = "//a[.='»']")
    private WebElement nextPageButton;

    @FindBy(xpath = "//li[@class='page-item active']/span")
    private WebElement currentPageIndexButton;

    @FindBy(xpath = "//a[.='«']")
    private WebElement previousPageButton;

    @FindBy(xpath = "//a[contains(.,'Back to User Point')]")
    private WebElement backtoUserPointbutton;

    @FindBy(css = ".content")
    private WebElement contentManagePointHistoryPage;

    @FindBy(name = "activity_id")
    private WebElement allActivityDropdown;

    @FindBy(id = "owner-point-expiry")
    private WebElement ownerPointExpiryInputText;

    @FindBy(id = "tenant-point-expiry")
    private WebElement tenantPointExpiryInputText;

    @FindBy(css = ".btn-primary")
    private WebElement pointExpirySaveButton;

    @FindBy(xpath = "(//*[@class='iCheck-helper'])[1]")
    private WebElement totalRoomCheckbox;

    @FindBy(name = "1[owner_received_payment_general][57][limit]")
    private WebElement pointLimitTextBox;

    @FindBy(xpath = "(//*[@class='btn btn-primary'])[71]")
    private WebElement saveTermsAndConditionButton;

    @FindBy(xpath = "//*[@class='alert alert-success alert-dismissable']")
    private WebElement successSaveTnCLabel;

    @FindBy(xpath = "//h3[@class='box-title']")
    private WebElement tenantPointSettingPage;

    @FindBy(xpath = "//*[@name='123[tenant_install_app][0][limit_type]']/following-sibling::input")
    private WebElement pointLimitInstallAppTextBox;

    @FindBy(xpath = "(//*[@class='btn btn-default'])[1]")
    private WebElement cancelInstallAppButton;

    @FindBy(xpath = "(//*[@class='btn btn-primary'])[1]")
    private WebElement saveInstallAppButton;

    @FindBy(xpath = "(//*[@class='btn btn-primary'])[37]")
    private WebElement saveTermsAndConditionTenantSettingButton;

    @FindBy(xpath = "//div[@id='popup-bulk-blacklist']//button[@class='btn btn-primary']")
    private WebElement submitBulkUpdateButton;

    @FindBy(xpath = "//div[@id='popup-bulk-adjust-point']//button[@class='btn btn-primary']")
    private WebElement submitBulkAdjustPointButton;

    @FindBy(xpath = "//td[6]/a[contains(.,'Blacklist')]")
    private WebElement blacklistStatus;

    /**
     * Set Keyword on Search Field
     *
     * @param keyword Keyword
     */
    public void setKeywordSearchField(String keyword) {
        selenium.enterText(keywordSearchField, keyword, false);
    }

    /**
     * Click on Search Button
     *
     * @throws InterruptedException
     */
    public void clickOnSearchButton() throws InterruptedException {
        selenium.clickOn(searchButton);
        selenium.hardWait(3);
    }

    /**
     * Get Number of Filter Result
     *
     * @return number of result
     */
    public int getFilterResultNumber() {
        return filterResultList.size();
    }

    /**
     * Get Text of Filter Result
     *
     * @param row    row data
     * @param column column data
     * @return Text of Filter Result
     */
    public String getFilterResultList(int row, int column) {
        return selenium.getText(By.xpath("//tbody/tr[" + row + "]/td[" + column + "]"));
    }

    /**
     * Click on User Filter Dropdown
     *
     * @throws InterruptedException
     */
    public void clickOnUserDropdown() throws InterruptedException {
        selenium.clickOn(userDropdown);
    }

    /**
     * Select User Filter
     *
     * @param userType Tenant or Owner
     * @throws InterruptedException
     */
    public void selectUserFilter(String userType) throws InterruptedException {
        selenium.clickOn(By.xpath("//option[text()='" + userType + "']"));
    }

    /**
     * Get Text of User Column from User Point List
     *
     * @param index row data user point
     * @return text of user column
     */
    public String getTextUserColumn(int index) {
        return selenium.getText(userList.get(index));
    }

    /**
     * Get Text of Status Column from User Point List
     *
     * @param index row data user point
     * @return text of user column
     */
    public String getTextStatusColumn(int index) {
        return selenium.getText(statusList.get(index));
    }

    /**
     * Click on Status Filter Dropdown
     *
     * @throws InterruptedException
     */
    public void clickOnStatusDropdown() throws InterruptedException {
        selenium.clickOn(statusDropdown);
    }

    /**
     * Select Status Filter
     *
     * @param status Blacklist or Whitelist
     * @throws InterruptedException
     */
    public void selectStatusFilter(String status) throws InterruptedException {
        selenium.clickOn(By.xpath("//option[text()='" + status + "']"));
    }

    /**
     * Click on Total Point Header
     *
     * @throws InterruptedException
     */
    public void clickOnTotalPointHeader() throws InterruptedException {
        selenium.clickOn(totalPointHeader);
    }

    /**
     * Get Text of Total Point from User Point List
     *
     * @param index row data
     * @return text of Total Point
     */
    public String getTextTotalPoint(int index) {
        return selenium.getText(totalPointList.get(index));
    }

    /**
     * Click on user point status
     *
     * @param initialStatus initial status of user point
     * @throws InterruptedException
     */
    public void clickOnUserPointStatus(String initialStatus) throws InterruptedException {
        selenium.clickOn(By.xpath("//td[6]/a[contains(.,'" + initialStatus + "')]"));
    }

    /**
     * Get Text of Title from Pop Up Confirmation Change Status
     *
     * @return text of Pop Up Confirmation Change Status Title
     */
    public String getTitlePopUpConfirmationChangeStatus() {
        return selenium.getText(popUpConfirmationChangeStatusTitle);
    }

    /**
     * Get Text of Body from Pop Up Confirmation Change Status
     *
     * @return text of Pop Up COnfirmation Change Status Body
     */
    public String getBodyPopUpConfirmationChangeStatus() {
        return selenium.getText(popUpConfirmationChangeStatusBody);
    }

    /**
     * Click on Button in Pop Up Confirmation
     *
     * @param confirmation to Whitelist or Blacklist
     * @throws InterruptedException
     */
    public void clickOnPopUpCOnfirmationButton(String confirmation) throws InterruptedException {
        selenium.clickOn(By.xpath("//button[text()='" + confirmation + "']"));
    }

    /**
     * Click on Adjust Point Icon
     *
     * @throws InterruptedException
     */
    public void clickOnAdjustPointIcon() throws InterruptedException {
        selenium.clickOn(adjustPointIcon);
    }

    /**
     * Choose Point Adjustment Type
     *
     * @param type point adjustmant type
     * @throws InterruptedException
     */
    public void choosePointAdjustmentType(String type) throws InterruptedException {
        selenium.clickOn(By.xpath("//label[contains(.,'" + type + "')]"));
    }

    /**
     * Set Point Amount
     *
     * @param pointAmount point amount
     */
    public void setPointAmount(String pointAmount) {
        selenium.enterText(pointAmountField, pointAmount, false);
    }

    /**
     * Set Point Adjustment Note
     *
     * @param note note
     */
    public void setPointAdjusmentNote(String note) {
        selenium.enterText(noteField, note, false);
    }

    /**
     * Click on Submit Adjust Point Button
     *
     * @throws InterruptedException
     */
    public void clickOnSubmitAdjustPointButton() throws InterruptedException {
        selenium.clickOn(submitButton);
    }

    /**
     * Verify search button is visible
     *
     * @return true if search button is visible
     */
    public boolean verifySearchButton() {
        return selenium.waitInCaseElementVisible(searchButton, 3) != null;
    }

    /**
     * Check if Bulk Adjust Point button is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkBulkAdjustPointButton() throws InterruptedException {
        return selenium.isElementPresent((By) bulkAdjustPointButton);
    }

    /**
     * Check if Bulk Adjust Point button is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkBulkUpdateBlacklistButton() throws InterruptedException {
        return selenium.isElementPresent((By) bulkUpdateBlacklistButton);
    }

    /**
     * Check if Keyword Filter is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkKeywordFilter() throws InterruptedException {
        return selenium.isElementPresent((By) keywordFilter);
    }

    /**
     * Check if User Filter is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkUserFilter() throws InterruptedException {
        return selenium.isElementPresent((By) userFilter);
    }

    /**
     * Check if Status Filter is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkStatusFilter() throws InterruptedException {
        return selenium.isElementPresent((By) statusFilter);
    }

    /**
     * Check if Search Button is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkSearchButton() throws InterruptedException {
        return selenium.isElementPresent((By) searchButtonFilter);
    }

    /**
     * Check if Name is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkName() throws InterruptedException {
        return selenium.isElementPresent((By) nameListColumn);
    }

    /**
     * Check if Email is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkEmail() throws InterruptedException {
        return selenium.isElementPresent((By) emailListColumn);
    }

    /**
     * Check if Phone Number is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkPhoneNumber() throws InterruptedException {
        return selenium.isElementPresent((By) phoneNumberListColumn);
    }

    /**
     * Check if User is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkUser() throws InterruptedException {
        return selenium.isElementPresent((By) userListColumn);
    }

    /**
     * Check if Total Point is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkTotalPoint() throws InterruptedException {
        return selenium.isElementPresent((By) totalPointListColumn);
    }

    /**
     * Check if Status is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkStatus() throws InterruptedException {
        return selenium.isElementPresent((By) statusListColumn);
    }

    /**
     * Check if Adjust Point is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkAdjustPointIcon() throws InterruptedException {
        return selenium.isElementPresent((By) adjustPointButton);
    }

    /**
     * Check if History is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkHistoryIcon() throws InterruptedException {
        return selenium.isElementDisplayed(historyButton);
    }

    /**
     * Check if Pagination is present inside Manage User Point
     *
     * @throws InterruptedException
     */
    public boolean checkPagination() throws InterruptedException {
        return selenium.isElementPresent((By) paginationList);
    }

    /**
     * Click On next page
     * @throws InterruptedException
     */
    public void clickNextPage() throws InterruptedException {
        if (selenium.isElementDisplayed(nextPageButton)) {
            selenium.clickOn(nextPageButton);
        }
    }

    /**
     * Click On previous page
     * @throws InterruptedException
     */
    public void clickPrevPage() throws InterruptedException {
        if (selenium.isElementDisplayed(previousPageButton)) {
            selenium.clickOn(previousPageButton);
        }
    }

    /**
     * Get current pagination index
     * @return String page index
     */
    public String getPageIndex() {
        String currentPage;
        if (selenium.isElementDisplayed(nextPageButton)) {
            currentPage = selenium.getText(currentPageIndexButton);
        }else
            currentPage = null;
        return currentPage;
    }

    /**
     * Click On history icon
     * @throws InterruptedException
     */
    public void clickHistoryIcon() throws InterruptedException {
        selenium.clickOn(historyButton);
    }

    /**
     * Click On Back to User Point Button
     * @throws InterruptedException
     */
    public void clickOnBacktoUserPointBurron() throws InterruptedException {
        if (selenium.isElementDisplayed(backtoUserPointbutton)) {
            selenium.clickOn(backtoUserPointbutton);
        }
    }

    /**
     * Click On Back to User Point Button
     * @throws InterruptedException
     */
    public void clickOn() throws InterruptedException {
        if (selenium.isElementDisplayed(backtoUserPointbutton)) {
            selenium.clickOn(backtoUserPointbutton);
        }
    }

    /**
     * Get all text on manage point history page
     * @return all text on manage point history page
     */
    public String getContentManagePointHistoryPage() {
        return selenium.getText(contentManagePointHistoryPage);
    }


    /**
     * Click on dropdown filter all activity
     * @value to choose which value we want to filter
     * @throws InterruptedException
     */
    public void chooseDropDownAllActivity(String value){
        selenium.selectDropdownValueByText(allActivityDropdown,value);
    }

    /**
     * Fill Owner Point Expiry
     * @param value input string that will be used to fill Owner Point Expiry
     */
    public void fillOwnerPointExpiry(String value){
        selenium.enterText(ownerPointExpiryInputText, value, true);
    }

    /**
     * Fill Tenant Point Expiry
     * @param value input string that will be used to fill Tenant Point Expiry
     */
    public void fillTenantPointExpiry(String value){
        selenium.enterText(tenantPointExpiryInputText, value, true);
    }

    /**
     * Click On Point Expiry Save button
     * @throws InterruptedException
     */
    public void clickOnPointExpirySaveButton() throws InterruptedException {
        selenium.clickOn(pointExpirySaveButton);
    }

    /**
     * Click On Checkbox total room
     * @throws InterruptedException
     */
    public void clickOnCheckboxTotalRoom() throws InterruptedException {
        selenium.javascriptClickOn(totalRoomCheckbox);
    }

    /**
     * Input Point Limit
     *
     * @param pointlimit Input to textbox
     */
    public void enterPointLimit(String pointlimit) {
        selenium.enterText(pointLimitTextBox, pointlimit, true);
    }

    /**
     * Click On save Terms and Condition
     * @throws InterruptedException
     */
    public void clickOnSaveTermsAndCondition() throws InterruptedException {
        selenium.clickOn(saveTermsAndConditionButton);
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * Check success save term and condition
     *
     * @return status true or false
     */
    public boolean successSaveTnCIsDisplayed() throws InterruptedException{
        selenium.hardWait(4);
        return selenium.waitInCaseElementVisible(successSaveTnCLabel, 5) != null;
    }

    /**
     * Check page manage tenant point setting is visible
     *
     * @throws InterruptedException
     */
    public boolean isPageTenantPointSettingVisible() throws InterruptedException {
        return selenium.waitInCaseElementVisible(tenantPointSettingPage,3) != null;
    }

    /**
     * Input Point Limit
     *
     * @param pointlimit Input to textbox
     */
    public void enterPointLimitInstallApp(String pointlimit) {
        selenium.enterText(pointLimitInstallAppTextBox, pointlimit, true);
    }

    /**
     * Click On Tenant Point Setting Install app cancel button
     * @throws InterruptedException
     */
    public void clickOnPInstallAppCancelButton() throws InterruptedException {
        selenium.javascriptClickOn(cancelInstallAppButton);
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * Click On Tenant Point Setting Install app save button
     * @throws InterruptedException
     */
    public void clickOnPInstallAppSaveButton() throws InterruptedException {
        selenium.javascriptClickOn(saveInstallAppButton);
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * Click On save Terms and Condition
     * @throws InterruptedException
     */
    public void clickOnSaveTermsAndConditionTenantSetting() throws InterruptedException {
        selenium.javascriptClickOn(saveTermsAndConditionTenantSettingButton);
        selenium.waitTillAlertPresent();
        selenium.acceptAlert();
    }

    /**
     * Click On Bulk Update Blacklist Button
     * @throws InterruptedException
     */
    public void clickOnBulkUpdateBlacklistButton() throws InterruptedException {
        selenium.javascriptClickOn(bulkUpdateBlacklistButton);
    }

    /**
     * Click On Bulk Update Blacklist Button
     * @throws InterruptedException
     */
    public void clickOnBulkAdjustPointButton() {
        selenium.javascriptClickOn(bulkAdjustPointButton);
    }

    /**
     * Check if pop up Bulk Update Blacklist is present
     *
     * @throws InterruptedException
     */
    public boolean checkBulkUpdateBlacklistPopUp(String popUp) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//h4[.='"+popUp+"']"));
        return selenium.waitInCaseElementVisible(element,3) !=null;
    }

    /**
     * Upload csv file for mass voucher
     * @throws InterruptedException
     */
    public void uploadCSVFile(String typeFile) throws InterruptedException {
        WebElement inpCSVFile = driver.findElement(By.cssSelector("input[name='csv_"+typeFile+"']"));
        String projectPath = System.getProperty("user.dir");
        String csvPath = "/src/main/resources/file/massVoucherFile.csv";
        File file = new File(projectPath + csvPath);
        inpCSVFile.sendKeys(file.getAbsolutePath());
        selenium.hardWait(3);
    }

    /**
     * Click On Submit Bulk Update Blacklist Button
     * @throws InterruptedException
     */
    public void clickOnSubmitBulkUpdateButton() {
        selenium.javascriptClickOn(submitBulkUpdateButton);
    }

    /**
     * Click On Submit Bulk Adjust Point Button
     * @throws InterruptedException
     */
    public void clickOnSubmitBulkAdjustPointButton(){
        selenium.javascriptClickOn(submitBulkAdjustPointButton);
    }

    /**
     * Set default status to Whitelist
     * @throws InterruptedException
     */
    public void setDefaultStatusToWhitelist() throws InterruptedException {
        if (selenium.isElementDisplayed(blacklistStatus)) {
            selenium.clickOn(blacklistStatus);
            selenium.clickOn(By.xpath("//button[text()='Yes, Do It!']"));
            selenium.hardWait(3);
        }
    }

}