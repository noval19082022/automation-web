package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class ListKostLevelPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public ListKostLevelPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//a[@class='btn btn-primary']")
    private WebElement addKostLevelButton;

    @FindBy(xpath = "//i[@class='fa fa-trash']")
    private WebElement trashIcon;

    @FindBy(xpath = "//a[@title='Edit']")
    private WebElement updateIcon;

    @FindBy(xpath = "//input[@value='DELETE']")
    private WebElement okButton;

    @FindBys(@FindBy(xpath = "//table[@class='table table-striped']//tr/td"))
    private List<WebElement> valueColumn;

    @FindBy(xpath = "//a[@rel='next']")
    private WebElement nextPageButton;

    @FindBy(xpath = "//input[@placeholder='Level Name']")
    private WebElement levelNameEditText;

    @FindBy(id = "buttonSearch")
    private WebElement searchButton;

    @FindBy(xpath = "//table[@class='table table-striped']//tbody/tr")
    private WebElement listKostLevelNotEmpty;

    /**
     * Search kost level
     * @param levelName
     */
    public void searchKostLevel(String levelName) throws InterruptedException {
        selenium.enterText(levelNameEditText, levelName, true);
        selenium.clickOn(searchButton);
    }

    /**
     * List kost level is appeared
     * @return boolean status list kost level
     */
    public boolean listKostLevelIsAppeared() {
        return selenium.isElementDisplayed(listKostLevelNotEmpty);
    }

    /**
     * Click on next page button
     * @throws InterruptedException
     */
    public void clickOnNextPageButton() throws InterruptedException {
            selenium.clickOn(nextPageButton);
    }

    /**
     * Click on page number button
     * @throws InterruptedException
     */
    public void clickOnPageNumber(String pageNumber) throws InterruptedException {
        selenium.javascriptClickOn(By.xpath("//a[@class='page-link'][text()='" + pageNumber + "']"));
    }

    /**
     * Verify page number button is active
     * @return String attribute value
     */
    public String pageNumberButtonIsActive(String pageNumber) {
        return selenium.getElementAttributeValue(By.xpath("//span[@class='page-link'][text()='" + pageNumber +"']/parent::li"), "class");
    }

    /**
     * Click on add kost level button
     * @throws InterruptedException
     */
    public void clickOnAddKostLevelButton() throws InterruptedException {
        selenium.clickOn(addKostLevelButton);
    }

    /**
     * Get value of each column
     * @param index
     */
    public String getValueColumnKostLevel(int index) {
        return selenium.getText(valueColumn.get(index));
    }

    /**
     * Click on delete icon
     * @throws  InterruptedException
     */
    public void clickOnDeleteKostLevelIcon() throws InterruptedException {
        selenium.waitForJavascriptToLoad();
        selenium.pageScrollInView(trashIcon);
        selenium.doubleClickOnElement(trashIcon);
    }

    /**
     * Click on update icon
     */
    public void clickOnUpdateKostLevelIcon() {
        selenium.pageScrollInView(updateIcon);
        selenium.doubleClickOnElement(updateIcon);
    }
}
