package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

public class KostPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public KostPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "//input[@placeholder='Nama Kost']")
    private WebElement kosNameSearch;

    @FindBy(xpath = "//tbody[1]/tr[1]//button[@class='btn btn-default dropdown-toggle']")
    private WebElement firstActionButton;

    @FindBy(xpath = "//a[contains(.,'Thanos')]")
    private WebElement thanosButton;


    /**
     * Search kos name then hit enter
     * @param kosName is Kos Name
     */
    public void searchKosName(String kosName) throws InterruptedException {
        selenium.enterText(kosNameSearch, kosName, true);
        kosNameSearch.sendKeys(Keys.ENTER);
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementVisible(By.xpath("//table[@id='tableListDesigner']//tr[1]/td[3]/strong[contains(text(), '" + kosName + "')]"), 5);
    }

    /**
     * Click on first action button
     */
    public void clickOnFirstActionButton() {
        selenium.javascriptClickOn(firstActionButton);
    }

    /**
     * Click on first thanos button
     * @throws InterruptedException
     */
    public void clickOnFirstThanosButton() throws InterruptedException {
        this.clickOnFirstActionButton();
        selenium.hardWait(2);
        selenium.waitForJavascriptToLoad();
        selenium.waitInCaseElementVisible(thanosButton,3);
        selenium.clickOn(thanosButton);
        selenium.hardWait(1);
    }

}
