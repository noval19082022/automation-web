package pageobjects.mamikosAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.Constants;
import utilities.SeleniumHelpers;

import java.util.List;

public class TransferDepositTenantPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public TransferDepositTenantPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.PAGEFACTORY_WAIT_DURATION), this);
    }

    /*
     * All WebElements are identified by @FindBy annotation
     *
     * @FindBy can accept tagName, partialLinkText, name, linkText, id, css,
     * className, xpath as attributes.
     */

    @FindBy(xpath = "(//*[@class='transfer-deposit__button transfer-deposit__button-default open_modal'])[1]")
    private WebElement detailButton;

    @FindBy(id = "transferModalLabel")
    private WebElement transferDepositPopUp;

    @FindBy(css = "[data-target='#markAsTransferred114']")
    private WebElement sudahTransferButton;

    @FindBy(id = "markAsTransferredLabel")
    private WebElement tandaiSudahTransferPopUp;

    @FindBy(css = "[data-target='#transferModal114']")
    private WebElement transferUlangButton;

    @FindBy(name = "search_value")
    private WebElement searchColumn;

    @FindBy(xpath = "//table[@class='table table-hover table-bordered transfer-deposit__table-paid']//th")
    private List<WebElement> nameColumn;

    @FindBy(xpath = "//*[@class='form-group']/following::label")
    private List<WebElement> fieldLabel;

    @FindBy(xpath = "(//*[@class='btn btn-default'])[1]")
    private WebElement backButton;

    @FindBy(xpath = "(//*[@class='transfer-deposit__button transfer-deposit__button-success'])[1]")
    private WebElement receiptButton;

    @FindBy(xpath = "//*[@class='callout callout-success']")
    private WebElement successDownloadText;

    @FindBy(css = "[data-target='#transferModal125']")
    private WebElement transferButton;

    @FindBy(xpath = "(//*[@class='btn btn-success mark-the-transfer'])[1]")
    private WebElement transferUlangButtonPopUp;

    @FindBy(xpath = "(//button[@class='btn btn-default']/following-sibling::a)[1]")
    private WebElement downloadBuktiTransferButton;

    @FindBy(name = "search_value")
    WebElement invoiceNumberTextBox;

    @FindBy(xpath = "(//h3)[2]")
    private WebElement invalidPhoneNumberText;

    @FindBy(xpath = "//input[@class='btn btn-success btn-md']")
    WebElement cariInvoiceButton;

    /**
     * Choose Tab on Transfer Sisa Deposit Managament
     * @throws InterruptedException
     */
    public void chooseTabOnTransferDeposit(String tab) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("//h4[.='"+tab+"']"));
        selenium.clickOn(element);
    }

    /**
     * Click on Detail Button
     * @throws InterruptedException
     */
    public void clickOnDetailButton() throws InterruptedException {
        selenium.clickOn(detailButton);
    }

    /**
     * Verify pop up appear
     * @return true if page appear
     * @throws InterruptedException
     */
    public boolean popUpDetailTransferAppear() throws InterruptedException{
        selenium.hardWait(3);
        return selenium.waitInCaseElementVisible(transferDepositPopUp,5)!=null;
    }

    /**
     * Get text Title pop up detail transfer
     * @return Title pop up detail transfer
     */
    public String getTitlePopUpDetailTransfer() {
        return selenium.getText(transferDepositPopUp);
    }

    /**
     * Click on Tandai Sudah Transfer Button
     * @throws InterruptedException
     */
    public void clickOnTandaiSudahTransferButton() throws InterruptedException {
        selenium.clickOn(sudahTransferButton);
    }

    /**
     * Verify pop up appear on tab failed
     * @return true if page appear
     * @throws InterruptedException
     */
    public boolean popUpTandaiSudahTransferAppear() throws InterruptedException{
        selenium.hardWait(3);
        return selenium.waitInCaseElementVisible(tandaiSudahTransferPopUp,5)!=null;
    }

    /**
     * Get text Title pop up tandai sudah transfer
     * @return Title pop up tandai sudah transfer
     */
    public String getTitlePopUpTandaiSudahTransfer() {
        return selenium.getText(tandaiSudahTransferPopUp);
    }

    /**
     * Click on Transfer Ulang Button
     * @throws InterruptedException
     */
    public void clickOnTransferUlangButton() throws InterruptedException {
        selenium.clickOn(transferUlangButton);
    }

    /**
     * search column is appeared
     * @return boolean search column is appeared
     */
    public boolean searchColumnIsAppeared() {
        return selenium.isElementDisplayed(searchColumn);
    }

    /**
     * Get value of each column
     * @param index
     */
    public String getNameColumnTabFailed(int index) {
        return selenium.getText(nameColumn.get(index));
    }

    /**
     * all label on pop up is appeared
     * @return boolean label is appeared
     */
    public boolean labelOnPopUpIsAppeared(int index) {
        return selenium.isElementDisplayed(fieldLabel.get(index));
    }

    /**
     * button back is appeared
     * @return boolean button back is appeared
     */
    public boolean buttonBackIsAppeared() {
        return selenium.isElementDisplayed(backButton);
    }

    /**
     * Click on Receipt Button
     * @throws InterruptedException
     */
    public void clickOnReceiptButton() throws InterruptedException {
        selenium.clickOn(receiptButton);
    }

    /**
     * Get message success download
     * @return string
     */
    public String getSuccessDownloadMessage() {
        return selenium.getText(successDownloadText);
    }

    /**
     * Click on Transfer Button
     * @throws InterruptedException
     */
    public void clickOnTransferButton() throws InterruptedException {
        selenium.clickOn(transferButton);
    }

    /**
     * Click on Back Button
     * @throws InterruptedException
     */
    public void clickOnBackButton() throws InterruptedException{
        selenium.clickOn(backButton);
    }

    /**
     * Click on Transfer Ulang Button
     * @throws InterruptedException
     */
    public void clickOnTransferUlangButtonPopUp() throws InterruptedException{
        selenium.clickOn(transferUlangButtonPopUp);
    }

    /**
     * button download bukti transfer is appeared
     * @return boolean button download is appeared
     */
    public boolean buttonDownloadIsAppeared() {
        return selenium.isElementDisplayed(downloadBuktiTransferButton);
    }

    /**
     * user input phone number
     *
     * @throws InterruptedException
     */
    public void inputPhoneNumber(String phoneNumber) throws InterruptedException {
        selenium.hardWait(2);
        selenium.enterText(invoiceNumberTextBox,phoneNumber,true);
    }

    /**
     * field package invoice list
     *
     * @throws InterruptedException
     */
    public void verifyListShortByPhoneNumber(String phoneNumber) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("(//*[contains(text(),'"+phoneNumber+"')])[1]"));
        selenium.isElementDisplayed(element);
    }

    /**
     * get message search invalid phone number
     * @throws InterruptedException
     * @return text invalid phone number
     */
    public String getInvalidPhoneNumberText() throws InterruptedException{
        selenium.hardWait(2);
        return selenium.getText(invalidPhoneNumberText);
    }

    /**
     * click button search on page transfer sisa deposit management
     * @throws InterruptedException
     */
    public void clickOnSearchButton() throws InterruptedException {
        selenium.hardWait(3);
        selenium.clickOn(cariInvoiceButton);
    }
}
