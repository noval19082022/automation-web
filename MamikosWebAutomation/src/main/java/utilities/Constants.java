package utilities;

public class Constants
{
	public static final String PROPERTYFILE="src/main/resources/constants.properties";	
	
	//Environment
	public static final String ENV = JavaHelpers.setSystemVariable(PROPERTYFILE,"Env");
	
	//Application URL
	public static final String MAMIKOS_URL = JavaHelpers.getPropertyValue(PROPERTYFILE,"mamikos_url_" + ENV);
	public static final String MAMIKOS_URL2 = JavaHelpers.getPropertyValue(PROPERTYFILE, "mamikos_url2_" + ENV);
	public static final String OWNER_URL = JavaHelpers.getPropertyValue(PROPERTYFILE,"owner_url_" + ENV);
	public static final String BACKOFFICE_URL = JavaHelpers.getPropertyValue(PROPERTYFILE,"backoffice_url_" + ENV);
	public static final String BACKOFFICE_BASE_URL = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_base_url_" + ENV);
	public static final String CONSULTANT_URL = JavaHelpers.getPropertyValue(PROPERTYFILE, "consultant_url_" + ENV);
	public static final String ADMIN_URL = JavaHelpers.getPropertyValue(PROPERTYFILE, "admin_url_login_" + ENV);
	public static final String SINGGAHSINI_URL = JavaHelpers.getPropertyValue(PROPERTYFILE, "singgahSini_url_" + ENV);
	public static final String PMS_SINGGAHSINI_URL = JavaHelpers.getPropertyValue(PROPERTYFILE, "pms_singgahSini_url_" + ENV);

	//Owner Login Details
	public static final String OWNER_PHONE= JavaHelpers.getPropertyValue(PROPERTYFILE, "owner_phone_" + ENV);
	public static final String OWNER_PASSWORD= JavaHelpers.getPropertyValue(PROPERTYFILE,"owner_password_" + ENV);
	public static final String OWNER_PHONE_MARS= JavaHelpers.getPropertyValue(PROPERTYFILE, "owner_phone_mars_" + ENV);
	public static final String OWNER_PASSWORD_MARS= JavaHelpers.getPropertyValue(PROPERTYFILE,"owner_password_mars_" + ENV);
	public static final String TENANT_PHONE_MARS= JavaHelpers.getPropertyValue(PROPERTYFILE, "tenant_phone_mars_" + ENV);
	public static final String TENANT_PASSWORD_MARS= JavaHelpers.getPropertyValue(PROPERTYFILE,"tenant_password_mars_" + ENV);
	public static final String OWNER_NAME = JavaHelpers.getPropertyValue(PROPERTYFILE,"owner_name_" + ENV);
	public static final String OWNER_PHONE_MARS_2= JavaHelpers.getPropertyValue(PROPERTYFILE, "owner_phone_mars_2_" + ENV);
	public static final String OWNER_PASSWORD_MARS_2= JavaHelpers.getPropertyValue(PROPERTYFILE,"owner_password_mars_2_" + ENV);

	//Consultant Login Details
	public static final String CONSULTANT_EMAIL= JavaHelpers.getPropertyValue(PROPERTYFILE, "consultant_email_" + ENV);
	public static final String CONSULTANT_PASSWORD= JavaHelpers.getPropertyValue(PROPERTYFILE,"consultant_password_" + ENV);
	
	//Tenant Facebook Login Details
	public static final String TENANT_FACEBOOK_EMAIL= JavaHelpers.getPropertyValue(PROPERTYFILE, "tenant_facebook_email_" + ENV);
	public static final String TENANT_FACEBOOK_PASSWORD= JavaHelpers.getPropertyValue(PROPERTYFILE,"tenant_facebook_password_" + ENV);
	public static final String TENANT_FACEBOOK_NAME= JavaHelpers.getPropertyValue(PROPERTYFILE,"tenant_facebook_name_" + ENV);

	//Tenant Facebook Login Details
	public static final String BNI_PAYMENT= JavaHelpers.getPropertyValue(PROPERTYFILE, "bni_payment_" + ENV);
	public static final String MANDIRI_PAYMENT= JavaHelpers.getPropertyValue(PROPERTYFILE, "mandiri_payment_" + ENV);
	public static final String PERMATA_PAYMENT= JavaHelpers.getPropertyValue(PROPERTYFILE, "permata_payment_" + ENV);

	//Back Office login details
	public static final String BACKOFFICE_LOGIN_EMAIL= JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_login_email_" + ENV);
	public static final String BACKOFFICE_LOGIN_PASSWORD= JavaHelpers.getPropertyValue(PROPERTYFILE,"backoffice_login_password_" + ENV);
	public static final String BACKOFFICE_LOGIN_EMAIL_BACKOFFICE= JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_login_email_backoffice_" + ENV);
	public static final String BACKOFFICE_LOGIN_PASSWORD_BACKOFFICE= JavaHelpers.getPropertyValue(PROPERTYFILE,"backoffice_login_password_backoffice_" + ENV);
	public static final String BACKOFFICE_PMAN01_EMAIL = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_PMAN01_email_" + ENV);
	public static final String BACKOFFICE_PMAN01_PASSWORD = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_PMAN01_password_" + ENV);
	public static final String BACKOFFICE_PMAN02_EMAIL = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_PMAN02_email_" + ENV);
	public static final String BACKOFFICE_PMAN02_PASSWORD = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_PMAN02_password_" + ENV);
	public static final String BACKOFFICE_PMAN03_EMAIL = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_PMAN03_email_" + ENV);
	public static final String BACKOFFICE_PMAN03_PASSWORD = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_PMAN03_password_" + ENV);
	public static final String BACKOFFICE_BBM01_EMAIL = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_BBM01_email_" + ENV);
	public static final String BACKOFFICE_BBM01_PASSWORD = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_BBM01_password_" + ENV);
	public static final String BACKOFFICE_BBM02_EMAIL = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_BBM02_email_" + ENV);
	public static final String BACKOFFICE_BBM02_PASSWORD = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_BBM02_password_" + ENV);
	public static final String BACKOFFICE_BBM03_EMAIL = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_BBM03_email_" + ENV);
	public static final String BACKOFFICE_BBM03_PASSWORD = JavaHelpers.getPropertyValue(PROPERTYFILE, "backoffice_BBM03_password_" + ENV);

	//Selenium constants
	public static final int WEBDRIVER_WAIT_DURATION= Integer.parseInt(JavaHelpers.getPropertyValue(PROPERTYFILE,"WebDriverWaitDuration"));
	public static final int MINIMUM_WEBDRIVER_WAIT_DURATION= Integer.parseInt(JavaHelpers.getPropertyValue(PROPERTYFILE,"MinimumWebDriverWaitDuration"));
	public static final int PAGEFACTORY_WAIT_DURATION= Integer.parseInt(JavaHelpers.getPropertyValue(PROPERTYFILE,"PageFactoryWaitDuration"));
	public static final int PAGELOAD_WAIT_DURATION= Integer.parseInt(JavaHelpers.getPropertyValue(PROPERTYFILE,"PageLoadTimeout"));
	
	//Other
	public static final String SCREENSHOT_LOCATION= JavaHelpers.getPropertyValue(PROPERTYFILE,"ScreenshotLocation");
}


