package utilities;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

public class CookiesHelpers {
    WebDriver driver;
    SeleniumHelpers selenium;
    JavaHelpers java = new JavaHelpers();

    public CookiesHelpers(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);
    }

    /**
     *Store cookies data to desired path and name
     * @param pathName folder path name
     * @param fileName file name
     */
    public void storeCookiesData(String pathName,String fileName) {
        File file = new File(System.getProperty("user.dir") + pathName + fileName );
        try{
            //file.delete();
            //file.createNewFile();
            FileWriter fileWrite = new FileWriter(file);
            BufferedWriter bWrite = new BufferedWriter(fileWrite);
            for(Cookie ck : driver.manage().getCookies()) {
                bWrite.write((ck.getName())+";"
                        +ck.getValue()+";"
                        +ck.getDomain()+";"
                        +ck.getPath()+";"
                        +ck.getExpiry()+";"
                        +ck.isSecure());
                bWrite.newLine();
            }
            bWrite.close();
            fileWrite.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Write cookies data from file to web browser
     * @param pathName folder path name
     * @param fileName file name
     * @throws IOException
     */
    public void writeCookiesData(String pathName, String fileName) throws IOException, InterruptedException {
        String todayNameMonthDateClock = java.getTimeStamp("MMM dd HH:mm:ss");
        int yearsPlusFive = Integer.parseInt(java.getTimeStamp("yyyy")) + 5;
        String targetExpiryDate = todayNameMonthDateClock + " WIB " + yearsPlusFive;
        SimpleDateFormat format = new SimpleDateFormat("MMM dd HH:mm:ss z yyyy");

        try{
            File file = new File(System.getProperty("user.dir") + pathName+ fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader buffReader = new BufferedReader(fileReader);
            String strline;
            while ((strline=buffReader.readLine())!=null){
                StringTokenizer token = new StringTokenizer(strline, ";");
                while (token.hasMoreTokens()) {
                    String name = token.nextToken();
                    String value = token.nextToken();
                    String domain = token.nextToken();
                    String path = token.nextToken();
                    Date expiry = null;
                    String val;
                    if(!(val=token.nextToken()).equals("null")){
                        expiry = format.parse(targetExpiryDate);
                    }
                    boolean isSecure = Boolean.parseBoolean(token.nextToken());
                    Cookie ck = new Cookie(name, value, domain, path, expiry, isSecure);
                    System.out.println(ck);
                    selenium.addCookies(ck);
                }
            }
            System.out.println("Write cookies data from " + fileName + " success");
        } catch (FileNotFoundException | ParseException e) {
            e.printStackTrace();
        }
        selenium.hardWait(2);
    }

    /**
     * Clear all cookies data
     */
    public void clearAllCookiesData() throws InterruptedException {
        selenium.deleteAllCookies();
        selenium.hardWait(2);
    }
}
